﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"








extern "C" void Context_t2636657155_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Context_t2636657155_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Context_t2636657155_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Context_t2636657155_0_0_0;
extern "C" void Escape_t169451053_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Escape_t169451053_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Escape_t169451053_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Escape_t169451053_0_0_0;
extern "C" void PreviousInfo_t581002487_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void PreviousInfo_t581002487_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void PreviousInfo_t581002487_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType PreviousInfo_t581002487_0_0_0;
extern "C" void DelegatePInvokeWrapper_AppDomainInitializer_t3898244613();
extern const RuntimeType AppDomainInitializer_t3898244613_0_0_0;
extern "C" void DelegatePInvokeWrapper_Swapper_t2637371637();
extern const RuntimeType Swapper_t2637371637_0_0_0;
extern "C" void DictionaryEntry_t3048875398_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DictionaryEntry_t3048875398_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DictionaryEntry_t3048875398_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType DictionaryEntry_t3048875398_0_0_0;
extern "C" void Slot_t2022531261_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Slot_t2022531261_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Slot_t2022531261_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Slot_t2022531261_0_0_0;
extern "C" void Slot_t2267560602_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Slot_t2267560602_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Slot_t2267560602_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Slot_t2267560602_0_0_0;
extern "C" void Enum_t2459695545_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Enum_t2459695545_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Enum_t2459695545_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Enum_t2459695545_0_0_0;
extern "C" void DelegatePInvokeWrapper_ReadDelegate_t3184826381();
extern const RuntimeType ReadDelegate_t3184826381_0_0_0;
extern "C" void DelegatePInvokeWrapper_WriteDelegate_t489908132();
extern const RuntimeType WriteDelegate_t489908132_0_0_0;
extern "C" void MonoIOStat_t1621921065_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoIOStat_t1621921065_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoIOStat_t1621921065_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoIOStat_t1621921065_0_0_0;
extern "C" void MonoEnumInfo_t2335995564_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoEnumInfo_t2335995564_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoEnumInfo_t2335995564_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoEnumInfo_t2335995564_0_0_0;
extern "C" void CustomAttributeNamedArgument_t94157543_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CustomAttributeNamedArgument_t94157543_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CustomAttributeNamedArgument_t94157543_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CustomAttributeNamedArgument_t94157543_0_0_0;
extern "C" void CustomAttributeTypedArgument_t1498197914_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CustomAttributeTypedArgument_t1498197914_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CustomAttributeTypedArgument_t1498197914_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CustomAttributeTypedArgument_t1498197914_0_0_0;
extern "C" void ILTokenInfo_t149559338_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ILTokenInfo_t149559338_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ILTokenInfo_t149559338_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ILTokenInfo_t149559338_0_0_0;
extern "C" void MonoEventInfo_t2190036573_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoEventInfo_t2190036573_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoEventInfo_t2190036573_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoEventInfo_t2190036573_0_0_0;
extern "C" void MonoMethodInfo_t3646562144_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoMethodInfo_t3646562144_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoMethodInfo_t3646562144_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoMethodInfo_t3646562144_0_0_0;
extern "C" void MonoPropertyInfo_t486106184_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoPropertyInfo_t486106184_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoPropertyInfo_t486106184_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoPropertyInfo_t486106184_0_0_0;
extern "C" void ParameterModifier_t1820634920_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ParameterModifier_t1820634920_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ParameterModifier_t1820634920_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ParameterModifier_t1820634920_0_0_0;
extern "C" void ResourceCacheItem_t333236149_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ResourceCacheItem_t333236149_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ResourceCacheItem_t333236149_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ResourceCacheItem_t333236149_0_0_0;
extern "C" void ResourceInfo_t3933049236_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ResourceInfo_t3933049236_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ResourceInfo_t3933049236_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ResourceInfo_t3933049236_0_0_0;
extern "C" void DelegatePInvokeWrapper_CrossContextDelegate_t754146990();
extern const RuntimeType CrossContextDelegate_t754146990_0_0_0;
extern "C" void DelegatePInvokeWrapper_CallbackHandler_t362827733();
extern const RuntimeType CallbackHandler_t362827733_0_0_0;
extern "C" void SerializationEntry_t3485203212_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SerializationEntry_t3485203212_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SerializationEntry_t3485203212_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SerializationEntry_t3485203212_0_0_0;
extern "C" void StreamingContext_t1417235061_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void StreamingContext_t1417235061_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void StreamingContext_t1417235061_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType StreamingContext_t1417235061_0_0_0;
extern "C" void DSAParameters_t1872138834_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DSAParameters_t1872138834_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DSAParameters_t1872138834_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType DSAParameters_t1872138834_0_0_0;
extern "C" void RSAParameters_t1462703416_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RSAParameters_t1462703416_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RSAParameters_t1462703416_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RSAParameters_t1462703416_0_0_0;
extern "C" void SecurityFrame_t1002202659_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SecurityFrame_t1002202659_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SecurityFrame_t1002202659_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SecurityFrame_t1002202659_0_0_0;
extern "C" void DelegatePInvokeWrapper_ThreadStart_t3437517264();
extern const RuntimeType ThreadStart_t3437517264_0_0_0;
extern "C" void ValueType_t3507792607_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ValueType_t3507792607_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ValueType_t3507792607_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ValueType_t3507792607_0_0_0;
extern "C" void DelegatePInvokeWrapper_ReadMethod_t3362229488();
extern const RuntimeType ReadMethod_t3362229488_0_0_0;
extern "C" void DelegatePInvokeWrapper_UnmanagedReadOrWrite_t1990215745();
extern const RuntimeType UnmanagedReadOrWrite_t1990215745_0_0_0;
extern "C" void DelegatePInvokeWrapper_WriteMethod_t1894833619();
extern const RuntimeType WriteMethod_t1894833619_0_0_0;
extern "C" void ifaddrs_t2532459533_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ifaddrs_t2532459533_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ifaddrs_t2532459533_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ifaddrs_t2532459533_0_0_0;
extern "C" void in6_addr_t4035827331_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void in6_addr_t4035827331_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void in6_addr_t4035827331_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType in6_addr_t4035827331_0_0_0;
extern "C" void ifaddrs_t937751619_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ifaddrs_t937751619_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ifaddrs_t937751619_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ifaddrs_t937751619_0_0_0;
extern "C" void in6_addr_t1014645363_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void in6_addr_t1014645363_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void in6_addr_t1014645363_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType in6_addr_t1014645363_0_0_0;
extern "C" void sockaddr_dl_t3955242742_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void sockaddr_dl_t3955242742_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void sockaddr_dl_t3955242742_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType sockaddr_dl_t3955242742_0_0_0;
extern "C" void sockaddr_in6_t834146887_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void sockaddr_in6_t834146887_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void sockaddr_in6_t834146887_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType sockaddr_in6_t834146887_0_0_0;
extern "C" void sockaddr_in6_t2899168071_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void sockaddr_in6_t2899168071_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void sockaddr_in6_t2899168071_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType sockaddr_in6_t2899168071_0_0_0;
extern "C" void sockaddr_ll_t1681025498_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void sockaddr_ll_t1681025498_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void sockaddr_ll_t1681025498_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType sockaddr_ll_t1681025498_0_0_0;
extern "C" void Win32_IP_ADAPTER_ADDRESSES_t680756680_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Win32_IP_ADAPTER_ADDRESSES_t680756680_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Win32_IP_ADAPTER_ADDRESSES_t680756680_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Win32_IP_ADAPTER_ADDRESSES_t680756680_0_0_0;
extern "C" void Win32_MIB_IFROW_t4215928996_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Win32_MIB_IFROW_t4215928996_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Win32_MIB_IFROW_t4215928996_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Win32_MIB_IFROW_t4215928996_0_0_0;
extern "C" void DelegatePInvokeWrapper_SocketAsyncCall_t3737776727();
extern const RuntimeType SocketAsyncCall_t3737776727_0_0_0;
extern "C" void SocketAsyncResult_t2959281146_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SocketAsyncResult_t2959281146_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SocketAsyncResult_t2959281146_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SocketAsyncResult_t2959281146_0_0_0;
extern "C" void X509ChainStatus_t4278378721_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void X509ChainStatus_t4278378721_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void X509ChainStatus_t4278378721_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType X509ChainStatus_t4278378721_0_0_0;
extern "C" void IntStack_t273560425_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void IntStack_t273560425_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void IntStack_t273560425_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType IntStack_t273560425_0_0_0;
extern "C" void Interval_t2354235237_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Interval_t2354235237_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Interval_t2354235237_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Interval_t2354235237_0_0_0;
extern "C" void DelegatePInvokeWrapper_CostDelegate_t1824458113();
extern const RuntimeType CostDelegate_t1824458113_0_0_0;
extern "C" void UriScheme_t1876590943_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UriScheme_t1876590943_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UriScheme_t1876590943_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType UriScheme_t1876590943_0_0_0;
extern "C" void DelegatePInvokeWrapper_Action_t3226471752();
extern const RuntimeType Action_t3226471752_0_0_0;
extern "C" void NsDecl_t3210081295_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void NsDecl_t3210081295_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void NsDecl_t3210081295_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType NsDecl_t3210081295_0_0_0;
extern "C" void AnimationCurve_t3306541151_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AnimationCurve_t3306541151_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AnimationCurve_t3306541151_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AnimationCurve_t3306541151_0_0_0;
extern "C" void AnimationEvent_t2428323300_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AnimationEvent_t2428323300_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AnimationEvent_t2428323300_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AnimationEvent_t2428323300_0_0_0;
extern "C" void AnimatorTransitionInfo_t2410896200_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AnimatorTransitionInfo_t2410896200_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AnimatorTransitionInfo_t2410896200_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AnimatorTransitionInfo_t2410896200_0_0_0;
extern "C" void DelegatePInvokeWrapper_LogCallback_t1867914413();
extern const RuntimeType LogCallback_t1867914413_0_0_0;
extern "C" void DelegatePInvokeWrapper_LowMemoryCallback_t642977590();
extern const RuntimeType LowMemoryCallback_t642977590_0_0_0;
extern "C" void AssetBundleRequest_t2674559435_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AssetBundleRequest_t2674559435_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AssetBundleRequest_t2674559435_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AssetBundleRequest_t2674559435_0_0_0;
extern "C" void AsyncOperation_t3814632279_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AsyncOperation_t3814632279_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AsyncOperation_t3814632279_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AsyncOperation_t3814632279_0_0_0;
extern "C" void DelegatePInvokeWrapper_PCMReaderCallback_t3007145346();
extern const RuntimeType PCMReaderCallback_t3007145346_0_0_0;
extern "C" void DelegatePInvokeWrapper_PCMSetPositionCallback_t421863554();
extern const RuntimeType PCMSetPositionCallback_t421863554_0_0_0;
extern "C" void DelegatePInvokeWrapper_AudioConfigurationChangeHandler_t3743753033();
extern const RuntimeType AudioConfigurationChangeHandler_t3743753033_0_0_0;
extern "C" void DelegatePInvokeWrapper_WillRenderCanvases_t3522132132();
extern const RuntimeType WillRenderCanvases_t3522132132_0_0_0;
extern "C" void Collision_t2876846408_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Collision_t2876846408_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Collision_t2876846408_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Collision_t2876846408_0_0_0;
extern "C" void Collision2D_t1539500754_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Collision2D_t1539500754_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Collision2D_t1539500754_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Collision2D_t1539500754_0_0_0;
extern "C" void ContactFilter2D_t1672660996_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ContactFilter2D_t1672660996_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ContactFilter2D_t1672660996_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ContactFilter2D_t1672660996_0_0_0;
extern "C" void ControllerColliderHit_t4070855101_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ControllerColliderHit_t4070855101_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ControllerColliderHit_t4070855101_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ControllerColliderHit_t4070855101_0_0_0;
extern "C" void Coroutine_t2299508840_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Coroutine_t2299508840_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Coroutine_t2299508840_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Coroutine_t2299508840_0_0_0;
extern "C" void DelegatePInvokeWrapper_CSSMeasureFunc_t2092170507();
extern const RuntimeType CSSMeasureFunc_t2092170507_0_0_0;
extern "C" void CullingGroup_t1091689465_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CullingGroup_t1091689465_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CullingGroup_t1091689465_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CullingGroup_t1091689465_0_0_0;
extern "C" void DelegatePInvokeWrapper_StateChanged_t2480912210();
extern const RuntimeType StateChanged_t2480912210_0_0_0;
extern "C" void DelegatePInvokeWrapper_DisplaysUpdatedDelegate_t3423469815();
extern const RuntimeType DisplaysUpdatedDelegate_t3423469815_0_0_0;
extern "C" void Event_t3028476042_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Event_t3028476042_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Event_t3028476042_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Event_t3028476042_0_0_0;
extern "C" void DelegatePInvokeWrapper_UnityAction_t4025899511();
extern const RuntimeType UnityAction_t4025899511_0_0_0;
extern "C" void FailedToLoadScriptObject_t3605283378_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void FailedToLoadScriptObject_t3605283378_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void FailedToLoadScriptObject_t3605283378_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType FailedToLoadScriptObject_t3605283378_0_0_0;
extern "C" void DelegatePInvokeWrapper_FontTextureRebuildCallback_t1272078033();
extern const RuntimeType FontTextureRebuildCallback_t1272078033_0_0_0;
extern "C" void Gradient_t3600583008_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Gradient_t3600583008_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Gradient_t3600583008_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Gradient_t3600583008_0_0_0;
extern "C" void DelegatePInvokeWrapper_WindowFunction_t3486805455();
extern const RuntimeType WindowFunction_t3486805455_0_0_0;
extern "C" void GUIContent_t4210063000_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GUIContent_t4210063000_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GUIContent_t4210063000_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GUIContent_t4210063000_0_0_0;
extern "C" void DelegatePInvokeWrapper_SkinChangedDelegate_t3594822336();
extern const RuntimeType SkinChangedDelegate_t3594822336_0_0_0;
extern "C" void GUIStyle_t1799908754_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GUIStyle_t1799908754_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GUIStyle_t1799908754_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GUIStyle_t1799908754_0_0_0;
extern "C" void GUIStyleState_t3801000545_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GUIStyleState_t3801000545_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GUIStyleState_t3801000545_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GUIStyleState_t3801000545_0_0_0;
extern "C" void HostData_t3480691970_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void HostData_t3480691970_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void HostData_t3480691970_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType HostData_t3480691970_0_0_0;
extern "C" void HumanBone_t1529896151_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void HumanBone_t1529896151_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void HumanBone_t1529896151_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType HumanBone_t1529896151_0_0_0;
extern "C" void DownloadHandler_t1216180266_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DownloadHandler_t1216180266_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DownloadHandler_t1216180266_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType DownloadHandler_t1216180266_0_0_0;
extern "C" void DownloadHandlerBuffer_t3443159558_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DownloadHandlerBuffer_t3443159558_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DownloadHandlerBuffer_t3443159558_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType DownloadHandlerBuffer_t3443159558_0_0_0;
extern "C" void UnityWebRequest_t254341728_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UnityWebRequest_t254341728_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UnityWebRequest_t254341728_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType UnityWebRequest_t254341728_0_0_0;
extern "C" void UploadHandler_t3552561393_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UploadHandler_t3552561393_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UploadHandler_t3552561393_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType UploadHandler_t3552561393_0_0_0;
extern "C" void UploadHandlerRaw_t3420491431_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UploadHandlerRaw_t3420491431_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UploadHandlerRaw_t3420491431_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType UploadHandlerRaw_t3420491431_0_0_0;
extern "C" void Object_t1021602117_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Object_t1021602117_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Object_t1021602117_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Object_t1021602117_0_0_0;
extern "C" void PlayableBinding_t2498078091_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void PlayableBinding_t2498078091_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void PlayableBinding_t2498078091_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType PlayableBinding_t2498078091_0_0_0;
extern "C" void RaycastHit_t87180320_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RaycastHit_t87180320_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RaycastHit_t87180320_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RaycastHit_t87180320_0_0_0;
extern "C" void RaycastHit2D_t4063908774_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RaycastHit2D_t4063908774_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RaycastHit2D_t4063908774_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RaycastHit2D_t4063908774_0_0_0;
extern "C" void RectOffset_t3387826427_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RectOffset_t3387826427_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RectOffset_t3387826427_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RectOffset_t3387826427_0_0_0;
extern "C" void DelegatePInvokeWrapper_UpdatedEventHandler_t3033456180();
extern const RuntimeType UpdatedEventHandler_t3033456180_0_0_0;
extern "C" void ResourceRequest_t2560315377_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ResourceRequest_t2560315377_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ResourceRequest_t2560315377_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ResourceRequest_t2560315377_0_0_0;
extern "C" void ScriptableObject_t1975622470_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ScriptableObject_t1975622470_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ScriptableObject_t1975622470_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ScriptableObject_t1975622470_0_0_0;
extern "C" void HitInfo_t1761367055_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void HitInfo_t1761367055_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void HitInfo_t1761367055_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType HitInfo_t1761367055_0_0_0;
extern "C" void SkeletonBone_t345082847_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SkeletonBone_t345082847_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SkeletonBone_t345082847_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SkeletonBone_t345082847_0_0_0;
extern "C" void GcAchievementData_t1754866149_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcAchievementData_t1754866149_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcAchievementData_t1754866149_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcAchievementData_t1754866149_0_0_0;
extern "C" void GcAchievementDescriptionData_t960725851_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcAchievementDescriptionData_t960725851_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcAchievementDescriptionData_t960725851_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcAchievementDescriptionData_t960725851_0_0_0;
extern "C" void GcLeaderboard_t453887929_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcLeaderboard_t453887929_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcLeaderboard_t453887929_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcLeaderboard_t453887929_0_0_0;
extern "C" void GcScoreData_t3676783238_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcScoreData_t3676783238_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcScoreData_t3676783238_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcScoreData_t3676783238_0_0_0;
extern "C" void GcUserProfileData_t3198293052_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcUserProfileData_t3198293052_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcUserProfileData_t3198293052_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcUserProfileData_t3198293052_0_0_0;
extern "C" void TextGenerationSettings_t2543476768_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TextGenerationSettings_t2543476768_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TextGenerationSettings_t2543476768_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType TextGenerationSettings_t2543476768_0_0_0;
extern "C" void TextGenerator_t647235000_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TextGenerator_t647235000_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TextGenerator_t647235000_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType TextGenerator_t647235000_0_0_0;
extern "C" void TrackedReference_t1045890189_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TrackedReference_t1045890189_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TrackedReference_t1045890189_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType TrackedReference_t1045890189_0_0_0;
extern "C" void DelegatePInvokeWrapper_RequestAtlasCallback_t2704894725();
extern const RuntimeType RequestAtlasCallback_t2704894725_0_0_0;
extern "C" void WorkRequest_t1154022482_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void WorkRequest_t1154022482_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void WorkRequest_t1154022482_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType WorkRequest_t1154022482_0_0_0;
extern "C" void WaitForSeconds_t3839502067_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void WaitForSeconds_t3839502067_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void WaitForSeconds_t3839502067_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType WaitForSeconds_t3839502067_0_0_0;
extern "C" void WebCamDevice_t3983871389_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void WebCamDevice_t3983871389_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void WebCamDevice_t3983871389_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType WebCamDevice_t3983871389_0_0_0;
extern "C" void YieldInstruction_t3462875981_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void YieldInstruction_t3462875981_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void YieldInstruction_t3462875981_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType YieldInstruction_t3462875981_0_0_0;
extern "C" void DelegatePInvokeWrapper_DispatcherFactory_t1307565918();
extern const RuntimeType DispatcherFactory_t1307565918_0_0_0;
extern "C" void RaycastResult_t21186376_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RaycastResult_t21186376_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RaycastResult_t21186376_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RaycastResult_t21186376_0_0_0;
extern "C" void ColorTween_t3438117476_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ColorTween_t3438117476_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ColorTween_t3438117476_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ColorTween_t3438117476_0_0_0;
extern "C" void FloatTween_t2986189219_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void FloatTween_t2986189219_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void FloatTween_t2986189219_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType FloatTween_t2986189219_0_0_0;
extern "C" void Resources_t2975512894_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Resources_t2975512894_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Resources_t2975512894_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Resources_t2975512894_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnValidateInput_t1946318473();
extern const RuntimeType OnValidateInput_t1946318473_0_0_0;
extern "C" void Navigation_t1571958496_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Navigation_t1571958496_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Navigation_t1571958496_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Navigation_t1571958496_0_0_0;
extern "C" void SpriteState_t1353336012_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SpriteState_t1353336012_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SpriteState_t1353336012_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SpriteState_t1353336012_0_0_0;
extern "C" void ARAnchor_t1161832412_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ARAnchor_t1161832412_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ARAnchor_t1161832412_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ARAnchor_t1161832412_0_0_0;
extern "C" void ARHitTestResult_t3275513025_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ARHitTestResult_t3275513025_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ARHitTestResult_t3275513025_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ARHitTestResult_t3275513025_0_0_0;
extern "C" void ARKitSessionConfiguration_t318899795_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ARKitSessionConfiguration_t318899795_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ARKitSessionConfiguration_t318899795_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ARKitSessionConfiguration_t318899795_0_0_0;
extern "C" void ARKitWorldTackingSessionConfiguration_t1821734930_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ARKitWorldTackingSessionConfiguration_t1821734930_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ARKitWorldTackingSessionConfiguration_t1821734930_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ARKitWorldTackingSessionConfiguration_t1821734930_0_0_0;
extern "C" void ARPlaneAnchor_t1439520888_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ARPlaneAnchor_t1439520888_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ARPlaneAnchor_t1439520888_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ARPlaneAnchor_t1439520888_0_0_0;
extern "C" void UnityARCamera_t4198559457_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UnityARCamera_t4198559457_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UnityARCamera_t4198559457_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType UnityARCamera_t4198559457_0_0_0;
extern "C" void UnityARHitTestResult_t4129824344_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UnityARHitTestResult_t4129824344_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UnityARHitTestResult_t4129824344_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType UnityARHitTestResult_t4129824344_0_0_0;
extern "C" void DelegatePInvokeWrapper_ARAnchorAdded_t2646854145();
extern const RuntimeType ARAnchorAdded_t2646854145_0_0_0;
extern "C" void DelegatePInvokeWrapper_ARAnchorRemoved_t142665927();
extern const RuntimeType ARAnchorRemoved_t142665927_0_0_0;
extern "C" void DelegatePInvokeWrapper_ARAnchorUpdated_t3886071158();
extern const RuntimeType ARAnchorUpdated_t3886071158_0_0_0;
extern "C" void DelegatePInvokeWrapper_ARSessionFailed_t872580813();
extern const RuntimeType ARSessionFailed_t872580813_0_0_0;
extern "C" void DelegatePInvokeWrapper_internal_ARAnchorAdded_t1622117597();
extern const RuntimeType internal_ARAnchorAdded_t1622117597_0_0_0;
extern "C" void DelegatePInvokeWrapper_internal_ARAnchorRemoved_t3189755211();
extern const RuntimeType internal_ARAnchorRemoved_t3189755211_0_0_0;
extern "C" void DelegatePInvokeWrapper_internal_ARAnchorUpdated_t3705772742();
extern const RuntimeType internal_ARAnchorUpdated_t3705772742_0_0_0;
extern "C" void DelegatePInvokeWrapper_internal_ARFrameUpdate_t3296518558();
extern const RuntimeType internal_ARFrameUpdate_t3296518558_0_0_0;
extern "C" void AotCompilation_t21908242_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AotCompilation_t21908242_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AotCompilation_t21908242_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AotCompilation_t21908242_0_0_0;
extern "C" void fsResult_t862419890_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void fsResult_t862419890_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void fsResult_t862419890_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType fsResult_t862419890_0_0_0;
extern "C" void AttributeQuery_t604298480_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AttributeQuery_t604298480_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AttributeQuery_t604298480_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AttributeQuery_t604298480_0_0_0;
extern "C" void fsVersionedType_t654750358_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void fsVersionedType_t654750358_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void fsVersionedType_t654750358_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType fsVersionedType_t654750358_0_0_0;
extern "C" void DelegatePInvokeWrapper_ProgressEvent_t4185145044();
extern const RuntimeType ProgressEvent_t4185145044_0_0_0;
extern "C" void DelegatePInvokeWrapper_GetDebugInfo_t2665501740();
extern const RuntimeType GetDebugInfo_t2665501740_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnTestsComplete_t1412473303();
extern const RuntimeType OnTestsComplete_t1412473303_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnMessageCallback_t476834366();
extern const RuntimeType OnMessageCallback_t476834366_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnDeleteCollection_t603439291();
extern const RuntimeType OnDeleteCollection_t603439291_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnDeleteConfiguration_t1574428889();
extern const RuntimeType OnDeleteConfiguration_t1574428889_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnDeleteDocument_t3959738954();
extern const RuntimeType OnDeleteDocument_t3959738954_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnDeleteEnvironment_t1826514308();
extern const RuntimeType OnDeleteEnvironment_t1826514308_0_0_0;
extern "C" void DelegatePInvokeWrapper_LoadFileDelegate_t484637430();
extern const RuntimeType LoadFileDelegate_t484637430_0_0_0;
extern "C" void DelegatePInvokeWrapper_IdentifyCallback_t942722308();
extern const RuntimeType IdentifyCallback_t942722308_0_0_0;
extern "C" void DelegatePInvokeWrapper_IdentifyCallback_t682135812();
extern const RuntimeType IdentifyCallback_t682135812_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnDeleteClassifier_t3710463002();
extern const RuntimeType OnDeleteClassifier_t3710463002_0_0_0;
extern "C" void DelegatePInvokeWrapper_LoadFileDelegate_t1720639326();
extern const RuntimeType LoadFileDelegate_t1720639326_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnDeleteCluster_t1281715199();
extern const RuntimeType OnDeleteCluster_t1281715199_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnDeleteClusterConfig_t2578936137();
extern const RuntimeType OnDeleteClusterConfig_t2578936137_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnDeleteRanker_t2889964448();
extern const RuntimeType OnDeleteRanker_t2889964448_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnGetClusterConfig_t1069230450();
extern const RuntimeType OnGetClusterConfig_t1069230450_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnSaveClusterConfig_t1862884805();
extern const RuntimeType OnSaveClusterConfig_t1862884805_0_0_0;
extern "C" void DelegatePInvokeWrapper_SaveFileDelegate_t3125227661();
extern const RuntimeType SaveFileDelegate_t3125227661_0_0_0;
extern "C" void DelegatePInvokeWrapper_ServiceStatus_t1443707987();
extern const RuntimeType ServiceStatus_t1443707987_0_0_0;
extern "C" void DelegatePInvokeWrapper_AddCustomWordsCallback_t4067999203();
extern const RuntimeType AddCustomWordsCallback_t4067999203_0_0_0;
extern "C" void DelegatePInvokeWrapper_ErrorEvent_t3049611749();
extern const RuntimeType ErrorEvent_t3049611749_0_0_0;
extern "C" void DelegatePInvokeWrapper_LoadFileDelegate_t351561590();
extern const RuntimeType LoadFileDelegate_t351561590_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnAddCustomCorpusCallback_t1567930907();
extern const RuntimeType OnAddCustomCorpusCallback_t1567930907_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnDeleteCustomCorpusCallback_t3393304107();
extern const RuntimeType OnDeleteCustomCorpusCallback_t3393304107_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnDeleteCustomizationCallback_t1940302465();
extern const RuntimeType OnDeleteCustomizationCallback_t1940302465_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnDeleteCustomWordCallback_t2228320037();
extern const RuntimeType OnDeleteCustomWordCallback_t2228320037_0_0_0;
extern "C" void DelegatePInvokeWrapper_ResetCustomizationCallback_t1922530872();
extern const RuntimeType ResetCustomizationCallback_t1922530872_0_0_0;
extern "C" void DelegatePInvokeWrapper_TrainCustomizationCallback_t2705176209();
extern const RuntimeType TrainCustomizationCallback_t2705176209_0_0_0;
extern "C" void DelegatePInvokeWrapper_UpgradeCustomizationCallback_t567321637();
extern const RuntimeType UpgradeCustomizationCallback_t567321637_0_0_0;
extern "C" void DelegatePInvokeWrapper_AddCustomizationWordCallback_t3775539180();
extern const RuntimeType AddCustomizationWordCallback_t3775539180_0_0_0;
extern "C" void DelegatePInvokeWrapper_AddCustomizationWordsCallback_t2578578585();
extern const RuntimeType AddCustomizationWordsCallback_t2578578585_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnDeleteCustomizationCallback_t2862971265();
extern const RuntimeType OnDeleteCustomizationCallback_t2862971265_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnDeleteCustomizationWordCallback_t2544578463();
extern const RuntimeType OnDeleteCustomizationWordCallback_t2544578463_0_0_0;
extern "C" void DelegatePInvokeWrapper_UpdateCustomizationCallback_t3026233620();
extern const RuntimeType UpdateCustomizationCallback_t3026233620_0_0_0;
extern "C" void DelegatePInvokeWrapper_LoadFileDelegate_t3728218844();
extern const RuntimeType LoadFileDelegate_t3728218844_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnDeleteClassifier_t917631666();
extern const RuntimeType OnDeleteClassifier_t917631666_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnDeleteCollection_t1354946561();
extern const RuntimeType OnDeleteCollection_t1354946561_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnDeleteCollectionImage_t241020072();
extern const RuntimeType OnDeleteCollectionImage_t241020072_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnDeleteImageMetadata_t2373116469();
extern const RuntimeType OnDeleteImageMetadata_t2373116469_0_0_0;
extern "C" void TouchData_t3880599975_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TouchData_t3880599975_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TouchData_t3880599975_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType TouchData_t3880599975_0_0_0;
extern "C" void TouchHit_t4186847494_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TouchHit_t4186847494_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TouchHit_t4186847494_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType TouchHit_t4186847494_0_0_0;
extern Il2CppInteropData g_Il2CppInteropData[183] = 
{
	{ NULL, Context_t2636657155_marshal_pinvoke, Context_t2636657155_marshal_pinvoke_back, Context_t2636657155_marshal_pinvoke_cleanup, NULL, NULL, &Context_t2636657155_0_0_0 } /* Mono.Globalization.Unicode.SimpleCollator/Context */,
	{ NULL, Escape_t169451053_marshal_pinvoke, Escape_t169451053_marshal_pinvoke_back, Escape_t169451053_marshal_pinvoke_cleanup, NULL, NULL, &Escape_t169451053_0_0_0 } /* Mono.Globalization.Unicode.SimpleCollator/Escape */,
	{ NULL, PreviousInfo_t581002487_marshal_pinvoke, PreviousInfo_t581002487_marshal_pinvoke_back, PreviousInfo_t581002487_marshal_pinvoke_cleanup, NULL, NULL, &PreviousInfo_t581002487_0_0_0 } /* Mono.Globalization.Unicode.SimpleCollator/PreviousInfo */,
	{ DelegatePInvokeWrapper_AppDomainInitializer_t3898244613, NULL, NULL, NULL, NULL, NULL, &AppDomainInitializer_t3898244613_0_0_0 } /* System.AppDomainInitializer */,
	{ DelegatePInvokeWrapper_Swapper_t2637371637, NULL, NULL, NULL, NULL, NULL, &Swapper_t2637371637_0_0_0 } /* System.Array/Swapper */,
	{ NULL, DictionaryEntry_t3048875398_marshal_pinvoke, DictionaryEntry_t3048875398_marshal_pinvoke_back, DictionaryEntry_t3048875398_marshal_pinvoke_cleanup, NULL, NULL, &DictionaryEntry_t3048875398_0_0_0 } /* System.Collections.DictionaryEntry */,
	{ NULL, Slot_t2022531261_marshal_pinvoke, Slot_t2022531261_marshal_pinvoke_back, Slot_t2022531261_marshal_pinvoke_cleanup, NULL, NULL, &Slot_t2022531261_0_0_0 } /* System.Collections.Hashtable/Slot */,
	{ NULL, Slot_t2267560602_marshal_pinvoke, Slot_t2267560602_marshal_pinvoke_back, Slot_t2267560602_marshal_pinvoke_cleanup, NULL, NULL, &Slot_t2267560602_0_0_0 } /* System.Collections.SortedList/Slot */,
	{ NULL, Enum_t2459695545_marshal_pinvoke, Enum_t2459695545_marshal_pinvoke_back, Enum_t2459695545_marshal_pinvoke_cleanup, NULL, NULL, &Enum_t2459695545_0_0_0 } /* System.Enum */,
	{ DelegatePInvokeWrapper_ReadDelegate_t3184826381, NULL, NULL, NULL, NULL, NULL, &ReadDelegate_t3184826381_0_0_0 } /* System.IO.FileStream/ReadDelegate */,
	{ DelegatePInvokeWrapper_WriteDelegate_t489908132, NULL, NULL, NULL, NULL, NULL, &WriteDelegate_t489908132_0_0_0 } /* System.IO.FileStream/WriteDelegate */,
	{ NULL, MonoIOStat_t1621921065_marshal_pinvoke, MonoIOStat_t1621921065_marshal_pinvoke_back, MonoIOStat_t1621921065_marshal_pinvoke_cleanup, NULL, NULL, &MonoIOStat_t1621921065_0_0_0 } /* System.IO.MonoIOStat */,
	{ NULL, MonoEnumInfo_t2335995564_marshal_pinvoke, MonoEnumInfo_t2335995564_marshal_pinvoke_back, MonoEnumInfo_t2335995564_marshal_pinvoke_cleanup, NULL, NULL, &MonoEnumInfo_t2335995564_0_0_0 } /* System.MonoEnumInfo */,
	{ NULL, CustomAttributeNamedArgument_t94157543_marshal_pinvoke, CustomAttributeNamedArgument_t94157543_marshal_pinvoke_back, CustomAttributeNamedArgument_t94157543_marshal_pinvoke_cleanup, NULL, NULL, &CustomAttributeNamedArgument_t94157543_0_0_0 } /* System.Reflection.CustomAttributeNamedArgument */,
	{ NULL, CustomAttributeTypedArgument_t1498197914_marshal_pinvoke, CustomAttributeTypedArgument_t1498197914_marshal_pinvoke_back, CustomAttributeTypedArgument_t1498197914_marshal_pinvoke_cleanup, NULL, NULL, &CustomAttributeTypedArgument_t1498197914_0_0_0 } /* System.Reflection.CustomAttributeTypedArgument */,
	{ NULL, ILTokenInfo_t149559338_marshal_pinvoke, ILTokenInfo_t149559338_marshal_pinvoke_back, ILTokenInfo_t149559338_marshal_pinvoke_cleanup, NULL, NULL, &ILTokenInfo_t149559338_0_0_0 } /* System.Reflection.Emit.ILTokenInfo */,
	{ NULL, MonoEventInfo_t2190036573_marshal_pinvoke, MonoEventInfo_t2190036573_marshal_pinvoke_back, MonoEventInfo_t2190036573_marshal_pinvoke_cleanup, NULL, NULL, &MonoEventInfo_t2190036573_0_0_0 } /* System.Reflection.MonoEventInfo */,
	{ NULL, MonoMethodInfo_t3646562144_marshal_pinvoke, MonoMethodInfo_t3646562144_marshal_pinvoke_back, MonoMethodInfo_t3646562144_marshal_pinvoke_cleanup, NULL, NULL, &MonoMethodInfo_t3646562144_0_0_0 } /* System.Reflection.MonoMethodInfo */,
	{ NULL, MonoPropertyInfo_t486106184_marshal_pinvoke, MonoPropertyInfo_t486106184_marshal_pinvoke_back, MonoPropertyInfo_t486106184_marshal_pinvoke_cleanup, NULL, NULL, &MonoPropertyInfo_t486106184_0_0_0 } /* System.Reflection.MonoPropertyInfo */,
	{ NULL, ParameterModifier_t1820634920_marshal_pinvoke, ParameterModifier_t1820634920_marshal_pinvoke_back, ParameterModifier_t1820634920_marshal_pinvoke_cleanup, NULL, NULL, &ParameterModifier_t1820634920_0_0_0 } /* System.Reflection.ParameterModifier */,
	{ NULL, ResourceCacheItem_t333236149_marshal_pinvoke, ResourceCacheItem_t333236149_marshal_pinvoke_back, ResourceCacheItem_t333236149_marshal_pinvoke_cleanup, NULL, NULL, &ResourceCacheItem_t333236149_0_0_0 } /* System.Resources.ResourceReader/ResourceCacheItem */,
	{ NULL, ResourceInfo_t3933049236_marshal_pinvoke, ResourceInfo_t3933049236_marshal_pinvoke_back, ResourceInfo_t3933049236_marshal_pinvoke_cleanup, NULL, NULL, &ResourceInfo_t3933049236_0_0_0 } /* System.Resources.ResourceReader/ResourceInfo */,
	{ DelegatePInvokeWrapper_CrossContextDelegate_t754146990, NULL, NULL, NULL, NULL, NULL, &CrossContextDelegate_t754146990_0_0_0 } /* System.Runtime.Remoting.Contexts.CrossContextDelegate */,
	{ DelegatePInvokeWrapper_CallbackHandler_t362827733, NULL, NULL, NULL, NULL, NULL, &CallbackHandler_t362827733_0_0_0 } /* System.Runtime.Serialization.SerializationCallbacks/CallbackHandler */,
	{ NULL, SerializationEntry_t3485203212_marshal_pinvoke, SerializationEntry_t3485203212_marshal_pinvoke_back, SerializationEntry_t3485203212_marshal_pinvoke_cleanup, NULL, NULL, &SerializationEntry_t3485203212_0_0_0 } /* System.Runtime.Serialization.SerializationEntry */,
	{ NULL, StreamingContext_t1417235061_marshal_pinvoke, StreamingContext_t1417235061_marshal_pinvoke_back, StreamingContext_t1417235061_marshal_pinvoke_cleanup, NULL, NULL, &StreamingContext_t1417235061_0_0_0 } /* System.Runtime.Serialization.StreamingContext */,
	{ NULL, DSAParameters_t1872138834_marshal_pinvoke, DSAParameters_t1872138834_marshal_pinvoke_back, DSAParameters_t1872138834_marshal_pinvoke_cleanup, NULL, NULL, &DSAParameters_t1872138834_0_0_0 } /* System.Security.Cryptography.DSAParameters */,
	{ NULL, RSAParameters_t1462703416_marshal_pinvoke, RSAParameters_t1462703416_marshal_pinvoke_back, RSAParameters_t1462703416_marshal_pinvoke_cleanup, NULL, NULL, &RSAParameters_t1462703416_0_0_0 } /* System.Security.Cryptography.RSAParameters */,
	{ NULL, SecurityFrame_t1002202659_marshal_pinvoke, SecurityFrame_t1002202659_marshal_pinvoke_back, SecurityFrame_t1002202659_marshal_pinvoke_cleanup, NULL, NULL, &SecurityFrame_t1002202659_0_0_0 } /* System.Security.SecurityFrame */,
	{ DelegatePInvokeWrapper_ThreadStart_t3437517264, NULL, NULL, NULL, NULL, NULL, &ThreadStart_t3437517264_0_0_0 } /* System.Threading.ThreadStart */,
	{ NULL, ValueType_t3507792607_marshal_pinvoke, ValueType_t3507792607_marshal_pinvoke_back, ValueType_t3507792607_marshal_pinvoke_cleanup, NULL, NULL, &ValueType_t3507792607_0_0_0 } /* System.ValueType */,
	{ DelegatePInvokeWrapper_ReadMethod_t3362229488, NULL, NULL, NULL, NULL, NULL, &ReadMethod_t3362229488_0_0_0 } /* System.IO.Compression.DeflateStream/ReadMethod */,
	{ DelegatePInvokeWrapper_UnmanagedReadOrWrite_t1990215745, NULL, NULL, NULL, NULL, NULL, &UnmanagedReadOrWrite_t1990215745_0_0_0 } /* System.IO.Compression.DeflateStream/UnmanagedReadOrWrite */,
	{ DelegatePInvokeWrapper_WriteMethod_t1894833619, NULL, NULL, NULL, NULL, NULL, &WriteMethod_t1894833619_0_0_0 } /* System.IO.Compression.DeflateStream/WriteMethod */,
	{ NULL, ifaddrs_t2532459533_marshal_pinvoke, ifaddrs_t2532459533_marshal_pinvoke_back, ifaddrs_t2532459533_marshal_pinvoke_cleanup, NULL, NULL, &ifaddrs_t2532459533_0_0_0 } /* System.Net.NetworkInformation.ifaddrs */,
	{ NULL, in6_addr_t4035827331_marshal_pinvoke, in6_addr_t4035827331_marshal_pinvoke_back, in6_addr_t4035827331_marshal_pinvoke_cleanup, NULL, NULL, &in6_addr_t4035827331_0_0_0 } /* System.Net.NetworkInformation.in6_addr */,
	{ NULL, ifaddrs_t937751619_marshal_pinvoke, ifaddrs_t937751619_marshal_pinvoke_back, ifaddrs_t937751619_marshal_pinvoke_cleanup, NULL, NULL, &ifaddrs_t937751619_0_0_0 } /* System.Net.NetworkInformation.MacOsStructs.ifaddrs */,
	{ NULL, in6_addr_t1014645363_marshal_pinvoke, in6_addr_t1014645363_marshal_pinvoke_back, in6_addr_t1014645363_marshal_pinvoke_cleanup, NULL, NULL, &in6_addr_t1014645363_0_0_0 } /* System.Net.NetworkInformation.MacOsStructs.in6_addr */,
	{ NULL, sockaddr_dl_t3955242742_marshal_pinvoke, sockaddr_dl_t3955242742_marshal_pinvoke_back, sockaddr_dl_t3955242742_marshal_pinvoke_cleanup, NULL, NULL, &sockaddr_dl_t3955242742_0_0_0 } /* System.Net.NetworkInformation.MacOsStructs.sockaddr_dl */,
	{ NULL, sockaddr_in6_t834146887_marshal_pinvoke, sockaddr_in6_t834146887_marshal_pinvoke_back, sockaddr_in6_t834146887_marshal_pinvoke_cleanup, NULL, NULL, &sockaddr_in6_t834146887_0_0_0 } /* System.Net.NetworkInformation.MacOsStructs.sockaddr_in6 */,
	{ NULL, sockaddr_in6_t2899168071_marshal_pinvoke, sockaddr_in6_t2899168071_marshal_pinvoke_back, sockaddr_in6_t2899168071_marshal_pinvoke_cleanup, NULL, NULL, &sockaddr_in6_t2899168071_0_0_0 } /* System.Net.NetworkInformation.sockaddr_in6 */,
	{ NULL, sockaddr_ll_t1681025498_marshal_pinvoke, sockaddr_ll_t1681025498_marshal_pinvoke_back, sockaddr_ll_t1681025498_marshal_pinvoke_cleanup, NULL, NULL, &sockaddr_ll_t1681025498_0_0_0 } /* System.Net.NetworkInformation.sockaddr_ll */,
	{ NULL, Win32_IP_ADAPTER_ADDRESSES_t680756680_marshal_pinvoke, Win32_IP_ADAPTER_ADDRESSES_t680756680_marshal_pinvoke_back, Win32_IP_ADAPTER_ADDRESSES_t680756680_marshal_pinvoke_cleanup, NULL, NULL, &Win32_IP_ADAPTER_ADDRESSES_t680756680_0_0_0 } /* System.Net.NetworkInformation.Win32_IP_ADAPTER_ADDRESSES */,
	{ NULL, Win32_MIB_IFROW_t4215928996_marshal_pinvoke, Win32_MIB_IFROW_t4215928996_marshal_pinvoke_back, Win32_MIB_IFROW_t4215928996_marshal_pinvoke_cleanup, NULL, NULL, &Win32_MIB_IFROW_t4215928996_0_0_0 } /* System.Net.NetworkInformation.Win32_MIB_IFROW */,
	{ DelegatePInvokeWrapper_SocketAsyncCall_t3737776727, NULL, NULL, NULL, NULL, NULL, &SocketAsyncCall_t3737776727_0_0_0 } /* System.Net.Sockets.Socket/SocketAsyncCall */,
	{ NULL, SocketAsyncResult_t2959281146_marshal_pinvoke, SocketAsyncResult_t2959281146_marshal_pinvoke_back, SocketAsyncResult_t2959281146_marshal_pinvoke_cleanup, NULL, NULL, &SocketAsyncResult_t2959281146_0_0_0 } /* System.Net.Sockets.Socket/SocketAsyncResult */,
	{ NULL, X509ChainStatus_t4278378721_marshal_pinvoke, X509ChainStatus_t4278378721_marshal_pinvoke_back, X509ChainStatus_t4278378721_marshal_pinvoke_cleanup, NULL, NULL, &X509ChainStatus_t4278378721_0_0_0 } /* System.Security.Cryptography.X509Certificates.X509ChainStatus */,
	{ NULL, IntStack_t273560425_marshal_pinvoke, IntStack_t273560425_marshal_pinvoke_back, IntStack_t273560425_marshal_pinvoke_cleanup, NULL, NULL, &IntStack_t273560425_0_0_0 } /* System.Text.RegularExpressions.Interpreter/IntStack */,
	{ NULL, Interval_t2354235237_marshal_pinvoke, Interval_t2354235237_marshal_pinvoke_back, Interval_t2354235237_marshal_pinvoke_cleanup, NULL, NULL, &Interval_t2354235237_0_0_0 } /* System.Text.RegularExpressions.Interval */,
	{ DelegatePInvokeWrapper_CostDelegate_t1824458113, NULL, NULL, NULL, NULL, NULL, &CostDelegate_t1824458113_0_0_0 } /* System.Text.RegularExpressions.IntervalCollection/CostDelegate */,
	{ NULL, UriScheme_t1876590943_marshal_pinvoke, UriScheme_t1876590943_marshal_pinvoke_back, UriScheme_t1876590943_marshal_pinvoke_cleanup, NULL, NULL, &UriScheme_t1876590943_0_0_0 } /* System.Uri/UriScheme */,
	{ DelegatePInvokeWrapper_Action_t3226471752, NULL, NULL, NULL, NULL, NULL, &Action_t3226471752_0_0_0 } /* System.Action */,
	{ NULL, NsDecl_t3210081295_marshal_pinvoke, NsDecl_t3210081295_marshal_pinvoke_back, NsDecl_t3210081295_marshal_pinvoke_cleanup, NULL, NULL, &NsDecl_t3210081295_0_0_0 } /* System.Xml.XmlNamespaceManager/NsDecl */,
	{ NULL, AnimationCurve_t3306541151_marshal_pinvoke, AnimationCurve_t3306541151_marshal_pinvoke_back, AnimationCurve_t3306541151_marshal_pinvoke_cleanup, NULL, NULL, &AnimationCurve_t3306541151_0_0_0 } /* UnityEngine.AnimationCurve */,
	{ NULL, AnimationEvent_t2428323300_marshal_pinvoke, AnimationEvent_t2428323300_marshal_pinvoke_back, AnimationEvent_t2428323300_marshal_pinvoke_cleanup, NULL, NULL, &AnimationEvent_t2428323300_0_0_0 } /* UnityEngine.AnimationEvent */,
	{ NULL, AnimatorTransitionInfo_t2410896200_marshal_pinvoke, AnimatorTransitionInfo_t2410896200_marshal_pinvoke_back, AnimatorTransitionInfo_t2410896200_marshal_pinvoke_cleanup, NULL, NULL, &AnimatorTransitionInfo_t2410896200_0_0_0 } /* UnityEngine.AnimatorTransitionInfo */,
	{ DelegatePInvokeWrapper_LogCallback_t1867914413, NULL, NULL, NULL, NULL, NULL, &LogCallback_t1867914413_0_0_0 } /* UnityEngine.Application/LogCallback */,
	{ DelegatePInvokeWrapper_LowMemoryCallback_t642977590, NULL, NULL, NULL, NULL, NULL, &LowMemoryCallback_t642977590_0_0_0 } /* UnityEngine.Application/LowMemoryCallback */,
	{ NULL, AssetBundleRequest_t2674559435_marshal_pinvoke, AssetBundleRequest_t2674559435_marshal_pinvoke_back, AssetBundleRequest_t2674559435_marshal_pinvoke_cleanup, NULL, NULL, &AssetBundleRequest_t2674559435_0_0_0 } /* UnityEngine.AssetBundleRequest */,
	{ NULL, AsyncOperation_t3814632279_marshal_pinvoke, AsyncOperation_t3814632279_marshal_pinvoke_back, AsyncOperation_t3814632279_marshal_pinvoke_cleanup, NULL, NULL, &AsyncOperation_t3814632279_0_0_0 } /* UnityEngine.AsyncOperation */,
	{ DelegatePInvokeWrapper_PCMReaderCallback_t3007145346, NULL, NULL, NULL, NULL, NULL, &PCMReaderCallback_t3007145346_0_0_0 } /* UnityEngine.AudioClip/PCMReaderCallback */,
	{ DelegatePInvokeWrapper_PCMSetPositionCallback_t421863554, NULL, NULL, NULL, NULL, NULL, &PCMSetPositionCallback_t421863554_0_0_0 } /* UnityEngine.AudioClip/PCMSetPositionCallback */,
	{ DelegatePInvokeWrapper_AudioConfigurationChangeHandler_t3743753033, NULL, NULL, NULL, NULL, NULL, &AudioConfigurationChangeHandler_t3743753033_0_0_0 } /* UnityEngine.AudioSettings/AudioConfigurationChangeHandler */,
	{ DelegatePInvokeWrapper_WillRenderCanvases_t3522132132, NULL, NULL, NULL, NULL, NULL, &WillRenderCanvases_t3522132132_0_0_0 } /* UnityEngine.Canvas/WillRenderCanvases */,
	{ NULL, Collision_t2876846408_marshal_pinvoke, Collision_t2876846408_marshal_pinvoke_back, Collision_t2876846408_marshal_pinvoke_cleanup, NULL, NULL, &Collision_t2876846408_0_0_0 } /* UnityEngine.Collision */,
	{ NULL, Collision2D_t1539500754_marshal_pinvoke, Collision2D_t1539500754_marshal_pinvoke_back, Collision2D_t1539500754_marshal_pinvoke_cleanup, NULL, NULL, &Collision2D_t1539500754_0_0_0 } /* UnityEngine.Collision2D */,
	{ NULL, ContactFilter2D_t1672660996_marshal_pinvoke, ContactFilter2D_t1672660996_marshal_pinvoke_back, ContactFilter2D_t1672660996_marshal_pinvoke_cleanup, NULL, NULL, &ContactFilter2D_t1672660996_0_0_0 } /* UnityEngine.ContactFilter2D */,
	{ NULL, ControllerColliderHit_t4070855101_marshal_pinvoke, ControllerColliderHit_t4070855101_marshal_pinvoke_back, ControllerColliderHit_t4070855101_marshal_pinvoke_cleanup, NULL, NULL, &ControllerColliderHit_t4070855101_0_0_0 } /* UnityEngine.ControllerColliderHit */,
	{ NULL, Coroutine_t2299508840_marshal_pinvoke, Coroutine_t2299508840_marshal_pinvoke_back, Coroutine_t2299508840_marshal_pinvoke_cleanup, NULL, NULL, &Coroutine_t2299508840_0_0_0 } /* UnityEngine.Coroutine */,
	{ DelegatePInvokeWrapper_CSSMeasureFunc_t2092170507, NULL, NULL, NULL, NULL, NULL, &CSSMeasureFunc_t2092170507_0_0_0 } /* UnityEngine.CSSLayout.CSSMeasureFunc */,
	{ NULL, CullingGroup_t1091689465_marshal_pinvoke, CullingGroup_t1091689465_marshal_pinvoke_back, CullingGroup_t1091689465_marshal_pinvoke_cleanup, NULL, NULL, &CullingGroup_t1091689465_0_0_0 } /* UnityEngine.CullingGroup */,
	{ DelegatePInvokeWrapper_StateChanged_t2480912210, NULL, NULL, NULL, NULL, NULL, &StateChanged_t2480912210_0_0_0 } /* UnityEngine.CullingGroup/StateChanged */,
	{ DelegatePInvokeWrapper_DisplaysUpdatedDelegate_t3423469815, NULL, NULL, NULL, NULL, NULL, &DisplaysUpdatedDelegate_t3423469815_0_0_0 } /* UnityEngine.Display/DisplaysUpdatedDelegate */,
	{ NULL, Event_t3028476042_marshal_pinvoke, Event_t3028476042_marshal_pinvoke_back, Event_t3028476042_marshal_pinvoke_cleanup, NULL, NULL, &Event_t3028476042_0_0_0 } /* UnityEngine.Event */,
	{ DelegatePInvokeWrapper_UnityAction_t4025899511, NULL, NULL, NULL, NULL, NULL, &UnityAction_t4025899511_0_0_0 } /* UnityEngine.Events.UnityAction */,
	{ NULL, FailedToLoadScriptObject_t3605283378_marshal_pinvoke, FailedToLoadScriptObject_t3605283378_marshal_pinvoke_back, FailedToLoadScriptObject_t3605283378_marshal_pinvoke_cleanup, NULL, NULL, &FailedToLoadScriptObject_t3605283378_0_0_0 } /* UnityEngine.FailedToLoadScriptObject */,
	{ DelegatePInvokeWrapper_FontTextureRebuildCallback_t1272078033, NULL, NULL, NULL, NULL, NULL, &FontTextureRebuildCallback_t1272078033_0_0_0 } /* UnityEngine.Font/FontTextureRebuildCallback */,
	{ NULL, Gradient_t3600583008_marshal_pinvoke, Gradient_t3600583008_marshal_pinvoke_back, Gradient_t3600583008_marshal_pinvoke_cleanup, NULL, NULL, &Gradient_t3600583008_0_0_0 } /* UnityEngine.Gradient */,
	{ DelegatePInvokeWrapper_WindowFunction_t3486805455, NULL, NULL, NULL, NULL, NULL, &WindowFunction_t3486805455_0_0_0 } /* UnityEngine.GUI/WindowFunction */,
	{ NULL, GUIContent_t4210063000_marshal_pinvoke, GUIContent_t4210063000_marshal_pinvoke_back, GUIContent_t4210063000_marshal_pinvoke_cleanup, NULL, NULL, &GUIContent_t4210063000_0_0_0 } /* UnityEngine.GUIContent */,
	{ DelegatePInvokeWrapper_SkinChangedDelegate_t3594822336, NULL, NULL, NULL, NULL, NULL, &SkinChangedDelegate_t3594822336_0_0_0 } /* UnityEngine.GUISkin/SkinChangedDelegate */,
	{ NULL, GUIStyle_t1799908754_marshal_pinvoke, GUIStyle_t1799908754_marshal_pinvoke_back, GUIStyle_t1799908754_marshal_pinvoke_cleanup, NULL, NULL, &GUIStyle_t1799908754_0_0_0 } /* UnityEngine.GUIStyle */,
	{ NULL, GUIStyleState_t3801000545_marshal_pinvoke, GUIStyleState_t3801000545_marshal_pinvoke_back, GUIStyleState_t3801000545_marshal_pinvoke_cleanup, NULL, NULL, &GUIStyleState_t3801000545_0_0_0 } /* UnityEngine.GUIStyleState */,
	{ NULL, HostData_t3480691970_marshal_pinvoke, HostData_t3480691970_marshal_pinvoke_back, HostData_t3480691970_marshal_pinvoke_cleanup, NULL, NULL, &HostData_t3480691970_0_0_0 } /* UnityEngine.HostData */,
	{ NULL, HumanBone_t1529896151_marshal_pinvoke, HumanBone_t1529896151_marshal_pinvoke_back, HumanBone_t1529896151_marshal_pinvoke_cleanup, NULL, NULL, &HumanBone_t1529896151_0_0_0 } /* UnityEngine.HumanBone */,
	{ NULL, DownloadHandler_t1216180266_marshal_pinvoke, DownloadHandler_t1216180266_marshal_pinvoke_back, DownloadHandler_t1216180266_marshal_pinvoke_cleanup, NULL, NULL, &DownloadHandler_t1216180266_0_0_0 } /* UnityEngine.Networking.DownloadHandler */,
	{ NULL, DownloadHandlerBuffer_t3443159558_marshal_pinvoke, DownloadHandlerBuffer_t3443159558_marshal_pinvoke_back, DownloadHandlerBuffer_t3443159558_marshal_pinvoke_cleanup, NULL, NULL, &DownloadHandlerBuffer_t3443159558_0_0_0 } /* UnityEngine.Networking.DownloadHandlerBuffer */,
	{ NULL, UnityWebRequest_t254341728_marshal_pinvoke, UnityWebRequest_t254341728_marshal_pinvoke_back, UnityWebRequest_t254341728_marshal_pinvoke_cleanup, NULL, NULL, &UnityWebRequest_t254341728_0_0_0 } /* UnityEngine.Networking.UnityWebRequest */,
	{ NULL, UploadHandler_t3552561393_marshal_pinvoke, UploadHandler_t3552561393_marshal_pinvoke_back, UploadHandler_t3552561393_marshal_pinvoke_cleanup, NULL, NULL, &UploadHandler_t3552561393_0_0_0 } /* UnityEngine.Networking.UploadHandler */,
	{ NULL, UploadHandlerRaw_t3420491431_marshal_pinvoke, UploadHandlerRaw_t3420491431_marshal_pinvoke_back, UploadHandlerRaw_t3420491431_marshal_pinvoke_cleanup, NULL, NULL, &UploadHandlerRaw_t3420491431_0_0_0 } /* UnityEngine.Networking.UploadHandlerRaw */,
	{ NULL, Object_t1021602117_marshal_pinvoke, Object_t1021602117_marshal_pinvoke_back, Object_t1021602117_marshal_pinvoke_cleanup, NULL, NULL, &Object_t1021602117_0_0_0 } /* UnityEngine.Object */,
	{ NULL, PlayableBinding_t2498078091_marshal_pinvoke, PlayableBinding_t2498078091_marshal_pinvoke_back, PlayableBinding_t2498078091_marshal_pinvoke_cleanup, NULL, NULL, &PlayableBinding_t2498078091_0_0_0 } /* UnityEngine.Playables.PlayableBinding */,
	{ NULL, RaycastHit_t87180320_marshal_pinvoke, RaycastHit_t87180320_marshal_pinvoke_back, RaycastHit_t87180320_marshal_pinvoke_cleanup, NULL, NULL, &RaycastHit_t87180320_0_0_0 } /* UnityEngine.RaycastHit */,
	{ NULL, RaycastHit2D_t4063908774_marshal_pinvoke, RaycastHit2D_t4063908774_marshal_pinvoke_back, RaycastHit2D_t4063908774_marshal_pinvoke_cleanup, NULL, NULL, &RaycastHit2D_t4063908774_0_0_0 } /* UnityEngine.RaycastHit2D */,
	{ NULL, RectOffset_t3387826427_marshal_pinvoke, RectOffset_t3387826427_marshal_pinvoke_back, RectOffset_t3387826427_marshal_pinvoke_cleanup, NULL, NULL, &RectOffset_t3387826427_0_0_0 } /* UnityEngine.RectOffset */,
	{ DelegatePInvokeWrapper_UpdatedEventHandler_t3033456180, NULL, NULL, NULL, NULL, NULL, &UpdatedEventHandler_t3033456180_0_0_0 } /* UnityEngine.RemoteSettings/UpdatedEventHandler */,
	{ NULL, ResourceRequest_t2560315377_marshal_pinvoke, ResourceRequest_t2560315377_marshal_pinvoke_back, ResourceRequest_t2560315377_marshal_pinvoke_cleanup, NULL, NULL, &ResourceRequest_t2560315377_0_0_0 } /* UnityEngine.ResourceRequest */,
	{ NULL, ScriptableObject_t1975622470_marshal_pinvoke, ScriptableObject_t1975622470_marshal_pinvoke_back, ScriptableObject_t1975622470_marshal_pinvoke_cleanup, NULL, NULL, &ScriptableObject_t1975622470_0_0_0 } /* UnityEngine.ScriptableObject */,
	{ NULL, HitInfo_t1761367055_marshal_pinvoke, HitInfo_t1761367055_marshal_pinvoke_back, HitInfo_t1761367055_marshal_pinvoke_cleanup, NULL, NULL, &HitInfo_t1761367055_0_0_0 } /* UnityEngine.SendMouseEvents/HitInfo */,
	{ NULL, SkeletonBone_t345082847_marshal_pinvoke, SkeletonBone_t345082847_marshal_pinvoke_back, SkeletonBone_t345082847_marshal_pinvoke_cleanup, NULL, NULL, &SkeletonBone_t345082847_0_0_0 } /* UnityEngine.SkeletonBone */,
	{ NULL, GcAchievementData_t1754866149_marshal_pinvoke, GcAchievementData_t1754866149_marshal_pinvoke_back, GcAchievementData_t1754866149_marshal_pinvoke_cleanup, NULL, NULL, &GcAchievementData_t1754866149_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcAchievementData */,
	{ NULL, GcAchievementDescriptionData_t960725851_marshal_pinvoke, GcAchievementDescriptionData_t960725851_marshal_pinvoke_back, GcAchievementDescriptionData_t960725851_marshal_pinvoke_cleanup, NULL, NULL, &GcAchievementDescriptionData_t960725851_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData */,
	{ NULL, GcLeaderboard_t453887929_marshal_pinvoke, GcLeaderboard_t453887929_marshal_pinvoke_back, GcLeaderboard_t453887929_marshal_pinvoke_cleanup, NULL, NULL, &GcLeaderboard_t453887929_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard */,
	{ NULL, GcScoreData_t3676783238_marshal_pinvoke, GcScoreData_t3676783238_marshal_pinvoke_back, GcScoreData_t3676783238_marshal_pinvoke_cleanup, NULL, NULL, &GcScoreData_t3676783238_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcScoreData */,
	{ NULL, GcUserProfileData_t3198293052_marshal_pinvoke, GcUserProfileData_t3198293052_marshal_pinvoke_back, GcUserProfileData_t3198293052_marshal_pinvoke_cleanup, NULL, NULL, &GcUserProfileData_t3198293052_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData */,
	{ NULL, TextGenerationSettings_t2543476768_marshal_pinvoke, TextGenerationSettings_t2543476768_marshal_pinvoke_back, TextGenerationSettings_t2543476768_marshal_pinvoke_cleanup, NULL, NULL, &TextGenerationSettings_t2543476768_0_0_0 } /* UnityEngine.TextGenerationSettings */,
	{ NULL, TextGenerator_t647235000_marshal_pinvoke, TextGenerator_t647235000_marshal_pinvoke_back, TextGenerator_t647235000_marshal_pinvoke_cleanup, NULL, NULL, &TextGenerator_t647235000_0_0_0 } /* UnityEngine.TextGenerator */,
	{ NULL, TrackedReference_t1045890189_marshal_pinvoke, TrackedReference_t1045890189_marshal_pinvoke_back, TrackedReference_t1045890189_marshal_pinvoke_cleanup, NULL, NULL, &TrackedReference_t1045890189_0_0_0 } /* UnityEngine.TrackedReference */,
	{ DelegatePInvokeWrapper_RequestAtlasCallback_t2704894725, NULL, NULL, NULL, NULL, NULL, &RequestAtlasCallback_t2704894725_0_0_0 } /* UnityEngine.U2D.SpriteAtlasManager/RequestAtlasCallback */,
	{ NULL, WorkRequest_t1154022482_marshal_pinvoke, WorkRequest_t1154022482_marshal_pinvoke_back, WorkRequest_t1154022482_marshal_pinvoke_cleanup, NULL, NULL, &WorkRequest_t1154022482_0_0_0 } /* UnityEngine.UnitySynchronizationContext/WorkRequest */,
	{ NULL, WaitForSeconds_t3839502067_marshal_pinvoke, WaitForSeconds_t3839502067_marshal_pinvoke_back, WaitForSeconds_t3839502067_marshal_pinvoke_cleanup, NULL, NULL, &WaitForSeconds_t3839502067_0_0_0 } /* UnityEngine.WaitForSeconds */,
	{ NULL, WebCamDevice_t3983871389_marshal_pinvoke, WebCamDevice_t3983871389_marshal_pinvoke_back, WebCamDevice_t3983871389_marshal_pinvoke_cleanup, NULL, NULL, &WebCamDevice_t3983871389_0_0_0 } /* UnityEngine.WebCamDevice */,
	{ NULL, YieldInstruction_t3462875981_marshal_pinvoke, YieldInstruction_t3462875981_marshal_pinvoke_back, YieldInstruction_t3462875981_marshal_pinvoke_cleanup, NULL, NULL, &YieldInstruction_t3462875981_0_0_0 } /* UnityEngine.YieldInstruction */,
	{ DelegatePInvokeWrapper_DispatcherFactory_t1307565918, NULL, NULL, NULL, NULL, NULL, &DispatcherFactory_t1307565918_0_0_0 } /* Boo.Lang.Runtime.DynamicDispatching.DispatcherCache/DispatcherFactory */,
	{ NULL, RaycastResult_t21186376_marshal_pinvoke, RaycastResult_t21186376_marshal_pinvoke_back, RaycastResult_t21186376_marshal_pinvoke_cleanup, NULL, NULL, &RaycastResult_t21186376_0_0_0 } /* UnityEngine.EventSystems.RaycastResult */,
	{ NULL, ColorTween_t3438117476_marshal_pinvoke, ColorTween_t3438117476_marshal_pinvoke_back, ColorTween_t3438117476_marshal_pinvoke_cleanup, NULL, NULL, &ColorTween_t3438117476_0_0_0 } /* UnityEngine.UI.CoroutineTween.ColorTween */,
	{ NULL, FloatTween_t2986189219_marshal_pinvoke, FloatTween_t2986189219_marshal_pinvoke_back, FloatTween_t2986189219_marshal_pinvoke_cleanup, NULL, NULL, &FloatTween_t2986189219_0_0_0 } /* UnityEngine.UI.CoroutineTween.FloatTween */,
	{ NULL, Resources_t2975512894_marshal_pinvoke, Resources_t2975512894_marshal_pinvoke_back, Resources_t2975512894_marshal_pinvoke_cleanup, NULL, NULL, &Resources_t2975512894_0_0_0 } /* UnityEngine.UI.DefaultControls/Resources */,
	{ DelegatePInvokeWrapper_OnValidateInput_t1946318473, NULL, NULL, NULL, NULL, NULL, &OnValidateInput_t1946318473_0_0_0 } /* UnityEngine.UI.InputField/OnValidateInput */,
	{ NULL, Navigation_t1571958496_marshal_pinvoke, Navigation_t1571958496_marshal_pinvoke_back, Navigation_t1571958496_marshal_pinvoke_cleanup, NULL, NULL, &Navigation_t1571958496_0_0_0 } /* UnityEngine.UI.Navigation */,
	{ NULL, SpriteState_t1353336012_marshal_pinvoke, SpriteState_t1353336012_marshal_pinvoke_back, SpriteState_t1353336012_marshal_pinvoke_cleanup, NULL, NULL, &SpriteState_t1353336012_0_0_0 } /* UnityEngine.UI.SpriteState */,
	{ NULL, ARAnchor_t1161832412_marshal_pinvoke, ARAnchor_t1161832412_marshal_pinvoke_back, ARAnchor_t1161832412_marshal_pinvoke_cleanup, NULL, NULL, &ARAnchor_t1161832412_0_0_0 } /* UnityEngine.XR.iOS.ARAnchor */,
	{ NULL, ARHitTestResult_t3275513025_marshal_pinvoke, ARHitTestResult_t3275513025_marshal_pinvoke_back, ARHitTestResult_t3275513025_marshal_pinvoke_cleanup, NULL, NULL, &ARHitTestResult_t3275513025_0_0_0 } /* UnityEngine.XR.iOS.ARHitTestResult */,
	{ NULL, ARKitSessionConfiguration_t318899795_marshal_pinvoke, ARKitSessionConfiguration_t318899795_marshal_pinvoke_back, ARKitSessionConfiguration_t318899795_marshal_pinvoke_cleanup, NULL, NULL, &ARKitSessionConfiguration_t318899795_0_0_0 } /* UnityEngine.XR.iOS.ARKitSessionConfiguration */,
	{ NULL, ARKitWorldTackingSessionConfiguration_t1821734930_marshal_pinvoke, ARKitWorldTackingSessionConfiguration_t1821734930_marshal_pinvoke_back, ARKitWorldTackingSessionConfiguration_t1821734930_marshal_pinvoke_cleanup, NULL, NULL, &ARKitWorldTackingSessionConfiguration_t1821734930_0_0_0 } /* UnityEngine.XR.iOS.ARKitWorldTackingSessionConfiguration */,
	{ NULL, ARPlaneAnchor_t1439520888_marshal_pinvoke, ARPlaneAnchor_t1439520888_marshal_pinvoke_back, ARPlaneAnchor_t1439520888_marshal_pinvoke_cleanup, NULL, NULL, &ARPlaneAnchor_t1439520888_0_0_0 } /* UnityEngine.XR.iOS.ARPlaneAnchor */,
	{ NULL, UnityARCamera_t4198559457_marshal_pinvoke, UnityARCamera_t4198559457_marshal_pinvoke_back, UnityARCamera_t4198559457_marshal_pinvoke_cleanup, NULL, NULL, &UnityARCamera_t4198559457_0_0_0 } /* UnityEngine.XR.iOS.UnityARCamera */,
	{ NULL, UnityARHitTestResult_t4129824344_marshal_pinvoke, UnityARHitTestResult_t4129824344_marshal_pinvoke_back, UnityARHitTestResult_t4129824344_marshal_pinvoke_cleanup, NULL, NULL, &UnityARHitTestResult_t4129824344_0_0_0 } /* UnityEngine.XR.iOS.UnityARHitTestResult */,
	{ DelegatePInvokeWrapper_ARAnchorAdded_t2646854145, NULL, NULL, NULL, NULL, NULL, &ARAnchorAdded_t2646854145_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorAdded */,
	{ DelegatePInvokeWrapper_ARAnchorRemoved_t142665927, NULL, NULL, NULL, NULL, NULL, &ARAnchorRemoved_t142665927_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorRemoved */,
	{ DelegatePInvokeWrapper_ARAnchorUpdated_t3886071158, NULL, NULL, NULL, NULL, NULL, &ARAnchorUpdated_t3886071158_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorUpdated */,
	{ DelegatePInvokeWrapper_ARSessionFailed_t872580813, NULL, NULL, NULL, NULL, NULL, &ARSessionFailed_t872580813_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARSessionFailed */,
	{ DelegatePInvokeWrapper_internal_ARAnchorAdded_t1622117597, NULL, NULL, NULL, NULL, NULL, &internal_ARAnchorAdded_t1622117597_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorAdded */,
	{ DelegatePInvokeWrapper_internal_ARAnchorRemoved_t3189755211, NULL, NULL, NULL, NULL, NULL, &internal_ARAnchorRemoved_t3189755211_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorRemoved */,
	{ DelegatePInvokeWrapper_internal_ARAnchorUpdated_t3705772742, NULL, NULL, NULL, NULL, NULL, &internal_ARAnchorUpdated_t3705772742_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorUpdated */,
	{ DelegatePInvokeWrapper_internal_ARFrameUpdate_t3296518558, NULL, NULL, NULL, NULL, NULL, &internal_ARFrameUpdate_t3296518558_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFrameUpdate */,
	{ NULL, AotCompilation_t21908242_marshal_pinvoke, AotCompilation_t21908242_marshal_pinvoke_back, AotCompilation_t21908242_marshal_pinvoke_cleanup, NULL, NULL, &AotCompilation_t21908242_0_0_0 } /* FullSerializer.fsAotCompilationManager/AotCompilation */,
	{ NULL, fsResult_t862419890_marshal_pinvoke, fsResult_t862419890_marshal_pinvoke_back, fsResult_t862419890_marshal_pinvoke_cleanup, NULL, NULL, &fsResult_t862419890_0_0_0 } /* FullSerializer.fsResult */,
	{ NULL, AttributeQuery_t604298480_marshal_pinvoke, AttributeQuery_t604298480_marshal_pinvoke_back, AttributeQuery_t604298480_marshal_pinvoke_cleanup, NULL, NULL, &AttributeQuery_t604298480_0_0_0 } /* FullSerializer.Internal.fsPortableReflection/AttributeQuery */,
	{ NULL, fsVersionedType_t654750358_marshal_pinvoke, fsVersionedType_t654750358_marshal_pinvoke_back, fsVersionedType_t654750358_marshal_pinvoke_cleanup, NULL, NULL, &fsVersionedType_t654750358_0_0_0 } /* FullSerializer.Internal.fsVersionedType */,
	{ DelegatePInvokeWrapper_ProgressEvent_t4185145044, NULL, NULL, NULL, NULL, NULL, &ProgressEvent_t4185145044_0_0_0 } /* IBM.Watson.DeveloperCloud.Connection.RESTConnector/ProgressEvent */,
	{ DelegatePInvokeWrapper_GetDebugInfo_t2665501740, NULL, NULL, NULL, NULL, NULL, &GetDebugInfo_t2665501740_0_0_0 } /* IBM.Watson.DeveloperCloud.Debug.DebugConsole/GetDebugInfo */,
	{ DelegatePInvokeWrapper_OnTestsComplete_t1412473303, NULL, NULL, NULL, NULL, NULL, &OnTestsComplete_t1412473303_0_0_0 } /* IBM.Watson.DeveloperCloud.Editor.UnitTestManager/OnTestsComplete */,
	{ DelegatePInvokeWrapper_OnMessageCallback_t476834366, NULL, NULL, NULL, NULL, NULL, &OnMessageCallback_t476834366_0_0_0 } /* IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.ConversationExperimental/OnMessageCallback */,
	{ DelegatePInvokeWrapper_OnDeleteCollection_t603439291, NULL, NULL, NULL, NULL, NULL, &OnDeleteCollection_t603439291_0_0_0 } /* IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/OnDeleteCollection */,
	{ DelegatePInvokeWrapper_OnDeleteConfiguration_t1574428889, NULL, NULL, NULL, NULL, NULL, &OnDeleteConfiguration_t1574428889_0_0_0 } /* IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/OnDeleteConfiguration */,
	{ DelegatePInvokeWrapper_OnDeleteDocument_t3959738954, NULL, NULL, NULL, NULL, NULL, &OnDeleteDocument_t3959738954_0_0_0 } /* IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/OnDeleteDocument */,
	{ DelegatePInvokeWrapper_OnDeleteEnvironment_t1826514308, NULL, NULL, NULL, NULL, NULL, &OnDeleteEnvironment_t1826514308_0_0_0 } /* IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/OnDeleteEnvironment */,
	{ DelegatePInvokeWrapper_LoadFileDelegate_t484637430, NULL, NULL, NULL, NULL, NULL, &LoadFileDelegate_t484637430_0_0_0 } /* IBM.Watson.DeveloperCloud.Services.DocumentConversion.v1.DocumentConversion/LoadFileDelegate */,
	{ DelegatePInvokeWrapper_IdentifyCallback_t942722308, NULL, NULL, NULL, NULL, NULL, &IdentifyCallback_t942722308_0_0_0 } /* IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.LanguageTranslation/IdentifyCallback */,
	{ DelegatePInvokeWrapper_IdentifyCallback_t682135812, NULL, NULL, NULL, NULL, NULL, &IdentifyCallback_t682135812_0_0_0 } /* IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.LanguageTranslator/IdentifyCallback */,
	{ DelegatePInvokeWrapper_OnDeleteClassifier_t3710463002, NULL, NULL, NULL, NULL, NULL, &OnDeleteClassifier_t3710463002_0_0_0 } /* IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.NaturalLanguageClassifier/OnDeleteClassifier */,
	{ DelegatePInvokeWrapper_LoadFileDelegate_t1720639326, NULL, NULL, NULL, NULL, NULL, &LoadFileDelegate_t1720639326_0_0_0 } /* IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/LoadFileDelegate */,
	{ DelegatePInvokeWrapper_OnDeleteCluster_t1281715199, NULL, NULL, NULL, NULL, NULL, &OnDeleteCluster_t1281715199_0_0_0 } /* IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/OnDeleteCluster */,
	{ DelegatePInvokeWrapper_OnDeleteClusterConfig_t2578936137, NULL, NULL, NULL, NULL, NULL, &OnDeleteClusterConfig_t2578936137_0_0_0 } /* IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/OnDeleteClusterConfig */,
	{ DelegatePInvokeWrapper_OnDeleteRanker_t2889964448, NULL, NULL, NULL, NULL, NULL, &OnDeleteRanker_t2889964448_0_0_0 } /* IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/OnDeleteRanker */,
	{ DelegatePInvokeWrapper_OnGetClusterConfig_t1069230450, NULL, NULL, NULL, NULL, NULL, &OnGetClusterConfig_t1069230450_0_0_0 } /* IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/OnGetClusterConfig */,
	{ DelegatePInvokeWrapper_OnSaveClusterConfig_t1862884805, NULL, NULL, NULL, NULL, NULL, &OnSaveClusterConfig_t1862884805_0_0_0 } /* IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/OnSaveClusterConfig */,
	{ DelegatePInvokeWrapper_SaveFileDelegate_t3125227661, NULL, NULL, NULL, NULL, NULL, &SaveFileDelegate_t3125227661_0_0_0 } /* IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/SaveFileDelegate */,
	{ DelegatePInvokeWrapper_ServiceStatus_t1443707987, NULL, NULL, NULL, NULL, NULL, &ServiceStatus_t1443707987_0_0_0 } /* IBM.Watson.DeveloperCloud.Services.ServiceStatus */,
	{ DelegatePInvokeWrapper_AddCustomWordsCallback_t4067999203, NULL, NULL, NULL, NULL, NULL, &AddCustomWordsCallback_t4067999203_0_0_0 } /* IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/AddCustomWordsCallback */,
	{ DelegatePInvokeWrapper_ErrorEvent_t3049611749, NULL, NULL, NULL, NULL, NULL, &ErrorEvent_t3049611749_0_0_0 } /* IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/ErrorEvent */,
	{ DelegatePInvokeWrapper_LoadFileDelegate_t351561590, NULL, NULL, NULL, NULL, NULL, &LoadFileDelegate_t351561590_0_0_0 } /* IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/LoadFileDelegate */,
	{ DelegatePInvokeWrapper_OnAddCustomCorpusCallback_t1567930907, NULL, NULL, NULL, NULL, NULL, &OnAddCustomCorpusCallback_t1567930907_0_0_0 } /* IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/OnAddCustomCorpusCallback */,
	{ DelegatePInvokeWrapper_OnDeleteCustomCorpusCallback_t3393304107, NULL, NULL, NULL, NULL, NULL, &OnDeleteCustomCorpusCallback_t3393304107_0_0_0 } /* IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/OnDeleteCustomCorpusCallback */,
	{ DelegatePInvokeWrapper_OnDeleteCustomizationCallback_t1940302465, NULL, NULL, NULL, NULL, NULL, &OnDeleteCustomizationCallback_t1940302465_0_0_0 } /* IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/OnDeleteCustomizationCallback */,
	{ DelegatePInvokeWrapper_OnDeleteCustomWordCallback_t2228320037, NULL, NULL, NULL, NULL, NULL, &OnDeleteCustomWordCallback_t2228320037_0_0_0 } /* IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/OnDeleteCustomWordCallback */,
	{ DelegatePInvokeWrapper_ResetCustomizationCallback_t1922530872, NULL, NULL, NULL, NULL, NULL, &ResetCustomizationCallback_t1922530872_0_0_0 } /* IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/ResetCustomizationCallback */,
	{ DelegatePInvokeWrapper_TrainCustomizationCallback_t2705176209, NULL, NULL, NULL, NULL, NULL, &TrainCustomizationCallback_t2705176209_0_0_0 } /* IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/TrainCustomizationCallback */,
	{ DelegatePInvokeWrapper_UpgradeCustomizationCallback_t567321637, NULL, NULL, NULL, NULL, NULL, &UpgradeCustomizationCallback_t567321637_0_0_0 } /* IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/UpgradeCustomizationCallback */,
	{ DelegatePInvokeWrapper_AddCustomizationWordCallback_t3775539180, NULL, NULL, NULL, NULL, NULL, &AddCustomizationWordCallback_t3775539180_0_0_0 } /* IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordCallback */,
	{ DelegatePInvokeWrapper_AddCustomizationWordsCallback_t2578578585, NULL, NULL, NULL, NULL, NULL, &AddCustomizationWordsCallback_t2578578585_0_0_0 } /* IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordsCallback */,
	{ DelegatePInvokeWrapper_OnDeleteCustomizationCallback_t2862971265, NULL, NULL, NULL, NULL, NULL, &OnDeleteCustomizationCallback_t2862971265_0_0_0 } /* IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/OnDeleteCustomizationCallback */,
	{ DelegatePInvokeWrapper_OnDeleteCustomizationWordCallback_t2544578463, NULL, NULL, NULL, NULL, NULL, &OnDeleteCustomizationWordCallback_t2544578463_0_0_0 } /* IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/OnDeleteCustomizationWordCallback */,
	{ DelegatePInvokeWrapper_UpdateCustomizationCallback_t3026233620, NULL, NULL, NULL, NULL, NULL, &UpdateCustomizationCallback_t3026233620_0_0_0 } /* IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/UpdateCustomizationCallback */,
	{ DelegatePInvokeWrapper_LoadFileDelegate_t3728218844, NULL, NULL, NULL, NULL, NULL, &LoadFileDelegate_t3728218844_0_0_0 } /* IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/LoadFileDelegate */,
	{ DelegatePInvokeWrapper_OnDeleteClassifier_t917631666, NULL, NULL, NULL, NULL, NULL, &OnDeleteClassifier_t917631666_0_0_0 } /* IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/OnDeleteClassifier */,
	{ DelegatePInvokeWrapper_OnDeleteCollection_t1354946561, NULL, NULL, NULL, NULL, NULL, &OnDeleteCollection_t1354946561_0_0_0 } /* IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/OnDeleteCollection */,
	{ DelegatePInvokeWrapper_OnDeleteCollectionImage_t241020072, NULL, NULL, NULL, NULL, NULL, &OnDeleteCollectionImage_t241020072_0_0_0 } /* IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/OnDeleteCollectionImage */,
	{ DelegatePInvokeWrapper_OnDeleteImageMetadata_t2373116469, NULL, NULL, NULL, NULL, NULL, &OnDeleteImageMetadata_t2373116469_0_0_0 } /* IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/OnDeleteImageMetadata */,
	{ NULL, TouchData_t3880599975_marshal_pinvoke, TouchData_t3880599975_marshal_pinvoke_back, TouchData_t3880599975_marshal_pinvoke_cleanup, NULL, NULL, &TouchData_t3880599975_0_0_0 } /* TouchScript.Gestures.UI.UIGesture/TouchData */,
	{ NULL, TouchHit_t4186847494_marshal_pinvoke, TouchHit_t4186847494_marshal_pinvoke_back, TouchHit_t4186847494_marshal_pinvoke_cleanup, NULL, NULL, &TouchHit_t4186847494_0_0_0 } /* TouchScript.Hit.TouchHit */,
	NULL,
};
