﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.TimeStamp
struct TimeStamp_t82276870;
// System.String
struct String_t;
// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Word
struct Word_t3204175642;
// System.String[]
struct StringU5BU5D_t1642385972;
// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordAlternativeResult
struct WordAlternativeResult_t3127007908;
// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordAlternativeResults
struct WordAlternativeResults_t1270965811;
// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordAlternativeResult[]
struct WordAlternativeResultU5BU5D_t598168141;
// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordConfidence
struct WordConfidence_t2757029394;
// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordData
struct WordData_t612265514;
// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Words
struct Words_t4062187109;
// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Word[]
struct WordU5BU5D_t2800973631;
// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordsList
struct WordsList_t29283159;
// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordData[]
struct WordDataU5BU5D_t1726140911;
// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordType
struct WordType_t1970187882;
// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordTypeToAdd
struct WordTypeToAdd_t2941578038;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Customization
struct Customization_t2261013253;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomizationID
struct CustomizationID_t344279642;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Customizations
struct Customizations_t482380184;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Customization[]
struct CustomizationU5BU5D_t626929480;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomizationWords
struct CustomizationWords_t949522428;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Word[]
struct WordU5BU5D_t3686373631;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Word
struct Word_t4274554970;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomVoice
struct CustomVoice_t330695553;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomVoiceUpdate
struct CustomVoiceUpdate_t1706248840;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.ErrorModel
struct ErrorModel_t3210737207;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Pronunciation
struct Pronunciation_t2711344207;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech
struct TextToSpeech_t3349357562;
// System.Collections.Generic.Dictionary`2<IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.VoiceType,System.String>
struct Dictionary_2_t1569883280;
// System.Collections.Generic.Dictionary`2<IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.VoiceType,System.Object>
struct Dictionary_2_t2230112342;
// System.Collections.Generic.Dictionary`2<IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.AudioFormatType,System.String>
struct Dictionary_2_t2576372715;
// System.Collections.Generic.Dictionary`2<IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.AudioFormatType,System.Object>
struct Dictionary_2_t3236601777;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetVoicesCallback
struct GetVoicesCallback_t3012885511;
// System.ArgumentNullException
struct ArgumentNullException_t628810857;
// IBM.Watson.DeveloperCloud.Connection.RESTConnector
struct RESTConnector_t3705102247;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetVoicesReq
struct GetVoicesReq_t1741653746;
// IBM.Watson.DeveloperCloud.Connection.RESTConnector/ResponseEvent
struct ResponseEvent_t1616568356;
// IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request
struct Request_t466816980;
// IBM.Watson.DeveloperCloud.Connection.RESTConnector/Response
struct Response_t429319368;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Voices
struct Voices_t4221445733;
// System.Text.Encoding
struct Encoding_t663144255;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// FullSerializer.fsData
struct fsData_t2583805605;
// IBM.Watson.DeveloperCloud.Utilities.WatsonException
struct WatsonException_t1186470237;
// System.Type
struct Type_t;
// FullSerializer.fsSerializer
struct fsSerializer_t4193731081;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetVoiceCallback
struct GetVoiceCallback_t2752546046;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetVoiceReq
struct GetVoiceReq_t1980513795;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Voice
struct Voice_t3646862260;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/ToSpeechCallback
struct ToSpeechCallback_t3422748631;
// IBM.Watson.DeveloperCloud.Utilities.DataCache
struct DataCache_t4250340070;
// UnityEngine.AudioClip
struct AudioClip_t1932558630;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/ToSpeechRequest
struct ToSpeechRequest_t3970128895;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t2281509423;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;
// UnityEngine.Object
struct Object_t1021602117;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetPronunciationCallback
struct GetPronunciationCallback_t2622344657;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetPronunciationReq
struct GetPronunciationReq_t2533372478;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationsCallback
struct GetCustomizationsCallback_t201646696;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationsReq
struct GetCustomizationsReq_t2206589443;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/CreateCustomizationCallback
struct CreateCustomizationCallback_t3071584865;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/CreateCustomizationRequest
struct CreateCustomizationRequest_t295745401;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/OnDeleteCustomizationCallback
struct OnDeleteCustomizationCallback_t2862971265;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/DeleteCustomizationRequest
struct DeleteCustomizationRequest_t4044506578;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationCallback
struct GetCustomizationCallback_t4165371535;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationRequest
struct GetCustomizationRequest_t1351603463;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/UpdateCustomizationCallback
struct UpdateCustomizationCallback_t3026233620;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/UpdateCustomizationRequest
struct UpdateCustomizationRequest_t1669343124;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordsCallback
struct GetCustomizationWordsCallback_t2709615664;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordsRequest
struct GetCustomizationWordsRequest_t110549728;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Words
struct Words_t2948214629;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordsCallback
struct AddCustomizationWordsCallback_t2578578585;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordsRequest
struct AddCustomizationWordsRequest_t993580433;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/OnDeleteCustomizationWordCallback
struct OnDeleteCustomizationWordCallback_t2544578463;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/DeleteCustomizationWordRequest
struct DeleteCustomizationWordRequest_t3182066146;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordCallback
struct GetCustomizationWordCallback_t2485962781;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordRequest
struct GetCustomizationWordRequest_t1280810157;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Translation
struct Translation_t2174867539;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordCallback
struct AddCustomizationWordCallback_t3775539180;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordRequest
struct AddCustomizationWordRequest_t864480132;
// IBM.Watson.DeveloperCloud.Services.ServiceStatus
struct ServiceStatus_t1443707987;
// IBM.Watson.DeveloperCloud.Utilities.Config
struct Config_t3637807320;
// IBM.Watson.DeveloperCloud.Utilities.Config/CredentialInfo
struct CredentialInfo_t34154441;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/CheckServiceStatus
struct CheckServiceStatus_t3651453794;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// System.Delegate
struct Delegate_t3022476291;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Voice[]
struct VoiceU5BU5D_t260285181;
// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.DocumentTone
struct DocumentTone_t2537937785;
// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneCategory[]
struct ToneCategoryU5BU5D_t3565596459;
// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneCategory
struct ToneCategory_t3724297374;
// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.SentenceTone
struct SentenceTone_t3234952115;
// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.Tone[]
struct ToneU5BU5D_t4171600311;
// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.Tone
struct Tone_t1138009090;
// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzer
struct ToneAnalyzer_t1356110496;
// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzer/OnGetToneAnalyzed
struct OnGetToneAnalyzed_t3813655652;
// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzer/GetToneAnalyzerRequest
struct GetToneAnalyzerRequest_t1769916618;
// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzerResponse
struct ToneAnalyzerResponse_t3391014235;
// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzer/CheckServiceStatus
struct CheckServiceStatus_t1108037484;
// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.SentenceTone[]
struct SentenceToneU5BU5D_t255016994;
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Anchor
struct Anchor_t1470404909;
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Position
struct Position_t3597491459;
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.ApplicationData
struct ApplicationData_t1885408412;
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.ApplicationDataValue
struct ApplicationDataValue_t1721671971;
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.CategoricalRange
struct CategoricalRange_t943840333;
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Range
struct Range_t2396059803;
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column
struct Column_t2612486824;
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Config
struct Config_t1927135816;
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Drivers
struct Drivers_t1652706281;
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Params
struct Params_t3159428742;
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.DateRange
struct DateRange_t3049499159;
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.DilemmasResponse
struct DilemmasResponse_t3767705817;
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Problem
struct Problem_t2814813345;
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Resolution
struct Resolution_t3867497320;
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Map
struct Map_t1914475838;
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Anchor[]
struct AnchorU5BU5D_t2762965888;
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Node[]
struct NodeU5BU5D_t283985611;
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Node
struct Node_t3986951550;
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Metrics
struct Metrics_t236865991;
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Option
struct Option_t1775127045;
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column[]
struct ColumnU5BU5D_t4200576441;
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Option[]
struct OptionU5BU5D_t3811736648;
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Solution[]
struct SolutionU5BU5D_t2153136770;
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Solution
struct Solution_t204511443;
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.StatusCause
struct StatusCause_t3777693303;
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics
struct TradeoffAnalytics_t100002595;
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics/OnDilemma
struct OnDilemma_t401820821;
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics/GetDilemmaRequest
struct GetDilemmaRequest_t37205599;
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics/CheckServiceStatus
struct CheckServiceStatus_t584740610;
// System.Collections.Generic.List`1<IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column>
struct List_1_t1981607956;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.ValueRange
struct ValueRange_t543481982;
// System.Collections.Generic.List`1<IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Option>
struct List_1_t1144248177;
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics/PingDataValue
struct PingDataValue_t526342856;
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics/PingData
struct PingData_t2961942429;
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.Age
struct Age_t582693723;
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.Class
struct Class_t4054986160;
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyParameters
struct ClassifyParameters_t933043320;
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyPerClassifier
struct ClassifyPerClassifier_t2357796318;
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassResult[]
struct ClassResultU5BU5D_t2895066752;
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassResult
struct ClassResult_t3791036973;
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyTopLevelMultiple
struct ClassifyTopLevelMultiple_t317282463;
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyTopLevelSingle[]
struct ClassifyTopLevelSingleU5BU5D_t3685983982;
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyTopLevelSingle
struct ClassifyTopLevelSingle_t3733844951;
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.WarningInfo[]
struct WarningInfoU5BU5D_t1932079499;
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.WarningInfo
struct WarningInfo_t624028094;
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ErrorInfoNoCode
struct ErrorInfoNoCode_t2062371116;
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyPerClassifier[]
struct ClassifyPerClassifierU5BU5D_t1895762155;
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.CollectionImagesConfig
struct CollectionImagesConfig_t1273369062;
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.CollectionsConfig
struct CollectionsConfig_t2493163663;
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.CollectionImagesConfig[]
struct CollectionImagesConfigU5BU5D_t666950595;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.Collections.Generic.Link[]
struct LinkU5BU5D_t62501539;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_t1241853011;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.Collections.Generic.Dictionary`2/Transform`1<System.String,System.Object,System.Collections.DictionaryEntry>
struct Transform_1_t2738473778;
// System.Collections.Generic.HashSet`1<System.String>
struct HashSet_1_t362681087;
// System.Collections.Generic.Dictionary`2<System.Type,FullSerializer.fsBaseConverter>
struct Dictionary_2_t3179035323;
// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.List`1<FullSerializer.fsObjectProcessor>>
struct Dictionary_2_t3992733373;
// System.Collections.Generic.List`1<FullSerializer.fsConverter>
struct List_1_t4130846565;
// System.Collections.Generic.Dictionary`2<System.Type,FullSerializer.fsDirectConverter>
struct Dictionary_2_t2700818715;
// System.Collections.Generic.List`1<FullSerializer.fsObjectProcessor>
struct List_1_t2055375476;
// FullSerializer.Internal.fsCyclicReferenceManager
struct fsCyclicReferenceManager_t1995018378;
// FullSerializer.fsSerializer/fsLazyCycleDefinitionWriter
struct fsLazyCycleDefinitionWriter_t2327014926;
// FullSerializer.fsContext
struct fsContext_t2896355488;
// System.Text.DecoderFallback
struct DecoderFallback_t1715117820;
// System.Text.EncoderFallback
struct EncoderFallback_t1756452756;
// System.Reflection.Assembly
struct Assembly_t4268412390;
// System.IntPtr[]
struct IntPtrU5BU5D_t169632028;
// System.Collections.IDictionary
struct IDictionary_t596158605;
// System.Collections.Generic.Dictionary`2/Transform`1<System.String,System.String,System.Collections.DictionaryEntry>
struct Transform_1_t766225616;
// System.Collections.Generic.List`1<IBM.Watson.DeveloperCloud.Utilities.Config/CredentialInfo>
struct List_1_t3698242869;
// System.Collections.Generic.List`1<IBM.Watson.DeveloperCloud.Utilities.Config/Variable>
struct List_1_t4101323382;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.Collections.Generic.Dictionary`2<System.String,IBM.Watson.DeveloperCloud.Connection.RESTConnector/Form>
struct Dictionary_2_t2694055125;
// IBM.Watson.DeveloperCloud.Connection.RESTConnector/ProgressEvent
struct ProgressEvent_t4185145044;
// IBM.Watson.DeveloperCloud.Utilities.Credentials
struct Credentials_t1494051450;
// System.Collections.Generic.Dictionary`2<System.String,IBM.Watson.DeveloperCloud.Connection.RESTConnector>
struct Dictionary_2_t1324914213;
// System.Collections.Generic.Queue`1<IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request>
struct Queue_1_t286473815;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.AudioFormatType[]
struct AudioFormatTypeU5BU5D_t274759192;
// System.Collections.Generic.IEqualityComparer`1<IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.AudioFormatType>
struct IEqualityComparer_1_t1969417023;
// System.Collections.Generic.Dictionary`2/Transform`1<IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.AudioFormatType,System.String,System.Collections.DictionaryEntry>
struct Transform_1_t2162557148;
// System.Collections.Generic.Dictionary`2<System.String,IBM.Watson.DeveloperCloud.Utilities.DataCache/CacheItem>
struct Dictionary_2_t92195697;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.VoiceType[]
struct VoiceTypeU5BU5D_t3563237053;
// System.Collections.Generic.IEqualityComparer`1<IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.VoiceType>
struct IEqualityComparer_1_t193658302;
// System.Collections.Generic.Dictionary`2/Transform`1<IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.VoiceType,System.String,System.Collections.DictionaryEntry>
struct Transform_1_t514852867;
// System.Void
struct Void_t1841601450;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1572802995;
// System.Type[]
struct TypeU5BU5D_t1664964607;
// System.Reflection.MemberFilter
struct MemberFilter_t3405857066;
// UnityEngine.AudioClip/PCMReaderCallback
struct PCMReaderCallback_t3007145346;
// UnityEngine.AudioClip/PCMSetPositionCallback
struct PCMSetPositionCallback_t421863554;

extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Customization_HasData_m1527546784_MetadataUsageId;
extern RuntimeClass* Dictionary_2_t1569883280_il2cpp_TypeInfo_var;
extern RuntimeClass* Dictionary_2_t2576372715_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Dictionary_2__ctor_m3248355552_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_Add_m2797377672_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2__ctor_m3553715201_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_Add_m714379753_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral265785388;
extern Il2CppCodeGenString* _stringLiteral12833108;
extern Il2CppCodeGenString* _stringLiteral259305399;
extern Il2CppCodeGenString* _stringLiteral3063767281;
extern Il2CppCodeGenString* _stringLiteral3213630373;
extern Il2CppCodeGenString* _stringLiteral3546966851;
extern Il2CppCodeGenString* _stringLiteral1742657926;
extern Il2CppCodeGenString* _stringLiteral2170191983;
extern Il2CppCodeGenString* _stringLiteral2896349867;
extern Il2CppCodeGenString* _stringLiteral4200298573;
extern Il2CppCodeGenString* _stringLiteral742976274;
extern Il2CppCodeGenString* _stringLiteral961201264;
extern Il2CppCodeGenString* _stringLiteral3732944705;
extern Il2CppCodeGenString* _stringLiteral2751057514;
extern Il2CppCodeGenString* _stringLiteral1657819649;
extern Il2CppCodeGenString* _stringLiteral911428805;
extern const uint32_t TextToSpeech__ctor_m3182460987_MetadataUsageId;
extern RuntimeClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern RuntimeClass* VoiceType_t981025524_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Dictionary_2_ContainsKey_m1508741739_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_TryGetValue_m2360694639_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral2221276358;
extern Il2CppCodeGenString* _stringLiteral1465181607;
extern const uint32_t TextToSpeech_GetVoiceType_m999042752_MetadataUsageId;
extern RuntimeClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern RuntimeClass* RESTConnector_t3705102247_il2cpp_TypeInfo_var;
extern RuntimeClass* GetVoicesReq_t1741653746_il2cpp_TypeInfo_var;
extern RuntimeClass* ResponseEvent_t1616568356_il2cpp_TypeInfo_var;
extern const RuntimeMethod* TextToSpeech_OnGetVoicesResp_m1902289587_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral1913944053;
extern Il2CppCodeGenString* _stringLiteral2566220529;
extern Il2CppCodeGenString* _stringLiteral2053051312;
extern const uint32_t TextToSpeech_GetVoices_m2898245765_MetadataUsageId;
extern RuntimeClass* Voices_t4221445733_il2cpp_TypeInfo_var;
extern RuntimeClass* Encoding_t663144255_il2cpp_TypeInfo_var;
extern RuntimeClass* WatsonException_t1186470237_il2cpp_TypeInfo_var;
extern RuntimeClass* TextToSpeech_t3349357562_il2cpp_TypeInfo_var;
extern RuntimeClass* Exception_t1927440687_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2263597084;
extern Il2CppCodeGenString* _stringLiteral1468624296;
extern const uint32_t TextToSpeech_OnGetVoicesResp_m1902289587_MetadataUsageId;
extern RuntimeClass* GetVoiceReq_t1980513795_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Nullable_1_get_HasValue_m925507304_RuntimeMethod_var;
extern const RuntimeMethod* Nullable_1__ctor_m2553508639_RuntimeMethod_var;
extern const RuntimeMethod* Nullable_1_get_Value_m2811304662_RuntimeMethod_var;
extern const RuntimeMethod* TextToSpeech_OnGetVoiceResp_m4256129680_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral3015659863;
extern const uint32_t TextToSpeech_GetVoice_m3804271456_MetadataUsageId;
extern RuntimeClass* Voice_t3646862260_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2814884231;
extern const uint32_t TextToSpeech_OnGetVoiceResp_m4256129680_MetadataUsageId;
extern RuntimeClass* AudioFormatType_t2756784245_il2cpp_TypeInfo_var;
extern RuntimeClass* Utility_t666154278_il2cpp_TypeInfo_var;
extern RuntimeClass* DataCache_t4250340070_il2cpp_TypeInfo_var;
extern RuntimeClass* ToSpeechRequest_t3970128895_il2cpp_TypeInfo_var;
extern RuntimeClass* Dictionary_2_t3943999495_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Dictionary_2_ContainsKey_m2816287462_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_get_Item_m2485813692_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_get_Item_m3261277235_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_set_Item_m3411472609_RuntimeMethod_var;
extern const RuntimeMethod* TextToSpeech_ToSpeechResponse_m166731737_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2__ctor_m760167321_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_set_Item_m4244870320_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral3423761293;
extern Il2CppCodeGenString* _stringLiteral2447829758;
extern Il2CppCodeGenString* _stringLiteral2185331571;
extern Il2CppCodeGenString* _stringLiteral1978428949;
extern Il2CppCodeGenString* _stringLiteral2685996351;
extern Il2CppCodeGenString* _stringLiteral1706667859;
extern Il2CppCodeGenString* _stringLiteral3819355434;
extern Il2CppCodeGenString* _stringLiteral795243372;
extern Il2CppCodeGenString* _stringLiteral1048821954;
extern Il2CppCodeGenString* _stringLiteral1391431453;
extern const uint32_t TextToSpeech_ToSpeech_m1235459968_MetadataUsageId;
extern RuntimeClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3588881476;
extern Il2CppCodeGenString* _stringLiteral3238191036;
extern const uint32_t TextToSpeech_ToSpeechResponse_m166731737_MetadataUsageId;
extern const uint32_t TextToSpeech_ProcessResponse_m2113435348_MetadataUsageId;
extern RuntimeClass* GetPronunciationReq_t2533372478_il2cpp_TypeInfo_var;
extern const RuntimeMethod* TextToSpeech_OnGetPronunciationResp_m2803422241_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral2428510368;
extern Il2CppCodeGenString* _stringLiteral1334200623;
extern Il2CppCodeGenString* _stringLiteral2108007291;
extern const uint32_t TextToSpeech_GetPronunciation_m4262540122_MetadataUsageId;
extern RuntimeClass* Pronunciation_t2711344207_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2798243012;
extern Il2CppCodeGenString* _stringLiteral3851805490;
extern const uint32_t TextToSpeech_OnGetPronunciationResp_m2803422241_MetadataUsageId;
extern RuntimeClass* GetCustomizationsReq_t2206589443_il2cpp_TypeInfo_var;
extern const RuntimeMethod* TextToSpeech_OnGetCustomizationsResp_m3083543054_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral3384877589;
extern const uint32_t TextToSpeech_GetCustomizations_m1701334691_MetadataUsageId;
extern RuntimeClass* Customizations_t482380184_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2952090043;
extern const uint32_t TextToSpeech_OnGetCustomizationsResp_m3083543054_MetadataUsageId;
extern RuntimeClass* CustomVoice_t330695553_il2cpp_TypeInfo_var;
extern RuntimeClass* CreateCustomizationRequest_t295745401_il2cpp_TypeInfo_var;
extern const RuntimeMethod* TextToSpeech_OnCreateCustomizationResp_m1680638733_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral701067223;
extern Il2CppCodeGenString* _stringLiteral3819387978;
extern const uint32_t TextToSpeech_CreateCustomization_m1090628485_MetadataUsageId;
extern RuntimeClass* CustomizationID_t344279642_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1155517690;
extern const uint32_t TextToSpeech_OnCreateCustomizationResp_m1680638733_MetadataUsageId;
extern RuntimeClass* DeleteCustomizationRequest_t4044506578_il2cpp_TypeInfo_var;
extern const RuntimeMethod* TextToSpeech_OnDeleteCustomizationResp_m4092725412_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral1878474307;
extern Il2CppCodeGenString* _stringLiteral2160400232;
extern const uint32_t TextToSpeech_DeleteCustomization_m2634893744_MetadataUsageId;
extern const uint32_t TextToSpeech_OnDeleteCustomizationResp_m4092725412_MetadataUsageId;
extern RuntimeClass* GetCustomizationRequest_t1351603463_il2cpp_TypeInfo_var;
extern const RuntimeMethod* TextToSpeech_OnGetCustomizationResp_m2741583135_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral3132213353;
extern const uint32_t TextToSpeech_GetCustomization_m2331125653_MetadataUsageId;
extern RuntimeClass* Customization_t2261013253_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1529540406;
extern const uint32_t TextToSpeech_OnGetCustomizationResp_m2741583135_MetadataUsageId;
extern RuntimeClass* UpdateCustomizationRequest_t1669343124_il2cpp_TypeInfo_var;
extern const RuntimeMethod* TextToSpeech_OnUpdateCustomizationResp_m1295820546_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral1734094646;
extern Il2CppCodeGenString* _stringLiteral62319874;
extern const uint32_t TextToSpeech_UpdateCustomization_m3116906675_MetadataUsageId;
extern const uint32_t TextToSpeech_OnUpdateCustomizationResp_m1295820546_MetadataUsageId;
extern RuntimeClass* GetCustomizationWordsRequest_t110549728_il2cpp_TypeInfo_var;
extern const RuntimeMethod* TextToSpeech_OnGetCustomizationWordsResp_m3329896646_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral1774655039;
extern Il2CppCodeGenString* _stringLiteral3620071178;
extern const uint32_t TextToSpeech_GetCustomizationWords_m2592214181_MetadataUsageId;
extern RuntimeClass* Words_t2948214629_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3622233617;
extern const uint32_t TextToSpeech_OnGetCustomizationWordsResp_m3329896646_MetadataUsageId;
extern RuntimeClass* AddCustomizationWordsRequest_t993580433_il2cpp_TypeInfo_var;
extern const RuntimeMethod* TextToSpeech_OnAddCustomizationWordsResp_m1340193549_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral2371147763;
extern const uint32_t TextToSpeech_AddCustomizationWords_m1578949184_MetadataUsageId;
extern const uint32_t TextToSpeech_OnAddCustomizationWordsResp_m1340193549_MetadataUsageId;
extern RuntimeClass* DeleteCustomizationWordRequest_t3182066146_il2cpp_TypeInfo_var;
extern const RuntimeMethod* TextToSpeech_OnDeleteCustomizationWordResp_m2067181128_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral1088384551;
extern Il2CppCodeGenString* _stringLiteral2593542011;
extern Il2CppCodeGenString* _stringLiteral3235994642;
extern const uint32_t TextToSpeech_DeleteCustomizationWord_m309917500_MetadataUsageId;
extern const uint32_t TextToSpeech_OnDeleteCustomizationWordResp_m2067181128_MetadataUsageId;
extern RuntimeClass* GetCustomizationWordRequest_t1280810157_il2cpp_TypeInfo_var;
extern const RuntimeMethod* TextToSpeech_OnGetCustomizationWordResp_m802699301_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral898991076;
extern const uint32_t TextToSpeech_GetCustomizationWord_m1273455755_MetadataUsageId;
extern RuntimeClass* Translation_t2174867539_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral82143046;
extern const uint32_t TextToSpeech_OnGetCustomizationWordResp_m802699301_MetadataUsageId;
extern RuntimeClass* AddCustomizationWordRequest_t864480132_il2cpp_TypeInfo_var;
extern const RuntimeMethod* TextToSpeech_OnAddCustomizationWordResp_m4145766090_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral1178600151;
extern Il2CppCodeGenString* _stringLiteral4143128774;
extern Il2CppCodeGenString* _stringLiteral426991755;
extern Il2CppCodeGenString* _stringLiteral542026571;
extern Il2CppCodeGenString* _stringLiteral2168002853;
extern Il2CppCodeGenString* _stringLiteral1226460116;
extern Il2CppCodeGenString* _stringLiteral884247665;
extern const uint32_t TextToSpeech_AddCustomizationWord_m352902665_MetadataUsageId;
extern const uint32_t TextToSpeech_OnAddCustomizationWordResp_m4145766090_MetadataUsageId;
extern const uint32_t TextToSpeech_GetServiceID_m1942818028_MetadataUsageId;
extern RuntimeClass* Config_t3637807320_il2cpp_TypeInfo_var;
extern RuntimeClass* CheckServiceStatus_t3651453794_il2cpp_TypeInfo_var;
extern const uint32_t TextToSpeech_GetServiceStatus_m4146958314_MetadataUsageId;
extern RuntimeClass* fsSerializer_t4193731081_il2cpp_TypeInfo_var;
extern const uint32_t TextToSpeech__cctor_m3307453164_MetadataUsageId;
extern RuntimeClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern const uint32_t AddCustomizationWordCallback_BeginInvoke_m1747296861_MetadataUsageId;
extern const uint32_t AddCustomizationWordsCallback_BeginInvoke_m2132419308_MetadataUsageId;
extern RuntimeClass* GetVoicesCallback_t3012885511_il2cpp_TypeInfo_var;
extern const RuntimeMethod* CheckServiceStatus_OnCheckService_m1588091368_RuntimeMethod_var;
extern const uint32_t CheckServiceStatus__ctor_m3941399121_MetadataUsageId;
extern const uint32_t CheckServiceStatus_OnCheckService_m1588091368_MetadataUsageId;
extern const uint32_t OnDeleteCustomizationCallback_BeginInvoke_m2955953730_MetadataUsageId;
extern const uint32_t OnDeleteCustomizationWordCallback_BeginInvoke_m1909293590_MetadataUsageId;
extern const uint32_t UpdateCustomizationCallback_BeginInvoke_m3225856991_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3312868321;
extern const uint32_t DocumentTone_ToString_m851423115_MetadataUsageId;
extern RuntimeClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral631001884;
extern const uint32_t SentenceTone_ToString_m3030164881_MetadataUsageId;
extern RuntimeClass* Double_t4078015681_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1553323438;
extern const uint32_t Tone_ToString_m3031539698_MetadataUsageId;
extern RuntimeClass* GetToneAnalyzerRequest_t1769916618_il2cpp_TypeInfo_var;
extern const RuntimeMethod* ToneAnalyzer_GetToneAnalyzerResponse_m3585275770_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral988188995;
extern Il2CppCodeGenString* _stringLiteral2179659185;
extern Il2CppCodeGenString* _stringLiteral372029312;
extern Il2CppCodeGenString* _stringLiteral3617362;
extern Il2CppCodeGenString* _stringLiteral1900084403;
extern Il2CppCodeGenString* _stringLiteral3177042952;
extern Il2CppCodeGenString* _stringLiteral3323263070;
extern const uint32_t ToneAnalyzer_GetToneAnalyze_m2599887229_MetadataUsageId;
extern RuntimeClass* ToneAnalyzerResponse_t3391014235_il2cpp_TypeInfo_var;
extern RuntimeClass* ToneAnalyzer_t1356110496_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2389966620;
extern Il2CppCodeGenString* _stringLiteral435905614;
extern const uint32_t ToneAnalyzer_GetToneAnalyzerResponse_m3585275770_MetadataUsageId;
extern const uint32_t ToneAnalyzer_GetServiceID_m3304906614_MetadataUsageId;
extern RuntimeClass* CheckServiceStatus_t1108037484_il2cpp_TypeInfo_var;
extern const uint32_t ToneAnalyzer_GetServiceStatus_m2690870132_MetadataUsageId;
extern const uint32_t ToneAnalyzer__cctor_m4035307958_MetadataUsageId;
extern RuntimeClass* OnGetToneAnalyzed_t3813655652_il2cpp_TypeInfo_var;
extern const RuntimeMethod* CheckServiceStatus_OnGetToneAnalyzed_m3084319673_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral3423762534;
extern const uint32_t CheckServiceStatus__ctor_m57170065_MetadataUsageId;
extern const uint32_t CheckServiceStatus_OnGetToneAnalyzed_m3084319673_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1688738322;
extern Il2CppCodeGenString* _stringLiteral164989177;
extern const uint32_t ToneAnalyzerResponse_ToString_m3908036033_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2235585657;
extern const uint32_t ToneCategory_ToString_m2915677366_MetadataUsageId;
extern RuntimeClass* GetDilemmaRequest_t37205599_il2cpp_TypeInfo_var;
extern RuntimeClass* TradeoffAnalytics_t100002595_il2cpp_TypeInfo_var;
extern const RuntimeMethod* TradeoffAnalytics_GetDilemmaResponse_m4122534449_RuntimeMethod_var;
extern const RuntimeMethod* fsSerializer_TrySerialize_TisProblem_t2814813345_m3668382148_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral1036380510;
extern Il2CppCodeGenString* _stringLiteral3719323877;
extern Il2CppCodeGenString* _stringLiteral1968262614;
extern const uint32_t TradeoffAnalytics_GetDilemma_m3487116940_MetadataUsageId;
extern RuntimeClass* DilemmasResponse_t3767705817_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1552884091;
extern Il2CppCodeGenString* _stringLiteral2441933943;
extern const uint32_t TradeoffAnalytics_GetDilemmaResponse_m4122534449_MetadataUsageId;
extern const uint32_t TradeoffAnalytics_GetServiceID_m7835884_MetadataUsageId;
extern RuntimeClass* CheckServiceStatus_t584740610_il2cpp_TypeInfo_var;
extern const uint32_t TradeoffAnalytics_GetServiceStatus_m3183265306_MetadataUsageId;
extern const uint32_t TradeoffAnalytics__cctor_m3384192268_MetadataUsageId;
extern RuntimeClass* Problem_t2814813345_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t1981607956_il2cpp_TypeInfo_var;
extern RuntimeClass* Column_t2612486824_il2cpp_TypeInfo_var;
extern RuntimeClass* ValueRange_t543481982_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t1144248177_il2cpp_TypeInfo_var;
extern RuntimeClass* Option_t1775127045_il2cpp_TypeInfo_var;
extern RuntimeClass* PingDataValue_t526342856_il2cpp_TypeInfo_var;
extern RuntimeClass* OnDilemma_t401820821_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1__ctor_m1814123416_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m952068700_RuntimeMethod_var;
extern const RuntimeMethod* List_1_ToArray_m3175624038_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m3074099389_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m2195314017_RuntimeMethod_var;
extern const RuntimeMethod* List_1_ToArray_m1652548499_RuntimeMethod_var;
extern const RuntimeMethod* CheckServiceStatus_OnGetDilemma_m1023184799_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral2294719762;
extern Il2CppCodeGenString* _stringLiteral2113949763;
extern Il2CppCodeGenString* _stringLiteral339799074;
extern Il2CppCodeGenString* _stringLiteral395016341;
extern const uint32_t CheckServiceStatus__ctor_m3332446737_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1580212492;
extern const uint32_t CheckServiceStatus_OnGetDilemma_m1023184799_MetadataUsageId;
extern const uint32_t CheckServiceStatus_OnFailure_m1533919926_MetadataUsageId;

struct StringU5BU5D_t1642385972;
struct WordAlternativeResultU5BU5D_t598168141;
struct WordU5BU5D_t2800973631;
struct WordDataU5BU5D_t1726140911;
struct CustomizationU5BU5D_t626929480;
struct WordU5BU5D_t3686373631;
struct ObjectU5BU5D_t3614634134;
struct ByteU5BU5D_t3397334013;
struct VoiceU5BU5D_t260285181;
struct ToneCategoryU5BU5D_t3565596459;
struct ToneU5BU5D_t4171600311;
struct SentenceToneU5BU5D_t255016994;
struct AnchorU5BU5D_t2762965888;
struct NodeU5BU5D_t283985611;
struct ColumnU5BU5D_t4200576441;
struct OptionU5BU5D_t3811736648;
struct SolutionU5BU5D_t2153136770;
struct ClassResultU5BU5D_t2895066752;
struct ClassifyTopLevelSingleU5BU5D_t3685983982;
struct WarningInfoU5BU5D_t1932079499;
struct ClassifyPerClassifierU5BU5D_t1895762155;
struct CollectionImagesConfigU5BU5D_t666950595;


#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef METRICS_T236865991_H
#define METRICS_T236865991_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Metrics
struct  Metrics_t236865991  : public RuntimeObject
{
public:
	// System.Double IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Metrics::<final_kappa>k__BackingField
	double ___U3Cfinal_kappaU3Ek__BackingField_0;
	// System.Double IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Metrics::<kappa>k__BackingField
	double ___U3CkappaU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3Cfinal_kappaU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Metrics_t236865991, ___U3Cfinal_kappaU3Ek__BackingField_0)); }
	inline double get_U3Cfinal_kappaU3Ek__BackingField_0() const { return ___U3Cfinal_kappaU3Ek__BackingField_0; }
	inline double* get_address_of_U3Cfinal_kappaU3Ek__BackingField_0() { return &___U3Cfinal_kappaU3Ek__BackingField_0; }
	inline void set_U3Cfinal_kappaU3Ek__BackingField_0(double value)
	{
		___U3Cfinal_kappaU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CkappaU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Metrics_t236865991, ___U3CkappaU3Ek__BackingField_1)); }
	inline double get_U3CkappaU3Ek__BackingField_1() const { return ___U3CkappaU3Ek__BackingField_1; }
	inline double* get_address_of_U3CkappaU3Ek__BackingField_1() { return &___U3CkappaU3Ek__BackingField_1; }
	inline void set_U3CkappaU3Ek__BackingField_1(double value)
	{
		___U3CkappaU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METRICS_T236865991_H
#ifndef OPTION_T1775127045_H
#define OPTION_T1775127045_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Option
struct  Option_t1775127045  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.ApplicationData IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Option::<app_data>k__BackingField
	ApplicationData_t1885408412 * ___U3Capp_dataU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Option::<description_html>k__BackingField
	String_t* ___U3Cdescription_htmlU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Option::<key>k__BackingField
	String_t* ___U3CkeyU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Option::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_3;
	// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.ApplicationDataValue IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Option::<values>k__BackingField
	ApplicationDataValue_t1721671971 * ___U3CvaluesU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3Capp_dataU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Option_t1775127045, ___U3Capp_dataU3Ek__BackingField_0)); }
	inline ApplicationData_t1885408412 * get_U3Capp_dataU3Ek__BackingField_0() const { return ___U3Capp_dataU3Ek__BackingField_0; }
	inline ApplicationData_t1885408412 ** get_address_of_U3Capp_dataU3Ek__BackingField_0() { return &___U3Capp_dataU3Ek__BackingField_0; }
	inline void set_U3Capp_dataU3Ek__BackingField_0(ApplicationData_t1885408412 * value)
	{
		___U3Capp_dataU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Capp_dataU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3Cdescription_htmlU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Option_t1775127045, ___U3Cdescription_htmlU3Ek__BackingField_1)); }
	inline String_t* get_U3Cdescription_htmlU3Ek__BackingField_1() const { return ___U3Cdescription_htmlU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3Cdescription_htmlU3Ek__BackingField_1() { return &___U3Cdescription_htmlU3Ek__BackingField_1; }
	inline void set_U3Cdescription_htmlU3Ek__BackingField_1(String_t* value)
	{
		___U3Cdescription_htmlU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cdescription_htmlU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CkeyU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Option_t1775127045, ___U3CkeyU3Ek__BackingField_2)); }
	inline String_t* get_U3CkeyU3Ek__BackingField_2() const { return ___U3CkeyU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CkeyU3Ek__BackingField_2() { return &___U3CkeyU3Ek__BackingField_2; }
	inline void set_U3CkeyU3Ek__BackingField_2(String_t* value)
	{
		___U3CkeyU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CkeyU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Option_t1775127045, ___U3CnameU3Ek__BackingField_3)); }
	inline String_t* get_U3CnameU3Ek__BackingField_3() const { return ___U3CnameU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_3() { return &___U3CnameU3Ek__BackingField_3; }
	inline void set_U3CnameU3Ek__BackingField_3(String_t* value)
	{
		___U3CnameU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CvaluesU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Option_t1775127045, ___U3CvaluesU3Ek__BackingField_4)); }
	inline ApplicationDataValue_t1721671971 * get_U3CvaluesU3Ek__BackingField_4() const { return ___U3CvaluesU3Ek__BackingField_4; }
	inline ApplicationDataValue_t1721671971 ** get_address_of_U3CvaluesU3Ek__BackingField_4() { return &___U3CvaluesU3Ek__BackingField_4; }
	inline void set_U3CvaluesU3Ek__BackingField_4(ApplicationDataValue_t1721671971 * value)
	{
		___U3CvaluesU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CvaluesU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPTION_T1775127045_H
#ifndef STATUSCAUSE_T3777693303_H
#define STATUSCAUSE_T3777693303_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.StatusCause
struct  StatusCause_t3777693303  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.StatusCause::<error_code>k__BackingField
	String_t* ___U3Cerror_codeU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.StatusCause::<message>k__BackingField
	String_t* ___U3CmessageU3Ek__BackingField_1;
	// System.String[] IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.StatusCause::<tokens>k__BackingField
	StringU5BU5D_t1642385972* ___U3CtokensU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3Cerror_codeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StatusCause_t3777693303, ___U3Cerror_codeU3Ek__BackingField_0)); }
	inline String_t* get_U3Cerror_codeU3Ek__BackingField_0() const { return ___U3Cerror_codeU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3Cerror_codeU3Ek__BackingField_0() { return &___U3Cerror_codeU3Ek__BackingField_0; }
	inline void set_U3Cerror_codeU3Ek__BackingField_0(String_t* value)
	{
		___U3Cerror_codeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cerror_codeU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CmessageU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(StatusCause_t3777693303, ___U3CmessageU3Ek__BackingField_1)); }
	inline String_t* get_U3CmessageU3Ek__BackingField_1() const { return ___U3CmessageU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CmessageU3Ek__BackingField_1() { return &___U3CmessageU3Ek__BackingField_1; }
	inline void set_U3CmessageU3Ek__BackingField_1(String_t* value)
	{
		___U3CmessageU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmessageU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CtokensU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(StatusCause_t3777693303, ___U3CtokensU3Ek__BackingField_2)); }
	inline StringU5BU5D_t1642385972* get_U3CtokensU3Ek__BackingField_2() const { return ___U3CtokensU3Ek__BackingField_2; }
	inline StringU5BU5D_t1642385972** get_address_of_U3CtokensU3Ek__BackingField_2() { return &___U3CtokensU3Ek__BackingField_2; }
	inline void set_U3CtokensU3Ek__BackingField_2(StringU5BU5D_t1642385972* value)
	{
		___U3CtokensU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtokensU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATUSCAUSE_T3777693303_H
#ifndef DICTIONARY_2_T309261261_H
#define DICTIONARY_2_T309261261_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct  Dictionary_2_t309261261  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t3030399641* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t62501539* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	StringU5BU5D_t1642385972* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	ObjectU5BU5D_t3614634134* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t228987430 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t309261261, ___table_4)); }
	inline Int32U5BU5D_t3030399641* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t3030399641** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t3030399641* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t309261261, ___linkSlots_5)); }
	inline LinkU5BU5D_t62501539* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t62501539** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t62501539* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t309261261, ___keySlots_6)); }
	inline StringU5BU5D_t1642385972* get_keySlots_6() const { return ___keySlots_6; }
	inline StringU5BU5D_t1642385972** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(StringU5BU5D_t1642385972* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t309261261, ___valueSlots_7)); }
	inline ObjectU5BU5D_t3614634134* get_valueSlots_7() const { return ___valueSlots_7; }
	inline ObjectU5BU5D_t3614634134** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(ObjectU5BU5D_t3614634134* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t309261261, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t309261261, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t309261261, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t309261261, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t309261261, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t309261261, ___serialization_info_13)); }
	inline SerializationInfo_t228987430 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t228987430 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t228987430 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t309261261, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t309261261_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t2738473778 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t309261261_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t2738473778 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t2738473778 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t2738473778 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T309261261_H
#ifndef SOLUTION_T204511443_H
#define SOLUTION_T204511443_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Solution
struct  Solution_t204511443  : public RuntimeObject
{
public:
	// System.String[] IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Solution::<shadow_me>k__BackingField
	StringU5BU5D_t1642385972* ___U3Cshadow_meU3Ek__BackingField_0;
	// System.String[] IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Solution::<shadows>k__BackingField
	StringU5BU5D_t1642385972* ___U3CshadowsU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Solution::<solution_ref>k__BackingField
	String_t* ___U3Csolution_refU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Solution::<status>k__BackingField
	String_t* ___U3CstatusU3Ek__BackingField_3;
	// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.StatusCause IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Solution::<status_cause>k__BackingField
	StatusCause_t3777693303 * ___U3Cstatus_causeU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3Cshadow_meU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Solution_t204511443, ___U3Cshadow_meU3Ek__BackingField_0)); }
	inline StringU5BU5D_t1642385972* get_U3Cshadow_meU3Ek__BackingField_0() const { return ___U3Cshadow_meU3Ek__BackingField_0; }
	inline StringU5BU5D_t1642385972** get_address_of_U3Cshadow_meU3Ek__BackingField_0() { return &___U3Cshadow_meU3Ek__BackingField_0; }
	inline void set_U3Cshadow_meU3Ek__BackingField_0(StringU5BU5D_t1642385972* value)
	{
		___U3Cshadow_meU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cshadow_meU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CshadowsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Solution_t204511443, ___U3CshadowsU3Ek__BackingField_1)); }
	inline StringU5BU5D_t1642385972* get_U3CshadowsU3Ek__BackingField_1() const { return ___U3CshadowsU3Ek__BackingField_1; }
	inline StringU5BU5D_t1642385972** get_address_of_U3CshadowsU3Ek__BackingField_1() { return &___U3CshadowsU3Ek__BackingField_1; }
	inline void set_U3CshadowsU3Ek__BackingField_1(StringU5BU5D_t1642385972* value)
	{
		___U3CshadowsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CshadowsU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3Csolution_refU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Solution_t204511443, ___U3Csolution_refU3Ek__BackingField_2)); }
	inline String_t* get_U3Csolution_refU3Ek__BackingField_2() const { return ___U3Csolution_refU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3Csolution_refU3Ek__BackingField_2() { return &___U3Csolution_refU3Ek__BackingField_2; }
	inline void set_U3Csolution_refU3Ek__BackingField_2(String_t* value)
	{
		___U3Csolution_refU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3Csolution_refU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CstatusU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Solution_t204511443, ___U3CstatusU3Ek__BackingField_3)); }
	inline String_t* get_U3CstatusU3Ek__BackingField_3() const { return ___U3CstatusU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CstatusU3Ek__BackingField_3() { return &___U3CstatusU3Ek__BackingField_3; }
	inline void set_U3CstatusU3Ek__BackingField_3(String_t* value)
	{
		___U3CstatusU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstatusU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3Cstatus_causeU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Solution_t204511443, ___U3Cstatus_causeU3Ek__BackingField_4)); }
	inline StatusCause_t3777693303 * get_U3Cstatus_causeU3Ek__BackingField_4() const { return ___U3Cstatus_causeU3Ek__BackingField_4; }
	inline StatusCause_t3777693303 ** get_address_of_U3Cstatus_causeU3Ek__BackingField_4() { return &___U3Cstatus_causeU3Ek__BackingField_4; }
	inline void set_U3Cstatus_causeU3Ek__BackingField_4(StatusCause_t3777693303 * value)
	{
		___U3Cstatus_causeU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cstatus_causeU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOLUTION_T204511443_H
#ifndef PROBLEM_T2814813345_H
#define PROBLEM_T2814813345_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Problem
struct  Problem_t2814813345  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column[] IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Problem::<columns>k__BackingField
	ColumnU5BU5D_t4200576441* ___U3CcolumnsU3Ek__BackingField_0;
	// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Option[] IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Problem::<options>k__BackingField
	OptionU5BU5D_t3811736648* ___U3CoptionsU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Problem::<subject>k__BackingField
	String_t* ___U3CsubjectU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CcolumnsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Problem_t2814813345, ___U3CcolumnsU3Ek__BackingField_0)); }
	inline ColumnU5BU5D_t4200576441* get_U3CcolumnsU3Ek__BackingField_0() const { return ___U3CcolumnsU3Ek__BackingField_0; }
	inline ColumnU5BU5D_t4200576441** get_address_of_U3CcolumnsU3Ek__BackingField_0() { return &___U3CcolumnsU3Ek__BackingField_0; }
	inline void set_U3CcolumnsU3Ek__BackingField_0(ColumnU5BU5D_t4200576441* value)
	{
		___U3CcolumnsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcolumnsU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CoptionsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Problem_t2814813345, ___U3CoptionsU3Ek__BackingField_1)); }
	inline OptionU5BU5D_t3811736648* get_U3CoptionsU3Ek__BackingField_1() const { return ___U3CoptionsU3Ek__BackingField_1; }
	inline OptionU5BU5D_t3811736648** get_address_of_U3CoptionsU3Ek__BackingField_1() { return &___U3CoptionsU3Ek__BackingField_1; }
	inline void set_U3CoptionsU3Ek__BackingField_1(OptionU5BU5D_t3811736648* value)
	{
		___U3CoptionsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CoptionsU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CsubjectU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Problem_t2814813345, ___U3CsubjectU3Ek__BackingField_2)); }
	inline String_t* get_U3CsubjectU3Ek__BackingField_2() const { return ___U3CsubjectU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CsubjectU3Ek__BackingField_2() { return &___U3CsubjectU3Ek__BackingField_2; }
	inline void set_U3CsubjectU3Ek__BackingField_2(String_t* value)
	{
		___U3CsubjectU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsubjectU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROBLEM_T2814813345_H
#ifndef DILEMMASRESPONSE_T3767705817_H
#define DILEMMASRESPONSE_T3767705817_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.DilemmasResponse
struct  DilemmasResponse_t3767705817  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Problem IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.DilemmasResponse::<problem>k__BackingField
	Problem_t2814813345 * ___U3CproblemU3Ek__BackingField_0;
	// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Resolution IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.DilemmasResponse::<resolution>k__BackingField
	Resolution_t3867497320 * ___U3CresolutionU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CproblemU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DilemmasResponse_t3767705817, ___U3CproblemU3Ek__BackingField_0)); }
	inline Problem_t2814813345 * get_U3CproblemU3Ek__BackingField_0() const { return ___U3CproblemU3Ek__BackingField_0; }
	inline Problem_t2814813345 ** get_address_of_U3CproblemU3Ek__BackingField_0() { return &___U3CproblemU3Ek__BackingField_0; }
	inline void set_U3CproblemU3Ek__BackingField_0(Problem_t2814813345 * value)
	{
		___U3CproblemU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CproblemU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CresolutionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(DilemmasResponse_t3767705817, ___U3CresolutionU3Ek__BackingField_1)); }
	inline Resolution_t3867497320 * get_U3CresolutionU3Ek__BackingField_1() const { return ___U3CresolutionU3Ek__BackingField_1; }
	inline Resolution_t3867497320 ** get_address_of_U3CresolutionU3Ek__BackingField_1() { return &___U3CresolutionU3Ek__BackingField_1; }
	inline void set_U3CresolutionU3Ek__BackingField_1(Resolution_t3867497320 * value)
	{
		___U3CresolutionU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CresolutionU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DILEMMASRESPONSE_T3767705817_H
#ifndef RESOLUTION_T3867497320_H
#define RESOLUTION_T3867497320_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Resolution
struct  Resolution_t3867497320  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Map IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Resolution::<map>k__BackingField
	Map_t1914475838 * ___U3CmapU3Ek__BackingField_0;
	// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Solution[] IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Resolution::<solutions>k__BackingField
	SolutionU5BU5D_t2153136770* ___U3CsolutionsU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmapU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Resolution_t3867497320, ___U3CmapU3Ek__BackingField_0)); }
	inline Map_t1914475838 * get_U3CmapU3Ek__BackingField_0() const { return ___U3CmapU3Ek__BackingField_0; }
	inline Map_t1914475838 ** get_address_of_U3CmapU3Ek__BackingField_0() { return &___U3CmapU3Ek__BackingField_0; }
	inline void set_U3CmapU3Ek__BackingField_0(Map_t1914475838 * value)
	{
		___U3CmapU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmapU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CsolutionsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Resolution_t3867497320, ___U3CsolutionsU3Ek__BackingField_1)); }
	inline SolutionU5BU5D_t2153136770* get_U3CsolutionsU3Ek__BackingField_1() const { return ___U3CsolutionsU3Ek__BackingField_1; }
	inline SolutionU5BU5D_t2153136770** get_address_of_U3CsolutionsU3Ek__BackingField_1() { return &___U3CsolutionsU3Ek__BackingField_1; }
	inline void set_U3CsolutionsU3Ek__BackingField_1(SolutionU5BU5D_t2153136770* value)
	{
		___U3CsolutionsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsolutionsU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESOLUTION_T3867497320_H
#ifndef NODE_T3986951550_H
#define NODE_T3986951550_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Node
struct  Node_t3986951550  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Position IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Node::<coordinates>k__BackingField
	Position_t3597491459 * ___U3CcoordinatesU3Ek__BackingField_0;
	// System.String[] IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Node::<solution_refs>k__BackingField
	StringU5BU5D_t1642385972* ___U3Csolution_refsU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CcoordinatesU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Node_t3986951550, ___U3CcoordinatesU3Ek__BackingField_0)); }
	inline Position_t3597491459 * get_U3CcoordinatesU3Ek__BackingField_0() const { return ___U3CcoordinatesU3Ek__BackingField_0; }
	inline Position_t3597491459 ** get_address_of_U3CcoordinatesU3Ek__BackingField_0() { return &___U3CcoordinatesU3Ek__BackingField_0; }
	inline void set_U3CcoordinatesU3Ek__BackingField_0(Position_t3597491459 * value)
	{
		___U3CcoordinatesU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcoordinatesU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3Csolution_refsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Node_t3986951550, ___U3Csolution_refsU3Ek__BackingField_1)); }
	inline StringU5BU5D_t1642385972* get_U3Csolution_refsU3Ek__BackingField_1() const { return ___U3Csolution_refsU3Ek__BackingField_1; }
	inline StringU5BU5D_t1642385972** get_address_of_U3Csolution_refsU3Ek__BackingField_1() { return &___U3Csolution_refsU3Ek__BackingField_1; }
	inline void set_U3Csolution_refsU3Ek__BackingField_1(StringU5BU5D_t1642385972* value)
	{
		___U3Csolution_refsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3Csolution_refsU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NODE_T3986951550_H
#ifndef MAP_T1914475838_H
#define MAP_T1914475838_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Map
struct  Map_t1914475838  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Anchor[] IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Map::<anchors>k__BackingField
	AnchorU5BU5D_t2762965888* ___U3CanchorsU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Map::<comments>k__BackingField
	String_t* ___U3CcommentsU3Ek__BackingField_1;
	// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Config IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Map::<config>k__BackingField
	Config_t1927135816 * ___U3CconfigU3Ek__BackingField_2;
	// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Node[] IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Map::<nodes>k__BackingField
	NodeU5BU5D_t283985611* ___U3CnodesU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CanchorsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Map_t1914475838, ___U3CanchorsU3Ek__BackingField_0)); }
	inline AnchorU5BU5D_t2762965888* get_U3CanchorsU3Ek__BackingField_0() const { return ___U3CanchorsU3Ek__BackingField_0; }
	inline AnchorU5BU5D_t2762965888** get_address_of_U3CanchorsU3Ek__BackingField_0() { return &___U3CanchorsU3Ek__BackingField_0; }
	inline void set_U3CanchorsU3Ek__BackingField_0(AnchorU5BU5D_t2762965888* value)
	{
		___U3CanchorsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CanchorsU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CcommentsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Map_t1914475838, ___U3CcommentsU3Ek__BackingField_1)); }
	inline String_t* get_U3CcommentsU3Ek__BackingField_1() const { return ___U3CcommentsU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CcommentsU3Ek__BackingField_1() { return &___U3CcommentsU3Ek__BackingField_1; }
	inline void set_U3CcommentsU3Ek__BackingField_1(String_t* value)
	{
		___U3CcommentsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcommentsU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CconfigU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Map_t1914475838, ___U3CconfigU3Ek__BackingField_2)); }
	inline Config_t1927135816 * get_U3CconfigU3Ek__BackingField_2() const { return ___U3CconfigU3Ek__BackingField_2; }
	inline Config_t1927135816 ** get_address_of_U3CconfigU3Ek__BackingField_2() { return &___U3CconfigU3Ek__BackingField_2; }
	inline void set_U3CconfigU3Ek__BackingField_2(Config_t1927135816 * value)
	{
		___U3CconfigU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CconfigU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CnodesU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Map_t1914475838, ___U3CnodesU3Ek__BackingField_3)); }
	inline NodeU5BU5D_t283985611* get_U3CnodesU3Ek__BackingField_3() const { return ___U3CnodesU3Ek__BackingField_3; }
	inline NodeU5BU5D_t283985611** get_address_of_U3CnodesU3Ek__BackingField_3() { return &___U3CnodesU3Ek__BackingField_3; }
	inline void set_U3CnodesU3Ek__BackingField_3(NodeU5BU5D_t283985611* value)
	{
		___U3CnodesU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnodesU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAP_T1914475838_H
#ifndef FSSERIALIZER_T4193731081_H
#define FSSERIALIZER_T4193731081_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsSerializer
struct  fsSerializer_t4193731081  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,FullSerializer.fsBaseConverter> FullSerializer.fsSerializer::_cachedConverters
	Dictionary_2_t3179035323 * ____cachedConverters_6;
	// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.List`1<FullSerializer.fsObjectProcessor>> FullSerializer.fsSerializer::_cachedProcessors
	Dictionary_2_t3992733373 * ____cachedProcessors_7;
	// System.Collections.Generic.List`1<FullSerializer.fsConverter> FullSerializer.fsSerializer::_availableConverters
	List_1_t4130846565 * ____availableConverters_8;
	// System.Collections.Generic.Dictionary`2<System.Type,FullSerializer.fsDirectConverter> FullSerializer.fsSerializer::_availableDirectConverters
	Dictionary_2_t2700818715 * ____availableDirectConverters_9;
	// System.Collections.Generic.List`1<FullSerializer.fsObjectProcessor> FullSerializer.fsSerializer::_processors
	List_1_t2055375476 * ____processors_10;
	// FullSerializer.Internal.fsCyclicReferenceManager FullSerializer.fsSerializer::_references
	fsCyclicReferenceManager_t1995018378 * ____references_11;
	// FullSerializer.fsSerializer/fsLazyCycleDefinitionWriter FullSerializer.fsSerializer::_lazyReferenceWriter
	fsLazyCycleDefinitionWriter_t2327014926 * ____lazyReferenceWriter_12;
	// FullSerializer.fsContext FullSerializer.fsSerializer::Context
	fsContext_t2896355488 * ___Context_13;

public:
	inline static int32_t get_offset_of__cachedConverters_6() { return static_cast<int32_t>(offsetof(fsSerializer_t4193731081, ____cachedConverters_6)); }
	inline Dictionary_2_t3179035323 * get__cachedConverters_6() const { return ____cachedConverters_6; }
	inline Dictionary_2_t3179035323 ** get_address_of__cachedConverters_6() { return &____cachedConverters_6; }
	inline void set__cachedConverters_6(Dictionary_2_t3179035323 * value)
	{
		____cachedConverters_6 = value;
		Il2CppCodeGenWriteBarrier((&____cachedConverters_6), value);
	}

	inline static int32_t get_offset_of__cachedProcessors_7() { return static_cast<int32_t>(offsetof(fsSerializer_t4193731081, ____cachedProcessors_7)); }
	inline Dictionary_2_t3992733373 * get__cachedProcessors_7() const { return ____cachedProcessors_7; }
	inline Dictionary_2_t3992733373 ** get_address_of__cachedProcessors_7() { return &____cachedProcessors_7; }
	inline void set__cachedProcessors_7(Dictionary_2_t3992733373 * value)
	{
		____cachedProcessors_7 = value;
		Il2CppCodeGenWriteBarrier((&____cachedProcessors_7), value);
	}

	inline static int32_t get_offset_of__availableConverters_8() { return static_cast<int32_t>(offsetof(fsSerializer_t4193731081, ____availableConverters_8)); }
	inline List_1_t4130846565 * get__availableConverters_8() const { return ____availableConverters_8; }
	inline List_1_t4130846565 ** get_address_of__availableConverters_8() { return &____availableConverters_8; }
	inline void set__availableConverters_8(List_1_t4130846565 * value)
	{
		____availableConverters_8 = value;
		Il2CppCodeGenWriteBarrier((&____availableConverters_8), value);
	}

	inline static int32_t get_offset_of__availableDirectConverters_9() { return static_cast<int32_t>(offsetof(fsSerializer_t4193731081, ____availableDirectConverters_9)); }
	inline Dictionary_2_t2700818715 * get__availableDirectConverters_9() const { return ____availableDirectConverters_9; }
	inline Dictionary_2_t2700818715 ** get_address_of__availableDirectConverters_9() { return &____availableDirectConverters_9; }
	inline void set__availableDirectConverters_9(Dictionary_2_t2700818715 * value)
	{
		____availableDirectConverters_9 = value;
		Il2CppCodeGenWriteBarrier((&____availableDirectConverters_9), value);
	}

	inline static int32_t get_offset_of__processors_10() { return static_cast<int32_t>(offsetof(fsSerializer_t4193731081, ____processors_10)); }
	inline List_1_t2055375476 * get__processors_10() const { return ____processors_10; }
	inline List_1_t2055375476 ** get_address_of__processors_10() { return &____processors_10; }
	inline void set__processors_10(List_1_t2055375476 * value)
	{
		____processors_10 = value;
		Il2CppCodeGenWriteBarrier((&____processors_10), value);
	}

	inline static int32_t get_offset_of__references_11() { return static_cast<int32_t>(offsetof(fsSerializer_t4193731081, ____references_11)); }
	inline fsCyclicReferenceManager_t1995018378 * get__references_11() const { return ____references_11; }
	inline fsCyclicReferenceManager_t1995018378 ** get_address_of__references_11() { return &____references_11; }
	inline void set__references_11(fsCyclicReferenceManager_t1995018378 * value)
	{
		____references_11 = value;
		Il2CppCodeGenWriteBarrier((&____references_11), value);
	}

	inline static int32_t get_offset_of__lazyReferenceWriter_12() { return static_cast<int32_t>(offsetof(fsSerializer_t4193731081, ____lazyReferenceWriter_12)); }
	inline fsLazyCycleDefinitionWriter_t2327014926 * get__lazyReferenceWriter_12() const { return ____lazyReferenceWriter_12; }
	inline fsLazyCycleDefinitionWriter_t2327014926 ** get_address_of__lazyReferenceWriter_12() { return &____lazyReferenceWriter_12; }
	inline void set__lazyReferenceWriter_12(fsLazyCycleDefinitionWriter_t2327014926 * value)
	{
		____lazyReferenceWriter_12 = value;
		Il2CppCodeGenWriteBarrier((&____lazyReferenceWriter_12), value);
	}

	inline static int32_t get_offset_of_Context_13() { return static_cast<int32_t>(offsetof(fsSerializer_t4193731081, ___Context_13)); }
	inline fsContext_t2896355488 * get_Context_13() const { return ___Context_13; }
	inline fsContext_t2896355488 ** get_address_of_Context_13() { return &___Context_13; }
	inline void set_Context_13(fsContext_t2896355488 * value)
	{
		___Context_13 = value;
		Il2CppCodeGenWriteBarrier((&___Context_13), value);
	}
};

struct fsSerializer_t4193731081_StaticFields
{
public:
	// System.Collections.Generic.HashSet`1<System.String> FullSerializer.fsSerializer::_reservedKeywords
	HashSet_1_t362681087 * ____reservedKeywords_0;

public:
	inline static int32_t get_offset_of__reservedKeywords_0() { return static_cast<int32_t>(offsetof(fsSerializer_t4193731081_StaticFields, ____reservedKeywords_0)); }
	inline HashSet_1_t362681087 * get__reservedKeywords_0() const { return ____reservedKeywords_0; }
	inline HashSet_1_t362681087 ** get_address_of__reservedKeywords_0() { return &____reservedKeywords_0; }
	inline void set__reservedKeywords_0(HashSet_1_t362681087 * value)
	{
		____reservedKeywords_0 = value;
		Il2CppCodeGenWriteBarrier((&____reservedKeywords_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSSERIALIZER_T4193731081_H
#ifndef LIST_1_T1144248177_H
#define LIST_1_T1144248177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Option>
struct  List_1_t1144248177  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	OptionU5BU5D_t3811736648* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t1144248177, ____items_1)); }
	inline OptionU5BU5D_t3811736648* get__items_1() const { return ____items_1; }
	inline OptionU5BU5D_t3811736648** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(OptionU5BU5D_t3811736648* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t1144248177, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t1144248177, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t1144248177_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	OptionU5BU5D_t3811736648* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t1144248177_StaticFields, ___EmptyArray_4)); }
	inline OptionU5BU5D_t3811736648* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline OptionU5BU5D_t3811736648** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(OptionU5BU5D_t3811736648* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T1144248177_H
#ifndef ENCODING_T663144255_H
#define ENCODING_T663144255_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.Encoding
struct  Encoding_t663144255  : public RuntimeObject
{
public:
	// System.Int32 System.Text.Encoding::codePage
	int32_t ___codePage_0;
	// System.Int32 System.Text.Encoding::windows_code_page
	int32_t ___windows_code_page_1;
	// System.Boolean System.Text.Encoding::is_readonly
	bool ___is_readonly_2;
	// System.Text.DecoderFallback System.Text.Encoding::decoder_fallback
	DecoderFallback_t1715117820 * ___decoder_fallback_3;
	// System.Text.EncoderFallback System.Text.Encoding::encoder_fallback
	EncoderFallback_t1756452756 * ___encoder_fallback_4;
	// System.String System.Text.Encoding::body_name
	String_t* ___body_name_8;
	// System.String System.Text.Encoding::encoding_name
	String_t* ___encoding_name_9;
	// System.String System.Text.Encoding::header_name
	String_t* ___header_name_10;
	// System.Boolean System.Text.Encoding::is_mail_news_display
	bool ___is_mail_news_display_11;
	// System.Boolean System.Text.Encoding::is_mail_news_save
	bool ___is_mail_news_save_12;
	// System.Boolean System.Text.Encoding::is_browser_save
	bool ___is_browser_save_13;
	// System.Boolean System.Text.Encoding::is_browser_display
	bool ___is_browser_display_14;
	// System.String System.Text.Encoding::web_name
	String_t* ___web_name_15;

public:
	inline static int32_t get_offset_of_codePage_0() { return static_cast<int32_t>(offsetof(Encoding_t663144255, ___codePage_0)); }
	inline int32_t get_codePage_0() const { return ___codePage_0; }
	inline int32_t* get_address_of_codePage_0() { return &___codePage_0; }
	inline void set_codePage_0(int32_t value)
	{
		___codePage_0 = value;
	}

	inline static int32_t get_offset_of_windows_code_page_1() { return static_cast<int32_t>(offsetof(Encoding_t663144255, ___windows_code_page_1)); }
	inline int32_t get_windows_code_page_1() const { return ___windows_code_page_1; }
	inline int32_t* get_address_of_windows_code_page_1() { return &___windows_code_page_1; }
	inline void set_windows_code_page_1(int32_t value)
	{
		___windows_code_page_1 = value;
	}

	inline static int32_t get_offset_of_is_readonly_2() { return static_cast<int32_t>(offsetof(Encoding_t663144255, ___is_readonly_2)); }
	inline bool get_is_readonly_2() const { return ___is_readonly_2; }
	inline bool* get_address_of_is_readonly_2() { return &___is_readonly_2; }
	inline void set_is_readonly_2(bool value)
	{
		___is_readonly_2 = value;
	}

	inline static int32_t get_offset_of_decoder_fallback_3() { return static_cast<int32_t>(offsetof(Encoding_t663144255, ___decoder_fallback_3)); }
	inline DecoderFallback_t1715117820 * get_decoder_fallback_3() const { return ___decoder_fallback_3; }
	inline DecoderFallback_t1715117820 ** get_address_of_decoder_fallback_3() { return &___decoder_fallback_3; }
	inline void set_decoder_fallback_3(DecoderFallback_t1715117820 * value)
	{
		___decoder_fallback_3 = value;
		Il2CppCodeGenWriteBarrier((&___decoder_fallback_3), value);
	}

	inline static int32_t get_offset_of_encoder_fallback_4() { return static_cast<int32_t>(offsetof(Encoding_t663144255, ___encoder_fallback_4)); }
	inline EncoderFallback_t1756452756 * get_encoder_fallback_4() const { return ___encoder_fallback_4; }
	inline EncoderFallback_t1756452756 ** get_address_of_encoder_fallback_4() { return &___encoder_fallback_4; }
	inline void set_encoder_fallback_4(EncoderFallback_t1756452756 * value)
	{
		___encoder_fallback_4 = value;
		Il2CppCodeGenWriteBarrier((&___encoder_fallback_4), value);
	}

	inline static int32_t get_offset_of_body_name_8() { return static_cast<int32_t>(offsetof(Encoding_t663144255, ___body_name_8)); }
	inline String_t* get_body_name_8() const { return ___body_name_8; }
	inline String_t** get_address_of_body_name_8() { return &___body_name_8; }
	inline void set_body_name_8(String_t* value)
	{
		___body_name_8 = value;
		Il2CppCodeGenWriteBarrier((&___body_name_8), value);
	}

	inline static int32_t get_offset_of_encoding_name_9() { return static_cast<int32_t>(offsetof(Encoding_t663144255, ___encoding_name_9)); }
	inline String_t* get_encoding_name_9() const { return ___encoding_name_9; }
	inline String_t** get_address_of_encoding_name_9() { return &___encoding_name_9; }
	inline void set_encoding_name_9(String_t* value)
	{
		___encoding_name_9 = value;
		Il2CppCodeGenWriteBarrier((&___encoding_name_9), value);
	}

	inline static int32_t get_offset_of_header_name_10() { return static_cast<int32_t>(offsetof(Encoding_t663144255, ___header_name_10)); }
	inline String_t* get_header_name_10() const { return ___header_name_10; }
	inline String_t** get_address_of_header_name_10() { return &___header_name_10; }
	inline void set_header_name_10(String_t* value)
	{
		___header_name_10 = value;
		Il2CppCodeGenWriteBarrier((&___header_name_10), value);
	}

	inline static int32_t get_offset_of_is_mail_news_display_11() { return static_cast<int32_t>(offsetof(Encoding_t663144255, ___is_mail_news_display_11)); }
	inline bool get_is_mail_news_display_11() const { return ___is_mail_news_display_11; }
	inline bool* get_address_of_is_mail_news_display_11() { return &___is_mail_news_display_11; }
	inline void set_is_mail_news_display_11(bool value)
	{
		___is_mail_news_display_11 = value;
	}

	inline static int32_t get_offset_of_is_mail_news_save_12() { return static_cast<int32_t>(offsetof(Encoding_t663144255, ___is_mail_news_save_12)); }
	inline bool get_is_mail_news_save_12() const { return ___is_mail_news_save_12; }
	inline bool* get_address_of_is_mail_news_save_12() { return &___is_mail_news_save_12; }
	inline void set_is_mail_news_save_12(bool value)
	{
		___is_mail_news_save_12 = value;
	}

	inline static int32_t get_offset_of_is_browser_save_13() { return static_cast<int32_t>(offsetof(Encoding_t663144255, ___is_browser_save_13)); }
	inline bool get_is_browser_save_13() const { return ___is_browser_save_13; }
	inline bool* get_address_of_is_browser_save_13() { return &___is_browser_save_13; }
	inline void set_is_browser_save_13(bool value)
	{
		___is_browser_save_13 = value;
	}

	inline static int32_t get_offset_of_is_browser_display_14() { return static_cast<int32_t>(offsetof(Encoding_t663144255, ___is_browser_display_14)); }
	inline bool get_is_browser_display_14() const { return ___is_browser_display_14; }
	inline bool* get_address_of_is_browser_display_14() { return &___is_browser_display_14; }
	inline void set_is_browser_display_14(bool value)
	{
		___is_browser_display_14 = value;
	}

	inline static int32_t get_offset_of_web_name_15() { return static_cast<int32_t>(offsetof(Encoding_t663144255, ___web_name_15)); }
	inline String_t* get_web_name_15() const { return ___web_name_15; }
	inline String_t** get_address_of_web_name_15() { return &___web_name_15; }
	inline void set_web_name_15(String_t* value)
	{
		___web_name_15 = value;
		Il2CppCodeGenWriteBarrier((&___web_name_15), value);
	}
};

struct Encoding_t663144255_StaticFields
{
public:
	// System.Reflection.Assembly System.Text.Encoding::i18nAssembly
	Assembly_t4268412390 * ___i18nAssembly_5;
	// System.Boolean System.Text.Encoding::i18nDisabled
	bool ___i18nDisabled_6;
	// System.Object[] System.Text.Encoding::encodings
	ObjectU5BU5D_t3614634134* ___encodings_7;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::asciiEncoding
	Encoding_t663144255 * ___asciiEncoding_16;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::bigEndianEncoding
	Encoding_t663144255 * ___bigEndianEncoding_17;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::defaultEncoding
	Encoding_t663144255 * ___defaultEncoding_18;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf7Encoding
	Encoding_t663144255 * ___utf7Encoding_19;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf8EncodingWithMarkers
	Encoding_t663144255 * ___utf8EncodingWithMarkers_20;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf8EncodingWithoutMarkers
	Encoding_t663144255 * ___utf8EncodingWithoutMarkers_21;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::unicodeEncoding
	Encoding_t663144255 * ___unicodeEncoding_22;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::isoLatin1Encoding
	Encoding_t663144255 * ___isoLatin1Encoding_23;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf8EncodingUnsafe
	Encoding_t663144255 * ___utf8EncodingUnsafe_24;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf32Encoding
	Encoding_t663144255 * ___utf32Encoding_25;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::bigEndianUTF32Encoding
	Encoding_t663144255 * ___bigEndianUTF32Encoding_26;
	// System.Object System.Text.Encoding::lockobj
	RuntimeObject * ___lockobj_27;

public:
	inline static int32_t get_offset_of_i18nAssembly_5() { return static_cast<int32_t>(offsetof(Encoding_t663144255_StaticFields, ___i18nAssembly_5)); }
	inline Assembly_t4268412390 * get_i18nAssembly_5() const { return ___i18nAssembly_5; }
	inline Assembly_t4268412390 ** get_address_of_i18nAssembly_5() { return &___i18nAssembly_5; }
	inline void set_i18nAssembly_5(Assembly_t4268412390 * value)
	{
		___i18nAssembly_5 = value;
		Il2CppCodeGenWriteBarrier((&___i18nAssembly_5), value);
	}

	inline static int32_t get_offset_of_i18nDisabled_6() { return static_cast<int32_t>(offsetof(Encoding_t663144255_StaticFields, ___i18nDisabled_6)); }
	inline bool get_i18nDisabled_6() const { return ___i18nDisabled_6; }
	inline bool* get_address_of_i18nDisabled_6() { return &___i18nDisabled_6; }
	inline void set_i18nDisabled_6(bool value)
	{
		___i18nDisabled_6 = value;
	}

	inline static int32_t get_offset_of_encodings_7() { return static_cast<int32_t>(offsetof(Encoding_t663144255_StaticFields, ___encodings_7)); }
	inline ObjectU5BU5D_t3614634134* get_encodings_7() const { return ___encodings_7; }
	inline ObjectU5BU5D_t3614634134** get_address_of_encodings_7() { return &___encodings_7; }
	inline void set_encodings_7(ObjectU5BU5D_t3614634134* value)
	{
		___encodings_7 = value;
		Il2CppCodeGenWriteBarrier((&___encodings_7), value);
	}

	inline static int32_t get_offset_of_asciiEncoding_16() { return static_cast<int32_t>(offsetof(Encoding_t663144255_StaticFields, ___asciiEncoding_16)); }
	inline Encoding_t663144255 * get_asciiEncoding_16() const { return ___asciiEncoding_16; }
	inline Encoding_t663144255 ** get_address_of_asciiEncoding_16() { return &___asciiEncoding_16; }
	inline void set_asciiEncoding_16(Encoding_t663144255 * value)
	{
		___asciiEncoding_16 = value;
		Il2CppCodeGenWriteBarrier((&___asciiEncoding_16), value);
	}

	inline static int32_t get_offset_of_bigEndianEncoding_17() { return static_cast<int32_t>(offsetof(Encoding_t663144255_StaticFields, ___bigEndianEncoding_17)); }
	inline Encoding_t663144255 * get_bigEndianEncoding_17() const { return ___bigEndianEncoding_17; }
	inline Encoding_t663144255 ** get_address_of_bigEndianEncoding_17() { return &___bigEndianEncoding_17; }
	inline void set_bigEndianEncoding_17(Encoding_t663144255 * value)
	{
		___bigEndianEncoding_17 = value;
		Il2CppCodeGenWriteBarrier((&___bigEndianEncoding_17), value);
	}

	inline static int32_t get_offset_of_defaultEncoding_18() { return static_cast<int32_t>(offsetof(Encoding_t663144255_StaticFields, ___defaultEncoding_18)); }
	inline Encoding_t663144255 * get_defaultEncoding_18() const { return ___defaultEncoding_18; }
	inline Encoding_t663144255 ** get_address_of_defaultEncoding_18() { return &___defaultEncoding_18; }
	inline void set_defaultEncoding_18(Encoding_t663144255 * value)
	{
		___defaultEncoding_18 = value;
		Il2CppCodeGenWriteBarrier((&___defaultEncoding_18), value);
	}

	inline static int32_t get_offset_of_utf7Encoding_19() { return static_cast<int32_t>(offsetof(Encoding_t663144255_StaticFields, ___utf7Encoding_19)); }
	inline Encoding_t663144255 * get_utf7Encoding_19() const { return ___utf7Encoding_19; }
	inline Encoding_t663144255 ** get_address_of_utf7Encoding_19() { return &___utf7Encoding_19; }
	inline void set_utf7Encoding_19(Encoding_t663144255 * value)
	{
		___utf7Encoding_19 = value;
		Il2CppCodeGenWriteBarrier((&___utf7Encoding_19), value);
	}

	inline static int32_t get_offset_of_utf8EncodingWithMarkers_20() { return static_cast<int32_t>(offsetof(Encoding_t663144255_StaticFields, ___utf8EncodingWithMarkers_20)); }
	inline Encoding_t663144255 * get_utf8EncodingWithMarkers_20() const { return ___utf8EncodingWithMarkers_20; }
	inline Encoding_t663144255 ** get_address_of_utf8EncodingWithMarkers_20() { return &___utf8EncodingWithMarkers_20; }
	inline void set_utf8EncodingWithMarkers_20(Encoding_t663144255 * value)
	{
		___utf8EncodingWithMarkers_20 = value;
		Il2CppCodeGenWriteBarrier((&___utf8EncodingWithMarkers_20), value);
	}

	inline static int32_t get_offset_of_utf8EncodingWithoutMarkers_21() { return static_cast<int32_t>(offsetof(Encoding_t663144255_StaticFields, ___utf8EncodingWithoutMarkers_21)); }
	inline Encoding_t663144255 * get_utf8EncodingWithoutMarkers_21() const { return ___utf8EncodingWithoutMarkers_21; }
	inline Encoding_t663144255 ** get_address_of_utf8EncodingWithoutMarkers_21() { return &___utf8EncodingWithoutMarkers_21; }
	inline void set_utf8EncodingWithoutMarkers_21(Encoding_t663144255 * value)
	{
		___utf8EncodingWithoutMarkers_21 = value;
		Il2CppCodeGenWriteBarrier((&___utf8EncodingWithoutMarkers_21), value);
	}

	inline static int32_t get_offset_of_unicodeEncoding_22() { return static_cast<int32_t>(offsetof(Encoding_t663144255_StaticFields, ___unicodeEncoding_22)); }
	inline Encoding_t663144255 * get_unicodeEncoding_22() const { return ___unicodeEncoding_22; }
	inline Encoding_t663144255 ** get_address_of_unicodeEncoding_22() { return &___unicodeEncoding_22; }
	inline void set_unicodeEncoding_22(Encoding_t663144255 * value)
	{
		___unicodeEncoding_22 = value;
		Il2CppCodeGenWriteBarrier((&___unicodeEncoding_22), value);
	}

	inline static int32_t get_offset_of_isoLatin1Encoding_23() { return static_cast<int32_t>(offsetof(Encoding_t663144255_StaticFields, ___isoLatin1Encoding_23)); }
	inline Encoding_t663144255 * get_isoLatin1Encoding_23() const { return ___isoLatin1Encoding_23; }
	inline Encoding_t663144255 ** get_address_of_isoLatin1Encoding_23() { return &___isoLatin1Encoding_23; }
	inline void set_isoLatin1Encoding_23(Encoding_t663144255 * value)
	{
		___isoLatin1Encoding_23 = value;
		Il2CppCodeGenWriteBarrier((&___isoLatin1Encoding_23), value);
	}

	inline static int32_t get_offset_of_utf8EncodingUnsafe_24() { return static_cast<int32_t>(offsetof(Encoding_t663144255_StaticFields, ___utf8EncodingUnsafe_24)); }
	inline Encoding_t663144255 * get_utf8EncodingUnsafe_24() const { return ___utf8EncodingUnsafe_24; }
	inline Encoding_t663144255 ** get_address_of_utf8EncodingUnsafe_24() { return &___utf8EncodingUnsafe_24; }
	inline void set_utf8EncodingUnsafe_24(Encoding_t663144255 * value)
	{
		___utf8EncodingUnsafe_24 = value;
		Il2CppCodeGenWriteBarrier((&___utf8EncodingUnsafe_24), value);
	}

	inline static int32_t get_offset_of_utf32Encoding_25() { return static_cast<int32_t>(offsetof(Encoding_t663144255_StaticFields, ___utf32Encoding_25)); }
	inline Encoding_t663144255 * get_utf32Encoding_25() const { return ___utf32Encoding_25; }
	inline Encoding_t663144255 ** get_address_of_utf32Encoding_25() { return &___utf32Encoding_25; }
	inline void set_utf32Encoding_25(Encoding_t663144255 * value)
	{
		___utf32Encoding_25 = value;
		Il2CppCodeGenWriteBarrier((&___utf32Encoding_25), value);
	}

	inline static int32_t get_offset_of_bigEndianUTF32Encoding_26() { return static_cast<int32_t>(offsetof(Encoding_t663144255_StaticFields, ___bigEndianUTF32Encoding_26)); }
	inline Encoding_t663144255 * get_bigEndianUTF32Encoding_26() const { return ___bigEndianUTF32Encoding_26; }
	inline Encoding_t663144255 ** get_address_of_bigEndianUTF32Encoding_26() { return &___bigEndianUTF32Encoding_26; }
	inline void set_bigEndianUTF32Encoding_26(Encoding_t663144255 * value)
	{
		___bigEndianUTF32Encoding_26 = value;
		Il2CppCodeGenWriteBarrier((&___bigEndianUTF32Encoding_26), value);
	}

	inline static int32_t get_offset_of_lockobj_27() { return static_cast<int32_t>(offsetof(Encoding_t663144255_StaticFields, ___lockobj_27)); }
	inline RuntimeObject * get_lockobj_27() const { return ___lockobj_27; }
	inline RuntimeObject ** get_address_of_lockobj_27() { return &___lockobj_27; }
	inline void set_lockobj_27(RuntimeObject * value)
	{
		___lockobj_27 = value;
		Il2CppCodeGenWriteBarrier((&___lockobj_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCODING_T663144255_H
#ifndef VOICES_T4221445733_H
#define VOICES_T4221445733_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Voices
struct  Voices_t4221445733  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Voice[] IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Voices::<voices>k__BackingField
	VoiceU5BU5D_t260285181* ___U3CvoicesU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CvoicesU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Voices_t4221445733, ___U3CvoicesU3Ek__BackingField_0)); }
	inline VoiceU5BU5D_t260285181* get_U3CvoicesU3Ek__BackingField_0() const { return ___U3CvoicesU3Ek__BackingField_0; }
	inline VoiceU5BU5D_t260285181** get_address_of_U3CvoicesU3Ek__BackingField_0() { return &___U3CvoicesU3Ek__BackingField_0; }
	inline void set_U3CvoicesU3Ek__BackingField_0(VoiceU5BU5D_t260285181* value)
	{
		___U3CvoicesU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CvoicesU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOICES_T4221445733_H
#ifndef EXCEPTION_T1927440687_H
#define EXCEPTION_T1927440687_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t1927440687  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t169632028* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t1927440687 * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t169632028* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t169632028** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t169632028* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___inner_exception_1)); }
	inline Exception_t1927440687 * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t1927440687 ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t1927440687 * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T1927440687_H
#ifndef TRADEOFFANALYTICS_T100002595_H
#define TRADEOFFANALYTICS_T100002595_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics
struct  TradeoffAnalytics_t100002595  : public RuntimeObject
{
public:

public:
};

struct TradeoffAnalytics_t100002595_StaticFields
{
public:
	// FullSerializer.fsSerializer IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics::sm_Serializer
	fsSerializer_t4193731081 * ___sm_Serializer_1;

public:
	inline static int32_t get_offset_of_sm_Serializer_1() { return static_cast<int32_t>(offsetof(TradeoffAnalytics_t100002595_StaticFields, ___sm_Serializer_1)); }
	inline fsSerializer_t4193731081 * get_sm_Serializer_1() const { return ___sm_Serializer_1; }
	inline fsSerializer_t4193731081 ** get_address_of_sm_Serializer_1() { return &___sm_Serializer_1; }
	inline void set_sm_Serializer_1(fsSerializer_t4193731081 * value)
	{
		___sm_Serializer_1 = value;
		Il2CppCodeGenWriteBarrier((&___sm_Serializer_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRADEOFFANALYTICS_T100002595_H
#ifndef DICTIONARY_2_T3943999495_H
#define DICTIONARY_2_T3943999495_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct  Dictionary_2_t3943999495  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t3030399641* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t62501539* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	StringU5BU5D_t1642385972* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	StringU5BU5D_t1642385972* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t228987430 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t3943999495, ___table_4)); }
	inline Int32U5BU5D_t3030399641* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t3030399641** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t3030399641* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t3943999495, ___linkSlots_5)); }
	inline LinkU5BU5D_t62501539* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t62501539** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t62501539* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t3943999495, ___keySlots_6)); }
	inline StringU5BU5D_t1642385972* get_keySlots_6() const { return ___keySlots_6; }
	inline StringU5BU5D_t1642385972** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(StringU5BU5D_t1642385972* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t3943999495, ___valueSlots_7)); }
	inline StringU5BU5D_t1642385972* get_valueSlots_7() const { return ___valueSlots_7; }
	inline StringU5BU5D_t1642385972** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(StringU5BU5D_t1642385972* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t3943999495, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t3943999495, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t3943999495, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t3943999495, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t3943999495, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t3943999495, ___serialization_info_13)); }
	inline SerializationInfo_t228987430 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t228987430 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t228987430 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t3943999495, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t3943999495_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t766225616 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t3943999495_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t766225616 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t766225616 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t766225616 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T3943999495_H
#ifndef CHECKSERVICESTATUS_T584740610_H
#define CHECKSERVICESTATUS_T584740610_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics/CheckServiceStatus
struct  CheckServiceStatus_t584740610  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics/CheckServiceStatus::m_Service
	TradeoffAnalytics_t100002595 * ___m_Service_0;
	// IBM.Watson.DeveloperCloud.Services.ServiceStatus IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics/CheckServiceStatus::m_Callback
	ServiceStatus_t1443707987 * ___m_Callback_1;

public:
	inline static int32_t get_offset_of_m_Service_0() { return static_cast<int32_t>(offsetof(CheckServiceStatus_t584740610, ___m_Service_0)); }
	inline TradeoffAnalytics_t100002595 * get_m_Service_0() const { return ___m_Service_0; }
	inline TradeoffAnalytics_t100002595 ** get_address_of_m_Service_0() { return &___m_Service_0; }
	inline void set_m_Service_0(TradeoffAnalytics_t100002595 * value)
	{
		___m_Service_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Service_0), value);
	}

	inline static int32_t get_offset_of_m_Callback_1() { return static_cast<int32_t>(offsetof(CheckServiceStatus_t584740610, ___m_Callback_1)); }
	inline ServiceStatus_t1443707987 * get_m_Callback_1() const { return ___m_Callback_1; }
	inline ServiceStatus_t1443707987 ** get_address_of_m_Callback_1() { return &___m_Callback_1; }
	inline void set_m_Callback_1(ServiceStatus_t1443707987 * value)
	{
		___m_Callback_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Callback_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHECKSERVICESTATUS_T584740610_H
#ifndef LIST_1_T1981607956_H
#define LIST_1_T1981607956_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column>
struct  List_1_t1981607956  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ColumnU5BU5D_t4200576441* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t1981607956, ____items_1)); }
	inline ColumnU5BU5D_t4200576441* get__items_1() const { return ____items_1; }
	inline ColumnU5BU5D_t4200576441** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ColumnU5BU5D_t4200576441* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t1981607956, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t1981607956, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t1981607956_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	ColumnU5BU5D_t4200576441* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t1981607956_StaticFields, ___EmptyArray_4)); }
	inline ColumnU5BU5D_t4200576441* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline ColumnU5BU5D_t4200576441** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(ColumnU5BU5D_t4200576441* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T1981607956_H
#ifndef VOICE_T3646862260_H
#define VOICE_T3646862260_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Voice
struct  Voice_t3646862260  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Voice::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Voice::<language>k__BackingField
	String_t* ___U3ClanguageU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Voice::<gender>k__BackingField
	String_t* ___U3CgenderU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Voice::<url>k__BackingField
	String_t* ___U3CurlU3Ek__BackingField_3;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Voice::<description>k__BackingField
	String_t* ___U3CdescriptionU3Ek__BackingField_4;
	// System.Boolean IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Voice::<customizable>k__BackingField
	bool ___U3CcustomizableU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Voice_t3646862260, ___U3CnameU3Ek__BackingField_0)); }
	inline String_t* get_U3CnameU3Ek__BackingField_0() const { return ___U3CnameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_0() { return &___U3CnameU3Ek__BackingField_0; }
	inline void set_U3CnameU3Ek__BackingField_0(String_t* value)
	{
		___U3CnameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3ClanguageU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Voice_t3646862260, ___U3ClanguageU3Ek__BackingField_1)); }
	inline String_t* get_U3ClanguageU3Ek__BackingField_1() const { return ___U3ClanguageU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3ClanguageU3Ek__BackingField_1() { return &___U3ClanguageU3Ek__BackingField_1; }
	inline void set_U3ClanguageU3Ek__BackingField_1(String_t* value)
	{
		___U3ClanguageU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClanguageU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CgenderU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Voice_t3646862260, ___U3CgenderU3Ek__BackingField_2)); }
	inline String_t* get_U3CgenderU3Ek__BackingField_2() const { return ___U3CgenderU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CgenderU3Ek__BackingField_2() { return &___U3CgenderU3Ek__BackingField_2; }
	inline void set_U3CgenderU3Ek__BackingField_2(String_t* value)
	{
		___U3CgenderU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CgenderU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CurlU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Voice_t3646862260, ___U3CurlU3Ek__BackingField_3)); }
	inline String_t* get_U3CurlU3Ek__BackingField_3() const { return ___U3CurlU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CurlU3Ek__BackingField_3() { return &___U3CurlU3Ek__BackingField_3; }
	inline void set_U3CurlU3Ek__BackingField_3(String_t* value)
	{
		___U3CurlU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CurlU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CdescriptionU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Voice_t3646862260, ___U3CdescriptionU3Ek__BackingField_4)); }
	inline String_t* get_U3CdescriptionU3Ek__BackingField_4() const { return ___U3CdescriptionU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CdescriptionU3Ek__BackingField_4() { return &___U3CdescriptionU3Ek__BackingField_4; }
	inline void set_U3CdescriptionU3Ek__BackingField_4(String_t* value)
	{
		___U3CdescriptionU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdescriptionU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CcustomizableU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Voice_t3646862260, ___U3CcustomizableU3Ek__BackingField_5)); }
	inline bool get_U3CcustomizableU3Ek__BackingField_5() const { return ___U3CcustomizableU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CcustomizableU3Ek__BackingField_5() { return &___U3CcustomizableU3Ek__BackingField_5; }
	inline void set_U3CcustomizableU3Ek__BackingField_5(bool value)
	{
		___U3CcustomizableU3Ek__BackingField_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOICE_T3646862260_H
#ifndef PARAMS_T3159428742_H
#define PARAMS_T3159428742_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Params
struct  Params_t3159428742  : public RuntimeObject
{
public:
	// System.Double IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Params::<alpha_init>k__BackingField
	double ___U3Calpha_initU3Ek__BackingField_0;
	// System.Double IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Params::<anchor_epoch>k__BackingField
	double ___U3Canchor_epochU3Ek__BackingField_1;
	// System.Double IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Params::<map_size>k__BackingField
	double ___U3Cmap_sizeU3Ek__BackingField_2;
	// System.Double IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Params::<rAnchor>k__BackingField
	double ___U3CrAnchorU3Ek__BackingField_3;
	// System.Double IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Params::<rFinish>k__BackingField
	double ___U3CrFinishU3Ek__BackingField_4;
	// System.Double IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Params::<rInit>k__BackingField
	double ___U3CrInitU3Ek__BackingField_5;
	// System.Double IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Params::<seed>k__BackingField
	double ___U3CseedU3Ek__BackingField_6;
	// System.Double IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Params::<training_period>k__BackingField
	double ___U3Ctraining_periodU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_U3Calpha_initU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Params_t3159428742, ___U3Calpha_initU3Ek__BackingField_0)); }
	inline double get_U3Calpha_initU3Ek__BackingField_0() const { return ___U3Calpha_initU3Ek__BackingField_0; }
	inline double* get_address_of_U3Calpha_initU3Ek__BackingField_0() { return &___U3Calpha_initU3Ek__BackingField_0; }
	inline void set_U3Calpha_initU3Ek__BackingField_0(double value)
	{
		___U3Calpha_initU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3Canchor_epochU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Params_t3159428742, ___U3Canchor_epochU3Ek__BackingField_1)); }
	inline double get_U3Canchor_epochU3Ek__BackingField_1() const { return ___U3Canchor_epochU3Ek__BackingField_1; }
	inline double* get_address_of_U3Canchor_epochU3Ek__BackingField_1() { return &___U3Canchor_epochU3Ek__BackingField_1; }
	inline void set_U3Canchor_epochU3Ek__BackingField_1(double value)
	{
		___U3Canchor_epochU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3Cmap_sizeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Params_t3159428742, ___U3Cmap_sizeU3Ek__BackingField_2)); }
	inline double get_U3Cmap_sizeU3Ek__BackingField_2() const { return ___U3Cmap_sizeU3Ek__BackingField_2; }
	inline double* get_address_of_U3Cmap_sizeU3Ek__BackingField_2() { return &___U3Cmap_sizeU3Ek__BackingField_2; }
	inline void set_U3Cmap_sizeU3Ek__BackingField_2(double value)
	{
		___U3Cmap_sizeU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CrAnchorU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Params_t3159428742, ___U3CrAnchorU3Ek__BackingField_3)); }
	inline double get_U3CrAnchorU3Ek__BackingField_3() const { return ___U3CrAnchorU3Ek__BackingField_3; }
	inline double* get_address_of_U3CrAnchorU3Ek__BackingField_3() { return &___U3CrAnchorU3Ek__BackingField_3; }
	inline void set_U3CrAnchorU3Ek__BackingField_3(double value)
	{
		___U3CrAnchorU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CrFinishU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Params_t3159428742, ___U3CrFinishU3Ek__BackingField_4)); }
	inline double get_U3CrFinishU3Ek__BackingField_4() const { return ___U3CrFinishU3Ek__BackingField_4; }
	inline double* get_address_of_U3CrFinishU3Ek__BackingField_4() { return &___U3CrFinishU3Ek__BackingField_4; }
	inline void set_U3CrFinishU3Ek__BackingField_4(double value)
	{
		___U3CrFinishU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CrInitU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Params_t3159428742, ___U3CrInitU3Ek__BackingField_5)); }
	inline double get_U3CrInitU3Ek__BackingField_5() const { return ___U3CrInitU3Ek__BackingField_5; }
	inline double* get_address_of_U3CrInitU3Ek__BackingField_5() { return &___U3CrInitU3Ek__BackingField_5; }
	inline void set_U3CrInitU3Ek__BackingField_5(double value)
	{
		___U3CrInitU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CseedU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Params_t3159428742, ___U3CseedU3Ek__BackingField_6)); }
	inline double get_U3CseedU3Ek__BackingField_6() const { return ___U3CseedU3Ek__BackingField_6; }
	inline double* get_address_of_U3CseedU3Ek__BackingField_6() { return &___U3CseedU3Ek__BackingField_6; }
	inline void set_U3CseedU3Ek__BackingField_6(double value)
	{
		___U3CseedU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3Ctraining_periodU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(Params_t3159428742, ___U3Ctraining_periodU3Ek__BackingField_7)); }
	inline double get_U3Ctraining_periodU3Ek__BackingField_7() const { return ___U3Ctraining_periodU3Ek__BackingField_7; }
	inline double* get_address_of_U3Ctraining_periodU3Ek__BackingField_7() { return &___U3Ctraining_periodU3Ek__BackingField_7; }
	inline void set_U3Ctraining_periodU3Ek__BackingField_7(double value)
	{
		___U3Ctraining_periodU3Ek__BackingField_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMS_T3159428742_H
#ifndef TONE_T1138009090_H
#define TONE_T1138009090_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.Tone
struct  Tone_t1138009090  : public RuntimeObject
{
public:
	// System.Double IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.Tone::<score>k__BackingField
	double ___U3CscoreU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.Tone::<tone_id>k__BackingField
	String_t* ___U3Ctone_idU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.Tone::<tone_name>k__BackingField
	String_t* ___U3Ctone_nameU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CscoreU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Tone_t1138009090, ___U3CscoreU3Ek__BackingField_0)); }
	inline double get_U3CscoreU3Ek__BackingField_0() const { return ___U3CscoreU3Ek__BackingField_0; }
	inline double* get_address_of_U3CscoreU3Ek__BackingField_0() { return &___U3CscoreU3Ek__BackingField_0; }
	inline void set_U3CscoreU3Ek__BackingField_0(double value)
	{
		___U3CscoreU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3Ctone_idU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Tone_t1138009090, ___U3Ctone_idU3Ek__BackingField_1)); }
	inline String_t* get_U3Ctone_idU3Ek__BackingField_1() const { return ___U3Ctone_idU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3Ctone_idU3Ek__BackingField_1() { return &___U3Ctone_idU3Ek__BackingField_1; }
	inline void set_U3Ctone_idU3Ek__BackingField_1(String_t* value)
	{
		___U3Ctone_idU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3Ctone_idU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3Ctone_nameU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Tone_t1138009090, ___U3Ctone_nameU3Ek__BackingField_2)); }
	inline String_t* get_U3Ctone_nameU3Ek__BackingField_2() const { return ___U3Ctone_nameU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3Ctone_nameU3Ek__BackingField_2() { return &___U3Ctone_nameU3Ek__BackingField_2; }
	inline void set_U3Ctone_nameU3Ek__BackingField_2(String_t* value)
	{
		___U3Ctone_nameU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3Ctone_nameU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TONE_T1138009090_H
#ifndef SENTENCETONE_T3234952115_H
#define SENTENCETONE_T3234952115_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.SentenceTone
struct  SentenceTone_t3234952115  : public RuntimeObject
{
public:
	// System.Int32 IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.SentenceTone::<sentence_id>k__BackingField
	int32_t ___U3Csentence_idU3Ek__BackingField_0;
	// System.Int32 IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.SentenceTone::<input_from>k__BackingField
	int32_t ___U3Cinput_fromU3Ek__BackingField_1;
	// System.Int32 IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.SentenceTone::<input_to>k__BackingField
	int32_t ___U3Cinput_toU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.SentenceTone::<text>k__BackingField
	String_t* ___U3CtextU3Ek__BackingField_3;
	// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneCategory[] IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.SentenceTone::<tone_categories>k__BackingField
	ToneCategoryU5BU5D_t3565596459* ___U3Ctone_categoriesU3Ek__BackingField_4;
	// System.Double IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.SentenceTone::m_HighestScore
	double ___m_HighestScore_5;
	// System.String IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.SentenceTone::m_HighestScoreToneName
	String_t* ___m_HighestScoreToneName_6;
	// System.String IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.SentenceTone::m_HighestScoreToneCategoryName
	String_t* ___m_HighestScoreToneCategoryName_7;

public:
	inline static int32_t get_offset_of_U3Csentence_idU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SentenceTone_t3234952115, ___U3Csentence_idU3Ek__BackingField_0)); }
	inline int32_t get_U3Csentence_idU3Ek__BackingField_0() const { return ___U3Csentence_idU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3Csentence_idU3Ek__BackingField_0() { return &___U3Csentence_idU3Ek__BackingField_0; }
	inline void set_U3Csentence_idU3Ek__BackingField_0(int32_t value)
	{
		___U3Csentence_idU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3Cinput_fromU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(SentenceTone_t3234952115, ___U3Cinput_fromU3Ek__BackingField_1)); }
	inline int32_t get_U3Cinput_fromU3Ek__BackingField_1() const { return ___U3Cinput_fromU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3Cinput_fromU3Ek__BackingField_1() { return &___U3Cinput_fromU3Ek__BackingField_1; }
	inline void set_U3Cinput_fromU3Ek__BackingField_1(int32_t value)
	{
		___U3Cinput_fromU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3Cinput_toU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SentenceTone_t3234952115, ___U3Cinput_toU3Ek__BackingField_2)); }
	inline int32_t get_U3Cinput_toU3Ek__BackingField_2() const { return ___U3Cinput_toU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3Cinput_toU3Ek__BackingField_2() { return &___U3Cinput_toU3Ek__BackingField_2; }
	inline void set_U3Cinput_toU3Ek__BackingField_2(int32_t value)
	{
		___U3Cinput_toU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CtextU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SentenceTone_t3234952115, ___U3CtextU3Ek__BackingField_3)); }
	inline String_t* get_U3CtextU3Ek__BackingField_3() const { return ___U3CtextU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CtextU3Ek__BackingField_3() { return &___U3CtextU3Ek__BackingField_3; }
	inline void set_U3CtextU3Ek__BackingField_3(String_t* value)
	{
		___U3CtextU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3Ctone_categoriesU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(SentenceTone_t3234952115, ___U3Ctone_categoriesU3Ek__BackingField_4)); }
	inline ToneCategoryU5BU5D_t3565596459* get_U3Ctone_categoriesU3Ek__BackingField_4() const { return ___U3Ctone_categoriesU3Ek__BackingField_4; }
	inline ToneCategoryU5BU5D_t3565596459** get_address_of_U3Ctone_categoriesU3Ek__BackingField_4() { return &___U3Ctone_categoriesU3Ek__BackingField_4; }
	inline void set_U3Ctone_categoriesU3Ek__BackingField_4(ToneCategoryU5BU5D_t3565596459* value)
	{
		___U3Ctone_categoriesU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3Ctone_categoriesU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_m_HighestScore_5() { return static_cast<int32_t>(offsetof(SentenceTone_t3234952115, ___m_HighestScore_5)); }
	inline double get_m_HighestScore_5() const { return ___m_HighestScore_5; }
	inline double* get_address_of_m_HighestScore_5() { return &___m_HighestScore_5; }
	inline void set_m_HighestScore_5(double value)
	{
		___m_HighestScore_5 = value;
	}

	inline static int32_t get_offset_of_m_HighestScoreToneName_6() { return static_cast<int32_t>(offsetof(SentenceTone_t3234952115, ___m_HighestScoreToneName_6)); }
	inline String_t* get_m_HighestScoreToneName_6() const { return ___m_HighestScoreToneName_6; }
	inline String_t** get_address_of_m_HighestScoreToneName_6() { return &___m_HighestScoreToneName_6; }
	inline void set_m_HighestScoreToneName_6(String_t* value)
	{
		___m_HighestScoreToneName_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_HighestScoreToneName_6), value);
	}

	inline static int32_t get_offset_of_m_HighestScoreToneCategoryName_7() { return static_cast<int32_t>(offsetof(SentenceTone_t3234952115, ___m_HighestScoreToneCategoryName_7)); }
	inline String_t* get_m_HighestScoreToneCategoryName_7() const { return ___m_HighestScoreToneCategoryName_7; }
	inline String_t** get_address_of_m_HighestScoreToneCategoryName_7() { return &___m_HighestScoreToneCategoryName_7; }
	inline void set_m_HighestScoreToneCategoryName_7(String_t* value)
	{
		___m_HighestScoreToneCategoryName_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_HighestScoreToneCategoryName_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SENTENCETONE_T3234952115_H
#ifndef TONEANALYZER_T1356110496_H
#define TONEANALYZER_T1356110496_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzer
struct  ToneAnalyzer_t1356110496  : public RuntimeObject
{
public:

public:
};

struct ToneAnalyzer_t1356110496_StaticFields
{
public:
	// FullSerializer.fsSerializer IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzer::sm_Serializer
	fsSerializer_t4193731081 * ___sm_Serializer_1;

public:
	inline static int32_t get_offset_of_sm_Serializer_1() { return static_cast<int32_t>(offsetof(ToneAnalyzer_t1356110496_StaticFields, ___sm_Serializer_1)); }
	inline fsSerializer_t4193731081 * get_sm_Serializer_1() const { return ___sm_Serializer_1; }
	inline fsSerializer_t4193731081 ** get_address_of_sm_Serializer_1() { return &___sm_Serializer_1; }
	inline void set_sm_Serializer_1(fsSerializer_t4193731081 * value)
	{
		___sm_Serializer_1 = value;
		Il2CppCodeGenWriteBarrier((&___sm_Serializer_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TONEANALYZER_T1356110496_H
#ifndef TONEANALYZERRESPONSE_T3391014235_H
#define TONEANALYZERRESPONSE_T3391014235_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzerResponse
struct  ToneAnalyzerResponse_t3391014235  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.DocumentTone IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzerResponse::<document_tone>k__BackingField
	DocumentTone_t2537937785 * ___U3Cdocument_toneU3Ek__BackingField_0;
	// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.SentenceTone[] IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzerResponse::<sentences_tone>k__BackingField
	SentenceToneU5BU5D_t255016994* ___U3Csentences_toneU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3Cdocument_toneU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ToneAnalyzerResponse_t3391014235, ___U3Cdocument_toneU3Ek__BackingField_0)); }
	inline DocumentTone_t2537937785 * get_U3Cdocument_toneU3Ek__BackingField_0() const { return ___U3Cdocument_toneU3Ek__BackingField_0; }
	inline DocumentTone_t2537937785 ** get_address_of_U3Cdocument_toneU3Ek__BackingField_0() { return &___U3Cdocument_toneU3Ek__BackingField_0; }
	inline void set_U3Cdocument_toneU3Ek__BackingField_0(DocumentTone_t2537937785 * value)
	{
		___U3Cdocument_toneU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cdocument_toneU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3Csentences_toneU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ToneAnalyzerResponse_t3391014235, ___U3Csentences_toneU3Ek__BackingField_1)); }
	inline SentenceToneU5BU5D_t255016994* get_U3Csentences_toneU3Ek__BackingField_1() const { return ___U3Csentences_toneU3Ek__BackingField_1; }
	inline SentenceToneU5BU5D_t255016994** get_address_of_U3Csentences_toneU3Ek__BackingField_1() { return &___U3Csentences_toneU3Ek__BackingField_1; }
	inline void set_U3Csentences_toneU3Ek__BackingField_1(SentenceToneU5BU5D_t255016994* value)
	{
		___U3Csentences_toneU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3Csentences_toneU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TONEANALYZERRESPONSE_T3391014235_H
#ifndef TRANSLATION_T2174867539_H
#define TRANSLATION_T2174867539_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Translation
struct  Translation_t2174867539  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Translation::<translation>k__BackingField
	String_t* ___U3CtranslationU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CtranslationU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Translation_t2174867539, ___U3CtranslationU3Ek__BackingField_0)); }
	inline String_t* get_U3CtranslationU3Ek__BackingField_0() const { return ___U3CtranslationU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CtranslationU3Ek__BackingField_0() { return &___U3CtranslationU3Ek__BackingField_0; }
	inline void set_U3CtranslationU3Ek__BackingField_0(String_t* value)
	{
		___U3CtranslationU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtranslationU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSLATION_T2174867539_H
#ifndef TONECATEGORY_T3724297374_H
#define TONECATEGORY_T3724297374_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneCategory
struct  ToneCategory_t3724297374  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneCategory::<category_id>k__BackingField
	String_t* ___U3Ccategory_idU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneCategory::<category_name>k__BackingField
	String_t* ___U3Ccategory_nameU3Ek__BackingField_1;
	// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.Tone[] IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneCategory::<tones>k__BackingField
	ToneU5BU5D_t4171600311* ___U3CtonesU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3Ccategory_idU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ToneCategory_t3724297374, ___U3Ccategory_idU3Ek__BackingField_0)); }
	inline String_t* get_U3Ccategory_idU3Ek__BackingField_0() const { return ___U3Ccategory_idU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3Ccategory_idU3Ek__BackingField_0() { return &___U3Ccategory_idU3Ek__BackingField_0; }
	inline void set_U3Ccategory_idU3Ek__BackingField_0(String_t* value)
	{
		___U3Ccategory_idU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Ccategory_idU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3Ccategory_nameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ToneCategory_t3724297374, ___U3Ccategory_nameU3Ek__BackingField_1)); }
	inline String_t* get_U3Ccategory_nameU3Ek__BackingField_1() const { return ___U3Ccategory_nameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3Ccategory_nameU3Ek__BackingField_1() { return &___U3Ccategory_nameU3Ek__BackingField_1; }
	inline void set_U3Ccategory_nameU3Ek__BackingField_1(String_t* value)
	{
		___U3Ccategory_nameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3Ccategory_nameU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CtonesU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ToneCategory_t3724297374, ___U3CtonesU3Ek__BackingField_2)); }
	inline ToneU5BU5D_t4171600311* get_U3CtonesU3Ek__BackingField_2() const { return ___U3CtonesU3Ek__BackingField_2; }
	inline ToneU5BU5D_t4171600311** get_address_of_U3CtonesU3Ek__BackingField_2() { return &___U3CtonesU3Ek__BackingField_2; }
	inline void set_U3CtonesU3Ek__BackingField_2(ToneU5BU5D_t4171600311* value)
	{
		___U3CtonesU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtonesU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TONECATEGORY_T3724297374_H
#ifndef DOCUMENTTONE_T2537937785_H
#define DOCUMENTTONE_T2537937785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.DocumentTone
struct  DocumentTone_t2537937785  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneCategory[] IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.DocumentTone::<tone_categories>k__BackingField
	ToneCategoryU5BU5D_t3565596459* ___U3Ctone_categoriesU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3Ctone_categoriesU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DocumentTone_t2537937785, ___U3Ctone_categoriesU3Ek__BackingField_0)); }
	inline ToneCategoryU5BU5D_t3565596459* get_U3Ctone_categoriesU3Ek__BackingField_0() const { return ___U3Ctone_categoriesU3Ek__BackingField_0; }
	inline ToneCategoryU5BU5D_t3565596459** get_address_of_U3Ctone_categoriesU3Ek__BackingField_0() { return &___U3Ctone_categoriesU3Ek__BackingField_0; }
	inline void set_U3Ctone_categoriesU3Ek__BackingField_0(ToneCategoryU5BU5D_t3565596459* value)
	{
		___U3Ctone_categoriesU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Ctone_categoriesU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOCUMENTTONE_T2537937785_H
#ifndef CHECKSERVICESTATUS_T3651453794_H
#define CHECKSERVICESTATUS_T3651453794_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/CheckServiceStatus
struct  CheckServiceStatus_t3651453794  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/CheckServiceStatus::m_Service
	TextToSpeech_t3349357562 * ___m_Service_0;
	// IBM.Watson.DeveloperCloud.Services.ServiceStatus IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/CheckServiceStatus::m_Callback
	ServiceStatus_t1443707987 * ___m_Callback_1;

public:
	inline static int32_t get_offset_of_m_Service_0() { return static_cast<int32_t>(offsetof(CheckServiceStatus_t3651453794, ___m_Service_0)); }
	inline TextToSpeech_t3349357562 * get_m_Service_0() const { return ___m_Service_0; }
	inline TextToSpeech_t3349357562 ** get_address_of_m_Service_0() { return &___m_Service_0; }
	inline void set_m_Service_0(TextToSpeech_t3349357562 * value)
	{
		___m_Service_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Service_0), value);
	}

	inline static int32_t get_offset_of_m_Callback_1() { return static_cast<int32_t>(offsetof(CheckServiceStatus_t3651453794, ___m_Callback_1)); }
	inline ServiceStatus_t1443707987 * get_m_Callback_1() const { return ___m_Callback_1; }
	inline ServiceStatus_t1443707987 ** get_address_of_m_Callback_1() { return &___m_Callback_1; }
	inline void set_m_Callback_1(ServiceStatus_t1443707987 * value)
	{
		___m_Callback_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Callback_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHECKSERVICESTATUS_T3651453794_H
#ifndef CONFIG_T3637807320_H
#define CONFIG_T3637807320_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Utilities.Config
struct  Config_t3637807320  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Utilities.Config::m_ClassifierDirectory
	String_t* ___m_ClassifierDirectory_0;
	// System.Single IBM.Watson.DeveloperCloud.Utilities.Config::m_TimeOut
	float ___m_TimeOut_1;
	// System.Int32 IBM.Watson.DeveloperCloud.Utilities.Config::m_MaxRestConnections
	int32_t ___m_MaxRestConnections_2;
	// System.Collections.Generic.List`1<IBM.Watson.DeveloperCloud.Utilities.Config/CredentialInfo> IBM.Watson.DeveloperCloud.Utilities.Config::m_Credentials
	List_1_t3698242869 * ___m_Credentials_3;
	// System.Collections.Generic.List`1<IBM.Watson.DeveloperCloud.Utilities.Config/Variable> IBM.Watson.DeveloperCloud.Utilities.Config::m_Variables
	List_1_t4101323382 * ___m_Variables_4;
	// System.Boolean IBM.Watson.DeveloperCloud.Utilities.Config::<ConfigLoaded>k__BackingField
	bool ___U3CConfigLoadedU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_m_ClassifierDirectory_0() { return static_cast<int32_t>(offsetof(Config_t3637807320, ___m_ClassifierDirectory_0)); }
	inline String_t* get_m_ClassifierDirectory_0() const { return ___m_ClassifierDirectory_0; }
	inline String_t** get_address_of_m_ClassifierDirectory_0() { return &___m_ClassifierDirectory_0; }
	inline void set_m_ClassifierDirectory_0(String_t* value)
	{
		___m_ClassifierDirectory_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ClassifierDirectory_0), value);
	}

	inline static int32_t get_offset_of_m_TimeOut_1() { return static_cast<int32_t>(offsetof(Config_t3637807320, ___m_TimeOut_1)); }
	inline float get_m_TimeOut_1() const { return ___m_TimeOut_1; }
	inline float* get_address_of_m_TimeOut_1() { return &___m_TimeOut_1; }
	inline void set_m_TimeOut_1(float value)
	{
		___m_TimeOut_1 = value;
	}

	inline static int32_t get_offset_of_m_MaxRestConnections_2() { return static_cast<int32_t>(offsetof(Config_t3637807320, ___m_MaxRestConnections_2)); }
	inline int32_t get_m_MaxRestConnections_2() const { return ___m_MaxRestConnections_2; }
	inline int32_t* get_address_of_m_MaxRestConnections_2() { return &___m_MaxRestConnections_2; }
	inline void set_m_MaxRestConnections_2(int32_t value)
	{
		___m_MaxRestConnections_2 = value;
	}

	inline static int32_t get_offset_of_m_Credentials_3() { return static_cast<int32_t>(offsetof(Config_t3637807320, ___m_Credentials_3)); }
	inline List_1_t3698242869 * get_m_Credentials_3() const { return ___m_Credentials_3; }
	inline List_1_t3698242869 ** get_address_of_m_Credentials_3() { return &___m_Credentials_3; }
	inline void set_m_Credentials_3(List_1_t3698242869 * value)
	{
		___m_Credentials_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Credentials_3), value);
	}

	inline static int32_t get_offset_of_m_Variables_4() { return static_cast<int32_t>(offsetof(Config_t3637807320, ___m_Variables_4)); }
	inline List_1_t4101323382 * get_m_Variables_4() const { return ___m_Variables_4; }
	inline List_1_t4101323382 ** get_address_of_m_Variables_4() { return &___m_Variables_4; }
	inline void set_m_Variables_4(List_1_t4101323382 * value)
	{
		___m_Variables_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Variables_4), value);
	}

	inline static int32_t get_offset_of_U3CConfigLoadedU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Config_t3637807320, ___U3CConfigLoadedU3Ek__BackingField_6)); }
	inline bool get_U3CConfigLoadedU3Ek__BackingField_6() const { return ___U3CConfigLoadedU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CConfigLoadedU3Ek__BackingField_6() { return &___U3CConfigLoadedU3Ek__BackingField_6; }
	inline void set_U3CConfigLoadedU3Ek__BackingField_6(bool value)
	{
		___U3CConfigLoadedU3Ek__BackingField_6 = value;
	}
};

struct Config_t3637807320_StaticFields
{
public:
	// FullSerializer.fsSerializer IBM.Watson.DeveloperCloud.Utilities.Config::sm_Serializer
	fsSerializer_t4193731081 * ___sm_Serializer_5;

public:
	inline static int32_t get_offset_of_sm_Serializer_5() { return static_cast<int32_t>(offsetof(Config_t3637807320_StaticFields, ___sm_Serializer_5)); }
	inline fsSerializer_t4193731081 * get_sm_Serializer_5() const { return ___sm_Serializer_5; }
	inline fsSerializer_t4193731081 ** get_address_of_sm_Serializer_5() { return &___sm_Serializer_5; }
	inline void set_sm_Serializer_5(fsSerializer_t4193731081 * value)
	{
		___sm_Serializer_5 = value;
		Il2CppCodeGenWriteBarrier((&___sm_Serializer_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIG_T3637807320_H
#ifndef CREDENTIALINFO_T34154441_H
#define CREDENTIALINFO_T34154441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Utilities.Config/CredentialInfo
struct  CredentialInfo_t34154441  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Utilities.Config/CredentialInfo::m_ServiceID
	String_t* ___m_ServiceID_0;
	// System.String IBM.Watson.DeveloperCloud.Utilities.Config/CredentialInfo::m_URL
	String_t* ___m_URL_1;
	// System.String IBM.Watson.DeveloperCloud.Utilities.Config/CredentialInfo::m_User
	String_t* ___m_User_2;
	// System.String IBM.Watson.DeveloperCloud.Utilities.Config/CredentialInfo::m_Password
	String_t* ___m_Password_3;
	// System.String IBM.Watson.DeveloperCloud.Utilities.Config/CredentialInfo::m_Apikey
	String_t* ___m_Apikey_4;
	// System.String IBM.Watson.DeveloperCloud.Utilities.Config/CredentialInfo::m_Note
	String_t* ___m_Note_5;

public:
	inline static int32_t get_offset_of_m_ServiceID_0() { return static_cast<int32_t>(offsetof(CredentialInfo_t34154441, ___m_ServiceID_0)); }
	inline String_t* get_m_ServiceID_0() const { return ___m_ServiceID_0; }
	inline String_t** get_address_of_m_ServiceID_0() { return &___m_ServiceID_0; }
	inline void set_m_ServiceID_0(String_t* value)
	{
		___m_ServiceID_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ServiceID_0), value);
	}

	inline static int32_t get_offset_of_m_URL_1() { return static_cast<int32_t>(offsetof(CredentialInfo_t34154441, ___m_URL_1)); }
	inline String_t* get_m_URL_1() const { return ___m_URL_1; }
	inline String_t** get_address_of_m_URL_1() { return &___m_URL_1; }
	inline void set_m_URL_1(String_t* value)
	{
		___m_URL_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_URL_1), value);
	}

	inline static int32_t get_offset_of_m_User_2() { return static_cast<int32_t>(offsetof(CredentialInfo_t34154441, ___m_User_2)); }
	inline String_t* get_m_User_2() const { return ___m_User_2; }
	inline String_t** get_address_of_m_User_2() { return &___m_User_2; }
	inline void set_m_User_2(String_t* value)
	{
		___m_User_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_User_2), value);
	}

	inline static int32_t get_offset_of_m_Password_3() { return static_cast<int32_t>(offsetof(CredentialInfo_t34154441, ___m_Password_3)); }
	inline String_t* get_m_Password_3() const { return ___m_Password_3; }
	inline String_t** get_address_of_m_Password_3() { return &___m_Password_3; }
	inline void set_m_Password_3(String_t* value)
	{
		___m_Password_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Password_3), value);
	}

	inline static int32_t get_offset_of_m_Apikey_4() { return static_cast<int32_t>(offsetof(CredentialInfo_t34154441, ___m_Apikey_4)); }
	inline String_t* get_m_Apikey_4() const { return ___m_Apikey_4; }
	inline String_t** get_address_of_m_Apikey_4() { return &___m_Apikey_4; }
	inline void set_m_Apikey_4(String_t* value)
	{
		___m_Apikey_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Apikey_4), value);
	}

	inline static int32_t get_offset_of_m_Note_5() { return static_cast<int32_t>(offsetof(CredentialInfo_t34154441, ___m_Note_5)); }
	inline String_t* get_m_Note_5() const { return ___m_Note_5; }
	inline String_t** get_address_of_m_Note_5() { return &___m_Note_5; }
	inline void set_m_Note_5(String_t* value)
	{
		___m_Note_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Note_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREDENTIALINFO_T34154441_H
#ifndef RANGE_T2396059803_H
#define RANGE_T2396059803_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Range
struct  Range_t2396059803  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANGE_T2396059803_H
#ifndef APPLICATIONDATAVALUE_T1721671971_H
#define APPLICATIONDATAVALUE_T1721671971_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.ApplicationDataValue
struct  ApplicationDataValue_t1721671971  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPLICATIONDATAVALUE_T1721671971_H
#ifndef COLUMN_T2612486824_H
#define COLUMN_T2612486824_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column
struct  Column_t2612486824  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column::<key>k__BackingField
	String_t* ___U3CkeyU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column::<type>k__BackingField
	String_t* ___U3CtypeU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column::<description>k__BackingField
	String_t* ___U3CdescriptionU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column::<format>k__BackingField
	String_t* ___U3CformatU3Ek__BackingField_3;
	// System.String IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column::<full_name>k__BackingField
	String_t* ___U3Cfull_nameU3Ek__BackingField_4;
	// System.String IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column::<goal>k__BackingField
	String_t* ___U3CgoalU3Ek__BackingField_5;
	// System.Int32 IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column::<insignificant_loss>k__BackingField
	int32_t ___U3Cinsignificant_lossU3Ek__BackingField_6;
	// System.Boolean IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column::<is_objective>k__BackingField
	bool ___U3Cis_objectiveU3Ek__BackingField_7;
	// System.String[] IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column::<preference>k__BackingField
	StringU5BU5D_t1642385972* ___U3CpreferenceU3Ek__BackingField_8;
	// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Range IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column::<range>k__BackingField
	Range_t2396059803 * ___U3CrangeU3Ek__BackingField_9;
	// System.Int64 IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column::<significant_gain>k__BackingField
	int64_t ___U3Csignificant_gainU3Ek__BackingField_10;
	// System.Int64 IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column::<significant_loss>k__BackingField
	int64_t ___U3Csignificant_lossU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_U3CkeyU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Column_t2612486824, ___U3CkeyU3Ek__BackingField_0)); }
	inline String_t* get_U3CkeyU3Ek__BackingField_0() const { return ___U3CkeyU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CkeyU3Ek__BackingField_0() { return &___U3CkeyU3Ek__BackingField_0; }
	inline void set_U3CkeyU3Ek__BackingField_0(String_t* value)
	{
		___U3CkeyU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CkeyU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CtypeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Column_t2612486824, ___U3CtypeU3Ek__BackingField_1)); }
	inline String_t* get_U3CtypeU3Ek__BackingField_1() const { return ___U3CtypeU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CtypeU3Ek__BackingField_1() { return &___U3CtypeU3Ek__BackingField_1; }
	inline void set_U3CtypeU3Ek__BackingField_1(String_t* value)
	{
		___U3CtypeU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtypeU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CdescriptionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Column_t2612486824, ___U3CdescriptionU3Ek__BackingField_2)); }
	inline String_t* get_U3CdescriptionU3Ek__BackingField_2() const { return ___U3CdescriptionU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CdescriptionU3Ek__BackingField_2() { return &___U3CdescriptionU3Ek__BackingField_2; }
	inline void set_U3CdescriptionU3Ek__BackingField_2(String_t* value)
	{
		___U3CdescriptionU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdescriptionU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CformatU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Column_t2612486824, ___U3CformatU3Ek__BackingField_3)); }
	inline String_t* get_U3CformatU3Ek__BackingField_3() const { return ___U3CformatU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CformatU3Ek__BackingField_3() { return &___U3CformatU3Ek__BackingField_3; }
	inline void set_U3CformatU3Ek__BackingField_3(String_t* value)
	{
		___U3CformatU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CformatU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3Cfull_nameU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Column_t2612486824, ___U3Cfull_nameU3Ek__BackingField_4)); }
	inline String_t* get_U3Cfull_nameU3Ek__BackingField_4() const { return ___U3Cfull_nameU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3Cfull_nameU3Ek__BackingField_4() { return &___U3Cfull_nameU3Ek__BackingField_4; }
	inline void set_U3Cfull_nameU3Ek__BackingField_4(String_t* value)
	{
		___U3Cfull_nameU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cfull_nameU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CgoalU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Column_t2612486824, ___U3CgoalU3Ek__BackingField_5)); }
	inline String_t* get_U3CgoalU3Ek__BackingField_5() const { return ___U3CgoalU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CgoalU3Ek__BackingField_5() { return &___U3CgoalU3Ek__BackingField_5; }
	inline void set_U3CgoalU3Ek__BackingField_5(String_t* value)
	{
		___U3CgoalU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CgoalU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3Cinsignificant_lossU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Column_t2612486824, ___U3Cinsignificant_lossU3Ek__BackingField_6)); }
	inline int32_t get_U3Cinsignificant_lossU3Ek__BackingField_6() const { return ___U3Cinsignificant_lossU3Ek__BackingField_6; }
	inline int32_t* get_address_of_U3Cinsignificant_lossU3Ek__BackingField_6() { return &___U3Cinsignificant_lossU3Ek__BackingField_6; }
	inline void set_U3Cinsignificant_lossU3Ek__BackingField_6(int32_t value)
	{
		___U3Cinsignificant_lossU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3Cis_objectiveU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(Column_t2612486824, ___U3Cis_objectiveU3Ek__BackingField_7)); }
	inline bool get_U3Cis_objectiveU3Ek__BackingField_7() const { return ___U3Cis_objectiveU3Ek__BackingField_7; }
	inline bool* get_address_of_U3Cis_objectiveU3Ek__BackingField_7() { return &___U3Cis_objectiveU3Ek__BackingField_7; }
	inline void set_U3Cis_objectiveU3Ek__BackingField_7(bool value)
	{
		___U3Cis_objectiveU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CpreferenceU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(Column_t2612486824, ___U3CpreferenceU3Ek__BackingField_8)); }
	inline StringU5BU5D_t1642385972* get_U3CpreferenceU3Ek__BackingField_8() const { return ___U3CpreferenceU3Ek__BackingField_8; }
	inline StringU5BU5D_t1642385972** get_address_of_U3CpreferenceU3Ek__BackingField_8() { return &___U3CpreferenceU3Ek__BackingField_8; }
	inline void set_U3CpreferenceU3Ek__BackingField_8(StringU5BU5D_t1642385972* value)
	{
		___U3CpreferenceU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpreferenceU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CrangeU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(Column_t2612486824, ___U3CrangeU3Ek__BackingField_9)); }
	inline Range_t2396059803 * get_U3CrangeU3Ek__BackingField_9() const { return ___U3CrangeU3Ek__BackingField_9; }
	inline Range_t2396059803 ** get_address_of_U3CrangeU3Ek__BackingField_9() { return &___U3CrangeU3Ek__BackingField_9; }
	inline void set_U3CrangeU3Ek__BackingField_9(Range_t2396059803 * value)
	{
		___U3CrangeU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrangeU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3Csignificant_gainU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(Column_t2612486824, ___U3Csignificant_gainU3Ek__BackingField_10)); }
	inline int64_t get_U3Csignificant_gainU3Ek__BackingField_10() const { return ___U3Csignificant_gainU3Ek__BackingField_10; }
	inline int64_t* get_address_of_U3Csignificant_gainU3Ek__BackingField_10() { return &___U3Csignificant_gainU3Ek__BackingField_10; }
	inline void set_U3Csignificant_gainU3Ek__BackingField_10(int64_t value)
	{
		___U3Csignificant_gainU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3Csignificant_lossU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(Column_t2612486824, ___U3Csignificant_lossU3Ek__BackingField_11)); }
	inline int64_t get_U3Csignificant_lossU3Ek__BackingField_11() const { return ___U3Csignificant_lossU3Ek__BackingField_11; }
	inline int64_t* get_address_of_U3Csignificant_lossU3Ek__BackingField_11() { return &___U3Csignificant_lossU3Ek__BackingField_11; }
	inline void set_U3Csignificant_lossU3Ek__BackingField_11(int64_t value)
	{
		___U3Csignificant_lossU3Ek__BackingField_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLUMN_T2612486824_H
#ifndef DRIVERS_T1652706281_H
#define DRIVERS_T1652706281_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Drivers
struct  Drivers_t1652706281  : public RuntimeObject
{
public:
	// System.Double IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Drivers::<alpha_init>k__BackingField
	double ___U3Calpha_initU3Ek__BackingField_0;
	// System.Double IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Drivers::<data_multiplier>k__BackingField
	double ___U3Cdata_multiplierU3Ek__BackingField_1;
	// System.Double IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Drivers::<max_map_size>k__BackingField
	double ___U3Cmax_map_sizeU3Ek__BackingField_2;
	// System.Double IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Drivers::<r_anchor_init>k__BackingField
	double ___U3Cr_anchor_initU3Ek__BackingField_3;
	// System.Double IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Drivers::<r_fin>k__BackingField
	double ___U3Cr_finU3Ek__BackingField_4;
	// System.Double IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Drivers::<r_init>k__BackingField
	double ___U3Cr_initU3Ek__BackingField_5;
	// System.Double IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Drivers::<training_anchors>k__BackingField
	double ___U3Ctraining_anchorsU3Ek__BackingField_6;
	// System.Double IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Drivers::<training_length>k__BackingField
	double ___U3Ctraining_lengthU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_U3Calpha_initU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Drivers_t1652706281, ___U3Calpha_initU3Ek__BackingField_0)); }
	inline double get_U3Calpha_initU3Ek__BackingField_0() const { return ___U3Calpha_initU3Ek__BackingField_0; }
	inline double* get_address_of_U3Calpha_initU3Ek__BackingField_0() { return &___U3Calpha_initU3Ek__BackingField_0; }
	inline void set_U3Calpha_initU3Ek__BackingField_0(double value)
	{
		___U3Calpha_initU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3Cdata_multiplierU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Drivers_t1652706281, ___U3Cdata_multiplierU3Ek__BackingField_1)); }
	inline double get_U3Cdata_multiplierU3Ek__BackingField_1() const { return ___U3Cdata_multiplierU3Ek__BackingField_1; }
	inline double* get_address_of_U3Cdata_multiplierU3Ek__BackingField_1() { return &___U3Cdata_multiplierU3Ek__BackingField_1; }
	inline void set_U3Cdata_multiplierU3Ek__BackingField_1(double value)
	{
		___U3Cdata_multiplierU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3Cmax_map_sizeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Drivers_t1652706281, ___U3Cmax_map_sizeU3Ek__BackingField_2)); }
	inline double get_U3Cmax_map_sizeU3Ek__BackingField_2() const { return ___U3Cmax_map_sizeU3Ek__BackingField_2; }
	inline double* get_address_of_U3Cmax_map_sizeU3Ek__BackingField_2() { return &___U3Cmax_map_sizeU3Ek__BackingField_2; }
	inline void set_U3Cmax_map_sizeU3Ek__BackingField_2(double value)
	{
		___U3Cmax_map_sizeU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3Cr_anchor_initU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Drivers_t1652706281, ___U3Cr_anchor_initU3Ek__BackingField_3)); }
	inline double get_U3Cr_anchor_initU3Ek__BackingField_3() const { return ___U3Cr_anchor_initU3Ek__BackingField_3; }
	inline double* get_address_of_U3Cr_anchor_initU3Ek__BackingField_3() { return &___U3Cr_anchor_initU3Ek__BackingField_3; }
	inline void set_U3Cr_anchor_initU3Ek__BackingField_3(double value)
	{
		___U3Cr_anchor_initU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3Cr_finU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Drivers_t1652706281, ___U3Cr_finU3Ek__BackingField_4)); }
	inline double get_U3Cr_finU3Ek__BackingField_4() const { return ___U3Cr_finU3Ek__BackingField_4; }
	inline double* get_address_of_U3Cr_finU3Ek__BackingField_4() { return &___U3Cr_finU3Ek__BackingField_4; }
	inline void set_U3Cr_finU3Ek__BackingField_4(double value)
	{
		___U3Cr_finU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3Cr_initU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Drivers_t1652706281, ___U3Cr_initU3Ek__BackingField_5)); }
	inline double get_U3Cr_initU3Ek__BackingField_5() const { return ___U3Cr_initU3Ek__BackingField_5; }
	inline double* get_address_of_U3Cr_initU3Ek__BackingField_5() { return &___U3Cr_initU3Ek__BackingField_5; }
	inline void set_U3Cr_initU3Ek__BackingField_5(double value)
	{
		___U3Cr_initU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3Ctraining_anchorsU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Drivers_t1652706281, ___U3Ctraining_anchorsU3Ek__BackingField_6)); }
	inline double get_U3Ctraining_anchorsU3Ek__BackingField_6() const { return ___U3Ctraining_anchorsU3Ek__BackingField_6; }
	inline double* get_address_of_U3Ctraining_anchorsU3Ek__BackingField_6() { return &___U3Ctraining_anchorsU3Ek__BackingField_6; }
	inline void set_U3Ctraining_anchorsU3Ek__BackingField_6(double value)
	{
		___U3Ctraining_anchorsU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3Ctraining_lengthU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(Drivers_t1652706281, ___U3Ctraining_lengthU3Ek__BackingField_7)); }
	inline double get_U3Ctraining_lengthU3Ek__BackingField_7() const { return ___U3Ctraining_lengthU3Ek__BackingField_7; }
	inline double* get_address_of_U3Ctraining_lengthU3Ek__BackingField_7() { return &___U3Ctraining_lengthU3Ek__BackingField_7; }
	inline void set_U3Ctraining_lengthU3Ek__BackingField_7(double value)
	{
		___U3Ctraining_lengthU3Ek__BackingField_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRIVERS_T1652706281_H
#ifndef CONFIG_T1927135816_H
#define CONFIG_T1927135816_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Config
struct  Config_t1927135816  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Drivers IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Config::<drivers>k__BackingField
	Drivers_t1652706281 * ___U3CdriversU3Ek__BackingField_0;
	// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Params IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Config::<params>k__BackingField
	Params_t3159428742 * ___U3CparamsU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CdriversU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Config_t1927135816, ___U3CdriversU3Ek__BackingField_0)); }
	inline Drivers_t1652706281 * get_U3CdriversU3Ek__BackingField_0() const { return ___U3CdriversU3Ek__BackingField_0; }
	inline Drivers_t1652706281 ** get_address_of_U3CdriversU3Ek__BackingField_0() { return &___U3CdriversU3Ek__BackingField_0; }
	inline void set_U3CdriversU3Ek__BackingField_0(Drivers_t1652706281 * value)
	{
		___U3CdriversU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdriversU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CparamsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Config_t1927135816, ___U3CparamsU3Ek__BackingField_1)); }
	inline Params_t3159428742 * get_U3CparamsU3Ek__BackingField_1() const { return ___U3CparamsU3Ek__BackingField_1; }
	inline Params_t3159428742 ** get_address_of_U3CparamsU3Ek__BackingField_1() { return &___U3CparamsU3Ek__BackingField_1; }
	inline void set_U3CparamsU3Ek__BackingField_1(Params_t3159428742 * value)
	{
		___U3CparamsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CparamsU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIG_T1927135816_H
#ifndef ANCHOR_T1470404909_H
#define ANCHOR_T1470404909_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Anchor
struct  Anchor_t1470404909  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Anchor::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_0;
	// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Position IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Anchor::<position>k__BackingField
	Position_t3597491459 * ___U3CpositionU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Anchor_t1470404909, ___U3CnameU3Ek__BackingField_0)); }
	inline String_t* get_U3CnameU3Ek__BackingField_0() const { return ___U3CnameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_0() { return &___U3CnameU3Ek__BackingField_0; }
	inline void set_U3CnameU3Ek__BackingField_0(String_t* value)
	{
		___U3CnameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CpositionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Anchor_t1470404909, ___U3CpositionU3Ek__BackingField_1)); }
	inline Position_t3597491459 * get_U3CpositionU3Ek__BackingField_1() const { return ___U3CpositionU3Ek__BackingField_1; }
	inline Position_t3597491459 ** get_address_of_U3CpositionU3Ek__BackingField_1() { return &___U3CpositionU3Ek__BackingField_1; }
	inline void set_U3CpositionU3Ek__BackingField_1(Position_t3597491459 * value)
	{
		___U3CpositionU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpositionU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANCHOR_T1470404909_H
#ifndef CHECKSERVICESTATUS_T1108037484_H
#define CHECKSERVICESTATUS_T1108037484_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzer/CheckServiceStatus
struct  CheckServiceStatus_t1108037484  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzer IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzer/CheckServiceStatus::m_Service
	ToneAnalyzer_t1356110496 * ___m_Service_0;
	// IBM.Watson.DeveloperCloud.Services.ServiceStatus IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzer/CheckServiceStatus::m_Callback
	ServiceStatus_t1443707987 * ___m_Callback_1;

public:
	inline static int32_t get_offset_of_m_Service_0() { return static_cast<int32_t>(offsetof(CheckServiceStatus_t1108037484, ___m_Service_0)); }
	inline ToneAnalyzer_t1356110496 * get_m_Service_0() const { return ___m_Service_0; }
	inline ToneAnalyzer_t1356110496 ** get_address_of_m_Service_0() { return &___m_Service_0; }
	inline void set_m_Service_0(ToneAnalyzer_t1356110496 * value)
	{
		___m_Service_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Service_0), value);
	}

	inline static int32_t get_offset_of_m_Callback_1() { return static_cast<int32_t>(offsetof(CheckServiceStatus_t1108037484, ___m_Callback_1)); }
	inline ServiceStatus_t1443707987 * get_m_Callback_1() const { return ___m_Callback_1; }
	inline ServiceStatus_t1443707987 ** get_address_of_m_Callback_1() { return &___m_Callback_1; }
	inline void set_m_Callback_1(ServiceStatus_t1443707987 * value)
	{
		___m_Callback_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Callback_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHECKSERVICESTATUS_T1108037484_H
#ifndef POSITION_T3597491459_H
#define POSITION_T3597491459_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Position
struct  Position_t3597491459  : public RuntimeObject
{
public:
	// System.Double IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Position::<x>k__BackingField
	double ___U3CxU3Ek__BackingField_0;
	// System.Double IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Position::<y>k__BackingField
	double ___U3CyU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CxU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Position_t3597491459, ___U3CxU3Ek__BackingField_0)); }
	inline double get_U3CxU3Ek__BackingField_0() const { return ___U3CxU3Ek__BackingField_0; }
	inline double* get_address_of_U3CxU3Ek__BackingField_0() { return &___U3CxU3Ek__BackingField_0; }
	inline void set_U3CxU3Ek__BackingField_0(double value)
	{
		___U3CxU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CyU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Position_t3597491459, ___U3CyU3Ek__BackingField_1)); }
	inline double get_U3CyU3Ek__BackingField_1() const { return ___U3CyU3Ek__BackingField_1; }
	inline double* get_address_of_U3CyU3Ek__BackingField_1() { return &___U3CyU3Ek__BackingField_1; }
	inline void set_U3CyU3Ek__BackingField_1(double value)
	{
		___U3CyU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSITION_T3597491459_H
#ifndef WORDS_T2948214629_H
#define WORDS_T2948214629_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Words
struct  Words_t2948214629  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Word[] IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Words::<words>k__BackingField
	WordU5BU5D_t3686373631* ___U3CwordsU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CwordsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Words_t2948214629, ___U3CwordsU3Ek__BackingField_0)); }
	inline WordU5BU5D_t3686373631* get_U3CwordsU3Ek__BackingField_0() const { return ___U3CwordsU3Ek__BackingField_0; }
	inline WordU5BU5D_t3686373631** get_address_of_U3CwordsU3Ek__BackingField_0() { return &___U3CwordsU3Ek__BackingField_0; }
	inline void set_U3CwordsU3Ek__BackingField_0(WordU5BU5D_t3686373631* value)
	{
		___U3CwordsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwordsU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORDS_T2948214629_H
#ifndef APPLICATIONDATA_T1885408412_H
#define APPLICATIONDATA_T1885408412_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.ApplicationData
struct  ApplicationData_t1885408412  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPLICATIONDATA_T1885408412_H
#ifndef AGE_T582693723_H
#define AGE_T582693723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.Age
struct  Age_t582693723  : public RuntimeObject
{
public:
	// System.Int32 IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.Age::<min>k__BackingField
	int32_t ___U3CminU3Ek__BackingField_0;
	// System.Int32 IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.Age::<max>k__BackingField
	int32_t ___U3CmaxU3Ek__BackingField_1;
	// System.Double IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.Age::<score>k__BackingField
	double ___U3CscoreU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CminU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Age_t582693723, ___U3CminU3Ek__BackingField_0)); }
	inline int32_t get_U3CminU3Ek__BackingField_0() const { return ___U3CminU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CminU3Ek__BackingField_0() { return &___U3CminU3Ek__BackingField_0; }
	inline void set_U3CminU3Ek__BackingField_0(int32_t value)
	{
		___U3CminU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CmaxU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Age_t582693723, ___U3CmaxU3Ek__BackingField_1)); }
	inline int32_t get_U3CmaxU3Ek__BackingField_1() const { return ___U3CmaxU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CmaxU3Ek__BackingField_1() { return &___U3CmaxU3Ek__BackingField_1; }
	inline void set_U3CmaxU3Ek__BackingField_1(int32_t value)
	{
		___U3CmaxU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CscoreU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Age_t582693723, ___U3CscoreU3Ek__BackingField_2)); }
	inline double get_U3CscoreU3Ek__BackingField_2() const { return ___U3CscoreU3Ek__BackingField_2; }
	inline double* get_address_of_U3CscoreU3Ek__BackingField_2() { return &___U3CscoreU3Ek__BackingField_2; }
	inline void set_U3CscoreU3Ek__BackingField_2(double value)
	{
		___U3CscoreU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AGE_T582693723_H
#ifndef CUSTOMIZATION_T2261013253_H
#define CUSTOMIZATION_T2261013253_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Customization
struct  Customization_t2261013253  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Customization::<customization_id>k__BackingField
	String_t* ___U3Ccustomization_idU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Customization::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Customization::<language>k__BackingField
	String_t* ___U3ClanguageU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Customization::<owner>k__BackingField
	String_t* ___U3CownerU3Ek__BackingField_3;
	// System.Double IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Customization::<created>k__BackingField
	double ___U3CcreatedU3Ek__BackingField_4;
	// System.Double IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Customization::<last_modified>k__BackingField
	double ___U3Clast_modifiedU3Ek__BackingField_5;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Customization::<description>k__BackingField
	String_t* ___U3CdescriptionU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3Ccustomization_idU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Customization_t2261013253, ___U3Ccustomization_idU3Ek__BackingField_0)); }
	inline String_t* get_U3Ccustomization_idU3Ek__BackingField_0() const { return ___U3Ccustomization_idU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3Ccustomization_idU3Ek__BackingField_0() { return &___U3Ccustomization_idU3Ek__BackingField_0; }
	inline void set_U3Ccustomization_idU3Ek__BackingField_0(String_t* value)
	{
		___U3Ccustomization_idU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Ccustomization_idU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Customization_t2261013253, ___U3CnameU3Ek__BackingField_1)); }
	inline String_t* get_U3CnameU3Ek__BackingField_1() const { return ___U3CnameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_1() { return &___U3CnameU3Ek__BackingField_1; }
	inline void set_U3CnameU3Ek__BackingField_1(String_t* value)
	{
		___U3CnameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3ClanguageU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Customization_t2261013253, ___U3ClanguageU3Ek__BackingField_2)); }
	inline String_t* get_U3ClanguageU3Ek__BackingField_2() const { return ___U3ClanguageU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3ClanguageU3Ek__BackingField_2() { return &___U3ClanguageU3Ek__BackingField_2; }
	inline void set_U3ClanguageU3Ek__BackingField_2(String_t* value)
	{
		___U3ClanguageU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClanguageU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CownerU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Customization_t2261013253, ___U3CownerU3Ek__BackingField_3)); }
	inline String_t* get_U3CownerU3Ek__BackingField_3() const { return ___U3CownerU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CownerU3Ek__BackingField_3() { return &___U3CownerU3Ek__BackingField_3; }
	inline void set_U3CownerU3Ek__BackingField_3(String_t* value)
	{
		___U3CownerU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CownerU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CcreatedU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Customization_t2261013253, ___U3CcreatedU3Ek__BackingField_4)); }
	inline double get_U3CcreatedU3Ek__BackingField_4() const { return ___U3CcreatedU3Ek__BackingField_4; }
	inline double* get_address_of_U3CcreatedU3Ek__BackingField_4() { return &___U3CcreatedU3Ek__BackingField_4; }
	inline void set_U3CcreatedU3Ek__BackingField_4(double value)
	{
		___U3CcreatedU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3Clast_modifiedU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Customization_t2261013253, ___U3Clast_modifiedU3Ek__BackingField_5)); }
	inline double get_U3Clast_modifiedU3Ek__BackingField_5() const { return ___U3Clast_modifiedU3Ek__BackingField_5; }
	inline double* get_address_of_U3Clast_modifiedU3Ek__BackingField_5() { return &___U3Clast_modifiedU3Ek__BackingField_5; }
	inline void set_U3Clast_modifiedU3Ek__BackingField_5(double value)
	{
		___U3Clast_modifiedU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CdescriptionU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Customization_t2261013253, ___U3CdescriptionU3Ek__BackingField_6)); }
	inline String_t* get_U3CdescriptionU3Ek__BackingField_6() const { return ___U3CdescriptionU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CdescriptionU3Ek__BackingField_6() { return &___U3CdescriptionU3Ek__BackingField_6; }
	inline void set_U3CdescriptionU3Ek__BackingField_6(String_t* value)
	{
		___U3CdescriptionU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdescriptionU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMIZATION_T2261013253_H
#ifndef COLLECTIONSCONFIG_T2493163663_H
#define COLLECTIONSCONFIG_T2493163663_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.CollectionsConfig
struct  CollectionsConfig_t2493163663  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.CollectionImagesConfig[] IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.CollectionsConfig::<images>k__BackingField
	CollectionImagesConfigU5BU5D_t666950595* ___U3CimagesU3Ek__BackingField_0;
	// System.Int32 IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.CollectionsConfig::<images_processed>k__BackingField
	int32_t ___U3Cimages_processedU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CimagesU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CollectionsConfig_t2493163663, ___U3CimagesU3Ek__BackingField_0)); }
	inline CollectionImagesConfigU5BU5D_t666950595* get_U3CimagesU3Ek__BackingField_0() const { return ___U3CimagesU3Ek__BackingField_0; }
	inline CollectionImagesConfigU5BU5D_t666950595** get_address_of_U3CimagesU3Ek__BackingField_0() { return &___U3CimagesU3Ek__BackingField_0; }
	inline void set_U3CimagesU3Ek__BackingField_0(CollectionImagesConfigU5BU5D_t666950595* value)
	{
		___U3CimagesU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CimagesU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3Cimages_processedU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CollectionsConfig_t2493163663, ___U3Cimages_processedU3Ek__BackingField_1)); }
	inline int32_t get_U3Cimages_processedU3Ek__BackingField_1() const { return ___U3Cimages_processedU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3Cimages_processedU3Ek__BackingField_1() { return &___U3Cimages_processedU3Ek__BackingField_1; }
	inline void set_U3Cimages_processedU3Ek__BackingField_1(int32_t value)
	{
		___U3Cimages_processedU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTIONSCONFIG_T2493163663_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef WORDTYPE_T1970187882_H
#define WORDTYPE_T1970187882_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordType
struct  WordType_t1970187882  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORDTYPE_T1970187882_H
#ifndef WORDTYPETOADD_T2941578038_H
#define WORDTYPETOADD_T2941578038_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordTypeToAdd
struct  WordTypeToAdd_t2941578038  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORDTYPETOADD_T2941578038_H
#ifndef CUSTOMIZATIONID_T344279642_H
#define CUSTOMIZATIONID_T344279642_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomizationID
struct  CustomizationID_t344279642  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomizationID::<customization_id>k__BackingField
	String_t* ___U3Ccustomization_idU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3Ccustomization_idU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CustomizationID_t344279642, ___U3Ccustomization_idU3Ek__BackingField_0)); }
	inline String_t* get_U3Ccustomization_idU3Ek__BackingField_0() const { return ___U3Ccustomization_idU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3Ccustomization_idU3Ek__BackingField_0() { return &___U3Ccustomization_idU3Ek__BackingField_0; }
	inline void set_U3Ccustomization_idU3Ek__BackingField_0(String_t* value)
	{
		___U3Ccustomization_idU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Ccustomization_idU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMIZATIONID_T344279642_H
#ifndef CUSTOMVOICE_T330695553_H
#define CUSTOMVOICE_T330695553_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomVoice
struct  CustomVoice_t330695553  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomVoice::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomVoice::<language>k__BackingField
	String_t* ___U3ClanguageU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomVoice::<description>k__BackingField
	String_t* ___U3CdescriptionU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CustomVoice_t330695553, ___U3CnameU3Ek__BackingField_0)); }
	inline String_t* get_U3CnameU3Ek__BackingField_0() const { return ___U3CnameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_0() { return &___U3CnameU3Ek__BackingField_0; }
	inline void set_U3CnameU3Ek__BackingField_0(String_t* value)
	{
		___U3CnameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3ClanguageU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CustomVoice_t330695553, ___U3ClanguageU3Ek__BackingField_1)); }
	inline String_t* get_U3ClanguageU3Ek__BackingField_1() const { return ___U3ClanguageU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3ClanguageU3Ek__BackingField_1() { return &___U3ClanguageU3Ek__BackingField_1; }
	inline void set_U3ClanguageU3Ek__BackingField_1(String_t* value)
	{
		___U3ClanguageU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClanguageU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CdescriptionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CustomVoice_t330695553, ___U3CdescriptionU3Ek__BackingField_2)); }
	inline String_t* get_U3CdescriptionU3Ek__BackingField_2() const { return ___U3CdescriptionU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CdescriptionU3Ek__BackingField_2() { return &___U3CdescriptionU3Ek__BackingField_2; }
	inline void set_U3CdescriptionU3Ek__BackingField_2(String_t* value)
	{
		___U3CdescriptionU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdescriptionU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMVOICE_T330695553_H
#ifndef CUSTOMVOICEUPDATE_T1706248840_H
#define CUSTOMVOICEUPDATE_T1706248840_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomVoiceUpdate
struct  CustomVoiceUpdate_t1706248840  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomVoiceUpdate::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomVoiceUpdate::<description>k__BackingField
	String_t* ___U3CdescriptionU3Ek__BackingField_1;
	// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Word[] IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomVoiceUpdate::<words>k__BackingField
	WordU5BU5D_t3686373631* ___U3CwordsU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CustomVoiceUpdate_t1706248840, ___U3CnameU3Ek__BackingField_0)); }
	inline String_t* get_U3CnameU3Ek__BackingField_0() const { return ___U3CnameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_0() { return &___U3CnameU3Ek__BackingField_0; }
	inline void set_U3CnameU3Ek__BackingField_0(String_t* value)
	{
		___U3CnameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CdescriptionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CustomVoiceUpdate_t1706248840, ___U3CdescriptionU3Ek__BackingField_1)); }
	inline String_t* get_U3CdescriptionU3Ek__BackingField_1() const { return ___U3CdescriptionU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CdescriptionU3Ek__BackingField_1() { return &___U3CdescriptionU3Ek__BackingField_1; }
	inline void set_U3CdescriptionU3Ek__BackingField_1(String_t* value)
	{
		___U3CdescriptionU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdescriptionU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CwordsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CustomVoiceUpdate_t1706248840, ___U3CwordsU3Ek__BackingField_2)); }
	inline WordU5BU5D_t3686373631* get_U3CwordsU3Ek__BackingField_2() const { return ___U3CwordsU3Ek__BackingField_2; }
	inline WordU5BU5D_t3686373631** get_address_of_U3CwordsU3Ek__BackingField_2() { return &___U3CwordsU3Ek__BackingField_2; }
	inline void set_U3CwordsU3Ek__BackingField_2(WordU5BU5D_t3686373631* value)
	{
		___U3CwordsU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwordsU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMVOICEUPDATE_T1706248840_H
#ifndef WORD_T4274554970_H
#define WORD_T4274554970_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Word
struct  Word_t4274554970  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Word::<word>k__BackingField
	String_t* ___U3CwordU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Word::<translation>k__BackingField
	String_t* ___U3CtranslationU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CwordU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Word_t4274554970, ___U3CwordU3Ek__BackingField_0)); }
	inline String_t* get_U3CwordU3Ek__BackingField_0() const { return ___U3CwordU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CwordU3Ek__BackingField_0() { return &___U3CwordU3Ek__BackingField_0; }
	inline void set_U3CwordU3Ek__BackingField_0(String_t* value)
	{
		___U3CwordU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwordU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CtranslationU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Word_t4274554970, ___U3CtranslationU3Ek__BackingField_1)); }
	inline String_t* get_U3CtranslationU3Ek__BackingField_1() const { return ___U3CtranslationU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CtranslationU3Ek__BackingField_1() { return &___U3CtranslationU3Ek__BackingField_1; }
	inline void set_U3CtranslationU3Ek__BackingField_1(String_t* value)
	{
		___U3CtranslationU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtranslationU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORD_T4274554970_H
#ifndef CUSTOMIZATIONS_T482380184_H
#define CUSTOMIZATIONS_T482380184_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Customizations
struct  Customizations_t482380184  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Customization[] IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Customizations::<customizations>k__BackingField
	CustomizationU5BU5D_t626929480* ___U3CcustomizationsU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CcustomizationsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Customizations_t482380184, ___U3CcustomizationsU3Ek__BackingField_0)); }
	inline CustomizationU5BU5D_t626929480* get_U3CcustomizationsU3Ek__BackingField_0() const { return ___U3CcustomizationsU3Ek__BackingField_0; }
	inline CustomizationU5BU5D_t626929480** get_address_of_U3CcustomizationsU3Ek__BackingField_0() { return &___U3CcustomizationsU3Ek__BackingField_0; }
	inline void set_U3CcustomizationsU3Ek__BackingField_0(CustomizationU5BU5D_t626929480* value)
	{
		___U3CcustomizationsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcustomizationsU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMIZATIONS_T482380184_H
#ifndef CUSTOMIZATIONWORDS_T949522428_H
#define CUSTOMIZATIONWORDS_T949522428_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomizationWords
struct  CustomizationWords_t949522428  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomizationWords::<customization_id>k__BackingField
	String_t* ___U3Ccustomization_idU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomizationWords::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomizationWords::<language>k__BackingField
	String_t* ___U3ClanguageU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomizationWords::<owner>k__BackingField
	String_t* ___U3CownerU3Ek__BackingField_3;
	// System.Double IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomizationWords::<created>k__BackingField
	double ___U3CcreatedU3Ek__BackingField_4;
	// System.Double IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomizationWords::<last_modified>k__BackingField
	double ___U3Clast_modifiedU3Ek__BackingField_5;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomizationWords::<description>k__BackingField
	String_t* ___U3CdescriptionU3Ek__BackingField_6;
	// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Word[] IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomizationWords::<words>k__BackingField
	WordU5BU5D_t3686373631* ___U3CwordsU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_U3Ccustomization_idU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CustomizationWords_t949522428, ___U3Ccustomization_idU3Ek__BackingField_0)); }
	inline String_t* get_U3Ccustomization_idU3Ek__BackingField_0() const { return ___U3Ccustomization_idU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3Ccustomization_idU3Ek__BackingField_0() { return &___U3Ccustomization_idU3Ek__BackingField_0; }
	inline void set_U3Ccustomization_idU3Ek__BackingField_0(String_t* value)
	{
		___U3Ccustomization_idU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Ccustomization_idU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CustomizationWords_t949522428, ___U3CnameU3Ek__BackingField_1)); }
	inline String_t* get_U3CnameU3Ek__BackingField_1() const { return ___U3CnameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_1() { return &___U3CnameU3Ek__BackingField_1; }
	inline void set_U3CnameU3Ek__BackingField_1(String_t* value)
	{
		___U3CnameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3ClanguageU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CustomizationWords_t949522428, ___U3ClanguageU3Ek__BackingField_2)); }
	inline String_t* get_U3ClanguageU3Ek__BackingField_2() const { return ___U3ClanguageU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3ClanguageU3Ek__BackingField_2() { return &___U3ClanguageU3Ek__BackingField_2; }
	inline void set_U3ClanguageU3Ek__BackingField_2(String_t* value)
	{
		___U3ClanguageU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClanguageU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CownerU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(CustomizationWords_t949522428, ___U3CownerU3Ek__BackingField_3)); }
	inline String_t* get_U3CownerU3Ek__BackingField_3() const { return ___U3CownerU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CownerU3Ek__BackingField_3() { return &___U3CownerU3Ek__BackingField_3; }
	inline void set_U3CownerU3Ek__BackingField_3(String_t* value)
	{
		___U3CownerU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CownerU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CcreatedU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(CustomizationWords_t949522428, ___U3CcreatedU3Ek__BackingField_4)); }
	inline double get_U3CcreatedU3Ek__BackingField_4() const { return ___U3CcreatedU3Ek__BackingField_4; }
	inline double* get_address_of_U3CcreatedU3Ek__BackingField_4() { return &___U3CcreatedU3Ek__BackingField_4; }
	inline void set_U3CcreatedU3Ek__BackingField_4(double value)
	{
		___U3CcreatedU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3Clast_modifiedU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(CustomizationWords_t949522428, ___U3Clast_modifiedU3Ek__BackingField_5)); }
	inline double get_U3Clast_modifiedU3Ek__BackingField_5() const { return ___U3Clast_modifiedU3Ek__BackingField_5; }
	inline double* get_address_of_U3Clast_modifiedU3Ek__BackingField_5() { return &___U3Clast_modifiedU3Ek__BackingField_5; }
	inline void set_U3Clast_modifiedU3Ek__BackingField_5(double value)
	{
		___U3Clast_modifiedU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CdescriptionU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(CustomizationWords_t949522428, ___U3CdescriptionU3Ek__BackingField_6)); }
	inline String_t* get_U3CdescriptionU3Ek__BackingField_6() const { return ___U3CdescriptionU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CdescriptionU3Ek__BackingField_6() { return &___U3CdescriptionU3Ek__BackingField_6; }
	inline void set_U3CdescriptionU3Ek__BackingField_6(String_t* value)
	{
		___U3CdescriptionU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdescriptionU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CwordsU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(CustomizationWords_t949522428, ___U3CwordsU3Ek__BackingField_7)); }
	inline WordU5BU5D_t3686373631* get_U3CwordsU3Ek__BackingField_7() const { return ___U3CwordsU3Ek__BackingField_7; }
	inline WordU5BU5D_t3686373631** get_address_of_U3CwordsU3Ek__BackingField_7() { return &___U3CwordsU3Ek__BackingField_7; }
	inline void set_U3CwordsU3Ek__BackingField_7(WordU5BU5D_t3686373631* value)
	{
		___U3CwordsU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwordsU3Ek__BackingField_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMIZATIONWORDS_T949522428_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t1328083999* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t1328083999* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t1328083999** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t1328083999* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef WORD_T3204175642_H
#define WORD_T3204175642_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Word
struct  Word_t3204175642  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Word::<word>k__BackingField
	String_t* ___U3CwordU3Ek__BackingField_0;
	// System.String[] IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Word::<sounds_like>k__BackingField
	StringU5BU5D_t1642385972* ___U3Csounds_likeU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Word::<display_as>k__BackingField
	String_t* ___U3Cdisplay_asU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CwordU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Word_t3204175642, ___U3CwordU3Ek__BackingField_0)); }
	inline String_t* get_U3CwordU3Ek__BackingField_0() const { return ___U3CwordU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CwordU3Ek__BackingField_0() { return &___U3CwordU3Ek__BackingField_0; }
	inline void set_U3CwordU3Ek__BackingField_0(String_t* value)
	{
		___U3CwordU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwordU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3Csounds_likeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Word_t3204175642, ___U3Csounds_likeU3Ek__BackingField_1)); }
	inline StringU5BU5D_t1642385972* get_U3Csounds_likeU3Ek__BackingField_1() const { return ___U3Csounds_likeU3Ek__BackingField_1; }
	inline StringU5BU5D_t1642385972** get_address_of_U3Csounds_likeU3Ek__BackingField_1() { return &___U3Csounds_likeU3Ek__BackingField_1; }
	inline void set_U3Csounds_likeU3Ek__BackingField_1(StringU5BU5D_t1642385972* value)
	{
		___U3Csounds_likeU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3Csounds_likeU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3Cdisplay_asU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Word_t3204175642, ___U3Cdisplay_asU3Ek__BackingField_2)); }
	inline String_t* get_U3Cdisplay_asU3Ek__BackingField_2() const { return ___U3Cdisplay_asU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3Cdisplay_asU3Ek__BackingField_2() { return &___U3Cdisplay_asU3Ek__BackingField_2; }
	inline void set_U3Cdisplay_asU3Ek__BackingField_2(String_t* value)
	{
		___U3Cdisplay_asU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cdisplay_asU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORD_T3204175642_H
#ifndef MEMBERINFO_T_H
#define MEMBERINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFO_T_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef TIMESTAMP_T82276870_H
#define TIMESTAMP_T82276870_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.TimeStamp
struct  TimeStamp_t82276870  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.TimeStamp::<Word>k__BackingField
	String_t* ___U3CWordU3Ek__BackingField_0;
	// System.Double IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.TimeStamp::<Start>k__BackingField
	double ___U3CStartU3Ek__BackingField_1;
	// System.Double IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.TimeStamp::<End>k__BackingField
	double ___U3CEndU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CWordU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TimeStamp_t82276870, ___U3CWordU3Ek__BackingField_0)); }
	inline String_t* get_U3CWordU3Ek__BackingField_0() const { return ___U3CWordU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CWordU3Ek__BackingField_0() { return &___U3CWordU3Ek__BackingField_0; }
	inline void set_U3CWordU3Ek__BackingField_0(String_t* value)
	{
		___U3CWordU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CWordU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CStartU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TimeStamp_t82276870, ___U3CStartU3Ek__BackingField_1)); }
	inline double get_U3CStartU3Ek__BackingField_1() const { return ___U3CStartU3Ek__BackingField_1; }
	inline double* get_address_of_U3CStartU3Ek__BackingField_1() { return &___U3CStartU3Ek__BackingField_1; }
	inline void set_U3CStartU3Ek__BackingField_1(double value)
	{
		___U3CStartU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CEndU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(TimeStamp_t82276870, ___U3CEndU3Ek__BackingField_2)); }
	inline double get_U3CEndU3Ek__BackingField_2() const { return ___U3CEndU3Ek__BackingField_2; }
	inline double* get_address_of_U3CEndU3Ek__BackingField_2() { return &___U3CEndU3Ek__BackingField_2; }
	inline void set_U3CEndU3Ek__BackingField_2(double value)
	{
		___U3CEndU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESTAMP_T82276870_H
#ifndef WORDALTERNATIVERESULT_T3127007908_H
#define WORDALTERNATIVERESULT_T3127007908_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordAlternativeResult
struct  WordAlternativeResult_t3127007908  : public RuntimeObject
{
public:
	// System.Double IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordAlternativeResult::<confidence>k__BackingField
	double ___U3CconfidenceU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordAlternativeResult::<word>k__BackingField
	String_t* ___U3CwordU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CconfidenceU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(WordAlternativeResult_t3127007908, ___U3CconfidenceU3Ek__BackingField_0)); }
	inline double get_U3CconfidenceU3Ek__BackingField_0() const { return ___U3CconfidenceU3Ek__BackingField_0; }
	inline double* get_address_of_U3CconfidenceU3Ek__BackingField_0() { return &___U3CconfidenceU3Ek__BackingField_0; }
	inline void set_U3CconfidenceU3Ek__BackingField_0(double value)
	{
		___U3CconfidenceU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CwordU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(WordAlternativeResult_t3127007908, ___U3CwordU3Ek__BackingField_1)); }
	inline String_t* get_U3CwordU3Ek__BackingField_1() const { return ___U3CwordU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CwordU3Ek__BackingField_1() { return &___U3CwordU3Ek__BackingField_1; }
	inline void set_U3CwordU3Ek__BackingField_1(String_t* value)
	{
		___U3CwordU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwordU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORDALTERNATIVERESULT_T3127007908_H
#ifndef WORDS_T4062187109_H
#define WORDS_T4062187109_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Words
struct  Words_t4062187109  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Word[] IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Words::<words>k__BackingField
	WordU5BU5D_t2800973631* ___U3CwordsU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CwordsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Words_t4062187109, ___U3CwordsU3Ek__BackingField_0)); }
	inline WordU5BU5D_t2800973631* get_U3CwordsU3Ek__BackingField_0() const { return ___U3CwordsU3Ek__BackingField_0; }
	inline WordU5BU5D_t2800973631** get_address_of_U3CwordsU3Ek__BackingField_0() { return &___U3CwordsU3Ek__BackingField_0; }
	inline void set_U3CwordsU3Ek__BackingField_0(WordU5BU5D_t2800973631* value)
	{
		___U3CwordsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwordsU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORDS_T4062187109_H
#ifndef WORDSLIST_T29283159_H
#define WORDSLIST_T29283159_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordsList
struct  WordsList_t29283159  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordData[] IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordsList::<words>k__BackingField
	WordDataU5BU5D_t1726140911* ___U3CwordsU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CwordsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(WordsList_t29283159, ___U3CwordsU3Ek__BackingField_0)); }
	inline WordDataU5BU5D_t1726140911* get_U3CwordsU3Ek__BackingField_0() const { return ___U3CwordsU3Ek__BackingField_0; }
	inline WordDataU5BU5D_t1726140911** get_address_of_U3CwordsU3Ek__BackingField_0() { return &___U3CwordsU3Ek__BackingField_0; }
	inline void set_U3CwordsU3Ek__BackingField_0(WordDataU5BU5D_t1726140911* value)
	{
		___U3CwordsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwordsU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORDSLIST_T29283159_H
#ifndef WORDDATA_T612265514_H
#define WORDDATA_T612265514_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordData
struct  WordData_t612265514  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordData::<word>k__BackingField
	String_t* ___U3CwordU3Ek__BackingField_0;
	// System.String[] IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordData::<sounds_like>k__BackingField
	StringU5BU5D_t1642385972* ___U3Csounds_likeU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordData::<display_as>k__BackingField
	String_t* ___U3Cdisplay_asU3Ek__BackingField_2;
	// System.String[] IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordData::<source>k__BackingField
	StringU5BU5D_t1642385972* ___U3CsourceU3Ek__BackingField_3;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordData::<error>k__BackingField
	String_t* ___U3CerrorU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CwordU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(WordData_t612265514, ___U3CwordU3Ek__BackingField_0)); }
	inline String_t* get_U3CwordU3Ek__BackingField_0() const { return ___U3CwordU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CwordU3Ek__BackingField_0() { return &___U3CwordU3Ek__BackingField_0; }
	inline void set_U3CwordU3Ek__BackingField_0(String_t* value)
	{
		___U3CwordU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwordU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3Csounds_likeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(WordData_t612265514, ___U3Csounds_likeU3Ek__BackingField_1)); }
	inline StringU5BU5D_t1642385972* get_U3Csounds_likeU3Ek__BackingField_1() const { return ___U3Csounds_likeU3Ek__BackingField_1; }
	inline StringU5BU5D_t1642385972** get_address_of_U3Csounds_likeU3Ek__BackingField_1() { return &___U3Csounds_likeU3Ek__BackingField_1; }
	inline void set_U3Csounds_likeU3Ek__BackingField_1(StringU5BU5D_t1642385972* value)
	{
		___U3Csounds_likeU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3Csounds_likeU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3Cdisplay_asU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(WordData_t612265514, ___U3Cdisplay_asU3Ek__BackingField_2)); }
	inline String_t* get_U3Cdisplay_asU3Ek__BackingField_2() const { return ___U3Cdisplay_asU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3Cdisplay_asU3Ek__BackingField_2() { return &___U3Cdisplay_asU3Ek__BackingField_2; }
	inline void set_U3Cdisplay_asU3Ek__BackingField_2(String_t* value)
	{
		___U3Cdisplay_asU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cdisplay_asU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CsourceU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(WordData_t612265514, ___U3CsourceU3Ek__BackingField_3)); }
	inline StringU5BU5D_t1642385972* get_U3CsourceU3Ek__BackingField_3() const { return ___U3CsourceU3Ek__BackingField_3; }
	inline StringU5BU5D_t1642385972** get_address_of_U3CsourceU3Ek__BackingField_3() { return &___U3CsourceU3Ek__BackingField_3; }
	inline void set_U3CsourceU3Ek__BackingField_3(StringU5BU5D_t1642385972* value)
	{
		___U3CsourceU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsourceU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CerrorU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(WordData_t612265514, ___U3CerrorU3Ek__BackingField_4)); }
	inline String_t* get_U3CerrorU3Ek__BackingField_4() const { return ___U3CerrorU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CerrorU3Ek__BackingField_4() { return &___U3CerrorU3Ek__BackingField_4; }
	inline void set_U3CerrorU3Ek__BackingField_4(String_t* value)
	{
		___U3CerrorU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CerrorU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORDDATA_T612265514_H
#ifndef WORDALTERNATIVERESULTS_T1270965811_H
#define WORDALTERNATIVERESULTS_T1270965811_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordAlternativeResults
struct  WordAlternativeResults_t1270965811  : public RuntimeObject
{
public:
	// System.Double IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordAlternativeResults::<start_time>k__BackingField
	double ___U3Cstart_timeU3Ek__BackingField_0;
	// System.Double IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordAlternativeResults::<end_time>k__BackingField
	double ___U3Cend_timeU3Ek__BackingField_1;
	// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordAlternativeResult[] IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordAlternativeResults::<alternatives>k__BackingField
	WordAlternativeResultU5BU5D_t598168141* ___U3CalternativesU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3Cstart_timeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(WordAlternativeResults_t1270965811, ___U3Cstart_timeU3Ek__BackingField_0)); }
	inline double get_U3Cstart_timeU3Ek__BackingField_0() const { return ___U3Cstart_timeU3Ek__BackingField_0; }
	inline double* get_address_of_U3Cstart_timeU3Ek__BackingField_0() { return &___U3Cstart_timeU3Ek__BackingField_0; }
	inline void set_U3Cstart_timeU3Ek__BackingField_0(double value)
	{
		___U3Cstart_timeU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3Cend_timeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(WordAlternativeResults_t1270965811, ___U3Cend_timeU3Ek__BackingField_1)); }
	inline double get_U3Cend_timeU3Ek__BackingField_1() const { return ___U3Cend_timeU3Ek__BackingField_1; }
	inline double* get_address_of_U3Cend_timeU3Ek__BackingField_1() { return &___U3Cend_timeU3Ek__BackingField_1; }
	inline void set_U3Cend_timeU3Ek__BackingField_1(double value)
	{
		___U3Cend_timeU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CalternativesU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(WordAlternativeResults_t1270965811, ___U3CalternativesU3Ek__BackingField_2)); }
	inline WordAlternativeResultU5BU5D_t598168141* get_U3CalternativesU3Ek__BackingField_2() const { return ___U3CalternativesU3Ek__BackingField_2; }
	inline WordAlternativeResultU5BU5D_t598168141** get_address_of_U3CalternativesU3Ek__BackingField_2() { return &___U3CalternativesU3Ek__BackingField_2; }
	inline void set_U3CalternativesU3Ek__BackingField_2(WordAlternativeResultU5BU5D_t598168141* value)
	{
		___U3CalternativesU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CalternativesU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORDALTERNATIVERESULTS_T1270965811_H
#ifndef WORDCONFIDENCE_T2757029394_H
#define WORDCONFIDENCE_T2757029394_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordConfidence
struct  WordConfidence_t2757029394  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordConfidence::<Word>k__BackingField
	String_t* ___U3CWordU3Ek__BackingField_0;
	// System.Double IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordConfidence::<Confidence>k__BackingField
	double ___U3CConfidenceU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CWordU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(WordConfidence_t2757029394, ___U3CWordU3Ek__BackingField_0)); }
	inline String_t* get_U3CWordU3Ek__BackingField_0() const { return ___U3CWordU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CWordU3Ek__BackingField_0() { return &___U3CWordU3Ek__BackingField_0; }
	inline void set_U3CWordU3Ek__BackingField_0(String_t* value)
	{
		___U3CWordU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CWordU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CConfidenceU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(WordConfidence_t2757029394, ___U3CConfidenceU3Ek__BackingField_1)); }
	inline double get_U3CConfidenceU3Ek__BackingField_1() const { return ___U3CConfidenceU3Ek__BackingField_1; }
	inline double* get_address_of_U3CConfidenceU3Ek__BackingField_1() { return &___U3CConfidenceU3Ek__BackingField_1; }
	inline void set_U3CConfidenceU3Ek__BackingField_1(double value)
	{
		___U3CConfidenceU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORDCONFIDENCE_T2757029394_H
#ifndef CLASSRESULT_T3791036973_H
#define CLASSRESULT_T3791036973_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassResult
struct  ClassResult_t3791036973  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassResult::<m_class>k__BackingField
	String_t* ___U3Cm_classU3Ek__BackingField_0;
	// System.Double IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassResult::<score>k__BackingField
	double ___U3CscoreU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassResult::<type_hierarchy>k__BackingField
	String_t* ___U3Ctype_hierarchyU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3Cm_classU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ClassResult_t3791036973, ___U3Cm_classU3Ek__BackingField_0)); }
	inline String_t* get_U3Cm_classU3Ek__BackingField_0() const { return ___U3Cm_classU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3Cm_classU3Ek__BackingField_0() { return &___U3Cm_classU3Ek__BackingField_0; }
	inline void set_U3Cm_classU3Ek__BackingField_0(String_t* value)
	{
		___U3Cm_classU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cm_classU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CscoreU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ClassResult_t3791036973, ___U3CscoreU3Ek__BackingField_1)); }
	inline double get_U3CscoreU3Ek__BackingField_1() const { return ___U3CscoreU3Ek__BackingField_1; }
	inline double* get_address_of_U3CscoreU3Ek__BackingField_1() { return &___U3CscoreU3Ek__BackingField_1; }
	inline void set_U3CscoreU3Ek__BackingField_1(double value)
	{
		___U3CscoreU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3Ctype_hierarchyU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ClassResult_t3791036973, ___U3Ctype_hierarchyU3Ek__BackingField_2)); }
	inline String_t* get_U3Ctype_hierarchyU3Ek__BackingField_2() const { return ___U3Ctype_hierarchyU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3Ctype_hierarchyU3Ek__BackingField_2() { return &___U3Ctype_hierarchyU3Ek__BackingField_2; }
	inline void set_U3Ctype_hierarchyU3Ek__BackingField_2(String_t* value)
	{
		___U3Ctype_hierarchyU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3Ctype_hierarchyU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLASSRESULT_T3791036973_H
#ifndef REQUEST_T466816980_H
#define REQUEST_T466816980_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request
struct  Request_t466816980  : public RuntimeObject
{
public:
	// System.Single IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Timeout>k__BackingField
	float ___U3CTimeoutU3Ek__BackingField_0;
	// System.Boolean IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Cancel>k__BackingField
	bool ___U3CCancelU3Ek__BackingField_1;
	// System.Boolean IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Delete>k__BackingField
	bool ___U3CDeleteU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Function>k__BackingField
	String_t* ___U3CFunctionU3Ek__BackingField_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Parameters>k__BackingField
	Dictionary_2_t309261261 * ___U3CParametersU3Ek__BackingField_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Headers>k__BackingField
	Dictionary_2_t3943999495 * ___U3CHeadersU3Ek__BackingField_5;
	// System.Byte[] IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Send>k__BackingField
	ByteU5BU5D_t3397334013* ___U3CSendU3Ek__BackingField_6;
	// System.Collections.Generic.Dictionary`2<System.String,IBM.Watson.DeveloperCloud.Connection.RESTConnector/Form> IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Forms>k__BackingField
	Dictionary_2_t2694055125 * ___U3CFormsU3Ek__BackingField_7;
	// IBM.Watson.DeveloperCloud.Connection.RESTConnector/ResponseEvent IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<OnResponse>k__BackingField
	ResponseEvent_t1616568356 * ___U3COnResponseU3Ek__BackingField_8;
	// IBM.Watson.DeveloperCloud.Connection.RESTConnector/ProgressEvent IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<OnDownloadProgress>k__BackingField
	ProgressEvent_t4185145044 * ___U3COnDownloadProgressU3Ek__BackingField_9;
	// IBM.Watson.DeveloperCloud.Connection.RESTConnector/ProgressEvent IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<OnUploadProgress>k__BackingField
	ProgressEvent_t4185145044 * ___U3COnUploadProgressU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of_U3CTimeoutU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CTimeoutU3Ek__BackingField_0)); }
	inline float get_U3CTimeoutU3Ek__BackingField_0() const { return ___U3CTimeoutU3Ek__BackingField_0; }
	inline float* get_address_of_U3CTimeoutU3Ek__BackingField_0() { return &___U3CTimeoutU3Ek__BackingField_0; }
	inline void set_U3CTimeoutU3Ek__BackingField_0(float value)
	{
		___U3CTimeoutU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CCancelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CCancelU3Ek__BackingField_1)); }
	inline bool get_U3CCancelU3Ek__BackingField_1() const { return ___U3CCancelU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CCancelU3Ek__BackingField_1() { return &___U3CCancelU3Ek__BackingField_1; }
	inline void set_U3CCancelU3Ek__BackingField_1(bool value)
	{
		___U3CCancelU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CDeleteU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CDeleteU3Ek__BackingField_2)); }
	inline bool get_U3CDeleteU3Ek__BackingField_2() const { return ___U3CDeleteU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CDeleteU3Ek__BackingField_2() { return &___U3CDeleteU3Ek__BackingField_2; }
	inline void set_U3CDeleteU3Ek__BackingField_2(bool value)
	{
		___U3CDeleteU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CFunctionU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CFunctionU3Ek__BackingField_3)); }
	inline String_t* get_U3CFunctionU3Ek__BackingField_3() const { return ___U3CFunctionU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CFunctionU3Ek__BackingField_3() { return &___U3CFunctionU3Ek__BackingField_3; }
	inline void set_U3CFunctionU3Ek__BackingField_3(String_t* value)
	{
		___U3CFunctionU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFunctionU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CParametersU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CParametersU3Ek__BackingField_4)); }
	inline Dictionary_2_t309261261 * get_U3CParametersU3Ek__BackingField_4() const { return ___U3CParametersU3Ek__BackingField_4; }
	inline Dictionary_2_t309261261 ** get_address_of_U3CParametersU3Ek__BackingField_4() { return &___U3CParametersU3Ek__BackingField_4; }
	inline void set_U3CParametersU3Ek__BackingField_4(Dictionary_2_t309261261 * value)
	{
		___U3CParametersU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CParametersU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CHeadersU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CHeadersU3Ek__BackingField_5)); }
	inline Dictionary_2_t3943999495 * get_U3CHeadersU3Ek__BackingField_5() const { return ___U3CHeadersU3Ek__BackingField_5; }
	inline Dictionary_2_t3943999495 ** get_address_of_U3CHeadersU3Ek__BackingField_5() { return &___U3CHeadersU3Ek__BackingField_5; }
	inline void set_U3CHeadersU3Ek__BackingField_5(Dictionary_2_t3943999495 * value)
	{
		___U3CHeadersU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CHeadersU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CSendU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CSendU3Ek__BackingField_6)); }
	inline ByteU5BU5D_t3397334013* get_U3CSendU3Ek__BackingField_6() const { return ___U3CSendU3Ek__BackingField_6; }
	inline ByteU5BU5D_t3397334013** get_address_of_U3CSendU3Ek__BackingField_6() { return &___U3CSendU3Ek__BackingField_6; }
	inline void set_U3CSendU3Ek__BackingField_6(ByteU5BU5D_t3397334013* value)
	{
		___U3CSendU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSendU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CFormsU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CFormsU3Ek__BackingField_7)); }
	inline Dictionary_2_t2694055125 * get_U3CFormsU3Ek__BackingField_7() const { return ___U3CFormsU3Ek__BackingField_7; }
	inline Dictionary_2_t2694055125 ** get_address_of_U3CFormsU3Ek__BackingField_7() { return &___U3CFormsU3Ek__BackingField_7; }
	inline void set_U3CFormsU3Ek__BackingField_7(Dictionary_2_t2694055125 * value)
	{
		___U3CFormsU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFormsU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3COnResponseU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3COnResponseU3Ek__BackingField_8)); }
	inline ResponseEvent_t1616568356 * get_U3COnResponseU3Ek__BackingField_8() const { return ___U3COnResponseU3Ek__BackingField_8; }
	inline ResponseEvent_t1616568356 ** get_address_of_U3COnResponseU3Ek__BackingField_8() { return &___U3COnResponseU3Ek__BackingField_8; }
	inline void set_U3COnResponseU3Ek__BackingField_8(ResponseEvent_t1616568356 * value)
	{
		___U3COnResponseU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3COnResponseU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3COnDownloadProgressU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3COnDownloadProgressU3Ek__BackingField_9)); }
	inline ProgressEvent_t4185145044 * get_U3COnDownloadProgressU3Ek__BackingField_9() const { return ___U3COnDownloadProgressU3Ek__BackingField_9; }
	inline ProgressEvent_t4185145044 ** get_address_of_U3COnDownloadProgressU3Ek__BackingField_9() { return &___U3COnDownloadProgressU3Ek__BackingField_9; }
	inline void set_U3COnDownloadProgressU3Ek__BackingField_9(ProgressEvent_t4185145044 * value)
	{
		___U3COnDownloadProgressU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3COnDownloadProgressU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3COnUploadProgressU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3COnUploadProgressU3Ek__BackingField_10)); }
	inline ProgressEvent_t4185145044 * get_U3COnUploadProgressU3Ek__BackingField_10() const { return ___U3COnUploadProgressU3Ek__BackingField_10; }
	inline ProgressEvent_t4185145044 ** get_address_of_U3COnUploadProgressU3Ek__BackingField_10() { return &___U3COnUploadProgressU3Ek__BackingField_10; }
	inline void set_U3COnUploadProgressU3Ek__BackingField_10(ProgressEvent_t4185145044 * value)
	{
		___U3COnUploadProgressU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3COnUploadProgressU3Ek__BackingField_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUEST_T466816980_H
#ifndef FSDATA_T2583805605_H
#define FSDATA_T2583805605_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsData
struct  fsData_t2583805605  : public RuntimeObject
{
public:
	// System.Object FullSerializer.fsData::_value
	RuntimeObject * ____value_0;

public:
	inline static int32_t get_offset_of__value_0() { return static_cast<int32_t>(offsetof(fsData_t2583805605, ____value_0)); }
	inline RuntimeObject * get__value_0() const { return ____value_0; }
	inline RuntimeObject ** get_address_of__value_0() { return &____value_0; }
	inline void set__value_0(RuntimeObject * value)
	{
		____value_0 = value;
		Il2CppCodeGenWriteBarrier((&____value_0), value);
	}
};

struct fsData_t2583805605_StaticFields
{
public:
	// FullSerializer.fsData FullSerializer.fsData::True
	fsData_t2583805605 * ___True_1;
	// FullSerializer.fsData FullSerializer.fsData::False
	fsData_t2583805605 * ___False_2;
	// FullSerializer.fsData FullSerializer.fsData::Null
	fsData_t2583805605 * ___Null_3;

public:
	inline static int32_t get_offset_of_True_1() { return static_cast<int32_t>(offsetof(fsData_t2583805605_StaticFields, ___True_1)); }
	inline fsData_t2583805605 * get_True_1() const { return ___True_1; }
	inline fsData_t2583805605 ** get_address_of_True_1() { return &___True_1; }
	inline void set_True_1(fsData_t2583805605 * value)
	{
		___True_1 = value;
		Il2CppCodeGenWriteBarrier((&___True_1), value);
	}

	inline static int32_t get_offset_of_False_2() { return static_cast<int32_t>(offsetof(fsData_t2583805605_StaticFields, ___False_2)); }
	inline fsData_t2583805605 * get_False_2() const { return ___False_2; }
	inline fsData_t2583805605 ** get_address_of_False_2() { return &___False_2; }
	inline void set_False_2(fsData_t2583805605 * value)
	{
		___False_2 = value;
		Il2CppCodeGenWriteBarrier((&___False_2), value);
	}

	inline static int32_t get_offset_of_Null_3() { return static_cast<int32_t>(offsetof(fsData_t2583805605_StaticFields, ___Null_3)); }
	inline fsData_t2583805605 * get_Null_3() const { return ___Null_3; }
	inline fsData_t2583805605 ** get_address_of_Null_3() { return &___Null_3; }
	inline void set_Null_3(fsData_t2583805605 * value)
	{
		___Null_3 = value;
		Il2CppCodeGenWriteBarrier((&___Null_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSDATA_T2583805605_H
#ifndef CLASSIFYTOPLEVELSINGLE_T3733844951_H
#define CLASSIFYTOPLEVELSINGLE_T3733844951_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyTopLevelSingle
struct  ClassifyTopLevelSingle_t3733844951  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyTopLevelSingle::<source_url>k__BackingField
	String_t* ___U3Csource_urlU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyTopLevelSingle::<resolved_url>k__BackingField
	String_t* ___U3Cresolved_urlU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyTopLevelSingle::<image>k__BackingField
	String_t* ___U3CimageU3Ek__BackingField_2;
	// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ErrorInfoNoCode IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyTopLevelSingle::<error>k__BackingField
	ErrorInfoNoCode_t2062371116 * ___U3CerrorU3Ek__BackingField_3;
	// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyPerClassifier[] IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyTopLevelSingle::<classifiers>k__BackingField
	ClassifyPerClassifierU5BU5D_t1895762155* ___U3CclassifiersU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3Csource_urlU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ClassifyTopLevelSingle_t3733844951, ___U3Csource_urlU3Ek__BackingField_0)); }
	inline String_t* get_U3Csource_urlU3Ek__BackingField_0() const { return ___U3Csource_urlU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3Csource_urlU3Ek__BackingField_0() { return &___U3Csource_urlU3Ek__BackingField_0; }
	inline void set_U3Csource_urlU3Ek__BackingField_0(String_t* value)
	{
		___U3Csource_urlU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Csource_urlU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3Cresolved_urlU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ClassifyTopLevelSingle_t3733844951, ___U3Cresolved_urlU3Ek__BackingField_1)); }
	inline String_t* get_U3Cresolved_urlU3Ek__BackingField_1() const { return ___U3Cresolved_urlU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3Cresolved_urlU3Ek__BackingField_1() { return &___U3Cresolved_urlU3Ek__BackingField_1; }
	inline void set_U3Cresolved_urlU3Ek__BackingField_1(String_t* value)
	{
		___U3Cresolved_urlU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cresolved_urlU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CimageU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ClassifyTopLevelSingle_t3733844951, ___U3CimageU3Ek__BackingField_2)); }
	inline String_t* get_U3CimageU3Ek__BackingField_2() const { return ___U3CimageU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CimageU3Ek__BackingField_2() { return &___U3CimageU3Ek__BackingField_2; }
	inline void set_U3CimageU3Ek__BackingField_2(String_t* value)
	{
		___U3CimageU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CimageU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CerrorU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ClassifyTopLevelSingle_t3733844951, ___U3CerrorU3Ek__BackingField_3)); }
	inline ErrorInfoNoCode_t2062371116 * get_U3CerrorU3Ek__BackingField_3() const { return ___U3CerrorU3Ek__BackingField_3; }
	inline ErrorInfoNoCode_t2062371116 ** get_address_of_U3CerrorU3Ek__BackingField_3() { return &___U3CerrorU3Ek__BackingField_3; }
	inline void set_U3CerrorU3Ek__BackingField_3(ErrorInfoNoCode_t2062371116 * value)
	{
		___U3CerrorU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CerrorU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CclassifiersU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(ClassifyTopLevelSingle_t3733844951, ___U3CclassifiersU3Ek__BackingField_4)); }
	inline ClassifyPerClassifierU5BU5D_t1895762155* get_U3CclassifiersU3Ek__BackingField_4() const { return ___U3CclassifiersU3Ek__BackingField_4; }
	inline ClassifyPerClassifierU5BU5D_t1895762155** get_address_of_U3CclassifiersU3Ek__BackingField_4() { return &___U3CclassifiersU3Ek__BackingField_4; }
	inline void set_U3CclassifiersU3Ek__BackingField_4(ClassifyPerClassifierU5BU5D_t1895762155* value)
	{
		___U3CclassifiersU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CclassifiersU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLASSIFYTOPLEVELSINGLE_T3733844951_H
#ifndef CLASSIFYTOPLEVELMULTIPLE_T317282463_H
#define CLASSIFYTOPLEVELMULTIPLE_T317282463_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyTopLevelMultiple
struct  ClassifyTopLevelMultiple_t317282463  : public RuntimeObject
{
public:
	// System.Int32 IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyTopLevelMultiple::<images_processed>k__BackingField
	int32_t ___U3Cimages_processedU3Ek__BackingField_0;
	// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyTopLevelSingle[] IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyTopLevelMultiple::<images>k__BackingField
	ClassifyTopLevelSingleU5BU5D_t3685983982* ___U3CimagesU3Ek__BackingField_1;
	// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.WarningInfo[] IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyTopLevelMultiple::<warnings>k__BackingField
	WarningInfoU5BU5D_t1932079499* ___U3CwarningsU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3Cimages_processedU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ClassifyTopLevelMultiple_t317282463, ___U3Cimages_processedU3Ek__BackingField_0)); }
	inline int32_t get_U3Cimages_processedU3Ek__BackingField_0() const { return ___U3Cimages_processedU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3Cimages_processedU3Ek__BackingField_0() { return &___U3Cimages_processedU3Ek__BackingField_0; }
	inline void set_U3Cimages_processedU3Ek__BackingField_0(int32_t value)
	{
		___U3Cimages_processedU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CimagesU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ClassifyTopLevelMultiple_t317282463, ___U3CimagesU3Ek__BackingField_1)); }
	inline ClassifyTopLevelSingleU5BU5D_t3685983982* get_U3CimagesU3Ek__BackingField_1() const { return ___U3CimagesU3Ek__BackingField_1; }
	inline ClassifyTopLevelSingleU5BU5D_t3685983982** get_address_of_U3CimagesU3Ek__BackingField_1() { return &___U3CimagesU3Ek__BackingField_1; }
	inline void set_U3CimagesU3Ek__BackingField_1(ClassifyTopLevelSingleU5BU5D_t3685983982* value)
	{
		___U3CimagesU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CimagesU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CwarningsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ClassifyTopLevelMultiple_t317282463, ___U3CwarningsU3Ek__BackingField_2)); }
	inline WarningInfoU5BU5D_t1932079499* get_U3CwarningsU3Ek__BackingField_2() const { return ___U3CwarningsU3Ek__BackingField_2; }
	inline WarningInfoU5BU5D_t1932079499** get_address_of_U3CwarningsU3Ek__BackingField_2() { return &___U3CwarningsU3Ek__BackingField_2; }
	inline void set_U3CwarningsU3Ek__BackingField_2(WarningInfoU5BU5D_t1932079499* value)
	{
		___U3CwarningsU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwarningsU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLASSIFYTOPLEVELMULTIPLE_T317282463_H
#ifndef RESPONSE_T429319368_H
#define RESPONSE_T429319368_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Connection.RESTConnector/Response
struct  Response_t429319368  : public RuntimeObject
{
public:
	// System.Boolean IBM.Watson.DeveloperCloud.Connection.RESTConnector/Response::<Success>k__BackingField
	bool ___U3CSuccessU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Connection.RESTConnector/Response::<Error>k__BackingField
	String_t* ___U3CErrorU3Ek__BackingField_1;
	// System.Byte[] IBM.Watson.DeveloperCloud.Connection.RESTConnector/Response::<Data>k__BackingField
	ByteU5BU5D_t3397334013* ___U3CDataU3Ek__BackingField_2;
	// System.Single IBM.Watson.DeveloperCloud.Connection.RESTConnector/Response::<ElapsedTime>k__BackingField
	float ___U3CElapsedTimeU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CSuccessU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Response_t429319368, ___U3CSuccessU3Ek__BackingField_0)); }
	inline bool get_U3CSuccessU3Ek__BackingField_0() const { return ___U3CSuccessU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CSuccessU3Ek__BackingField_0() { return &___U3CSuccessU3Ek__BackingField_0; }
	inline void set_U3CSuccessU3Ek__BackingField_0(bool value)
	{
		___U3CSuccessU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CErrorU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Response_t429319368, ___U3CErrorU3Ek__BackingField_1)); }
	inline String_t* get_U3CErrorU3Ek__BackingField_1() const { return ___U3CErrorU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CErrorU3Ek__BackingField_1() { return &___U3CErrorU3Ek__BackingField_1; }
	inline void set_U3CErrorU3Ek__BackingField_1(String_t* value)
	{
		___U3CErrorU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CErrorU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Response_t429319368, ___U3CDataU3Ek__BackingField_2)); }
	inline ByteU5BU5D_t3397334013* get_U3CDataU3Ek__BackingField_2() const { return ___U3CDataU3Ek__BackingField_2; }
	inline ByteU5BU5D_t3397334013** get_address_of_U3CDataU3Ek__BackingField_2() { return &___U3CDataU3Ek__BackingField_2; }
	inline void set_U3CDataU3Ek__BackingField_2(ByteU5BU5D_t3397334013* value)
	{
		___U3CDataU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CElapsedTimeU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Response_t429319368, ___U3CElapsedTimeU3Ek__BackingField_3)); }
	inline float get_U3CElapsedTimeU3Ek__BackingField_3() const { return ___U3CElapsedTimeU3Ek__BackingField_3; }
	inline float* get_address_of_U3CElapsedTimeU3Ek__BackingField_3() { return &___U3CElapsedTimeU3Ek__BackingField_3; }
	inline void set_U3CElapsedTimeU3Ek__BackingField_3(float value)
	{
		___U3CElapsedTimeU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESPONSE_T429319368_H
#ifndef CLASS_T4054986160_H
#define CLASS_T4054986160_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.Class
struct  Class_t4054986160  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.Class::<m_Class>k__BackingField
	String_t* ___U3Cm_ClassU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3Cm_ClassU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Class_t4054986160, ___U3Cm_ClassU3Ek__BackingField_0)); }
	inline String_t* get_U3Cm_ClassU3Ek__BackingField_0() const { return ___U3Cm_ClassU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3Cm_ClassU3Ek__BackingField_0() { return &___U3Cm_ClassU3Ek__BackingField_0; }
	inline void set_U3Cm_ClassU3Ek__BackingField_0(String_t* value)
	{
		___U3Cm_ClassU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cm_ClassU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLASS_T4054986160_H
#ifndef CLASSIFYPARAMETERS_T933043320_H
#define CLASSIFYPARAMETERS_T933043320_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyParameters
struct  ClassifyParameters_t933043320  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyParameters::<url>k__BackingField
	String_t* ___U3CurlU3Ek__BackingField_0;
	// System.String[] IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyParameters::<classifier_ids>k__BackingField
	StringU5BU5D_t1642385972* ___U3Cclassifier_idsU3Ek__BackingField_1;
	// System.String[] IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyParameters::<owners>k__BackingField
	StringU5BU5D_t1642385972* ___U3CownersU3Ek__BackingField_2;
	// System.Single IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyParameters::<threshold>k__BackingField
	float ___U3CthresholdU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CurlU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ClassifyParameters_t933043320, ___U3CurlU3Ek__BackingField_0)); }
	inline String_t* get_U3CurlU3Ek__BackingField_0() const { return ___U3CurlU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CurlU3Ek__BackingField_0() { return &___U3CurlU3Ek__BackingField_0; }
	inline void set_U3CurlU3Ek__BackingField_0(String_t* value)
	{
		___U3CurlU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CurlU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3Cclassifier_idsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ClassifyParameters_t933043320, ___U3Cclassifier_idsU3Ek__BackingField_1)); }
	inline StringU5BU5D_t1642385972* get_U3Cclassifier_idsU3Ek__BackingField_1() const { return ___U3Cclassifier_idsU3Ek__BackingField_1; }
	inline StringU5BU5D_t1642385972** get_address_of_U3Cclassifier_idsU3Ek__BackingField_1() { return &___U3Cclassifier_idsU3Ek__BackingField_1; }
	inline void set_U3Cclassifier_idsU3Ek__BackingField_1(StringU5BU5D_t1642385972* value)
	{
		___U3Cclassifier_idsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cclassifier_idsU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CownersU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ClassifyParameters_t933043320, ___U3CownersU3Ek__BackingField_2)); }
	inline StringU5BU5D_t1642385972* get_U3CownersU3Ek__BackingField_2() const { return ___U3CownersU3Ek__BackingField_2; }
	inline StringU5BU5D_t1642385972** get_address_of_U3CownersU3Ek__BackingField_2() { return &___U3CownersU3Ek__BackingField_2; }
	inline void set_U3CownersU3Ek__BackingField_2(StringU5BU5D_t1642385972* value)
	{
		___U3CownersU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CownersU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CthresholdU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ClassifyParameters_t933043320, ___U3CthresholdU3Ek__BackingField_3)); }
	inline float get_U3CthresholdU3Ek__BackingField_3() const { return ___U3CthresholdU3Ek__BackingField_3; }
	inline float* get_address_of_U3CthresholdU3Ek__BackingField_3() { return &___U3CthresholdU3Ek__BackingField_3; }
	inline void set_U3CthresholdU3Ek__BackingField_3(float value)
	{
		___U3CthresholdU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLASSIFYPARAMETERS_T933043320_H
#ifndef RESTCONNECTOR_T3705102247_H
#define RESTCONNECTOR_T3705102247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Connection.RESTConnector
struct  RESTConnector_t3705102247  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Connection.RESTConnector::<URL>k__BackingField
	String_t* ___U3CURLU3Ek__BackingField_1;
	// IBM.Watson.DeveloperCloud.Utilities.Credentials IBM.Watson.DeveloperCloud.Connection.RESTConnector::<Authentication>k__BackingField
	Credentials_t1494051450 * ___U3CAuthenticationU3Ek__BackingField_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> IBM.Watson.DeveloperCloud.Connection.RESTConnector::<Headers>k__BackingField
	Dictionary_2_t3943999495 * ___U3CHeadersU3Ek__BackingField_3;
	// System.Int32 IBM.Watson.DeveloperCloud.Connection.RESTConnector::m_ActiveConnections
	int32_t ___m_ActiveConnections_5;
	// System.Collections.Generic.Queue`1<IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request> IBM.Watson.DeveloperCloud.Connection.RESTConnector::m_Requests
	Queue_1_t286473815 * ___m_Requests_6;

public:
	inline static int32_t get_offset_of_U3CURLU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RESTConnector_t3705102247, ___U3CURLU3Ek__BackingField_1)); }
	inline String_t* get_U3CURLU3Ek__BackingField_1() const { return ___U3CURLU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CURLU3Ek__BackingField_1() { return &___U3CURLU3Ek__BackingField_1; }
	inline void set_U3CURLU3Ek__BackingField_1(String_t* value)
	{
		___U3CURLU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CURLU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CAuthenticationU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(RESTConnector_t3705102247, ___U3CAuthenticationU3Ek__BackingField_2)); }
	inline Credentials_t1494051450 * get_U3CAuthenticationU3Ek__BackingField_2() const { return ___U3CAuthenticationU3Ek__BackingField_2; }
	inline Credentials_t1494051450 ** get_address_of_U3CAuthenticationU3Ek__BackingField_2() { return &___U3CAuthenticationU3Ek__BackingField_2; }
	inline void set_U3CAuthenticationU3Ek__BackingField_2(Credentials_t1494051450 * value)
	{
		___U3CAuthenticationU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAuthenticationU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CHeadersU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(RESTConnector_t3705102247, ___U3CHeadersU3Ek__BackingField_3)); }
	inline Dictionary_2_t3943999495 * get_U3CHeadersU3Ek__BackingField_3() const { return ___U3CHeadersU3Ek__BackingField_3; }
	inline Dictionary_2_t3943999495 ** get_address_of_U3CHeadersU3Ek__BackingField_3() { return &___U3CHeadersU3Ek__BackingField_3; }
	inline void set_U3CHeadersU3Ek__BackingField_3(Dictionary_2_t3943999495 * value)
	{
		___U3CHeadersU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CHeadersU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_m_ActiveConnections_5() { return static_cast<int32_t>(offsetof(RESTConnector_t3705102247, ___m_ActiveConnections_5)); }
	inline int32_t get_m_ActiveConnections_5() const { return ___m_ActiveConnections_5; }
	inline int32_t* get_address_of_m_ActiveConnections_5() { return &___m_ActiveConnections_5; }
	inline void set_m_ActiveConnections_5(int32_t value)
	{
		___m_ActiveConnections_5 = value;
	}

	inline static int32_t get_offset_of_m_Requests_6() { return static_cast<int32_t>(offsetof(RESTConnector_t3705102247, ___m_Requests_6)); }
	inline Queue_1_t286473815 * get_m_Requests_6() const { return ___m_Requests_6; }
	inline Queue_1_t286473815 ** get_address_of_m_Requests_6() { return &___m_Requests_6; }
	inline void set_m_Requests_6(Queue_1_t286473815 * value)
	{
		___m_Requests_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Requests_6), value);
	}
};

struct RESTConnector_t3705102247_StaticFields
{
public:
	// System.Single IBM.Watson.DeveloperCloud.Connection.RESTConnector::sm_LogResponseTime
	float ___sm_LogResponseTime_0;
	// System.Collections.Generic.Dictionary`2<System.String,IBM.Watson.DeveloperCloud.Connection.RESTConnector> IBM.Watson.DeveloperCloud.Connection.RESTConnector::sm_Connectors
	Dictionary_2_t1324914213 * ___sm_Connectors_4;

public:
	inline static int32_t get_offset_of_sm_LogResponseTime_0() { return static_cast<int32_t>(offsetof(RESTConnector_t3705102247_StaticFields, ___sm_LogResponseTime_0)); }
	inline float get_sm_LogResponseTime_0() const { return ___sm_LogResponseTime_0; }
	inline float* get_address_of_sm_LogResponseTime_0() { return &___sm_LogResponseTime_0; }
	inline void set_sm_LogResponseTime_0(float value)
	{
		___sm_LogResponseTime_0 = value;
	}

	inline static int32_t get_offset_of_sm_Connectors_4() { return static_cast<int32_t>(offsetof(RESTConnector_t3705102247_StaticFields, ___sm_Connectors_4)); }
	inline Dictionary_2_t1324914213 * get_sm_Connectors_4() const { return ___sm_Connectors_4; }
	inline Dictionary_2_t1324914213 ** get_address_of_sm_Connectors_4() { return &___sm_Connectors_4; }
	inline void set_sm_Connectors_4(Dictionary_2_t1324914213 * value)
	{
		___sm_Connectors_4 = value;
		Il2CppCodeGenWriteBarrier((&___sm_Connectors_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESTCONNECTOR_T3705102247_H
#ifndef CLASSIFYPERCLASSIFIER_T2357796318_H
#define CLASSIFYPERCLASSIFIER_T2357796318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyPerClassifier
struct  ClassifyPerClassifier_t2357796318  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyPerClassifier::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyPerClassifier::<classifier_id>k__BackingField
	String_t* ___U3Cclassifier_idU3Ek__BackingField_1;
	// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassResult[] IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyPerClassifier::<classes>k__BackingField
	ClassResultU5BU5D_t2895066752* ___U3CclassesU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ClassifyPerClassifier_t2357796318, ___U3CnameU3Ek__BackingField_0)); }
	inline String_t* get_U3CnameU3Ek__BackingField_0() const { return ___U3CnameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_0() { return &___U3CnameU3Ek__BackingField_0; }
	inline void set_U3CnameU3Ek__BackingField_0(String_t* value)
	{
		___U3CnameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3Cclassifier_idU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ClassifyPerClassifier_t2357796318, ___U3Cclassifier_idU3Ek__BackingField_1)); }
	inline String_t* get_U3Cclassifier_idU3Ek__BackingField_1() const { return ___U3Cclassifier_idU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3Cclassifier_idU3Ek__BackingField_1() { return &___U3Cclassifier_idU3Ek__BackingField_1; }
	inline void set_U3Cclassifier_idU3Ek__BackingField_1(String_t* value)
	{
		___U3Cclassifier_idU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cclassifier_idU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CclassesU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ClassifyPerClassifier_t2357796318, ___U3CclassesU3Ek__BackingField_2)); }
	inline ClassResultU5BU5D_t2895066752* get_U3CclassesU3Ek__BackingField_2() const { return ___U3CclassesU3Ek__BackingField_2; }
	inline ClassResultU5BU5D_t2895066752** get_address_of_U3CclassesU3Ek__BackingField_2() { return &___U3CclassesU3Ek__BackingField_2; }
	inline void set_U3CclassesU3Ek__BackingField_2(ClassResultU5BU5D_t2895066752* value)
	{
		___U3CclassesU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CclassesU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLASSIFYPERCLASSIFIER_T2357796318_H
#ifndef PRONUNCIATION_T2711344207_H
#define PRONUNCIATION_T2711344207_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Pronunciation
struct  Pronunciation_t2711344207  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Pronunciation::<pronunciation>k__BackingField
	String_t* ___U3CpronunciationU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CpronunciationU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Pronunciation_t2711344207, ___U3CpronunciationU3Ek__BackingField_0)); }
	inline String_t* get_U3CpronunciationU3Ek__BackingField_0() const { return ___U3CpronunciationU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CpronunciationU3Ek__BackingField_0() { return &___U3CpronunciationU3Ek__BackingField_0; }
	inline void set_U3CpronunciationU3Ek__BackingField_0(String_t* value)
	{
		___U3CpronunciationU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpronunciationU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRONUNCIATION_T2711344207_H
#ifndef ERRORINFONOCODE_T2062371116_H
#define ERRORINFONOCODE_T2062371116_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ErrorInfoNoCode
struct  ErrorInfoNoCode_t2062371116  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ErrorInfoNoCode::<error_id>k__BackingField
	String_t* ___U3Cerror_idU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ErrorInfoNoCode::<description>k__BackingField
	String_t* ___U3CdescriptionU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3Cerror_idU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ErrorInfoNoCode_t2062371116, ___U3Cerror_idU3Ek__BackingField_0)); }
	inline String_t* get_U3Cerror_idU3Ek__BackingField_0() const { return ___U3Cerror_idU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3Cerror_idU3Ek__BackingField_0() { return &___U3Cerror_idU3Ek__BackingField_0; }
	inline void set_U3Cerror_idU3Ek__BackingField_0(String_t* value)
	{
		___U3Cerror_idU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cerror_idU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CdescriptionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ErrorInfoNoCode_t2062371116, ___U3CdescriptionU3Ek__BackingField_1)); }
	inline String_t* get_U3CdescriptionU3Ek__BackingField_1() const { return ___U3CdescriptionU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CdescriptionU3Ek__BackingField_1() { return &___U3CdescriptionU3Ek__BackingField_1; }
	inline void set_U3CdescriptionU3Ek__BackingField_1(String_t* value)
	{
		___U3CdescriptionU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdescriptionU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERRORINFONOCODE_T2062371116_H
#ifndef ERRORMODEL_T3210737207_H
#define ERRORMODEL_T3210737207_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.ErrorModel
struct  ErrorModel_t3210737207  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.ErrorModel::<error>k__BackingField
	String_t* ___U3CerrorU3Ek__BackingField_0;
	// System.Int32 IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.ErrorModel::<code>k__BackingField
	int32_t ___U3CcodeU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.ErrorModel::<code_description>k__BackingField
	String_t* ___U3Ccode_descriptionU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CerrorU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ErrorModel_t3210737207, ___U3CerrorU3Ek__BackingField_0)); }
	inline String_t* get_U3CerrorU3Ek__BackingField_0() const { return ___U3CerrorU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CerrorU3Ek__BackingField_0() { return &___U3CerrorU3Ek__BackingField_0; }
	inline void set_U3CerrorU3Ek__BackingField_0(String_t* value)
	{
		___U3CerrorU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CerrorU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CcodeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ErrorModel_t3210737207, ___U3CcodeU3Ek__BackingField_1)); }
	inline int32_t get_U3CcodeU3Ek__BackingField_1() const { return ___U3CcodeU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CcodeU3Ek__BackingField_1() { return &___U3CcodeU3Ek__BackingField_1; }
	inline void set_U3CcodeU3Ek__BackingField_1(int32_t value)
	{
		___U3CcodeU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3Ccode_descriptionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ErrorModel_t3210737207, ___U3Ccode_descriptionU3Ek__BackingField_2)); }
	inline String_t* get_U3Ccode_descriptionU3Ek__BackingField_2() const { return ___U3Ccode_descriptionU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3Ccode_descriptionU3Ek__BackingField_2() { return &___U3Ccode_descriptionU3Ek__BackingField_2; }
	inline void set_U3Ccode_descriptionU3Ek__BackingField_2(String_t* value)
	{
		___U3Ccode_descriptionU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3Ccode_descriptionU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERRORMODEL_T3210737207_H
#ifndef COLLECTIONIMAGESCONFIG_T1273369062_H
#define COLLECTIONIMAGESCONFIG_T1273369062_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.CollectionImagesConfig
struct  CollectionImagesConfig_t1273369062  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.CollectionImagesConfig::<image_id>k__BackingField
	String_t* ___U3Cimage_idU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.CollectionImagesConfig::<created>k__BackingField
	String_t* ___U3CcreatedU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.CollectionImagesConfig::<image_file>k__BackingField
	String_t* ___U3Cimage_fileU3Ek__BackingField_2;
	// System.Object IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.CollectionImagesConfig::<metadata>k__BackingField
	RuntimeObject * ___U3CmetadataU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3Cimage_idU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CollectionImagesConfig_t1273369062, ___U3Cimage_idU3Ek__BackingField_0)); }
	inline String_t* get_U3Cimage_idU3Ek__BackingField_0() const { return ___U3Cimage_idU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3Cimage_idU3Ek__BackingField_0() { return &___U3Cimage_idU3Ek__BackingField_0; }
	inline void set_U3Cimage_idU3Ek__BackingField_0(String_t* value)
	{
		___U3Cimage_idU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cimage_idU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CcreatedU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CollectionImagesConfig_t1273369062, ___U3CcreatedU3Ek__BackingField_1)); }
	inline String_t* get_U3CcreatedU3Ek__BackingField_1() const { return ___U3CcreatedU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CcreatedU3Ek__BackingField_1() { return &___U3CcreatedU3Ek__BackingField_1; }
	inline void set_U3CcreatedU3Ek__BackingField_1(String_t* value)
	{
		___U3CcreatedU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcreatedU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3Cimage_fileU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CollectionImagesConfig_t1273369062, ___U3Cimage_fileU3Ek__BackingField_2)); }
	inline String_t* get_U3Cimage_fileU3Ek__BackingField_2() const { return ___U3Cimage_fileU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3Cimage_fileU3Ek__BackingField_2() { return &___U3Cimage_fileU3Ek__BackingField_2; }
	inline void set_U3Cimage_fileU3Ek__BackingField_2(String_t* value)
	{
		___U3Cimage_fileU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cimage_fileU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CmetadataU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(CollectionImagesConfig_t1273369062, ___U3CmetadataU3Ek__BackingField_3)); }
	inline RuntimeObject * get_U3CmetadataU3Ek__BackingField_3() const { return ___U3CmetadataU3Ek__BackingField_3; }
	inline RuntimeObject ** get_address_of_U3CmetadataU3Ek__BackingField_3() { return &___U3CmetadataU3Ek__BackingField_3; }
	inline void set_U3CmetadataU3Ek__BackingField_3(RuntimeObject * value)
	{
		___U3CmetadataU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmetadataU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTIONIMAGESCONFIG_T1273369062_H
#ifndef WARNINGINFO_T624028094_H
#define WARNINGINFO_T624028094_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.WarningInfo
struct  WarningInfo_t624028094  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.WarningInfo::<warning_id>k__BackingField
	String_t* ___U3Cwarning_idU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.WarningInfo::<description>k__BackingField
	String_t* ___U3CdescriptionU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3Cwarning_idU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(WarningInfo_t624028094, ___U3Cwarning_idU3Ek__BackingField_0)); }
	inline String_t* get_U3Cwarning_idU3Ek__BackingField_0() const { return ___U3Cwarning_idU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3Cwarning_idU3Ek__BackingField_0() { return &___U3Cwarning_idU3Ek__BackingField_0; }
	inline void set_U3Cwarning_idU3Ek__BackingField_0(String_t* value)
	{
		___U3Cwarning_idU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cwarning_idU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CdescriptionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(WarningInfo_t624028094, ___U3CdescriptionU3Ek__BackingField_1)); }
	inline String_t* get_U3CdescriptionU3Ek__BackingField_1() const { return ___U3CdescriptionU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CdescriptionU3Ek__BackingField_1() { return &___U3CdescriptionU3Ek__BackingField_1; }
	inline void set_U3CdescriptionU3Ek__BackingField_1(String_t* value)
	{
		___U3CdescriptionU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdescriptionU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WARNINGINFO_T624028094_H
#ifndef DICTIONARY_2_T2576372715_H
#define DICTIONARY_2_T2576372715_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.AudioFormatType,System.String>
struct  Dictionary_2_t2576372715  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t3030399641* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t62501539* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	AudioFormatTypeU5BU5D_t274759192* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	StringU5BU5D_t1642385972* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t228987430 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t2576372715, ___table_4)); }
	inline Int32U5BU5D_t3030399641* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t3030399641** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t3030399641* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t2576372715, ___linkSlots_5)); }
	inline LinkU5BU5D_t62501539* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t62501539** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t62501539* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t2576372715, ___keySlots_6)); }
	inline AudioFormatTypeU5BU5D_t274759192* get_keySlots_6() const { return ___keySlots_6; }
	inline AudioFormatTypeU5BU5D_t274759192** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(AudioFormatTypeU5BU5D_t274759192* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t2576372715, ___valueSlots_7)); }
	inline StringU5BU5D_t1642385972* get_valueSlots_7() const { return ___valueSlots_7; }
	inline StringU5BU5D_t1642385972** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(StringU5BU5D_t1642385972* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t2576372715, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t2576372715, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t2576372715, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t2576372715, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t2576372715, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t2576372715, ___serialization_info_13)); }
	inline SerializationInfo_t228987430 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t228987430 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t228987430 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t2576372715, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t2576372715_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t2162557148 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t2576372715_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t2162557148 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t2162557148 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t2162557148 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T2576372715_H
#ifndef DATACACHE_T4250340070_H
#define DATACACHE_T4250340070_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Utilities.DataCache
struct  DataCache_t4250340070  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Utilities.DataCache::m_CachePath
	String_t* ___m_CachePath_0;
	// System.Int64 IBM.Watson.DeveloperCloud.Utilities.DataCache::m_MaxCacheSize
	int64_t ___m_MaxCacheSize_1;
	// System.Double IBM.Watson.DeveloperCloud.Utilities.DataCache::m_MaxCacheAge
	double ___m_MaxCacheAge_2;
	// System.Int64 IBM.Watson.DeveloperCloud.Utilities.DataCache::m_CurrentCacheSize
	int64_t ___m_CurrentCacheSize_3;
	// System.Collections.Generic.Dictionary`2<System.String,IBM.Watson.DeveloperCloud.Utilities.DataCache/CacheItem> IBM.Watson.DeveloperCloud.Utilities.DataCache::m_Cache
	Dictionary_2_t92195697 * ___m_Cache_4;

public:
	inline static int32_t get_offset_of_m_CachePath_0() { return static_cast<int32_t>(offsetof(DataCache_t4250340070, ___m_CachePath_0)); }
	inline String_t* get_m_CachePath_0() const { return ___m_CachePath_0; }
	inline String_t** get_address_of_m_CachePath_0() { return &___m_CachePath_0; }
	inline void set_m_CachePath_0(String_t* value)
	{
		___m_CachePath_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_CachePath_0), value);
	}

	inline static int32_t get_offset_of_m_MaxCacheSize_1() { return static_cast<int32_t>(offsetof(DataCache_t4250340070, ___m_MaxCacheSize_1)); }
	inline int64_t get_m_MaxCacheSize_1() const { return ___m_MaxCacheSize_1; }
	inline int64_t* get_address_of_m_MaxCacheSize_1() { return &___m_MaxCacheSize_1; }
	inline void set_m_MaxCacheSize_1(int64_t value)
	{
		___m_MaxCacheSize_1 = value;
	}

	inline static int32_t get_offset_of_m_MaxCacheAge_2() { return static_cast<int32_t>(offsetof(DataCache_t4250340070, ___m_MaxCacheAge_2)); }
	inline double get_m_MaxCacheAge_2() const { return ___m_MaxCacheAge_2; }
	inline double* get_address_of_m_MaxCacheAge_2() { return &___m_MaxCacheAge_2; }
	inline void set_m_MaxCacheAge_2(double value)
	{
		___m_MaxCacheAge_2 = value;
	}

	inline static int32_t get_offset_of_m_CurrentCacheSize_3() { return static_cast<int32_t>(offsetof(DataCache_t4250340070, ___m_CurrentCacheSize_3)); }
	inline int64_t get_m_CurrentCacheSize_3() const { return ___m_CurrentCacheSize_3; }
	inline int64_t* get_address_of_m_CurrentCacheSize_3() { return &___m_CurrentCacheSize_3; }
	inline void set_m_CurrentCacheSize_3(int64_t value)
	{
		___m_CurrentCacheSize_3 = value;
	}

	inline static int32_t get_offset_of_m_Cache_4() { return static_cast<int32_t>(offsetof(DataCache_t4250340070, ___m_Cache_4)); }
	inline Dictionary_2_t92195697 * get_m_Cache_4() const { return ___m_Cache_4; }
	inline Dictionary_2_t92195697 ** get_address_of_m_Cache_4() { return &___m_Cache_4; }
	inline void set_m_Cache_4(Dictionary_2_t92195697 * value)
	{
		___m_Cache_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Cache_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATACACHE_T4250340070_H
#ifndef DICTIONARY_2_T1569883280_H
#define DICTIONARY_2_T1569883280_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.VoiceType,System.String>
struct  Dictionary_2_t1569883280  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t3030399641* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t62501539* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	VoiceTypeU5BU5D_t3563237053* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	StringU5BU5D_t1642385972* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t228987430 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t1569883280, ___table_4)); }
	inline Int32U5BU5D_t3030399641* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t3030399641** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t3030399641* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t1569883280, ___linkSlots_5)); }
	inline LinkU5BU5D_t62501539* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t62501539** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t62501539* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t1569883280, ___keySlots_6)); }
	inline VoiceTypeU5BU5D_t3563237053* get_keySlots_6() const { return ___keySlots_6; }
	inline VoiceTypeU5BU5D_t3563237053** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(VoiceTypeU5BU5D_t3563237053* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t1569883280, ___valueSlots_7)); }
	inline StringU5BU5D_t1642385972* get_valueSlots_7() const { return ___valueSlots_7; }
	inline StringU5BU5D_t1642385972** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(StringU5BU5D_t1642385972* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t1569883280, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t1569883280, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t1569883280, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t1569883280, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t1569883280, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t1569883280, ___serialization_info_13)); }
	inline SerializationInfo_t228987430 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t228987430 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t228987430 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t1569883280, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t1569883280_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t514852867 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t1569883280_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t514852867 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t514852867 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t514852867 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T1569883280_H
#ifndef PINGDATA_T2961942429_H
#define PINGDATA_T2961942429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics/PingData
struct  PingData_t2961942429  : public ApplicationData_t1885408412
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PINGDATA_T2961942429_H
#ifndef CATEGORICALRANGE_T943840333_H
#define CATEGORICALRANGE_T943840333_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.CategoricalRange
struct  CategoricalRange_t943840333  : public Range_t2396059803
{
public:
	// System.String[] IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.CategoricalRange::<keys>k__BackingField
	StringU5BU5D_t1642385972* ___U3CkeysU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CkeysU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CategoricalRange_t943840333, ___U3CkeysU3Ek__BackingField_0)); }
	inline StringU5BU5D_t1642385972* get_U3CkeysU3Ek__BackingField_0() const { return ___U3CkeysU3Ek__BackingField_0; }
	inline StringU5BU5D_t1642385972** get_address_of_U3CkeysU3Ek__BackingField_0() { return &___U3CkeysU3Ek__BackingField_0; }
	inline void set_U3CkeysU3Ek__BackingField_0(StringU5BU5D_t1642385972* value)
	{
		___U3CkeysU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CkeysU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CATEGORICALRANGE_T943840333_H
#ifndef GETDILEMMAREQUEST_T37205599_H
#define GETDILEMMAREQUEST_T37205599_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics/GetDilemmaRequest
struct  GetDilemmaRequest_t37205599  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics/OnDilemma IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics/GetDilemmaRequest::<Callback>k__BackingField
	OnDilemma_t401820821 * ___U3CCallbackU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetDilemmaRequest_t37205599, ___U3CCallbackU3Ek__BackingField_11)); }
	inline OnDilemma_t401820821 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline OnDilemma_t401820821 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(OnDilemma_t401820821 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETDILEMMAREQUEST_T37205599_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t1328083999* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t1328083999* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t1328083999** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t1328083999* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef VALUERANGE_T543481982_H
#define VALUERANGE_T543481982_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.ValueRange
struct  ValueRange_t543481982  : public Range_t2396059803
{
public:
	// System.Double IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.ValueRange::<low>k__BackingField
	double ___U3ClowU3Ek__BackingField_0;
	// System.Double IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.ValueRange::<high>k__BackingField
	double ___U3ChighU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3ClowU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ValueRange_t543481982, ___U3ClowU3Ek__BackingField_0)); }
	inline double get_U3ClowU3Ek__BackingField_0() const { return ___U3ClowU3Ek__BackingField_0; }
	inline double* get_address_of_U3ClowU3Ek__BackingField_0() { return &___U3ClowU3Ek__BackingField_0; }
	inline void set_U3ClowU3Ek__BackingField_0(double value)
	{
		___U3ClowU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3ChighU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ValueRange_t543481982, ___U3ChighU3Ek__BackingField_1)); }
	inline double get_U3ChighU3Ek__BackingField_1() const { return ___U3ChighU3Ek__BackingField_1; }
	inline double* get_address_of_U3ChighU3Ek__BackingField_1() { return &___U3ChighU3Ek__BackingField_1; }
	inline void set_U3ChighU3Ek__BackingField_1(double value)
	{
		___U3ChighU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALUERANGE_T543481982_H
#ifndef GETTONEANALYZERREQUEST_T1769916618_H
#define GETTONEANALYZERREQUEST_T1769916618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzer/GetToneAnalyzerRequest
struct  GetToneAnalyzerRequest_t1769916618  : public Request_t466816980
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzer/GetToneAnalyzerRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_11;
	// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzer/OnGetToneAnalyzed IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzer/GetToneAnalyzerRequest::<Callback>k__BackingField
	OnGetToneAnalyzed_t3813655652 * ___U3CCallbackU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetToneAnalyzerRequest_t1769916618, ___U3CDataU3Ek__BackingField_11)); }
	inline String_t* get_U3CDataU3Ek__BackingField_11() const { return ___U3CDataU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_11() { return &___U3CDataU3Ek__BackingField_11; }
	inline void set_U3CDataU3Ek__BackingField_11(String_t* value)
	{
		___U3CDataU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GetToneAnalyzerRequest_t1769916618, ___U3CCallbackU3Ek__BackingField_12)); }
	inline OnGetToneAnalyzed_t3813655652 * get_U3CCallbackU3Ek__BackingField_12() const { return ___U3CCallbackU3Ek__BackingField_12; }
	inline OnGetToneAnalyzed_t3813655652 ** get_address_of_U3CCallbackU3Ek__BackingField_12() { return &___U3CCallbackU3Ek__BackingField_12; }
	inline void set_U3CCallbackU3Ek__BackingField_12(OnGetToneAnalyzed_t3813655652 * value)
	{
		___U3CCallbackU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETTONEANALYZERREQUEST_T1769916618_H
#ifndef PINGDATAVALUE_T526342856_H
#define PINGDATAVALUE_T526342856_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics/PingDataValue
struct  PingDataValue_t526342856  : public ApplicationDataValue_t1721671971
{
public:
	// System.Double IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics/PingDataValue::<ping>k__BackingField
	double ___U3CpingU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CpingU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(PingDataValue_t526342856, ___U3CpingU3Ek__BackingField_0)); }
	inline double get_U3CpingU3Ek__BackingField_0() const { return ___U3CpingU3Ek__BackingField_0; }
	inline double* get_address_of_U3CpingU3Ek__BackingField_0() { return &___U3CpingU3Ek__BackingField_0; }
	inline void set_U3CpingU3Ek__BackingField_0(double value)
	{
		___U3CpingU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PINGDATAVALUE_T526342856_H
#ifndef DATERANGE_T3049499159_H
#define DATERANGE_T3049499159_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.DateRange
struct  DateRange_t3049499159  : public Range_t2396059803
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.DateRange::<low>k__BackingField
	String_t* ___U3ClowU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.DateRange::<high>k__BackingField
	String_t* ___U3ChighU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3ClowU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DateRange_t3049499159, ___U3ClowU3Ek__BackingField_0)); }
	inline String_t* get_U3ClowU3Ek__BackingField_0() const { return ___U3ClowU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3ClowU3Ek__BackingField_0() { return &___U3ClowU3Ek__BackingField_0; }
	inline void set_U3ClowU3Ek__BackingField_0(String_t* value)
	{
		___U3ClowU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClowU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3ChighU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(DateRange_t3049499159, ___U3ChighU3Ek__BackingField_1)); }
	inline String_t* get_U3ChighU3Ek__BackingField_1() const { return ___U3ChighU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3ChighU3Ek__BackingField_1() { return &___U3ChighU3Ek__BackingField_1; }
	inline void set_U3ChighU3Ek__BackingField_1(String_t* value)
	{
		___U3ChighU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3ChighU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATERANGE_T3049499159_H
#ifndef DELETECUSTOMIZATIONREQUEST_T4044506578_H
#define DELETECUSTOMIZATIONREQUEST_T4044506578_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/DeleteCustomizationRequest
struct  DeleteCustomizationRequest_t4044506578  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/OnDeleteCustomizationCallback IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/DeleteCustomizationRequest::<Callback>k__BackingField
	OnDeleteCustomizationCallback_t2862971265 * ___U3CCallbackU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/DeleteCustomizationRequest::<CustomizationID>k__BackingField
	String_t* ___U3CCustomizationIDU3Ek__BackingField_12;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/DeleteCustomizationRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(DeleteCustomizationRequest_t4044506578, ___U3CCallbackU3Ek__BackingField_11)); }
	inline OnDeleteCustomizationCallback_t2862971265 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline OnDeleteCustomizationCallback_t2862971265 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(OnDeleteCustomizationCallback_t2862971265 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCustomizationIDU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(DeleteCustomizationRequest_t4044506578, ___U3CCustomizationIDU3Ek__BackingField_12)); }
	inline String_t* get_U3CCustomizationIDU3Ek__BackingField_12() const { return ___U3CCustomizationIDU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CCustomizationIDU3Ek__BackingField_12() { return &___U3CCustomizationIDU3Ek__BackingField_12; }
	inline void set_U3CCustomizationIDU3Ek__BackingField_12(String_t* value)
	{
		___U3CCustomizationIDU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCustomizationIDU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(DeleteCustomizationRequest_t4044506578, ___U3CDataU3Ek__BackingField_13)); }
	inline String_t* get_U3CDataU3Ek__BackingField_13() const { return ___U3CDataU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_13() { return &___U3CDataU3Ek__BackingField_13; }
	inline void set_U3CDataU3Ek__BackingField_13(String_t* value)
	{
		___U3CDataU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELETECUSTOMIZATIONREQUEST_T4044506578_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	IntPtr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline IntPtr_t get_Zero_1() const { return ___Zero_1; }
	inline IntPtr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(IntPtr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef CREATECUSTOMIZATIONREQUEST_T295745401_H
#define CREATECUSTOMIZATIONREQUEST_T295745401_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/CreateCustomizationRequest
struct  CreateCustomizationRequest_t295745401  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/CreateCustomizationCallback IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/CreateCustomizationRequest::<Callback>k__BackingField
	CreateCustomizationCallback_t3071584865 * ___U3CCallbackU3Ek__BackingField_11;
	// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomVoice IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/CreateCustomizationRequest::<CustomVoice>k__BackingField
	CustomVoice_t330695553 * ___U3CCustomVoiceU3Ek__BackingField_12;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/CreateCustomizationRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(CreateCustomizationRequest_t295745401, ___U3CCallbackU3Ek__BackingField_11)); }
	inline CreateCustomizationCallback_t3071584865 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline CreateCustomizationCallback_t3071584865 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(CreateCustomizationCallback_t3071584865 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCustomVoiceU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(CreateCustomizationRequest_t295745401, ___U3CCustomVoiceU3Ek__BackingField_12)); }
	inline CustomVoice_t330695553 * get_U3CCustomVoiceU3Ek__BackingField_12() const { return ___U3CCustomVoiceU3Ek__BackingField_12; }
	inline CustomVoice_t330695553 ** get_address_of_U3CCustomVoiceU3Ek__BackingField_12() { return &___U3CCustomVoiceU3Ek__BackingField_12; }
	inline void set_U3CCustomVoiceU3Ek__BackingField_12(CustomVoice_t330695553 * value)
	{
		___U3CCustomVoiceU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCustomVoiceU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(CreateCustomizationRequest_t295745401, ___U3CDataU3Ek__BackingField_13)); }
	inline String_t* get_U3CDataU3Ek__BackingField_13() const { return ___U3CDataU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_13() { return &___U3CDataU3Ek__BackingField_13; }
	inline void set_U3CDataU3Ek__BackingField_13(String_t* value)
	{
		___U3CDataU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREATECUSTOMIZATIONREQUEST_T295745401_H
#ifndef SINGLE_T2076509932_H
#define SINGLE_T2076509932_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t2076509932 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t2076509932, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T2076509932_H
#ifndef UPDATECUSTOMIZATIONREQUEST_T1669343124_H
#define UPDATECUSTOMIZATIONREQUEST_T1669343124_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/UpdateCustomizationRequest
struct  UpdateCustomizationRequest_t1669343124  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/UpdateCustomizationCallback IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/UpdateCustomizationRequest::<Callback>k__BackingField
	UpdateCustomizationCallback_t3026233620 * ___U3CCallbackU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/UpdateCustomizationRequest::<CustomizationID>k__BackingField
	String_t* ___U3CCustomizationIDU3Ek__BackingField_12;
	// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomVoiceUpdate IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/UpdateCustomizationRequest::<CustomVoiceUpdate>k__BackingField
	CustomVoiceUpdate_t1706248840 * ___U3CCustomVoiceUpdateU3Ek__BackingField_13;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/UpdateCustomizationRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(UpdateCustomizationRequest_t1669343124, ___U3CCallbackU3Ek__BackingField_11)); }
	inline UpdateCustomizationCallback_t3026233620 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline UpdateCustomizationCallback_t3026233620 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(UpdateCustomizationCallback_t3026233620 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCustomizationIDU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(UpdateCustomizationRequest_t1669343124, ___U3CCustomizationIDU3Ek__BackingField_12)); }
	inline String_t* get_U3CCustomizationIDU3Ek__BackingField_12() const { return ___U3CCustomizationIDU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CCustomizationIDU3Ek__BackingField_12() { return &___U3CCustomizationIDU3Ek__BackingField_12; }
	inline void set_U3CCustomizationIDU3Ek__BackingField_12(String_t* value)
	{
		___U3CCustomizationIDU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCustomizationIDU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CCustomVoiceUpdateU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(UpdateCustomizationRequest_t1669343124, ___U3CCustomVoiceUpdateU3Ek__BackingField_13)); }
	inline CustomVoiceUpdate_t1706248840 * get_U3CCustomVoiceUpdateU3Ek__BackingField_13() const { return ___U3CCustomVoiceUpdateU3Ek__BackingField_13; }
	inline CustomVoiceUpdate_t1706248840 ** get_address_of_U3CCustomVoiceUpdateU3Ek__BackingField_13() { return &___U3CCustomVoiceUpdateU3Ek__BackingField_13; }
	inline void set_U3CCustomVoiceUpdateU3Ek__BackingField_13(CustomVoiceUpdate_t1706248840 * value)
	{
		___U3CCustomVoiceUpdateU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCustomVoiceUpdateU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(UpdateCustomizationRequest_t1669343124, ___U3CDataU3Ek__BackingField_14)); }
	inline String_t* get_U3CDataU3Ek__BackingField_14() const { return ___U3CDataU3Ek__BackingField_14; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_14() { return &___U3CDataU3Ek__BackingField_14; }
	inline void set_U3CDataU3Ek__BackingField_14(String_t* value)
	{
		___U3CDataU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATECUSTOMIZATIONREQUEST_T1669343124_H
#ifndef INT32_T2071877448_H
#define INT32_T2071877448_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2071877448 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2071877448, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2071877448_H
#ifndef GETCUSTOMIZATIONREQUEST_T1351603463_H
#define GETCUSTOMIZATIONREQUEST_T1351603463_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationRequest
struct  GetCustomizationRequest_t1351603463  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationCallback IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationRequest::<Callback>k__BackingField
	GetCustomizationCallback_t4165371535 * ___U3CCallbackU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationRequest::<CustomizationID>k__BackingField
	String_t* ___U3CCustomizationIDU3Ek__BackingField_12;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetCustomizationRequest_t1351603463, ___U3CCallbackU3Ek__BackingField_11)); }
	inline GetCustomizationCallback_t4165371535 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline GetCustomizationCallback_t4165371535 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(GetCustomizationCallback_t4165371535 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCustomizationIDU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GetCustomizationRequest_t1351603463, ___U3CCustomizationIDU3Ek__BackingField_12)); }
	inline String_t* get_U3CCustomizationIDU3Ek__BackingField_12() const { return ___U3CCustomizationIDU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CCustomizationIDU3Ek__BackingField_12() { return &___U3CCustomizationIDU3Ek__BackingField_12; }
	inline void set_U3CCustomizationIDU3Ek__BackingField_12(String_t* value)
	{
		___U3CCustomizationIDU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCustomizationIDU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(GetCustomizationRequest_t1351603463, ___U3CDataU3Ek__BackingField_13)); }
	inline String_t* get_U3CDataU3Ek__BackingField_13() const { return ___U3CDataU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_13() { return &___U3CDataU3Ek__BackingField_13; }
	inline void set_U3CDataU3Ek__BackingField_13(String_t* value)
	{
		___U3CDataU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETCUSTOMIZATIONREQUEST_T1351603463_H
#ifndef WATSONEXCEPTION_T1186470237_H
#define WATSONEXCEPTION_T1186470237_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Utilities.WatsonException
struct  WatsonException_t1186470237  : public Exception_t1927440687
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WATSONEXCEPTION_T1186470237_H
#ifndef GETVOICEREQ_T1980513795_H
#define GETVOICEREQ_T1980513795_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetVoiceReq
struct  GetVoiceReq_t1980513795  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetVoiceCallback IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetVoiceReq::<Callback>k__BackingField
	GetVoiceCallback_t2752546046 * ___U3CCallbackU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetVoiceReq_t1980513795, ___U3CCallbackU3Ek__BackingField_11)); }
	inline GetVoiceCallback_t2752546046 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline GetVoiceCallback_t2752546046 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(GetVoiceCallback_t2752546046 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETVOICEREQ_T1980513795_H
#ifndef TOSPEECHREQUEST_T3970128895_H
#define TOSPEECHREQUEST_T3970128895_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/ToSpeechRequest
struct  ToSpeechRequest_t3970128895  : public Request_t466816980
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/ToSpeechRequest::<TextId>k__BackingField
	String_t* ___U3CTextIdU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/ToSpeechRequest::<Text>k__BackingField
	String_t* ___U3CTextU3Ek__BackingField_12;
	// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/ToSpeechCallback IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/ToSpeechRequest::<Callback>k__BackingField
	ToSpeechCallback_t3422748631 * ___U3CCallbackU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_U3CTextIdU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(ToSpeechRequest_t3970128895, ___U3CTextIdU3Ek__BackingField_11)); }
	inline String_t* get_U3CTextIdU3Ek__BackingField_11() const { return ___U3CTextIdU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CTextIdU3Ek__BackingField_11() { return &___U3CTextIdU3Ek__BackingField_11; }
	inline void set_U3CTextIdU3Ek__BackingField_11(String_t* value)
	{
		___U3CTextIdU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTextIdU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CTextU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(ToSpeechRequest_t3970128895, ___U3CTextU3Ek__BackingField_12)); }
	inline String_t* get_U3CTextU3Ek__BackingField_12() const { return ___U3CTextU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CTextU3Ek__BackingField_12() { return &___U3CTextU3Ek__BackingField_12; }
	inline void set_U3CTextU3Ek__BackingField_12(String_t* value)
	{
		___U3CTextU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTextU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(ToSpeechRequest_t3970128895, ___U3CCallbackU3Ek__BackingField_13)); }
	inline ToSpeechCallback_t3422748631 * get_U3CCallbackU3Ek__BackingField_13() const { return ___U3CCallbackU3Ek__BackingField_13; }
	inline ToSpeechCallback_t3422748631 ** get_address_of_U3CCallbackU3Ek__BackingField_13() { return &___U3CCallbackU3Ek__BackingField_13; }
	inline void set_U3CCallbackU3Ek__BackingField_13(ToSpeechCallback_t3422748631 * value)
	{
		___U3CCallbackU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOSPEECHREQUEST_T3970128895_H
#ifndef BYTE_T3683104436_H
#define BYTE_T3683104436_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Byte
struct  Byte_t3683104436 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Byte_t3683104436, ___m_value_2)); }
	inline uint8_t get_m_value_2() const { return ___m_value_2; }
	inline uint8_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(uint8_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTE_T3683104436_H
#ifndef GETVOICESREQ_T1741653746_H
#define GETVOICESREQ_T1741653746_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetVoicesReq
struct  GetVoicesReq_t1741653746  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetVoicesCallback IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetVoicesReq::<Callback>k__BackingField
	GetVoicesCallback_t3012885511 * ___U3CCallbackU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetVoicesReq_t1741653746, ___U3CCallbackU3Ek__BackingField_11)); }
	inline GetVoicesCallback_t3012885511 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline GetVoicesCallback_t3012885511 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(GetVoicesCallback_t3012885511 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETVOICESREQ_T1741653746_H
#ifndef GETCUSTOMIZATIONSREQ_T2206589443_H
#define GETCUSTOMIZATIONSREQ_T2206589443_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationsReq
struct  GetCustomizationsReq_t2206589443  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationsCallback IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationsReq::<Callback>k__BackingField
	GetCustomizationsCallback_t201646696 * ___U3CCallbackU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationsReq::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetCustomizationsReq_t2206589443, ___U3CCallbackU3Ek__BackingField_11)); }
	inline GetCustomizationsCallback_t201646696 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline GetCustomizationsCallback_t201646696 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(GetCustomizationsCallback_t201646696 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GetCustomizationsReq_t2206589443, ___U3CDataU3Ek__BackingField_12)); }
	inline String_t* get_U3CDataU3Ek__BackingField_12() const { return ___U3CDataU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_12() { return &___U3CDataU3Ek__BackingField_12; }
	inline void set_U3CDataU3Ek__BackingField_12(String_t* value)
	{
		___U3CDataU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETCUSTOMIZATIONSREQ_T2206589443_H
#ifndef FSRESULT_T862419890_H
#define FSRESULT_T862419890_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsResult
struct  fsResult_t862419890 
{
public:
	// System.Boolean FullSerializer.fsResult::_success
	bool ____success_1;
	// System.Collections.Generic.List`1<System.String> FullSerializer.fsResult::_messages
	List_1_t1398341365 * ____messages_2;

public:
	inline static int32_t get_offset_of__success_1() { return static_cast<int32_t>(offsetof(fsResult_t862419890, ____success_1)); }
	inline bool get__success_1() const { return ____success_1; }
	inline bool* get_address_of__success_1() { return &____success_1; }
	inline void set__success_1(bool value)
	{
		____success_1 = value;
	}

	inline static int32_t get_offset_of__messages_2() { return static_cast<int32_t>(offsetof(fsResult_t862419890, ____messages_2)); }
	inline List_1_t1398341365 * get__messages_2() const { return ____messages_2; }
	inline List_1_t1398341365 ** get_address_of__messages_2() { return &____messages_2; }
	inline void set__messages_2(List_1_t1398341365 * value)
	{
		____messages_2 = value;
		Il2CppCodeGenWriteBarrier((&____messages_2), value);
	}
};

struct fsResult_t862419890_StaticFields
{
public:
	// System.String[] FullSerializer.fsResult::EmptyStringArray
	StringU5BU5D_t1642385972* ___EmptyStringArray_0;
	// FullSerializer.fsResult FullSerializer.fsResult::Success
	fsResult_t862419890  ___Success_3;

public:
	inline static int32_t get_offset_of_EmptyStringArray_0() { return static_cast<int32_t>(offsetof(fsResult_t862419890_StaticFields, ___EmptyStringArray_0)); }
	inline StringU5BU5D_t1642385972* get_EmptyStringArray_0() const { return ___EmptyStringArray_0; }
	inline StringU5BU5D_t1642385972** get_address_of_EmptyStringArray_0() { return &___EmptyStringArray_0; }
	inline void set_EmptyStringArray_0(StringU5BU5D_t1642385972* value)
	{
		___EmptyStringArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyStringArray_0), value);
	}

	inline static int32_t get_offset_of_Success_3() { return static_cast<int32_t>(offsetof(fsResult_t862419890_StaticFields, ___Success_3)); }
	inline fsResult_t862419890  get_Success_3() const { return ___Success_3; }
	inline fsResult_t862419890 * get_address_of_Success_3() { return &___Success_3; }
	inline void set_Success_3(fsResult_t862419890  value)
	{
		___Success_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of FullSerializer.fsResult
struct fsResult_t862419890_marshaled_pinvoke
{
	int32_t ____success_1;
	List_1_t1398341365 * ____messages_2;
};
// Native definition for COM marshalling of FullSerializer.fsResult
struct fsResult_t862419890_marshaled_com
{
	int32_t ____success_1;
	List_1_t1398341365 * ____messages_2;
};
#endif // FSRESULT_T862419890_H
#ifndef GETCUSTOMIZATIONWORDREQUEST_T1280810157_H
#define GETCUSTOMIZATIONWORDREQUEST_T1280810157_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordRequest
struct  GetCustomizationWordRequest_t1280810157  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordCallback IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordRequest::<Callback>k__BackingField
	GetCustomizationWordCallback_t2485962781 * ___U3CCallbackU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordRequest::<CustomizationID>k__BackingField
	String_t* ___U3CCustomizationIDU3Ek__BackingField_12;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordRequest::<Word>k__BackingField
	String_t* ___U3CWordU3Ek__BackingField_13;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetCustomizationWordRequest_t1280810157, ___U3CCallbackU3Ek__BackingField_11)); }
	inline GetCustomizationWordCallback_t2485962781 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline GetCustomizationWordCallback_t2485962781 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(GetCustomizationWordCallback_t2485962781 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCustomizationIDU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GetCustomizationWordRequest_t1280810157, ___U3CCustomizationIDU3Ek__BackingField_12)); }
	inline String_t* get_U3CCustomizationIDU3Ek__BackingField_12() const { return ___U3CCustomizationIDU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CCustomizationIDU3Ek__BackingField_12() { return &___U3CCustomizationIDU3Ek__BackingField_12; }
	inline void set_U3CCustomizationIDU3Ek__BackingField_12(String_t* value)
	{
		___U3CCustomizationIDU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCustomizationIDU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CWordU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(GetCustomizationWordRequest_t1280810157, ___U3CWordU3Ek__BackingField_13)); }
	inline String_t* get_U3CWordU3Ek__BackingField_13() const { return ___U3CWordU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CWordU3Ek__BackingField_13() { return &___U3CWordU3Ek__BackingField_13; }
	inline void set_U3CWordU3Ek__BackingField_13(String_t* value)
	{
		___U3CWordU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CWordU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(GetCustomizationWordRequest_t1280810157, ___U3CDataU3Ek__BackingField_14)); }
	inline String_t* get_U3CDataU3Ek__BackingField_14() const { return ___U3CDataU3Ek__BackingField_14; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_14() { return &___U3CDataU3Ek__BackingField_14; }
	inline void set_U3CDataU3Ek__BackingField_14(String_t* value)
	{
		___U3CDataU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETCUSTOMIZATIONWORDREQUEST_T1280810157_H
#ifndef DOUBLE_T4078015681_H
#define DOUBLE_T4078015681_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Double
struct  Double_t4078015681 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_13;

public:
	inline static int32_t get_offset_of_m_value_13() { return static_cast<int32_t>(offsetof(Double_t4078015681, ___m_value_13)); }
	inline double get_m_value_13() const { return ___m_value_13; }
	inline double* get_address_of_m_value_13() { return &___m_value_13; }
	inline void set_m_value_13(double value)
	{
		___m_value_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLE_T4078015681_H
#ifndef DELETECUSTOMIZATIONWORDREQUEST_T3182066146_H
#define DELETECUSTOMIZATIONWORDREQUEST_T3182066146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/DeleteCustomizationWordRequest
struct  DeleteCustomizationWordRequest_t3182066146  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/OnDeleteCustomizationWordCallback IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/DeleteCustomizationWordRequest::<Callback>k__BackingField
	OnDeleteCustomizationWordCallback_t2544578463 * ___U3CCallbackU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/DeleteCustomizationWordRequest::<CustomizationID>k__BackingField
	String_t* ___U3CCustomizationIDU3Ek__BackingField_12;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/DeleteCustomizationWordRequest::<Word>k__BackingField
	String_t* ___U3CWordU3Ek__BackingField_13;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/DeleteCustomizationWordRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(DeleteCustomizationWordRequest_t3182066146, ___U3CCallbackU3Ek__BackingField_11)); }
	inline OnDeleteCustomizationWordCallback_t2544578463 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline OnDeleteCustomizationWordCallback_t2544578463 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(OnDeleteCustomizationWordCallback_t2544578463 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCustomizationIDU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(DeleteCustomizationWordRequest_t3182066146, ___U3CCustomizationIDU3Ek__BackingField_12)); }
	inline String_t* get_U3CCustomizationIDU3Ek__BackingField_12() const { return ___U3CCustomizationIDU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CCustomizationIDU3Ek__BackingField_12() { return &___U3CCustomizationIDU3Ek__BackingField_12; }
	inline void set_U3CCustomizationIDU3Ek__BackingField_12(String_t* value)
	{
		___U3CCustomizationIDU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCustomizationIDU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CWordU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(DeleteCustomizationWordRequest_t3182066146, ___U3CWordU3Ek__BackingField_13)); }
	inline String_t* get_U3CWordU3Ek__BackingField_13() const { return ___U3CWordU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CWordU3Ek__BackingField_13() { return &___U3CWordU3Ek__BackingField_13; }
	inline void set_U3CWordU3Ek__BackingField_13(String_t* value)
	{
		___U3CWordU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CWordU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(DeleteCustomizationWordRequest_t3182066146, ___U3CDataU3Ek__BackingField_14)); }
	inline String_t* get_U3CDataU3Ek__BackingField_14() const { return ___U3CDataU3Ek__BackingField_14; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_14() { return &___U3CDataU3Ek__BackingField_14; }
	inline void set_U3CDataU3Ek__BackingField_14(String_t* value)
	{
		___U3CDataU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELETECUSTOMIZATIONWORDREQUEST_T3182066146_H
#ifndef INT64_T909078037_H
#define INT64_T909078037_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int64
struct  Int64_t909078037 
{
public:
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int64_t909078037, ___m_value_2)); }
	inline int64_t get_m_value_2() const { return ___m_value_2; }
	inline int64_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int64_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT64_T909078037_H
#ifndef ADDCUSTOMIZATIONWORDREQUEST_T864480132_H
#define ADDCUSTOMIZATIONWORDREQUEST_T864480132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordRequest
struct  AddCustomizationWordRequest_t864480132  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordCallback IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordRequest::<Callback>k__BackingField
	AddCustomizationWordCallback_t3775539180 * ___U3CCallbackU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordRequest::<CustomizationID>k__BackingField
	String_t* ___U3CCustomizationIDU3Ek__BackingField_12;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordRequest::<Word>k__BackingField
	String_t* ___U3CWordU3Ek__BackingField_13;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordRequest::<Translation>k__BackingField
	String_t* ___U3CTranslationU3Ek__BackingField_14;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_15;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(AddCustomizationWordRequest_t864480132, ___U3CCallbackU3Ek__BackingField_11)); }
	inline AddCustomizationWordCallback_t3775539180 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline AddCustomizationWordCallback_t3775539180 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(AddCustomizationWordCallback_t3775539180 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCustomizationIDU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(AddCustomizationWordRequest_t864480132, ___U3CCustomizationIDU3Ek__BackingField_12)); }
	inline String_t* get_U3CCustomizationIDU3Ek__BackingField_12() const { return ___U3CCustomizationIDU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CCustomizationIDU3Ek__BackingField_12() { return &___U3CCustomizationIDU3Ek__BackingField_12; }
	inline void set_U3CCustomizationIDU3Ek__BackingField_12(String_t* value)
	{
		___U3CCustomizationIDU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCustomizationIDU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CWordU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(AddCustomizationWordRequest_t864480132, ___U3CWordU3Ek__BackingField_13)); }
	inline String_t* get_U3CWordU3Ek__BackingField_13() const { return ___U3CWordU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CWordU3Ek__BackingField_13() { return &___U3CWordU3Ek__BackingField_13; }
	inline void set_U3CWordU3Ek__BackingField_13(String_t* value)
	{
		___U3CWordU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CWordU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CTranslationU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(AddCustomizationWordRequest_t864480132, ___U3CTranslationU3Ek__BackingField_14)); }
	inline String_t* get_U3CTranslationU3Ek__BackingField_14() const { return ___U3CTranslationU3Ek__BackingField_14; }
	inline String_t** get_address_of_U3CTranslationU3Ek__BackingField_14() { return &___U3CTranslationU3Ek__BackingField_14; }
	inline void set_U3CTranslationU3Ek__BackingField_14(String_t* value)
	{
		___U3CTranslationU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTranslationU3Ek__BackingField_14), value);
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(AddCustomizationWordRequest_t864480132, ___U3CDataU3Ek__BackingField_15)); }
	inline String_t* get_U3CDataU3Ek__BackingField_15() const { return ___U3CDataU3Ek__BackingField_15; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_15() { return &___U3CDataU3Ek__BackingField_15; }
	inline void set_U3CDataU3Ek__BackingField_15(String_t* value)
	{
		___U3CDataU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDCUSTOMIZATIONWORDREQUEST_T864480132_H
#ifndef VOID_T1841601450_H
#define VOID_T1841601450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1841601450 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1841601450_H
#ifndef SYSTEMEXCEPTION_T3877406272_H
#define SYSTEMEXCEPTION_T3877406272_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t3877406272  : public Exception_t1927440687
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T3877406272_H
#ifndef GETCUSTOMIZATIONWORDSREQUEST_T110549728_H
#define GETCUSTOMIZATIONWORDSREQUEST_T110549728_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordsRequest
struct  GetCustomizationWordsRequest_t110549728  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordsCallback IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordsRequest::<Callback>k__BackingField
	GetCustomizationWordsCallback_t2709615664 * ___U3CCallbackU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordsRequest::<CustomizationID>k__BackingField
	String_t* ___U3CCustomizationIDU3Ek__BackingField_12;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordsRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetCustomizationWordsRequest_t110549728, ___U3CCallbackU3Ek__BackingField_11)); }
	inline GetCustomizationWordsCallback_t2709615664 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline GetCustomizationWordsCallback_t2709615664 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(GetCustomizationWordsCallback_t2709615664 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCustomizationIDU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GetCustomizationWordsRequest_t110549728, ___U3CCustomizationIDU3Ek__BackingField_12)); }
	inline String_t* get_U3CCustomizationIDU3Ek__BackingField_12() const { return ___U3CCustomizationIDU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CCustomizationIDU3Ek__BackingField_12() { return &___U3CCustomizationIDU3Ek__BackingField_12; }
	inline void set_U3CCustomizationIDU3Ek__BackingField_12(String_t* value)
	{
		___U3CCustomizationIDU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCustomizationIDU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(GetCustomizationWordsRequest_t110549728, ___U3CDataU3Ek__BackingField_13)); }
	inline String_t* get_U3CDataU3Ek__BackingField_13() const { return ___U3CDataU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_13() { return &___U3CDataU3Ek__BackingField_13; }
	inline void set_U3CDataU3Ek__BackingField_13(String_t* value)
	{
		___U3CDataU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETCUSTOMIZATIONWORDSREQUEST_T110549728_H
#ifndef ADDCUSTOMIZATIONWORDSREQUEST_T993580433_H
#define ADDCUSTOMIZATIONWORDSREQUEST_T993580433_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordsRequest
struct  AddCustomizationWordsRequest_t993580433  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordsCallback IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordsRequest::<Callback>k__BackingField
	AddCustomizationWordsCallback_t2578578585 * ___U3CCallbackU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordsRequest::<CustomizationID>k__BackingField
	String_t* ___U3CCustomizationIDU3Ek__BackingField_12;
	// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Words IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordsRequest::<Words>k__BackingField
	Words_t2948214629 * ___U3CWordsU3Ek__BackingField_13;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordsRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(AddCustomizationWordsRequest_t993580433, ___U3CCallbackU3Ek__BackingField_11)); }
	inline AddCustomizationWordsCallback_t2578578585 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline AddCustomizationWordsCallback_t2578578585 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(AddCustomizationWordsCallback_t2578578585 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCustomizationIDU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(AddCustomizationWordsRequest_t993580433, ___U3CCustomizationIDU3Ek__BackingField_12)); }
	inline String_t* get_U3CCustomizationIDU3Ek__BackingField_12() const { return ___U3CCustomizationIDU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CCustomizationIDU3Ek__BackingField_12() { return &___U3CCustomizationIDU3Ek__BackingField_12; }
	inline void set_U3CCustomizationIDU3Ek__BackingField_12(String_t* value)
	{
		___U3CCustomizationIDU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCustomizationIDU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CWordsU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(AddCustomizationWordsRequest_t993580433, ___U3CWordsU3Ek__BackingField_13)); }
	inline Words_t2948214629 * get_U3CWordsU3Ek__BackingField_13() const { return ___U3CWordsU3Ek__BackingField_13; }
	inline Words_t2948214629 ** get_address_of_U3CWordsU3Ek__BackingField_13() { return &___U3CWordsU3Ek__BackingField_13; }
	inline void set_U3CWordsU3Ek__BackingField_13(Words_t2948214629 * value)
	{
		___U3CWordsU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CWordsU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(AddCustomizationWordsRequest_t993580433, ___U3CDataU3Ek__BackingField_14)); }
	inline String_t* get_U3CDataU3Ek__BackingField_14() const { return ___U3CDataU3Ek__BackingField_14; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_14() { return &___U3CDataU3Ek__BackingField_14; }
	inline void set_U3CDataU3Ek__BackingField_14(String_t* value)
	{
		___U3CDataU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDCUSTOMIZATIONWORDSREQUEST_T993580433_H
#ifndef BOOLEAN_T3825574718_H
#define BOOLEAN_T3825574718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t3825574718 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t3825574718, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t3825574718_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t3825574718_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t3825574718_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T3825574718_H
#ifndef RUNTIMETYPEHANDLE_T2330101084_H
#define RUNTIMETYPEHANDLE_T2330101084_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t2330101084 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	IntPtr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t2330101084, ___value_0)); }
	inline IntPtr_t get_value_0() const { return ___value_0; }
	inline IntPtr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(IntPtr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMETYPEHANDLE_T2330101084_H
#ifndef AUDIOFORMATTYPE_T2756784245_H
#define AUDIOFORMATTYPE_T2756784245_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.AudioFormatType
struct  AudioFormatType_t2756784245 
{
public:
	// System.Int32 IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.AudioFormatType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AudioFormatType_t2756784245, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOFORMATTYPE_T2756784245_H
#ifndef BINDINGFLAGS_T1082350898_H
#define BINDINGFLAGS_T1082350898_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_t1082350898 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BindingFlags_t1082350898, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_T1082350898_H
#ifndef ARGUMENTEXCEPTION_T3259014390_H
#define ARGUMENTEXCEPTION_T3259014390_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArgumentException
struct  ArgumentException_t3259014390  : public SystemException_t3877406272
{
public:
	// System.String System.ArgumentException::param_name
	String_t* ___param_name_12;

public:
	inline static int32_t get_offset_of_param_name_12() { return static_cast<int32_t>(offsetof(ArgumentException_t3259014390, ___param_name_12)); }
	inline String_t* get_param_name_12() const { return ___param_name_12; }
	inline String_t** get_address_of_param_name_12() { return &___param_name_12; }
	inline void set_param_name_12(String_t* value)
	{
		___param_name_12 = value;
		Il2CppCodeGenWriteBarrier((&___param_name_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTEXCEPTION_T3259014390_H
#ifndef VOICETYPE_T981025524_H
#define VOICETYPE_T981025524_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.VoiceType
struct  VoiceType_t981025524 
{
public:
	// System.Int32 IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.VoiceType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(VoiceType_t981025524, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOICETYPE_T981025524_H
#ifndef OBJECT_T1021602117_H
#define OBJECT_T1021602117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1021602117  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	IntPtr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1021602117, ___m_CachedPtr_0)); }
	inline IntPtr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline IntPtr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(IntPtr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1021602117_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1021602117_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1021602117_H
#ifndef DELEGATE_T3022476291_H
#define DELEGATE_T3022476291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t3022476291  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	IntPtr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	IntPtr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	IntPtr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	IntPtr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1572802995 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___invoke_impl_1)); }
	inline IntPtr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline IntPtr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(IntPtr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_3)); }
	inline IntPtr_t get_method_3() const { return ___method_3; }
	inline IntPtr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(IntPtr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___delegate_trampoline_4)); }
	inline IntPtr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline IntPtr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(IntPtr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_code_5)); }
	inline IntPtr_t get_method_code_5() const { return ___method_code_5; }
	inline IntPtr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(IntPtr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___data_8)); }
	inline DelegateData_t1572802995 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1572802995 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1572802995 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T3022476291_H
#ifndef GETPRONUNCIATIONREQ_T2533372478_H
#define GETPRONUNCIATIONREQ_T2533372478_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetPronunciationReq
struct  GetPronunciationReq_t2533372478  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetPronunciationCallback IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetPronunciationReq::<Callback>k__BackingField
	GetPronunciationCallback_t2622344657 * ___U3CCallbackU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetPronunciationReq::<Text>k__BackingField
	String_t* ___U3CTextU3Ek__BackingField_12;
	// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.VoiceType IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetPronunciationReq::<Voice>k__BackingField
	int32_t ___U3CVoiceU3Ek__BackingField_13;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetPronunciationReq::<Format>k__BackingField
	String_t* ___U3CFormatU3Ek__BackingField_14;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetPronunciationReq::<Customization_ID>k__BackingField
	String_t* ___U3CCustomization_IDU3Ek__BackingField_15;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetPronunciationReq_t2533372478, ___U3CCallbackU3Ek__BackingField_11)); }
	inline GetPronunciationCallback_t2622344657 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline GetPronunciationCallback_t2622344657 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(GetPronunciationCallback_t2622344657 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CTextU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GetPronunciationReq_t2533372478, ___U3CTextU3Ek__BackingField_12)); }
	inline String_t* get_U3CTextU3Ek__BackingField_12() const { return ___U3CTextU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CTextU3Ek__BackingField_12() { return &___U3CTextU3Ek__BackingField_12; }
	inline void set_U3CTextU3Ek__BackingField_12(String_t* value)
	{
		___U3CTextU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTextU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CVoiceU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(GetPronunciationReq_t2533372478, ___U3CVoiceU3Ek__BackingField_13)); }
	inline int32_t get_U3CVoiceU3Ek__BackingField_13() const { return ___U3CVoiceU3Ek__BackingField_13; }
	inline int32_t* get_address_of_U3CVoiceU3Ek__BackingField_13() { return &___U3CVoiceU3Ek__BackingField_13; }
	inline void set_U3CVoiceU3Ek__BackingField_13(int32_t value)
	{
		___U3CVoiceU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CFormatU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(GetPronunciationReq_t2533372478, ___U3CFormatU3Ek__BackingField_14)); }
	inline String_t* get_U3CFormatU3Ek__BackingField_14() const { return ___U3CFormatU3Ek__BackingField_14; }
	inline String_t** get_address_of_U3CFormatU3Ek__BackingField_14() { return &___U3CFormatU3Ek__BackingField_14; }
	inline void set_U3CFormatU3Ek__BackingField_14(String_t* value)
	{
		___U3CFormatU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFormatU3Ek__BackingField_14), value);
	}

	inline static int32_t get_offset_of_U3CCustomization_IDU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(GetPronunciationReq_t2533372478, ___U3CCustomization_IDU3Ek__BackingField_15)); }
	inline String_t* get_U3CCustomization_IDU3Ek__BackingField_15() const { return ___U3CCustomization_IDU3Ek__BackingField_15; }
	inline String_t** get_address_of_U3CCustomization_IDU3Ek__BackingField_15() { return &___U3CCustomization_IDU3Ek__BackingField_15; }
	inline void set_U3CCustomization_IDU3Ek__BackingField_15(String_t* value)
	{
		___U3CCustomization_IDU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCustomization_IDU3Ek__BackingField_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETPRONUNCIATIONREQ_T2533372478_H
#ifndef TYPE_T_H
#define TYPE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t2330101084  ____impl_1;

public:
	inline static int32_t get_offset_of__impl_1() { return static_cast<int32_t>(offsetof(Type_t, ____impl_1)); }
	inline RuntimeTypeHandle_t2330101084  get__impl_1() const { return ____impl_1; }
	inline RuntimeTypeHandle_t2330101084 * get_address_of__impl_1() { return &____impl_1; }
	inline void set__impl_1(RuntimeTypeHandle_t2330101084  value)
	{
		____impl_1 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_2;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t1664964607* ___EmptyTypes_3;
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t3405857066 * ___FilterAttribute_4;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t3405857066 * ___FilterName_5;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t3405857066 * ___FilterNameIgnoreCase_6;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_7;

public:
	inline static int32_t get_offset_of_Delimiter_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_2)); }
	inline Il2CppChar get_Delimiter_2() const { return ___Delimiter_2; }
	inline Il2CppChar* get_address_of_Delimiter_2() { return &___Delimiter_2; }
	inline void set_Delimiter_2(Il2CppChar value)
	{
		___Delimiter_2 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_3)); }
	inline TypeU5BU5D_t1664964607* get_EmptyTypes_3() const { return ___EmptyTypes_3; }
	inline TypeU5BU5D_t1664964607** get_address_of_EmptyTypes_3() { return &___EmptyTypes_3; }
	inline void set_EmptyTypes_3(TypeU5BU5D_t1664964607* value)
	{
		___EmptyTypes_3 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_3), value);
	}

	inline static int32_t get_offset_of_FilterAttribute_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_4)); }
	inline MemberFilter_t3405857066 * get_FilterAttribute_4() const { return ___FilterAttribute_4; }
	inline MemberFilter_t3405857066 ** get_address_of_FilterAttribute_4() { return &___FilterAttribute_4; }
	inline void set_FilterAttribute_4(MemberFilter_t3405857066 * value)
	{
		___FilterAttribute_4 = value;
		Il2CppCodeGenWriteBarrier((&___FilterAttribute_4), value);
	}

	inline static int32_t get_offset_of_FilterName_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_5)); }
	inline MemberFilter_t3405857066 * get_FilterName_5() const { return ___FilterName_5; }
	inline MemberFilter_t3405857066 ** get_address_of_FilterName_5() { return &___FilterName_5; }
	inline void set_FilterName_5(MemberFilter_t3405857066 * value)
	{
		___FilterName_5 = value;
		Il2CppCodeGenWriteBarrier((&___FilterName_5), value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_6)); }
	inline MemberFilter_t3405857066 * get_FilterNameIgnoreCase_6() const { return ___FilterNameIgnoreCase_6; }
	inline MemberFilter_t3405857066 ** get_address_of_FilterNameIgnoreCase_6() { return &___FilterNameIgnoreCase_6; }
	inline void set_FilterNameIgnoreCase_6(MemberFilter_t3405857066 * value)
	{
		___FilterNameIgnoreCase_6 = value;
		Il2CppCodeGenWriteBarrier((&___FilterNameIgnoreCase_6), value);
	}

	inline static int32_t get_offset_of_Missing_7() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_7)); }
	inline RuntimeObject * get_Missing_7() const { return ___Missing_7; }
	inline RuntimeObject ** get_address_of_Missing_7() { return &___Missing_7; }
	inline void set_Missing_7(RuntimeObject * value)
	{
		___Missing_7 = value;
		Il2CppCodeGenWriteBarrier((&___Missing_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T_H
#ifndef TEXTTOSPEECH_T3349357562_H
#define TEXTTOSPEECH_T3349357562_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech
struct  TextToSpeech_t3349357562  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Utilities.DataCache IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech::m_SpeechCache
	DataCache_t4250340070 * ___m_SpeechCache_0;
	// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.VoiceType IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech::m_Voice
	int32_t ___m_Voice_1;
	// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.AudioFormatType IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech::m_AudioFormat
	int32_t ___m_AudioFormat_2;
	// System.Collections.Generic.Dictionary`2<IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.VoiceType,System.String> IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech::m_VoiceTypes
	Dictionary_2_t1569883280 * ___m_VoiceTypes_3;
	// System.Collections.Generic.Dictionary`2<IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.AudioFormatType,System.String> IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech::m_AudioFormats
	Dictionary_2_t2576372715 * ___m_AudioFormats_4;
	// System.Boolean IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech::<DisableCache>k__BackingField
	bool ___U3CDisableCacheU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of_m_SpeechCache_0() { return static_cast<int32_t>(offsetof(TextToSpeech_t3349357562, ___m_SpeechCache_0)); }
	inline DataCache_t4250340070 * get_m_SpeechCache_0() const { return ___m_SpeechCache_0; }
	inline DataCache_t4250340070 ** get_address_of_m_SpeechCache_0() { return &___m_SpeechCache_0; }
	inline void set_m_SpeechCache_0(DataCache_t4250340070 * value)
	{
		___m_SpeechCache_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_SpeechCache_0), value);
	}

	inline static int32_t get_offset_of_m_Voice_1() { return static_cast<int32_t>(offsetof(TextToSpeech_t3349357562, ___m_Voice_1)); }
	inline int32_t get_m_Voice_1() const { return ___m_Voice_1; }
	inline int32_t* get_address_of_m_Voice_1() { return &___m_Voice_1; }
	inline void set_m_Voice_1(int32_t value)
	{
		___m_Voice_1 = value;
	}

	inline static int32_t get_offset_of_m_AudioFormat_2() { return static_cast<int32_t>(offsetof(TextToSpeech_t3349357562, ___m_AudioFormat_2)); }
	inline int32_t get_m_AudioFormat_2() const { return ___m_AudioFormat_2; }
	inline int32_t* get_address_of_m_AudioFormat_2() { return &___m_AudioFormat_2; }
	inline void set_m_AudioFormat_2(int32_t value)
	{
		___m_AudioFormat_2 = value;
	}

	inline static int32_t get_offset_of_m_VoiceTypes_3() { return static_cast<int32_t>(offsetof(TextToSpeech_t3349357562, ___m_VoiceTypes_3)); }
	inline Dictionary_2_t1569883280 * get_m_VoiceTypes_3() const { return ___m_VoiceTypes_3; }
	inline Dictionary_2_t1569883280 ** get_address_of_m_VoiceTypes_3() { return &___m_VoiceTypes_3; }
	inline void set_m_VoiceTypes_3(Dictionary_2_t1569883280 * value)
	{
		___m_VoiceTypes_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_VoiceTypes_3), value);
	}

	inline static int32_t get_offset_of_m_AudioFormats_4() { return static_cast<int32_t>(offsetof(TextToSpeech_t3349357562, ___m_AudioFormats_4)); }
	inline Dictionary_2_t2576372715 * get_m_AudioFormats_4() const { return ___m_AudioFormats_4; }
	inline Dictionary_2_t2576372715 ** get_address_of_m_AudioFormats_4() { return &___m_AudioFormats_4; }
	inline void set_m_AudioFormats_4(Dictionary_2_t2576372715 * value)
	{
		___m_AudioFormats_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_AudioFormats_4), value);
	}

	inline static int32_t get_offset_of_U3CDisableCacheU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(TextToSpeech_t3349357562, ___U3CDisableCacheU3Ek__BackingField_8)); }
	inline bool get_U3CDisableCacheU3Ek__BackingField_8() const { return ___U3CDisableCacheU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CDisableCacheU3Ek__BackingField_8() { return &___U3CDisableCacheU3Ek__BackingField_8; }
	inline void set_U3CDisableCacheU3Ek__BackingField_8(bool value)
	{
		___U3CDisableCacheU3Ek__BackingField_8 = value;
	}
};

struct TextToSpeech_t3349357562_StaticFields
{
public:
	// FullSerializer.fsSerializer IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech::sm_Serializer
	fsSerializer_t4193731081 * ___sm_Serializer_6;

public:
	inline static int32_t get_offset_of_sm_Serializer_6() { return static_cast<int32_t>(offsetof(TextToSpeech_t3349357562_StaticFields, ___sm_Serializer_6)); }
	inline fsSerializer_t4193731081 * get_sm_Serializer_6() const { return ___sm_Serializer_6; }
	inline fsSerializer_t4193731081 ** get_address_of_sm_Serializer_6() { return &___sm_Serializer_6; }
	inline void set_sm_Serializer_6(fsSerializer_t4193731081 * value)
	{
		___sm_Serializer_6 = value;
		Il2CppCodeGenWriteBarrier((&___sm_Serializer_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTTOSPEECH_T3349357562_H
#ifndef MULTICASTDELEGATE_T3201952435_H
#define MULTICASTDELEGATE_T3201952435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t3201952435  : public Delegate_t3022476291
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t3201952435 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t3201952435 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___prev_9)); }
	inline MulticastDelegate_t3201952435 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t3201952435 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t3201952435 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___kpm_next_10)); }
	inline MulticastDelegate_t3201952435 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t3201952435 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t3201952435 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T3201952435_H
#ifndef ARGUMENTNULLEXCEPTION_T628810857_H
#define ARGUMENTNULLEXCEPTION_T628810857_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArgumentNullException
struct  ArgumentNullException_t628810857  : public ArgumentException_t3259014390
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTNULLEXCEPTION_T628810857_H
#ifndef NULLABLE_1_T3539059135_H
#define NULLABLE_1_T3539059135_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.VoiceType>
struct  Nullable_1_t3539059135 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t3539059135, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t3539059135, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T3539059135_H
#ifndef AUDIOCLIP_T1932558630_H
#define AUDIOCLIP_T1932558630_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AudioClip
struct  AudioClip_t1932558630  : public Object_t1021602117
{
public:
	// UnityEngine.AudioClip/PCMReaderCallback UnityEngine.AudioClip::m_PCMReaderCallback
	PCMReaderCallback_t3007145346 * ___m_PCMReaderCallback_2;
	// UnityEngine.AudioClip/PCMSetPositionCallback UnityEngine.AudioClip::m_PCMSetPositionCallback
	PCMSetPositionCallback_t421863554 * ___m_PCMSetPositionCallback_3;

public:
	inline static int32_t get_offset_of_m_PCMReaderCallback_2() { return static_cast<int32_t>(offsetof(AudioClip_t1932558630, ___m_PCMReaderCallback_2)); }
	inline PCMReaderCallback_t3007145346 * get_m_PCMReaderCallback_2() const { return ___m_PCMReaderCallback_2; }
	inline PCMReaderCallback_t3007145346 ** get_address_of_m_PCMReaderCallback_2() { return &___m_PCMReaderCallback_2; }
	inline void set_m_PCMReaderCallback_2(PCMReaderCallback_t3007145346 * value)
	{
		___m_PCMReaderCallback_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_PCMReaderCallback_2), value);
	}

	inline static int32_t get_offset_of_m_PCMSetPositionCallback_3() { return static_cast<int32_t>(offsetof(AudioClip_t1932558630, ___m_PCMSetPositionCallback_3)); }
	inline PCMSetPositionCallback_t421863554 * get_m_PCMSetPositionCallback_3() const { return ___m_PCMSetPositionCallback_3; }
	inline PCMSetPositionCallback_t421863554 ** get_address_of_m_PCMSetPositionCallback_3() { return &___m_PCMSetPositionCallback_3; }
	inline void set_m_PCMSetPositionCallback_3(PCMSetPositionCallback_t421863554 * value)
	{
		___m_PCMSetPositionCallback_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_PCMSetPositionCallback_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOCLIP_T1932558630_H
#ifndef ADDCUSTOMIZATIONWORDSCALLBACK_T2578578585_H
#define ADDCUSTOMIZATIONWORDSCALLBACK_T2578578585_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordsCallback
struct  AddCustomizationWordsCallback_t2578578585  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDCUSTOMIZATIONWORDSCALLBACK_T2578578585_H
#ifndef ONDELETECUSTOMIZATIONWORDCALLBACK_T2544578463_H
#define ONDELETECUSTOMIZATIONWORDCALLBACK_T2544578463_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/OnDeleteCustomizationWordCallback
struct  OnDeleteCustomizationWordCallback_t2544578463  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDELETECUSTOMIZATIONWORDCALLBACK_T2544578463_H
#ifndef GETCUSTOMIZATIONWORDSCALLBACK_T2709615664_H
#define GETCUSTOMIZATIONWORDSCALLBACK_T2709615664_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordsCallback
struct  GetCustomizationWordsCallback_t2709615664  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETCUSTOMIZATIONWORDSCALLBACK_T2709615664_H
#ifndef UPDATECUSTOMIZATIONCALLBACK_T3026233620_H
#define UPDATECUSTOMIZATIONCALLBACK_T3026233620_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/UpdateCustomizationCallback
struct  UpdateCustomizationCallback_t3026233620  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATECUSTOMIZATIONCALLBACK_T3026233620_H
#ifndef GETCUSTOMIZATIONWORDCALLBACK_T2485962781_H
#define GETCUSTOMIZATIONWORDCALLBACK_T2485962781_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordCallback
struct  GetCustomizationWordCallback_t2485962781  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETCUSTOMIZATIONWORDCALLBACK_T2485962781_H
#ifndef ONGETTONEANALYZED_T3813655652_H
#define ONGETTONEANALYZED_T3813655652_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzer/OnGetToneAnalyzed
struct  OnGetToneAnalyzed_t3813655652  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONGETTONEANALYZED_T3813655652_H
#ifndef SERVICESTATUS_T1443707987_H
#define SERVICESTATUS_T1443707987_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.ServiceStatus
struct  ServiceStatus_t1443707987  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERVICESTATUS_T1443707987_H
#ifndef ADDCUSTOMIZATIONWORDCALLBACK_T3775539180_H
#define ADDCUSTOMIZATIONWORDCALLBACK_T3775539180_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordCallback
struct  AddCustomizationWordCallback_t3775539180  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDCUSTOMIZATIONWORDCALLBACK_T3775539180_H
#ifndef ASYNCCALLBACK_T163412349_H
#define ASYNCCALLBACK_T163412349_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.AsyncCallback
struct  AsyncCallback_t163412349  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCCALLBACK_T163412349_H
#ifndef GETPRONUNCIATIONCALLBACK_T2622344657_H
#define GETPRONUNCIATIONCALLBACK_T2622344657_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetPronunciationCallback
struct  GetPronunciationCallback_t2622344657  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETPRONUNCIATIONCALLBACK_T2622344657_H
#ifndef GETCUSTOMIZATIONSCALLBACK_T201646696_H
#define GETCUSTOMIZATIONSCALLBACK_T201646696_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationsCallback
struct  GetCustomizationsCallback_t201646696  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETCUSTOMIZATIONSCALLBACK_T201646696_H
#ifndef GETVOICECALLBACK_T2752546046_H
#define GETVOICECALLBACK_T2752546046_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetVoiceCallback
struct  GetVoiceCallback_t2752546046  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETVOICECALLBACK_T2752546046_H
#ifndef ONDILEMMA_T401820821_H
#define ONDILEMMA_T401820821_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics/OnDilemma
struct  OnDilemma_t401820821  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDILEMMA_T401820821_H
#ifndef TOSPEECHCALLBACK_T3422748631_H
#define TOSPEECHCALLBACK_T3422748631_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/ToSpeechCallback
struct  ToSpeechCallback_t3422748631  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOSPEECHCALLBACK_T3422748631_H
#ifndef GETVOICESCALLBACK_T3012885511_H
#define GETVOICESCALLBACK_T3012885511_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetVoicesCallback
struct  GetVoicesCallback_t3012885511  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETVOICESCALLBACK_T3012885511_H
#ifndef GETCUSTOMIZATIONCALLBACK_T4165371535_H
#define GETCUSTOMIZATIONCALLBACK_T4165371535_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationCallback
struct  GetCustomizationCallback_t4165371535  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETCUSTOMIZATIONCALLBACK_T4165371535_H
#ifndef ONDELETECUSTOMIZATIONCALLBACK_T2862971265_H
#define ONDELETECUSTOMIZATIONCALLBACK_T2862971265_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/OnDeleteCustomizationCallback
struct  OnDeleteCustomizationCallback_t2862971265  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDELETECUSTOMIZATIONCALLBACK_T2862971265_H
#ifndef CREATECUSTOMIZATIONCALLBACK_T3071584865_H
#define CREATECUSTOMIZATIONCALLBACK_T3071584865_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/CreateCustomizationCallback
struct  CreateCustomizationCallback_t3071584865  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREATECUSTOMIZATIONCALLBACK_T3071584865_H
#ifndef RESPONSEEVENT_T1616568356_H
#define RESPONSEEVENT_T1616568356_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Connection.RESTConnector/ResponseEvent
struct  ResponseEvent_t1616568356  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESPONSEEVENT_T1616568356_H
// System.String[]
struct StringU5BU5D_t1642385972  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordAlternativeResult[]
struct WordAlternativeResultU5BU5D_t598168141  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) WordAlternativeResult_t3127007908 * m_Items[1];

public:
	inline WordAlternativeResult_t3127007908 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline WordAlternativeResult_t3127007908 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, WordAlternativeResult_t3127007908 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline WordAlternativeResult_t3127007908 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline WordAlternativeResult_t3127007908 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, WordAlternativeResult_t3127007908 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Word[]
struct WordU5BU5D_t2800973631  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Word_t3204175642 * m_Items[1];

public:
	inline Word_t3204175642 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Word_t3204175642 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Word_t3204175642 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Word_t3204175642 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Word_t3204175642 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Word_t3204175642 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordData[]
struct WordDataU5BU5D_t1726140911  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) WordData_t612265514 * m_Items[1];

public:
	inline WordData_t612265514 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline WordData_t612265514 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, WordData_t612265514 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline WordData_t612265514 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline WordData_t612265514 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, WordData_t612265514 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Customization[]
struct CustomizationU5BU5D_t626929480  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Customization_t2261013253 * m_Items[1];

public:
	inline Customization_t2261013253 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Customization_t2261013253 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Customization_t2261013253 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Customization_t2261013253 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Customization_t2261013253 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Customization_t2261013253 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Word[]
struct WordU5BU5D_t3686373631  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Word_t4274554970 * m_Items[1];

public:
	inline Word_t4274554970 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Word_t4274554970 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Word_t4274554970 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Word_t4274554970 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Word_t4274554970 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Word_t4274554970 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Object[]
struct ObjectU5BU5D_t3614634134  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Byte[]
struct ByteU5BU5D_t3397334013  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Voice[]
struct VoiceU5BU5D_t260285181  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Voice_t3646862260 * m_Items[1];

public:
	inline Voice_t3646862260 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Voice_t3646862260 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Voice_t3646862260 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Voice_t3646862260 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Voice_t3646862260 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Voice_t3646862260 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneCategory[]
struct ToneCategoryU5BU5D_t3565596459  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) ToneCategory_t3724297374 * m_Items[1];

public:
	inline ToneCategory_t3724297374 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ToneCategory_t3724297374 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ToneCategory_t3724297374 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline ToneCategory_t3724297374 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ToneCategory_t3724297374 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ToneCategory_t3724297374 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.Tone[]
struct ToneU5BU5D_t4171600311  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Tone_t1138009090 * m_Items[1];

public:
	inline Tone_t1138009090 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Tone_t1138009090 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Tone_t1138009090 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Tone_t1138009090 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Tone_t1138009090 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Tone_t1138009090 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.SentenceTone[]
struct SentenceToneU5BU5D_t255016994  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) SentenceTone_t3234952115 * m_Items[1];

public:
	inline SentenceTone_t3234952115 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline SentenceTone_t3234952115 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, SentenceTone_t3234952115 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline SentenceTone_t3234952115 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline SentenceTone_t3234952115 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, SentenceTone_t3234952115 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Anchor[]
struct AnchorU5BU5D_t2762965888  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Anchor_t1470404909 * m_Items[1];

public:
	inline Anchor_t1470404909 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Anchor_t1470404909 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Anchor_t1470404909 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Anchor_t1470404909 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Anchor_t1470404909 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Anchor_t1470404909 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Node[]
struct NodeU5BU5D_t283985611  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Node_t3986951550 * m_Items[1];

public:
	inline Node_t3986951550 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Node_t3986951550 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Node_t3986951550 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Node_t3986951550 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Node_t3986951550 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Node_t3986951550 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column[]
struct ColumnU5BU5D_t4200576441  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Column_t2612486824 * m_Items[1];

public:
	inline Column_t2612486824 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Column_t2612486824 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Column_t2612486824 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Column_t2612486824 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Column_t2612486824 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Column_t2612486824 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Option[]
struct OptionU5BU5D_t3811736648  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Option_t1775127045 * m_Items[1];

public:
	inline Option_t1775127045 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Option_t1775127045 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Option_t1775127045 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Option_t1775127045 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Option_t1775127045 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Option_t1775127045 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Solution[]
struct SolutionU5BU5D_t2153136770  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Solution_t204511443 * m_Items[1];

public:
	inline Solution_t204511443 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Solution_t204511443 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Solution_t204511443 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Solution_t204511443 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Solution_t204511443 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Solution_t204511443 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassResult[]
struct ClassResultU5BU5D_t2895066752  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) ClassResult_t3791036973 * m_Items[1];

public:
	inline ClassResult_t3791036973 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ClassResult_t3791036973 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ClassResult_t3791036973 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline ClassResult_t3791036973 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ClassResult_t3791036973 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ClassResult_t3791036973 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyTopLevelSingle[]
struct ClassifyTopLevelSingleU5BU5D_t3685983982  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) ClassifyTopLevelSingle_t3733844951 * m_Items[1];

public:
	inline ClassifyTopLevelSingle_t3733844951 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ClassifyTopLevelSingle_t3733844951 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ClassifyTopLevelSingle_t3733844951 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline ClassifyTopLevelSingle_t3733844951 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ClassifyTopLevelSingle_t3733844951 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ClassifyTopLevelSingle_t3733844951 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.WarningInfo[]
struct WarningInfoU5BU5D_t1932079499  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) WarningInfo_t624028094 * m_Items[1];

public:
	inline WarningInfo_t624028094 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline WarningInfo_t624028094 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, WarningInfo_t624028094 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline WarningInfo_t624028094 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline WarningInfo_t624028094 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, WarningInfo_t624028094 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyPerClassifier[]
struct ClassifyPerClassifierU5BU5D_t1895762155  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) ClassifyPerClassifier_t2357796318 * m_Items[1];

public:
	inline ClassifyPerClassifier_t2357796318 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ClassifyPerClassifier_t2357796318 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ClassifyPerClassifier_t2357796318 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline ClassifyPerClassifier_t2357796318 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ClassifyPerClassifier_t2357796318 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ClassifyPerClassifier_t2357796318 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.CollectionImagesConfig[]
struct CollectionImagesConfigU5BU5D_t666950595  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) CollectionImagesConfig_t1273369062 * m_Items[1];

public:
	inline CollectionImagesConfig_t1273369062 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline CollectionImagesConfig_t1273369062 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, CollectionImagesConfig_t1273369062 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline CollectionImagesConfig_t1273369062 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline CollectionImagesConfig_t1273369062 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, CollectionImagesConfig_t1273369062 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};


// System.Void System.Collections.Generic.Dictionary`2<IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.VoiceType,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m3226142901_gshared (Dictionary_2_t2230112342 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.VoiceType,System.Object>::Add(!0,!1)
extern "C"  void Dictionary_2_Add_m2206004909_gshared (Dictionary_2_t2230112342 * __this, int32_t p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.AudioFormatType,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m2207348332_gshared (Dictionary_2_t3236601777 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.AudioFormatType,System.Object>::Add(!0,!1)
extern "C"  void Dictionary_2_Add_m3744295588_gshared (Dictionary_2_t3236601777 * __this, int32_t p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.VoiceType,System.Object>::ContainsKey(!0)
extern "C"  bool Dictionary_2_ContainsKey_m2063750452_gshared (Dictionary_2_t2230112342 * __this, int32_t p0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.VoiceType,System.Object>::TryGetValue(!0,!1&)
extern "C"  bool Dictionary_2_TryGetValue_m3999961612_gshared (Dictionary_2_t2230112342 * __this, int32_t p0, RuntimeObject ** p1, const RuntimeMethod* method);
// System.Boolean System.Nullable`1<IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.VoiceType>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m925507304_gshared (Nullable_1_t3539059135 * __this, const RuntimeMethod* method);
// System.Void System.Nullable`1<IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.VoiceType>::.ctor(!0)
extern "C"  void Nullable_1__ctor_m2553508639_gshared (Nullable_1_t3539059135 * __this, int32_t p0, const RuntimeMethod* method);
// !0 System.Nullable`1<IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.VoiceType>::get_Value()
extern "C"  int32_t Nullable_1_get_Value_m2811304662_gshared (Nullable_1_t3539059135 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.AudioFormatType,System.Object>::ContainsKey(!0)
extern "C"  bool Dictionary_2_ContainsKey_m3257396879_gshared (Dictionary_2_t3236601777 * __this, int32_t p0, const RuntimeMethod* method);
// !1 System.Collections.Generic.Dictionary`2<IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.VoiceType,System.Object>::get_Item(!0)
extern "C"  RuntimeObject * Dictionary_2_get_Item_m2783033631_gshared (Dictionary_2_t2230112342 * __this, int32_t p0, const RuntimeMethod* method);
// !1 System.Collections.Generic.Dictionary`2<IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.AudioFormatType,System.Object>::get_Item(!0)
extern "C"  RuntimeObject * Dictionary_2_get_Item_m4165059534_gshared (Dictionary_2_t3236601777 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::set_Item(!0,!1)
extern "C"  void Dictionary_2_set_Item_m1004257024_gshared (Dictionary_2_t2281509423 * __this, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m584589095_gshared (Dictionary_2_t2281509423 * __this, const RuntimeMethod* method);
// FullSerializer.fsResult FullSerializer.fsSerializer::TrySerialize<System.Object>(T,FullSerializer.fsData&)
extern "C"  fsResult_t862419890  fsSerializer_TrySerialize_TisRuntimeObject_m4289598478_gshared (fsSerializer_t4193731081 * __this, RuntimeObject * ___instance0, fsData_t2583805605 ** ___data1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C"  void List_1__ctor_m310736118_gshared (List_1_t2058570427 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
extern "C"  void List_1_Add_m4157722533_gshared (List_1_t2058570427 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// !0[] System.Collections.Generic.List`1<System.Object>::ToArray()
extern "C"  ObjectU5BU5D_t3614634134* List_1_ToArray_m546658539_gshared (List_1_t2058570427 * __this, const RuntimeMethod* method);

// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m2551263788 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Customization::get_customization_id()
extern "C"  String_t* Customization_get_customization_id_m2343239751 (Customization_t2261013253 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::IsNullOrEmpty(System.String)
extern "C"  bool String_IsNullOrEmpty_m2802126737 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Customization[] IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Customizations::get_customizations()
extern "C"  CustomizationU5BU5D_t626929480* Customizations_get_customizations_m4036906150 (Customizations_t482380184 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Word[] IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomVoiceUpdate::get_words()
extern "C"  WordU5BU5D_t3686373631* CustomVoiceUpdate_get_words_m99370996 (CustomVoiceUpdate_t1706248840 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.VoiceType,System.String>::.ctor()
#define Dictionary_2__ctor_m3248355552(__this, method) ((  void (*) (Dictionary_2_t1569883280 *, const RuntimeMethod*))Dictionary_2__ctor_m3226142901_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.VoiceType,System.String>::Add(!0,!1)
#define Dictionary_2_Add_m2797377672(__this, p0, p1, method) ((  void (*) (Dictionary_2_t1569883280 *, int32_t, String_t*, const RuntimeMethod*))Dictionary_2_Add_m2206004909_gshared)(__this, p0, p1, method)
// System.Void System.Collections.Generic.Dictionary`2<IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.AudioFormatType,System.String>::.ctor()
#define Dictionary_2__ctor_m3553715201(__this, method) ((  void (*) (Dictionary_2_t2576372715 *, const RuntimeMethod*))Dictionary_2__ctor_m2207348332_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.AudioFormatType,System.String>::Add(!0,!1)
#define Dictionary_2_Add_m714379753(__this, p0, p1, method) ((  void (*) (Dictionary_2_t2576372715 *, int32_t, String_t*, const RuntimeMethod*))Dictionary_2_Add_m3744295588_gshared)(__this, p0, p1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.VoiceType,System.String>::ContainsKey(!0)
#define Dictionary_2_ContainsKey_m1508741739(__this, p0, method) ((  bool (*) (Dictionary_2_t1569883280 *, int32_t, const RuntimeMethod*))Dictionary_2_ContainsKey_m2063750452_gshared)(__this, p0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.VoiceType,System.String>::TryGetValue(!0,!1&)
#define Dictionary_2_TryGetValue_m2360694639(__this, p0, p1, method) ((  bool (*) (Dictionary_2_t1569883280 *, int32_t, String_t**, const RuntimeMethod*))Dictionary_2_TryGetValue_m3999961612_gshared)(__this, p0, p1, method)
// System.Void IBM.Watson.DeveloperCloud.Logging.Log::Warning(System.String,System.String,System.Object[])
extern "C"  void Log_Warning_m3862795843 (RuntimeObject * __this /* static, unused */, String_t* ___subSystem0, String_t* ___messageFmt1, ObjectU5BU5D_t3614634134* ___args2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentNullException::.ctor(System.String)
extern "C"  void ArgumentNullException__ctor_m3380712306 (ArgumentNullException_t628810857 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// IBM.Watson.DeveloperCloud.Connection.RESTConnector IBM.Watson.DeveloperCloud.Connection.RESTConnector::GetConnector(System.String,System.String,System.Boolean)
extern "C"  RESTConnector_t3705102247 * RESTConnector_GetConnector_m3262139854 (RuntimeObject * __this /* static, unused */, String_t* ___serviceID0, String_t* ___function1, bool ___useCache2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetVoicesReq::.ctor()
extern "C"  void GetVoicesReq__ctor_m1962143417 (GetVoicesReq_t1741653746 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetVoicesReq::set_Callback(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetVoicesCallback)
extern "C"  void GetVoicesReq_set_Callback_m1978410448 (GetVoicesReq_t1741653746 * __this, GetVoicesCallback_t3012885511 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Connection.RESTConnector/ResponseEvent::.ctor(System.Object,System.IntPtr)
extern "C"  void ResponseEvent__ctor_m3385394859 (ResponseEvent_t1616568356 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::set_OnResponse(IBM.Watson.DeveloperCloud.Connection.RESTConnector/ResponseEvent)
extern "C"  void Request_set_OnResponse_m2226001952 (Request_t466816980 * __this, ResponseEvent_t1616568356 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean IBM.Watson.DeveloperCloud.Connection.RESTConnector::Send(IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request)
extern "C"  bool RESTConnector_Send_m2925366401 (RESTConnector_t3705102247 * __this, Request_t466816980 * ___request0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Voices::.ctor()
extern "C"  void Voices__ctor_m2371932580 (Voices_t4221445733 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean IBM.Watson.DeveloperCloud.Connection.RESTConnector/Response::get_Success()
extern "C"  bool Response_get_Success_m2313267345 (Response_t429319368 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Text.Encoding System.Text.Encoding::get_UTF8()
extern "C"  Encoding_t663144255 * Encoding_get_UTF8_m1752852937 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Byte[] IBM.Watson.DeveloperCloud.Connection.RESTConnector/Response::get_Data()
extern "C"  ByteU5BU5D_t3397334013* Response_get_Data_m3078004018 (Response_t429319368 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// FullSerializer.fsResult FullSerializer.fsJsonParser::Parse(System.String,FullSerializer.fsData&)
extern "C"  fsResult_t862419890  fsJsonParser_Parse_m4200302395 (RuntimeObject * __this /* static, unused */, String_t* ___input0, fsData_t2583805605 ** ___data1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean FullSerializer.fsResult::get_Succeeded()
extern "C"  bool fsResult_get_Succeeded_m3237049006 (fsResult_t862419890 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String FullSerializer.fsResult::get_FormattedMessages()
extern "C"  String_t* fsResult_get_FormattedMessages_m3414202484 (fsResult_t862419890 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Utilities.WatsonException::.ctor(System.String)
extern "C"  void WatsonException__ctor_m3825115789 (WatsonException_t1186470237 * __this, String_t* ___message0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Type System.Object::GetType()
extern "C"  Type_t * Object_GetType_m191970594 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// FullSerializer.fsResult FullSerializer.fsSerializer::TryDeserialize(FullSerializer.fsData,System.Type,System.Object&)
extern "C"  fsResult_t862419890  fsSerializer_TryDeserialize_m4134098758 (fsSerializer_t4193731081 * __this, fsData_t2583805605 * ___data0, Type_t * ___storageType1, RuntimeObject ** ___result2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Logging.Log::Error(System.String,System.String,System.Object[])
extern "C"  void Log_Error_m685270089 (RuntimeObject * __this /* static, unused */, String_t* ___subSystem0, String_t* ___messageFmt1, ObjectU5BU5D_t3614634134* ___args2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Connection.RESTConnector/Response::set_Success(System.Boolean)
extern "C"  void Response_set_Success_m3746673474 (Response_t429319368 * __this, bool ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetVoicesCallback IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetVoicesReq::get_Callback()
extern "C"  GetVoicesCallback_t3012885511 * GetVoicesReq_get_Callback_m4057882599 (GetVoicesReq_t1741653746 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetVoicesCallback::Invoke(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Voices)
extern "C"  void GetVoicesCallback_Invoke_m1235110721 (GetVoicesCallback_t3012885511 * __this, Voices_t4221445733 * ___voices0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Nullable`1<IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.VoiceType>::get_HasValue()
#define Nullable_1_get_HasValue_m925507304(__this, method) ((  bool (*) (Nullable_1_t3539059135 *, const RuntimeMethod*))Nullable_1_get_HasValue_m925507304_gshared)(__this, method)
// System.Void System.Nullable`1<IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.VoiceType>::.ctor(!0)
#define Nullable_1__ctor_m2553508639(__this, p0, method) ((  void (*) (Nullable_1_t3539059135 *, int32_t, const RuntimeMethod*))Nullable_1__ctor_m2553508639_gshared)(__this, p0, method)
// !0 System.Nullable`1<IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.VoiceType>::get_Value()
#define Nullable_1_get_Value_m2811304662(__this, method) ((  int32_t (*) (Nullable_1_t3539059135 *, const RuntimeMethod*))Nullable_1_get_Value_m2811304662_gshared)(__this, method)
// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech::GetVoiceType(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.VoiceType)
extern "C"  String_t* TextToSpeech_GetVoiceType_m999042752 (TextToSpeech_t3349357562 * __this, int32_t ___voiceType0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Format(System.String,System.Object)
extern "C"  String_t* String_Format_m2024975688 (RuntimeObject * __this /* static, unused */, String_t* p0, RuntimeObject * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetVoiceReq::.ctor()
extern "C"  void GetVoiceReq__ctor_m1969519438 (GetVoiceReq_t1980513795 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetVoiceReq::set_Callback(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetVoiceCallback)
extern "C"  void GetVoiceReq_set_Callback_m1200200228 (GetVoiceReq_t1980513795 * __this, GetVoiceCallback_t2752546046 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Voice::.ctor()
extern "C"  void Voice__ctor_m3947940203 (Voice_t3646862260 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetVoiceCallback IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetVoiceReq::get_Callback()
extern "C"  GetVoiceCallback_t2752546046 * GetVoiceReq_get_Callback_m2983428311 (GetVoiceReq_t1980513795 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetVoiceCallback::Invoke(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Voice)
extern "C"  void GetVoiceCallback_Invoke_m2628701565 (GetVoiceCallback_t2752546046 * __this, Voice_t3646862260 * ___voice0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2<IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.AudioFormatType,System.String>::ContainsKey(!0)
#define Dictionary_2_ContainsKey_m2816287462(__this, p0, method) ((  bool (*) (Dictionary_2_t2576372715 *, int32_t, const RuntimeMethod*))Dictionary_2_ContainsKey_m3257396879_gshared)(__this, p0, method)
// System.String IBM.Watson.DeveloperCloud.Utilities.Utility::GetMD5(System.String)
extern "C"  String_t* Utility_GetMD5_m2388485409 (RuntimeObject * __this /* static, unused */, String_t* ___s0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech::get_DisableCache()
extern "C"  bool TextToSpeech_get_DisableCache_m444348792 (TextToSpeech_t3349357562 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !1 System.Collections.Generic.Dictionary`2<IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.VoiceType,System.String>::get_Item(!0)
#define Dictionary_2_get_Item_m2485813692(__this, p0, method) ((  String_t* (*) (Dictionary_2_t1569883280 *, int32_t, const RuntimeMethod*))Dictionary_2_get_Item_m2783033631_gshared)(__this, p0, method)
// System.String System.String::Concat(System.String,System.String)
extern "C"  String_t* String_Concat_m2596409543 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Utilities.DataCache::.ctor(System.String,System.Int64,System.Double)
extern "C"  void DataCache__ctor_m1332098046 (DataCache_t4250340070 * __this, String_t* ___cacheName0, int64_t ___maxCacheSize1, double ___maxCacheAge2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Byte[] IBM.Watson.DeveloperCloud.Utilities.DataCache::Find(System.String)
extern "C"  ByteU5BU5D_t3397334013* DataCache_Find_m2954824875 (DataCache_t4250340070 * __this, String_t* ___id0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioClip IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech::ProcessResponse(System.String,System.Byte[])
extern "C"  AudioClip_t1932558630 * TextToSpeech_ProcessResponse_m2113435348 (TextToSpeech_t3349357562 * __this, String_t* ___textId0, ByteU5BU5D_t3397334013* ___data1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/ToSpeechCallback::Invoke(UnityEngine.AudioClip)
extern "C"  void ToSpeechCallback_Invoke_m1017023077 (ToSpeechCallback_t3422748631 * __this, AudioClip_t1932558630 * ___clip0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/ToSpeechRequest::.ctor()
extern "C"  void ToSpeechRequest__ctor_m105557656 (ToSpeechRequest_t3970128895 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/ToSpeechRequest::set_TextId(System.String)
extern "C"  void ToSpeechRequest_set_TextId_m2283266821 (ToSpeechRequest_t3970128895 * __this, String_t* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/ToSpeechRequest::set_Text(System.String)
extern "C"  void ToSpeechRequest_set_Text_m84855046 (ToSpeechRequest_t3970128895 * __this, String_t* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/ToSpeechRequest::set_Callback(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/ToSpeechCallback)
extern "C"  void ToSpeechRequest_set_Callback_m683453887 (ToSpeechRequest_t3970128895 * __this, ToSpeechCallback_t3422748631 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.Object> IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::get_Parameters()
extern "C"  Dictionary_2_t309261261 * Request_get_Parameters_m1217035494 (Request_t466816980 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !1 System.Collections.Generic.Dictionary`2<IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.AudioFormatType,System.String>::get_Item(!0)
#define Dictionary_2_get_Item_m3261277235(__this, p0, method) ((  String_t* (*) (Dictionary_2_t2576372715 *, int32_t, const RuntimeMethod*))Dictionary_2_get_Item_m4165059534_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Object>::set_Item(!0,!1)
#define Dictionary_2_set_Item_m3411472609(__this, p0, p1, method) ((  void (*) (Dictionary_2_t309261261 *, String_t*, RuntimeObject *, const RuntimeMethod*))Dictionary_2_set_Item_m1004257024_gshared)(__this, p0, p1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::.ctor()
#define Dictionary_2__ctor_m760167321(__this, method) ((  void (*) (Dictionary_2_t3943999495 *, const RuntimeMethod*))Dictionary_2__ctor_m584589095_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::set_Item(!0,!1)
#define Dictionary_2_set_Item_m4244870320(__this, p0, p1, method) ((  void (*) (Dictionary_2_t3943999495 *, String_t*, String_t*, const RuntimeMethod*))Dictionary_2_set_Item_m1004257024_gshared)(__this, p0, p1, method)
// System.String MiniJSON.Json::Serialize(System.Object)
extern "C"  String_t* Json_Serialize_m779708699 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___obj0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::set_Send(System.Byte[])
extern "C"  void Request_set_Send_m296178747 (Request_t466816980 * __this, ByteU5BU5D_t3397334013* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::get_Headers()
extern "C"  Dictionary_2_t3943999495 * Request_get_Headers_m340270624 (Request_t466816980 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/ToSpeechRequest::get_TextId()
extern "C"  String_t* ToSpeechRequest_get_TextId_m3218788966 (ToSpeechRequest_t3970128895 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m3764089466 (RuntimeObject * __this /* static, unused */, Object_t1021602117 * p0, Object_t1021602117 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String IBM.Watson.DeveloperCloud.Connection.RESTConnector/Response::get_Error()
extern "C"  String_t* Response_get_Error_m276091539 (Response_t429319368 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m2402264703 (RuntimeObject * __this /* static, unused */, Object_t1021602117 * p0, Object_t1021602117 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Utilities.DataCache::Save(System.String,System.Byte[])
extern "C"  void DataCache_Save_m3872508888 (DataCache_t4250340070 * __this, String_t* ___id0, ByteU5BU5D_t3397334013* ___data1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/ToSpeechCallback IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/ToSpeechRequest::get_Callback()
extern "C"  ToSpeechCallback_t3422748631 * ToSpeechRequest_get_Callback_m490611990 (ToSpeechRequest_t3970128895 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioClip IBM.Watson.DeveloperCloud.Utilities.WaveFile::ParseWAV(System.String,System.Byte[])
extern "C"  AudioClip_t1932558630 * WaveFile_ParseWAV_m3088437927 (RuntimeObject * __this /* static, unused */, String_t* ___clipName0, ByteU5BU5D_t3397334013* ___data1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetPronunciationReq::.ctor()
extern "C"  void GetPronunciationReq__ctor_m239301707 (GetPronunciationReq_t2533372478 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetPronunciationReq::set_Callback(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetPronunciationCallback)
extern "C"  void GetPronunciationReq_set_Callback_m1752878660 (GetPronunciationReq_t2533372478 * __this, GetPronunciationCallback_t2622344657 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetPronunciationReq::set_Text(System.String)
extern "C"  void GetPronunciationReq_set_Text_m2524915157 (GetPronunciationReq_t2533372478 * __this, String_t* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetPronunciationReq::set_Voice(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.VoiceType)
extern "C"  void GetPronunciationReq_set_Voice_m2970561308 (GetPronunciationReq_t2533372478 * __this, int32_t ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetPronunciationReq::set_Format(System.String)
extern "C"  void GetPronunciationReq_set_Format_m175712339 (GetPronunciationReq_t2533372478 * __this, String_t* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetPronunciationReq::set_Customization_ID(System.String)
extern "C"  void GetPronunciationReq_set_Customization_ID_m776457007 (GetPronunciationReq_t2533372478 * __this, String_t* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Pronunciation::.ctor()
extern "C"  void Pronunciation__ctor_m2815357694 (Pronunciation_t2711344207 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetPronunciationCallback IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetPronunciationReq::get_Callback()
extern "C"  GetPronunciationCallback_t2622344657 * GetPronunciationReq_get_Callback_m3167238787 (GetPronunciationReq_t2533372478 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetPronunciationCallback::Invoke(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Pronunciation)
extern "C"  void GetPronunciationCallback_Invoke_m2518062913 (GetPronunciationCallback_t2622344657 * __this, Pronunciation_t2711344207 * ___pronunciation0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationsReq::.ctor()
extern "C"  void GetCustomizationsReq__ctor_m724592476 (GetCustomizationsReq_t2206589443 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationsReq::set_Callback(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationsCallback)
extern "C"  void GetCustomizationsReq_set_Callback_m483150754 (GetCustomizationsReq_t2206589443 * __this, GetCustomizationsCallback_t201646696 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationsReq::set_Data(System.String)
extern "C"  void GetCustomizationsReq_set_Data_m3616098737 (GetCustomizationsReq_t2206589443 * __this, String_t* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Customizations::.ctor()
extern "C"  void Customizations__ctor_m1298341255 (Customizations_t482380184 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationsCallback IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationsReq::get_Callback()
extern "C"  GetCustomizationsCallback_t201646696 * GetCustomizationsReq_get_Callback_m4047984775 (GetCustomizationsReq_t2206589443 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationsReq::get_Data()
extern "C"  String_t* GetCustomizationsReq_get_Data_m1806572778 (GetCustomizationsReq_t2206589443 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationsCallback::Invoke(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Customizations,System.String)
extern "C"  void GetCustomizationsCallback_Invoke_m340907479 (GetCustomizationsCallback_t201646696 * __this, Customizations_t482380184 * ___customizations0, String_t* ___data1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomVoice::.ctor()
extern "C"  void CustomVoice__ctor_m2182765280 (CustomVoice_t330695553 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomVoice::set_name(System.String)
extern "C"  void CustomVoice_set_name_m2044531626 (CustomVoice_t330695553 * __this, String_t* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomVoice::set_language(System.String)
extern "C"  void CustomVoice_set_language_m2534117753 (CustomVoice_t330695553 * __this, String_t* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomVoice::set_description(System.String)
extern "C"  void CustomVoice_set_description_m1558525425 (CustomVoice_t330695553 * __this, String_t* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// FullSerializer.fsResult FullSerializer.fsSerializer::TrySerialize(System.Type,System.Object,FullSerializer.fsData&)
extern "C"  fsResult_t862419890  fsSerializer_TrySerialize_m867329083 (fsSerializer_t4193731081 * __this, Type_t * ___storageType0, RuntimeObject * ___instance1, fsData_t2583805605 ** ___data2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// FullSerializer.fsResult FullSerializer.fsResult::AssertSuccessWithoutWarnings()
extern "C"  fsResult_t862419890  fsResult_AssertSuccessWithoutWarnings_m1273657906 (fsResult_t862419890 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String FullSerializer.fsJsonPrinter::CompressedJson(FullSerializer.fsData,System.Boolean)
extern "C"  String_t* fsJsonPrinter_CompressedJson_m2458408074 (RuntimeObject * __this /* static, unused */, fsData_t2583805605 * ___data0, bool ___dontAddIfNull1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/CreateCustomizationRequest::.ctor()
extern "C"  void CreateCustomizationRequest__ctor_m3981671984 (CreateCustomizationRequest_t295745401 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/CreateCustomizationRequest::set_Callback(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/CreateCustomizationCallback)
extern "C"  void CreateCustomizationRequest_set_Callback_m709816875 (CreateCustomizationRequest_t295745401 * __this, CreateCustomizationCallback_t3071584865 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/CreateCustomizationRequest::set_CustomVoice(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomVoice)
extern "C"  void CreateCustomizationRequest_set_CustomVoice_m1331556427 (CreateCustomizationRequest_t295745401 * __this, CustomVoice_t330695553 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/CreateCustomizationRequest::set_Data(System.String)
extern "C"  void CreateCustomizationRequest_set_Data_m3310641787 (CreateCustomizationRequest_t295745401 * __this, String_t* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomizationID::.ctor()
extern "C"  void CustomizationID__ctor_m4015391793 (CustomizationID_t344279642 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/CreateCustomizationCallback IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/CreateCustomizationRequest::get_Callback()
extern "C"  CreateCustomizationCallback_t3071584865 * CreateCustomizationRequest_get_Callback_m3064008816 (CreateCustomizationRequest_t295745401 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/CreateCustomizationRequest::get_Data()
extern "C"  String_t* CreateCustomizationRequest_get_Data_m2559910470 (CreateCustomizationRequest_t295745401 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/CreateCustomizationCallback::Invoke(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomizationID,System.String)
extern "C"  void CreateCustomizationCallback_Invoke_m3243793162 (CreateCustomizationCallback_t3071584865 * __this, CustomizationID_t344279642 * ___customizationID0, String_t* ___data1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/DeleteCustomizationRequest::.ctor()
extern "C"  void DeleteCustomizationRequest__ctor_m2097311445 (DeleteCustomizationRequest_t4044506578 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/DeleteCustomizationRequest::set_Callback(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/OnDeleteCustomizationCallback)
extern "C"  void DeleteCustomizationRequest_set_Callback_m1090382596 (DeleteCustomizationRequest_t4044506578 * __this, OnDeleteCustomizationCallback_t2862971265 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/DeleteCustomizationRequest::set_CustomizationID(System.String)
extern "C"  void DeleteCustomizationRequest_set_CustomizationID_m1101845264 (DeleteCustomizationRequest_t4044506578 * __this, String_t* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/DeleteCustomizationRequest::set_Data(System.String)
extern "C"  void DeleteCustomizationRequest_set_Data_m3245021746 (DeleteCustomizationRequest_t4044506578 * __this, String_t* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::set_Timeout(System.Single)
extern "C"  void Request_set_Timeout_m3686318020 (Request_t466816980 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::set_Delete(System.Boolean)
extern "C"  void Request_set_Delete_m357548768 (Request_t466816980 * __this, bool ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/OnDeleteCustomizationCallback IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/DeleteCustomizationRequest::get_Callback()
extern "C"  OnDeleteCustomizationCallback_t2862971265 * DeleteCustomizationRequest_get_Callback_m3675390565 (DeleteCustomizationRequest_t4044506578 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/DeleteCustomizationRequest::get_Data()
extern "C"  String_t* DeleteCustomizationRequest_get_Data_m669478649 (DeleteCustomizationRequest_t4044506578 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/OnDeleteCustomizationCallback::Invoke(System.Boolean,System.String)
extern "C"  void OnDeleteCustomizationCallback_Invoke_m2846474177 (OnDeleteCustomizationCallback_t2862971265 * __this, bool ___success0, String_t* ___data1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationRequest::.ctor()
extern "C"  void GetCustomizationRequest__ctor_m2422485738 (GetCustomizationRequest_t1351603463 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationRequest::set_Callback(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationCallback)
extern "C"  void GetCustomizationRequest_set_Callback_m3900696383 (GetCustomizationRequest_t1351603463 * __this, GetCustomizationCallback_t4165371535 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationRequest::set_CustomizationID(System.String)
extern "C"  void GetCustomizationRequest_set_CustomizationID_m4212369937 (GetCustomizationRequest_t1351603463 * __this, String_t* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationRequest::set_Data(System.String)
extern "C"  void GetCustomizationRequest_set_Data_m2028509081 (GetCustomizationRequest_t1351603463 * __this, String_t* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Customization::.ctor()
extern "C"  void Customization__ctor_m1020302414 (Customization_t2261013253 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationCallback IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationRequest::get_Callback()
extern "C"  GetCustomizationCallback_t4165371535 * GetCustomizationRequest_get_Callback_m1998934598 (GetCustomizationRequest_t1351603463 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationRequest::get_Data()
extern "C"  String_t* GetCustomizationRequest_get_Data_m2763780886 (GetCustomizationRequest_t1351603463 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationCallback::Invoke(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Customization,System.String)
extern "C"  void GetCustomizationCallback_Invoke_m3398604559 (GetCustomizationCallback_t4165371535 * __this, Customization_t2261013253 * ___customization0, String_t* ___data1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomVoiceUpdate::HasData()
extern "C"  bool CustomVoiceUpdate_HasData_m2393568391 (CustomVoiceUpdate_t1706248840 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/UpdateCustomizationRequest::.ctor()
extern "C"  void UpdateCustomizationRequest__ctor_m2167726953 (UpdateCustomizationRequest_t1669343124 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/UpdateCustomizationRequest::set_Callback(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/UpdateCustomizationCallback)
extern "C"  void UpdateCustomizationRequest_set_Callback_m374347127 (UpdateCustomizationRequest_t1669343124 * __this, UpdateCustomizationCallback_t3026233620 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/UpdateCustomizationRequest::set_CustomVoiceUpdate(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomVoiceUpdate)
extern "C"  void UpdateCustomizationRequest_set_CustomVoiceUpdate_m1632905410 (UpdateCustomizationRequest_t1669343124 * __this, CustomVoiceUpdate_t1706248840 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/UpdateCustomizationRequest::set_CustomizationID(System.String)
extern "C"  void UpdateCustomizationRequest_set_CustomizationID_m3383711138 (UpdateCustomizationRequest_t1669343124 * __this, String_t* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/UpdateCustomizationRequest::set_Data(System.String)
extern "C"  void UpdateCustomizationRequest_set_Data_m3367529024 (UpdateCustomizationRequest_t1669343124 * __this, String_t* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/UpdateCustomizationCallback IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/UpdateCustomizationRequest::get_Callback()
extern "C"  UpdateCustomizationCallback_t3026233620 * UpdateCustomizationRequest_get_Callback_m1649430960 (UpdateCustomizationRequest_t1669343124 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/UpdateCustomizationRequest::get_Data()
extern "C"  String_t* UpdateCustomizationRequest_get_Data_m3622793189 (UpdateCustomizationRequest_t1669343124 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/UpdateCustomizationCallback::Invoke(System.Boolean,System.String)
extern "C"  void UpdateCustomizationCallback_Invoke_m16749586 (UpdateCustomizationCallback_t3026233620 * __this, bool ___success0, String_t* ___data1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordsRequest::.ctor()
extern "C"  void GetCustomizationWordsRequest__ctor_m1707632875 (GetCustomizationWordsRequest_t110549728 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordsRequest::set_Callback(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordsCallback)
extern "C"  void GetCustomizationWordsRequest_set_Callback_m1563474383 (GetCustomizationWordsRequest_t110549728 * __this, GetCustomizationWordsCallback_t2709615664 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordsRequest::set_CustomizationID(System.String)
extern "C"  void GetCustomizationWordsRequest_set_CustomizationID_m3489863342 (GetCustomizationWordsRequest_t110549728 * __this, String_t* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordsRequest::set_Data(System.String)
extern "C"  void GetCustomizationWordsRequest_set_Data_m3341658500 (GetCustomizationWordsRequest_t110549728 * __this, String_t* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Words::.ctor()
extern "C"  void Words__ctor_m314888114 (Words_t2948214629 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordsCallback IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordsRequest::get_Callback()
extern "C"  GetCustomizationWordsCallback_t2709615664 * GetCustomizationWordsRequest_get_Callback_m1338404144 (GetCustomizationWordsRequest_t110549728 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordsRequest::get_Data()
extern "C"  String_t* GetCustomizationWordsRequest_get_Data_m3001775595 (GetCustomizationWordsRequest_t110549728 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordsCallback::Invoke(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Words,System.String)
extern "C"  void GetCustomizationWordsCallback_Invoke_m2042980066 (GetCustomizationWordsCallback_t2709615664 * __this, Words_t2948214629 * ___words0, String_t* ___data1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Words::HasData()
extern "C"  bool Words_HasData_m2703714480 (Words_t2948214629 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordsRequest::.ctor()
extern "C"  void AddCustomizationWordsRequest__ctor_m885260432 (AddCustomizationWordsRequest_t993580433 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordsRequest::set_Callback(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordsCallback)
extern "C"  void AddCustomizationWordsRequest_set_Callback_m244551819 (AddCustomizationWordsRequest_t993580433 * __this, AddCustomizationWordsCallback_t2578578585 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordsRequest::set_Words(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Words)
extern "C"  void AddCustomizationWordsRequest_set_Words_m3960587283 (AddCustomizationWordsRequest_t993580433 * __this, Words_t2948214629 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordsRequest::set_CustomizationID(System.String)
extern "C"  void AddCustomizationWordsRequest_set_CustomizationID_m2223789655 (AddCustomizationWordsRequest_t993580433 * __this, String_t* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordsRequest::set_Data(System.String)
extern "C"  void AddCustomizationWordsRequest_set_Data_m3233382931 (AddCustomizationWordsRequest_t993580433 * __this, String_t* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordsCallback IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordsRequest::get_Callback()
extern "C"  AddCustomizationWordsCallback_t2578578585 * AddCustomizationWordsRequest_get_Callback_m4169137936 (AddCustomizationWordsRequest_t993580433 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordsRequest::get_Data()
extern "C"  String_t* AddCustomizationWordsRequest_get_Data_m2916608766 (AddCustomizationWordsRequest_t993580433 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordsCallback::Invoke(System.Boolean,System.String)
extern "C"  void AddCustomizationWordsCallback_Invoke_m187986097 (AddCustomizationWordsCallback_t2578578585 * __this, bool ___success0, String_t* ___data1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/DeleteCustomizationWordRequest::.ctor()
extern "C"  void DeleteCustomizationWordRequest__ctor_m2141073395 (DeleteCustomizationWordRequest_t3182066146 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/DeleteCustomizationWordRequest::set_Callback(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/OnDeleteCustomizationWordCallback)
extern "C"  void DeleteCustomizationWordRequest_set_Callback_m4219025788 (DeleteCustomizationWordRequest_t3182066146 * __this, OnDeleteCustomizationWordCallback_t2544578463 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/DeleteCustomizationWordRequest::set_CustomizationID(System.String)
extern "C"  void DeleteCustomizationWordRequest_set_CustomizationID_m2192251384 (DeleteCustomizationWordRequest_t3182066146 * __this, String_t* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/DeleteCustomizationWordRequest::set_Word(System.String)
extern "C"  void DeleteCustomizationWordRequest_set_Word_m2052926514 (DeleteCustomizationWordRequest_t3182066146 * __this, String_t* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/DeleteCustomizationWordRequest::set_Data(System.String)
extern "C"  void DeleteCustomizationWordRequest_set_Data_m4265011282 (DeleteCustomizationWordRequest_t3182066146 * __this, String_t* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Format(System.String,System.Object,System.Object)
extern "C"  String_t* String_Format_m1811873526 (RuntimeObject * __this /* static, unused */, String_t* p0, RuntimeObject * p1, RuntimeObject * p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/OnDeleteCustomizationWordCallback IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/DeleteCustomizationWordRequest::get_Callback()
extern "C"  OnDeleteCustomizationWordCallback_t2544578463 * DeleteCustomizationWordRequest_get_Callback_m18553317 (DeleteCustomizationWordRequest_t3182066146 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/DeleteCustomizationWordRequest::get_Data()
extern "C"  String_t* DeleteCustomizationWordRequest_get_Data_m3747700475 (DeleteCustomizationWordRequest_t3182066146 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/OnDeleteCustomizationWordCallback::Invoke(System.Boolean,System.String)
extern "C"  void OnDeleteCustomizationWordCallback_Invoke_m226387231 (OnDeleteCustomizationWordCallback_t2544578463 * __this, bool ___success0, String_t* ___data1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordRequest::.ctor()
extern "C"  void GetCustomizationWordRequest__ctor_m3012566346 (GetCustomizationWordRequest_t1280810157 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordRequest::set_Callback(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordCallback)
extern "C"  void GetCustomizationWordRequest_set_Callback_m1678004927 (GetCustomizationWordRequest_t1280810157 * __this, GetCustomizationWordCallback_t2485962781 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordRequest::set_CustomizationID(System.String)
extern "C"  void GetCustomizationWordRequest_set_CustomizationID_m3921965539 (GetCustomizationWordRequest_t1280810157 * __this, String_t* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordRequest::set_Word(System.String)
extern "C"  void GetCustomizationWordRequest_set_Word_m3821257485 (GetCustomizationWordRequest_t1280810157 * __this, String_t* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordRequest::set_Data(System.String)
extern "C"  void GetCustomizationWordRequest_set_Data_m1038605743 (GetCustomizationWordRequest_t1280810157 * __this, String_t* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Translation::.ctor()
extern "C"  void Translation__ctor_m1959656352 (Translation_t2174867539 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordCallback IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordRequest::get_Callback()
extern "C"  GetCustomizationWordCallback_t2485962781 * GetCustomizationWordRequest_get_Callback_m3381062138 (GetCustomizationWordRequest_t1280810157 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordRequest::get_Data()
extern "C"  String_t* GetCustomizationWordRequest_get_Data_m1350863650 (GetCustomizationWordRequest_t1280810157 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordCallback::Invoke(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Translation,System.String)
extern "C"  void GetCustomizationWordCallback_Invoke_m3773263563 (GetCustomizationWordCallback_t2485962781 * __this, Translation_t2174867539 * ___translation0, String_t* ___data1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String,System.String)
extern "C"  String_t* String_Concat_m612901809 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, String_t* p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordRequest::.ctor()
extern "C"  void AddCustomizationWordRequest__ctor_m2967135291 (AddCustomizationWordRequest_t864480132 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordRequest::set_Callback(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordCallback)
extern "C"  void AddCustomizationWordRequest_set_Callback_m3706326815 (AddCustomizationWordRequest_t864480132 * __this, AddCustomizationWordCallback_t3775539180 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordRequest::set_CustomizationID(System.String)
extern "C"  void AddCustomizationWordRequest_set_CustomizationID_m904633046 (AddCustomizationWordRequest_t864480132 * __this, String_t* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordRequest::set_Word(System.String)
extern "C"  void AddCustomizationWordRequest_set_Word_m1970087552 (AddCustomizationWordRequest_t864480132 * __this, String_t* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordRequest::set_Translation(System.String)
extern "C"  void AddCustomizationWordRequest_set_Translation_m4095350635 (AddCustomizationWordRequest_t864480132 * __this, String_t* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordRequest::set_Data(System.String)
extern "C"  void AddCustomizationWordRequest_set_Data_m1163374356 (AddCustomizationWordRequest_t864480132 * __this, String_t* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordCallback IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordRequest::get_Callback()
extern "C"  AddCustomizationWordCallback_t3775539180 * AddCustomizationWordRequest_get_Callback_m793839120 (AddCustomizationWordRequest_t864480132 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordRequest::get_Data()
extern "C"  String_t* AddCustomizationWordRequest_get_Data_m2144573351 (AddCustomizationWordRequest_t864480132 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordCallback::Invoke(System.Boolean,System.String)
extern "C"  void AddCustomizationWordCallback_Invoke_m2326839634 (AddCustomizationWordCallback_t3775539180 * __this, bool ___success0, String_t* ___data1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// IBM.Watson.DeveloperCloud.Utilities.Config IBM.Watson.DeveloperCloud.Utilities.Config::get_Instance()
extern "C"  Config_t3637807320 * Config_get_Instance_m430784956 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// IBM.Watson.DeveloperCloud.Utilities.Config/CredentialInfo IBM.Watson.DeveloperCloud.Utilities.Config::FindCredentials(System.String)
extern "C"  CredentialInfo_t34154441 * Config_FindCredentials_m3250710905 (Config_t3637807320 * __this, String_t* ___serviceID0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/CheckServiceStatus::.ctor(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech,IBM.Watson.DeveloperCloud.Services.ServiceStatus)
extern "C"  void CheckServiceStatus__ctor_m3941399121 (CheckServiceStatus_t3651453794 * __this, TextToSpeech_t3349357562 * ___service0, ServiceStatus_t1443707987 * ___callback1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.ServiceStatus::Invoke(System.String,System.Boolean)
extern "C"  void ServiceStatus_Invoke_m1869726628 (ServiceStatus_t1443707987 * __this, String_t* ___serviceID0, bool ___active1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void FullSerializer.fsSerializer::.ctor()
extern "C"  void fsSerializer__ctor_m3368490715 (fsSerializer_t4193731081 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::.ctor()
extern "C"  void Request__ctor_m1928448681 (Request_t466816980 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetVoicesCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetVoicesCallback__ctor_m717172724 (GetVoicesCallback_t3012885511 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech::GetVoices(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetVoicesCallback)
extern "C"  bool TextToSpeech_GetVoices_m2898245765 (TextToSpeech_t3349357562 * __this, GetVoicesCallback_t3012885511 * ___callback0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Object System.Delegate::get_Target()
extern "C"  RuntimeObject * Delegate_get_Target_m896795953 (Delegate_t3022476291 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Voice[] IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Voices::get_voices()
extern "C"  VoiceU5BU5D_t260285181* Voices_get_voices_m849753461 (Voices_t4221445733 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Word[] IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Words::get_words()
extern "C"  WordU5BU5D_t3686373631* Words_get_words_m3194785885 (Words_t2948214629 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneCategory[] IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.DocumentTone::get_tone_categories()
extern "C"  ToneCategoryU5BU5D_t3565596459* DocumentTone_get_tone_categories_m3219943273 (DocumentTone_t2537937785 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneCategory[] IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.SentenceTone::get_tone_categories()
extern "C"  ToneCategoryU5BU5D_t3565596459* SentenceTone_get_tone_categories_m2735460483 (SentenceTone_t3234952115 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.Tone[] IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneCategory::get_tones()
extern "C"  ToneU5BU5D_t4171600311* ToneCategory_get_tones_m2345877352 (ToneCategory_t3724297374 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Double IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.Tone::get_score()
extern "C"  double Tone_get_score_m3537196787 (Tone_t1138009090 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.Tone::get_tone_name()
extern "C"  String_t* Tone_get_tone_name_m4125480123 (Tone_t1138009090 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneCategory::get_category_name()
extern "C"  String_t* ToneCategory_get_category_name_m1377209155 (ToneCategory_t3724297374 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.SentenceTone::get_sentence_id()
extern "C"  int32_t SentenceTone_get_sentence_id_m1350765144 (SentenceTone_t3234952115 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.SentenceTone::get_input_from()
extern "C"  int32_t SentenceTone_get_input_from_m4195132438 (SentenceTone_t3234952115 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.SentenceTone::get_input_to()
extern "C"  int32_t SentenceTone_get_input_to_m1797854435 (SentenceTone_t3234952115 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.SentenceTone::get_text()
extern "C"  String_t* SentenceTone_get_text_m1133044659 (SentenceTone_t3234952115 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Format(System.String,System.Object[])
extern "C"  String_t* String_Format_m1263743648 (RuntimeObject * __this /* static, unused */, String_t* p0, ObjectU5BU5D_t3614634134* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.Tone::get_tone_id()
extern "C"  String_t* Tone_get_tone_id_m1698165445 (Tone_t1138009090 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Format(System.String,System.Object,System.Object,System.Object)
extern "C"  String_t* String_Format_m4262916296 (RuntimeObject * __this /* static, unused */, String_t* p0, RuntimeObject * p1, RuntimeObject * p2, RuntimeObject * p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzer/GetToneAnalyzerRequest::.ctor()
extern "C"  void GetToneAnalyzerRequest__ctor_m3143334739 (GetToneAnalyzerRequest_t1769916618 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzer/GetToneAnalyzerRequest::set_Callback(IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzer/OnGetToneAnalyzed)
extern "C"  void GetToneAnalyzerRequest_set_Callback_m1849301549 (GetToneAnalyzerRequest_t1769916618 * __this, OnGetToneAnalyzed_t3813655652 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzer/GetToneAnalyzerRequest::set_Data(System.String)
extern "C"  void GetToneAnalyzerRequest_set_Data_m3436767482 (GetToneAnalyzerRequest_t1769916618 * __this, String_t* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzerResponse::.ctor()
extern "C"  void ToneAnalyzerResponse__ctor_m604375646 (ToneAnalyzerResponse_t3391014235 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzer/OnGetToneAnalyzed IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzer/GetToneAnalyzerRequest::get_Callback()
extern "C"  OnGetToneAnalyzed_t3813655652 * GetToneAnalyzerRequest_get_Callback_m3110919162 (GetToneAnalyzerRequest_t1769916618 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzer/GetToneAnalyzerRequest::get_Data()
extern "C"  String_t* GetToneAnalyzerRequest_get_Data_m4150767635 (GetToneAnalyzerRequest_t1769916618 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzer/OnGetToneAnalyzed::Invoke(IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzerResponse,System.String)
extern "C"  void OnGetToneAnalyzed_Invoke_m316833546 (OnGetToneAnalyzed_t3813655652 * __this, ToneAnalyzerResponse_t3391014235 * ___resp0, String_t* ___data1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzer/CheckServiceStatus::.ctor(IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzer,IBM.Watson.DeveloperCloud.Services.ServiceStatus)
extern "C"  void CheckServiceStatus__ctor_m57170065 (CheckServiceStatus_t1108037484 * __this, ToneAnalyzer_t1356110496 * ___service0, ServiceStatus_t1443707987 * ___callback1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzer/OnGetToneAnalyzed::.ctor(System.Object,System.IntPtr)
extern "C"  void OnGetToneAnalyzed__ctor_m3782744621 (OnGetToneAnalyzed_t3813655652 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzer::GetToneAnalyze(IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzer/OnGetToneAnalyzed,System.String,System.String)
extern "C"  bool ToneAnalyzer_GetToneAnalyze_m2599887229 (ToneAnalyzer_t1356110496 * __this, OnGetToneAnalyzed_t3813655652 * ___callback0, String_t* ___text1, String_t* ___data2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.SentenceTone[] IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzerResponse::get_sentences_tone()
extern "C"  SentenceToneU5BU5D_t255016994* ToneAnalyzerResponse_get_sentences_tone_m1788818906 (ToneAnalyzerResponse_t3391014235 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.DocumentTone IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzerResponse::get_document_tone()
extern "C"  DocumentTone_t2537937785 * ToneAnalyzerResponse_get_document_tone_m350112175 (ToneAnalyzerResponse_t3391014235 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneCategory::get_category_id()
extern "C"  String_t* ToneCategory_get_category_id_m1048041785 (ToneCategory_t3724297374 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Range::.ctor()
extern "C"  void Range__ctor_m3717125291 (Range_t2396059803 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics/GetDilemmaRequest::.ctor()
extern "C"  void GetDilemmaRequest__ctor_m1840711466 (GetDilemmaRequest_t37205599 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics/GetDilemmaRequest::set_Callback(IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics/OnDilemma)
extern "C"  void GetDilemmaRequest_set_Callback_m2673609369 (GetDilemmaRequest_t37205599 * __this, OnDilemma_t401820821 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Boolean::ToString()
extern "C"  String_t* Boolean_ToString_m1253164328 (bool* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// FullSerializer.fsResult FullSerializer.fsSerializer::TrySerialize<IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Problem>(T,FullSerializer.fsData&)
#define fsSerializer_TrySerialize_TisProblem_t2814813345_m3668382148(__this, ___instance0, ___data1, method) ((  fsResult_t862419890  (*) (fsSerializer_t4193731081 *, Problem_t2814813345 *, fsData_t2583805605 **, const RuntimeMethod*))fsSerializer_TrySerialize_TisRuntimeObject_m4289598478_gshared)(__this, ___instance0, ___data1, method)
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.DilemmasResponse::.ctor()
extern "C"  void DilemmasResponse__ctor_m2982690117 (DilemmasResponse_t3767705817 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics/OnDilemma IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics/GetDilemmaRequest::get_Callback()
extern "C"  OnDilemma_t401820821 * GetDilemmaRequest_get_Callback_m420212378 (GetDilemmaRequest_t37205599 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics/OnDilemma::Invoke(IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.DilemmasResponse)
extern "C"  void OnDilemma_Invoke_m3166755296 (OnDilemma_t401820821 * __this, DilemmasResponse_t3767705817 * ___resp0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics/CheckServiceStatus::.ctor(IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics,IBM.Watson.DeveloperCloud.Services.ServiceStatus)
extern "C"  void CheckServiceStatus__ctor_m3332446737 (CheckServiceStatus_t584740610 * __this, TradeoffAnalytics_t100002595 * ___service0, ServiceStatus_t1443707987 * ___callback1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Problem::.ctor()
extern "C"  void Problem__ctor_m2287231115 (Problem_t2814813345 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Problem::set_subject(System.String)
extern "C"  void Problem_set_subject_m3312909546 (Problem_t2814813345 * __this, String_t* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column>::.ctor()
#define List_1__ctor_m1814123416(__this, method) ((  void (*) (List_1_t1981607956 *, const RuntimeMethod*))List_1__ctor_m310736118_gshared)(__this, method)
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column::.ctor()
extern "C"  void Column__ctor_m3168083222 (Column_t2612486824 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column::set_key(System.String)
extern "C"  void Column_set_key_m3783075932 (Column_t2612486824 * __this, String_t* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column::set_type(System.String)
extern "C"  void Column_set_type_m3872918225 (Column_t2612486824 * __this, String_t* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column::set_goal(System.String)
extern "C"  void Column_set_goal_m1264124692 (Column_t2612486824 * __this, String_t* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column::set_is_objective(System.Boolean)
extern "C"  void Column_set_is_objective_m3607883288 (Column_t2612486824 * __this, bool ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.ValueRange::.ctor()
extern "C"  void ValueRange__ctor_m3640495210 (ValueRange_t543481982 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column::set_range(IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Range)
extern "C"  void Column_set_range_m3088237292 (Column_t2612486824 * __this, Range_t2396059803 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Range IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column::get_range()
extern "C"  Range_t2396059803 * Column_get_range_m2413620963 (Column_t2612486824 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.ValueRange::set_high(System.Double)
extern "C"  void ValueRange_set_high_m97411425 (ValueRange_t543481982 * __this, double ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.ValueRange::set_low(System.Double)
extern "C"  void ValueRange_set_low_m1674791863 (ValueRange_t543481982 * __this, double ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column>::Add(!0)
#define List_1_Add_m952068700(__this, p0, method) ((  void (*) (List_1_t1981607956 *, Column_t2612486824 *, const RuntimeMethod*))List_1_Add_m4157722533_gshared)(__this, p0, method)
// !0[] System.Collections.Generic.List`1<IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column>::ToArray()
#define List_1_ToArray_m3175624038(__this, method) ((  ColumnU5BU5D_t4200576441* (*) (List_1_t1981607956 *, const RuntimeMethod*))List_1_ToArray_m546658539_gshared)(__this, method)
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Problem::set_columns(IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column[])
extern "C"  void Problem_set_columns_m805733870 (Problem_t2814813345 * __this, ColumnU5BU5D_t4200576441* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Option>::.ctor()
#define List_1__ctor_m3074099389(__this, method) ((  void (*) (List_1_t1144248177 *, const RuntimeMethod*))List_1__ctor_m310736118_gshared)(__this, method)
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Option::.ctor()
extern "C"  void Option__ctor_m78809847 (Option_t1775127045 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Option::set_key(System.String)
extern "C"  void Option_set_key_m2662544107 (Option_t1775127045 * __this, String_t* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics/PingDataValue::.ctor()
extern "C"  void PingDataValue__ctor_m1063649603 (PingDataValue_t526342856 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Option::set_values(IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.ApplicationDataValue)
extern "C"  void Option_set_values_m1427637990 (Option_t1775127045 * __this, ApplicationDataValue_t1721671971 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.ApplicationDataValue IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Option::get_values()
extern "C"  ApplicationDataValue_t1721671971 * Option_get_values_m1682394903 (Option_t1775127045 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics/PingDataValue::set_ping(System.Double)
extern "C"  void PingDataValue_set_ping_m939741896 (PingDataValue_t526342856 * __this, double ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Option>::Add(!0)
#define List_1_Add_m2195314017(__this, p0, method) ((  void (*) (List_1_t1144248177 *, Option_t1775127045 *, const RuntimeMethod*))List_1_Add_m4157722533_gshared)(__this, p0, method)
// !0[] System.Collections.Generic.List`1<IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Option>::ToArray()
#define List_1_ToArray_m1652548499(__this, method) ((  OptionU5BU5D_t3811736648* (*) (List_1_t1144248177 *, const RuntimeMethod*))List_1_ToArray_m546658539_gshared)(__this, method)
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Problem::set_options(IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Option[])
extern "C"  void Problem_set_options_m3598639726 (Problem_t2814813345 * __this, OptionU5BU5D_t3811736648* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics/OnDilemma::.ctor(System.Object,System.IntPtr)
extern "C"  void OnDilemma__ctor_m1311248430 (OnDilemma_t401820821 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics::GetDilemma(IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics/OnDilemma,IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Problem,System.Boolean)
extern "C"  bool TradeoffAnalytics_GetDilemma_m3487116940 (TradeoffAnalytics_t100002595 * __this, OnDilemma_t401820821 * ___callback0, Problem_t2814813345 * ___problem1, bool ___generateVisualization2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics/CheckServiceStatus::OnFailure(System.String)
extern "C"  void CheckServiceStatus_OnFailure_m1533919926 (CheckServiceStatus_t584740610 * __this, String_t* ___msg0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.ApplicationData::.ctor()
extern "C"  void ApplicationData__ctor_m1879024432 (ApplicationData_t1885408412 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.ApplicationDataValue::.ctor()
extern "C"  void ApplicationDataValue__ctor_m4048002801 (ApplicationDataValue_t1721671971 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.TimeStamp::.ctor()
extern "C"  void TimeStamp__ctor_m290886511 (TimeStamp_t82276870 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.TimeStamp::get_Word()
extern "C"  String_t* TimeStamp_get_Word_m2793518109 (TimeStamp_t82276870 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CWordU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.TimeStamp::set_Word(System.String)
extern "C"  void TimeStamp_set_Word_m2485172742 (TimeStamp_t82276870 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CWordU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Double IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.TimeStamp::get_Start()
extern "C"  double TimeStamp_get_Start_m3994964963 (TimeStamp_t82276870 * __this, const RuntimeMethod* method)
{
	{
		double L_0 = __this->get_U3CStartU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.TimeStamp::set_Start(System.Double)
extern "C"  void TimeStamp_set_Start_m2999457816 (TimeStamp_t82276870 * __this, double ___value0, const RuntimeMethod* method)
{
	{
		double L_0 = ___value0;
		__this->set_U3CStartU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Double IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.TimeStamp::get_End()
extern "C"  double TimeStamp_get_End_m3662188760 (TimeStamp_t82276870 * __this, const RuntimeMethod* method)
{
	{
		double L_0 = __this->get_U3CEndU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.TimeStamp::set_End(System.Double)
extern "C"  void TimeStamp_set_End_m1465741047 (TimeStamp_t82276870 * __this, double ___value0, const RuntimeMethod* method)
{
	{
		double L_0 = ___value0;
		__this->set_U3CEndU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Word::.ctor()
extern "C"  void Word__ctor_m368574907 (Word_t3204175642 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Word::get_word()
extern "C"  String_t* Word_get_word_m3066129821 (Word_t3204175642 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CwordU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Word::set_word(System.String)
extern "C"  void Word_set_word_m290181806 (Word_t3204175642 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CwordU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String[] IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Word::get_sounds_like()
extern "C"  StringU5BU5D_t1642385972* Word_get_sounds_like_m3651574901 (Word_t3204175642 * __this, const RuntimeMethod* method)
{
	{
		StringU5BU5D_t1642385972* L_0 = __this->get_U3Csounds_likeU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Word::set_sounds_like(System.String[])
extern "C"  void Word_set_sounds_like_m2151145554 (Word_t3204175642 * __this, StringU5BU5D_t1642385972* ___value0, const RuntimeMethod* method)
{
	{
		StringU5BU5D_t1642385972* L_0 = ___value0;
		__this->set_U3Csounds_likeU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Word::get_display_as()
extern "C"  String_t* Word_get_display_as_m1633921488 (Word_t3204175642 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3Cdisplay_asU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Word::set_display_as(System.String)
extern "C"  void Word_set_display_as_m3321396127 (Word_t3204175642 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3Cdisplay_asU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordAlternativeResult::.ctor()
extern "C"  void WordAlternativeResult__ctor_m4034456913 (WordAlternativeResult_t3127007908 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Double IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordAlternativeResult::get_confidence()
extern "C"  double WordAlternativeResult_get_confidence_m1549862313 (WordAlternativeResult_t3127007908 * __this, const RuntimeMethod* method)
{
	{
		double L_0 = __this->get_U3CconfidenceU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordAlternativeResult::set_confidence(System.Double)
extern "C"  void WordAlternativeResult_set_confidence_m3264507840 (WordAlternativeResult_t3127007908 * __this, double ___value0, const RuntimeMethod* method)
{
	{
		double L_0 = ___value0;
		__this->set_U3CconfidenceU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordAlternativeResult::get_word()
extern "C"  String_t* WordAlternativeResult_get_word_m2802802843 (WordAlternativeResult_t3127007908 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CwordU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordAlternativeResult::set_word(System.String)
extern "C"  void WordAlternativeResult_set_word_m2326346176 (WordAlternativeResult_t3127007908 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CwordU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordAlternativeResults::.ctor()
extern "C"  void WordAlternativeResults__ctor_m3669567600 (WordAlternativeResults_t1270965811 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Double IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordAlternativeResults::get_start_time()
extern "C"  double WordAlternativeResults_get_start_time_m2260510024 (WordAlternativeResults_t1270965811 * __this, const RuntimeMethod* method)
{
	{
		double L_0 = __this->get_U3Cstart_timeU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordAlternativeResults::set_start_time(System.Double)
extern "C"  void WordAlternativeResults_set_start_time_m4055927303 (WordAlternativeResults_t1270965811 * __this, double ___value0, const RuntimeMethod* method)
{
	{
		double L_0 = ___value0;
		__this->set_U3Cstart_timeU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Double IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordAlternativeResults::get_end_time()
extern "C"  double WordAlternativeResults_get_end_time_m1961374871 (WordAlternativeResults_t1270965811 * __this, const RuntimeMethod* method)
{
	{
		double L_0 = __this->get_U3Cend_timeU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordAlternativeResults::set_end_time(System.Double)
extern "C"  void WordAlternativeResults_set_end_time_m765490074 (WordAlternativeResults_t1270965811 * __this, double ___value0, const RuntimeMethod* method)
{
	{
		double L_0 = ___value0;
		__this->set_U3Cend_timeU3Ek__BackingField_1(L_0);
		return;
	}
}
// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordAlternativeResult[] IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordAlternativeResults::get_alternatives()
extern "C"  WordAlternativeResultU5BU5D_t598168141* WordAlternativeResults_get_alternatives_m4100153518 (WordAlternativeResults_t1270965811 * __this, const RuntimeMethod* method)
{
	{
		WordAlternativeResultU5BU5D_t598168141* L_0 = __this->get_U3CalternativesU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordAlternativeResults::set_alternatives(IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordAlternativeResult[])
extern "C"  void WordAlternativeResults_set_alternatives_m1407580955 (WordAlternativeResults_t1270965811 * __this, WordAlternativeResultU5BU5D_t598168141* ___value0, const RuntimeMethod* method)
{
	{
		WordAlternativeResultU5BU5D_t598168141* L_0 = ___value0;
		__this->set_U3CalternativesU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordConfidence::.ctor()
extern "C"  void WordConfidence__ctor_m4002530679 (WordConfidence_t2757029394 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordConfidence::get_Word()
extern "C"  String_t* WordConfidence_get_Word_m1536419173 (WordConfidence_t2757029394 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CWordU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordConfidence::set_Word(System.String)
extern "C"  void WordConfidence_set_Word_m3122878146 (WordConfidence_t2757029394 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CWordU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Double IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordConfidence::get_Confidence()
extern "C"  double WordConfidence_get_Confidence_m1334396551 (WordConfidence_t2757029394 * __this, const RuntimeMethod* method)
{
	{
		double L_0 = __this->get_U3CConfidenceU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordConfidence::set_Confidence(System.Double)
extern "C"  void WordConfidence_set_Confidence_m795116246 (WordConfidence_t2757029394 * __this, double ___value0, const RuntimeMethod* method)
{
	{
		double L_0 = ___value0;
		__this->set_U3CConfidenceU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordData::.ctor()
extern "C"  void WordData__ctor_m188833931 (WordData_t612265514 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordData::get_word()
extern "C"  String_t* WordData_get_word_m1760735853 (WordData_t612265514 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CwordU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordData::set_word(System.String)
extern "C"  void WordData_set_word_m1363318590 (WordData_t612265514 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CwordU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String[] IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordData::get_sounds_like()
extern "C"  StringU5BU5D_t1642385972* WordData_get_sounds_like_m3571889925 (WordData_t612265514 * __this, const RuntimeMethod* method)
{
	{
		StringU5BU5D_t1642385972* L_0 = __this->get_U3Csounds_likeU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordData::set_sounds_like(System.String[])
extern "C"  void WordData_set_sounds_like_m3705778626 (WordData_t612265514 * __this, StringU5BU5D_t1642385972* ___value0, const RuntimeMethod* method)
{
	{
		StringU5BU5D_t1642385972* L_0 = ___value0;
		__this->set_U3Csounds_likeU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordData::get_display_as()
extern "C"  String_t* WordData_get_display_as_m3430812128 (WordData_t612265514 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3Cdisplay_asU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordData::set_display_as(System.String)
extern "C"  void WordData_set_display_as_m2249358831 (WordData_t612265514 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3Cdisplay_asU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String[] IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordData::get_source()
extern "C"  StringU5BU5D_t1642385972* WordData_get_source_m2776118154 (WordData_t612265514 * __this, const RuntimeMethod* method)
{
	{
		StringU5BU5D_t1642385972* L_0 = __this->get_U3CsourceU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordData::set_source(System.String[])
extern "C"  void WordData_set_source_m1113763287 (WordData_t612265514 * __this, StringU5BU5D_t1642385972* ___value0, const RuntimeMethod* method)
{
	{
		StringU5BU5D_t1642385972* L_0 = ___value0;
		__this->set_U3CsourceU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordData::get_error()
extern "C"  String_t* WordData_get_error_m1809855039 (WordData_t612265514 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CerrorU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordData::set_error(System.String)
extern "C"  void WordData_set_error_m909001678 (WordData_t612265514 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CerrorU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Words::.ctor()
extern "C"  void Words__ctor_m2630998802 (Words_t4062187109 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Word[] IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Words::get_words()
extern "C"  WordU5BU5D_t2800973631* Words_get_words_m199299741 (Words_t4062187109 * __this, const RuntimeMethod* method)
{
	{
		WordU5BU5D_t2800973631* L_0 = __this->get_U3CwordsU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Words::set_words(IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Word[])
extern "C"  void Words_set_words_m4017667900 (Words_t4062187109 * __this, WordU5BU5D_t2800973631* ___value0, const RuntimeMethod* method)
{
	{
		WordU5BU5D_t2800973631* L_0 = ___value0;
		__this->set_U3CwordsU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordsList::.ctor()
extern "C"  void WordsList__ctor_m3765143518 (WordsList_t29283159 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordData[] IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordsList::get_words()
extern "C"  WordDataU5BU5D_t1726140911* WordsList_get_words_m1064960151 (WordsList_t29283159 * __this, const RuntimeMethod* method)
{
	{
		WordDataU5BU5D_t1726140911* L_0 = __this->get_U3CwordsU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordsList::set_words(IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordData[])
extern "C"  void WordsList_set_words_m2239507620 (WordsList_t29283159 * __this, WordDataU5BU5D_t1726140911* ___value0, const RuntimeMethod* method)
{
	{
		WordDataU5BU5D_t1726140911* L_0 = ___value0;
		__this->set_U3CwordsU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordType::.ctor()
extern "C"  void WordType__ctor_m1973210899 (WordType_t1970187882 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordTypeToAdd::.ctor()
extern "C"  void WordTypeToAdd__ctor_m1375225341 (WordTypeToAdd_t2941578038 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Customization::.ctor()
extern "C"  void Customization__ctor_m1020302414 (Customization_t2261013253 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Customization::get_customization_id()
extern "C"  String_t* Customization_get_customization_id_m2343239751 (Customization_t2261013253 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3Ccustomization_idU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Customization::set_customization_id(System.String)
extern "C"  void Customization_set_customization_id_m3071828592 (Customization_t2261013253 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3Ccustomization_idU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Customization::get_name()
extern "C"  String_t* Customization_get_name_m2788779735 (Customization_t2261013253 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CnameU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Customization::set_name(System.String)
extern "C"  void Customization_set_name_m4270485628 (Customization_t2261013253 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CnameU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Customization::get_language()
extern "C"  String_t* Customization_get_language_m785628378 (Customization_t2261013253 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3ClanguageU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Customization::set_language(System.String)
extern "C"  void Customization_set_language_m3770128677 (Customization_t2261013253 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3ClanguageU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Customization::get_owner()
extern "C"  String_t* Customization_get_owner_m378870385 (Customization_t2261013253 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CownerU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Customization::set_owner(System.String)
extern "C"  void Customization_set_owner_m2828013224 (Customization_t2261013253 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CownerU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Double IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Customization::get_created()
extern "C"  double Customization_get_created_m4268357694 (Customization_t2261013253 * __this, const RuntimeMethod* method)
{
	{
		double L_0 = __this->get_U3CcreatedU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Customization::set_created(System.Double)
extern "C"  void Customization_set_created_m2014543479 (Customization_t2261013253 * __this, double ___value0, const RuntimeMethod* method)
{
	{
		double L_0 = ___value0;
		__this->set_U3CcreatedU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Double IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Customization::get_last_modified()
extern "C"  double Customization_get_last_modified_m328989972 (Customization_t2261013253 * __this, const RuntimeMethod* method)
{
	{
		double L_0 = __this->get_U3Clast_modifiedU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Customization::set_last_modified(System.Double)
extern "C"  void Customization_set_last_modified_m1263020135 (Customization_t2261013253 * __this, double ___value0, const RuntimeMethod* method)
{
	{
		double L_0 = ___value0;
		__this->set_U3Clast_modifiedU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Customization::get_description()
extern "C"  String_t* Customization_get_description_m891541724 (Customization_t2261013253 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CdescriptionU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Customization::set_description(System.String)
extern "C"  void Customization_set_description_m2856271965 (Customization_t2261013253 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CdescriptionU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.Boolean IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Customization::HasData()
extern "C"  bool Customization_HasData_m1527546784 (Customization_t2261013253 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Customization_HasData_m1527546784_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = Customization_get_customization_id_m2343239751(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomizationID::.ctor()
extern "C"  void CustomizationID__ctor_m4015391793 (CustomizationID_t344279642 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomizationID::get_customization_id()
extern "C"  String_t* CustomizationID_get_customization_id_m421809002 (CustomizationID_t344279642 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3Ccustomization_idU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomizationID::set_customization_id(System.String)
extern "C"  void CustomizationID_set_customization_id_m1105100373 (CustomizationID_t344279642 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3Ccustomization_idU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Customizations::.ctor()
extern "C"  void Customizations__ctor_m1298341255 (Customizations_t482380184 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Customization[] IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Customizations::get_customizations()
extern "C"  CustomizationU5BU5D_t626929480* Customizations_get_customizations_m4036906150 (Customizations_t482380184 * __this, const RuntimeMethod* method)
{
	{
		CustomizationU5BU5D_t626929480* L_0 = __this->get_U3CcustomizationsU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Customizations::set_customizations(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Customization[])
extern "C"  void Customizations_set_customizations_m842306085 (Customizations_t482380184 * __this, CustomizationU5BU5D_t626929480* ___value0, const RuntimeMethod* method)
{
	{
		CustomizationU5BU5D_t626929480* L_0 = ___value0;
		__this->set_U3CcustomizationsU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Boolean IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Customizations::HasData()
extern "C"  bool Customizations_HasData_m3919329153 (Customizations_t482380184 * __this, const RuntimeMethod* method)
{
	int32_t G_B3_0 = 0;
	{
		CustomizationU5BU5D_t626929480* L_0 = Customizations_get_customizations_m4036906150(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		CustomizationU5BU5D_t626929480* L_1 = Customizations_get_customizations_m4036906150(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		G_B3_0 = ((((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_1)->max_length))))) > ((int32_t)0))? 1 : 0);
		goto IL_0019;
	}

IL_0018:
	{
		G_B3_0 = 0;
	}

IL_0019:
	{
		return (bool)G_B3_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomizationWords::.ctor()
extern "C"  void CustomizationWords__ctor_m732022757 (CustomizationWords_t949522428 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomizationWords::get_customization_id()
extern "C"  String_t* CustomizationWords_get_customization_id_m2835936362 (CustomizationWords_t949522428 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3Ccustomization_idU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomizationWords::set_customization_id(System.String)
extern "C"  void CustomizationWords_set_customization_id_m4040455097 (CustomizationWords_t949522428 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3Ccustomization_idU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomizationWords::get_name()
extern "C"  String_t* CustomizationWords_get_name_m4072399994 (CustomizationWords_t949522428 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CnameU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomizationWords::set_name(System.String)
extern "C"  void CustomizationWords_set_name_m3654340701 (CustomizationWords_t949522428 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CnameU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomizationWords::get_language()
extern "C"  String_t* CustomizationWords_get_language_m284007355 (CustomizationWords_t949522428 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3ClanguageU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomizationWords::set_language(System.String)
extern "C"  void CustomizationWords_set_language_m3167556970 (CustomizationWords_t949522428 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3ClanguageU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomizationWords::get_owner()
extern "C"  String_t* CustomizationWords_get_owner_m383724962 (CustomizationWords_t949522428 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CownerU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomizationWords::set_owner(System.String)
extern "C"  void CustomizationWords_set_owner_m2582883707 (CustomizationWords_t949522428 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CownerU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Double IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomizationWords::get_created()
extern "C"  double CustomizationWords_get_created_m3510968409 (CustomizationWords_t949522428 * __this, const RuntimeMethod* method)
{
	{
		double L_0 = __this->get_U3CcreatedU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomizationWords::set_created(System.Double)
extern "C"  void CustomizationWords_set_created_m2569534638 (CustomizationWords_t949522428 * __this, double ___value0, const RuntimeMethod* method)
{
	{
		double L_0 = ___value0;
		__this->set_U3CcreatedU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Double IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomizationWords::get_last_modified()
extern "C"  double CustomizationWords_get_last_modified_m602192953 (CustomizationWords_t949522428 * __this, const RuntimeMethod* method)
{
	{
		double L_0 = __this->get_U3Clast_modifiedU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomizationWords::set_last_modified(System.Double)
extern "C"  void CustomizationWords_set_last_modified_m1090189384 (CustomizationWords_t949522428 * __this, double ___value0, const RuntimeMethod* method)
{
	{
		double L_0 = ___value0;
		__this->set_U3Clast_modifiedU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomizationWords::get_description()
extern "C"  String_t* CustomizationWords_get_description_m4002541947 (CustomizationWords_t949522428 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CdescriptionU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomizationWords::set_description(System.String)
extern "C"  void CustomizationWords_set_description_m3447624824 (CustomizationWords_t949522428 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CdescriptionU3Ek__BackingField_6(L_0);
		return;
	}
}
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Word[] IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomizationWords::get_words()
extern "C"  WordU5BU5D_t3686373631* CustomizationWords_get_words_m3957134490 (CustomizationWords_t949522428 * __this, const RuntimeMethod* method)
{
	{
		WordU5BU5D_t3686373631* L_0 = __this->get_U3CwordsU3Ek__BackingField_7();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomizationWords::set_words(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Word[])
extern "C"  void CustomizationWords_set_words_m1670489847 (CustomizationWords_t949522428 * __this, WordU5BU5D_t3686373631* ___value0, const RuntimeMethod* method)
{
	{
		WordU5BU5D_t3686373631* L_0 = ___value0;
		__this->set_U3CwordsU3Ek__BackingField_7(L_0);
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomVoice::.ctor()
extern "C"  void CustomVoice__ctor_m2182765280 (CustomVoice_t330695553 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomVoice::get_name()
extern "C"  String_t* CustomVoice_get_name_m2186266179 (CustomVoice_t330695553 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CnameU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomVoice::set_name(System.String)
extern "C"  void CustomVoice_set_name_m2044531626 (CustomVoice_t330695553 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CnameU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomVoice::get_language()
extern "C"  String_t* CustomVoice_get_language_m906390320 (CustomVoice_t330695553 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3ClanguageU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomVoice::set_language(System.String)
extern "C"  void CustomVoice_set_language_m2534117753 (CustomVoice_t330695553 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3ClanguageU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomVoice::get_description()
extern "C"  String_t* CustomVoice_get_description_m2465669630 (CustomVoice_t330695553 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CdescriptionU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomVoice::set_description(System.String)
extern "C"  void CustomVoice_set_description_m1558525425 (CustomVoice_t330695553 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CdescriptionU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomVoiceUpdate::.ctor()
extern "C"  void CustomVoiceUpdate__ctor_m2284289921 (CustomVoiceUpdate_t1706248840 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomVoiceUpdate::get_name()
extern "C"  String_t* CustomVoiceUpdate_get_name_m2983265764 (CustomVoiceUpdate_t1706248840 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CnameU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomVoiceUpdate::set_name(System.String)
extern "C"  void CustomVoiceUpdate_set_name_m3089249289 (CustomVoiceUpdate_t1706248840 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CnameU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomVoiceUpdate::get_description()
extern "C"  String_t* CustomVoiceUpdate_get_description_m3140784127 (CustomVoiceUpdate_t1706248840 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CdescriptionU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomVoiceUpdate::set_description(System.String)
extern "C"  void CustomVoiceUpdate_set_description_m3341345424 (CustomVoiceUpdate_t1706248840 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CdescriptionU3Ek__BackingField_1(L_0);
		return;
	}
}
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Word[] IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomVoiceUpdate::get_words()
extern "C"  WordU5BU5D_t3686373631* CustomVoiceUpdate_get_words_m99370996 (CustomVoiceUpdate_t1706248840 * __this, const RuntimeMethod* method)
{
	{
		WordU5BU5D_t3686373631* L_0 = __this->get_U3CwordsU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomVoiceUpdate::set_words(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Word[])
extern "C"  void CustomVoiceUpdate_set_words_m3486023799 (CustomVoiceUpdate_t1706248840 * __this, WordU5BU5D_t3686373631* ___value0, const RuntimeMethod* method)
{
	{
		WordU5BU5D_t3686373631* L_0 = ___value0;
		__this->set_U3CwordsU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Boolean IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomVoiceUpdate::HasData()
extern "C"  bool CustomVoiceUpdate_HasData_m2393568391 (CustomVoiceUpdate_t1706248840 * __this, const RuntimeMethod* method)
{
	int32_t G_B3_0 = 0;
	{
		WordU5BU5D_t3686373631* L_0 = CustomVoiceUpdate_get_words_m99370996(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		WordU5BU5D_t3686373631* L_1 = CustomVoiceUpdate_get_words_m99370996(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		G_B3_0 = ((((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_1)->max_length))))) > ((int32_t)0))? 1 : 0);
		goto IL_0019;
	}

IL_0018:
	{
		G_B3_0 = 0;
	}

IL_0019:
	{
		return (bool)G_B3_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.ErrorModel::.ctor()
extern "C"  void ErrorModel__ctor_m261121612 (ErrorModel_t3210737207 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.ErrorModel::get_error()
extern "C"  String_t* ErrorModel_get_error_m3442744256 (ErrorModel_t3210737207 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CerrorU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.ErrorModel::set_error(System.String)
extern "C"  void ErrorModel_set_error_m3167294049 (ErrorModel_t3210737207 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CerrorU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Int32 IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.ErrorModel::get_code()
extern "C"  int32_t ErrorModel_get_code_m1265953816 (ErrorModel_t3210737207 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_U3CcodeU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.ErrorModel::set_code(System.Int32)
extern "C"  void ErrorModel_set_code_m3709188375 (ErrorModel_t3210737207 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CcodeU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.ErrorModel::get_code_description()
extern "C"  String_t* ErrorModel_get_code_description_m1275866348 (ErrorModel_t3210737207 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3Ccode_descriptionU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.ErrorModel::set_code_description(System.String)
extern "C"  void ErrorModel_set_code_description_m1921330409 (ErrorModel_t3210737207 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3Ccode_descriptionU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Pronunciation::.ctor()
extern "C"  void Pronunciation__ctor_m2815357694 (Pronunciation_t2711344207 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Pronunciation::get_pronunciation()
extern "C"  String_t* Pronunciation_get_pronunciation_m1820531961 (Pronunciation_t2711344207 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CpronunciationU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Pronunciation::set_pronunciation(System.String)
extern "C"  void Pronunciation_set_pronunciation_m2333353158 (Pronunciation_t2711344207 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CpronunciationU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech::.ctor()
extern "C"  void TextToSpeech__ctor_m3182460987 (TextToSpeech_t3349357562 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextToSpeech__ctor_m3182460987_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Dictionary_2_t1569883280 * V_0 = NULL;
	Dictionary_2_t2576372715 * V_1 = NULL;
	{
		__this->set_m_AudioFormat_2(1);
		Dictionary_2_t1569883280 * L_0 = (Dictionary_2_t1569883280 *)il2cpp_codegen_object_new(Dictionary_2_t1569883280_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3248355552(L_0, /*hidden argument*/Dictionary_2__ctor_m3248355552_RuntimeMethod_var);
		V_0 = L_0;
		Dictionary_2_t1569883280 * L_1 = V_0;
		NullCheck(L_1);
		Dictionary_2_Add_m2797377672(L_1, 0, _stringLiteral265785388, /*hidden argument*/Dictionary_2_Add_m2797377672_RuntimeMethod_var);
		Dictionary_2_t1569883280 * L_2 = V_0;
		NullCheck(L_2);
		Dictionary_2_Add_m2797377672(L_2, 1, _stringLiteral12833108, /*hidden argument*/Dictionary_2_Add_m2797377672_RuntimeMethod_var);
		Dictionary_2_t1569883280 * L_3 = V_0;
		NullCheck(L_3);
		Dictionary_2_Add_m2797377672(L_3, 2, _stringLiteral259305399, /*hidden argument*/Dictionary_2_Add_m2797377672_RuntimeMethod_var);
		Dictionary_2_t1569883280 * L_4 = V_0;
		NullCheck(L_4);
		Dictionary_2_Add_m2797377672(L_4, 3, _stringLiteral3063767281, /*hidden argument*/Dictionary_2_Add_m2797377672_RuntimeMethod_var);
		Dictionary_2_t1569883280 * L_5 = V_0;
		NullCheck(L_5);
		Dictionary_2_Add_m2797377672(L_5, 4, _stringLiteral3213630373, /*hidden argument*/Dictionary_2_Add_m2797377672_RuntimeMethod_var);
		Dictionary_2_t1569883280 * L_6 = V_0;
		NullCheck(L_6);
		Dictionary_2_Add_m2797377672(L_6, 5, _stringLiteral3546966851, /*hidden argument*/Dictionary_2_Add_m2797377672_RuntimeMethod_var);
		Dictionary_2_t1569883280 * L_7 = V_0;
		NullCheck(L_7);
		Dictionary_2_Add_m2797377672(L_7, 6, _stringLiteral1742657926, /*hidden argument*/Dictionary_2_Add_m2797377672_RuntimeMethod_var);
		Dictionary_2_t1569883280 * L_8 = V_0;
		NullCheck(L_8);
		Dictionary_2_Add_m2797377672(L_8, 7, _stringLiteral2170191983, /*hidden argument*/Dictionary_2_Add_m2797377672_RuntimeMethod_var);
		Dictionary_2_t1569883280 * L_9 = V_0;
		NullCheck(L_9);
		Dictionary_2_Add_m2797377672(L_9, 8, _stringLiteral2896349867, /*hidden argument*/Dictionary_2_Add_m2797377672_RuntimeMethod_var);
		Dictionary_2_t1569883280 * L_10 = V_0;
		NullCheck(L_10);
		Dictionary_2_Add_m2797377672(L_10, ((int32_t)9), _stringLiteral4200298573, /*hidden argument*/Dictionary_2_Add_m2797377672_RuntimeMethod_var);
		Dictionary_2_t1569883280 * L_11 = V_0;
		NullCheck(L_11);
		Dictionary_2_Add_m2797377672(L_11, ((int32_t)10), _stringLiteral742976274, /*hidden argument*/Dictionary_2_Add_m2797377672_RuntimeMethod_var);
		Dictionary_2_t1569883280 * L_12 = V_0;
		NullCheck(L_12);
		Dictionary_2_Add_m2797377672(L_12, ((int32_t)11), _stringLiteral961201264, /*hidden argument*/Dictionary_2_Add_m2797377672_RuntimeMethod_var);
		Dictionary_2_t1569883280 * L_13 = V_0;
		NullCheck(L_13);
		Dictionary_2_Add_m2797377672(L_13, ((int32_t)12), _stringLiteral3732944705, /*hidden argument*/Dictionary_2_Add_m2797377672_RuntimeMethod_var);
		Dictionary_2_t1569883280 * L_14 = V_0;
		__this->set_m_VoiceTypes_3(L_14);
		Dictionary_2_t2576372715 * L_15 = (Dictionary_2_t2576372715 *)il2cpp_codegen_object_new(Dictionary_2_t2576372715_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3553715201(L_15, /*hidden argument*/Dictionary_2__ctor_m3553715201_RuntimeMethod_var);
		V_1 = L_15;
		Dictionary_2_t2576372715 * L_16 = V_1;
		NullCheck(L_16);
		Dictionary_2_Add_m714379753(L_16, 0, _stringLiteral2751057514, /*hidden argument*/Dictionary_2_Add_m714379753_RuntimeMethod_var);
		Dictionary_2_t2576372715 * L_17 = V_1;
		NullCheck(L_17);
		Dictionary_2_Add_m714379753(L_17, 1, _stringLiteral1657819649, /*hidden argument*/Dictionary_2_Add_m714379753_RuntimeMethod_var);
		Dictionary_2_t2576372715 * L_18 = V_1;
		NullCheck(L_18);
		Dictionary_2_Add_m714379753(L_18, 2, _stringLiteral911428805, /*hidden argument*/Dictionary_2_Add_m714379753_RuntimeMethod_var);
		Dictionary_2_t2576372715 * L_19 = V_1;
		__this->set_m_AudioFormats_4(L_19);
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech::get_DisableCache()
extern "C"  bool TextToSpeech_get_DisableCache_m444348792 (TextToSpeech_t3349357562 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_U3CDisableCacheU3Ek__BackingField_8();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech::set_DisableCache(System.Boolean)
extern "C"  void TextToSpeech_set_DisableCache_m2690961797 (TextToSpeech_t3349357562 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CDisableCacheU3Ek__BackingField_8(L_0);
		return;
	}
}
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.AudioFormatType IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech::get_AudioFormat()
extern "C"  int32_t TextToSpeech_get_AudioFormat_m826887859 (TextToSpeech_t3349357562 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_m_AudioFormat_2();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech::set_AudioFormat(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.AudioFormatType)
extern "C"  void TextToSpeech_set_AudioFormat_m3902781670 (TextToSpeech_t3349357562 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_m_AudioFormat_2(L_0);
		return;
	}
}
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.VoiceType IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech::get_Voice()
extern "C"  int32_t TextToSpeech_get_Voice_m2629584479 (TextToSpeech_t3349357562 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_m_Voice_1();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech::set_Voice(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.VoiceType)
extern "C"  void TextToSpeech_set_Voice_m4019057068 (TextToSpeech_t3349357562 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_m_Voice_1();
		int32_t L_1 = ___value0;
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_2 = ___value0;
		__this->set_m_Voice_1(L_2);
		__this->set_m_SpeechCache_0((DataCache_t4250340070 *)NULL);
	}

IL_001a:
	{
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech::GetVoiceType(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.VoiceType)
extern "C"  String_t* TextToSpeech_GetVoiceType_m999042752 (TextToSpeech_t3349357562 * __this, int32_t ___voiceType0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextToSpeech_GetVoiceType_m999042752_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		Dictionary_2_t1569883280 * L_0 = __this->get_m_VoiceTypes_3();
		int32_t L_1 = ___voiceType0;
		NullCheck(L_0);
		bool L_2 = Dictionary_2_ContainsKey_m1508741739(L_0, L_1, /*hidden argument*/Dictionary_2_ContainsKey_m1508741739_RuntimeMethod_var);
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		V_0 = L_3;
		Dictionary_2_t1569883280 * L_4 = __this->get_m_VoiceTypes_3();
		int32_t L_5 = ___voiceType0;
		NullCheck(L_4);
		Dictionary_2_TryGetValue_m2360694639(L_4, L_5, (&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m2360694639_RuntimeMethod_var);
		String_t* L_6 = V_0;
		return L_6;
	}

IL_0028:
	{
		ObjectU5BU5D_t3614634134* L_7 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1));
		int32_t L_8 = ___voiceType0;
		int32_t L_9 = L_8;
		RuntimeObject * L_10 = Box(VoiceType_t981025524_il2cpp_TypeInfo_var, &L_9);
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, L_10);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_10);
		Log_Warning_m3862795843(NULL /*static, unused*/, _stringLiteral2221276358, _stringLiteral1465181607, L_7, /*hidden argument*/NULL);
		return (String_t*)NULL;
	}
}
// System.Boolean IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech::GetVoices(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetVoicesCallback)
extern "C"  bool TextToSpeech_GetVoices_m2898245765 (TextToSpeech_t3349357562 * __this, GetVoicesCallback_t3012885511 * ___callback0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextToSpeech_GetVoices_m2898245765_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RESTConnector_t3705102247 * V_0 = NULL;
	GetVoicesReq_t1741653746 * V_1 = NULL;
	{
		GetVoicesCallback_t3012885511 * L_0 = ___callback0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, _stringLiteral1913944053, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(RESTConnector_t3705102247_il2cpp_TypeInfo_var);
		RESTConnector_t3705102247 * L_2 = RESTConnector_GetConnector_m3262139854(NULL /*static, unused*/, _stringLiteral2566220529, _stringLiteral2053051312, (bool)1, /*hidden argument*/NULL);
		V_0 = L_2;
		RESTConnector_t3705102247 * L_3 = V_0;
		if (L_3)
		{
			goto IL_002a;
		}
	}
	{
		return (bool)0;
	}

IL_002a:
	{
		GetVoicesReq_t1741653746 * L_4 = (GetVoicesReq_t1741653746 *)il2cpp_codegen_object_new(GetVoicesReq_t1741653746_il2cpp_TypeInfo_var);
		GetVoicesReq__ctor_m1962143417(L_4, /*hidden argument*/NULL);
		V_1 = L_4;
		GetVoicesReq_t1741653746 * L_5 = V_1;
		GetVoicesCallback_t3012885511 * L_6 = ___callback0;
		NullCheck(L_5);
		GetVoicesReq_set_Callback_m1978410448(L_5, L_6, /*hidden argument*/NULL);
		GetVoicesReq_t1741653746 * L_7 = V_1;
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)TextToSpeech_OnGetVoicesResp_m1902289587_RuntimeMethod_var);
		ResponseEvent_t1616568356 * L_9 = (ResponseEvent_t1616568356 *)il2cpp_codegen_object_new(ResponseEvent_t1616568356_il2cpp_TypeInfo_var);
		ResponseEvent__ctor_m3385394859(L_9, __this, L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		Request_set_OnResponse_m2226001952(L_7, L_9, /*hidden argument*/NULL);
		RESTConnector_t3705102247 * L_10 = V_0;
		GetVoicesReq_t1741653746 * L_11 = V_1;
		NullCheck(L_10);
		bool L_12 = RESTConnector_Send_m2925366401(L_10, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech::OnGetVoicesResp(IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request,IBM.Watson.DeveloperCloud.Connection.RESTConnector/Response)
extern "C"  void TextToSpeech_OnGetVoicesResp_m1902289587 (TextToSpeech_t3349357562 * __this, Request_t466816980 * ___req0, Response_t429319368 * ___resp1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextToSpeech_OnGetVoicesResp_m1902289587_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Voices_t4221445733 * V_0 = NULL;
	fsData_t2583805605 * V_1 = NULL;
	fsResult_t862419890  V_2;
	memset(&V_2, 0, sizeof(V_2));
	RuntimeObject * V_3 = NULL;
	Exception_t1927440687 * V_4 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	GetVoicesCallback_t3012885511 * G_B10_0 = NULL;
	GetVoicesCallback_t3012885511 * G_B9_0 = NULL;
	Voices_t4221445733 * G_B11_0 = NULL;
	GetVoicesCallback_t3012885511 * G_B11_1 = NULL;
	{
		Voices_t4221445733 * L_0 = (Voices_t4221445733 *)il2cpp_codegen_object_new(Voices_t4221445733_il2cpp_TypeInfo_var);
		Voices__ctor_m2371932580(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Response_t429319368 * L_1 = ___resp1;
		NullCheck(L_1);
		bool L_2 = Response_get_Success_m2313267345(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_00a5;
		}
	}

IL_0011:
	try
	{ // begin try (depth: 1)
		{
			V_1 = (fsData_t2583805605 *)NULL;
			IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
			Encoding_t663144255 * L_3 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
			Response_t429319368 * L_4 = ___resp1;
			NullCheck(L_4);
			ByteU5BU5D_t3397334013* L_5 = Response_get_Data_m3078004018(L_4, /*hidden argument*/NULL);
			NullCheck(L_3);
			String_t* L_6 = VirtFuncInvoker1< String_t*, ByteU5BU5D_t3397334013* >::Invoke(22 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_3, L_5);
			fsResult_t862419890  L_7 = fsJsonParser_Parse_m4200302395(NULL /*static, unused*/, L_6, (&V_1), /*hidden argument*/NULL);
			V_2 = L_7;
			bool L_8 = fsResult_get_Succeeded_m3237049006((&V_2), /*hidden argument*/NULL);
			if (L_8)
			{
				goto IL_0044;
			}
		}

IL_0037:
		{
			String_t* L_9 = fsResult_get_FormattedMessages_m3414202484((&V_2), /*hidden argument*/NULL);
			WatsonException_t1186470237 * L_10 = (WatsonException_t1186470237 *)il2cpp_codegen_object_new(WatsonException_t1186470237_il2cpp_TypeInfo_var);
			WatsonException__ctor_m3825115789(L_10, L_9, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_10);
		}

IL_0044:
		{
			Voices_t4221445733 * L_11 = V_0;
			V_3 = L_11;
			IL2CPP_RUNTIME_CLASS_INIT(TextToSpeech_t3349357562_il2cpp_TypeInfo_var);
			fsSerializer_t4193731081 * L_12 = ((TextToSpeech_t3349357562_StaticFields*)il2cpp_codegen_static_fields_for(TextToSpeech_t3349357562_il2cpp_TypeInfo_var))->get_sm_Serializer_6();
			fsData_t2583805605 * L_13 = V_1;
			RuntimeObject * L_14 = V_3;
			NullCheck(L_14);
			Type_t * L_15 = Object_GetType_m191970594(L_14, /*hidden argument*/NULL);
			NullCheck(L_12);
			fsResult_t862419890  L_16 = fsSerializer_TryDeserialize_m4134098758(L_12, L_13, L_15, (&V_3), /*hidden argument*/NULL);
			V_2 = L_16;
			bool L_17 = fsResult_get_Succeeded_m3237049006((&V_2), /*hidden argument*/NULL);
			if (L_17)
			{
				goto IL_0073;
			}
		}

IL_0066:
		{
			String_t* L_18 = fsResult_get_FormattedMessages_m3414202484((&V_2), /*hidden argument*/NULL);
			WatsonException_t1186470237 * L_19 = (WatsonException_t1186470237 *)il2cpp_codegen_object_new(WatsonException_t1186470237_il2cpp_TypeInfo_var);
			WatsonException__ctor_m3825115789(L_19, L_18, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_19);
		}

IL_0073:
		{
			goto IL_00a5;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1927440687_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0078;
		throw e;
	}

CATCH_0078:
	{ // begin catch(System.Exception)
		V_4 = ((Exception_t1927440687 *)__exception_local);
		ObjectU5BU5D_t3614634134* L_20 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1));
		Exception_t1927440687 * L_21 = V_4;
		NullCheck(L_21);
		String_t* L_22 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_21);
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, L_22);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_22);
		Log_Error_m685270089(NULL /*static, unused*/, _stringLiteral2263597084, _stringLiteral1468624296, L_20, /*hidden argument*/NULL);
		Response_t429319368 * L_23 = ___resp1;
		NullCheck(L_23);
		Response_set_Success_m3746673474(L_23, (bool)0, /*hidden argument*/NULL);
		goto IL_00a5;
	} // end catch (depth: 1)

IL_00a5:
	{
		Request_t466816980 * L_24 = ___req0;
		NullCheck(((GetVoicesReq_t1741653746 *)CastclassClass((RuntimeObject*)L_24, GetVoicesReq_t1741653746_il2cpp_TypeInfo_var)));
		GetVoicesCallback_t3012885511 * L_25 = GetVoicesReq_get_Callback_m4057882599(((GetVoicesReq_t1741653746 *)CastclassClass((RuntimeObject*)L_24, GetVoicesReq_t1741653746_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00d7;
		}
	}
	{
		Request_t466816980 * L_26 = ___req0;
		NullCheck(((GetVoicesReq_t1741653746 *)CastclassClass((RuntimeObject*)L_26, GetVoicesReq_t1741653746_il2cpp_TypeInfo_var)));
		GetVoicesCallback_t3012885511 * L_27 = GetVoicesReq_get_Callback_m4057882599(((GetVoicesReq_t1741653746 *)CastclassClass((RuntimeObject*)L_26, GetVoicesReq_t1741653746_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		Response_t429319368 * L_28 = ___resp1;
		NullCheck(L_28);
		bool L_29 = Response_get_Success_m2313267345(L_28, /*hidden argument*/NULL);
		G_B9_0 = L_27;
		if (!L_29)
		{
			G_B10_0 = L_27;
			goto IL_00d1;
		}
	}
	{
		Voices_t4221445733 * L_30 = V_0;
		G_B11_0 = L_30;
		G_B11_1 = G_B9_0;
		goto IL_00d2;
	}

IL_00d1:
	{
		G_B11_0 = ((Voices_t4221445733 *)(NULL));
		G_B11_1 = G_B10_0;
	}

IL_00d2:
	{
		NullCheck(G_B11_1);
		GetVoicesCallback_Invoke_m1235110721(G_B11_1, G_B11_0, /*hidden argument*/NULL);
	}

IL_00d7:
	{
		return;
	}
}
// System.Boolean IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech::GetVoice(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetVoiceCallback,System.Nullable`1<IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.VoiceType>)
extern "C"  bool TextToSpeech_GetVoice_m3804271456 (TextToSpeech_t3349357562 * __this, GetVoiceCallback_t2752546046 * ___callback0, Nullable_1_t3539059135  ___voice1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextToSpeech_GetVoice_m3804271456_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	RESTConnector_t3705102247 * V_1 = NULL;
	GetVoiceReq_t1980513795 * V_2 = NULL;
	{
		GetVoiceCallback_t2752546046 * L_0 = ___callback0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, _stringLiteral1913944053, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		bool L_2 = Nullable_1_get_HasValue_m925507304((&___voice1), /*hidden argument*/Nullable_1_get_HasValue_m925507304_RuntimeMethod_var);
		if (!((((int32_t)L_2) == ((int32_t)0))? 1 : 0))
		{
			goto IL_002d;
		}
	}
	{
		int32_t L_3 = __this->get_m_Voice_1();
		Nullable_1_t3539059135  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Nullable_1__ctor_m2553508639((&L_4), L_3, /*hidden argument*/Nullable_1__ctor_m2553508639_RuntimeMethod_var);
		___voice1 = L_4;
	}

IL_002d:
	{
		V_0 = _stringLiteral3015659863;
		String_t* L_5 = V_0;
		int32_t L_6 = Nullable_1_get_Value_m2811304662((&___voice1), /*hidden argument*/Nullable_1_get_Value_m2811304662_RuntimeMethod_var);
		String_t* L_7 = TextToSpeech_GetVoiceType_m999042752(__this, L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Format_m2024975688(NULL /*static, unused*/, L_5, L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RESTConnector_t3705102247_il2cpp_TypeInfo_var);
		RESTConnector_t3705102247 * L_9 = RESTConnector_GetConnector_m3262139854(NULL /*static, unused*/, _stringLiteral2566220529, L_8, (bool)1, /*hidden argument*/NULL);
		V_1 = L_9;
		RESTConnector_t3705102247 * L_10 = V_1;
		if (L_10)
		{
			goto IL_005a;
		}
	}
	{
		return (bool)0;
	}

IL_005a:
	{
		GetVoiceReq_t1980513795 * L_11 = (GetVoiceReq_t1980513795 *)il2cpp_codegen_object_new(GetVoiceReq_t1980513795_il2cpp_TypeInfo_var);
		GetVoiceReq__ctor_m1969519438(L_11, /*hidden argument*/NULL);
		V_2 = L_11;
		GetVoiceReq_t1980513795 * L_12 = V_2;
		GetVoiceCallback_t2752546046 * L_13 = ___callback0;
		NullCheck(L_12);
		GetVoiceReq_set_Callback_m1200200228(L_12, L_13, /*hidden argument*/NULL);
		GetVoiceReq_t1980513795 * L_14 = V_2;
		IntPtr_t L_15;
		L_15.set_m_value_0((void*)(void*)TextToSpeech_OnGetVoiceResp_m4256129680_RuntimeMethod_var);
		ResponseEvent_t1616568356 * L_16 = (ResponseEvent_t1616568356 *)il2cpp_codegen_object_new(ResponseEvent_t1616568356_il2cpp_TypeInfo_var);
		ResponseEvent__ctor_m3385394859(L_16, __this, L_15, /*hidden argument*/NULL);
		NullCheck(L_14);
		Request_set_OnResponse_m2226001952(L_14, L_16, /*hidden argument*/NULL);
		RESTConnector_t3705102247 * L_17 = V_1;
		GetVoiceReq_t1980513795 * L_18 = V_2;
		NullCheck(L_17);
		bool L_19 = RESTConnector_Send_m2925366401(L_17, L_18, /*hidden argument*/NULL);
		return L_19;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech::OnGetVoiceResp(IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request,IBM.Watson.DeveloperCloud.Connection.RESTConnector/Response)
extern "C"  void TextToSpeech_OnGetVoiceResp_m4256129680 (TextToSpeech_t3349357562 * __this, Request_t466816980 * ___req0, Response_t429319368 * ___resp1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextToSpeech_OnGetVoiceResp_m4256129680_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Voice_t3646862260 * V_0 = NULL;
	fsData_t2583805605 * V_1 = NULL;
	fsResult_t862419890  V_2;
	memset(&V_2, 0, sizeof(V_2));
	RuntimeObject * V_3 = NULL;
	Exception_t1927440687 * V_4 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	GetVoiceCallback_t2752546046 * G_B10_0 = NULL;
	GetVoiceCallback_t2752546046 * G_B9_0 = NULL;
	Voice_t3646862260 * G_B11_0 = NULL;
	GetVoiceCallback_t2752546046 * G_B11_1 = NULL;
	{
		Voice_t3646862260 * L_0 = (Voice_t3646862260 *)il2cpp_codegen_object_new(Voice_t3646862260_il2cpp_TypeInfo_var);
		Voice__ctor_m3947940203(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Response_t429319368 * L_1 = ___resp1;
		NullCheck(L_1);
		bool L_2 = Response_get_Success_m2313267345(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_00a5;
		}
	}

IL_0011:
	try
	{ // begin try (depth: 1)
		{
			V_1 = (fsData_t2583805605 *)NULL;
			IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
			Encoding_t663144255 * L_3 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
			Response_t429319368 * L_4 = ___resp1;
			NullCheck(L_4);
			ByteU5BU5D_t3397334013* L_5 = Response_get_Data_m3078004018(L_4, /*hidden argument*/NULL);
			NullCheck(L_3);
			String_t* L_6 = VirtFuncInvoker1< String_t*, ByteU5BU5D_t3397334013* >::Invoke(22 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_3, L_5);
			fsResult_t862419890  L_7 = fsJsonParser_Parse_m4200302395(NULL /*static, unused*/, L_6, (&V_1), /*hidden argument*/NULL);
			V_2 = L_7;
			bool L_8 = fsResult_get_Succeeded_m3237049006((&V_2), /*hidden argument*/NULL);
			if (L_8)
			{
				goto IL_0044;
			}
		}

IL_0037:
		{
			String_t* L_9 = fsResult_get_FormattedMessages_m3414202484((&V_2), /*hidden argument*/NULL);
			WatsonException_t1186470237 * L_10 = (WatsonException_t1186470237 *)il2cpp_codegen_object_new(WatsonException_t1186470237_il2cpp_TypeInfo_var);
			WatsonException__ctor_m3825115789(L_10, L_9, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_10);
		}

IL_0044:
		{
			Voice_t3646862260 * L_11 = V_0;
			V_3 = L_11;
			IL2CPP_RUNTIME_CLASS_INIT(TextToSpeech_t3349357562_il2cpp_TypeInfo_var);
			fsSerializer_t4193731081 * L_12 = ((TextToSpeech_t3349357562_StaticFields*)il2cpp_codegen_static_fields_for(TextToSpeech_t3349357562_il2cpp_TypeInfo_var))->get_sm_Serializer_6();
			fsData_t2583805605 * L_13 = V_1;
			RuntimeObject * L_14 = V_3;
			NullCheck(L_14);
			Type_t * L_15 = Object_GetType_m191970594(L_14, /*hidden argument*/NULL);
			NullCheck(L_12);
			fsResult_t862419890  L_16 = fsSerializer_TryDeserialize_m4134098758(L_12, L_13, L_15, (&V_3), /*hidden argument*/NULL);
			V_2 = L_16;
			bool L_17 = fsResult_get_Succeeded_m3237049006((&V_2), /*hidden argument*/NULL);
			if (L_17)
			{
				goto IL_0073;
			}
		}

IL_0066:
		{
			String_t* L_18 = fsResult_get_FormattedMessages_m3414202484((&V_2), /*hidden argument*/NULL);
			WatsonException_t1186470237 * L_19 = (WatsonException_t1186470237 *)il2cpp_codegen_object_new(WatsonException_t1186470237_il2cpp_TypeInfo_var);
			WatsonException__ctor_m3825115789(L_19, L_18, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_19);
		}

IL_0073:
		{
			goto IL_00a5;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1927440687_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0078;
		throw e;
	}

CATCH_0078:
	{ // begin catch(System.Exception)
		V_4 = ((Exception_t1927440687 *)__exception_local);
		ObjectU5BU5D_t3614634134* L_20 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1));
		Exception_t1927440687 * L_21 = V_4;
		NullCheck(L_21);
		String_t* L_22 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_21);
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, L_22);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_22);
		Log_Error_m685270089(NULL /*static, unused*/, _stringLiteral2221276358, _stringLiteral2814884231, L_20, /*hidden argument*/NULL);
		Response_t429319368 * L_23 = ___resp1;
		NullCheck(L_23);
		Response_set_Success_m3746673474(L_23, (bool)0, /*hidden argument*/NULL);
		goto IL_00a5;
	} // end catch (depth: 1)

IL_00a5:
	{
		Request_t466816980 * L_24 = ___req0;
		NullCheck(((GetVoiceReq_t1980513795 *)CastclassClass((RuntimeObject*)L_24, GetVoiceReq_t1980513795_il2cpp_TypeInfo_var)));
		GetVoiceCallback_t2752546046 * L_25 = GetVoiceReq_get_Callback_m2983428311(((GetVoiceReq_t1980513795 *)CastclassClass((RuntimeObject*)L_24, GetVoiceReq_t1980513795_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00d7;
		}
	}
	{
		Request_t466816980 * L_26 = ___req0;
		NullCheck(((GetVoiceReq_t1980513795 *)CastclassClass((RuntimeObject*)L_26, GetVoiceReq_t1980513795_il2cpp_TypeInfo_var)));
		GetVoiceCallback_t2752546046 * L_27 = GetVoiceReq_get_Callback_m2983428311(((GetVoiceReq_t1980513795 *)CastclassClass((RuntimeObject*)L_26, GetVoiceReq_t1980513795_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		Response_t429319368 * L_28 = ___resp1;
		NullCheck(L_28);
		bool L_29 = Response_get_Success_m2313267345(L_28, /*hidden argument*/NULL);
		G_B9_0 = L_27;
		if (!L_29)
		{
			G_B10_0 = L_27;
			goto IL_00d1;
		}
	}
	{
		Voice_t3646862260 * L_30 = V_0;
		G_B11_0 = L_30;
		G_B11_1 = G_B9_0;
		goto IL_00d2;
	}

IL_00d1:
	{
		G_B11_0 = ((Voice_t3646862260 *)(NULL));
		G_B11_1 = G_B10_0;
	}

IL_00d2:
	{
		NullCheck(G_B11_1);
		GetVoiceCallback_Invoke_m2628701565(G_B11_1, G_B11_0, /*hidden argument*/NULL);
	}

IL_00d7:
	{
		return;
	}
}
// System.Boolean IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech::ToSpeech(System.String,IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/ToSpeechCallback,System.Boolean)
extern "C"  bool TextToSpeech_ToSpeech_m1235459968 (TextToSpeech_t3349357562 * __this, String_t* ___text0, ToSpeechCallback_t3422748631 * ___callback1, bool ___usePost2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextToSpeech_ToSpeech_m1235459968_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	ByteU5BU5D_t3397334013* V_1 = NULL;
	AudioClip_t1932558630 * V_2 = NULL;
	RESTConnector_t3705102247 * V_3 = NULL;
	ToSpeechRequest_t3970128895 * V_4 = NULL;
	Dictionary_2_t3943999495 * V_5 = NULL;
	{
		String_t* L_0 = ___text0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		ArgumentNullException_t628810857 * L_2 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_2, _stringLiteral3423761293, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0016:
	{
		ToSpeechCallback_t3422748631 * L_3 = ___callback1;
		if (L_3)
		{
			goto IL_0027;
		}
	}
	{
		ArgumentNullException_t628810857 * L_4 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_4, _stringLiteral1913944053, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0027:
	{
		Dictionary_2_t2576372715 * L_5 = __this->get_m_AudioFormats_4();
		int32_t L_6 = __this->get_m_AudioFormat_2();
		NullCheck(L_5);
		bool L_7 = Dictionary_2_ContainsKey_m2816287462(L_5, L_6, /*hidden argument*/Dictionary_2_ContainsKey_m2816287462_RuntimeMethod_var);
		if (L_7)
		{
			goto IL_0068;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_8 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1));
		int32_t* L_9 = __this->get_address_of_m_AudioFormat_2();
		RuntimeObject * L_10 = Box(AudioFormatType_t2756784245_il2cpp_TypeInfo_var, L_9);
		NullCheck(L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_10);
		*L_9 = *(int32_t*)UnBox(L_10);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_11);
		Log_Error_m685270089(NULL /*static, unused*/, _stringLiteral2221276358, _stringLiteral2447829758, L_8, /*hidden argument*/NULL);
		return (bool)0;
	}

IL_0068:
	{
		Dictionary_2_t1569883280 * L_12 = __this->get_m_VoiceTypes_3();
		int32_t L_13 = __this->get_m_Voice_1();
		NullCheck(L_12);
		bool L_14 = Dictionary_2_ContainsKey_m1508741739(L_12, L_13, /*hidden argument*/Dictionary_2_ContainsKey_m1508741739_RuntimeMethod_var);
		if (L_14)
		{
			goto IL_00a9;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_15 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1));
		int32_t* L_16 = __this->get_address_of_m_Voice_1();
		RuntimeObject * L_17 = Box(VoiceType_t981025524_il2cpp_TypeInfo_var, L_16);
		NullCheck(L_17);
		String_t* L_18 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_17);
		*L_16 = *(int32_t*)UnBox(L_17);
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, L_18);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_18);
		Log_Error_m685270089(NULL /*static, unused*/, _stringLiteral2221276358, _stringLiteral2185331571, L_15, /*hidden argument*/NULL);
		return (bool)0;
	}

IL_00a9:
	{
		String_t* L_19 = ___text0;
		IL2CPP_RUNTIME_CLASS_INIT(Utility_t666154278_il2cpp_TypeInfo_var);
		String_t* L_20 = Utility_GetMD5_m2388485409(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		V_0 = L_20;
		bool L_21 = TextToSpeech_get_DisableCache_m444348792(__this, /*hidden argument*/NULL);
		if (L_21)
		{
			goto IL_0120;
		}
	}
	{
		DataCache_t4250340070 * L_22 = __this->get_m_SpeechCache_0();
		if (L_22)
		{
			goto IL_00fb;
		}
	}
	{
		Dictionary_2_t1569883280 * L_23 = __this->get_m_VoiceTypes_3();
		int32_t L_24 = __this->get_m_Voice_1();
		NullCheck(L_23);
		String_t* L_25 = Dictionary_2_get_Item_m2485813692(L_23, L_24, /*hidden argument*/Dictionary_2_get_Item_m2485813692_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_26 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral1978428949, L_25, /*hidden argument*/NULL);
		DataCache_t4250340070 * L_27 = (DataCache_t4250340070 *)il2cpp_codegen_object_new(DataCache_t4250340070_il2cpp_TypeInfo_var);
		DataCache__ctor_m1332098046(L_27, L_26, (((int64_t)((int64_t)((int32_t)52428800)))), (168.0), /*hidden argument*/NULL);
		__this->set_m_SpeechCache_0(L_27);
	}

IL_00fb:
	{
		DataCache_t4250340070 * L_28 = __this->get_m_SpeechCache_0();
		String_t* L_29 = V_0;
		NullCheck(L_28);
		ByteU5BU5D_t3397334013* L_30 = DataCache_Find_m2954824875(L_28, L_29, /*hidden argument*/NULL);
		V_1 = L_30;
		ByteU5BU5D_t3397334013* L_31 = V_1;
		if (!L_31)
		{
			goto IL_0120;
		}
	}
	{
		String_t* L_32 = V_0;
		ByteU5BU5D_t3397334013* L_33 = V_1;
		AudioClip_t1932558630 * L_34 = TextToSpeech_ProcessResponse_m2113435348(__this, L_32, L_33, /*hidden argument*/NULL);
		V_2 = L_34;
		ToSpeechCallback_t3422748631 * L_35 = ___callback1;
		AudioClip_t1932558630 * L_36 = V_2;
		NullCheck(L_35);
		ToSpeechCallback_Invoke_m1017023077(L_35, L_36, /*hidden argument*/NULL);
		return (bool)1;
	}

IL_0120:
	{
		IL2CPP_RUNTIME_CLASS_INIT(RESTConnector_t3705102247_il2cpp_TypeInfo_var);
		RESTConnector_t3705102247 * L_37 = RESTConnector_GetConnector_m3262139854(NULL /*static, unused*/, _stringLiteral2566220529, _stringLiteral2685996351, (bool)1, /*hidden argument*/NULL);
		V_3 = L_37;
		RESTConnector_t3705102247 * L_38 = V_3;
		if (L_38)
		{
			goto IL_014e;
		}
	}
	{
		Log_Error_m685270089(NULL /*static, unused*/, _stringLiteral2221276358, _stringLiteral1706667859, ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		return (bool)0;
	}

IL_014e:
	{
		ToSpeechRequest_t3970128895 * L_39 = (ToSpeechRequest_t3970128895 *)il2cpp_codegen_object_new(ToSpeechRequest_t3970128895_il2cpp_TypeInfo_var);
		ToSpeechRequest__ctor_m105557656(L_39, /*hidden argument*/NULL);
		V_4 = L_39;
		ToSpeechRequest_t3970128895 * L_40 = V_4;
		String_t* L_41 = V_0;
		NullCheck(L_40);
		ToSpeechRequest_set_TextId_m2283266821(L_40, L_41, /*hidden argument*/NULL);
		ToSpeechRequest_t3970128895 * L_42 = V_4;
		String_t* L_43 = ___text0;
		NullCheck(L_42);
		ToSpeechRequest_set_Text_m84855046(L_42, L_43, /*hidden argument*/NULL);
		ToSpeechRequest_t3970128895 * L_44 = V_4;
		ToSpeechCallback_t3422748631 * L_45 = ___callback1;
		NullCheck(L_44);
		ToSpeechRequest_set_Callback_m683453887(L_44, L_45, /*hidden argument*/NULL);
		ToSpeechRequest_t3970128895 * L_46 = V_4;
		NullCheck(L_46);
		Dictionary_2_t309261261 * L_47 = Request_get_Parameters_m1217035494(L_46, /*hidden argument*/NULL);
		Dictionary_2_t2576372715 * L_48 = __this->get_m_AudioFormats_4();
		int32_t L_49 = __this->get_m_AudioFormat_2();
		NullCheck(L_48);
		String_t* L_50 = Dictionary_2_get_Item_m3261277235(L_48, L_49, /*hidden argument*/Dictionary_2_get_Item_m3261277235_RuntimeMethod_var);
		NullCheck(L_47);
		Dictionary_2_set_Item_m3411472609(L_47, _stringLiteral3819355434, L_50, /*hidden argument*/Dictionary_2_set_Item_m3411472609_RuntimeMethod_var);
		ToSpeechRequest_t3970128895 * L_51 = V_4;
		NullCheck(L_51);
		Dictionary_2_t309261261 * L_52 = Request_get_Parameters_m1217035494(L_51, /*hidden argument*/NULL);
		Dictionary_2_t1569883280 * L_53 = __this->get_m_VoiceTypes_3();
		int32_t L_54 = __this->get_m_Voice_1();
		NullCheck(L_53);
		String_t* L_55 = Dictionary_2_get_Item_m2485813692(L_53, L_54, /*hidden argument*/Dictionary_2_get_Item_m2485813692_RuntimeMethod_var);
		NullCheck(L_52);
		Dictionary_2_set_Item_m3411472609(L_52, _stringLiteral795243372, L_55, /*hidden argument*/Dictionary_2_set_Item_m3411472609_RuntimeMethod_var);
		ToSpeechRequest_t3970128895 * L_56 = V_4;
		IntPtr_t L_57;
		L_57.set_m_value_0((void*)(void*)TextToSpeech_ToSpeechResponse_m166731737_RuntimeMethod_var);
		ResponseEvent_t1616568356 * L_58 = (ResponseEvent_t1616568356 *)il2cpp_codegen_object_new(ResponseEvent_t1616568356_il2cpp_TypeInfo_var);
		ResponseEvent__ctor_m3385394859(L_58, __this, L_57, /*hidden argument*/NULL);
		NullCheck(L_56);
		Request_set_OnResponse_m2226001952(L_56, L_58, /*hidden argument*/NULL);
		bool L_59 = ___usePost2;
		if (!L_59)
		{
			goto IL_0211;
		}
	}
	{
		Dictionary_2_t3943999495 * L_60 = (Dictionary_2_t3943999495 *)il2cpp_codegen_object_new(Dictionary_2_t3943999495_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m760167321(L_60, /*hidden argument*/Dictionary_2__ctor_m760167321_RuntimeMethod_var);
		V_5 = L_60;
		Dictionary_2_t3943999495 * L_61 = V_5;
		String_t* L_62 = ___text0;
		NullCheck(L_61);
		Dictionary_2_set_Item_m4244870320(L_61, _stringLiteral3423761293, L_62, /*hidden argument*/Dictionary_2_set_Item_m4244870320_RuntimeMethod_var);
		ToSpeechRequest_t3970128895 * L_63 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
		Encoding_t663144255 * L_64 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
		Dictionary_2_t3943999495 * L_65 = V_5;
		String_t* L_66 = Json_Serialize_m779708699(NULL /*static, unused*/, L_65, /*hidden argument*/NULL);
		NullCheck(L_64);
		ByteU5BU5D_t3397334013* L_67 = VirtFuncInvoker1< ByteU5BU5D_t3397334013*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_64, L_66);
		NullCheck(L_63);
		Request_set_Send_m296178747(L_63, L_67, /*hidden argument*/NULL);
		ToSpeechRequest_t3970128895 * L_68 = V_4;
		NullCheck(L_68);
		Dictionary_2_t3943999495 * L_69 = Request_get_Headers_m340270624(L_68, /*hidden argument*/NULL);
		NullCheck(L_69);
		Dictionary_2_set_Item_m4244870320(L_69, _stringLiteral1048821954, _stringLiteral1391431453, /*hidden argument*/Dictionary_2_set_Item_m4244870320_RuntimeMethod_var);
		goto IL_0223;
	}

IL_0211:
	{
		ToSpeechRequest_t3970128895 * L_70 = V_4;
		NullCheck(L_70);
		Dictionary_2_t309261261 * L_71 = Request_get_Parameters_m1217035494(L_70, /*hidden argument*/NULL);
		String_t* L_72 = ___text0;
		NullCheck(L_71);
		Dictionary_2_set_Item_m3411472609(L_71, _stringLiteral3423761293, L_72, /*hidden argument*/Dictionary_2_set_Item_m3411472609_RuntimeMethod_var);
	}

IL_0223:
	{
		RESTConnector_t3705102247 * L_73 = V_3;
		ToSpeechRequest_t3970128895 * L_74 = V_4;
		NullCheck(L_73);
		bool L_75 = RESTConnector_Send_m2925366401(L_73, L_74, /*hidden argument*/NULL);
		return L_75;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech::ToSpeechResponse(IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request,IBM.Watson.DeveloperCloud.Connection.RESTConnector/Response)
extern "C"  void TextToSpeech_ToSpeechResponse_m166731737 (TextToSpeech_t3349357562 * __this, Request_t466816980 * ___req0, Response_t429319368 * ___resp1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextToSpeech_ToSpeechResponse_m166731737_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ToSpeechRequest_t3970128895 * V_0 = NULL;
	AudioClip_t1932558630 * V_1 = NULL;
	AudioClip_t1932558630 * G_B5_0 = NULL;
	{
		Request_t466816980 * L_0 = ___req0;
		V_0 = ((ToSpeechRequest_t3970128895 *)IsInstClass((RuntimeObject*)L_0, ToSpeechRequest_t3970128895_il2cpp_TypeInfo_var));
		ToSpeechRequest_t3970128895 * L_1 = V_0;
		if (L_1)
		{
			goto IL_0018;
		}
	}
	{
		WatsonException_t1186470237 * L_2 = (WatsonException_t1186470237 *)il2cpp_codegen_object_new(WatsonException_t1186470237_il2cpp_TypeInfo_var);
		WatsonException__ctor_m3825115789(L_2, _stringLiteral3588881476, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0018:
	{
		Response_t429319368 * L_3 = ___resp1;
		NullCheck(L_3);
		bool L_4 = Response_get_Success_m2313267345(L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_003a;
		}
	}
	{
		ToSpeechRequest_t3970128895 * L_5 = V_0;
		NullCheck(L_5);
		String_t* L_6 = ToSpeechRequest_get_TextId_m3218788966(L_5, /*hidden argument*/NULL);
		Response_t429319368 * L_7 = ___resp1;
		NullCheck(L_7);
		ByteU5BU5D_t3397334013* L_8 = Response_get_Data_m3078004018(L_7, /*hidden argument*/NULL);
		AudioClip_t1932558630 * L_9 = TextToSpeech_ProcessResponse_m2113435348(__this, L_6, L_8, /*hidden argument*/NULL);
		G_B5_0 = L_9;
		goto IL_003b;
	}

IL_003a:
	{
		G_B5_0 = ((AudioClip_t1932558630 *)(NULL));
	}

IL_003b:
	{
		V_1 = G_B5_0;
		AudioClip_t1932558630 * L_10 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_11 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_10, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0066;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_12 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1));
		Response_t429319368 * L_13 = ___resp1;
		NullCheck(L_13);
		String_t* L_14 = Response_get_Error_m276091539(L_13, /*hidden argument*/NULL);
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_14);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_14);
		Log_Error_m685270089(NULL /*static, unused*/, _stringLiteral2221276358, _stringLiteral3238191036, L_12, /*hidden argument*/NULL);
	}

IL_0066:
	{
		DataCache_t4250340070 * L_15 = __this->get_m_SpeechCache_0();
		if (!L_15)
		{
			goto IL_0094;
		}
	}
	{
		AudioClip_t1932558630 * L_16 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_17 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_16, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_0094;
		}
	}
	{
		DataCache_t4250340070 * L_18 = __this->get_m_SpeechCache_0();
		ToSpeechRequest_t3970128895 * L_19 = V_0;
		NullCheck(L_19);
		String_t* L_20 = ToSpeechRequest_get_TextId_m3218788966(L_19, /*hidden argument*/NULL);
		Response_t429319368 * L_21 = ___resp1;
		NullCheck(L_21);
		ByteU5BU5D_t3397334013* L_22 = Response_get_Data_m3078004018(L_21, /*hidden argument*/NULL);
		NullCheck(L_18);
		DataCache_Save_m3872508888(L_18, L_20, L_22, /*hidden argument*/NULL);
	}

IL_0094:
	{
		ToSpeechRequest_t3970128895 * L_23 = V_0;
		NullCheck(L_23);
		ToSpeechCallback_t3422748631 * L_24 = ToSpeechRequest_get_Callback_m490611990(L_23, /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_00ab;
		}
	}
	{
		ToSpeechRequest_t3970128895 * L_25 = V_0;
		NullCheck(L_25);
		ToSpeechCallback_t3422748631 * L_26 = ToSpeechRequest_get_Callback_m490611990(L_25, /*hidden argument*/NULL);
		AudioClip_t1932558630 * L_27 = V_1;
		NullCheck(L_26);
		ToSpeechCallback_Invoke_m1017023077(L_26, L_27, /*hidden argument*/NULL);
	}

IL_00ab:
	{
		return;
	}
}
// UnityEngine.AudioClip IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech::ProcessResponse(System.String,System.Byte[])
extern "C"  AudioClip_t1932558630 * TextToSpeech_ProcessResponse_m2113435348 (TextToSpeech_t3349357562 * __this, String_t* ___textId0, ByteU5BU5D_t3397334013* ___data1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextToSpeech_ProcessResponse_m2113435348_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_AudioFormat_2();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0013;
		}
	}
	{
		goto IL_001b;
	}

IL_0013:
	{
		String_t* L_2 = ___textId0;
		ByteU5BU5D_t3397334013* L_3 = ___data1;
		AudioClip_t1932558630 * L_4 = WaveFile_ParseWAV_m3088437927(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}

IL_001b:
	{
		goto IL_0020;
	}

IL_0020:
	{
		ObjectU5BU5D_t3614634134* L_5 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1));
		int32_t* L_6 = __this->get_address_of_m_AudioFormat_2();
		RuntimeObject * L_7 = Box(AudioFormatType_t2756784245_il2cpp_TypeInfo_var, L_6);
		NullCheck(L_7);
		String_t* L_8 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_7);
		*L_6 = *(int32_t*)UnBox(L_7);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_8);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_8);
		Log_Error_m685270089(NULL /*static, unused*/, _stringLiteral2221276358, _stringLiteral2447829758, L_5, /*hidden argument*/NULL);
		return (AudioClip_t1932558630 *)NULL;
	}
}
// System.Boolean IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech::GetPronunciation(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetPronunciationCallback,System.String,System.Nullable`1<IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.VoiceType>,System.String,System.String)
extern "C"  bool TextToSpeech_GetPronunciation_m4262540122 (TextToSpeech_t3349357562 * __this, GetPronunciationCallback_t2622344657 * ___callback0, String_t* ___text1, Nullable_1_t3539059135  ___voice2, String_t* ___format3, String_t* ___customization_id4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextToSpeech_GetPronunciation_m4262540122_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RESTConnector_t3705102247 * V_0 = NULL;
	GetPronunciationReq_t2533372478 * V_1 = NULL;
	{
		GetPronunciationCallback_t2622344657 * L_0 = ___callback0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, _stringLiteral1913944053, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		String_t* L_2 = ___text1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0027;
		}
	}
	{
		ArgumentNullException_t628810857 * L_4 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_4, _stringLiteral3423761293, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0027:
	{
		bool L_5 = Nullable_1_get_HasValue_m925507304((&___voice2), /*hidden argument*/Nullable_1_get_HasValue_m925507304_RuntimeMethod_var);
		if (!((((int32_t)L_5) == ((int32_t)0))? 1 : 0))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_6 = __this->get_m_Voice_1();
		Nullable_1_t3539059135  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Nullable_1__ctor_m2553508639((&L_7), L_6, /*hidden argument*/Nullable_1__ctor_m2553508639_RuntimeMethod_var);
		___voice2 = L_7;
	}

IL_0043:
	{
		IL2CPP_RUNTIME_CLASS_INIT(RESTConnector_t3705102247_il2cpp_TypeInfo_var);
		RESTConnector_t3705102247 * L_8 = RESTConnector_GetConnector_m3262139854(NULL /*static, unused*/, _stringLiteral2566220529, _stringLiteral2428510368, (bool)1, /*hidden argument*/NULL);
		V_0 = L_8;
		RESTConnector_t3705102247 * L_9 = V_0;
		if (L_9)
		{
			goto IL_005c;
		}
	}
	{
		return (bool)0;
	}

IL_005c:
	{
		GetPronunciationReq_t2533372478 * L_10 = (GetPronunciationReq_t2533372478 *)il2cpp_codegen_object_new(GetPronunciationReq_t2533372478_il2cpp_TypeInfo_var);
		GetPronunciationReq__ctor_m239301707(L_10, /*hidden argument*/NULL);
		V_1 = L_10;
		GetPronunciationReq_t2533372478 * L_11 = V_1;
		GetPronunciationCallback_t2622344657 * L_12 = ___callback0;
		NullCheck(L_11);
		GetPronunciationReq_set_Callback_m1752878660(L_11, L_12, /*hidden argument*/NULL);
		GetPronunciationReq_t2533372478 * L_13 = V_1;
		String_t* L_14 = ___text1;
		NullCheck(L_13);
		GetPronunciationReq_set_Text_m2524915157(L_13, L_14, /*hidden argument*/NULL);
		GetPronunciationReq_t2533372478 * L_15 = V_1;
		int32_t L_16 = Nullable_1_get_Value_m2811304662((&___voice2), /*hidden argument*/Nullable_1_get_Value_m2811304662_RuntimeMethod_var);
		NullCheck(L_15);
		GetPronunciationReq_set_Voice_m2970561308(L_15, L_16, /*hidden argument*/NULL);
		GetPronunciationReq_t2533372478 * L_17 = V_1;
		String_t* L_18 = ___format3;
		NullCheck(L_17);
		GetPronunciationReq_set_Format_m175712339(L_17, L_18, /*hidden argument*/NULL);
		GetPronunciationReq_t2533372478 * L_19 = V_1;
		String_t* L_20 = ___customization_id4;
		NullCheck(L_19);
		GetPronunciationReq_set_Customization_ID_m776457007(L_19, L_20, /*hidden argument*/NULL);
		GetPronunciationReq_t2533372478 * L_21 = V_1;
		NullCheck(L_21);
		Dictionary_2_t309261261 * L_22 = Request_get_Parameters_m1217035494(L_21, /*hidden argument*/NULL);
		String_t* L_23 = ___text1;
		NullCheck(L_22);
		Dictionary_2_set_Item_m3411472609(L_22, _stringLiteral3423761293, L_23, /*hidden argument*/Dictionary_2_set_Item_m3411472609_RuntimeMethod_var);
		GetPronunciationReq_t2533372478 * L_24 = V_1;
		NullCheck(L_24);
		Dictionary_2_t309261261 * L_25 = Request_get_Parameters_m1217035494(L_24, /*hidden argument*/NULL);
		int32_t L_26 = Nullable_1_get_Value_m2811304662((&___voice2), /*hidden argument*/Nullable_1_get_Value_m2811304662_RuntimeMethod_var);
		String_t* L_27 = TextToSpeech_GetVoiceType_m999042752(__this, L_26, /*hidden argument*/NULL);
		NullCheck(L_25);
		Dictionary_2_set_Item_m3411472609(L_25, _stringLiteral795243372, L_27, /*hidden argument*/Dictionary_2_set_Item_m3411472609_RuntimeMethod_var);
		GetPronunciationReq_t2533372478 * L_28 = V_1;
		NullCheck(L_28);
		Dictionary_2_t309261261 * L_29 = Request_get_Parameters_m1217035494(L_28, /*hidden argument*/NULL);
		String_t* L_30 = ___format3;
		NullCheck(L_29);
		Dictionary_2_set_Item_m3411472609(L_29, _stringLiteral1334200623, L_30, /*hidden argument*/Dictionary_2_set_Item_m3411472609_RuntimeMethod_var);
		String_t* L_31 = ___customization_id4;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_32 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		if (L_32)
		{
			goto IL_00eb;
		}
	}
	{
		GetPronunciationReq_t2533372478 * L_33 = V_1;
		NullCheck(L_33);
		Dictionary_2_t309261261 * L_34 = Request_get_Parameters_m1217035494(L_33, /*hidden argument*/NULL);
		String_t* L_35 = ___customization_id4;
		NullCheck(L_34);
		Dictionary_2_set_Item_m3411472609(L_34, _stringLiteral2108007291, L_35, /*hidden argument*/Dictionary_2_set_Item_m3411472609_RuntimeMethod_var);
	}

IL_00eb:
	{
		GetPronunciationReq_t2533372478 * L_36 = V_1;
		IntPtr_t L_37;
		L_37.set_m_value_0((void*)(void*)TextToSpeech_OnGetPronunciationResp_m2803422241_RuntimeMethod_var);
		ResponseEvent_t1616568356 * L_38 = (ResponseEvent_t1616568356 *)il2cpp_codegen_object_new(ResponseEvent_t1616568356_il2cpp_TypeInfo_var);
		ResponseEvent__ctor_m3385394859(L_38, __this, L_37, /*hidden argument*/NULL);
		NullCheck(L_36);
		Request_set_OnResponse_m2226001952(L_36, L_38, /*hidden argument*/NULL);
		RESTConnector_t3705102247 * L_39 = V_0;
		GetPronunciationReq_t2533372478 * L_40 = V_1;
		NullCheck(L_39);
		bool L_41 = RESTConnector_Send_m2925366401(L_39, L_40, /*hidden argument*/NULL);
		return L_41;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech::OnGetPronunciationResp(IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request,IBM.Watson.DeveloperCloud.Connection.RESTConnector/Response)
extern "C"  void TextToSpeech_OnGetPronunciationResp_m2803422241 (TextToSpeech_t3349357562 * __this, Request_t466816980 * ___req0, Response_t429319368 * ___resp1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextToSpeech_OnGetPronunciationResp_m2803422241_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Pronunciation_t2711344207 * V_0 = NULL;
	fsData_t2583805605 * V_1 = NULL;
	fsResult_t862419890  V_2;
	memset(&V_2, 0, sizeof(V_2));
	RuntimeObject * V_3 = NULL;
	Exception_t1927440687 * V_4 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	GetPronunciationCallback_t2622344657 * G_B10_0 = NULL;
	GetPronunciationCallback_t2622344657 * G_B9_0 = NULL;
	Pronunciation_t2711344207 * G_B11_0 = NULL;
	GetPronunciationCallback_t2622344657 * G_B11_1 = NULL;
	{
		Pronunciation_t2711344207 * L_0 = (Pronunciation_t2711344207 *)il2cpp_codegen_object_new(Pronunciation_t2711344207_il2cpp_TypeInfo_var);
		Pronunciation__ctor_m2815357694(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Response_t429319368 * L_1 = ___resp1;
		NullCheck(L_1);
		bool L_2 = Response_get_Success_m2313267345(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_00a5;
		}
	}

IL_0011:
	try
	{ // begin try (depth: 1)
		{
			V_1 = (fsData_t2583805605 *)NULL;
			IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
			Encoding_t663144255 * L_3 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
			Response_t429319368 * L_4 = ___resp1;
			NullCheck(L_4);
			ByteU5BU5D_t3397334013* L_5 = Response_get_Data_m3078004018(L_4, /*hidden argument*/NULL);
			NullCheck(L_3);
			String_t* L_6 = VirtFuncInvoker1< String_t*, ByteU5BU5D_t3397334013* >::Invoke(22 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_3, L_5);
			fsResult_t862419890  L_7 = fsJsonParser_Parse_m4200302395(NULL /*static, unused*/, L_6, (&V_1), /*hidden argument*/NULL);
			V_2 = L_7;
			bool L_8 = fsResult_get_Succeeded_m3237049006((&V_2), /*hidden argument*/NULL);
			if (L_8)
			{
				goto IL_0044;
			}
		}

IL_0037:
		{
			String_t* L_9 = fsResult_get_FormattedMessages_m3414202484((&V_2), /*hidden argument*/NULL);
			WatsonException_t1186470237 * L_10 = (WatsonException_t1186470237 *)il2cpp_codegen_object_new(WatsonException_t1186470237_il2cpp_TypeInfo_var);
			WatsonException__ctor_m3825115789(L_10, L_9, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_10);
		}

IL_0044:
		{
			Pronunciation_t2711344207 * L_11 = V_0;
			V_3 = L_11;
			IL2CPP_RUNTIME_CLASS_INIT(TextToSpeech_t3349357562_il2cpp_TypeInfo_var);
			fsSerializer_t4193731081 * L_12 = ((TextToSpeech_t3349357562_StaticFields*)il2cpp_codegen_static_fields_for(TextToSpeech_t3349357562_il2cpp_TypeInfo_var))->get_sm_Serializer_6();
			fsData_t2583805605 * L_13 = V_1;
			RuntimeObject * L_14 = V_3;
			NullCheck(L_14);
			Type_t * L_15 = Object_GetType_m191970594(L_14, /*hidden argument*/NULL);
			NullCheck(L_12);
			fsResult_t862419890  L_16 = fsSerializer_TryDeserialize_m4134098758(L_12, L_13, L_15, (&V_3), /*hidden argument*/NULL);
			V_2 = L_16;
			bool L_17 = fsResult_get_Succeeded_m3237049006((&V_2), /*hidden argument*/NULL);
			if (L_17)
			{
				goto IL_0073;
			}
		}

IL_0066:
		{
			String_t* L_18 = fsResult_get_FormattedMessages_m3414202484((&V_2), /*hidden argument*/NULL);
			WatsonException_t1186470237 * L_19 = (WatsonException_t1186470237 *)il2cpp_codegen_object_new(WatsonException_t1186470237_il2cpp_TypeInfo_var);
			WatsonException__ctor_m3825115789(L_19, L_18, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_19);
		}

IL_0073:
		{
			goto IL_00a5;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1927440687_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0078;
		throw e;
	}

CATCH_0078:
	{ // begin catch(System.Exception)
		V_4 = ((Exception_t1927440687 *)__exception_local);
		ObjectU5BU5D_t3614634134* L_20 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1));
		Exception_t1927440687 * L_21 = V_4;
		NullCheck(L_21);
		String_t* L_22 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_21);
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, L_22);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_22);
		Log_Error_m685270089(NULL /*static, unused*/, _stringLiteral2798243012, _stringLiteral3851805490, L_20, /*hidden argument*/NULL);
		Response_t429319368 * L_23 = ___resp1;
		NullCheck(L_23);
		Response_set_Success_m3746673474(L_23, (bool)0, /*hidden argument*/NULL);
		goto IL_00a5;
	} // end catch (depth: 1)

IL_00a5:
	{
		Request_t466816980 * L_24 = ___req0;
		NullCheck(((GetPronunciationReq_t2533372478 *)CastclassClass((RuntimeObject*)L_24, GetPronunciationReq_t2533372478_il2cpp_TypeInfo_var)));
		GetPronunciationCallback_t2622344657 * L_25 = GetPronunciationReq_get_Callback_m3167238787(((GetPronunciationReq_t2533372478 *)CastclassClass((RuntimeObject*)L_24, GetPronunciationReq_t2533372478_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00d7;
		}
	}
	{
		Request_t466816980 * L_26 = ___req0;
		NullCheck(((GetPronunciationReq_t2533372478 *)CastclassClass((RuntimeObject*)L_26, GetPronunciationReq_t2533372478_il2cpp_TypeInfo_var)));
		GetPronunciationCallback_t2622344657 * L_27 = GetPronunciationReq_get_Callback_m3167238787(((GetPronunciationReq_t2533372478 *)CastclassClass((RuntimeObject*)L_26, GetPronunciationReq_t2533372478_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		Response_t429319368 * L_28 = ___resp1;
		NullCheck(L_28);
		bool L_29 = Response_get_Success_m2313267345(L_28, /*hidden argument*/NULL);
		G_B9_0 = L_27;
		if (!L_29)
		{
			G_B10_0 = L_27;
			goto IL_00d1;
		}
	}
	{
		Pronunciation_t2711344207 * L_30 = V_0;
		G_B11_0 = L_30;
		G_B11_1 = G_B9_0;
		goto IL_00d2;
	}

IL_00d1:
	{
		G_B11_0 = ((Pronunciation_t2711344207 *)(NULL));
		G_B11_1 = G_B10_0;
	}

IL_00d2:
	{
		NullCheck(G_B11_1);
		GetPronunciationCallback_Invoke_m2518062913(G_B11_1, G_B11_0, /*hidden argument*/NULL);
	}

IL_00d7:
	{
		return;
	}
}
// System.Boolean IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech::GetCustomizations(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationsCallback,System.String)
extern "C"  bool TextToSpeech_GetCustomizations_m1701334691 (TextToSpeech_t3349357562 * __this, GetCustomizationsCallback_t201646696 * ___callback0, String_t* ___customData1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextToSpeech_GetCustomizations_m1701334691_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GetCustomizationsReq_t2206589443 * V_0 = NULL;
	RESTConnector_t3705102247 * V_1 = NULL;
	{
		GetCustomizationsCallback_t201646696 * L_0 = ___callback0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, _stringLiteral1913944053, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		GetCustomizationsReq_t2206589443 * L_2 = (GetCustomizationsReq_t2206589443 *)il2cpp_codegen_object_new(GetCustomizationsReq_t2206589443_il2cpp_TypeInfo_var);
		GetCustomizationsReq__ctor_m724592476(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		GetCustomizationsReq_t2206589443 * L_3 = V_0;
		GetCustomizationsCallback_t201646696 * L_4 = ___callback0;
		NullCheck(L_3);
		GetCustomizationsReq_set_Callback_m483150754(L_3, L_4, /*hidden argument*/NULL);
		GetCustomizationsReq_t2206589443 * L_5 = V_0;
		String_t* L_6 = ___customData1;
		NullCheck(L_5);
		GetCustomizationsReq_set_Data_m3616098737(L_5, L_6, /*hidden argument*/NULL);
		GetCustomizationsReq_t2206589443 * L_7 = V_0;
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)TextToSpeech_OnGetCustomizationsResp_m3083543054_RuntimeMethod_var);
		ResponseEvent_t1616568356 * L_9 = (ResponseEvent_t1616568356 *)il2cpp_codegen_object_new(ResponseEvent_t1616568356_il2cpp_TypeInfo_var);
		ResponseEvent__ctor_m3385394859(L_9, __this, L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		Request_set_OnResponse_m2226001952(L_7, L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RESTConnector_t3705102247_il2cpp_TypeInfo_var);
		RESTConnector_t3705102247 * L_10 = RESTConnector_GetConnector_m3262139854(NULL /*static, unused*/, _stringLiteral2566220529, _stringLiteral3384877589, (bool)1, /*hidden argument*/NULL);
		V_1 = L_10;
		RESTConnector_t3705102247 * L_11 = V_1;
		if (L_11)
		{
			goto IL_0050;
		}
	}
	{
		return (bool)0;
	}

IL_0050:
	{
		RESTConnector_t3705102247 * L_12 = V_1;
		GetCustomizationsReq_t2206589443 * L_13 = V_0;
		NullCheck(L_12);
		bool L_14 = RESTConnector_Send_m2925366401(L_12, L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech::OnGetCustomizationsResp(IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request,IBM.Watson.DeveloperCloud.Connection.RESTConnector/Response)
extern "C"  void TextToSpeech_OnGetCustomizationsResp_m3083543054 (TextToSpeech_t3349357562 * __this, Request_t466816980 * ___req0, Response_t429319368 * ___resp1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextToSpeech_OnGetCustomizationsResp_m3083543054_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Customizations_t482380184 * V_0 = NULL;
	fsData_t2583805605 * V_1 = NULL;
	fsResult_t862419890  V_2;
	memset(&V_2, 0, sizeof(V_2));
	RuntimeObject * V_3 = NULL;
	Exception_t1927440687 * V_4 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	GetCustomizationsCallback_t201646696 * G_B10_0 = NULL;
	GetCustomizationsCallback_t201646696 * G_B9_0 = NULL;
	Customizations_t482380184 * G_B11_0 = NULL;
	GetCustomizationsCallback_t201646696 * G_B11_1 = NULL;
	{
		Customizations_t482380184 * L_0 = (Customizations_t482380184 *)il2cpp_codegen_object_new(Customizations_t482380184_il2cpp_TypeInfo_var);
		Customizations__ctor_m1298341255(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Response_t429319368 * L_1 = ___resp1;
		NullCheck(L_1);
		bool L_2 = Response_get_Success_m2313267345(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_00a5;
		}
	}

IL_0011:
	try
	{ // begin try (depth: 1)
		{
			V_1 = (fsData_t2583805605 *)NULL;
			IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
			Encoding_t663144255 * L_3 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
			Response_t429319368 * L_4 = ___resp1;
			NullCheck(L_4);
			ByteU5BU5D_t3397334013* L_5 = Response_get_Data_m3078004018(L_4, /*hidden argument*/NULL);
			NullCheck(L_3);
			String_t* L_6 = VirtFuncInvoker1< String_t*, ByteU5BU5D_t3397334013* >::Invoke(22 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_3, L_5);
			fsResult_t862419890  L_7 = fsJsonParser_Parse_m4200302395(NULL /*static, unused*/, L_6, (&V_1), /*hidden argument*/NULL);
			V_2 = L_7;
			bool L_8 = fsResult_get_Succeeded_m3237049006((&V_2), /*hidden argument*/NULL);
			if (L_8)
			{
				goto IL_0044;
			}
		}

IL_0037:
		{
			String_t* L_9 = fsResult_get_FormattedMessages_m3414202484((&V_2), /*hidden argument*/NULL);
			WatsonException_t1186470237 * L_10 = (WatsonException_t1186470237 *)il2cpp_codegen_object_new(WatsonException_t1186470237_il2cpp_TypeInfo_var);
			WatsonException__ctor_m3825115789(L_10, L_9, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_10);
		}

IL_0044:
		{
			Customizations_t482380184 * L_11 = V_0;
			V_3 = L_11;
			IL2CPP_RUNTIME_CLASS_INIT(TextToSpeech_t3349357562_il2cpp_TypeInfo_var);
			fsSerializer_t4193731081 * L_12 = ((TextToSpeech_t3349357562_StaticFields*)il2cpp_codegen_static_fields_for(TextToSpeech_t3349357562_il2cpp_TypeInfo_var))->get_sm_Serializer_6();
			fsData_t2583805605 * L_13 = V_1;
			RuntimeObject * L_14 = V_3;
			NullCheck(L_14);
			Type_t * L_15 = Object_GetType_m191970594(L_14, /*hidden argument*/NULL);
			NullCheck(L_12);
			fsResult_t862419890  L_16 = fsSerializer_TryDeserialize_m4134098758(L_12, L_13, L_15, (&V_3), /*hidden argument*/NULL);
			V_2 = L_16;
			bool L_17 = fsResult_get_Succeeded_m3237049006((&V_2), /*hidden argument*/NULL);
			if (L_17)
			{
				goto IL_0073;
			}
		}

IL_0066:
		{
			String_t* L_18 = fsResult_get_FormattedMessages_m3414202484((&V_2), /*hidden argument*/NULL);
			WatsonException_t1186470237 * L_19 = (WatsonException_t1186470237 *)il2cpp_codegen_object_new(WatsonException_t1186470237_il2cpp_TypeInfo_var);
			WatsonException__ctor_m3825115789(L_19, L_18, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_19);
		}

IL_0073:
		{
			goto IL_00a5;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1927440687_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0078;
		throw e;
	}

CATCH_0078:
	{ // begin catch(System.Exception)
		V_4 = ((Exception_t1927440687 *)__exception_local);
		ObjectU5BU5D_t3614634134* L_20 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1));
		Exception_t1927440687 * L_21 = V_4;
		NullCheck(L_21);
		String_t* L_22 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_21);
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, L_22);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_22);
		Log_Error_m685270089(NULL /*static, unused*/, _stringLiteral2798243012, _stringLiteral2952090043, L_20, /*hidden argument*/NULL);
		Response_t429319368 * L_23 = ___resp1;
		NullCheck(L_23);
		Response_set_Success_m3746673474(L_23, (bool)0, /*hidden argument*/NULL);
		goto IL_00a5;
	} // end catch (depth: 1)

IL_00a5:
	{
		Request_t466816980 * L_24 = ___req0;
		NullCheck(((GetCustomizationsReq_t2206589443 *)CastclassClass((RuntimeObject*)L_24, GetCustomizationsReq_t2206589443_il2cpp_TypeInfo_var)));
		GetCustomizationsCallback_t201646696 * L_25 = GetCustomizationsReq_get_Callback_m4047984775(((GetCustomizationsReq_t2206589443 *)CastclassClass((RuntimeObject*)L_24, GetCustomizationsReq_t2206589443_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00e2;
		}
	}
	{
		Request_t466816980 * L_26 = ___req0;
		NullCheck(((GetCustomizationsReq_t2206589443 *)CastclassClass((RuntimeObject*)L_26, GetCustomizationsReq_t2206589443_il2cpp_TypeInfo_var)));
		GetCustomizationsCallback_t201646696 * L_27 = GetCustomizationsReq_get_Callback_m4047984775(((GetCustomizationsReq_t2206589443 *)CastclassClass((RuntimeObject*)L_26, GetCustomizationsReq_t2206589443_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		Response_t429319368 * L_28 = ___resp1;
		NullCheck(L_28);
		bool L_29 = Response_get_Success_m2313267345(L_28, /*hidden argument*/NULL);
		G_B9_0 = L_27;
		if (!L_29)
		{
			G_B10_0 = L_27;
			goto IL_00d1;
		}
	}
	{
		Customizations_t482380184 * L_30 = V_0;
		G_B11_0 = L_30;
		G_B11_1 = G_B9_0;
		goto IL_00d2;
	}

IL_00d1:
	{
		G_B11_0 = ((Customizations_t482380184 *)(NULL));
		G_B11_1 = G_B10_0;
	}

IL_00d2:
	{
		Request_t466816980 * L_31 = ___req0;
		NullCheck(((GetCustomizationsReq_t2206589443 *)CastclassClass((RuntimeObject*)L_31, GetCustomizationsReq_t2206589443_il2cpp_TypeInfo_var)));
		String_t* L_32 = GetCustomizationsReq_get_Data_m1806572778(((GetCustomizationsReq_t2206589443 *)CastclassClass((RuntimeObject*)L_31, GetCustomizationsReq_t2206589443_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		NullCheck(G_B11_1);
		GetCustomizationsCallback_Invoke_m340907479(G_B11_1, G_B11_0, L_32, /*hidden argument*/NULL);
	}

IL_00e2:
	{
		return;
	}
}
// System.Boolean IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech::CreateCustomization(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/CreateCustomizationCallback,System.String,System.String,System.String,System.String)
extern "C"  bool TextToSpeech_CreateCustomization_m1090628485 (TextToSpeech_t3349357562 * __this, CreateCustomizationCallback_t3071584865 * ___callback0, String_t* ___name1, String_t* ___language2, String_t* ___description3, String_t* ___customData4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextToSpeech_CreateCustomization_m1090628485_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CustomVoice_t330695553 * V_0 = NULL;
	fsData_t2583805605 * V_1 = NULL;
	fsResult_t862419890  V_2;
	memset(&V_2, 0, sizeof(V_2));
	String_t* V_3 = NULL;
	CreateCustomizationRequest_t295745401 * V_4 = NULL;
	RESTConnector_t3705102247 * V_5 = NULL;
	{
		CreateCustomizationCallback_t3071584865 * L_0 = ___callback0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, _stringLiteral1913944053, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		String_t* L_2 = ___name1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0027;
		}
	}
	{
		ArgumentNullException_t628810857 * L_4 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_4, _stringLiteral701067223, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0027:
	{
		CustomVoice_t330695553 * L_5 = (CustomVoice_t330695553 *)il2cpp_codegen_object_new(CustomVoice_t330695553_il2cpp_TypeInfo_var);
		CustomVoice__ctor_m2182765280(L_5, /*hidden argument*/NULL);
		V_0 = L_5;
		CustomVoice_t330695553 * L_6 = V_0;
		String_t* L_7 = ___name1;
		NullCheck(L_6);
		CustomVoice_set_name_m2044531626(L_6, L_7, /*hidden argument*/NULL);
		CustomVoice_t330695553 * L_8 = V_0;
		String_t* L_9 = ___language2;
		NullCheck(L_8);
		CustomVoice_set_language_m2534117753(L_8, L_9, /*hidden argument*/NULL);
		CustomVoice_t330695553 * L_10 = V_0;
		String_t* L_11 = ___description3;
		NullCheck(L_10);
		CustomVoice_set_description_m1558525425(L_10, L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TextToSpeech_t3349357562_il2cpp_TypeInfo_var);
		fsSerializer_t4193731081 * L_12 = ((TextToSpeech_t3349357562_StaticFields*)il2cpp_codegen_static_fields_for(TextToSpeech_t3349357562_il2cpp_TypeInfo_var))->get_sm_Serializer_6();
		CustomVoice_t330695553 * L_13 = V_0;
		NullCheck(L_13);
		Type_t * L_14 = Object_GetType_m191970594(L_13, /*hidden argument*/NULL);
		CustomVoice_t330695553 * L_15 = V_0;
		NullCheck(L_12);
		fsResult_t862419890  L_16 = fsSerializer_TrySerialize_m867329083(L_12, L_14, L_15, (&V_1), /*hidden argument*/NULL);
		V_2 = L_16;
		fsResult_AssertSuccessWithoutWarnings_m1273657906((&V_2), /*hidden argument*/NULL);
		fsData_t2583805605 * L_17 = V_1;
		String_t* L_18 = fsJsonPrinter_CompressedJson_m2458408074(NULL /*static, unused*/, L_17, (bool)0, /*hidden argument*/NULL);
		V_3 = L_18;
		CreateCustomizationRequest_t295745401 * L_19 = (CreateCustomizationRequest_t295745401 *)il2cpp_codegen_object_new(CreateCustomizationRequest_t295745401_il2cpp_TypeInfo_var);
		CreateCustomizationRequest__ctor_m3981671984(L_19, /*hidden argument*/NULL);
		V_4 = L_19;
		CreateCustomizationRequest_t295745401 * L_20 = V_4;
		CreateCustomizationCallback_t3071584865 * L_21 = ___callback0;
		NullCheck(L_20);
		CreateCustomizationRequest_set_Callback_m709816875(L_20, L_21, /*hidden argument*/NULL);
		CreateCustomizationRequest_t295745401 * L_22 = V_4;
		CustomVoice_t330695553 * L_23 = V_0;
		NullCheck(L_22);
		CreateCustomizationRequest_set_CustomVoice_m1331556427(L_22, L_23, /*hidden argument*/NULL);
		CreateCustomizationRequest_t295745401 * L_24 = V_4;
		String_t* L_25 = ___customData4;
		NullCheck(L_24);
		CreateCustomizationRequest_set_Data_m3310641787(L_24, L_25, /*hidden argument*/NULL);
		CreateCustomizationRequest_t295745401 * L_26 = V_4;
		NullCheck(L_26);
		Dictionary_2_t3943999495 * L_27 = Request_get_Headers_m340270624(L_26, /*hidden argument*/NULL);
		NullCheck(L_27);
		Dictionary_2_set_Item_m4244870320(L_27, _stringLiteral1048821954, _stringLiteral1391431453, /*hidden argument*/Dictionary_2_set_Item_m4244870320_RuntimeMethod_var);
		CreateCustomizationRequest_t295745401 * L_28 = V_4;
		NullCheck(L_28);
		Dictionary_2_t3943999495 * L_29 = Request_get_Headers_m340270624(L_28, /*hidden argument*/NULL);
		NullCheck(L_29);
		Dictionary_2_set_Item_m4244870320(L_29, _stringLiteral3819387978, _stringLiteral1391431453, /*hidden argument*/Dictionary_2_set_Item_m4244870320_RuntimeMethod_var);
		CreateCustomizationRequest_t295745401 * L_30 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
		Encoding_t663144255 * L_31 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_32 = V_3;
		NullCheck(L_31);
		ByteU5BU5D_t3397334013* L_33 = VirtFuncInvoker1< ByteU5BU5D_t3397334013*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_31, L_32);
		NullCheck(L_30);
		Request_set_Send_m296178747(L_30, L_33, /*hidden argument*/NULL);
		CreateCustomizationRequest_t295745401 * L_34 = V_4;
		IntPtr_t L_35;
		L_35.set_m_value_0((void*)(void*)TextToSpeech_OnCreateCustomizationResp_m1680638733_RuntimeMethod_var);
		ResponseEvent_t1616568356 * L_36 = (ResponseEvent_t1616568356 *)il2cpp_codegen_object_new(ResponseEvent_t1616568356_il2cpp_TypeInfo_var);
		ResponseEvent__ctor_m3385394859(L_36, __this, L_35, /*hidden argument*/NULL);
		NullCheck(L_34);
		Request_set_OnResponse_m2226001952(L_34, L_36, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RESTConnector_t3705102247_il2cpp_TypeInfo_var);
		RESTConnector_t3705102247 * L_37 = RESTConnector_GetConnector_m3262139854(NULL /*static, unused*/, _stringLiteral2566220529, _stringLiteral3384877589, (bool)1, /*hidden argument*/NULL);
		V_5 = L_37;
		RESTConnector_t3705102247 * L_38 = V_5;
		if (L_38)
		{
			goto IL_00f3;
		}
	}
	{
		return (bool)0;
	}

IL_00f3:
	{
		RESTConnector_t3705102247 * L_39 = V_5;
		CreateCustomizationRequest_t295745401 * L_40 = V_4;
		NullCheck(L_39);
		bool L_41 = RESTConnector_Send_m2925366401(L_39, L_40, /*hidden argument*/NULL);
		return L_41;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech::OnCreateCustomizationResp(IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request,IBM.Watson.DeveloperCloud.Connection.RESTConnector/Response)
extern "C"  void TextToSpeech_OnCreateCustomizationResp_m1680638733 (TextToSpeech_t3349357562 * __this, Request_t466816980 * ___req0, Response_t429319368 * ___resp1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextToSpeech_OnCreateCustomizationResp_m1680638733_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CustomizationID_t344279642 * V_0 = NULL;
	fsData_t2583805605 * V_1 = NULL;
	fsResult_t862419890  V_2;
	memset(&V_2, 0, sizeof(V_2));
	RuntimeObject * V_3 = NULL;
	Exception_t1927440687 * V_4 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	CreateCustomizationCallback_t3071584865 * G_B10_0 = NULL;
	CreateCustomizationCallback_t3071584865 * G_B9_0 = NULL;
	CustomizationID_t344279642 * G_B11_0 = NULL;
	CreateCustomizationCallback_t3071584865 * G_B11_1 = NULL;
	{
		CustomizationID_t344279642 * L_0 = (CustomizationID_t344279642 *)il2cpp_codegen_object_new(CustomizationID_t344279642_il2cpp_TypeInfo_var);
		CustomizationID__ctor_m4015391793(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Response_t429319368 * L_1 = ___resp1;
		NullCheck(L_1);
		bool L_2 = Response_get_Success_m2313267345(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_00a5;
		}
	}

IL_0011:
	try
	{ // begin try (depth: 1)
		{
			V_1 = (fsData_t2583805605 *)NULL;
			IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
			Encoding_t663144255 * L_3 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
			Response_t429319368 * L_4 = ___resp1;
			NullCheck(L_4);
			ByteU5BU5D_t3397334013* L_5 = Response_get_Data_m3078004018(L_4, /*hidden argument*/NULL);
			NullCheck(L_3);
			String_t* L_6 = VirtFuncInvoker1< String_t*, ByteU5BU5D_t3397334013* >::Invoke(22 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_3, L_5);
			fsResult_t862419890  L_7 = fsJsonParser_Parse_m4200302395(NULL /*static, unused*/, L_6, (&V_1), /*hidden argument*/NULL);
			V_2 = L_7;
			bool L_8 = fsResult_get_Succeeded_m3237049006((&V_2), /*hidden argument*/NULL);
			if (L_8)
			{
				goto IL_0044;
			}
		}

IL_0037:
		{
			String_t* L_9 = fsResult_get_FormattedMessages_m3414202484((&V_2), /*hidden argument*/NULL);
			WatsonException_t1186470237 * L_10 = (WatsonException_t1186470237 *)il2cpp_codegen_object_new(WatsonException_t1186470237_il2cpp_TypeInfo_var);
			WatsonException__ctor_m3825115789(L_10, L_9, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_10);
		}

IL_0044:
		{
			CustomizationID_t344279642 * L_11 = V_0;
			V_3 = L_11;
			IL2CPP_RUNTIME_CLASS_INIT(TextToSpeech_t3349357562_il2cpp_TypeInfo_var);
			fsSerializer_t4193731081 * L_12 = ((TextToSpeech_t3349357562_StaticFields*)il2cpp_codegen_static_fields_for(TextToSpeech_t3349357562_il2cpp_TypeInfo_var))->get_sm_Serializer_6();
			fsData_t2583805605 * L_13 = V_1;
			RuntimeObject * L_14 = V_3;
			NullCheck(L_14);
			Type_t * L_15 = Object_GetType_m191970594(L_14, /*hidden argument*/NULL);
			NullCheck(L_12);
			fsResult_t862419890  L_16 = fsSerializer_TryDeserialize_m4134098758(L_12, L_13, L_15, (&V_3), /*hidden argument*/NULL);
			V_2 = L_16;
			bool L_17 = fsResult_get_Succeeded_m3237049006((&V_2), /*hidden argument*/NULL);
			if (L_17)
			{
				goto IL_0073;
			}
		}

IL_0066:
		{
			String_t* L_18 = fsResult_get_FormattedMessages_m3414202484((&V_2), /*hidden argument*/NULL);
			WatsonException_t1186470237 * L_19 = (WatsonException_t1186470237 *)il2cpp_codegen_object_new(WatsonException_t1186470237_il2cpp_TypeInfo_var);
			WatsonException__ctor_m3825115789(L_19, L_18, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_19);
		}

IL_0073:
		{
			goto IL_00a5;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1927440687_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0078;
		throw e;
	}

CATCH_0078:
	{ // begin catch(System.Exception)
		V_4 = ((Exception_t1927440687 *)__exception_local);
		ObjectU5BU5D_t3614634134* L_20 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1));
		Exception_t1927440687 * L_21 = V_4;
		NullCheck(L_21);
		String_t* L_22 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_21);
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, L_22);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_22);
		Log_Error_m685270089(NULL /*static, unused*/, _stringLiteral2798243012, _stringLiteral1155517690, L_20, /*hidden argument*/NULL);
		Response_t429319368 * L_23 = ___resp1;
		NullCheck(L_23);
		Response_set_Success_m3746673474(L_23, (bool)0, /*hidden argument*/NULL);
		goto IL_00a5;
	} // end catch (depth: 1)

IL_00a5:
	{
		Request_t466816980 * L_24 = ___req0;
		NullCheck(((CreateCustomizationRequest_t295745401 *)CastclassClass((RuntimeObject*)L_24, CreateCustomizationRequest_t295745401_il2cpp_TypeInfo_var)));
		CreateCustomizationCallback_t3071584865 * L_25 = CreateCustomizationRequest_get_Callback_m3064008816(((CreateCustomizationRequest_t295745401 *)CastclassClass((RuntimeObject*)L_24, CreateCustomizationRequest_t295745401_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00e2;
		}
	}
	{
		Request_t466816980 * L_26 = ___req0;
		NullCheck(((CreateCustomizationRequest_t295745401 *)CastclassClass((RuntimeObject*)L_26, CreateCustomizationRequest_t295745401_il2cpp_TypeInfo_var)));
		CreateCustomizationCallback_t3071584865 * L_27 = CreateCustomizationRequest_get_Callback_m3064008816(((CreateCustomizationRequest_t295745401 *)CastclassClass((RuntimeObject*)L_26, CreateCustomizationRequest_t295745401_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		Response_t429319368 * L_28 = ___resp1;
		NullCheck(L_28);
		bool L_29 = Response_get_Success_m2313267345(L_28, /*hidden argument*/NULL);
		G_B9_0 = L_27;
		if (!L_29)
		{
			G_B10_0 = L_27;
			goto IL_00d1;
		}
	}
	{
		CustomizationID_t344279642 * L_30 = V_0;
		G_B11_0 = L_30;
		G_B11_1 = G_B9_0;
		goto IL_00d2;
	}

IL_00d1:
	{
		G_B11_0 = ((CustomizationID_t344279642 *)(NULL));
		G_B11_1 = G_B10_0;
	}

IL_00d2:
	{
		Request_t466816980 * L_31 = ___req0;
		NullCheck(((CreateCustomizationRequest_t295745401 *)CastclassClass((RuntimeObject*)L_31, CreateCustomizationRequest_t295745401_il2cpp_TypeInfo_var)));
		String_t* L_32 = CreateCustomizationRequest_get_Data_m2559910470(((CreateCustomizationRequest_t295745401 *)CastclassClass((RuntimeObject*)L_31, CreateCustomizationRequest_t295745401_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		NullCheck(G_B11_1);
		CreateCustomizationCallback_Invoke_m3243793162(G_B11_1, G_B11_0, L_32, /*hidden argument*/NULL);
	}

IL_00e2:
	{
		return;
	}
}
// System.Boolean IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech::DeleteCustomization(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/OnDeleteCustomizationCallback,System.String,System.String)
extern "C"  bool TextToSpeech_DeleteCustomization_m2634893744 (TextToSpeech_t3349357562 * __this, OnDeleteCustomizationCallback_t2862971265 * ___callback0, String_t* ___customizationID1, String_t* ___customData2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextToSpeech_DeleteCustomization_m2634893744_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DeleteCustomizationRequest_t4044506578 * V_0 = NULL;
	String_t* V_1 = NULL;
	RESTConnector_t3705102247 * V_2 = NULL;
	{
		OnDeleteCustomizationCallback_t2862971265 * L_0 = ___callback0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, _stringLiteral1913944053, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		String_t* L_2 = ___customizationID1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0027;
		}
	}
	{
		ArgumentNullException_t628810857 * L_4 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_4, _stringLiteral1878474307, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0027:
	{
		DeleteCustomizationRequest_t4044506578 * L_5 = (DeleteCustomizationRequest_t4044506578 *)il2cpp_codegen_object_new(DeleteCustomizationRequest_t4044506578_il2cpp_TypeInfo_var);
		DeleteCustomizationRequest__ctor_m2097311445(L_5, /*hidden argument*/NULL);
		V_0 = L_5;
		DeleteCustomizationRequest_t4044506578 * L_6 = V_0;
		OnDeleteCustomizationCallback_t2862971265 * L_7 = ___callback0;
		NullCheck(L_6);
		DeleteCustomizationRequest_set_Callback_m1090382596(L_6, L_7, /*hidden argument*/NULL);
		DeleteCustomizationRequest_t4044506578 * L_8 = V_0;
		String_t* L_9 = ___customizationID1;
		NullCheck(L_8);
		DeleteCustomizationRequest_set_CustomizationID_m1101845264(L_8, L_9, /*hidden argument*/NULL);
		DeleteCustomizationRequest_t4044506578 * L_10 = V_0;
		String_t* L_11 = ___customData2;
		NullCheck(L_10);
		DeleteCustomizationRequest_set_Data_m3245021746(L_10, L_11, /*hidden argument*/NULL);
		DeleteCustomizationRequest_t4044506578 * L_12 = V_0;
		NullCheck(L_12);
		Request_set_Timeout_m3686318020(L_12, (600.0f), /*hidden argument*/NULL);
		DeleteCustomizationRequest_t4044506578 * L_13 = V_0;
		NullCheck(L_13);
		Request_set_Delete_m357548768(L_13, (bool)1, /*hidden argument*/NULL);
		DeleteCustomizationRequest_t4044506578 * L_14 = V_0;
		IntPtr_t L_15;
		L_15.set_m_value_0((void*)(void*)TextToSpeech_OnDeleteCustomizationResp_m4092725412_RuntimeMethod_var);
		ResponseEvent_t1616568356 * L_16 = (ResponseEvent_t1616568356 *)il2cpp_codegen_object_new(ResponseEvent_t1616568356_il2cpp_TypeInfo_var);
		ResponseEvent__ctor_m3385394859(L_16, __this, L_15, /*hidden argument*/NULL);
		NullCheck(L_14);
		Request_set_OnResponse_m2226001952(L_14, L_16, /*hidden argument*/NULL);
		V_1 = _stringLiteral2160400232;
		String_t* L_17 = V_1;
		String_t* L_18 = ___customizationID1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = String_Format_m2024975688(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RESTConnector_t3705102247_il2cpp_TypeInfo_var);
		RESTConnector_t3705102247 * L_20 = RESTConnector_GetConnector_m3262139854(NULL /*static, unused*/, _stringLiteral2566220529, L_19, (bool)1, /*hidden argument*/NULL);
		V_2 = L_20;
		RESTConnector_t3705102247 * L_21 = V_2;
		if (L_21)
		{
			goto IL_0087;
		}
	}
	{
		return (bool)0;
	}

IL_0087:
	{
		RESTConnector_t3705102247 * L_22 = V_2;
		DeleteCustomizationRequest_t4044506578 * L_23 = V_0;
		NullCheck(L_22);
		bool L_24 = RESTConnector_Send_m2925366401(L_22, L_23, /*hidden argument*/NULL);
		return L_24;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech::OnDeleteCustomizationResp(IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request,IBM.Watson.DeveloperCloud.Connection.RESTConnector/Response)
extern "C"  void TextToSpeech_OnDeleteCustomizationResp_m4092725412 (TextToSpeech_t3349357562 * __this, Request_t466816980 * ___req0, Response_t429319368 * ___resp1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextToSpeech_OnDeleteCustomizationResp_m4092725412_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Request_t466816980 * L_0 = ___req0;
		NullCheck(((DeleteCustomizationRequest_t4044506578 *)CastclassClass((RuntimeObject*)L_0, DeleteCustomizationRequest_t4044506578_il2cpp_TypeInfo_var)));
		OnDeleteCustomizationCallback_t2862971265 * L_1 = DeleteCustomizationRequest_get_Callback_m3675390565(((DeleteCustomizationRequest_t4044506578 *)CastclassClass((RuntimeObject*)L_0, DeleteCustomizationRequest_t4044506578_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0031;
		}
	}
	{
		Request_t466816980 * L_2 = ___req0;
		NullCheck(((DeleteCustomizationRequest_t4044506578 *)CastclassClass((RuntimeObject*)L_2, DeleteCustomizationRequest_t4044506578_il2cpp_TypeInfo_var)));
		OnDeleteCustomizationCallback_t2862971265 * L_3 = DeleteCustomizationRequest_get_Callback_m3675390565(((DeleteCustomizationRequest_t4044506578 *)CastclassClass((RuntimeObject*)L_2, DeleteCustomizationRequest_t4044506578_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		Response_t429319368 * L_4 = ___resp1;
		NullCheck(L_4);
		bool L_5 = Response_get_Success_m2313267345(L_4, /*hidden argument*/NULL);
		Request_t466816980 * L_6 = ___req0;
		NullCheck(((DeleteCustomizationRequest_t4044506578 *)CastclassClass((RuntimeObject*)L_6, DeleteCustomizationRequest_t4044506578_il2cpp_TypeInfo_var)));
		String_t* L_7 = DeleteCustomizationRequest_get_Data_m669478649(((DeleteCustomizationRequest_t4044506578 *)CastclassClass((RuntimeObject*)L_6, DeleteCustomizationRequest_t4044506578_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		NullCheck(L_3);
		OnDeleteCustomizationCallback_Invoke_m2846474177(L_3, L_5, L_7, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Boolean IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech::GetCustomization(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationCallback,System.String,System.String)
extern "C"  bool TextToSpeech_GetCustomization_m2331125653 (TextToSpeech_t3349357562 * __this, GetCustomizationCallback_t4165371535 * ___callback0, String_t* ___customizationID1, String_t* ___customData2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextToSpeech_GetCustomization_m2331125653_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GetCustomizationRequest_t1351603463 * V_0 = NULL;
	String_t* V_1 = NULL;
	RESTConnector_t3705102247 * V_2 = NULL;
	{
		GetCustomizationCallback_t4165371535 * L_0 = ___callback0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, _stringLiteral1913944053, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		String_t* L_2 = ___customizationID1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0027;
		}
	}
	{
		ArgumentNullException_t628810857 * L_4 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_4, _stringLiteral3132213353, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0027:
	{
		GetCustomizationRequest_t1351603463 * L_5 = (GetCustomizationRequest_t1351603463 *)il2cpp_codegen_object_new(GetCustomizationRequest_t1351603463_il2cpp_TypeInfo_var);
		GetCustomizationRequest__ctor_m2422485738(L_5, /*hidden argument*/NULL);
		V_0 = L_5;
		GetCustomizationRequest_t1351603463 * L_6 = V_0;
		GetCustomizationCallback_t4165371535 * L_7 = ___callback0;
		NullCheck(L_6);
		GetCustomizationRequest_set_Callback_m3900696383(L_6, L_7, /*hidden argument*/NULL);
		GetCustomizationRequest_t1351603463 * L_8 = V_0;
		String_t* L_9 = ___customizationID1;
		NullCheck(L_8);
		GetCustomizationRequest_set_CustomizationID_m4212369937(L_8, L_9, /*hidden argument*/NULL);
		GetCustomizationRequest_t1351603463 * L_10 = V_0;
		String_t* L_11 = ___customData2;
		NullCheck(L_10);
		GetCustomizationRequest_set_Data_m2028509081(L_10, L_11, /*hidden argument*/NULL);
		GetCustomizationRequest_t1351603463 * L_12 = V_0;
		IntPtr_t L_13;
		L_13.set_m_value_0((void*)(void*)TextToSpeech_OnGetCustomizationResp_m2741583135_RuntimeMethod_var);
		ResponseEvent_t1616568356 * L_14 = (ResponseEvent_t1616568356 *)il2cpp_codegen_object_new(ResponseEvent_t1616568356_il2cpp_TypeInfo_var);
		ResponseEvent__ctor_m3385394859(L_14, __this, L_13, /*hidden argument*/NULL);
		NullCheck(L_12);
		Request_set_OnResponse_m2226001952(L_12, L_14, /*hidden argument*/NULL);
		V_1 = _stringLiteral2160400232;
		String_t* L_15 = V_1;
		String_t* L_16 = ___customizationID1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_17 = String_Format_m2024975688(NULL /*static, unused*/, L_15, L_16, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RESTConnector_t3705102247_il2cpp_TypeInfo_var);
		RESTConnector_t3705102247 * L_18 = RESTConnector_GetConnector_m3262139854(NULL /*static, unused*/, _stringLiteral2566220529, L_17, (bool)1, /*hidden argument*/NULL);
		V_2 = L_18;
		RESTConnector_t3705102247 * L_19 = V_2;
		if (L_19)
		{
			goto IL_0075;
		}
	}
	{
		return (bool)0;
	}

IL_0075:
	{
		RESTConnector_t3705102247 * L_20 = V_2;
		GetCustomizationRequest_t1351603463 * L_21 = V_0;
		NullCheck(L_20);
		bool L_22 = RESTConnector_Send_m2925366401(L_20, L_21, /*hidden argument*/NULL);
		return L_22;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech::OnGetCustomizationResp(IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request,IBM.Watson.DeveloperCloud.Connection.RESTConnector/Response)
extern "C"  void TextToSpeech_OnGetCustomizationResp_m2741583135 (TextToSpeech_t3349357562 * __this, Request_t466816980 * ___req0, Response_t429319368 * ___resp1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextToSpeech_OnGetCustomizationResp_m2741583135_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Customization_t2261013253 * V_0 = NULL;
	fsData_t2583805605 * V_1 = NULL;
	fsResult_t862419890  V_2;
	memset(&V_2, 0, sizeof(V_2));
	RuntimeObject * V_3 = NULL;
	Exception_t1927440687 * V_4 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	GetCustomizationCallback_t4165371535 * G_B10_0 = NULL;
	GetCustomizationCallback_t4165371535 * G_B9_0 = NULL;
	Customization_t2261013253 * G_B11_0 = NULL;
	GetCustomizationCallback_t4165371535 * G_B11_1 = NULL;
	{
		Customization_t2261013253 * L_0 = (Customization_t2261013253 *)il2cpp_codegen_object_new(Customization_t2261013253_il2cpp_TypeInfo_var);
		Customization__ctor_m1020302414(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Response_t429319368 * L_1 = ___resp1;
		NullCheck(L_1);
		bool L_2 = Response_get_Success_m2313267345(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_00a5;
		}
	}

IL_0011:
	try
	{ // begin try (depth: 1)
		{
			V_1 = (fsData_t2583805605 *)NULL;
			IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
			Encoding_t663144255 * L_3 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
			Response_t429319368 * L_4 = ___resp1;
			NullCheck(L_4);
			ByteU5BU5D_t3397334013* L_5 = Response_get_Data_m3078004018(L_4, /*hidden argument*/NULL);
			NullCheck(L_3);
			String_t* L_6 = VirtFuncInvoker1< String_t*, ByteU5BU5D_t3397334013* >::Invoke(22 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_3, L_5);
			fsResult_t862419890  L_7 = fsJsonParser_Parse_m4200302395(NULL /*static, unused*/, L_6, (&V_1), /*hidden argument*/NULL);
			V_2 = L_7;
			bool L_8 = fsResult_get_Succeeded_m3237049006((&V_2), /*hidden argument*/NULL);
			if (L_8)
			{
				goto IL_0044;
			}
		}

IL_0037:
		{
			String_t* L_9 = fsResult_get_FormattedMessages_m3414202484((&V_2), /*hidden argument*/NULL);
			WatsonException_t1186470237 * L_10 = (WatsonException_t1186470237 *)il2cpp_codegen_object_new(WatsonException_t1186470237_il2cpp_TypeInfo_var);
			WatsonException__ctor_m3825115789(L_10, L_9, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_10);
		}

IL_0044:
		{
			Customization_t2261013253 * L_11 = V_0;
			V_3 = L_11;
			IL2CPP_RUNTIME_CLASS_INIT(TextToSpeech_t3349357562_il2cpp_TypeInfo_var);
			fsSerializer_t4193731081 * L_12 = ((TextToSpeech_t3349357562_StaticFields*)il2cpp_codegen_static_fields_for(TextToSpeech_t3349357562_il2cpp_TypeInfo_var))->get_sm_Serializer_6();
			fsData_t2583805605 * L_13 = V_1;
			RuntimeObject * L_14 = V_3;
			NullCheck(L_14);
			Type_t * L_15 = Object_GetType_m191970594(L_14, /*hidden argument*/NULL);
			NullCheck(L_12);
			fsResult_t862419890  L_16 = fsSerializer_TryDeserialize_m4134098758(L_12, L_13, L_15, (&V_3), /*hidden argument*/NULL);
			V_2 = L_16;
			bool L_17 = fsResult_get_Succeeded_m3237049006((&V_2), /*hidden argument*/NULL);
			if (L_17)
			{
				goto IL_0073;
			}
		}

IL_0066:
		{
			String_t* L_18 = fsResult_get_FormattedMessages_m3414202484((&V_2), /*hidden argument*/NULL);
			WatsonException_t1186470237 * L_19 = (WatsonException_t1186470237 *)il2cpp_codegen_object_new(WatsonException_t1186470237_il2cpp_TypeInfo_var);
			WatsonException__ctor_m3825115789(L_19, L_18, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_19);
		}

IL_0073:
		{
			goto IL_00a5;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1927440687_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0078;
		throw e;
	}

CATCH_0078:
	{ // begin catch(System.Exception)
		V_4 = ((Exception_t1927440687 *)__exception_local);
		ObjectU5BU5D_t3614634134* L_20 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1));
		Exception_t1927440687 * L_21 = V_4;
		NullCheck(L_21);
		String_t* L_22 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_21);
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, L_22);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_22);
		Log_Error_m685270089(NULL /*static, unused*/, _stringLiteral2798243012, _stringLiteral1529540406, L_20, /*hidden argument*/NULL);
		Response_t429319368 * L_23 = ___resp1;
		NullCheck(L_23);
		Response_set_Success_m3746673474(L_23, (bool)0, /*hidden argument*/NULL);
		goto IL_00a5;
	} // end catch (depth: 1)

IL_00a5:
	{
		Request_t466816980 * L_24 = ___req0;
		NullCheck(((GetCustomizationRequest_t1351603463 *)CastclassClass((RuntimeObject*)L_24, GetCustomizationRequest_t1351603463_il2cpp_TypeInfo_var)));
		GetCustomizationCallback_t4165371535 * L_25 = GetCustomizationRequest_get_Callback_m1998934598(((GetCustomizationRequest_t1351603463 *)CastclassClass((RuntimeObject*)L_24, GetCustomizationRequest_t1351603463_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00e2;
		}
	}
	{
		Request_t466816980 * L_26 = ___req0;
		NullCheck(((GetCustomizationRequest_t1351603463 *)CastclassClass((RuntimeObject*)L_26, GetCustomizationRequest_t1351603463_il2cpp_TypeInfo_var)));
		GetCustomizationCallback_t4165371535 * L_27 = GetCustomizationRequest_get_Callback_m1998934598(((GetCustomizationRequest_t1351603463 *)CastclassClass((RuntimeObject*)L_26, GetCustomizationRequest_t1351603463_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		Response_t429319368 * L_28 = ___resp1;
		NullCheck(L_28);
		bool L_29 = Response_get_Success_m2313267345(L_28, /*hidden argument*/NULL);
		G_B9_0 = L_27;
		if (!L_29)
		{
			G_B10_0 = L_27;
			goto IL_00d1;
		}
	}
	{
		Customization_t2261013253 * L_30 = V_0;
		G_B11_0 = L_30;
		G_B11_1 = G_B9_0;
		goto IL_00d2;
	}

IL_00d1:
	{
		G_B11_0 = ((Customization_t2261013253 *)(NULL));
		G_B11_1 = G_B10_0;
	}

IL_00d2:
	{
		Request_t466816980 * L_31 = ___req0;
		NullCheck(((GetCustomizationRequest_t1351603463 *)CastclassClass((RuntimeObject*)L_31, GetCustomizationRequest_t1351603463_il2cpp_TypeInfo_var)));
		String_t* L_32 = GetCustomizationRequest_get_Data_m2763780886(((GetCustomizationRequest_t1351603463 *)CastclassClass((RuntimeObject*)L_31, GetCustomizationRequest_t1351603463_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		NullCheck(G_B11_1);
		GetCustomizationCallback_Invoke_m3398604559(G_B11_1, G_B11_0, L_32, /*hidden argument*/NULL);
	}

IL_00e2:
	{
		return;
	}
}
// System.Boolean IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech::UpdateCustomization(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/UpdateCustomizationCallback,System.String,IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomVoiceUpdate,System.String)
extern "C"  bool TextToSpeech_UpdateCustomization_m3116906675 (TextToSpeech_t3349357562 * __this, UpdateCustomizationCallback_t3026233620 * ___callback0, String_t* ___customizationID1, CustomVoiceUpdate_t1706248840 * ___customVoiceUpdate2, String_t* ___customData3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextToSpeech_UpdateCustomization_m3116906675_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	fsData_t2583805605 * V_0 = NULL;
	fsResult_t862419890  V_1;
	memset(&V_1, 0, sizeof(V_1));
	String_t* V_2 = NULL;
	UpdateCustomizationRequest_t1669343124 * V_3 = NULL;
	String_t* V_4 = NULL;
	RESTConnector_t3705102247 * V_5 = NULL;
	{
		UpdateCustomizationCallback_t3026233620 * L_0 = ___callback0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, _stringLiteral1913944053, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		String_t* L_2 = ___customizationID1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0027;
		}
	}
	{
		ArgumentNullException_t628810857 * L_4 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_4, _stringLiteral1734094646, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0027:
	{
		CustomVoiceUpdate_t1706248840 * L_5 = ___customVoiceUpdate2;
		NullCheck(L_5);
		bool L_6 = CustomVoiceUpdate_HasData_m2393568391(L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_003d;
		}
	}
	{
		ArgumentNullException_t628810857 * L_7 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_7, _stringLiteral62319874, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_003d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TextToSpeech_t3349357562_il2cpp_TypeInfo_var);
		fsSerializer_t4193731081 * L_8 = ((TextToSpeech_t3349357562_StaticFields*)il2cpp_codegen_static_fields_for(TextToSpeech_t3349357562_il2cpp_TypeInfo_var))->get_sm_Serializer_6();
		CustomVoiceUpdate_t1706248840 * L_9 = ___customVoiceUpdate2;
		NullCheck(L_9);
		Type_t * L_10 = Object_GetType_m191970594(L_9, /*hidden argument*/NULL);
		CustomVoiceUpdate_t1706248840 * L_11 = ___customVoiceUpdate2;
		NullCheck(L_8);
		fsResult_t862419890  L_12 = fsSerializer_TrySerialize_m867329083(L_8, L_10, L_11, (&V_0), /*hidden argument*/NULL);
		V_1 = L_12;
		fsResult_AssertSuccessWithoutWarnings_m1273657906((&V_1), /*hidden argument*/NULL);
		fsData_t2583805605 * L_13 = V_0;
		String_t* L_14 = fsJsonPrinter_CompressedJson_m2458408074(NULL /*static, unused*/, L_13, (bool)0, /*hidden argument*/NULL);
		V_2 = L_14;
		UpdateCustomizationRequest_t1669343124 * L_15 = (UpdateCustomizationRequest_t1669343124 *)il2cpp_codegen_object_new(UpdateCustomizationRequest_t1669343124_il2cpp_TypeInfo_var);
		UpdateCustomizationRequest__ctor_m2167726953(L_15, /*hidden argument*/NULL);
		V_3 = L_15;
		UpdateCustomizationRequest_t1669343124 * L_16 = V_3;
		UpdateCustomizationCallback_t3026233620 * L_17 = ___callback0;
		NullCheck(L_16);
		UpdateCustomizationRequest_set_Callback_m374347127(L_16, L_17, /*hidden argument*/NULL);
		UpdateCustomizationRequest_t1669343124 * L_18 = V_3;
		CustomVoiceUpdate_t1706248840 * L_19 = ___customVoiceUpdate2;
		NullCheck(L_18);
		UpdateCustomizationRequest_set_CustomVoiceUpdate_m1632905410(L_18, L_19, /*hidden argument*/NULL);
		UpdateCustomizationRequest_t1669343124 * L_20 = V_3;
		String_t* L_21 = ___customizationID1;
		NullCheck(L_20);
		UpdateCustomizationRequest_set_CustomizationID_m3383711138(L_20, L_21, /*hidden argument*/NULL);
		UpdateCustomizationRequest_t1669343124 * L_22 = V_3;
		String_t* L_23 = ___customData3;
		NullCheck(L_22);
		UpdateCustomizationRequest_set_Data_m3367529024(L_22, L_23, /*hidden argument*/NULL);
		UpdateCustomizationRequest_t1669343124 * L_24 = V_3;
		NullCheck(L_24);
		Dictionary_2_t3943999495 * L_25 = Request_get_Headers_m340270624(L_24, /*hidden argument*/NULL);
		NullCheck(L_25);
		Dictionary_2_set_Item_m4244870320(L_25, _stringLiteral1048821954, _stringLiteral1391431453, /*hidden argument*/Dictionary_2_set_Item_m4244870320_RuntimeMethod_var);
		UpdateCustomizationRequest_t1669343124 * L_26 = V_3;
		NullCheck(L_26);
		Dictionary_2_t3943999495 * L_27 = Request_get_Headers_m340270624(L_26, /*hidden argument*/NULL);
		NullCheck(L_27);
		Dictionary_2_set_Item_m4244870320(L_27, _stringLiteral3819387978, _stringLiteral1391431453, /*hidden argument*/Dictionary_2_set_Item_m4244870320_RuntimeMethod_var);
		UpdateCustomizationRequest_t1669343124 * L_28 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
		Encoding_t663144255 * L_29 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_30 = V_2;
		NullCheck(L_29);
		ByteU5BU5D_t3397334013* L_31 = VirtFuncInvoker1< ByteU5BU5D_t3397334013*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_29, L_30);
		NullCheck(L_28);
		Request_set_Send_m296178747(L_28, L_31, /*hidden argument*/NULL);
		UpdateCustomizationRequest_t1669343124 * L_32 = V_3;
		IntPtr_t L_33;
		L_33.set_m_value_0((void*)(void*)TextToSpeech_OnUpdateCustomizationResp_m1295820546_RuntimeMethod_var);
		ResponseEvent_t1616568356 * L_34 = (ResponseEvent_t1616568356 *)il2cpp_codegen_object_new(ResponseEvent_t1616568356_il2cpp_TypeInfo_var);
		ResponseEvent__ctor_m3385394859(L_34, __this, L_33, /*hidden argument*/NULL);
		NullCheck(L_32);
		Request_set_OnResponse_m2226001952(L_32, L_34, /*hidden argument*/NULL);
		V_4 = _stringLiteral2160400232;
		String_t* L_35 = V_4;
		String_t* L_36 = ___customizationID1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_37 = String_Format_m2024975688(NULL /*static, unused*/, L_35, L_36, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RESTConnector_t3705102247_il2cpp_TypeInfo_var);
		RESTConnector_t3705102247 * L_38 = RESTConnector_GetConnector_m3262139854(NULL /*static, unused*/, _stringLiteral2566220529, L_37, (bool)1, /*hidden argument*/NULL);
		V_5 = L_38;
		RESTConnector_t3705102247 * L_39 = V_5;
		if (L_39)
		{
			goto IL_00f6;
		}
	}
	{
		return (bool)0;
	}

IL_00f6:
	{
		RESTConnector_t3705102247 * L_40 = V_5;
		UpdateCustomizationRequest_t1669343124 * L_41 = V_3;
		NullCheck(L_40);
		bool L_42 = RESTConnector_Send_m2925366401(L_40, L_41, /*hidden argument*/NULL);
		return L_42;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech::OnUpdateCustomizationResp(IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request,IBM.Watson.DeveloperCloud.Connection.RESTConnector/Response)
extern "C"  void TextToSpeech_OnUpdateCustomizationResp_m1295820546 (TextToSpeech_t3349357562 * __this, Request_t466816980 * ___req0, Response_t429319368 * ___resp1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextToSpeech_OnUpdateCustomizationResp_m1295820546_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Request_t466816980 * L_0 = ___req0;
		NullCheck(((UpdateCustomizationRequest_t1669343124 *)CastclassClass((RuntimeObject*)L_0, UpdateCustomizationRequest_t1669343124_il2cpp_TypeInfo_var)));
		UpdateCustomizationCallback_t3026233620 * L_1 = UpdateCustomizationRequest_get_Callback_m1649430960(((UpdateCustomizationRequest_t1669343124 *)CastclassClass((RuntimeObject*)L_0, UpdateCustomizationRequest_t1669343124_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0031;
		}
	}
	{
		Request_t466816980 * L_2 = ___req0;
		NullCheck(((UpdateCustomizationRequest_t1669343124 *)CastclassClass((RuntimeObject*)L_2, UpdateCustomizationRequest_t1669343124_il2cpp_TypeInfo_var)));
		UpdateCustomizationCallback_t3026233620 * L_3 = UpdateCustomizationRequest_get_Callback_m1649430960(((UpdateCustomizationRequest_t1669343124 *)CastclassClass((RuntimeObject*)L_2, UpdateCustomizationRequest_t1669343124_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		Response_t429319368 * L_4 = ___resp1;
		NullCheck(L_4);
		bool L_5 = Response_get_Success_m2313267345(L_4, /*hidden argument*/NULL);
		Request_t466816980 * L_6 = ___req0;
		NullCheck(((UpdateCustomizationRequest_t1669343124 *)CastclassClass((RuntimeObject*)L_6, UpdateCustomizationRequest_t1669343124_il2cpp_TypeInfo_var)));
		String_t* L_7 = UpdateCustomizationRequest_get_Data_m3622793189(((UpdateCustomizationRequest_t1669343124 *)CastclassClass((RuntimeObject*)L_6, UpdateCustomizationRequest_t1669343124_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		NullCheck(L_3);
		UpdateCustomizationCallback_Invoke_m16749586(L_3, L_5, L_7, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Boolean IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech::GetCustomizationWords(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordsCallback,System.String,System.String)
extern "C"  bool TextToSpeech_GetCustomizationWords_m2592214181 (TextToSpeech_t3349357562 * __this, GetCustomizationWordsCallback_t2709615664 * ___callback0, String_t* ___customizationID1, String_t* ___customData2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextToSpeech_GetCustomizationWords_m2592214181_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GetCustomizationWordsRequest_t110549728 * V_0 = NULL;
	String_t* V_1 = NULL;
	RESTConnector_t3705102247 * V_2 = NULL;
	{
		GetCustomizationWordsCallback_t2709615664 * L_0 = ___callback0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, _stringLiteral1913944053, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		String_t* L_2 = ___customizationID1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0027;
		}
	}
	{
		ArgumentNullException_t628810857 * L_4 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_4, _stringLiteral1774655039, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0027:
	{
		GetCustomizationWordsRequest_t110549728 * L_5 = (GetCustomizationWordsRequest_t110549728 *)il2cpp_codegen_object_new(GetCustomizationWordsRequest_t110549728_il2cpp_TypeInfo_var);
		GetCustomizationWordsRequest__ctor_m1707632875(L_5, /*hidden argument*/NULL);
		V_0 = L_5;
		GetCustomizationWordsRequest_t110549728 * L_6 = V_0;
		GetCustomizationWordsCallback_t2709615664 * L_7 = ___callback0;
		NullCheck(L_6);
		GetCustomizationWordsRequest_set_Callback_m1563474383(L_6, L_7, /*hidden argument*/NULL);
		GetCustomizationWordsRequest_t110549728 * L_8 = V_0;
		String_t* L_9 = ___customizationID1;
		NullCheck(L_8);
		GetCustomizationWordsRequest_set_CustomizationID_m3489863342(L_8, L_9, /*hidden argument*/NULL);
		GetCustomizationWordsRequest_t110549728 * L_10 = V_0;
		String_t* L_11 = ___customData2;
		NullCheck(L_10);
		GetCustomizationWordsRequest_set_Data_m3341658500(L_10, L_11, /*hidden argument*/NULL);
		GetCustomizationWordsRequest_t110549728 * L_12 = V_0;
		IntPtr_t L_13;
		L_13.set_m_value_0((void*)(void*)TextToSpeech_OnGetCustomizationWordsResp_m3329896646_RuntimeMethod_var);
		ResponseEvent_t1616568356 * L_14 = (ResponseEvent_t1616568356 *)il2cpp_codegen_object_new(ResponseEvent_t1616568356_il2cpp_TypeInfo_var);
		ResponseEvent__ctor_m3385394859(L_14, __this, L_13, /*hidden argument*/NULL);
		NullCheck(L_12);
		Request_set_OnResponse_m2226001952(L_12, L_14, /*hidden argument*/NULL);
		V_1 = _stringLiteral3620071178;
		String_t* L_15 = V_1;
		String_t* L_16 = ___customizationID1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_17 = String_Format_m2024975688(NULL /*static, unused*/, L_15, L_16, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RESTConnector_t3705102247_il2cpp_TypeInfo_var);
		RESTConnector_t3705102247 * L_18 = RESTConnector_GetConnector_m3262139854(NULL /*static, unused*/, _stringLiteral2566220529, L_17, (bool)1, /*hidden argument*/NULL);
		V_2 = L_18;
		RESTConnector_t3705102247 * L_19 = V_2;
		if (L_19)
		{
			goto IL_0075;
		}
	}
	{
		return (bool)0;
	}

IL_0075:
	{
		RESTConnector_t3705102247 * L_20 = V_2;
		GetCustomizationWordsRequest_t110549728 * L_21 = V_0;
		NullCheck(L_20);
		bool L_22 = RESTConnector_Send_m2925366401(L_20, L_21, /*hidden argument*/NULL);
		return L_22;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech::OnGetCustomizationWordsResp(IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request,IBM.Watson.DeveloperCloud.Connection.RESTConnector/Response)
extern "C"  void TextToSpeech_OnGetCustomizationWordsResp_m3329896646 (TextToSpeech_t3349357562 * __this, Request_t466816980 * ___req0, Response_t429319368 * ___resp1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextToSpeech_OnGetCustomizationWordsResp_m3329896646_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Words_t2948214629 * V_0 = NULL;
	fsData_t2583805605 * V_1 = NULL;
	fsResult_t862419890  V_2;
	memset(&V_2, 0, sizeof(V_2));
	RuntimeObject * V_3 = NULL;
	Exception_t1927440687 * V_4 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	GetCustomizationWordsCallback_t2709615664 * G_B10_0 = NULL;
	GetCustomizationWordsCallback_t2709615664 * G_B9_0 = NULL;
	Words_t2948214629 * G_B11_0 = NULL;
	GetCustomizationWordsCallback_t2709615664 * G_B11_1 = NULL;
	{
		Words_t2948214629 * L_0 = (Words_t2948214629 *)il2cpp_codegen_object_new(Words_t2948214629_il2cpp_TypeInfo_var);
		Words__ctor_m314888114(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Response_t429319368 * L_1 = ___resp1;
		NullCheck(L_1);
		bool L_2 = Response_get_Success_m2313267345(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_00a5;
		}
	}

IL_0011:
	try
	{ // begin try (depth: 1)
		{
			V_1 = (fsData_t2583805605 *)NULL;
			IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
			Encoding_t663144255 * L_3 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
			Response_t429319368 * L_4 = ___resp1;
			NullCheck(L_4);
			ByteU5BU5D_t3397334013* L_5 = Response_get_Data_m3078004018(L_4, /*hidden argument*/NULL);
			NullCheck(L_3);
			String_t* L_6 = VirtFuncInvoker1< String_t*, ByteU5BU5D_t3397334013* >::Invoke(22 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_3, L_5);
			fsResult_t862419890  L_7 = fsJsonParser_Parse_m4200302395(NULL /*static, unused*/, L_6, (&V_1), /*hidden argument*/NULL);
			V_2 = L_7;
			bool L_8 = fsResult_get_Succeeded_m3237049006((&V_2), /*hidden argument*/NULL);
			if (L_8)
			{
				goto IL_0044;
			}
		}

IL_0037:
		{
			String_t* L_9 = fsResult_get_FormattedMessages_m3414202484((&V_2), /*hidden argument*/NULL);
			WatsonException_t1186470237 * L_10 = (WatsonException_t1186470237 *)il2cpp_codegen_object_new(WatsonException_t1186470237_il2cpp_TypeInfo_var);
			WatsonException__ctor_m3825115789(L_10, L_9, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_10);
		}

IL_0044:
		{
			Words_t2948214629 * L_11 = V_0;
			V_3 = L_11;
			IL2CPP_RUNTIME_CLASS_INIT(TextToSpeech_t3349357562_il2cpp_TypeInfo_var);
			fsSerializer_t4193731081 * L_12 = ((TextToSpeech_t3349357562_StaticFields*)il2cpp_codegen_static_fields_for(TextToSpeech_t3349357562_il2cpp_TypeInfo_var))->get_sm_Serializer_6();
			fsData_t2583805605 * L_13 = V_1;
			RuntimeObject * L_14 = V_3;
			NullCheck(L_14);
			Type_t * L_15 = Object_GetType_m191970594(L_14, /*hidden argument*/NULL);
			NullCheck(L_12);
			fsResult_t862419890  L_16 = fsSerializer_TryDeserialize_m4134098758(L_12, L_13, L_15, (&V_3), /*hidden argument*/NULL);
			V_2 = L_16;
			bool L_17 = fsResult_get_Succeeded_m3237049006((&V_2), /*hidden argument*/NULL);
			if (L_17)
			{
				goto IL_0073;
			}
		}

IL_0066:
		{
			String_t* L_18 = fsResult_get_FormattedMessages_m3414202484((&V_2), /*hidden argument*/NULL);
			WatsonException_t1186470237 * L_19 = (WatsonException_t1186470237 *)il2cpp_codegen_object_new(WatsonException_t1186470237_il2cpp_TypeInfo_var);
			WatsonException__ctor_m3825115789(L_19, L_18, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_19);
		}

IL_0073:
		{
			goto IL_00a5;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1927440687_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0078;
		throw e;
	}

CATCH_0078:
	{ // begin catch(System.Exception)
		V_4 = ((Exception_t1927440687 *)__exception_local);
		ObjectU5BU5D_t3614634134* L_20 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1));
		Exception_t1927440687 * L_21 = V_4;
		NullCheck(L_21);
		String_t* L_22 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_21);
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, L_22);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_22);
		Log_Error_m685270089(NULL /*static, unused*/, _stringLiteral2798243012, _stringLiteral3622233617, L_20, /*hidden argument*/NULL);
		Response_t429319368 * L_23 = ___resp1;
		NullCheck(L_23);
		Response_set_Success_m3746673474(L_23, (bool)0, /*hidden argument*/NULL);
		goto IL_00a5;
	} // end catch (depth: 1)

IL_00a5:
	{
		Request_t466816980 * L_24 = ___req0;
		NullCheck(((GetCustomizationWordsRequest_t110549728 *)CastclassClass((RuntimeObject*)L_24, GetCustomizationWordsRequest_t110549728_il2cpp_TypeInfo_var)));
		GetCustomizationWordsCallback_t2709615664 * L_25 = GetCustomizationWordsRequest_get_Callback_m1338404144(((GetCustomizationWordsRequest_t110549728 *)CastclassClass((RuntimeObject*)L_24, GetCustomizationWordsRequest_t110549728_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00e2;
		}
	}
	{
		Request_t466816980 * L_26 = ___req0;
		NullCheck(((GetCustomizationWordsRequest_t110549728 *)CastclassClass((RuntimeObject*)L_26, GetCustomizationWordsRequest_t110549728_il2cpp_TypeInfo_var)));
		GetCustomizationWordsCallback_t2709615664 * L_27 = GetCustomizationWordsRequest_get_Callback_m1338404144(((GetCustomizationWordsRequest_t110549728 *)CastclassClass((RuntimeObject*)L_26, GetCustomizationWordsRequest_t110549728_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		Response_t429319368 * L_28 = ___resp1;
		NullCheck(L_28);
		bool L_29 = Response_get_Success_m2313267345(L_28, /*hidden argument*/NULL);
		G_B9_0 = L_27;
		if (!L_29)
		{
			G_B10_0 = L_27;
			goto IL_00d1;
		}
	}
	{
		Words_t2948214629 * L_30 = V_0;
		G_B11_0 = L_30;
		G_B11_1 = G_B9_0;
		goto IL_00d2;
	}

IL_00d1:
	{
		G_B11_0 = ((Words_t2948214629 *)(NULL));
		G_B11_1 = G_B10_0;
	}

IL_00d2:
	{
		Request_t466816980 * L_31 = ___req0;
		NullCheck(((GetCustomizationWordsRequest_t110549728 *)CastclassClass((RuntimeObject*)L_31, GetCustomizationWordsRequest_t110549728_il2cpp_TypeInfo_var)));
		String_t* L_32 = GetCustomizationWordsRequest_get_Data_m3001775595(((GetCustomizationWordsRequest_t110549728 *)CastclassClass((RuntimeObject*)L_31, GetCustomizationWordsRequest_t110549728_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		NullCheck(G_B11_1);
		GetCustomizationWordsCallback_Invoke_m2042980066(G_B11_1, G_B11_0, L_32, /*hidden argument*/NULL);
	}

IL_00e2:
	{
		return;
	}
}
// System.Boolean IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech::AddCustomizationWords(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordsCallback,System.String,IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Words,System.String)
extern "C"  bool TextToSpeech_AddCustomizationWords_m1578949184 (TextToSpeech_t3349357562 * __this, AddCustomizationWordsCallback_t2578578585 * ___callback0, String_t* ___customizationID1, Words_t2948214629 * ___words2, String_t* ___customData3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextToSpeech_AddCustomizationWords_m1578949184_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	fsData_t2583805605 * V_0 = NULL;
	fsResult_t862419890  V_1;
	memset(&V_1, 0, sizeof(V_1));
	String_t* V_2 = NULL;
	AddCustomizationWordsRequest_t993580433 * V_3 = NULL;
	String_t* V_4 = NULL;
	RESTConnector_t3705102247 * V_5 = NULL;
	{
		AddCustomizationWordsCallback_t2578578585 * L_0 = ___callback0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, _stringLiteral1913944053, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		String_t* L_2 = ___customizationID1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0027;
		}
	}
	{
		ArgumentNullException_t628810857 * L_4 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_4, _stringLiteral1734094646, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0027:
	{
		Words_t2948214629 * L_5 = ___words2;
		NullCheck(L_5);
		bool L_6 = Words_HasData_m2703714480(L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_003d;
		}
	}
	{
		ArgumentNullException_t628810857 * L_7 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_7, _stringLiteral2371147763, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_003d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TextToSpeech_t3349357562_il2cpp_TypeInfo_var);
		fsSerializer_t4193731081 * L_8 = ((TextToSpeech_t3349357562_StaticFields*)il2cpp_codegen_static_fields_for(TextToSpeech_t3349357562_il2cpp_TypeInfo_var))->get_sm_Serializer_6();
		Words_t2948214629 * L_9 = ___words2;
		NullCheck(L_9);
		Type_t * L_10 = Object_GetType_m191970594(L_9, /*hidden argument*/NULL);
		Words_t2948214629 * L_11 = ___words2;
		NullCheck(L_8);
		fsResult_t862419890  L_12 = fsSerializer_TrySerialize_m867329083(L_8, L_10, L_11, (&V_0), /*hidden argument*/NULL);
		V_1 = L_12;
		fsResult_AssertSuccessWithoutWarnings_m1273657906((&V_1), /*hidden argument*/NULL);
		fsData_t2583805605 * L_13 = V_0;
		String_t* L_14 = fsJsonPrinter_CompressedJson_m2458408074(NULL /*static, unused*/, L_13, (bool)0, /*hidden argument*/NULL);
		V_2 = L_14;
		AddCustomizationWordsRequest_t993580433 * L_15 = (AddCustomizationWordsRequest_t993580433 *)il2cpp_codegen_object_new(AddCustomizationWordsRequest_t993580433_il2cpp_TypeInfo_var);
		AddCustomizationWordsRequest__ctor_m885260432(L_15, /*hidden argument*/NULL);
		V_3 = L_15;
		AddCustomizationWordsRequest_t993580433 * L_16 = V_3;
		AddCustomizationWordsCallback_t2578578585 * L_17 = ___callback0;
		NullCheck(L_16);
		AddCustomizationWordsRequest_set_Callback_m244551819(L_16, L_17, /*hidden argument*/NULL);
		AddCustomizationWordsRequest_t993580433 * L_18 = V_3;
		Words_t2948214629 * L_19 = ___words2;
		NullCheck(L_18);
		AddCustomizationWordsRequest_set_Words_m3960587283(L_18, L_19, /*hidden argument*/NULL);
		AddCustomizationWordsRequest_t993580433 * L_20 = V_3;
		String_t* L_21 = ___customizationID1;
		NullCheck(L_20);
		AddCustomizationWordsRequest_set_CustomizationID_m2223789655(L_20, L_21, /*hidden argument*/NULL);
		AddCustomizationWordsRequest_t993580433 * L_22 = V_3;
		String_t* L_23 = ___customData3;
		NullCheck(L_22);
		AddCustomizationWordsRequest_set_Data_m3233382931(L_22, L_23, /*hidden argument*/NULL);
		AddCustomizationWordsRequest_t993580433 * L_24 = V_3;
		NullCheck(L_24);
		Dictionary_2_t3943999495 * L_25 = Request_get_Headers_m340270624(L_24, /*hidden argument*/NULL);
		NullCheck(L_25);
		Dictionary_2_set_Item_m4244870320(L_25, _stringLiteral1048821954, _stringLiteral1391431453, /*hidden argument*/Dictionary_2_set_Item_m4244870320_RuntimeMethod_var);
		AddCustomizationWordsRequest_t993580433 * L_26 = V_3;
		NullCheck(L_26);
		Dictionary_2_t3943999495 * L_27 = Request_get_Headers_m340270624(L_26, /*hidden argument*/NULL);
		NullCheck(L_27);
		Dictionary_2_set_Item_m4244870320(L_27, _stringLiteral3819387978, _stringLiteral1391431453, /*hidden argument*/Dictionary_2_set_Item_m4244870320_RuntimeMethod_var);
		AddCustomizationWordsRequest_t993580433 * L_28 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
		Encoding_t663144255 * L_29 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_30 = V_2;
		NullCheck(L_29);
		ByteU5BU5D_t3397334013* L_31 = VirtFuncInvoker1< ByteU5BU5D_t3397334013*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_29, L_30);
		NullCheck(L_28);
		Request_set_Send_m296178747(L_28, L_31, /*hidden argument*/NULL);
		AddCustomizationWordsRequest_t993580433 * L_32 = V_3;
		IntPtr_t L_33;
		L_33.set_m_value_0((void*)(void*)TextToSpeech_OnAddCustomizationWordsResp_m1340193549_RuntimeMethod_var);
		ResponseEvent_t1616568356 * L_34 = (ResponseEvent_t1616568356 *)il2cpp_codegen_object_new(ResponseEvent_t1616568356_il2cpp_TypeInfo_var);
		ResponseEvent__ctor_m3385394859(L_34, __this, L_33, /*hidden argument*/NULL);
		NullCheck(L_32);
		Request_set_OnResponse_m2226001952(L_32, L_34, /*hidden argument*/NULL);
		V_4 = _stringLiteral3620071178;
		String_t* L_35 = V_4;
		String_t* L_36 = ___customizationID1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_37 = String_Format_m2024975688(NULL /*static, unused*/, L_35, L_36, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RESTConnector_t3705102247_il2cpp_TypeInfo_var);
		RESTConnector_t3705102247 * L_38 = RESTConnector_GetConnector_m3262139854(NULL /*static, unused*/, _stringLiteral2566220529, L_37, (bool)1, /*hidden argument*/NULL);
		V_5 = L_38;
		RESTConnector_t3705102247 * L_39 = V_5;
		if (L_39)
		{
			goto IL_00f6;
		}
	}
	{
		return (bool)0;
	}

IL_00f6:
	{
		RESTConnector_t3705102247 * L_40 = V_5;
		AddCustomizationWordsRequest_t993580433 * L_41 = V_3;
		NullCheck(L_40);
		bool L_42 = RESTConnector_Send_m2925366401(L_40, L_41, /*hidden argument*/NULL);
		return L_42;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech::OnAddCustomizationWordsResp(IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request,IBM.Watson.DeveloperCloud.Connection.RESTConnector/Response)
extern "C"  void TextToSpeech_OnAddCustomizationWordsResp_m1340193549 (TextToSpeech_t3349357562 * __this, Request_t466816980 * ___req0, Response_t429319368 * ___resp1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextToSpeech_OnAddCustomizationWordsResp_m1340193549_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Request_t466816980 * L_0 = ___req0;
		NullCheck(((AddCustomizationWordsRequest_t993580433 *)CastclassClass((RuntimeObject*)L_0, AddCustomizationWordsRequest_t993580433_il2cpp_TypeInfo_var)));
		AddCustomizationWordsCallback_t2578578585 * L_1 = AddCustomizationWordsRequest_get_Callback_m4169137936(((AddCustomizationWordsRequest_t993580433 *)CastclassClass((RuntimeObject*)L_0, AddCustomizationWordsRequest_t993580433_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0031;
		}
	}
	{
		Request_t466816980 * L_2 = ___req0;
		NullCheck(((AddCustomizationWordsRequest_t993580433 *)CastclassClass((RuntimeObject*)L_2, AddCustomizationWordsRequest_t993580433_il2cpp_TypeInfo_var)));
		AddCustomizationWordsCallback_t2578578585 * L_3 = AddCustomizationWordsRequest_get_Callback_m4169137936(((AddCustomizationWordsRequest_t993580433 *)CastclassClass((RuntimeObject*)L_2, AddCustomizationWordsRequest_t993580433_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		Response_t429319368 * L_4 = ___resp1;
		NullCheck(L_4);
		bool L_5 = Response_get_Success_m2313267345(L_4, /*hidden argument*/NULL);
		Request_t466816980 * L_6 = ___req0;
		NullCheck(((AddCustomizationWordsRequest_t993580433 *)CastclassClass((RuntimeObject*)L_6, AddCustomizationWordsRequest_t993580433_il2cpp_TypeInfo_var)));
		String_t* L_7 = AddCustomizationWordsRequest_get_Data_m2916608766(((AddCustomizationWordsRequest_t993580433 *)CastclassClass((RuntimeObject*)L_6, AddCustomizationWordsRequest_t993580433_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		NullCheck(L_3);
		AddCustomizationWordsCallback_Invoke_m187986097(L_3, L_5, L_7, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Boolean IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech::DeleteCustomizationWord(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/OnDeleteCustomizationWordCallback,System.String,System.String,System.String)
extern "C"  bool TextToSpeech_DeleteCustomizationWord_m309917500 (TextToSpeech_t3349357562 * __this, OnDeleteCustomizationWordCallback_t2544578463 * ___callback0, String_t* ___customizationID1, String_t* ___word2, String_t* ___customData3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextToSpeech_DeleteCustomizationWord_m309917500_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DeleteCustomizationWordRequest_t3182066146 * V_0 = NULL;
	String_t* V_1 = NULL;
	RESTConnector_t3705102247 * V_2 = NULL;
	{
		OnDeleteCustomizationWordCallback_t2544578463 * L_0 = ___callback0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, _stringLiteral1913944053, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		String_t* L_2 = ___customizationID1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0027;
		}
	}
	{
		ArgumentNullException_t628810857 * L_4 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_4, _stringLiteral1088384551, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0027:
	{
		String_t* L_5 = ___word2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_6 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_003d;
		}
	}
	{
		ArgumentNullException_t628810857 * L_7 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_7, _stringLiteral2593542011, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_003d:
	{
		DeleteCustomizationWordRequest_t3182066146 * L_8 = (DeleteCustomizationWordRequest_t3182066146 *)il2cpp_codegen_object_new(DeleteCustomizationWordRequest_t3182066146_il2cpp_TypeInfo_var);
		DeleteCustomizationWordRequest__ctor_m2141073395(L_8, /*hidden argument*/NULL);
		V_0 = L_8;
		DeleteCustomizationWordRequest_t3182066146 * L_9 = V_0;
		OnDeleteCustomizationWordCallback_t2544578463 * L_10 = ___callback0;
		NullCheck(L_9);
		DeleteCustomizationWordRequest_set_Callback_m4219025788(L_9, L_10, /*hidden argument*/NULL);
		DeleteCustomizationWordRequest_t3182066146 * L_11 = V_0;
		String_t* L_12 = ___customizationID1;
		NullCheck(L_11);
		DeleteCustomizationWordRequest_set_CustomizationID_m2192251384(L_11, L_12, /*hidden argument*/NULL);
		DeleteCustomizationWordRequest_t3182066146 * L_13 = V_0;
		String_t* L_14 = ___word2;
		NullCheck(L_13);
		DeleteCustomizationWordRequest_set_Word_m2052926514(L_13, L_14, /*hidden argument*/NULL);
		DeleteCustomizationWordRequest_t3182066146 * L_15 = V_0;
		String_t* L_16 = ___customData3;
		NullCheck(L_15);
		DeleteCustomizationWordRequest_set_Data_m4265011282(L_15, L_16, /*hidden argument*/NULL);
		DeleteCustomizationWordRequest_t3182066146 * L_17 = V_0;
		NullCheck(L_17);
		Request_set_Timeout_m3686318020(L_17, (600.0f), /*hidden argument*/NULL);
		DeleteCustomizationWordRequest_t3182066146 * L_18 = V_0;
		NullCheck(L_18);
		Request_set_Delete_m357548768(L_18, (bool)1, /*hidden argument*/NULL);
		DeleteCustomizationWordRequest_t3182066146 * L_19 = V_0;
		IntPtr_t L_20;
		L_20.set_m_value_0((void*)(void*)TextToSpeech_OnDeleteCustomizationWordResp_m2067181128_RuntimeMethod_var);
		ResponseEvent_t1616568356 * L_21 = (ResponseEvent_t1616568356 *)il2cpp_codegen_object_new(ResponseEvent_t1616568356_il2cpp_TypeInfo_var);
		ResponseEvent__ctor_m3385394859(L_21, __this, L_20, /*hidden argument*/NULL);
		NullCheck(L_19);
		Request_set_OnResponse_m2226001952(L_19, L_21, /*hidden argument*/NULL);
		V_1 = _stringLiteral3235994642;
		String_t* L_22 = V_1;
		String_t* L_23 = ___customizationID1;
		String_t* L_24 = ___word2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_25 = String_Format_m1811873526(NULL /*static, unused*/, L_22, L_23, L_24, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RESTConnector_t3705102247_il2cpp_TypeInfo_var);
		RESTConnector_t3705102247 * L_26 = RESTConnector_GetConnector_m3262139854(NULL /*static, unused*/, _stringLiteral2566220529, L_25, (bool)1, /*hidden argument*/NULL);
		V_2 = L_26;
		RESTConnector_t3705102247 * L_27 = V_2;
		if (L_27)
		{
			goto IL_00a6;
		}
	}
	{
		return (bool)0;
	}

IL_00a6:
	{
		RESTConnector_t3705102247 * L_28 = V_2;
		DeleteCustomizationWordRequest_t3182066146 * L_29 = V_0;
		NullCheck(L_28);
		bool L_30 = RESTConnector_Send_m2925366401(L_28, L_29, /*hidden argument*/NULL);
		return L_30;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech::OnDeleteCustomizationWordResp(IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request,IBM.Watson.DeveloperCloud.Connection.RESTConnector/Response)
extern "C"  void TextToSpeech_OnDeleteCustomizationWordResp_m2067181128 (TextToSpeech_t3349357562 * __this, Request_t466816980 * ___req0, Response_t429319368 * ___resp1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextToSpeech_OnDeleteCustomizationWordResp_m2067181128_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Request_t466816980 * L_0 = ___req0;
		NullCheck(((DeleteCustomizationWordRequest_t3182066146 *)CastclassClass((RuntimeObject*)L_0, DeleteCustomizationWordRequest_t3182066146_il2cpp_TypeInfo_var)));
		OnDeleteCustomizationWordCallback_t2544578463 * L_1 = DeleteCustomizationWordRequest_get_Callback_m18553317(((DeleteCustomizationWordRequest_t3182066146 *)CastclassClass((RuntimeObject*)L_0, DeleteCustomizationWordRequest_t3182066146_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0031;
		}
	}
	{
		Request_t466816980 * L_2 = ___req0;
		NullCheck(((DeleteCustomizationWordRequest_t3182066146 *)CastclassClass((RuntimeObject*)L_2, DeleteCustomizationWordRequest_t3182066146_il2cpp_TypeInfo_var)));
		OnDeleteCustomizationWordCallback_t2544578463 * L_3 = DeleteCustomizationWordRequest_get_Callback_m18553317(((DeleteCustomizationWordRequest_t3182066146 *)CastclassClass((RuntimeObject*)L_2, DeleteCustomizationWordRequest_t3182066146_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		Response_t429319368 * L_4 = ___resp1;
		NullCheck(L_4);
		bool L_5 = Response_get_Success_m2313267345(L_4, /*hidden argument*/NULL);
		Request_t466816980 * L_6 = ___req0;
		NullCheck(((DeleteCustomizationWordRequest_t3182066146 *)CastclassClass((RuntimeObject*)L_6, DeleteCustomizationWordRequest_t3182066146_il2cpp_TypeInfo_var)));
		String_t* L_7 = DeleteCustomizationWordRequest_get_Data_m3747700475(((DeleteCustomizationWordRequest_t3182066146 *)CastclassClass((RuntimeObject*)L_6, DeleteCustomizationWordRequest_t3182066146_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		NullCheck(L_3);
		OnDeleteCustomizationWordCallback_Invoke_m226387231(L_3, L_5, L_7, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Boolean IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech::GetCustomizationWord(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordCallback,System.String,System.String,System.String)
extern "C"  bool TextToSpeech_GetCustomizationWord_m1273455755 (TextToSpeech_t3349357562 * __this, GetCustomizationWordCallback_t2485962781 * ___callback0, String_t* ___customizationID1, String_t* ___word2, String_t* ___customData3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextToSpeech_GetCustomizationWord_m1273455755_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GetCustomizationWordRequest_t1280810157 * V_0 = NULL;
	String_t* V_1 = NULL;
	RESTConnector_t3705102247 * V_2 = NULL;
	{
		GetCustomizationWordCallback_t2485962781 * L_0 = ___callback0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, _stringLiteral1913944053, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		String_t* L_2 = ___customizationID1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0027;
		}
	}
	{
		ArgumentNullException_t628810857 * L_4 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_4, _stringLiteral1774655039, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0027:
	{
		String_t* L_5 = ___word2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_6 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_003d;
		}
	}
	{
		ArgumentNullException_t628810857 * L_7 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_7, _stringLiteral898991076, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_003d:
	{
		GetCustomizationWordRequest_t1280810157 * L_8 = (GetCustomizationWordRequest_t1280810157 *)il2cpp_codegen_object_new(GetCustomizationWordRequest_t1280810157_il2cpp_TypeInfo_var);
		GetCustomizationWordRequest__ctor_m3012566346(L_8, /*hidden argument*/NULL);
		V_0 = L_8;
		GetCustomizationWordRequest_t1280810157 * L_9 = V_0;
		GetCustomizationWordCallback_t2485962781 * L_10 = ___callback0;
		NullCheck(L_9);
		GetCustomizationWordRequest_set_Callback_m1678004927(L_9, L_10, /*hidden argument*/NULL);
		GetCustomizationWordRequest_t1280810157 * L_11 = V_0;
		String_t* L_12 = ___customizationID1;
		NullCheck(L_11);
		GetCustomizationWordRequest_set_CustomizationID_m3921965539(L_11, L_12, /*hidden argument*/NULL);
		GetCustomizationWordRequest_t1280810157 * L_13 = V_0;
		String_t* L_14 = ___word2;
		NullCheck(L_13);
		GetCustomizationWordRequest_set_Word_m3821257485(L_13, L_14, /*hidden argument*/NULL);
		GetCustomizationWordRequest_t1280810157 * L_15 = V_0;
		String_t* L_16 = ___customData3;
		NullCheck(L_15);
		GetCustomizationWordRequest_set_Data_m1038605743(L_15, L_16, /*hidden argument*/NULL);
		GetCustomizationWordRequest_t1280810157 * L_17 = V_0;
		IntPtr_t L_18;
		L_18.set_m_value_0((void*)(void*)TextToSpeech_OnGetCustomizationWordResp_m802699301_RuntimeMethod_var);
		ResponseEvent_t1616568356 * L_19 = (ResponseEvent_t1616568356 *)il2cpp_codegen_object_new(ResponseEvent_t1616568356_il2cpp_TypeInfo_var);
		ResponseEvent__ctor_m3385394859(L_19, __this, L_18, /*hidden argument*/NULL);
		NullCheck(L_17);
		Request_set_OnResponse_m2226001952(L_17, L_19, /*hidden argument*/NULL);
		V_1 = _stringLiteral3235994642;
		String_t* L_20 = V_1;
		String_t* L_21 = ___customizationID1;
		String_t* L_22 = ___word2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_23 = String_Format_m1811873526(NULL /*static, unused*/, L_20, L_21, L_22, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RESTConnector_t3705102247_il2cpp_TypeInfo_var);
		RESTConnector_t3705102247 * L_24 = RESTConnector_GetConnector_m3262139854(NULL /*static, unused*/, _stringLiteral2566220529, L_23, (bool)1, /*hidden argument*/NULL);
		V_2 = L_24;
		RESTConnector_t3705102247 * L_25 = V_2;
		if (L_25)
		{
			goto IL_0094;
		}
	}
	{
		return (bool)0;
	}

IL_0094:
	{
		RESTConnector_t3705102247 * L_26 = V_2;
		GetCustomizationWordRequest_t1280810157 * L_27 = V_0;
		NullCheck(L_26);
		bool L_28 = RESTConnector_Send_m2925366401(L_26, L_27, /*hidden argument*/NULL);
		return L_28;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech::OnGetCustomizationWordResp(IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request,IBM.Watson.DeveloperCloud.Connection.RESTConnector/Response)
extern "C"  void TextToSpeech_OnGetCustomizationWordResp_m802699301 (TextToSpeech_t3349357562 * __this, Request_t466816980 * ___req0, Response_t429319368 * ___resp1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextToSpeech_OnGetCustomizationWordResp_m802699301_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Translation_t2174867539 * V_0 = NULL;
	fsData_t2583805605 * V_1 = NULL;
	fsResult_t862419890  V_2;
	memset(&V_2, 0, sizeof(V_2));
	RuntimeObject * V_3 = NULL;
	Exception_t1927440687 * V_4 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	GetCustomizationWordCallback_t2485962781 * G_B10_0 = NULL;
	GetCustomizationWordCallback_t2485962781 * G_B9_0 = NULL;
	Translation_t2174867539 * G_B11_0 = NULL;
	GetCustomizationWordCallback_t2485962781 * G_B11_1 = NULL;
	{
		Translation_t2174867539 * L_0 = (Translation_t2174867539 *)il2cpp_codegen_object_new(Translation_t2174867539_il2cpp_TypeInfo_var);
		Translation__ctor_m1959656352(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Response_t429319368 * L_1 = ___resp1;
		NullCheck(L_1);
		bool L_2 = Response_get_Success_m2313267345(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_00a5;
		}
	}

IL_0011:
	try
	{ // begin try (depth: 1)
		{
			V_1 = (fsData_t2583805605 *)NULL;
			IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
			Encoding_t663144255 * L_3 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
			Response_t429319368 * L_4 = ___resp1;
			NullCheck(L_4);
			ByteU5BU5D_t3397334013* L_5 = Response_get_Data_m3078004018(L_4, /*hidden argument*/NULL);
			NullCheck(L_3);
			String_t* L_6 = VirtFuncInvoker1< String_t*, ByteU5BU5D_t3397334013* >::Invoke(22 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_3, L_5);
			fsResult_t862419890  L_7 = fsJsonParser_Parse_m4200302395(NULL /*static, unused*/, L_6, (&V_1), /*hidden argument*/NULL);
			V_2 = L_7;
			bool L_8 = fsResult_get_Succeeded_m3237049006((&V_2), /*hidden argument*/NULL);
			if (L_8)
			{
				goto IL_0044;
			}
		}

IL_0037:
		{
			String_t* L_9 = fsResult_get_FormattedMessages_m3414202484((&V_2), /*hidden argument*/NULL);
			WatsonException_t1186470237 * L_10 = (WatsonException_t1186470237 *)il2cpp_codegen_object_new(WatsonException_t1186470237_il2cpp_TypeInfo_var);
			WatsonException__ctor_m3825115789(L_10, L_9, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_10);
		}

IL_0044:
		{
			Translation_t2174867539 * L_11 = V_0;
			V_3 = L_11;
			IL2CPP_RUNTIME_CLASS_INIT(TextToSpeech_t3349357562_il2cpp_TypeInfo_var);
			fsSerializer_t4193731081 * L_12 = ((TextToSpeech_t3349357562_StaticFields*)il2cpp_codegen_static_fields_for(TextToSpeech_t3349357562_il2cpp_TypeInfo_var))->get_sm_Serializer_6();
			fsData_t2583805605 * L_13 = V_1;
			RuntimeObject * L_14 = V_3;
			NullCheck(L_14);
			Type_t * L_15 = Object_GetType_m191970594(L_14, /*hidden argument*/NULL);
			NullCheck(L_12);
			fsResult_t862419890  L_16 = fsSerializer_TryDeserialize_m4134098758(L_12, L_13, L_15, (&V_3), /*hidden argument*/NULL);
			V_2 = L_16;
			bool L_17 = fsResult_get_Succeeded_m3237049006((&V_2), /*hidden argument*/NULL);
			if (L_17)
			{
				goto IL_0073;
			}
		}

IL_0066:
		{
			String_t* L_18 = fsResult_get_FormattedMessages_m3414202484((&V_2), /*hidden argument*/NULL);
			WatsonException_t1186470237 * L_19 = (WatsonException_t1186470237 *)il2cpp_codegen_object_new(WatsonException_t1186470237_il2cpp_TypeInfo_var);
			WatsonException__ctor_m3825115789(L_19, L_18, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_19);
		}

IL_0073:
		{
			goto IL_00a5;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1927440687_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0078;
		throw e;
	}

CATCH_0078:
	{ // begin catch(System.Exception)
		V_4 = ((Exception_t1927440687 *)__exception_local);
		ObjectU5BU5D_t3614634134* L_20 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1));
		Exception_t1927440687 * L_21 = V_4;
		NullCheck(L_21);
		String_t* L_22 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_21);
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, L_22);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_22);
		Log_Error_m685270089(NULL /*static, unused*/, _stringLiteral2798243012, _stringLiteral82143046, L_20, /*hidden argument*/NULL);
		Response_t429319368 * L_23 = ___resp1;
		NullCheck(L_23);
		Response_set_Success_m3746673474(L_23, (bool)0, /*hidden argument*/NULL);
		goto IL_00a5;
	} // end catch (depth: 1)

IL_00a5:
	{
		Request_t466816980 * L_24 = ___req0;
		NullCheck(((GetCustomizationWordRequest_t1280810157 *)CastclassClass((RuntimeObject*)L_24, GetCustomizationWordRequest_t1280810157_il2cpp_TypeInfo_var)));
		GetCustomizationWordCallback_t2485962781 * L_25 = GetCustomizationWordRequest_get_Callback_m3381062138(((GetCustomizationWordRequest_t1280810157 *)CastclassClass((RuntimeObject*)L_24, GetCustomizationWordRequest_t1280810157_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00e2;
		}
	}
	{
		Request_t466816980 * L_26 = ___req0;
		NullCheck(((GetCustomizationWordRequest_t1280810157 *)CastclassClass((RuntimeObject*)L_26, GetCustomizationWordRequest_t1280810157_il2cpp_TypeInfo_var)));
		GetCustomizationWordCallback_t2485962781 * L_27 = GetCustomizationWordRequest_get_Callback_m3381062138(((GetCustomizationWordRequest_t1280810157 *)CastclassClass((RuntimeObject*)L_26, GetCustomizationWordRequest_t1280810157_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		Response_t429319368 * L_28 = ___resp1;
		NullCheck(L_28);
		bool L_29 = Response_get_Success_m2313267345(L_28, /*hidden argument*/NULL);
		G_B9_0 = L_27;
		if (!L_29)
		{
			G_B10_0 = L_27;
			goto IL_00d1;
		}
	}
	{
		Translation_t2174867539 * L_30 = V_0;
		G_B11_0 = L_30;
		G_B11_1 = G_B9_0;
		goto IL_00d2;
	}

IL_00d1:
	{
		G_B11_0 = ((Translation_t2174867539 *)(NULL));
		G_B11_1 = G_B10_0;
	}

IL_00d2:
	{
		Request_t466816980 * L_31 = ___req0;
		NullCheck(((GetCustomizationWordRequest_t1280810157 *)CastclassClass((RuntimeObject*)L_31, GetCustomizationWordRequest_t1280810157_il2cpp_TypeInfo_var)));
		String_t* L_32 = GetCustomizationWordRequest_get_Data_m1350863650(((GetCustomizationWordRequest_t1280810157 *)CastclassClass((RuntimeObject*)L_31, GetCustomizationWordRequest_t1280810157_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		NullCheck(G_B11_1);
		GetCustomizationWordCallback_Invoke_m3773263563(G_B11_1, G_B11_0, L_32, /*hidden argument*/NULL);
	}

IL_00e2:
	{
		return;
	}
}
// System.Boolean IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech::AddCustomizationWord(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordCallback,System.String,System.String,System.String,System.String)
extern "C"  bool TextToSpeech_AddCustomizationWord_m352902665 (TextToSpeech_t3349357562 * __this, AddCustomizationWordCallback_t3775539180 * ___callback0, String_t* ___customizationID1, String_t* ___word2, String_t* ___translation3, String_t* ___customData4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextToSpeech_AddCustomizationWord_m352902665_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	AddCustomizationWordRequest_t864480132 * V_1 = NULL;
	String_t* V_2 = NULL;
	RESTConnector_t3705102247 * V_3 = NULL;
	{
		Log_Error_m685270089(NULL /*static, unused*/, _stringLiteral2221276358, _stringLiteral1178600151, ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		AddCustomizationWordCallback_t3775539180 * L_0 = ___callback0;
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, _stringLiteral1913944053, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0026:
	{
		String_t* L_2 = ___customizationID1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_003c;
		}
	}
	{
		ArgumentNullException_t628810857 * L_4 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_4, _stringLiteral1734094646, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_003c:
	{
		String_t* L_5 = ___word2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_6 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0052;
		}
	}
	{
		ArgumentNullException_t628810857 * L_7 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_7, _stringLiteral4143128774, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_0052:
	{
		String_t* L_8 = ___translation3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_9 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0069;
		}
	}
	{
		ArgumentNullException_t628810857 * L_10 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_10, _stringLiteral426991755, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_10);
	}

IL_0069:
	{
		String_t* L_11 = ___translation3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral542026571, L_11, _stringLiteral2168002853, /*hidden argument*/NULL);
		V_0 = L_12;
		AddCustomizationWordRequest_t864480132 * L_13 = (AddCustomizationWordRequest_t864480132 *)il2cpp_codegen_object_new(AddCustomizationWordRequest_t864480132_il2cpp_TypeInfo_var);
		AddCustomizationWordRequest__ctor_m2967135291(L_13, /*hidden argument*/NULL);
		V_1 = L_13;
		AddCustomizationWordRequest_t864480132 * L_14 = V_1;
		AddCustomizationWordCallback_t3775539180 * L_15 = ___callback0;
		NullCheck(L_14);
		AddCustomizationWordRequest_set_Callback_m3706326815(L_14, L_15, /*hidden argument*/NULL);
		AddCustomizationWordRequest_t864480132 * L_16 = V_1;
		String_t* L_17 = ___customizationID1;
		NullCheck(L_16);
		AddCustomizationWordRequest_set_CustomizationID_m904633046(L_16, L_17, /*hidden argument*/NULL);
		AddCustomizationWordRequest_t864480132 * L_18 = V_1;
		String_t* L_19 = ___word2;
		NullCheck(L_18);
		AddCustomizationWordRequest_set_Word_m1970087552(L_18, L_19, /*hidden argument*/NULL);
		AddCustomizationWordRequest_t864480132 * L_20 = V_1;
		String_t* L_21 = ___translation3;
		NullCheck(L_20);
		AddCustomizationWordRequest_set_Translation_m4095350635(L_20, L_21, /*hidden argument*/NULL);
		AddCustomizationWordRequest_t864480132 * L_22 = V_1;
		String_t* L_23 = ___customData4;
		NullCheck(L_22);
		AddCustomizationWordRequest_set_Data_m1163374356(L_22, L_23, /*hidden argument*/NULL);
		AddCustomizationWordRequest_t864480132 * L_24 = V_1;
		NullCheck(L_24);
		Dictionary_2_t3943999495 * L_25 = Request_get_Headers_m340270624(L_24, /*hidden argument*/NULL);
		NullCheck(L_25);
		Dictionary_2_set_Item_m4244870320(L_25, _stringLiteral1048821954, _stringLiteral1391431453, /*hidden argument*/Dictionary_2_set_Item_m4244870320_RuntimeMethod_var);
		AddCustomizationWordRequest_t864480132 * L_26 = V_1;
		NullCheck(L_26);
		Dictionary_2_t3943999495 * L_27 = Request_get_Headers_m340270624(L_26, /*hidden argument*/NULL);
		NullCheck(L_27);
		Dictionary_2_set_Item_m4244870320(L_27, _stringLiteral3819387978, _stringLiteral1391431453, /*hidden argument*/Dictionary_2_set_Item_m4244870320_RuntimeMethod_var);
		AddCustomizationWordRequest_t864480132 * L_28 = V_1;
		NullCheck(L_28);
		Dictionary_2_t3943999495 * L_29 = Request_get_Headers_m340270624(L_28, /*hidden argument*/NULL);
		NullCheck(L_29);
		Dictionary_2_set_Item_m4244870320(L_29, _stringLiteral1226460116, _stringLiteral884247665, /*hidden argument*/Dictionary_2_set_Item_m4244870320_RuntimeMethod_var);
		AddCustomizationWordRequest_t864480132 * L_30 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
		Encoding_t663144255 * L_31 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_32 = V_0;
		NullCheck(L_31);
		ByteU5BU5D_t3397334013* L_33 = VirtFuncInvoker1< ByteU5BU5D_t3397334013*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_31, L_32);
		NullCheck(L_30);
		Request_set_Send_m296178747(L_30, L_33, /*hidden argument*/NULL);
		AddCustomizationWordRequest_t864480132 * L_34 = V_1;
		IntPtr_t L_35;
		L_35.set_m_value_0((void*)(void*)TextToSpeech_OnAddCustomizationWordResp_m4145766090_RuntimeMethod_var);
		ResponseEvent_t1616568356 * L_36 = (ResponseEvent_t1616568356 *)il2cpp_codegen_object_new(ResponseEvent_t1616568356_il2cpp_TypeInfo_var);
		ResponseEvent__ctor_m3385394859(L_36, __this, L_35, /*hidden argument*/NULL);
		NullCheck(L_34);
		Request_set_OnResponse_m2226001952(L_34, L_36, /*hidden argument*/NULL);
		V_2 = _stringLiteral3235994642;
		String_t* L_37 = V_2;
		String_t* L_38 = ___customizationID1;
		String_t* L_39 = ___word2;
		String_t* L_40 = String_Format_m1811873526(NULL /*static, unused*/, L_37, L_38, L_39, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RESTConnector_t3705102247_il2cpp_TypeInfo_var);
		RESTConnector_t3705102247 * L_41 = RESTConnector_GetConnector_m3262139854(NULL /*static, unused*/, _stringLiteral2566220529, L_40, (bool)1, /*hidden argument*/NULL);
		V_3 = L_41;
		RESTConnector_t3705102247 * L_42 = V_3;
		if (L_42)
		{
			goto IL_012a;
		}
	}
	{
		return (bool)0;
	}

IL_012a:
	{
		RESTConnector_t3705102247 * L_43 = V_3;
		AddCustomizationWordRequest_t864480132 * L_44 = V_1;
		NullCheck(L_43);
		bool L_45 = RESTConnector_Send_m2925366401(L_43, L_44, /*hidden argument*/NULL);
		return L_45;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech::OnAddCustomizationWordResp(IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request,IBM.Watson.DeveloperCloud.Connection.RESTConnector/Response)
extern "C"  void TextToSpeech_OnAddCustomizationWordResp_m4145766090 (TextToSpeech_t3349357562 * __this, Request_t466816980 * ___req0, Response_t429319368 * ___resp1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextToSpeech_OnAddCustomizationWordResp_m4145766090_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Request_t466816980 * L_0 = ___req0;
		NullCheck(((AddCustomizationWordRequest_t864480132 *)CastclassClass((RuntimeObject*)L_0, AddCustomizationWordRequest_t864480132_il2cpp_TypeInfo_var)));
		AddCustomizationWordCallback_t3775539180 * L_1 = AddCustomizationWordRequest_get_Callback_m793839120(((AddCustomizationWordRequest_t864480132 *)CastclassClass((RuntimeObject*)L_0, AddCustomizationWordRequest_t864480132_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0031;
		}
	}
	{
		Request_t466816980 * L_2 = ___req0;
		NullCheck(((AddCustomizationWordRequest_t864480132 *)CastclassClass((RuntimeObject*)L_2, AddCustomizationWordRequest_t864480132_il2cpp_TypeInfo_var)));
		AddCustomizationWordCallback_t3775539180 * L_3 = AddCustomizationWordRequest_get_Callback_m793839120(((AddCustomizationWordRequest_t864480132 *)CastclassClass((RuntimeObject*)L_2, AddCustomizationWordRequest_t864480132_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		Response_t429319368 * L_4 = ___resp1;
		NullCheck(L_4);
		bool L_5 = Response_get_Success_m2313267345(L_4, /*hidden argument*/NULL);
		Request_t466816980 * L_6 = ___req0;
		NullCheck(((AddCustomizationWordRequest_t864480132 *)CastclassClass((RuntimeObject*)L_6, AddCustomizationWordRequest_t864480132_il2cpp_TypeInfo_var)));
		String_t* L_7 = AddCustomizationWordRequest_get_Data_m2144573351(((AddCustomizationWordRequest_t864480132 *)CastclassClass((RuntimeObject*)L_6, AddCustomizationWordRequest_t864480132_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		NullCheck(L_3);
		AddCustomizationWordCallback_Invoke_m2326839634(L_3, L_5, L_7, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech::GetServiceID()
extern "C"  String_t* TextToSpeech_GetServiceID_m1942818028 (TextToSpeech_t3349357562 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextToSpeech_GetServiceID_m1942818028_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		return _stringLiteral2566220529;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech::GetServiceStatus(IBM.Watson.DeveloperCloud.Services.ServiceStatus)
extern "C"  void TextToSpeech_GetServiceStatus_m4146958314 (TextToSpeech_t3349357562 * __this, ServiceStatus_t1443707987 * ___callback0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextToSpeech_GetServiceStatus_m4146958314_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Config_t3637807320_il2cpp_TypeInfo_var);
		Config_t3637807320 * L_0 = Config_get_Instance_m430784956(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		CredentialInfo_t34154441 * L_1 = Config_FindCredentials_m3250710905(L_0, _stringLiteral2566220529, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		ServiceStatus_t1443707987 * L_2 = ___callback0;
		CheckServiceStatus_t3651453794 * L_3 = (CheckServiceStatus_t3651453794 *)il2cpp_codegen_object_new(CheckServiceStatus_t3651453794_il2cpp_TypeInfo_var);
		CheckServiceStatus__ctor_m3941399121(L_3, __this, L_2, /*hidden argument*/NULL);
		goto IL_002d;
	}

IL_0021:
	{
		ServiceStatus_t1443707987 * L_4 = ___callback0;
		NullCheck(L_4);
		ServiceStatus_Invoke_m1869726628(L_4, _stringLiteral2566220529, (bool)0, /*hidden argument*/NULL);
	}

IL_002d:
	{
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech::.cctor()
extern "C"  void TextToSpeech__cctor_m3307453164 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextToSpeech__cctor_m3307453164_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		fsSerializer_t4193731081 * L_0 = (fsSerializer_t4193731081 *)il2cpp_codegen_object_new(fsSerializer_t4193731081_il2cpp_TypeInfo_var);
		fsSerializer__ctor_m3368490715(L_0, /*hidden argument*/NULL);
		((TextToSpeech_t3349357562_StaticFields*)il2cpp_codegen_static_fields_for(TextToSpeech_t3349357562_il2cpp_TypeInfo_var))->set_sm_Serializer_6(L_0);
		return;
	}
}
extern "C"  void DelegatePInvokeWrapper_AddCustomizationWordCallback_t3775539180 (AddCustomizationWordCallback_t3775539180 * __this, bool ___success0, String_t* ___data1, const RuntimeMethod* method)
{
	typedef void (STDCALL *PInvokeFunc)(int32_t, char*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((Il2CppDelegate*)__this)->method));

	// Marshaling of parameter '___data1' to native representation
	char* ____data1_marshaled = NULL;
	____data1_marshaled = il2cpp_codegen_marshal_string(___data1);

	// Native function invocation
	il2cppPInvokeFunc(static_cast<int32_t>(___success0), ____data1_marshaled);

	// Marshaling cleanup of parameter '___data1' native representation
	il2cpp_codegen_marshal_free(____data1_marshaled);
	____data1_marshaled = NULL;

}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void AddCustomizationWordCallback__ctor_m4173888147 (AddCustomizationWordCallback_t3775539180 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordCallback::Invoke(System.Boolean,System.String)
extern "C"  void AddCustomizationWordCallback_Invoke_m2326839634 (AddCustomizationWordCallback_t3775539180 * __this, bool ___success0, String_t* ___data1, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		AddCustomizationWordCallback_Invoke_m2326839634((AddCustomizationWordCallback_t3775539180 *)__this->get_prev_9(),___success0, ___data1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, bool ___success0, String_t* ___data1, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___success0, ___data1,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, bool ___success0, String_t* ___data1, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___success0, ___data1,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordCallback::BeginInvoke(System.Boolean,System.String,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* AddCustomizationWordCallback_BeginInvoke_m1747296861 (AddCustomizationWordCallback_t3775539180 * __this, bool ___success0, String_t* ___data1, AsyncCallback_t163412349 * ___callback2, RuntimeObject * ___object3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AddCustomizationWordCallback_BeginInvoke_m1747296861_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &___success0);
	__d_args[1] = ___data1;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (RuntimeObject*)___object3);
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordCallback::EndInvoke(System.IAsyncResult)
extern "C"  void AddCustomizationWordCallback_EndInvoke_m887376653 (AddCustomizationWordCallback_t3775539180 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordRequest::.ctor()
extern "C"  void AddCustomizationWordRequest__ctor_m2967135291 (AddCustomizationWordRequest_t864480132 * __this, const RuntimeMethod* method)
{
	{
		Request__ctor_m1928448681(__this, /*hidden argument*/NULL);
		return;
	}
}
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordCallback IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordRequest::get_Callback()
extern "C"  AddCustomizationWordCallback_t3775539180 * AddCustomizationWordRequest_get_Callback_m793839120 (AddCustomizationWordRequest_t864480132 * __this, const RuntimeMethod* method)
{
	{
		AddCustomizationWordCallback_t3775539180 * L_0 = __this->get_U3CCallbackU3Ek__BackingField_11();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordRequest::set_Callback(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordCallback)
extern "C"  void AddCustomizationWordRequest_set_Callback_m3706326815 (AddCustomizationWordRequest_t864480132 * __this, AddCustomizationWordCallback_t3775539180 * ___value0, const RuntimeMethod* method)
{
	{
		AddCustomizationWordCallback_t3775539180 * L_0 = ___value0;
		__this->set_U3CCallbackU3Ek__BackingField_11(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordRequest::get_CustomizationID()
extern "C"  String_t* AddCustomizationWordRequest_get_CustomizationID_m1794584443 (AddCustomizationWordRequest_t864480132 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CCustomizationIDU3Ek__BackingField_12();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordRequest::set_CustomizationID(System.String)
extern "C"  void AddCustomizationWordRequest_set_CustomizationID_m904633046 (AddCustomizationWordRequest_t864480132 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CCustomizationIDU3Ek__BackingField_12(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordRequest::get_Word()
extern "C"  String_t* AddCustomizationWordRequest_get_Word_m3163311201 (AddCustomizationWordRequest_t864480132 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CWordU3Ek__BackingField_13();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordRequest::set_Word(System.String)
extern "C"  void AddCustomizationWordRequest_set_Word_m1970087552 (AddCustomizationWordRequest_t864480132 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CWordU3Ek__BackingField_13(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordRequest::get_Translation()
extern "C"  String_t* AddCustomizationWordRequest_get_Translation_m2357208630 (AddCustomizationWordRequest_t864480132 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CTranslationU3Ek__BackingField_14();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordRequest::set_Translation(System.String)
extern "C"  void AddCustomizationWordRequest_set_Translation_m4095350635 (AddCustomizationWordRequest_t864480132 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CTranslationU3Ek__BackingField_14(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordRequest::get_Data()
extern "C"  String_t* AddCustomizationWordRequest_get_Data_m2144573351 (AddCustomizationWordRequest_t864480132 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CDataU3Ek__BackingField_15();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordRequest::set_Data(System.String)
extern "C"  void AddCustomizationWordRequest_set_Data_m1163374356 (AddCustomizationWordRequest_t864480132 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CDataU3Ek__BackingField_15(L_0);
		return;
	}
}
extern "C"  void DelegatePInvokeWrapper_AddCustomizationWordsCallback_t2578578585 (AddCustomizationWordsCallback_t2578578585 * __this, bool ___success0, String_t* ___data1, const RuntimeMethod* method)
{
	typedef void (STDCALL *PInvokeFunc)(int32_t, char*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((Il2CppDelegate*)__this)->method));

	// Marshaling of parameter '___data1' to native representation
	char* ____data1_marshaled = NULL;
	____data1_marshaled = il2cpp_codegen_marshal_string(___data1);

	// Native function invocation
	il2cppPInvokeFunc(static_cast<int32_t>(___success0), ____data1_marshaled);

	// Marshaling cleanup of parameter '___data1' native representation
	il2cpp_codegen_marshal_free(____data1_marshaled);
	____data1_marshaled = NULL;

}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordsCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void AddCustomizationWordsCallback__ctor_m2991679366 (AddCustomizationWordsCallback_t2578578585 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordsCallback::Invoke(System.Boolean,System.String)
extern "C"  void AddCustomizationWordsCallback_Invoke_m187986097 (AddCustomizationWordsCallback_t2578578585 * __this, bool ___success0, String_t* ___data1, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		AddCustomizationWordsCallback_Invoke_m187986097((AddCustomizationWordsCallback_t2578578585 *)__this->get_prev_9(),___success0, ___data1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, bool ___success0, String_t* ___data1, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___success0, ___data1,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, bool ___success0, String_t* ___data1, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___success0, ___data1,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordsCallback::BeginInvoke(System.Boolean,System.String,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* AddCustomizationWordsCallback_BeginInvoke_m2132419308 (AddCustomizationWordsCallback_t2578578585 * __this, bool ___success0, String_t* ___data1, AsyncCallback_t163412349 * ___callback2, RuntimeObject * ___object3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AddCustomizationWordsCallback_BeginInvoke_m2132419308_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &___success0);
	__d_args[1] = ___data1;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (RuntimeObject*)___object3);
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordsCallback::EndInvoke(System.IAsyncResult)
extern "C"  void AddCustomizationWordsCallback_EndInvoke_m1618971604 (AddCustomizationWordsCallback_t2578578585 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordsRequest::.ctor()
extern "C"  void AddCustomizationWordsRequest__ctor_m885260432 (AddCustomizationWordsRequest_t993580433 * __this, const RuntimeMethod* method)
{
	{
		Request__ctor_m1928448681(__this, /*hidden argument*/NULL);
		return;
	}
}
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordsCallback IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordsRequest::get_Callback()
extern "C"  AddCustomizationWordsCallback_t2578578585 * AddCustomizationWordsRequest_get_Callback_m4169137936 (AddCustomizationWordsRequest_t993580433 * __this, const RuntimeMethod* method)
{
	{
		AddCustomizationWordsCallback_t2578578585 * L_0 = __this->get_U3CCallbackU3Ek__BackingField_11();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordsRequest::set_Callback(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordsCallback)
extern "C"  void AddCustomizationWordsRequest_set_Callback_m244551819 (AddCustomizationWordsRequest_t993580433 * __this, AddCustomizationWordsCallback_t2578578585 * ___value0, const RuntimeMethod* method)
{
	{
		AddCustomizationWordsCallback_t2578578585 * L_0 = ___value0;
		__this->set_U3CCallbackU3Ek__BackingField_11(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordsRequest::get_CustomizationID()
extern "C"  String_t* AddCustomizationWordsRequest_get_CustomizationID_m97456200 (AddCustomizationWordsRequest_t993580433 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CCustomizationIDU3Ek__BackingField_12();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordsRequest::set_CustomizationID(System.String)
extern "C"  void AddCustomizationWordsRequest_set_CustomizationID_m2223789655 (AddCustomizationWordsRequest_t993580433 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CCustomizationIDU3Ek__BackingField_12(L_0);
		return;
	}
}
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Words IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordsRequest::get_Words()
extern "C"  Words_t2948214629 * AddCustomizationWordsRequest_get_Words_m975864500 (AddCustomizationWordsRequest_t993580433 * __this, const RuntimeMethod* method)
{
	{
		Words_t2948214629 * L_0 = __this->get_U3CWordsU3Ek__BackingField_13();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordsRequest::set_Words(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Words)
extern "C"  void AddCustomizationWordsRequest_set_Words_m3960587283 (AddCustomizationWordsRequest_t993580433 * __this, Words_t2948214629 * ___value0, const RuntimeMethod* method)
{
	{
		Words_t2948214629 * L_0 = ___value0;
		__this->set_U3CWordsU3Ek__BackingField_13(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordsRequest::get_Data()
extern "C"  String_t* AddCustomizationWordsRequest_get_Data_m2916608766 (AddCustomizationWordsRequest_t993580433 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CDataU3Ek__BackingField_14();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordsRequest::set_Data(System.String)
extern "C"  void AddCustomizationWordsRequest_set_Data_m3233382931 (AddCustomizationWordsRequest_t993580433 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CDataU3Ek__BackingField_14(L_0);
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/CheckServiceStatus::.ctor(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech,IBM.Watson.DeveloperCloud.Services.ServiceStatus)
extern "C"  void CheckServiceStatus__ctor_m3941399121 (CheckServiceStatus_t3651453794 * __this, TextToSpeech_t3349357562 * ___service0, ServiceStatus_t1443707987 * ___callback1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CheckServiceStatus__ctor_m3941399121_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		TextToSpeech_t3349357562 * L_0 = ___service0;
		__this->set_m_Service_0(L_0);
		ServiceStatus_t1443707987 * L_1 = ___callback1;
		__this->set_m_Callback_1(L_1);
		TextToSpeech_t3349357562 * L_2 = __this->get_m_Service_0();
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)CheckServiceStatus_OnCheckService_m1588091368_RuntimeMethod_var);
		GetVoicesCallback_t3012885511 * L_4 = (GetVoicesCallback_t3012885511 *)il2cpp_codegen_object_new(GetVoicesCallback_t3012885511_il2cpp_TypeInfo_var);
		GetVoicesCallback__ctor_m717172724(L_4, __this, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		bool L_5 = TextToSpeech_GetVoices_m2898245765(L_2, L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0041;
		}
	}
	{
		ServiceStatus_t1443707987 * L_6 = __this->get_m_Callback_1();
		NullCheck(L_6);
		ServiceStatus_Invoke_m1869726628(L_6, _stringLiteral2566220529, (bool)0, /*hidden argument*/NULL);
	}

IL_0041:
	{
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/CheckServiceStatus::OnCheckService(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Voices)
extern "C"  void CheckServiceStatus_OnCheckService_m1588091368 (CheckServiceStatus_t3651453794 * __this, Voices_t4221445733 * ___voices0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CheckServiceStatus_OnCheckService_m1588091368_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ServiceStatus_t1443707987 * L_0 = __this->get_m_Callback_1();
		if (!L_0)
		{
			goto IL_0032;
		}
	}
	{
		ServiceStatus_t1443707987 * L_1 = __this->get_m_Callback_1();
		NullCheck(L_1);
		RuntimeObject * L_2 = Delegate_get_Target_m896795953(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0032;
		}
	}
	{
		ServiceStatus_t1443707987 * L_3 = __this->get_m_Callback_1();
		Voices_t4221445733 * L_4 = ___voices0;
		NullCheck(L_3);
		ServiceStatus_Invoke_m1869726628(L_3, _stringLiteral2566220529, (bool)((((int32_t)((((RuntimeObject*)(Voices_t4221445733 *)L_4) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
	}

IL_0032:
	{
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/CreateCustomizationCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void CreateCustomizationCallback__ctor_m1620718734 (CreateCustomizationCallback_t3071584865 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/CreateCustomizationCallback::Invoke(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomizationID,System.String)
extern "C"  void CreateCustomizationCallback_Invoke_m3243793162 (CreateCustomizationCallback_t3071584865 * __this, CustomizationID_t344279642 * ___customizationID0, String_t* ___data1, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		CreateCustomizationCallback_Invoke_m3243793162((CreateCustomizationCallback_t3071584865 *)__this->get_prev_9(),___customizationID0, ___data1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, CustomizationID_t344279642 * ___customizationID0, String_t* ___data1, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___customizationID0, ___data1,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, CustomizationID_t344279642 * ___customizationID0, String_t* ___data1, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___customizationID0, ___data1,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___data1, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___customizationID0, ___data1,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/CreateCustomizationCallback::BeginInvoke(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomizationID,System.String,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* CreateCustomizationCallback_BeginInvoke_m662534503 (CreateCustomizationCallback_t3071584865 * __this, CustomizationID_t344279642 * ___customizationID0, String_t* ___data1, AsyncCallback_t163412349 * ___callback2, RuntimeObject * ___object3, const RuntimeMethod* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___customizationID0;
	__d_args[1] = ___data1;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (RuntimeObject*)___object3);
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/CreateCustomizationCallback::EndInvoke(System.IAsyncResult)
extern "C"  void CreateCustomizationCallback_EndInvoke_m1632452716 (CreateCustomizationCallback_t3071584865 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/CreateCustomizationRequest::.ctor()
extern "C"  void CreateCustomizationRequest__ctor_m3981671984 (CreateCustomizationRequest_t295745401 * __this, const RuntimeMethod* method)
{
	{
		Request__ctor_m1928448681(__this, /*hidden argument*/NULL);
		return;
	}
}
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/CreateCustomizationCallback IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/CreateCustomizationRequest::get_Callback()
extern "C"  CreateCustomizationCallback_t3071584865 * CreateCustomizationRequest_get_Callback_m3064008816 (CreateCustomizationRequest_t295745401 * __this, const RuntimeMethod* method)
{
	{
		CreateCustomizationCallback_t3071584865 * L_0 = __this->get_U3CCallbackU3Ek__BackingField_11();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/CreateCustomizationRequest::set_Callback(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/CreateCustomizationCallback)
extern "C"  void CreateCustomizationRequest_set_Callback_m709816875 (CreateCustomizationRequest_t295745401 * __this, CreateCustomizationCallback_t3071584865 * ___value0, const RuntimeMethod* method)
{
	{
		CreateCustomizationCallback_t3071584865 * L_0 = ___value0;
		__this->set_U3CCallbackU3Ek__BackingField_11(L_0);
		return;
	}
}
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomVoice IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/CreateCustomizationRequest::get_CustomVoice()
extern "C"  CustomVoice_t330695553 * CreateCustomizationRequest_get_CustomVoice_m3206963772 (CreateCustomizationRequest_t295745401 * __this, const RuntimeMethod* method)
{
	{
		CustomVoice_t330695553 * L_0 = __this->get_U3CCustomVoiceU3Ek__BackingField_12();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/CreateCustomizationRequest::set_CustomVoice(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomVoice)
extern "C"  void CreateCustomizationRequest_set_CustomVoice_m1331556427 (CreateCustomizationRequest_t295745401 * __this, CustomVoice_t330695553 * ___value0, const RuntimeMethod* method)
{
	{
		CustomVoice_t330695553 * L_0 = ___value0;
		__this->set_U3CCustomVoiceU3Ek__BackingField_12(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/CreateCustomizationRequest::get_Data()
extern "C"  String_t* CreateCustomizationRequest_get_Data_m2559910470 (CreateCustomizationRequest_t295745401 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CDataU3Ek__BackingField_13();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/CreateCustomizationRequest::set_Data(System.String)
extern "C"  void CreateCustomizationRequest_set_Data_m3310641787 (CreateCustomizationRequest_t295745401 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CDataU3Ek__BackingField_13(L_0);
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/DeleteCustomizationRequest::.ctor()
extern "C"  void DeleteCustomizationRequest__ctor_m2097311445 (DeleteCustomizationRequest_t4044506578 * __this, const RuntimeMethod* method)
{
	{
		Request__ctor_m1928448681(__this, /*hidden argument*/NULL);
		return;
	}
}
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/OnDeleteCustomizationCallback IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/DeleteCustomizationRequest::get_Callback()
extern "C"  OnDeleteCustomizationCallback_t2862971265 * DeleteCustomizationRequest_get_Callback_m3675390565 (DeleteCustomizationRequest_t4044506578 * __this, const RuntimeMethod* method)
{
	{
		OnDeleteCustomizationCallback_t2862971265 * L_0 = __this->get_U3CCallbackU3Ek__BackingField_11();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/DeleteCustomizationRequest::set_Callback(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/OnDeleteCustomizationCallback)
extern "C"  void DeleteCustomizationRequest_set_Callback_m1090382596 (DeleteCustomizationRequest_t4044506578 * __this, OnDeleteCustomizationCallback_t2862971265 * ___value0, const RuntimeMethod* method)
{
	{
		OnDeleteCustomizationCallback_t2862971265 * L_0 = ___value0;
		__this->set_U3CCallbackU3Ek__BackingField_11(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/DeleteCustomizationRequest::get_CustomizationID()
extern "C"  String_t* DeleteCustomizationRequest_get_CustomizationID_m69420757 (DeleteCustomizationRequest_t4044506578 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CCustomizationIDU3Ek__BackingField_12();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/DeleteCustomizationRequest::set_CustomizationID(System.String)
extern "C"  void DeleteCustomizationRequest_set_CustomizationID_m1101845264 (DeleteCustomizationRequest_t4044506578 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CCustomizationIDU3Ek__BackingField_12(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/DeleteCustomizationRequest::get_Data()
extern "C"  String_t* DeleteCustomizationRequest_get_Data_m669478649 (DeleteCustomizationRequest_t4044506578 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CDataU3Ek__BackingField_13();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/DeleteCustomizationRequest::set_Data(System.String)
extern "C"  void DeleteCustomizationRequest_set_Data_m3245021746 (DeleteCustomizationRequest_t4044506578 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CDataU3Ek__BackingField_13(L_0);
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/DeleteCustomizationWordRequest::.ctor()
extern "C"  void DeleteCustomizationWordRequest__ctor_m2141073395 (DeleteCustomizationWordRequest_t3182066146 * __this, const RuntimeMethod* method)
{
	{
		Request__ctor_m1928448681(__this, /*hidden argument*/NULL);
		return;
	}
}
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/OnDeleteCustomizationWordCallback IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/DeleteCustomizationWordRequest::get_Callback()
extern "C"  OnDeleteCustomizationWordCallback_t2544578463 * DeleteCustomizationWordRequest_get_Callback_m18553317 (DeleteCustomizationWordRequest_t3182066146 * __this, const RuntimeMethod* method)
{
	{
		OnDeleteCustomizationWordCallback_t2544578463 * L_0 = __this->get_U3CCallbackU3Ek__BackingField_11();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/DeleteCustomizationWordRequest::set_Callback(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/OnDeleteCustomizationWordCallback)
extern "C"  void DeleteCustomizationWordRequest_set_Callback_m4219025788 (DeleteCustomizationWordRequest_t3182066146 * __this, OnDeleteCustomizationWordCallback_t2544578463 * ___value0, const RuntimeMethod* method)
{
	{
		OnDeleteCustomizationWordCallback_t2544578463 * L_0 = ___value0;
		__this->set_U3CCallbackU3Ek__BackingField_11(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/DeleteCustomizationWordRequest::get_CustomizationID()
extern "C"  String_t* DeleteCustomizationWordRequest_get_CustomizationID_m4256140679 (DeleteCustomizationWordRequest_t3182066146 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CCustomizationIDU3Ek__BackingField_12();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/DeleteCustomizationWordRequest::set_CustomizationID(System.String)
extern "C"  void DeleteCustomizationWordRequest_set_CustomizationID_m2192251384 (DeleteCustomizationWordRequest_t3182066146 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CCustomizationIDU3Ek__BackingField_12(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/DeleteCustomizationWordRequest::get_Word()
extern "C"  String_t* DeleteCustomizationWordRequest_get_Word_m475388269 (DeleteCustomizationWordRequest_t3182066146 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CWordU3Ek__BackingField_13();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/DeleteCustomizationWordRequest::set_Word(System.String)
extern "C"  void DeleteCustomizationWordRequest_set_Word_m2052926514 (DeleteCustomizationWordRequest_t3182066146 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CWordU3Ek__BackingField_13(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/DeleteCustomizationWordRequest::get_Data()
extern "C"  String_t* DeleteCustomizationWordRequest_get_Data_m3747700475 (DeleteCustomizationWordRequest_t3182066146 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CDataU3Ek__BackingField_14();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/DeleteCustomizationWordRequest::set_Data(System.String)
extern "C"  void DeleteCustomizationWordRequest_set_Data_m4265011282 (DeleteCustomizationWordRequest_t3182066146 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CDataU3Ek__BackingField_14(L_0);
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetCustomizationCallback__ctor_m1261225860 (GetCustomizationCallback_t4165371535 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationCallback::Invoke(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Customization,System.String)
extern "C"  void GetCustomizationCallback_Invoke_m3398604559 (GetCustomizationCallback_t4165371535 * __this, Customization_t2261013253 * ___customization0, String_t* ___data1, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		GetCustomizationCallback_Invoke_m3398604559((GetCustomizationCallback_t4165371535 *)__this->get_prev_9(),___customization0, ___data1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, Customization_t2261013253 * ___customization0, String_t* ___data1, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___customization0, ___data1,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Customization_t2261013253 * ___customization0, String_t* ___data1, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___customization0, ___data1,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___data1, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___customization0, ___data1,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationCallback::BeginInvoke(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Customization,System.String,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* GetCustomizationCallback_BeginInvoke_m4266818606 (GetCustomizationCallback_t4165371535 * __this, Customization_t2261013253 * ___customization0, String_t* ___data1, AsyncCallback_t163412349 * ___callback2, RuntimeObject * ___object3, const RuntimeMethod* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___customization0;
	__d_args[1] = ___data1;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (RuntimeObject*)___object3);
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetCustomizationCallback_EndInvoke_m4144051306 (GetCustomizationCallback_t4165371535 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationRequest::.ctor()
extern "C"  void GetCustomizationRequest__ctor_m2422485738 (GetCustomizationRequest_t1351603463 * __this, const RuntimeMethod* method)
{
	{
		Request__ctor_m1928448681(__this, /*hidden argument*/NULL);
		return;
	}
}
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationCallback IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationRequest::get_Callback()
extern "C"  GetCustomizationCallback_t4165371535 * GetCustomizationRequest_get_Callback_m1998934598 (GetCustomizationRequest_t1351603463 * __this, const RuntimeMethod* method)
{
	{
		GetCustomizationCallback_t4165371535 * L_0 = __this->get_U3CCallbackU3Ek__BackingField_11();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationRequest::set_Callback(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationCallback)
extern "C"  void GetCustomizationRequest_set_Callback_m3900696383 (GetCustomizationRequest_t1351603463 * __this, GetCustomizationCallback_t4165371535 * ___value0, const RuntimeMethod* method)
{
	{
		GetCustomizationCallback_t4165371535 * L_0 = ___value0;
		__this->set_U3CCallbackU3Ek__BackingField_11(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationRequest::get_CustomizationID()
extern "C"  String_t* GetCustomizationRequest_get_CustomizationID_m2753278348 (GetCustomizationRequest_t1351603463 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CCustomizationIDU3Ek__BackingField_12();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationRequest::set_CustomizationID(System.String)
extern "C"  void GetCustomizationRequest_set_CustomizationID_m4212369937 (GetCustomizationRequest_t1351603463 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CCustomizationIDU3Ek__BackingField_12(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationRequest::get_Data()
extern "C"  String_t* GetCustomizationRequest_get_Data_m2763780886 (GetCustomizationRequest_t1351603463 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CDataU3Ek__BackingField_13();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationRequest::set_Data(System.String)
extern "C"  void GetCustomizationRequest_set_Data_m2028509081 (GetCustomizationRequest_t1351603463 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CDataU3Ek__BackingField_13(L_0);
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationsCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetCustomizationsCallback__ctor_m707246161 (GetCustomizationsCallback_t201646696 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationsCallback::Invoke(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Customizations,System.String)
extern "C"  void GetCustomizationsCallback_Invoke_m340907479 (GetCustomizationsCallback_t201646696 * __this, Customizations_t482380184 * ___customizations0, String_t* ___data1, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		GetCustomizationsCallback_Invoke_m340907479((GetCustomizationsCallback_t201646696 *)__this->get_prev_9(),___customizations0, ___data1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, Customizations_t482380184 * ___customizations0, String_t* ___data1, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___customizations0, ___data1,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Customizations_t482380184 * ___customizations0, String_t* ___data1, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___customizations0, ___data1,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___data1, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___customizations0, ___data1,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationsCallback::BeginInvoke(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Customizations,System.String,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* GetCustomizationsCallback_BeginInvoke_m3979094898 (GetCustomizationsCallback_t201646696 * __this, Customizations_t482380184 * ___customizations0, String_t* ___data1, AsyncCallback_t163412349 * ___callback2, RuntimeObject * ___object3, const RuntimeMethod* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___customizations0;
	__d_args[1] = ___data1;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (RuntimeObject*)___object3);
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationsCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetCustomizationsCallback_EndInvoke_m2689499123 (GetCustomizationsCallback_t201646696 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationsReq::.ctor()
extern "C"  void GetCustomizationsReq__ctor_m724592476 (GetCustomizationsReq_t2206589443 * __this, const RuntimeMethod* method)
{
	{
		Request__ctor_m1928448681(__this, /*hidden argument*/NULL);
		return;
	}
}
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationsCallback IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationsReq::get_Callback()
extern "C"  GetCustomizationsCallback_t201646696 * GetCustomizationsReq_get_Callback_m4047984775 (GetCustomizationsReq_t2206589443 * __this, const RuntimeMethod* method)
{
	{
		GetCustomizationsCallback_t201646696 * L_0 = __this->get_U3CCallbackU3Ek__BackingField_11();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationsReq::set_Callback(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationsCallback)
extern "C"  void GetCustomizationsReq_set_Callback_m483150754 (GetCustomizationsReq_t2206589443 * __this, GetCustomizationsCallback_t201646696 * ___value0, const RuntimeMethod* method)
{
	{
		GetCustomizationsCallback_t201646696 * L_0 = ___value0;
		__this->set_U3CCallbackU3Ek__BackingField_11(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationsReq::get_Data()
extern "C"  String_t* GetCustomizationsReq_get_Data_m1806572778 (GetCustomizationsReq_t2206589443 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CDataU3Ek__BackingField_12();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationsReq::set_Data(System.String)
extern "C"  void GetCustomizationsReq_set_Data_m3616098737 (GetCustomizationsReq_t2206589443 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CDataU3Ek__BackingField_12(L_0);
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetCustomizationWordCallback__ctor_m3528377892 (GetCustomizationWordCallback_t2485962781 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordCallback::Invoke(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Translation,System.String)
extern "C"  void GetCustomizationWordCallback_Invoke_m3773263563 (GetCustomizationWordCallback_t2485962781 * __this, Translation_t2174867539 * ___translation0, String_t* ___data1, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		GetCustomizationWordCallback_Invoke_m3773263563((GetCustomizationWordCallback_t2485962781 *)__this->get_prev_9(),___translation0, ___data1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, Translation_t2174867539 * ___translation0, String_t* ___data1, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___translation0, ___data1,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Translation_t2174867539 * ___translation0, String_t* ___data1, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___translation0, ___data1,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___data1, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___translation0, ___data1,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordCallback::BeginInvoke(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Translation,System.String,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* GetCustomizationWordCallback_BeginInvoke_m2658618476 (GetCustomizationWordCallback_t2485962781 * __this, Translation_t2174867539 * ___translation0, String_t* ___data1, AsyncCallback_t163412349 * ___callback2, RuntimeObject * ___object3, const RuntimeMethod* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___translation0;
	__d_args[1] = ___data1;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (RuntimeObject*)___object3);
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetCustomizationWordCallback_EndInvoke_m513448530 (GetCustomizationWordCallback_t2485962781 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordRequest::.ctor()
extern "C"  void GetCustomizationWordRequest__ctor_m3012566346 (GetCustomizationWordRequest_t1280810157 * __this, const RuntimeMethod* method)
{
	{
		Request__ctor_m1928448681(__this, /*hidden argument*/NULL);
		return;
	}
}
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordCallback IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordRequest::get_Callback()
extern "C"  GetCustomizationWordCallback_t2485962781 * GetCustomizationWordRequest_get_Callback_m3381062138 (GetCustomizationWordRequest_t1280810157 * __this, const RuntimeMethod* method)
{
	{
		GetCustomizationWordCallback_t2485962781 * L_0 = __this->get_U3CCallbackU3Ek__BackingField_11();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordRequest::set_Callback(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordCallback)
extern "C"  void GetCustomizationWordRequest_set_Callback_m1678004927 (GetCustomizationWordRequest_t1280810157 * __this, GetCustomizationWordCallback_t2485962781 * ___value0, const RuntimeMethod* method)
{
	{
		GetCustomizationWordCallback_t2485962781 * L_0 = ___value0;
		__this->set_U3CCallbackU3Ek__BackingField_11(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordRequest::get_CustomizationID()
extern "C"  String_t* GetCustomizationWordRequest_get_CustomizationID_m407727844 (GetCustomizationWordRequest_t1280810157 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CCustomizationIDU3Ek__BackingField_12();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordRequest::set_CustomizationID(System.String)
extern "C"  void GetCustomizationWordRequest_set_CustomizationID_m3921965539 (GetCustomizationWordRequest_t1280810157 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CCustomizationIDU3Ek__BackingField_12(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordRequest::get_Word()
extern "C"  String_t* GetCustomizationWordRequest_get_Word_m14623758 (GetCustomizationWordRequest_t1280810157 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CWordU3Ek__BackingField_13();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordRequest::set_Word(System.String)
extern "C"  void GetCustomizationWordRequest_set_Word_m3821257485 (GetCustomizationWordRequest_t1280810157 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CWordU3Ek__BackingField_13(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordRequest::get_Data()
extern "C"  String_t* GetCustomizationWordRequest_get_Data_m1350863650 (GetCustomizationWordRequest_t1280810157 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CDataU3Ek__BackingField_14();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordRequest::set_Data(System.String)
extern "C"  void GetCustomizationWordRequest_set_Data_m1038605743 (GetCustomizationWordRequest_t1280810157 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CDataU3Ek__BackingField_14(L_0);
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordsCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetCustomizationWordsCallback__ctor_m3241484371 (GetCustomizationWordsCallback_t2709615664 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordsCallback::Invoke(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Words,System.String)
extern "C"  void GetCustomizationWordsCallback_Invoke_m2042980066 (GetCustomizationWordsCallback_t2709615664 * __this, Words_t2948214629 * ___words0, String_t* ___data1, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		GetCustomizationWordsCallback_Invoke_m2042980066((GetCustomizationWordsCallback_t2709615664 *)__this->get_prev_9(),___words0, ___data1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, Words_t2948214629 * ___words0, String_t* ___data1, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___words0, ___data1,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Words_t2948214629 * ___words0, String_t* ___data1, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___words0, ___data1,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___data1, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___words0, ___data1,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordsCallback::BeginInvoke(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Words,System.String,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* GetCustomizationWordsCallback_BeginInvoke_m3834631965 (GetCustomizationWordsCallback_t2709615664 * __this, Words_t2948214629 * ___words0, String_t* ___data1, AsyncCallback_t163412349 * ___callback2, RuntimeObject * ___object3, const RuntimeMethod* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___words0;
	__d_args[1] = ___data1;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (RuntimeObject*)___object3);
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordsCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetCustomizationWordsCallback_EndInvoke_m1798701017 (GetCustomizationWordsCallback_t2709615664 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordsRequest::.ctor()
extern "C"  void GetCustomizationWordsRequest__ctor_m1707632875 (GetCustomizationWordsRequest_t110549728 * __this, const RuntimeMethod* method)
{
	{
		Request__ctor_m1928448681(__this, /*hidden argument*/NULL);
		return;
	}
}
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordsCallback IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordsRequest::get_Callback()
extern "C"  GetCustomizationWordsCallback_t2709615664 * GetCustomizationWordsRequest_get_Callback_m1338404144 (GetCustomizationWordsRequest_t110549728 * __this, const RuntimeMethod* method)
{
	{
		GetCustomizationWordsCallback_t2709615664 * L_0 = __this->get_U3CCallbackU3Ek__BackingField_11();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordsRequest::set_Callback(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordsCallback)
extern "C"  void GetCustomizationWordsRequest_set_Callback_m1563474383 (GetCustomizationWordsRequest_t110549728 * __this, GetCustomizationWordsCallback_t2709615664 * ___value0, const RuntimeMethod* method)
{
	{
		GetCustomizationWordsCallback_t2709615664 * L_0 = ___value0;
		__this->set_U3CCallbackU3Ek__BackingField_11(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordsRequest::get_CustomizationID()
extern "C"  String_t* GetCustomizationWordsRequest_get_CustomizationID_m2438918999 (GetCustomizationWordsRequest_t110549728 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CCustomizationIDU3Ek__BackingField_12();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordsRequest::set_CustomizationID(System.String)
extern "C"  void GetCustomizationWordsRequest_set_CustomizationID_m3489863342 (GetCustomizationWordsRequest_t110549728 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CCustomizationIDU3Ek__BackingField_12(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordsRequest::get_Data()
extern "C"  String_t* GetCustomizationWordsRequest_get_Data_m3001775595 (GetCustomizationWordsRequest_t110549728 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CDataU3Ek__BackingField_13();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordsRequest::set_Data(System.String)
extern "C"  void GetCustomizationWordsRequest_set_Data_m3341658500 (GetCustomizationWordsRequest_t110549728 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CDataU3Ek__BackingField_13(L_0);
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetPronunciationCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetPronunciationCallback__ctor_m3016817384 (GetPronunciationCallback_t2622344657 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetPronunciationCallback::Invoke(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Pronunciation)
extern "C"  void GetPronunciationCallback_Invoke_m2518062913 (GetPronunciationCallback_t2622344657 * __this, Pronunciation_t2711344207 * ___pronunciation0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		GetPronunciationCallback_Invoke_m2518062913((GetPronunciationCallback_t2622344657 *)__this->get_prev_9(),___pronunciation0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, Pronunciation_t2711344207 * ___pronunciation0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pronunciation0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Pronunciation_t2711344207 * ___pronunciation0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pronunciation0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___pronunciation0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetPronunciationCallback::BeginInvoke(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Pronunciation,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* GetPronunciationCallback_BeginInvoke_m2977623770 (GetPronunciationCallback_t2622344657 * __this, Pronunciation_t2711344207 * ___pronunciation0, AsyncCallback_t163412349 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___pronunciation0;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetPronunciationCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetPronunciationCallback_EndInvoke_m2202153598 (GetPronunciationCallback_t2622344657 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetPronunciationReq::.ctor()
extern "C"  void GetPronunciationReq__ctor_m239301707 (GetPronunciationReq_t2533372478 * __this, const RuntimeMethod* method)
{
	{
		Request__ctor_m1928448681(__this, /*hidden argument*/NULL);
		return;
	}
}
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetPronunciationCallback IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetPronunciationReq::get_Callback()
extern "C"  GetPronunciationCallback_t2622344657 * GetPronunciationReq_get_Callback_m3167238787 (GetPronunciationReq_t2533372478 * __this, const RuntimeMethod* method)
{
	{
		GetPronunciationCallback_t2622344657 * L_0 = __this->get_U3CCallbackU3Ek__BackingField_11();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetPronunciationReq::set_Callback(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetPronunciationCallback)
extern "C"  void GetPronunciationReq_set_Callback_m1752878660 (GetPronunciationReq_t2533372478 * __this, GetPronunciationCallback_t2622344657 * ___value0, const RuntimeMethod* method)
{
	{
		GetPronunciationCallback_t2622344657 * L_0 = ___value0;
		__this->set_U3CCallbackU3Ek__BackingField_11(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetPronunciationReq::get_Text()
extern "C"  String_t* GetPronunciationReq_get_Text_m3030959908 (GetPronunciationReq_t2533372478 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CTextU3Ek__BackingField_12();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetPronunciationReq::set_Text(System.String)
extern "C"  void GetPronunciationReq_set_Text_m2524915157 (GetPronunciationReq_t2533372478 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CTextU3Ek__BackingField_12(L_0);
		return;
	}
}
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.VoiceType IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetPronunciationReq::get_Voice()
extern "C"  int32_t GetPronunciationReq_get_Voice_m3467103471 (GetPronunciationReq_t2533372478 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_U3CVoiceU3Ek__BackingField_13();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetPronunciationReq::set_Voice(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.VoiceType)
extern "C"  void GetPronunciationReq_set_Voice_m2970561308 (GetPronunciationReq_t2533372478 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CVoiceU3Ek__BackingField_13(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetPronunciationReq::get_Format()
extern "C"  String_t* GetPronunciationReq_get_Format_m2557463904 (GetPronunciationReq_t2533372478 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CFormatU3Ek__BackingField_14();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetPronunciationReq::set_Format(System.String)
extern "C"  void GetPronunciationReq_set_Format_m175712339 (GetPronunciationReq_t2533372478 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CFormatU3Ek__BackingField_14(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetPronunciationReq::get_Customization_ID()
extern "C"  String_t* GetPronunciationReq_get_Customization_ID_m2296626588 (GetPronunciationReq_t2533372478 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CCustomization_IDU3Ek__BackingField_15();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetPronunciationReq::set_Customization_ID(System.String)
extern "C"  void GetPronunciationReq_set_Customization_ID_m776457007 (GetPronunciationReq_t2533372478 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CCustomization_IDU3Ek__BackingField_15(L_0);
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetVoiceCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetVoiceCallback__ctor_m3031632389 (GetVoiceCallback_t2752546046 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetVoiceCallback::Invoke(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Voice)
extern "C"  void GetVoiceCallback_Invoke_m2628701565 (GetVoiceCallback_t2752546046 * __this, Voice_t3646862260 * ___voice0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		GetVoiceCallback_Invoke_m2628701565((GetVoiceCallback_t2752546046 *)__this->get_prev_9(),___voice0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, Voice_t3646862260 * ___voice0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___voice0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Voice_t3646862260 * ___voice0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___voice0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___voice0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetVoiceCallback::BeginInvoke(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Voice,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* GetVoiceCallback_BeginInvoke_m3298519226 (GetVoiceCallback_t2752546046 * __this, Voice_t3646862260 * ___voice0, AsyncCallback_t163412349 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___voice0;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetVoiceCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetVoiceCallback_EndInvoke_m1291078907 (GetVoiceCallback_t2752546046 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetVoiceReq::.ctor()
extern "C"  void GetVoiceReq__ctor_m1969519438 (GetVoiceReq_t1980513795 * __this, const RuntimeMethod* method)
{
	{
		Request__ctor_m1928448681(__this, /*hidden argument*/NULL);
		return;
	}
}
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetVoiceCallback IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetVoiceReq::get_Callback()
extern "C"  GetVoiceCallback_t2752546046 * GetVoiceReq_get_Callback_m2983428311 (GetVoiceReq_t1980513795 * __this, const RuntimeMethod* method)
{
	{
		GetVoiceCallback_t2752546046 * L_0 = __this->get_U3CCallbackU3Ek__BackingField_11();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetVoiceReq::set_Callback(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetVoiceCallback)
extern "C"  void GetVoiceReq_set_Callback_m1200200228 (GetVoiceReq_t1980513795 * __this, GetVoiceCallback_t2752546046 * ___value0, const RuntimeMethod* method)
{
	{
		GetVoiceCallback_t2752546046 * L_0 = ___value0;
		__this->set_U3CCallbackU3Ek__BackingField_11(L_0);
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetVoicesCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetVoicesCallback__ctor_m717172724 (GetVoicesCallback_t3012885511 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetVoicesCallback::Invoke(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Voices)
extern "C"  void GetVoicesCallback_Invoke_m1235110721 (GetVoicesCallback_t3012885511 * __this, Voices_t4221445733 * ___voices0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		GetVoicesCallback_Invoke_m1235110721((GetVoicesCallback_t3012885511 *)__this->get_prev_9(),___voices0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, Voices_t4221445733 * ___voices0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___voices0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Voices_t4221445733 * ___voices0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___voices0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___voices0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetVoicesCallback::BeginInvoke(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Voices,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* GetVoicesCallback_BeginInvoke_m1801097192 (GetVoicesCallback_t3012885511 * __this, Voices_t4221445733 * ___voices0, AsyncCallback_t163412349 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___voices0;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetVoicesCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetVoicesCallback_EndInvoke_m3872279938 (GetVoicesCallback_t3012885511 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetVoicesReq::.ctor()
extern "C"  void GetVoicesReq__ctor_m1962143417 (GetVoicesReq_t1741653746 * __this, const RuntimeMethod* method)
{
	{
		Request__ctor_m1928448681(__this, /*hidden argument*/NULL);
		return;
	}
}
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetVoicesCallback IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetVoicesReq::get_Callback()
extern "C"  GetVoicesCallback_t3012885511 * GetVoicesReq_get_Callback_m4057882599 (GetVoicesReq_t1741653746 * __this, const RuntimeMethod* method)
{
	{
		GetVoicesCallback_t3012885511 * L_0 = __this->get_U3CCallbackU3Ek__BackingField_11();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetVoicesReq::set_Callback(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetVoicesCallback)
extern "C"  void GetVoicesReq_set_Callback_m1978410448 (GetVoicesReq_t1741653746 * __this, GetVoicesCallback_t3012885511 * ___value0, const RuntimeMethod* method)
{
	{
		GetVoicesCallback_t3012885511 * L_0 = ___value0;
		__this->set_U3CCallbackU3Ek__BackingField_11(L_0);
		return;
	}
}
extern "C"  void DelegatePInvokeWrapper_OnDeleteCustomizationCallback_t2862971265 (OnDeleteCustomizationCallback_t2862971265 * __this, bool ___success0, String_t* ___data1, const RuntimeMethod* method)
{
	typedef void (STDCALL *PInvokeFunc)(int32_t, char*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((Il2CppDelegate*)__this)->method));

	// Marshaling of parameter '___data1' to native representation
	char* ____data1_marshaled = NULL;
	____data1_marshaled = il2cpp_codegen_marshal_string(___data1);

	// Native function invocation
	il2cppPInvokeFunc(static_cast<int32_t>(___success0), ____data1_marshaled);

	// Marshaling cleanup of parameter '___data1' native representation
	il2cpp_codegen_marshal_free(____data1_marshaled);
	____data1_marshaled = NULL;

}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/OnDeleteCustomizationCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void OnDeleteCustomizationCallback__ctor_m481843348 (OnDeleteCustomizationCallback_t2862971265 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/OnDeleteCustomizationCallback::Invoke(System.Boolean,System.String)
extern "C"  void OnDeleteCustomizationCallback_Invoke_m2846474177 (OnDeleteCustomizationCallback_t2862971265 * __this, bool ___success0, String_t* ___data1, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		OnDeleteCustomizationCallback_Invoke_m2846474177((OnDeleteCustomizationCallback_t2862971265 *)__this->get_prev_9(),___success0, ___data1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, bool ___success0, String_t* ___data1, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___success0, ___data1,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, bool ___success0, String_t* ___data1, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___success0, ___data1,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/OnDeleteCustomizationCallback::BeginInvoke(System.Boolean,System.String,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* OnDeleteCustomizationCallback_BeginInvoke_m2955953730 (OnDeleteCustomizationCallback_t2862971265 * __this, bool ___success0, String_t* ___data1, AsyncCallback_t163412349 * ___callback2, RuntimeObject * ___object3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OnDeleteCustomizationCallback_BeginInvoke_m2955953730_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &___success0);
	__d_args[1] = ___data1;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (RuntimeObject*)___object3);
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/OnDeleteCustomizationCallback::EndInvoke(System.IAsyncResult)
extern "C"  void OnDeleteCustomizationCallback_EndInvoke_m2924259146 (OnDeleteCustomizationCallback_t2862971265 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C"  void DelegatePInvokeWrapper_OnDeleteCustomizationWordCallback_t2544578463 (OnDeleteCustomizationWordCallback_t2544578463 * __this, bool ___success0, String_t* ___data1, const RuntimeMethod* method)
{
	typedef void (STDCALL *PInvokeFunc)(int32_t, char*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((Il2CppDelegate*)__this)->method));

	// Marshaling of parameter '___data1' to native representation
	char* ____data1_marshaled = NULL;
	____data1_marshaled = il2cpp_codegen_marshal_string(___data1);

	// Native function invocation
	il2cppPInvokeFunc(static_cast<int32_t>(___success0), ____data1_marshaled);

	// Marshaling cleanup of parameter '___data1' native representation
	il2cpp_codegen_marshal_free(____data1_marshaled);
	____data1_marshaled = NULL;

}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/OnDeleteCustomizationWordCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void OnDeleteCustomizationWordCallback__ctor_m3934343164 (OnDeleteCustomizationWordCallback_t2544578463 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/OnDeleteCustomizationWordCallback::Invoke(System.Boolean,System.String)
extern "C"  void OnDeleteCustomizationWordCallback_Invoke_m226387231 (OnDeleteCustomizationWordCallback_t2544578463 * __this, bool ___success0, String_t* ___data1, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		OnDeleteCustomizationWordCallback_Invoke_m226387231((OnDeleteCustomizationWordCallback_t2544578463 *)__this->get_prev_9(),___success0, ___data1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, bool ___success0, String_t* ___data1, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___success0, ___data1,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, bool ___success0, String_t* ___data1, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___success0, ___data1,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/OnDeleteCustomizationWordCallback::BeginInvoke(System.Boolean,System.String,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* OnDeleteCustomizationWordCallback_BeginInvoke_m1909293590 (OnDeleteCustomizationWordCallback_t2544578463 * __this, bool ___success0, String_t* ___data1, AsyncCallback_t163412349 * ___callback2, RuntimeObject * ___object3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OnDeleteCustomizationWordCallback_BeginInvoke_m1909293590_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &___success0);
	__d_args[1] = ___data1;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (RuntimeObject*)___object3);
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/OnDeleteCustomizationWordCallback::EndInvoke(System.IAsyncResult)
extern "C"  void OnDeleteCustomizationWordCallback_EndInvoke_m2034419690 (OnDeleteCustomizationWordCallback_t2544578463 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/ToSpeechCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void ToSpeechCallback__ctor_m3323489678 (ToSpeechCallback_t3422748631 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/ToSpeechCallback::Invoke(UnityEngine.AudioClip)
extern "C"  void ToSpeechCallback_Invoke_m1017023077 (ToSpeechCallback_t3422748631 * __this, AudioClip_t1932558630 * ___clip0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ToSpeechCallback_Invoke_m1017023077((ToSpeechCallback_t3422748631 *)__this->get_prev_9(),___clip0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, AudioClip_t1932558630 * ___clip0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___clip0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, AudioClip_t1932558630 * ___clip0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___clip0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___clip0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/ToSpeechCallback::BeginInvoke(UnityEngine.AudioClip,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* ToSpeechCallback_BeginInvoke_m3066139262 (ToSpeechCallback_t3422748631 * __this, AudioClip_t1932558630 * ___clip0, AsyncCallback_t163412349 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___clip0;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/ToSpeechCallback::EndInvoke(System.IAsyncResult)
extern "C"  void ToSpeechCallback_EndInvoke_m104062360 (ToSpeechCallback_t3422748631 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/ToSpeechRequest::.ctor()
extern "C"  void ToSpeechRequest__ctor_m105557656 (ToSpeechRequest_t3970128895 * __this, const RuntimeMethod* method)
{
	{
		Request__ctor_m1928448681(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/ToSpeechRequest::get_TextId()
extern "C"  String_t* ToSpeechRequest_get_TextId_m3218788966 (ToSpeechRequest_t3970128895 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CTextIdU3Ek__BackingField_11();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/ToSpeechRequest::set_TextId(System.String)
extern "C"  void ToSpeechRequest_set_TextId_m2283266821 (ToSpeechRequest_t3970128895 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CTextIdU3Ek__BackingField_11(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/ToSpeechRequest::get_Text()
extern "C"  String_t* ToSpeechRequest_get_Text_m596768487 (ToSpeechRequest_t3970128895 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CTextU3Ek__BackingField_12();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/ToSpeechRequest::set_Text(System.String)
extern "C"  void ToSpeechRequest_set_Text_m84855046 (ToSpeechRequest_t3970128895 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CTextU3Ek__BackingField_12(L_0);
		return;
	}
}
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/ToSpeechCallback IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/ToSpeechRequest::get_Callback()
extern "C"  ToSpeechCallback_t3422748631 * ToSpeechRequest_get_Callback_m490611990 (ToSpeechRequest_t3970128895 * __this, const RuntimeMethod* method)
{
	{
		ToSpeechCallback_t3422748631 * L_0 = __this->get_U3CCallbackU3Ek__BackingField_13();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/ToSpeechRequest::set_Callback(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/ToSpeechCallback)
extern "C"  void ToSpeechRequest_set_Callback_m683453887 (ToSpeechRequest_t3970128895 * __this, ToSpeechCallback_t3422748631 * ___value0, const RuntimeMethod* method)
{
	{
		ToSpeechCallback_t3422748631 * L_0 = ___value0;
		__this->set_U3CCallbackU3Ek__BackingField_13(L_0);
		return;
	}
}
extern "C"  void DelegatePInvokeWrapper_UpdateCustomizationCallback_t3026233620 (UpdateCustomizationCallback_t3026233620 * __this, bool ___success0, String_t* ___data1, const RuntimeMethod* method)
{
	typedef void (STDCALL *PInvokeFunc)(int32_t, char*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((Il2CppDelegate*)__this)->method));

	// Marshaling of parameter '___data1' to native representation
	char* ____data1_marshaled = NULL;
	____data1_marshaled = il2cpp_codegen_marshal_string(___data1);

	// Native function invocation
	il2cppPInvokeFunc(static_cast<int32_t>(___success0), ____data1_marshaled);

	// Marshaling cleanup of parameter '___data1' native representation
	il2cpp_codegen_marshal_free(____data1_marshaled);
	____data1_marshaled = NULL;

}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/UpdateCustomizationCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void UpdateCustomizationCallback__ctor_m1151577965 (UpdateCustomizationCallback_t3026233620 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/UpdateCustomizationCallback::Invoke(System.Boolean,System.String)
extern "C"  void UpdateCustomizationCallback_Invoke_m16749586 (UpdateCustomizationCallback_t3026233620 * __this, bool ___success0, String_t* ___data1, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UpdateCustomizationCallback_Invoke_m16749586((UpdateCustomizationCallback_t3026233620 *)__this->get_prev_9(),___success0, ___data1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, bool ___success0, String_t* ___data1, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___success0, ___data1,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, bool ___success0, String_t* ___data1, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___success0, ___data1,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/UpdateCustomizationCallback::BeginInvoke(System.Boolean,System.String,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* UpdateCustomizationCallback_BeginInvoke_m3225856991 (UpdateCustomizationCallback_t3026233620 * __this, bool ___success0, String_t* ___data1, AsyncCallback_t163412349 * ___callback2, RuntimeObject * ___object3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UpdateCustomizationCallback_BeginInvoke_m3225856991_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &___success0);
	__d_args[1] = ___data1;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (RuntimeObject*)___object3);
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/UpdateCustomizationCallback::EndInvoke(System.IAsyncResult)
extern "C"  void UpdateCustomizationCallback_EndInvoke_m3235683175 (UpdateCustomizationCallback_t3026233620 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/UpdateCustomizationRequest::.ctor()
extern "C"  void UpdateCustomizationRequest__ctor_m2167726953 (UpdateCustomizationRequest_t1669343124 * __this, const RuntimeMethod* method)
{
	{
		Request__ctor_m1928448681(__this, /*hidden argument*/NULL);
		return;
	}
}
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/UpdateCustomizationCallback IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/UpdateCustomizationRequest::get_Callback()
extern "C"  UpdateCustomizationCallback_t3026233620 * UpdateCustomizationRequest_get_Callback_m1649430960 (UpdateCustomizationRequest_t1669343124 * __this, const RuntimeMethod* method)
{
	{
		UpdateCustomizationCallback_t3026233620 * L_0 = __this->get_U3CCallbackU3Ek__BackingField_11();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/UpdateCustomizationRequest::set_Callback(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/UpdateCustomizationCallback)
extern "C"  void UpdateCustomizationRequest_set_Callback_m374347127 (UpdateCustomizationRequest_t1669343124 * __this, UpdateCustomizationCallback_t3026233620 * ___value0, const RuntimeMethod* method)
{
	{
		UpdateCustomizationCallback_t3026233620 * L_0 = ___value0;
		__this->set_U3CCallbackU3Ek__BackingField_11(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/UpdateCustomizationRequest::get_CustomizationID()
extern "C"  String_t* UpdateCustomizationRequest_get_CustomizationID_m593664537 (UpdateCustomizationRequest_t1669343124 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CCustomizationIDU3Ek__BackingField_12();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/UpdateCustomizationRequest::set_CustomizationID(System.String)
extern "C"  void UpdateCustomizationRequest_set_CustomizationID_m3383711138 (UpdateCustomizationRequest_t1669343124 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CCustomizationIDU3Ek__BackingField_12(L_0);
		return;
	}
}
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomVoiceUpdate IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/UpdateCustomizationRequest::get_CustomVoiceUpdate()
extern "C"  CustomVoiceUpdate_t1706248840 * UpdateCustomizationRequest_get_CustomVoiceUpdate_m3190369009 (UpdateCustomizationRequest_t1669343124 * __this, const RuntimeMethod* method)
{
	{
		CustomVoiceUpdate_t1706248840 * L_0 = __this->get_U3CCustomVoiceUpdateU3Ek__BackingField_13();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/UpdateCustomizationRequest::set_CustomVoiceUpdate(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomVoiceUpdate)
extern "C"  void UpdateCustomizationRequest_set_CustomVoiceUpdate_m1632905410 (UpdateCustomizationRequest_t1669343124 * __this, CustomVoiceUpdate_t1706248840 * ___value0, const RuntimeMethod* method)
{
	{
		CustomVoiceUpdate_t1706248840 * L_0 = ___value0;
		__this->set_U3CCustomVoiceUpdateU3Ek__BackingField_13(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/UpdateCustomizationRequest::get_Data()
extern "C"  String_t* UpdateCustomizationRequest_get_Data_m3622793189 (UpdateCustomizationRequest_t1669343124 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CDataU3Ek__BackingField_14();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/UpdateCustomizationRequest::set_Data(System.String)
extern "C"  void UpdateCustomizationRequest_set_Data_m3367529024 (UpdateCustomizationRequest_t1669343124 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CDataU3Ek__BackingField_14(L_0);
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Translation::.ctor()
extern "C"  void Translation__ctor_m1959656352 (Translation_t2174867539 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Translation::get_translation()
extern "C"  String_t* Translation_get_translation_m2050062025 (Translation_t2174867539 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CtranslationU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Translation::set_translation(System.String)
extern "C"  void Translation_set_translation_m4139441334 (Translation_t2174867539 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CtranslationU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Voice::.ctor()
extern "C"  void Voice__ctor_m3947940203 (Voice_t3646862260 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Voice::get_name()
extern "C"  String_t* Voice_get_name_m2895487336 (Voice_t3646862260 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CnameU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Voice::set_name(System.String)
extern "C"  void Voice_set_name_m3282035103 (Voice_t3646862260 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CnameU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Voice::get_language()
extern "C"  String_t* Voice_get_language_m1645066969 (Voice_t3646862260 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3ClanguageU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Voice::set_language(System.String)
extern "C"  void Voice_set_language_m3704244978 (Voice_t3646862260 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3ClanguageU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Voice::get_gender()
extern "C"  String_t* Voice_get_gender_m1915758474 (Voice_t3646862260 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CgenderU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Voice::set_gender(System.String)
extern "C"  void Voice_set_gender_m1968067651 (Voice_t3646862260 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CgenderU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Voice::get_url()
extern "C"  String_t* Voice_get_url_m4024008598 (Voice_t3646862260 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CurlU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Voice::set_url(System.String)
extern "C"  void Voice_set_url_m417064879 (Voice_t3646862260 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CurlU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Voice::get_description()
extern "C"  String_t* Voice_get_description_m2342482349 (Voice_t3646862260 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CdescriptionU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Voice::set_description(System.String)
extern "C"  void Voice_set_description_m1741712476 (Voice_t3646862260 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CdescriptionU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Boolean IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Voice::get_customizable()
extern "C"  bool Voice_get_customizable_m2687311792 (Voice_t3646862260 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_U3CcustomizableU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Voice::set_customizable(System.Boolean)
extern "C"  void Voice_set_customizable_m1655135427 (Voice_t3646862260 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CcustomizableU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Voices::.ctor()
extern "C"  void Voices__ctor_m2371932580 (Voices_t4221445733 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Voice[] IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Voices::get_voices()
extern "C"  VoiceU5BU5D_t260285181* Voices_get_voices_m849753461 (Voices_t4221445733 * __this, const RuntimeMethod* method)
{
	{
		VoiceU5BU5D_t260285181* L_0 = __this->get_U3CvoicesU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Voices::set_voices(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Voice[])
extern "C"  void Voices_set_voices_m3501541780 (Voices_t4221445733 * __this, VoiceU5BU5D_t260285181* ___value0, const RuntimeMethod* method)
{
	{
		VoiceU5BU5D_t260285181* L_0 = ___value0;
		__this->set_U3CvoicesU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Boolean IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Voices::HasData()
extern "C"  bool Voices_HasData_m4075844530 (Voices_t4221445733 * __this, const RuntimeMethod* method)
{
	int32_t G_B3_0 = 0;
	{
		VoiceU5BU5D_t260285181* L_0 = Voices_get_voices_m849753461(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		VoiceU5BU5D_t260285181* L_1 = Voices_get_voices_m849753461(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		G_B3_0 = ((((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_1)->max_length))))) > ((int32_t)0))? 1 : 0);
		goto IL_0019;
	}

IL_0018:
	{
		G_B3_0 = 0;
	}

IL_0019:
	{
		return (bool)G_B3_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Word::.ctor()
extern "C"  void Word__ctor_m709387227 (Word_t4274554970 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Word::get_word()
extern "C"  String_t* Word_get_word_m4109338749 (Word_t4274554970 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CwordU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Word::set_word(System.String)
extern "C"  void Word_set_word_m2973666318 (Word_t4274554970 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CwordU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Word::get_translation()
extern "C"  String_t* Word_get_translation_m1626656560 (Word_t4274554970 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CtranslationU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Word::set_translation(System.String)
extern "C"  void Word_set_translation_m1468819251 (Word_t4274554970 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CtranslationU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Words::.ctor()
extern "C"  void Words__ctor_m314888114 (Words_t2948214629 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Word[] IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Words::get_words()
extern "C"  WordU5BU5D_t3686373631* Words_get_words_m3194785885 (Words_t2948214629 * __this, const RuntimeMethod* method)
{
	{
		WordU5BU5D_t3686373631* L_0 = __this->get_U3CwordsU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Words::set_words(IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Word[])
extern "C"  void Words_set_words_m1521063420 (Words_t2948214629 * __this, WordU5BU5D_t3686373631* ___value0, const RuntimeMethod* method)
{
	{
		WordU5BU5D_t3686373631* L_0 = ___value0;
		__this->set_U3CwordsU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Boolean IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Words::HasData()
extern "C"  bool Words_HasData_m2703714480 (Words_t2948214629 * __this, const RuntimeMethod* method)
{
	int32_t G_B3_0 = 0;
	{
		WordU5BU5D_t3686373631* L_0 = Words_get_words_m3194785885(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		WordU5BU5D_t3686373631* L_1 = Words_get_words_m3194785885(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		G_B3_0 = ((((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_1)->max_length))))) > ((int32_t)0))? 1 : 0);
		goto IL_0019;
	}

IL_0018:
	{
		G_B3_0 = 0;
	}

IL_0019:
	{
		return (bool)G_B3_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.DocumentTone::.ctor()
extern "C"  void DocumentTone__ctor_m1646230138 (DocumentTone_t2537937785 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneCategory[] IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.DocumentTone::get_tone_categories()
extern "C"  ToneCategoryU5BU5D_t3565596459* DocumentTone_get_tone_categories_m3219943273 (DocumentTone_t2537937785 * __this, const RuntimeMethod* method)
{
	{
		ToneCategoryU5BU5D_t3565596459* L_0 = __this->get_U3Ctone_categoriesU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.DocumentTone::set_tone_categories(IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneCategory[])
extern "C"  void DocumentTone_set_tone_categories_m2272049780 (DocumentTone_t2537937785 * __this, ToneCategoryU5BU5D_t3565596459* ___value0, const RuntimeMethod* method)
{
	{
		ToneCategoryU5BU5D_t3565596459* L_0 = ___value0;
		__this->set_U3Ctone_categoriesU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.DocumentTone::ToString()
extern "C"  String_t* DocumentTone_ToString_m851423115 (DocumentTone_t2537937785 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DocumentTone_ToString_m851423115_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		V_0 = L_0;
		V_1 = 0;
		goto IL_0025;
	}

IL_000d:
	{
		String_t* L_1 = V_0;
		ToneCategoryU5BU5D_t3565596459* L_2 = DocumentTone_get_tone_categories_m3219943273(__this, /*hidden argument*/NULL);
		int32_t L_3 = V_1;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		ToneCategory_t3724297374 * L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		NullCheck(L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_5);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m2596409543(NULL /*static, unused*/, L_1, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		int32_t L_8 = V_1;
		V_1 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0025:
	{
		ToneCategoryU5BU5D_t3565596459* L_9 = DocumentTone_get_tone_categories_m3219943273(__this, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_003e;
		}
	}
	{
		int32_t L_10 = V_1;
		ToneCategoryU5BU5D_t3565596459* L_11 = DocumentTone_get_tone_categories_m3219943273(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_11)->max_length)))))))
		{
			goto IL_000d;
		}
	}

IL_003e:
	{
		String_t* L_12 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral3312868321, L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.SentenceTone::.ctor()
extern "C"  void SentenceTone__ctor_m1255722546 (SentenceTone_t3234952115 * __this, const RuntimeMethod* method)
{
	{
		__this->set_m_HighestScore_5((-1.0));
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.SentenceTone::get_sentence_id()
extern "C"  int32_t SentenceTone_get_sentence_id_m1350765144 (SentenceTone_t3234952115 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_U3Csentence_idU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.SentenceTone::set_sentence_id(System.Int32)
extern "C"  void SentenceTone_set_sentence_id_m1129825609 (SentenceTone_t3234952115 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3Csentence_idU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Int32 IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.SentenceTone::get_input_from()
extern "C"  int32_t SentenceTone_get_input_from_m4195132438 (SentenceTone_t3234952115 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_U3Cinput_fromU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.SentenceTone::set_input_from(System.Int32)
extern "C"  void SentenceTone_set_input_from_m948451717 (SentenceTone_t3234952115 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3Cinput_fromU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Int32 IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.SentenceTone::get_input_to()
extern "C"  int32_t SentenceTone_get_input_to_m1797854435 (SentenceTone_t3234952115 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_U3Cinput_toU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.SentenceTone::set_input_to(System.Int32)
extern "C"  void SentenceTone_set_input_to_m1916494436 (SentenceTone_t3234952115 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3Cinput_toU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.SentenceTone::get_text()
extern "C"  String_t* SentenceTone_get_text_m1133044659 (SentenceTone_t3234952115 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CtextU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.SentenceTone::set_text(System.String)
extern "C"  void SentenceTone_set_text_m3746658244 (SentenceTone_t3234952115 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CtextU3Ek__BackingField_3(L_0);
		return;
	}
}
// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneCategory[] IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.SentenceTone::get_tone_categories()
extern "C"  ToneCategoryU5BU5D_t3565596459* SentenceTone_get_tone_categories_m2735460483 (SentenceTone_t3234952115 * __this, const RuntimeMethod* method)
{
	{
		ToneCategoryU5BU5D_t3565596459* L_0 = __this->get_U3Ctone_categoriesU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.SentenceTone::set_tone_categories(IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneCategory[])
extern "C"  void SentenceTone_set_tone_categories_m2363326556 (SentenceTone_t3234952115 * __this, ToneCategoryU5BU5D_t3565596459* ___value0, const RuntimeMethod* method)
{
	{
		ToneCategoryU5BU5D_t3565596459* L_0 = ___value0;
		__this->set_U3Ctone_categoriesU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Double IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.SentenceTone::get_HighestScore()
extern "C"  double SentenceTone_get_HighestScore_m655985808 (SentenceTone_t3234952115 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		double L_0 = __this->get_m_HighestScore_5();
		if ((!(((double)L_0) < ((double)(0.0)))))
		{
			goto IL_00d0;
		}
	}
	{
		V_0 = 0;
		goto IL_00b7;
	}

IL_001b:
	{
		V_1 = 0;
		goto IL_008c;
	}

IL_0022:
	{
		double L_1 = __this->get_m_HighestScore_5();
		ToneCategoryU5BU5D_t3565596459* L_2 = SentenceTone_get_tone_categories_m2735460483(__this, /*hidden argument*/NULL);
		int32_t L_3 = V_0;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		ToneCategory_t3724297374 * L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		NullCheck(L_5);
		ToneU5BU5D_t4171600311* L_6 = ToneCategory_get_tones_m2345877352(L_5, /*hidden argument*/NULL);
		int32_t L_7 = V_1;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		Tone_t1138009090 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		NullCheck(L_9);
		double L_10 = Tone_get_score_m3537196787(L_9, /*hidden argument*/NULL);
		if ((!(((double)L_1) < ((double)L_10))))
		{
			goto IL_0088;
		}
	}
	{
		ToneCategoryU5BU5D_t3565596459* L_11 = SentenceTone_get_tone_categories_m2735460483(__this, /*hidden argument*/NULL);
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		ToneCategory_t3724297374 * L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck(L_14);
		ToneU5BU5D_t4171600311* L_15 = ToneCategory_get_tones_m2345877352(L_14, /*hidden argument*/NULL);
		int32_t L_16 = V_1;
		NullCheck(L_15);
		int32_t L_17 = L_16;
		Tone_t1138009090 * L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		NullCheck(L_18);
		double L_19 = Tone_get_score_m3537196787(L_18, /*hidden argument*/NULL);
		__this->set_m_HighestScore_5(L_19);
		ToneCategoryU5BU5D_t3565596459* L_20 = SentenceTone_get_tone_categories_m2735460483(__this, /*hidden argument*/NULL);
		int32_t L_21 = V_0;
		NullCheck(L_20);
		int32_t L_22 = L_21;
		ToneCategory_t3724297374 * L_23 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_22));
		NullCheck(L_23);
		ToneU5BU5D_t4171600311* L_24 = ToneCategory_get_tones_m2345877352(L_23, /*hidden argument*/NULL);
		int32_t L_25 = V_1;
		NullCheck(L_24);
		int32_t L_26 = L_25;
		Tone_t1138009090 * L_27 = (L_24)->GetAt(static_cast<il2cpp_array_size_t>(L_26));
		NullCheck(L_27);
		String_t* L_28 = Tone_get_tone_name_m4125480123(L_27, /*hidden argument*/NULL);
		__this->set_m_HighestScoreToneName_6(L_28);
		ToneCategoryU5BU5D_t3565596459* L_29 = SentenceTone_get_tone_categories_m2735460483(__this, /*hidden argument*/NULL);
		int32_t L_30 = V_0;
		NullCheck(L_29);
		int32_t L_31 = L_30;
		ToneCategory_t3724297374 * L_32 = (L_29)->GetAt(static_cast<il2cpp_array_size_t>(L_31));
		NullCheck(L_32);
		String_t* L_33 = ToneCategory_get_category_name_m1377209155(L_32, /*hidden argument*/NULL);
		__this->set_m_HighestScoreToneCategoryName_7(L_33);
	}

IL_0088:
	{
		int32_t L_34 = V_1;
		V_1 = ((int32_t)((int32_t)L_34+(int32_t)1));
	}

IL_008c:
	{
		ToneCategoryU5BU5D_t3565596459* L_35 = SentenceTone_get_tone_categories_m2735460483(__this, /*hidden argument*/NULL);
		int32_t L_36 = V_0;
		NullCheck(L_35);
		int32_t L_37 = L_36;
		ToneCategory_t3724297374 * L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_38);
		ToneU5BU5D_t4171600311* L_39 = ToneCategory_get_tones_m2345877352(L_38, /*hidden argument*/NULL);
		if (!L_39)
		{
			goto IL_00b3;
		}
	}
	{
		int32_t L_40 = V_1;
		ToneCategoryU5BU5D_t3565596459* L_41 = SentenceTone_get_tone_categories_m2735460483(__this, /*hidden argument*/NULL);
		int32_t L_42 = V_0;
		NullCheck(L_41);
		int32_t L_43 = L_42;
		ToneCategory_t3724297374 * L_44 = (L_41)->GetAt(static_cast<il2cpp_array_size_t>(L_43));
		NullCheck(L_44);
		ToneU5BU5D_t4171600311* L_45 = ToneCategory_get_tones_m2345877352(L_44, /*hidden argument*/NULL);
		NullCheck(L_45);
		if ((((int32_t)L_40) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_45)->max_length)))))))
		{
			goto IL_0022;
		}
	}

IL_00b3:
	{
		int32_t L_46 = V_0;
		V_0 = ((int32_t)((int32_t)L_46+(int32_t)1));
	}

IL_00b7:
	{
		ToneCategoryU5BU5D_t3565596459* L_47 = SentenceTone_get_tone_categories_m2735460483(__this, /*hidden argument*/NULL);
		if (!L_47)
		{
			goto IL_00d0;
		}
	}
	{
		int32_t L_48 = V_0;
		ToneCategoryU5BU5D_t3565596459* L_49 = SentenceTone_get_tone_categories_m2735460483(__this, /*hidden argument*/NULL);
		NullCheck(L_49);
		if ((((int32_t)L_48) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_49)->max_length)))))))
		{
			goto IL_001b;
		}
	}

IL_00d0:
	{
		double L_50 = __this->get_m_HighestScore_5();
		return L_50;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.SentenceTone::get_HighestScoreToneName()
extern "C"  String_t* SentenceTone_get_HighestScoreToneName_m3603568465 (SentenceTone_t3234952115 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_m_HighestScoreToneName_6();
		return L_0;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.SentenceTone::get_HighestScoreToneCategoryName()
extern "C"  String_t* SentenceTone_get_HighestScoreToneCategoryName_m1438144885 (SentenceTone_t3234952115 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_m_HighestScoreToneCategoryName_7();
		return L_0;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.SentenceTone::ToString()
extern "C"  String_t* SentenceTone_ToString_m3030164881 (SentenceTone_t3234952115 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SentenceTone_ToString_m3030164881_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		V_0 = L_0;
		V_1 = 0;
		goto IL_0025;
	}

IL_000d:
	{
		String_t* L_1 = V_0;
		ToneCategoryU5BU5D_t3565596459* L_2 = SentenceTone_get_tone_categories_m2735460483(__this, /*hidden argument*/NULL);
		int32_t L_3 = V_1;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		ToneCategory_t3724297374 * L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		NullCheck(L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_5);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m2596409543(NULL /*static, unused*/, L_1, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		int32_t L_8 = V_1;
		V_1 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0025:
	{
		ToneCategoryU5BU5D_t3565596459* L_9 = SentenceTone_get_tone_categories_m2735460483(__this, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_003e;
		}
	}
	{
		int32_t L_10 = V_1;
		ToneCategoryU5BU5D_t3565596459* L_11 = SentenceTone_get_tone_categories_m2735460483(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_11)->max_length)))))))
		{
			goto IL_000d;
		}
	}

IL_003e:
	{
		ObjectU5BU5D_t3614634134* L_12 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		int32_t L_13 = SentenceTone_get_sentence_id_m1350765144(__this, /*hidden argument*/NULL);
		int32_t L_14 = L_13;
		RuntimeObject * L_15 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_15);
		ObjectU5BU5D_t3614634134* L_16 = L_12;
		int32_t L_17 = SentenceTone_get_input_from_m4195132438(__this, /*hidden argument*/NULL);
		int32_t L_18 = L_17;
		RuntimeObject * L_19 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_18);
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, L_19);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_19);
		ObjectU5BU5D_t3614634134* L_20 = L_16;
		int32_t L_21 = SentenceTone_get_input_to_m1797854435(__this, /*hidden argument*/NULL);
		int32_t L_22 = L_21;
		RuntimeObject * L_23 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_22);
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, L_23);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_23);
		ObjectU5BU5D_t3614634134* L_24 = L_20;
		String_t* L_25 = SentenceTone_get_text_m1133044659(__this, /*hidden argument*/NULL);
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, L_25);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_25);
		ObjectU5BU5D_t3614634134* L_26 = L_24;
		String_t* L_27 = V_0;
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, L_27);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(4), (RuntimeObject *)L_27);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_28 = String_Format_m1263743648(NULL /*static, unused*/, _stringLiteral631001884, L_26, /*hidden argument*/NULL);
		return L_28;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.Tone::.ctor()
extern "C"  void Tone__ctor_m327556015 (Tone_t1138009090 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Double IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.Tone::get_score()
extern "C"  double Tone_get_score_m3537196787 (Tone_t1138009090 * __this, const RuntimeMethod* method)
{
	{
		double L_0 = __this->get_U3CscoreU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.Tone::set_score(System.Double)
extern "C"  void Tone_set_score_m4188578218 (Tone_t1138009090 * __this, double ___value0, const RuntimeMethod* method)
{
	{
		double L_0 = ___value0;
		__this->set_U3CscoreU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.Tone::get_tone_id()
extern "C"  String_t* Tone_get_tone_id_m1698165445 (Tone_t1138009090 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3Ctone_idU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.Tone::set_tone_id(System.String)
extern "C"  void Tone_set_tone_id_m1822637388 (Tone_t1138009090 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3Ctone_idU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.Tone::get_tone_name()
extern "C"  String_t* Tone_get_tone_name_m4125480123 (Tone_t1138009090 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3Ctone_nameU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.Tone::set_tone_name(System.String)
extern "C"  void Tone_set_tone_name_m1038959206 (Tone_t1138009090 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3Ctone_nameU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.Tone::ToString()
extern "C"  String_t* Tone_ToString_m3031539698 (Tone_t1138009090 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tone_ToString_m3031539698_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		double L_0 = Tone_get_score_m3537196787(__this, /*hidden argument*/NULL);
		double L_1 = L_0;
		RuntimeObject * L_2 = Box(Double_t4078015681_il2cpp_TypeInfo_var, &L_1);
		String_t* L_3 = Tone_get_tone_id_m1698165445(__this, /*hidden argument*/NULL);
		String_t* L_4 = Tone_get_tone_name_m4125480123(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Format_m4262916296(NULL /*static, unused*/, _stringLiteral1553323438, L_2, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzer::.ctor()
extern "C"  void ToneAnalyzer__ctor_m2989271281 (ToneAnalyzer_t1356110496 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzer::GetToneAnalyze(IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzer/OnGetToneAnalyzed,System.String,System.String)
extern "C"  bool ToneAnalyzer_GetToneAnalyze_m2599887229 (ToneAnalyzer_t1356110496 * __this, OnGetToneAnalyzed_t3813655652 * ___callback0, String_t* ___text1, String_t* ___data2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ToneAnalyzer_GetToneAnalyze_m2599887229_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RESTConnector_t3705102247 * V_0 = NULL;
	GetToneAnalyzerRequest_t1769916618 * V_1 = NULL;
	Dictionary_2_t3943999495 * V_2 = NULL;
	{
		OnGetToneAnalyzed_t3813655652 * L_0 = ___callback0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, _stringLiteral1913944053, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(RESTConnector_t3705102247_il2cpp_TypeInfo_var);
		RESTConnector_t3705102247 * L_2 = RESTConnector_GetConnector_m3262139854(NULL /*static, unused*/, _stringLiteral988188995, _stringLiteral2179659185, (bool)1, /*hidden argument*/NULL);
		V_0 = L_2;
		RESTConnector_t3705102247 * L_3 = V_0;
		if (L_3)
		{
			goto IL_002a;
		}
	}
	{
		return (bool)0;
	}

IL_002a:
	{
		GetToneAnalyzerRequest_t1769916618 * L_4 = (GetToneAnalyzerRequest_t1769916618 *)il2cpp_codegen_object_new(GetToneAnalyzerRequest_t1769916618_il2cpp_TypeInfo_var);
		GetToneAnalyzerRequest__ctor_m3143334739(L_4, /*hidden argument*/NULL);
		V_1 = L_4;
		GetToneAnalyzerRequest_t1769916618 * L_5 = V_1;
		OnGetToneAnalyzed_t3813655652 * L_6 = ___callback0;
		NullCheck(L_5);
		GetToneAnalyzerRequest_set_Callback_m1849301549(L_5, L_6, /*hidden argument*/NULL);
		GetToneAnalyzerRequest_t1769916618 * L_7 = V_1;
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)ToneAnalyzer_GetToneAnalyzerResponse_m3585275770_RuntimeMethod_var);
		ResponseEvent_t1616568356 * L_9 = (ResponseEvent_t1616568356 *)il2cpp_codegen_object_new(ResponseEvent_t1616568356_il2cpp_TypeInfo_var);
		ResponseEvent__ctor_m3385394859(L_9, __this, L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		Request_set_OnResponse_m2226001952(L_7, L_9, /*hidden argument*/NULL);
		Dictionary_2_t3943999495 * L_10 = (Dictionary_2_t3943999495 *)il2cpp_codegen_object_new(Dictionary_2_t3943999495_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m760167321(L_10, /*hidden argument*/Dictionary_2__ctor_m760167321_RuntimeMethod_var);
		V_2 = L_10;
		Dictionary_2_t3943999495 * L_11 = V_2;
		String_t* L_12 = ___text1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral372029312, L_12, _stringLiteral372029312, /*hidden argument*/NULL);
		NullCheck(L_11);
		Dictionary_2_set_Item_m4244870320(L_11, _stringLiteral3423761293, L_13, /*hidden argument*/Dictionary_2_set_Item_m4244870320_RuntimeMethod_var);
		GetToneAnalyzerRequest_t1769916618 * L_14 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
		Encoding_t663144255 * L_15 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
		Dictionary_2_t3943999495 * L_16 = V_2;
		String_t* L_17 = Json_Serialize_m779708699(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		NullCheck(L_15);
		ByteU5BU5D_t3397334013* L_18 = VirtFuncInvoker1< ByteU5BU5D_t3397334013*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_15, L_17);
		NullCheck(L_14);
		Request_set_Send_m296178747(L_14, L_18, /*hidden argument*/NULL);
		GetToneAnalyzerRequest_t1769916618 * L_19 = V_1;
		String_t* L_20 = ___data2;
		NullCheck(L_19);
		GetToneAnalyzerRequest_set_Data_m3436767482(L_19, L_20, /*hidden argument*/NULL);
		GetToneAnalyzerRequest_t1769916618 * L_21 = V_1;
		NullCheck(L_21);
		Dictionary_2_t3943999495 * L_22 = Request_get_Headers_m340270624(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		Dictionary_2_set_Item_m4244870320(L_22, _stringLiteral1048821954, _stringLiteral1391431453, /*hidden argument*/Dictionary_2_set_Item_m4244870320_RuntimeMethod_var);
		GetToneAnalyzerRequest_t1769916618 * L_23 = V_1;
		NullCheck(L_23);
		Dictionary_2_t309261261 * L_24 = Request_get_Parameters_m1217035494(L_23, /*hidden argument*/NULL);
		NullCheck(L_24);
		Dictionary_2_set_Item_m3411472609(L_24, _stringLiteral3617362, _stringLiteral1900084403, /*hidden argument*/Dictionary_2_set_Item_m3411472609_RuntimeMethod_var);
		GetToneAnalyzerRequest_t1769916618 * L_25 = V_1;
		NullCheck(L_25);
		Dictionary_2_t309261261 * L_26 = Request_get_Parameters_m1217035494(L_25, /*hidden argument*/NULL);
		NullCheck(L_26);
		Dictionary_2_set_Item_m3411472609(L_26, _stringLiteral3177042952, _stringLiteral3323263070, /*hidden argument*/Dictionary_2_set_Item_m3411472609_RuntimeMethod_var);
		RESTConnector_t3705102247 * L_27 = V_0;
		GetToneAnalyzerRequest_t1769916618 * L_28 = V_1;
		NullCheck(L_27);
		bool L_29 = RESTConnector_Send_m2925366401(L_27, L_28, /*hidden argument*/NULL);
		return L_29;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzer::GetToneAnalyzerResponse(IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request,IBM.Watson.DeveloperCloud.Connection.RESTConnector/Response)
extern "C"  void ToneAnalyzer_GetToneAnalyzerResponse_m3585275770 (ToneAnalyzer_t1356110496 * __this, Request_t466816980 * ___req0, Response_t429319368 * ___resp1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ToneAnalyzer_GetToneAnalyzerResponse_m3585275770_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ToneAnalyzerResponse_t3391014235 * V_0 = NULL;
	fsData_t2583805605 * V_1 = NULL;
	fsResult_t862419890  V_2;
	memset(&V_2, 0, sizeof(V_2));
	RuntimeObject * V_3 = NULL;
	Exception_t1927440687 * V_4 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	OnGetToneAnalyzed_t3813655652 * G_B10_0 = NULL;
	OnGetToneAnalyzed_t3813655652 * G_B9_0 = NULL;
	ToneAnalyzerResponse_t3391014235 * G_B11_0 = NULL;
	OnGetToneAnalyzed_t3813655652 * G_B11_1 = NULL;
	{
		ToneAnalyzerResponse_t3391014235 * L_0 = (ToneAnalyzerResponse_t3391014235 *)il2cpp_codegen_object_new(ToneAnalyzerResponse_t3391014235_il2cpp_TypeInfo_var);
		ToneAnalyzerResponse__ctor_m604375646(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Response_t429319368 * L_1 = ___resp1;
		NullCheck(L_1);
		bool L_2 = Response_get_Success_m2313267345(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_00a5;
		}
	}

IL_0011:
	try
	{ // begin try (depth: 1)
		{
			V_1 = (fsData_t2583805605 *)NULL;
			IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
			Encoding_t663144255 * L_3 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
			Response_t429319368 * L_4 = ___resp1;
			NullCheck(L_4);
			ByteU5BU5D_t3397334013* L_5 = Response_get_Data_m3078004018(L_4, /*hidden argument*/NULL);
			NullCheck(L_3);
			String_t* L_6 = VirtFuncInvoker1< String_t*, ByteU5BU5D_t3397334013* >::Invoke(22 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_3, L_5);
			fsResult_t862419890  L_7 = fsJsonParser_Parse_m4200302395(NULL /*static, unused*/, L_6, (&V_1), /*hidden argument*/NULL);
			V_2 = L_7;
			bool L_8 = fsResult_get_Succeeded_m3237049006((&V_2), /*hidden argument*/NULL);
			if (L_8)
			{
				goto IL_0044;
			}
		}

IL_0037:
		{
			String_t* L_9 = fsResult_get_FormattedMessages_m3414202484((&V_2), /*hidden argument*/NULL);
			WatsonException_t1186470237 * L_10 = (WatsonException_t1186470237 *)il2cpp_codegen_object_new(WatsonException_t1186470237_il2cpp_TypeInfo_var);
			WatsonException__ctor_m3825115789(L_10, L_9, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_10);
		}

IL_0044:
		{
			ToneAnalyzerResponse_t3391014235 * L_11 = V_0;
			V_3 = L_11;
			IL2CPP_RUNTIME_CLASS_INIT(ToneAnalyzer_t1356110496_il2cpp_TypeInfo_var);
			fsSerializer_t4193731081 * L_12 = ((ToneAnalyzer_t1356110496_StaticFields*)il2cpp_codegen_static_fields_for(ToneAnalyzer_t1356110496_il2cpp_TypeInfo_var))->get_sm_Serializer_1();
			fsData_t2583805605 * L_13 = V_1;
			RuntimeObject * L_14 = V_3;
			NullCheck(L_14);
			Type_t * L_15 = Object_GetType_m191970594(L_14, /*hidden argument*/NULL);
			NullCheck(L_12);
			fsResult_t862419890  L_16 = fsSerializer_TryDeserialize_m4134098758(L_12, L_13, L_15, (&V_3), /*hidden argument*/NULL);
			V_2 = L_16;
			bool L_17 = fsResult_get_Succeeded_m3237049006((&V_2), /*hidden argument*/NULL);
			if (L_17)
			{
				goto IL_0073;
			}
		}

IL_0066:
		{
			String_t* L_18 = fsResult_get_FormattedMessages_m3414202484((&V_2), /*hidden argument*/NULL);
			WatsonException_t1186470237 * L_19 = (WatsonException_t1186470237 *)il2cpp_codegen_object_new(WatsonException_t1186470237_il2cpp_TypeInfo_var);
			WatsonException__ctor_m3825115789(L_19, L_18, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_19);
		}

IL_0073:
		{
			goto IL_00a5;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1927440687_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0078;
		throw e;
	}

CATCH_0078:
	{ // begin catch(System.Exception)
		V_4 = ((Exception_t1927440687 *)__exception_local);
		ObjectU5BU5D_t3614634134* L_20 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1));
		Exception_t1927440687 * L_21 = V_4;
		NullCheck(L_21);
		String_t* L_22 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_21);
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, L_22);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_22);
		Log_Error_m685270089(NULL /*static, unused*/, _stringLiteral2389966620, _stringLiteral435905614, L_20, /*hidden argument*/NULL);
		Response_t429319368 * L_23 = ___resp1;
		NullCheck(L_23);
		Response_set_Success_m3746673474(L_23, (bool)0, /*hidden argument*/NULL);
		goto IL_00a5;
	} // end catch (depth: 1)

IL_00a5:
	{
		Request_t466816980 * L_24 = ___req0;
		NullCheck(((GetToneAnalyzerRequest_t1769916618 *)CastclassClass((RuntimeObject*)L_24, GetToneAnalyzerRequest_t1769916618_il2cpp_TypeInfo_var)));
		OnGetToneAnalyzed_t3813655652 * L_25 = GetToneAnalyzerRequest_get_Callback_m3110919162(((GetToneAnalyzerRequest_t1769916618 *)CastclassClass((RuntimeObject*)L_24, GetToneAnalyzerRequest_t1769916618_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00e2;
		}
	}
	{
		Request_t466816980 * L_26 = ___req0;
		NullCheck(((GetToneAnalyzerRequest_t1769916618 *)CastclassClass((RuntimeObject*)L_26, GetToneAnalyzerRequest_t1769916618_il2cpp_TypeInfo_var)));
		OnGetToneAnalyzed_t3813655652 * L_27 = GetToneAnalyzerRequest_get_Callback_m3110919162(((GetToneAnalyzerRequest_t1769916618 *)CastclassClass((RuntimeObject*)L_26, GetToneAnalyzerRequest_t1769916618_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		Response_t429319368 * L_28 = ___resp1;
		NullCheck(L_28);
		bool L_29 = Response_get_Success_m2313267345(L_28, /*hidden argument*/NULL);
		G_B9_0 = L_27;
		if (!L_29)
		{
			G_B10_0 = L_27;
			goto IL_00d1;
		}
	}
	{
		ToneAnalyzerResponse_t3391014235 * L_30 = V_0;
		G_B11_0 = L_30;
		G_B11_1 = G_B9_0;
		goto IL_00d2;
	}

IL_00d1:
	{
		G_B11_0 = ((ToneAnalyzerResponse_t3391014235 *)(NULL));
		G_B11_1 = G_B10_0;
	}

IL_00d2:
	{
		Request_t466816980 * L_31 = ___req0;
		NullCheck(((GetToneAnalyzerRequest_t1769916618 *)CastclassClass((RuntimeObject*)L_31, GetToneAnalyzerRequest_t1769916618_il2cpp_TypeInfo_var)));
		String_t* L_32 = GetToneAnalyzerRequest_get_Data_m4150767635(((GetToneAnalyzerRequest_t1769916618 *)CastclassClass((RuntimeObject*)L_31, GetToneAnalyzerRequest_t1769916618_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		NullCheck(G_B11_1);
		OnGetToneAnalyzed_Invoke_m316833546(G_B11_1, G_B11_0, L_32, /*hidden argument*/NULL);
	}

IL_00e2:
	{
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzer::GetServiceID()
extern "C"  String_t* ToneAnalyzer_GetServiceID_m3304906614 (ToneAnalyzer_t1356110496 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ToneAnalyzer_GetServiceID_m3304906614_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		return _stringLiteral988188995;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzer::GetServiceStatus(IBM.Watson.DeveloperCloud.Services.ServiceStatus)
extern "C"  void ToneAnalyzer_GetServiceStatus_m2690870132 (ToneAnalyzer_t1356110496 * __this, ServiceStatus_t1443707987 * ___callback0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ToneAnalyzer_GetServiceStatus_m2690870132_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Config_t3637807320_il2cpp_TypeInfo_var);
		Config_t3637807320 * L_0 = Config_get_Instance_m430784956(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		CredentialInfo_t34154441 * L_1 = Config_FindCredentials_m3250710905(L_0, _stringLiteral988188995, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		ServiceStatus_t1443707987 * L_2 = ___callback0;
		CheckServiceStatus_t1108037484 * L_3 = (CheckServiceStatus_t1108037484 *)il2cpp_codegen_object_new(CheckServiceStatus_t1108037484_il2cpp_TypeInfo_var);
		CheckServiceStatus__ctor_m57170065(L_3, __this, L_2, /*hidden argument*/NULL);
		goto IL_002d;
	}

IL_0021:
	{
		ServiceStatus_t1443707987 * L_4 = ___callback0;
		NullCheck(L_4);
		ServiceStatus_Invoke_m1869726628(L_4, _stringLiteral988188995, (bool)0, /*hidden argument*/NULL);
	}

IL_002d:
	{
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzer::.cctor()
extern "C"  void ToneAnalyzer__cctor_m4035307958 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ToneAnalyzer__cctor_m4035307958_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		fsSerializer_t4193731081 * L_0 = (fsSerializer_t4193731081 *)il2cpp_codegen_object_new(fsSerializer_t4193731081_il2cpp_TypeInfo_var);
		fsSerializer__ctor_m3368490715(L_0, /*hidden argument*/NULL);
		((ToneAnalyzer_t1356110496_StaticFields*)il2cpp_codegen_static_fields_for(ToneAnalyzer_t1356110496_il2cpp_TypeInfo_var))->set_sm_Serializer_1(L_0);
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzer/CheckServiceStatus::.ctor(IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzer,IBM.Watson.DeveloperCloud.Services.ServiceStatus)
extern "C"  void CheckServiceStatus__ctor_m57170065 (CheckServiceStatus_t1108037484 * __this, ToneAnalyzer_t1356110496 * ___service0, ServiceStatus_t1443707987 * ___callback1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CheckServiceStatus__ctor_m57170065_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		ToneAnalyzer_t1356110496 * L_0 = ___service0;
		__this->set_m_Service_0(L_0);
		ServiceStatus_t1443707987 * L_1 = ___callback1;
		__this->set_m_Callback_1(L_1);
		ToneAnalyzer_t1356110496 * L_2 = __this->get_m_Service_0();
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)CheckServiceStatus_OnGetToneAnalyzed_m3084319673_RuntimeMethod_var);
		OnGetToneAnalyzed_t3813655652 * L_4 = (OnGetToneAnalyzed_t3813655652 *)il2cpp_codegen_object_new(OnGetToneAnalyzed_t3813655652_il2cpp_TypeInfo_var);
		OnGetToneAnalyzed__ctor_m3782744621(L_4, __this, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		bool L_5 = ToneAnalyzer_GetToneAnalyze_m2599887229(L_2, L_4, _stringLiteral3423762534, (String_t*)NULL, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0047;
		}
	}
	{
		ServiceStatus_t1443707987 * L_6 = __this->get_m_Callback_1();
		NullCheck(L_6);
		ServiceStatus_Invoke_m1869726628(L_6, _stringLiteral988188995, (bool)0, /*hidden argument*/NULL);
	}

IL_0047:
	{
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzer/CheckServiceStatus::OnGetToneAnalyzed(IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzerResponse,System.String)
extern "C"  void CheckServiceStatus_OnGetToneAnalyzed_m3084319673 (CheckServiceStatus_t1108037484 * __this, ToneAnalyzerResponse_t3391014235 * ___resp0, String_t* ___data1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CheckServiceStatus_OnGetToneAnalyzed_m3084319673_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ServiceStatus_t1443707987 * L_0 = __this->get_m_Callback_1();
		if (!L_0)
		{
			goto IL_0022;
		}
	}
	{
		ServiceStatus_t1443707987 * L_1 = __this->get_m_Callback_1();
		ToneAnalyzerResponse_t3391014235 * L_2 = ___resp0;
		NullCheck(L_1);
		ServiceStatus_Invoke_m1869726628(L_1, _stringLiteral988188995, (bool)((((int32_t)((((RuntimeObject*)(ToneAnalyzerResponse_t3391014235 *)L_2) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
	}

IL_0022:
	{
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzer/GetToneAnalyzerRequest::.ctor()
extern "C"  void GetToneAnalyzerRequest__ctor_m3143334739 (GetToneAnalyzerRequest_t1769916618 * __this, const RuntimeMethod* method)
{
	{
		Request__ctor_m1928448681(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzer/GetToneAnalyzerRequest::get_Data()
extern "C"  String_t* GetToneAnalyzerRequest_get_Data_m4150767635 (GetToneAnalyzerRequest_t1769916618 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CDataU3Ek__BackingField_11();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzer/GetToneAnalyzerRequest::set_Data(System.String)
extern "C"  void GetToneAnalyzerRequest_set_Data_m3436767482 (GetToneAnalyzerRequest_t1769916618 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CDataU3Ek__BackingField_11(L_0);
		return;
	}
}
// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzer/OnGetToneAnalyzed IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzer/GetToneAnalyzerRequest::get_Callback()
extern "C"  OnGetToneAnalyzed_t3813655652 * GetToneAnalyzerRequest_get_Callback_m3110919162 (GetToneAnalyzerRequest_t1769916618 * __this, const RuntimeMethod* method)
{
	{
		OnGetToneAnalyzed_t3813655652 * L_0 = __this->get_U3CCallbackU3Ek__BackingField_12();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzer/GetToneAnalyzerRequest::set_Callback(IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzer/OnGetToneAnalyzed)
extern "C"  void GetToneAnalyzerRequest_set_Callback_m1849301549 (GetToneAnalyzerRequest_t1769916618 * __this, OnGetToneAnalyzed_t3813655652 * ___value0, const RuntimeMethod* method)
{
	{
		OnGetToneAnalyzed_t3813655652 * L_0 = ___value0;
		__this->set_U3CCallbackU3Ek__BackingField_12(L_0);
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzer/OnGetToneAnalyzed::.ctor(System.Object,System.IntPtr)
extern "C"  void OnGetToneAnalyzed__ctor_m3782744621 (OnGetToneAnalyzed_t3813655652 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzer/OnGetToneAnalyzed::Invoke(IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzerResponse,System.String)
extern "C"  void OnGetToneAnalyzed_Invoke_m316833546 (OnGetToneAnalyzed_t3813655652 * __this, ToneAnalyzerResponse_t3391014235 * ___resp0, String_t* ___data1, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		OnGetToneAnalyzed_Invoke_m316833546((OnGetToneAnalyzed_t3813655652 *)__this->get_prev_9(),___resp0, ___data1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, ToneAnalyzerResponse_t3391014235 * ___resp0, String_t* ___data1, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___resp0, ___data1,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, ToneAnalyzerResponse_t3391014235 * ___resp0, String_t* ___data1, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___resp0, ___data1,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___data1, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___resp0, ___data1,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzer/OnGetToneAnalyzed::BeginInvoke(IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzerResponse,System.String,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* OnGetToneAnalyzed_BeginInvoke_m3489740613 (OnGetToneAnalyzed_t3813655652 * __this, ToneAnalyzerResponse_t3391014235 * ___resp0, String_t* ___data1, AsyncCallback_t163412349 * ___callback2, RuntimeObject * ___object3, const RuntimeMethod* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___resp0;
	__d_args[1] = ___data1;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (RuntimeObject*)___object3);
}
// System.Void IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzer/OnGetToneAnalyzed::EndInvoke(System.IAsyncResult)
extern "C"  void OnGetToneAnalyzed_EndInvoke_m3464792823 (OnGetToneAnalyzed_t3813655652 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzerResponse::.ctor()
extern "C"  void ToneAnalyzerResponse__ctor_m604375646 (ToneAnalyzerResponse_t3391014235 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.DocumentTone IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzerResponse::get_document_tone()
extern "C"  DocumentTone_t2537937785 * ToneAnalyzerResponse_get_document_tone_m350112175 (ToneAnalyzerResponse_t3391014235 * __this, const RuntimeMethod* method)
{
	{
		DocumentTone_t2537937785 * L_0 = __this->get_U3Cdocument_toneU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzerResponse::set_document_tone(IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.DocumentTone)
extern "C"  void ToneAnalyzerResponse_set_document_tone_m199900698 (ToneAnalyzerResponse_t3391014235 * __this, DocumentTone_t2537937785 * ___value0, const RuntimeMethod* method)
{
	{
		DocumentTone_t2537937785 * L_0 = ___value0;
		__this->set_U3Cdocument_toneU3Ek__BackingField_0(L_0);
		return;
	}
}
// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.SentenceTone[] IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzerResponse::get_sentences_tone()
extern "C"  SentenceToneU5BU5D_t255016994* ToneAnalyzerResponse_get_sentences_tone_m1788818906 (ToneAnalyzerResponse_t3391014235 * __this, const RuntimeMethod* method)
{
	{
		SentenceToneU5BU5D_t255016994* L_0 = __this->get_U3Csentences_toneU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzerResponse::set_sentences_tone(IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.SentenceTone[])
extern "C"  void ToneAnalyzerResponse_set_sentences_tone_m2072860057 (ToneAnalyzerResponse_t3391014235 * __this, SentenceToneU5BU5D_t255016994* ___value0, const RuntimeMethod* method)
{
	{
		SentenceToneU5BU5D_t255016994* L_0 = ___value0;
		__this->set_U3Csentences_toneU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzerResponse::ToString()
extern "C"  String_t* ToneAnalyzerResponse_ToString_m3908036033 (ToneAnalyzerResponse_t3391014235 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ToneAnalyzerResponse_ToString_m3908036033_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* G_B4_0 = NULL;
	{
		SentenceToneU5BU5D_t255016994* L_0 = ToneAnalyzerResponse_get_sentences_tone_m1788818906(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_002b;
		}
	}
	{
		SentenceToneU5BU5D_t255016994* L_1 = ToneAnalyzerResponse_get_sentences_tone_m1788818906(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		if ((((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_1)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_002b;
		}
	}
	{
		SentenceToneU5BU5D_t255016994* L_2 = ToneAnalyzerResponse_get_sentences_tone_m1788818906(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = 0;
		SentenceTone_t3234952115 * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		NullCheck(L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_4);
		G_B4_0 = L_5;
		goto IL_0030;
	}

IL_002b:
	{
		G_B4_0 = _stringLiteral1688738322;
	}

IL_0030:
	{
		V_0 = G_B4_0;
		DocumentTone_t2537937785 * L_6 = ToneAnalyzerResponse_get_document_tone_m350112175(__this, /*hidden argument*/NULL);
		String_t* L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Format_m1811873526(NULL /*static, unused*/, _stringLiteral164989177, L_6, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneCategory::.ctor()
extern "C"  void ToneCategory__ctor_m3301130311 (ToneCategory_t3724297374 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneCategory::get_category_id()
extern "C"  String_t* ToneCategory_get_category_id_m1048041785 (ToneCategory_t3724297374 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3Ccategory_idU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneCategory::set_category_id(System.String)
extern "C"  void ToneCategory_set_category_id_m3636196020 (ToneCategory_t3724297374 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3Ccategory_idU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneCategory::get_category_name()
extern "C"  String_t* ToneCategory_get_category_name_m1377209155 (ToneCategory_t3724297374 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3Ccategory_nameU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneCategory::set_category_name(System.String)
extern "C"  void ToneCategory_set_category_name_m275322330 (ToneCategory_t3724297374 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3Ccategory_nameU3Ek__BackingField_1(L_0);
		return;
	}
}
// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.Tone[] IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneCategory::get_tones()
extern "C"  ToneU5BU5D_t4171600311* ToneCategory_get_tones_m2345877352 (ToneCategory_t3724297374 * __this, const RuntimeMethod* method)
{
	{
		ToneU5BU5D_t4171600311* L_0 = __this->get_U3CtonesU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneCategory::set_tones(IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.Tone[])
extern "C"  void ToneCategory_set_tones_m3415244035 (ToneCategory_t3724297374 * __this, ToneU5BU5D_t4171600311* ___value0, const RuntimeMethod* method)
{
	{
		ToneU5BU5D_t4171600311* L_0 = ___value0;
		__this->set_U3CtonesU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneCategory::ToString()
extern "C"  String_t* ToneCategory_ToString_m2915677366 (ToneCategory_t3724297374 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ToneCategory_ToString_m2915677366_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		V_0 = L_0;
		V_1 = 0;
		goto IL_0025;
	}

IL_000d:
	{
		String_t* L_1 = V_0;
		ToneU5BU5D_t4171600311* L_2 = ToneCategory_get_tones_m2345877352(__this, /*hidden argument*/NULL);
		int32_t L_3 = V_1;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		Tone_t1138009090 * L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		NullCheck(L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_5);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m2596409543(NULL /*static, unused*/, L_1, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		int32_t L_8 = V_1;
		V_1 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0025:
	{
		ToneU5BU5D_t4171600311* L_9 = ToneCategory_get_tones_m2345877352(__this, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_003e;
		}
	}
	{
		int32_t L_10 = V_1;
		ToneU5BU5D_t4171600311* L_11 = ToneCategory_get_tones_m2345877352(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_11)->max_length)))))))
		{
			goto IL_000d;
		}
	}

IL_003e:
	{
		String_t* L_12 = ToneCategory_get_category_id_m1048041785(__this, /*hidden argument*/NULL);
		String_t* L_13 = ToneCategory_get_category_name_m1377209155(__this, /*hidden argument*/NULL);
		String_t* L_14 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = String_Format_m4262916296(NULL /*static, unused*/, _stringLiteral2235585657, L_12, L_13, L_14, /*hidden argument*/NULL);
		return L_15;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Anchor::.ctor()
extern "C"  void Anchor__ctor_m3047131185 (Anchor_t1470404909 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Anchor::get_name()
extern "C"  String_t* Anchor_get_name_m761723720 (Anchor_t1470404909 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CnameU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Anchor::set_name(System.String)
extern "C"  void Anchor_set_name_m3678524289 (Anchor_t1470404909 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CnameU3Ek__BackingField_0(L_0);
		return;
	}
}
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Position IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Anchor::get_position()
extern "C"  Position_t3597491459 * Anchor_get_position_m2142122472 (Anchor_t1470404909 * __this, const RuntimeMethod* method)
{
	{
		Position_t3597491459 * L_0 = __this->get_U3CpositionU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Anchor::set_position(IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Position)
extern "C"  void Anchor_set_position_m3631138119 (Anchor_t1470404909 * __this, Position_t3597491459 * ___value0, const RuntimeMethod* method)
{
	{
		Position_t3597491459 * L_0 = ___value0;
		__this->set_U3CpositionU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.ApplicationData::.ctor()
extern "C"  void ApplicationData__ctor_m1879024432 (ApplicationData_t1885408412 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.ApplicationDataValue::.ctor()
extern "C"  void ApplicationDataValue__ctor_m4048002801 (ApplicationDataValue_t1721671971 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.CategoricalRange::.ctor()
extern "C"  void CategoricalRange__ctor_m2900890557 (CategoricalRange_t943840333 * __this, const RuntimeMethod* method)
{
	{
		Range__ctor_m3717125291(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String[] IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.CategoricalRange::get_keys()
extern "C"  StringU5BU5D_t1642385972* CategoricalRange_get_keys_m117167839 (CategoricalRange_t943840333 * __this, const RuntimeMethod* method)
{
	{
		StringU5BU5D_t1642385972* L_0 = __this->get_U3CkeysU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.CategoricalRange::set_keys(System.String[])
extern "C"  void CategoricalRange_set_keys_m1178974960 (CategoricalRange_t943840333 * __this, StringU5BU5D_t1642385972* ___value0, const RuntimeMethod* method)
{
	{
		StringU5BU5D_t1642385972* L_0 = ___value0;
		__this->set_U3CkeysU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column::.ctor()
extern "C"  void Column__ctor_m3168083222 (Column_t2612486824 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column::get_key()
extern "C"  String_t* Column_get_key_m1340004237 (Column_t2612486824 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CkeyU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column::set_key(System.String)
extern "C"  void Column_set_key_m3783075932 (Column_t2612486824 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CkeyU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column::get_type()
extern "C"  String_t* Column_get_type_m2432028040 (Column_t2612486824 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CtypeU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column::set_type(System.String)
extern "C"  void Column_set_type_m3872918225 (Column_t2612486824 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CtypeU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column::get_description()
extern "C"  String_t* Column_get_description_m3145848568 (Column_t2612486824 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CdescriptionU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column::set_description(System.String)
extern "C"  void Column_set_description_m4195074823 (Column_t2612486824 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CdescriptionU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column::get_format()
extern "C"  String_t* Column_get_format_m74498709 (Column_t2612486824 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CformatU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column::set_format(System.String)
extern "C"  void Column_set_format_m4115467280 (Column_t2612486824 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CformatU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column::get_full_name()
extern "C"  String_t* Column_get_full_name_m3808814225 (Column_t2612486824 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3Cfull_nameU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column::set_full_name(System.String)
extern "C"  void Column_set_full_name_m409965306 (Column_t2612486824 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3Cfull_nameU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column::get_goal()
extern "C"  String_t* Column_get_goal_m1933291103 (Column_t2612486824 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CgoalU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column::set_goal(System.String)
extern "C"  void Column_set_goal_m1264124692 (Column_t2612486824 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CgoalU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.Int32 IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column::get_insignificant_loss()
extern "C"  int32_t Column_get_insignificant_loss_m1027757927 (Column_t2612486824 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_U3Cinsignificant_lossU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column::set_insignificant_loss(System.Int32)
extern "C"  void Column_set_insignificant_loss_m263362664 (Column_t2612486824 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3Cinsignificant_lossU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.Boolean IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column::get_is_objective()
extern "C"  bool Column_get_is_objective_m3746779281 (Column_t2612486824 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_U3Cis_objectiveU3Ek__BackingField_7();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column::set_is_objective(System.Boolean)
extern "C"  void Column_set_is_objective_m3607883288 (Column_t2612486824 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3Cis_objectiveU3Ek__BackingField_7(L_0);
		return;
	}
}
// System.String[] IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column::get_preference()
extern "C"  StringU5BU5D_t1642385972* Column_get_preference_m3777531147 (Column_t2612486824 * __this, const RuntimeMethod* method)
{
	{
		StringU5BU5D_t1642385972* L_0 = __this->get_U3CpreferenceU3Ek__BackingField_8();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column::set_preference(System.String[])
extern "C"  void Column_set_preference_m2402251550 (Column_t2612486824 * __this, StringU5BU5D_t1642385972* ___value0, const RuntimeMethod* method)
{
	{
		StringU5BU5D_t1642385972* L_0 = ___value0;
		__this->set_U3CpreferenceU3Ek__BackingField_8(L_0);
		return;
	}
}
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Range IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column::get_range()
extern "C"  Range_t2396059803 * Column_get_range_m2413620963 (Column_t2612486824 * __this, const RuntimeMethod* method)
{
	{
		Range_t2396059803 * L_0 = __this->get_U3CrangeU3Ek__BackingField_9();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column::set_range(IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Range)
extern "C"  void Column_set_range_m3088237292 (Column_t2612486824 * __this, Range_t2396059803 * ___value0, const RuntimeMethod* method)
{
	{
		Range_t2396059803 * L_0 = ___value0;
		__this->set_U3CrangeU3Ek__BackingField_9(L_0);
		return;
	}
}
// System.Int64 IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column::get_significant_gain()
extern "C"  int64_t Column_get_significant_gain_m4113871389 (Column_t2612486824 * __this, const RuntimeMethod* method)
{
	{
		int64_t L_0 = __this->get_U3Csignificant_gainU3Ek__BackingField_10();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column::set_significant_gain(System.Int64)
extern "C"  void Column_set_significant_gain_m4294272822 (Column_t2612486824 * __this, int64_t ___value0, const RuntimeMethod* method)
{
	{
		int64_t L_0 = ___value0;
		__this->set_U3Csignificant_gainU3Ek__BackingField_10(L_0);
		return;
	}
}
// System.Int64 IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column::get_significant_loss()
extern "C"  int64_t Column_get_significant_loss_m158498039 (Column_t2612486824 * __this, const RuntimeMethod* method)
{
	{
		int64_t L_0 = __this->get_U3Csignificant_lossU3Ek__BackingField_11();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column::set_significant_loss(System.Int64)
extern "C"  void Column_set_significant_loss_m1449268454 (Column_t2612486824 * __this, int64_t ___value0, const RuntimeMethod* method)
{
	{
		int64_t L_0 = ___value0;
		__this->set_U3Csignificant_lossU3Ek__BackingField_11(L_0);
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Config::.ctor()
extern "C"  void Config__ctor_m1708323370 (Config_t1927135816 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Drivers IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Config::get_drivers()
extern "C"  Drivers_t1652706281 * Config_get_drivers_m799056583 (Config_t1927135816 * __this, const RuntimeMethod* method)
{
	{
		Drivers_t1652706281 * L_0 = __this->get_U3CdriversU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Config::set_drivers(IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Drivers)
extern "C"  void Config_set_drivers_m3860367284 (Config_t1927135816 * __this, Drivers_t1652706281 * ___value0, const RuntimeMethod* method)
{
	{
		Drivers_t1652706281 * L_0 = ___value0;
		__this->set_U3CdriversU3Ek__BackingField_0(L_0);
		return;
	}
}
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Params IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Config::get_params()
extern "C"  Params_t3159428742 * Config_get_params_m2841026407 (Config_t1927135816 * __this, const RuntimeMethod* method)
{
	{
		Params_t3159428742 * L_0 = __this->get_U3CparamsU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Config::set_params(IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Params)
extern "C"  void Config_set_params_m851614630 (Config_t1927135816 * __this, Params_t3159428742 * ___value0, const RuntimeMethod* method)
{
	{
		Params_t3159428742 * L_0 = ___value0;
		__this->set_U3CparamsU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.DateRange::.ctor()
extern "C"  void DateRange__ctor_m3223304975 (DateRange_t3049499159 * __this, const RuntimeMethod* method)
{
	{
		Range__ctor_m3717125291(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.DateRange::get_low()
extern "C"  String_t* DateRange_get_low_m206432871 (DateRange_t3049499159 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3ClowU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.DateRange::set_low(System.String)
extern "C"  void DateRange_set_low_m2164092042 (DateRange_t3049499159 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3ClowU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.DateRange::get_high()
extern "C"  String_t* DateRange_get_high_m2152987925 (DateRange_t3049499159 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3ChighU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.DateRange::set_high(System.String)
extern "C"  void DateRange_set_high_m3007518322 (DateRange_t3049499159 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3ChighU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.DilemmasResponse::.ctor()
extern "C"  void DilemmasResponse__ctor_m2982690117 (DilemmasResponse_t3767705817 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Problem IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.DilemmasResponse::get_problem()
extern "C"  Problem_t2814813345 * DilemmasResponse_get_problem_m1689384496 (DilemmasResponse_t3767705817 * __this, const RuntimeMethod* method)
{
	{
		Problem_t2814813345 * L_0 = __this->get_U3CproblemU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.DilemmasResponse::set_problem(IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Problem)
extern "C"  void DilemmasResponse_set_problem_m652037195 (DilemmasResponse_t3767705817 * __this, Problem_t2814813345 * ___value0, const RuntimeMethod* method)
{
	{
		Problem_t2814813345 * L_0 = ___value0;
		__this->set_U3CproblemU3Ek__BackingField_0(L_0);
		return;
	}
}
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Resolution IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.DilemmasResponse::get_resolution()
extern "C"  Resolution_t3867497320 * DilemmasResponse_get_resolution_m113114478 (DilemmasResponse_t3767705817 * __this, const RuntimeMethod* method)
{
	{
		Resolution_t3867497320 * L_0 = __this->get_U3CresolutionU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.DilemmasResponse::set_resolution(IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Resolution)
extern "C"  void DilemmasResponse_set_resolution_m3309505007 (DilemmasResponse_t3767705817 * __this, Resolution_t3867497320 * ___value0, const RuntimeMethod* method)
{
	{
		Resolution_t3867497320 * L_0 = ___value0;
		__this->set_U3CresolutionU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Drivers::.ctor()
extern "C"  void Drivers__ctor_m1121216445 (Drivers_t1652706281 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Double IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Drivers::get_alpha_init()
extern "C"  double Drivers_get_alpha_init_m1404732098 (Drivers_t1652706281 * __this, const RuntimeMethod* method)
{
	{
		double L_0 = __this->get_U3Calpha_initU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Drivers::set_alpha_init(System.Double)
extern "C"  void Drivers_set_alpha_init_m527343679 (Drivers_t1652706281 * __this, double ___value0, const RuntimeMethod* method)
{
	{
		double L_0 = ___value0;
		__this->set_U3Calpha_initU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Double IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Drivers::get_data_multiplier()
extern "C"  double Drivers_get_data_multiplier_m2848607045 (Drivers_t1652706281 * __this, const RuntimeMethod* method)
{
	{
		double L_0 = __this->get_U3Cdata_multiplierU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Drivers::set_data_multiplier(System.Double)
extern "C"  void Drivers_set_data_multiplier_m4251386108 (Drivers_t1652706281 * __this, double ___value0, const RuntimeMethod* method)
{
	{
		double L_0 = ___value0;
		__this->set_U3Cdata_multiplierU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Double IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Drivers::get_max_map_size()
extern "C"  double Drivers_get_max_map_size_m3058795832 (Drivers_t1652706281 * __this, const RuntimeMethod* method)
{
	{
		double L_0 = __this->get_U3Cmax_map_sizeU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Drivers::set_max_map_size(System.Double)
extern "C"  void Drivers_set_max_map_size_m977578599 (Drivers_t1652706281 * __this, double ___value0, const RuntimeMethod* method)
{
	{
		double L_0 = ___value0;
		__this->set_U3Cmax_map_sizeU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Double IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Drivers::get_r_anchor_init()
extern "C"  double Drivers_get_r_anchor_init_m2704326784 (Drivers_t1652706281 * __this, const RuntimeMethod* method)
{
	{
		double L_0 = __this->get_U3Cr_anchor_initU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Drivers::set_r_anchor_init(System.Double)
extern "C"  void Drivers_set_r_anchor_init_m2014850323 (Drivers_t1652706281 * __this, double ___value0, const RuntimeMethod* method)
{
	{
		double L_0 = ___value0;
		__this->set_U3Cr_anchor_initU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Double IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Drivers::get_r_fin()
extern "C"  double Drivers_get_r_fin_m690785741 (Drivers_t1652706281 * __this, const RuntimeMethod* method)
{
	{
		double L_0 = __this->get_U3Cr_finU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Drivers::set_r_fin(System.Double)
extern "C"  void Drivers_set_r_fin_m1511912662 (Drivers_t1652706281 * __this, double ___value0, const RuntimeMethod* method)
{
	{
		double L_0 = ___value0;
		__this->set_U3Cr_finU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Double IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Drivers::get_r_init()
extern "C"  double Drivers_get_r_init_m3251749436 (Drivers_t1652706281 * __this, const RuntimeMethod* method)
{
	{
		double L_0 = __this->get_U3Cr_initU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Drivers::set_r_init(System.Double)
extern "C"  void Drivers_set_r_init_m3929733645 (Drivers_t1652706281 * __this, double ___value0, const RuntimeMethod* method)
{
	{
		double L_0 = ___value0;
		__this->set_U3Cr_initU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.Double IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Drivers::get_training_anchors()
extern "C"  double Drivers_get_training_anchors_m408614170 (Drivers_t1652706281 * __this, const RuntimeMethod* method)
{
	{
		double L_0 = __this->get_U3Ctraining_anchorsU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Drivers::set_training_anchors(System.Double)
extern "C"  void Drivers_set_training_anchors_m2713683899 (Drivers_t1652706281 * __this, double ___value0, const RuntimeMethod* method)
{
	{
		double L_0 = ___value0;
		__this->set_U3Ctraining_anchorsU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.Double IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Drivers::get_training_length()
extern "C"  double Drivers_get_training_length_m3081825994 (Drivers_t1652706281 * __this, const RuntimeMethod* method)
{
	{
		double L_0 = __this->get_U3Ctraining_lengthU3Ek__BackingField_7();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Drivers::set_training_length(System.Double)
extern "C"  void Drivers_set_training_length_m3382187309 (Drivers_t1652706281 * __this, double ___value0, const RuntimeMethod* method)
{
	{
		double L_0 = ___value0;
		__this->set_U3Ctraining_lengthU3Ek__BackingField_7(L_0);
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Map::.ctor()
extern "C"  void Map__ctor_m813477900 (Map_t1914475838 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Anchor[] IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Map::get_anchors()
extern "C"  AnchorU5BU5D_t2762965888* Map_get_anchors_m3004979918 (Map_t1914475838 * __this, const RuntimeMethod* method)
{
	{
		AnchorU5BU5D_t2762965888* L_0 = __this->get_U3CanchorsU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Map::set_anchors(IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Anchor[])
extern "C"  void Map_set_anchors_m2647286603 (Map_t1914475838 * __this, AnchorU5BU5D_t2762965888* ___value0, const RuntimeMethod* method)
{
	{
		AnchorU5BU5D_t2762965888* L_0 = ___value0;
		__this->set_U3CanchorsU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Map::get_comments()
extern "C"  String_t* Map_get_comments_m239145382 (Map_t1914475838 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CcommentsU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Map::set_comments(System.String)
extern "C"  void Map_set_comments_m4293098887 (Map_t1914475838 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CcommentsU3Ek__BackingField_1(L_0);
		return;
	}
}
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Config IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Map::get_config()
extern "C"  Config_t1927135816 * Map_get_config_m4034032785 (Map_t1914475838 * __this, const RuntimeMethod* method)
{
	{
		Config_t1927135816 * L_0 = __this->get_U3CconfigU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Map::set_config(IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Config)
extern "C"  void Map_set_config_m1422359504 (Map_t1914475838 * __this, Config_t1927135816 * ___value0, const RuntimeMethod* method)
{
	{
		Config_t1927135816 * L_0 = ___value0;
		__this->set_U3CconfigU3Ek__BackingField_2(L_0);
		return;
	}
}
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Node[] IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Map::get_nodes()
extern "C"  NodeU5BU5D_t283985611* Map_get_nodes_m2576771502 (Map_t1914475838 * __this, const RuntimeMethod* method)
{
	{
		NodeU5BU5D_t283985611* L_0 = __this->get_U3CnodesU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Map::set_nodes(IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Node[])
extern "C"  void Map_set_nodes_m2102257355 (Map_t1914475838 * __this, NodeU5BU5D_t283985611* ___value0, const RuntimeMethod* method)
{
	{
		NodeU5BU5D_t283985611* L_0 = ___value0;
		__this->set_U3CnodesU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Metrics::.ctor()
extern "C"  void Metrics__ctor_m2148879595 (Metrics_t236865991 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Double IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Metrics::get_final_kappa()
extern "C"  double Metrics_get_final_kappa_m1369945139 (Metrics_t236865991 * __this, const RuntimeMethod* method)
{
	{
		double L_0 = __this->get_U3Cfinal_kappaU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Metrics::set_final_kappa(System.Double)
extern "C"  void Metrics_set_final_kappa_m804593332 (Metrics_t236865991 * __this, double ___value0, const RuntimeMethod* method)
{
	{
		double L_0 = ___value0;
		__this->set_U3Cfinal_kappaU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Double IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Metrics::get_kappa()
extern "C"  double Metrics_get_kappa_m1773965580 (Metrics_t236865991 * __this, const RuntimeMethod* method)
{
	{
		double L_0 = __this->get_U3CkappaU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Metrics::set_kappa(System.Double)
extern "C"  void Metrics_set_kappa_m1750742299 (Metrics_t236865991 * __this, double ___value0, const RuntimeMethod* method)
{
	{
		double L_0 = ___value0;
		__this->set_U3CkappaU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Node::.ctor()
extern "C"  void Node__ctor_m2547547340 (Node_t3986951550 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Position IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Node::get_coordinates()
extern "C"  Position_t3597491459 * Node_get_coordinates_m1626699361 (Node_t3986951550 * __this, const RuntimeMethod* method)
{
	{
		Position_t3597491459 * L_0 = __this->get_U3CcoordinatesU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Node::set_coordinates(IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Position)
extern "C"  void Node_set_coordinates_m2875279350 (Node_t3986951550 * __this, Position_t3597491459 * ___value0, const RuntimeMethod* method)
{
	{
		Position_t3597491459 * L_0 = ___value0;
		__this->set_U3CcoordinatesU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String[] IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Node::get_solution_refs()
extern "C"  StringU5BU5D_t1642385972* Node_get_solution_refs_m501922214 (Node_t3986951550 * __this, const RuntimeMethod* method)
{
	{
		StringU5BU5D_t1642385972* L_0 = __this->get_U3Csolution_refsU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Node::set_solution_refs(System.String[])
extern "C"  void Node_set_solution_refs_m2183770917 (Node_t3986951550 * __this, StringU5BU5D_t1642385972* ___value0, const RuntimeMethod* method)
{
	{
		StringU5BU5D_t1642385972* L_0 = ___value0;
		__this->set_U3Csolution_refsU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Option::.ctor()
extern "C"  void Option__ctor_m78809847 (Option_t1775127045 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.ApplicationData IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Option::get_app_data()
extern "C"  ApplicationData_t1885408412 * Option_get_app_data_m728718952 (Option_t1775127045 * __this, const RuntimeMethod* method)
{
	{
		ApplicationData_t1885408412 * L_0 = __this->get_U3Capp_dataU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Option::set_app_data(IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.ApplicationData)
extern "C"  void Option_set_app_data_m3997575045 (Option_t1775127045 * __this, ApplicationData_t1885408412 * ___value0, const RuntimeMethod* method)
{
	{
		ApplicationData_t1885408412 * L_0 = ___value0;
		__this->set_U3Capp_dataU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Option::get_description_html()
extern "C"  String_t* Option_get_description_html_m1352122775 (Option_t1775127045 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3Cdescription_htmlU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Option::set_description_html(System.String)
extern "C"  void Option_set_description_html_m224700786 (Option_t1775127045 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3Cdescription_htmlU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Option::get_key()
extern "C"  String_t* Option_get_key_m1288240400 (Option_t1775127045 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CkeyU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Option::set_key(System.String)
extern "C"  void Option_set_key_m2662544107 (Option_t1775127045 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CkeyU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Option::get_name()
extern "C"  String_t* Option_get_name_m1279243456 (Option_t1775127045 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CnameU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Option::set_name(System.String)
extern "C"  void Option_set_name_m2036632347 (Option_t1775127045 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CnameU3Ek__BackingField_3(L_0);
		return;
	}
}
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.ApplicationDataValue IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Option::get_values()
extern "C"  ApplicationDataValue_t1721671971 * Option_get_values_m1682394903 (Option_t1775127045 * __this, const RuntimeMethod* method)
{
	{
		ApplicationDataValue_t1721671971 * L_0 = __this->get_U3CvaluesU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Option::set_values(IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.ApplicationDataValue)
extern "C"  void Option_set_values_m1427637990 (Option_t1775127045 * __this, ApplicationDataValue_t1721671971 * ___value0, const RuntimeMethod* method)
{
	{
		ApplicationDataValue_t1721671971 * L_0 = ___value0;
		__this->set_U3CvaluesU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Params::.ctor()
extern "C"  void Params__ctor_m1446514638 (Params_t3159428742 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Double IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Params::get_alpha_init()
extern "C"  double Params_get_alpha_init_m805194201 (Params_t3159428742 * __this, const RuntimeMethod* method)
{
	{
		double L_0 = __this->get_U3Calpha_initU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Params::set_alpha_init(System.Double)
extern "C"  void Params_set_alpha_init_m563998288 (Params_t3159428742 * __this, double ___value0, const RuntimeMethod* method)
{
	{
		double L_0 = ___value0;
		__this->set_U3Calpha_initU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Double IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Params::get_anchor_epoch()
extern "C"  double Params_get_anchor_epoch_m2117384023 (Params_t3159428742 * __this, const RuntimeMethod* method)
{
	{
		double L_0 = __this->get_U3Canchor_epochU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Params::set_anchor_epoch(System.Double)
extern "C"  void Params_set_anchor_epoch_m892603852 (Params_t3159428742 * __this, double ___value0, const RuntimeMethod* method)
{
	{
		double L_0 = ___value0;
		__this->set_U3Canchor_epochU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Double IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Params::get_map_size()
extern "C"  double Params_get_map_size_m3153264310 (Params_t3159428742 * __this, const RuntimeMethod* method)
{
	{
		double L_0 = __this->get_U3Cmap_sizeU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Params::set_map_size(System.Double)
extern "C"  void Params_set_map_size_m3115907149 (Params_t3159428742 * __this, double ___value0, const RuntimeMethod* method)
{
	{
		double L_0 = ___value0;
		__this->set_U3Cmap_sizeU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Double IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Params::get_rAnchor()
extern "C"  double Params_get_rAnchor_m3380297539 (Params_t3159428742 * __this, const RuntimeMethod* method)
{
	{
		double L_0 = __this->get_U3CrAnchorU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Params::set_rAnchor(System.Double)
extern "C"  void Params_set_rAnchor_m3298392552 (Params_t3159428742 * __this, double ___value0, const RuntimeMethod* method)
{
	{
		double L_0 = ___value0;
		__this->set_U3CrAnchorU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Double IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Params::get_rFinish()
extern "C"  double Params_get_rFinish_m694668617 (Params_t3159428742 * __this, const RuntimeMethod* method)
{
	{
		double L_0 = __this->get_U3CrFinishU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Params::set_rFinish(System.Double)
extern "C"  void Params_set_rFinish_m2592682658 (Params_t3159428742 * __this, double ___value0, const RuntimeMethod* method)
{
	{
		double L_0 = ___value0;
		__this->set_U3CrFinishU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Double IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Params::get_rInit()
extern "C"  double Params_get_rInit_m1223893602 (Params_t3159428742 * __this, const RuntimeMethod* method)
{
	{
		double L_0 = __this->get_U3CrInitU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Params::set_rInit(System.Double)
extern "C"  void Params_set_rInit_m2692324327 (Params_t3159428742 * __this, double ___value0, const RuntimeMethod* method)
{
	{
		double L_0 = ___value0;
		__this->set_U3CrInitU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.Double IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Params::get_seed()
extern "C"  double Params_get_seed_m3400212019 (Params_t3159428742 * __this, const RuntimeMethod* method)
{
	{
		double L_0 = __this->get_U3CseedU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Params::set_seed(System.Double)
extern "C"  void Params_set_seed_m3915007182 (Params_t3159428742 * __this, double ___value0, const RuntimeMethod* method)
{
	{
		double L_0 = ___value0;
		__this->set_U3CseedU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.Double IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Params::get_training_period()
extern "C"  double Params_get_training_period_m1395529520 (Params_t3159428742 * __this, const RuntimeMethod* method)
{
	{
		double L_0 = __this->get_U3Ctraining_periodU3Ek__BackingField_7();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Params::set_training_period(System.Double)
extern "C"  void Params_set_training_period_m3029534885 (Params_t3159428742 * __this, double ___value0, const RuntimeMethod* method)
{
	{
		double L_0 = ___value0;
		__this->set_U3Ctraining_periodU3Ek__BackingField_7(L_0);
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Position::.ctor()
extern "C"  void Position__ctor_m2657836317 (Position_t3597491459 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Double IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Position::get_x()
extern "C"  double Position_get_x_m2618542913 (Position_t3597491459 * __this, const RuntimeMethod* method)
{
	{
		double L_0 = __this->get_U3CxU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Position::set_x(System.Double)
extern "C"  void Position_set_x_m2647757540 (Position_t3597491459 * __this, double ___value0, const RuntimeMethod* method)
{
	{
		double L_0 = ___value0;
		__this->set_U3CxU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Double IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Position::get_y()
extern "C"  double Position_get_y_m2618542946 (Position_t3597491459 * __this, const RuntimeMethod* method)
{
	{
		double L_0 = __this->get_U3CyU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Position::set_y(System.Double)
extern "C"  void Position_set_y_m4028491331 (Position_t3597491459 * __this, double ___value0, const RuntimeMethod* method)
{
	{
		double L_0 = ___value0;
		__this->set_U3CyU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Problem::.ctor()
extern "C"  void Problem__ctor_m2287231115 (Problem_t2814813345 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column[] IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Problem::get_columns()
extern "C"  ColumnU5BU5D_t4200576441* Problem_get_columns_m4014791723 (Problem_t2814813345 * __this, const RuntimeMethod* method)
{
	{
		ColumnU5BU5D_t4200576441* L_0 = __this->get_U3CcolumnsU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Problem::set_columns(IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column[])
extern "C"  void Problem_set_columns_m805733870 (Problem_t2814813345 * __this, ColumnU5BU5D_t4200576441* ___value0, const RuntimeMethod* method)
{
	{
		ColumnU5BU5D_t4200576441* L_0 = ___value0;
		__this->set_U3CcolumnsU3Ek__BackingField_0(L_0);
		return;
	}
}
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Option[] IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Problem::get_options()
extern "C"  OptionU5BU5D_t3811736648* Problem_get_options_m3226966347 (Problem_t2814813345 * __this, const RuntimeMethod* method)
{
	{
		OptionU5BU5D_t3811736648* L_0 = __this->get_U3CoptionsU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Problem::set_options(IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Option[])
extern "C"  void Problem_set_options_m3598639726 (Problem_t2814813345 * __this, OptionU5BU5D_t3811736648* ___value0, const RuntimeMethod* method)
{
	{
		OptionU5BU5D_t3811736648* L_0 = ___value0;
		__this->set_U3CoptionsU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Problem::get_subject()
extern "C"  String_t* Problem_get_subject_m4103365671 (Problem_t2814813345 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CsubjectU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Problem::set_subject(System.String)
extern "C"  void Problem_set_subject_m3312909546 (Problem_t2814813345 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CsubjectU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Range::.ctor()
extern "C"  void Range__ctor_m3717125291 (Range_t2396059803 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Resolution::.ctor()
extern "C"  void Resolution__ctor_m40778190 (Resolution_t3867497320 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Map IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Resolution::get_map()
extern "C"  Map_t1914475838 * Resolution_get_map_m12112883 (Resolution_t3867497320 * __this, const RuntimeMethod* method)
{
	{
		Map_t1914475838 * L_0 = __this->get_U3CmapU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Resolution::set_map(IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Map)
extern "C"  void Resolution_set_map_m3102661772 (Resolution_t3867497320 * __this, Map_t1914475838 * ___value0, const RuntimeMethod* method)
{
	{
		Map_t1914475838 * L_0 = ___value0;
		__this->set_U3CmapU3Ek__BackingField_0(L_0);
		return;
	}
}
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Solution[] IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Resolution::get_solutions()
extern "C"  SolutionU5BU5D_t2153136770* Resolution_get_solutions_m2526459574 (Resolution_t3867497320 * __this, const RuntimeMethod* method)
{
	{
		SolutionU5BU5D_t2153136770* L_0 = __this->get_U3CsolutionsU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Resolution::set_solutions(IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Solution[])
extern "C"  void Resolution_set_solutions_m794645937 (Resolution_t3867497320 * __this, SolutionU5BU5D_t2153136770* ___value0, const RuntimeMethod* method)
{
	{
		SolutionU5BU5D_t2153136770* L_0 = ___value0;
		__this->set_U3CsolutionsU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Solution::.ctor()
extern "C"  void Solution__ctor_m3951417589 (Solution_t204511443 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String[] IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Solution::get_shadow_me()
extern "C"  StringU5BU5D_t1642385972* Solution_get_shadow_me_m2150272082 (Solution_t204511443 * __this, const RuntimeMethod* method)
{
	{
		StringU5BU5D_t1642385972* L_0 = __this->get_U3Cshadow_meU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Solution::set_shadow_me(System.String[])
extern "C"  void Solution_set_shadow_me_m2506753609 (Solution_t204511443 * __this, StringU5BU5D_t1642385972* ___value0, const RuntimeMethod* method)
{
	{
		StringU5BU5D_t1642385972* L_0 = ___value0;
		__this->set_U3Cshadow_meU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String[] IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Solution::get_shadows()
extern "C"  StringU5BU5D_t1642385972* Solution_get_shadows_m1654640052 (Solution_t204511443 * __this, const RuntimeMethod* method)
{
	{
		StringU5BU5D_t1642385972* L_0 = __this->get_U3CshadowsU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Solution::set_shadows(System.String[])
extern "C"  void Solution_set_shadows_m1574436769 (Solution_t204511443 * __this, StringU5BU5D_t1642385972* ___value0, const RuntimeMethod* method)
{
	{
		StringU5BU5D_t1642385972* L_0 = ___value0;
		__this->set_U3CshadowsU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Solution::get_solution_ref()
extern "C"  String_t* Solution_get_solution_ref_m3857227232 (Solution_t204511443 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3Csolution_refU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Solution::set_solution_ref(System.String)
extern "C"  void Solution_set_solution_ref_m1166533753 (Solution_t204511443 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3Csolution_refU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Solution::get_status()
extern "C"  String_t* Solution_get_status_m3721440741 (Solution_t204511443 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CstatusU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Solution::set_status(System.String)
extern "C"  void Solution_set_status_m596276304 (Solution_t204511443 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CstatusU3Ek__BackingField_3(L_0);
		return;
	}
}
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.StatusCause IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Solution::get_status_cause()
extern "C"  StatusCause_t3777693303 * Solution_get_status_cause_m1096775687 (Solution_t204511443 * __this, const RuntimeMethod* method)
{
	{
		StatusCause_t3777693303 * L_0 = __this->get_U3Cstatus_causeU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Solution::set_status_cause(IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.StatusCause)
extern "C"  void Solution_set_status_cause_m1634420886 (Solution_t204511443 * __this, StatusCause_t3777693303 * ___value0, const RuntimeMethod* method)
{
	{
		StatusCause_t3777693303 * L_0 = ___value0;
		__this->set_U3Cstatus_causeU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.StatusCause::.ctor()
extern "C"  void StatusCause__ctor_m1524473301 (StatusCause_t3777693303 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.StatusCause::get_error_code()
extern "C"  String_t* StatusCause_get_error_code_m2150550667 (StatusCause_t3777693303 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3Cerror_codeU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.StatusCause::set_error_code(System.String)
extern "C"  void StatusCause_set_error_code_m1425322044 (StatusCause_t3777693303 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3Cerror_codeU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.StatusCause::get_message()
extern "C"  String_t* StatusCause_get_message_m3785860864 (StatusCause_t3777693303 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CmessageU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.StatusCause::set_message(System.String)
extern "C"  void StatusCause_set_message_m488116765 (StatusCause_t3777693303 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CmessageU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String[] IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.StatusCause::get_tokens()
extern "C"  StringU5BU5D_t1642385972* StatusCause_get_tokens_m774317143 (StatusCause_t3777693303 * __this, const RuntimeMethod* method)
{
	{
		StringU5BU5D_t1642385972* L_0 = __this->get_U3CtokensU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.StatusCause::set_tokens(System.String[])
extern "C"  void StatusCause_set_tokens_m1592808568 (StatusCause_t3777693303 * __this, StringU5BU5D_t1642385972* ___value0, const RuntimeMethod* method)
{
	{
		StringU5BU5D_t1642385972* L_0 = ___value0;
		__this->set_U3CtokensU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics::.ctor()
extern "C"  void TradeoffAnalytics__ctor_m95378715 (TradeoffAnalytics_t100002595 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics::GetDilemma(IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics/OnDilemma,IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Problem,System.Boolean)
extern "C"  bool TradeoffAnalytics_GetDilemma_m3487116940 (TradeoffAnalytics_t100002595 * __this, OnDilemma_t401820821 * ___callback0, Problem_t2814813345 * ___problem1, bool ___generateVisualization2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TradeoffAnalytics_GetDilemma_m3487116940_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RESTConnector_t3705102247 * V_0 = NULL;
	GetDilemmaRequest_t37205599 * V_1 = NULL;
	fsData_t2583805605 * V_2 = NULL;
	{
		OnDilemma_t401820821 * L_0 = ___callback0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, _stringLiteral1913944053, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(RESTConnector_t3705102247_il2cpp_TypeInfo_var);
		RESTConnector_t3705102247 * L_2 = RESTConnector_GetConnector_m3262139854(NULL /*static, unused*/, _stringLiteral1036380510, _stringLiteral3719323877, (bool)1, /*hidden argument*/NULL);
		V_0 = L_2;
		RESTConnector_t3705102247 * L_3 = V_0;
		if (L_3)
		{
			goto IL_002a;
		}
	}
	{
		return (bool)0;
	}

IL_002a:
	{
		GetDilemmaRequest_t37205599 * L_4 = (GetDilemmaRequest_t37205599 *)il2cpp_codegen_object_new(GetDilemmaRequest_t37205599_il2cpp_TypeInfo_var);
		GetDilemmaRequest__ctor_m1840711466(L_4, /*hidden argument*/NULL);
		V_1 = L_4;
		GetDilemmaRequest_t37205599 * L_5 = V_1;
		OnDilemma_t401820821 * L_6 = ___callback0;
		NullCheck(L_5);
		GetDilemmaRequest_set_Callback_m2673609369(L_5, L_6, /*hidden argument*/NULL);
		GetDilemmaRequest_t37205599 * L_7 = V_1;
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)TradeoffAnalytics_GetDilemmaResponse_m4122534449_RuntimeMethod_var);
		ResponseEvent_t1616568356 * L_9 = (ResponseEvent_t1616568356 *)il2cpp_codegen_object_new(ResponseEvent_t1616568356_il2cpp_TypeInfo_var);
		ResponseEvent__ctor_m3385394859(L_9, __this, L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		Request_set_OnResponse_m2226001952(L_7, L_9, /*hidden argument*/NULL);
		GetDilemmaRequest_t37205599 * L_10 = V_1;
		NullCheck(L_10);
		Dictionary_2_t309261261 * L_11 = Request_get_Parameters_m1217035494(L_10, /*hidden argument*/NULL);
		String_t* L_12 = Boolean_ToString_m1253164328((&___generateVisualization2), /*hidden argument*/NULL);
		NullCheck(L_11);
		Dictionary_2_set_Item_m3411472609(L_11, _stringLiteral1968262614, L_12, /*hidden argument*/Dictionary_2_set_Item_m3411472609_RuntimeMethod_var);
		V_2 = (fsData_t2583805605 *)NULL;
		IL2CPP_RUNTIME_CLASS_INIT(TradeoffAnalytics_t100002595_il2cpp_TypeInfo_var);
		fsSerializer_t4193731081 * L_13 = ((TradeoffAnalytics_t100002595_StaticFields*)il2cpp_codegen_static_fields_for(TradeoffAnalytics_t100002595_il2cpp_TypeInfo_var))->get_sm_Serializer_1();
		Problem_t2814813345 * L_14 = ___problem1;
		NullCheck(L_13);
		fsSerializer_TrySerialize_TisProblem_t2814813345_m3668382148(L_13, L_14, (&V_2), /*hidden argument*/fsSerializer_TrySerialize_TisProblem_t2814813345_m3668382148_RuntimeMethod_var);
		GetDilemmaRequest_t37205599 * L_15 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
		Encoding_t663144255 * L_16 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
		fsData_t2583805605 * L_17 = V_2;
		NullCheck(L_17);
		String_t* L_18 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_17);
		NullCheck(L_16);
		ByteU5BU5D_t3397334013* L_19 = VirtFuncInvoker1< ByteU5BU5D_t3397334013*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_16, L_18);
		NullCheck(L_15);
		Request_set_Send_m296178747(L_15, L_19, /*hidden argument*/NULL);
		GetDilemmaRequest_t37205599 * L_20 = V_1;
		NullCheck(L_20);
		Dictionary_2_t3943999495 * L_21 = Request_get_Headers_m340270624(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		Dictionary_2_set_Item_m4244870320(L_21, _stringLiteral1048821954, _stringLiteral1391431453, /*hidden argument*/Dictionary_2_set_Item_m4244870320_RuntimeMethod_var);
		RESTConnector_t3705102247 * L_22 = V_0;
		GetDilemmaRequest_t37205599 * L_23 = V_1;
		NullCheck(L_22);
		bool L_24 = RESTConnector_Send_m2925366401(L_22, L_23, /*hidden argument*/NULL);
		return L_24;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics::GetDilemmaResponse(IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request,IBM.Watson.DeveloperCloud.Connection.RESTConnector/Response)
extern "C"  void TradeoffAnalytics_GetDilemmaResponse_m4122534449 (TradeoffAnalytics_t100002595 * __this, Request_t466816980 * ___req0, Response_t429319368 * ___resp1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TradeoffAnalytics_GetDilemmaResponse_m4122534449_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DilemmasResponse_t3767705817 * V_0 = NULL;
	fsData_t2583805605 * V_1 = NULL;
	fsResult_t862419890  V_2;
	memset(&V_2, 0, sizeof(V_2));
	RuntimeObject * V_3 = NULL;
	Exception_t1927440687 * V_4 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	OnDilemma_t401820821 * G_B10_0 = NULL;
	OnDilemma_t401820821 * G_B9_0 = NULL;
	DilemmasResponse_t3767705817 * G_B11_0 = NULL;
	OnDilemma_t401820821 * G_B11_1 = NULL;
	{
		DilemmasResponse_t3767705817 * L_0 = (DilemmasResponse_t3767705817 *)il2cpp_codegen_object_new(DilemmasResponse_t3767705817_il2cpp_TypeInfo_var);
		DilemmasResponse__ctor_m2982690117(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Response_t429319368 * L_1 = ___resp1;
		NullCheck(L_1);
		bool L_2 = Response_get_Success_m2313267345(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_00a5;
		}
	}

IL_0011:
	try
	{ // begin try (depth: 1)
		{
			V_1 = (fsData_t2583805605 *)NULL;
			IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
			Encoding_t663144255 * L_3 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
			Response_t429319368 * L_4 = ___resp1;
			NullCheck(L_4);
			ByteU5BU5D_t3397334013* L_5 = Response_get_Data_m3078004018(L_4, /*hidden argument*/NULL);
			NullCheck(L_3);
			String_t* L_6 = VirtFuncInvoker1< String_t*, ByteU5BU5D_t3397334013* >::Invoke(22 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_3, L_5);
			fsResult_t862419890  L_7 = fsJsonParser_Parse_m4200302395(NULL /*static, unused*/, L_6, (&V_1), /*hidden argument*/NULL);
			V_2 = L_7;
			bool L_8 = fsResult_get_Succeeded_m3237049006((&V_2), /*hidden argument*/NULL);
			if (L_8)
			{
				goto IL_0044;
			}
		}

IL_0037:
		{
			String_t* L_9 = fsResult_get_FormattedMessages_m3414202484((&V_2), /*hidden argument*/NULL);
			WatsonException_t1186470237 * L_10 = (WatsonException_t1186470237 *)il2cpp_codegen_object_new(WatsonException_t1186470237_il2cpp_TypeInfo_var);
			WatsonException__ctor_m3825115789(L_10, L_9, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_10);
		}

IL_0044:
		{
			DilemmasResponse_t3767705817 * L_11 = V_0;
			V_3 = L_11;
			IL2CPP_RUNTIME_CLASS_INIT(TradeoffAnalytics_t100002595_il2cpp_TypeInfo_var);
			fsSerializer_t4193731081 * L_12 = ((TradeoffAnalytics_t100002595_StaticFields*)il2cpp_codegen_static_fields_for(TradeoffAnalytics_t100002595_il2cpp_TypeInfo_var))->get_sm_Serializer_1();
			fsData_t2583805605 * L_13 = V_1;
			RuntimeObject * L_14 = V_3;
			NullCheck(L_14);
			Type_t * L_15 = Object_GetType_m191970594(L_14, /*hidden argument*/NULL);
			NullCheck(L_12);
			fsResult_t862419890  L_16 = fsSerializer_TryDeserialize_m4134098758(L_12, L_13, L_15, (&V_3), /*hidden argument*/NULL);
			V_2 = L_16;
			bool L_17 = fsResult_get_Succeeded_m3237049006((&V_2), /*hidden argument*/NULL);
			if (L_17)
			{
				goto IL_0073;
			}
		}

IL_0066:
		{
			String_t* L_18 = fsResult_get_FormattedMessages_m3414202484((&V_2), /*hidden argument*/NULL);
			WatsonException_t1186470237 * L_19 = (WatsonException_t1186470237 *)il2cpp_codegen_object_new(WatsonException_t1186470237_il2cpp_TypeInfo_var);
			WatsonException__ctor_m3825115789(L_19, L_18, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_19);
		}

IL_0073:
		{
			goto IL_00a5;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1927440687_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0078;
		throw e;
	}

CATCH_0078:
	{ // begin catch(System.Exception)
		V_4 = ((Exception_t1927440687 *)__exception_local);
		ObjectU5BU5D_t3614634134* L_20 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1));
		Exception_t1927440687 * L_21 = V_4;
		NullCheck(L_21);
		String_t* L_22 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_21);
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, L_22);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_22);
		Log_Error_m685270089(NULL /*static, unused*/, _stringLiteral1552884091, _stringLiteral2441933943, L_20, /*hidden argument*/NULL);
		Response_t429319368 * L_23 = ___resp1;
		NullCheck(L_23);
		Response_set_Success_m3746673474(L_23, (bool)0, /*hidden argument*/NULL);
		goto IL_00a5;
	} // end catch (depth: 1)

IL_00a5:
	{
		Request_t466816980 * L_24 = ___req0;
		NullCheck(((GetDilemmaRequest_t37205599 *)CastclassClass((RuntimeObject*)L_24, GetDilemmaRequest_t37205599_il2cpp_TypeInfo_var)));
		OnDilemma_t401820821 * L_25 = GetDilemmaRequest_get_Callback_m420212378(((GetDilemmaRequest_t37205599 *)CastclassClass((RuntimeObject*)L_24, GetDilemmaRequest_t37205599_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00d7;
		}
	}
	{
		Request_t466816980 * L_26 = ___req0;
		NullCheck(((GetDilemmaRequest_t37205599 *)CastclassClass((RuntimeObject*)L_26, GetDilemmaRequest_t37205599_il2cpp_TypeInfo_var)));
		OnDilemma_t401820821 * L_27 = GetDilemmaRequest_get_Callback_m420212378(((GetDilemmaRequest_t37205599 *)CastclassClass((RuntimeObject*)L_26, GetDilemmaRequest_t37205599_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		Response_t429319368 * L_28 = ___resp1;
		NullCheck(L_28);
		bool L_29 = Response_get_Success_m2313267345(L_28, /*hidden argument*/NULL);
		G_B9_0 = L_27;
		if (!L_29)
		{
			G_B10_0 = L_27;
			goto IL_00d1;
		}
	}
	{
		DilemmasResponse_t3767705817 * L_30 = V_0;
		G_B11_0 = L_30;
		G_B11_1 = G_B9_0;
		goto IL_00d2;
	}

IL_00d1:
	{
		G_B11_0 = ((DilemmasResponse_t3767705817 *)(NULL));
		G_B11_1 = G_B10_0;
	}

IL_00d2:
	{
		NullCheck(G_B11_1);
		OnDilemma_Invoke_m3166755296(G_B11_1, G_B11_0, /*hidden argument*/NULL);
	}

IL_00d7:
	{
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics::GetServiceID()
extern "C"  String_t* TradeoffAnalytics_GetServiceID_m7835884 (TradeoffAnalytics_t100002595 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TradeoffAnalytics_GetServiceID_m7835884_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		return _stringLiteral1036380510;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics::GetServiceStatus(IBM.Watson.DeveloperCloud.Services.ServiceStatus)
extern "C"  void TradeoffAnalytics_GetServiceStatus_m3183265306 (TradeoffAnalytics_t100002595 * __this, ServiceStatus_t1443707987 * ___callback0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TradeoffAnalytics_GetServiceStatus_m3183265306_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Config_t3637807320_il2cpp_TypeInfo_var);
		Config_t3637807320 * L_0 = Config_get_Instance_m430784956(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		CredentialInfo_t34154441 * L_1 = Config_FindCredentials_m3250710905(L_0, _stringLiteral1036380510, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		ServiceStatus_t1443707987 * L_2 = ___callback0;
		CheckServiceStatus_t584740610 * L_3 = (CheckServiceStatus_t584740610 *)il2cpp_codegen_object_new(CheckServiceStatus_t584740610_il2cpp_TypeInfo_var);
		CheckServiceStatus__ctor_m3332446737(L_3, __this, L_2, /*hidden argument*/NULL);
		goto IL_002d;
	}

IL_0021:
	{
		ServiceStatus_t1443707987 * L_4 = ___callback0;
		NullCheck(L_4);
		ServiceStatus_Invoke_m1869726628(L_4, _stringLiteral1036380510, (bool)0, /*hidden argument*/NULL);
	}

IL_002d:
	{
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics::.cctor()
extern "C"  void TradeoffAnalytics__cctor_m3384192268 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TradeoffAnalytics__cctor_m3384192268_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		fsSerializer_t4193731081 * L_0 = (fsSerializer_t4193731081 *)il2cpp_codegen_object_new(fsSerializer_t4193731081_il2cpp_TypeInfo_var);
		fsSerializer__ctor_m3368490715(L_0, /*hidden argument*/NULL);
		((TradeoffAnalytics_t100002595_StaticFields*)il2cpp_codegen_static_fields_for(TradeoffAnalytics_t100002595_il2cpp_TypeInfo_var))->set_sm_Serializer_1(L_0);
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics/CheckServiceStatus::.ctor(IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics,IBM.Watson.DeveloperCloud.Services.ServiceStatus)
extern "C"  void CheckServiceStatus__ctor_m3332446737 (CheckServiceStatus_t584740610 * __this, TradeoffAnalytics_t100002595 * ___service0, ServiceStatus_t1443707987 * ___callback1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CheckServiceStatus__ctor_m3332446737_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Problem_t2814813345 * V_0 = NULL;
	List_1_t1981607956 * V_1 = NULL;
	Column_t2612486824 * V_2 = NULL;
	List_1_t1144248177 * V_3 = NULL;
	Option_t1775127045 * V_4 = NULL;
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		TradeoffAnalytics_t100002595 * L_0 = ___service0;
		__this->set_m_Service_0(L_0);
		ServiceStatus_t1443707987 * L_1 = ___callback1;
		__this->set_m_Callback_1(L_1);
		Problem_t2814813345 * L_2 = (Problem_t2814813345 *)il2cpp_codegen_object_new(Problem_t2814813345_il2cpp_TypeInfo_var);
		Problem__ctor_m2287231115(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		Problem_t2814813345 * L_3 = V_0;
		NullCheck(L_3);
		Problem_set_subject_m3312909546(L_3, _stringLiteral2294719762, /*hidden argument*/NULL);
		List_1_t1981607956 * L_4 = (List_1_t1981607956 *)il2cpp_codegen_object_new(List_1_t1981607956_il2cpp_TypeInfo_var);
		List_1__ctor_m1814123416(L_4, /*hidden argument*/List_1__ctor_m1814123416_RuntimeMethod_var);
		V_1 = L_4;
		Column_t2612486824 * L_5 = (Column_t2612486824 *)il2cpp_codegen_object_new(Column_t2612486824_il2cpp_TypeInfo_var);
		Column__ctor_m3168083222(L_5, /*hidden argument*/NULL);
		V_2 = L_5;
		Column_t2612486824 * L_6 = V_2;
		NullCheck(L_6);
		Column_set_key_m3783075932(L_6, _stringLiteral2294719762, /*hidden argument*/NULL);
		Column_t2612486824 * L_7 = V_2;
		NullCheck(L_7);
		Column_set_type_m3872918225(L_7, _stringLiteral2113949763, /*hidden argument*/NULL);
		Column_t2612486824 * L_8 = V_2;
		NullCheck(L_8);
		Column_set_goal_m1264124692(L_8, _stringLiteral339799074, /*hidden argument*/NULL);
		Column_t2612486824 * L_9 = V_2;
		NullCheck(L_9);
		Column_set_is_objective_m3607883288(L_9, (bool)1, /*hidden argument*/NULL);
		Column_t2612486824 * L_10 = V_2;
		ValueRange_t543481982 * L_11 = (ValueRange_t543481982 *)il2cpp_codegen_object_new(ValueRange_t543481982_il2cpp_TypeInfo_var);
		ValueRange__ctor_m3640495210(L_11, /*hidden argument*/NULL);
		NullCheck(L_10);
		Column_set_range_m3088237292(L_10, L_11, /*hidden argument*/NULL);
		Column_t2612486824 * L_12 = V_2;
		NullCheck(L_12);
		Range_t2396059803 * L_13 = Column_get_range_m2413620963(L_12, /*hidden argument*/NULL);
		NullCheck(((ValueRange_t543481982 *)CastclassClass((RuntimeObject*)L_13, ValueRange_t543481982_il2cpp_TypeInfo_var)));
		ValueRange_set_high_m97411425(((ValueRange_t543481982 *)CastclassClass((RuntimeObject*)L_13, ValueRange_t543481982_il2cpp_TypeInfo_var)), (1.0), /*hidden argument*/NULL);
		Column_t2612486824 * L_14 = V_2;
		NullCheck(L_14);
		Range_t2396059803 * L_15 = Column_get_range_m2413620963(L_14, /*hidden argument*/NULL);
		NullCheck(((ValueRange_t543481982 *)CastclassClass((RuntimeObject*)L_15, ValueRange_t543481982_il2cpp_TypeInfo_var)));
		ValueRange_set_low_m1674791863(((ValueRange_t543481982 *)CastclassClass((RuntimeObject*)L_15, ValueRange_t543481982_il2cpp_TypeInfo_var)), (0.0), /*hidden argument*/NULL);
		List_1_t1981607956 * L_16 = V_1;
		Column_t2612486824 * L_17 = V_2;
		NullCheck(L_16);
		List_1_Add_m952068700(L_16, L_17, /*hidden argument*/List_1_Add_m952068700_RuntimeMethod_var);
		Problem_t2814813345 * L_18 = V_0;
		List_1_t1981607956 * L_19 = V_1;
		NullCheck(L_19);
		ColumnU5BU5D_t4200576441* L_20 = List_1_ToArray_m3175624038(L_19, /*hidden argument*/List_1_ToArray_m3175624038_RuntimeMethod_var);
		NullCheck(L_18);
		Problem_set_columns_m805733870(L_18, L_20, /*hidden argument*/NULL);
		List_1_t1144248177 * L_21 = (List_1_t1144248177 *)il2cpp_codegen_object_new(List_1_t1144248177_il2cpp_TypeInfo_var);
		List_1__ctor_m3074099389(L_21, /*hidden argument*/List_1__ctor_m3074099389_RuntimeMethod_var);
		V_3 = L_21;
		Option_t1775127045 * L_22 = (Option_t1775127045 *)il2cpp_codegen_object_new(Option_t1775127045_il2cpp_TypeInfo_var);
		Option__ctor_m78809847(L_22, /*hidden argument*/NULL);
		V_4 = L_22;
		Option_t1775127045 * L_23 = V_4;
		NullCheck(L_23);
		Option_set_key_m2662544107(L_23, _stringLiteral2294719762, /*hidden argument*/NULL);
		Option_t1775127045 * L_24 = V_4;
		PingDataValue_t526342856 * L_25 = (PingDataValue_t526342856 *)il2cpp_codegen_object_new(PingDataValue_t526342856_il2cpp_TypeInfo_var);
		PingDataValue__ctor_m1063649603(L_25, /*hidden argument*/NULL);
		NullCheck(L_24);
		Option_set_values_m1427637990(L_24, L_25, /*hidden argument*/NULL);
		Option_t1775127045 * L_26 = V_4;
		NullCheck(L_26);
		ApplicationDataValue_t1721671971 * L_27 = Option_get_values_m1682394903(L_26, /*hidden argument*/NULL);
		NullCheck(((PingDataValue_t526342856 *)IsInstClass((RuntimeObject*)L_27, PingDataValue_t526342856_il2cpp_TypeInfo_var)));
		PingDataValue_set_ping_m939741896(((PingDataValue_t526342856 *)IsInstClass((RuntimeObject*)L_27, PingDataValue_t526342856_il2cpp_TypeInfo_var)), (0.0), /*hidden argument*/NULL);
		List_1_t1144248177 * L_28 = V_3;
		Option_t1775127045 * L_29 = V_4;
		NullCheck(L_28);
		List_1_Add_m2195314017(L_28, L_29, /*hidden argument*/List_1_Add_m2195314017_RuntimeMethod_var);
		Problem_t2814813345 * L_30 = V_0;
		List_1_t1144248177 * L_31 = V_3;
		NullCheck(L_31);
		OptionU5BU5D_t3811736648* L_32 = List_1_ToArray_m1652548499(L_31, /*hidden argument*/List_1_ToArray_m1652548499_RuntimeMethod_var);
		NullCheck(L_30);
		Problem_set_options_m3598639726(L_30, L_32, /*hidden argument*/NULL);
		TradeoffAnalytics_t100002595 * L_33 = __this->get_m_Service_0();
		IntPtr_t L_34;
		L_34.set_m_value_0((void*)(void*)CheckServiceStatus_OnGetDilemma_m1023184799_RuntimeMethod_var);
		OnDilemma_t401820821 * L_35 = (OnDilemma_t401820821 *)il2cpp_codegen_object_new(OnDilemma_t401820821_il2cpp_TypeInfo_var);
		OnDilemma__ctor_m1311248430(L_35, __this, L_34, /*hidden argument*/NULL);
		Problem_t2814813345 * L_36 = V_0;
		NullCheck(L_33);
		bool L_37 = TradeoffAnalytics_GetDilemma_m3487116940(L_33, L_35, L_36, (bool)0, /*hidden argument*/NULL);
		if (L_37)
		{
			goto IL_0125;
		}
	}
	{
		CheckServiceStatus_OnFailure_m1533919926(__this, _stringLiteral395016341, /*hidden argument*/NULL);
	}

IL_0125:
	{
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics/CheckServiceStatus::OnGetDilemma(IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.DilemmasResponse)
extern "C"  void CheckServiceStatus_OnGetDilemma_m1023184799 (CheckServiceStatus_t584740610 * __this, DilemmasResponse_t3767705817 * ___resp0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CheckServiceStatus_OnGetDilemma_m1023184799_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ServiceStatus_t1443707987 * L_0 = __this->get_m_Callback_1();
		if (!L_0)
		{
			goto IL_0027;
		}
	}
	{
		DilemmasResponse_t3767705817 * L_1 = ___resp0;
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		ServiceStatus_t1443707987 * L_2 = __this->get_m_Callback_1();
		NullCheck(L_2);
		ServiceStatus_Invoke_m1869726628(L_2, _stringLiteral1036380510, (bool)1, /*hidden argument*/NULL);
		goto IL_0032;
	}

IL_0027:
	{
		CheckServiceStatus_OnFailure_m1533919926(__this, _stringLiteral1580212492, /*hidden argument*/NULL);
	}

IL_0032:
	{
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics/CheckServiceStatus::OnFailure(System.String)
extern "C"  void CheckServiceStatus_OnFailure_m1533919926 (CheckServiceStatus_t584740610 * __this, String_t* ___msg0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CheckServiceStatus_OnFailure_m1533919926_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___msg0;
		Log_Error_m685270089(NULL /*static, unused*/, _stringLiteral1552884091, L_0, ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		ServiceStatus_t1443707987 * L_1 = __this->get_m_Callback_1();
		NullCheck(L_1);
		ServiceStatus_Invoke_m1869726628(L_1, _stringLiteral1036380510, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics/GetDilemmaRequest::.ctor()
extern "C"  void GetDilemmaRequest__ctor_m1840711466 (GetDilemmaRequest_t37205599 * __this, const RuntimeMethod* method)
{
	{
		Request__ctor_m1928448681(__this, /*hidden argument*/NULL);
		return;
	}
}
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics/OnDilemma IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics/GetDilemmaRequest::get_Callback()
extern "C"  OnDilemma_t401820821 * GetDilemmaRequest_get_Callback_m420212378 (GetDilemmaRequest_t37205599 * __this, const RuntimeMethod* method)
{
	{
		OnDilemma_t401820821 * L_0 = __this->get_U3CCallbackU3Ek__BackingField_11();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics/GetDilemmaRequest::set_Callback(IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics/OnDilemma)
extern "C"  void GetDilemmaRequest_set_Callback_m2673609369 (GetDilemmaRequest_t37205599 * __this, OnDilemma_t401820821 * ___value0, const RuntimeMethod* method)
{
	{
		OnDilemma_t401820821 * L_0 = ___value0;
		__this->set_U3CCallbackU3Ek__BackingField_11(L_0);
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics/OnDilemma::.ctor(System.Object,System.IntPtr)
extern "C"  void OnDilemma__ctor_m1311248430 (OnDilemma_t401820821 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics/OnDilemma::Invoke(IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.DilemmasResponse)
extern "C"  void OnDilemma_Invoke_m3166755296 (OnDilemma_t401820821 * __this, DilemmasResponse_t3767705817 * ___resp0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		OnDilemma_Invoke_m3166755296((OnDilemma_t401820821 *)__this->get_prev_9(),___resp0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, DilemmasResponse_t3767705817 * ___resp0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___resp0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, DilemmasResponse_t3767705817 * ___resp0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___resp0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___resp0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics/OnDilemma::BeginInvoke(IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.DilemmasResponse,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* OnDilemma_BeginInvoke_m1326115853 (OnDilemma_t401820821 * __this, DilemmasResponse_t3767705817 * ___resp0, AsyncCallback_t163412349 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___resp0;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics/OnDilemma::EndInvoke(System.IAsyncResult)
extern "C"  void OnDilemma_EndInvoke_m2343911848 (OnDilemma_t401820821 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics/PingData::.ctor()
extern "C"  void PingData__ctor_m1019711138 (PingData_t2961942429 * __this, const RuntimeMethod* method)
{
	{
		ApplicationData__ctor_m1879024432(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics/PingDataValue::.ctor()
extern "C"  void PingDataValue__ctor_m1063649603 (PingDataValue_t526342856 * __this, const RuntimeMethod* method)
{
	{
		ApplicationDataValue__ctor_m4048002801(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Double IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics/PingDataValue::get_ping()
extern "C"  double PingDataValue_get_ping_m2076643895 (PingDataValue_t526342856 * __this, const RuntimeMethod* method)
{
	{
		double L_0 = __this->get_U3CpingU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics/PingDataValue::set_ping(System.Double)
extern "C"  void PingDataValue_set_ping_m939741896 (PingDataValue_t526342856 * __this, double ___value0, const RuntimeMethod* method)
{
	{
		double L_0 = ___value0;
		__this->set_U3CpingU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.ValueRange::.ctor()
extern "C"  void ValueRange__ctor_m3640495210 (ValueRange_t543481982 * __this, const RuntimeMethod* method)
{
	{
		Range__ctor_m3717125291(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Double IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.ValueRange::get_low()
extern "C"  double ValueRange_get_low_m490373576 (ValueRange_t543481982 * __this, const RuntimeMethod* method)
{
	{
		double L_0 = __this->get_U3ClowU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.ValueRange::set_low(System.Double)
extern "C"  void ValueRange_set_low_m1674791863 (ValueRange_t543481982 * __this, double ___value0, const RuntimeMethod* method)
{
	{
		double L_0 = ___value0;
		__this->set_U3ClowU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Double IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.ValueRange::get_high()
extern "C"  double ValueRange_get_high_m3236942236 (ValueRange_t543481982 * __this, const RuntimeMethod* method)
{
	{
		double L_0 = __this->get_U3ChighU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.ValueRange::set_high(System.Double)
extern "C"  void ValueRange_set_high_m97411425 (ValueRange_t543481982 * __this, double ___value0, const RuntimeMethod* method)
{
	{
		double L_0 = ___value0;
		__this->set_U3ChighU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.Age::.ctor()
extern "C"  void Age__ctor_m202715325 (Age_t582693723 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.Age::get_min()
extern "C"  int32_t Age_get_min_m4160773758 (Age_t582693723 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_U3CminU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.Age::set_min(System.Int32)
extern "C"  void Age_set_min_m3311872571 (Age_t582693723 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CminU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Int32 IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.Age::get_max()
extern "C"  int32_t Age_get_max_m2319012304 (Age_t582693723 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_U3CmaxU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.Age::set_max(System.Int32)
extern "C"  void Age_set_max_m310780577 (Age_t582693723 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CmaxU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Double IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.Age::get_score()
extern "C"  double Age_get_score_m1134015865 (Age_t582693723 * __this, const RuntimeMethod* method)
{
	{
		double L_0 = __this->get_U3CscoreU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.Age::set_score(System.Double)
extern "C"  void Age_set_score_m3654807736 (Age_t582693723 * __this, double ___value0, const RuntimeMethod* method)
{
	{
		double L_0 = ___value0;
		__this->set_U3CscoreU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.Class::.ctor()
extern "C"  void Class__ctor_m1215118178 (Class_t4054986160 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.Class::get_m_Class()
extern "C"  String_t* Class_get_m_Class_m2707375192 (Class_t4054986160 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3Cm_ClassU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.Class::set_m_Class(System.String)
extern "C"  void Class_set_m_Class_m3573078907 (Class_t4054986160 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3Cm_ClassU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyParameters::.ctor()
extern "C"  void ClassifyParameters__ctor_m1279123458 (ClassifyParameters_t933043320 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyParameters::get_url()
extern "C"  String_t* ClassifyParameters_get_url_m3613476527 (ClassifyParameters_t933043320 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CurlU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyParameters::set_url(System.String)
extern "C"  void ClassifyParameters_set_url_m1635694586 (ClassifyParameters_t933043320 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CurlU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String[] IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyParameters::get_classifier_ids()
extern "C"  StringU5BU5D_t1642385972* ClassifyParameters_get_classifier_ids_m596657372 (ClassifyParameters_t933043320 * __this, const RuntimeMethod* method)
{
	{
		StringU5BU5D_t1642385972* L_0 = __this->get_U3Cclassifier_idsU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyParameters::set_classifier_ids(System.String[])
extern "C"  void ClassifyParameters_set_classifier_ids_m1446346819 (ClassifyParameters_t933043320 * __this, StringU5BU5D_t1642385972* ___value0, const RuntimeMethod* method)
{
	{
		StringU5BU5D_t1642385972* L_0 = ___value0;
		__this->set_U3Cclassifier_idsU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String[] IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyParameters::get_owners()
extern "C"  StringU5BU5D_t1642385972* ClassifyParameters_get_owners_m2237895636 (ClassifyParameters_t933043320 * __this, const RuntimeMethod* method)
{
	{
		StringU5BU5D_t1642385972* L_0 = __this->get_U3CownersU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyParameters::set_owners(System.String[])
extern "C"  void ClassifyParameters_set_owners_m2549374967 (ClassifyParameters_t933043320 * __this, StringU5BU5D_t1642385972* ___value0, const RuntimeMethod* method)
{
	{
		StringU5BU5D_t1642385972* L_0 = ___value0;
		__this->set_U3CownersU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Single IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyParameters::get_threshold()
extern "C"  float ClassifyParameters_get_threshold_m90414640 (ClassifyParameters_t933043320 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_U3CthresholdU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyParameters::set_threshold(System.Single)
extern "C"  void ClassifyParameters_set_threshold_m1134498475 (ClassifyParameters_t933043320 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_U3CthresholdU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyPerClassifier::.ctor()
extern "C"  void ClassifyPerClassifier__ctor_m1031309758 (ClassifyPerClassifier_t2357796318 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyPerClassifier::get_name()
extern "C"  String_t* ClassifyPerClassifier_get_name_m4061835129 (ClassifyPerClassifier_t2357796318 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CnameU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyPerClassifier::set_name(System.String)
extern "C"  void ClassifyPerClassifier_set_name_m1031891212 (ClassifyPerClassifier_t2357796318 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CnameU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyPerClassifier::get_classifier_id()
extern "C"  String_t* ClassifyPerClassifier_get_classifier_id_m2783893831 (ClassifyPerClassifier_t2357796318 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3Cclassifier_idU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyPerClassifier::set_classifier_id(System.String)
extern "C"  void ClassifyPerClassifier_set_classifier_id_m1002820176 (ClassifyPerClassifier_t2357796318 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3Cclassifier_idU3Ek__BackingField_1(L_0);
		return;
	}
}
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassResult[] IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyPerClassifier::get_classes()
extern "C"  ClassResultU5BU5D_t2895066752* ClassifyPerClassifier_get_classes_m2443798190 (ClassifyPerClassifier_t2357796318 * __this, const RuntimeMethod* method)
{
	{
		ClassResultU5BU5D_t2895066752* L_0 = __this->get_U3CclassesU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyPerClassifier::set_classes(IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassResult[])
extern "C"  void ClassifyPerClassifier_set_classes_m2369560891 (ClassifyPerClassifier_t2357796318 * __this, ClassResultU5BU5D_t2895066752* ___value0, const RuntimeMethod* method)
{
	{
		ClassResultU5BU5D_t2895066752* L_0 = ___value0;
		__this->set_U3CclassesU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyTopLevelMultiple::.ctor()
extern "C"  void ClassifyTopLevelMultiple__ctor_m2803580807 (ClassifyTopLevelMultiple_t317282463 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyTopLevelMultiple::get_images_processed()
extern "C"  int32_t ClassifyTopLevelMultiple_get_images_processed_m1622413261 (ClassifyTopLevelMultiple_t317282463 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_U3Cimages_processedU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyTopLevelMultiple::set_images_processed(System.Int32)
extern "C"  void ClassifyTopLevelMultiple_set_images_processed_m77903674 (ClassifyTopLevelMultiple_t317282463 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3Cimages_processedU3Ek__BackingField_0(L_0);
		return;
	}
}
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyTopLevelSingle[] IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyTopLevelMultiple::get_images()
extern "C"  ClassifyTopLevelSingleU5BU5D_t3685983982* ClassifyTopLevelMultiple_get_images_m3822111337 (ClassifyTopLevelMultiple_t317282463 * __this, const RuntimeMethod* method)
{
	{
		ClassifyTopLevelSingleU5BU5D_t3685983982* L_0 = __this->get_U3CimagesU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyTopLevelMultiple::set_images(IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyTopLevelSingle[])
extern "C"  void ClassifyTopLevelMultiple_set_images_m3672658668 (ClassifyTopLevelMultiple_t317282463 * __this, ClassifyTopLevelSingleU5BU5D_t3685983982* ___value0, const RuntimeMethod* method)
{
	{
		ClassifyTopLevelSingleU5BU5D_t3685983982* L_0 = ___value0;
		__this->set_U3CimagesU3Ek__BackingField_1(L_0);
		return;
	}
}
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.WarningInfo[] IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyTopLevelMultiple::get_warnings()
extern "C"  WarningInfoU5BU5D_t1932079499* ClassifyTopLevelMultiple_get_warnings_m1165187099 (ClassifyTopLevelMultiple_t317282463 * __this, const RuntimeMethod* method)
{
	{
		WarningInfoU5BU5D_t1932079499* L_0 = __this->get_U3CwarningsU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyTopLevelMultiple::set_warnings(IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.WarningInfo[])
extern "C"  void ClassifyTopLevelMultiple_set_warnings_m4252567390 (ClassifyTopLevelMultiple_t317282463 * __this, WarningInfoU5BU5D_t1932079499* ___value0, const RuntimeMethod* method)
{
	{
		WarningInfoU5BU5D_t1932079499* L_0 = ___value0;
		__this->set_U3CwarningsU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyTopLevelSingle::.ctor()
extern "C"  void ClassifyTopLevelSingle__ctor_m2885736377 (ClassifyTopLevelSingle_t3733844951 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyTopLevelSingle::get_source_url()
extern "C"  String_t* ClassifyTopLevelSingle_get_source_url_m461114160 (ClassifyTopLevelSingle_t3733844951 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3Csource_urlU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyTopLevelSingle::set_source_url(System.String)
extern "C"  void ClassifyTopLevelSingle_set_source_url_m1076487207 (ClassifyTopLevelSingle_t3733844951 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3Csource_urlU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyTopLevelSingle::get_resolved_url()
extern "C"  String_t* ClassifyTopLevelSingle_get_resolved_url_m273967813 (ClassifyTopLevelSingle_t3733844951 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3Cresolved_urlU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyTopLevelSingle::set_resolved_url(System.String)
extern "C"  void ClassifyTopLevelSingle_set_resolved_url_m1907604666 (ClassifyTopLevelSingle_t3733844951 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3Cresolved_urlU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyTopLevelSingle::get_image()
extern "C"  String_t* ClassifyTopLevelSingle_get_image_m307471386 (ClassifyTopLevelSingle_t3733844951 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CimageU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyTopLevelSingle::set_image(System.String)
extern "C"  void ClassifyTopLevelSingle_set_image_m2671317621 (ClassifyTopLevelSingle_t3733844951 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CimageU3Ek__BackingField_2(L_0);
		return;
	}
}
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ErrorInfoNoCode IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyTopLevelSingle::get_error()
extern "C"  ErrorInfoNoCode_t2062371116 * ClassifyTopLevelSingle_get_error_m3590527570 (ClassifyTopLevelSingle_t3733844951 * __this, const RuntimeMethod* method)
{
	{
		ErrorInfoNoCode_t2062371116 * L_0 = __this->get_U3CerrorU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyTopLevelSingle::set_error(IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ErrorInfoNoCode)
extern "C"  void ClassifyTopLevelSingle_set_error_m4232244343 (ClassifyTopLevelSingle_t3733844951 * __this, ErrorInfoNoCode_t2062371116 * ___value0, const RuntimeMethod* method)
{
	{
		ErrorInfoNoCode_t2062371116 * L_0 = ___value0;
		__this->set_U3CerrorU3Ek__BackingField_3(L_0);
		return;
	}
}
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyPerClassifier[] IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyTopLevelSingle::get_classifiers()
extern "C"  ClassifyPerClassifierU5BU5D_t1895762155* ClassifyTopLevelSingle_get_classifiers_m2973919082 (ClassifyTopLevelSingle_t3733844951 * __this, const RuntimeMethod* method)
{
	{
		ClassifyPerClassifierU5BU5D_t1895762155* L_0 = __this->get_U3CclassifiersU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyTopLevelSingle::set_classifiers(IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyPerClassifier[])
extern "C"  void ClassifyTopLevelSingle_set_classifiers_m1957022159 (ClassifyTopLevelSingle_t3733844951 * __this, ClassifyPerClassifierU5BU5D_t1895762155* ___value0, const RuntimeMethod* method)
{
	{
		ClassifyPerClassifierU5BU5D_t1895762155* L_0 = ___value0;
		__this->set_U3CclassifiersU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassResult::.ctor()
extern "C"  void ClassResult__ctor_m3286998349 (ClassResult_t3791036973 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassResult::get_m_class()
extern "C"  String_t* ClassResult_get_m_class_m4181176445 (ClassResult_t3791036973 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3Cm_classU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassResult::set_m_class(System.String)
extern "C"  void ClassResult_set_m_class_m3895700416 (ClassResult_t3791036973 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3Cm_classU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Double IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassResult::get_score()
extern "C"  double ClassResult_get_score_m3920420873 (ClassResult_t3791036973 * __this, const RuntimeMethod* method)
{
	{
		double L_0 = __this->get_U3CscoreU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassResult::set_score(System.Double)
extern "C"  void ClassResult_set_score_m3011022526 (ClassResult_t3791036973 * __this, double ___value0, const RuntimeMethod* method)
{
	{
		double L_0 = ___value0;
		__this->set_U3CscoreU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassResult::get_type_hierarchy()
extern "C"  String_t* ClassResult_get_type_hierarchy_m3157224273 (ClassResult_t3791036973 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3Ctype_hierarchyU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassResult::set_type_hierarchy(System.String)
extern "C"  void ClassResult_set_type_hierarchy_m2317838894 (ClassResult_t3791036973 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3Ctype_hierarchyU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.CollectionImagesConfig::.ctor()
extern "C"  void CollectionImagesConfig__ctor_m3882389612 (CollectionImagesConfig_t1273369062 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.CollectionImagesConfig::get_image_id()
extern "C"  String_t* CollectionImagesConfig_get_image_id_m2457275707 (CollectionImagesConfig_t1273369062 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3Cimage_idU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.CollectionImagesConfig::set_image_id(System.String)
extern "C"  void CollectionImagesConfig_set_image_id_m3519311910 (CollectionImagesConfig_t1273369062 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3Cimage_idU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.CollectionImagesConfig::get_created()
extern "C"  String_t* CollectionImagesConfig_get_created_m2019043440 (CollectionImagesConfig_t1273369062 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CcreatedU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.CollectionImagesConfig::set_created(System.String)
extern "C"  void CollectionImagesConfig_set_created_m3278240297 (CollectionImagesConfig_t1273369062 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CcreatedU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.CollectionImagesConfig::get_image_file()
extern "C"  String_t* CollectionImagesConfig_get_image_file_m90981318 (CollectionImagesConfig_t1273369062 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3Cimage_fileU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.CollectionImagesConfig::set_image_file(System.String)
extern "C"  void CollectionImagesConfig_set_image_file_m150514971 (CollectionImagesConfig_t1273369062 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3Cimage_fileU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Object IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.CollectionImagesConfig::get_metadata()
extern "C"  RuntimeObject * CollectionImagesConfig_get_metadata_m2711321315 (CollectionImagesConfig_t1273369062 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CmetadataU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.CollectionImagesConfig::set_metadata(System.Object)
extern "C"  void CollectionImagesConfig_set_metadata_m3978114078 (CollectionImagesConfig_t1273369062 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___value0;
		__this->set_U3CmetadataU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.CollectionsConfig::.ctor()
extern "C"  void CollectionsConfig__ctor_m944697609 (CollectionsConfig_t2493163663 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.CollectionImagesConfig[] IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.CollectionsConfig::get_images()
extern "C"  CollectionImagesConfigU5BU5D_t666950595* CollectionsConfig_get_images_m2240461656 (CollectionsConfig_t2493163663 * __this, const RuntimeMethod* method)
{
	{
		CollectionImagesConfigU5BU5D_t666950595* L_0 = __this->get_U3CimagesU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.CollectionsConfig::set_images(IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.CollectionImagesConfig[])
extern "C"  void CollectionsConfig_set_images_m2855958695 (CollectionsConfig_t2493163663 * __this, CollectionImagesConfigU5BU5D_t666950595* ___value0, const RuntimeMethod* method)
{
	{
		CollectionImagesConfigU5BU5D_t666950595* L_0 = ___value0;
		__this->set_U3CimagesU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Int32 IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.CollectionsConfig::get_images_processed()
extern "C"  int32_t CollectionsConfig_get_images_processed_m1790747983 (CollectionsConfig_t2493163663 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_U3Cimages_processedU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.CollectionsConfig::set_images_processed(System.Int32)
extern "C"  void CollectionsConfig_set_images_processed_m2238041970 (CollectionsConfig_t2493163663 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3Cimages_processedU3Ek__BackingField_1(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
