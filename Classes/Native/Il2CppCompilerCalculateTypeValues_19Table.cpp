﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// Boo.Lang.List`1<System.Reflection.MemberInfo>
struct List_1_t1414935582;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Type
struct Type_t;
// Boo.Lang.Runtime.DynamicDispatching.DispatcherCache
struct DispatcherCache_t2227862569;
// Boo.Lang.Runtime.ExtensionRegistry
struct ExtensionRegistry_t1402255936;
// System.Collections.Generic.IEnumerator`1<System.Reflection.MemberInfo>
struct IEnumerator_1_t1518621087;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Collections.Generic.IEqualityComparer`1<Boo.Lang.Runtime.DynamicDispatching.DispatcherKey>
struct IEqualityComparer_1_t4216550924;
// System.String
struct String_t;
// System.Type[]
struct TypeU5BU5D_t1664964607;
// System.Collections.Generic.Dictionary`2<Boo.Lang.Runtime.DynamicDispatching.DispatcherKey,Boo.Lang.Runtime.DynamicDispatching.Dispatcher>
struct Dictionary_2_t1794457568;
// System.Collections.Generic.Dictionary`2<System.String,HtmlAgilityPack.HtmlAttribute>
struct Dictionary_2_t3719302665;
// HtmlAgilityPack.HtmlNode
struct HtmlNode_t2048434459;
// System.Collections.Generic.List`1<HtmlAgilityPack.HtmlAttribute>
struct List_1_t1173644535;
// System.EventHandler`1<TUIOsharp.DataProcessors.TuioBlobEventArgs>
struct EventHandler_1_t2154285351;
// System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioBlob>
struct Dictionary_2_t1054769049;
// System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>
struct List_1_t1416064546;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1440998580;
// System.EventHandler`1<TUIOsharp.DataProcessors.TuioObjectEventArgs>
struct EventHandler_1_t402050513;
// System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioObject>
struct Dictionary_2_t244646649;
// System.Collections.Generic.List`1<TUIOsharp.Entities.TuioObject>
struct List_1_t605942146;
// System.EventHandler`1<TUIOsharp.DataProcessors.TuioCursorEventArgs>
struct EventHandler_1_t3249264480;
// System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioCursor>
struct Dictionary_2_t858177054;
// System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>
struct List_1_t1219472551;
// System.UInt32[]
struct UInt32U5BU5D_t59386216;
// System.Collections.Generic.List`1<HtmlAgilityPack.HtmlNode>
struct List_1_t1417555591;
// System.EventHandler`1<OSCsharp.Utils.ExceptionEventArgs>
struct EventHandler_1_t3778988004;
// OSCsharp.Net.UDPReceiver
struct UDPReceiver_t3846109956;
// System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>
struct List_1_t1583176531;
// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>
struct List_1_t1070465849;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>>
struct Dictionary_2_t2985245111;
// System.IntPtr[]
struct IntPtrU5BU5D_t169632028;
// System.Collections.IDictionary
struct IDictionary_t596158605;
// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageEvent
struct MessageEvent_t2167079021;
// UnityEngine.Experimental.Rendering.IRenderPipelineAsset
struct IRenderPipelineAsset_t345810019;
// UnityEngine.Experimental.Rendering.IRenderPipeline
struct IRenderPipeline_t2611978095;
// System.Collections.Generic.List`1<UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers>
struct List_1_t1660627182;
// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/ConnectionChangeEvent
struct ConnectionChangeEvent_t536719976;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t2295673753;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t339478082;
// System.Collections.Generic.Dictionary`2<System.IntPtr,System.WeakReference>
struct Dictionary_2_t1519430885;
// TUIOsharp.Entities.TuioObject
struct TuioObject_t1236821014;
// TUIOsharp.Entities.TuioCursor
struct TuioCursor_t1850351419;
// TUIOsharp.Entities.TuioBlob
struct TuioBlob_t2046943414;
// System.Void
struct Void_t1841601450;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t2510243513;
// System.Xml.NameTable
struct NameTable_t594386929;
// System.Text.Encoding
struct Encoding_t663144255;
// System.DelegateData
struct DelegateData_t1572802995;
// HtmlAgilityPack.HtmlDocument
struct HtmlDocument_t556432108;
// HtmlAgilityPack.HtmlNameTable
struct HtmlNameTable_t3848610378;
// UnityEngine.ILogHandler
struct ILogHandler_t264057413;
// HtmlAgilityPack.HtmlAttributeCollection
struct HtmlAttributeCollection_t1787476631;
// HtmlAgilityPack.HtmlNodeCollection
struct HtmlNodeCollection_t2542734491;
// System.Collections.Generic.Dictionary`2<System.String,HtmlAgilityPack.HtmlElementFlag>
struct Dictionary_2_t2175053619;
// HtmlAgilityPack.Crc32
struct Crc32_t83196531;
// HtmlAgilityPack.HtmlAttribute
struct HtmlAttribute_t1804523403;
// System.Collections.Generic.Dictionary`2<System.String,HtmlAgilityPack.HtmlNode>
struct Dictionary_2_t3963213721;
// System.Collections.Generic.Dictionary`2<System.Int32,HtmlAgilityPack.HtmlNode>
struct Dictionary_2_t1056260094;
// System.Collections.Generic.List`1<HtmlAgilityPack.HtmlParseError>
struct List_1_t484300294;
// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents
struct PlayerEditorConnectionEvents_t2252784345;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// Boo.Lang.Runtime.DynamicDispatching.Dispatcher
struct Dispatcher_t2240407071;




#ifndef U3CMODULEU3E_T3783534222_H
#define U3CMODULEU3E_T3783534222_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t3783534222 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T3783534222_H
#ifndef U3CMODULEU3E_T3783534223_H
#define U3CMODULEU3E_T3783534223_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t3783534223 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T3783534223_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CMODULEU3E_T3783534224_H
#define U3CMODULEU3E_T3783534224_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t3783534224 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T3783534224_H
#ifndef U3CMODULEU3E_T3783534221_H
#define U3CMODULEU3E_T3783534221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t3783534221 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T3783534221_H
#ifndef EXTENSIONREGISTRY_T1402255936_H
#define EXTENSIONREGISTRY_T1402255936_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Boo.Lang.Runtime.ExtensionRegistry
struct  ExtensionRegistry_t1402255936  : public RuntimeObject
{
public:
	// Boo.Lang.List`1<System.Reflection.MemberInfo> Boo.Lang.Runtime.ExtensionRegistry::_extensions
	List_1_t1414935582 * ____extensions_0;
	// System.Object Boo.Lang.Runtime.ExtensionRegistry::_classLock
	RuntimeObject * ____classLock_1;

public:
	inline static int32_t get_offset_of__extensions_0() { return static_cast<int32_t>(offsetof(ExtensionRegistry_t1402255936, ____extensions_0)); }
	inline List_1_t1414935582 * get__extensions_0() const { return ____extensions_0; }
	inline List_1_t1414935582 ** get_address_of__extensions_0() { return &____extensions_0; }
	inline void set__extensions_0(List_1_t1414935582 * value)
	{
		____extensions_0 = value;
		Il2CppCodeGenWriteBarrier((&____extensions_0), value);
	}

	inline static int32_t get_offset_of__classLock_1() { return static_cast<int32_t>(offsetof(ExtensionRegistry_t1402255936, ____classLock_1)); }
	inline RuntimeObject * get__classLock_1() const { return ____classLock_1; }
	inline RuntimeObject ** get_address_of__classLock_1() { return &____classLock_1; }
	inline void set__classLock_1(RuntimeObject * value)
	{
		____classLock_1 = value;
		Il2CppCodeGenWriteBarrier((&____classLock_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENSIONREGISTRY_T1402255936_H
#ifndef RUNTIMESERVICES_T1910041954_H
#define RUNTIMESERVICES_T1910041954_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Boo.Lang.Runtime.RuntimeServices
struct  RuntimeServices_t1910041954  : public RuntimeObject
{
public:

public:
};

struct RuntimeServices_t1910041954_StaticFields
{
public:
	// System.Object[] Boo.Lang.Runtime.RuntimeServices::NoArguments
	ObjectU5BU5D_t3614634134* ___NoArguments_0;
	// System.Type Boo.Lang.Runtime.RuntimeServices::RuntimeServicesType
	Type_t * ___RuntimeServicesType_1;
	// Boo.Lang.Runtime.DynamicDispatching.DispatcherCache Boo.Lang.Runtime.RuntimeServices::_cache
	DispatcherCache_t2227862569 * ____cache_2;
	// Boo.Lang.Runtime.ExtensionRegistry Boo.Lang.Runtime.RuntimeServices::_extensions
	ExtensionRegistry_t1402255936 * ____extensions_3;
	// System.Object Boo.Lang.Runtime.RuntimeServices::True
	RuntimeObject * ___True_4;

public:
	inline static int32_t get_offset_of_NoArguments_0() { return static_cast<int32_t>(offsetof(RuntimeServices_t1910041954_StaticFields, ___NoArguments_0)); }
	inline ObjectU5BU5D_t3614634134* get_NoArguments_0() const { return ___NoArguments_0; }
	inline ObjectU5BU5D_t3614634134** get_address_of_NoArguments_0() { return &___NoArguments_0; }
	inline void set_NoArguments_0(ObjectU5BU5D_t3614634134* value)
	{
		___NoArguments_0 = value;
		Il2CppCodeGenWriteBarrier((&___NoArguments_0), value);
	}

	inline static int32_t get_offset_of_RuntimeServicesType_1() { return static_cast<int32_t>(offsetof(RuntimeServices_t1910041954_StaticFields, ___RuntimeServicesType_1)); }
	inline Type_t * get_RuntimeServicesType_1() const { return ___RuntimeServicesType_1; }
	inline Type_t ** get_address_of_RuntimeServicesType_1() { return &___RuntimeServicesType_1; }
	inline void set_RuntimeServicesType_1(Type_t * value)
	{
		___RuntimeServicesType_1 = value;
		Il2CppCodeGenWriteBarrier((&___RuntimeServicesType_1), value);
	}

	inline static int32_t get_offset_of__cache_2() { return static_cast<int32_t>(offsetof(RuntimeServices_t1910041954_StaticFields, ____cache_2)); }
	inline DispatcherCache_t2227862569 * get__cache_2() const { return ____cache_2; }
	inline DispatcherCache_t2227862569 ** get_address_of__cache_2() { return &____cache_2; }
	inline void set__cache_2(DispatcherCache_t2227862569 * value)
	{
		____cache_2 = value;
		Il2CppCodeGenWriteBarrier((&____cache_2), value);
	}

	inline static int32_t get_offset_of__extensions_3() { return static_cast<int32_t>(offsetof(RuntimeServices_t1910041954_StaticFields, ____extensions_3)); }
	inline ExtensionRegistry_t1402255936 * get__extensions_3() const { return ____extensions_3; }
	inline ExtensionRegistry_t1402255936 ** get_address_of__extensions_3() { return &____extensions_3; }
	inline void set__extensions_3(ExtensionRegistry_t1402255936 * value)
	{
		____extensions_3 = value;
		Il2CppCodeGenWriteBarrier((&____extensions_3), value);
	}

	inline static int32_t get_offset_of_True_4() { return static_cast<int32_t>(offsetof(RuntimeServices_t1910041954_StaticFields, ___True_4)); }
	inline RuntimeObject * get_True_4() const { return ___True_4; }
	inline RuntimeObject ** get_address_of_True_4() { return &___True_4; }
	inline void set_True_4(RuntimeObject * value)
	{
		___True_4 = value;
		Il2CppCodeGenWriteBarrier((&___True_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMESERVICES_T1910041954_H
#ifndef _EQUALITYCOMPARER_T1344382164_H
#define _EQUALITYCOMPARER_T1344382164_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Boo.Lang.Runtime.DynamicDispatching.DispatcherKey/_EqualityComparer
struct  _EqualityComparer_t1344382164  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // _EQUALITYCOMPARER_T1344382164_H
#ifndef U3CCOERCEU3EC__ANONSTOREY1D_T752649758_H
#define U3CCOERCEU3EC__ANONSTOREY1D_T752649758_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Boo.Lang.Runtime.RuntimeServices/<Coerce>c__AnonStorey1D
struct  U3CCoerceU3Ec__AnonStorey1D_t752649758  : public RuntimeObject
{
public:
	// System.Object Boo.Lang.Runtime.RuntimeServices/<Coerce>c__AnonStorey1D::value
	RuntimeObject * ___value_0;
	// System.Type Boo.Lang.Runtime.RuntimeServices/<Coerce>c__AnonStorey1D::toType
	Type_t * ___toType_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(U3CCoerceU3Ec__AnonStorey1D_t752649758, ___value_0)); }
	inline RuntimeObject * get_value_0() const { return ___value_0; }
	inline RuntimeObject ** get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(RuntimeObject * value)
	{
		___value_0 = value;
		Il2CppCodeGenWriteBarrier((&___value_0), value);
	}

	inline static int32_t get_offset_of_toType_1() { return static_cast<int32_t>(offsetof(U3CCoerceU3Ec__AnonStorey1D_t752649758, ___toType_1)); }
	inline Type_t * get_toType_1() const { return ___toType_1; }
	inline Type_t ** get_address_of_toType_1() { return &___toType_1; }
	inline void set_toType_1(Type_t * value)
	{
		___toType_1 = value;
		Il2CppCodeGenWriteBarrier((&___toType_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOERCEU3EC__ANONSTOREY1D_T752649758_H
#ifndef U3CGETEXTENSIONMETHODSU3EC__ITERATORC_T1600278906_H
#define U3CGETEXTENSIONMETHODSU3EC__ITERATORC_T1600278906_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Boo.Lang.Runtime.RuntimeServices/<GetExtensionMethods>c__IteratorC
struct  U3CGetExtensionMethodsU3Ec__IteratorC_t1600278906  : public RuntimeObject
{
public:
	// System.Collections.Generic.IEnumerator`1<System.Reflection.MemberInfo> Boo.Lang.Runtime.RuntimeServices/<GetExtensionMethods>c__IteratorC::<$s_49>__0
	RuntimeObject* ___U3CU24s_49U3E__0_0;
	// System.Reflection.MemberInfo Boo.Lang.Runtime.RuntimeServices/<GetExtensionMethods>c__IteratorC::<member>__1
	MemberInfo_t * ___U3CmemberU3E__1_1;
	// System.Int32 Boo.Lang.Runtime.RuntimeServices/<GetExtensionMethods>c__IteratorC::$PC
	int32_t ___U24PC_2;
	// System.Reflection.MethodInfo Boo.Lang.Runtime.RuntimeServices/<GetExtensionMethods>c__IteratorC::$current
	MethodInfo_t * ___U24current_3;

public:
	inline static int32_t get_offset_of_U3CU24s_49U3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetExtensionMethodsU3Ec__IteratorC_t1600278906, ___U3CU24s_49U3E__0_0)); }
	inline RuntimeObject* get_U3CU24s_49U3E__0_0() const { return ___U3CU24s_49U3E__0_0; }
	inline RuntimeObject** get_address_of_U3CU24s_49U3E__0_0() { return &___U3CU24s_49U3E__0_0; }
	inline void set_U3CU24s_49U3E__0_0(RuntimeObject* value)
	{
		___U3CU24s_49U3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU24s_49U3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CmemberU3E__1_1() { return static_cast<int32_t>(offsetof(U3CGetExtensionMethodsU3Ec__IteratorC_t1600278906, ___U3CmemberU3E__1_1)); }
	inline MemberInfo_t * get_U3CmemberU3E__1_1() const { return ___U3CmemberU3E__1_1; }
	inline MemberInfo_t ** get_address_of_U3CmemberU3E__1_1() { return &___U3CmemberU3E__1_1; }
	inline void set_U3CmemberU3E__1_1(MemberInfo_t * value)
	{
		___U3CmemberU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmemberU3E__1_1), value);
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CGetExtensionMethodsU3Ec__IteratorC_t1600278906, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CGetExtensionMethodsU3Ec__IteratorC_t1600278906, ___U24current_3)); }
	inline MethodInfo_t * get_U24current_3() const { return ___U24current_3; }
	inline MethodInfo_t ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(MethodInfo_t * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETEXTENSIONMETHODSU3EC__ITERATORC_T1600278906_H
#ifndef DISPATCHERKEY_T708950850_H
#define DISPATCHERKEY_T708950850_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Boo.Lang.Runtime.DynamicDispatching.DispatcherKey
struct  DispatcherKey_t708950850  : public RuntimeObject
{
public:
	// System.Type Boo.Lang.Runtime.DynamicDispatching.DispatcherKey::_type
	Type_t * ____type_1;
	// System.String Boo.Lang.Runtime.DynamicDispatching.DispatcherKey::_name
	String_t* ____name_2;
	// System.Type[] Boo.Lang.Runtime.DynamicDispatching.DispatcherKey::_arguments
	TypeU5BU5D_t1664964607* ____arguments_3;

public:
	inline static int32_t get_offset_of__type_1() { return static_cast<int32_t>(offsetof(DispatcherKey_t708950850, ____type_1)); }
	inline Type_t * get__type_1() const { return ____type_1; }
	inline Type_t ** get_address_of__type_1() { return &____type_1; }
	inline void set__type_1(Type_t * value)
	{
		____type_1 = value;
		Il2CppCodeGenWriteBarrier((&____type_1), value);
	}

	inline static int32_t get_offset_of__name_2() { return static_cast<int32_t>(offsetof(DispatcherKey_t708950850, ____name_2)); }
	inline String_t* get__name_2() const { return ____name_2; }
	inline String_t** get_address_of__name_2() { return &____name_2; }
	inline void set__name_2(String_t* value)
	{
		____name_2 = value;
		Il2CppCodeGenWriteBarrier((&____name_2), value);
	}

	inline static int32_t get_offset_of__arguments_3() { return static_cast<int32_t>(offsetof(DispatcherKey_t708950850, ____arguments_3)); }
	inline TypeU5BU5D_t1664964607* get__arguments_3() const { return ____arguments_3; }
	inline TypeU5BU5D_t1664964607** get_address_of__arguments_3() { return &____arguments_3; }
	inline void set__arguments_3(TypeU5BU5D_t1664964607* value)
	{
		____arguments_3 = value;
		Il2CppCodeGenWriteBarrier((&____arguments_3), value);
	}
};

struct DispatcherKey_t708950850_StaticFields
{
public:
	// System.Collections.Generic.IEqualityComparer`1<Boo.Lang.Runtime.DynamicDispatching.DispatcherKey> Boo.Lang.Runtime.DynamicDispatching.DispatcherKey::EqualityComparer
	RuntimeObject* ___EqualityComparer_0;

public:
	inline static int32_t get_offset_of_EqualityComparer_0() { return static_cast<int32_t>(offsetof(DispatcherKey_t708950850_StaticFields, ___EqualityComparer_0)); }
	inline RuntimeObject* get_EqualityComparer_0() const { return ___EqualityComparer_0; }
	inline RuntimeObject** get_address_of_EqualityComparer_0() { return &___EqualityComparer_0; }
	inline void set_EqualityComparer_0(RuntimeObject* value)
	{
		___EqualityComparer_0 = value;
		Il2CppCodeGenWriteBarrier((&___EqualityComparer_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISPATCHERKEY_T708950850_H
#ifndef STACK_T1043988394_H
#define STACK_T1043988394_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Stack
struct  Stack_t1043988394  : public RuntimeObject
{
public:
	// System.Object[] System.Collections.Stack::contents
	ObjectU5BU5D_t3614634134* ___contents_0;
	// System.Int32 System.Collections.Stack::current
	int32_t ___current_1;
	// System.Int32 System.Collections.Stack::count
	int32_t ___count_2;
	// System.Int32 System.Collections.Stack::capacity
	int32_t ___capacity_3;
	// System.Int32 System.Collections.Stack::modCount
	int32_t ___modCount_4;

public:
	inline static int32_t get_offset_of_contents_0() { return static_cast<int32_t>(offsetof(Stack_t1043988394, ___contents_0)); }
	inline ObjectU5BU5D_t3614634134* get_contents_0() const { return ___contents_0; }
	inline ObjectU5BU5D_t3614634134** get_address_of_contents_0() { return &___contents_0; }
	inline void set_contents_0(ObjectU5BU5D_t3614634134* value)
	{
		___contents_0 = value;
		Il2CppCodeGenWriteBarrier((&___contents_0), value);
	}

	inline static int32_t get_offset_of_current_1() { return static_cast<int32_t>(offsetof(Stack_t1043988394, ___current_1)); }
	inline int32_t get_current_1() const { return ___current_1; }
	inline int32_t* get_address_of_current_1() { return &___current_1; }
	inline void set_current_1(int32_t value)
	{
		___current_1 = value;
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(Stack_t1043988394, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_capacity_3() { return static_cast<int32_t>(offsetof(Stack_t1043988394, ___capacity_3)); }
	inline int32_t get_capacity_3() const { return ___capacity_3; }
	inline int32_t* get_address_of_capacity_3() { return &___capacity_3; }
	inline void set_capacity_3(int32_t value)
	{
		___capacity_3 = value;
	}

	inline static int32_t get_offset_of_modCount_4() { return static_cast<int32_t>(offsetof(Stack_t1043988394, ___modCount_4)); }
	inline int32_t get_modCount_4() const { return ___modCount_4; }
	inline int32_t* get_address_of_modCount_4() { return &___modCount_4; }
	inline void set_modCount_4(int32_t value)
	{
		___modCount_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STACK_T1043988394_H
#ifndef DISPATCHERCACHE_T2227862569_H
#define DISPATCHERCACHE_T2227862569_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Boo.Lang.Runtime.DynamicDispatching.DispatcherCache
struct  DispatcherCache_t2227862569  : public RuntimeObject
{
public:

public:
};

struct DispatcherCache_t2227862569_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<Boo.Lang.Runtime.DynamicDispatching.DispatcherKey,Boo.Lang.Runtime.DynamicDispatching.Dispatcher> Boo.Lang.Runtime.DynamicDispatching.DispatcherCache::_cache
	Dictionary_2_t1794457568 * ____cache_0;

public:
	inline static int32_t get_offset_of__cache_0() { return static_cast<int32_t>(offsetof(DispatcherCache_t2227862569_StaticFields, ____cache_0)); }
	inline Dictionary_2_t1794457568 * get__cache_0() const { return ____cache_0; }
	inline Dictionary_2_t1794457568 ** get_address_of__cache_0() { return &____cache_0; }
	inline void set__cache_0(Dictionary_2_t1794457568 * value)
	{
		____cache_0 = value;
		Il2CppCodeGenWriteBarrier((&____cache_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISPATCHERCACHE_T2227862569_H
#ifndef TUIOENTITY_T295276718_H
#define TUIOENTITY_T295276718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TUIOsharp.Entities.TuioEntity
struct  TuioEntity_t295276718  : public RuntimeObject
{
public:
	// System.Int32 TUIOsharp.Entities.TuioEntity::<Id>k__BackingField
	int32_t ___U3CIdU3Ek__BackingField_0;
	// System.Single TUIOsharp.Entities.TuioEntity::<X>k__BackingField
	float ___U3CXU3Ek__BackingField_1;
	// System.Single TUIOsharp.Entities.TuioEntity::<Y>k__BackingField
	float ___U3CYU3Ek__BackingField_2;
	// System.Single TUIOsharp.Entities.TuioEntity::<VelocityX>k__BackingField
	float ___U3CVelocityXU3Ek__BackingField_3;
	// System.Single TUIOsharp.Entities.TuioEntity::<VelocityY>k__BackingField
	float ___U3CVelocityYU3Ek__BackingField_4;
	// System.Single TUIOsharp.Entities.TuioEntity::<Acceleration>k__BackingField
	float ___U3CAccelerationU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CIdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TuioEntity_t295276718, ___U3CIdU3Ek__BackingField_0)); }
	inline int32_t get_U3CIdU3Ek__BackingField_0() const { return ___U3CIdU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CIdU3Ek__BackingField_0() { return &___U3CIdU3Ek__BackingField_0; }
	inline void set_U3CIdU3Ek__BackingField_0(int32_t value)
	{
		___U3CIdU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CXU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TuioEntity_t295276718, ___U3CXU3Ek__BackingField_1)); }
	inline float get_U3CXU3Ek__BackingField_1() const { return ___U3CXU3Ek__BackingField_1; }
	inline float* get_address_of_U3CXU3Ek__BackingField_1() { return &___U3CXU3Ek__BackingField_1; }
	inline void set_U3CXU3Ek__BackingField_1(float value)
	{
		___U3CXU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CYU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(TuioEntity_t295276718, ___U3CYU3Ek__BackingField_2)); }
	inline float get_U3CYU3Ek__BackingField_2() const { return ___U3CYU3Ek__BackingField_2; }
	inline float* get_address_of_U3CYU3Ek__BackingField_2() { return &___U3CYU3Ek__BackingField_2; }
	inline void set_U3CYU3Ek__BackingField_2(float value)
	{
		___U3CYU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CVelocityXU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(TuioEntity_t295276718, ___U3CVelocityXU3Ek__BackingField_3)); }
	inline float get_U3CVelocityXU3Ek__BackingField_3() const { return ___U3CVelocityXU3Ek__BackingField_3; }
	inline float* get_address_of_U3CVelocityXU3Ek__BackingField_3() { return &___U3CVelocityXU3Ek__BackingField_3; }
	inline void set_U3CVelocityXU3Ek__BackingField_3(float value)
	{
		___U3CVelocityXU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CVelocityYU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(TuioEntity_t295276718, ___U3CVelocityYU3Ek__BackingField_4)); }
	inline float get_U3CVelocityYU3Ek__BackingField_4() const { return ___U3CVelocityYU3Ek__BackingField_4; }
	inline float* get_address_of_U3CVelocityYU3Ek__BackingField_4() { return &___U3CVelocityYU3Ek__BackingField_4; }
	inline void set_U3CVelocityYU3Ek__BackingField_4(float value)
	{
		___U3CVelocityYU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CAccelerationU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(TuioEntity_t295276718, ___U3CAccelerationU3Ek__BackingField_5)); }
	inline float get_U3CAccelerationU3Ek__BackingField_5() const { return ___U3CAccelerationU3Ek__BackingField_5; }
	inline float* get_address_of_U3CAccelerationU3Ek__BackingField_5() { return &___U3CAccelerationU3Ek__BackingField_5; }
	inline void set_U3CAccelerationU3Ek__BackingField_5(float value)
	{
		___U3CAccelerationU3Ek__BackingField_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUIOENTITY_T295276718_H
#ifndef NUMERICPROMOTIONS_T3345193737_H
#define NUMERICPROMOTIONS_T3345193737_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Boo.Lang.Runtime.DynamicDispatching.NumericPromotions
struct  NumericPromotions_t3345193737  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NUMERICPROMOTIONS_T3345193737_H
#ifndef BUILTINS_T3763248930_H
#define BUILTINS_T3763248930_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Boo.Lang.Builtins
struct  Builtins_t3763248930  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILTINS_T3763248930_H
#ifndef U3CEMITIMPLICITCONVERSIONDISPATCHERU3EC__ANONSTOREY1E_T187060723_H
#define U3CEMITIMPLICITCONVERSIONDISPATCHERU3EC__ANONSTOREY1E_T187060723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Boo.Lang.Runtime.RuntimeServices/<EmitImplicitConversionDispatcher>c__AnonStorey1E
struct  U3CEmitImplicitConversionDispatcherU3Ec__AnonStorey1E_t187060723  : public RuntimeObject
{
public:
	// System.Reflection.MethodInfo Boo.Lang.Runtime.RuntimeServices/<EmitImplicitConversionDispatcher>c__AnonStorey1E::method
	MethodInfo_t * ___method_0;

public:
	inline static int32_t get_offset_of_method_0() { return static_cast<int32_t>(offsetof(U3CEmitImplicitConversionDispatcherU3Ec__AnonStorey1E_t187060723, ___method_0)); }
	inline MethodInfo_t * get_method_0() const { return ___method_0; }
	inline MethodInfo_t ** get_address_of_method_0() { return &___method_0; }
	inline void set_method_0(MethodInfo_t * value)
	{
		___method_0 = value;
		Il2CppCodeGenWriteBarrier((&___method_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CEMITIMPLICITCONVERSIONDISPATCHERU3EC__ANONSTOREY1E_T187060723_H
#ifndef UTILITIES_T3593287176_H
#define UTILITIES_T3593287176_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HtmlAgilityPack.Utilities
struct  Utilities_t3593287176  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UTILITIES_T3593287176_H
#ifndef HTMLATTRIBUTECOLLECTION_T1787476631_H
#define HTMLATTRIBUTECOLLECTION_T1787476631_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HtmlAgilityPack.HtmlAttributeCollection
struct  HtmlAttributeCollection_t1787476631  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,HtmlAgilityPack.HtmlAttribute> HtmlAgilityPack.HtmlAttributeCollection::Hashitems
	Dictionary_2_t3719302665 * ___Hashitems_0;
	// HtmlAgilityPack.HtmlNode HtmlAgilityPack.HtmlAttributeCollection::_ownernode
	HtmlNode_t2048434459 * ____ownernode_1;
	// System.Collections.Generic.List`1<HtmlAgilityPack.HtmlAttribute> HtmlAgilityPack.HtmlAttributeCollection::items
	List_1_t1173644535 * ___items_2;

public:
	inline static int32_t get_offset_of_Hashitems_0() { return static_cast<int32_t>(offsetof(HtmlAttributeCollection_t1787476631, ___Hashitems_0)); }
	inline Dictionary_2_t3719302665 * get_Hashitems_0() const { return ___Hashitems_0; }
	inline Dictionary_2_t3719302665 ** get_address_of_Hashitems_0() { return &___Hashitems_0; }
	inline void set_Hashitems_0(Dictionary_2_t3719302665 * value)
	{
		___Hashitems_0 = value;
		Il2CppCodeGenWriteBarrier((&___Hashitems_0), value);
	}

	inline static int32_t get_offset_of__ownernode_1() { return static_cast<int32_t>(offsetof(HtmlAttributeCollection_t1787476631, ____ownernode_1)); }
	inline HtmlNode_t2048434459 * get__ownernode_1() const { return ____ownernode_1; }
	inline HtmlNode_t2048434459 ** get_address_of__ownernode_1() { return &____ownernode_1; }
	inline void set__ownernode_1(HtmlNode_t2048434459 * value)
	{
		____ownernode_1 = value;
		Il2CppCodeGenWriteBarrier((&____ownernode_1), value);
	}

	inline static int32_t get_offset_of_items_2() { return static_cast<int32_t>(offsetof(HtmlAttributeCollection_t1787476631, ___items_2)); }
	inline List_1_t1173644535 * get_items_2() const { return ___items_2; }
	inline List_1_t1173644535 ** get_address_of_items_2() { return &___items_2; }
	inline void set_items_2(List_1_t1173644535 * value)
	{
		___items_2 = value;
		Il2CppCodeGenWriteBarrier((&___items_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTMLATTRIBUTECOLLECTION_T1787476631_H
#ifndef BLOBPROCESSOR_T3603341577_H
#define BLOBPROCESSOR_T3603341577_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TUIOsharp.DataProcessors.BlobProcessor
struct  BlobProcessor_t3603341577  : public RuntimeObject
{
public:
	// System.EventHandler`1<TUIOsharp.DataProcessors.TuioBlobEventArgs> TUIOsharp.DataProcessors.BlobProcessor::BlobAdded
	EventHandler_1_t2154285351 * ___BlobAdded_0;
	// System.EventHandler`1<TUIOsharp.DataProcessors.TuioBlobEventArgs> TUIOsharp.DataProcessors.BlobProcessor::BlobUpdated
	EventHandler_1_t2154285351 * ___BlobUpdated_1;
	// System.EventHandler`1<TUIOsharp.DataProcessors.TuioBlobEventArgs> TUIOsharp.DataProcessors.BlobProcessor::BlobRemoved
	EventHandler_1_t2154285351 * ___BlobRemoved_2;
	// System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioBlob> TUIOsharp.DataProcessors.BlobProcessor::blobs
	Dictionary_2_t1054769049 * ___blobs_3;
	// System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob> TUIOsharp.DataProcessors.BlobProcessor::updatedBlobs
	List_1_t1416064546 * ___updatedBlobs_4;
	// System.Collections.Generic.List`1<System.Int32> TUIOsharp.DataProcessors.BlobProcessor::addedBlobs
	List_1_t1440998580 * ___addedBlobs_5;
	// System.Collections.Generic.List`1<System.Int32> TUIOsharp.DataProcessors.BlobProcessor::removedBlobs
	List_1_t1440998580 * ___removedBlobs_6;
	// System.Int32 TUIOsharp.DataProcessors.BlobProcessor::<FrameNumber>k__BackingField
	int32_t ___U3CFrameNumberU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_BlobAdded_0() { return static_cast<int32_t>(offsetof(BlobProcessor_t3603341577, ___BlobAdded_0)); }
	inline EventHandler_1_t2154285351 * get_BlobAdded_0() const { return ___BlobAdded_0; }
	inline EventHandler_1_t2154285351 ** get_address_of_BlobAdded_0() { return &___BlobAdded_0; }
	inline void set_BlobAdded_0(EventHandler_1_t2154285351 * value)
	{
		___BlobAdded_0 = value;
		Il2CppCodeGenWriteBarrier((&___BlobAdded_0), value);
	}

	inline static int32_t get_offset_of_BlobUpdated_1() { return static_cast<int32_t>(offsetof(BlobProcessor_t3603341577, ___BlobUpdated_1)); }
	inline EventHandler_1_t2154285351 * get_BlobUpdated_1() const { return ___BlobUpdated_1; }
	inline EventHandler_1_t2154285351 ** get_address_of_BlobUpdated_1() { return &___BlobUpdated_1; }
	inline void set_BlobUpdated_1(EventHandler_1_t2154285351 * value)
	{
		___BlobUpdated_1 = value;
		Il2CppCodeGenWriteBarrier((&___BlobUpdated_1), value);
	}

	inline static int32_t get_offset_of_BlobRemoved_2() { return static_cast<int32_t>(offsetof(BlobProcessor_t3603341577, ___BlobRemoved_2)); }
	inline EventHandler_1_t2154285351 * get_BlobRemoved_2() const { return ___BlobRemoved_2; }
	inline EventHandler_1_t2154285351 ** get_address_of_BlobRemoved_2() { return &___BlobRemoved_2; }
	inline void set_BlobRemoved_2(EventHandler_1_t2154285351 * value)
	{
		___BlobRemoved_2 = value;
		Il2CppCodeGenWriteBarrier((&___BlobRemoved_2), value);
	}

	inline static int32_t get_offset_of_blobs_3() { return static_cast<int32_t>(offsetof(BlobProcessor_t3603341577, ___blobs_3)); }
	inline Dictionary_2_t1054769049 * get_blobs_3() const { return ___blobs_3; }
	inline Dictionary_2_t1054769049 ** get_address_of_blobs_3() { return &___blobs_3; }
	inline void set_blobs_3(Dictionary_2_t1054769049 * value)
	{
		___blobs_3 = value;
		Il2CppCodeGenWriteBarrier((&___blobs_3), value);
	}

	inline static int32_t get_offset_of_updatedBlobs_4() { return static_cast<int32_t>(offsetof(BlobProcessor_t3603341577, ___updatedBlobs_4)); }
	inline List_1_t1416064546 * get_updatedBlobs_4() const { return ___updatedBlobs_4; }
	inline List_1_t1416064546 ** get_address_of_updatedBlobs_4() { return &___updatedBlobs_4; }
	inline void set_updatedBlobs_4(List_1_t1416064546 * value)
	{
		___updatedBlobs_4 = value;
		Il2CppCodeGenWriteBarrier((&___updatedBlobs_4), value);
	}

	inline static int32_t get_offset_of_addedBlobs_5() { return static_cast<int32_t>(offsetof(BlobProcessor_t3603341577, ___addedBlobs_5)); }
	inline List_1_t1440998580 * get_addedBlobs_5() const { return ___addedBlobs_5; }
	inline List_1_t1440998580 ** get_address_of_addedBlobs_5() { return &___addedBlobs_5; }
	inline void set_addedBlobs_5(List_1_t1440998580 * value)
	{
		___addedBlobs_5 = value;
		Il2CppCodeGenWriteBarrier((&___addedBlobs_5), value);
	}

	inline static int32_t get_offset_of_removedBlobs_6() { return static_cast<int32_t>(offsetof(BlobProcessor_t3603341577, ___removedBlobs_6)); }
	inline List_1_t1440998580 * get_removedBlobs_6() const { return ___removedBlobs_6; }
	inline List_1_t1440998580 ** get_address_of_removedBlobs_6() { return &___removedBlobs_6; }
	inline void set_removedBlobs_6(List_1_t1440998580 * value)
	{
		___removedBlobs_6 = value;
		Il2CppCodeGenWriteBarrier((&___removedBlobs_6), value);
	}

	inline static int32_t get_offset_of_U3CFrameNumberU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(BlobProcessor_t3603341577, ___U3CFrameNumberU3Ek__BackingField_7)); }
	inline int32_t get_U3CFrameNumberU3Ek__BackingField_7() const { return ___U3CFrameNumberU3Ek__BackingField_7; }
	inline int32_t* get_address_of_U3CFrameNumberU3Ek__BackingField_7() { return &___U3CFrameNumberU3Ek__BackingField_7; }
	inline void set_U3CFrameNumberU3Ek__BackingField_7(int32_t value)
	{
		___U3CFrameNumberU3Ek__BackingField_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOBPROCESSOR_T3603341577_H
#ifndef OBJECTPROCESSOR_T221569383_H
#define OBJECTPROCESSOR_T221569383_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TUIOsharp.DataProcessors.ObjectProcessor
struct  ObjectProcessor_t221569383  : public RuntimeObject
{
public:
	// System.EventHandler`1<TUIOsharp.DataProcessors.TuioObjectEventArgs> TUIOsharp.DataProcessors.ObjectProcessor::ObjectAdded
	EventHandler_1_t402050513 * ___ObjectAdded_0;
	// System.EventHandler`1<TUIOsharp.DataProcessors.TuioObjectEventArgs> TUIOsharp.DataProcessors.ObjectProcessor::ObjectUpdated
	EventHandler_1_t402050513 * ___ObjectUpdated_1;
	// System.EventHandler`1<TUIOsharp.DataProcessors.TuioObjectEventArgs> TUIOsharp.DataProcessors.ObjectProcessor::ObjectRemoved
	EventHandler_1_t402050513 * ___ObjectRemoved_2;
	// System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioObject> TUIOsharp.DataProcessors.ObjectProcessor::objects
	Dictionary_2_t244646649 * ___objects_3;
	// System.Collections.Generic.List`1<TUIOsharp.Entities.TuioObject> TUIOsharp.DataProcessors.ObjectProcessor::updatedObjects
	List_1_t605942146 * ___updatedObjects_4;
	// System.Collections.Generic.List`1<System.Int32> TUIOsharp.DataProcessors.ObjectProcessor::addedObjects
	List_1_t1440998580 * ___addedObjects_5;
	// System.Collections.Generic.List`1<System.Int32> TUIOsharp.DataProcessors.ObjectProcessor::removedObjects
	List_1_t1440998580 * ___removedObjects_6;
	// System.Int32 TUIOsharp.DataProcessors.ObjectProcessor::<FrameNumber>k__BackingField
	int32_t ___U3CFrameNumberU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_ObjectAdded_0() { return static_cast<int32_t>(offsetof(ObjectProcessor_t221569383, ___ObjectAdded_0)); }
	inline EventHandler_1_t402050513 * get_ObjectAdded_0() const { return ___ObjectAdded_0; }
	inline EventHandler_1_t402050513 ** get_address_of_ObjectAdded_0() { return &___ObjectAdded_0; }
	inline void set_ObjectAdded_0(EventHandler_1_t402050513 * value)
	{
		___ObjectAdded_0 = value;
		Il2CppCodeGenWriteBarrier((&___ObjectAdded_0), value);
	}

	inline static int32_t get_offset_of_ObjectUpdated_1() { return static_cast<int32_t>(offsetof(ObjectProcessor_t221569383, ___ObjectUpdated_1)); }
	inline EventHandler_1_t402050513 * get_ObjectUpdated_1() const { return ___ObjectUpdated_1; }
	inline EventHandler_1_t402050513 ** get_address_of_ObjectUpdated_1() { return &___ObjectUpdated_1; }
	inline void set_ObjectUpdated_1(EventHandler_1_t402050513 * value)
	{
		___ObjectUpdated_1 = value;
		Il2CppCodeGenWriteBarrier((&___ObjectUpdated_1), value);
	}

	inline static int32_t get_offset_of_ObjectRemoved_2() { return static_cast<int32_t>(offsetof(ObjectProcessor_t221569383, ___ObjectRemoved_2)); }
	inline EventHandler_1_t402050513 * get_ObjectRemoved_2() const { return ___ObjectRemoved_2; }
	inline EventHandler_1_t402050513 ** get_address_of_ObjectRemoved_2() { return &___ObjectRemoved_2; }
	inline void set_ObjectRemoved_2(EventHandler_1_t402050513 * value)
	{
		___ObjectRemoved_2 = value;
		Il2CppCodeGenWriteBarrier((&___ObjectRemoved_2), value);
	}

	inline static int32_t get_offset_of_objects_3() { return static_cast<int32_t>(offsetof(ObjectProcessor_t221569383, ___objects_3)); }
	inline Dictionary_2_t244646649 * get_objects_3() const { return ___objects_3; }
	inline Dictionary_2_t244646649 ** get_address_of_objects_3() { return &___objects_3; }
	inline void set_objects_3(Dictionary_2_t244646649 * value)
	{
		___objects_3 = value;
		Il2CppCodeGenWriteBarrier((&___objects_3), value);
	}

	inline static int32_t get_offset_of_updatedObjects_4() { return static_cast<int32_t>(offsetof(ObjectProcessor_t221569383, ___updatedObjects_4)); }
	inline List_1_t605942146 * get_updatedObjects_4() const { return ___updatedObjects_4; }
	inline List_1_t605942146 ** get_address_of_updatedObjects_4() { return &___updatedObjects_4; }
	inline void set_updatedObjects_4(List_1_t605942146 * value)
	{
		___updatedObjects_4 = value;
		Il2CppCodeGenWriteBarrier((&___updatedObjects_4), value);
	}

	inline static int32_t get_offset_of_addedObjects_5() { return static_cast<int32_t>(offsetof(ObjectProcessor_t221569383, ___addedObjects_5)); }
	inline List_1_t1440998580 * get_addedObjects_5() const { return ___addedObjects_5; }
	inline List_1_t1440998580 ** get_address_of_addedObjects_5() { return &___addedObjects_5; }
	inline void set_addedObjects_5(List_1_t1440998580 * value)
	{
		___addedObjects_5 = value;
		Il2CppCodeGenWriteBarrier((&___addedObjects_5), value);
	}

	inline static int32_t get_offset_of_removedObjects_6() { return static_cast<int32_t>(offsetof(ObjectProcessor_t221569383, ___removedObjects_6)); }
	inline List_1_t1440998580 * get_removedObjects_6() const { return ___removedObjects_6; }
	inline List_1_t1440998580 ** get_address_of_removedObjects_6() { return &___removedObjects_6; }
	inline void set_removedObjects_6(List_1_t1440998580 * value)
	{
		___removedObjects_6 = value;
		Il2CppCodeGenWriteBarrier((&___removedObjects_6), value);
	}

	inline static int32_t get_offset_of_U3CFrameNumberU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(ObjectProcessor_t221569383, ___U3CFrameNumberU3Ek__BackingField_7)); }
	inline int32_t get_U3CFrameNumberU3Ek__BackingField_7() const { return ___U3CFrameNumberU3Ek__BackingField_7; }
	inline int32_t* get_address_of_U3CFrameNumberU3Ek__BackingField_7() { return &___U3CFrameNumberU3Ek__BackingField_7; }
	inline void set_U3CFrameNumberU3Ek__BackingField_7(int32_t value)
	{
		___U3CFrameNumberU3Ek__BackingField_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTPROCESSOR_T221569383_H
#ifndef CURSORPROCESSOR_T1785954004_H
#define CURSORPROCESSOR_T1785954004_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TUIOsharp.DataProcessors.CursorProcessor
struct  CursorProcessor_t1785954004  : public RuntimeObject
{
public:
	// System.EventHandler`1<TUIOsharp.DataProcessors.TuioCursorEventArgs> TUIOsharp.DataProcessors.CursorProcessor::CursorAdded
	EventHandler_1_t3249264480 * ___CursorAdded_0;
	// System.EventHandler`1<TUIOsharp.DataProcessors.TuioCursorEventArgs> TUIOsharp.DataProcessors.CursorProcessor::CursorUpdated
	EventHandler_1_t3249264480 * ___CursorUpdated_1;
	// System.EventHandler`1<TUIOsharp.DataProcessors.TuioCursorEventArgs> TUIOsharp.DataProcessors.CursorProcessor::CursorRemoved
	EventHandler_1_t3249264480 * ___CursorRemoved_2;
	// System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioCursor> TUIOsharp.DataProcessors.CursorProcessor::cursors
	Dictionary_2_t858177054 * ___cursors_3;
	// System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor> TUIOsharp.DataProcessors.CursorProcessor::updatedCursors
	List_1_t1219472551 * ___updatedCursors_4;
	// System.Collections.Generic.List`1<System.Int32> TUIOsharp.DataProcessors.CursorProcessor::addedCursors
	List_1_t1440998580 * ___addedCursors_5;
	// System.Collections.Generic.List`1<System.Int32> TUIOsharp.DataProcessors.CursorProcessor::removedCursors
	List_1_t1440998580 * ___removedCursors_6;
	// System.Int32 TUIOsharp.DataProcessors.CursorProcessor::<FrameNumber>k__BackingField
	int32_t ___U3CFrameNumberU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_CursorAdded_0() { return static_cast<int32_t>(offsetof(CursorProcessor_t1785954004, ___CursorAdded_0)); }
	inline EventHandler_1_t3249264480 * get_CursorAdded_0() const { return ___CursorAdded_0; }
	inline EventHandler_1_t3249264480 ** get_address_of_CursorAdded_0() { return &___CursorAdded_0; }
	inline void set_CursorAdded_0(EventHandler_1_t3249264480 * value)
	{
		___CursorAdded_0 = value;
		Il2CppCodeGenWriteBarrier((&___CursorAdded_0), value);
	}

	inline static int32_t get_offset_of_CursorUpdated_1() { return static_cast<int32_t>(offsetof(CursorProcessor_t1785954004, ___CursorUpdated_1)); }
	inline EventHandler_1_t3249264480 * get_CursorUpdated_1() const { return ___CursorUpdated_1; }
	inline EventHandler_1_t3249264480 ** get_address_of_CursorUpdated_1() { return &___CursorUpdated_1; }
	inline void set_CursorUpdated_1(EventHandler_1_t3249264480 * value)
	{
		___CursorUpdated_1 = value;
		Il2CppCodeGenWriteBarrier((&___CursorUpdated_1), value);
	}

	inline static int32_t get_offset_of_CursorRemoved_2() { return static_cast<int32_t>(offsetof(CursorProcessor_t1785954004, ___CursorRemoved_2)); }
	inline EventHandler_1_t3249264480 * get_CursorRemoved_2() const { return ___CursorRemoved_2; }
	inline EventHandler_1_t3249264480 ** get_address_of_CursorRemoved_2() { return &___CursorRemoved_2; }
	inline void set_CursorRemoved_2(EventHandler_1_t3249264480 * value)
	{
		___CursorRemoved_2 = value;
		Il2CppCodeGenWriteBarrier((&___CursorRemoved_2), value);
	}

	inline static int32_t get_offset_of_cursors_3() { return static_cast<int32_t>(offsetof(CursorProcessor_t1785954004, ___cursors_3)); }
	inline Dictionary_2_t858177054 * get_cursors_3() const { return ___cursors_3; }
	inline Dictionary_2_t858177054 ** get_address_of_cursors_3() { return &___cursors_3; }
	inline void set_cursors_3(Dictionary_2_t858177054 * value)
	{
		___cursors_3 = value;
		Il2CppCodeGenWriteBarrier((&___cursors_3), value);
	}

	inline static int32_t get_offset_of_updatedCursors_4() { return static_cast<int32_t>(offsetof(CursorProcessor_t1785954004, ___updatedCursors_4)); }
	inline List_1_t1219472551 * get_updatedCursors_4() const { return ___updatedCursors_4; }
	inline List_1_t1219472551 ** get_address_of_updatedCursors_4() { return &___updatedCursors_4; }
	inline void set_updatedCursors_4(List_1_t1219472551 * value)
	{
		___updatedCursors_4 = value;
		Il2CppCodeGenWriteBarrier((&___updatedCursors_4), value);
	}

	inline static int32_t get_offset_of_addedCursors_5() { return static_cast<int32_t>(offsetof(CursorProcessor_t1785954004, ___addedCursors_5)); }
	inline List_1_t1440998580 * get_addedCursors_5() const { return ___addedCursors_5; }
	inline List_1_t1440998580 ** get_address_of_addedCursors_5() { return &___addedCursors_5; }
	inline void set_addedCursors_5(List_1_t1440998580 * value)
	{
		___addedCursors_5 = value;
		Il2CppCodeGenWriteBarrier((&___addedCursors_5), value);
	}

	inline static int32_t get_offset_of_removedCursors_6() { return static_cast<int32_t>(offsetof(CursorProcessor_t1785954004, ___removedCursors_6)); }
	inline List_1_t1440998580 * get_removedCursors_6() const { return ___removedCursors_6; }
	inline List_1_t1440998580 ** get_address_of_removedCursors_6() { return &___removedCursors_6; }
	inline void set_removedCursors_6(List_1_t1440998580 * value)
	{
		___removedCursors_6 = value;
		Il2CppCodeGenWriteBarrier((&___removedCursors_6), value);
	}

	inline static int32_t get_offset_of_U3CFrameNumberU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(CursorProcessor_t1785954004, ___U3CFrameNumberU3Ek__BackingField_7)); }
	inline int32_t get_U3CFrameNumberU3Ek__BackingField_7() const { return ___U3CFrameNumberU3Ek__BackingField_7; }
	inline int32_t* get_address_of_U3CFrameNumberU3Ek__BackingField_7() { return &___U3CFrameNumberU3Ek__BackingField_7; }
	inline void set_U3CFrameNumberU3Ek__BackingField_7(int32_t value)
	{
		___U3CFrameNumberU3Ek__BackingField_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURSORPROCESSOR_T1785954004_H
#ifndef CRC32_T83196531_H
#define CRC32_T83196531_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HtmlAgilityPack.Crc32
struct  Crc32_t83196531  : public RuntimeObject
{
public:
	// System.UInt32 HtmlAgilityPack.Crc32::_crc32
	uint32_t ____crc32_0;

public:
	inline static int32_t get_offset_of__crc32_0() { return static_cast<int32_t>(offsetof(Crc32_t83196531, ____crc32_0)); }
	inline uint32_t get__crc32_0() const { return ____crc32_0; }
	inline uint32_t* get_address_of__crc32_0() { return &____crc32_0; }
	inline void set__crc32_0(uint32_t value)
	{
		____crc32_0 = value;
	}
};

struct Crc32_t83196531_StaticFields
{
public:
	// System.UInt32[] HtmlAgilityPack.Crc32::crc_32_tab
	UInt32U5BU5D_t59386216* ___crc_32_tab_1;

public:
	inline static int32_t get_offset_of_crc_32_tab_1() { return static_cast<int32_t>(offsetof(Crc32_t83196531_StaticFields, ___crc_32_tab_1)); }
	inline UInt32U5BU5D_t59386216* get_crc_32_tab_1() const { return ___crc_32_tab_1; }
	inline UInt32U5BU5D_t59386216** get_address_of_crc_32_tab_1() { return &___crc_32_tab_1; }
	inline void set_crc_32_tab_1(UInt32U5BU5D_t59386216* value)
	{
		___crc_32_tab_1 = value;
		Il2CppCodeGenWriteBarrier((&___crc_32_tab_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRC32_T83196531_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef ATTRIBUTE_T542643598_H
#define ATTRIBUTE_T542643598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t542643598  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T542643598_H
#ifndef HTMLNODECOLLECTION_T2542734491_H
#define HTMLNODECOLLECTION_T2542734491_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HtmlAgilityPack.HtmlNodeCollection
struct  HtmlNodeCollection_t2542734491  : public RuntimeObject
{
public:
	// HtmlAgilityPack.HtmlNode HtmlAgilityPack.HtmlNodeCollection::_parentnode
	HtmlNode_t2048434459 * ____parentnode_0;
	// System.Collections.Generic.List`1<HtmlAgilityPack.HtmlNode> HtmlAgilityPack.HtmlNodeCollection::_items
	List_1_t1417555591 * ____items_1;

public:
	inline static int32_t get_offset_of__parentnode_0() { return static_cast<int32_t>(offsetof(HtmlNodeCollection_t2542734491, ____parentnode_0)); }
	inline HtmlNode_t2048434459 * get__parentnode_0() const { return ____parentnode_0; }
	inline HtmlNode_t2048434459 ** get_address_of__parentnode_0() { return &____parentnode_0; }
	inline void set__parentnode_0(HtmlNode_t2048434459 * value)
	{
		____parentnode_0 = value;
		Il2CppCodeGenWriteBarrier((&____parentnode_0), value);
	}

	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(HtmlNodeCollection_t2542734491, ____items_1)); }
	inline List_1_t1417555591 * get__items_1() const { return ____items_1; }
	inline List_1_t1417555591 ** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(List_1_t1417555591 * value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTMLNODECOLLECTION_T2542734491_H
#ifndef TUIOSERVER_T595520884_H
#define TUIOSERVER_T595520884_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TUIOsharp.TuioServer
struct  TuioServer_t595520884  : public RuntimeObject
{
public:
	// System.EventHandler`1<OSCsharp.Utils.ExceptionEventArgs> TUIOsharp.TuioServer::ErrorOccured
	EventHandler_1_t3778988004 * ___ErrorOccured_0;
	// OSCsharp.Net.UDPReceiver TUIOsharp.TuioServer::udpReceiver
	UDPReceiver_t3846109956 * ___udpReceiver_1;
	// System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor> TUIOsharp.TuioServer::processors
	List_1_t1583176531 * ___processors_2;
	// System.Int32 TUIOsharp.TuioServer::<Port>k__BackingField
	int32_t ___U3CPortU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_ErrorOccured_0() { return static_cast<int32_t>(offsetof(TuioServer_t595520884, ___ErrorOccured_0)); }
	inline EventHandler_1_t3778988004 * get_ErrorOccured_0() const { return ___ErrorOccured_0; }
	inline EventHandler_1_t3778988004 ** get_address_of_ErrorOccured_0() { return &___ErrorOccured_0; }
	inline void set_ErrorOccured_0(EventHandler_1_t3778988004 * value)
	{
		___ErrorOccured_0 = value;
		Il2CppCodeGenWriteBarrier((&___ErrorOccured_0), value);
	}

	inline static int32_t get_offset_of_udpReceiver_1() { return static_cast<int32_t>(offsetof(TuioServer_t595520884, ___udpReceiver_1)); }
	inline UDPReceiver_t3846109956 * get_udpReceiver_1() const { return ___udpReceiver_1; }
	inline UDPReceiver_t3846109956 ** get_address_of_udpReceiver_1() { return &___udpReceiver_1; }
	inline void set_udpReceiver_1(UDPReceiver_t3846109956 * value)
	{
		___udpReceiver_1 = value;
		Il2CppCodeGenWriteBarrier((&___udpReceiver_1), value);
	}

	inline static int32_t get_offset_of_processors_2() { return static_cast<int32_t>(offsetof(TuioServer_t595520884, ___processors_2)); }
	inline List_1_t1583176531 * get_processors_2() const { return ___processors_2; }
	inline List_1_t1583176531 ** get_address_of_processors_2() { return &___processors_2; }
	inline void set_processors_2(List_1_t1583176531 * value)
	{
		___processors_2 = value;
		Il2CppCodeGenWriteBarrier((&___processors_2), value);
	}

	inline static int32_t get_offset_of_U3CPortU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(TuioServer_t595520884, ___U3CPortU3Ek__BackingField_3)); }
	inline int32_t get_U3CPortU3Ek__BackingField_3() const { return ___U3CPortU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CPortU3Ek__BackingField_3() { return &___U3CPortU3Ek__BackingField_3; }
	inline void set_U3CPortU3Ek__BackingField_3(int32_t value)
	{
		___U3CPortU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUIOSERVER_T595520884_H
#ifndef NAMEVALUEPAIRLIST_T1351137418_H
#define NAMEVALUEPAIRLIST_T1351137418_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HtmlAgilityPack.NameValuePairList
struct  NameValuePairList_t1351137418  : public RuntimeObject
{
public:
	// System.String HtmlAgilityPack.NameValuePairList::Text
	String_t* ___Text_0;
	// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>> HtmlAgilityPack.NameValuePairList::_allPairs
	List_1_t1070465849 * ____allPairs_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>> HtmlAgilityPack.NameValuePairList::_pairsWithName
	Dictionary_2_t2985245111 * ____pairsWithName_2;

public:
	inline static int32_t get_offset_of_Text_0() { return static_cast<int32_t>(offsetof(NameValuePairList_t1351137418, ___Text_0)); }
	inline String_t* get_Text_0() const { return ___Text_0; }
	inline String_t** get_address_of_Text_0() { return &___Text_0; }
	inline void set_Text_0(String_t* value)
	{
		___Text_0 = value;
		Il2CppCodeGenWriteBarrier((&___Text_0), value);
	}

	inline static int32_t get_offset_of__allPairs_1() { return static_cast<int32_t>(offsetof(NameValuePairList_t1351137418, ____allPairs_1)); }
	inline List_1_t1070465849 * get__allPairs_1() const { return ____allPairs_1; }
	inline List_1_t1070465849 ** get_address_of__allPairs_1() { return &____allPairs_1; }
	inline void set__allPairs_1(List_1_t1070465849 * value)
	{
		____allPairs_1 = value;
		Il2CppCodeGenWriteBarrier((&____allPairs_1), value);
	}

	inline static int32_t get_offset_of__pairsWithName_2() { return static_cast<int32_t>(offsetof(NameValuePairList_t1351137418, ____pairsWithName_2)); }
	inline Dictionary_2_t2985245111 * get__pairsWithName_2() const { return ____pairsWithName_2; }
	inline Dictionary_2_t2985245111 ** get_address_of__pairsWithName_2() { return &____pairsWithName_2; }
	inline void set__pairsWithName_2(Dictionary_2_t2985245111 * value)
	{
		____pairsWithName_2 = value;
		Il2CppCodeGenWriteBarrier((&____pairsWithName_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMEVALUEPAIRLIST_T1351137418_H
#ifndef EXCEPTION_T1927440687_H
#define EXCEPTION_T1927440687_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t1927440687  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t169632028* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t1927440687 * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t169632028* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t169632028** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t169632028* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___inner_exception_1)); }
	inline Exception_t1927440687 * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t1927440687 ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t1927440687 * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T1927440687_H
#ifndef EVENTARGS_T3289624707_H
#define EVENTARGS_T3289624707_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventArgs
struct  EventArgs_t3289624707  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t3289624707_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t3289624707 * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t3289624707_StaticFields, ___Empty_0)); }
	inline EventArgs_t3289624707 * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t3289624707 ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t3289624707 * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTARGS_T3289624707_H
#ifndef MESSAGETYPESUBSCRIBERS_T2291506050_H
#define MESSAGETYPESUBSCRIBERS_T2291506050_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers
struct  MessageTypeSubscribers_t2291506050  : public RuntimeObject
{
public:
	// System.String UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers::m_messageTypeId
	String_t* ___m_messageTypeId_0;
	// System.Int32 UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers::subscriberCount
	int32_t ___subscriberCount_1;
	// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageEvent UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers::messageCallback
	MessageEvent_t2167079021 * ___messageCallback_2;

public:
	inline static int32_t get_offset_of_m_messageTypeId_0() { return static_cast<int32_t>(offsetof(MessageTypeSubscribers_t2291506050, ___m_messageTypeId_0)); }
	inline String_t* get_m_messageTypeId_0() const { return ___m_messageTypeId_0; }
	inline String_t** get_address_of_m_messageTypeId_0() { return &___m_messageTypeId_0; }
	inline void set_m_messageTypeId_0(String_t* value)
	{
		___m_messageTypeId_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_messageTypeId_0), value);
	}

	inline static int32_t get_offset_of_subscriberCount_1() { return static_cast<int32_t>(offsetof(MessageTypeSubscribers_t2291506050, ___subscriberCount_1)); }
	inline int32_t get_subscriberCount_1() const { return ___subscriberCount_1; }
	inline int32_t* get_address_of_subscriberCount_1() { return &___subscriberCount_1; }
	inline void set_subscriberCount_1(int32_t value)
	{
		___subscriberCount_1 = value;
	}

	inline static int32_t get_offset_of_messageCallback_2() { return static_cast<int32_t>(offsetof(MessageTypeSubscribers_t2291506050, ___messageCallback_2)); }
	inline MessageEvent_t2167079021 * get_messageCallback_2() const { return ___messageCallback_2; }
	inline MessageEvent_t2167079021 ** get_address_of_messageCallback_2() { return &___messageCallback_2; }
	inline void set_messageCallback_2(MessageEvent_t2167079021 * value)
	{
		___messageCallback_2 = value;
		Il2CppCodeGenWriteBarrier((&___messageCallback_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGETYPESUBSCRIBERS_T2291506050_H
#ifndef RENDERPIPELINEMANAGER_T984453155_H
#define RENDERPIPELINEMANAGER_T984453155_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Rendering.RenderPipelineManager
struct  RenderPipelineManager_t984453155  : public RuntimeObject
{
public:

public:
};

struct RenderPipelineManager_t984453155_StaticFields
{
public:
	// UnityEngine.Experimental.Rendering.IRenderPipelineAsset UnityEngine.Experimental.Rendering.RenderPipelineManager::s_CurrentPipelineAsset
	RuntimeObject* ___s_CurrentPipelineAsset_0;
	// UnityEngine.Experimental.Rendering.IRenderPipeline UnityEngine.Experimental.Rendering.RenderPipelineManager::<currentPipeline>k__BackingField
	RuntimeObject* ___U3CcurrentPipelineU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_s_CurrentPipelineAsset_0() { return static_cast<int32_t>(offsetof(RenderPipelineManager_t984453155_StaticFields, ___s_CurrentPipelineAsset_0)); }
	inline RuntimeObject* get_s_CurrentPipelineAsset_0() const { return ___s_CurrentPipelineAsset_0; }
	inline RuntimeObject** get_address_of_s_CurrentPipelineAsset_0() { return &___s_CurrentPipelineAsset_0; }
	inline void set_s_CurrentPipelineAsset_0(RuntimeObject* value)
	{
		___s_CurrentPipelineAsset_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_CurrentPipelineAsset_0), value);
	}

	inline static int32_t get_offset_of_U3CcurrentPipelineU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RenderPipelineManager_t984453155_StaticFields, ___U3CcurrentPipelineU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CcurrentPipelineU3Ek__BackingField_1() const { return ___U3CcurrentPipelineU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CcurrentPipelineU3Ek__BackingField_1() { return &___U3CcurrentPipelineU3Ek__BackingField_1; }
	inline void set_U3CcurrentPipelineU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CcurrentPipelineU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcurrentPipelineU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERPIPELINEMANAGER_T984453155_H
#ifndef XMLNAMETABLE_T1345805268_H
#define XMLNAMETABLE_T1345805268_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNameTable
struct  XmlNameTable_t1345805268  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNAMETABLE_T1345805268_H
#ifndef PLAYEREDITORCONNECTIONEVENTS_T2252784345_H
#define PLAYEREDITORCONNECTIONEVENTS_T2252784345_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents
struct  PlayerEditorConnectionEvents_t2252784345  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers> UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents::messageTypeSubscribers
	List_1_t1660627182 * ___messageTypeSubscribers_0;
	// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/ConnectionChangeEvent UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents::connectionEvent
	ConnectionChangeEvent_t536719976 * ___connectionEvent_1;
	// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/ConnectionChangeEvent UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents::disconnectionEvent
	ConnectionChangeEvent_t536719976 * ___disconnectionEvent_2;

public:
	inline static int32_t get_offset_of_messageTypeSubscribers_0() { return static_cast<int32_t>(offsetof(PlayerEditorConnectionEvents_t2252784345, ___messageTypeSubscribers_0)); }
	inline List_1_t1660627182 * get_messageTypeSubscribers_0() const { return ___messageTypeSubscribers_0; }
	inline List_1_t1660627182 ** get_address_of_messageTypeSubscribers_0() { return &___messageTypeSubscribers_0; }
	inline void set_messageTypeSubscribers_0(List_1_t1660627182 * value)
	{
		___messageTypeSubscribers_0 = value;
		Il2CppCodeGenWriteBarrier((&___messageTypeSubscribers_0), value);
	}

	inline static int32_t get_offset_of_connectionEvent_1() { return static_cast<int32_t>(offsetof(PlayerEditorConnectionEvents_t2252784345, ___connectionEvent_1)); }
	inline ConnectionChangeEvent_t536719976 * get_connectionEvent_1() const { return ___connectionEvent_1; }
	inline ConnectionChangeEvent_t536719976 ** get_address_of_connectionEvent_1() { return &___connectionEvent_1; }
	inline void set_connectionEvent_1(ConnectionChangeEvent_t536719976 * value)
	{
		___connectionEvent_1 = value;
		Il2CppCodeGenWriteBarrier((&___connectionEvent_1), value);
	}

	inline static int32_t get_offset_of_disconnectionEvent_2() { return static_cast<int32_t>(offsetof(PlayerEditorConnectionEvents_t2252784345, ___disconnectionEvent_2)); }
	inline ConnectionChangeEvent_t536719976 * get_disconnectionEvent_2() const { return ___disconnectionEvent_2; }
	inline ConnectionChangeEvent_t536719976 ** get_address_of_disconnectionEvent_2() { return &___disconnectionEvent_2; }
	inline void set_disconnectionEvent_2(ConnectionChangeEvent_t536719976 * value)
	{
		___disconnectionEvent_2 = value;
		Il2CppCodeGenWriteBarrier((&___disconnectionEvent_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYEREDITORCONNECTIONEVENTS_T2252784345_H
#ifndef PLAYABLEBEHAVIOUR_T2517866383_H
#define PLAYABLEBEHAVIOUR_T2517866383_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableBehaviour
struct  PlayableBehaviour_t2517866383  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEBEHAVIOUR_T2517866383_H
#ifndef XPATHITEM_T3130801258_H
#define XPATHITEM_T3130801258_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathItem
struct  XPathItem_t3130801258  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHITEM_T3130801258_H
#ifndef PLAYABLEEXTENSIONS_T2358977238_H
#define PLAYABLEEXTENSIONS_T2358977238_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableExtensions
struct  PlayableExtensions_t2358977238  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEEXTENSIONS_T2358977238_H
#ifndef MESSAGEEVENTARGS_T301283622_H
#define MESSAGEEVENTARGS_T301283622_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.PlayerConnection.MessageEventArgs
struct  MessageEventArgs_t301283622  : public RuntimeObject
{
public:
	// System.Int32 UnityEngine.Networking.PlayerConnection.MessageEventArgs::playerId
	int32_t ___playerId_0;
	// System.Byte[] UnityEngine.Networking.PlayerConnection.MessageEventArgs::data
	ByteU5BU5D_t3397334013* ___data_1;

public:
	inline static int32_t get_offset_of_playerId_0() { return static_cast<int32_t>(offsetof(MessageEventArgs_t301283622, ___playerId_0)); }
	inline int32_t get_playerId_0() const { return ___playerId_0; }
	inline int32_t* get_address_of_playerId_0() { return &___playerId_0; }
	inline void set_playerId_0(int32_t value)
	{
		___playerId_0 = value;
	}

	inline static int32_t get_offset_of_data_1() { return static_cast<int32_t>(offsetof(MessageEventArgs_t301283622, ___data_1)); }
	inline ByteU5BU5D_t3397334013* get_data_1() const { return ___data_1; }
	inline ByteU5BU5D_t3397334013** get_address_of_data_1() { return &___data_1; }
	inline void set_data_1(ByteU5BU5D_t3397334013* value)
	{
		___data_1 = value;
		Il2CppCodeGenWriteBarrier((&___data_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGEEVENTARGS_T301283622_H
#ifndef UNITYEVENTBASE_T828812576_H
#define UNITYEVENTBASE_T828812576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t828812576  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t2295673753 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t339478082 * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t828812576, ___m_Calls_0)); }
	inline InvokableCallList_t2295673753 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t2295673753 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t2295673753 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t828812576, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t339478082 * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t339478082 ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t339478082 * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t828812576, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t828812576, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T828812576_H
#ifndef NATIVE_T2308112383_H
#define NATIVE_T2308112383_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CSSLayout.Native
struct  Native_t2308112383  : public RuntimeObject
{
public:

public:
};

struct Native_t2308112383_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.IntPtr,System.WeakReference> UnityEngine.CSSLayout.Native::s_MeasureFunctions
	Dictionary_2_t1519430885 * ___s_MeasureFunctions_0;

public:
	inline static int32_t get_offset_of_s_MeasureFunctions_0() { return static_cast<int32_t>(offsetof(Native_t2308112383_StaticFields, ___s_MeasureFunctions_0)); }
	inline Dictionary_2_t1519430885 * get_s_MeasureFunctions_0() const { return ___s_MeasureFunctions_0; }
	inline Dictionary_2_t1519430885 ** get_address_of_s_MeasureFunctions_0() { return &___s_MeasureFunctions_0; }
	inline void set_s_MeasureFunctions_0(Dictionary_2_t1519430885 * value)
	{
		___s_MeasureFunctions_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_MeasureFunctions_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVE_T2308112383_H
#ifndef NETFXCOREEXTENSIONS_T4275971970_H
#define NETFXCOREEXTENSIONS_T4275971970_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngineInternal.NetFxCoreExtensions
struct  NetFxCoreExtensions_t4275971970  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETFXCOREEXTENSIONS_T4275971970_H
#ifndef TUIOCURSOR_T1850351419_H
#define TUIOCURSOR_T1850351419_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TUIOsharp.Entities.TuioCursor
struct  TuioCursor_t1850351419  : public TuioEntity_t295276718
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUIOCURSOR_T1850351419_H
#ifndef TUIOOBJECT_T1236821014_H
#define TUIOOBJECT_T1236821014_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TUIOsharp.Entities.TuioObject
struct  TuioObject_t1236821014  : public TuioEntity_t295276718
{
public:
	// System.Int32 TUIOsharp.Entities.TuioObject::<ClassId>k__BackingField
	int32_t ___U3CClassIdU3Ek__BackingField_6;
	// System.Single TUIOsharp.Entities.TuioObject::<Angle>k__BackingField
	float ___U3CAngleU3Ek__BackingField_7;
	// System.Single TUIOsharp.Entities.TuioObject::<RotationVelocity>k__BackingField
	float ___U3CRotationVelocityU3Ek__BackingField_8;
	// System.Single TUIOsharp.Entities.TuioObject::<RotationAcceleration>k__BackingField
	float ___U3CRotationAccelerationU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of_U3CClassIdU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(TuioObject_t1236821014, ___U3CClassIdU3Ek__BackingField_6)); }
	inline int32_t get_U3CClassIdU3Ek__BackingField_6() const { return ___U3CClassIdU3Ek__BackingField_6; }
	inline int32_t* get_address_of_U3CClassIdU3Ek__BackingField_6() { return &___U3CClassIdU3Ek__BackingField_6; }
	inline void set_U3CClassIdU3Ek__BackingField_6(int32_t value)
	{
		___U3CClassIdU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CAngleU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(TuioObject_t1236821014, ___U3CAngleU3Ek__BackingField_7)); }
	inline float get_U3CAngleU3Ek__BackingField_7() const { return ___U3CAngleU3Ek__BackingField_7; }
	inline float* get_address_of_U3CAngleU3Ek__BackingField_7() { return &___U3CAngleU3Ek__BackingField_7; }
	inline void set_U3CAngleU3Ek__BackingField_7(float value)
	{
		___U3CAngleU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CRotationVelocityU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(TuioObject_t1236821014, ___U3CRotationVelocityU3Ek__BackingField_8)); }
	inline float get_U3CRotationVelocityU3Ek__BackingField_8() const { return ___U3CRotationVelocityU3Ek__BackingField_8; }
	inline float* get_address_of_U3CRotationVelocityU3Ek__BackingField_8() { return &___U3CRotationVelocityU3Ek__BackingField_8; }
	inline void set_U3CRotationVelocityU3Ek__BackingField_8(float value)
	{
		___U3CRotationVelocityU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CRotationAccelerationU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(TuioObject_t1236821014, ___U3CRotationAccelerationU3Ek__BackingField_9)); }
	inline float get_U3CRotationAccelerationU3Ek__BackingField_9() const { return ___U3CRotationAccelerationU3Ek__BackingField_9; }
	inline float* get_address_of_U3CRotationAccelerationU3Ek__BackingField_9() { return &___U3CRotationAccelerationU3Ek__BackingField_9; }
	inline void set_U3CRotationAccelerationU3Ek__BackingField_9(float value)
	{
		___U3CRotationAccelerationU3Ek__BackingField_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUIOOBJECT_T1236821014_H
#ifndef __STATICARRAYINITTYPESIZEU3D1024_T2165070692_H
#define __STATICARRAYINITTYPESIZEU3D1024_T2165070692_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>{83185D3B-3939-439C-A54F-260F9279D9C8}/__StaticArrayInitTypeSize=1024
struct  __StaticArrayInitTypeSizeU3D1024_t2165070692 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D1024_t2165070692__padding[1024];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D1024_T2165070692_H
#ifndef GENERICSTACK_T3718539591_H
#define GENERICSTACK_T3718539591_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngineInternal.GenericStack
struct  GenericStack_t3718539591  : public Stack_t1043988394
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICSTACK_T3718539591_H
#ifndef SINGLE_T2076509932_H
#define SINGLE_T2076509932_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t2076509932 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t2076509932, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T2076509932_H
#ifndef EXCLUDEFROMDOCSATTRIBUTE_T665825653_H
#define EXCLUDEFROMDOCSATTRIBUTE_T665825653_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Internal.ExcludeFromDocsAttribute
struct  ExcludeFromDocsAttribute_t665825653  : public Attribute_t542643598
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCLUDEFROMDOCSATTRIBUTE_T665825653_H
#ifndef CSSSIZE_T1564391216_H
#define CSSSIZE_T1564391216_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CSSLayout.CSSSize
struct  CSSSize_t1564391216 
{
public:
	// System.Single UnityEngine.CSSLayout.CSSSize::width
	float ___width_0;
	// System.Single UnityEngine.CSSLayout.CSSSize::height
	float ___height_1;

public:
	inline static int32_t get_offset_of_width_0() { return static_cast<int32_t>(offsetof(CSSSize_t1564391216, ___width_0)); }
	inline float get_width_0() const { return ___width_0; }
	inline float* get_address_of_width_0() { return &___width_0; }
	inline void set_width_0(float value)
	{
		___width_0 = value;
	}

	inline static int32_t get_offset_of_height_1() { return static_cast<int32_t>(offsetof(CSSSize_t1564391216, ___height_1)); }
	inline float get_height_1() const { return ___height_1; }
	inline float* get_address_of_height_1() { return &___height_1; }
	inline void set_height_1(float value)
	{
		___height_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CSSSIZE_T1564391216_H
#ifndef TUIOOBJECTEVENTARGS_T1810743341_H
#define TUIOOBJECTEVENTARGS_T1810743341_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TUIOsharp.DataProcessors.TuioObjectEventArgs
struct  TuioObjectEventArgs_t1810743341  : public EventArgs_t3289624707
{
public:
	// TUIOsharp.Entities.TuioObject TUIOsharp.DataProcessors.TuioObjectEventArgs::Object
	TuioObject_t1236821014 * ___Object_1;

public:
	inline static int32_t get_offset_of_Object_1() { return static_cast<int32_t>(offsetof(TuioObjectEventArgs_t1810743341, ___Object_1)); }
	inline TuioObject_t1236821014 * get_Object_1() const { return ___Object_1; }
	inline TuioObject_t1236821014 ** get_address_of_Object_1() { return &___Object_1; }
	inline void set_Object_1(TuioObject_t1236821014 * value)
	{
		___Object_1 = value;
		Il2CppCodeGenWriteBarrier((&___Object_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUIOOBJECTEVENTARGS_T1810743341_H
#ifndef TUIOBLOB_T2046943414_H
#define TUIOBLOB_T2046943414_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TUIOsharp.Entities.TuioBlob
struct  TuioBlob_t2046943414  : public TuioEntity_t295276718
{
public:
	// System.Single TUIOsharp.Entities.TuioBlob::<Angle>k__BackingField
	float ___U3CAngleU3Ek__BackingField_6;
	// System.Single TUIOsharp.Entities.TuioBlob::<Width>k__BackingField
	float ___U3CWidthU3Ek__BackingField_7;
	// System.Single TUIOsharp.Entities.TuioBlob::<Height>k__BackingField
	float ___U3CHeightU3Ek__BackingField_8;
	// System.Single TUIOsharp.Entities.TuioBlob::<Area>k__BackingField
	float ___U3CAreaU3Ek__BackingField_9;
	// System.Single TUIOsharp.Entities.TuioBlob::<RotationVelocity>k__BackingField
	float ___U3CRotationVelocityU3Ek__BackingField_10;
	// System.Single TUIOsharp.Entities.TuioBlob::<RotationAcceleration>k__BackingField
	float ___U3CRotationAccelerationU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_U3CAngleU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(TuioBlob_t2046943414, ___U3CAngleU3Ek__BackingField_6)); }
	inline float get_U3CAngleU3Ek__BackingField_6() const { return ___U3CAngleU3Ek__BackingField_6; }
	inline float* get_address_of_U3CAngleU3Ek__BackingField_6() { return &___U3CAngleU3Ek__BackingField_6; }
	inline void set_U3CAngleU3Ek__BackingField_6(float value)
	{
		___U3CAngleU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CWidthU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(TuioBlob_t2046943414, ___U3CWidthU3Ek__BackingField_7)); }
	inline float get_U3CWidthU3Ek__BackingField_7() const { return ___U3CWidthU3Ek__BackingField_7; }
	inline float* get_address_of_U3CWidthU3Ek__BackingField_7() { return &___U3CWidthU3Ek__BackingField_7; }
	inline void set_U3CWidthU3Ek__BackingField_7(float value)
	{
		___U3CWidthU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CHeightU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(TuioBlob_t2046943414, ___U3CHeightU3Ek__BackingField_8)); }
	inline float get_U3CHeightU3Ek__BackingField_8() const { return ___U3CHeightU3Ek__BackingField_8; }
	inline float* get_address_of_U3CHeightU3Ek__BackingField_8() { return &___U3CHeightU3Ek__BackingField_8; }
	inline void set_U3CHeightU3Ek__BackingField_8(float value)
	{
		___U3CHeightU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CAreaU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(TuioBlob_t2046943414, ___U3CAreaU3Ek__BackingField_9)); }
	inline float get_U3CAreaU3Ek__BackingField_9() const { return ___U3CAreaU3Ek__BackingField_9; }
	inline float* get_address_of_U3CAreaU3Ek__BackingField_9() { return &___U3CAreaU3Ek__BackingField_9; }
	inline void set_U3CAreaU3Ek__BackingField_9(float value)
	{
		___U3CAreaU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CRotationVelocityU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(TuioBlob_t2046943414, ___U3CRotationVelocityU3Ek__BackingField_10)); }
	inline float get_U3CRotationVelocityU3Ek__BackingField_10() const { return ___U3CRotationVelocityU3Ek__BackingField_10; }
	inline float* get_address_of_U3CRotationVelocityU3Ek__BackingField_10() { return &___U3CRotationVelocityU3Ek__BackingField_10; }
	inline void set_U3CRotationVelocityU3Ek__BackingField_10(float value)
	{
		___U3CRotationVelocityU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CRotationAccelerationU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(TuioBlob_t2046943414, ___U3CRotationAccelerationU3Ek__BackingField_11)); }
	inline float get_U3CRotationAccelerationU3Ek__BackingField_11() const { return ___U3CRotationAccelerationU3Ek__BackingField_11; }
	inline float* get_address_of_U3CRotationAccelerationU3Ek__BackingField_11() { return &___U3CRotationAccelerationU3Ek__BackingField_11; }
	inline void set_U3CRotationAccelerationU3Ek__BackingField_11(float value)
	{
		___U3CRotationAccelerationU3Ek__BackingField_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUIOBLOB_T2046943414_H
#ifndef TUIOCURSOREVENTARGS_T362990012_H
#define TUIOCURSOREVENTARGS_T362990012_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TUIOsharp.DataProcessors.TuioCursorEventArgs
struct  TuioCursorEventArgs_t362990012  : public EventArgs_t3289624707
{
public:
	// TUIOsharp.Entities.TuioCursor TUIOsharp.DataProcessors.TuioCursorEventArgs::Cursor
	TuioCursor_t1850351419 * ___Cursor_1;

public:
	inline static int32_t get_offset_of_Cursor_1() { return static_cast<int32_t>(offsetof(TuioCursorEventArgs_t362990012, ___Cursor_1)); }
	inline TuioCursor_t1850351419 * get_Cursor_1() const { return ___Cursor_1; }
	inline TuioCursor_t1850351419 ** get_address_of_Cursor_1() { return &___Cursor_1; }
	inline void set_Cursor_1(TuioCursor_t1850351419 * value)
	{
		___Cursor_1 = value;
		Il2CppCodeGenWriteBarrier((&___Cursor_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUIOCURSOREVENTARGS_T362990012_H
#ifndef DEFAULTVALUEATTRIBUTE_T1027170048_H
#define DEFAULTVALUEATTRIBUTE_T1027170048_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Internal.DefaultValueAttribute
struct  DefaultValueAttribute_t1027170048  : public Attribute_t542643598
{
public:
	// System.Object UnityEngine.Internal.DefaultValueAttribute::DefaultValue
	RuntimeObject * ___DefaultValue_0;

public:
	inline static int32_t get_offset_of_DefaultValue_0() { return static_cast<int32_t>(offsetof(DefaultValueAttribute_t1027170048, ___DefaultValue_0)); }
	inline RuntimeObject * get_DefaultValue_0() const { return ___DefaultValue_0; }
	inline RuntimeObject ** get_address_of_DefaultValue_0() { return &___DefaultValue_0; }
	inline void set_DefaultValue_0(RuntimeObject * value)
	{
		___DefaultValue_0 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultValue_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTVALUEATTRIBUTE_T1027170048_H
#ifndef TUIOBLOBEVENTARGS_T3562978179_H
#define TUIOBLOBEVENTARGS_T3562978179_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TUIOsharp.DataProcessors.TuioBlobEventArgs
struct  TuioBlobEventArgs_t3562978179  : public EventArgs_t3289624707
{
public:
	// TUIOsharp.Entities.TuioBlob TUIOsharp.DataProcessors.TuioBlobEventArgs::Blob
	TuioBlob_t2046943414 * ___Blob_1;

public:
	inline static int32_t get_offset_of_Blob_1() { return static_cast<int32_t>(offsetof(TuioBlobEventArgs_t3562978179, ___Blob_1)); }
	inline TuioBlob_t2046943414 * get_Blob_1() const { return ___Blob_1; }
	inline TuioBlob_t2046943414 ** get_address_of_Blob_1() { return &___Blob_1; }
	inline void set_Blob_1(TuioBlob_t2046943414 * value)
	{
		___Blob_1 = value;
		Il2CppCodeGenWriteBarrier((&___Blob_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUIOBLOBEVENTARGS_T3562978179_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	IntPtr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline IntPtr_t get_Zero_1() const { return ___Zero_1; }
	inline IntPtr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(IntPtr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef REQUIREDBYNATIVECODEATTRIBUTE_T1913052472_H
#define REQUIREDBYNATIVECODEATTRIBUTE_T1913052472_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Scripting.RequiredByNativeCodeAttribute
struct  RequiredByNativeCodeAttribute_t1913052472  : public Attribute_t542643598
{
public:
	// System.String UnityEngine.Scripting.RequiredByNativeCodeAttribute::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// System.Boolean UnityEngine.Scripting.RequiredByNativeCodeAttribute::<Optional>k__BackingField
	bool ___U3COptionalU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RequiredByNativeCodeAttribute_t1913052472, ___U3CNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3COptionalU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RequiredByNativeCodeAttribute_t1913052472, ___U3COptionalU3Ek__BackingField_1)); }
	inline bool get_U3COptionalU3Ek__BackingField_1() const { return ___U3COptionalU3Ek__BackingField_1; }
	inline bool* get_address_of_U3COptionalU3Ek__BackingField_1() { return &___U3COptionalU3Ek__BackingField_1; }
	inline void set_U3COptionalU3Ek__BackingField_1(bool value)
	{
		___U3COptionalU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUIREDBYNATIVECODEATTRIBUTE_T1913052472_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t1328083999* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t1328083999* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t1328083999** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t1328083999* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef MOVEDFROMATTRIBUTE_T922195725_H
#define MOVEDFROMATTRIBUTE_T922195725_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Scripting.APIUpdating.MovedFromAttribute
struct  MovedFromAttribute_t922195725  : public Attribute_t542643598
{
public:
	// System.String UnityEngine.Scripting.APIUpdating.MovedFromAttribute::<Namespace>k__BackingField
	String_t* ___U3CNamespaceU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CNamespaceU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MovedFromAttribute_t922195725, ___U3CNamespaceU3Ek__BackingField_0)); }
	inline String_t* get_U3CNamespaceU3Ek__BackingField_0() const { return ___U3CNamespaceU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNamespaceU3Ek__BackingField_0() { return &___U3CNamespaceU3Ek__BackingField_0; }
	inline void set_U3CNamespaceU3Ek__BackingField_0(String_t* value)
	{
		___U3CNamespaceU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNamespaceU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVEDFROMATTRIBUTE_T922195725_H
#ifndef UNITYEVENT_1_T339633637_H
#define UNITYEVENT_1_T339633637_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<UnityEngine.Networking.PlayerConnection.MessageEventArgs>
struct  UnityEvent_1_t339633637  : public UnityEventBase_t828812576
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3614634134* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t339633637, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3614634134* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3614634134* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T339633637_H
#ifndef USEDBYNATIVECODEATTRIBUTE_T3212052468_H
#define USEDBYNATIVECODEATTRIBUTE_T3212052468_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Scripting.UsedByNativeCodeAttribute
struct  UsedByNativeCodeAttribute_t3212052468  : public Attribute_t542643598
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USEDBYNATIVECODEATTRIBUTE_T3212052468_H
#ifndef GUID_T_H
#define GUID_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Guid
struct  Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_0;
	// System.Int16 System.Guid::_b
	int16_t ____b_1;
	// System.Int16 System.Guid::_c
	int16_t ____c_2;
	// System.Byte System.Guid::_d
	uint8_t ____d_3;
	// System.Byte System.Guid::_e
	uint8_t ____e_4;
	// System.Byte System.Guid::_f
	uint8_t ____f_5;
	// System.Byte System.Guid::_g
	uint8_t ____g_6;
	// System.Byte System.Guid::_h
	uint8_t ____h_7;
	// System.Byte System.Guid::_i
	uint8_t ____i_8;
	// System.Byte System.Guid::_j
	uint8_t ____j_9;
	// System.Byte System.Guid::_k
	uint8_t ____k_10;

public:
	inline static int32_t get_offset_of__a_0() { return static_cast<int32_t>(offsetof(Guid_t, ____a_0)); }
	inline int32_t get__a_0() const { return ____a_0; }
	inline int32_t* get_address_of__a_0() { return &____a_0; }
	inline void set__a_0(int32_t value)
	{
		____a_0 = value;
	}

	inline static int32_t get_offset_of__b_1() { return static_cast<int32_t>(offsetof(Guid_t, ____b_1)); }
	inline int16_t get__b_1() const { return ____b_1; }
	inline int16_t* get_address_of__b_1() { return &____b_1; }
	inline void set__b_1(int16_t value)
	{
		____b_1 = value;
	}

	inline static int32_t get_offset_of__c_2() { return static_cast<int32_t>(offsetof(Guid_t, ____c_2)); }
	inline int16_t get__c_2() const { return ____c_2; }
	inline int16_t* get_address_of__c_2() { return &____c_2; }
	inline void set__c_2(int16_t value)
	{
		____c_2 = value;
	}

	inline static int32_t get_offset_of__d_3() { return static_cast<int32_t>(offsetof(Guid_t, ____d_3)); }
	inline uint8_t get__d_3() const { return ____d_3; }
	inline uint8_t* get_address_of__d_3() { return &____d_3; }
	inline void set__d_3(uint8_t value)
	{
		____d_3 = value;
	}

	inline static int32_t get_offset_of__e_4() { return static_cast<int32_t>(offsetof(Guid_t, ____e_4)); }
	inline uint8_t get__e_4() const { return ____e_4; }
	inline uint8_t* get_address_of__e_4() { return &____e_4; }
	inline void set__e_4(uint8_t value)
	{
		____e_4 = value;
	}

	inline static int32_t get_offset_of__f_5() { return static_cast<int32_t>(offsetof(Guid_t, ____f_5)); }
	inline uint8_t get__f_5() const { return ____f_5; }
	inline uint8_t* get_address_of__f_5() { return &____f_5; }
	inline void set__f_5(uint8_t value)
	{
		____f_5 = value;
	}

	inline static int32_t get_offset_of__g_6() { return static_cast<int32_t>(offsetof(Guid_t, ____g_6)); }
	inline uint8_t get__g_6() const { return ____g_6; }
	inline uint8_t* get_address_of__g_6() { return &____g_6; }
	inline void set__g_6(uint8_t value)
	{
		____g_6 = value;
	}

	inline static int32_t get_offset_of__h_7() { return static_cast<int32_t>(offsetof(Guid_t, ____h_7)); }
	inline uint8_t get__h_7() const { return ____h_7; }
	inline uint8_t* get_address_of__h_7() { return &____h_7; }
	inline void set__h_7(uint8_t value)
	{
		____h_7 = value;
	}

	inline static int32_t get_offset_of__i_8() { return static_cast<int32_t>(offsetof(Guid_t, ____i_8)); }
	inline uint8_t get__i_8() const { return ____i_8; }
	inline uint8_t* get_address_of__i_8() { return &____i_8; }
	inline void set__i_8(uint8_t value)
	{
		____i_8 = value;
	}

	inline static int32_t get_offset_of__j_9() { return static_cast<int32_t>(offsetof(Guid_t, ____j_9)); }
	inline uint8_t get__j_9() const { return ____j_9; }
	inline uint8_t* get_address_of__j_9() { return &____j_9; }
	inline void set__j_9(uint8_t value)
	{
		____j_9 = value;
	}

	inline static int32_t get_offset_of__k_10() { return static_cast<int32_t>(offsetof(Guid_t, ____k_10)); }
	inline uint8_t get__k_10() const { return ____k_10; }
	inline uint8_t* get_address_of__k_10() { return &____k_10; }
	inline void set__k_10(uint8_t value)
	{
		____k_10 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_11;
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_12;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t2510243513 * ____rng_13;

public:
	inline static int32_t get_offset_of_Empty_11() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_11)); }
	inline Guid_t  get_Empty_11() const { return ___Empty_11; }
	inline Guid_t * get_address_of_Empty_11() { return &___Empty_11; }
	inline void set_Empty_11(Guid_t  value)
	{
		___Empty_11 = value;
	}

	inline static int32_t get_offset_of__rngAccess_12() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_12)); }
	inline RuntimeObject * get__rngAccess_12() const { return ____rngAccess_12; }
	inline RuntimeObject ** get_address_of__rngAccess_12() { return &____rngAccess_12; }
	inline void set__rngAccess_12(RuntimeObject * value)
	{
		____rngAccess_12 = value;
		Il2CppCodeGenWriteBarrier((&____rngAccess_12), value);
	}

	inline static int32_t get_offset_of__rng_13() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_13)); }
	inline RandomNumberGenerator_t2510243513 * get__rng_13() const { return ____rng_13; }
	inline RandomNumberGenerator_t2510243513 ** get_address_of__rng_13() { return &____rng_13; }
	inline void set__rng_13(RandomNumberGenerator_t2510243513 * value)
	{
		____rng_13 = value;
		Il2CppCodeGenWriteBarrier((&____rng_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUID_T_H
#ifndef UNITYEVENT_1_T2110227463_H
#define UNITYEVENT_1_T2110227463_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.Int32>
struct  UnityEvent_1_t2110227463  : public UnityEventBase_t828812576
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3614634134* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t2110227463, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3614634134* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3614634134* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T2110227463_H
#ifndef HTMLNAMETABLE_T3848610378_H
#define HTMLNAMETABLE_T3848610378_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HtmlAgilityPack.HtmlNameTable
struct  HtmlNameTable_t3848610378  : public XmlNameTable_t1345805268
{
public:
	// System.Xml.NameTable HtmlAgilityPack.HtmlNameTable::_nametable
	NameTable_t594386929 * ____nametable_0;

public:
	inline static int32_t get_offset_of__nametable_0() { return static_cast<int32_t>(offsetof(HtmlNameTable_t3848610378, ____nametable_0)); }
	inline NameTable_t594386929 * get__nametable_0() const { return ____nametable_0; }
	inline NameTable_t594386929 ** get_address_of__nametable_0() { return &____nametable_0; }
	inline void set__nametable_0(NameTable_t594386929 * value)
	{
		____nametable_0 = value;
		Il2CppCodeGenWriteBarrier((&____nametable_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTMLNAMETABLE_T3848610378_H
#ifndef TYPEINFERENCERULEATTRIBUTE_T1390152093_H
#define TYPEINFERENCERULEATTRIBUTE_T1390152093_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngineInternal.TypeInferenceRuleAttribute
struct  TypeInferenceRuleAttribute_t1390152093  : public Attribute_t542643598
{
public:
	// System.String UnityEngineInternal.TypeInferenceRuleAttribute::_rule
	String_t* ____rule_0;

public:
	inline static int32_t get_offset_of__rule_0() { return static_cast<int32_t>(offsetof(TypeInferenceRuleAttribute_t1390152093, ____rule_0)); }
	inline String_t* get__rule_0() const { return ____rule_0; }
	inline String_t** get_address_of__rule_0() { return &____rule_0; }
	inline void set__rule_0(String_t* value)
	{
		____rule_0 = value;
		Il2CppCodeGenWriteBarrier((&____rule_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEINFERENCERULEATTRIBUTE_T1390152093_H
#ifndef ENCODINGFOUNDEXCEPTION_T1471015948_H
#define ENCODINGFOUNDEXCEPTION_T1471015948_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HtmlAgilityPack.EncodingFoundException
struct  EncodingFoundException_t1471015948  : public Exception_t1927440687
{
public:
	// System.Text.Encoding HtmlAgilityPack.EncodingFoundException::_encoding
	Encoding_t663144255 * ____encoding_11;

public:
	inline static int32_t get_offset_of__encoding_11() { return static_cast<int32_t>(offsetof(EncodingFoundException_t1471015948, ____encoding_11)); }
	inline Encoding_t663144255 * get__encoding_11() const { return ____encoding_11; }
	inline Encoding_t663144255 ** get_address_of__encoding_11() { return &____encoding_11; }
	inline void set__encoding_11(Encoding_t663144255 * value)
	{
		____encoding_11 = value;
		Il2CppCodeGenWriteBarrier((&____encoding_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCODINGFOUNDEXCEPTION_T1471015948_H
#ifndef FORMERLYSERIALIZEDASATTRIBUTE_T3673080018_H
#define FORMERLYSERIALIZEDASATTRIBUTE_T3673080018_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Serialization.FormerlySerializedAsAttribute
struct  FormerlySerializedAsAttribute_t3673080018  : public Attribute_t542643598
{
public:
	// System.String UnityEngine.Serialization.FormerlySerializedAsAttribute::m_oldName
	String_t* ___m_oldName_0;

public:
	inline static int32_t get_offset_of_m_oldName_0() { return static_cast<int32_t>(offsetof(FormerlySerializedAsAttribute_t3673080018, ___m_oldName_0)); }
	inline String_t* get_m_oldName_0() const { return ___m_oldName_0; }
	inline String_t** get_address_of_m_oldName_0() { return &___m_oldName_0; }
	inline void set_m_oldName_0(String_t* value)
	{
		___m_oldName_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_oldName_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMERLYSERIALIZEDASATTRIBUTE_T3673080018_H
#ifndef XPATHNAVIGATOR_T3981235968_H
#define XPATHNAVIGATOR_T3981235968_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathNavigator
struct  XPathNavigator_t3981235968  : public XPathItem_t3130801258
{
public:

public:
};

struct XPathNavigator_t3981235968_StaticFields
{
public:
	// System.Char[] System.Xml.XPath.XPathNavigator::escape_text_chars
	CharU5BU5D_t1328083999* ___escape_text_chars_0;
	// System.Char[] System.Xml.XPath.XPathNavigator::escape_attr_chars
	CharU5BU5D_t1328083999* ___escape_attr_chars_1;

public:
	inline static int32_t get_offset_of_escape_text_chars_0() { return static_cast<int32_t>(offsetof(XPathNavigator_t3981235968_StaticFields, ___escape_text_chars_0)); }
	inline CharU5BU5D_t1328083999* get_escape_text_chars_0() const { return ___escape_text_chars_0; }
	inline CharU5BU5D_t1328083999** get_address_of_escape_text_chars_0() { return &___escape_text_chars_0; }
	inline void set_escape_text_chars_0(CharU5BU5D_t1328083999* value)
	{
		___escape_text_chars_0 = value;
		Il2CppCodeGenWriteBarrier((&___escape_text_chars_0), value);
	}

	inline static int32_t get_offset_of_escape_attr_chars_1() { return static_cast<int32_t>(offsetof(XPathNavigator_t3981235968_StaticFields, ___escape_attr_chars_1)); }
	inline CharU5BU5D_t1328083999* get_escape_attr_chars_1() const { return ___escape_attr_chars_1; }
	inline CharU5BU5D_t1328083999** get_address_of_escape_attr_chars_1() { return &___escape_attr_chars_1; }
	inline void set_escape_attr_chars_1(CharU5BU5D_t1328083999* value)
	{
		___escape_attr_chars_1 = value;
		Il2CppCodeGenWriteBarrier((&___escape_attr_chars_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHNAVIGATOR_T3981235968_H
#ifndef PLAYABLEOUTPUTHANDLE_T551742311_H
#define PLAYABLEOUTPUTHANDLE_T551742311_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableOutputHandle
struct  PlayableOutputHandle_t551742311 
{
public:
	// System.IntPtr UnityEngine.Playables.PlayableOutputHandle::m_Handle
	IntPtr_t ___m_Handle_0;
	// System.Int32 UnityEngine.Playables.PlayableOutputHandle::m_Version
	int32_t ___m_Version_1;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(PlayableOutputHandle_t551742311, ___m_Handle_0)); }
	inline IntPtr_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline IntPtr_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(IntPtr_t value)
	{
		___m_Handle_0 = value;
	}

	inline static int32_t get_offset_of_m_Version_1() { return static_cast<int32_t>(offsetof(PlayableOutputHandle_t551742311, ___m_Version_1)); }
	inline int32_t get_m_Version_1() const { return ___m_Version_1; }
	inline int32_t* get_address_of_m_Version_1() { return &___m_Version_1; }
	inline void set_m_Version_1(int32_t value)
	{
		___m_Version_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEOUTPUTHANDLE_T551742311_H
#ifndef OBJECT_T1021602117_H
#define OBJECT_T1021602117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1021602117  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	IntPtr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1021602117, ___m_CachedPtr_0)); }
	inline IntPtr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline IntPtr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(IntPtr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1021602117_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1021602117_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1021602117_H
#ifndef DELEGATE_T3022476291_H
#define DELEGATE_T3022476291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t3022476291  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	IntPtr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	IntPtr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	IntPtr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	IntPtr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1572802995 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___invoke_impl_1)); }
	inline IntPtr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline IntPtr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(IntPtr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_3)); }
	inline IntPtr_t get_method_3() const { return ___method_3; }
	inline IntPtr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(IntPtr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___delegate_trampoline_4)); }
	inline IntPtr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline IntPtr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(IntPtr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_code_5)); }
	inline IntPtr_t get_method_code_5() const { return ___method_code_5; }
	inline IntPtr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(IntPtr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___data_8)); }
	inline DelegateData_t1572802995 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1572802995 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1572802995 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T3022476291_H
#ifndef LOGTYPE_T1559732862_H
#define LOGTYPE_T1559732862_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LogType
struct  LogType_t1559732862 
{
public:
	// System.Int32 UnityEngine.LogType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LogType_t1559732862, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGTYPE_T1559732862_H
#ifndef EVENTHANDLE_T942672932_H
#define EVENTHANDLE_T942672932_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.EventHandle
struct  EventHandle_t942672932 
{
public:
	// System.Int32 UnityEngine.EventSystems.EventHandle::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EventHandle_t942672932, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTHANDLE_T942672932_H
#ifndef HTMLNODETYPE_T1941241835_H
#define HTMLNODETYPE_T1941241835_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HtmlAgilityPack.HtmlNodeType
struct  HtmlNodeType_t1941241835 
{
public:
	// System.Int32 HtmlAgilityPack.HtmlNodeType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HtmlNodeType_t1941241835, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTMLNODETYPE_T1941241835_H
#ifndef CSSMEASUREMODE_T2118763144_H
#define CSSMEASUREMODE_T2118763144_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CSSLayout.CSSMeasureMode
struct  CSSMeasureMode_t2118763144 
{
public:
	// System.Int32 UnityEngine.CSSLayout.CSSMeasureMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CSSMeasureMode_t2118763144, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CSSMEASUREMODE_T2118763144_H
#ifndef PARSESTATE_T1851939866_H
#define PARSESTATE_T1851939866_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HtmlAgilityPack.HtmlDocument/ParseState
struct  ParseState_t1851939866 
{
public:
	// System.Int32 HtmlAgilityPack.HtmlDocument/ParseState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ParseState_t1851939866, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARSESTATE_T1851939866_H
#ifndef HTMLNODENAVIGATOR_T2752606360_H
#define HTMLNODENAVIGATOR_T2752606360_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HtmlAgilityPack.HtmlNodeNavigator
struct  HtmlNodeNavigator_t2752606360  : public XPathNavigator_t3981235968
{
public:
	// System.Int32 HtmlAgilityPack.HtmlNodeNavigator::_attindex
	int32_t ____attindex_2;
	// HtmlAgilityPack.HtmlNode HtmlAgilityPack.HtmlNodeNavigator::_currentnode
	HtmlNode_t2048434459 * ____currentnode_3;
	// HtmlAgilityPack.HtmlDocument HtmlAgilityPack.HtmlNodeNavigator::_doc
	HtmlDocument_t556432108 * ____doc_4;
	// HtmlAgilityPack.HtmlNameTable HtmlAgilityPack.HtmlNodeNavigator::_nametable
	HtmlNameTable_t3848610378 * ____nametable_5;

public:
	inline static int32_t get_offset_of__attindex_2() { return static_cast<int32_t>(offsetof(HtmlNodeNavigator_t2752606360, ____attindex_2)); }
	inline int32_t get__attindex_2() const { return ____attindex_2; }
	inline int32_t* get_address_of__attindex_2() { return &____attindex_2; }
	inline void set__attindex_2(int32_t value)
	{
		____attindex_2 = value;
	}

	inline static int32_t get_offset_of__currentnode_3() { return static_cast<int32_t>(offsetof(HtmlNodeNavigator_t2752606360, ____currentnode_3)); }
	inline HtmlNode_t2048434459 * get__currentnode_3() const { return ____currentnode_3; }
	inline HtmlNode_t2048434459 ** get_address_of__currentnode_3() { return &____currentnode_3; }
	inline void set__currentnode_3(HtmlNode_t2048434459 * value)
	{
		____currentnode_3 = value;
		Il2CppCodeGenWriteBarrier((&____currentnode_3), value);
	}

	inline static int32_t get_offset_of__doc_4() { return static_cast<int32_t>(offsetof(HtmlNodeNavigator_t2752606360, ____doc_4)); }
	inline HtmlDocument_t556432108 * get__doc_4() const { return ____doc_4; }
	inline HtmlDocument_t556432108 ** get_address_of__doc_4() { return &____doc_4; }
	inline void set__doc_4(HtmlDocument_t556432108 * value)
	{
		____doc_4 = value;
		Il2CppCodeGenWriteBarrier((&____doc_4), value);
	}

	inline static int32_t get_offset_of__nametable_5() { return static_cast<int32_t>(offsetof(HtmlNodeNavigator_t2752606360, ____nametable_5)); }
	inline HtmlNameTable_t3848610378 * get__nametable_5() const { return ____nametable_5; }
	inline HtmlNameTable_t3848610378 ** get_address_of__nametable_5() { return &____nametable_5; }
	inline void set__nametable_5(HtmlNameTable_t3848610378 * value)
	{
		____nametable_5 = value;
		Il2CppCodeGenWriteBarrier((&____nametable_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTMLNODENAVIGATOR_T2752606360_H
#ifndef CONNECTIONCHANGEEVENT_T536719976_H
#define CONNECTIONCHANGEEVENT_T536719976_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/ConnectionChangeEvent
struct  ConnectionChangeEvent_t536719976  : public UnityEvent_1_t2110227463
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTIONCHANGEEVENT_T536719976_H
#ifndef MESSAGEEVENT_T2167079021_H
#define MESSAGEEVENT_T2167079021_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageEvent
struct  MessageEvent_t2167079021  : public UnityEvent_1_t339633637
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGEEVENT_T2167079021_H
#ifndef TYPEINFERENCERULES_T1810425448_H
#define TYPEINFERENCERULES_T1810425448_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngineInternal.TypeInferenceRules
struct  TypeInferenceRules_t1810425448 
{
public:
	// System.Int32 UnityEngineInternal.TypeInferenceRules::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TypeInferenceRules_t1810425448, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEINFERENCERULES_T1810425448_H
#ifndef U3CINVOKEMESSAGEIDSUBSCRIBERSU3EC__ANONSTOREY0_T1899782350_H
#define U3CINVOKEMESSAGEIDSUBSCRIBERSU3EC__ANONSTOREY0_T1899782350_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/<InvokeMessageIdSubscribers>c__AnonStorey0
struct  U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t1899782350  : public RuntimeObject
{
public:
	// System.Guid UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/<InvokeMessageIdSubscribers>c__AnonStorey0::messageId
	Guid_t  ___messageId_0;

public:
	inline static int32_t get_offset_of_messageId_0() { return static_cast<int32_t>(offsetof(U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t1899782350, ___messageId_0)); }
	inline Guid_t  get_messageId_0() const { return ___messageId_0; }
	inline Guid_t * get_address_of_messageId_0() { return &___messageId_0; }
	inline void set_messageId_0(Guid_t  value)
	{
		___messageId_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINVOKEMESSAGEIDSUBSCRIBERSU3EC__ANONSTOREY0_T1899782350_H
#ifndef HTMLELEMENTFLAG_T260274357_H
#define HTMLELEMENTFLAG_T260274357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HtmlAgilityPack.HtmlElementFlag
struct  HtmlElementFlag_t260274357 
{
public:
	// System.Int32 HtmlAgilityPack.HtmlElementFlag::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HtmlElementFlag_t260274357, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTMLELEMENTFLAG_T260274357_H
#ifndef ATTRIBUTEVALUEQUOTE_T3949719843_H
#define ATTRIBUTEVALUEQUOTE_T3949719843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HtmlAgilityPack.AttributeValueQuote
struct  AttributeValueQuote_t3949719843 
{
public:
	// System.Int32 HtmlAgilityPack.AttributeValueQuote::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AttributeValueQuote_t3949719843, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTEVALUEQUOTE_T3949719843_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3EU7B83185D3BU2D3939U2D439CU2DA54FU2D260F9279D9C8U7D_T691081255_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3EU7B83185D3BU2D3939U2D439CU2DA54FU2D260F9279D9C8U7D_T691081255_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>{83185D3B-3939-439C-A54F-260F9279D9C8}
struct  U3CPrivateImplementationDetailsU3EU7B83185D3BU2D3939U2D439CU2DA54FU2D260F9279D9C8U7D_t691081255  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3EU7B83185D3BU2D3939U2D439CU2DA54FU2D260F9279D9C8U7D_t691081255_StaticFields
{
public:
	// System.Int64 <PrivateImplementationDetails>{83185D3B-3939-439C-A54F-260F9279D9C8}::$$method0x600012f-1
	int64_t ___U24U24method0x600012fU2D1_0;
	// <PrivateImplementationDetails>{83185D3B-3939-439C-A54F-260F9279D9C8}/__StaticArrayInitTypeSize=1024 <PrivateImplementationDetails>{83185D3B-3939-439C-A54F-260F9279D9C8}::$$method0x6000287-1
	__StaticArrayInitTypeSizeU3D1024_t2165070692  ___U24U24method0x6000287U2D1_1;

public:
	inline static int32_t get_offset_of_U24U24method0x600012fU2D1_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7B83185D3BU2D3939U2D439CU2DA54FU2D260F9279D9C8U7D_t691081255_StaticFields, ___U24U24method0x600012fU2D1_0)); }
	inline int64_t get_U24U24method0x600012fU2D1_0() const { return ___U24U24method0x600012fU2D1_0; }
	inline int64_t* get_address_of_U24U24method0x600012fU2D1_0() { return &___U24U24method0x600012fU2D1_0; }
	inline void set_U24U24method0x600012fU2D1_0(int64_t value)
	{
		___U24U24method0x600012fU2D1_0 = value;
	}

	inline static int32_t get_offset_of_U24U24method0x6000287U2D1_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7B83185D3BU2D3939U2D439CU2DA54FU2D260F9279D9C8U7D_t691081255_StaticFields, ___U24U24method0x6000287U2D1_1)); }
	inline __StaticArrayInitTypeSizeU3D1024_t2165070692  get_U24U24method0x6000287U2D1_1() const { return ___U24U24method0x6000287U2D1_1; }
	inline __StaticArrayInitTypeSizeU3D1024_t2165070692 * get_address_of_U24U24method0x6000287U2D1_1() { return &___U24U24method0x6000287U2D1_1; }
	inline void set_U24U24method0x6000287U2D1_1(__StaticArrayInitTypeSizeU3D1024_t2165070692  value)
	{
		___U24U24method0x6000287U2D1_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3EU7B83185D3BU2D3939U2D439CU2DA54FU2D260F9279D9C8U7D_T691081255_H
#ifndef HTMLPARSEERRORCODE_T3491517481_H
#define HTMLPARSEERRORCODE_T3491517481_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HtmlAgilityPack.HtmlParseErrorCode
struct  HtmlParseErrorCode_t3491517481 
{
public:
	// System.Int32 HtmlAgilityPack.HtmlParseErrorCode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HtmlParseErrorCode_t3491517481, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTMLPARSEERRORCODE_T3491517481_H
#ifndef LOGGER_T3328995178_H
#define LOGGER_T3328995178_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Logger
struct  Logger_t3328995178  : public RuntimeObject
{
public:
	// UnityEngine.ILogHandler UnityEngine.Logger::<logHandler>k__BackingField
	RuntimeObject* ___U3ClogHandlerU3Ek__BackingField_0;
	// System.Boolean UnityEngine.Logger::<logEnabled>k__BackingField
	bool ___U3ClogEnabledU3Ek__BackingField_1;
	// UnityEngine.LogType UnityEngine.Logger::<filterLogType>k__BackingField
	int32_t ___U3CfilterLogTypeU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3ClogHandlerU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Logger_t3328995178, ___U3ClogHandlerU3Ek__BackingField_0)); }
	inline RuntimeObject* get_U3ClogHandlerU3Ek__BackingField_0() const { return ___U3ClogHandlerU3Ek__BackingField_0; }
	inline RuntimeObject** get_address_of_U3ClogHandlerU3Ek__BackingField_0() { return &___U3ClogHandlerU3Ek__BackingField_0; }
	inline void set_U3ClogHandlerU3Ek__BackingField_0(RuntimeObject* value)
	{
		___U3ClogHandlerU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClogHandlerU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3ClogEnabledU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Logger_t3328995178, ___U3ClogEnabledU3Ek__BackingField_1)); }
	inline bool get_U3ClogEnabledU3Ek__BackingField_1() const { return ___U3ClogEnabledU3Ek__BackingField_1; }
	inline bool* get_address_of_U3ClogEnabledU3Ek__BackingField_1() { return &___U3ClogEnabledU3Ek__BackingField_1; }
	inline void set_U3ClogEnabledU3Ek__BackingField_1(bool value)
	{
		___U3ClogEnabledU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CfilterLogTypeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Logger_t3328995178, ___U3CfilterLogTypeU3Ek__BackingField_2)); }
	inline int32_t get_U3CfilterLogTypeU3Ek__BackingField_2() const { return ___U3CfilterLogTypeU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CfilterLogTypeU3Ek__BackingField_2() { return &___U3CfilterLogTypeU3Ek__BackingField_2; }
	inline void set_U3CfilterLogTypeU3Ek__BackingField_2(int32_t value)
	{
		___U3CfilterLogTypeU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGGER_T3328995178_H
#ifndef MULTICASTDELEGATE_T3201952435_H
#define MULTICASTDELEGATE_T3201952435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t3201952435  : public Delegate_t3022476291
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t3201952435 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t3201952435 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___prev_9)); }
	inline MulticastDelegate_t3201952435 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t3201952435 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t3201952435 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___kpm_next_10)); }
	inline MulticastDelegate_t3201952435 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t3201952435 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t3201952435 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T3201952435_H
#ifndef HTMLPARSEERROR_T1115179162_H
#define HTMLPARSEERROR_T1115179162_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HtmlAgilityPack.HtmlParseError
struct  HtmlParseError_t1115179162  : public RuntimeObject
{
public:
	// HtmlAgilityPack.HtmlParseErrorCode HtmlAgilityPack.HtmlParseError::_code
	int32_t ____code_0;
	// System.Int32 HtmlAgilityPack.HtmlParseError::_line
	int32_t ____line_1;
	// System.Int32 HtmlAgilityPack.HtmlParseError::_linePosition
	int32_t ____linePosition_2;
	// System.String HtmlAgilityPack.HtmlParseError::_reason
	String_t* ____reason_3;
	// System.String HtmlAgilityPack.HtmlParseError::_sourceText
	String_t* ____sourceText_4;
	// System.Int32 HtmlAgilityPack.HtmlParseError::_streamPosition
	int32_t ____streamPosition_5;

public:
	inline static int32_t get_offset_of__code_0() { return static_cast<int32_t>(offsetof(HtmlParseError_t1115179162, ____code_0)); }
	inline int32_t get__code_0() const { return ____code_0; }
	inline int32_t* get_address_of__code_0() { return &____code_0; }
	inline void set__code_0(int32_t value)
	{
		____code_0 = value;
	}

	inline static int32_t get_offset_of__line_1() { return static_cast<int32_t>(offsetof(HtmlParseError_t1115179162, ____line_1)); }
	inline int32_t get__line_1() const { return ____line_1; }
	inline int32_t* get_address_of__line_1() { return &____line_1; }
	inline void set__line_1(int32_t value)
	{
		____line_1 = value;
	}

	inline static int32_t get_offset_of__linePosition_2() { return static_cast<int32_t>(offsetof(HtmlParseError_t1115179162, ____linePosition_2)); }
	inline int32_t get__linePosition_2() const { return ____linePosition_2; }
	inline int32_t* get_address_of__linePosition_2() { return &____linePosition_2; }
	inline void set__linePosition_2(int32_t value)
	{
		____linePosition_2 = value;
	}

	inline static int32_t get_offset_of__reason_3() { return static_cast<int32_t>(offsetof(HtmlParseError_t1115179162, ____reason_3)); }
	inline String_t* get__reason_3() const { return ____reason_3; }
	inline String_t** get_address_of__reason_3() { return &____reason_3; }
	inline void set__reason_3(String_t* value)
	{
		____reason_3 = value;
		Il2CppCodeGenWriteBarrier((&____reason_3), value);
	}

	inline static int32_t get_offset_of__sourceText_4() { return static_cast<int32_t>(offsetof(HtmlParseError_t1115179162, ____sourceText_4)); }
	inline String_t* get__sourceText_4() const { return ____sourceText_4; }
	inline String_t** get_address_of__sourceText_4() { return &____sourceText_4; }
	inline void set__sourceText_4(String_t* value)
	{
		____sourceText_4 = value;
		Il2CppCodeGenWriteBarrier((&____sourceText_4), value);
	}

	inline static int32_t get_offset_of__streamPosition_5() { return static_cast<int32_t>(offsetof(HtmlParseError_t1115179162, ____streamPosition_5)); }
	inline int32_t get__streamPosition_5() const { return ____streamPosition_5; }
	inline int32_t* get_address_of__streamPosition_5() { return &____streamPosition_5; }
	inline void set__streamPosition_5(int32_t value)
	{
		____streamPosition_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTMLPARSEERROR_T1115179162_H
#ifndef HTMLNODE_T2048434459_H
#define HTMLNODE_T2048434459_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HtmlAgilityPack.HtmlNode
struct  HtmlNode_t2048434459  : public RuntimeObject
{
public:
	// HtmlAgilityPack.HtmlAttributeCollection HtmlAgilityPack.HtmlNode::_attributes
	HtmlAttributeCollection_t1787476631 * ____attributes_0;
	// HtmlAgilityPack.HtmlNodeCollection HtmlAgilityPack.HtmlNode::_childnodes
	HtmlNodeCollection_t2542734491 * ____childnodes_1;
	// HtmlAgilityPack.HtmlNode HtmlAgilityPack.HtmlNode::_endnode
	HtmlNode_t2048434459 * ____endnode_2;
	// System.Boolean HtmlAgilityPack.HtmlNode::_innerchanged
	bool ____innerchanged_3;
	// System.String HtmlAgilityPack.HtmlNode::_innerhtml
	String_t* ____innerhtml_4;
	// System.Int32 HtmlAgilityPack.HtmlNode::_innerlength
	int32_t ____innerlength_5;
	// System.Int32 HtmlAgilityPack.HtmlNode::_innerstartindex
	int32_t ____innerstartindex_6;
	// System.Int32 HtmlAgilityPack.HtmlNode::_line
	int32_t ____line_7;
	// System.Int32 HtmlAgilityPack.HtmlNode::_lineposition
	int32_t ____lineposition_8;
	// System.String HtmlAgilityPack.HtmlNode::_name
	String_t* ____name_9;
	// System.Int32 HtmlAgilityPack.HtmlNode::_namelength
	int32_t ____namelength_10;
	// System.Int32 HtmlAgilityPack.HtmlNode::_namestartindex
	int32_t ____namestartindex_11;
	// HtmlAgilityPack.HtmlNode HtmlAgilityPack.HtmlNode::_nextnode
	HtmlNode_t2048434459 * ____nextnode_12;
	// HtmlAgilityPack.HtmlNodeType HtmlAgilityPack.HtmlNode::_nodetype
	int32_t ____nodetype_13;
	// System.Boolean HtmlAgilityPack.HtmlNode::_outerchanged
	bool ____outerchanged_14;
	// System.String HtmlAgilityPack.HtmlNode::_outerhtml
	String_t* ____outerhtml_15;
	// System.Int32 HtmlAgilityPack.HtmlNode::_outerlength
	int32_t ____outerlength_16;
	// System.Int32 HtmlAgilityPack.HtmlNode::_outerstartindex
	int32_t ____outerstartindex_17;
	// System.String HtmlAgilityPack.HtmlNode::_optimizedName
	String_t* ____optimizedName_18;
	// HtmlAgilityPack.HtmlDocument HtmlAgilityPack.HtmlNode::_ownerdocument
	HtmlDocument_t556432108 * ____ownerdocument_19;
	// HtmlAgilityPack.HtmlNode HtmlAgilityPack.HtmlNode::_parentnode
	HtmlNode_t2048434459 * ____parentnode_20;
	// HtmlAgilityPack.HtmlNode HtmlAgilityPack.HtmlNode::_prevnode
	HtmlNode_t2048434459 * ____prevnode_21;
	// HtmlAgilityPack.HtmlNode HtmlAgilityPack.HtmlNode::_prevwithsamename
	HtmlNode_t2048434459 * ____prevwithsamename_22;
	// System.Boolean HtmlAgilityPack.HtmlNode::_starttag
	bool ____starttag_23;
	// System.Int32 HtmlAgilityPack.HtmlNode::_streamposition
	int32_t ____streamposition_24;

public:
	inline static int32_t get_offset_of__attributes_0() { return static_cast<int32_t>(offsetof(HtmlNode_t2048434459, ____attributes_0)); }
	inline HtmlAttributeCollection_t1787476631 * get__attributes_0() const { return ____attributes_0; }
	inline HtmlAttributeCollection_t1787476631 ** get_address_of__attributes_0() { return &____attributes_0; }
	inline void set__attributes_0(HtmlAttributeCollection_t1787476631 * value)
	{
		____attributes_0 = value;
		Il2CppCodeGenWriteBarrier((&____attributes_0), value);
	}

	inline static int32_t get_offset_of__childnodes_1() { return static_cast<int32_t>(offsetof(HtmlNode_t2048434459, ____childnodes_1)); }
	inline HtmlNodeCollection_t2542734491 * get__childnodes_1() const { return ____childnodes_1; }
	inline HtmlNodeCollection_t2542734491 ** get_address_of__childnodes_1() { return &____childnodes_1; }
	inline void set__childnodes_1(HtmlNodeCollection_t2542734491 * value)
	{
		____childnodes_1 = value;
		Il2CppCodeGenWriteBarrier((&____childnodes_1), value);
	}

	inline static int32_t get_offset_of__endnode_2() { return static_cast<int32_t>(offsetof(HtmlNode_t2048434459, ____endnode_2)); }
	inline HtmlNode_t2048434459 * get__endnode_2() const { return ____endnode_2; }
	inline HtmlNode_t2048434459 ** get_address_of__endnode_2() { return &____endnode_2; }
	inline void set__endnode_2(HtmlNode_t2048434459 * value)
	{
		____endnode_2 = value;
		Il2CppCodeGenWriteBarrier((&____endnode_2), value);
	}

	inline static int32_t get_offset_of__innerchanged_3() { return static_cast<int32_t>(offsetof(HtmlNode_t2048434459, ____innerchanged_3)); }
	inline bool get__innerchanged_3() const { return ____innerchanged_3; }
	inline bool* get_address_of__innerchanged_3() { return &____innerchanged_3; }
	inline void set__innerchanged_3(bool value)
	{
		____innerchanged_3 = value;
	}

	inline static int32_t get_offset_of__innerhtml_4() { return static_cast<int32_t>(offsetof(HtmlNode_t2048434459, ____innerhtml_4)); }
	inline String_t* get__innerhtml_4() const { return ____innerhtml_4; }
	inline String_t** get_address_of__innerhtml_4() { return &____innerhtml_4; }
	inline void set__innerhtml_4(String_t* value)
	{
		____innerhtml_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerhtml_4), value);
	}

	inline static int32_t get_offset_of__innerlength_5() { return static_cast<int32_t>(offsetof(HtmlNode_t2048434459, ____innerlength_5)); }
	inline int32_t get__innerlength_5() const { return ____innerlength_5; }
	inline int32_t* get_address_of__innerlength_5() { return &____innerlength_5; }
	inline void set__innerlength_5(int32_t value)
	{
		____innerlength_5 = value;
	}

	inline static int32_t get_offset_of__innerstartindex_6() { return static_cast<int32_t>(offsetof(HtmlNode_t2048434459, ____innerstartindex_6)); }
	inline int32_t get__innerstartindex_6() const { return ____innerstartindex_6; }
	inline int32_t* get_address_of__innerstartindex_6() { return &____innerstartindex_6; }
	inline void set__innerstartindex_6(int32_t value)
	{
		____innerstartindex_6 = value;
	}

	inline static int32_t get_offset_of__line_7() { return static_cast<int32_t>(offsetof(HtmlNode_t2048434459, ____line_7)); }
	inline int32_t get__line_7() const { return ____line_7; }
	inline int32_t* get_address_of__line_7() { return &____line_7; }
	inline void set__line_7(int32_t value)
	{
		____line_7 = value;
	}

	inline static int32_t get_offset_of__lineposition_8() { return static_cast<int32_t>(offsetof(HtmlNode_t2048434459, ____lineposition_8)); }
	inline int32_t get__lineposition_8() const { return ____lineposition_8; }
	inline int32_t* get_address_of__lineposition_8() { return &____lineposition_8; }
	inline void set__lineposition_8(int32_t value)
	{
		____lineposition_8 = value;
	}

	inline static int32_t get_offset_of__name_9() { return static_cast<int32_t>(offsetof(HtmlNode_t2048434459, ____name_9)); }
	inline String_t* get__name_9() const { return ____name_9; }
	inline String_t** get_address_of__name_9() { return &____name_9; }
	inline void set__name_9(String_t* value)
	{
		____name_9 = value;
		Il2CppCodeGenWriteBarrier((&____name_9), value);
	}

	inline static int32_t get_offset_of__namelength_10() { return static_cast<int32_t>(offsetof(HtmlNode_t2048434459, ____namelength_10)); }
	inline int32_t get__namelength_10() const { return ____namelength_10; }
	inline int32_t* get_address_of__namelength_10() { return &____namelength_10; }
	inline void set__namelength_10(int32_t value)
	{
		____namelength_10 = value;
	}

	inline static int32_t get_offset_of__namestartindex_11() { return static_cast<int32_t>(offsetof(HtmlNode_t2048434459, ____namestartindex_11)); }
	inline int32_t get__namestartindex_11() const { return ____namestartindex_11; }
	inline int32_t* get_address_of__namestartindex_11() { return &____namestartindex_11; }
	inline void set__namestartindex_11(int32_t value)
	{
		____namestartindex_11 = value;
	}

	inline static int32_t get_offset_of__nextnode_12() { return static_cast<int32_t>(offsetof(HtmlNode_t2048434459, ____nextnode_12)); }
	inline HtmlNode_t2048434459 * get__nextnode_12() const { return ____nextnode_12; }
	inline HtmlNode_t2048434459 ** get_address_of__nextnode_12() { return &____nextnode_12; }
	inline void set__nextnode_12(HtmlNode_t2048434459 * value)
	{
		____nextnode_12 = value;
		Il2CppCodeGenWriteBarrier((&____nextnode_12), value);
	}

	inline static int32_t get_offset_of__nodetype_13() { return static_cast<int32_t>(offsetof(HtmlNode_t2048434459, ____nodetype_13)); }
	inline int32_t get__nodetype_13() const { return ____nodetype_13; }
	inline int32_t* get_address_of__nodetype_13() { return &____nodetype_13; }
	inline void set__nodetype_13(int32_t value)
	{
		____nodetype_13 = value;
	}

	inline static int32_t get_offset_of__outerchanged_14() { return static_cast<int32_t>(offsetof(HtmlNode_t2048434459, ____outerchanged_14)); }
	inline bool get__outerchanged_14() const { return ____outerchanged_14; }
	inline bool* get_address_of__outerchanged_14() { return &____outerchanged_14; }
	inline void set__outerchanged_14(bool value)
	{
		____outerchanged_14 = value;
	}

	inline static int32_t get_offset_of__outerhtml_15() { return static_cast<int32_t>(offsetof(HtmlNode_t2048434459, ____outerhtml_15)); }
	inline String_t* get__outerhtml_15() const { return ____outerhtml_15; }
	inline String_t** get_address_of__outerhtml_15() { return &____outerhtml_15; }
	inline void set__outerhtml_15(String_t* value)
	{
		____outerhtml_15 = value;
		Il2CppCodeGenWriteBarrier((&____outerhtml_15), value);
	}

	inline static int32_t get_offset_of__outerlength_16() { return static_cast<int32_t>(offsetof(HtmlNode_t2048434459, ____outerlength_16)); }
	inline int32_t get__outerlength_16() const { return ____outerlength_16; }
	inline int32_t* get_address_of__outerlength_16() { return &____outerlength_16; }
	inline void set__outerlength_16(int32_t value)
	{
		____outerlength_16 = value;
	}

	inline static int32_t get_offset_of__outerstartindex_17() { return static_cast<int32_t>(offsetof(HtmlNode_t2048434459, ____outerstartindex_17)); }
	inline int32_t get__outerstartindex_17() const { return ____outerstartindex_17; }
	inline int32_t* get_address_of__outerstartindex_17() { return &____outerstartindex_17; }
	inline void set__outerstartindex_17(int32_t value)
	{
		____outerstartindex_17 = value;
	}

	inline static int32_t get_offset_of__optimizedName_18() { return static_cast<int32_t>(offsetof(HtmlNode_t2048434459, ____optimizedName_18)); }
	inline String_t* get__optimizedName_18() const { return ____optimizedName_18; }
	inline String_t** get_address_of__optimizedName_18() { return &____optimizedName_18; }
	inline void set__optimizedName_18(String_t* value)
	{
		____optimizedName_18 = value;
		Il2CppCodeGenWriteBarrier((&____optimizedName_18), value);
	}

	inline static int32_t get_offset_of__ownerdocument_19() { return static_cast<int32_t>(offsetof(HtmlNode_t2048434459, ____ownerdocument_19)); }
	inline HtmlDocument_t556432108 * get__ownerdocument_19() const { return ____ownerdocument_19; }
	inline HtmlDocument_t556432108 ** get_address_of__ownerdocument_19() { return &____ownerdocument_19; }
	inline void set__ownerdocument_19(HtmlDocument_t556432108 * value)
	{
		____ownerdocument_19 = value;
		Il2CppCodeGenWriteBarrier((&____ownerdocument_19), value);
	}

	inline static int32_t get_offset_of__parentnode_20() { return static_cast<int32_t>(offsetof(HtmlNode_t2048434459, ____parentnode_20)); }
	inline HtmlNode_t2048434459 * get__parentnode_20() const { return ____parentnode_20; }
	inline HtmlNode_t2048434459 ** get_address_of__parentnode_20() { return &____parentnode_20; }
	inline void set__parentnode_20(HtmlNode_t2048434459 * value)
	{
		____parentnode_20 = value;
		Il2CppCodeGenWriteBarrier((&____parentnode_20), value);
	}

	inline static int32_t get_offset_of__prevnode_21() { return static_cast<int32_t>(offsetof(HtmlNode_t2048434459, ____prevnode_21)); }
	inline HtmlNode_t2048434459 * get__prevnode_21() const { return ____prevnode_21; }
	inline HtmlNode_t2048434459 ** get_address_of__prevnode_21() { return &____prevnode_21; }
	inline void set__prevnode_21(HtmlNode_t2048434459 * value)
	{
		____prevnode_21 = value;
		Il2CppCodeGenWriteBarrier((&____prevnode_21), value);
	}

	inline static int32_t get_offset_of__prevwithsamename_22() { return static_cast<int32_t>(offsetof(HtmlNode_t2048434459, ____prevwithsamename_22)); }
	inline HtmlNode_t2048434459 * get__prevwithsamename_22() const { return ____prevwithsamename_22; }
	inline HtmlNode_t2048434459 ** get_address_of__prevwithsamename_22() { return &____prevwithsamename_22; }
	inline void set__prevwithsamename_22(HtmlNode_t2048434459 * value)
	{
		____prevwithsamename_22 = value;
		Il2CppCodeGenWriteBarrier((&____prevwithsamename_22), value);
	}

	inline static int32_t get_offset_of__starttag_23() { return static_cast<int32_t>(offsetof(HtmlNode_t2048434459, ____starttag_23)); }
	inline bool get__starttag_23() const { return ____starttag_23; }
	inline bool* get_address_of__starttag_23() { return &____starttag_23; }
	inline void set__starttag_23(bool value)
	{
		____starttag_23 = value;
	}

	inline static int32_t get_offset_of__streamposition_24() { return static_cast<int32_t>(offsetof(HtmlNode_t2048434459, ____streamposition_24)); }
	inline int32_t get__streamposition_24() const { return ____streamposition_24; }
	inline int32_t* get_address_of__streamposition_24() { return &____streamposition_24; }
	inline void set__streamposition_24(int32_t value)
	{
		____streamposition_24 = value;
	}
};

struct HtmlNode_t2048434459_StaticFields
{
public:
	// System.String HtmlAgilityPack.HtmlNode::HtmlNodeTypeNameComment
	String_t* ___HtmlNodeTypeNameComment_25;
	// System.String HtmlAgilityPack.HtmlNode::HtmlNodeTypeNameDocument
	String_t* ___HtmlNodeTypeNameDocument_26;
	// System.String HtmlAgilityPack.HtmlNode::HtmlNodeTypeNameText
	String_t* ___HtmlNodeTypeNameText_27;
	// System.Collections.Generic.Dictionary`2<System.String,HtmlAgilityPack.HtmlElementFlag> HtmlAgilityPack.HtmlNode::ElementsFlags
	Dictionary_2_t2175053619 * ___ElementsFlags_28;

public:
	inline static int32_t get_offset_of_HtmlNodeTypeNameComment_25() { return static_cast<int32_t>(offsetof(HtmlNode_t2048434459_StaticFields, ___HtmlNodeTypeNameComment_25)); }
	inline String_t* get_HtmlNodeTypeNameComment_25() const { return ___HtmlNodeTypeNameComment_25; }
	inline String_t** get_address_of_HtmlNodeTypeNameComment_25() { return &___HtmlNodeTypeNameComment_25; }
	inline void set_HtmlNodeTypeNameComment_25(String_t* value)
	{
		___HtmlNodeTypeNameComment_25 = value;
		Il2CppCodeGenWriteBarrier((&___HtmlNodeTypeNameComment_25), value);
	}

	inline static int32_t get_offset_of_HtmlNodeTypeNameDocument_26() { return static_cast<int32_t>(offsetof(HtmlNode_t2048434459_StaticFields, ___HtmlNodeTypeNameDocument_26)); }
	inline String_t* get_HtmlNodeTypeNameDocument_26() const { return ___HtmlNodeTypeNameDocument_26; }
	inline String_t** get_address_of_HtmlNodeTypeNameDocument_26() { return &___HtmlNodeTypeNameDocument_26; }
	inline void set_HtmlNodeTypeNameDocument_26(String_t* value)
	{
		___HtmlNodeTypeNameDocument_26 = value;
		Il2CppCodeGenWriteBarrier((&___HtmlNodeTypeNameDocument_26), value);
	}

	inline static int32_t get_offset_of_HtmlNodeTypeNameText_27() { return static_cast<int32_t>(offsetof(HtmlNode_t2048434459_StaticFields, ___HtmlNodeTypeNameText_27)); }
	inline String_t* get_HtmlNodeTypeNameText_27() const { return ___HtmlNodeTypeNameText_27; }
	inline String_t** get_address_of_HtmlNodeTypeNameText_27() { return &___HtmlNodeTypeNameText_27; }
	inline void set_HtmlNodeTypeNameText_27(String_t* value)
	{
		___HtmlNodeTypeNameText_27 = value;
		Il2CppCodeGenWriteBarrier((&___HtmlNodeTypeNameText_27), value);
	}

	inline static int32_t get_offset_of_ElementsFlags_28() { return static_cast<int32_t>(offsetof(HtmlNode_t2048434459_StaticFields, ___ElementsFlags_28)); }
	inline Dictionary_2_t2175053619 * get_ElementsFlags_28() const { return ___ElementsFlags_28; }
	inline Dictionary_2_t2175053619 ** get_address_of_ElementsFlags_28() { return &___ElementsFlags_28; }
	inline void set_ElementsFlags_28(Dictionary_2_t2175053619 * value)
	{
		___ElementsFlags_28 = value;
		Il2CppCodeGenWriteBarrier((&___ElementsFlags_28), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTMLNODE_T2048434459_H
#ifndef HTMLDOCUMENT_T556432108_H
#define HTMLDOCUMENT_T556432108_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HtmlAgilityPack.HtmlDocument
struct  HtmlDocument_t556432108  : public RuntimeObject
{
public:
	// System.Int32 HtmlAgilityPack.HtmlDocument::_c
	int32_t ____c_0;
	// HtmlAgilityPack.Crc32 HtmlAgilityPack.HtmlDocument::_crc32
	Crc32_t83196531 * ____crc32_1;
	// HtmlAgilityPack.HtmlAttribute HtmlAgilityPack.HtmlDocument::_currentattribute
	HtmlAttribute_t1804523403 * ____currentattribute_2;
	// HtmlAgilityPack.HtmlNode HtmlAgilityPack.HtmlDocument::_currentnode
	HtmlNode_t2048434459 * ____currentnode_3;
	// System.Text.Encoding HtmlAgilityPack.HtmlDocument::_declaredencoding
	Encoding_t663144255 * ____declaredencoding_4;
	// HtmlAgilityPack.HtmlNode HtmlAgilityPack.HtmlDocument::_documentnode
	HtmlNode_t2048434459 * ____documentnode_5;
	// System.Boolean HtmlAgilityPack.HtmlDocument::_fullcomment
	bool ____fullcomment_6;
	// System.Int32 HtmlAgilityPack.HtmlDocument::_index
	int32_t ____index_7;
	// System.Collections.Generic.Dictionary`2<System.String,HtmlAgilityPack.HtmlNode> HtmlAgilityPack.HtmlDocument::Lastnodes
	Dictionary_2_t3963213721 * ___Lastnodes_8;
	// HtmlAgilityPack.HtmlNode HtmlAgilityPack.HtmlDocument::_lastparentnode
	HtmlNode_t2048434459 * ____lastparentnode_9;
	// System.Int32 HtmlAgilityPack.HtmlDocument::_line
	int32_t ____line_10;
	// System.Int32 HtmlAgilityPack.HtmlDocument::_lineposition
	int32_t ____lineposition_11;
	// System.Int32 HtmlAgilityPack.HtmlDocument::_maxlineposition
	int32_t ____maxlineposition_12;
	// System.Collections.Generic.Dictionary`2<System.String,HtmlAgilityPack.HtmlNode> HtmlAgilityPack.HtmlDocument::Nodesid
	Dictionary_2_t3963213721 * ___Nodesid_13;
	// HtmlAgilityPack.HtmlDocument/ParseState HtmlAgilityPack.HtmlDocument::_oldstate
	int32_t ____oldstate_14;
	// System.Boolean HtmlAgilityPack.HtmlDocument::_onlyDetectEncoding
	bool ____onlyDetectEncoding_15;
	// System.Collections.Generic.Dictionary`2<System.Int32,HtmlAgilityPack.HtmlNode> HtmlAgilityPack.HtmlDocument::Openednodes
	Dictionary_2_t1056260094 * ___Openednodes_16;
	// System.Collections.Generic.List`1<HtmlAgilityPack.HtmlParseError> HtmlAgilityPack.HtmlDocument::_parseerrors
	List_1_t484300294 * ____parseerrors_17;
	// System.String HtmlAgilityPack.HtmlDocument::_remainder
	String_t* ____remainder_18;
	// System.Int32 HtmlAgilityPack.HtmlDocument::_remainderOffset
	int32_t ____remainderOffset_19;
	// HtmlAgilityPack.HtmlDocument/ParseState HtmlAgilityPack.HtmlDocument::_state
	int32_t ____state_20;
	// System.Text.Encoding HtmlAgilityPack.HtmlDocument::_streamencoding
	Encoding_t663144255 * ____streamencoding_21;
	// System.String HtmlAgilityPack.HtmlDocument::Text
	String_t* ___Text_22;
	// System.Boolean HtmlAgilityPack.HtmlDocument::OptionAddDebuggingAttributes
	bool ___OptionAddDebuggingAttributes_23;
	// System.Boolean HtmlAgilityPack.HtmlDocument::OptionAutoCloseOnEnd
	bool ___OptionAutoCloseOnEnd_24;
	// System.Boolean HtmlAgilityPack.HtmlDocument::OptionCheckSyntax
	bool ___OptionCheckSyntax_25;
	// System.Boolean HtmlAgilityPack.HtmlDocument::OptionComputeChecksum
	bool ___OptionComputeChecksum_26;
	// System.Text.Encoding HtmlAgilityPack.HtmlDocument::OptionDefaultStreamEncoding
	Encoding_t663144255 * ___OptionDefaultStreamEncoding_27;
	// System.Boolean HtmlAgilityPack.HtmlDocument::OptionExtractErrorSourceText
	bool ___OptionExtractErrorSourceText_28;
	// System.Int32 HtmlAgilityPack.HtmlDocument::OptionExtractErrorSourceTextMaxLength
	int32_t ___OptionExtractErrorSourceTextMaxLength_29;
	// System.Boolean HtmlAgilityPack.HtmlDocument::OptionFixNestedTags
	bool ___OptionFixNestedTags_30;
	// System.Boolean HtmlAgilityPack.HtmlDocument::OptionOutputAsXml
	bool ___OptionOutputAsXml_31;
	// System.Boolean HtmlAgilityPack.HtmlDocument::OptionOutputOptimizeAttributeValues
	bool ___OptionOutputOptimizeAttributeValues_32;
	// System.Boolean HtmlAgilityPack.HtmlDocument::OptionOutputOriginalCase
	bool ___OptionOutputOriginalCase_33;
	// System.Boolean HtmlAgilityPack.HtmlDocument::OptionOutputUpperCase
	bool ___OptionOutputUpperCase_34;
	// System.Boolean HtmlAgilityPack.HtmlDocument::OptionReadEncoding
	bool ___OptionReadEncoding_35;
	// System.String HtmlAgilityPack.HtmlDocument::OptionStopperNodeName
	String_t* ___OptionStopperNodeName_36;
	// System.Boolean HtmlAgilityPack.HtmlDocument::OptionUseIdAttribute
	bool ___OptionUseIdAttribute_37;
	// System.Boolean HtmlAgilityPack.HtmlDocument::OptionWriteEmptyNodes
	bool ___OptionWriteEmptyNodes_38;

public:
	inline static int32_t get_offset_of__c_0() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ____c_0)); }
	inline int32_t get__c_0() const { return ____c_0; }
	inline int32_t* get_address_of__c_0() { return &____c_0; }
	inline void set__c_0(int32_t value)
	{
		____c_0 = value;
	}

	inline static int32_t get_offset_of__crc32_1() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ____crc32_1)); }
	inline Crc32_t83196531 * get__crc32_1() const { return ____crc32_1; }
	inline Crc32_t83196531 ** get_address_of__crc32_1() { return &____crc32_1; }
	inline void set__crc32_1(Crc32_t83196531 * value)
	{
		____crc32_1 = value;
		Il2CppCodeGenWriteBarrier((&____crc32_1), value);
	}

	inline static int32_t get_offset_of__currentattribute_2() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ____currentattribute_2)); }
	inline HtmlAttribute_t1804523403 * get__currentattribute_2() const { return ____currentattribute_2; }
	inline HtmlAttribute_t1804523403 ** get_address_of__currentattribute_2() { return &____currentattribute_2; }
	inline void set__currentattribute_2(HtmlAttribute_t1804523403 * value)
	{
		____currentattribute_2 = value;
		Il2CppCodeGenWriteBarrier((&____currentattribute_2), value);
	}

	inline static int32_t get_offset_of__currentnode_3() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ____currentnode_3)); }
	inline HtmlNode_t2048434459 * get__currentnode_3() const { return ____currentnode_3; }
	inline HtmlNode_t2048434459 ** get_address_of__currentnode_3() { return &____currentnode_3; }
	inline void set__currentnode_3(HtmlNode_t2048434459 * value)
	{
		____currentnode_3 = value;
		Il2CppCodeGenWriteBarrier((&____currentnode_3), value);
	}

	inline static int32_t get_offset_of__declaredencoding_4() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ____declaredencoding_4)); }
	inline Encoding_t663144255 * get__declaredencoding_4() const { return ____declaredencoding_4; }
	inline Encoding_t663144255 ** get_address_of__declaredencoding_4() { return &____declaredencoding_4; }
	inline void set__declaredencoding_4(Encoding_t663144255 * value)
	{
		____declaredencoding_4 = value;
		Il2CppCodeGenWriteBarrier((&____declaredencoding_4), value);
	}

	inline static int32_t get_offset_of__documentnode_5() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ____documentnode_5)); }
	inline HtmlNode_t2048434459 * get__documentnode_5() const { return ____documentnode_5; }
	inline HtmlNode_t2048434459 ** get_address_of__documentnode_5() { return &____documentnode_5; }
	inline void set__documentnode_5(HtmlNode_t2048434459 * value)
	{
		____documentnode_5 = value;
		Il2CppCodeGenWriteBarrier((&____documentnode_5), value);
	}

	inline static int32_t get_offset_of__fullcomment_6() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ____fullcomment_6)); }
	inline bool get__fullcomment_6() const { return ____fullcomment_6; }
	inline bool* get_address_of__fullcomment_6() { return &____fullcomment_6; }
	inline void set__fullcomment_6(bool value)
	{
		____fullcomment_6 = value;
	}

	inline static int32_t get_offset_of__index_7() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ____index_7)); }
	inline int32_t get__index_7() const { return ____index_7; }
	inline int32_t* get_address_of__index_7() { return &____index_7; }
	inline void set__index_7(int32_t value)
	{
		____index_7 = value;
	}

	inline static int32_t get_offset_of_Lastnodes_8() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ___Lastnodes_8)); }
	inline Dictionary_2_t3963213721 * get_Lastnodes_8() const { return ___Lastnodes_8; }
	inline Dictionary_2_t3963213721 ** get_address_of_Lastnodes_8() { return &___Lastnodes_8; }
	inline void set_Lastnodes_8(Dictionary_2_t3963213721 * value)
	{
		___Lastnodes_8 = value;
		Il2CppCodeGenWriteBarrier((&___Lastnodes_8), value);
	}

	inline static int32_t get_offset_of__lastparentnode_9() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ____lastparentnode_9)); }
	inline HtmlNode_t2048434459 * get__lastparentnode_9() const { return ____lastparentnode_9; }
	inline HtmlNode_t2048434459 ** get_address_of__lastparentnode_9() { return &____lastparentnode_9; }
	inline void set__lastparentnode_9(HtmlNode_t2048434459 * value)
	{
		____lastparentnode_9 = value;
		Il2CppCodeGenWriteBarrier((&____lastparentnode_9), value);
	}

	inline static int32_t get_offset_of__line_10() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ____line_10)); }
	inline int32_t get__line_10() const { return ____line_10; }
	inline int32_t* get_address_of__line_10() { return &____line_10; }
	inline void set__line_10(int32_t value)
	{
		____line_10 = value;
	}

	inline static int32_t get_offset_of__lineposition_11() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ____lineposition_11)); }
	inline int32_t get__lineposition_11() const { return ____lineposition_11; }
	inline int32_t* get_address_of__lineposition_11() { return &____lineposition_11; }
	inline void set__lineposition_11(int32_t value)
	{
		____lineposition_11 = value;
	}

	inline static int32_t get_offset_of__maxlineposition_12() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ____maxlineposition_12)); }
	inline int32_t get__maxlineposition_12() const { return ____maxlineposition_12; }
	inline int32_t* get_address_of__maxlineposition_12() { return &____maxlineposition_12; }
	inline void set__maxlineposition_12(int32_t value)
	{
		____maxlineposition_12 = value;
	}

	inline static int32_t get_offset_of_Nodesid_13() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ___Nodesid_13)); }
	inline Dictionary_2_t3963213721 * get_Nodesid_13() const { return ___Nodesid_13; }
	inline Dictionary_2_t3963213721 ** get_address_of_Nodesid_13() { return &___Nodesid_13; }
	inline void set_Nodesid_13(Dictionary_2_t3963213721 * value)
	{
		___Nodesid_13 = value;
		Il2CppCodeGenWriteBarrier((&___Nodesid_13), value);
	}

	inline static int32_t get_offset_of__oldstate_14() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ____oldstate_14)); }
	inline int32_t get__oldstate_14() const { return ____oldstate_14; }
	inline int32_t* get_address_of__oldstate_14() { return &____oldstate_14; }
	inline void set__oldstate_14(int32_t value)
	{
		____oldstate_14 = value;
	}

	inline static int32_t get_offset_of__onlyDetectEncoding_15() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ____onlyDetectEncoding_15)); }
	inline bool get__onlyDetectEncoding_15() const { return ____onlyDetectEncoding_15; }
	inline bool* get_address_of__onlyDetectEncoding_15() { return &____onlyDetectEncoding_15; }
	inline void set__onlyDetectEncoding_15(bool value)
	{
		____onlyDetectEncoding_15 = value;
	}

	inline static int32_t get_offset_of_Openednodes_16() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ___Openednodes_16)); }
	inline Dictionary_2_t1056260094 * get_Openednodes_16() const { return ___Openednodes_16; }
	inline Dictionary_2_t1056260094 ** get_address_of_Openednodes_16() { return &___Openednodes_16; }
	inline void set_Openednodes_16(Dictionary_2_t1056260094 * value)
	{
		___Openednodes_16 = value;
		Il2CppCodeGenWriteBarrier((&___Openednodes_16), value);
	}

	inline static int32_t get_offset_of__parseerrors_17() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ____parseerrors_17)); }
	inline List_1_t484300294 * get__parseerrors_17() const { return ____parseerrors_17; }
	inline List_1_t484300294 ** get_address_of__parseerrors_17() { return &____parseerrors_17; }
	inline void set__parseerrors_17(List_1_t484300294 * value)
	{
		____parseerrors_17 = value;
		Il2CppCodeGenWriteBarrier((&____parseerrors_17), value);
	}

	inline static int32_t get_offset_of__remainder_18() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ____remainder_18)); }
	inline String_t* get__remainder_18() const { return ____remainder_18; }
	inline String_t** get_address_of__remainder_18() { return &____remainder_18; }
	inline void set__remainder_18(String_t* value)
	{
		____remainder_18 = value;
		Il2CppCodeGenWriteBarrier((&____remainder_18), value);
	}

	inline static int32_t get_offset_of__remainderOffset_19() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ____remainderOffset_19)); }
	inline int32_t get__remainderOffset_19() const { return ____remainderOffset_19; }
	inline int32_t* get_address_of__remainderOffset_19() { return &____remainderOffset_19; }
	inline void set__remainderOffset_19(int32_t value)
	{
		____remainderOffset_19 = value;
	}

	inline static int32_t get_offset_of__state_20() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ____state_20)); }
	inline int32_t get__state_20() const { return ____state_20; }
	inline int32_t* get_address_of__state_20() { return &____state_20; }
	inline void set__state_20(int32_t value)
	{
		____state_20 = value;
	}

	inline static int32_t get_offset_of__streamencoding_21() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ____streamencoding_21)); }
	inline Encoding_t663144255 * get__streamencoding_21() const { return ____streamencoding_21; }
	inline Encoding_t663144255 ** get_address_of__streamencoding_21() { return &____streamencoding_21; }
	inline void set__streamencoding_21(Encoding_t663144255 * value)
	{
		____streamencoding_21 = value;
		Il2CppCodeGenWriteBarrier((&____streamencoding_21), value);
	}

	inline static int32_t get_offset_of_Text_22() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ___Text_22)); }
	inline String_t* get_Text_22() const { return ___Text_22; }
	inline String_t** get_address_of_Text_22() { return &___Text_22; }
	inline void set_Text_22(String_t* value)
	{
		___Text_22 = value;
		Il2CppCodeGenWriteBarrier((&___Text_22), value);
	}

	inline static int32_t get_offset_of_OptionAddDebuggingAttributes_23() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ___OptionAddDebuggingAttributes_23)); }
	inline bool get_OptionAddDebuggingAttributes_23() const { return ___OptionAddDebuggingAttributes_23; }
	inline bool* get_address_of_OptionAddDebuggingAttributes_23() { return &___OptionAddDebuggingAttributes_23; }
	inline void set_OptionAddDebuggingAttributes_23(bool value)
	{
		___OptionAddDebuggingAttributes_23 = value;
	}

	inline static int32_t get_offset_of_OptionAutoCloseOnEnd_24() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ___OptionAutoCloseOnEnd_24)); }
	inline bool get_OptionAutoCloseOnEnd_24() const { return ___OptionAutoCloseOnEnd_24; }
	inline bool* get_address_of_OptionAutoCloseOnEnd_24() { return &___OptionAutoCloseOnEnd_24; }
	inline void set_OptionAutoCloseOnEnd_24(bool value)
	{
		___OptionAutoCloseOnEnd_24 = value;
	}

	inline static int32_t get_offset_of_OptionCheckSyntax_25() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ___OptionCheckSyntax_25)); }
	inline bool get_OptionCheckSyntax_25() const { return ___OptionCheckSyntax_25; }
	inline bool* get_address_of_OptionCheckSyntax_25() { return &___OptionCheckSyntax_25; }
	inline void set_OptionCheckSyntax_25(bool value)
	{
		___OptionCheckSyntax_25 = value;
	}

	inline static int32_t get_offset_of_OptionComputeChecksum_26() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ___OptionComputeChecksum_26)); }
	inline bool get_OptionComputeChecksum_26() const { return ___OptionComputeChecksum_26; }
	inline bool* get_address_of_OptionComputeChecksum_26() { return &___OptionComputeChecksum_26; }
	inline void set_OptionComputeChecksum_26(bool value)
	{
		___OptionComputeChecksum_26 = value;
	}

	inline static int32_t get_offset_of_OptionDefaultStreamEncoding_27() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ___OptionDefaultStreamEncoding_27)); }
	inline Encoding_t663144255 * get_OptionDefaultStreamEncoding_27() const { return ___OptionDefaultStreamEncoding_27; }
	inline Encoding_t663144255 ** get_address_of_OptionDefaultStreamEncoding_27() { return &___OptionDefaultStreamEncoding_27; }
	inline void set_OptionDefaultStreamEncoding_27(Encoding_t663144255 * value)
	{
		___OptionDefaultStreamEncoding_27 = value;
		Il2CppCodeGenWriteBarrier((&___OptionDefaultStreamEncoding_27), value);
	}

	inline static int32_t get_offset_of_OptionExtractErrorSourceText_28() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ___OptionExtractErrorSourceText_28)); }
	inline bool get_OptionExtractErrorSourceText_28() const { return ___OptionExtractErrorSourceText_28; }
	inline bool* get_address_of_OptionExtractErrorSourceText_28() { return &___OptionExtractErrorSourceText_28; }
	inline void set_OptionExtractErrorSourceText_28(bool value)
	{
		___OptionExtractErrorSourceText_28 = value;
	}

	inline static int32_t get_offset_of_OptionExtractErrorSourceTextMaxLength_29() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ___OptionExtractErrorSourceTextMaxLength_29)); }
	inline int32_t get_OptionExtractErrorSourceTextMaxLength_29() const { return ___OptionExtractErrorSourceTextMaxLength_29; }
	inline int32_t* get_address_of_OptionExtractErrorSourceTextMaxLength_29() { return &___OptionExtractErrorSourceTextMaxLength_29; }
	inline void set_OptionExtractErrorSourceTextMaxLength_29(int32_t value)
	{
		___OptionExtractErrorSourceTextMaxLength_29 = value;
	}

	inline static int32_t get_offset_of_OptionFixNestedTags_30() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ___OptionFixNestedTags_30)); }
	inline bool get_OptionFixNestedTags_30() const { return ___OptionFixNestedTags_30; }
	inline bool* get_address_of_OptionFixNestedTags_30() { return &___OptionFixNestedTags_30; }
	inline void set_OptionFixNestedTags_30(bool value)
	{
		___OptionFixNestedTags_30 = value;
	}

	inline static int32_t get_offset_of_OptionOutputAsXml_31() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ___OptionOutputAsXml_31)); }
	inline bool get_OptionOutputAsXml_31() const { return ___OptionOutputAsXml_31; }
	inline bool* get_address_of_OptionOutputAsXml_31() { return &___OptionOutputAsXml_31; }
	inline void set_OptionOutputAsXml_31(bool value)
	{
		___OptionOutputAsXml_31 = value;
	}

	inline static int32_t get_offset_of_OptionOutputOptimizeAttributeValues_32() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ___OptionOutputOptimizeAttributeValues_32)); }
	inline bool get_OptionOutputOptimizeAttributeValues_32() const { return ___OptionOutputOptimizeAttributeValues_32; }
	inline bool* get_address_of_OptionOutputOptimizeAttributeValues_32() { return &___OptionOutputOptimizeAttributeValues_32; }
	inline void set_OptionOutputOptimizeAttributeValues_32(bool value)
	{
		___OptionOutputOptimizeAttributeValues_32 = value;
	}

	inline static int32_t get_offset_of_OptionOutputOriginalCase_33() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ___OptionOutputOriginalCase_33)); }
	inline bool get_OptionOutputOriginalCase_33() const { return ___OptionOutputOriginalCase_33; }
	inline bool* get_address_of_OptionOutputOriginalCase_33() { return &___OptionOutputOriginalCase_33; }
	inline void set_OptionOutputOriginalCase_33(bool value)
	{
		___OptionOutputOriginalCase_33 = value;
	}

	inline static int32_t get_offset_of_OptionOutputUpperCase_34() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ___OptionOutputUpperCase_34)); }
	inline bool get_OptionOutputUpperCase_34() const { return ___OptionOutputUpperCase_34; }
	inline bool* get_address_of_OptionOutputUpperCase_34() { return &___OptionOutputUpperCase_34; }
	inline void set_OptionOutputUpperCase_34(bool value)
	{
		___OptionOutputUpperCase_34 = value;
	}

	inline static int32_t get_offset_of_OptionReadEncoding_35() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ___OptionReadEncoding_35)); }
	inline bool get_OptionReadEncoding_35() const { return ___OptionReadEncoding_35; }
	inline bool* get_address_of_OptionReadEncoding_35() { return &___OptionReadEncoding_35; }
	inline void set_OptionReadEncoding_35(bool value)
	{
		___OptionReadEncoding_35 = value;
	}

	inline static int32_t get_offset_of_OptionStopperNodeName_36() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ___OptionStopperNodeName_36)); }
	inline String_t* get_OptionStopperNodeName_36() const { return ___OptionStopperNodeName_36; }
	inline String_t** get_address_of_OptionStopperNodeName_36() { return &___OptionStopperNodeName_36; }
	inline void set_OptionStopperNodeName_36(String_t* value)
	{
		___OptionStopperNodeName_36 = value;
		Il2CppCodeGenWriteBarrier((&___OptionStopperNodeName_36), value);
	}

	inline static int32_t get_offset_of_OptionUseIdAttribute_37() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ___OptionUseIdAttribute_37)); }
	inline bool get_OptionUseIdAttribute_37() const { return ___OptionUseIdAttribute_37; }
	inline bool* get_address_of_OptionUseIdAttribute_37() { return &___OptionUseIdAttribute_37; }
	inline void set_OptionUseIdAttribute_37(bool value)
	{
		___OptionUseIdAttribute_37 = value;
	}

	inline static int32_t get_offset_of_OptionWriteEmptyNodes_38() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ___OptionWriteEmptyNodes_38)); }
	inline bool get_OptionWriteEmptyNodes_38() const { return ___OptionWriteEmptyNodes_38; }
	inline bool* get_address_of_OptionWriteEmptyNodes_38() { return &___OptionWriteEmptyNodes_38; }
	inline void set_OptionWriteEmptyNodes_38(bool value)
	{
		___OptionWriteEmptyNodes_38 = value;
	}
};

struct HtmlDocument_t556432108_StaticFields
{
public:
	// System.String HtmlAgilityPack.HtmlDocument::HtmlExceptionRefNotChild
	String_t* ___HtmlExceptionRefNotChild_39;
	// System.String HtmlAgilityPack.HtmlDocument::HtmlExceptionUseIdAttributeFalse
	String_t* ___HtmlExceptionUseIdAttributeFalse_40;

public:
	inline static int32_t get_offset_of_HtmlExceptionRefNotChild_39() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108_StaticFields, ___HtmlExceptionRefNotChild_39)); }
	inline String_t* get_HtmlExceptionRefNotChild_39() const { return ___HtmlExceptionRefNotChild_39; }
	inline String_t** get_address_of_HtmlExceptionRefNotChild_39() { return &___HtmlExceptionRefNotChild_39; }
	inline void set_HtmlExceptionRefNotChild_39(String_t* value)
	{
		___HtmlExceptionRefNotChild_39 = value;
		Il2CppCodeGenWriteBarrier((&___HtmlExceptionRefNotChild_39), value);
	}

	inline static int32_t get_offset_of_HtmlExceptionUseIdAttributeFalse_40() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108_StaticFields, ___HtmlExceptionUseIdAttributeFalse_40)); }
	inline String_t* get_HtmlExceptionUseIdAttributeFalse_40() const { return ___HtmlExceptionUseIdAttributeFalse_40; }
	inline String_t** get_address_of_HtmlExceptionUseIdAttributeFalse_40() { return &___HtmlExceptionUseIdAttributeFalse_40; }
	inline void set_HtmlExceptionUseIdAttributeFalse_40(String_t* value)
	{
		___HtmlExceptionUseIdAttributeFalse_40 = value;
		Il2CppCodeGenWriteBarrier((&___HtmlExceptionUseIdAttributeFalse_40), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTMLDOCUMENT_T556432108_H
#ifndef HTMLATTRIBUTE_T1804523403_H
#define HTMLATTRIBUTE_T1804523403_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HtmlAgilityPack.HtmlAttribute
struct  HtmlAttribute_t1804523403  : public RuntimeObject
{
public:
	// System.Int32 HtmlAgilityPack.HtmlAttribute::_line
	int32_t ____line_0;
	// System.Int32 HtmlAgilityPack.HtmlAttribute::_lineposition
	int32_t ____lineposition_1;
	// System.String HtmlAgilityPack.HtmlAttribute::_name
	String_t* ____name_2;
	// System.Int32 HtmlAgilityPack.HtmlAttribute::_namelength
	int32_t ____namelength_3;
	// System.Int32 HtmlAgilityPack.HtmlAttribute::_namestartindex
	int32_t ____namestartindex_4;
	// HtmlAgilityPack.HtmlDocument HtmlAgilityPack.HtmlAttribute::_ownerdocument
	HtmlDocument_t556432108 * ____ownerdocument_5;
	// HtmlAgilityPack.HtmlNode HtmlAgilityPack.HtmlAttribute::_ownernode
	HtmlNode_t2048434459 * ____ownernode_6;
	// HtmlAgilityPack.AttributeValueQuote HtmlAgilityPack.HtmlAttribute::_quoteType
	int32_t ____quoteType_7;
	// System.Int32 HtmlAgilityPack.HtmlAttribute::_streamposition
	int32_t ____streamposition_8;
	// System.String HtmlAgilityPack.HtmlAttribute::_value
	String_t* ____value_9;
	// System.Int32 HtmlAgilityPack.HtmlAttribute::_valuelength
	int32_t ____valuelength_10;
	// System.Int32 HtmlAgilityPack.HtmlAttribute::_valuestartindex
	int32_t ____valuestartindex_11;

public:
	inline static int32_t get_offset_of__line_0() { return static_cast<int32_t>(offsetof(HtmlAttribute_t1804523403, ____line_0)); }
	inline int32_t get__line_0() const { return ____line_0; }
	inline int32_t* get_address_of__line_0() { return &____line_0; }
	inline void set__line_0(int32_t value)
	{
		____line_0 = value;
	}

	inline static int32_t get_offset_of__lineposition_1() { return static_cast<int32_t>(offsetof(HtmlAttribute_t1804523403, ____lineposition_1)); }
	inline int32_t get__lineposition_1() const { return ____lineposition_1; }
	inline int32_t* get_address_of__lineposition_1() { return &____lineposition_1; }
	inline void set__lineposition_1(int32_t value)
	{
		____lineposition_1 = value;
	}

	inline static int32_t get_offset_of__name_2() { return static_cast<int32_t>(offsetof(HtmlAttribute_t1804523403, ____name_2)); }
	inline String_t* get__name_2() const { return ____name_2; }
	inline String_t** get_address_of__name_2() { return &____name_2; }
	inline void set__name_2(String_t* value)
	{
		____name_2 = value;
		Il2CppCodeGenWriteBarrier((&____name_2), value);
	}

	inline static int32_t get_offset_of__namelength_3() { return static_cast<int32_t>(offsetof(HtmlAttribute_t1804523403, ____namelength_3)); }
	inline int32_t get__namelength_3() const { return ____namelength_3; }
	inline int32_t* get_address_of__namelength_3() { return &____namelength_3; }
	inline void set__namelength_3(int32_t value)
	{
		____namelength_3 = value;
	}

	inline static int32_t get_offset_of__namestartindex_4() { return static_cast<int32_t>(offsetof(HtmlAttribute_t1804523403, ____namestartindex_4)); }
	inline int32_t get__namestartindex_4() const { return ____namestartindex_4; }
	inline int32_t* get_address_of__namestartindex_4() { return &____namestartindex_4; }
	inline void set__namestartindex_4(int32_t value)
	{
		____namestartindex_4 = value;
	}

	inline static int32_t get_offset_of__ownerdocument_5() { return static_cast<int32_t>(offsetof(HtmlAttribute_t1804523403, ____ownerdocument_5)); }
	inline HtmlDocument_t556432108 * get__ownerdocument_5() const { return ____ownerdocument_5; }
	inline HtmlDocument_t556432108 ** get_address_of__ownerdocument_5() { return &____ownerdocument_5; }
	inline void set__ownerdocument_5(HtmlDocument_t556432108 * value)
	{
		____ownerdocument_5 = value;
		Il2CppCodeGenWriteBarrier((&____ownerdocument_5), value);
	}

	inline static int32_t get_offset_of__ownernode_6() { return static_cast<int32_t>(offsetof(HtmlAttribute_t1804523403, ____ownernode_6)); }
	inline HtmlNode_t2048434459 * get__ownernode_6() const { return ____ownernode_6; }
	inline HtmlNode_t2048434459 ** get_address_of__ownernode_6() { return &____ownernode_6; }
	inline void set__ownernode_6(HtmlNode_t2048434459 * value)
	{
		____ownernode_6 = value;
		Il2CppCodeGenWriteBarrier((&____ownernode_6), value);
	}

	inline static int32_t get_offset_of__quoteType_7() { return static_cast<int32_t>(offsetof(HtmlAttribute_t1804523403, ____quoteType_7)); }
	inline int32_t get__quoteType_7() const { return ____quoteType_7; }
	inline int32_t* get_address_of__quoteType_7() { return &____quoteType_7; }
	inline void set__quoteType_7(int32_t value)
	{
		____quoteType_7 = value;
	}

	inline static int32_t get_offset_of__streamposition_8() { return static_cast<int32_t>(offsetof(HtmlAttribute_t1804523403, ____streamposition_8)); }
	inline int32_t get__streamposition_8() const { return ____streamposition_8; }
	inline int32_t* get_address_of__streamposition_8() { return &____streamposition_8; }
	inline void set__streamposition_8(int32_t value)
	{
		____streamposition_8 = value;
	}

	inline static int32_t get_offset_of__value_9() { return static_cast<int32_t>(offsetof(HtmlAttribute_t1804523403, ____value_9)); }
	inline String_t* get__value_9() const { return ____value_9; }
	inline String_t** get_address_of__value_9() { return &____value_9; }
	inline void set__value_9(String_t* value)
	{
		____value_9 = value;
		Il2CppCodeGenWriteBarrier((&____value_9), value);
	}

	inline static int32_t get_offset_of__valuelength_10() { return static_cast<int32_t>(offsetof(HtmlAttribute_t1804523403, ____valuelength_10)); }
	inline int32_t get__valuelength_10() const { return ____valuelength_10; }
	inline int32_t* get_address_of__valuelength_10() { return &____valuelength_10; }
	inline void set__valuelength_10(int32_t value)
	{
		____valuelength_10 = value;
	}

	inline static int32_t get_offset_of__valuestartindex_11() { return static_cast<int32_t>(offsetof(HtmlAttribute_t1804523403, ____valuestartindex_11)); }
	inline int32_t get__valuestartindex_11() const { return ____valuestartindex_11; }
	inline int32_t* get_address_of__valuestartindex_11() { return &____valuestartindex_11; }
	inline void set__valuestartindex_11(int32_t value)
	{
		____valuestartindex_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTMLATTRIBUTE_T1804523403_H
#ifndef SCRIPTABLEOBJECT_T1975622470_H
#define SCRIPTABLEOBJECT_T1975622470_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t1975622470  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t1975622470_marshaled_pinvoke : public Object_t1021602117_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t1975622470_marshaled_com : public Object_t1021602117_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T1975622470_H
#ifndef SCRIPTPLAYABLEOUTPUT_T1666207618_H
#define SCRIPTPLAYABLEOUTPUT_T1666207618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.ScriptPlayableOutput
struct  ScriptPlayableOutput_t1666207618 
{
public:
	// UnityEngine.Playables.PlayableOutputHandle UnityEngine.Playables.ScriptPlayableOutput::m_Handle
	PlayableOutputHandle_t551742311  ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(ScriptPlayableOutput_t1666207618, ___m_Handle_0)); }
	inline PlayableOutputHandle_t551742311  get_m_Handle_0() const { return ___m_Handle_0; }
	inline PlayableOutputHandle_t551742311 * get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(PlayableOutputHandle_t551742311  value)
	{
		___m_Handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRIPTPLAYABLEOUTPUT_T1666207618_H
#ifndef PLAYERCONNECTION_T3517219175_H
#define PLAYERCONNECTION_T3517219175_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.PlayerConnection.PlayerConnection
struct  PlayerConnection_t3517219175  : public ScriptableObject_t1975622470
{
public:
	// UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents UnityEngine.Networking.PlayerConnection.PlayerConnection::m_PlayerEditorConnectionEvents
	PlayerEditorConnectionEvents_t2252784345 * ___m_PlayerEditorConnectionEvents_2;
	// System.Collections.Generic.List`1<System.Int32> UnityEngine.Networking.PlayerConnection.PlayerConnection::m_connectedPlayers
	List_1_t1440998580 * ___m_connectedPlayers_3;

public:
	inline static int32_t get_offset_of_m_PlayerEditorConnectionEvents_2() { return static_cast<int32_t>(offsetof(PlayerConnection_t3517219175, ___m_PlayerEditorConnectionEvents_2)); }
	inline PlayerEditorConnectionEvents_t2252784345 * get_m_PlayerEditorConnectionEvents_2() const { return ___m_PlayerEditorConnectionEvents_2; }
	inline PlayerEditorConnectionEvents_t2252784345 ** get_address_of_m_PlayerEditorConnectionEvents_2() { return &___m_PlayerEditorConnectionEvents_2; }
	inline void set_m_PlayerEditorConnectionEvents_2(PlayerEditorConnectionEvents_t2252784345 * value)
	{
		___m_PlayerEditorConnectionEvents_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_PlayerEditorConnectionEvents_2), value);
	}

	inline static int32_t get_offset_of_m_connectedPlayers_3() { return static_cast<int32_t>(offsetof(PlayerConnection_t3517219175, ___m_connectedPlayers_3)); }
	inline List_1_t1440998580 * get_m_connectedPlayers_3() const { return ___m_connectedPlayers_3; }
	inline List_1_t1440998580 ** get_address_of_m_connectedPlayers_3() { return &___m_connectedPlayers_3; }
	inline void set_m_connectedPlayers_3(List_1_t1440998580 * value)
	{
		___m_connectedPlayers_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_connectedPlayers_3), value);
	}
};

struct PlayerConnection_t3517219175_StaticFields
{
public:
	// UnityEngine.Networking.PlayerConnection.PlayerConnection UnityEngine.Networking.PlayerConnection.PlayerConnection::s_Instance
	PlayerConnection_t3517219175 * ___s_Instance_4;

public:
	inline static int32_t get_offset_of_s_Instance_4() { return static_cast<int32_t>(offsetof(PlayerConnection_t3517219175_StaticFields, ___s_Instance_4)); }
	inline PlayerConnection_t3517219175 * get_s_Instance_4() const { return ___s_Instance_4; }
	inline PlayerConnection_t3517219175 ** get_address_of_s_Instance_4() { return &___s_Instance_4; }
	inline void set_s_Instance_4(PlayerConnection_t3517219175 * value)
	{
		___s_Instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERCONNECTION_T3517219175_H
#ifndef PLAYABLEASSET_T2298101656_H
#define PLAYABLEASSET_T2298101656_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableAsset
struct  PlayableAsset_t2298101656  : public ScriptableObject_t1975622470
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEASSET_T2298101656_H
#ifndef DISPATCHER_T2240407071_H
#define DISPATCHER_T2240407071_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Boo.Lang.Runtime.DynamicDispatching.Dispatcher
struct  Dispatcher_t2240407071  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISPATCHER_T2240407071_H
#ifndef HTMLTEXTNODE_T2710098554_H
#define HTMLTEXTNODE_T2710098554_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HtmlAgilityPack.HtmlTextNode
struct  HtmlTextNode_t2710098554  : public HtmlNode_t2048434459
{
public:
	// System.String HtmlAgilityPack.HtmlTextNode::_text
	String_t* ____text_29;

public:
	inline static int32_t get_offset_of__text_29() { return static_cast<int32_t>(offsetof(HtmlTextNode_t2710098554, ____text_29)); }
	inline String_t* get__text_29() const { return ____text_29; }
	inline String_t** get_address_of__text_29() { return &____text_29; }
	inline void set__text_29(String_t* value)
	{
		____text_29 = value;
		Il2CppCodeGenWriteBarrier((&____text_29), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTMLTEXTNODE_T2710098554_H
#ifndef DISPATCHERFACTORY_T1307565918_H
#define DISPATCHERFACTORY_T1307565918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Boo.Lang.Runtime.DynamicDispatching.DispatcherCache/DispatcherFactory
struct  DispatcherFactory_t1307565918  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISPATCHERFACTORY_T1307565918_H
#ifndef CSSMEASUREFUNC_T2092170507_H
#define CSSMEASUREFUNC_T2092170507_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CSSLayout.CSSMeasureFunc
struct  CSSMeasureFunc_t2092170507  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CSSMEASUREFUNC_T2092170507_H
#ifndef HTMLCOMMENTNODE_T1992371332_H
#define HTMLCOMMENTNODE_T1992371332_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HtmlAgilityPack.HtmlCommentNode
struct  HtmlCommentNode_t1992371332  : public HtmlNode_t2048434459
{
public:
	// System.String HtmlAgilityPack.HtmlCommentNode::_comment
	String_t* ____comment_29;

public:
	inline static int32_t get_offset_of__comment_29() { return static_cast<int32_t>(offsetof(HtmlCommentNode_t1992371332, ____comment_29)); }
	inline String_t* get__comment_29() const { return ____comment_29; }
	inline String_t** get_address_of__comment_29() { return &____comment_29; }
	inline void set__comment_29(String_t* value)
	{
		____comment_29 = value;
		Il2CppCodeGenWriteBarrier((&____comment_29), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTMLCOMMENTNODE_T1992371332_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1900 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1901 = { sizeof (PlayableAsset_t2298101656), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1902 = { sizeof (PlayableBehaviour_t2517866383), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1903 = { sizeof (PlayableExtensions_t2358977238), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1904 = { sizeof (ScriptPlayableOutput_t1666207618)+ sizeof (RuntimeObject), sizeof(ScriptPlayableOutput_t1666207618 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1904[1] = 
{
	ScriptPlayableOutput_t1666207618::get_offset_of_m_Handle_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1905 = { sizeof (DefaultValueAttribute_t1027170048), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1905[1] = 
{
	DefaultValueAttribute_t1027170048::get_offset_of_DefaultValue_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1906 = { sizeof (ExcludeFromDocsAttribute_t665825653), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1907 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1908 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1909 = { sizeof (Logger_t3328995178), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1909[3] = 
{
	Logger_t3328995178::get_offset_of_U3ClogHandlerU3Ek__BackingField_0(),
	Logger_t3328995178::get_offset_of_U3ClogEnabledU3Ek__BackingField_1(),
	Logger_t3328995178::get_offset_of_U3CfilterLogTypeU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1910 = { sizeof (MessageEventArgs_t301283622), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1910[2] = 
{
	MessageEventArgs_t301283622::get_offset_of_playerId_0(),
	MessageEventArgs_t301283622::get_offset_of_data_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1911 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1912 = { sizeof (PlayerConnection_t3517219175), -1, sizeof(PlayerConnection_t3517219175_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1912[3] = 
{
	PlayerConnection_t3517219175::get_offset_of_m_PlayerEditorConnectionEvents_2(),
	PlayerConnection_t3517219175::get_offset_of_m_connectedPlayers_3(),
	PlayerConnection_t3517219175_StaticFields::get_offset_of_s_Instance_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1913 = { sizeof (PlayerEditorConnectionEvents_t2252784345), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1913[3] = 
{
	PlayerEditorConnectionEvents_t2252784345::get_offset_of_messageTypeSubscribers_0(),
	PlayerEditorConnectionEvents_t2252784345::get_offset_of_connectionEvent_1(),
	PlayerEditorConnectionEvents_t2252784345::get_offset_of_disconnectionEvent_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1914 = { sizeof (MessageEvent_t2167079021), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1915 = { sizeof (ConnectionChangeEvent_t536719976), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1916 = { sizeof (MessageTypeSubscribers_t2291506050), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1916[3] = 
{
	MessageTypeSubscribers_t2291506050::get_offset_of_m_messageTypeId_0(),
	MessageTypeSubscribers_t2291506050::get_offset_of_subscriberCount_1(),
	MessageTypeSubscribers_t2291506050::get_offset_of_messageCallback_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1917 = { sizeof (U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t1899782350), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1917[1] = 
{
	U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t1899782350::get_offset_of_messageId_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1918 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1919 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1920 = { sizeof (RenderPipelineManager_t984453155), -1, sizeof(RenderPipelineManager_t984453155_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1920[2] = 
{
	RenderPipelineManager_t984453155_StaticFields::get_offset_of_s_CurrentPipelineAsset_0(),
	RenderPipelineManager_t984453155_StaticFields::get_offset_of_U3CcurrentPipelineU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1921 = { sizeof (UsedByNativeCodeAttribute_t3212052468), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1922 = { sizeof (RequiredByNativeCodeAttribute_t1913052472), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1922[2] = 
{
	RequiredByNativeCodeAttribute_t1913052472::get_offset_of_U3CNameU3Ek__BackingField_0(),
	RequiredByNativeCodeAttribute_t1913052472::get_offset_of_U3COptionalU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1923 = { sizeof (MovedFromAttribute_t922195725), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1923[1] = 
{
	MovedFromAttribute_t922195725::get_offset_of_U3CNamespaceU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1924 = { sizeof (FormerlySerializedAsAttribute_t3673080018), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1924[1] = 
{
	FormerlySerializedAsAttribute_t3673080018::get_offset_of_m_oldName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1925 = { sizeof (TypeInferenceRules_t1810425448)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1925[5] = 
{
	TypeInferenceRules_t1810425448::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1926 = { sizeof (TypeInferenceRuleAttribute_t1390152093), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1926[1] = 
{
	TypeInferenceRuleAttribute_t1390152093::get_offset_of__rule_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1927 = { sizeof (GenericStack_t3718539591), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1928 = { sizeof (NetFxCoreExtensions_t4275971970), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1929 = { sizeof (CSSMeasureFunc_t2092170507), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1930 = { sizeof (CSSMeasureMode_t2118763144)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1930[4] = 
{
	CSSMeasureMode_t2118763144::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1931 = { sizeof (CSSSize_t1564391216)+ sizeof (RuntimeObject), sizeof(CSSSize_t1564391216 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1931[2] = 
{
	CSSSize_t1564391216::get_offset_of_width_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CSSSize_t1564391216::get_offset_of_height_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1932 = { sizeof (Native_t2308112383), -1, sizeof(Native_t2308112383_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1932[1] = 
{
	Native_t2308112383_StaticFields::get_offset_of_s_MeasureFunctions_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1933 = { sizeof (U3CModuleU3E_t3783534221), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1934 = { sizeof (Builtins_t3763248930), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1935 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1935[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1936 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1936[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1937 = { sizeof (DispatcherCache_t2227862569), -1, sizeof(DispatcherCache_t2227862569_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1937[1] = 
{
	DispatcherCache_t2227862569_StaticFields::get_offset_of__cache_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1938 = { sizeof (DispatcherFactory_t1307565918), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1939 = { sizeof (DispatcherKey_t708950850), -1, sizeof(DispatcherKey_t708950850_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1939[4] = 
{
	DispatcherKey_t708950850_StaticFields::get_offset_of_EqualityComparer_0(),
	DispatcherKey_t708950850::get_offset_of__type_1(),
	DispatcherKey_t708950850::get_offset_of__name_2(),
	DispatcherKey_t708950850::get_offset_of__arguments_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1940 = { sizeof (_EqualityComparer_t1344382164), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1941 = { sizeof (NumericPromotions_t3345193737), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1942 = { sizeof (ExtensionRegistry_t1402255936), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1942[2] = 
{
	ExtensionRegistry_t1402255936::get_offset_of__extensions_0(),
	ExtensionRegistry_t1402255936::get_offset_of__classLock_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1943 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1944 = { sizeof (RuntimeServices_t1910041954), -1, sizeof(RuntimeServices_t1910041954_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1944[5] = 
{
	RuntimeServices_t1910041954_StaticFields::get_offset_of_NoArguments_0(),
	RuntimeServices_t1910041954_StaticFields::get_offset_of_RuntimeServicesType_1(),
	RuntimeServices_t1910041954_StaticFields::get_offset_of__cache_2(),
	RuntimeServices_t1910041954_StaticFields::get_offset_of__extensions_3(),
	RuntimeServices_t1910041954_StaticFields::get_offset_of_True_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1945 = { sizeof (U3CGetExtensionMethodsU3Ec__IteratorC_t1600278906), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1945[4] = 
{
	U3CGetExtensionMethodsU3Ec__IteratorC_t1600278906::get_offset_of_U3CU24s_49U3E__0_0(),
	U3CGetExtensionMethodsU3Ec__IteratorC_t1600278906::get_offset_of_U3CmemberU3E__1_1(),
	U3CGetExtensionMethodsU3Ec__IteratorC_t1600278906::get_offset_of_U24PC_2(),
	U3CGetExtensionMethodsU3Ec__IteratorC_t1600278906::get_offset_of_U24current_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1946 = { sizeof (U3CCoerceU3Ec__AnonStorey1D_t752649758), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1946[2] = 
{
	U3CCoerceU3Ec__AnonStorey1D_t752649758::get_offset_of_value_0(),
	U3CCoerceU3Ec__AnonStorey1D_t752649758::get_offset_of_toType_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1947 = { sizeof (U3CEmitImplicitConversionDispatcherU3Ec__AnonStorey1E_t187060723), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1947[1] = 
{
	U3CEmitImplicitConversionDispatcherU3Ec__AnonStorey1E_t187060723::get_offset_of_method_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1948 = { sizeof (Dispatcher_t2240407071), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1949 = { sizeof (U3CModuleU3E_t3783534222), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1950 = { sizeof (HtmlNodeType_t1941241835)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1950[5] = 
{
	HtmlNodeType_t1941241835::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1951 = { sizeof (HtmlNodeNavigator_t2752606360), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1951[4] = 
{
	HtmlNodeNavigator_t2752606360::get_offset_of__attindex_2(),
	HtmlNodeNavigator_t2752606360::get_offset_of__currentnode_3(),
	HtmlNodeNavigator_t2752606360::get_offset_of__doc_4(),
	HtmlNodeNavigator_t2752606360::get_offset_of__nametable_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1952 = { sizeof (HtmlNode_t2048434459), -1, sizeof(HtmlNode_t2048434459_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1952[29] = 
{
	HtmlNode_t2048434459::get_offset_of__attributes_0(),
	HtmlNode_t2048434459::get_offset_of__childnodes_1(),
	HtmlNode_t2048434459::get_offset_of__endnode_2(),
	HtmlNode_t2048434459::get_offset_of__innerchanged_3(),
	HtmlNode_t2048434459::get_offset_of__innerhtml_4(),
	HtmlNode_t2048434459::get_offset_of__innerlength_5(),
	HtmlNode_t2048434459::get_offset_of__innerstartindex_6(),
	HtmlNode_t2048434459::get_offset_of__line_7(),
	HtmlNode_t2048434459::get_offset_of__lineposition_8(),
	HtmlNode_t2048434459::get_offset_of__name_9(),
	HtmlNode_t2048434459::get_offset_of__namelength_10(),
	HtmlNode_t2048434459::get_offset_of__namestartindex_11(),
	HtmlNode_t2048434459::get_offset_of__nextnode_12(),
	HtmlNode_t2048434459::get_offset_of__nodetype_13(),
	HtmlNode_t2048434459::get_offset_of__outerchanged_14(),
	HtmlNode_t2048434459::get_offset_of__outerhtml_15(),
	HtmlNode_t2048434459::get_offset_of__outerlength_16(),
	HtmlNode_t2048434459::get_offset_of__outerstartindex_17(),
	HtmlNode_t2048434459::get_offset_of__optimizedName_18(),
	HtmlNode_t2048434459::get_offset_of__ownerdocument_19(),
	HtmlNode_t2048434459::get_offset_of__parentnode_20(),
	HtmlNode_t2048434459::get_offset_of__prevnode_21(),
	HtmlNode_t2048434459::get_offset_of__prevwithsamename_22(),
	HtmlNode_t2048434459::get_offset_of__starttag_23(),
	HtmlNode_t2048434459::get_offset_of__streamposition_24(),
	HtmlNode_t2048434459_StaticFields::get_offset_of_HtmlNodeTypeNameComment_25(),
	HtmlNode_t2048434459_StaticFields::get_offset_of_HtmlNodeTypeNameDocument_26(),
	HtmlNode_t2048434459_StaticFields::get_offset_of_HtmlNodeTypeNameText_27(),
	HtmlNode_t2048434459_StaticFields::get_offset_of_ElementsFlags_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1953 = { sizeof (HtmlTextNode_t2710098554), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1953[1] = 
{
	HtmlTextNode_t2710098554::get_offset_of__text_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1954 = { sizeof (HtmlDocument_t556432108), -1, sizeof(HtmlDocument_t556432108_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1954[41] = 
{
	HtmlDocument_t556432108::get_offset_of__c_0(),
	HtmlDocument_t556432108::get_offset_of__crc32_1(),
	HtmlDocument_t556432108::get_offset_of__currentattribute_2(),
	HtmlDocument_t556432108::get_offset_of__currentnode_3(),
	HtmlDocument_t556432108::get_offset_of__declaredencoding_4(),
	HtmlDocument_t556432108::get_offset_of__documentnode_5(),
	HtmlDocument_t556432108::get_offset_of__fullcomment_6(),
	HtmlDocument_t556432108::get_offset_of__index_7(),
	HtmlDocument_t556432108::get_offset_of_Lastnodes_8(),
	HtmlDocument_t556432108::get_offset_of__lastparentnode_9(),
	HtmlDocument_t556432108::get_offset_of__line_10(),
	HtmlDocument_t556432108::get_offset_of__lineposition_11(),
	HtmlDocument_t556432108::get_offset_of__maxlineposition_12(),
	HtmlDocument_t556432108::get_offset_of_Nodesid_13(),
	HtmlDocument_t556432108::get_offset_of__oldstate_14(),
	HtmlDocument_t556432108::get_offset_of__onlyDetectEncoding_15(),
	HtmlDocument_t556432108::get_offset_of_Openednodes_16(),
	HtmlDocument_t556432108::get_offset_of__parseerrors_17(),
	HtmlDocument_t556432108::get_offset_of__remainder_18(),
	HtmlDocument_t556432108::get_offset_of__remainderOffset_19(),
	HtmlDocument_t556432108::get_offset_of__state_20(),
	HtmlDocument_t556432108::get_offset_of__streamencoding_21(),
	HtmlDocument_t556432108::get_offset_of_Text_22(),
	HtmlDocument_t556432108::get_offset_of_OptionAddDebuggingAttributes_23(),
	HtmlDocument_t556432108::get_offset_of_OptionAutoCloseOnEnd_24(),
	HtmlDocument_t556432108::get_offset_of_OptionCheckSyntax_25(),
	HtmlDocument_t556432108::get_offset_of_OptionComputeChecksum_26(),
	HtmlDocument_t556432108::get_offset_of_OptionDefaultStreamEncoding_27(),
	HtmlDocument_t556432108::get_offset_of_OptionExtractErrorSourceText_28(),
	HtmlDocument_t556432108::get_offset_of_OptionExtractErrorSourceTextMaxLength_29(),
	HtmlDocument_t556432108::get_offset_of_OptionFixNestedTags_30(),
	HtmlDocument_t556432108::get_offset_of_OptionOutputAsXml_31(),
	HtmlDocument_t556432108::get_offset_of_OptionOutputOptimizeAttributeValues_32(),
	HtmlDocument_t556432108::get_offset_of_OptionOutputOriginalCase_33(),
	HtmlDocument_t556432108::get_offset_of_OptionOutputUpperCase_34(),
	HtmlDocument_t556432108::get_offset_of_OptionReadEncoding_35(),
	HtmlDocument_t556432108::get_offset_of_OptionStopperNodeName_36(),
	HtmlDocument_t556432108::get_offset_of_OptionUseIdAttribute_37(),
	HtmlDocument_t556432108::get_offset_of_OptionWriteEmptyNodes_38(),
	HtmlDocument_t556432108_StaticFields::get_offset_of_HtmlExceptionRefNotChild_39(),
	HtmlDocument_t556432108_StaticFields::get_offset_of_HtmlExceptionUseIdAttributeFalse_40(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1955 = { sizeof (ParseState_t1851939866)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1955[14] = 
{
	ParseState_t1851939866::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1956 = { sizeof (HtmlAttribute_t1804523403), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1956[12] = 
{
	HtmlAttribute_t1804523403::get_offset_of__line_0(),
	HtmlAttribute_t1804523403::get_offset_of__lineposition_1(),
	HtmlAttribute_t1804523403::get_offset_of__name_2(),
	HtmlAttribute_t1804523403::get_offset_of__namelength_3(),
	HtmlAttribute_t1804523403::get_offset_of__namestartindex_4(),
	HtmlAttribute_t1804523403::get_offset_of__ownerdocument_5(),
	HtmlAttribute_t1804523403::get_offset_of__ownernode_6(),
	HtmlAttribute_t1804523403::get_offset_of__quoteType_7(),
	HtmlAttribute_t1804523403::get_offset_of__streamposition_8(),
	HtmlAttribute_t1804523403::get_offset_of__value_9(),
	HtmlAttribute_t1804523403::get_offset_of__valuelength_10(),
	HtmlAttribute_t1804523403::get_offset_of__valuestartindex_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1957 = { sizeof (AttributeValueQuote_t3949719843)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1957[3] = 
{
	AttributeValueQuote_t3949719843::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1958 = { sizeof (HtmlNodeCollection_t2542734491), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1958[2] = 
{
	HtmlNodeCollection_t2542734491::get_offset_of__parentnode_0(),
	HtmlNodeCollection_t2542734491::get_offset_of__items_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1959 = { sizeof (HtmlNameTable_t3848610378), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1959[1] = 
{
	HtmlNameTable_t3848610378::get_offset_of__nametable_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1960 = { sizeof (HtmlCommentNode_t1992371332), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1960[1] = 
{
	HtmlCommentNode_t1992371332::get_offset_of__comment_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1961 = { sizeof (EncodingFoundException_t1471015948), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1961[1] = 
{
	EncodingFoundException_t1471015948::get_offset_of__encoding_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1962 = { sizeof (NameValuePairList_t1351137418), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1962[3] = 
{
	NameValuePairList_t1351137418::get_offset_of_Text_0(),
	NameValuePairList_t1351137418::get_offset_of__allPairs_1(),
	NameValuePairList_t1351137418::get_offset_of__pairsWithName_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1963 = { sizeof (HtmlElementFlag_t260274357)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1963[5] = 
{
	HtmlElementFlag_t260274357::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1964 = { sizeof (HtmlParseErrorCode_t3491517481)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1964[6] = 
{
	HtmlParseErrorCode_t3491517481::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1965 = { sizeof (Crc32_t83196531), -1, sizeof(Crc32_t83196531_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1965[2] = 
{
	Crc32_t83196531::get_offset_of__crc32_0(),
	Crc32_t83196531_StaticFields::get_offset_of_crc_32_tab_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1966 = { sizeof (HtmlAttributeCollection_t1787476631), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1966[3] = 
{
	HtmlAttributeCollection_t1787476631::get_offset_of_Hashitems_0(),
	HtmlAttributeCollection_t1787476631::get_offset_of__ownernode_1(),
	HtmlAttributeCollection_t1787476631::get_offset_of_items_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1967 = { sizeof (Utilities_t3593287176), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1968 = { sizeof (HtmlParseError_t1115179162), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1968[6] = 
{
	HtmlParseError_t1115179162::get_offset_of__code_0(),
	HtmlParseError_t1115179162::get_offset_of__line_1(),
	HtmlParseError_t1115179162::get_offset_of__linePosition_2(),
	HtmlParseError_t1115179162::get_offset_of__reason_3(),
	HtmlParseError_t1115179162::get_offset_of__sourceText_4(),
	HtmlParseError_t1115179162::get_offset_of__streamPosition_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1969 = { sizeof (U3CPrivateImplementationDetailsU3EU7B83185D3BU2D3939U2D439CU2DA54FU2D260F9279D9C8U7D_t691081255), -1, sizeof(U3CPrivateImplementationDetailsU3EU7B83185D3BU2D3939U2D439CU2DA54FU2D260F9279D9C8U7D_t691081255_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1969[2] = 
{
	U3CPrivateImplementationDetailsU3EU7B83185D3BU2D3939U2D439CU2DA54FU2D260F9279D9C8U7D_t691081255_StaticFields::get_offset_of_U24U24method0x600012fU2D1_0(),
	U3CPrivateImplementationDetailsU3EU7B83185D3BU2D3939U2D439CU2DA54FU2D260F9279D9C8U7D_t691081255_StaticFields::get_offset_of_U24U24method0x6000287U2D1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1970 = { sizeof (__StaticArrayInitTypeSizeU3D1024_t2165070692)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D1024_t2165070692 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1971 = { sizeof (U3CModuleU3E_t3783534223), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1972 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1973 = { sizeof (BlobProcessor_t3603341577), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1973[8] = 
{
	BlobProcessor_t3603341577::get_offset_of_BlobAdded_0(),
	BlobProcessor_t3603341577::get_offset_of_BlobUpdated_1(),
	BlobProcessor_t3603341577::get_offset_of_BlobRemoved_2(),
	BlobProcessor_t3603341577::get_offset_of_blobs_3(),
	BlobProcessor_t3603341577::get_offset_of_updatedBlobs_4(),
	BlobProcessor_t3603341577::get_offset_of_addedBlobs_5(),
	BlobProcessor_t3603341577::get_offset_of_removedBlobs_6(),
	BlobProcessor_t3603341577::get_offset_of_U3CFrameNumberU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1974 = { sizeof (TuioBlobEventArgs_t3562978179), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1974[1] = 
{
	TuioBlobEventArgs_t3562978179::get_offset_of_Blob_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1975 = { sizeof (CursorProcessor_t1785954004), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1975[8] = 
{
	CursorProcessor_t1785954004::get_offset_of_CursorAdded_0(),
	CursorProcessor_t1785954004::get_offset_of_CursorUpdated_1(),
	CursorProcessor_t1785954004::get_offset_of_CursorRemoved_2(),
	CursorProcessor_t1785954004::get_offset_of_cursors_3(),
	CursorProcessor_t1785954004::get_offset_of_updatedCursors_4(),
	CursorProcessor_t1785954004::get_offset_of_addedCursors_5(),
	CursorProcessor_t1785954004::get_offset_of_removedCursors_6(),
	CursorProcessor_t1785954004::get_offset_of_U3CFrameNumberU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1976 = { sizeof (TuioCursorEventArgs_t362990012), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1976[1] = 
{
	TuioCursorEventArgs_t362990012::get_offset_of_Cursor_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1977 = { sizeof (ObjectProcessor_t221569383), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1977[8] = 
{
	ObjectProcessor_t221569383::get_offset_of_ObjectAdded_0(),
	ObjectProcessor_t221569383::get_offset_of_ObjectUpdated_1(),
	ObjectProcessor_t221569383::get_offset_of_ObjectRemoved_2(),
	ObjectProcessor_t221569383::get_offset_of_objects_3(),
	ObjectProcessor_t221569383::get_offset_of_updatedObjects_4(),
	ObjectProcessor_t221569383::get_offset_of_addedObjects_5(),
	ObjectProcessor_t221569383::get_offset_of_removedObjects_6(),
	ObjectProcessor_t221569383::get_offset_of_U3CFrameNumberU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1978 = { sizeof (TuioObjectEventArgs_t1810743341), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1978[1] = 
{
	TuioObjectEventArgs_t1810743341::get_offset_of_Object_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1979 = { sizeof (TuioEntity_t295276718), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1979[6] = 
{
	TuioEntity_t295276718::get_offset_of_U3CIdU3Ek__BackingField_0(),
	TuioEntity_t295276718::get_offset_of_U3CXU3Ek__BackingField_1(),
	TuioEntity_t295276718::get_offset_of_U3CYU3Ek__BackingField_2(),
	TuioEntity_t295276718::get_offset_of_U3CVelocityXU3Ek__BackingField_3(),
	TuioEntity_t295276718::get_offset_of_U3CVelocityYU3Ek__BackingField_4(),
	TuioEntity_t295276718::get_offset_of_U3CAccelerationU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1980 = { sizeof (TuioBlob_t2046943414), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1980[6] = 
{
	TuioBlob_t2046943414::get_offset_of_U3CAngleU3Ek__BackingField_6(),
	TuioBlob_t2046943414::get_offset_of_U3CWidthU3Ek__BackingField_7(),
	TuioBlob_t2046943414::get_offset_of_U3CHeightU3Ek__BackingField_8(),
	TuioBlob_t2046943414::get_offset_of_U3CAreaU3Ek__BackingField_9(),
	TuioBlob_t2046943414::get_offset_of_U3CRotationVelocityU3Ek__BackingField_10(),
	TuioBlob_t2046943414::get_offset_of_U3CRotationAccelerationU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1981 = { sizeof (TuioObject_t1236821014), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1981[4] = 
{
	TuioObject_t1236821014::get_offset_of_U3CClassIdU3Ek__BackingField_6(),
	TuioObject_t1236821014::get_offset_of_U3CAngleU3Ek__BackingField_7(),
	TuioObject_t1236821014::get_offset_of_U3CRotationVelocityU3Ek__BackingField_8(),
	TuioObject_t1236821014::get_offset_of_U3CRotationAccelerationU3Ek__BackingField_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1982 = { sizeof (TuioCursor_t1850351419), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1983 = { sizeof (TuioServer_t595520884), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1983[4] = 
{
	TuioServer_t595520884::get_offset_of_ErrorOccured_0(),
	TuioServer_t595520884::get_offset_of_udpReceiver_1(),
	TuioServer_t595520884::get_offset_of_processors_2(),
	TuioServer_t595520884::get_offset_of_U3CPortU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1984 = { sizeof (U3CModuleU3E_t3783534224), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1985 = { sizeof (EventHandle_t942672932)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1985[3] = 
{
	EventHandle_t942672932::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1986 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1987 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1988 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1989 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1990 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1991 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1992 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1993 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1994 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1995 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1996 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1997 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1998 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1999 = { 0, -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
