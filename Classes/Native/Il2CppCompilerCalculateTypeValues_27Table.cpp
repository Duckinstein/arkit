﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.String
struct String_t;
// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Word[]
struct WordU5BU5D_t2800973631;
// System.String[]
struct StringU5BU5D_t1642385972;
// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText
struct SpeechToText_t2713896346;
// IBM.Watson.DeveloperCloud.Services.ServiceStatus
struct ServiceStatus_t1443707987;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Collections.Generic.Dictionary`2<System.String,IBM.Watson.DeveloperCloud.Connection.RESTConnector/Form>
struct Dictionary_2_t2694055125;
// IBM.Watson.DeveloperCloud.Connection.RESTConnector/ResponseEvent
struct ResponseEvent_t1616568356;
// IBM.Watson.DeveloperCloud.Connection.RESTConnector/ProgressEvent
struct ProgressEvent_t4185145044;
// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordData[]
struct WordDataU5BU5D_t1726140911;
// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Corpus[]
struct CorpusU5BU5D_t2250486967;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Customization[]
struct CustomizationU5BU5D_t626929480;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Voice[]
struct VoiceU5BU5D_t260285181;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Word[]
struct WordU5BU5D_t3686373631;
// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Customization[]
struct CustomizationU5BU5D_t543472456;
// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechRecognitionResult[]
struct SpeechRecognitionResultU5BU5D_t1533902383;
// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechRecognitionAlternative[]
struct SpeechRecognitionAlternativeU5BU5D_t769752751;
// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.KeywordResults
struct KeywordResults_t4114097975;
// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordAlternativeResults
struct WordAlternativeResults_t1270965811;
// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.KeywordResult[]
struct KeywordResultU5BU5D_t1719463693;
// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.TimeStamp[]
struct TimeStampU5BU5D_t2852280099;
// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordConfidence[]
struct WordConfidenceU5BU5D_t3370323303;
// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank
struct RetrieveAndRank_t1381045327;
// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SupportedFeatures
struct SupportedFeatures_t1255602913;
// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Model[]
struct ModelU5BU5D_t545738874;
// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordAlternativeResult[]
struct WordAlternativeResultU5BU5D_t598168141;
// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.JobStatus[]
struct JobStatusU5BU5D_t38003402;
// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SessionStatus
struct SessionStatus_t3800674532;
// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/GetCustomWordCallback
struct GetCustomWordCallback_t4225912831;
// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/OnDeleteCustomCorpusCallback
struct OnDeleteCustomCorpusCallback_t3393304107;
// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/OnAddCustomCorpusCallback
struct OnAddCustomCorpusCallback_t1567930907;
// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/OnDeleteCustomWordCallback
struct OnDeleteCustomWordCallback_t2228320037;
// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/AddCustomWordsCallback
struct AddCustomWordsCallback_t4067999203;
// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/GetCustomWordsCallback
struct GetCustomWordsCallback_t3048138280;
// System.Char[]
struct CharU5BU5D_t1328083999;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetVoiceCallback
struct GetVoiceCallback_t2752546046;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetVoicesCallback
struct GetVoicesCallback_t3012885511;
// System.Void
struct Void_t1841601450;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/ToSpeechCallback
struct ToSpeechCallback_t3422748631;
// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/OnDeleteCustomizationCallback
struct OnDeleteCustomizationCallback_t1940302465;
// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/ResetCustomizationCallback
struct ResetCustomizationCallback_t1922530872;
// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/CreateCustomizationCallback
struct CreateCustomizationCallback_t3694377825;
// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.CustomLanguage
struct CustomLanguage_t1423423885;
// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/OnDeleteRanker
struct OnDeleteRanker_t2889964448;
// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/GetCustomizationCallback
struct GetCustomizationCallback_t267872143;
// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/OnGetRanker
struct OnGetRanker_t299592683;
// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/TrainCustomizationCallback
struct TrainCustomizationCallback_t2705176209;
// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/GetCustomizationsCallback
struct GetCustomizationsCallback_t2433078120;
// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/OnGetModels
struct OnGetModels_t1162909112;
// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/GetCustomCorporaCallback
struct GetCustomCorporaCallback_t1452492251;
// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/OnCreateRanker
struct OnCreateRanker_t496698577;
// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/OnGetModel
struct OnGetModel_t3671144581;
// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/OnRank
struct OnRank_t4002112472;
// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/UpgradeCustomizationCallback
struct UpgradeCustomizationCallback_t567321637;
// UnityEngine.AudioClip
struct AudioClip_t1932558630;
// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/OnRecognize
struct OnRecognize_t3538205802;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1572802995;
// IBM.Watson.DeveloperCloud.Utilities.DataCache
struct DataCache_t4250340070;
// System.Collections.Generic.Dictionary`2<IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.VoiceType,System.String>
struct Dictionary_2_t1569883280;
// System.Collections.Generic.Dictionary`2<IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.AudioFormatType,System.String>
struct Dictionary_2_t2576372715;
// FullSerializer.fsSerializer
struct fsSerializer_t4193731081;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Voice
struct Voice_t3646862260;
// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RankerStatusPayload
struct RankerStatusPayload_t2644736411;
// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/LoadFileDelegate
struct LoadFileDelegate_t351561590;
// IBM.Watson.DeveloperCloud.Connection.WSConnector
struct WSConnector_t2552241039;
// System.Collections.Generic.Queue`1<IBM.Watson.DeveloperCloud.DataTypes.AudioData>
struct Queue_1_t3714055359;
// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/ErrorEvent
struct ErrorEvent_t3049611749;
// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RankerOutputPayload
struct RankerOutputPayload_t247035662;
// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordData
struct WordData_t612265514;
// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Corpora
struct Corpora_t3715340602;
// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordsList
struct WordsList_t29283159;
// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechRecognitionEvent
struct SpeechRecognitionEvent_t1591882439;
// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Customizations
struct Customizations_t4082930968;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Voices
struct Voices_t4221445733;
// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Model
struct Model_t2340542523;
// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Customization
struct Customization_t47167237;
// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.CustomizationID
struct CustomizationID_t1324928090;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef WORDTYPE_T1970187882_H
#define WORDTYPE_T1970187882_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordType
struct  WordType_t1970187882  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORDTYPE_T1970187882_H
#ifndef WORDS_T4062187109_H
#define WORDS_T4062187109_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Words
struct  Words_t4062187109  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Word[] IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Words::<words>k__BackingField
	WordU5BU5D_t2800973631* ___U3CwordsU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CwordsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Words_t4062187109, ___U3CwordsU3Ek__BackingField_0)); }
	inline WordU5BU5D_t2800973631* get_U3CwordsU3Ek__BackingField_0() const { return ___U3CwordsU3Ek__BackingField_0; }
	inline WordU5BU5D_t2800973631** get_address_of_U3CwordsU3Ek__BackingField_0() { return &___U3CwordsU3Ek__BackingField_0; }
	inline void set_U3CwordsU3Ek__BackingField_0(WordU5BU5D_t2800973631* value)
	{
		___U3CwordsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwordsU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORDS_T4062187109_H
#ifndef WORD_T3204175642_H
#define WORD_T3204175642_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Word
struct  Word_t3204175642  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Word::<word>k__BackingField
	String_t* ___U3CwordU3Ek__BackingField_0;
	// System.String[] IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Word::<sounds_like>k__BackingField
	StringU5BU5D_t1642385972* ___U3Csounds_likeU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Word::<display_as>k__BackingField
	String_t* ___U3Cdisplay_asU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CwordU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Word_t3204175642, ___U3CwordU3Ek__BackingField_0)); }
	inline String_t* get_U3CwordU3Ek__BackingField_0() const { return ___U3CwordU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CwordU3Ek__BackingField_0() { return &___U3CwordU3Ek__BackingField_0; }
	inline void set_U3CwordU3Ek__BackingField_0(String_t* value)
	{
		___U3CwordU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwordU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3Csounds_likeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Word_t3204175642, ___U3Csounds_likeU3Ek__BackingField_1)); }
	inline StringU5BU5D_t1642385972* get_U3Csounds_likeU3Ek__BackingField_1() const { return ___U3Csounds_likeU3Ek__BackingField_1; }
	inline StringU5BU5D_t1642385972** get_address_of_U3Csounds_likeU3Ek__BackingField_1() { return &___U3Csounds_likeU3Ek__BackingField_1; }
	inline void set_U3Csounds_likeU3Ek__BackingField_1(StringU5BU5D_t1642385972* value)
	{
		___U3Csounds_likeU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3Csounds_likeU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3Cdisplay_asU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Word_t3204175642, ___U3Cdisplay_asU3Ek__BackingField_2)); }
	inline String_t* get_U3Cdisplay_asU3Ek__BackingField_2() const { return ___U3Cdisplay_asU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3Cdisplay_asU3Ek__BackingField_2() { return &___U3Cdisplay_asU3Ek__BackingField_2; }
	inline void set_U3Cdisplay_asU3Ek__BackingField_2(String_t* value)
	{
		___U3Cdisplay_asU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cdisplay_asU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORD_T3204175642_H
#ifndef U3CKEEPALIVEU3EC__ITERATOR0_T1986357758_H
#define U3CKEEPALIVEU3EC__ITERATOR0_T1986357758_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/<KeepAlive>c__Iterator0
struct  U3CKeepAliveU3Ec__Iterator0_t1986357758  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/<KeepAlive>c__Iterator0::$this
	SpeechToText_t2713896346 * ___U24this_0;
	// System.Object IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/<KeepAlive>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/<KeepAlive>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/<KeepAlive>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CKeepAliveU3Ec__Iterator0_t1986357758, ___U24this_0)); }
	inline SpeechToText_t2713896346 * get_U24this_0() const { return ___U24this_0; }
	inline SpeechToText_t2713896346 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(SpeechToText_t2713896346 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CKeepAliveU3Ec__Iterator0_t1986357758, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CKeepAliveU3Ec__Iterator0_t1986357758, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CKeepAliveU3Ec__Iterator0_t1986357758, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CKEEPALIVEU3EC__ITERATOR0_T1986357758_H
#ifndef CHECKSERVICESTATUS_T790185058_H
#define CHECKSERVICESTATUS_T790185058_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/CheckServiceStatus
struct  CheckServiceStatus_t790185058  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/CheckServiceStatus::m_Service
	SpeechToText_t2713896346 * ___m_Service_0;
	// IBM.Watson.DeveloperCloud.Services.ServiceStatus IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/CheckServiceStatus::m_Callback
	ServiceStatus_t1443707987 * ___m_Callback_1;

public:
	inline static int32_t get_offset_of_m_Service_0() { return static_cast<int32_t>(offsetof(CheckServiceStatus_t790185058, ___m_Service_0)); }
	inline SpeechToText_t2713896346 * get_m_Service_0() const { return ___m_Service_0; }
	inline SpeechToText_t2713896346 ** get_address_of_m_Service_0() { return &___m_Service_0; }
	inline void set_m_Service_0(SpeechToText_t2713896346 * value)
	{
		___m_Service_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Service_0), value);
	}

	inline static int32_t get_offset_of_m_Callback_1() { return static_cast<int32_t>(offsetof(CheckServiceStatus_t790185058, ___m_Callback_1)); }
	inline ServiceStatus_t1443707987 * get_m_Callback_1() const { return ___m_Callback_1; }
	inline ServiceStatus_t1443707987 ** get_address_of_m_Callback_1() { return &___m_Callback_1; }
	inline void set_m_Callback_1(ServiceStatus_t1443707987 * value)
	{
		___m_Callback_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Callback_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHECKSERVICESTATUS_T790185058_H
#ifndef REQUEST_T466816980_H
#define REQUEST_T466816980_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request
struct  Request_t466816980  : public RuntimeObject
{
public:
	// System.Single IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Timeout>k__BackingField
	float ___U3CTimeoutU3Ek__BackingField_0;
	// System.Boolean IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Cancel>k__BackingField
	bool ___U3CCancelU3Ek__BackingField_1;
	// System.Boolean IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Delete>k__BackingField
	bool ___U3CDeleteU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Function>k__BackingField
	String_t* ___U3CFunctionU3Ek__BackingField_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Parameters>k__BackingField
	Dictionary_2_t309261261 * ___U3CParametersU3Ek__BackingField_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Headers>k__BackingField
	Dictionary_2_t3943999495 * ___U3CHeadersU3Ek__BackingField_5;
	// System.Byte[] IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Send>k__BackingField
	ByteU5BU5D_t3397334013* ___U3CSendU3Ek__BackingField_6;
	// System.Collections.Generic.Dictionary`2<System.String,IBM.Watson.DeveloperCloud.Connection.RESTConnector/Form> IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Forms>k__BackingField
	Dictionary_2_t2694055125 * ___U3CFormsU3Ek__BackingField_7;
	// IBM.Watson.DeveloperCloud.Connection.RESTConnector/ResponseEvent IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<OnResponse>k__BackingField
	ResponseEvent_t1616568356 * ___U3COnResponseU3Ek__BackingField_8;
	// IBM.Watson.DeveloperCloud.Connection.RESTConnector/ProgressEvent IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<OnDownloadProgress>k__BackingField
	ProgressEvent_t4185145044 * ___U3COnDownloadProgressU3Ek__BackingField_9;
	// IBM.Watson.DeveloperCloud.Connection.RESTConnector/ProgressEvent IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<OnUploadProgress>k__BackingField
	ProgressEvent_t4185145044 * ___U3COnUploadProgressU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of_U3CTimeoutU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CTimeoutU3Ek__BackingField_0)); }
	inline float get_U3CTimeoutU3Ek__BackingField_0() const { return ___U3CTimeoutU3Ek__BackingField_0; }
	inline float* get_address_of_U3CTimeoutU3Ek__BackingField_0() { return &___U3CTimeoutU3Ek__BackingField_0; }
	inline void set_U3CTimeoutU3Ek__BackingField_0(float value)
	{
		___U3CTimeoutU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CCancelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CCancelU3Ek__BackingField_1)); }
	inline bool get_U3CCancelU3Ek__BackingField_1() const { return ___U3CCancelU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CCancelU3Ek__BackingField_1() { return &___U3CCancelU3Ek__BackingField_1; }
	inline void set_U3CCancelU3Ek__BackingField_1(bool value)
	{
		___U3CCancelU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CDeleteU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CDeleteU3Ek__BackingField_2)); }
	inline bool get_U3CDeleteU3Ek__BackingField_2() const { return ___U3CDeleteU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CDeleteU3Ek__BackingField_2() { return &___U3CDeleteU3Ek__BackingField_2; }
	inline void set_U3CDeleteU3Ek__BackingField_2(bool value)
	{
		___U3CDeleteU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CFunctionU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CFunctionU3Ek__BackingField_3)); }
	inline String_t* get_U3CFunctionU3Ek__BackingField_3() const { return ___U3CFunctionU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CFunctionU3Ek__BackingField_3() { return &___U3CFunctionU3Ek__BackingField_3; }
	inline void set_U3CFunctionU3Ek__BackingField_3(String_t* value)
	{
		___U3CFunctionU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFunctionU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CParametersU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CParametersU3Ek__BackingField_4)); }
	inline Dictionary_2_t309261261 * get_U3CParametersU3Ek__BackingField_4() const { return ___U3CParametersU3Ek__BackingField_4; }
	inline Dictionary_2_t309261261 ** get_address_of_U3CParametersU3Ek__BackingField_4() { return &___U3CParametersU3Ek__BackingField_4; }
	inline void set_U3CParametersU3Ek__BackingField_4(Dictionary_2_t309261261 * value)
	{
		___U3CParametersU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CParametersU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CHeadersU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CHeadersU3Ek__BackingField_5)); }
	inline Dictionary_2_t3943999495 * get_U3CHeadersU3Ek__BackingField_5() const { return ___U3CHeadersU3Ek__BackingField_5; }
	inline Dictionary_2_t3943999495 ** get_address_of_U3CHeadersU3Ek__BackingField_5() { return &___U3CHeadersU3Ek__BackingField_5; }
	inline void set_U3CHeadersU3Ek__BackingField_5(Dictionary_2_t3943999495 * value)
	{
		___U3CHeadersU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CHeadersU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CSendU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CSendU3Ek__BackingField_6)); }
	inline ByteU5BU5D_t3397334013* get_U3CSendU3Ek__BackingField_6() const { return ___U3CSendU3Ek__BackingField_6; }
	inline ByteU5BU5D_t3397334013** get_address_of_U3CSendU3Ek__BackingField_6() { return &___U3CSendU3Ek__BackingField_6; }
	inline void set_U3CSendU3Ek__BackingField_6(ByteU5BU5D_t3397334013* value)
	{
		___U3CSendU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSendU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CFormsU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CFormsU3Ek__BackingField_7)); }
	inline Dictionary_2_t2694055125 * get_U3CFormsU3Ek__BackingField_7() const { return ___U3CFormsU3Ek__BackingField_7; }
	inline Dictionary_2_t2694055125 ** get_address_of_U3CFormsU3Ek__BackingField_7() { return &___U3CFormsU3Ek__BackingField_7; }
	inline void set_U3CFormsU3Ek__BackingField_7(Dictionary_2_t2694055125 * value)
	{
		___U3CFormsU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFormsU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3COnResponseU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3COnResponseU3Ek__BackingField_8)); }
	inline ResponseEvent_t1616568356 * get_U3COnResponseU3Ek__BackingField_8() const { return ___U3COnResponseU3Ek__BackingField_8; }
	inline ResponseEvent_t1616568356 ** get_address_of_U3COnResponseU3Ek__BackingField_8() { return &___U3COnResponseU3Ek__BackingField_8; }
	inline void set_U3COnResponseU3Ek__BackingField_8(ResponseEvent_t1616568356 * value)
	{
		___U3COnResponseU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3COnResponseU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3COnDownloadProgressU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3COnDownloadProgressU3Ek__BackingField_9)); }
	inline ProgressEvent_t4185145044 * get_U3COnDownloadProgressU3Ek__BackingField_9() const { return ___U3COnDownloadProgressU3Ek__BackingField_9; }
	inline ProgressEvent_t4185145044 ** get_address_of_U3COnDownloadProgressU3Ek__BackingField_9() { return &___U3COnDownloadProgressU3Ek__BackingField_9; }
	inline void set_U3COnDownloadProgressU3Ek__BackingField_9(ProgressEvent_t4185145044 * value)
	{
		___U3COnDownloadProgressU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3COnDownloadProgressU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3COnUploadProgressU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3COnUploadProgressU3Ek__BackingField_10)); }
	inline ProgressEvent_t4185145044 * get_U3COnUploadProgressU3Ek__BackingField_10() const { return ___U3COnUploadProgressU3Ek__BackingField_10; }
	inline ProgressEvent_t4185145044 ** get_address_of_U3COnUploadProgressU3Ek__BackingField_10() { return &___U3COnUploadProgressU3Ek__BackingField_10; }
	inline void set_U3COnUploadProgressU3Ek__BackingField_10(ProgressEvent_t4185145044 * value)
	{
		___U3COnUploadProgressU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3COnUploadProgressU3Ek__BackingField_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUEST_T466816980_H
#ifndef WORDDATA_T612265514_H
#define WORDDATA_T612265514_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordData
struct  WordData_t612265514  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordData::<word>k__BackingField
	String_t* ___U3CwordU3Ek__BackingField_0;
	// System.String[] IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordData::<sounds_like>k__BackingField
	StringU5BU5D_t1642385972* ___U3Csounds_likeU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordData::<display_as>k__BackingField
	String_t* ___U3Cdisplay_asU3Ek__BackingField_2;
	// System.String[] IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordData::<source>k__BackingField
	StringU5BU5D_t1642385972* ___U3CsourceU3Ek__BackingField_3;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordData::<error>k__BackingField
	String_t* ___U3CerrorU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CwordU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(WordData_t612265514, ___U3CwordU3Ek__BackingField_0)); }
	inline String_t* get_U3CwordU3Ek__BackingField_0() const { return ___U3CwordU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CwordU3Ek__BackingField_0() { return &___U3CwordU3Ek__BackingField_0; }
	inline void set_U3CwordU3Ek__BackingField_0(String_t* value)
	{
		___U3CwordU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwordU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3Csounds_likeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(WordData_t612265514, ___U3Csounds_likeU3Ek__BackingField_1)); }
	inline StringU5BU5D_t1642385972* get_U3Csounds_likeU3Ek__BackingField_1() const { return ___U3Csounds_likeU3Ek__BackingField_1; }
	inline StringU5BU5D_t1642385972** get_address_of_U3Csounds_likeU3Ek__BackingField_1() { return &___U3Csounds_likeU3Ek__BackingField_1; }
	inline void set_U3Csounds_likeU3Ek__BackingField_1(StringU5BU5D_t1642385972* value)
	{
		___U3Csounds_likeU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3Csounds_likeU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3Cdisplay_asU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(WordData_t612265514, ___U3Cdisplay_asU3Ek__BackingField_2)); }
	inline String_t* get_U3Cdisplay_asU3Ek__BackingField_2() const { return ___U3Cdisplay_asU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3Cdisplay_asU3Ek__BackingField_2() { return &___U3Cdisplay_asU3Ek__BackingField_2; }
	inline void set_U3Cdisplay_asU3Ek__BackingField_2(String_t* value)
	{
		___U3Cdisplay_asU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cdisplay_asU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CsourceU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(WordData_t612265514, ___U3CsourceU3Ek__BackingField_3)); }
	inline StringU5BU5D_t1642385972* get_U3CsourceU3Ek__BackingField_3() const { return ___U3CsourceU3Ek__BackingField_3; }
	inline StringU5BU5D_t1642385972** get_address_of_U3CsourceU3Ek__BackingField_3() { return &___U3CsourceU3Ek__BackingField_3; }
	inline void set_U3CsourceU3Ek__BackingField_3(StringU5BU5D_t1642385972* value)
	{
		___U3CsourceU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsourceU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CerrorU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(WordData_t612265514, ___U3CerrorU3Ek__BackingField_4)); }
	inline String_t* get_U3CerrorU3Ek__BackingField_4() const { return ___U3CerrorU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CerrorU3Ek__BackingField_4() { return &___U3CerrorU3Ek__BackingField_4; }
	inline void set_U3CerrorU3Ek__BackingField_4(String_t* value)
	{
		___U3CerrorU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CerrorU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORDDATA_T612265514_H
#ifndef CUSTOMLANGUAGE_T1423423885_H
#define CUSTOMLANGUAGE_T1423423885_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.CustomLanguage
struct  CustomLanguage_t1423423885  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.CustomLanguage::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.CustomLanguage::<base_model_name>k__BackingField
	String_t* ___U3Cbase_model_nameU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.CustomLanguage::<description>k__BackingField
	String_t* ___U3CdescriptionU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CustomLanguage_t1423423885, ___U3CnameU3Ek__BackingField_0)); }
	inline String_t* get_U3CnameU3Ek__BackingField_0() const { return ___U3CnameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_0() { return &___U3CnameU3Ek__BackingField_0; }
	inline void set_U3CnameU3Ek__BackingField_0(String_t* value)
	{
		___U3CnameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3Cbase_model_nameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CustomLanguage_t1423423885, ___U3Cbase_model_nameU3Ek__BackingField_1)); }
	inline String_t* get_U3Cbase_model_nameU3Ek__BackingField_1() const { return ___U3Cbase_model_nameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3Cbase_model_nameU3Ek__BackingField_1() { return &___U3Cbase_model_nameU3Ek__BackingField_1; }
	inline void set_U3Cbase_model_nameU3Ek__BackingField_1(String_t* value)
	{
		___U3Cbase_model_nameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cbase_model_nameU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CdescriptionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CustomLanguage_t1423423885, ___U3CdescriptionU3Ek__BackingField_2)); }
	inline String_t* get_U3CdescriptionU3Ek__BackingField_2() const { return ___U3CdescriptionU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CdescriptionU3Ek__BackingField_2() { return &___U3CdescriptionU3Ek__BackingField_2; }
	inline void set_U3CdescriptionU3Ek__BackingField_2(String_t* value)
	{
		___U3CdescriptionU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdescriptionU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMLANGUAGE_T1423423885_H
#ifndef CUSTOMIZATIONID_T1324928090_H
#define CUSTOMIZATIONID_T1324928090_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.CustomizationID
struct  CustomizationID_t1324928090  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.CustomizationID::<customization_id>k__BackingField
	String_t* ___U3Ccustomization_idU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3Ccustomization_idU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CustomizationID_t1324928090, ___U3Ccustomization_idU3Ek__BackingField_0)); }
	inline String_t* get_U3Ccustomization_idU3Ek__BackingField_0() const { return ___U3Ccustomization_idU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3Ccustomization_idU3Ek__BackingField_0() { return &___U3Ccustomization_idU3Ek__BackingField_0; }
	inline void set_U3Ccustomization_idU3Ek__BackingField_0(String_t* value)
	{
		___U3Ccustomization_idU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Ccustomization_idU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMIZATIONID_T1324928090_H
#ifndef CUSTOMIZATION_T47167237_H
#define CUSTOMIZATION_T47167237_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Customization
struct  Customization_t47167237  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Customization::<customization_id>k__BackingField
	String_t* ___U3Ccustomization_idU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Customization::<created>k__BackingField
	String_t* ___U3CcreatedU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Customization::<language>k__BackingField
	String_t* ___U3ClanguageU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Customization::<owner>k__BackingField
	String_t* ___U3CownerU3Ek__BackingField_3;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Customization::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_4;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Customization::<description>k__BackingField
	String_t* ___U3CdescriptionU3Ek__BackingField_5;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Customization::<base_model_name>k__BackingField
	String_t* ___U3Cbase_model_nameU3Ek__BackingField_6;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Customization::<status>k__BackingField
	String_t* ___U3CstatusU3Ek__BackingField_7;
	// System.Int32 IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Customization::<progress>k__BackingField
	int32_t ___U3CprogressU3Ek__BackingField_8;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Customization::<warnings>k__BackingField
	String_t* ___U3CwarningsU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of_U3Ccustomization_idU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Customization_t47167237, ___U3Ccustomization_idU3Ek__BackingField_0)); }
	inline String_t* get_U3Ccustomization_idU3Ek__BackingField_0() const { return ___U3Ccustomization_idU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3Ccustomization_idU3Ek__BackingField_0() { return &___U3Ccustomization_idU3Ek__BackingField_0; }
	inline void set_U3Ccustomization_idU3Ek__BackingField_0(String_t* value)
	{
		___U3Ccustomization_idU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Ccustomization_idU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CcreatedU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Customization_t47167237, ___U3CcreatedU3Ek__BackingField_1)); }
	inline String_t* get_U3CcreatedU3Ek__BackingField_1() const { return ___U3CcreatedU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CcreatedU3Ek__BackingField_1() { return &___U3CcreatedU3Ek__BackingField_1; }
	inline void set_U3CcreatedU3Ek__BackingField_1(String_t* value)
	{
		___U3CcreatedU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcreatedU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3ClanguageU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Customization_t47167237, ___U3ClanguageU3Ek__BackingField_2)); }
	inline String_t* get_U3ClanguageU3Ek__BackingField_2() const { return ___U3ClanguageU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3ClanguageU3Ek__BackingField_2() { return &___U3ClanguageU3Ek__BackingField_2; }
	inline void set_U3ClanguageU3Ek__BackingField_2(String_t* value)
	{
		___U3ClanguageU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClanguageU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CownerU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Customization_t47167237, ___U3CownerU3Ek__BackingField_3)); }
	inline String_t* get_U3CownerU3Ek__BackingField_3() const { return ___U3CownerU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CownerU3Ek__BackingField_3() { return &___U3CownerU3Ek__BackingField_3; }
	inline void set_U3CownerU3Ek__BackingField_3(String_t* value)
	{
		___U3CownerU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CownerU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Customization_t47167237, ___U3CnameU3Ek__BackingField_4)); }
	inline String_t* get_U3CnameU3Ek__BackingField_4() const { return ___U3CnameU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_4() { return &___U3CnameU3Ek__BackingField_4; }
	inline void set_U3CnameU3Ek__BackingField_4(String_t* value)
	{
		___U3CnameU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CdescriptionU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Customization_t47167237, ___U3CdescriptionU3Ek__BackingField_5)); }
	inline String_t* get_U3CdescriptionU3Ek__BackingField_5() const { return ___U3CdescriptionU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CdescriptionU3Ek__BackingField_5() { return &___U3CdescriptionU3Ek__BackingField_5; }
	inline void set_U3CdescriptionU3Ek__BackingField_5(String_t* value)
	{
		___U3CdescriptionU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdescriptionU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3Cbase_model_nameU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Customization_t47167237, ___U3Cbase_model_nameU3Ek__BackingField_6)); }
	inline String_t* get_U3Cbase_model_nameU3Ek__BackingField_6() const { return ___U3Cbase_model_nameU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3Cbase_model_nameU3Ek__BackingField_6() { return &___U3Cbase_model_nameU3Ek__BackingField_6; }
	inline void set_U3Cbase_model_nameU3Ek__BackingField_6(String_t* value)
	{
		___U3Cbase_model_nameU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cbase_model_nameU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CstatusU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(Customization_t47167237, ___U3CstatusU3Ek__BackingField_7)); }
	inline String_t* get_U3CstatusU3Ek__BackingField_7() const { return ___U3CstatusU3Ek__BackingField_7; }
	inline String_t** get_address_of_U3CstatusU3Ek__BackingField_7() { return &___U3CstatusU3Ek__BackingField_7; }
	inline void set_U3CstatusU3Ek__BackingField_7(String_t* value)
	{
		___U3CstatusU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstatusU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CprogressU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(Customization_t47167237, ___U3CprogressU3Ek__BackingField_8)); }
	inline int32_t get_U3CprogressU3Ek__BackingField_8() const { return ___U3CprogressU3Ek__BackingField_8; }
	inline int32_t* get_address_of_U3CprogressU3Ek__BackingField_8() { return &___U3CprogressU3Ek__BackingField_8; }
	inline void set_U3CprogressU3Ek__BackingField_8(int32_t value)
	{
		___U3CprogressU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CwarningsU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(Customization_t47167237, ___U3CwarningsU3Ek__BackingField_9)); }
	inline String_t* get_U3CwarningsU3Ek__BackingField_9() const { return ___U3CwarningsU3Ek__BackingField_9; }
	inline String_t** get_address_of_U3CwarningsU3Ek__BackingField_9() { return &___U3CwarningsU3Ek__BackingField_9; }
	inline void set_U3CwarningsU3Ek__BackingField_9(String_t* value)
	{
		___U3CwarningsU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwarningsU3Ek__BackingField_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMIZATION_T47167237_H
#ifndef WORDSLIST_T29283159_H
#define WORDSLIST_T29283159_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordsList
struct  WordsList_t29283159  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordData[] IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordsList::<words>k__BackingField
	WordDataU5BU5D_t1726140911* ___U3CwordsU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CwordsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(WordsList_t29283159, ___U3CwordsU3Ek__BackingField_0)); }
	inline WordDataU5BU5D_t1726140911* get_U3CwordsU3Ek__BackingField_0() const { return ___U3CwordsU3Ek__BackingField_0; }
	inline WordDataU5BU5D_t1726140911** get_address_of_U3CwordsU3Ek__BackingField_0() { return &___U3CwordsU3Ek__BackingField_0; }
	inline void set_U3CwordsU3Ek__BackingField_0(WordDataU5BU5D_t1726140911* value)
	{
		___U3CwordsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwordsU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORDSLIST_T29283159_H
#ifndef CORPUS_T3396664578_H
#define CORPUS_T3396664578_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Corpus
struct  Corpus_t3396664578  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Corpus::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_0;
	// System.Int32 IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Corpus::<total_words>k__BackingField
	int32_t ___U3Ctotal_wordsU3Ek__BackingField_1;
	// System.Int32 IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Corpus::<out_of_vocabulary_words>k__BackingField
	int32_t ___U3Cout_of_vocabulary_wordsU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Corpus::<status>k__BackingField
	String_t* ___U3CstatusU3Ek__BackingField_3;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Corpus::<error>k__BackingField
	String_t* ___U3CerrorU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Corpus_t3396664578, ___U3CnameU3Ek__BackingField_0)); }
	inline String_t* get_U3CnameU3Ek__BackingField_0() const { return ___U3CnameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_0() { return &___U3CnameU3Ek__BackingField_0; }
	inline void set_U3CnameU3Ek__BackingField_0(String_t* value)
	{
		___U3CnameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3Ctotal_wordsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Corpus_t3396664578, ___U3Ctotal_wordsU3Ek__BackingField_1)); }
	inline int32_t get_U3Ctotal_wordsU3Ek__BackingField_1() const { return ___U3Ctotal_wordsU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3Ctotal_wordsU3Ek__BackingField_1() { return &___U3Ctotal_wordsU3Ek__BackingField_1; }
	inline void set_U3Ctotal_wordsU3Ek__BackingField_1(int32_t value)
	{
		___U3Ctotal_wordsU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3Cout_of_vocabulary_wordsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Corpus_t3396664578, ___U3Cout_of_vocabulary_wordsU3Ek__BackingField_2)); }
	inline int32_t get_U3Cout_of_vocabulary_wordsU3Ek__BackingField_2() const { return ___U3Cout_of_vocabulary_wordsU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3Cout_of_vocabulary_wordsU3Ek__BackingField_2() { return &___U3Cout_of_vocabulary_wordsU3Ek__BackingField_2; }
	inline void set_U3Cout_of_vocabulary_wordsU3Ek__BackingField_2(int32_t value)
	{
		___U3Cout_of_vocabulary_wordsU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CstatusU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Corpus_t3396664578, ___U3CstatusU3Ek__BackingField_3)); }
	inline String_t* get_U3CstatusU3Ek__BackingField_3() const { return ___U3CstatusU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CstatusU3Ek__BackingField_3() { return &___U3CstatusU3Ek__BackingField_3; }
	inline void set_U3CstatusU3Ek__BackingField_3(String_t* value)
	{
		___U3CstatusU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstatusU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CerrorU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Corpus_t3396664578, ___U3CerrorU3Ek__BackingField_4)); }
	inline String_t* get_U3CerrorU3Ek__BackingField_4() const { return ___U3CerrorU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CerrorU3Ek__BackingField_4() { return &___U3CerrorU3Ek__BackingField_4; }
	inline void set_U3CerrorU3Ek__BackingField_4(String_t* value)
	{
		___U3CerrorU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CerrorU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CORPUS_T3396664578_H
#ifndef CORPORA_T3715340602_H
#define CORPORA_T3715340602_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Corpora
struct  Corpora_t3715340602  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Corpus[] IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Corpora::<corpora>k__BackingField
	CorpusU5BU5D_t2250486967* ___U3CcorporaU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CcorporaU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Corpora_t3715340602, ___U3CcorporaU3Ek__BackingField_0)); }
	inline CorpusU5BU5D_t2250486967* get_U3CcorporaU3Ek__BackingField_0() const { return ___U3CcorporaU3Ek__BackingField_0; }
	inline CorpusU5BU5D_t2250486967** get_address_of_U3CcorporaU3Ek__BackingField_0() { return &___U3CcorporaU3Ek__BackingField_0; }
	inline void set_U3CcorporaU3Ek__BackingField_0(CorpusU5BU5D_t2250486967* value)
	{
		___U3CcorporaU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcorporaU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CORPORA_T3715340602_H
#ifndef CUSTOMIZATIONS_T482380184_H
#define CUSTOMIZATIONS_T482380184_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Customizations
struct  Customizations_t482380184  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Customization[] IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Customizations::<customizations>k__BackingField
	CustomizationU5BU5D_t626929480* ___U3CcustomizationsU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CcustomizationsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Customizations_t482380184, ___U3CcustomizationsU3Ek__BackingField_0)); }
	inline CustomizationU5BU5D_t626929480* get_U3CcustomizationsU3Ek__BackingField_0() const { return ___U3CcustomizationsU3Ek__BackingField_0; }
	inline CustomizationU5BU5D_t626929480** get_address_of_U3CcustomizationsU3Ek__BackingField_0() { return &___U3CcustomizationsU3Ek__BackingField_0; }
	inline void set_U3CcustomizationsU3Ek__BackingField_0(CustomizationU5BU5D_t626929480* value)
	{
		___U3CcustomizationsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcustomizationsU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMIZATIONS_T482380184_H
#ifndef CUSTOMIZATION_T2261013253_H
#define CUSTOMIZATION_T2261013253_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Customization
struct  Customization_t2261013253  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Customization::<customization_id>k__BackingField
	String_t* ___U3Ccustomization_idU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Customization::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Customization::<language>k__BackingField
	String_t* ___U3ClanguageU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Customization::<owner>k__BackingField
	String_t* ___U3CownerU3Ek__BackingField_3;
	// System.Double IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Customization::<created>k__BackingField
	double ___U3CcreatedU3Ek__BackingField_4;
	// System.Double IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Customization::<last_modified>k__BackingField
	double ___U3Clast_modifiedU3Ek__BackingField_5;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Customization::<description>k__BackingField
	String_t* ___U3CdescriptionU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3Ccustomization_idU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Customization_t2261013253, ___U3Ccustomization_idU3Ek__BackingField_0)); }
	inline String_t* get_U3Ccustomization_idU3Ek__BackingField_0() const { return ___U3Ccustomization_idU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3Ccustomization_idU3Ek__BackingField_0() { return &___U3Ccustomization_idU3Ek__BackingField_0; }
	inline void set_U3Ccustomization_idU3Ek__BackingField_0(String_t* value)
	{
		___U3Ccustomization_idU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Ccustomization_idU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Customization_t2261013253, ___U3CnameU3Ek__BackingField_1)); }
	inline String_t* get_U3CnameU3Ek__BackingField_1() const { return ___U3CnameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_1() { return &___U3CnameU3Ek__BackingField_1; }
	inline void set_U3CnameU3Ek__BackingField_1(String_t* value)
	{
		___U3CnameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3ClanguageU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Customization_t2261013253, ___U3ClanguageU3Ek__BackingField_2)); }
	inline String_t* get_U3ClanguageU3Ek__BackingField_2() const { return ___U3ClanguageU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3ClanguageU3Ek__BackingField_2() { return &___U3ClanguageU3Ek__BackingField_2; }
	inline void set_U3ClanguageU3Ek__BackingField_2(String_t* value)
	{
		___U3ClanguageU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClanguageU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CownerU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Customization_t2261013253, ___U3CownerU3Ek__BackingField_3)); }
	inline String_t* get_U3CownerU3Ek__BackingField_3() const { return ___U3CownerU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CownerU3Ek__BackingField_3() { return &___U3CownerU3Ek__BackingField_3; }
	inline void set_U3CownerU3Ek__BackingField_3(String_t* value)
	{
		___U3CownerU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CownerU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CcreatedU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Customization_t2261013253, ___U3CcreatedU3Ek__BackingField_4)); }
	inline double get_U3CcreatedU3Ek__BackingField_4() const { return ___U3CcreatedU3Ek__BackingField_4; }
	inline double* get_address_of_U3CcreatedU3Ek__BackingField_4() { return &___U3CcreatedU3Ek__BackingField_4; }
	inline void set_U3CcreatedU3Ek__BackingField_4(double value)
	{
		___U3CcreatedU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3Clast_modifiedU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Customization_t2261013253, ___U3Clast_modifiedU3Ek__BackingField_5)); }
	inline double get_U3Clast_modifiedU3Ek__BackingField_5() const { return ___U3Clast_modifiedU3Ek__BackingField_5; }
	inline double* get_address_of_U3Clast_modifiedU3Ek__BackingField_5() { return &___U3Clast_modifiedU3Ek__BackingField_5; }
	inline void set_U3Clast_modifiedU3Ek__BackingField_5(double value)
	{
		___U3Clast_modifiedU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CdescriptionU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Customization_t2261013253, ___U3CdescriptionU3Ek__BackingField_6)); }
	inline String_t* get_U3CdescriptionU3Ek__BackingField_6() const { return ___U3CdescriptionU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CdescriptionU3Ek__BackingField_6() { return &___U3CdescriptionU3Ek__BackingField_6; }
	inline void set_U3CdescriptionU3Ek__BackingField_6(String_t* value)
	{
		___U3CdescriptionU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdescriptionU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMIZATION_T2261013253_H
#ifndef CUSTOMIZATIONID_T344279642_H
#define CUSTOMIZATIONID_T344279642_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomizationID
struct  CustomizationID_t344279642  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomizationID::<customization_id>k__BackingField
	String_t* ___U3Ccustomization_idU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3Ccustomization_idU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CustomizationID_t344279642, ___U3Ccustomization_idU3Ek__BackingField_0)); }
	inline String_t* get_U3Ccustomization_idU3Ek__BackingField_0() const { return ___U3Ccustomization_idU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3Ccustomization_idU3Ek__BackingField_0() { return &___U3Ccustomization_idU3Ek__BackingField_0; }
	inline void set_U3Ccustomization_idU3Ek__BackingField_0(String_t* value)
	{
		___U3Ccustomization_idU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Ccustomization_idU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMIZATIONID_T344279642_H
#ifndef VOICE_T3646862260_H
#define VOICE_T3646862260_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Voice
struct  Voice_t3646862260  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Voice::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Voice::<language>k__BackingField
	String_t* ___U3ClanguageU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Voice::<gender>k__BackingField
	String_t* ___U3CgenderU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Voice::<url>k__BackingField
	String_t* ___U3CurlU3Ek__BackingField_3;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Voice::<description>k__BackingField
	String_t* ___U3CdescriptionU3Ek__BackingField_4;
	// System.Boolean IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Voice::<customizable>k__BackingField
	bool ___U3CcustomizableU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Voice_t3646862260, ___U3CnameU3Ek__BackingField_0)); }
	inline String_t* get_U3CnameU3Ek__BackingField_0() const { return ___U3CnameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_0() { return &___U3CnameU3Ek__BackingField_0; }
	inline void set_U3CnameU3Ek__BackingField_0(String_t* value)
	{
		___U3CnameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3ClanguageU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Voice_t3646862260, ___U3ClanguageU3Ek__BackingField_1)); }
	inline String_t* get_U3ClanguageU3Ek__BackingField_1() const { return ___U3ClanguageU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3ClanguageU3Ek__BackingField_1() { return &___U3ClanguageU3Ek__BackingField_1; }
	inline void set_U3ClanguageU3Ek__BackingField_1(String_t* value)
	{
		___U3ClanguageU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClanguageU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CgenderU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Voice_t3646862260, ___U3CgenderU3Ek__BackingField_2)); }
	inline String_t* get_U3CgenderU3Ek__BackingField_2() const { return ___U3CgenderU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CgenderU3Ek__BackingField_2() { return &___U3CgenderU3Ek__BackingField_2; }
	inline void set_U3CgenderU3Ek__BackingField_2(String_t* value)
	{
		___U3CgenderU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CgenderU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CurlU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Voice_t3646862260, ___U3CurlU3Ek__BackingField_3)); }
	inline String_t* get_U3CurlU3Ek__BackingField_3() const { return ___U3CurlU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CurlU3Ek__BackingField_3() { return &___U3CurlU3Ek__BackingField_3; }
	inline void set_U3CurlU3Ek__BackingField_3(String_t* value)
	{
		___U3CurlU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CurlU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CdescriptionU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Voice_t3646862260, ___U3CdescriptionU3Ek__BackingField_4)); }
	inline String_t* get_U3CdescriptionU3Ek__BackingField_4() const { return ___U3CdescriptionU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CdescriptionU3Ek__BackingField_4() { return &___U3CdescriptionU3Ek__BackingField_4; }
	inline void set_U3CdescriptionU3Ek__BackingField_4(String_t* value)
	{
		___U3CdescriptionU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdescriptionU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CcustomizableU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Voice_t3646862260, ___U3CcustomizableU3Ek__BackingField_5)); }
	inline bool get_U3CcustomizableU3Ek__BackingField_5() const { return ___U3CcustomizableU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CcustomizableU3Ek__BackingField_5() { return &___U3CcustomizableU3Ek__BackingField_5; }
	inline void set_U3CcustomizableU3Ek__BackingField_5(bool value)
	{
		___U3CcustomizableU3Ek__BackingField_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOICE_T3646862260_H
#ifndef VOICES_T4221445733_H
#define VOICES_T4221445733_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Voices
struct  Voices_t4221445733  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Voice[] IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Voices::<voices>k__BackingField
	VoiceU5BU5D_t260285181* ___U3CvoicesU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CvoicesU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Voices_t4221445733, ___U3CvoicesU3Ek__BackingField_0)); }
	inline VoiceU5BU5D_t260285181* get_U3CvoicesU3Ek__BackingField_0() const { return ___U3CvoicesU3Ek__BackingField_0; }
	inline VoiceU5BU5D_t260285181** get_address_of_U3CvoicesU3Ek__BackingField_0() { return &___U3CvoicesU3Ek__BackingField_0; }
	inline void set_U3CvoicesU3Ek__BackingField_0(VoiceU5BU5D_t260285181* value)
	{
		___U3CvoicesU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CvoicesU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOICES_T4221445733_H
#ifndef PRONUNCIATION_T2711344207_H
#define PRONUNCIATION_T2711344207_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Pronunciation
struct  Pronunciation_t2711344207  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Pronunciation::<pronunciation>k__BackingField
	String_t* ___U3CpronunciationU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CpronunciationU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Pronunciation_t2711344207, ___U3CpronunciationU3Ek__BackingField_0)); }
	inline String_t* get_U3CpronunciationU3Ek__BackingField_0() const { return ___U3CpronunciationU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CpronunciationU3Ek__BackingField_0() { return &___U3CpronunciationU3Ek__BackingField_0; }
	inline void set_U3CpronunciationU3Ek__BackingField_0(String_t* value)
	{
		___U3CpronunciationU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpronunciationU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRONUNCIATION_T2711344207_H
#ifndef CUSTOMVOICE_T330695553_H
#define CUSTOMVOICE_T330695553_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomVoice
struct  CustomVoice_t330695553  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomVoice::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomVoice::<language>k__BackingField
	String_t* ___U3ClanguageU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomVoice::<description>k__BackingField
	String_t* ___U3CdescriptionU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CustomVoice_t330695553, ___U3CnameU3Ek__BackingField_0)); }
	inline String_t* get_U3CnameU3Ek__BackingField_0() const { return ___U3CnameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_0() { return &___U3CnameU3Ek__BackingField_0; }
	inline void set_U3CnameU3Ek__BackingField_0(String_t* value)
	{
		___U3CnameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3ClanguageU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CustomVoice_t330695553, ___U3ClanguageU3Ek__BackingField_1)); }
	inline String_t* get_U3ClanguageU3Ek__BackingField_1() const { return ___U3ClanguageU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3ClanguageU3Ek__BackingField_1() { return &___U3ClanguageU3Ek__BackingField_1; }
	inline void set_U3ClanguageU3Ek__BackingField_1(String_t* value)
	{
		___U3ClanguageU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClanguageU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CdescriptionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CustomVoice_t330695553, ___U3CdescriptionU3Ek__BackingField_2)); }
	inline String_t* get_U3CdescriptionU3Ek__BackingField_2() const { return ___U3CdescriptionU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CdescriptionU3Ek__BackingField_2() { return &___U3CdescriptionU3Ek__BackingField_2; }
	inline void set_U3CdescriptionU3Ek__BackingField_2(String_t* value)
	{
		___U3CdescriptionU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdescriptionU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMVOICE_T330695553_H
#ifndef CUSTOMVOICEUPDATE_T1706248840_H
#define CUSTOMVOICEUPDATE_T1706248840_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomVoiceUpdate
struct  CustomVoiceUpdate_t1706248840  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomVoiceUpdate::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomVoiceUpdate::<description>k__BackingField
	String_t* ___U3CdescriptionU3Ek__BackingField_1;
	// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Word[] IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomVoiceUpdate::<words>k__BackingField
	WordU5BU5D_t3686373631* ___U3CwordsU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CustomVoiceUpdate_t1706248840, ___U3CnameU3Ek__BackingField_0)); }
	inline String_t* get_U3CnameU3Ek__BackingField_0() const { return ___U3CnameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_0() { return &___U3CnameU3Ek__BackingField_0; }
	inline void set_U3CnameU3Ek__BackingField_0(String_t* value)
	{
		___U3CnameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CdescriptionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CustomVoiceUpdate_t1706248840, ___U3CdescriptionU3Ek__BackingField_1)); }
	inline String_t* get_U3CdescriptionU3Ek__BackingField_1() const { return ___U3CdescriptionU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CdescriptionU3Ek__BackingField_1() { return &___U3CdescriptionU3Ek__BackingField_1; }
	inline void set_U3CdescriptionU3Ek__BackingField_1(String_t* value)
	{
		___U3CdescriptionU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdescriptionU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CwordsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CustomVoiceUpdate_t1706248840, ___U3CwordsU3Ek__BackingField_2)); }
	inline WordU5BU5D_t3686373631* get_U3CwordsU3Ek__BackingField_2() const { return ___U3CwordsU3Ek__BackingField_2; }
	inline WordU5BU5D_t3686373631** get_address_of_U3CwordsU3Ek__BackingField_2() { return &___U3CwordsU3Ek__BackingField_2; }
	inline void set_U3CwordsU3Ek__BackingField_2(WordU5BU5D_t3686373631* value)
	{
		___U3CwordsU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwordsU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMVOICEUPDATE_T1706248840_H
#ifndef TRANSLATION_T2174867539_H
#define TRANSLATION_T2174867539_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Translation
struct  Translation_t2174867539  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Translation::<translation>k__BackingField
	String_t* ___U3CtranslationU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CtranslationU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Translation_t2174867539, ___U3CtranslationU3Ek__BackingField_0)); }
	inline String_t* get_U3CtranslationU3Ek__BackingField_0() const { return ___U3CtranslationU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CtranslationU3Ek__BackingField_0() { return &___U3CtranslationU3Ek__BackingField_0; }
	inline void set_U3CtranslationU3Ek__BackingField_0(String_t* value)
	{
		___U3CtranslationU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtranslationU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSLATION_T2174867539_H
#ifndef ERRORMODEL_T3210737207_H
#define ERRORMODEL_T3210737207_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.ErrorModel
struct  ErrorModel_t3210737207  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.ErrorModel::<error>k__BackingField
	String_t* ___U3CerrorU3Ek__BackingField_0;
	// System.Int32 IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.ErrorModel::<code>k__BackingField
	int32_t ___U3CcodeU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.ErrorModel::<code_description>k__BackingField
	String_t* ___U3Ccode_descriptionU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CerrorU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ErrorModel_t3210737207, ___U3CerrorU3Ek__BackingField_0)); }
	inline String_t* get_U3CerrorU3Ek__BackingField_0() const { return ___U3CerrorU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CerrorU3Ek__BackingField_0() { return &___U3CerrorU3Ek__BackingField_0; }
	inline void set_U3CerrorU3Ek__BackingField_0(String_t* value)
	{
		___U3CerrorU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CerrorU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CcodeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ErrorModel_t3210737207, ___U3CcodeU3Ek__BackingField_1)); }
	inline int32_t get_U3CcodeU3Ek__BackingField_1() const { return ___U3CcodeU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CcodeU3Ek__BackingField_1() { return &___U3CcodeU3Ek__BackingField_1; }
	inline void set_U3CcodeU3Ek__BackingField_1(int32_t value)
	{
		___U3CcodeU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3Ccode_descriptionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ErrorModel_t3210737207, ___U3Ccode_descriptionU3Ek__BackingField_2)); }
	inline String_t* get_U3Ccode_descriptionU3Ek__BackingField_2() const { return ___U3Ccode_descriptionU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3Ccode_descriptionU3Ek__BackingField_2() { return &___U3Ccode_descriptionU3Ek__BackingField_2; }
	inline void set_U3Ccode_descriptionU3Ek__BackingField_2(String_t* value)
	{
		___U3Ccode_descriptionU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3Ccode_descriptionU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERRORMODEL_T3210737207_H
#ifndef CUSTOMIZATIONWORDS_T949522428_H
#define CUSTOMIZATIONWORDS_T949522428_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomizationWords
struct  CustomizationWords_t949522428  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomizationWords::<customization_id>k__BackingField
	String_t* ___U3Ccustomization_idU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomizationWords::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomizationWords::<language>k__BackingField
	String_t* ___U3ClanguageU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomizationWords::<owner>k__BackingField
	String_t* ___U3CownerU3Ek__BackingField_3;
	// System.Double IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomizationWords::<created>k__BackingField
	double ___U3CcreatedU3Ek__BackingField_4;
	// System.Double IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomizationWords::<last_modified>k__BackingField
	double ___U3Clast_modifiedU3Ek__BackingField_5;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomizationWords::<description>k__BackingField
	String_t* ___U3CdescriptionU3Ek__BackingField_6;
	// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Word[] IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomizationWords::<words>k__BackingField
	WordU5BU5D_t3686373631* ___U3CwordsU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_U3Ccustomization_idU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CustomizationWords_t949522428, ___U3Ccustomization_idU3Ek__BackingField_0)); }
	inline String_t* get_U3Ccustomization_idU3Ek__BackingField_0() const { return ___U3Ccustomization_idU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3Ccustomization_idU3Ek__BackingField_0() { return &___U3Ccustomization_idU3Ek__BackingField_0; }
	inline void set_U3Ccustomization_idU3Ek__BackingField_0(String_t* value)
	{
		___U3Ccustomization_idU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Ccustomization_idU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CustomizationWords_t949522428, ___U3CnameU3Ek__BackingField_1)); }
	inline String_t* get_U3CnameU3Ek__BackingField_1() const { return ___U3CnameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_1() { return &___U3CnameU3Ek__BackingField_1; }
	inline void set_U3CnameU3Ek__BackingField_1(String_t* value)
	{
		___U3CnameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3ClanguageU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CustomizationWords_t949522428, ___U3ClanguageU3Ek__BackingField_2)); }
	inline String_t* get_U3ClanguageU3Ek__BackingField_2() const { return ___U3ClanguageU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3ClanguageU3Ek__BackingField_2() { return &___U3ClanguageU3Ek__BackingField_2; }
	inline void set_U3ClanguageU3Ek__BackingField_2(String_t* value)
	{
		___U3ClanguageU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClanguageU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CownerU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(CustomizationWords_t949522428, ___U3CownerU3Ek__BackingField_3)); }
	inline String_t* get_U3CownerU3Ek__BackingField_3() const { return ___U3CownerU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CownerU3Ek__BackingField_3() { return &___U3CownerU3Ek__BackingField_3; }
	inline void set_U3CownerU3Ek__BackingField_3(String_t* value)
	{
		___U3CownerU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CownerU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CcreatedU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(CustomizationWords_t949522428, ___U3CcreatedU3Ek__BackingField_4)); }
	inline double get_U3CcreatedU3Ek__BackingField_4() const { return ___U3CcreatedU3Ek__BackingField_4; }
	inline double* get_address_of_U3CcreatedU3Ek__BackingField_4() { return &___U3CcreatedU3Ek__BackingField_4; }
	inline void set_U3CcreatedU3Ek__BackingField_4(double value)
	{
		___U3CcreatedU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3Clast_modifiedU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(CustomizationWords_t949522428, ___U3Clast_modifiedU3Ek__BackingField_5)); }
	inline double get_U3Clast_modifiedU3Ek__BackingField_5() const { return ___U3Clast_modifiedU3Ek__BackingField_5; }
	inline double* get_address_of_U3Clast_modifiedU3Ek__BackingField_5() { return &___U3Clast_modifiedU3Ek__BackingField_5; }
	inline void set_U3Clast_modifiedU3Ek__BackingField_5(double value)
	{
		___U3Clast_modifiedU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CdescriptionU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(CustomizationWords_t949522428, ___U3CdescriptionU3Ek__BackingField_6)); }
	inline String_t* get_U3CdescriptionU3Ek__BackingField_6() const { return ___U3CdescriptionU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CdescriptionU3Ek__BackingField_6() { return &___U3CdescriptionU3Ek__BackingField_6; }
	inline void set_U3CdescriptionU3Ek__BackingField_6(String_t* value)
	{
		___U3CdescriptionU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdescriptionU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CwordsU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(CustomizationWords_t949522428, ___U3CwordsU3Ek__BackingField_7)); }
	inline WordU5BU5D_t3686373631* get_U3CwordsU3Ek__BackingField_7() const { return ___U3CwordsU3Ek__BackingField_7; }
	inline WordU5BU5D_t3686373631** get_address_of_U3CwordsU3Ek__BackingField_7() { return &___U3CwordsU3Ek__BackingField_7; }
	inline void set_U3CwordsU3Ek__BackingField_7(WordU5BU5D_t3686373631* value)
	{
		___U3CwordsU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwordsU3Ek__BackingField_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMIZATIONWORDS_T949522428_H
#ifndef WORDS_T2948214629_H
#define WORDS_T2948214629_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Words
struct  Words_t2948214629  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Word[] IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Words::<words>k__BackingField
	WordU5BU5D_t3686373631* ___U3CwordsU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CwordsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Words_t2948214629, ___U3CwordsU3Ek__BackingField_0)); }
	inline WordU5BU5D_t3686373631* get_U3CwordsU3Ek__BackingField_0() const { return ___U3CwordsU3Ek__BackingField_0; }
	inline WordU5BU5D_t3686373631** get_address_of_U3CwordsU3Ek__BackingField_0() { return &___U3CwordsU3Ek__BackingField_0; }
	inline void set_U3CwordsU3Ek__BackingField_0(WordU5BU5D_t3686373631* value)
	{
		___U3CwordsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwordsU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORDS_T2948214629_H
#ifndef WORD_T4274554970_H
#define WORD_T4274554970_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Word
struct  Word_t4274554970  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Word::<word>k__BackingField
	String_t* ___U3CwordU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Word::<translation>k__BackingField
	String_t* ___U3CtranslationU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CwordU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Word_t4274554970, ___U3CwordU3Ek__BackingField_0)); }
	inline String_t* get_U3CwordU3Ek__BackingField_0() const { return ___U3CwordU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CwordU3Ek__BackingField_0() { return &___U3CwordU3Ek__BackingField_0; }
	inline void set_U3CwordU3Ek__BackingField_0(String_t* value)
	{
		___U3CwordU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwordU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CtranslationU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Word_t4274554970, ___U3CtranslationU3Ek__BackingField_1)); }
	inline String_t* get_U3CtranslationU3Ek__BackingField_1() const { return ___U3CtranslationU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CtranslationU3Ek__BackingField_1() { return &___U3CtranslationU3Ek__BackingField_1; }
	inline void set_U3CtranslationU3Ek__BackingField_1(String_t* value)
	{
		___U3CtranslationU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtranslationU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORD_T4274554970_H
#ifndef CUSTOMIZATIONS_T4082930968_H
#define CUSTOMIZATIONS_T4082930968_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Customizations
struct  Customizations_t4082930968  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Customization[] IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Customizations::<customizations>k__BackingField
	CustomizationU5BU5D_t543472456* ___U3CcustomizationsU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CcustomizationsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Customizations_t4082930968, ___U3CcustomizationsU3Ek__BackingField_0)); }
	inline CustomizationU5BU5D_t543472456* get_U3CcustomizationsU3Ek__BackingField_0() const { return ___U3CcustomizationsU3Ek__BackingField_0; }
	inline CustomizationU5BU5D_t543472456** get_address_of_U3CcustomizationsU3Ek__BackingField_0() { return &___U3CcustomizationsU3Ek__BackingField_0; }
	inline void set_U3CcustomizationsU3Ek__BackingField_0(CustomizationU5BU5D_t543472456* value)
	{
		___U3CcustomizationsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcustomizationsU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMIZATIONS_T4082930968_H
#ifndef SPEECHRECOGNITIONEVENT_T1591882439_H
#define SPEECHRECOGNITIONEVENT_T1591882439_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechRecognitionEvent
struct  SpeechRecognitionEvent_t1591882439  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechRecognitionResult[] IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechRecognitionEvent::<results>k__BackingField
	SpeechRecognitionResultU5BU5D_t1533902383* ___U3CresultsU3Ek__BackingField_0;
	// System.Int32 IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechRecognitionEvent::<result_index>k__BackingField
	int32_t ___U3Cresult_indexU3Ek__BackingField_1;
	// System.String[] IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechRecognitionEvent::<warnings>k__BackingField
	StringU5BU5D_t1642385972* ___U3CwarningsU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CresultsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SpeechRecognitionEvent_t1591882439, ___U3CresultsU3Ek__BackingField_0)); }
	inline SpeechRecognitionResultU5BU5D_t1533902383* get_U3CresultsU3Ek__BackingField_0() const { return ___U3CresultsU3Ek__BackingField_0; }
	inline SpeechRecognitionResultU5BU5D_t1533902383** get_address_of_U3CresultsU3Ek__BackingField_0() { return &___U3CresultsU3Ek__BackingField_0; }
	inline void set_U3CresultsU3Ek__BackingField_0(SpeechRecognitionResultU5BU5D_t1533902383* value)
	{
		___U3CresultsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CresultsU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3Cresult_indexU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(SpeechRecognitionEvent_t1591882439, ___U3Cresult_indexU3Ek__BackingField_1)); }
	inline int32_t get_U3Cresult_indexU3Ek__BackingField_1() const { return ___U3Cresult_indexU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3Cresult_indexU3Ek__BackingField_1() { return &___U3Cresult_indexU3Ek__BackingField_1; }
	inline void set_U3Cresult_indexU3Ek__BackingField_1(int32_t value)
	{
		___U3Cresult_indexU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CwarningsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SpeechRecognitionEvent_t1591882439, ___U3CwarningsU3Ek__BackingField_2)); }
	inline StringU5BU5D_t1642385972* get_U3CwarningsU3Ek__BackingField_2() const { return ___U3CwarningsU3Ek__BackingField_2; }
	inline StringU5BU5D_t1642385972** get_address_of_U3CwarningsU3Ek__BackingField_2() { return &___U3CwarningsU3Ek__BackingField_2; }
	inline void set_U3CwarningsU3Ek__BackingField_2(StringU5BU5D_t1642385972* value)
	{
		___U3CwarningsU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwarningsU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPEECHRECOGNITIONEVENT_T1591882439_H
#ifndef SESSION_T1940512798_H
#define SESSION_T1940512798_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Session
struct  Session_t1940512798  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Session::<session_id>k__BackingField
	String_t* ___U3Csession_idU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Session::<new_session_uri>k__BackingField
	String_t* ___U3Cnew_session_uriU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Session::<recognize>k__BackingField
	String_t* ___U3CrecognizeU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Session::<observe_result>k__BackingField
	String_t* ___U3Cobserve_resultU3Ek__BackingField_3;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Session::<recognizeWS>k__BackingField
	String_t* ___U3CrecognizeWSU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3Csession_idU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Session_t1940512798, ___U3Csession_idU3Ek__BackingField_0)); }
	inline String_t* get_U3Csession_idU3Ek__BackingField_0() const { return ___U3Csession_idU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3Csession_idU3Ek__BackingField_0() { return &___U3Csession_idU3Ek__BackingField_0; }
	inline void set_U3Csession_idU3Ek__BackingField_0(String_t* value)
	{
		___U3Csession_idU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Csession_idU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3Cnew_session_uriU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Session_t1940512798, ___U3Cnew_session_uriU3Ek__BackingField_1)); }
	inline String_t* get_U3Cnew_session_uriU3Ek__BackingField_1() const { return ___U3Cnew_session_uriU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3Cnew_session_uriU3Ek__BackingField_1() { return &___U3Cnew_session_uriU3Ek__BackingField_1; }
	inline void set_U3Cnew_session_uriU3Ek__BackingField_1(String_t* value)
	{
		___U3Cnew_session_uriU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cnew_session_uriU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CrecognizeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Session_t1940512798, ___U3CrecognizeU3Ek__BackingField_2)); }
	inline String_t* get_U3CrecognizeU3Ek__BackingField_2() const { return ___U3CrecognizeU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CrecognizeU3Ek__BackingField_2() { return &___U3CrecognizeU3Ek__BackingField_2; }
	inline void set_U3CrecognizeU3Ek__BackingField_2(String_t* value)
	{
		___U3CrecognizeU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrecognizeU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3Cobserve_resultU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Session_t1940512798, ___U3Cobserve_resultU3Ek__BackingField_3)); }
	inline String_t* get_U3Cobserve_resultU3Ek__BackingField_3() const { return ___U3Cobserve_resultU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3Cobserve_resultU3Ek__BackingField_3() { return &___U3Cobserve_resultU3Ek__BackingField_3; }
	inline void set_U3Cobserve_resultU3Ek__BackingField_3(String_t* value)
	{
		___U3Cobserve_resultU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cobserve_resultU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CrecognizeWSU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Session_t1940512798, ___U3CrecognizeWSU3Ek__BackingField_4)); }
	inline String_t* get_U3CrecognizeWSU3Ek__BackingField_4() const { return ___U3CrecognizeWSU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CrecognizeWSU3Ek__BackingField_4() { return &___U3CrecognizeWSU3Ek__BackingField_4; }
	inline void set_U3CrecognizeWSU3Ek__BackingField_4(String_t* value)
	{
		___U3CrecognizeWSU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrecognizeWSU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SESSION_T1940512798_H
#ifndef SPEECHRECOGNITIONRESULT_T3827468010_H
#define SPEECHRECOGNITIONRESULT_T3827468010_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechRecognitionResult
struct  SpeechRecognitionResult_t3827468010  : public RuntimeObject
{
public:
	// System.Boolean IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechRecognitionResult::<final>k__BackingField
	bool ___U3CfinalU3Ek__BackingField_0;
	// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechRecognitionAlternative[] IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechRecognitionResult::<alternatives>k__BackingField
	SpeechRecognitionAlternativeU5BU5D_t769752751* ___U3CalternativesU3Ek__BackingField_1;
	// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.KeywordResults IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechRecognitionResult::<keywords_result>k__BackingField
	KeywordResults_t4114097975 * ___U3Ckeywords_resultU3Ek__BackingField_2;
	// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordAlternativeResults IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechRecognitionResult::<word_alternatives>k__BackingField
	WordAlternativeResults_t1270965811 * ___U3Cword_alternativesU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CfinalU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SpeechRecognitionResult_t3827468010, ___U3CfinalU3Ek__BackingField_0)); }
	inline bool get_U3CfinalU3Ek__BackingField_0() const { return ___U3CfinalU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CfinalU3Ek__BackingField_0() { return &___U3CfinalU3Ek__BackingField_0; }
	inline void set_U3CfinalU3Ek__BackingField_0(bool value)
	{
		___U3CfinalU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CalternativesU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(SpeechRecognitionResult_t3827468010, ___U3CalternativesU3Ek__BackingField_1)); }
	inline SpeechRecognitionAlternativeU5BU5D_t769752751* get_U3CalternativesU3Ek__BackingField_1() const { return ___U3CalternativesU3Ek__BackingField_1; }
	inline SpeechRecognitionAlternativeU5BU5D_t769752751** get_address_of_U3CalternativesU3Ek__BackingField_1() { return &___U3CalternativesU3Ek__BackingField_1; }
	inline void set_U3CalternativesU3Ek__BackingField_1(SpeechRecognitionAlternativeU5BU5D_t769752751* value)
	{
		___U3CalternativesU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CalternativesU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3Ckeywords_resultU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SpeechRecognitionResult_t3827468010, ___U3Ckeywords_resultU3Ek__BackingField_2)); }
	inline KeywordResults_t4114097975 * get_U3Ckeywords_resultU3Ek__BackingField_2() const { return ___U3Ckeywords_resultU3Ek__BackingField_2; }
	inline KeywordResults_t4114097975 ** get_address_of_U3Ckeywords_resultU3Ek__BackingField_2() { return &___U3Ckeywords_resultU3Ek__BackingField_2; }
	inline void set_U3Ckeywords_resultU3Ek__BackingField_2(KeywordResults_t4114097975 * value)
	{
		___U3Ckeywords_resultU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3Ckeywords_resultU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3Cword_alternativesU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SpeechRecognitionResult_t3827468010, ___U3Cword_alternativesU3Ek__BackingField_3)); }
	inline WordAlternativeResults_t1270965811 * get_U3Cword_alternativesU3Ek__BackingField_3() const { return ___U3Cword_alternativesU3Ek__BackingField_3; }
	inline WordAlternativeResults_t1270965811 ** get_address_of_U3Cword_alternativesU3Ek__BackingField_3() { return &___U3Cword_alternativesU3Ek__BackingField_3; }
	inline void set_U3Cword_alternativesU3Ek__BackingField_3(WordAlternativeResults_t1270965811 * value)
	{
		___U3Cword_alternativesU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cword_alternativesU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPEECHRECOGNITIONRESULT_T3827468010_H
#ifndef KEYWORDRESULTS_T4114097975_H
#define KEYWORDRESULTS_T4114097975_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.KeywordResults
struct  KeywordResults_t4114097975  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.KeywordResult[] IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.KeywordResults::<keyword>k__BackingField
	KeywordResultU5BU5D_t1719463693* ___U3CkeywordU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CkeywordU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(KeywordResults_t4114097975, ___U3CkeywordU3Ek__BackingField_0)); }
	inline KeywordResultU5BU5D_t1719463693* get_U3CkeywordU3Ek__BackingField_0() const { return ___U3CkeywordU3Ek__BackingField_0; }
	inline KeywordResultU5BU5D_t1719463693** get_address_of_U3CkeywordU3Ek__BackingField_0() { return &___U3CkeywordU3Ek__BackingField_0; }
	inline void set_U3CkeywordU3Ek__BackingField_0(KeywordResultU5BU5D_t1719463693* value)
	{
		___U3CkeywordU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CkeywordU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYWORDRESULTS_T4114097975_H
#ifndef SPEECHRECOGNITIONALTERNATIVE_T84095082_H
#define SPEECHRECOGNITIONALTERNATIVE_T84095082_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechRecognitionAlternative
struct  SpeechRecognitionAlternative_t84095082  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechRecognitionAlternative::<transcript>k__BackingField
	String_t* ___U3CtranscriptU3Ek__BackingField_0;
	// System.Double IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechRecognitionAlternative::<confidence>k__BackingField
	double ___U3CconfidenceU3Ek__BackingField_1;
	// System.String[] IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechRecognitionAlternative::<timestamps>k__BackingField
	StringU5BU5D_t1642385972* ___U3CtimestampsU3Ek__BackingField_2;
	// System.String[] IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechRecognitionAlternative::<word_confidence>k__BackingField
	StringU5BU5D_t1642385972* ___U3Cword_confidenceU3Ek__BackingField_3;
	// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.TimeStamp[] IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechRecognitionAlternative::<Timestamps>k__BackingField
	TimeStampU5BU5D_t2852280099* ___U3CTimestampsU3Ek__BackingField_4;
	// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordConfidence[] IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechRecognitionAlternative::<WordConfidence>k__BackingField
	WordConfidenceU5BU5D_t3370323303* ___U3CWordConfidenceU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CtranscriptU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SpeechRecognitionAlternative_t84095082, ___U3CtranscriptU3Ek__BackingField_0)); }
	inline String_t* get_U3CtranscriptU3Ek__BackingField_0() const { return ___U3CtranscriptU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CtranscriptU3Ek__BackingField_0() { return &___U3CtranscriptU3Ek__BackingField_0; }
	inline void set_U3CtranscriptU3Ek__BackingField_0(String_t* value)
	{
		___U3CtranscriptU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtranscriptU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CconfidenceU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(SpeechRecognitionAlternative_t84095082, ___U3CconfidenceU3Ek__BackingField_1)); }
	inline double get_U3CconfidenceU3Ek__BackingField_1() const { return ___U3CconfidenceU3Ek__BackingField_1; }
	inline double* get_address_of_U3CconfidenceU3Ek__BackingField_1() { return &___U3CconfidenceU3Ek__BackingField_1; }
	inline void set_U3CconfidenceU3Ek__BackingField_1(double value)
	{
		___U3CconfidenceU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CtimestampsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SpeechRecognitionAlternative_t84095082, ___U3CtimestampsU3Ek__BackingField_2)); }
	inline StringU5BU5D_t1642385972* get_U3CtimestampsU3Ek__BackingField_2() const { return ___U3CtimestampsU3Ek__BackingField_2; }
	inline StringU5BU5D_t1642385972** get_address_of_U3CtimestampsU3Ek__BackingField_2() { return &___U3CtimestampsU3Ek__BackingField_2; }
	inline void set_U3CtimestampsU3Ek__BackingField_2(StringU5BU5D_t1642385972* value)
	{
		___U3CtimestampsU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtimestampsU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3Cword_confidenceU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SpeechRecognitionAlternative_t84095082, ___U3Cword_confidenceU3Ek__BackingField_3)); }
	inline StringU5BU5D_t1642385972* get_U3Cword_confidenceU3Ek__BackingField_3() const { return ___U3Cword_confidenceU3Ek__BackingField_3; }
	inline StringU5BU5D_t1642385972** get_address_of_U3Cword_confidenceU3Ek__BackingField_3() { return &___U3Cword_confidenceU3Ek__BackingField_3; }
	inline void set_U3Cword_confidenceU3Ek__BackingField_3(StringU5BU5D_t1642385972* value)
	{
		___U3Cword_confidenceU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cword_confidenceU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CTimestampsU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(SpeechRecognitionAlternative_t84095082, ___U3CTimestampsU3Ek__BackingField_4)); }
	inline TimeStampU5BU5D_t2852280099* get_U3CTimestampsU3Ek__BackingField_4() const { return ___U3CTimestampsU3Ek__BackingField_4; }
	inline TimeStampU5BU5D_t2852280099** get_address_of_U3CTimestampsU3Ek__BackingField_4() { return &___U3CTimestampsU3Ek__BackingField_4; }
	inline void set_U3CTimestampsU3Ek__BackingField_4(TimeStampU5BU5D_t2852280099* value)
	{
		___U3CTimestampsU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTimestampsU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CWordConfidenceU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(SpeechRecognitionAlternative_t84095082, ___U3CWordConfidenceU3Ek__BackingField_5)); }
	inline WordConfidenceU5BU5D_t3370323303* get_U3CWordConfidenceU3Ek__BackingField_5() const { return ___U3CWordConfidenceU3Ek__BackingField_5; }
	inline WordConfidenceU5BU5D_t3370323303** get_address_of_U3CWordConfidenceU3Ek__BackingField_5() { return &___U3CWordConfidenceU3Ek__BackingField_5; }
	inline void set_U3CWordConfidenceU3Ek__BackingField_5(WordConfidenceU5BU5D_t3370323303* value)
	{
		___U3CWordConfidenceU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CWordConfidenceU3Ek__BackingField_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPEECHRECOGNITIONALTERNATIVE_T84095082_H
#ifndef SUPPORTEDFEATURES_T1255602913_H
#define SUPPORTEDFEATURES_T1255602913_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SupportedFeatures
struct  SupportedFeatures_t1255602913  : public RuntimeObject
{
public:
	// System.Boolean IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SupportedFeatures::<custom_language_model>k__BackingField
	bool ___U3Ccustom_language_modelU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3Ccustom_language_modelU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SupportedFeatures_t1255602913, ___U3Ccustom_language_modelU3Ek__BackingField_0)); }
	inline bool get_U3Ccustom_language_modelU3Ek__BackingField_0() const { return ___U3Ccustom_language_modelU3Ek__BackingField_0; }
	inline bool* get_address_of_U3Ccustom_language_modelU3Ek__BackingField_0() { return &___U3Ccustom_language_modelU3Ek__BackingField_0; }
	inline void set_U3Ccustom_language_modelU3Ek__BackingField_0(bool value)
	{
		___U3Ccustom_language_modelU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUPPORTEDFEATURES_T1255602913_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef WORDTYPETOADD_T2941578038_H
#define WORDTYPETOADD_T2941578038_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordTypeToAdd
struct  WordTypeToAdd_t2941578038  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORDTYPETOADD_T2941578038_H
#ifndef CHECKSERVICESTATUS_T4192214874_H
#define CHECKSERVICESTATUS_T4192214874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/CheckServiceStatus
struct  CheckServiceStatus_t4192214874  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/CheckServiceStatus::m_Service
	RetrieveAndRank_t1381045327 * ___m_Service_0;
	// IBM.Watson.DeveloperCloud.Services.ServiceStatus IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/CheckServiceStatus::m_Callback
	ServiceStatus_t1443707987 * ___m_Callback_1;

public:
	inline static int32_t get_offset_of_m_Service_0() { return static_cast<int32_t>(offsetof(CheckServiceStatus_t4192214874, ___m_Service_0)); }
	inline RetrieveAndRank_t1381045327 * get_m_Service_0() const { return ___m_Service_0; }
	inline RetrieveAndRank_t1381045327 ** get_address_of_m_Service_0() { return &___m_Service_0; }
	inline void set_m_Service_0(RetrieveAndRank_t1381045327 * value)
	{
		___m_Service_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Service_0), value);
	}

	inline static int32_t get_offset_of_m_Callback_1() { return static_cast<int32_t>(offsetof(CheckServiceStatus_t4192214874, ___m_Callback_1)); }
	inline ServiceStatus_t1443707987 * get_m_Callback_1() const { return ___m_Callback_1; }
	inline ServiceStatus_t1443707987 ** get_address_of_m_Callback_1() { return &___m_Callback_1; }
	inline void set_m_Callback_1(ServiceStatus_t1443707987 * value)
	{
		___m_Callback_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Callback_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHECKSERVICESTATUS_T4192214874_H
#ifndef MODEL_T2340542523_H
#define MODEL_T2340542523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Model
struct  Model_t2340542523  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Model::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Model::<language>k__BackingField
	String_t* ___U3ClanguageU3Ek__BackingField_1;
	// System.Int64 IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Model::<rate>k__BackingField
	int64_t ___U3CrateU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Model::<url>k__BackingField
	String_t* ___U3CurlU3Ek__BackingField_3;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Model::<sessions>k__BackingField
	String_t* ___U3CsessionsU3Ek__BackingField_4;
	// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SupportedFeatures IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Model::<supported_features>k__BackingField
	SupportedFeatures_t1255602913 * ___U3Csupported_featuresU3Ek__BackingField_5;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Model::<description>k__BackingField
	String_t* ___U3CdescriptionU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Model_t2340542523, ___U3CnameU3Ek__BackingField_0)); }
	inline String_t* get_U3CnameU3Ek__BackingField_0() const { return ___U3CnameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_0() { return &___U3CnameU3Ek__BackingField_0; }
	inline void set_U3CnameU3Ek__BackingField_0(String_t* value)
	{
		___U3CnameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3ClanguageU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Model_t2340542523, ___U3ClanguageU3Ek__BackingField_1)); }
	inline String_t* get_U3ClanguageU3Ek__BackingField_1() const { return ___U3ClanguageU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3ClanguageU3Ek__BackingField_1() { return &___U3ClanguageU3Ek__BackingField_1; }
	inline void set_U3ClanguageU3Ek__BackingField_1(String_t* value)
	{
		___U3ClanguageU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClanguageU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CrateU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Model_t2340542523, ___U3CrateU3Ek__BackingField_2)); }
	inline int64_t get_U3CrateU3Ek__BackingField_2() const { return ___U3CrateU3Ek__BackingField_2; }
	inline int64_t* get_address_of_U3CrateU3Ek__BackingField_2() { return &___U3CrateU3Ek__BackingField_2; }
	inline void set_U3CrateU3Ek__BackingField_2(int64_t value)
	{
		___U3CrateU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CurlU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Model_t2340542523, ___U3CurlU3Ek__BackingField_3)); }
	inline String_t* get_U3CurlU3Ek__BackingField_3() const { return ___U3CurlU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CurlU3Ek__BackingField_3() { return &___U3CurlU3Ek__BackingField_3; }
	inline void set_U3CurlU3Ek__BackingField_3(String_t* value)
	{
		___U3CurlU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CurlU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CsessionsU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Model_t2340542523, ___U3CsessionsU3Ek__BackingField_4)); }
	inline String_t* get_U3CsessionsU3Ek__BackingField_4() const { return ___U3CsessionsU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CsessionsU3Ek__BackingField_4() { return &___U3CsessionsU3Ek__BackingField_4; }
	inline void set_U3CsessionsU3Ek__BackingField_4(String_t* value)
	{
		___U3CsessionsU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsessionsU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3Csupported_featuresU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Model_t2340542523, ___U3Csupported_featuresU3Ek__BackingField_5)); }
	inline SupportedFeatures_t1255602913 * get_U3Csupported_featuresU3Ek__BackingField_5() const { return ___U3Csupported_featuresU3Ek__BackingField_5; }
	inline SupportedFeatures_t1255602913 ** get_address_of_U3Csupported_featuresU3Ek__BackingField_5() { return &___U3Csupported_featuresU3Ek__BackingField_5; }
	inline void set_U3Csupported_featuresU3Ek__BackingField_5(SupportedFeatures_t1255602913 * value)
	{
		___U3Csupported_featuresU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3Csupported_featuresU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CdescriptionU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Model_t2340542523, ___U3CdescriptionU3Ek__BackingField_6)); }
	inline String_t* get_U3CdescriptionU3Ek__BackingField_6() const { return ___U3CdescriptionU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CdescriptionU3Ek__BackingField_6() { return &___U3CdescriptionU3Ek__BackingField_6; }
	inline void set_U3CdescriptionU3Ek__BackingField_6(String_t* value)
	{
		___U3CdescriptionU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdescriptionU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODEL_T2340542523_H
#ifndef MODELSET_T2925843349_H
#define MODELSET_T2925843349_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.ModelSet
struct  ModelSet_t2925843349  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Model[] IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.ModelSet::<models>k__BackingField
	ModelU5BU5D_t545738874* ___U3CmodelsU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CmodelsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ModelSet_t2925843349, ___U3CmodelsU3Ek__BackingField_0)); }
	inline ModelU5BU5D_t545738874* get_U3CmodelsU3Ek__BackingField_0() const { return ___U3CmodelsU3Ek__BackingField_0; }
	inline ModelU5BU5D_t545738874** get_address_of_U3CmodelsU3Ek__BackingField_0() { return &___U3CmodelsU3Ek__BackingField_0; }
	inline void set_U3CmodelsU3Ek__BackingField_0(ModelU5BU5D_t545738874* value)
	{
		___U3CmodelsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmodelsU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODELSET_T2925843349_H
#ifndef WORDALTERNATIVERESULTS_T1270965811_H
#define WORDALTERNATIVERESULTS_T1270965811_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordAlternativeResults
struct  WordAlternativeResults_t1270965811  : public RuntimeObject
{
public:
	// System.Double IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordAlternativeResults::<start_time>k__BackingField
	double ___U3Cstart_timeU3Ek__BackingField_0;
	// System.Double IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordAlternativeResults::<end_time>k__BackingField
	double ___U3Cend_timeU3Ek__BackingField_1;
	// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordAlternativeResult[] IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordAlternativeResults::<alternatives>k__BackingField
	WordAlternativeResultU5BU5D_t598168141* ___U3CalternativesU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3Cstart_timeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(WordAlternativeResults_t1270965811, ___U3Cstart_timeU3Ek__BackingField_0)); }
	inline double get_U3Cstart_timeU3Ek__BackingField_0() const { return ___U3Cstart_timeU3Ek__BackingField_0; }
	inline double* get_address_of_U3Cstart_timeU3Ek__BackingField_0() { return &___U3Cstart_timeU3Ek__BackingField_0; }
	inline void set_U3Cstart_timeU3Ek__BackingField_0(double value)
	{
		___U3Cstart_timeU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3Cend_timeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(WordAlternativeResults_t1270965811, ___U3Cend_timeU3Ek__BackingField_1)); }
	inline double get_U3Cend_timeU3Ek__BackingField_1() const { return ___U3Cend_timeU3Ek__BackingField_1; }
	inline double* get_address_of_U3Cend_timeU3Ek__BackingField_1() { return &___U3Cend_timeU3Ek__BackingField_1; }
	inline void set_U3Cend_timeU3Ek__BackingField_1(double value)
	{
		___U3Cend_timeU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CalternativesU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(WordAlternativeResults_t1270965811, ___U3CalternativesU3Ek__BackingField_2)); }
	inline WordAlternativeResultU5BU5D_t598168141* get_U3CalternativesU3Ek__BackingField_2() const { return ___U3CalternativesU3Ek__BackingField_2; }
	inline WordAlternativeResultU5BU5D_t598168141** get_address_of_U3CalternativesU3Ek__BackingField_2() { return &___U3CalternativesU3Ek__BackingField_2; }
	inline void set_U3CalternativesU3Ek__BackingField_2(WordAlternativeResultU5BU5D_t598168141* value)
	{
		___U3CalternativesU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CalternativesU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORDALTERNATIVERESULTS_T1270965811_H
#ifndef TIMESTAMP_T82276870_H
#define TIMESTAMP_T82276870_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.TimeStamp
struct  TimeStamp_t82276870  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.TimeStamp::<Word>k__BackingField
	String_t* ___U3CWordU3Ek__BackingField_0;
	// System.Double IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.TimeStamp::<Start>k__BackingField
	double ___U3CStartU3Ek__BackingField_1;
	// System.Double IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.TimeStamp::<End>k__BackingField
	double ___U3CEndU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CWordU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TimeStamp_t82276870, ___U3CWordU3Ek__BackingField_0)); }
	inline String_t* get_U3CWordU3Ek__BackingField_0() const { return ___U3CWordU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CWordU3Ek__BackingField_0() { return &___U3CWordU3Ek__BackingField_0; }
	inline void set_U3CWordU3Ek__BackingField_0(String_t* value)
	{
		___U3CWordU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CWordU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CStartU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TimeStamp_t82276870, ___U3CStartU3Ek__BackingField_1)); }
	inline double get_U3CStartU3Ek__BackingField_1() const { return ___U3CStartU3Ek__BackingField_1; }
	inline double* get_address_of_U3CStartU3Ek__BackingField_1() { return &___U3CStartU3Ek__BackingField_1; }
	inline void set_U3CStartU3Ek__BackingField_1(double value)
	{
		___U3CStartU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CEndU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(TimeStamp_t82276870, ___U3CEndU3Ek__BackingField_2)); }
	inline double get_U3CEndU3Ek__BackingField_2() const { return ___U3CEndU3Ek__BackingField_2; }
	inline double* get_address_of_U3CEndU3Ek__BackingField_2() { return &___U3CEndU3Ek__BackingField_2; }
	inline void set_U3CEndU3Ek__BackingField_2(double value)
	{
		___U3CEndU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESTAMP_T82276870_H
#ifndef WORDCONFIDENCE_T2757029394_H
#define WORDCONFIDENCE_T2757029394_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordConfidence
struct  WordConfidence_t2757029394  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordConfidence::<Word>k__BackingField
	String_t* ___U3CWordU3Ek__BackingField_0;
	// System.Double IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordConfidence::<Confidence>k__BackingField
	double ___U3CConfidenceU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CWordU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(WordConfidence_t2757029394, ___U3CWordU3Ek__BackingField_0)); }
	inline String_t* get_U3CWordU3Ek__BackingField_0() const { return ___U3CWordU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CWordU3Ek__BackingField_0() { return &___U3CWordU3Ek__BackingField_0; }
	inline void set_U3CWordU3Ek__BackingField_0(String_t* value)
	{
		___U3CWordU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CWordU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CConfidenceU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(WordConfidence_t2757029394, ___U3CConfidenceU3Ek__BackingField_1)); }
	inline double get_U3CConfidenceU3Ek__BackingField_1() const { return ___U3CConfidenceU3Ek__BackingField_1; }
	inline double* get_address_of_U3CConfidenceU3Ek__BackingField_1() { return &___U3CConfidenceU3Ek__BackingField_1; }
	inline void set_U3CConfidenceU3Ek__BackingField_1(double value)
	{
		___U3CConfidenceU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORDCONFIDENCE_T2757029394_H
#ifndef JOBSTATUSNEW_T1469635187_H
#define JOBSTATUSNEW_T1469635187_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.JobStatusNew
struct  JobStatusNew_t1469635187  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.JobStatusNew::<created>k__BackingField
	String_t* ___U3CcreatedU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.JobStatusNew::<id>k__BackingField
	String_t* ___U3CidU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.JobStatusNew::<url>k__BackingField
	String_t* ___U3CurlU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.JobStatusNew::<status>k__BackingField
	String_t* ___U3CstatusU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CcreatedU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(JobStatusNew_t1469635187, ___U3CcreatedU3Ek__BackingField_0)); }
	inline String_t* get_U3CcreatedU3Ek__BackingField_0() const { return ___U3CcreatedU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CcreatedU3Ek__BackingField_0() { return &___U3CcreatedU3Ek__BackingField_0; }
	inline void set_U3CcreatedU3Ek__BackingField_0(String_t* value)
	{
		___U3CcreatedU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcreatedU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CidU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(JobStatusNew_t1469635187, ___U3CidU3Ek__BackingField_1)); }
	inline String_t* get_U3CidU3Ek__BackingField_1() const { return ___U3CidU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CidU3Ek__BackingField_1() { return &___U3CidU3Ek__BackingField_1; }
	inline void set_U3CidU3Ek__BackingField_1(String_t* value)
	{
		___U3CidU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CidU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CurlU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(JobStatusNew_t1469635187, ___U3CurlU3Ek__BackingField_2)); }
	inline String_t* get_U3CurlU3Ek__BackingField_2() const { return ___U3CurlU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CurlU3Ek__BackingField_2() { return &___U3CurlU3Ek__BackingField_2; }
	inline void set_U3CurlU3Ek__BackingField_2(String_t* value)
	{
		___U3CurlU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CurlU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CstatusU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(JobStatusNew_t1469635187, ___U3CstatusU3Ek__BackingField_3)); }
	inline String_t* get_U3CstatusU3Ek__BackingField_3() const { return ___U3CstatusU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CstatusU3Ek__BackingField_3() { return &___U3CstatusU3Ek__BackingField_3; }
	inline void set_U3CstatusU3Ek__BackingField_3(String_t* value)
	{
		___U3CstatusU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstatusU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOBSTATUSNEW_T1469635187_H
#ifndef JOBSTATUS_T2358384683_H
#define JOBSTATUS_T2358384683_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.JobStatus
struct  JobStatus_t2358384683  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.JobStatus::<id>k__BackingField
	String_t* ___U3CidU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.JobStatus::<created>k__BackingField
	String_t* ___U3CcreatedU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.JobStatus::<updated>k__BackingField
	String_t* ___U3CupdatedU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.JobStatus::<status>k__BackingField
	String_t* ___U3CstatusU3Ek__BackingField_3;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.JobStatus::<user_token>k__BackingField
	String_t* ___U3Cuser_tokenU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CidU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(JobStatus_t2358384683, ___U3CidU3Ek__BackingField_0)); }
	inline String_t* get_U3CidU3Ek__BackingField_0() const { return ___U3CidU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CidU3Ek__BackingField_0() { return &___U3CidU3Ek__BackingField_0; }
	inline void set_U3CidU3Ek__BackingField_0(String_t* value)
	{
		___U3CidU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CidU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CcreatedU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(JobStatus_t2358384683, ___U3CcreatedU3Ek__BackingField_1)); }
	inline String_t* get_U3CcreatedU3Ek__BackingField_1() const { return ___U3CcreatedU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CcreatedU3Ek__BackingField_1() { return &___U3CcreatedU3Ek__BackingField_1; }
	inline void set_U3CcreatedU3Ek__BackingField_1(String_t* value)
	{
		___U3CcreatedU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcreatedU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CupdatedU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(JobStatus_t2358384683, ___U3CupdatedU3Ek__BackingField_2)); }
	inline String_t* get_U3CupdatedU3Ek__BackingField_2() const { return ___U3CupdatedU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CupdatedU3Ek__BackingField_2() { return &___U3CupdatedU3Ek__BackingField_2; }
	inline void set_U3CupdatedU3Ek__BackingField_2(String_t* value)
	{
		___U3CupdatedU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CupdatedU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CstatusU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(JobStatus_t2358384683, ___U3CstatusU3Ek__BackingField_3)); }
	inline String_t* get_U3CstatusU3Ek__BackingField_3() const { return ___U3CstatusU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CstatusU3Ek__BackingField_3() { return &___U3CstatusU3Ek__BackingField_3; }
	inline void set_U3CstatusU3Ek__BackingField_3(String_t* value)
	{
		___U3CstatusU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstatusU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3Cuser_tokenU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(JobStatus_t2358384683, ___U3Cuser_tokenU3Ek__BackingField_4)); }
	inline String_t* get_U3Cuser_tokenU3Ek__BackingField_4() const { return ___U3Cuser_tokenU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3Cuser_tokenU3Ek__BackingField_4() { return &___U3Cuser_tokenU3Ek__BackingField_4; }
	inline void set_U3Cuser_tokenU3Ek__BackingField_4(String_t* value)
	{
		___U3Cuser_tokenU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cuser_tokenU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOBSTATUS_T2358384683_H
#ifndef JOBSSTATUSLIST_T625315452_H
#define JOBSSTATUSLIST_T625315452_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.JobsStatusList
struct  JobsStatusList_t625315452  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.JobStatus[] IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.JobsStatusList::<recognitions>k__BackingField
	JobStatusU5BU5D_t38003402* ___U3CrecognitionsU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CrecognitionsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(JobsStatusList_t625315452, ___U3CrecognitionsU3Ek__BackingField_0)); }
	inline JobStatusU5BU5D_t38003402* get_U3CrecognitionsU3Ek__BackingField_0() const { return ___U3CrecognitionsU3Ek__BackingField_0; }
	inline JobStatusU5BU5D_t38003402** get_address_of_U3CrecognitionsU3Ek__BackingField_0() { return &___U3CrecognitionsU3Ek__BackingField_0; }
	inline void set_U3CrecognitionsU3Ek__BackingField_0(JobStatusU5BU5D_t38003402* value)
	{
		___U3CrecognitionsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrecognitionsU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOBSSTATUSLIST_T625315452_H
#ifndef REGISTERSTATUS_T3750858893_H
#define REGISTERSTATUS_T3750858893_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.RegisterStatus
struct  RegisterStatus_t3750858893  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.RegisterStatus::<status>k__BackingField
	String_t* ___U3CstatusU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.RegisterStatus::<url>k__BackingField
	String_t* ___U3CurlU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CstatusU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RegisterStatus_t3750858893, ___U3CstatusU3Ek__BackingField_0)); }
	inline String_t* get_U3CstatusU3Ek__BackingField_0() const { return ___U3CstatusU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CstatusU3Ek__BackingField_0() { return &___U3CstatusU3Ek__BackingField_0; }
	inline void set_U3CstatusU3Ek__BackingField_0(String_t* value)
	{
		___U3CstatusU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstatusU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CurlU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RegisterStatus_t3750858893, ___U3CurlU3Ek__BackingField_1)); }
	inline String_t* get_U3CurlU3Ek__BackingField_1() const { return ___U3CurlU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CurlU3Ek__BackingField_1() { return &___U3CurlU3Ek__BackingField_1; }
	inline void set_U3CurlU3Ek__BackingField_1(String_t* value)
	{
		___U3CurlU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CurlU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGISTERSTATUS_T3750858893_H
#ifndef KEYWORDRESULT_T2819026148_H
#define KEYWORDRESULT_T2819026148_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.KeywordResult
struct  KeywordResult_t2819026148  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.KeywordResult::<normalized_text>k__BackingField
	String_t* ___U3Cnormalized_textU3Ek__BackingField_0;
	// System.Double IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.KeywordResult::<start_time>k__BackingField
	double ___U3Cstart_timeU3Ek__BackingField_1;
	// System.Double IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.KeywordResult::<end_time>k__BackingField
	double ___U3Cend_timeU3Ek__BackingField_2;
	// System.Double IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.KeywordResult::<confidence>k__BackingField
	double ___U3CconfidenceU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3Cnormalized_textU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(KeywordResult_t2819026148, ___U3Cnormalized_textU3Ek__BackingField_0)); }
	inline String_t* get_U3Cnormalized_textU3Ek__BackingField_0() const { return ___U3Cnormalized_textU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3Cnormalized_textU3Ek__BackingField_0() { return &___U3Cnormalized_textU3Ek__BackingField_0; }
	inline void set_U3Cnormalized_textU3Ek__BackingField_0(String_t* value)
	{
		___U3Cnormalized_textU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cnormalized_textU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3Cstart_timeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(KeywordResult_t2819026148, ___U3Cstart_timeU3Ek__BackingField_1)); }
	inline double get_U3Cstart_timeU3Ek__BackingField_1() const { return ___U3Cstart_timeU3Ek__BackingField_1; }
	inline double* get_address_of_U3Cstart_timeU3Ek__BackingField_1() { return &___U3Cstart_timeU3Ek__BackingField_1; }
	inline void set_U3Cstart_timeU3Ek__BackingField_1(double value)
	{
		___U3Cstart_timeU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3Cend_timeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(KeywordResult_t2819026148, ___U3Cend_timeU3Ek__BackingField_2)); }
	inline double get_U3Cend_timeU3Ek__BackingField_2() const { return ___U3Cend_timeU3Ek__BackingField_2; }
	inline double* get_address_of_U3Cend_timeU3Ek__BackingField_2() { return &___U3Cend_timeU3Ek__BackingField_2; }
	inline void set_U3Cend_timeU3Ek__BackingField_2(double value)
	{
		___U3Cend_timeU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CconfidenceU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(KeywordResult_t2819026148, ___U3CconfidenceU3Ek__BackingField_3)); }
	inline double get_U3CconfidenceU3Ek__BackingField_3() const { return ___U3CconfidenceU3Ek__BackingField_3; }
	inline double* get_address_of_U3CconfidenceU3Ek__BackingField_3() { return &___U3CconfidenceU3Ek__BackingField_3; }
	inline void set_U3CconfidenceU3Ek__BackingField_3(double value)
	{
		___U3CconfidenceU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYWORDRESULT_T2819026148_H
#ifndef WORDALTERNATIVERESULT_T3127007908_H
#define WORDALTERNATIVERESULT_T3127007908_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordAlternativeResult
struct  WordAlternativeResult_t3127007908  : public RuntimeObject
{
public:
	// System.Double IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordAlternativeResult::<confidence>k__BackingField
	double ___U3CconfidenceU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.WordAlternativeResult::<word>k__BackingField
	String_t* ___U3CwordU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CconfidenceU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(WordAlternativeResult_t3127007908, ___U3CconfidenceU3Ek__BackingField_0)); }
	inline double get_U3CconfidenceU3Ek__BackingField_0() const { return ___U3CconfidenceU3Ek__BackingField_0; }
	inline double* get_address_of_U3CconfidenceU3Ek__BackingField_0() { return &___U3CconfidenceU3Ek__BackingField_0; }
	inline void set_U3CconfidenceU3Ek__BackingField_0(double value)
	{
		___U3CconfidenceU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CwordU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(WordAlternativeResult_t3127007908, ___U3CwordU3Ek__BackingField_1)); }
	inline String_t* get_U3CwordU3Ek__BackingField_1() const { return ___U3CwordU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CwordU3Ek__BackingField_1() { return &___U3CwordU3Ek__BackingField_1; }
	inline void set_U3CwordU3Ek__BackingField_1(String_t* value)
	{
		___U3CwordU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwordU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORDALTERNATIVERESULT_T3127007908_H
#ifndef SESSIONSTATUS_T3800674532_H
#define SESSIONSTATUS_T3800674532_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SessionStatus
struct  SessionStatus_t3800674532  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SessionStatus::<state>k__BackingField
	String_t* ___U3CstateU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SessionStatus::<model>k__BackingField
	String_t* ___U3CmodelU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SessionStatus::<recognize>k__BackingField
	String_t* ___U3CrecognizeU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SessionStatus::<observe_result>k__BackingField
	String_t* ___U3Cobserve_resultU3Ek__BackingField_3;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SessionStatus::<recognizeWS>k__BackingField
	String_t* ___U3CrecognizeWSU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CstateU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SessionStatus_t3800674532, ___U3CstateU3Ek__BackingField_0)); }
	inline String_t* get_U3CstateU3Ek__BackingField_0() const { return ___U3CstateU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CstateU3Ek__BackingField_0() { return &___U3CstateU3Ek__BackingField_0; }
	inline void set_U3CstateU3Ek__BackingField_0(String_t* value)
	{
		___U3CstateU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstateU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CmodelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(SessionStatus_t3800674532, ___U3CmodelU3Ek__BackingField_1)); }
	inline String_t* get_U3CmodelU3Ek__BackingField_1() const { return ___U3CmodelU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CmodelU3Ek__BackingField_1() { return &___U3CmodelU3Ek__BackingField_1; }
	inline void set_U3CmodelU3Ek__BackingField_1(String_t* value)
	{
		___U3CmodelU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmodelU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CrecognizeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SessionStatus_t3800674532, ___U3CrecognizeU3Ek__BackingField_2)); }
	inline String_t* get_U3CrecognizeU3Ek__BackingField_2() const { return ___U3CrecognizeU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CrecognizeU3Ek__BackingField_2() { return &___U3CrecognizeU3Ek__BackingField_2; }
	inline void set_U3CrecognizeU3Ek__BackingField_2(String_t* value)
	{
		___U3CrecognizeU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrecognizeU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3Cobserve_resultU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SessionStatus_t3800674532, ___U3Cobserve_resultU3Ek__BackingField_3)); }
	inline String_t* get_U3Cobserve_resultU3Ek__BackingField_3() const { return ___U3Cobserve_resultU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3Cobserve_resultU3Ek__BackingField_3() { return &___U3Cobserve_resultU3Ek__BackingField_3; }
	inline void set_U3Cobserve_resultU3Ek__BackingField_3(String_t* value)
	{
		___U3Cobserve_resultU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cobserve_resultU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CrecognizeWSU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(SessionStatus_t3800674532, ___U3CrecognizeWSU3Ek__BackingField_4)); }
	inline String_t* get_U3CrecognizeWSU3Ek__BackingField_4() const { return ___U3CrecognizeWSU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CrecognizeWSU3Ek__BackingField_4() { return &___U3CrecognizeWSU3Ek__BackingField_4; }
	inline void set_U3CrecognizeWSU3Ek__BackingField_4(String_t* value)
	{
		___U3CrecognizeWSU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrecognizeWSU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SESSIONSTATUS_T3800674532_H
#ifndef RECOGNIZESTATUS_T1596590804_H
#define RECOGNIZESTATUS_T1596590804_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.RecognizeStatus
struct  RecognizeStatus_t1596590804  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SessionStatus IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.RecognizeStatus::<session>k__BackingField
	SessionStatus_t3800674532 * ___U3CsessionU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CsessionU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RecognizeStatus_t1596590804, ___U3CsessionU3Ek__BackingField_0)); }
	inline SessionStatus_t3800674532 * get_U3CsessionU3Ek__BackingField_0() const { return ___U3CsessionU3Ek__BackingField_0; }
	inline SessionStatus_t3800674532 ** get_address_of_U3CsessionU3Ek__BackingField_0() { return &___U3CsessionU3Ek__BackingField_0; }
	inline void set_U3CsessionU3Ek__BackingField_0(SessionStatus_t3800674532 * value)
	{
		___U3CsessionU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsessionU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECOGNIZESTATUS_T1596590804_H
#ifndef GETCUSTOMWORDREQ_T1487061866_H
#define GETCUSTOMWORDREQ_T1487061866_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/GetCustomWordReq
struct  GetCustomWordReq_t1487061866  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/GetCustomWordCallback IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/GetCustomWordReq::<Callback>k__BackingField
	GetCustomWordCallback_t4225912831 * ___U3CCallbackU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/GetCustomWordReq::<CustomizationID>k__BackingField
	String_t* ___U3CCustomizationIDU3Ek__BackingField_12;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/GetCustomWordReq::<Word>k__BackingField
	String_t* ___U3CWordU3Ek__BackingField_13;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/GetCustomWordReq::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetCustomWordReq_t1487061866, ___U3CCallbackU3Ek__BackingField_11)); }
	inline GetCustomWordCallback_t4225912831 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline GetCustomWordCallback_t4225912831 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(GetCustomWordCallback_t4225912831 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCustomizationIDU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GetCustomWordReq_t1487061866, ___U3CCustomizationIDU3Ek__BackingField_12)); }
	inline String_t* get_U3CCustomizationIDU3Ek__BackingField_12() const { return ___U3CCustomizationIDU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CCustomizationIDU3Ek__BackingField_12() { return &___U3CCustomizationIDU3Ek__BackingField_12; }
	inline void set_U3CCustomizationIDU3Ek__BackingField_12(String_t* value)
	{
		___U3CCustomizationIDU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCustomizationIDU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CWordU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(GetCustomWordReq_t1487061866, ___U3CWordU3Ek__BackingField_13)); }
	inline String_t* get_U3CWordU3Ek__BackingField_13() const { return ___U3CWordU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CWordU3Ek__BackingField_13() { return &___U3CWordU3Ek__BackingField_13; }
	inline void set_U3CWordU3Ek__BackingField_13(String_t* value)
	{
		___U3CWordU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CWordU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(GetCustomWordReq_t1487061866, ___U3CDataU3Ek__BackingField_14)); }
	inline String_t* get_U3CDataU3Ek__BackingField_14() const { return ___U3CDataU3Ek__BackingField_14; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_14() { return &___U3CDataU3Ek__BackingField_14; }
	inline void set_U3CDataU3Ek__BackingField_14(String_t* value)
	{
		___U3CDataU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETCUSTOMWORDREQ_T1487061866_H
#ifndef DELETECUSTOMCORPUSREQUEST_T3178306160_H
#define DELETECUSTOMCORPUSREQUEST_T3178306160_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/DeleteCustomCorpusRequest
struct  DeleteCustomCorpusRequest_t3178306160  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/OnDeleteCustomCorpusCallback IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/DeleteCustomCorpusRequest::<Callback>k__BackingField
	OnDeleteCustomCorpusCallback_t3393304107 * ___U3CCallbackU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/DeleteCustomCorpusRequest::<CustomizationID>k__BackingField
	String_t* ___U3CCustomizationIDU3Ek__BackingField_12;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/DeleteCustomCorpusRequest::<CorpusName>k__BackingField
	String_t* ___U3CCorpusNameU3Ek__BackingField_13;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/DeleteCustomCorpusRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(DeleteCustomCorpusRequest_t3178306160, ___U3CCallbackU3Ek__BackingField_11)); }
	inline OnDeleteCustomCorpusCallback_t3393304107 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline OnDeleteCustomCorpusCallback_t3393304107 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(OnDeleteCustomCorpusCallback_t3393304107 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCustomizationIDU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(DeleteCustomCorpusRequest_t3178306160, ___U3CCustomizationIDU3Ek__BackingField_12)); }
	inline String_t* get_U3CCustomizationIDU3Ek__BackingField_12() const { return ___U3CCustomizationIDU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CCustomizationIDU3Ek__BackingField_12() { return &___U3CCustomizationIDU3Ek__BackingField_12; }
	inline void set_U3CCustomizationIDU3Ek__BackingField_12(String_t* value)
	{
		___U3CCustomizationIDU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCustomizationIDU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CCorpusNameU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(DeleteCustomCorpusRequest_t3178306160, ___U3CCorpusNameU3Ek__BackingField_13)); }
	inline String_t* get_U3CCorpusNameU3Ek__BackingField_13() const { return ___U3CCorpusNameU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CCorpusNameU3Ek__BackingField_13() { return &___U3CCorpusNameU3Ek__BackingField_13; }
	inline void set_U3CCorpusNameU3Ek__BackingField_13(String_t* value)
	{
		___U3CCorpusNameU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCorpusNameU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(DeleteCustomCorpusRequest_t3178306160, ___U3CDataU3Ek__BackingField_14)); }
	inline String_t* get_U3CDataU3Ek__BackingField_14() const { return ___U3CDataU3Ek__BackingField_14; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_14() { return &___U3CDataU3Ek__BackingField_14; }
	inline void set_U3CDataU3Ek__BackingField_14(String_t* value)
	{
		___U3CDataU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELETECUSTOMCORPUSREQUEST_T3178306160_H
#ifndef ADDCUSTOMCORPUSREQUEST_T2449242150_H
#define ADDCUSTOMCORPUSREQUEST_T2449242150_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/AddCustomCorpusRequest
struct  AddCustomCorpusRequest_t2449242150  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/OnAddCustomCorpusCallback IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/AddCustomCorpusRequest::<Callback>k__BackingField
	OnAddCustomCorpusCallback_t1567930907 * ___U3CCallbackU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/AddCustomCorpusRequest::<CustomizationID>k__BackingField
	String_t* ___U3CCustomizationIDU3Ek__BackingField_12;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/AddCustomCorpusRequest::<CorpusName>k__BackingField
	String_t* ___U3CCorpusNameU3Ek__BackingField_13;
	// System.Boolean IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/AddCustomCorpusRequest::<AllowOverwrite>k__BackingField
	bool ___U3CAllowOverwriteU3Ek__BackingField_14;
	// System.Byte[] IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/AddCustomCorpusRequest::<TrainingData>k__BackingField
	ByteU5BU5D_t3397334013* ___U3CTrainingDataU3Ek__BackingField_15;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/AddCustomCorpusRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_16;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(AddCustomCorpusRequest_t2449242150, ___U3CCallbackU3Ek__BackingField_11)); }
	inline OnAddCustomCorpusCallback_t1567930907 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline OnAddCustomCorpusCallback_t1567930907 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(OnAddCustomCorpusCallback_t1567930907 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCustomizationIDU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(AddCustomCorpusRequest_t2449242150, ___U3CCustomizationIDU3Ek__BackingField_12)); }
	inline String_t* get_U3CCustomizationIDU3Ek__BackingField_12() const { return ___U3CCustomizationIDU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CCustomizationIDU3Ek__BackingField_12() { return &___U3CCustomizationIDU3Ek__BackingField_12; }
	inline void set_U3CCustomizationIDU3Ek__BackingField_12(String_t* value)
	{
		___U3CCustomizationIDU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCustomizationIDU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CCorpusNameU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(AddCustomCorpusRequest_t2449242150, ___U3CCorpusNameU3Ek__BackingField_13)); }
	inline String_t* get_U3CCorpusNameU3Ek__BackingField_13() const { return ___U3CCorpusNameU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CCorpusNameU3Ek__BackingField_13() { return &___U3CCorpusNameU3Ek__BackingField_13; }
	inline void set_U3CCorpusNameU3Ek__BackingField_13(String_t* value)
	{
		___U3CCorpusNameU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCorpusNameU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CAllowOverwriteU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(AddCustomCorpusRequest_t2449242150, ___U3CAllowOverwriteU3Ek__BackingField_14)); }
	inline bool get_U3CAllowOverwriteU3Ek__BackingField_14() const { return ___U3CAllowOverwriteU3Ek__BackingField_14; }
	inline bool* get_address_of_U3CAllowOverwriteU3Ek__BackingField_14() { return &___U3CAllowOverwriteU3Ek__BackingField_14; }
	inline void set_U3CAllowOverwriteU3Ek__BackingField_14(bool value)
	{
		___U3CAllowOverwriteU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CTrainingDataU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(AddCustomCorpusRequest_t2449242150, ___U3CTrainingDataU3Ek__BackingField_15)); }
	inline ByteU5BU5D_t3397334013* get_U3CTrainingDataU3Ek__BackingField_15() const { return ___U3CTrainingDataU3Ek__BackingField_15; }
	inline ByteU5BU5D_t3397334013** get_address_of_U3CTrainingDataU3Ek__BackingField_15() { return &___U3CTrainingDataU3Ek__BackingField_15; }
	inline void set_U3CTrainingDataU3Ek__BackingField_15(ByteU5BU5D_t3397334013* value)
	{
		___U3CTrainingDataU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTrainingDataU3Ek__BackingField_15), value);
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(AddCustomCorpusRequest_t2449242150, ___U3CDataU3Ek__BackingField_16)); }
	inline String_t* get_U3CDataU3Ek__BackingField_16() const { return ___U3CDataU3Ek__BackingField_16; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_16() { return &___U3CDataU3Ek__BackingField_16; }
	inline void set_U3CDataU3Ek__BackingField_16(String_t* value)
	{
		___U3CDataU3Ek__BackingField_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDCUSTOMCORPUSREQUEST_T2449242150_H
#ifndef DELETECUSTOMWORDREQUEST_T4122978004_H
#define DELETECUSTOMWORDREQUEST_T4122978004_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/DeleteCustomWordRequest
struct  DeleteCustomWordRequest_t4122978004  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/OnDeleteCustomWordCallback IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/DeleteCustomWordRequest::<Callback>k__BackingField
	OnDeleteCustomWordCallback_t2228320037 * ___U3CCallbackU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/DeleteCustomWordRequest::<CustomizationID>k__BackingField
	String_t* ___U3CCustomizationIDU3Ek__BackingField_12;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/DeleteCustomWordRequest::<Word>k__BackingField
	String_t* ___U3CWordU3Ek__BackingField_13;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/DeleteCustomWordRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(DeleteCustomWordRequest_t4122978004, ___U3CCallbackU3Ek__BackingField_11)); }
	inline OnDeleteCustomWordCallback_t2228320037 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline OnDeleteCustomWordCallback_t2228320037 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(OnDeleteCustomWordCallback_t2228320037 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCustomizationIDU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(DeleteCustomWordRequest_t4122978004, ___U3CCustomizationIDU3Ek__BackingField_12)); }
	inline String_t* get_U3CCustomizationIDU3Ek__BackingField_12() const { return ___U3CCustomizationIDU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CCustomizationIDU3Ek__BackingField_12() { return &___U3CCustomizationIDU3Ek__BackingField_12; }
	inline void set_U3CCustomizationIDU3Ek__BackingField_12(String_t* value)
	{
		___U3CCustomizationIDU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCustomizationIDU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CWordU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(DeleteCustomWordRequest_t4122978004, ___U3CWordU3Ek__BackingField_13)); }
	inline String_t* get_U3CWordU3Ek__BackingField_13() const { return ___U3CWordU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CWordU3Ek__BackingField_13() { return &___U3CWordU3Ek__BackingField_13; }
	inline void set_U3CWordU3Ek__BackingField_13(String_t* value)
	{
		___U3CWordU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CWordU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(DeleteCustomWordRequest_t4122978004, ___U3CDataU3Ek__BackingField_14)); }
	inline String_t* get_U3CDataU3Ek__BackingField_14() const { return ___U3CDataU3Ek__BackingField_14; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_14() { return &___U3CDataU3Ek__BackingField_14; }
	inline void set_U3CDataU3Ek__BackingField_14(String_t* value)
	{
		___U3CDataU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELETECUSTOMWORDREQUEST_T4122978004_H
#ifndef ADDCUSTOMWORDSREQUEST_T1867500931_H
#define ADDCUSTOMWORDSREQUEST_T1867500931_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/AddCustomWordsRequest
struct  AddCustomWordsRequest_t1867500931  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/AddCustomWordsCallback IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/AddCustomWordsRequest::<Callback>k__BackingField
	AddCustomWordsCallback_t4067999203 * ___U3CCallbackU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/AddCustomWordsRequest::<CustomizationID>k__BackingField
	String_t* ___U3CCustomizationIDU3Ek__BackingField_12;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/AddCustomWordsRequest::<WordsJson>k__BackingField
	String_t* ___U3CWordsJsonU3Ek__BackingField_13;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/AddCustomWordsRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(AddCustomWordsRequest_t1867500931, ___U3CCallbackU3Ek__BackingField_11)); }
	inline AddCustomWordsCallback_t4067999203 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline AddCustomWordsCallback_t4067999203 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(AddCustomWordsCallback_t4067999203 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCustomizationIDU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(AddCustomWordsRequest_t1867500931, ___U3CCustomizationIDU3Ek__BackingField_12)); }
	inline String_t* get_U3CCustomizationIDU3Ek__BackingField_12() const { return ___U3CCustomizationIDU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CCustomizationIDU3Ek__BackingField_12() { return &___U3CCustomizationIDU3Ek__BackingField_12; }
	inline void set_U3CCustomizationIDU3Ek__BackingField_12(String_t* value)
	{
		___U3CCustomizationIDU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCustomizationIDU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CWordsJsonU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(AddCustomWordsRequest_t1867500931, ___U3CWordsJsonU3Ek__BackingField_13)); }
	inline String_t* get_U3CWordsJsonU3Ek__BackingField_13() const { return ___U3CWordsJsonU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CWordsJsonU3Ek__BackingField_13() { return &___U3CWordsJsonU3Ek__BackingField_13; }
	inline void set_U3CWordsJsonU3Ek__BackingField_13(String_t* value)
	{
		___U3CWordsJsonU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CWordsJsonU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(AddCustomWordsRequest_t1867500931, ___U3CDataU3Ek__BackingField_14)); }
	inline String_t* get_U3CDataU3Ek__BackingField_14() const { return ___U3CDataU3Ek__BackingField_14; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_14() { return &___U3CDataU3Ek__BackingField_14; }
	inline void set_U3CDataU3Ek__BackingField_14(String_t* value)
	{
		___U3CDataU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDCUSTOMWORDSREQUEST_T1867500931_H
#ifndef GETCUSTOMWORDSREQ_T1009704523_H
#define GETCUSTOMWORDSREQ_T1009704523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/GetCustomWordsReq
struct  GetCustomWordsReq_t1009704523  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/GetCustomWordsCallback IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/GetCustomWordsReq::<Callback>k__BackingField
	GetCustomWordsCallback_t3048138280 * ___U3CCallbackU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/GetCustomWordsReq::<CustomizationID>k__BackingField
	String_t* ___U3CCustomizationIDU3Ek__BackingField_12;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/GetCustomWordsReq::<WordType>k__BackingField
	String_t* ___U3CWordTypeU3Ek__BackingField_13;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/GetCustomWordsReq::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetCustomWordsReq_t1009704523, ___U3CCallbackU3Ek__BackingField_11)); }
	inline GetCustomWordsCallback_t3048138280 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline GetCustomWordsCallback_t3048138280 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(GetCustomWordsCallback_t3048138280 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCustomizationIDU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GetCustomWordsReq_t1009704523, ___U3CCustomizationIDU3Ek__BackingField_12)); }
	inline String_t* get_U3CCustomizationIDU3Ek__BackingField_12() const { return ___U3CCustomizationIDU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CCustomizationIDU3Ek__BackingField_12() { return &___U3CCustomizationIDU3Ek__BackingField_12; }
	inline void set_U3CCustomizationIDU3Ek__BackingField_12(String_t* value)
	{
		___U3CCustomizationIDU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCustomizationIDU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CWordTypeU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(GetCustomWordsReq_t1009704523, ___U3CWordTypeU3Ek__BackingField_13)); }
	inline String_t* get_U3CWordTypeU3Ek__BackingField_13() const { return ___U3CWordTypeU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CWordTypeU3Ek__BackingField_13() { return &___U3CWordTypeU3Ek__BackingField_13; }
	inline void set_U3CWordTypeU3Ek__BackingField_13(String_t* value)
	{
		___U3CWordTypeU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CWordTypeU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(GetCustomWordsReq_t1009704523, ___U3CDataU3Ek__BackingField_14)); }
	inline String_t* get_U3CDataU3Ek__BackingField_14() const { return ___U3CDataU3Ek__BackingField_14; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_14() { return &___U3CDataU3Ek__BackingField_14; }
	inline void set_U3CDataU3Ek__BackingField_14(String_t* value)
	{
		___U3CDataU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETCUSTOMWORDSREQ_T1009704523_H
#ifndef BOOLEAN_T3825574718_H
#define BOOLEAN_T3825574718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t3825574718 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t3825574718, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t3825574718_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t3825574718_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t3825574718_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T3825574718_H
#ifndef VOID_T1841601450_H
#define VOID_T1841601450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1841601450 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1841601450_H
#ifndef TIMESPAN_T3430258949_H
#define TIMESPAN_T3430258949_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t3430258949 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_8;

public:
	inline static int32_t get_offset_of__ticks_8() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949, ____ticks_8)); }
	inline int64_t get__ticks_8() const { return ____ticks_8; }
	inline int64_t* get_address_of__ticks_8() { return &____ticks_8; }
	inline void set__ticks_8(int64_t value)
	{
		____ticks_8 = value;
	}
};

struct TimeSpan_t3430258949_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t3430258949  ___MaxValue_5;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t3430258949  ___MinValue_6;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t3430258949  ___Zero_7;

public:
	inline static int32_t get_offset_of_MaxValue_5() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949_StaticFields, ___MaxValue_5)); }
	inline TimeSpan_t3430258949  get_MaxValue_5() const { return ___MaxValue_5; }
	inline TimeSpan_t3430258949 * get_address_of_MaxValue_5() { return &___MaxValue_5; }
	inline void set_MaxValue_5(TimeSpan_t3430258949  value)
	{
		___MaxValue_5 = value;
	}

	inline static int32_t get_offset_of_MinValue_6() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949_StaticFields, ___MinValue_6)); }
	inline TimeSpan_t3430258949  get_MinValue_6() const { return ___MinValue_6; }
	inline TimeSpan_t3430258949 * get_address_of_MinValue_6() { return &___MinValue_6; }
	inline void set_MinValue_6(TimeSpan_t3430258949  value)
	{
		___MinValue_6 = value;
	}

	inline static int32_t get_offset_of_Zero_7() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949_StaticFields, ___Zero_7)); }
	inline TimeSpan_t3430258949  get_Zero_7() const { return ___Zero_7; }
	inline TimeSpan_t3430258949 * get_address_of_Zero_7() { return &___Zero_7; }
	inline void set_Zero_7(TimeSpan_t3430258949  value)
	{
		___Zero_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T3430258949_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t1328083999* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t1328083999* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t1328083999** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t1328083999* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef GETVOICEREQ_T1980513795_H
#define GETVOICEREQ_T1980513795_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetVoiceReq
struct  GetVoiceReq_t1980513795  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetVoiceCallback IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetVoiceReq::<Callback>k__BackingField
	GetVoiceCallback_t2752546046 * ___U3CCallbackU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetVoiceReq_t1980513795, ___U3CCallbackU3Ek__BackingField_11)); }
	inline GetVoiceCallback_t2752546046 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline GetVoiceCallback_t2752546046 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(GetVoiceCallback_t2752546046 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETVOICEREQ_T1980513795_H
#ifndef GETVOICESREQ_T1741653746_H
#define GETVOICESREQ_T1741653746_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetVoicesReq
struct  GetVoicesReq_t1741653746  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetVoicesCallback IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetVoicesReq::<Callback>k__BackingField
	GetVoicesCallback_t3012885511 * ___U3CCallbackU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetVoicesReq_t1741653746, ___U3CCallbackU3Ek__BackingField_11)); }
	inline GetVoicesCallback_t3012885511 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline GetVoicesCallback_t3012885511 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(GetVoicesCallback_t3012885511 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETVOICESREQ_T1741653746_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	IntPtr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline IntPtr_t get_Zero_1() const { return ___Zero_1; }
	inline IntPtr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(IntPtr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef TOSPEECHREQUEST_T3970128895_H
#define TOSPEECHREQUEST_T3970128895_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/ToSpeechRequest
struct  ToSpeechRequest_t3970128895  : public Request_t466816980
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/ToSpeechRequest::<TextId>k__BackingField
	String_t* ___U3CTextIdU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/ToSpeechRequest::<Text>k__BackingField
	String_t* ___U3CTextU3Ek__BackingField_12;
	// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/ToSpeechCallback IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/ToSpeechRequest::<Callback>k__BackingField
	ToSpeechCallback_t3422748631 * ___U3CCallbackU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_U3CTextIdU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(ToSpeechRequest_t3970128895, ___U3CTextIdU3Ek__BackingField_11)); }
	inline String_t* get_U3CTextIdU3Ek__BackingField_11() const { return ___U3CTextIdU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CTextIdU3Ek__BackingField_11() { return &___U3CTextIdU3Ek__BackingField_11; }
	inline void set_U3CTextIdU3Ek__BackingField_11(String_t* value)
	{
		___U3CTextIdU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTextIdU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CTextU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(ToSpeechRequest_t3970128895, ___U3CTextU3Ek__BackingField_12)); }
	inline String_t* get_U3CTextU3Ek__BackingField_12() const { return ___U3CTextU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CTextU3Ek__BackingField_12() { return &___U3CTextU3Ek__BackingField_12; }
	inline void set_U3CTextU3Ek__BackingField_12(String_t* value)
	{
		___U3CTextU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTextU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(ToSpeechRequest_t3970128895, ___U3CCallbackU3Ek__BackingField_13)); }
	inline ToSpeechCallback_t3422748631 * get_U3CCallbackU3Ek__BackingField_13() const { return ___U3CCallbackU3Ek__BackingField_13; }
	inline ToSpeechCallback_t3422748631 ** get_address_of_U3CCallbackU3Ek__BackingField_13() { return &___U3CCallbackU3Ek__BackingField_13; }
	inline void set_U3CCallbackU3Ek__BackingField_13(ToSpeechCallback_t3422748631 * value)
	{
		___U3CCallbackU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOSPEECHREQUEST_T3970128895_H
#ifndef DELETECUSTOMIZATIONREQUEST_T4276045522_H
#define DELETECUSTOMIZATIONREQUEST_T4276045522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/DeleteCustomizationRequest
struct  DeleteCustomizationRequest_t4276045522  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/OnDeleteCustomizationCallback IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/DeleteCustomizationRequest::<Callback>k__BackingField
	OnDeleteCustomizationCallback_t1940302465 * ___U3CCallbackU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/DeleteCustomizationRequest::<CustomizationID>k__BackingField
	String_t* ___U3CCustomizationIDU3Ek__BackingField_12;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/DeleteCustomizationRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(DeleteCustomizationRequest_t4276045522, ___U3CCallbackU3Ek__BackingField_11)); }
	inline OnDeleteCustomizationCallback_t1940302465 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline OnDeleteCustomizationCallback_t1940302465 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(OnDeleteCustomizationCallback_t1940302465 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCustomizationIDU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(DeleteCustomizationRequest_t4276045522, ___U3CCustomizationIDU3Ek__BackingField_12)); }
	inline String_t* get_U3CCustomizationIDU3Ek__BackingField_12() const { return ___U3CCustomizationIDU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CCustomizationIDU3Ek__BackingField_12() { return &___U3CCustomizationIDU3Ek__BackingField_12; }
	inline void set_U3CCustomizationIDU3Ek__BackingField_12(String_t* value)
	{
		___U3CCustomizationIDU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCustomizationIDU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(DeleteCustomizationRequest_t4276045522, ___U3CDataU3Ek__BackingField_13)); }
	inline String_t* get_U3CDataU3Ek__BackingField_13() const { return ___U3CDataU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_13() { return &___U3CDataU3Ek__BackingField_13; }
	inline void set_U3CDataU3Ek__BackingField_13(String_t* value)
	{
		___U3CDataU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELETECUSTOMIZATIONREQUEST_T4276045522_H
#ifndef RESETCUSTOMIZATIONREQUEST_T497265392_H
#define RESETCUSTOMIZATIONREQUEST_T497265392_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/ResetCustomizationRequest
struct  ResetCustomizationRequest_t497265392  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/ResetCustomizationCallback IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/ResetCustomizationRequest::<Callback>k__BackingField
	ResetCustomizationCallback_t1922530872 * ___U3CCallbackU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/ResetCustomizationRequest::<CustomizationID>k__BackingField
	String_t* ___U3CCustomizationIDU3Ek__BackingField_12;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/ResetCustomizationRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(ResetCustomizationRequest_t497265392, ___U3CCallbackU3Ek__BackingField_11)); }
	inline ResetCustomizationCallback_t1922530872 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline ResetCustomizationCallback_t1922530872 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(ResetCustomizationCallback_t1922530872 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCustomizationIDU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(ResetCustomizationRequest_t497265392, ___U3CCustomizationIDU3Ek__BackingField_12)); }
	inline String_t* get_U3CCustomizationIDU3Ek__BackingField_12() const { return ___U3CCustomizationIDU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CCustomizationIDU3Ek__BackingField_12() { return &___U3CCustomizationIDU3Ek__BackingField_12; }
	inline void set_U3CCustomizationIDU3Ek__BackingField_12(String_t* value)
	{
		___U3CCustomizationIDU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCustomizationIDU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(ResetCustomizationRequest_t497265392, ___U3CDataU3Ek__BackingField_13)); }
	inline String_t* get_U3CDataU3Ek__BackingField_13() const { return ___U3CDataU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_13() { return &___U3CDataU3Ek__BackingField_13; }
	inline void set_U3CDataU3Ek__BackingField_13(String_t* value)
	{
		___U3CDataU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESETCUSTOMIZATIONREQUEST_T497265392_H
#ifndef CREATECUSTOMIZATIONREQUEST_T527284345_H
#define CREATECUSTOMIZATIONREQUEST_T527284345_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/CreateCustomizationRequest
struct  CreateCustomizationRequest_t527284345  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/CreateCustomizationCallback IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/CreateCustomizationRequest::<Callback>k__BackingField
	CreateCustomizationCallback_t3694377825 * ___U3CCallbackU3Ek__BackingField_11;
	// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.CustomLanguage IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/CreateCustomizationRequest::<CustomLanguage>k__BackingField
	CustomLanguage_t1423423885 * ___U3CCustomLanguageU3Ek__BackingField_12;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/CreateCustomizationRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(CreateCustomizationRequest_t527284345, ___U3CCallbackU3Ek__BackingField_11)); }
	inline CreateCustomizationCallback_t3694377825 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline CreateCustomizationCallback_t3694377825 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(CreateCustomizationCallback_t3694377825 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCustomLanguageU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(CreateCustomizationRequest_t527284345, ___U3CCustomLanguageU3Ek__BackingField_12)); }
	inline CustomLanguage_t1423423885 * get_U3CCustomLanguageU3Ek__BackingField_12() const { return ___U3CCustomLanguageU3Ek__BackingField_12; }
	inline CustomLanguage_t1423423885 ** get_address_of_U3CCustomLanguageU3Ek__BackingField_12() { return &___U3CCustomLanguageU3Ek__BackingField_12; }
	inline void set_U3CCustomLanguageU3Ek__BackingField_12(CustomLanguage_t1423423885 * value)
	{
		___U3CCustomLanguageU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCustomLanguageU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(CreateCustomizationRequest_t527284345, ___U3CDataU3Ek__BackingField_13)); }
	inline String_t* get_U3CDataU3Ek__BackingField_13() const { return ___U3CDataU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_13() { return &___U3CDataU3Ek__BackingField_13; }
	inline void set_U3CDataU3Ek__BackingField_13(String_t* value)
	{
		___U3CDataU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREATECUSTOMIZATIONREQUEST_T527284345_H
#ifndef DELETERANKERREQUEST_T1365655110_H
#define DELETERANKERREQUEST_T1365655110_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/DeleteRankerRequest
struct  DeleteRankerRequest_t1365655110  : public Request_t466816980
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/DeleteRankerRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/DeleteRankerRequest::<RankerID>k__BackingField
	String_t* ___U3CRankerIDU3Ek__BackingField_12;
	// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/OnDeleteRanker IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/DeleteRankerRequest::<Callback>k__BackingField
	OnDeleteRanker_t2889964448 * ___U3CCallbackU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(DeleteRankerRequest_t1365655110, ___U3CDataU3Ek__BackingField_11)); }
	inline String_t* get_U3CDataU3Ek__BackingField_11() const { return ___U3CDataU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_11() { return &___U3CDataU3Ek__BackingField_11; }
	inline void set_U3CDataU3Ek__BackingField_11(String_t* value)
	{
		___U3CDataU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CRankerIDU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(DeleteRankerRequest_t1365655110, ___U3CRankerIDU3Ek__BackingField_12)); }
	inline String_t* get_U3CRankerIDU3Ek__BackingField_12() const { return ___U3CRankerIDU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CRankerIDU3Ek__BackingField_12() { return &___U3CRankerIDU3Ek__BackingField_12; }
	inline void set_U3CRankerIDU3Ek__BackingField_12(String_t* value)
	{
		___U3CRankerIDU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRankerIDU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(DeleteRankerRequest_t1365655110, ___U3CCallbackU3Ek__BackingField_13)); }
	inline OnDeleteRanker_t2889964448 * get_U3CCallbackU3Ek__BackingField_13() const { return ___U3CCallbackU3Ek__BackingField_13; }
	inline OnDeleteRanker_t2889964448 ** get_address_of_U3CCallbackU3Ek__BackingField_13() { return &___U3CCallbackU3Ek__BackingField_13; }
	inline void set_U3CCallbackU3Ek__BackingField_13(OnDeleteRanker_t2889964448 * value)
	{
		___U3CCallbackU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELETERANKERREQUEST_T1365655110_H
#ifndef GETCUSTOMIZATIONREQUEST_T3501630983_H
#define GETCUSTOMIZATIONREQUEST_T3501630983_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/GetCustomizationRequest
struct  GetCustomizationRequest_t3501630983  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/GetCustomizationCallback IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/GetCustomizationRequest::<Callback>k__BackingField
	GetCustomizationCallback_t267872143 * ___U3CCallbackU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/GetCustomizationRequest::<CustomizationID>k__BackingField
	String_t* ___U3CCustomizationIDU3Ek__BackingField_12;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/GetCustomizationRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetCustomizationRequest_t3501630983, ___U3CCallbackU3Ek__BackingField_11)); }
	inline GetCustomizationCallback_t267872143 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline GetCustomizationCallback_t267872143 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(GetCustomizationCallback_t267872143 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCustomizationIDU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GetCustomizationRequest_t3501630983, ___U3CCustomizationIDU3Ek__BackingField_12)); }
	inline String_t* get_U3CCustomizationIDU3Ek__BackingField_12() const { return ___U3CCustomizationIDU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CCustomizationIDU3Ek__BackingField_12() { return &___U3CCustomizationIDU3Ek__BackingField_12; }
	inline void set_U3CCustomizationIDU3Ek__BackingField_12(String_t* value)
	{
		___U3CCustomizationIDU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCustomizationIDU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(GetCustomizationRequest_t3501630983, ___U3CDataU3Ek__BackingField_13)); }
	inline String_t* get_U3CDataU3Ek__BackingField_13() const { return ___U3CDataU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_13() { return &___U3CDataU3Ek__BackingField_13; }
	inline void set_U3CDataU3Ek__BackingField_13(String_t* value)
	{
		___U3CDataU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETCUSTOMIZATIONREQUEST_T3501630983_H
#ifndef GETRANKERREQUEST_T2534353187_H
#define GETRANKERREQUEST_T2534353187_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/GetRankerRequest
struct  GetRankerRequest_t2534353187  : public Request_t466816980
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/GetRankerRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/GetRankerRequest::<RankerID>k__BackingField
	String_t* ___U3CRankerIDU3Ek__BackingField_12;
	// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/OnGetRanker IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/GetRankerRequest::<Callback>k__BackingField
	OnGetRanker_t299592683 * ___U3CCallbackU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetRankerRequest_t2534353187, ___U3CDataU3Ek__BackingField_11)); }
	inline String_t* get_U3CDataU3Ek__BackingField_11() const { return ___U3CDataU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_11() { return &___U3CDataU3Ek__BackingField_11; }
	inline void set_U3CDataU3Ek__BackingField_11(String_t* value)
	{
		___U3CDataU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CRankerIDU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GetRankerRequest_t2534353187, ___U3CRankerIDU3Ek__BackingField_12)); }
	inline String_t* get_U3CRankerIDU3Ek__BackingField_12() const { return ___U3CRankerIDU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CRankerIDU3Ek__BackingField_12() { return &___U3CRankerIDU3Ek__BackingField_12; }
	inline void set_U3CRankerIDU3Ek__BackingField_12(String_t* value)
	{
		___U3CRankerIDU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRankerIDU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(GetRankerRequest_t2534353187, ___U3CCallbackU3Ek__BackingField_13)); }
	inline OnGetRanker_t299592683 * get_U3CCallbackU3Ek__BackingField_13() const { return ___U3CCallbackU3Ek__BackingField_13; }
	inline OnGetRanker_t299592683 ** get_address_of_U3CCallbackU3Ek__BackingField_13() { return &___U3CCallbackU3Ek__BackingField_13; }
	inline void set_U3CCallbackU3Ek__BackingField_13(OnGetRanker_t299592683 * value)
	{
		___U3CCallbackU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRANKERREQUEST_T2534353187_H
#ifndef TRAINCUSTOMIZATIONREQUEST_T3697413505_H
#define TRAINCUSTOMIZATIONREQUEST_T3697413505_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/TrainCustomizationRequest
struct  TrainCustomizationRequest_t3697413505  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/TrainCustomizationCallback IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/TrainCustomizationRequest::<Callback>k__BackingField
	TrainCustomizationCallback_t2705176209 * ___U3CCallbackU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/TrainCustomizationRequest::<CustomizationID>k__BackingField
	String_t* ___U3CCustomizationIDU3Ek__BackingField_12;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/TrainCustomizationRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(TrainCustomizationRequest_t3697413505, ___U3CCallbackU3Ek__BackingField_11)); }
	inline TrainCustomizationCallback_t2705176209 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline TrainCustomizationCallback_t2705176209 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(TrainCustomizationCallback_t2705176209 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCustomizationIDU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(TrainCustomizationRequest_t3697413505, ___U3CCustomizationIDU3Ek__BackingField_12)); }
	inline String_t* get_U3CCustomizationIDU3Ek__BackingField_12() const { return ___U3CCustomizationIDU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CCustomizationIDU3Ek__BackingField_12() { return &___U3CCustomizationIDU3Ek__BackingField_12; }
	inline void set_U3CCustomizationIDU3Ek__BackingField_12(String_t* value)
	{
		___U3CCustomizationIDU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCustomizationIDU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(TrainCustomizationRequest_t3697413505, ___U3CDataU3Ek__BackingField_13)); }
	inline String_t* get_U3CDataU3Ek__BackingField_13() const { return ___U3CDataU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_13() { return &___U3CDataU3Ek__BackingField_13; }
	inline void set_U3CDataU3Ek__BackingField_13(String_t* value)
	{
		___U3CDataU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRAINCUSTOMIZATIONREQUEST_T3697413505_H
#ifndef GETCUSTOMIZATIONSREQ_T2274001667_H
#define GETCUSTOMIZATIONSREQ_T2274001667_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/GetCustomizationsReq
struct  GetCustomizationsReq_t2274001667  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/GetCustomizationsCallback IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/GetCustomizationsReq::<Callback>k__BackingField
	GetCustomizationsCallback_t2433078120 * ___U3CCallbackU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/GetCustomizationsReq::<Language>k__BackingField
	String_t* ___U3CLanguageU3Ek__BackingField_12;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/GetCustomizationsReq::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetCustomizationsReq_t2274001667, ___U3CCallbackU3Ek__BackingField_11)); }
	inline GetCustomizationsCallback_t2433078120 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline GetCustomizationsCallback_t2433078120 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(GetCustomizationsCallback_t2433078120 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CLanguageU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GetCustomizationsReq_t2274001667, ___U3CLanguageU3Ek__BackingField_12)); }
	inline String_t* get_U3CLanguageU3Ek__BackingField_12() const { return ___U3CLanguageU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CLanguageU3Ek__BackingField_12() { return &___U3CLanguageU3Ek__BackingField_12; }
	inline void set_U3CLanguageU3Ek__BackingField_12(String_t* value)
	{
		___U3CLanguageU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLanguageU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(GetCustomizationsReq_t2274001667, ___U3CDataU3Ek__BackingField_13)); }
	inline String_t* get_U3CDataU3Ek__BackingField_13() const { return ___U3CDataU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_13() { return &___U3CDataU3Ek__BackingField_13; }
	inline void set_U3CDataU3Ek__BackingField_13(String_t* value)
	{
		___U3CDataU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETCUSTOMIZATIONSREQ_T2274001667_H
#ifndef GETMODELSREQUEST_T1345746968_H
#define GETMODELSREQUEST_T1345746968_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/GetModelsRequest
struct  GetModelsRequest_t1345746968  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/OnGetModels IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/GetModelsRequest::<Callback>k__BackingField
	OnGetModels_t1162909112 * ___U3CCallbackU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetModelsRequest_t1345746968, ___U3CCallbackU3Ek__BackingField_11)); }
	inline OnGetModels_t1162909112 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline OnGetModels_t1162909112 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(OnGetModels_t1162909112 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETMODELSREQUEST_T1345746968_H
#ifndef GETCUSTOMCORPORAREQ_T43374614_H
#define GETCUSTOMCORPORAREQ_T43374614_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/GetCustomCorporaReq
struct  GetCustomCorporaReq_t43374614  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/GetCustomCorporaCallback IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/GetCustomCorporaReq::<Callback>k__BackingField
	GetCustomCorporaCallback_t1452492251 * ___U3CCallbackU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/GetCustomCorporaReq::<CustomizationID>k__BackingField
	String_t* ___U3CCustomizationIDU3Ek__BackingField_12;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/GetCustomCorporaReq::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetCustomCorporaReq_t43374614, ___U3CCallbackU3Ek__BackingField_11)); }
	inline GetCustomCorporaCallback_t1452492251 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline GetCustomCorporaCallback_t1452492251 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(GetCustomCorporaCallback_t1452492251 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCustomizationIDU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GetCustomCorporaReq_t43374614, ___U3CCustomizationIDU3Ek__BackingField_12)); }
	inline String_t* get_U3CCustomizationIDU3Ek__BackingField_12() const { return ___U3CCustomizationIDU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CCustomizationIDU3Ek__BackingField_12() { return &___U3CCustomizationIDU3Ek__BackingField_12; }
	inline void set_U3CCustomizationIDU3Ek__BackingField_12(String_t* value)
	{
		___U3CCustomizationIDU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCustomizationIDU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(GetCustomCorporaReq_t43374614, ___U3CDataU3Ek__BackingField_13)); }
	inline String_t* get_U3CDataU3Ek__BackingField_13() const { return ___U3CDataU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_13() { return &___U3CDataU3Ek__BackingField_13; }
	inline void set_U3CDataU3Ek__BackingField_13(String_t* value)
	{
		___U3CDataU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETCUSTOMCORPORAREQ_T43374614_H
#ifndef CREATERANKERREQUEST_T2304103365_H
#define CREATERANKERREQUEST_T2304103365_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/CreateRankerRequest
struct  CreateRankerRequest_t2304103365  : public Request_t466816980
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/CreateRankerRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/CreateRankerRequest::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_12;
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/CreateRankerRequest::<TrainingDataPath>k__BackingField
	String_t* ___U3CTrainingDataPathU3Ek__BackingField_13;
	// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/OnCreateRanker IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/CreateRankerRequest::<Callback>k__BackingField
	OnCreateRanker_t496698577 * ___U3CCallbackU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(CreateRankerRequest_t2304103365, ___U3CDataU3Ek__BackingField_11)); }
	inline String_t* get_U3CDataU3Ek__BackingField_11() const { return ___U3CDataU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_11() { return &___U3CDataU3Ek__BackingField_11; }
	inline void set_U3CDataU3Ek__BackingField_11(String_t* value)
	{
		___U3CDataU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(CreateRankerRequest_t2304103365, ___U3CNameU3Ek__BackingField_12)); }
	inline String_t* get_U3CNameU3Ek__BackingField_12() const { return ___U3CNameU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_12() { return &___U3CNameU3Ek__BackingField_12; }
	inline void set_U3CNameU3Ek__BackingField_12(String_t* value)
	{
		___U3CNameU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CTrainingDataPathU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(CreateRankerRequest_t2304103365, ___U3CTrainingDataPathU3Ek__BackingField_13)); }
	inline String_t* get_U3CTrainingDataPathU3Ek__BackingField_13() const { return ___U3CTrainingDataPathU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CTrainingDataPathU3Ek__BackingField_13() { return &___U3CTrainingDataPathU3Ek__BackingField_13; }
	inline void set_U3CTrainingDataPathU3Ek__BackingField_13(String_t* value)
	{
		___U3CTrainingDataPathU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTrainingDataPathU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(CreateRankerRequest_t2304103365, ___U3CCallbackU3Ek__BackingField_14)); }
	inline OnCreateRanker_t496698577 * get_U3CCallbackU3Ek__BackingField_14() const { return ___U3CCallbackU3Ek__BackingField_14; }
	inline OnCreateRanker_t496698577 ** get_address_of_U3CCallbackU3Ek__BackingField_14() { return &___U3CCallbackU3Ek__BackingField_14; }
	inline void set_U3CCallbackU3Ek__BackingField_14(OnCreateRanker_t496698577 * value)
	{
		___U3CCallbackU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREATERANKERREQUEST_T2304103365_H
#ifndef GETMODELREQUEST_T3143683393_H
#define GETMODELREQUEST_T3143683393_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/GetModelRequest
struct  GetModelRequest_t3143683393  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/OnGetModel IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/GetModelRequest::<Callback>k__BackingField
	OnGetModel_t3671144581 * ___U3CCallbackU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/GetModelRequest::<ModelID>k__BackingField
	String_t* ___U3CModelIDU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetModelRequest_t3143683393, ___U3CCallbackU3Ek__BackingField_11)); }
	inline OnGetModel_t3671144581 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline OnGetModel_t3671144581 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(OnGetModel_t3671144581 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CModelIDU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GetModelRequest_t3143683393, ___U3CModelIDU3Ek__BackingField_12)); }
	inline String_t* get_U3CModelIDU3Ek__BackingField_12() const { return ___U3CModelIDU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CModelIDU3Ek__BackingField_12() { return &___U3CModelIDU3Ek__BackingField_12; }
	inline void set_U3CModelIDU3Ek__BackingField_12(String_t* value)
	{
		___U3CModelIDU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CModelIDU3Ek__BackingField_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETMODELREQUEST_T3143683393_H
#ifndef RANKREQUEST_T3422279034_H
#define RANKREQUEST_T3422279034_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/RankRequest
struct  RankRequest_t3422279034  : public Request_t466816980
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/RankRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/RankRequest::<RankerID>k__BackingField
	String_t* ___U3CRankerIDU3Ek__BackingField_12;
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/RankRequest::<SearchResultsPath>k__BackingField
	String_t* ___U3CSearchResultsPathU3Ek__BackingField_13;
	// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/OnRank IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/RankRequest::<Callback>k__BackingField
	OnRank_t4002112472 * ___U3CCallbackU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(RankRequest_t3422279034, ___U3CDataU3Ek__BackingField_11)); }
	inline String_t* get_U3CDataU3Ek__BackingField_11() const { return ___U3CDataU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_11() { return &___U3CDataU3Ek__BackingField_11; }
	inline void set_U3CDataU3Ek__BackingField_11(String_t* value)
	{
		___U3CDataU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CRankerIDU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(RankRequest_t3422279034, ___U3CRankerIDU3Ek__BackingField_12)); }
	inline String_t* get_U3CRankerIDU3Ek__BackingField_12() const { return ___U3CRankerIDU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CRankerIDU3Ek__BackingField_12() { return &___U3CRankerIDU3Ek__BackingField_12; }
	inline void set_U3CRankerIDU3Ek__BackingField_12(String_t* value)
	{
		___U3CRankerIDU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRankerIDU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CSearchResultsPathU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(RankRequest_t3422279034, ___U3CSearchResultsPathU3Ek__BackingField_13)); }
	inline String_t* get_U3CSearchResultsPathU3Ek__BackingField_13() const { return ___U3CSearchResultsPathU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CSearchResultsPathU3Ek__BackingField_13() { return &___U3CSearchResultsPathU3Ek__BackingField_13; }
	inline void set_U3CSearchResultsPathU3Ek__BackingField_13(String_t* value)
	{
		___U3CSearchResultsPathU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSearchResultsPathU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(RankRequest_t3422279034, ___U3CCallbackU3Ek__BackingField_14)); }
	inline OnRank_t4002112472 * get_U3CCallbackU3Ek__BackingField_14() const { return ___U3CCallbackU3Ek__BackingField_14; }
	inline OnRank_t4002112472 ** get_address_of_U3CCallbackU3Ek__BackingField_14() { return &___U3CCallbackU3Ek__BackingField_14; }
	inline void set_U3CCallbackU3Ek__BackingField_14(OnRank_t4002112472 * value)
	{
		___U3CCallbackU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANKREQUEST_T3422279034_H
#ifndef UPGRADECUSTOMIZATIONREQUEST_T2164609845_H
#define UPGRADECUSTOMIZATIONREQUEST_T2164609845_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/UpgradeCustomizationRequest
struct  UpgradeCustomizationRequest_t2164609845  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/UpgradeCustomizationCallback IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/UpgradeCustomizationRequest::<Callback>k__BackingField
	UpgradeCustomizationCallback_t567321637 * ___U3CCallbackU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/UpgradeCustomizationRequest::<CustomizationID>k__BackingField
	String_t* ___U3CCustomizationIDU3Ek__BackingField_12;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/UpgradeCustomizationRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(UpgradeCustomizationRequest_t2164609845, ___U3CCallbackU3Ek__BackingField_11)); }
	inline UpgradeCustomizationCallback_t567321637 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline UpgradeCustomizationCallback_t567321637 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(UpgradeCustomizationCallback_t567321637 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCustomizationIDU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(UpgradeCustomizationRequest_t2164609845, ___U3CCustomizationIDU3Ek__BackingField_12)); }
	inline String_t* get_U3CCustomizationIDU3Ek__BackingField_12() const { return ___U3CCustomizationIDU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CCustomizationIDU3Ek__BackingField_12() { return &___U3CCustomizationIDU3Ek__BackingField_12; }
	inline void set_U3CCustomizationIDU3Ek__BackingField_12(String_t* value)
	{
		___U3CCustomizationIDU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCustomizationIDU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(UpgradeCustomizationRequest_t2164609845, ___U3CDataU3Ek__BackingField_13)); }
	inline String_t* get_U3CDataU3Ek__BackingField_13() const { return ___U3CDataU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_13() { return &___U3CDataU3Ek__BackingField_13; }
	inline void set_U3CDataU3Ek__BackingField_13(String_t* value)
	{
		___U3CDataU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPGRADECUSTOMIZATIONREQUEST_T2164609845_H
#ifndef RECOGNIZEREQUEST_T3152278898_H
#define RECOGNIZEREQUEST_T3152278898_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/RecognizeRequest
struct  RecognizeRequest_t3152278898  : public Request_t466816980
{
public:
	// UnityEngine.AudioClip IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/RecognizeRequest::<Clip>k__BackingField
	AudioClip_t1932558630 * ___U3CClipU3Ek__BackingField_11;
	// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/OnRecognize IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/RecognizeRequest::<Callback>k__BackingField
	OnRecognize_t3538205802 * ___U3CCallbackU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_U3CClipU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(RecognizeRequest_t3152278898, ___U3CClipU3Ek__BackingField_11)); }
	inline AudioClip_t1932558630 * get_U3CClipU3Ek__BackingField_11() const { return ___U3CClipU3Ek__BackingField_11; }
	inline AudioClip_t1932558630 ** get_address_of_U3CClipU3Ek__BackingField_11() { return &___U3CClipU3Ek__BackingField_11; }
	inline void set_U3CClipU3Ek__BackingField_11(AudioClip_t1932558630 * value)
	{
		___U3CClipU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CClipU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(RecognizeRequest_t3152278898, ___U3CCallbackU3Ek__BackingField_12)); }
	inline OnRecognize_t3538205802 * get_U3CCallbackU3Ek__BackingField_12() const { return ___U3CCallbackU3Ek__BackingField_12; }
	inline OnRecognize_t3538205802 ** get_address_of_U3CCallbackU3Ek__BackingField_12() { return &___U3CCallbackU3Ek__BackingField_12; }
	inline void set_U3CCallbackU3Ek__BackingField_12(OnRecognize_t3538205802 * value)
	{
		___U3CCallbackU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECOGNIZEREQUEST_T3152278898_H
#ifndef VOICETYPE_T981025524_H
#define VOICETYPE_T981025524_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.VoiceType
struct  VoiceType_t981025524 
{
public:
	// System.Int32 IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.VoiceType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(VoiceType_t981025524, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOICETYPE_T981025524_H
#ifndef DATETIMEKIND_T2186819611_H
#define DATETIMEKIND_T2186819611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeKind
struct  DateTimeKind_t2186819611 
{
public:
	// System.Int32 System.DateTimeKind::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeKind_t2186819611, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEKIND_T2186819611_H
#ifndef DELEGATE_T3022476291_H
#define DELEGATE_T3022476291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t3022476291  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	IntPtr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	IntPtr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	IntPtr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	IntPtr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1572802995 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___invoke_impl_1)); }
	inline IntPtr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline IntPtr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(IntPtr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_3)); }
	inline IntPtr_t get_method_3() const { return ___method_3; }
	inline IntPtr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(IntPtr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___delegate_trampoline_4)); }
	inline IntPtr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline IntPtr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(IntPtr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_code_5)); }
	inline IntPtr_t get_method_code_5() const { return ___method_code_5; }
	inline IntPtr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(IntPtr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___data_8)); }
	inline DelegateData_t1572802995 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1572802995 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1572802995 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T3022476291_H
#ifndef AUDIOFORMATTYPE_T2756784245_H
#define AUDIOFORMATTYPE_T2756784245_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.AudioFormatType
struct  AudioFormatType_t2756784245 
{
public:
	// System.Int32 IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.AudioFormatType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AudioFormatType_t2756784245, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOFORMATTYPE_T2756784245_H
#ifndef TEXTTOSPEECH_T3349357562_H
#define TEXTTOSPEECH_T3349357562_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech
struct  TextToSpeech_t3349357562  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Utilities.DataCache IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech::m_SpeechCache
	DataCache_t4250340070 * ___m_SpeechCache_0;
	// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.VoiceType IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech::m_Voice
	int32_t ___m_Voice_1;
	// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.AudioFormatType IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech::m_AudioFormat
	int32_t ___m_AudioFormat_2;
	// System.Collections.Generic.Dictionary`2<IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.VoiceType,System.String> IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech::m_VoiceTypes
	Dictionary_2_t1569883280 * ___m_VoiceTypes_3;
	// System.Collections.Generic.Dictionary`2<IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.AudioFormatType,System.String> IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech::m_AudioFormats
	Dictionary_2_t2576372715 * ___m_AudioFormats_4;
	// System.Boolean IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech::<DisableCache>k__BackingField
	bool ___U3CDisableCacheU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of_m_SpeechCache_0() { return static_cast<int32_t>(offsetof(TextToSpeech_t3349357562, ___m_SpeechCache_0)); }
	inline DataCache_t4250340070 * get_m_SpeechCache_0() const { return ___m_SpeechCache_0; }
	inline DataCache_t4250340070 ** get_address_of_m_SpeechCache_0() { return &___m_SpeechCache_0; }
	inline void set_m_SpeechCache_0(DataCache_t4250340070 * value)
	{
		___m_SpeechCache_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_SpeechCache_0), value);
	}

	inline static int32_t get_offset_of_m_Voice_1() { return static_cast<int32_t>(offsetof(TextToSpeech_t3349357562, ___m_Voice_1)); }
	inline int32_t get_m_Voice_1() const { return ___m_Voice_1; }
	inline int32_t* get_address_of_m_Voice_1() { return &___m_Voice_1; }
	inline void set_m_Voice_1(int32_t value)
	{
		___m_Voice_1 = value;
	}

	inline static int32_t get_offset_of_m_AudioFormat_2() { return static_cast<int32_t>(offsetof(TextToSpeech_t3349357562, ___m_AudioFormat_2)); }
	inline int32_t get_m_AudioFormat_2() const { return ___m_AudioFormat_2; }
	inline int32_t* get_address_of_m_AudioFormat_2() { return &___m_AudioFormat_2; }
	inline void set_m_AudioFormat_2(int32_t value)
	{
		___m_AudioFormat_2 = value;
	}

	inline static int32_t get_offset_of_m_VoiceTypes_3() { return static_cast<int32_t>(offsetof(TextToSpeech_t3349357562, ___m_VoiceTypes_3)); }
	inline Dictionary_2_t1569883280 * get_m_VoiceTypes_3() const { return ___m_VoiceTypes_3; }
	inline Dictionary_2_t1569883280 ** get_address_of_m_VoiceTypes_3() { return &___m_VoiceTypes_3; }
	inline void set_m_VoiceTypes_3(Dictionary_2_t1569883280 * value)
	{
		___m_VoiceTypes_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_VoiceTypes_3), value);
	}

	inline static int32_t get_offset_of_m_AudioFormats_4() { return static_cast<int32_t>(offsetof(TextToSpeech_t3349357562, ___m_AudioFormats_4)); }
	inline Dictionary_2_t2576372715 * get_m_AudioFormats_4() const { return ___m_AudioFormats_4; }
	inline Dictionary_2_t2576372715 ** get_address_of_m_AudioFormats_4() { return &___m_AudioFormats_4; }
	inline void set_m_AudioFormats_4(Dictionary_2_t2576372715 * value)
	{
		___m_AudioFormats_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_AudioFormats_4), value);
	}

	inline static int32_t get_offset_of_U3CDisableCacheU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(TextToSpeech_t3349357562, ___U3CDisableCacheU3Ek__BackingField_8)); }
	inline bool get_U3CDisableCacheU3Ek__BackingField_8() const { return ___U3CDisableCacheU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CDisableCacheU3Ek__BackingField_8() { return &___U3CDisableCacheU3Ek__BackingField_8; }
	inline void set_U3CDisableCacheU3Ek__BackingField_8(bool value)
	{
		___U3CDisableCacheU3Ek__BackingField_8 = value;
	}
};

struct TextToSpeech_t3349357562_StaticFields
{
public:
	// FullSerializer.fsSerializer IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech::sm_Serializer
	fsSerializer_t4193731081 * ___sm_Serializer_6;

public:
	inline static int32_t get_offset_of_sm_Serializer_6() { return static_cast<int32_t>(offsetof(TextToSpeech_t3349357562_StaticFields, ___sm_Serializer_6)); }
	inline fsSerializer_t4193731081 * get_sm_Serializer_6() const { return ___sm_Serializer_6; }
	inline fsSerializer_t4193731081 ** get_address_of_sm_Serializer_6() { return &___sm_Serializer_6; }
	inline void set_sm_Serializer_6(fsSerializer_t4193731081 * value)
	{
		___sm_Serializer_6 = value;
		Il2CppCodeGenWriteBarrier((&___sm_Serializer_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTTOSPEECH_T3349357562_H
#ifndef MULTICASTDELEGATE_T3201952435_H
#define MULTICASTDELEGATE_T3201952435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t3201952435  : public Delegate_t3022476291
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t3201952435 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t3201952435 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___prev_9)); }
	inline MulticastDelegate_t3201952435 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t3201952435 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t3201952435 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___kpm_next_10)); }
	inline MulticastDelegate_t3201952435 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t3201952435 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t3201952435 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T3201952435_H
#ifndef DATETIME_T693205669_H
#define DATETIME_T693205669_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t693205669 
{
public:
	// System.TimeSpan System.DateTime::ticks
	TimeSpan_t3430258949  ___ticks_10;
	// System.DateTimeKind System.DateTime::kind
	int32_t ___kind_11;

public:
	inline static int32_t get_offset_of_ticks_10() { return static_cast<int32_t>(offsetof(DateTime_t693205669, ___ticks_10)); }
	inline TimeSpan_t3430258949  get_ticks_10() const { return ___ticks_10; }
	inline TimeSpan_t3430258949 * get_address_of_ticks_10() { return &___ticks_10; }
	inline void set_ticks_10(TimeSpan_t3430258949  value)
	{
		___ticks_10 = value;
	}

	inline static int32_t get_offset_of_kind_11() { return static_cast<int32_t>(offsetof(DateTime_t693205669, ___kind_11)); }
	inline int32_t get_kind_11() const { return ___kind_11; }
	inline int32_t* get_address_of_kind_11() { return &___kind_11; }
	inline void set_kind_11(int32_t value)
	{
		___kind_11 = value;
	}
};

struct DateTime_t693205669_StaticFields
{
public:
	// System.DateTime System.DateTime::MaxValue
	DateTime_t693205669  ___MaxValue_12;
	// System.DateTime System.DateTime::MinValue
	DateTime_t693205669  ___MinValue_13;
	// System.String[] System.DateTime::ParseTimeFormats
	StringU5BU5D_t1642385972* ___ParseTimeFormats_14;
	// System.String[] System.DateTime::ParseYearDayMonthFormats
	StringU5BU5D_t1642385972* ___ParseYearDayMonthFormats_15;
	// System.String[] System.DateTime::ParseYearMonthDayFormats
	StringU5BU5D_t1642385972* ___ParseYearMonthDayFormats_16;
	// System.String[] System.DateTime::ParseDayMonthYearFormats
	StringU5BU5D_t1642385972* ___ParseDayMonthYearFormats_17;
	// System.String[] System.DateTime::ParseMonthDayYearFormats
	StringU5BU5D_t1642385972* ___ParseMonthDayYearFormats_18;
	// System.String[] System.DateTime::MonthDayShortFormats
	StringU5BU5D_t1642385972* ___MonthDayShortFormats_19;
	// System.String[] System.DateTime::DayMonthShortFormats
	StringU5BU5D_t1642385972* ___DayMonthShortFormats_20;
	// System.Int32[] System.DateTime::daysmonth
	Int32U5BU5D_t3030399641* ___daysmonth_21;
	// System.Int32[] System.DateTime::daysmonthleap
	Int32U5BU5D_t3030399641* ___daysmonthleap_22;
	// System.Object System.DateTime::to_local_time_span_object
	RuntimeObject * ___to_local_time_span_object_23;
	// System.Int64 System.DateTime::last_now
	int64_t ___last_now_24;

public:
	inline static int32_t get_offset_of_MaxValue_12() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___MaxValue_12)); }
	inline DateTime_t693205669  get_MaxValue_12() const { return ___MaxValue_12; }
	inline DateTime_t693205669 * get_address_of_MaxValue_12() { return &___MaxValue_12; }
	inline void set_MaxValue_12(DateTime_t693205669  value)
	{
		___MaxValue_12 = value;
	}

	inline static int32_t get_offset_of_MinValue_13() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___MinValue_13)); }
	inline DateTime_t693205669  get_MinValue_13() const { return ___MinValue_13; }
	inline DateTime_t693205669 * get_address_of_MinValue_13() { return &___MinValue_13; }
	inline void set_MinValue_13(DateTime_t693205669  value)
	{
		___MinValue_13 = value;
	}

	inline static int32_t get_offset_of_ParseTimeFormats_14() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseTimeFormats_14)); }
	inline StringU5BU5D_t1642385972* get_ParseTimeFormats_14() const { return ___ParseTimeFormats_14; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseTimeFormats_14() { return &___ParseTimeFormats_14; }
	inline void set_ParseTimeFormats_14(StringU5BU5D_t1642385972* value)
	{
		___ParseTimeFormats_14 = value;
		Il2CppCodeGenWriteBarrier((&___ParseTimeFormats_14), value);
	}

	inline static int32_t get_offset_of_ParseYearDayMonthFormats_15() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseYearDayMonthFormats_15)); }
	inline StringU5BU5D_t1642385972* get_ParseYearDayMonthFormats_15() const { return ___ParseYearDayMonthFormats_15; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseYearDayMonthFormats_15() { return &___ParseYearDayMonthFormats_15; }
	inline void set_ParseYearDayMonthFormats_15(StringU5BU5D_t1642385972* value)
	{
		___ParseYearDayMonthFormats_15 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearDayMonthFormats_15), value);
	}

	inline static int32_t get_offset_of_ParseYearMonthDayFormats_16() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseYearMonthDayFormats_16)); }
	inline StringU5BU5D_t1642385972* get_ParseYearMonthDayFormats_16() const { return ___ParseYearMonthDayFormats_16; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseYearMonthDayFormats_16() { return &___ParseYearMonthDayFormats_16; }
	inline void set_ParseYearMonthDayFormats_16(StringU5BU5D_t1642385972* value)
	{
		___ParseYearMonthDayFormats_16 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearMonthDayFormats_16), value);
	}

	inline static int32_t get_offset_of_ParseDayMonthYearFormats_17() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseDayMonthYearFormats_17)); }
	inline StringU5BU5D_t1642385972* get_ParseDayMonthYearFormats_17() const { return ___ParseDayMonthYearFormats_17; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseDayMonthYearFormats_17() { return &___ParseDayMonthYearFormats_17; }
	inline void set_ParseDayMonthYearFormats_17(StringU5BU5D_t1642385972* value)
	{
		___ParseDayMonthYearFormats_17 = value;
		Il2CppCodeGenWriteBarrier((&___ParseDayMonthYearFormats_17), value);
	}

	inline static int32_t get_offset_of_ParseMonthDayYearFormats_18() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseMonthDayYearFormats_18)); }
	inline StringU5BU5D_t1642385972* get_ParseMonthDayYearFormats_18() const { return ___ParseMonthDayYearFormats_18; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseMonthDayYearFormats_18() { return &___ParseMonthDayYearFormats_18; }
	inline void set_ParseMonthDayYearFormats_18(StringU5BU5D_t1642385972* value)
	{
		___ParseMonthDayYearFormats_18 = value;
		Il2CppCodeGenWriteBarrier((&___ParseMonthDayYearFormats_18), value);
	}

	inline static int32_t get_offset_of_MonthDayShortFormats_19() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___MonthDayShortFormats_19)); }
	inline StringU5BU5D_t1642385972* get_MonthDayShortFormats_19() const { return ___MonthDayShortFormats_19; }
	inline StringU5BU5D_t1642385972** get_address_of_MonthDayShortFormats_19() { return &___MonthDayShortFormats_19; }
	inline void set_MonthDayShortFormats_19(StringU5BU5D_t1642385972* value)
	{
		___MonthDayShortFormats_19 = value;
		Il2CppCodeGenWriteBarrier((&___MonthDayShortFormats_19), value);
	}

	inline static int32_t get_offset_of_DayMonthShortFormats_20() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___DayMonthShortFormats_20)); }
	inline StringU5BU5D_t1642385972* get_DayMonthShortFormats_20() const { return ___DayMonthShortFormats_20; }
	inline StringU5BU5D_t1642385972** get_address_of_DayMonthShortFormats_20() { return &___DayMonthShortFormats_20; }
	inline void set_DayMonthShortFormats_20(StringU5BU5D_t1642385972* value)
	{
		___DayMonthShortFormats_20 = value;
		Il2CppCodeGenWriteBarrier((&___DayMonthShortFormats_20), value);
	}

	inline static int32_t get_offset_of_daysmonth_21() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___daysmonth_21)); }
	inline Int32U5BU5D_t3030399641* get_daysmonth_21() const { return ___daysmonth_21; }
	inline Int32U5BU5D_t3030399641** get_address_of_daysmonth_21() { return &___daysmonth_21; }
	inline void set_daysmonth_21(Int32U5BU5D_t3030399641* value)
	{
		___daysmonth_21 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonth_21), value);
	}

	inline static int32_t get_offset_of_daysmonthleap_22() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___daysmonthleap_22)); }
	inline Int32U5BU5D_t3030399641* get_daysmonthleap_22() const { return ___daysmonthleap_22; }
	inline Int32U5BU5D_t3030399641** get_address_of_daysmonthleap_22() { return &___daysmonthleap_22; }
	inline void set_daysmonthleap_22(Int32U5BU5D_t3030399641* value)
	{
		___daysmonthleap_22 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonthleap_22), value);
	}

	inline static int32_t get_offset_of_to_local_time_span_object_23() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___to_local_time_span_object_23)); }
	inline RuntimeObject * get_to_local_time_span_object_23() const { return ___to_local_time_span_object_23; }
	inline RuntimeObject ** get_address_of_to_local_time_span_object_23() { return &___to_local_time_span_object_23; }
	inline void set_to_local_time_span_object_23(RuntimeObject * value)
	{
		___to_local_time_span_object_23 = value;
		Il2CppCodeGenWriteBarrier((&___to_local_time_span_object_23), value);
	}

	inline static int32_t get_offset_of_last_now_24() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___last_now_24)); }
	inline int64_t get_last_now_24() const { return ___last_now_24; }
	inline int64_t* get_address_of_last_now_24() { return &___last_now_24; }
	inline void set_last_now_24(int64_t value)
	{
		___last_now_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T693205669_H
#ifndef ONDELETECUSTOMCORPUSCALLBACK_T3393304107_H
#define ONDELETECUSTOMCORPUSCALLBACK_T3393304107_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/OnDeleteCustomCorpusCallback
struct  OnDeleteCustomCorpusCallback_t3393304107  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDELETECUSTOMCORPUSCALLBACK_T3393304107_H
#ifndef LOADFILEDELEGATE_T351561590_H
#define LOADFILEDELEGATE_T351561590_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/LoadFileDelegate
struct  LoadFileDelegate_t351561590  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADFILEDELEGATE_T351561590_H
#ifndef GETVOICECALLBACK_T2752546046_H
#define GETVOICECALLBACK_T2752546046_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetVoiceCallback
struct  GetVoiceCallback_t2752546046  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETVOICECALLBACK_T2752546046_H
#ifndef TOSPEECHCALLBACK_T3422748631_H
#define TOSPEECHCALLBACK_T3422748631_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/ToSpeechCallback
struct  ToSpeechCallback_t3422748631  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOSPEECHCALLBACK_T3422748631_H
#ifndef ONDELETERANKER_T2889964448_H
#define ONDELETERANKER_T2889964448_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/OnDeleteRanker
struct  OnDeleteRanker_t2889964448  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDELETERANKER_T2889964448_H
#ifndef ONGETRANKER_T299592683_H
#define ONGETRANKER_T299592683_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/OnGetRanker
struct  OnGetRanker_t299592683  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONGETRANKER_T299592683_H
#ifndef SPEECHTOTEXT_T2713896346_H
#define SPEECHTOTEXT_T2713896346_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText
struct  SpeechToText_t2713896346  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/LoadFileDelegate IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText::<LoadFile>k__BackingField
	LoadFileDelegate_t351561590 * ___U3CLoadFileU3Ek__BackingField_5;
	// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/OnRecognize IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText::m_ListenCallback
	OnRecognize_t3538205802 * ___m_ListenCallback_6;
	// IBM.Watson.DeveloperCloud.Connection.WSConnector IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText::m_ListenSocket
	WSConnector_t2552241039 * ___m_ListenSocket_7;
	// System.Boolean IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText::m_ListenActive
	bool ___m_ListenActive_8;
	// System.Boolean IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText::m_AudioSent
	bool ___m_AudioSent_9;
	// System.Boolean IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText::m_IsListening
	bool ___m_IsListening_10;
	// System.Collections.Generic.Queue`1<IBM.Watson.DeveloperCloud.DataTypes.AudioData> IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText::m_ListenRecordings
	Queue_1_t3714055359 * ___m_ListenRecordings_11;
	// System.Int32 IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText::m_KeepAliveRoutine
	int32_t ___m_KeepAliveRoutine_12;
	// System.DateTime IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText::m_LastKeepAlive
	DateTime_t693205669  ___m_LastKeepAlive_13;
	// System.DateTime IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText::m_LastStartSent
	DateTime_t693205669  ___m_LastStartSent_14;
	// System.String IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText::m_RecognizeModel
	String_t* ___m_RecognizeModel_15;
	// System.Int32 IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText::m_MaxAlternatives
	int32_t ___m_MaxAlternatives_16;
	// System.Boolean IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText::m_Timestamps
	bool ___m_Timestamps_17;
	// System.Boolean IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText::m_WordConfidence
	bool ___m_WordConfidence_18;
	// System.Boolean IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText::m_DetectSilence
	bool ___m_DetectSilence_19;
	// System.Single IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText::m_SilenceThreshold
	float ___m_SilenceThreshold_20;
	// System.Int32 IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText::m_RecordingHZ
	int32_t ___m_RecordingHZ_21;
	// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/ErrorEvent IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText::<OnError>k__BackingField
	ErrorEvent_t3049611749 * ___U3COnErrorU3Ek__BackingField_23;
	// System.Boolean IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText::<EnableContinousRecognition>k__BackingField
	bool ___U3CEnableContinousRecognitionU3Ek__BackingField_24;
	// System.Boolean IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText::<EnableInterimResults>k__BackingField
	bool ___U3CEnableInterimResultsU3Ek__BackingField_25;

public:
	inline static int32_t get_offset_of_U3CLoadFileU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(SpeechToText_t2713896346, ___U3CLoadFileU3Ek__BackingField_5)); }
	inline LoadFileDelegate_t351561590 * get_U3CLoadFileU3Ek__BackingField_5() const { return ___U3CLoadFileU3Ek__BackingField_5; }
	inline LoadFileDelegate_t351561590 ** get_address_of_U3CLoadFileU3Ek__BackingField_5() { return &___U3CLoadFileU3Ek__BackingField_5; }
	inline void set_U3CLoadFileU3Ek__BackingField_5(LoadFileDelegate_t351561590 * value)
	{
		___U3CLoadFileU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLoadFileU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_m_ListenCallback_6() { return static_cast<int32_t>(offsetof(SpeechToText_t2713896346, ___m_ListenCallback_6)); }
	inline OnRecognize_t3538205802 * get_m_ListenCallback_6() const { return ___m_ListenCallback_6; }
	inline OnRecognize_t3538205802 ** get_address_of_m_ListenCallback_6() { return &___m_ListenCallback_6; }
	inline void set_m_ListenCallback_6(OnRecognize_t3538205802 * value)
	{
		___m_ListenCallback_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_ListenCallback_6), value);
	}

	inline static int32_t get_offset_of_m_ListenSocket_7() { return static_cast<int32_t>(offsetof(SpeechToText_t2713896346, ___m_ListenSocket_7)); }
	inline WSConnector_t2552241039 * get_m_ListenSocket_7() const { return ___m_ListenSocket_7; }
	inline WSConnector_t2552241039 ** get_address_of_m_ListenSocket_7() { return &___m_ListenSocket_7; }
	inline void set_m_ListenSocket_7(WSConnector_t2552241039 * value)
	{
		___m_ListenSocket_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_ListenSocket_7), value);
	}

	inline static int32_t get_offset_of_m_ListenActive_8() { return static_cast<int32_t>(offsetof(SpeechToText_t2713896346, ___m_ListenActive_8)); }
	inline bool get_m_ListenActive_8() const { return ___m_ListenActive_8; }
	inline bool* get_address_of_m_ListenActive_8() { return &___m_ListenActive_8; }
	inline void set_m_ListenActive_8(bool value)
	{
		___m_ListenActive_8 = value;
	}

	inline static int32_t get_offset_of_m_AudioSent_9() { return static_cast<int32_t>(offsetof(SpeechToText_t2713896346, ___m_AudioSent_9)); }
	inline bool get_m_AudioSent_9() const { return ___m_AudioSent_9; }
	inline bool* get_address_of_m_AudioSent_9() { return &___m_AudioSent_9; }
	inline void set_m_AudioSent_9(bool value)
	{
		___m_AudioSent_9 = value;
	}

	inline static int32_t get_offset_of_m_IsListening_10() { return static_cast<int32_t>(offsetof(SpeechToText_t2713896346, ___m_IsListening_10)); }
	inline bool get_m_IsListening_10() const { return ___m_IsListening_10; }
	inline bool* get_address_of_m_IsListening_10() { return &___m_IsListening_10; }
	inline void set_m_IsListening_10(bool value)
	{
		___m_IsListening_10 = value;
	}

	inline static int32_t get_offset_of_m_ListenRecordings_11() { return static_cast<int32_t>(offsetof(SpeechToText_t2713896346, ___m_ListenRecordings_11)); }
	inline Queue_1_t3714055359 * get_m_ListenRecordings_11() const { return ___m_ListenRecordings_11; }
	inline Queue_1_t3714055359 ** get_address_of_m_ListenRecordings_11() { return &___m_ListenRecordings_11; }
	inline void set_m_ListenRecordings_11(Queue_1_t3714055359 * value)
	{
		___m_ListenRecordings_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_ListenRecordings_11), value);
	}

	inline static int32_t get_offset_of_m_KeepAliveRoutine_12() { return static_cast<int32_t>(offsetof(SpeechToText_t2713896346, ___m_KeepAliveRoutine_12)); }
	inline int32_t get_m_KeepAliveRoutine_12() const { return ___m_KeepAliveRoutine_12; }
	inline int32_t* get_address_of_m_KeepAliveRoutine_12() { return &___m_KeepAliveRoutine_12; }
	inline void set_m_KeepAliveRoutine_12(int32_t value)
	{
		___m_KeepAliveRoutine_12 = value;
	}

	inline static int32_t get_offset_of_m_LastKeepAlive_13() { return static_cast<int32_t>(offsetof(SpeechToText_t2713896346, ___m_LastKeepAlive_13)); }
	inline DateTime_t693205669  get_m_LastKeepAlive_13() const { return ___m_LastKeepAlive_13; }
	inline DateTime_t693205669 * get_address_of_m_LastKeepAlive_13() { return &___m_LastKeepAlive_13; }
	inline void set_m_LastKeepAlive_13(DateTime_t693205669  value)
	{
		___m_LastKeepAlive_13 = value;
	}

	inline static int32_t get_offset_of_m_LastStartSent_14() { return static_cast<int32_t>(offsetof(SpeechToText_t2713896346, ___m_LastStartSent_14)); }
	inline DateTime_t693205669  get_m_LastStartSent_14() const { return ___m_LastStartSent_14; }
	inline DateTime_t693205669 * get_address_of_m_LastStartSent_14() { return &___m_LastStartSent_14; }
	inline void set_m_LastStartSent_14(DateTime_t693205669  value)
	{
		___m_LastStartSent_14 = value;
	}

	inline static int32_t get_offset_of_m_RecognizeModel_15() { return static_cast<int32_t>(offsetof(SpeechToText_t2713896346, ___m_RecognizeModel_15)); }
	inline String_t* get_m_RecognizeModel_15() const { return ___m_RecognizeModel_15; }
	inline String_t** get_address_of_m_RecognizeModel_15() { return &___m_RecognizeModel_15; }
	inline void set_m_RecognizeModel_15(String_t* value)
	{
		___m_RecognizeModel_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_RecognizeModel_15), value);
	}

	inline static int32_t get_offset_of_m_MaxAlternatives_16() { return static_cast<int32_t>(offsetof(SpeechToText_t2713896346, ___m_MaxAlternatives_16)); }
	inline int32_t get_m_MaxAlternatives_16() const { return ___m_MaxAlternatives_16; }
	inline int32_t* get_address_of_m_MaxAlternatives_16() { return &___m_MaxAlternatives_16; }
	inline void set_m_MaxAlternatives_16(int32_t value)
	{
		___m_MaxAlternatives_16 = value;
	}

	inline static int32_t get_offset_of_m_Timestamps_17() { return static_cast<int32_t>(offsetof(SpeechToText_t2713896346, ___m_Timestamps_17)); }
	inline bool get_m_Timestamps_17() const { return ___m_Timestamps_17; }
	inline bool* get_address_of_m_Timestamps_17() { return &___m_Timestamps_17; }
	inline void set_m_Timestamps_17(bool value)
	{
		___m_Timestamps_17 = value;
	}

	inline static int32_t get_offset_of_m_WordConfidence_18() { return static_cast<int32_t>(offsetof(SpeechToText_t2713896346, ___m_WordConfidence_18)); }
	inline bool get_m_WordConfidence_18() const { return ___m_WordConfidence_18; }
	inline bool* get_address_of_m_WordConfidence_18() { return &___m_WordConfidence_18; }
	inline void set_m_WordConfidence_18(bool value)
	{
		___m_WordConfidence_18 = value;
	}

	inline static int32_t get_offset_of_m_DetectSilence_19() { return static_cast<int32_t>(offsetof(SpeechToText_t2713896346, ___m_DetectSilence_19)); }
	inline bool get_m_DetectSilence_19() const { return ___m_DetectSilence_19; }
	inline bool* get_address_of_m_DetectSilence_19() { return &___m_DetectSilence_19; }
	inline void set_m_DetectSilence_19(bool value)
	{
		___m_DetectSilence_19 = value;
	}

	inline static int32_t get_offset_of_m_SilenceThreshold_20() { return static_cast<int32_t>(offsetof(SpeechToText_t2713896346, ___m_SilenceThreshold_20)); }
	inline float get_m_SilenceThreshold_20() const { return ___m_SilenceThreshold_20; }
	inline float* get_address_of_m_SilenceThreshold_20() { return &___m_SilenceThreshold_20; }
	inline void set_m_SilenceThreshold_20(float value)
	{
		___m_SilenceThreshold_20 = value;
	}

	inline static int32_t get_offset_of_m_RecordingHZ_21() { return static_cast<int32_t>(offsetof(SpeechToText_t2713896346, ___m_RecordingHZ_21)); }
	inline int32_t get_m_RecordingHZ_21() const { return ___m_RecordingHZ_21; }
	inline int32_t* get_address_of_m_RecordingHZ_21() { return &___m_RecordingHZ_21; }
	inline void set_m_RecordingHZ_21(int32_t value)
	{
		___m_RecordingHZ_21 = value;
	}

	inline static int32_t get_offset_of_U3COnErrorU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(SpeechToText_t2713896346, ___U3COnErrorU3Ek__BackingField_23)); }
	inline ErrorEvent_t3049611749 * get_U3COnErrorU3Ek__BackingField_23() const { return ___U3COnErrorU3Ek__BackingField_23; }
	inline ErrorEvent_t3049611749 ** get_address_of_U3COnErrorU3Ek__BackingField_23() { return &___U3COnErrorU3Ek__BackingField_23; }
	inline void set_U3COnErrorU3Ek__BackingField_23(ErrorEvent_t3049611749 * value)
	{
		___U3COnErrorU3Ek__BackingField_23 = value;
		Il2CppCodeGenWriteBarrier((&___U3COnErrorU3Ek__BackingField_23), value);
	}

	inline static int32_t get_offset_of_U3CEnableContinousRecognitionU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(SpeechToText_t2713896346, ___U3CEnableContinousRecognitionU3Ek__BackingField_24)); }
	inline bool get_U3CEnableContinousRecognitionU3Ek__BackingField_24() const { return ___U3CEnableContinousRecognitionU3Ek__BackingField_24; }
	inline bool* get_address_of_U3CEnableContinousRecognitionU3Ek__BackingField_24() { return &___U3CEnableContinousRecognitionU3Ek__BackingField_24; }
	inline void set_U3CEnableContinousRecognitionU3Ek__BackingField_24(bool value)
	{
		___U3CEnableContinousRecognitionU3Ek__BackingField_24 = value;
	}

	inline static int32_t get_offset_of_U3CEnableInterimResultsU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(SpeechToText_t2713896346, ___U3CEnableInterimResultsU3Ek__BackingField_25)); }
	inline bool get_U3CEnableInterimResultsU3Ek__BackingField_25() const { return ___U3CEnableInterimResultsU3Ek__BackingField_25; }
	inline bool* get_address_of_U3CEnableInterimResultsU3Ek__BackingField_25() { return &___U3CEnableInterimResultsU3Ek__BackingField_25; }
	inline void set_U3CEnableInterimResultsU3Ek__BackingField_25(bool value)
	{
		___U3CEnableInterimResultsU3Ek__BackingField_25 = value;
	}
};

struct SpeechToText_t2713896346_StaticFields
{
public:
	// FullSerializer.fsSerializer IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText::sm_Serializer
	fsSerializer_t4193731081 * ___sm_Serializer_22;

public:
	inline static int32_t get_offset_of_sm_Serializer_22() { return static_cast<int32_t>(offsetof(SpeechToText_t2713896346_StaticFields, ___sm_Serializer_22)); }
	inline fsSerializer_t4193731081 * get_sm_Serializer_22() const { return ___sm_Serializer_22; }
	inline fsSerializer_t4193731081 ** get_address_of_sm_Serializer_22() { return &___sm_Serializer_22; }
	inline void set_sm_Serializer_22(fsSerializer_t4193731081 * value)
	{
		___sm_Serializer_22 = value;
		Il2CppCodeGenWriteBarrier((&___sm_Serializer_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPEECHTOTEXT_T2713896346_H
#ifndef ONRANK_T4002112472_H
#define ONRANK_T4002112472_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/OnRank
struct  OnRank_t4002112472  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONRANK_T4002112472_H
#ifndef ERROREVENT_T3049611749_H
#define ERROREVENT_T3049611749_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/ErrorEvent
struct  ErrorEvent_t3049611749  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERROREVENT_T3049611749_H
#ifndef ONGETMODELS_T1162909112_H
#define ONGETMODELS_T1162909112_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/OnGetModels
struct  OnGetModels_t1162909112  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONGETMODELS_T1162909112_H
#ifndef ONDELETECUSTOMWORDCALLBACK_T2228320037_H
#define ONDELETECUSTOMWORDCALLBACK_T2228320037_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/OnDeleteCustomWordCallback
struct  OnDeleteCustomWordCallback_t2228320037  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDELETECUSTOMWORDCALLBACK_T2228320037_H
#ifndef UPGRADECUSTOMIZATIONCALLBACK_T567321637_H
#define UPGRADECUSTOMIZATIONCALLBACK_T567321637_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/UpgradeCustomizationCallback
struct  UpgradeCustomizationCallback_t567321637  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPGRADECUSTOMIZATIONCALLBACK_T567321637_H
#ifndef RESETCUSTOMIZATIONCALLBACK_T1922530872_H
#define RESETCUSTOMIZATIONCALLBACK_T1922530872_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/ResetCustomizationCallback
struct  ResetCustomizationCallback_t1922530872  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESETCUSTOMIZATIONCALLBACK_T1922530872_H
#ifndef GETCUSTOMWORDCALLBACK_T4225912831_H
#define GETCUSTOMWORDCALLBACK_T4225912831_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/GetCustomWordCallback
struct  GetCustomWordCallback_t4225912831  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETCUSTOMWORDCALLBACK_T4225912831_H
#ifndef GETCUSTOMCORPORACALLBACK_T1452492251_H
#define GETCUSTOMCORPORACALLBACK_T1452492251_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/GetCustomCorporaCallback
struct  GetCustomCorporaCallback_t1452492251  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETCUSTOMCORPORACALLBACK_T1452492251_H
#ifndef ONADDCUSTOMCORPUSCALLBACK_T1567930907_H
#define ONADDCUSTOMCORPUSCALLBACK_T1567930907_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/OnAddCustomCorpusCallback
struct  OnAddCustomCorpusCallback_t1567930907  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONADDCUSTOMCORPUSCALLBACK_T1567930907_H
#ifndef ADDCUSTOMWORDSCALLBACK_T4067999203_H
#define ADDCUSTOMWORDSCALLBACK_T4067999203_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/AddCustomWordsCallback
struct  AddCustomWordsCallback_t4067999203  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDCUSTOMWORDSCALLBACK_T4067999203_H
#ifndef GETCUSTOMWORDSCALLBACK_T3048138280_H
#define GETCUSTOMWORDSCALLBACK_T3048138280_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/GetCustomWordsCallback
struct  GetCustomWordsCallback_t3048138280  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETCUSTOMWORDSCALLBACK_T3048138280_H
#ifndef ONRECOGNIZE_T3538205802_H
#define ONRECOGNIZE_T3538205802_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/OnRecognize
struct  OnRecognize_t3538205802  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONRECOGNIZE_T3538205802_H
#ifndef GETCUSTOMIZATIONSCALLBACK_T2433078120_H
#define GETCUSTOMIZATIONSCALLBACK_T2433078120_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/GetCustomizationsCallback
struct  GetCustomizationsCallback_t2433078120  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETCUSTOMIZATIONSCALLBACK_T2433078120_H
#ifndef GETVOICESCALLBACK_T3012885511_H
#define GETVOICESCALLBACK_T3012885511_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetVoicesCallback
struct  GetVoicesCallback_t3012885511  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETVOICESCALLBACK_T3012885511_H
#ifndef ONGETMODEL_T3671144581_H
#define ONGETMODEL_T3671144581_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/OnGetModel
struct  OnGetModel_t3671144581  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONGETMODEL_T3671144581_H
#ifndef GETCUSTOMIZATIONCALLBACK_T267872143_H
#define GETCUSTOMIZATIONCALLBACK_T267872143_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/GetCustomizationCallback
struct  GetCustomizationCallback_t267872143  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETCUSTOMIZATIONCALLBACK_T267872143_H
#ifndef TRAINCUSTOMIZATIONCALLBACK_T2705176209_H
#define TRAINCUSTOMIZATIONCALLBACK_T2705176209_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/TrainCustomizationCallback
struct  TrainCustomizationCallback_t2705176209  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRAINCUSTOMIZATIONCALLBACK_T2705176209_H
#ifndef CREATECUSTOMIZATIONCALLBACK_T3694377825_H
#define CREATECUSTOMIZATIONCALLBACK_T3694377825_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/CreateCustomizationCallback
struct  CreateCustomizationCallback_t3694377825  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREATECUSTOMIZATIONCALLBACK_T3694377825_H
#ifndef ONDELETECUSTOMIZATIONCALLBACK_T1940302465_H
#define ONDELETECUSTOMIZATIONCALLBACK_T1940302465_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText/OnDeleteCustomizationCallback
struct  OnDeleteCustomizationCallback_t1940302465  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDELETECUSTOMIZATIONCALLBACK_T1940302465_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2700 = { sizeof (CreateRankerRequest_t2304103365), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2700[4] = 
{
	CreateRankerRequest_t2304103365::get_offset_of_U3CDataU3Ek__BackingField_11(),
	CreateRankerRequest_t2304103365::get_offset_of_U3CNameU3Ek__BackingField_12(),
	CreateRankerRequest_t2304103365::get_offset_of_U3CTrainingDataPathU3Ek__BackingField_13(),
	CreateRankerRequest_t2304103365::get_offset_of_U3CCallbackU3Ek__BackingField_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2701 = { sizeof (OnRank_t4002112472), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2702 = { sizeof (RankRequest_t3422279034), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2702[4] = 
{
	RankRequest_t3422279034::get_offset_of_U3CDataU3Ek__BackingField_11(),
	RankRequest_t3422279034::get_offset_of_U3CRankerIDU3Ek__BackingField_12(),
	RankRequest_t3422279034::get_offset_of_U3CSearchResultsPathU3Ek__BackingField_13(),
	RankRequest_t3422279034::get_offset_of_U3CCallbackU3Ek__BackingField_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2703 = { sizeof (OnDeleteRanker_t2889964448), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2704 = { sizeof (DeleteRankerRequest_t1365655110), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2704[3] = 
{
	DeleteRankerRequest_t1365655110::get_offset_of_U3CDataU3Ek__BackingField_11(),
	DeleteRankerRequest_t1365655110::get_offset_of_U3CRankerIDU3Ek__BackingField_12(),
	DeleteRankerRequest_t1365655110::get_offset_of_U3CCallbackU3Ek__BackingField_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2705 = { sizeof (OnGetRanker_t299592683), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2706 = { sizeof (GetRankerRequest_t2534353187), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2706[3] = 
{
	GetRankerRequest_t2534353187::get_offset_of_U3CDataU3Ek__BackingField_11(),
	GetRankerRequest_t2534353187::get_offset_of_U3CRankerIDU3Ek__BackingField_12(),
	GetRankerRequest_t2534353187::get_offset_of_U3CCallbackU3Ek__BackingField_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2707 = { sizeof (CheckServiceStatus_t4192214874), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2707[2] = 
{
	CheckServiceStatus_t4192214874::get_offset_of_m_Service_0(),
	CheckServiceStatus_t4192214874::get_offset_of_m_Callback_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2708 = { sizeof (ModelSet_t2925843349), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2708[1] = 
{
	ModelSet_t2925843349::get_offset_of_U3CmodelsU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2709 = { sizeof (Model_t2340542523), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2709[7] = 
{
	Model_t2340542523::get_offset_of_U3CnameU3Ek__BackingField_0(),
	Model_t2340542523::get_offset_of_U3ClanguageU3Ek__BackingField_1(),
	Model_t2340542523::get_offset_of_U3CrateU3Ek__BackingField_2(),
	Model_t2340542523::get_offset_of_U3CurlU3Ek__BackingField_3(),
	Model_t2340542523::get_offset_of_U3CsessionsU3Ek__BackingField_4(),
	Model_t2340542523::get_offset_of_U3Csupported_featuresU3Ek__BackingField_5(),
	Model_t2340542523::get_offset_of_U3CdescriptionU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2710 = { sizeof (SupportedFeatures_t1255602913), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2710[1] = 
{
	SupportedFeatures_t1255602913::get_offset_of_U3Ccustom_language_modelU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2711 = { sizeof (Session_t1940512798), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2711[5] = 
{
	Session_t1940512798::get_offset_of_U3Csession_idU3Ek__BackingField_0(),
	Session_t1940512798::get_offset_of_U3Cnew_session_uriU3Ek__BackingField_1(),
	Session_t1940512798::get_offset_of_U3CrecognizeU3Ek__BackingField_2(),
	Session_t1940512798::get_offset_of_U3Cobserve_resultU3Ek__BackingField_3(),
	Session_t1940512798::get_offset_of_U3CrecognizeWSU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2712 = { sizeof (SpeechRecognitionEvent_t1591882439), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2712[3] = 
{
	SpeechRecognitionEvent_t1591882439::get_offset_of_U3CresultsU3Ek__BackingField_0(),
	SpeechRecognitionEvent_t1591882439::get_offset_of_U3Cresult_indexU3Ek__BackingField_1(),
	SpeechRecognitionEvent_t1591882439::get_offset_of_U3CwarningsU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2713 = { sizeof (SpeechRecognitionResult_t3827468010), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2713[4] = 
{
	SpeechRecognitionResult_t3827468010::get_offset_of_U3CfinalU3Ek__BackingField_0(),
	SpeechRecognitionResult_t3827468010::get_offset_of_U3CalternativesU3Ek__BackingField_1(),
	SpeechRecognitionResult_t3827468010::get_offset_of_U3Ckeywords_resultU3Ek__BackingField_2(),
	SpeechRecognitionResult_t3827468010::get_offset_of_U3Cword_alternativesU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2714 = { sizeof (SpeechRecognitionAlternative_t84095082), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2714[6] = 
{
	SpeechRecognitionAlternative_t84095082::get_offset_of_U3CtranscriptU3Ek__BackingField_0(),
	SpeechRecognitionAlternative_t84095082::get_offset_of_U3CconfidenceU3Ek__BackingField_1(),
	SpeechRecognitionAlternative_t84095082::get_offset_of_U3CtimestampsU3Ek__BackingField_2(),
	SpeechRecognitionAlternative_t84095082::get_offset_of_U3Cword_confidenceU3Ek__BackingField_3(),
	SpeechRecognitionAlternative_t84095082::get_offset_of_U3CTimestampsU3Ek__BackingField_4(),
	SpeechRecognitionAlternative_t84095082::get_offset_of_U3CWordConfidenceU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2715 = { sizeof (KeywordResults_t4114097975), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2715[1] = 
{
	KeywordResults_t4114097975::get_offset_of_U3CkeywordU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2716 = { sizeof (WordAlternativeResults_t1270965811), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2716[3] = 
{
	WordAlternativeResults_t1270965811::get_offset_of_U3Cstart_timeU3Ek__BackingField_0(),
	WordAlternativeResults_t1270965811::get_offset_of_U3Cend_timeU3Ek__BackingField_1(),
	WordAlternativeResults_t1270965811::get_offset_of_U3CalternativesU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2717 = { sizeof (KeywordResult_t2819026148), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2717[4] = 
{
	KeywordResult_t2819026148::get_offset_of_U3Cnormalized_textU3Ek__BackingField_0(),
	KeywordResult_t2819026148::get_offset_of_U3Cstart_timeU3Ek__BackingField_1(),
	KeywordResult_t2819026148::get_offset_of_U3Cend_timeU3Ek__BackingField_2(),
	KeywordResult_t2819026148::get_offset_of_U3CconfidenceU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2718 = { sizeof (WordAlternativeResult_t3127007908), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2718[2] = 
{
	WordAlternativeResult_t3127007908::get_offset_of_U3CconfidenceU3Ek__BackingField_0(),
	WordAlternativeResult_t3127007908::get_offset_of_U3CwordU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2719 = { sizeof (RecognizeStatus_t1596590804), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2719[1] = 
{
	RecognizeStatus_t1596590804::get_offset_of_U3CsessionU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2720 = { sizeof (SessionStatus_t3800674532), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2720[5] = 
{
	SessionStatus_t3800674532::get_offset_of_U3CstateU3Ek__BackingField_0(),
	SessionStatus_t3800674532::get_offset_of_U3CmodelU3Ek__BackingField_1(),
	SessionStatus_t3800674532::get_offset_of_U3CrecognizeU3Ek__BackingField_2(),
	SessionStatus_t3800674532::get_offset_of_U3Cobserve_resultU3Ek__BackingField_3(),
	SessionStatus_t3800674532::get_offset_of_U3CrecognizeWSU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2721 = { sizeof (WordConfidence_t2757029394), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2721[2] = 
{
	WordConfidence_t2757029394::get_offset_of_U3CWordU3Ek__BackingField_0(),
	WordConfidence_t2757029394::get_offset_of_U3CConfidenceU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2722 = { sizeof (TimeStamp_t82276870), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2722[3] = 
{
	TimeStamp_t82276870::get_offset_of_U3CWordU3Ek__BackingField_0(),
	TimeStamp_t82276870::get_offset_of_U3CStartU3Ek__BackingField_1(),
	TimeStamp_t82276870::get_offset_of_U3CEndU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2723 = { sizeof (RegisterStatus_t3750858893), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2723[2] = 
{
	RegisterStatus_t3750858893::get_offset_of_U3CstatusU3Ek__BackingField_0(),
	RegisterStatus_t3750858893::get_offset_of_U3CurlU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2724 = { sizeof (JobsStatusList_t625315452), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2724[1] = 
{
	JobsStatusList_t625315452::get_offset_of_U3CrecognitionsU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2725 = { sizeof (JobStatus_t2358384683), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2725[5] = 
{
	JobStatus_t2358384683::get_offset_of_U3CidU3Ek__BackingField_0(),
	JobStatus_t2358384683::get_offset_of_U3CcreatedU3Ek__BackingField_1(),
	JobStatus_t2358384683::get_offset_of_U3CupdatedU3Ek__BackingField_2(),
	JobStatus_t2358384683::get_offset_of_U3CstatusU3Ek__BackingField_3(),
	JobStatus_t2358384683::get_offset_of_U3Cuser_tokenU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2726 = { sizeof (JobStatusNew_t1469635187), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2726[4] = 
{
	JobStatusNew_t1469635187::get_offset_of_U3CcreatedU3Ek__BackingField_0(),
	JobStatusNew_t1469635187::get_offset_of_U3CidU3Ek__BackingField_1(),
	JobStatusNew_t1469635187::get_offset_of_U3CurlU3Ek__BackingField_2(),
	JobStatusNew_t1469635187::get_offset_of_U3CstatusU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2727 = { sizeof (Customizations_t4082930968), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2727[1] = 
{
	Customizations_t4082930968::get_offset_of_U3CcustomizationsU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2728 = { sizeof (Customization_t47167237), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2728[10] = 
{
	Customization_t47167237::get_offset_of_U3Ccustomization_idU3Ek__BackingField_0(),
	Customization_t47167237::get_offset_of_U3CcreatedU3Ek__BackingField_1(),
	Customization_t47167237::get_offset_of_U3ClanguageU3Ek__BackingField_2(),
	Customization_t47167237::get_offset_of_U3CownerU3Ek__BackingField_3(),
	Customization_t47167237::get_offset_of_U3CnameU3Ek__BackingField_4(),
	Customization_t47167237::get_offset_of_U3CdescriptionU3Ek__BackingField_5(),
	Customization_t47167237::get_offset_of_U3Cbase_model_nameU3Ek__BackingField_6(),
	Customization_t47167237::get_offset_of_U3CstatusU3Ek__BackingField_7(),
	Customization_t47167237::get_offset_of_U3CprogressU3Ek__BackingField_8(),
	Customization_t47167237::get_offset_of_U3CwarningsU3Ek__BackingField_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2729 = { sizeof (CustomizationID_t1324928090), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2729[1] = 
{
	CustomizationID_t1324928090::get_offset_of_U3Ccustomization_idU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2730 = { sizeof (CustomLanguage_t1423423885), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2730[3] = 
{
	CustomLanguage_t1423423885::get_offset_of_U3CnameU3Ek__BackingField_0(),
	CustomLanguage_t1423423885::get_offset_of_U3Cbase_model_nameU3Ek__BackingField_1(),
	CustomLanguage_t1423423885::get_offset_of_U3CdescriptionU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2731 = { sizeof (WordTypeToAdd_t2941578038), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2731[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2732 = { sizeof (Corpora_t3715340602), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2732[1] = 
{
	Corpora_t3715340602::get_offset_of_U3CcorporaU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2733 = { sizeof (Corpus_t3396664578), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2733[5] = 
{
	Corpus_t3396664578::get_offset_of_U3CnameU3Ek__BackingField_0(),
	Corpus_t3396664578::get_offset_of_U3Ctotal_wordsU3Ek__BackingField_1(),
	Corpus_t3396664578::get_offset_of_U3Cout_of_vocabulary_wordsU3Ek__BackingField_2(),
	Corpus_t3396664578::get_offset_of_U3CstatusU3Ek__BackingField_3(),
	Corpus_t3396664578::get_offset_of_U3CerrorU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2734 = { sizeof (WordsList_t29283159), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2734[1] = 
{
	WordsList_t29283159::get_offset_of_U3CwordsU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2735 = { sizeof (WordData_t612265514), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2735[5] = 
{
	WordData_t612265514::get_offset_of_U3CwordU3Ek__BackingField_0(),
	WordData_t612265514::get_offset_of_U3Csounds_likeU3Ek__BackingField_1(),
	WordData_t612265514::get_offset_of_U3Cdisplay_asU3Ek__BackingField_2(),
	WordData_t612265514::get_offset_of_U3CsourceU3Ek__BackingField_3(),
	WordData_t612265514::get_offset_of_U3CerrorU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2736 = { sizeof (WordType_t1970187882), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2736[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2737 = { sizeof (Words_t4062187109), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2737[1] = 
{
	Words_t4062187109::get_offset_of_U3CwordsU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2738 = { sizeof (Word_t3204175642), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2738[3] = 
{
	Word_t3204175642::get_offset_of_U3CwordU3Ek__BackingField_0(),
	Word_t3204175642::get_offset_of_U3Csounds_likeU3Ek__BackingField_1(),
	Word_t3204175642::get_offset_of_U3Cdisplay_asU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2739 = { sizeof (SpeechToText_t2713896346), -1, sizeof(SpeechToText_t2713896346_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2739[26] = 
{
	0,
	0,
	0,
	0,
	0,
	SpeechToText_t2713896346::get_offset_of_U3CLoadFileU3Ek__BackingField_5(),
	SpeechToText_t2713896346::get_offset_of_m_ListenCallback_6(),
	SpeechToText_t2713896346::get_offset_of_m_ListenSocket_7(),
	SpeechToText_t2713896346::get_offset_of_m_ListenActive_8(),
	SpeechToText_t2713896346::get_offset_of_m_AudioSent_9(),
	SpeechToText_t2713896346::get_offset_of_m_IsListening_10(),
	SpeechToText_t2713896346::get_offset_of_m_ListenRecordings_11(),
	SpeechToText_t2713896346::get_offset_of_m_KeepAliveRoutine_12(),
	SpeechToText_t2713896346::get_offset_of_m_LastKeepAlive_13(),
	SpeechToText_t2713896346::get_offset_of_m_LastStartSent_14(),
	SpeechToText_t2713896346::get_offset_of_m_RecognizeModel_15(),
	SpeechToText_t2713896346::get_offset_of_m_MaxAlternatives_16(),
	SpeechToText_t2713896346::get_offset_of_m_Timestamps_17(),
	SpeechToText_t2713896346::get_offset_of_m_WordConfidence_18(),
	SpeechToText_t2713896346::get_offset_of_m_DetectSilence_19(),
	SpeechToText_t2713896346::get_offset_of_m_SilenceThreshold_20(),
	SpeechToText_t2713896346::get_offset_of_m_RecordingHZ_21(),
	SpeechToText_t2713896346_StaticFields::get_offset_of_sm_Serializer_22(),
	SpeechToText_t2713896346::get_offset_of_U3COnErrorU3Ek__BackingField_23(),
	SpeechToText_t2713896346::get_offset_of_U3CEnableContinousRecognitionU3Ek__BackingField_24(),
	SpeechToText_t2713896346::get_offset_of_U3CEnableInterimResultsU3Ek__BackingField_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2740 = { sizeof (ErrorEvent_t3049611749), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2741 = { sizeof (LoadFileDelegate_t351561590), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2742 = { sizeof (OnGetModels_t1162909112), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2743 = { sizeof (GetModelsRequest_t1345746968), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2743[1] = 
{
	GetModelsRequest_t1345746968::get_offset_of_U3CCallbackU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2744 = { sizeof (OnGetModel_t3671144581), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2745 = { sizeof (GetModelRequest_t3143683393), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2745[2] = 
{
	GetModelRequest_t3143683393::get_offset_of_U3CCallbackU3Ek__BackingField_11(),
	GetModelRequest_t3143683393::get_offset_of_U3CModelIDU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2746 = { sizeof (OnRecognize_t3538205802), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2747 = { sizeof (RecognizeRequest_t3152278898), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2747[2] = 
{
	RecognizeRequest_t3152278898::get_offset_of_U3CClipU3Ek__BackingField_11(),
	RecognizeRequest_t3152278898::get_offset_of_U3CCallbackU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2748 = { sizeof (GetCustomizationsCallback_t2433078120), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2749 = { sizeof (GetCustomizationsReq_t2274001667), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2749[3] = 
{
	GetCustomizationsReq_t2274001667::get_offset_of_U3CCallbackU3Ek__BackingField_11(),
	GetCustomizationsReq_t2274001667::get_offset_of_U3CLanguageU3Ek__BackingField_12(),
	GetCustomizationsReq_t2274001667::get_offset_of_U3CDataU3Ek__BackingField_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2750 = { sizeof (CreateCustomizationCallback_t3694377825), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2751 = { sizeof (CreateCustomizationRequest_t527284345), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2751[3] = 
{
	CreateCustomizationRequest_t527284345::get_offset_of_U3CCallbackU3Ek__BackingField_11(),
	CreateCustomizationRequest_t527284345::get_offset_of_U3CCustomLanguageU3Ek__BackingField_12(),
	CreateCustomizationRequest_t527284345::get_offset_of_U3CDataU3Ek__BackingField_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2752 = { sizeof (OnDeleteCustomizationCallback_t1940302465), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2753 = { sizeof (DeleteCustomizationRequest_t4276045522), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2753[3] = 
{
	DeleteCustomizationRequest_t4276045522::get_offset_of_U3CCallbackU3Ek__BackingField_11(),
	DeleteCustomizationRequest_t4276045522::get_offset_of_U3CCustomizationIDU3Ek__BackingField_12(),
	DeleteCustomizationRequest_t4276045522::get_offset_of_U3CDataU3Ek__BackingField_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2754 = { sizeof (GetCustomizationCallback_t267872143), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2755 = { sizeof (GetCustomizationRequest_t3501630983), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2755[3] = 
{
	GetCustomizationRequest_t3501630983::get_offset_of_U3CCallbackU3Ek__BackingField_11(),
	GetCustomizationRequest_t3501630983::get_offset_of_U3CCustomizationIDU3Ek__BackingField_12(),
	GetCustomizationRequest_t3501630983::get_offset_of_U3CDataU3Ek__BackingField_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2756 = { sizeof (TrainCustomizationCallback_t2705176209), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2757 = { sizeof (TrainCustomizationRequest_t3697413505), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2757[3] = 
{
	TrainCustomizationRequest_t3697413505::get_offset_of_U3CCallbackU3Ek__BackingField_11(),
	TrainCustomizationRequest_t3697413505::get_offset_of_U3CCustomizationIDU3Ek__BackingField_12(),
	TrainCustomizationRequest_t3697413505::get_offset_of_U3CDataU3Ek__BackingField_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2758 = { sizeof (ResetCustomizationCallback_t1922530872), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2759 = { sizeof (ResetCustomizationRequest_t497265392), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2759[3] = 
{
	ResetCustomizationRequest_t497265392::get_offset_of_U3CCallbackU3Ek__BackingField_11(),
	ResetCustomizationRequest_t497265392::get_offset_of_U3CCustomizationIDU3Ek__BackingField_12(),
	ResetCustomizationRequest_t497265392::get_offset_of_U3CDataU3Ek__BackingField_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2760 = { sizeof (UpgradeCustomizationCallback_t567321637), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2761 = { sizeof (UpgradeCustomizationRequest_t2164609845), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2761[3] = 
{
	UpgradeCustomizationRequest_t2164609845::get_offset_of_U3CCallbackU3Ek__BackingField_11(),
	UpgradeCustomizationRequest_t2164609845::get_offset_of_U3CCustomizationIDU3Ek__BackingField_12(),
	UpgradeCustomizationRequest_t2164609845::get_offset_of_U3CDataU3Ek__BackingField_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2762 = { sizeof (GetCustomCorporaCallback_t1452492251), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2763 = { sizeof (GetCustomCorporaReq_t43374614), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2763[3] = 
{
	GetCustomCorporaReq_t43374614::get_offset_of_U3CCallbackU3Ek__BackingField_11(),
	GetCustomCorporaReq_t43374614::get_offset_of_U3CCustomizationIDU3Ek__BackingField_12(),
	GetCustomCorporaReq_t43374614::get_offset_of_U3CDataU3Ek__BackingField_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2764 = { sizeof (OnDeleteCustomCorpusCallback_t3393304107), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2765 = { sizeof (DeleteCustomCorpusRequest_t3178306160), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2765[4] = 
{
	DeleteCustomCorpusRequest_t3178306160::get_offset_of_U3CCallbackU3Ek__BackingField_11(),
	DeleteCustomCorpusRequest_t3178306160::get_offset_of_U3CCustomizationIDU3Ek__BackingField_12(),
	DeleteCustomCorpusRequest_t3178306160::get_offset_of_U3CCorpusNameU3Ek__BackingField_13(),
	DeleteCustomCorpusRequest_t3178306160::get_offset_of_U3CDataU3Ek__BackingField_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2766 = { sizeof (OnAddCustomCorpusCallback_t1567930907), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2767 = { sizeof (AddCustomCorpusRequest_t2449242150), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2767[6] = 
{
	AddCustomCorpusRequest_t2449242150::get_offset_of_U3CCallbackU3Ek__BackingField_11(),
	AddCustomCorpusRequest_t2449242150::get_offset_of_U3CCustomizationIDU3Ek__BackingField_12(),
	AddCustomCorpusRequest_t2449242150::get_offset_of_U3CCorpusNameU3Ek__BackingField_13(),
	AddCustomCorpusRequest_t2449242150::get_offset_of_U3CAllowOverwriteU3Ek__BackingField_14(),
	AddCustomCorpusRequest_t2449242150::get_offset_of_U3CTrainingDataU3Ek__BackingField_15(),
	AddCustomCorpusRequest_t2449242150::get_offset_of_U3CDataU3Ek__BackingField_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2768 = { sizeof (GetCustomWordsCallback_t3048138280), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2769 = { sizeof (GetCustomWordsReq_t1009704523), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2769[4] = 
{
	GetCustomWordsReq_t1009704523::get_offset_of_U3CCallbackU3Ek__BackingField_11(),
	GetCustomWordsReq_t1009704523::get_offset_of_U3CCustomizationIDU3Ek__BackingField_12(),
	GetCustomWordsReq_t1009704523::get_offset_of_U3CWordTypeU3Ek__BackingField_13(),
	GetCustomWordsReq_t1009704523::get_offset_of_U3CDataU3Ek__BackingField_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2770 = { sizeof (AddCustomWordsCallback_t4067999203), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2771 = { sizeof (AddCustomWordsRequest_t1867500931), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2771[4] = 
{
	AddCustomWordsRequest_t1867500931::get_offset_of_U3CCallbackU3Ek__BackingField_11(),
	AddCustomWordsRequest_t1867500931::get_offset_of_U3CCustomizationIDU3Ek__BackingField_12(),
	AddCustomWordsRequest_t1867500931::get_offset_of_U3CWordsJsonU3Ek__BackingField_13(),
	AddCustomWordsRequest_t1867500931::get_offset_of_U3CDataU3Ek__BackingField_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2772 = { sizeof (OnDeleteCustomWordCallback_t2228320037), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2773 = { sizeof (DeleteCustomWordRequest_t4122978004), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2773[4] = 
{
	DeleteCustomWordRequest_t4122978004::get_offset_of_U3CCallbackU3Ek__BackingField_11(),
	DeleteCustomWordRequest_t4122978004::get_offset_of_U3CCustomizationIDU3Ek__BackingField_12(),
	DeleteCustomWordRequest_t4122978004::get_offset_of_U3CWordU3Ek__BackingField_13(),
	DeleteCustomWordRequest_t4122978004::get_offset_of_U3CDataU3Ek__BackingField_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2774 = { sizeof (GetCustomWordCallback_t4225912831), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2775 = { sizeof (GetCustomWordReq_t1487061866), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2775[4] = 
{
	GetCustomWordReq_t1487061866::get_offset_of_U3CCallbackU3Ek__BackingField_11(),
	GetCustomWordReq_t1487061866::get_offset_of_U3CCustomizationIDU3Ek__BackingField_12(),
	GetCustomWordReq_t1487061866::get_offset_of_U3CWordU3Ek__BackingField_13(),
	GetCustomWordReq_t1487061866::get_offset_of_U3CDataU3Ek__BackingField_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2776 = { sizeof (CheckServiceStatus_t790185058), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2776[2] = 
{
	CheckServiceStatus_t790185058::get_offset_of_m_Service_0(),
	CheckServiceStatus_t790185058::get_offset_of_m_Callback_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2777 = { sizeof (U3CKeepAliveU3Ec__Iterator0_t1986357758), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2777[4] = 
{
	U3CKeepAliveU3Ec__Iterator0_t1986357758::get_offset_of_U24this_0(),
	U3CKeepAliveU3Ec__Iterator0_t1986357758::get_offset_of_U24current_1(),
	U3CKeepAliveU3Ec__Iterator0_t1986357758::get_offset_of_U24disposing_2(),
	U3CKeepAliveU3Ec__Iterator0_t1986357758::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2778 = { sizeof (AudioFormatType_t2756784245)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2778[4] = 
{
	AudioFormatType_t2756784245::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2779 = { sizeof (VoiceType_t981025524)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2779[14] = 
{
	VoiceType_t981025524::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2780 = { sizeof (Voice_t3646862260), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2780[6] = 
{
	Voice_t3646862260::get_offset_of_U3CnameU3Ek__BackingField_0(),
	Voice_t3646862260::get_offset_of_U3ClanguageU3Ek__BackingField_1(),
	Voice_t3646862260::get_offset_of_U3CgenderU3Ek__BackingField_2(),
	Voice_t3646862260::get_offset_of_U3CurlU3Ek__BackingField_3(),
	Voice_t3646862260::get_offset_of_U3CdescriptionU3Ek__BackingField_4(),
	Voice_t3646862260::get_offset_of_U3CcustomizableU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2781 = { sizeof (Voices_t4221445733), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2781[1] = 
{
	Voices_t4221445733::get_offset_of_U3CvoicesU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2782 = { sizeof (Pronunciation_t2711344207), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2782[1] = 
{
	Pronunciation_t2711344207::get_offset_of_U3CpronunciationU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2783 = { sizeof (Customizations_t482380184), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2783[1] = 
{
	Customizations_t482380184::get_offset_of_U3CcustomizationsU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2784 = { sizeof (Customization_t2261013253), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2784[7] = 
{
	Customization_t2261013253::get_offset_of_U3Ccustomization_idU3Ek__BackingField_0(),
	Customization_t2261013253::get_offset_of_U3CnameU3Ek__BackingField_1(),
	Customization_t2261013253::get_offset_of_U3ClanguageU3Ek__BackingField_2(),
	Customization_t2261013253::get_offset_of_U3CownerU3Ek__BackingField_3(),
	Customization_t2261013253::get_offset_of_U3CcreatedU3Ek__BackingField_4(),
	Customization_t2261013253::get_offset_of_U3Clast_modifiedU3Ek__BackingField_5(),
	Customization_t2261013253::get_offset_of_U3CdescriptionU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2785 = { sizeof (CustomizationID_t344279642), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2785[1] = 
{
	CustomizationID_t344279642::get_offset_of_U3Ccustomization_idU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2786 = { sizeof (CustomVoice_t330695553), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2786[3] = 
{
	CustomVoice_t330695553::get_offset_of_U3CnameU3Ek__BackingField_0(),
	CustomVoice_t330695553::get_offset_of_U3ClanguageU3Ek__BackingField_1(),
	CustomVoice_t330695553::get_offset_of_U3CdescriptionU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2787 = { sizeof (CustomizationWords_t949522428), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2787[8] = 
{
	CustomizationWords_t949522428::get_offset_of_U3Ccustomization_idU3Ek__BackingField_0(),
	CustomizationWords_t949522428::get_offset_of_U3CnameU3Ek__BackingField_1(),
	CustomizationWords_t949522428::get_offset_of_U3ClanguageU3Ek__BackingField_2(),
	CustomizationWords_t949522428::get_offset_of_U3CownerU3Ek__BackingField_3(),
	CustomizationWords_t949522428::get_offset_of_U3CcreatedU3Ek__BackingField_4(),
	CustomizationWords_t949522428::get_offset_of_U3Clast_modifiedU3Ek__BackingField_5(),
	CustomizationWords_t949522428::get_offset_of_U3CdescriptionU3Ek__BackingField_6(),
	CustomizationWords_t949522428::get_offset_of_U3CwordsU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2788 = { sizeof (Words_t2948214629), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2788[1] = 
{
	Words_t2948214629::get_offset_of_U3CwordsU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2789 = { sizeof (Word_t4274554970), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2789[2] = 
{
	Word_t4274554970::get_offset_of_U3CwordU3Ek__BackingField_0(),
	Word_t4274554970::get_offset_of_U3CtranslationU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2790 = { sizeof (CustomVoiceUpdate_t1706248840), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2790[3] = 
{
	CustomVoiceUpdate_t1706248840::get_offset_of_U3CnameU3Ek__BackingField_0(),
	CustomVoiceUpdate_t1706248840::get_offset_of_U3CdescriptionU3Ek__BackingField_1(),
	CustomVoiceUpdate_t1706248840::get_offset_of_U3CwordsU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2791 = { sizeof (Translation_t2174867539), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2791[1] = 
{
	Translation_t2174867539::get_offset_of_U3CtranslationU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2792 = { sizeof (ErrorModel_t3210737207), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2792[3] = 
{
	ErrorModel_t3210737207::get_offset_of_U3CerrorU3Ek__BackingField_0(),
	ErrorModel_t3210737207::get_offset_of_U3CcodeU3Ek__BackingField_1(),
	ErrorModel_t3210737207::get_offset_of_U3Ccode_descriptionU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2793 = { sizeof (TextToSpeech_t3349357562), -1, sizeof(TextToSpeech_t3349357562_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2793[9] = 
{
	TextToSpeech_t3349357562::get_offset_of_m_SpeechCache_0(),
	TextToSpeech_t3349357562::get_offset_of_m_Voice_1(),
	TextToSpeech_t3349357562::get_offset_of_m_AudioFormat_2(),
	TextToSpeech_t3349357562::get_offset_of_m_VoiceTypes_3(),
	TextToSpeech_t3349357562::get_offset_of_m_AudioFormats_4(),
	0,
	TextToSpeech_t3349357562_StaticFields::get_offset_of_sm_Serializer_6(),
	0,
	TextToSpeech_t3349357562::get_offset_of_U3CDisableCacheU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2794 = { sizeof (GetVoicesCallback_t3012885511), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2795 = { sizeof (GetVoicesReq_t1741653746), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2795[1] = 
{
	GetVoicesReq_t1741653746::get_offset_of_U3CCallbackU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2796 = { sizeof (GetVoiceCallback_t2752546046), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2797 = { sizeof (GetVoiceReq_t1980513795), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2797[1] = 
{
	GetVoiceReq_t1980513795::get_offset_of_U3CCallbackU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2798 = { sizeof (ToSpeechCallback_t3422748631), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2799 = { sizeof (ToSpeechRequest_t3970128895), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2799[3] = 
{
	ToSpeechRequest_t3970128895::get_offset_of_U3CTextIdU3Ek__BackingField_11(),
	ToSpeechRequest_t3970128895::get_offset_of_U3CTextU3Ek__BackingField_12(),
	ToSpeechRequest_t3970128895::get_offset_of_U3CCallbackU3Ek__BackingField_13(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
