﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.Security.Cryptography.SymmetricAlgorithm
struct SymmetricAlgorithm_t1108166522;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t2510243513;
// System.Runtime.Remoting.ServerIdentity
struct ServerIdentity_t1656058977;
// System.Net.Sockets.UdpClient
struct UdpClient_t1278197702;
// System.Net.IPEndPoint
struct IPEndPoint_t2615413766;
// System.String
struct String_t;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;
// System.IntPtr[]
struct IntPtrU5BU5D_t169632028;
// System.Collections.IDictionary
struct IDictionary_t596158605;
// System.Xml.Xsl.IStaticXsltContext
struct IStaticXsltContext_t1565976071;
// System.IO.TextWriter
struct TextWriter_t4027217640;
// Mono.Xml.XPath.yydebug.yyDebug
struct yyDebug_t719813743;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Int16[]
struct Int16U5BU5D_t3104283263;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;
// System.UInt32[]
struct UInt32U5BU5D_t59386216;
// System.Exception
struct Exception_t1927440687;
// OSCsharp.Data.OscTimeTag
struct OscTimeTag_t625345318;
// OSCsharp.Data.OscBundle
struct OscBundle_t1126010605;
// OSCsharp.Data.OscPacket
struct OscPacket_t504761797;
// OSCsharp.Data.OscMessage
struct OscMessage_t2764280154;
// System.IO.MemoryStream
struct MemoryStream_t743994179;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.Void
struct Void_t1841601450;
// System.Collections.ArrayList
struct ArrayList_t4252133567;
// System.Xml.XPath.Expression
struct Expression_t1283317256;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1572802995;
// System.Security.Cryptography.KeySizes[]
struct KeySizesU5BU5D_t1153004758;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.EventHandler`1<OSCsharp.Net.OscPacketReceivedEventArgs>
struct EventHandler_1_t1873352441;
// System.EventHandler`1<OSCsharp.Net.OscBundleReceivedEventArgs>
struct EventHandler_1_t2264531613;
// System.EventHandler`1<OSCsharp.Utils.ExceptionEventArgs>
struct EventHandler_1_t3778988004;
// System.EventHandler`1<OSCsharp.Net.OscMessageReceivedEventArgs>
struct EventHandler_1_t4149316328;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// System.Net.IPAddress
struct IPAddress_t1399971723;
// Mono.Security.Protocol.Tls.Context
struct Context_t4285182719;
// Mono.Security.X509.X509CertificateCollection
struct X509CertificateCollection_t3592472866;
// System.Security.Cryptography.AsymmetricAlgorithm
struct AsymmetricAlgorithm_t784058677;
// System.Security.Cryptography.X509Certificates.X509Certificate
struct X509Certificate_t283079845;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.Security.Cryptography.X509Certificates.X509CertificateCollection
struct X509CertificateCollection_t1197680765;
// Mono.Math.BigInteger
struct BigInteger_t925946153;
// Mono.Security.Protocol.Tls.ValidationResult
struct ValidationResult_t1782558132;




#ifndef U3CMODULEU3E_T3783534219_H
#define U3CMODULEU3E_T3783534219_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t3783534219 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T3783534219_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CMODULEU3E_T3783534218_H
#define U3CMODULEU3E_T3783534218_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t3783534218 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T3783534218_H
#ifndef U3CMODULEU3E_T3783534217_H
#define U3CMODULEU3E_T3783534217_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t3783534217 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T3783534217_H
#ifndef SYMMETRICTRANSFORM_T1394030014_H
#define SYMMETRICTRANSFORM_T1394030014_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Cryptography.SymmetricTransform
struct  SymmetricTransform_t1394030014  : public RuntimeObject
{
public:
	// System.Security.Cryptography.SymmetricAlgorithm Mono.Security.Cryptography.SymmetricTransform::algo
	SymmetricAlgorithm_t1108166522 * ___algo_0;
	// System.Boolean Mono.Security.Cryptography.SymmetricTransform::encrypt
	bool ___encrypt_1;
	// System.Int32 Mono.Security.Cryptography.SymmetricTransform::BlockSizeByte
	int32_t ___BlockSizeByte_2;
	// System.Byte[] Mono.Security.Cryptography.SymmetricTransform::temp
	ByteU5BU5D_t3397334013* ___temp_3;
	// System.Byte[] Mono.Security.Cryptography.SymmetricTransform::temp2
	ByteU5BU5D_t3397334013* ___temp2_4;
	// System.Byte[] Mono.Security.Cryptography.SymmetricTransform::workBuff
	ByteU5BU5D_t3397334013* ___workBuff_5;
	// System.Byte[] Mono.Security.Cryptography.SymmetricTransform::workout
	ByteU5BU5D_t3397334013* ___workout_6;
	// System.Int32 Mono.Security.Cryptography.SymmetricTransform::FeedBackByte
	int32_t ___FeedBackByte_7;
	// System.Int32 Mono.Security.Cryptography.SymmetricTransform::FeedBackIter
	int32_t ___FeedBackIter_8;
	// System.Boolean Mono.Security.Cryptography.SymmetricTransform::m_disposed
	bool ___m_disposed_9;
	// System.Boolean Mono.Security.Cryptography.SymmetricTransform::lastBlock
	bool ___lastBlock_10;
	// System.Security.Cryptography.RandomNumberGenerator Mono.Security.Cryptography.SymmetricTransform::_rng
	RandomNumberGenerator_t2510243513 * ____rng_11;

public:
	inline static int32_t get_offset_of_algo_0() { return static_cast<int32_t>(offsetof(SymmetricTransform_t1394030014, ___algo_0)); }
	inline SymmetricAlgorithm_t1108166522 * get_algo_0() const { return ___algo_0; }
	inline SymmetricAlgorithm_t1108166522 ** get_address_of_algo_0() { return &___algo_0; }
	inline void set_algo_0(SymmetricAlgorithm_t1108166522 * value)
	{
		___algo_0 = value;
		Il2CppCodeGenWriteBarrier((&___algo_0), value);
	}

	inline static int32_t get_offset_of_encrypt_1() { return static_cast<int32_t>(offsetof(SymmetricTransform_t1394030014, ___encrypt_1)); }
	inline bool get_encrypt_1() const { return ___encrypt_1; }
	inline bool* get_address_of_encrypt_1() { return &___encrypt_1; }
	inline void set_encrypt_1(bool value)
	{
		___encrypt_1 = value;
	}

	inline static int32_t get_offset_of_BlockSizeByte_2() { return static_cast<int32_t>(offsetof(SymmetricTransform_t1394030014, ___BlockSizeByte_2)); }
	inline int32_t get_BlockSizeByte_2() const { return ___BlockSizeByte_2; }
	inline int32_t* get_address_of_BlockSizeByte_2() { return &___BlockSizeByte_2; }
	inline void set_BlockSizeByte_2(int32_t value)
	{
		___BlockSizeByte_2 = value;
	}

	inline static int32_t get_offset_of_temp_3() { return static_cast<int32_t>(offsetof(SymmetricTransform_t1394030014, ___temp_3)); }
	inline ByteU5BU5D_t3397334013* get_temp_3() const { return ___temp_3; }
	inline ByteU5BU5D_t3397334013** get_address_of_temp_3() { return &___temp_3; }
	inline void set_temp_3(ByteU5BU5D_t3397334013* value)
	{
		___temp_3 = value;
		Il2CppCodeGenWriteBarrier((&___temp_3), value);
	}

	inline static int32_t get_offset_of_temp2_4() { return static_cast<int32_t>(offsetof(SymmetricTransform_t1394030014, ___temp2_4)); }
	inline ByteU5BU5D_t3397334013* get_temp2_4() const { return ___temp2_4; }
	inline ByteU5BU5D_t3397334013** get_address_of_temp2_4() { return &___temp2_4; }
	inline void set_temp2_4(ByteU5BU5D_t3397334013* value)
	{
		___temp2_4 = value;
		Il2CppCodeGenWriteBarrier((&___temp2_4), value);
	}

	inline static int32_t get_offset_of_workBuff_5() { return static_cast<int32_t>(offsetof(SymmetricTransform_t1394030014, ___workBuff_5)); }
	inline ByteU5BU5D_t3397334013* get_workBuff_5() const { return ___workBuff_5; }
	inline ByteU5BU5D_t3397334013** get_address_of_workBuff_5() { return &___workBuff_5; }
	inline void set_workBuff_5(ByteU5BU5D_t3397334013* value)
	{
		___workBuff_5 = value;
		Il2CppCodeGenWriteBarrier((&___workBuff_5), value);
	}

	inline static int32_t get_offset_of_workout_6() { return static_cast<int32_t>(offsetof(SymmetricTransform_t1394030014, ___workout_6)); }
	inline ByteU5BU5D_t3397334013* get_workout_6() const { return ___workout_6; }
	inline ByteU5BU5D_t3397334013** get_address_of_workout_6() { return &___workout_6; }
	inline void set_workout_6(ByteU5BU5D_t3397334013* value)
	{
		___workout_6 = value;
		Il2CppCodeGenWriteBarrier((&___workout_6), value);
	}

	inline static int32_t get_offset_of_FeedBackByte_7() { return static_cast<int32_t>(offsetof(SymmetricTransform_t1394030014, ___FeedBackByte_7)); }
	inline int32_t get_FeedBackByte_7() const { return ___FeedBackByte_7; }
	inline int32_t* get_address_of_FeedBackByte_7() { return &___FeedBackByte_7; }
	inline void set_FeedBackByte_7(int32_t value)
	{
		___FeedBackByte_7 = value;
	}

	inline static int32_t get_offset_of_FeedBackIter_8() { return static_cast<int32_t>(offsetof(SymmetricTransform_t1394030014, ___FeedBackIter_8)); }
	inline int32_t get_FeedBackIter_8() const { return ___FeedBackIter_8; }
	inline int32_t* get_address_of_FeedBackIter_8() { return &___FeedBackIter_8; }
	inline void set_FeedBackIter_8(int32_t value)
	{
		___FeedBackIter_8 = value;
	}

	inline static int32_t get_offset_of_m_disposed_9() { return static_cast<int32_t>(offsetof(SymmetricTransform_t1394030014, ___m_disposed_9)); }
	inline bool get_m_disposed_9() const { return ___m_disposed_9; }
	inline bool* get_address_of_m_disposed_9() { return &___m_disposed_9; }
	inline void set_m_disposed_9(bool value)
	{
		___m_disposed_9 = value;
	}

	inline static int32_t get_offset_of_lastBlock_10() { return static_cast<int32_t>(offsetof(SymmetricTransform_t1394030014, ___lastBlock_10)); }
	inline bool get_lastBlock_10() const { return ___lastBlock_10; }
	inline bool* get_address_of_lastBlock_10() { return &___lastBlock_10; }
	inline void set_lastBlock_10(bool value)
	{
		___lastBlock_10 = value;
	}

	inline static int32_t get_offset_of__rng_11() { return static_cast<int32_t>(offsetof(SymmetricTransform_t1394030014, ____rng_11)); }
	inline RandomNumberGenerator_t2510243513 * get__rng_11() const { return ____rng_11; }
	inline RandomNumberGenerator_t2510243513 ** get_address_of__rng_11() { return &____rng_11; }
	inline void set__rng_11(RandomNumberGenerator_t2510243513 * value)
	{
		____rng_11 = value;
		Il2CppCodeGenWriteBarrier((&____rng_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYMMETRICTRANSFORM_T1394030014_H
#ifndef MARSHALBYREFOBJECT_T1285298191_H
#define MARSHALBYREFOBJECT_T1285298191_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MarshalByRefObject
struct  MarshalByRefObject_t1285298191  : public RuntimeObject
{
public:
	// System.Runtime.Remoting.ServerIdentity System.MarshalByRefObject::_identity
	ServerIdentity_t1656058977 * ____identity_0;

public:
	inline static int32_t get_offset_of__identity_0() { return static_cast<int32_t>(offsetof(MarshalByRefObject_t1285298191, ____identity_0)); }
	inline ServerIdentity_t1656058977 * get__identity_0() const { return ____identity_0; }
	inline ServerIdentity_t1656058977 ** get_address_of__identity_0() { return &____identity_0; }
	inline void set__identity_0(ServerIdentity_t1656058977 * value)
	{
		____identity_0 = value;
		Il2CppCodeGenWriteBarrier((&____identity_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MARSHALBYREFOBJECT_T1285298191_H
#ifndef UDPSTATE_T1122067727_H
#define UDPSTATE_T1122067727_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OSCsharp.Net.UDPReceiver/UdpState
struct  UdpState_t1122067727  : public RuntimeObject
{
public:
	// System.Net.Sockets.UdpClient OSCsharp.Net.UDPReceiver/UdpState::<Client>k__BackingField
	UdpClient_t1278197702 * ___U3CClientU3Ek__BackingField_0;
	// System.Net.IPEndPoint OSCsharp.Net.UDPReceiver/UdpState::<IPEndPoint>k__BackingField
	IPEndPoint_t2615413766 * ___U3CIPEndPointU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CClientU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(UdpState_t1122067727, ___U3CClientU3Ek__BackingField_0)); }
	inline UdpClient_t1278197702 * get_U3CClientU3Ek__BackingField_0() const { return ___U3CClientU3Ek__BackingField_0; }
	inline UdpClient_t1278197702 ** get_address_of_U3CClientU3Ek__BackingField_0() { return &___U3CClientU3Ek__BackingField_0; }
	inline void set_U3CClientU3Ek__BackingField_0(UdpClient_t1278197702 * value)
	{
		___U3CClientU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CClientU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CIPEndPointU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(UdpState_t1122067727, ___U3CIPEndPointU3Ek__BackingField_1)); }
	inline IPEndPoint_t2615413766 * get_U3CIPEndPointU3Ek__BackingField_1() const { return ___U3CIPEndPointU3Ek__BackingField_1; }
	inline IPEndPoint_t2615413766 ** get_address_of_U3CIPEndPointU3Ek__BackingField_1() { return &___U3CIPEndPointU3Ek__BackingField_1; }
	inline void set_U3CIPEndPointU3Ek__BackingField_1(IPEndPoint_t2615413766 * value)
	{
		___U3CIPEndPointU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CIPEndPointU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UDPSTATE_T1122067727_H
#ifndef LOCALE_T4255929017_H
#define LOCALE_T4255929017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Locale
struct  Locale_t4255929017  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCALE_T4255929017_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef OSCPACKET_T504761797_H
#define OSCPACKET_T504761797_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OSCsharp.Data.OscPacket
struct  OscPacket_t504761797  : public RuntimeObject
{
public:
	// System.String OSCsharp.Data.OscPacket::address
	String_t* ___address_1;
	// System.Collections.Generic.List`1<System.Object> OSCsharp.Data.OscPacket::data
	List_1_t2058570427 * ___data_2;

public:
	inline static int32_t get_offset_of_address_1() { return static_cast<int32_t>(offsetof(OscPacket_t504761797, ___address_1)); }
	inline String_t* get_address_1() const { return ___address_1; }
	inline String_t** get_address_of_address_1() { return &___address_1; }
	inline void set_address_1(String_t* value)
	{
		___address_1 = value;
		Il2CppCodeGenWriteBarrier((&___address_1), value);
	}

	inline static int32_t get_offset_of_data_2() { return static_cast<int32_t>(offsetof(OscPacket_t504761797, ___data_2)); }
	inline List_1_t2058570427 * get_data_2() const { return ___data_2; }
	inline List_1_t2058570427 ** get_address_of_data_2() { return &___data_2; }
	inline void set_data_2(List_1_t2058570427 * value)
	{
		___data_2 = value;
		Il2CppCodeGenWriteBarrier((&___data_2), value);
	}
};

struct OscPacket_t504761797_StaticFields
{
public:
	// System.Boolean OSCsharp.Data.OscPacket::littleEndianByteOrder
	bool ___littleEndianByteOrder_0;

public:
	inline static int32_t get_offset_of_littleEndianByteOrder_0() { return static_cast<int32_t>(offsetof(OscPacket_t504761797_StaticFields, ___littleEndianByteOrder_0)); }
	inline bool get_littleEndianByteOrder_0() const { return ___littleEndianByteOrder_0; }
	inline bool* get_address_of_littleEndianByteOrder_0() { return &___littleEndianByteOrder_0; }
	inline void set_littleEndianByteOrder_0(bool value)
	{
		___littleEndianByteOrder_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OSCPACKET_T504761797_H
#ifndef EVENTARGS_T3289624707_H
#define EVENTARGS_T3289624707_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventArgs
struct  EventArgs_t3289624707  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t3289624707_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t3289624707 * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t3289624707_StaticFields, ___Empty_0)); }
	inline EventArgs_t3289624707 * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t3289624707 ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t3289624707 * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTARGS_T3289624707_H
#ifndef KEYBUILDER_T3965881086_H
#define KEYBUILDER_T3965881086_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Cryptography.KeyBuilder
struct  KeyBuilder_t3965881086  : public RuntimeObject
{
public:

public:
};

struct KeyBuilder_t3965881086_StaticFields
{
public:
	// System.Security.Cryptography.RandomNumberGenerator Mono.Security.Cryptography.KeyBuilder::rng
	RandomNumberGenerator_t2510243513 * ___rng_0;

public:
	inline static int32_t get_offset_of_rng_0() { return static_cast<int32_t>(offsetof(KeyBuilder_t3965881086_StaticFields, ___rng_0)); }
	inline RandomNumberGenerator_t2510243513 * get_rng_0() const { return ___rng_0; }
	inline RandomNumberGenerator_t2510243513 ** get_address_of_rng_0() { return &___rng_0; }
	inline void set_rng_0(RandomNumberGenerator_t2510243513 * value)
	{
		___rng_0 = value;
		Il2CppCodeGenWriteBarrier((&___rng_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYBUILDER_T3965881086_H
#ifndef ENUMERABLE_T2148412300_H
#define ENUMERABLE_T2148412300_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Enumerable
struct  Enumerable_t2148412300  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERABLE_T2148412300_H
#ifndef CHECK_T578192424_H
#define CHECK_T578192424_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Check
struct  Check_t578192424  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHECK_T578192424_H
#ifndef XPATHFUNCTIONS_T976633782_H
#define XPATHFUNCTIONS_T976633782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathFunctions
struct  XPathFunctions_t976633782  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHFUNCTIONS_T976633782_H
#ifndef ATTRIBUTE_T542643598_H
#define ATTRIBUTE_T542643598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t542643598  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T542643598_H
#ifndef STREAM_T3255436806_H
#define STREAM_T3255436806_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream
struct  Stream_t3255436806  : public RuntimeObject
{
public:

public:
};

struct Stream_t3255436806_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_t3255436806 * ___Null_0;

public:
	inline static int32_t get_offset_of_Null_0() { return static_cast<int32_t>(offsetof(Stream_t3255436806_StaticFields, ___Null_0)); }
	inline Stream_t3255436806 * get_Null_0() const { return ___Null_0; }
	inline Stream_t3255436806 ** get_address_of_Null_0() { return &___Null_0; }
	inline void set_Null_0(Stream_t3255436806 * value)
	{
		___Null_0 = value;
		Il2CppCodeGenWriteBarrier((&___Null_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAM_T3255436806_H
#ifndef UTILITY_T2927649758_H
#define UTILITY_T2927649758_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OSCsharp.Utils.Utility
struct  Utility_t2927649758  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UTILITY_T2927649758_H
#ifndef EXPRESSION_T1283317256_H
#define EXPRESSION_T1283317256_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.Expression
struct  Expression_t1283317256  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRESSION_T1283317256_H
#ifndef EXCEPTION_T1927440687_H
#define EXCEPTION_T1927440687_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t1927440687  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t169632028* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t1927440687 * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t169632028* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t169632028** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t169632028* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___inner_exception_1)); }
	inline Exception_t1927440687 * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t1927440687 ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t1927440687 * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T1927440687_H
#ifndef XPATHPARSER_T1120482700_H
#define XPATHPARSER_T1120482700_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.XPath.XPathParser
struct  XPathParser_t1120482700  : public RuntimeObject
{
public:
	// System.Xml.Xsl.IStaticXsltContext Mono.Xml.XPath.XPathParser::Context
	RuntimeObject* ___Context_0;
	// System.IO.TextWriter Mono.Xml.XPath.XPathParser::ErrorOutput
	TextWriter_t4027217640 * ___ErrorOutput_1;
	// System.Int32 Mono.Xml.XPath.XPathParser::eof_token
	int32_t ___eof_token_2;
	// Mono.Xml.XPath.yydebug.yyDebug Mono.Xml.XPath.XPathParser::debug
	RuntimeObject* ___debug_3;
	// System.Int32 Mono.Xml.XPath.XPathParser::yyExpectingState
	int32_t ___yyExpectingState_6;
	// System.Int32 Mono.Xml.XPath.XPathParser::yyMax
	int32_t ___yyMax_7;

public:
	inline static int32_t get_offset_of_Context_0() { return static_cast<int32_t>(offsetof(XPathParser_t1120482700, ___Context_0)); }
	inline RuntimeObject* get_Context_0() const { return ___Context_0; }
	inline RuntimeObject** get_address_of_Context_0() { return &___Context_0; }
	inline void set_Context_0(RuntimeObject* value)
	{
		___Context_0 = value;
		Il2CppCodeGenWriteBarrier((&___Context_0), value);
	}

	inline static int32_t get_offset_of_ErrorOutput_1() { return static_cast<int32_t>(offsetof(XPathParser_t1120482700, ___ErrorOutput_1)); }
	inline TextWriter_t4027217640 * get_ErrorOutput_1() const { return ___ErrorOutput_1; }
	inline TextWriter_t4027217640 ** get_address_of_ErrorOutput_1() { return &___ErrorOutput_1; }
	inline void set_ErrorOutput_1(TextWriter_t4027217640 * value)
	{
		___ErrorOutput_1 = value;
		Il2CppCodeGenWriteBarrier((&___ErrorOutput_1), value);
	}

	inline static int32_t get_offset_of_eof_token_2() { return static_cast<int32_t>(offsetof(XPathParser_t1120482700, ___eof_token_2)); }
	inline int32_t get_eof_token_2() const { return ___eof_token_2; }
	inline int32_t* get_address_of_eof_token_2() { return &___eof_token_2; }
	inline void set_eof_token_2(int32_t value)
	{
		___eof_token_2 = value;
	}

	inline static int32_t get_offset_of_debug_3() { return static_cast<int32_t>(offsetof(XPathParser_t1120482700, ___debug_3)); }
	inline RuntimeObject* get_debug_3() const { return ___debug_3; }
	inline RuntimeObject** get_address_of_debug_3() { return &___debug_3; }
	inline void set_debug_3(RuntimeObject* value)
	{
		___debug_3 = value;
		Il2CppCodeGenWriteBarrier((&___debug_3), value);
	}

	inline static int32_t get_offset_of_yyExpectingState_6() { return static_cast<int32_t>(offsetof(XPathParser_t1120482700, ___yyExpectingState_6)); }
	inline int32_t get_yyExpectingState_6() const { return ___yyExpectingState_6; }
	inline int32_t* get_address_of_yyExpectingState_6() { return &___yyExpectingState_6; }
	inline void set_yyExpectingState_6(int32_t value)
	{
		___yyExpectingState_6 = value;
	}

	inline static int32_t get_offset_of_yyMax_7() { return static_cast<int32_t>(offsetof(XPathParser_t1120482700, ___yyMax_7)); }
	inline int32_t get_yyMax_7() const { return ___yyMax_7; }
	inline int32_t* get_address_of_yyMax_7() { return &___yyMax_7; }
	inline void set_yyMax_7(int32_t value)
	{
		___yyMax_7 = value;
	}
};

struct XPathParser_t1120482700_StaticFields
{
public:
	// System.Int32 Mono.Xml.XPath.XPathParser::yyFinal
	int32_t ___yyFinal_4;
	// System.String[] Mono.Xml.XPath.XPathParser::yyNames
	StringU5BU5D_t1642385972* ___yyNames_5;
	// System.Int16[] Mono.Xml.XPath.XPathParser::yyLhs
	Int16U5BU5D_t3104283263* ___yyLhs_8;
	// System.Int16[] Mono.Xml.XPath.XPathParser::yyLen
	Int16U5BU5D_t3104283263* ___yyLen_9;
	// System.Int16[] Mono.Xml.XPath.XPathParser::yyDefRed
	Int16U5BU5D_t3104283263* ___yyDefRed_10;
	// System.Int16[] Mono.Xml.XPath.XPathParser::yyDgoto
	Int16U5BU5D_t3104283263* ___yyDgoto_11;
	// System.Int16[] Mono.Xml.XPath.XPathParser::yySindex
	Int16U5BU5D_t3104283263* ___yySindex_12;
	// System.Int16[] Mono.Xml.XPath.XPathParser::yyRindex
	Int16U5BU5D_t3104283263* ___yyRindex_13;
	// System.Int16[] Mono.Xml.XPath.XPathParser::yyGindex
	Int16U5BU5D_t3104283263* ___yyGindex_14;
	// System.Int16[] Mono.Xml.XPath.XPathParser::yyTable
	Int16U5BU5D_t3104283263* ___yyTable_15;
	// System.Int16[] Mono.Xml.XPath.XPathParser::yyCheck
	Int16U5BU5D_t3104283263* ___yyCheck_16;

public:
	inline static int32_t get_offset_of_yyFinal_4() { return static_cast<int32_t>(offsetof(XPathParser_t1120482700_StaticFields, ___yyFinal_4)); }
	inline int32_t get_yyFinal_4() const { return ___yyFinal_4; }
	inline int32_t* get_address_of_yyFinal_4() { return &___yyFinal_4; }
	inline void set_yyFinal_4(int32_t value)
	{
		___yyFinal_4 = value;
	}

	inline static int32_t get_offset_of_yyNames_5() { return static_cast<int32_t>(offsetof(XPathParser_t1120482700_StaticFields, ___yyNames_5)); }
	inline StringU5BU5D_t1642385972* get_yyNames_5() const { return ___yyNames_5; }
	inline StringU5BU5D_t1642385972** get_address_of_yyNames_5() { return &___yyNames_5; }
	inline void set_yyNames_5(StringU5BU5D_t1642385972* value)
	{
		___yyNames_5 = value;
		Il2CppCodeGenWriteBarrier((&___yyNames_5), value);
	}

	inline static int32_t get_offset_of_yyLhs_8() { return static_cast<int32_t>(offsetof(XPathParser_t1120482700_StaticFields, ___yyLhs_8)); }
	inline Int16U5BU5D_t3104283263* get_yyLhs_8() const { return ___yyLhs_8; }
	inline Int16U5BU5D_t3104283263** get_address_of_yyLhs_8() { return &___yyLhs_8; }
	inline void set_yyLhs_8(Int16U5BU5D_t3104283263* value)
	{
		___yyLhs_8 = value;
		Il2CppCodeGenWriteBarrier((&___yyLhs_8), value);
	}

	inline static int32_t get_offset_of_yyLen_9() { return static_cast<int32_t>(offsetof(XPathParser_t1120482700_StaticFields, ___yyLen_9)); }
	inline Int16U5BU5D_t3104283263* get_yyLen_9() const { return ___yyLen_9; }
	inline Int16U5BU5D_t3104283263** get_address_of_yyLen_9() { return &___yyLen_9; }
	inline void set_yyLen_9(Int16U5BU5D_t3104283263* value)
	{
		___yyLen_9 = value;
		Il2CppCodeGenWriteBarrier((&___yyLen_9), value);
	}

	inline static int32_t get_offset_of_yyDefRed_10() { return static_cast<int32_t>(offsetof(XPathParser_t1120482700_StaticFields, ___yyDefRed_10)); }
	inline Int16U5BU5D_t3104283263* get_yyDefRed_10() const { return ___yyDefRed_10; }
	inline Int16U5BU5D_t3104283263** get_address_of_yyDefRed_10() { return &___yyDefRed_10; }
	inline void set_yyDefRed_10(Int16U5BU5D_t3104283263* value)
	{
		___yyDefRed_10 = value;
		Il2CppCodeGenWriteBarrier((&___yyDefRed_10), value);
	}

	inline static int32_t get_offset_of_yyDgoto_11() { return static_cast<int32_t>(offsetof(XPathParser_t1120482700_StaticFields, ___yyDgoto_11)); }
	inline Int16U5BU5D_t3104283263* get_yyDgoto_11() const { return ___yyDgoto_11; }
	inline Int16U5BU5D_t3104283263** get_address_of_yyDgoto_11() { return &___yyDgoto_11; }
	inline void set_yyDgoto_11(Int16U5BU5D_t3104283263* value)
	{
		___yyDgoto_11 = value;
		Il2CppCodeGenWriteBarrier((&___yyDgoto_11), value);
	}

	inline static int32_t get_offset_of_yySindex_12() { return static_cast<int32_t>(offsetof(XPathParser_t1120482700_StaticFields, ___yySindex_12)); }
	inline Int16U5BU5D_t3104283263* get_yySindex_12() const { return ___yySindex_12; }
	inline Int16U5BU5D_t3104283263** get_address_of_yySindex_12() { return &___yySindex_12; }
	inline void set_yySindex_12(Int16U5BU5D_t3104283263* value)
	{
		___yySindex_12 = value;
		Il2CppCodeGenWriteBarrier((&___yySindex_12), value);
	}

	inline static int32_t get_offset_of_yyRindex_13() { return static_cast<int32_t>(offsetof(XPathParser_t1120482700_StaticFields, ___yyRindex_13)); }
	inline Int16U5BU5D_t3104283263* get_yyRindex_13() const { return ___yyRindex_13; }
	inline Int16U5BU5D_t3104283263** get_address_of_yyRindex_13() { return &___yyRindex_13; }
	inline void set_yyRindex_13(Int16U5BU5D_t3104283263* value)
	{
		___yyRindex_13 = value;
		Il2CppCodeGenWriteBarrier((&___yyRindex_13), value);
	}

	inline static int32_t get_offset_of_yyGindex_14() { return static_cast<int32_t>(offsetof(XPathParser_t1120482700_StaticFields, ___yyGindex_14)); }
	inline Int16U5BU5D_t3104283263* get_yyGindex_14() const { return ___yyGindex_14; }
	inline Int16U5BU5D_t3104283263** get_address_of_yyGindex_14() { return &___yyGindex_14; }
	inline void set_yyGindex_14(Int16U5BU5D_t3104283263* value)
	{
		___yyGindex_14 = value;
		Il2CppCodeGenWriteBarrier((&___yyGindex_14), value);
	}

	inline static int32_t get_offset_of_yyTable_15() { return static_cast<int32_t>(offsetof(XPathParser_t1120482700_StaticFields, ___yyTable_15)); }
	inline Int16U5BU5D_t3104283263* get_yyTable_15() const { return ___yyTable_15; }
	inline Int16U5BU5D_t3104283263** get_address_of_yyTable_15() { return &___yyTable_15; }
	inline void set_yyTable_15(Int16U5BU5D_t3104283263* value)
	{
		___yyTable_15 = value;
		Il2CppCodeGenWriteBarrier((&___yyTable_15), value);
	}

	inline static int32_t get_offset_of_yyCheck_16() { return static_cast<int32_t>(offsetof(XPathParser_t1120482700_StaticFields, ___yyCheck_16)); }
	inline Int16U5BU5D_t3104283263* get_yyCheck_16() const { return ___yyCheck_16; }
	inline Int16U5BU5D_t3104283263** get_address_of_yyCheck_16() { return &___yyCheck_16; }
	inline void set_yyCheck_16(Int16U5BU5D_t3104283263* value)
	{
		___yyCheck_16 = value;
		Il2CppCodeGenWriteBarrier((&___yyCheck_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHPARSER_T1120482700_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3EU7B2BD1A3B7U2DFF70U2D4DFDU2D9ABDU2D9AF73AFC1A70U7D_T2052325035_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3EU7B2BD1A3B7U2DFF70U2D4DFDU2D9ABDU2D9AF73AFC1A70U7D_T2052325035_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>{2BD1A3B7-FF70-4DFD-9ABD-9AF73AFC1A70}
struct  U3CPrivateImplementationDetailsU3EU7B2BD1A3B7U2DFF70U2D4DFDU2D9ABDU2D9AF73AFC1A70U7D_t2052325035  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3EU7B2BD1A3B7U2DFF70U2D4DFDU2D9ABDU2D9AF73AFC1A70U7D_t2052325035_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> <PrivateImplementationDetails>{2BD1A3B7-FF70-4DFD-9ABD-9AF73AFC1A70}::$$method0x6000029-1
	Dictionary_2_t3986656710 * ___U24U24method0x6000029U2D1_0;

public:
	inline static int32_t get_offset_of_U24U24method0x6000029U2D1_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7B2BD1A3B7U2DFF70U2D4DFDU2D9ABDU2D9AF73AFC1A70U7D_t2052325035_StaticFields, ___U24U24method0x6000029U2D1_0)); }
	inline Dictionary_2_t3986656710 * get_U24U24method0x6000029U2D1_0() const { return ___U24U24method0x6000029U2D1_0; }
	inline Dictionary_2_t3986656710 ** get_address_of_U24U24method0x6000029U2D1_0() { return &___U24U24method0x6000029U2D1_0; }
	inline void set_U24U24method0x6000029U2D1_0(Dictionary_2_t3986656710 * value)
	{
		___U24U24method0x6000029U2D1_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24U24method0x6000029U2D1_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3EU7B2BD1A3B7U2DFF70U2D4DFDU2D9ABDU2D9AF73AFC1A70U7D_T2052325035_H
#ifndef U24ARRAYTYPEU24136_T2844921916_H
#define U24ARRAYTYPEU24136_T2844921916_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType$136
struct  U24ArrayTypeU24136_t2844921916 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU24136_t2844921916__padding[136];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU24136_T2844921916_H
#ifndef U24ARRAYTYPEU24120_T116038563_H
#define U24ARRAYTYPEU24120_T116038563_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType$120
struct  U24ArrayTypeU24120_t116038563 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU24120_t116038563__padding[120];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU24120_T116038563_H
#ifndef AESTRANSFORM_T3733702461_H
#define AESTRANSFORM_T3733702461_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.AesTransform
struct  AesTransform_t3733702461  : public SymmetricTransform_t1394030014
{
public:
	// System.UInt32[] System.Security.Cryptography.AesTransform::expandedKey
	UInt32U5BU5D_t59386216* ___expandedKey_12;
	// System.Int32 System.Security.Cryptography.AesTransform::Nk
	int32_t ___Nk_13;
	// System.Int32 System.Security.Cryptography.AesTransform::Nr
	int32_t ___Nr_14;

public:
	inline static int32_t get_offset_of_expandedKey_12() { return static_cast<int32_t>(offsetof(AesTransform_t3733702461, ___expandedKey_12)); }
	inline UInt32U5BU5D_t59386216* get_expandedKey_12() const { return ___expandedKey_12; }
	inline UInt32U5BU5D_t59386216** get_address_of_expandedKey_12() { return &___expandedKey_12; }
	inline void set_expandedKey_12(UInt32U5BU5D_t59386216* value)
	{
		___expandedKey_12 = value;
		Il2CppCodeGenWriteBarrier((&___expandedKey_12), value);
	}

	inline static int32_t get_offset_of_Nk_13() { return static_cast<int32_t>(offsetof(AesTransform_t3733702461, ___Nk_13)); }
	inline int32_t get_Nk_13() const { return ___Nk_13; }
	inline int32_t* get_address_of_Nk_13() { return &___Nk_13; }
	inline void set_Nk_13(int32_t value)
	{
		___Nk_13 = value;
	}

	inline static int32_t get_offset_of_Nr_14() { return static_cast<int32_t>(offsetof(AesTransform_t3733702461, ___Nr_14)); }
	inline int32_t get_Nr_14() const { return ___Nr_14; }
	inline int32_t* get_address_of_Nr_14() { return &___Nr_14; }
	inline void set_Nr_14(int32_t value)
	{
		___Nr_14 = value;
	}
};

struct AesTransform_t3733702461_StaticFields
{
public:
	// System.UInt32[] System.Security.Cryptography.AesTransform::Rcon
	UInt32U5BU5D_t59386216* ___Rcon_15;
	// System.Byte[] System.Security.Cryptography.AesTransform::SBox
	ByteU5BU5D_t3397334013* ___SBox_16;
	// System.Byte[] System.Security.Cryptography.AesTransform::iSBox
	ByteU5BU5D_t3397334013* ___iSBox_17;
	// System.UInt32[] System.Security.Cryptography.AesTransform::T0
	UInt32U5BU5D_t59386216* ___T0_18;
	// System.UInt32[] System.Security.Cryptography.AesTransform::T1
	UInt32U5BU5D_t59386216* ___T1_19;
	// System.UInt32[] System.Security.Cryptography.AesTransform::T2
	UInt32U5BU5D_t59386216* ___T2_20;
	// System.UInt32[] System.Security.Cryptography.AesTransform::T3
	UInt32U5BU5D_t59386216* ___T3_21;
	// System.UInt32[] System.Security.Cryptography.AesTransform::iT0
	UInt32U5BU5D_t59386216* ___iT0_22;
	// System.UInt32[] System.Security.Cryptography.AesTransform::iT1
	UInt32U5BU5D_t59386216* ___iT1_23;
	// System.UInt32[] System.Security.Cryptography.AesTransform::iT2
	UInt32U5BU5D_t59386216* ___iT2_24;
	// System.UInt32[] System.Security.Cryptography.AesTransform::iT3
	UInt32U5BU5D_t59386216* ___iT3_25;

public:
	inline static int32_t get_offset_of_Rcon_15() { return static_cast<int32_t>(offsetof(AesTransform_t3733702461_StaticFields, ___Rcon_15)); }
	inline UInt32U5BU5D_t59386216* get_Rcon_15() const { return ___Rcon_15; }
	inline UInt32U5BU5D_t59386216** get_address_of_Rcon_15() { return &___Rcon_15; }
	inline void set_Rcon_15(UInt32U5BU5D_t59386216* value)
	{
		___Rcon_15 = value;
		Il2CppCodeGenWriteBarrier((&___Rcon_15), value);
	}

	inline static int32_t get_offset_of_SBox_16() { return static_cast<int32_t>(offsetof(AesTransform_t3733702461_StaticFields, ___SBox_16)); }
	inline ByteU5BU5D_t3397334013* get_SBox_16() const { return ___SBox_16; }
	inline ByteU5BU5D_t3397334013** get_address_of_SBox_16() { return &___SBox_16; }
	inline void set_SBox_16(ByteU5BU5D_t3397334013* value)
	{
		___SBox_16 = value;
		Il2CppCodeGenWriteBarrier((&___SBox_16), value);
	}

	inline static int32_t get_offset_of_iSBox_17() { return static_cast<int32_t>(offsetof(AesTransform_t3733702461_StaticFields, ___iSBox_17)); }
	inline ByteU5BU5D_t3397334013* get_iSBox_17() const { return ___iSBox_17; }
	inline ByteU5BU5D_t3397334013** get_address_of_iSBox_17() { return &___iSBox_17; }
	inline void set_iSBox_17(ByteU5BU5D_t3397334013* value)
	{
		___iSBox_17 = value;
		Il2CppCodeGenWriteBarrier((&___iSBox_17), value);
	}

	inline static int32_t get_offset_of_T0_18() { return static_cast<int32_t>(offsetof(AesTransform_t3733702461_StaticFields, ___T0_18)); }
	inline UInt32U5BU5D_t59386216* get_T0_18() const { return ___T0_18; }
	inline UInt32U5BU5D_t59386216** get_address_of_T0_18() { return &___T0_18; }
	inline void set_T0_18(UInt32U5BU5D_t59386216* value)
	{
		___T0_18 = value;
		Il2CppCodeGenWriteBarrier((&___T0_18), value);
	}

	inline static int32_t get_offset_of_T1_19() { return static_cast<int32_t>(offsetof(AesTransform_t3733702461_StaticFields, ___T1_19)); }
	inline UInt32U5BU5D_t59386216* get_T1_19() const { return ___T1_19; }
	inline UInt32U5BU5D_t59386216** get_address_of_T1_19() { return &___T1_19; }
	inline void set_T1_19(UInt32U5BU5D_t59386216* value)
	{
		___T1_19 = value;
		Il2CppCodeGenWriteBarrier((&___T1_19), value);
	}

	inline static int32_t get_offset_of_T2_20() { return static_cast<int32_t>(offsetof(AesTransform_t3733702461_StaticFields, ___T2_20)); }
	inline UInt32U5BU5D_t59386216* get_T2_20() const { return ___T2_20; }
	inline UInt32U5BU5D_t59386216** get_address_of_T2_20() { return &___T2_20; }
	inline void set_T2_20(UInt32U5BU5D_t59386216* value)
	{
		___T2_20 = value;
		Il2CppCodeGenWriteBarrier((&___T2_20), value);
	}

	inline static int32_t get_offset_of_T3_21() { return static_cast<int32_t>(offsetof(AesTransform_t3733702461_StaticFields, ___T3_21)); }
	inline UInt32U5BU5D_t59386216* get_T3_21() const { return ___T3_21; }
	inline UInt32U5BU5D_t59386216** get_address_of_T3_21() { return &___T3_21; }
	inline void set_T3_21(UInt32U5BU5D_t59386216* value)
	{
		___T3_21 = value;
		Il2CppCodeGenWriteBarrier((&___T3_21), value);
	}

	inline static int32_t get_offset_of_iT0_22() { return static_cast<int32_t>(offsetof(AesTransform_t3733702461_StaticFields, ___iT0_22)); }
	inline UInt32U5BU5D_t59386216* get_iT0_22() const { return ___iT0_22; }
	inline UInt32U5BU5D_t59386216** get_address_of_iT0_22() { return &___iT0_22; }
	inline void set_iT0_22(UInt32U5BU5D_t59386216* value)
	{
		___iT0_22 = value;
		Il2CppCodeGenWriteBarrier((&___iT0_22), value);
	}

	inline static int32_t get_offset_of_iT1_23() { return static_cast<int32_t>(offsetof(AesTransform_t3733702461_StaticFields, ___iT1_23)); }
	inline UInt32U5BU5D_t59386216* get_iT1_23() const { return ___iT1_23; }
	inline UInt32U5BU5D_t59386216** get_address_of_iT1_23() { return &___iT1_23; }
	inline void set_iT1_23(UInt32U5BU5D_t59386216* value)
	{
		___iT1_23 = value;
		Il2CppCodeGenWriteBarrier((&___iT1_23), value);
	}

	inline static int32_t get_offset_of_iT2_24() { return static_cast<int32_t>(offsetof(AesTransform_t3733702461_StaticFields, ___iT2_24)); }
	inline UInt32U5BU5D_t59386216* get_iT2_24() const { return ___iT2_24; }
	inline UInt32U5BU5D_t59386216** get_address_of_iT2_24() { return &___iT2_24; }
	inline void set_iT2_24(UInt32U5BU5D_t59386216* value)
	{
		___iT2_24 = value;
		Il2CppCodeGenWriteBarrier((&___iT2_24), value);
	}

	inline static int32_t get_offset_of_iT3_25() { return static_cast<int32_t>(offsetof(AesTransform_t3733702461_StaticFields, ___iT3_25)); }
	inline UInt32U5BU5D_t59386216* get_iT3_25() const { return ___iT3_25; }
	inline UInt32U5BU5D_t59386216** get_address_of_iT3_25() { return &___iT3_25; }
	inline void set_iT3_25(UInt32U5BU5D_t59386216* value)
	{
		___iT3_25 = value;
		Il2CppCodeGenWriteBarrier((&___iT3_25), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AESTRANSFORM_T3733702461_H
#ifndef RSAPARAMETERS_T1462703416_H
#define RSAPARAMETERS_T1462703416_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.RSAParameters
struct  RSAParameters_t1462703416 
{
public:
	// System.Byte[] System.Security.Cryptography.RSAParameters::P
	ByteU5BU5D_t3397334013* ___P_0;
	// System.Byte[] System.Security.Cryptography.RSAParameters::Q
	ByteU5BU5D_t3397334013* ___Q_1;
	// System.Byte[] System.Security.Cryptography.RSAParameters::D
	ByteU5BU5D_t3397334013* ___D_2;
	// System.Byte[] System.Security.Cryptography.RSAParameters::DP
	ByteU5BU5D_t3397334013* ___DP_3;
	// System.Byte[] System.Security.Cryptography.RSAParameters::DQ
	ByteU5BU5D_t3397334013* ___DQ_4;
	// System.Byte[] System.Security.Cryptography.RSAParameters::InverseQ
	ByteU5BU5D_t3397334013* ___InverseQ_5;
	// System.Byte[] System.Security.Cryptography.RSAParameters::Modulus
	ByteU5BU5D_t3397334013* ___Modulus_6;
	// System.Byte[] System.Security.Cryptography.RSAParameters::Exponent
	ByteU5BU5D_t3397334013* ___Exponent_7;

public:
	inline static int32_t get_offset_of_P_0() { return static_cast<int32_t>(offsetof(RSAParameters_t1462703416, ___P_0)); }
	inline ByteU5BU5D_t3397334013* get_P_0() const { return ___P_0; }
	inline ByteU5BU5D_t3397334013** get_address_of_P_0() { return &___P_0; }
	inline void set_P_0(ByteU5BU5D_t3397334013* value)
	{
		___P_0 = value;
		Il2CppCodeGenWriteBarrier((&___P_0), value);
	}

	inline static int32_t get_offset_of_Q_1() { return static_cast<int32_t>(offsetof(RSAParameters_t1462703416, ___Q_1)); }
	inline ByteU5BU5D_t3397334013* get_Q_1() const { return ___Q_1; }
	inline ByteU5BU5D_t3397334013** get_address_of_Q_1() { return &___Q_1; }
	inline void set_Q_1(ByteU5BU5D_t3397334013* value)
	{
		___Q_1 = value;
		Il2CppCodeGenWriteBarrier((&___Q_1), value);
	}

	inline static int32_t get_offset_of_D_2() { return static_cast<int32_t>(offsetof(RSAParameters_t1462703416, ___D_2)); }
	inline ByteU5BU5D_t3397334013* get_D_2() const { return ___D_2; }
	inline ByteU5BU5D_t3397334013** get_address_of_D_2() { return &___D_2; }
	inline void set_D_2(ByteU5BU5D_t3397334013* value)
	{
		___D_2 = value;
		Il2CppCodeGenWriteBarrier((&___D_2), value);
	}

	inline static int32_t get_offset_of_DP_3() { return static_cast<int32_t>(offsetof(RSAParameters_t1462703416, ___DP_3)); }
	inline ByteU5BU5D_t3397334013* get_DP_3() const { return ___DP_3; }
	inline ByteU5BU5D_t3397334013** get_address_of_DP_3() { return &___DP_3; }
	inline void set_DP_3(ByteU5BU5D_t3397334013* value)
	{
		___DP_3 = value;
		Il2CppCodeGenWriteBarrier((&___DP_3), value);
	}

	inline static int32_t get_offset_of_DQ_4() { return static_cast<int32_t>(offsetof(RSAParameters_t1462703416, ___DQ_4)); }
	inline ByteU5BU5D_t3397334013* get_DQ_4() const { return ___DQ_4; }
	inline ByteU5BU5D_t3397334013** get_address_of_DQ_4() { return &___DQ_4; }
	inline void set_DQ_4(ByteU5BU5D_t3397334013* value)
	{
		___DQ_4 = value;
		Il2CppCodeGenWriteBarrier((&___DQ_4), value);
	}

	inline static int32_t get_offset_of_InverseQ_5() { return static_cast<int32_t>(offsetof(RSAParameters_t1462703416, ___InverseQ_5)); }
	inline ByteU5BU5D_t3397334013* get_InverseQ_5() const { return ___InverseQ_5; }
	inline ByteU5BU5D_t3397334013** get_address_of_InverseQ_5() { return &___InverseQ_5; }
	inline void set_InverseQ_5(ByteU5BU5D_t3397334013* value)
	{
		___InverseQ_5 = value;
		Il2CppCodeGenWriteBarrier((&___InverseQ_5), value);
	}

	inline static int32_t get_offset_of_Modulus_6() { return static_cast<int32_t>(offsetof(RSAParameters_t1462703416, ___Modulus_6)); }
	inline ByteU5BU5D_t3397334013* get_Modulus_6() const { return ___Modulus_6; }
	inline ByteU5BU5D_t3397334013** get_address_of_Modulus_6() { return &___Modulus_6; }
	inline void set_Modulus_6(ByteU5BU5D_t3397334013* value)
	{
		___Modulus_6 = value;
		Il2CppCodeGenWriteBarrier((&___Modulus_6), value);
	}

	inline static int32_t get_offset_of_Exponent_7() { return static_cast<int32_t>(offsetof(RSAParameters_t1462703416, ___Exponent_7)); }
	inline ByteU5BU5D_t3397334013* get_Exponent_7() const { return ___Exponent_7; }
	inline ByteU5BU5D_t3397334013** get_address_of_Exponent_7() { return &___Exponent_7; }
	inline void set_Exponent_7(ByteU5BU5D_t3397334013* value)
	{
		___Exponent_7 = value;
		Il2CppCodeGenWriteBarrier((&___Exponent_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Security.Cryptography.RSAParameters
struct RSAParameters_t1462703416_marshaled_pinvoke
{
	uint8_t* ___P_0;
	uint8_t* ___Q_1;
	uint8_t* ___D_2;
	uint8_t* ___DP_3;
	uint8_t* ___DQ_4;
	uint8_t* ___InverseQ_5;
	uint8_t* ___Modulus_6;
	uint8_t* ___Exponent_7;
};
// Native definition for COM marshalling of System.Security.Cryptography.RSAParameters
struct RSAParameters_t1462703416_marshaled_com
{
	uint8_t* ___P_0;
	uint8_t* ___Q_1;
	uint8_t* ___D_2;
	uint8_t* ___DP_3;
	uint8_t* ___DQ_4;
	uint8_t* ___InverseQ_5;
	uint8_t* ___Modulus_6;
	uint8_t* ___Exponent_7;
};
#endif // RSAPARAMETERS_T1462703416_H
#ifndef EXCEPTIONEVENTARGS_T892713536_H
#define EXCEPTIONEVENTARGS_T892713536_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OSCsharp.Utils.ExceptionEventArgs
struct  ExceptionEventArgs_t892713536  : public EventArgs_t3289624707
{
public:
	// System.Exception OSCsharp.Utils.ExceptionEventArgs::<Exception>k__BackingField
	Exception_t1927440687 * ___U3CExceptionU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CExceptionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ExceptionEventArgs_t892713536, ___U3CExceptionU3Ek__BackingField_1)); }
	inline Exception_t1927440687 * get_U3CExceptionU3Ek__BackingField_1() const { return ___U3CExceptionU3Ek__BackingField_1; }
	inline Exception_t1927440687 ** get_address_of_U3CExceptionU3Ek__BackingField_1() { return &___U3CExceptionU3Ek__BackingField_1; }
	inline void set_U3CExceptionU3Ek__BackingField_1(Exception_t1927440687 * value)
	{
		___U3CExceptionU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CExceptionU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTIONEVENTARGS_T892713536_H
#ifndef OSCMESSAGE_T2764280154_H
#define OSCMESSAGE_T2764280154_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OSCsharp.Data.OscMessage
struct  OscMessage_t2764280154  : public OscPacket_t504761797
{
public:
	// System.String OSCsharp.Data.OscMessage::typeTag
	String_t* ___typeTag_19;

public:
	inline static int32_t get_offset_of_typeTag_19() { return static_cast<int32_t>(offsetof(OscMessage_t2764280154, ___typeTag_19)); }
	inline String_t* get_typeTag_19() const { return ___typeTag_19; }
	inline String_t** get_address_of_typeTag_19() { return &___typeTag_19; }
	inline void set_typeTag_19(String_t* value)
	{
		___typeTag_19 = value;
		Il2CppCodeGenWriteBarrier((&___typeTag_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OSCMESSAGE_T2764280154_H
#ifndef YYRULES_T4159463463_H
#define YYRULES_T4159463463_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.XPath.XPathParser/YYRules
struct  YYRules_t4159463463  : public MarshalByRefObject_t1285298191
{
public:

public:
};

struct YYRules_t4159463463_StaticFields
{
public:
	// System.String[] Mono.Xml.XPath.XPathParser/YYRules::yyRule
	StringU5BU5D_t1642385972* ___yyRule_1;

public:
	inline static int32_t get_offset_of_yyRule_1() { return static_cast<int32_t>(offsetof(YYRules_t4159463463_StaticFields, ___yyRule_1)); }
	inline StringU5BU5D_t1642385972* get_yyRule_1() const { return ___yyRule_1; }
	inline StringU5BU5D_t1642385972** get_address_of_yyRule_1() { return &___yyRule_1; }
	inline void set_yyRule_1(StringU5BU5D_t1642385972* value)
	{
		___yyRule_1 = value;
		Il2CppCodeGenWriteBarrier((&___yyRule_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // YYRULES_T4159463463_H
#ifndef XPATHFUNCTION_T759167395_H
#define XPATHFUNCTION_T759167395_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathFunction
struct  XPathFunction_t759167395  : public Expression_t1283317256
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHFUNCTION_T759167395_H
#ifndef YYEXCEPTION_T2814964437_H
#define YYEXCEPTION_T2814964437_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.XPath.yyParser.yyException
struct  yyException_t2814964437  : public Exception_t1927440687
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // YYEXCEPTION_T2814964437_H
#ifndef OSCBUNDLE_T1126010605_H
#define OSCBUNDLE_T1126010605_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OSCsharp.Data.OscBundle
struct  OscBundle_t1126010605  : public OscPacket_t504761797
{
public:
	// OSCsharp.Data.OscTimeTag OSCsharp.Data.OscBundle::timeStamp
	OscTimeTag_t625345318 * ___timeStamp_4;

public:
	inline static int32_t get_offset_of_timeStamp_4() { return static_cast<int32_t>(offsetof(OscBundle_t1126010605, ___timeStamp_4)); }
	inline OscTimeTag_t625345318 * get_timeStamp_4() const { return ___timeStamp_4; }
	inline OscTimeTag_t625345318 ** get_address_of_timeStamp_4() { return &___timeStamp_4; }
	inline void set_timeStamp_4(OscTimeTag_t625345318 * value)
	{
		___timeStamp_4 = value;
		Il2CppCodeGenWriteBarrier((&___timeStamp_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OSCBUNDLE_T1126010605_H
#ifndef U24ARRAYTYPEU241024_T2672183895_H
#define U24ARRAYTYPEU241024_T2672183895_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType$1024
struct  U24ArrayTypeU241024_t2672183895 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU241024_t2672183895__padding[1024];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU241024_T2672183895_H
#ifndef U24ARRAYTYPEU24256_T2038352956_H
#define U24ARRAYTYPEU24256_T2038352956_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType$256
struct  U24ArrayTypeU24256_t2038352956 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU24256_t2038352956__padding[256];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU24256_T2038352956_H
#ifndef OSCBUNDLERECEIVEDEVENTARGS_T3673224441_H
#define OSCBUNDLERECEIVEDEVENTARGS_T3673224441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OSCsharp.Net.OscBundleReceivedEventArgs
struct  OscBundleReceivedEventArgs_t3673224441  : public EventArgs_t3289624707
{
public:
	// OSCsharp.Data.OscBundle OSCsharp.Net.OscBundleReceivedEventArgs::<Bundle>k__BackingField
	OscBundle_t1126010605 * ___U3CBundleU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CBundleU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(OscBundleReceivedEventArgs_t3673224441, ___U3CBundleU3Ek__BackingField_1)); }
	inline OscBundle_t1126010605 * get_U3CBundleU3Ek__BackingField_1() const { return ___U3CBundleU3Ek__BackingField_1; }
	inline OscBundle_t1126010605 ** get_address_of_U3CBundleU3Ek__BackingField_1() { return &___U3CBundleU3Ek__BackingField_1; }
	inline void set_U3CBundleU3Ek__BackingField_1(OscBundle_t1126010605 * value)
	{
		___U3CBundleU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBundleU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OSCBUNDLERECEIVEDEVENTARGS_T3673224441_H
#ifndef OSCPACKETRECEIVEDEVENTARGS_T3282045269_H
#define OSCPACKETRECEIVEDEVENTARGS_T3282045269_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OSCsharp.Net.OscPacketReceivedEventArgs
struct  OscPacketReceivedEventArgs_t3282045269  : public EventArgs_t3289624707
{
public:
	// OSCsharp.Data.OscPacket OSCsharp.Net.OscPacketReceivedEventArgs::<Packet>k__BackingField
	OscPacket_t504761797 * ___U3CPacketU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CPacketU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(OscPacketReceivedEventArgs_t3282045269, ___U3CPacketU3Ek__BackingField_1)); }
	inline OscPacket_t504761797 * get_U3CPacketU3Ek__BackingField_1() const { return ___U3CPacketU3Ek__BackingField_1; }
	inline OscPacket_t504761797 ** get_address_of_U3CPacketU3Ek__BackingField_1() { return &___U3CPacketU3Ek__BackingField_1; }
	inline void set_U3CPacketU3Ek__BackingField_1(OscPacket_t504761797 * value)
	{
		___U3CPacketU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPacketU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OSCPACKETRECEIVEDEVENTARGS_T3282045269_H
#ifndef OSCMESSAGERECEIVEDEVENTARGS_T1263041860_H
#define OSCMESSAGERECEIVEDEVENTARGS_T1263041860_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OSCsharp.Net.OscMessageReceivedEventArgs
struct  OscMessageReceivedEventArgs_t1263041860  : public EventArgs_t3289624707
{
public:
	// OSCsharp.Data.OscMessage OSCsharp.Net.OscMessageReceivedEventArgs::<Message>k__BackingField
	OscMessage_t2764280154 * ___U3CMessageU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CMessageU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(OscMessageReceivedEventArgs_t1263041860, ___U3CMessageU3Ek__BackingField_1)); }
	inline OscMessage_t2764280154 * get_U3CMessageU3Ek__BackingField_1() const { return ___U3CMessageU3Ek__BackingField_1; }
	inline OscMessage_t2764280154 ** get_address_of_U3CMessageU3Ek__BackingField_1() { return &___U3CMessageU3Ek__BackingField_1; }
	inline void set_U3CMessageU3Ek__BackingField_1(OscMessage_t2764280154 * value)
	{
		___U3CMessageU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMessageU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OSCMESSAGERECEIVEDEVENTARGS_T1263041860_H
#ifndef U24ARRAYTYPEU24256_T2038352955_H
#define U24ARRAYTYPEU24256_T2038352955_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType$256
struct  U24ArrayTypeU24256_t2038352955 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU24256_t2038352955__padding[256];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU24256_T2038352955_H
#ifndef U24ARRAYTYPEU243132_T1892466093_H
#define U24ARRAYTYPEU243132_T1892466093_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType$3132
struct  U24ArrayTypeU243132_t1892466093 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU243132_t1892466093__padding[3132];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU243132_T1892466093_H
#ifndef U24ARRAYTYPEU2432_T3672778806_H
#define U24ARRAYTYPEU2432_T3672778806_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType$32
struct  U24ArrayTypeU2432_t3672778806 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU2432_t3672778806__padding[32];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU2432_T3672778806_H
#ifndef U24ARRAYTYPEU2420_T540610922_H
#define U24ARRAYTYPEU2420_T540610922_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType$20
struct  U24ArrayTypeU2420_t540610922 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU2420_t540610922__padding[20];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU2420_T540610922_H
#ifndef VOID_T1841601450_H
#define VOID_T1841601450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1841601450 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1841601450_H
#ifndef TLSSTREAM_T4089752859_H
#define TLSSTREAM_T4089752859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.TlsStream
struct  TlsStream_t4089752859  : public Stream_t3255436806
{
public:
	// System.Boolean Mono.Security.Protocol.Tls.TlsStream::canRead
	bool ___canRead_1;
	// System.Boolean Mono.Security.Protocol.Tls.TlsStream::canWrite
	bool ___canWrite_2;
	// System.IO.MemoryStream Mono.Security.Protocol.Tls.TlsStream::buffer
	MemoryStream_t743994179 * ___buffer_3;
	// System.Byte[] Mono.Security.Protocol.Tls.TlsStream::temp
	ByteU5BU5D_t3397334013* ___temp_4;

public:
	inline static int32_t get_offset_of_canRead_1() { return static_cast<int32_t>(offsetof(TlsStream_t4089752859, ___canRead_1)); }
	inline bool get_canRead_1() const { return ___canRead_1; }
	inline bool* get_address_of_canRead_1() { return &___canRead_1; }
	inline void set_canRead_1(bool value)
	{
		___canRead_1 = value;
	}

	inline static int32_t get_offset_of_canWrite_2() { return static_cast<int32_t>(offsetof(TlsStream_t4089752859, ___canWrite_2)); }
	inline bool get_canWrite_2() const { return ___canWrite_2; }
	inline bool* get_address_of_canWrite_2() { return &___canWrite_2; }
	inline void set_canWrite_2(bool value)
	{
		___canWrite_2 = value;
	}

	inline static int32_t get_offset_of_buffer_3() { return static_cast<int32_t>(offsetof(TlsStream_t4089752859, ___buffer_3)); }
	inline MemoryStream_t743994179 * get_buffer_3() const { return ___buffer_3; }
	inline MemoryStream_t743994179 ** get_address_of_buffer_3() { return &___buffer_3; }
	inline void set_buffer_3(MemoryStream_t743994179 * value)
	{
		___buffer_3 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_3), value);
	}

	inline static int32_t get_offset_of_temp_4() { return static_cast<int32_t>(offsetof(TlsStream_t4089752859, ___temp_4)); }
	inline ByteU5BU5D_t3397334013* get_temp_4() const { return ___temp_4; }
	inline ByteU5BU5D_t3397334013** get_address_of_temp_4() { return &___temp_4; }
	inline void set_temp_4(ByteU5BU5D_t3397334013* value)
	{
		___temp_4 = value;
		Il2CppCodeGenWriteBarrier((&___temp_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSSTREAM_T4089752859_H
#ifndef TIMESPAN_T3430258949_H
#define TIMESPAN_T3430258949_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t3430258949 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_8;

public:
	inline static int32_t get_offset_of__ticks_8() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949, ____ticks_8)); }
	inline int64_t get__ticks_8() const { return ____ticks_8; }
	inline int64_t* get_address_of__ticks_8() { return &____ticks_8; }
	inline void set__ticks_8(int64_t value)
	{
		____ticks_8 = value;
	}
};

struct TimeSpan_t3430258949_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t3430258949  ___MaxValue_5;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t3430258949  ___MinValue_6;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t3430258949  ___Zero_7;

public:
	inline static int32_t get_offset_of_MaxValue_5() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949_StaticFields, ___MaxValue_5)); }
	inline TimeSpan_t3430258949  get_MaxValue_5() const { return ___MaxValue_5; }
	inline TimeSpan_t3430258949 * get_address_of_MaxValue_5() { return &___MaxValue_5; }
	inline void set_MaxValue_5(TimeSpan_t3430258949  value)
	{
		___MaxValue_5 = value;
	}

	inline static int32_t get_offset_of_MinValue_6() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949_StaticFields, ___MinValue_6)); }
	inline TimeSpan_t3430258949  get_MinValue_6() const { return ___MinValue_6; }
	inline TimeSpan_t3430258949 * get_address_of_MinValue_6() { return &___MinValue_6; }
	inline void set_MinValue_6(TimeSpan_t3430258949  value)
	{
		___MinValue_6 = value;
	}

	inline static int32_t get_offset_of_Zero_7() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949_StaticFields, ___Zero_7)); }
	inline TimeSpan_t3430258949  get_Zero_7() const { return ___Zero_7; }
	inline TimeSpan_t3430258949 * get_address_of_Zero_7() { return &___Zero_7; }
	inline void set_Zero_7(TimeSpan_t3430258949  value)
	{
		___Zero_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T3430258949_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t1328083999* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t1328083999* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t1328083999** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t1328083999* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef BOOLEAN_T3825574718_H
#define BOOLEAN_T3825574718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t3825574718 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t3825574718, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t3825574718_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t3825574718_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t3825574718_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T3825574718_H
#ifndef EXTENSIONATTRIBUTE_T1840441203_H
#define EXTENSIONATTRIBUTE_T1840441203_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.ExtensionAttribute
struct  ExtensionAttribute_t1840441203  : public Attribute_t542643598
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENSIONATTRIBUTE_T1840441203_H
#ifndef U24ARRAYTYPEU244_T1957337331_H
#define U24ARRAYTYPEU244_T1957337331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType$4
struct  U24ArrayTypeU244_t1957337331 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU244_t1957337331__padding[4];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU244_T1957337331_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	IntPtr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline IntPtr_t get_Zero_1() const { return ___Zero_1; }
	inline IntPtr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(IntPtr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef MONOTODOATTRIBUTE_T3487514021_H
#define MONOTODOATTRIBUTE_T3487514021_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MonoTODOAttribute
struct  MonoTODOAttribute_t3487514021  : public Attribute_t542643598
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOTODOATTRIBUTE_T3487514021_H
#ifndef U24ARRAYTYPEU2416_T1703410335_H
#define U24ARRAYTYPEU2416_T1703410335_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType$16
struct  U24ArrayTypeU2416_t1703410335 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU2416_t1703410335__padding[16];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU2416_T1703410335_H
#ifndef U24ARRAYTYPEU2412_T3672778805_H
#define U24ARRAYTYPEU2412_T3672778805_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType$12
struct  U24ArrayTypeU2412_t3672778805 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU2412_t3672778805__padding[12];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU2412_T3672778805_H
#ifndef U24ARRAYTYPEU2464_T2866209746_H
#define U24ARRAYTYPEU2464_T2866209746_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType$64
struct  U24ArrayTypeU2464_t2866209746 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU2464_t2866209746__padding[64];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU2464_T2866209746_H
#ifndef U24ARRAYTYPEU2448_T896841276_H
#define U24ARRAYTYPEU2448_T896841276_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType$48
struct  U24ArrayTypeU2448_t896841276 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU2448_t896841276__padding[48];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU2448_T896841276_H
#ifndef XPATHFUNCTIONCONCAT_T2913460711_H
#define XPATHFUNCTIONCONCAT_T2913460711_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathFunctionConcat
struct  XPathFunctionConcat_t2913460711  : public XPathFunction_t759167395
{
public:
	// System.Collections.ArrayList System.Xml.XPath.XPathFunctionConcat::rgs
	ArrayList_t4252133567 * ___rgs_0;

public:
	inline static int32_t get_offset_of_rgs_0() { return static_cast<int32_t>(offsetof(XPathFunctionConcat_t2913460711, ___rgs_0)); }
	inline ArrayList_t4252133567 * get_rgs_0() const { return ___rgs_0; }
	inline ArrayList_t4252133567 ** get_address_of_rgs_0() { return &___rgs_0; }
	inline void set_rgs_0(ArrayList_t4252133567 * value)
	{
		___rgs_0 = value;
		Il2CppCodeGenWriteBarrier((&___rgs_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHFUNCTIONCONCAT_T2913460711_H
#ifndef XPATHFUNCTIONSTARTSWITH_T1855488236_H
#define XPATHFUNCTIONSTARTSWITH_T1855488236_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathFunctionStartsWith
struct  XPathFunctionStartsWith_t1855488236  : public XPathFunction_t759167395
{
public:
	// System.Xml.XPath.Expression System.Xml.XPath.XPathFunctionStartsWith::arg0
	Expression_t1283317256 * ___arg0_0;
	// System.Xml.XPath.Expression System.Xml.XPath.XPathFunctionStartsWith::arg1
	Expression_t1283317256 * ___arg1_1;

public:
	inline static int32_t get_offset_of_arg0_0() { return static_cast<int32_t>(offsetof(XPathFunctionStartsWith_t1855488236, ___arg0_0)); }
	inline Expression_t1283317256 * get_arg0_0() const { return ___arg0_0; }
	inline Expression_t1283317256 ** get_address_of_arg0_0() { return &___arg0_0; }
	inline void set_arg0_0(Expression_t1283317256 * value)
	{
		___arg0_0 = value;
		Il2CppCodeGenWriteBarrier((&___arg0_0), value);
	}

	inline static int32_t get_offset_of_arg1_1() { return static_cast<int32_t>(offsetof(XPathFunctionStartsWith_t1855488236, ___arg1_1)); }
	inline Expression_t1283317256 * get_arg1_1() const { return ___arg1_1; }
	inline Expression_t1283317256 ** get_address_of_arg1_1() { return &___arg1_1; }
	inline void set_arg1_1(Expression_t1283317256 * value)
	{
		___arg1_1 = value;
		Il2CppCodeGenWriteBarrier((&___arg1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHFUNCTIONSTARTSWITH_T1855488236_H
#ifndef XPATHFUNCTIONSTRING_T3963481524_H
#define XPATHFUNCTIONSTRING_T3963481524_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathFunctionString
struct  XPathFunctionString_t3963481524  : public XPathFunction_t759167395
{
public:
	// System.Xml.XPath.Expression System.Xml.XPath.XPathFunctionString::arg0
	Expression_t1283317256 * ___arg0_0;

public:
	inline static int32_t get_offset_of_arg0_0() { return static_cast<int32_t>(offsetof(XPathFunctionString_t3963481524, ___arg0_0)); }
	inline Expression_t1283317256 * get_arg0_0() const { return ___arg0_0; }
	inline Expression_t1283317256 ** get_address_of_arg0_0() { return &___arg0_0; }
	inline void set_arg0_0(Expression_t1283317256 * value)
	{
		___arg0_0 = value;
		Il2CppCodeGenWriteBarrier((&___arg0_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHFUNCTIONSTRING_T3963481524_H
#ifndef XPATHFUNCTIONNAMESPACEURI_T448035136_H
#define XPATHFUNCTIONNAMESPACEURI_T448035136_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathFunctionNamespaceUri
struct  XPathFunctionNamespaceUri_t448035136  : public XPathFunction_t759167395
{
public:
	// System.Xml.XPath.Expression System.Xml.XPath.XPathFunctionNamespaceUri::arg0
	Expression_t1283317256 * ___arg0_0;

public:
	inline static int32_t get_offset_of_arg0_0() { return static_cast<int32_t>(offsetof(XPathFunctionNamespaceUri_t448035136, ___arg0_0)); }
	inline Expression_t1283317256 * get_arg0_0() const { return ___arg0_0; }
	inline Expression_t1283317256 ** get_address_of_arg0_0() { return &___arg0_0; }
	inline void set_arg0_0(Expression_t1283317256 * value)
	{
		___arg0_0 = value;
		Il2CppCodeGenWriteBarrier((&___arg0_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHFUNCTIONNAMESPACEURI_T448035136_H
#ifndef XPATHFUNCTIONLOCALNAME_T3935594891_H
#define XPATHFUNCTIONLOCALNAME_T3935594891_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathFunctionLocalName
struct  XPathFunctionLocalName_t3935594891  : public XPathFunction_t759167395
{
public:
	// System.Xml.XPath.Expression System.Xml.XPath.XPathFunctionLocalName::arg0
	Expression_t1283317256 * ___arg0_0;

public:
	inline static int32_t get_offset_of_arg0_0() { return static_cast<int32_t>(offsetof(XPathFunctionLocalName_t3935594891, ___arg0_0)); }
	inline Expression_t1283317256 * get_arg0_0() const { return ___arg0_0; }
	inline Expression_t1283317256 ** get_address_of_arg0_0() { return &___arg0_0; }
	inline void set_arg0_0(Expression_t1283317256 * value)
	{
		___arg0_0 = value;
		Il2CppCodeGenWriteBarrier((&___arg0_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHFUNCTIONLOCALNAME_T3935594891_H
#ifndef XPATHFUNCTIONID_T2998162272_H
#define XPATHFUNCTIONID_T2998162272_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathFunctionId
struct  XPathFunctionId_t2998162272  : public XPathFunction_t759167395
{
public:
	// System.Xml.XPath.Expression System.Xml.XPath.XPathFunctionId::arg0
	Expression_t1283317256 * ___arg0_0;

public:
	inline static int32_t get_offset_of_arg0_0() { return static_cast<int32_t>(offsetof(XPathFunctionId_t2998162272, ___arg0_0)); }
	inline Expression_t1283317256 * get_arg0_0() const { return ___arg0_0; }
	inline Expression_t1283317256 ** get_address_of_arg0_0() { return &___arg0_0; }
	inline void set_arg0_0(Expression_t1283317256 * value)
	{
		___arg0_0 = value;
		Il2CppCodeGenWriteBarrier((&___arg0_0), value);
	}
};

struct XPathFunctionId_t2998162272_StaticFields
{
public:
	// System.Char[] System.Xml.XPath.XPathFunctionId::rgchWhitespace
	CharU5BU5D_t1328083999* ___rgchWhitespace_1;

public:
	inline static int32_t get_offset_of_rgchWhitespace_1() { return static_cast<int32_t>(offsetof(XPathFunctionId_t2998162272_StaticFields, ___rgchWhitespace_1)); }
	inline CharU5BU5D_t1328083999* get_rgchWhitespace_1() const { return ___rgchWhitespace_1; }
	inline CharU5BU5D_t1328083999** get_address_of_rgchWhitespace_1() { return &___rgchWhitespace_1; }
	inline void set_rgchWhitespace_1(CharU5BU5D_t1328083999* value)
	{
		___rgchWhitespace_1 = value;
		Il2CppCodeGenWriteBarrier((&___rgchWhitespace_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHFUNCTIONID_T2998162272_H
#ifndef XPATHFUNCTIONNAME_T1880977850_H
#define XPATHFUNCTIONNAME_T1880977850_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathFunctionName
struct  XPathFunctionName_t1880977850  : public XPathFunction_t759167395
{
public:
	// System.Xml.XPath.Expression System.Xml.XPath.XPathFunctionName::arg0
	Expression_t1283317256 * ___arg0_0;

public:
	inline static int32_t get_offset_of_arg0_0() { return static_cast<int32_t>(offsetof(XPathFunctionName_t1880977850, ___arg0_0)); }
	inline Expression_t1283317256 * get_arg0_0() const { return ___arg0_0; }
	inline Expression_t1283317256 ** get_address_of_arg0_0() { return &___arg0_0; }
	inline void set_arg0_0(Expression_t1283317256 * value)
	{
		___arg0_0 = value;
		Il2CppCodeGenWriteBarrier((&___arg0_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHFUNCTIONNAME_T1880977850_H
#ifndef XPATHBOOLEANFUNCTION_T2511646183_H
#define XPATHBOOLEANFUNCTION_T2511646183_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathBooleanFunction
struct  XPathBooleanFunction_t2511646183  : public XPathFunction_t759167395
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHBOOLEANFUNCTION_T2511646183_H
#ifndef XPATHFUNCTIONSTRINGLENGTH_T2821584396_H
#define XPATHFUNCTIONSTRINGLENGTH_T2821584396_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathFunctionStringLength
struct  XPathFunctionStringLength_t2821584396  : public XPathFunction_t759167395
{
public:
	// System.Xml.XPath.Expression System.Xml.XPath.XPathFunctionStringLength::arg0
	Expression_t1283317256 * ___arg0_0;

public:
	inline static int32_t get_offset_of_arg0_0() { return static_cast<int32_t>(offsetof(XPathFunctionStringLength_t2821584396, ___arg0_0)); }
	inline Expression_t1283317256 * get_arg0_0() const { return ___arg0_0; }
	inline Expression_t1283317256 ** get_address_of_arg0_0() { return &___arg0_0; }
	inline void set_arg0_0(Expression_t1283317256 * value)
	{
		___arg0_0 = value;
		Il2CppCodeGenWriteBarrier((&___arg0_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHFUNCTIONSTRINGLENGTH_T2821584396_H
#ifndef XPATHFUNCTIONNORMALIZESPACE_T1288231156_H
#define XPATHFUNCTIONNORMALIZESPACE_T1288231156_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathFunctionNormalizeSpace
struct  XPathFunctionNormalizeSpace_t1288231156  : public XPathFunction_t759167395
{
public:
	// System.Xml.XPath.Expression System.Xml.XPath.XPathFunctionNormalizeSpace::arg0
	Expression_t1283317256 * ___arg0_0;

public:
	inline static int32_t get_offset_of_arg0_0() { return static_cast<int32_t>(offsetof(XPathFunctionNormalizeSpace_t1288231156, ___arg0_0)); }
	inline Expression_t1283317256 * get_arg0_0() const { return ___arg0_0; }
	inline Expression_t1283317256 ** get_address_of_arg0_0() { return &___arg0_0; }
	inline void set_arg0_0(Expression_t1283317256 * value)
	{
		___arg0_0 = value;
		Il2CppCodeGenWriteBarrier((&___arg0_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHFUNCTIONNORMALIZESPACE_T1288231156_H
#ifndef XPATHFUNCTIONTRANSLATE_T2720664953_H
#define XPATHFUNCTIONTRANSLATE_T2720664953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathFunctionTranslate
struct  XPathFunctionTranslate_t2720664953  : public XPathFunction_t759167395
{
public:
	// System.Xml.XPath.Expression System.Xml.XPath.XPathFunctionTranslate::arg0
	Expression_t1283317256 * ___arg0_0;
	// System.Xml.XPath.Expression System.Xml.XPath.XPathFunctionTranslate::arg1
	Expression_t1283317256 * ___arg1_1;
	// System.Xml.XPath.Expression System.Xml.XPath.XPathFunctionTranslate::arg2
	Expression_t1283317256 * ___arg2_2;

public:
	inline static int32_t get_offset_of_arg0_0() { return static_cast<int32_t>(offsetof(XPathFunctionTranslate_t2720664953, ___arg0_0)); }
	inline Expression_t1283317256 * get_arg0_0() const { return ___arg0_0; }
	inline Expression_t1283317256 ** get_address_of_arg0_0() { return &___arg0_0; }
	inline void set_arg0_0(Expression_t1283317256 * value)
	{
		___arg0_0 = value;
		Il2CppCodeGenWriteBarrier((&___arg0_0), value);
	}

	inline static int32_t get_offset_of_arg1_1() { return static_cast<int32_t>(offsetof(XPathFunctionTranslate_t2720664953, ___arg1_1)); }
	inline Expression_t1283317256 * get_arg1_1() const { return ___arg1_1; }
	inline Expression_t1283317256 ** get_address_of_arg1_1() { return &___arg1_1; }
	inline void set_arg1_1(Expression_t1283317256 * value)
	{
		___arg1_1 = value;
		Il2CppCodeGenWriteBarrier((&___arg1_1), value);
	}

	inline static int32_t get_offset_of_arg2_2() { return static_cast<int32_t>(offsetof(XPathFunctionTranslate_t2720664953, ___arg2_2)); }
	inline Expression_t1283317256 * get_arg2_2() const { return ___arg2_2; }
	inline Expression_t1283317256 ** get_address_of_arg2_2() { return &___arg2_2; }
	inline void set_arg2_2(Expression_t1283317256 * value)
	{
		___arg2_2 = value;
		Il2CppCodeGenWriteBarrier((&___arg2_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHFUNCTIONTRANSLATE_T2720664953_H
#ifndef XPATHFUNCTIONSUBSTRINGBEFORE_T1797727619_H
#define XPATHFUNCTIONSUBSTRINGBEFORE_T1797727619_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathFunctionSubstringBefore
struct  XPathFunctionSubstringBefore_t1797727619  : public XPathFunction_t759167395
{
public:
	// System.Xml.XPath.Expression System.Xml.XPath.XPathFunctionSubstringBefore::arg0
	Expression_t1283317256 * ___arg0_0;
	// System.Xml.XPath.Expression System.Xml.XPath.XPathFunctionSubstringBefore::arg1
	Expression_t1283317256 * ___arg1_1;

public:
	inline static int32_t get_offset_of_arg0_0() { return static_cast<int32_t>(offsetof(XPathFunctionSubstringBefore_t1797727619, ___arg0_0)); }
	inline Expression_t1283317256 * get_arg0_0() const { return ___arg0_0; }
	inline Expression_t1283317256 ** get_address_of_arg0_0() { return &___arg0_0; }
	inline void set_arg0_0(Expression_t1283317256 * value)
	{
		___arg0_0 = value;
		Il2CppCodeGenWriteBarrier((&___arg0_0), value);
	}

	inline static int32_t get_offset_of_arg1_1() { return static_cast<int32_t>(offsetof(XPathFunctionSubstringBefore_t1797727619, ___arg1_1)); }
	inline Expression_t1283317256 * get_arg1_1() const { return ___arg1_1; }
	inline Expression_t1283317256 ** get_address_of_arg1_1() { return &___arg1_1; }
	inline void set_arg1_1(Expression_t1283317256 * value)
	{
		___arg1_1 = value;
		Il2CppCodeGenWriteBarrier((&___arg1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHFUNCTIONSUBSTRINGBEFORE_T1797727619_H
#ifndef XPATHFUNCTIONCONTAINS_T4033230314_H
#define XPATHFUNCTIONCONTAINS_T4033230314_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathFunctionContains
struct  XPathFunctionContains_t4033230314  : public XPathFunction_t759167395
{
public:
	// System.Xml.XPath.Expression System.Xml.XPath.XPathFunctionContains::arg0
	Expression_t1283317256 * ___arg0_0;
	// System.Xml.XPath.Expression System.Xml.XPath.XPathFunctionContains::arg1
	Expression_t1283317256 * ___arg1_1;

public:
	inline static int32_t get_offset_of_arg0_0() { return static_cast<int32_t>(offsetof(XPathFunctionContains_t4033230314, ___arg0_0)); }
	inline Expression_t1283317256 * get_arg0_0() const { return ___arg0_0; }
	inline Expression_t1283317256 ** get_address_of_arg0_0() { return &___arg0_0; }
	inline void set_arg0_0(Expression_t1283317256 * value)
	{
		___arg0_0 = value;
		Il2CppCodeGenWriteBarrier((&___arg0_0), value);
	}

	inline static int32_t get_offset_of_arg1_1() { return static_cast<int32_t>(offsetof(XPathFunctionContains_t4033230314, ___arg1_1)); }
	inline Expression_t1283317256 * get_arg1_1() const { return ___arg1_1; }
	inline Expression_t1283317256 ** get_address_of_arg1_1() { return &___arg1_1; }
	inline void set_arg1_1(Expression_t1283317256 * value)
	{
		___arg1_1 = value;
		Il2CppCodeGenWriteBarrier((&___arg1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHFUNCTIONCONTAINS_T4033230314_H
#ifndef XPATHFUNCTIONSUBSTRING_T2124841904_H
#define XPATHFUNCTIONSUBSTRING_T2124841904_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathFunctionSubstring
struct  XPathFunctionSubstring_t2124841904  : public XPathFunction_t759167395
{
public:
	// System.Xml.XPath.Expression System.Xml.XPath.XPathFunctionSubstring::arg0
	Expression_t1283317256 * ___arg0_0;
	// System.Xml.XPath.Expression System.Xml.XPath.XPathFunctionSubstring::arg1
	Expression_t1283317256 * ___arg1_1;
	// System.Xml.XPath.Expression System.Xml.XPath.XPathFunctionSubstring::arg2
	Expression_t1283317256 * ___arg2_2;

public:
	inline static int32_t get_offset_of_arg0_0() { return static_cast<int32_t>(offsetof(XPathFunctionSubstring_t2124841904, ___arg0_0)); }
	inline Expression_t1283317256 * get_arg0_0() const { return ___arg0_0; }
	inline Expression_t1283317256 ** get_address_of_arg0_0() { return &___arg0_0; }
	inline void set_arg0_0(Expression_t1283317256 * value)
	{
		___arg0_0 = value;
		Il2CppCodeGenWriteBarrier((&___arg0_0), value);
	}

	inline static int32_t get_offset_of_arg1_1() { return static_cast<int32_t>(offsetof(XPathFunctionSubstring_t2124841904, ___arg1_1)); }
	inline Expression_t1283317256 * get_arg1_1() const { return ___arg1_1; }
	inline Expression_t1283317256 ** get_address_of_arg1_1() { return &___arg1_1; }
	inline void set_arg1_1(Expression_t1283317256 * value)
	{
		___arg1_1 = value;
		Il2CppCodeGenWriteBarrier((&___arg1_1), value);
	}

	inline static int32_t get_offset_of_arg2_2() { return static_cast<int32_t>(offsetof(XPathFunctionSubstring_t2124841904, ___arg2_2)); }
	inline Expression_t1283317256 * get_arg2_2() const { return ___arg2_2; }
	inline Expression_t1283317256 ** get_address_of_arg2_2() { return &___arg2_2; }
	inline void set_arg2_2(Expression_t1283317256 * value)
	{
		___arg2_2 = value;
		Il2CppCodeGenWriteBarrier((&___arg2_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHFUNCTIONSUBSTRING_T2124841904_H
#ifndef XPATHFUNCTIONSUBSTRINGAFTER_T1560316700_H
#define XPATHFUNCTIONSUBSTRINGAFTER_T1560316700_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathFunctionSubstringAfter
struct  XPathFunctionSubstringAfter_t1560316700  : public XPathFunction_t759167395
{
public:
	// System.Xml.XPath.Expression System.Xml.XPath.XPathFunctionSubstringAfter::arg0
	Expression_t1283317256 * ___arg0_0;
	// System.Xml.XPath.Expression System.Xml.XPath.XPathFunctionSubstringAfter::arg1
	Expression_t1283317256 * ___arg1_1;

public:
	inline static int32_t get_offset_of_arg0_0() { return static_cast<int32_t>(offsetof(XPathFunctionSubstringAfter_t1560316700, ___arg0_0)); }
	inline Expression_t1283317256 * get_arg0_0() const { return ___arg0_0; }
	inline Expression_t1283317256 ** get_address_of_arg0_0() { return &___arg0_0; }
	inline void set_arg0_0(Expression_t1283317256 * value)
	{
		___arg0_0 = value;
		Il2CppCodeGenWriteBarrier((&___arg0_0), value);
	}

	inline static int32_t get_offset_of_arg1_1() { return static_cast<int32_t>(offsetof(XPathFunctionSubstringAfter_t1560316700, ___arg1_1)); }
	inline Expression_t1283317256 * get_arg1_1() const { return ___arg1_1; }
	inline Expression_t1283317256 ** get_address_of_arg1_1() { return &___arg1_1; }
	inline void set_arg1_1(Expression_t1283317256 * value)
	{
		___arg1_1 = value;
		Il2CppCodeGenWriteBarrier((&___arg1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHFUNCTIONSUBSTRINGAFTER_T1560316700_H
#ifndef HANDSHAKETYPE_T2540099417_H
#define HANDSHAKETYPE_T2540099417_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.Handshake.HandshakeType
struct  HandshakeType_t2540099417 
{
public:
	// System.Byte Mono.Security.Protocol.Tls.Handshake.HandshakeType::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HandshakeType_t2540099417, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HANDSHAKETYPE_T2540099417_H
#ifndef CONTENTTYPE_T859870085_H
#define CONTENTTYPE_T859870085_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.ContentType
struct  ContentType_t859870085 
{
public:
	// System.Byte Mono.Security.Protocol.Tls.ContentType::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ContentType_t859870085, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTENTTYPE_T859870085_H
#ifndef DELEGATE_T3022476291_H
#define DELEGATE_T3022476291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t3022476291  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	IntPtr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	IntPtr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	IntPtr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	IntPtr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1572802995 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___invoke_impl_1)); }
	inline IntPtr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline IntPtr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(IntPtr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_3)); }
	inline IntPtr_t get_method_3() const { return ___method_3; }
	inline IntPtr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(IntPtr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___delegate_trampoline_4)); }
	inline IntPtr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline IntPtr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(IntPtr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_code_5)); }
	inline IntPtr_t get_method_code_5() const { return ___method_code_5; }
	inline IntPtr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(IntPtr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___data_8)); }
	inline DelegateData_t1572802995 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1572802995 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1572802995 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T3022476291_H
#ifndef CONFIDENCEFACTOR_T1997037802_H
#define CONFIDENCEFACTOR_T1997037802_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Math.Prime.ConfidenceFactor
struct  ConfidenceFactor_t1997037802 
{
public:
	// System.Int32 Mono.Math.Prime.ConfidenceFactor::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ConfidenceFactor_t1997037802, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIDENCEFACTOR_T1997037802_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1486305140_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1486305140_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t1486305140  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType$136 <PrivateImplementationDetails>::$$field-0
	U24ArrayTypeU24136_t2844921916  ___U24U24fieldU2D0_0;
	// <PrivateImplementationDetails>/$ArrayType$120 <PrivateImplementationDetails>::$$field-1
	U24ArrayTypeU24120_t116038563  ___U24U24fieldU2D1_1;
	// <PrivateImplementationDetails>/$ArrayType$256 <PrivateImplementationDetails>::$$field-2
	U24ArrayTypeU24256_t2038352956  ___U24U24fieldU2D2_2;
	// <PrivateImplementationDetails>/$ArrayType$256 <PrivateImplementationDetails>::$$field-3
	U24ArrayTypeU24256_t2038352956  ___U24U24fieldU2D3_3;
	// <PrivateImplementationDetails>/$ArrayType$1024 <PrivateImplementationDetails>::$$field-4
	U24ArrayTypeU241024_t2672183895  ___U24U24fieldU2D4_4;
	// <PrivateImplementationDetails>/$ArrayType$1024 <PrivateImplementationDetails>::$$field-5
	U24ArrayTypeU241024_t2672183895  ___U24U24fieldU2D5_5;
	// <PrivateImplementationDetails>/$ArrayType$1024 <PrivateImplementationDetails>::$$field-6
	U24ArrayTypeU241024_t2672183895  ___U24U24fieldU2D6_6;
	// <PrivateImplementationDetails>/$ArrayType$1024 <PrivateImplementationDetails>::$$field-7
	U24ArrayTypeU241024_t2672183895  ___U24U24fieldU2D7_7;
	// <PrivateImplementationDetails>/$ArrayType$1024 <PrivateImplementationDetails>::$$field-8
	U24ArrayTypeU241024_t2672183895  ___U24U24fieldU2D8_8;
	// <PrivateImplementationDetails>/$ArrayType$1024 <PrivateImplementationDetails>::$$field-9
	U24ArrayTypeU241024_t2672183895  ___U24U24fieldU2D9_9;
	// <PrivateImplementationDetails>/$ArrayType$1024 <PrivateImplementationDetails>::$$field-10
	U24ArrayTypeU241024_t2672183895  ___U24U24fieldU2D10_10;
	// <PrivateImplementationDetails>/$ArrayType$1024 <PrivateImplementationDetails>::$$field-11
	U24ArrayTypeU241024_t2672183895  ___U24U24fieldU2D11_11;

public:
	inline static int32_t get_offset_of_U24U24fieldU2D0_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields, ___U24U24fieldU2D0_0)); }
	inline U24ArrayTypeU24136_t2844921916  get_U24U24fieldU2D0_0() const { return ___U24U24fieldU2D0_0; }
	inline U24ArrayTypeU24136_t2844921916 * get_address_of_U24U24fieldU2D0_0() { return &___U24U24fieldU2D0_0; }
	inline void set_U24U24fieldU2D0_0(U24ArrayTypeU24136_t2844921916  value)
	{
		___U24U24fieldU2D0_0 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D1_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields, ___U24U24fieldU2D1_1)); }
	inline U24ArrayTypeU24120_t116038563  get_U24U24fieldU2D1_1() const { return ___U24U24fieldU2D1_1; }
	inline U24ArrayTypeU24120_t116038563 * get_address_of_U24U24fieldU2D1_1() { return &___U24U24fieldU2D1_1; }
	inline void set_U24U24fieldU2D1_1(U24ArrayTypeU24120_t116038563  value)
	{
		___U24U24fieldU2D1_1 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D2_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields, ___U24U24fieldU2D2_2)); }
	inline U24ArrayTypeU24256_t2038352956  get_U24U24fieldU2D2_2() const { return ___U24U24fieldU2D2_2; }
	inline U24ArrayTypeU24256_t2038352956 * get_address_of_U24U24fieldU2D2_2() { return &___U24U24fieldU2D2_2; }
	inline void set_U24U24fieldU2D2_2(U24ArrayTypeU24256_t2038352956  value)
	{
		___U24U24fieldU2D2_2 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D3_3() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields, ___U24U24fieldU2D3_3)); }
	inline U24ArrayTypeU24256_t2038352956  get_U24U24fieldU2D3_3() const { return ___U24U24fieldU2D3_3; }
	inline U24ArrayTypeU24256_t2038352956 * get_address_of_U24U24fieldU2D3_3() { return &___U24U24fieldU2D3_3; }
	inline void set_U24U24fieldU2D3_3(U24ArrayTypeU24256_t2038352956  value)
	{
		___U24U24fieldU2D3_3 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D4_4() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields, ___U24U24fieldU2D4_4)); }
	inline U24ArrayTypeU241024_t2672183895  get_U24U24fieldU2D4_4() const { return ___U24U24fieldU2D4_4; }
	inline U24ArrayTypeU241024_t2672183895 * get_address_of_U24U24fieldU2D4_4() { return &___U24U24fieldU2D4_4; }
	inline void set_U24U24fieldU2D4_4(U24ArrayTypeU241024_t2672183895  value)
	{
		___U24U24fieldU2D4_4 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D5_5() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields, ___U24U24fieldU2D5_5)); }
	inline U24ArrayTypeU241024_t2672183895  get_U24U24fieldU2D5_5() const { return ___U24U24fieldU2D5_5; }
	inline U24ArrayTypeU241024_t2672183895 * get_address_of_U24U24fieldU2D5_5() { return &___U24U24fieldU2D5_5; }
	inline void set_U24U24fieldU2D5_5(U24ArrayTypeU241024_t2672183895  value)
	{
		___U24U24fieldU2D5_5 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D6_6() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields, ___U24U24fieldU2D6_6)); }
	inline U24ArrayTypeU241024_t2672183895  get_U24U24fieldU2D6_6() const { return ___U24U24fieldU2D6_6; }
	inline U24ArrayTypeU241024_t2672183895 * get_address_of_U24U24fieldU2D6_6() { return &___U24U24fieldU2D6_6; }
	inline void set_U24U24fieldU2D6_6(U24ArrayTypeU241024_t2672183895  value)
	{
		___U24U24fieldU2D6_6 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D7_7() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields, ___U24U24fieldU2D7_7)); }
	inline U24ArrayTypeU241024_t2672183895  get_U24U24fieldU2D7_7() const { return ___U24U24fieldU2D7_7; }
	inline U24ArrayTypeU241024_t2672183895 * get_address_of_U24U24fieldU2D7_7() { return &___U24U24fieldU2D7_7; }
	inline void set_U24U24fieldU2D7_7(U24ArrayTypeU241024_t2672183895  value)
	{
		___U24U24fieldU2D7_7 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D8_8() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields, ___U24U24fieldU2D8_8)); }
	inline U24ArrayTypeU241024_t2672183895  get_U24U24fieldU2D8_8() const { return ___U24U24fieldU2D8_8; }
	inline U24ArrayTypeU241024_t2672183895 * get_address_of_U24U24fieldU2D8_8() { return &___U24U24fieldU2D8_8; }
	inline void set_U24U24fieldU2D8_8(U24ArrayTypeU241024_t2672183895  value)
	{
		___U24U24fieldU2D8_8 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D9_9() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields, ___U24U24fieldU2D9_9)); }
	inline U24ArrayTypeU241024_t2672183895  get_U24U24fieldU2D9_9() const { return ___U24U24fieldU2D9_9; }
	inline U24ArrayTypeU241024_t2672183895 * get_address_of_U24U24fieldU2D9_9() { return &___U24U24fieldU2D9_9; }
	inline void set_U24U24fieldU2D9_9(U24ArrayTypeU241024_t2672183895  value)
	{
		___U24U24fieldU2D9_9 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D10_10() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields, ___U24U24fieldU2D10_10)); }
	inline U24ArrayTypeU241024_t2672183895  get_U24U24fieldU2D10_10() const { return ___U24U24fieldU2D10_10; }
	inline U24ArrayTypeU241024_t2672183895 * get_address_of_U24U24fieldU2D10_10() { return &___U24U24fieldU2D10_10; }
	inline void set_U24U24fieldU2D10_10(U24ArrayTypeU241024_t2672183895  value)
	{
		___U24U24fieldU2D10_10 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D11_11() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields, ___U24U24fieldU2D11_11)); }
	inline U24ArrayTypeU241024_t2672183895  get_U24U24fieldU2D11_11() const { return ___U24U24fieldU2D11_11; }
	inline U24ArrayTypeU241024_t2672183895 * get_address_of_U24U24fieldU2D11_11() { return &___U24U24fieldU2D11_11; }
	inline void set_U24U24fieldU2D11_11(U24ArrayTypeU241024_t2672183895  value)
	{
		___U24U24fieldU2D11_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1486305140_H
#ifndef TRANSMISSIONTYPE_T529366678_H
#define TRANSMISSIONTYPE_T529366678_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OSCsharp.Net.TransmissionType
struct  TransmissionType_t529366678 
{
public:
	// System.Int32 OSCsharp.Net.TransmissionType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TransmissionType_t529366678, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSMISSIONTYPE_T529366678_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1486305139_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1486305139_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t1486305139  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType$3132 <PrivateImplementationDetails>::$$field-0
	U24ArrayTypeU243132_t1892466093  ___U24U24fieldU2D0_0;
	// <PrivateImplementationDetails>/$ArrayType$256 <PrivateImplementationDetails>::$$field-5
	U24ArrayTypeU24256_t2038352955  ___U24U24fieldU2D5_1;
	// <PrivateImplementationDetails>/$ArrayType$20 <PrivateImplementationDetails>::$$field-6
	U24ArrayTypeU2420_t540610922  ___U24U24fieldU2D6_2;
	// <PrivateImplementationDetails>/$ArrayType$32 <PrivateImplementationDetails>::$$field-7
	U24ArrayTypeU2432_t3672778806  ___U24U24fieldU2D7_3;
	// <PrivateImplementationDetails>/$ArrayType$48 <PrivateImplementationDetails>::$$field-8
	U24ArrayTypeU2448_t896841276  ___U24U24fieldU2D8_4;
	// <PrivateImplementationDetails>/$ArrayType$64 <PrivateImplementationDetails>::$$field-9
	U24ArrayTypeU2464_t2866209746  ___U24U24fieldU2D9_5;
	// <PrivateImplementationDetails>/$ArrayType$64 <PrivateImplementationDetails>::$$field-11
	U24ArrayTypeU2464_t2866209746  ___U24U24fieldU2D11_6;
	// <PrivateImplementationDetails>/$ArrayType$64 <PrivateImplementationDetails>::$$field-12
	U24ArrayTypeU2464_t2866209746  ___U24U24fieldU2D12_7;
	// <PrivateImplementationDetails>/$ArrayType$64 <PrivateImplementationDetails>::$$field-13
	U24ArrayTypeU2464_t2866209746  ___U24U24fieldU2D13_8;
	// <PrivateImplementationDetails>/$ArrayType$12 <PrivateImplementationDetails>::$$field-14
	U24ArrayTypeU2412_t3672778805  ___U24U24fieldU2D14_9;
	// <PrivateImplementationDetails>/$ArrayType$12 <PrivateImplementationDetails>::$$field-15
	U24ArrayTypeU2412_t3672778805  ___U24U24fieldU2D15_10;
	// <PrivateImplementationDetails>/$ArrayType$12 <PrivateImplementationDetails>::$$field-16
	U24ArrayTypeU2412_t3672778805  ___U24U24fieldU2D16_11;
	// <PrivateImplementationDetails>/$ArrayType$16 <PrivateImplementationDetails>::$$field-17
	U24ArrayTypeU2416_t1703410335  ___U24U24fieldU2D17_12;
	// <PrivateImplementationDetails>/$ArrayType$4 <PrivateImplementationDetails>::$$field-21
	U24ArrayTypeU244_t1957337331  ___U24U24fieldU2D21_13;
	// <PrivateImplementationDetails>/$ArrayType$4 <PrivateImplementationDetails>::$$field-22
	U24ArrayTypeU244_t1957337331  ___U24U24fieldU2D22_14;
	// <PrivateImplementationDetails>/$ArrayType$4 <PrivateImplementationDetails>::$$field-23
	U24ArrayTypeU244_t1957337331  ___U24U24fieldU2D23_15;

public:
	inline static int32_t get_offset_of_U24U24fieldU2D0_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields, ___U24U24fieldU2D0_0)); }
	inline U24ArrayTypeU243132_t1892466093  get_U24U24fieldU2D0_0() const { return ___U24U24fieldU2D0_0; }
	inline U24ArrayTypeU243132_t1892466093 * get_address_of_U24U24fieldU2D0_0() { return &___U24U24fieldU2D0_0; }
	inline void set_U24U24fieldU2D0_0(U24ArrayTypeU243132_t1892466093  value)
	{
		___U24U24fieldU2D0_0 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D5_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields, ___U24U24fieldU2D5_1)); }
	inline U24ArrayTypeU24256_t2038352955  get_U24U24fieldU2D5_1() const { return ___U24U24fieldU2D5_1; }
	inline U24ArrayTypeU24256_t2038352955 * get_address_of_U24U24fieldU2D5_1() { return &___U24U24fieldU2D5_1; }
	inline void set_U24U24fieldU2D5_1(U24ArrayTypeU24256_t2038352955  value)
	{
		___U24U24fieldU2D5_1 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D6_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields, ___U24U24fieldU2D6_2)); }
	inline U24ArrayTypeU2420_t540610922  get_U24U24fieldU2D6_2() const { return ___U24U24fieldU2D6_2; }
	inline U24ArrayTypeU2420_t540610922 * get_address_of_U24U24fieldU2D6_2() { return &___U24U24fieldU2D6_2; }
	inline void set_U24U24fieldU2D6_2(U24ArrayTypeU2420_t540610922  value)
	{
		___U24U24fieldU2D6_2 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D7_3() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields, ___U24U24fieldU2D7_3)); }
	inline U24ArrayTypeU2432_t3672778806  get_U24U24fieldU2D7_3() const { return ___U24U24fieldU2D7_3; }
	inline U24ArrayTypeU2432_t3672778806 * get_address_of_U24U24fieldU2D7_3() { return &___U24U24fieldU2D7_3; }
	inline void set_U24U24fieldU2D7_3(U24ArrayTypeU2432_t3672778806  value)
	{
		___U24U24fieldU2D7_3 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D8_4() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields, ___U24U24fieldU2D8_4)); }
	inline U24ArrayTypeU2448_t896841276  get_U24U24fieldU2D8_4() const { return ___U24U24fieldU2D8_4; }
	inline U24ArrayTypeU2448_t896841276 * get_address_of_U24U24fieldU2D8_4() { return &___U24U24fieldU2D8_4; }
	inline void set_U24U24fieldU2D8_4(U24ArrayTypeU2448_t896841276  value)
	{
		___U24U24fieldU2D8_4 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D9_5() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields, ___U24U24fieldU2D9_5)); }
	inline U24ArrayTypeU2464_t2866209746  get_U24U24fieldU2D9_5() const { return ___U24U24fieldU2D9_5; }
	inline U24ArrayTypeU2464_t2866209746 * get_address_of_U24U24fieldU2D9_5() { return &___U24U24fieldU2D9_5; }
	inline void set_U24U24fieldU2D9_5(U24ArrayTypeU2464_t2866209746  value)
	{
		___U24U24fieldU2D9_5 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D11_6() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields, ___U24U24fieldU2D11_6)); }
	inline U24ArrayTypeU2464_t2866209746  get_U24U24fieldU2D11_6() const { return ___U24U24fieldU2D11_6; }
	inline U24ArrayTypeU2464_t2866209746 * get_address_of_U24U24fieldU2D11_6() { return &___U24U24fieldU2D11_6; }
	inline void set_U24U24fieldU2D11_6(U24ArrayTypeU2464_t2866209746  value)
	{
		___U24U24fieldU2D11_6 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D12_7() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields, ___U24U24fieldU2D12_7)); }
	inline U24ArrayTypeU2464_t2866209746  get_U24U24fieldU2D12_7() const { return ___U24U24fieldU2D12_7; }
	inline U24ArrayTypeU2464_t2866209746 * get_address_of_U24U24fieldU2D12_7() { return &___U24U24fieldU2D12_7; }
	inline void set_U24U24fieldU2D12_7(U24ArrayTypeU2464_t2866209746  value)
	{
		___U24U24fieldU2D12_7 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D13_8() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields, ___U24U24fieldU2D13_8)); }
	inline U24ArrayTypeU2464_t2866209746  get_U24U24fieldU2D13_8() const { return ___U24U24fieldU2D13_8; }
	inline U24ArrayTypeU2464_t2866209746 * get_address_of_U24U24fieldU2D13_8() { return &___U24U24fieldU2D13_8; }
	inline void set_U24U24fieldU2D13_8(U24ArrayTypeU2464_t2866209746  value)
	{
		___U24U24fieldU2D13_8 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D14_9() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields, ___U24U24fieldU2D14_9)); }
	inline U24ArrayTypeU2412_t3672778805  get_U24U24fieldU2D14_9() const { return ___U24U24fieldU2D14_9; }
	inline U24ArrayTypeU2412_t3672778805 * get_address_of_U24U24fieldU2D14_9() { return &___U24U24fieldU2D14_9; }
	inline void set_U24U24fieldU2D14_9(U24ArrayTypeU2412_t3672778805  value)
	{
		___U24U24fieldU2D14_9 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D15_10() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields, ___U24U24fieldU2D15_10)); }
	inline U24ArrayTypeU2412_t3672778805  get_U24U24fieldU2D15_10() const { return ___U24U24fieldU2D15_10; }
	inline U24ArrayTypeU2412_t3672778805 * get_address_of_U24U24fieldU2D15_10() { return &___U24U24fieldU2D15_10; }
	inline void set_U24U24fieldU2D15_10(U24ArrayTypeU2412_t3672778805  value)
	{
		___U24U24fieldU2D15_10 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D16_11() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields, ___U24U24fieldU2D16_11)); }
	inline U24ArrayTypeU2412_t3672778805  get_U24U24fieldU2D16_11() const { return ___U24U24fieldU2D16_11; }
	inline U24ArrayTypeU2412_t3672778805 * get_address_of_U24U24fieldU2D16_11() { return &___U24U24fieldU2D16_11; }
	inline void set_U24U24fieldU2D16_11(U24ArrayTypeU2412_t3672778805  value)
	{
		___U24U24fieldU2D16_11 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D17_12() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields, ___U24U24fieldU2D17_12)); }
	inline U24ArrayTypeU2416_t1703410335  get_U24U24fieldU2D17_12() const { return ___U24U24fieldU2D17_12; }
	inline U24ArrayTypeU2416_t1703410335 * get_address_of_U24U24fieldU2D17_12() { return &___U24U24fieldU2D17_12; }
	inline void set_U24U24fieldU2D17_12(U24ArrayTypeU2416_t1703410335  value)
	{
		___U24U24fieldU2D17_12 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D21_13() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields, ___U24U24fieldU2D21_13)); }
	inline U24ArrayTypeU244_t1957337331  get_U24U24fieldU2D21_13() const { return ___U24U24fieldU2D21_13; }
	inline U24ArrayTypeU244_t1957337331 * get_address_of_U24U24fieldU2D21_13() { return &___U24U24fieldU2D21_13; }
	inline void set_U24U24fieldU2D21_13(U24ArrayTypeU244_t1957337331  value)
	{
		___U24U24fieldU2D21_13 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D22_14() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields, ___U24U24fieldU2D22_14)); }
	inline U24ArrayTypeU244_t1957337331  get_U24U24fieldU2D22_14() const { return ___U24U24fieldU2D22_14; }
	inline U24ArrayTypeU244_t1957337331 * get_address_of_U24U24fieldU2D22_14() { return &___U24U24fieldU2D22_14; }
	inline void set_U24U24fieldU2D22_14(U24ArrayTypeU244_t1957337331  value)
	{
		___U24U24fieldU2D22_14 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D23_15() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields, ___U24U24fieldU2D23_15)); }
	inline U24ArrayTypeU244_t1957337331  get_U24U24fieldU2D23_15() const { return ___U24U24fieldU2D23_15; }
	inline U24ArrayTypeU244_t1957337331 * get_address_of_U24U24fieldU2D23_15() { return &___U24U24fieldU2D23_15; }
	inline void set_U24U24fieldU2D23_15(U24ArrayTypeU244_t1957337331  value)
	{
		___U24U24fieldU2D23_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1486305139_H
#ifndef XPATHFUNCTIONLAST_T2734600875_H
#define XPATHFUNCTIONLAST_T2734600875_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathFunctionLast
struct  XPathFunctionLast_t2734600875  : public XPathFunction_t759167395
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHFUNCTIONLAST_T2734600875_H
#ifndef DATETIMEKIND_T2186819611_H
#define DATETIMEKIND_T2186819611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeKind
struct  DateTimeKind_t2186819611 
{
public:
	// System.Int32 System.DateTimeKind::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeKind_t2186819611, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEKIND_T2186819611_H
#ifndef XPATHFUNCTIONCOUNT_T711123082_H
#define XPATHFUNCTIONCOUNT_T711123082_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathFunctionCount
struct  XPathFunctionCount_t711123082  : public XPathFunction_t759167395
{
public:
	// System.Xml.XPath.Expression System.Xml.XPath.XPathFunctionCount::arg0
	Expression_t1283317256 * ___arg0_0;

public:
	inline static int32_t get_offset_of_arg0_0() { return static_cast<int32_t>(offsetof(XPathFunctionCount_t711123082, ___arg0_0)); }
	inline Expression_t1283317256 * get_arg0_0() const { return ___arg0_0; }
	inline Expression_t1283317256 ** get_address_of_arg0_0() { return &___arg0_0; }
	inline void set_arg0_0(Expression_t1283317256 * value)
	{
		___arg0_0 = value;
		Il2CppCodeGenWriteBarrier((&___arg0_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHFUNCTIONCOUNT_T711123082_H
#ifndef XPATHFUNCTIONPOSITION_T3711683936_H
#define XPATHFUNCTIONPOSITION_T3711683936_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathFunctionPosition
struct  XPathFunctionPosition_t3711683936  : public XPathFunction_t759167395
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHFUNCTIONPOSITION_T3711683936_H
#ifndef CIPHERMODE_T162592484_H
#define CIPHERMODE_T162592484_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.CipherMode
struct  CipherMode_t162592484 
{
public:
	// System.Int32 System.Security.Cryptography.CipherMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CipherMode_t162592484, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CIPHERMODE_T162592484_H
#ifndef YYUNEXPECTEDEOF_T3406522231_H
#define YYUNEXPECTEDEOF_T3406522231_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.XPath.yyParser.yyUnexpectedEof
struct  yyUnexpectedEof_t3406522231  : public yyException_t2814964437
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // YYUNEXPECTEDEOF_T3406522231_H
#ifndef PADDINGMODE_T3032142640_H
#define PADDINGMODE_T3032142640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.PaddingMode
struct  PaddingMode_t3032142640 
{
public:
	// System.Int32 System.Security.Cryptography.PaddingMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PaddingMode_t3032142640, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PADDINGMODE_T3032142640_H
#ifndef SYMMETRICALGORITHM_T1108166522_H
#define SYMMETRICALGORITHM_T1108166522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.SymmetricAlgorithm
struct  SymmetricAlgorithm_t1108166522  : public RuntimeObject
{
public:
	// System.Int32 System.Security.Cryptography.SymmetricAlgorithm::BlockSizeValue
	int32_t ___BlockSizeValue_0;
	// System.Byte[] System.Security.Cryptography.SymmetricAlgorithm::IVValue
	ByteU5BU5D_t3397334013* ___IVValue_1;
	// System.Int32 System.Security.Cryptography.SymmetricAlgorithm::KeySizeValue
	int32_t ___KeySizeValue_2;
	// System.Byte[] System.Security.Cryptography.SymmetricAlgorithm::KeyValue
	ByteU5BU5D_t3397334013* ___KeyValue_3;
	// System.Security.Cryptography.KeySizes[] System.Security.Cryptography.SymmetricAlgorithm::LegalBlockSizesValue
	KeySizesU5BU5D_t1153004758* ___LegalBlockSizesValue_4;
	// System.Security.Cryptography.KeySizes[] System.Security.Cryptography.SymmetricAlgorithm::LegalKeySizesValue
	KeySizesU5BU5D_t1153004758* ___LegalKeySizesValue_5;
	// System.Int32 System.Security.Cryptography.SymmetricAlgorithm::FeedbackSizeValue
	int32_t ___FeedbackSizeValue_6;
	// System.Security.Cryptography.CipherMode System.Security.Cryptography.SymmetricAlgorithm::ModeValue
	int32_t ___ModeValue_7;
	// System.Security.Cryptography.PaddingMode System.Security.Cryptography.SymmetricAlgorithm::PaddingValue
	int32_t ___PaddingValue_8;
	// System.Boolean System.Security.Cryptography.SymmetricAlgorithm::m_disposed
	bool ___m_disposed_9;

public:
	inline static int32_t get_offset_of_BlockSizeValue_0() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t1108166522, ___BlockSizeValue_0)); }
	inline int32_t get_BlockSizeValue_0() const { return ___BlockSizeValue_0; }
	inline int32_t* get_address_of_BlockSizeValue_0() { return &___BlockSizeValue_0; }
	inline void set_BlockSizeValue_0(int32_t value)
	{
		___BlockSizeValue_0 = value;
	}

	inline static int32_t get_offset_of_IVValue_1() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t1108166522, ___IVValue_1)); }
	inline ByteU5BU5D_t3397334013* get_IVValue_1() const { return ___IVValue_1; }
	inline ByteU5BU5D_t3397334013** get_address_of_IVValue_1() { return &___IVValue_1; }
	inline void set_IVValue_1(ByteU5BU5D_t3397334013* value)
	{
		___IVValue_1 = value;
		Il2CppCodeGenWriteBarrier((&___IVValue_1), value);
	}

	inline static int32_t get_offset_of_KeySizeValue_2() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t1108166522, ___KeySizeValue_2)); }
	inline int32_t get_KeySizeValue_2() const { return ___KeySizeValue_2; }
	inline int32_t* get_address_of_KeySizeValue_2() { return &___KeySizeValue_2; }
	inline void set_KeySizeValue_2(int32_t value)
	{
		___KeySizeValue_2 = value;
	}

	inline static int32_t get_offset_of_KeyValue_3() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t1108166522, ___KeyValue_3)); }
	inline ByteU5BU5D_t3397334013* get_KeyValue_3() const { return ___KeyValue_3; }
	inline ByteU5BU5D_t3397334013** get_address_of_KeyValue_3() { return &___KeyValue_3; }
	inline void set_KeyValue_3(ByteU5BU5D_t3397334013* value)
	{
		___KeyValue_3 = value;
		Il2CppCodeGenWriteBarrier((&___KeyValue_3), value);
	}

	inline static int32_t get_offset_of_LegalBlockSizesValue_4() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t1108166522, ___LegalBlockSizesValue_4)); }
	inline KeySizesU5BU5D_t1153004758* get_LegalBlockSizesValue_4() const { return ___LegalBlockSizesValue_4; }
	inline KeySizesU5BU5D_t1153004758** get_address_of_LegalBlockSizesValue_4() { return &___LegalBlockSizesValue_4; }
	inline void set_LegalBlockSizesValue_4(KeySizesU5BU5D_t1153004758* value)
	{
		___LegalBlockSizesValue_4 = value;
		Il2CppCodeGenWriteBarrier((&___LegalBlockSizesValue_4), value);
	}

	inline static int32_t get_offset_of_LegalKeySizesValue_5() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t1108166522, ___LegalKeySizesValue_5)); }
	inline KeySizesU5BU5D_t1153004758* get_LegalKeySizesValue_5() const { return ___LegalKeySizesValue_5; }
	inline KeySizesU5BU5D_t1153004758** get_address_of_LegalKeySizesValue_5() { return &___LegalKeySizesValue_5; }
	inline void set_LegalKeySizesValue_5(KeySizesU5BU5D_t1153004758* value)
	{
		___LegalKeySizesValue_5 = value;
		Il2CppCodeGenWriteBarrier((&___LegalKeySizesValue_5), value);
	}

	inline static int32_t get_offset_of_FeedbackSizeValue_6() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t1108166522, ___FeedbackSizeValue_6)); }
	inline int32_t get_FeedbackSizeValue_6() const { return ___FeedbackSizeValue_6; }
	inline int32_t* get_address_of_FeedbackSizeValue_6() { return &___FeedbackSizeValue_6; }
	inline void set_FeedbackSizeValue_6(int32_t value)
	{
		___FeedbackSizeValue_6 = value;
	}

	inline static int32_t get_offset_of_ModeValue_7() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t1108166522, ___ModeValue_7)); }
	inline int32_t get_ModeValue_7() const { return ___ModeValue_7; }
	inline int32_t* get_address_of_ModeValue_7() { return &___ModeValue_7; }
	inline void set_ModeValue_7(int32_t value)
	{
		___ModeValue_7 = value;
	}

	inline static int32_t get_offset_of_PaddingValue_8() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t1108166522, ___PaddingValue_8)); }
	inline int32_t get_PaddingValue_8() const { return ___PaddingValue_8; }
	inline int32_t* get_address_of_PaddingValue_8() { return &___PaddingValue_8; }
	inline void set_PaddingValue_8(int32_t value)
	{
		___PaddingValue_8 = value;
	}

	inline static int32_t get_offset_of_m_disposed_9() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t1108166522, ___m_disposed_9)); }
	inline bool get_m_disposed_9() const { return ___m_disposed_9; }
	inline bool* get_address_of_m_disposed_9() { return &___m_disposed_9; }
	inline void set_m_disposed_9(bool value)
	{
		___m_disposed_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYMMETRICALGORITHM_T1108166522_H
#ifndef MULTICASTDELEGATE_T3201952435_H
#define MULTICASTDELEGATE_T3201952435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t3201952435  : public Delegate_t3022476291
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t3201952435 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t3201952435 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___prev_9)); }
	inline MulticastDelegate_t3201952435 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t3201952435 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t3201952435 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___kpm_next_10)); }
	inline MulticastDelegate_t3201952435 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t3201952435 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t3201952435 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T3201952435_H
#ifndef DATETIME_T693205669_H
#define DATETIME_T693205669_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t693205669 
{
public:
	// System.TimeSpan System.DateTime::ticks
	TimeSpan_t3430258949  ___ticks_10;
	// System.DateTimeKind System.DateTime::kind
	int32_t ___kind_11;

public:
	inline static int32_t get_offset_of_ticks_10() { return static_cast<int32_t>(offsetof(DateTime_t693205669, ___ticks_10)); }
	inline TimeSpan_t3430258949  get_ticks_10() const { return ___ticks_10; }
	inline TimeSpan_t3430258949 * get_address_of_ticks_10() { return &___ticks_10; }
	inline void set_ticks_10(TimeSpan_t3430258949  value)
	{
		___ticks_10 = value;
	}

	inline static int32_t get_offset_of_kind_11() { return static_cast<int32_t>(offsetof(DateTime_t693205669, ___kind_11)); }
	inline int32_t get_kind_11() const { return ___kind_11; }
	inline int32_t* get_address_of_kind_11() { return &___kind_11; }
	inline void set_kind_11(int32_t value)
	{
		___kind_11 = value;
	}
};

struct DateTime_t693205669_StaticFields
{
public:
	// System.DateTime System.DateTime::MaxValue
	DateTime_t693205669  ___MaxValue_12;
	// System.DateTime System.DateTime::MinValue
	DateTime_t693205669  ___MinValue_13;
	// System.String[] System.DateTime::ParseTimeFormats
	StringU5BU5D_t1642385972* ___ParseTimeFormats_14;
	// System.String[] System.DateTime::ParseYearDayMonthFormats
	StringU5BU5D_t1642385972* ___ParseYearDayMonthFormats_15;
	// System.String[] System.DateTime::ParseYearMonthDayFormats
	StringU5BU5D_t1642385972* ___ParseYearMonthDayFormats_16;
	// System.String[] System.DateTime::ParseDayMonthYearFormats
	StringU5BU5D_t1642385972* ___ParseDayMonthYearFormats_17;
	// System.String[] System.DateTime::ParseMonthDayYearFormats
	StringU5BU5D_t1642385972* ___ParseMonthDayYearFormats_18;
	// System.String[] System.DateTime::MonthDayShortFormats
	StringU5BU5D_t1642385972* ___MonthDayShortFormats_19;
	// System.String[] System.DateTime::DayMonthShortFormats
	StringU5BU5D_t1642385972* ___DayMonthShortFormats_20;
	// System.Int32[] System.DateTime::daysmonth
	Int32U5BU5D_t3030399641* ___daysmonth_21;
	// System.Int32[] System.DateTime::daysmonthleap
	Int32U5BU5D_t3030399641* ___daysmonthleap_22;
	// System.Object System.DateTime::to_local_time_span_object
	RuntimeObject * ___to_local_time_span_object_23;
	// System.Int64 System.DateTime::last_now
	int64_t ___last_now_24;

public:
	inline static int32_t get_offset_of_MaxValue_12() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___MaxValue_12)); }
	inline DateTime_t693205669  get_MaxValue_12() const { return ___MaxValue_12; }
	inline DateTime_t693205669 * get_address_of_MaxValue_12() { return &___MaxValue_12; }
	inline void set_MaxValue_12(DateTime_t693205669  value)
	{
		___MaxValue_12 = value;
	}

	inline static int32_t get_offset_of_MinValue_13() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___MinValue_13)); }
	inline DateTime_t693205669  get_MinValue_13() const { return ___MinValue_13; }
	inline DateTime_t693205669 * get_address_of_MinValue_13() { return &___MinValue_13; }
	inline void set_MinValue_13(DateTime_t693205669  value)
	{
		___MinValue_13 = value;
	}

	inline static int32_t get_offset_of_ParseTimeFormats_14() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseTimeFormats_14)); }
	inline StringU5BU5D_t1642385972* get_ParseTimeFormats_14() const { return ___ParseTimeFormats_14; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseTimeFormats_14() { return &___ParseTimeFormats_14; }
	inline void set_ParseTimeFormats_14(StringU5BU5D_t1642385972* value)
	{
		___ParseTimeFormats_14 = value;
		Il2CppCodeGenWriteBarrier((&___ParseTimeFormats_14), value);
	}

	inline static int32_t get_offset_of_ParseYearDayMonthFormats_15() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseYearDayMonthFormats_15)); }
	inline StringU5BU5D_t1642385972* get_ParseYearDayMonthFormats_15() const { return ___ParseYearDayMonthFormats_15; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseYearDayMonthFormats_15() { return &___ParseYearDayMonthFormats_15; }
	inline void set_ParseYearDayMonthFormats_15(StringU5BU5D_t1642385972* value)
	{
		___ParseYearDayMonthFormats_15 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearDayMonthFormats_15), value);
	}

	inline static int32_t get_offset_of_ParseYearMonthDayFormats_16() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseYearMonthDayFormats_16)); }
	inline StringU5BU5D_t1642385972* get_ParseYearMonthDayFormats_16() const { return ___ParseYearMonthDayFormats_16; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseYearMonthDayFormats_16() { return &___ParseYearMonthDayFormats_16; }
	inline void set_ParseYearMonthDayFormats_16(StringU5BU5D_t1642385972* value)
	{
		___ParseYearMonthDayFormats_16 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearMonthDayFormats_16), value);
	}

	inline static int32_t get_offset_of_ParseDayMonthYearFormats_17() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseDayMonthYearFormats_17)); }
	inline StringU5BU5D_t1642385972* get_ParseDayMonthYearFormats_17() const { return ___ParseDayMonthYearFormats_17; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseDayMonthYearFormats_17() { return &___ParseDayMonthYearFormats_17; }
	inline void set_ParseDayMonthYearFormats_17(StringU5BU5D_t1642385972* value)
	{
		___ParseDayMonthYearFormats_17 = value;
		Il2CppCodeGenWriteBarrier((&___ParseDayMonthYearFormats_17), value);
	}

	inline static int32_t get_offset_of_ParseMonthDayYearFormats_18() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseMonthDayYearFormats_18)); }
	inline StringU5BU5D_t1642385972* get_ParseMonthDayYearFormats_18() const { return ___ParseMonthDayYearFormats_18; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseMonthDayYearFormats_18() { return &___ParseMonthDayYearFormats_18; }
	inline void set_ParseMonthDayYearFormats_18(StringU5BU5D_t1642385972* value)
	{
		___ParseMonthDayYearFormats_18 = value;
		Il2CppCodeGenWriteBarrier((&___ParseMonthDayYearFormats_18), value);
	}

	inline static int32_t get_offset_of_MonthDayShortFormats_19() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___MonthDayShortFormats_19)); }
	inline StringU5BU5D_t1642385972* get_MonthDayShortFormats_19() const { return ___MonthDayShortFormats_19; }
	inline StringU5BU5D_t1642385972** get_address_of_MonthDayShortFormats_19() { return &___MonthDayShortFormats_19; }
	inline void set_MonthDayShortFormats_19(StringU5BU5D_t1642385972* value)
	{
		___MonthDayShortFormats_19 = value;
		Il2CppCodeGenWriteBarrier((&___MonthDayShortFormats_19), value);
	}

	inline static int32_t get_offset_of_DayMonthShortFormats_20() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___DayMonthShortFormats_20)); }
	inline StringU5BU5D_t1642385972* get_DayMonthShortFormats_20() const { return ___DayMonthShortFormats_20; }
	inline StringU5BU5D_t1642385972** get_address_of_DayMonthShortFormats_20() { return &___DayMonthShortFormats_20; }
	inline void set_DayMonthShortFormats_20(StringU5BU5D_t1642385972* value)
	{
		___DayMonthShortFormats_20 = value;
		Il2CppCodeGenWriteBarrier((&___DayMonthShortFormats_20), value);
	}

	inline static int32_t get_offset_of_daysmonth_21() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___daysmonth_21)); }
	inline Int32U5BU5D_t3030399641* get_daysmonth_21() const { return ___daysmonth_21; }
	inline Int32U5BU5D_t3030399641** get_address_of_daysmonth_21() { return &___daysmonth_21; }
	inline void set_daysmonth_21(Int32U5BU5D_t3030399641* value)
	{
		___daysmonth_21 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonth_21), value);
	}

	inline static int32_t get_offset_of_daysmonthleap_22() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___daysmonthleap_22)); }
	inline Int32U5BU5D_t3030399641* get_daysmonthleap_22() const { return ___daysmonthleap_22; }
	inline Int32U5BU5D_t3030399641** get_address_of_daysmonthleap_22() { return &___daysmonthleap_22; }
	inline void set_daysmonthleap_22(Int32U5BU5D_t3030399641* value)
	{
		___daysmonthleap_22 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonthleap_22), value);
	}

	inline static int32_t get_offset_of_to_local_time_span_object_23() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___to_local_time_span_object_23)); }
	inline RuntimeObject * get_to_local_time_span_object_23() const { return ___to_local_time_span_object_23; }
	inline RuntimeObject ** get_address_of_to_local_time_span_object_23() { return &___to_local_time_span_object_23; }
	inline void set_to_local_time_span_object_23(RuntimeObject * value)
	{
		___to_local_time_span_object_23 = value;
		Il2CppCodeGenWriteBarrier((&___to_local_time_span_object_23), value);
	}

	inline static int32_t get_offset_of_last_now_24() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___last_now_24)); }
	inline int64_t get_last_now_24() const { return ___last_now_24; }
	inline int64_t* get_address_of_last_now_24() { return &___last_now_24; }
	inline void set_last_now_24(int64_t value)
	{
		___last_now_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T693205669_H
#ifndef UDPRECEIVER_T3846109956_H
#define UDPRECEIVER_T3846109956_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OSCsharp.Net.UDPReceiver
struct  UDPReceiver_t3846109956  : public RuntimeObject
{
public:
	// System.EventHandler`1<OSCsharp.Net.OscPacketReceivedEventArgs> OSCsharp.Net.UDPReceiver::PacketReceived
	EventHandler_1_t1873352441 * ___PacketReceived_0;
	// System.EventHandler`1<OSCsharp.Net.OscBundleReceivedEventArgs> OSCsharp.Net.UDPReceiver::BundleReceived
	EventHandler_1_t2264531613 * ___BundleReceived_1;
	// System.EventHandler`1<OSCsharp.Utils.ExceptionEventArgs> OSCsharp.Net.UDPReceiver::ErrorOccured
	EventHandler_1_t3778988004 * ___ErrorOccured_2;
	// System.EventHandler`1<OSCsharp.Net.OscMessageReceivedEventArgs> OSCsharp.Net.UDPReceiver::messageReceivedInvoker
	EventHandler_1_t4149316328 * ___messageReceivedInvoker_3;
	// System.Net.Sockets.UdpClient OSCsharp.Net.UDPReceiver::udpClient
	UdpClient_t1278197702 * ___udpClient_4;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) OSCsharp.Net.UDPReceiver::acceptingConnections
	bool ___acceptingConnections_5;
	// System.AsyncCallback OSCsharp.Net.UDPReceiver::callback
	AsyncCallback_t163412349 * ___callback_6;
	// System.Net.IPAddress OSCsharp.Net.UDPReceiver::<IPAddress>k__BackingField
	IPAddress_t1399971723 * ___U3CIPAddressU3Ek__BackingField_7;
	// System.Int32 OSCsharp.Net.UDPReceiver::<Port>k__BackingField
	int32_t ___U3CPortU3Ek__BackingField_8;
	// System.Net.IPAddress OSCsharp.Net.UDPReceiver::<MulticastAddress>k__BackingField
	IPAddress_t1399971723 * ___U3CMulticastAddressU3Ek__BackingField_9;
	// System.Net.IPEndPoint OSCsharp.Net.UDPReceiver::<IPEndPoint>k__BackingField
	IPEndPoint_t2615413766 * ___U3CIPEndPointU3Ek__BackingField_10;
	// OSCsharp.Net.TransmissionType OSCsharp.Net.UDPReceiver::<TransmissionType>k__BackingField
	int32_t ___U3CTransmissionTypeU3Ek__BackingField_11;
	// System.Boolean OSCsharp.Net.UDPReceiver::<ConsumeParsingExceptions>k__BackingField
	bool ___U3CConsumeParsingExceptionsU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_PacketReceived_0() { return static_cast<int32_t>(offsetof(UDPReceiver_t3846109956, ___PacketReceived_0)); }
	inline EventHandler_1_t1873352441 * get_PacketReceived_0() const { return ___PacketReceived_0; }
	inline EventHandler_1_t1873352441 ** get_address_of_PacketReceived_0() { return &___PacketReceived_0; }
	inline void set_PacketReceived_0(EventHandler_1_t1873352441 * value)
	{
		___PacketReceived_0 = value;
		Il2CppCodeGenWriteBarrier((&___PacketReceived_0), value);
	}

	inline static int32_t get_offset_of_BundleReceived_1() { return static_cast<int32_t>(offsetof(UDPReceiver_t3846109956, ___BundleReceived_1)); }
	inline EventHandler_1_t2264531613 * get_BundleReceived_1() const { return ___BundleReceived_1; }
	inline EventHandler_1_t2264531613 ** get_address_of_BundleReceived_1() { return &___BundleReceived_1; }
	inline void set_BundleReceived_1(EventHandler_1_t2264531613 * value)
	{
		___BundleReceived_1 = value;
		Il2CppCodeGenWriteBarrier((&___BundleReceived_1), value);
	}

	inline static int32_t get_offset_of_ErrorOccured_2() { return static_cast<int32_t>(offsetof(UDPReceiver_t3846109956, ___ErrorOccured_2)); }
	inline EventHandler_1_t3778988004 * get_ErrorOccured_2() const { return ___ErrorOccured_2; }
	inline EventHandler_1_t3778988004 ** get_address_of_ErrorOccured_2() { return &___ErrorOccured_2; }
	inline void set_ErrorOccured_2(EventHandler_1_t3778988004 * value)
	{
		___ErrorOccured_2 = value;
		Il2CppCodeGenWriteBarrier((&___ErrorOccured_2), value);
	}

	inline static int32_t get_offset_of_messageReceivedInvoker_3() { return static_cast<int32_t>(offsetof(UDPReceiver_t3846109956, ___messageReceivedInvoker_3)); }
	inline EventHandler_1_t4149316328 * get_messageReceivedInvoker_3() const { return ___messageReceivedInvoker_3; }
	inline EventHandler_1_t4149316328 ** get_address_of_messageReceivedInvoker_3() { return &___messageReceivedInvoker_3; }
	inline void set_messageReceivedInvoker_3(EventHandler_1_t4149316328 * value)
	{
		___messageReceivedInvoker_3 = value;
		Il2CppCodeGenWriteBarrier((&___messageReceivedInvoker_3), value);
	}

	inline static int32_t get_offset_of_udpClient_4() { return static_cast<int32_t>(offsetof(UDPReceiver_t3846109956, ___udpClient_4)); }
	inline UdpClient_t1278197702 * get_udpClient_4() const { return ___udpClient_4; }
	inline UdpClient_t1278197702 ** get_address_of_udpClient_4() { return &___udpClient_4; }
	inline void set_udpClient_4(UdpClient_t1278197702 * value)
	{
		___udpClient_4 = value;
		Il2CppCodeGenWriteBarrier((&___udpClient_4), value);
	}

	inline static int32_t get_offset_of_acceptingConnections_5() { return static_cast<int32_t>(offsetof(UDPReceiver_t3846109956, ___acceptingConnections_5)); }
	inline bool get_acceptingConnections_5() const { return ___acceptingConnections_5; }
	inline bool* get_address_of_acceptingConnections_5() { return &___acceptingConnections_5; }
	inline void set_acceptingConnections_5(bool value)
	{
		___acceptingConnections_5 = value;
	}

	inline static int32_t get_offset_of_callback_6() { return static_cast<int32_t>(offsetof(UDPReceiver_t3846109956, ___callback_6)); }
	inline AsyncCallback_t163412349 * get_callback_6() const { return ___callback_6; }
	inline AsyncCallback_t163412349 ** get_address_of_callback_6() { return &___callback_6; }
	inline void set_callback_6(AsyncCallback_t163412349 * value)
	{
		___callback_6 = value;
		Il2CppCodeGenWriteBarrier((&___callback_6), value);
	}

	inline static int32_t get_offset_of_U3CIPAddressU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(UDPReceiver_t3846109956, ___U3CIPAddressU3Ek__BackingField_7)); }
	inline IPAddress_t1399971723 * get_U3CIPAddressU3Ek__BackingField_7() const { return ___U3CIPAddressU3Ek__BackingField_7; }
	inline IPAddress_t1399971723 ** get_address_of_U3CIPAddressU3Ek__BackingField_7() { return &___U3CIPAddressU3Ek__BackingField_7; }
	inline void set_U3CIPAddressU3Ek__BackingField_7(IPAddress_t1399971723 * value)
	{
		___U3CIPAddressU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CIPAddressU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CPortU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(UDPReceiver_t3846109956, ___U3CPortU3Ek__BackingField_8)); }
	inline int32_t get_U3CPortU3Ek__BackingField_8() const { return ___U3CPortU3Ek__BackingField_8; }
	inline int32_t* get_address_of_U3CPortU3Ek__BackingField_8() { return &___U3CPortU3Ek__BackingField_8; }
	inline void set_U3CPortU3Ek__BackingField_8(int32_t value)
	{
		___U3CPortU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CMulticastAddressU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(UDPReceiver_t3846109956, ___U3CMulticastAddressU3Ek__BackingField_9)); }
	inline IPAddress_t1399971723 * get_U3CMulticastAddressU3Ek__BackingField_9() const { return ___U3CMulticastAddressU3Ek__BackingField_9; }
	inline IPAddress_t1399971723 ** get_address_of_U3CMulticastAddressU3Ek__BackingField_9() { return &___U3CMulticastAddressU3Ek__BackingField_9; }
	inline void set_U3CMulticastAddressU3Ek__BackingField_9(IPAddress_t1399971723 * value)
	{
		___U3CMulticastAddressU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMulticastAddressU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CIPEndPointU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(UDPReceiver_t3846109956, ___U3CIPEndPointU3Ek__BackingField_10)); }
	inline IPEndPoint_t2615413766 * get_U3CIPEndPointU3Ek__BackingField_10() const { return ___U3CIPEndPointU3Ek__BackingField_10; }
	inline IPEndPoint_t2615413766 ** get_address_of_U3CIPEndPointU3Ek__BackingField_10() { return &___U3CIPEndPointU3Ek__BackingField_10; }
	inline void set_U3CIPEndPointU3Ek__BackingField_10(IPEndPoint_t2615413766 * value)
	{
		___U3CIPEndPointU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CIPEndPointU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_U3CTransmissionTypeU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(UDPReceiver_t3846109956, ___U3CTransmissionTypeU3Ek__BackingField_11)); }
	inline int32_t get_U3CTransmissionTypeU3Ek__BackingField_11() const { return ___U3CTransmissionTypeU3Ek__BackingField_11; }
	inline int32_t* get_address_of_U3CTransmissionTypeU3Ek__BackingField_11() { return &___U3CTransmissionTypeU3Ek__BackingField_11; }
	inline void set_U3CTransmissionTypeU3Ek__BackingField_11(int32_t value)
	{
		___U3CTransmissionTypeU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CConsumeParsingExceptionsU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(UDPReceiver_t3846109956, ___U3CConsumeParsingExceptionsU3Ek__BackingField_12)); }
	inline bool get_U3CConsumeParsingExceptionsU3Ek__BackingField_12() const { return ___U3CConsumeParsingExceptionsU3Ek__BackingField_12; }
	inline bool* get_address_of_U3CConsumeParsingExceptionsU3Ek__BackingField_12() { return &___U3CConsumeParsingExceptionsU3Ek__BackingField_12; }
	inline void set_U3CConsumeParsingExceptionsU3Ek__BackingField_12(bool value)
	{
		___U3CConsumeParsingExceptionsU3Ek__BackingField_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UDPRECEIVER_T3846109956_H
#ifndef HANDSHAKEMESSAGE_T3938752374_H
#define HANDSHAKEMESSAGE_T3938752374_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.Handshake.HandshakeMessage
struct  HandshakeMessage_t3938752374  : public TlsStream_t4089752859
{
public:
	// Mono.Security.Protocol.Tls.Context Mono.Security.Protocol.Tls.Handshake.HandshakeMessage::context
	Context_t4285182719 * ___context_5;
	// Mono.Security.Protocol.Tls.Handshake.HandshakeType Mono.Security.Protocol.Tls.Handshake.HandshakeMessage::handshakeType
	uint8_t ___handshakeType_6;
	// Mono.Security.Protocol.Tls.ContentType Mono.Security.Protocol.Tls.Handshake.HandshakeMessage::contentType
	uint8_t ___contentType_7;
	// System.Byte[] Mono.Security.Protocol.Tls.Handshake.HandshakeMessage::cache
	ByteU5BU5D_t3397334013* ___cache_8;

public:
	inline static int32_t get_offset_of_context_5() { return static_cast<int32_t>(offsetof(HandshakeMessage_t3938752374, ___context_5)); }
	inline Context_t4285182719 * get_context_5() const { return ___context_5; }
	inline Context_t4285182719 ** get_address_of_context_5() { return &___context_5; }
	inline void set_context_5(Context_t4285182719 * value)
	{
		___context_5 = value;
		Il2CppCodeGenWriteBarrier((&___context_5), value);
	}

	inline static int32_t get_offset_of_handshakeType_6() { return static_cast<int32_t>(offsetof(HandshakeMessage_t3938752374, ___handshakeType_6)); }
	inline uint8_t get_handshakeType_6() const { return ___handshakeType_6; }
	inline uint8_t* get_address_of_handshakeType_6() { return &___handshakeType_6; }
	inline void set_handshakeType_6(uint8_t value)
	{
		___handshakeType_6 = value;
	}

	inline static int32_t get_offset_of_contentType_7() { return static_cast<int32_t>(offsetof(HandshakeMessage_t3938752374, ___contentType_7)); }
	inline uint8_t get_contentType_7() const { return ___contentType_7; }
	inline uint8_t* get_address_of_contentType_7() { return &___contentType_7; }
	inline void set_contentType_7(uint8_t value)
	{
		___contentType_7 = value;
	}

	inline static int32_t get_offset_of_cache_8() { return static_cast<int32_t>(offsetof(HandshakeMessage_t3938752374, ___cache_8)); }
	inline ByteU5BU5D_t3397334013* get_cache_8() const { return ___cache_8; }
	inline ByteU5BU5D_t3397334013** get_address_of_cache_8() { return &___cache_8; }
	inline void set_cache_8(ByteU5BU5D_t3397334013* value)
	{
		___cache_8 = value;
		Il2CppCodeGenWriteBarrier((&___cache_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HANDSHAKEMESSAGE_T3938752374_H
#ifndef TLSSERVERCERTIFICATE_T304799309_H
#define TLSSERVERCERTIFICATE_T304799309_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.Handshake.Server.TlsServerCertificate
struct  TlsServerCertificate_t304799309  : public HandshakeMessage_t3938752374
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSSERVERCERTIFICATE_T304799309_H
#ifndef TLSCLIENTKEYEXCHANGE_T835272394_H
#define TLSCLIENTKEYEXCHANGE_T835272394_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.Handshake.Server.TlsClientKeyExchange
struct  TlsClientKeyExchange_t835272394  : public HandshakeMessage_t3938752374
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSCLIENTKEYEXCHANGE_T835272394_H
#ifndef TLSSERVERCERTIFICATEREQUEST_T1805368644_H
#define TLSSERVERCERTIFICATEREQUEST_T1805368644_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.Handshake.Server.TlsServerCertificateRequest
struct  TlsServerCertificateRequest_t1805368644  : public HandshakeMessage_t3938752374
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSSERVERCERTIFICATEREQUEST_T1805368644_H
#ifndef TLSSERVERHELLO_T1964679492_H
#define TLSSERVERHELLO_T1964679492_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.Handshake.Server.TlsServerHello
struct  TlsServerHello_t1964679492  : public HandshakeMessage_t3938752374
{
public:
	// System.Int32 Mono.Security.Protocol.Tls.Handshake.Server.TlsServerHello::unixTime
	int32_t ___unixTime_9;
	// System.Byte[] Mono.Security.Protocol.Tls.Handshake.Server.TlsServerHello::random
	ByteU5BU5D_t3397334013* ___random_10;

public:
	inline static int32_t get_offset_of_unixTime_9() { return static_cast<int32_t>(offsetof(TlsServerHello_t1964679492, ___unixTime_9)); }
	inline int32_t get_unixTime_9() const { return ___unixTime_9; }
	inline int32_t* get_address_of_unixTime_9() { return &___unixTime_9; }
	inline void set_unixTime_9(int32_t value)
	{
		___unixTime_9 = value;
	}

	inline static int32_t get_offset_of_random_10() { return static_cast<int32_t>(offsetof(TlsServerHello_t1964679492, ___random_10)); }
	inline ByteU5BU5D_t3397334013* get_random_10() const { return ___random_10; }
	inline ByteU5BU5D_t3397334013** get_address_of_random_10() { return &___random_10; }
	inline void set_random_10(ByteU5BU5D_t3397334013* value)
	{
		___random_10 = value;
		Il2CppCodeGenWriteBarrier((&___random_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSSERVERHELLO_T1964679492_H
#ifndef TLSSERVERFINISHED_T1739223742_H
#define TLSSERVERFINISHED_T1739223742_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.Handshake.Server.TlsServerFinished
struct  TlsServerFinished_t1739223742  : public HandshakeMessage_t3938752374
{
public:

public:
};

struct TlsServerFinished_t1739223742_StaticFields
{
public:
	// System.Byte[] Mono.Security.Protocol.Tls.Handshake.Server.TlsServerFinished::Ssl3Marker
	ByteU5BU5D_t3397334013* ___Ssl3Marker_9;

public:
	inline static int32_t get_offset_of_Ssl3Marker_9() { return static_cast<int32_t>(offsetof(TlsServerFinished_t1739223742_StaticFields, ___Ssl3Marker_9)); }
	inline ByteU5BU5D_t3397334013* get_Ssl3Marker_9() const { return ___Ssl3Marker_9; }
	inline ByteU5BU5D_t3397334013** get_address_of_Ssl3Marker_9() { return &___Ssl3Marker_9; }
	inline void set_Ssl3Marker_9(ByteU5BU5D_t3397334013* value)
	{
		___Ssl3Marker_9 = value;
		Il2CppCodeGenWriteBarrier((&___Ssl3Marker_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSSERVERFINISHED_T1739223742_H
#ifndef TLSCLIENTHELLO_T362473704_H
#define TLSCLIENTHELLO_T362473704_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.Handshake.Server.TlsClientHello
struct  TlsClientHello_t362473704  : public HandshakeMessage_t3938752374
{
public:
	// System.Byte[] Mono.Security.Protocol.Tls.Handshake.Server.TlsClientHello::random
	ByteU5BU5D_t3397334013* ___random_9;
	// System.Byte[] Mono.Security.Protocol.Tls.Handshake.Server.TlsClientHello::sessionId
	ByteU5BU5D_t3397334013* ___sessionId_10;
	// System.Int16[] Mono.Security.Protocol.Tls.Handshake.Server.TlsClientHello::cipherSuites
	Int16U5BU5D_t3104283263* ___cipherSuites_11;
	// System.Byte[] Mono.Security.Protocol.Tls.Handshake.Server.TlsClientHello::compressionMethods
	ByteU5BU5D_t3397334013* ___compressionMethods_12;

public:
	inline static int32_t get_offset_of_random_9() { return static_cast<int32_t>(offsetof(TlsClientHello_t362473704, ___random_9)); }
	inline ByteU5BU5D_t3397334013* get_random_9() const { return ___random_9; }
	inline ByteU5BU5D_t3397334013** get_address_of_random_9() { return &___random_9; }
	inline void set_random_9(ByteU5BU5D_t3397334013* value)
	{
		___random_9 = value;
		Il2CppCodeGenWriteBarrier((&___random_9), value);
	}

	inline static int32_t get_offset_of_sessionId_10() { return static_cast<int32_t>(offsetof(TlsClientHello_t362473704, ___sessionId_10)); }
	inline ByteU5BU5D_t3397334013* get_sessionId_10() const { return ___sessionId_10; }
	inline ByteU5BU5D_t3397334013** get_address_of_sessionId_10() { return &___sessionId_10; }
	inline void set_sessionId_10(ByteU5BU5D_t3397334013* value)
	{
		___sessionId_10 = value;
		Il2CppCodeGenWriteBarrier((&___sessionId_10), value);
	}

	inline static int32_t get_offset_of_cipherSuites_11() { return static_cast<int32_t>(offsetof(TlsClientHello_t362473704, ___cipherSuites_11)); }
	inline Int16U5BU5D_t3104283263* get_cipherSuites_11() const { return ___cipherSuites_11; }
	inline Int16U5BU5D_t3104283263** get_address_of_cipherSuites_11() { return &___cipherSuites_11; }
	inline void set_cipherSuites_11(Int16U5BU5D_t3104283263* value)
	{
		___cipherSuites_11 = value;
		Il2CppCodeGenWriteBarrier((&___cipherSuites_11), value);
	}

	inline static int32_t get_offset_of_compressionMethods_12() { return static_cast<int32_t>(offsetof(TlsClientHello_t362473704, ___compressionMethods_12)); }
	inline ByteU5BU5D_t3397334013* get_compressionMethods_12() const { return ___compressionMethods_12; }
	inline ByteU5BU5D_t3397334013** get_address_of_compressionMethods_12() { return &___compressionMethods_12; }
	inline void set_compressionMethods_12(ByteU5BU5D_t3397334013* value)
	{
		___compressionMethods_12 = value;
		Il2CppCodeGenWriteBarrier((&___compressionMethods_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSCLIENTHELLO_T362473704_H
#ifndef TLSSERVERKEYEXCHANGE_T2172608670_H
#define TLSSERVERKEYEXCHANGE_T2172608670_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerKeyExchange
struct  TlsServerKeyExchange_t2172608670  : public HandshakeMessage_t3938752374
{
public:
	// System.Security.Cryptography.RSAParameters Mono.Security.Protocol.Tls.Handshake.Client.TlsServerKeyExchange::rsaParams
	RSAParameters_t1462703416  ___rsaParams_9;
	// System.Byte[] Mono.Security.Protocol.Tls.Handshake.Client.TlsServerKeyExchange::signedParams
	ByteU5BU5D_t3397334013* ___signedParams_10;

public:
	inline static int32_t get_offset_of_rsaParams_9() { return static_cast<int32_t>(offsetof(TlsServerKeyExchange_t2172608670, ___rsaParams_9)); }
	inline RSAParameters_t1462703416  get_rsaParams_9() const { return ___rsaParams_9; }
	inline RSAParameters_t1462703416 * get_address_of_rsaParams_9() { return &___rsaParams_9; }
	inline void set_rsaParams_9(RSAParameters_t1462703416  value)
	{
		___rsaParams_9 = value;
	}

	inline static int32_t get_offset_of_signedParams_10() { return static_cast<int32_t>(offsetof(TlsServerKeyExchange_t2172608670, ___signedParams_10)); }
	inline ByteU5BU5D_t3397334013* get_signedParams_10() const { return ___signedParams_10; }
	inline ByteU5BU5D_t3397334013** get_address_of_signedParams_10() { return &___signedParams_10; }
	inline void set_signedParams_10(ByteU5BU5D_t3397334013* value)
	{
		___signedParams_10 = value;
		Il2CppCodeGenWriteBarrier((&___signedParams_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSSERVERKEYEXCHANGE_T2172608670_H
#ifndef TLSSERVERHELLODONE_T530021076_H
#define TLSSERVERHELLODONE_T530021076_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHelloDone
struct  TlsServerHelloDone_t530021076  : public HandshakeMessage_t3938752374
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSSERVERHELLODONE_T530021076_H
#ifndef TLSCLIENTCERTIFICATE_T3862719489_H
#define TLSCLIENTCERTIFICATE_T3862719489_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.Handshake.Server.TlsClientCertificate
struct  TlsClientCertificate_t3862719489  : public HandshakeMessage_t3938752374
{
public:
	// Mono.Security.X509.X509CertificateCollection Mono.Security.Protocol.Tls.Handshake.Server.TlsClientCertificate::clientCertificates
	X509CertificateCollection_t3592472866 * ___clientCertificates_9;

public:
	inline static int32_t get_offset_of_clientCertificates_9() { return static_cast<int32_t>(offsetof(TlsClientCertificate_t3862719489, ___clientCertificates_9)); }
	inline X509CertificateCollection_t3592472866 * get_clientCertificates_9() const { return ___clientCertificates_9; }
	inline X509CertificateCollection_t3592472866 ** get_address_of_clientCertificates_9() { return &___clientCertificates_9; }
	inline void set_clientCertificates_9(X509CertificateCollection_t3592472866 * value)
	{
		___clientCertificates_9 = value;
		Il2CppCodeGenWriteBarrier((&___clientCertificates_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSCLIENTCERTIFICATE_T3862719489_H
#ifndef TLSCLIENTFINISHED_T2485700522_H
#define TLSCLIENTFINISHED_T2485700522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.Handshake.Server.TlsClientFinished
struct  TlsClientFinished_t2485700522  : public HandshakeMessage_t3938752374
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSCLIENTFINISHED_T2485700522_H
#ifndef TLSCLIENTCERTIFICATEVERIFY_T4146114522_H
#define TLSCLIENTCERTIFICATEVERIFY_T4146114522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.Handshake.Server.TlsClientCertificateVerify
struct  TlsClientCertificateVerify_t4146114522  : public HandshakeMessage_t3938752374
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSCLIENTCERTIFICATEVERIFY_T4146114522_H
#ifndef PRIVATEKEYSELECTIONCALLBACK_T1663566523_H
#define PRIVATEKEYSELECTIONCALLBACK_T1663566523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.PrivateKeySelectionCallback
struct  PrivateKeySelectionCallback_t1663566523  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRIVATEKEYSELECTIONCALLBACK_T1663566523_H
#ifndef CERTIFICATESELECTIONCALLBACK_T3721235490_H
#define CERTIFICATESELECTIONCALLBACK_T3721235490_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.CertificateSelectionCallback
struct  CertificateSelectionCallback_t3721235490  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CERTIFICATESELECTIONCALLBACK_T3721235490_H
#ifndef AES_T2354947465_H
#define AES_T2354947465_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.Aes
struct  Aes_t2354947465  : public SymmetricAlgorithm_t1108166522
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AES_T2354947465_H
#ifndef OSCTIMETAG_T625345318_H
#define OSCTIMETAG_T625345318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OSCsharp.Data.OscTimeTag
struct  OscTimeTag_t625345318  : public RuntimeObject
{
public:
	// System.DateTime OSCsharp.Data.OscTimeTag::timeStamp
	DateTime_t693205669  ___timeStamp_2;

public:
	inline static int32_t get_offset_of_timeStamp_2() { return static_cast<int32_t>(offsetof(OscTimeTag_t625345318, ___timeStamp_2)); }
	inline DateTime_t693205669  get_timeStamp_2() const { return ___timeStamp_2; }
	inline DateTime_t693205669 * get_address_of_timeStamp_2() { return &___timeStamp_2; }
	inline void set_timeStamp_2(DateTime_t693205669  value)
	{
		___timeStamp_2 = value;
	}
};

struct OscTimeTag_t625345318_StaticFields
{
public:
	// System.DateTime OSCsharp.Data.OscTimeTag::Epoch
	DateTime_t693205669  ___Epoch_0;
	// OSCsharp.Data.OscTimeTag OSCsharp.Data.OscTimeTag::MinValue
	OscTimeTag_t625345318 * ___MinValue_1;

public:
	inline static int32_t get_offset_of_Epoch_0() { return static_cast<int32_t>(offsetof(OscTimeTag_t625345318_StaticFields, ___Epoch_0)); }
	inline DateTime_t693205669  get_Epoch_0() const { return ___Epoch_0; }
	inline DateTime_t693205669 * get_address_of_Epoch_0() { return &___Epoch_0; }
	inline void set_Epoch_0(DateTime_t693205669  value)
	{
		___Epoch_0 = value;
	}

	inline static int32_t get_offset_of_MinValue_1() { return static_cast<int32_t>(offsetof(OscTimeTag_t625345318_StaticFields, ___MinValue_1)); }
	inline OscTimeTag_t625345318 * get_MinValue_1() const { return ___MinValue_1; }
	inline OscTimeTag_t625345318 ** get_address_of_MinValue_1() { return &___MinValue_1; }
	inline void set_MinValue_1(OscTimeTag_t625345318 * value)
	{
		___MinValue_1 = value;
		Il2CppCodeGenWriteBarrier((&___MinValue_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OSCTIMETAG_T625345318_H
#ifndef ACTION_T3226471752_H
#define ACTION_T3226471752_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action
struct  Action_t3226471752  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_T3226471752_H
#ifndef TLSSERVERKEYEXCHANGE_T115542990_H
#define TLSSERVERKEYEXCHANGE_T115542990_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.Handshake.Server.TlsServerKeyExchange
struct  TlsServerKeyExchange_t115542990  : public HandshakeMessage_t3938752374
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSSERVERKEYEXCHANGE_T115542990_H
#ifndef TLSSERVERHELLODONE_T2766712588_H
#define TLSSERVERHELLODONE_T2766712588_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.Handshake.Server.TlsServerHelloDone
struct  TlsServerHelloDone_t2766712588  : public HandshakeMessage_t3938752374
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSSERVERHELLODONE_T2766712588_H
#ifndef PRIMALITYTEST_T572679902_H
#define PRIMALITYTEST_T572679902_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Math.Prime.PrimalityTest
struct  PrimalityTest_t572679902  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRIMALITYTEST_T572679902_H
#ifndef CERTIFICATEVALIDATIONCALLBACK2_T3318447433_H
#define CERTIFICATEVALIDATIONCALLBACK2_T3318447433_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.CertificateValidationCallback2
struct  CertificateValidationCallback2_t3318447433  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CERTIFICATEVALIDATIONCALLBACK2_T3318447433_H
#ifndef CERTIFICATEVALIDATIONCALLBACK_T989458295_H
#define CERTIFICATEVALIDATIONCALLBACK_T989458295_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.CertificateValidationCallback
struct  CertificateValidationCallback_t989458295  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CERTIFICATEVALIDATIONCALLBACK_T989458295_H
#ifndef AESMANAGED_T3721278648_H
#define AESMANAGED_T3721278648_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.AesManaged
struct  AesManaged_t3721278648  : public Aes_t2354947465
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AESMANAGED_T3721278648_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1300 = { sizeof (TlsServerHelloDone_t530021076), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1301 = { sizeof (TlsServerKeyExchange_t2172608670), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1301[2] = 
{
	TlsServerKeyExchange_t2172608670::get_offset_of_rsaParams_9(),
	TlsServerKeyExchange_t2172608670::get_offset_of_signedParams_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1302 = { sizeof (TlsClientCertificate_t3862719489), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1302[1] = 
{
	TlsClientCertificate_t3862719489::get_offset_of_clientCertificates_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1303 = { sizeof (TlsClientCertificateVerify_t4146114522), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1304 = { sizeof (TlsClientFinished_t2485700522), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1305 = { sizeof (TlsClientHello_t362473704), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1305[4] = 
{
	TlsClientHello_t362473704::get_offset_of_random_9(),
	TlsClientHello_t362473704::get_offset_of_sessionId_10(),
	TlsClientHello_t362473704::get_offset_of_cipherSuites_11(),
	TlsClientHello_t362473704::get_offset_of_compressionMethods_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1306 = { sizeof (TlsClientKeyExchange_t835272394), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1307 = { sizeof (TlsServerCertificate_t304799309), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1308 = { sizeof (TlsServerCertificateRequest_t1805368644), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1309 = { sizeof (TlsServerFinished_t1739223742), -1, sizeof(TlsServerFinished_t1739223742_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1309[1] = 
{
	TlsServerFinished_t1739223742_StaticFields::get_offset_of_Ssl3Marker_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1310 = { sizeof (TlsServerHello_t1964679492), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1310[2] = 
{
	TlsServerHello_t1964679492::get_offset_of_unixTime_9(),
	TlsServerHello_t1964679492::get_offset_of_random_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1311 = { sizeof (TlsServerHelloDone_t2766712588), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1312 = { sizeof (TlsServerKeyExchange_t115542990), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1313 = { sizeof (PrimalityTest_t572679902), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1314 = { sizeof (CertificateValidationCallback_t989458295), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1315 = { sizeof (CertificateValidationCallback2_t3318447433), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1316 = { sizeof (CertificateSelectionCallback_t3721235490), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1317 = { sizeof (PrivateKeySelectionCallback_t1663566523), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1318 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305139), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1318[16] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24U24fieldU2D0_0(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24U24fieldU2D5_1(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24U24fieldU2D6_2(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24U24fieldU2D7_3(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24U24fieldU2D8_4(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24U24fieldU2D9_5(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24U24fieldU2D11_6(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24U24fieldU2D12_7(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24U24fieldU2D13_8(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24U24fieldU2D14_9(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24U24fieldU2D15_10(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24U24fieldU2D16_11(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24U24fieldU2D17_12(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24U24fieldU2D21_13(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24U24fieldU2D22_14(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24U24fieldU2D23_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1319 = { sizeof (U24ArrayTypeU243132_t1892466093)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU243132_t1892466093 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1320 = { sizeof (U24ArrayTypeU24256_t2038352955)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU24256_t2038352955 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1321 = { sizeof (U24ArrayTypeU2420_t540610922)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU2420_t540610922 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1322 = { sizeof (U24ArrayTypeU2432_t3672778806)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU2432_t3672778806 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1323 = { sizeof (U24ArrayTypeU2448_t896841276)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU2448_t896841276 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1324 = { sizeof (U24ArrayTypeU2464_t2866209746)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU2464_t2866209746 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1325 = { sizeof (U24ArrayTypeU2412_t3672778805)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU2412_t3672778805 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1326 = { sizeof (U24ArrayTypeU2416_t1703410335)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU2416_t1703410335 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1327 = { sizeof (U24ArrayTypeU244_t1957337331)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU244_t1957337331 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1328 = { sizeof (U3CModuleU3E_t3783534217), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1329 = { sizeof (ExtensionAttribute_t1840441203), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1330 = { sizeof (Locale_t4255929017), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1331 = { sizeof (MonoTODOAttribute_t3487514021), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1332 = { sizeof (KeyBuilder_t3965881086), -1, sizeof(KeyBuilder_t3965881086_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1332[1] = 
{
	KeyBuilder_t3965881086_StaticFields::get_offset_of_rng_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1333 = { sizeof (SymmetricTransform_t1394030014), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1333[12] = 
{
	SymmetricTransform_t1394030014::get_offset_of_algo_0(),
	SymmetricTransform_t1394030014::get_offset_of_encrypt_1(),
	SymmetricTransform_t1394030014::get_offset_of_BlockSizeByte_2(),
	SymmetricTransform_t1394030014::get_offset_of_temp_3(),
	SymmetricTransform_t1394030014::get_offset_of_temp2_4(),
	SymmetricTransform_t1394030014::get_offset_of_workBuff_5(),
	SymmetricTransform_t1394030014::get_offset_of_workout_6(),
	SymmetricTransform_t1394030014::get_offset_of_FeedBackByte_7(),
	SymmetricTransform_t1394030014::get_offset_of_FeedBackIter_8(),
	SymmetricTransform_t1394030014::get_offset_of_m_disposed_9(),
	SymmetricTransform_t1394030014::get_offset_of_lastBlock_10(),
	SymmetricTransform_t1394030014::get_offset_of__rng_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1334 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1334[14] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1335 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1335[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1336 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1336[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1337 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1337[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1338 = { sizeof (Check_t578192424), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1339 = { sizeof (Enumerable_t2148412300), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1340 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1340[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1341 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1341[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1342 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1342[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1343 = { sizeof (Aes_t2354947465), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1344 = { sizeof (AesManaged_t3721278648), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1345 = { sizeof (AesTransform_t3733702461), -1, sizeof(AesTransform_t3733702461_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1345[14] = 
{
	AesTransform_t3733702461::get_offset_of_expandedKey_12(),
	AesTransform_t3733702461::get_offset_of_Nk_13(),
	AesTransform_t3733702461::get_offset_of_Nr_14(),
	AesTransform_t3733702461_StaticFields::get_offset_of_Rcon_15(),
	AesTransform_t3733702461_StaticFields::get_offset_of_SBox_16(),
	AesTransform_t3733702461_StaticFields::get_offset_of_iSBox_17(),
	AesTransform_t3733702461_StaticFields::get_offset_of_T0_18(),
	AesTransform_t3733702461_StaticFields::get_offset_of_T1_19(),
	AesTransform_t3733702461_StaticFields::get_offset_of_T2_20(),
	AesTransform_t3733702461_StaticFields::get_offset_of_T3_21(),
	AesTransform_t3733702461_StaticFields::get_offset_of_iT0_22(),
	AesTransform_t3733702461_StaticFields::get_offset_of_iT1_23(),
	AesTransform_t3733702461_StaticFields::get_offset_of_iT2_24(),
	AesTransform_t3733702461_StaticFields::get_offset_of_iT3_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1346 = { sizeof (Action_t3226471752), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1347 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1348 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1349 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1350 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1351 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1352 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1353 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1354 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305140), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1354[12] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24U24fieldU2D0_0(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24U24fieldU2D1_1(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24U24fieldU2D2_2(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24U24fieldU2D3_3(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24U24fieldU2D4_4(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24U24fieldU2D5_5(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24U24fieldU2D6_6(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24U24fieldU2D7_7(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24U24fieldU2D8_8(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24U24fieldU2D9_9(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24U24fieldU2D10_10(),
	U3CPrivateImplementationDetailsU3E_t1486305140_StaticFields::get_offset_of_U24U24fieldU2D11_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1355 = { sizeof (U24ArrayTypeU24136_t2844921916)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU24136_t2844921916 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1356 = { sizeof (U24ArrayTypeU24120_t116038563)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU24120_t116038563 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1357 = { sizeof (U24ArrayTypeU24256_t2038352956)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU24256_t2038352956 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1358 = { sizeof (U24ArrayTypeU241024_t2672183895)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU241024_t2672183895 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1359 = { sizeof (U3CModuleU3E_t3783534218), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1360 = { sizeof (OscBundleReceivedEventArgs_t3673224441), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1360[1] = 
{
	OscBundleReceivedEventArgs_t3673224441::get_offset_of_U3CBundleU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1361 = { sizeof (OscMessageReceivedEventArgs_t1263041860), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1361[1] = 
{
	OscMessageReceivedEventArgs_t1263041860::get_offset_of_U3CMessageU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1362 = { sizeof (OscPacketReceivedEventArgs_t3282045269), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1362[1] = 
{
	OscPacketReceivedEventArgs_t3282045269::get_offset_of_U3CPacketU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1363 = { sizeof (TransmissionType_t529366678)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1363[5] = 
{
	TransmissionType_t529366678::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1364 = { sizeof (OscPacket_t504761797), -1, sizeof(OscPacket_t504761797_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1364[3] = 
{
	OscPacket_t504761797_StaticFields::get_offset_of_littleEndianByteOrder_0(),
	OscPacket_t504761797::get_offset_of_address_1(),
	OscPacket_t504761797::get_offset_of_data_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1365 = { sizeof (OscBundle_t1126010605), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1365[2] = 
{
	0,
	OscBundle_t1126010605::get_offset_of_timeStamp_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1366 = { sizeof (OscMessage_t2764280154), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1366[17] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	OscMessage_t2764280154::get_offset_of_typeTag_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1367 = { sizeof (OscTimeTag_t625345318), -1, sizeof(OscTimeTag_t625345318_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1367[3] = 
{
	OscTimeTag_t625345318_StaticFields::get_offset_of_Epoch_0(),
	OscTimeTag_t625345318_StaticFields::get_offset_of_MinValue_1(),
	OscTimeTag_t625345318::get_offset_of_timeStamp_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1368 = { sizeof (UDPReceiver_t3846109956), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1368[13] = 
{
	UDPReceiver_t3846109956::get_offset_of_PacketReceived_0(),
	UDPReceiver_t3846109956::get_offset_of_BundleReceived_1(),
	UDPReceiver_t3846109956::get_offset_of_ErrorOccured_2(),
	UDPReceiver_t3846109956::get_offset_of_messageReceivedInvoker_3(),
	UDPReceiver_t3846109956::get_offset_of_udpClient_4(),
	UDPReceiver_t3846109956::get_offset_of_acceptingConnections_5(),
	UDPReceiver_t3846109956::get_offset_of_callback_6(),
	UDPReceiver_t3846109956::get_offset_of_U3CIPAddressU3Ek__BackingField_7(),
	UDPReceiver_t3846109956::get_offset_of_U3CPortU3Ek__BackingField_8(),
	UDPReceiver_t3846109956::get_offset_of_U3CMulticastAddressU3Ek__BackingField_9(),
	UDPReceiver_t3846109956::get_offset_of_U3CIPEndPointU3Ek__BackingField_10(),
	UDPReceiver_t3846109956::get_offset_of_U3CTransmissionTypeU3Ek__BackingField_11(),
	UDPReceiver_t3846109956::get_offset_of_U3CConsumeParsingExceptionsU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1369 = { sizeof (UdpState_t1122067727), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1369[2] = 
{
	UdpState_t1122067727::get_offset_of_U3CClientU3Ek__BackingField_0(),
	UdpState_t1122067727::get_offset_of_U3CIPEndPointU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1370 = { sizeof (ExceptionEventArgs_t892713536), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1370[1] = 
{
	ExceptionEventArgs_t892713536::get_offset_of_U3CExceptionU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1371 = { sizeof (Utility_t2927649758), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1372 = { sizeof (U3CPrivateImplementationDetailsU3EU7B2BD1A3B7U2DFF70U2D4DFDU2D9ABDU2D9AF73AFC1A70U7D_t2052325035), -1, sizeof(U3CPrivateImplementationDetailsU3EU7B2BD1A3B7U2DFF70U2D4DFDU2D9ABDU2D9AF73AFC1A70U7D_t2052325035_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1372[1] = 
{
	U3CPrivateImplementationDetailsU3EU7B2BD1A3B7U2DFF70U2D4DFDU2D9ABDU2D9AF73AFC1A70U7D_t2052325035_StaticFields::get_offset_of_U24U24method0x6000029U2D1_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1373 = { sizeof (U3CModuleU3E_t3783534219), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1374 = { sizeof (XPathParser_t1120482700), -1, sizeof(XPathParser_t1120482700_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1374[17] = 
{
	XPathParser_t1120482700::get_offset_of_Context_0(),
	XPathParser_t1120482700::get_offset_of_ErrorOutput_1(),
	XPathParser_t1120482700::get_offset_of_eof_token_2(),
	XPathParser_t1120482700::get_offset_of_debug_3(),
	XPathParser_t1120482700_StaticFields::get_offset_of_yyFinal_4(),
	XPathParser_t1120482700_StaticFields::get_offset_of_yyNames_5(),
	XPathParser_t1120482700::get_offset_of_yyExpectingState_6(),
	XPathParser_t1120482700::get_offset_of_yyMax_7(),
	XPathParser_t1120482700_StaticFields::get_offset_of_yyLhs_8(),
	XPathParser_t1120482700_StaticFields::get_offset_of_yyLen_9(),
	XPathParser_t1120482700_StaticFields::get_offset_of_yyDefRed_10(),
	XPathParser_t1120482700_StaticFields::get_offset_of_yyDgoto_11(),
	XPathParser_t1120482700_StaticFields::get_offset_of_yySindex_12(),
	XPathParser_t1120482700_StaticFields::get_offset_of_yyRindex_13(),
	XPathParser_t1120482700_StaticFields::get_offset_of_yyGindex_14(),
	XPathParser_t1120482700_StaticFields::get_offset_of_yyTable_15(),
	XPathParser_t1120482700_StaticFields::get_offset_of_yyCheck_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1375 = { sizeof (YYRules_t4159463463), -1, sizeof(YYRules_t4159463463_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1375[1] = 
{
	YYRules_t4159463463_StaticFields::get_offset_of_yyRule_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1376 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1377 = { sizeof (yyException_t2814964437), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1378 = { sizeof (yyUnexpectedEof_t3406522231), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1379 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1380 = { sizeof (XPathFunctions_t976633782), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1381 = { sizeof (XPathFunction_t759167395), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1382 = { sizeof (XPathFunctionLast_t2734600875), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1383 = { sizeof (XPathFunctionPosition_t3711683936), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1384 = { sizeof (XPathFunctionCount_t711123082), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1384[1] = 
{
	XPathFunctionCount_t711123082::get_offset_of_arg0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1385 = { sizeof (XPathFunctionId_t2998162272), -1, sizeof(XPathFunctionId_t2998162272_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1385[2] = 
{
	XPathFunctionId_t2998162272::get_offset_of_arg0_0(),
	XPathFunctionId_t2998162272_StaticFields::get_offset_of_rgchWhitespace_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1386 = { sizeof (XPathFunctionLocalName_t3935594891), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1386[1] = 
{
	XPathFunctionLocalName_t3935594891::get_offset_of_arg0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1387 = { sizeof (XPathFunctionNamespaceUri_t448035136), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1387[1] = 
{
	XPathFunctionNamespaceUri_t448035136::get_offset_of_arg0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1388 = { sizeof (XPathFunctionName_t1880977850), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1388[1] = 
{
	XPathFunctionName_t1880977850::get_offset_of_arg0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1389 = { sizeof (XPathFunctionString_t3963481524), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1389[1] = 
{
	XPathFunctionString_t3963481524::get_offset_of_arg0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1390 = { sizeof (XPathFunctionConcat_t2913460711), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1390[1] = 
{
	XPathFunctionConcat_t2913460711::get_offset_of_rgs_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1391 = { sizeof (XPathFunctionStartsWith_t1855488236), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1391[2] = 
{
	XPathFunctionStartsWith_t1855488236::get_offset_of_arg0_0(),
	XPathFunctionStartsWith_t1855488236::get_offset_of_arg1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1392 = { sizeof (XPathFunctionContains_t4033230314), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1392[2] = 
{
	XPathFunctionContains_t4033230314::get_offset_of_arg0_0(),
	XPathFunctionContains_t4033230314::get_offset_of_arg1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1393 = { sizeof (XPathFunctionSubstringBefore_t1797727619), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1393[2] = 
{
	XPathFunctionSubstringBefore_t1797727619::get_offset_of_arg0_0(),
	XPathFunctionSubstringBefore_t1797727619::get_offset_of_arg1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1394 = { sizeof (XPathFunctionSubstringAfter_t1560316700), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1394[2] = 
{
	XPathFunctionSubstringAfter_t1560316700::get_offset_of_arg0_0(),
	XPathFunctionSubstringAfter_t1560316700::get_offset_of_arg1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1395 = { sizeof (XPathFunctionSubstring_t2124841904), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1395[3] = 
{
	XPathFunctionSubstring_t2124841904::get_offset_of_arg0_0(),
	XPathFunctionSubstring_t2124841904::get_offset_of_arg1_1(),
	XPathFunctionSubstring_t2124841904::get_offset_of_arg2_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1396 = { sizeof (XPathFunctionStringLength_t2821584396), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1396[1] = 
{
	XPathFunctionStringLength_t2821584396::get_offset_of_arg0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1397 = { sizeof (XPathFunctionNormalizeSpace_t1288231156), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1397[1] = 
{
	XPathFunctionNormalizeSpace_t1288231156::get_offset_of_arg0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1398 = { sizeof (XPathFunctionTranslate_t2720664953), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1398[3] = 
{
	XPathFunctionTranslate_t2720664953::get_offset_of_arg0_0(),
	XPathFunctionTranslate_t2720664953::get_offset_of_arg1_1(),
	XPathFunctionTranslate_t2720664953::get_offset_of_arg2_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1399 = { sizeof (XPathBooleanFunction_t2511646183), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
