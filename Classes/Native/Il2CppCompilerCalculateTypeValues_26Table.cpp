﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.String
struct String_t;
// IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v3.PersonalityInsights
struct PersonalityInsights_t2503525937;
// IBM.Watson.DeveloperCloud.Services.ServiceStatus
struct ServiceStatus_t1443707987;
// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.SolrClusterResponse[]
struct SolrClusterResponseU5BU5D_t3723086068;
// System.String[]
struct StringU5BU5D_t1642385972;
// IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v3.ConsumptionPreferencesNode[]
struct ConsumptionPreferencesNodeU5BU5D_t4282278992;
// FullSerializer.fsSerializer
struct fsSerializer_t4193731081;
// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RankerInfoPayload[]
struct RankerInfoPayloadU5BU5D_t3973908944;
// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/LoadFileDelegate
struct LoadFileDelegate_t1720639326;
// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/SaveFileDelegate
struct SaveFileDelegate_t3125227661;
// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RankedAnswer[]
struct RankedAnswerU5BU5D_t2259955342;
// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.SolrClusterResponse
struct SolrClusterResponse_t1008480713;
// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.ResponseHeader
struct ResponseHeader_t2485747570;
// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.CollectionsResponse[]
struct CollectionsResponseU5BU5D_t1328740077;
// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.QueryParams
struct QueryParams_t3040774976;
// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.Doc[]
struct DocU5BU5D_t1574444043;
// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.Response
struct Response_t1432761593;
// IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.NaturalLanguageClassifier
struct NaturalLanguageClassifier_t2492034444;
// IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.NaturalLanguageClassifier/OnFindClassifier
struct OnFindClassifier_t2470508822;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Collections.Generic.Dictionary`2<System.String,IBM.Watson.DeveloperCloud.Connection.RESTConnector/Form>
struct Dictionary_2_t2694055125;
// IBM.Watson.DeveloperCloud.Connection.RESTConnector/ResponseEvent
struct ResponseEvent_t1616568356;
// IBM.Watson.DeveloperCloud.Connection.RESTConnector/ProgressEvent
struct ProgressEvent_t4185145044;
// IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v2.TraitTreeNode[]
struct TraitTreeNodeU5BU5D_t4145450379;
// IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v2.TraitTreeNode
struct TraitTreeNode_t694336958;
// IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v2.Warning[]
struct WarningU5BU5D_t523936175;
// System.Collections.Generic.Dictionary`2<System.String,IBM.Watson.DeveloperCloud.Utilities.DataCache>
struct Dictionary_2_t1870152036;
// IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.LanguageTranslator
struct LanguageTranslator_t2981742234;
// IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.Class[]
struct ClassU5BU5D_t3189134507;
// IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.Classifier[]
struct ClassifierU5BU5D_t2606195416;
// IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v2.PersonalityInsights
struct PersonalityInsights_t582046669;
// IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v3.TraitTreeNode[]
struct TraitTreeNodeU5BU5D_t476229367;
// IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v3.BehaviorNode[]
struct BehaviorNodeU5BU5D_t2246947787;
// IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v3.ConsumptionPreferencesCategoryNode[]
struct ConsumptionPreferencesCategoryNodeU5BU5D_t273403704;
// IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v3.Warning[]
struct WarningU5BU5D_t2063588977;
// IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v2.ContentItem[]
struct ContentItemU5BU5D_t836316745;
// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/OnGetClusters
struct OnGetClusters_t1915248271;
// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/OnGetCluster
struct OnGetCluster_t3066553756;
// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/OnGetClusterConfigs
struct OnGetClusterConfigs_t22213349;
// System.Void
struct Void_t1841601450;
// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/OnDeleteCluster
struct OnDeleteCluster_t1281715199;
// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/OnUploadClusterConfig
struct OnUploadClusterConfig_t589517099;
// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/OnGetClusterConfig
struct OnGetClusterConfig_t1069230450;
// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/OnCreateCluster
struct OnCreateCluster_t1489102388;
// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/OnDeleteClusterConfig
struct OnDeleteClusterConfig_t2578936137;
// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/OnGetRankers
struct OnGetRankers_t1731100556;
// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/OnSearch
struct OnSearch_t2575532994;
// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/OnCollections
struct OnCollections_t2275029757;
// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/OnIndexDocuments
struct OnIndexDocuments_t1217349952;
// IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.NaturalLanguageClassifier/OnClassify
struct OnClassify_t2074661398;
// IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.NaturalLanguageClassifier/OnGetClassifiers
struct OnGetClassifiers_t1859727718;
// IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v2.PersonalityInsights/OnGetProfile
struct OnGetProfile_t2522047396;
// IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.NaturalLanguageClassifier/OnGetClassifier
struct OnGetClassifier_t1783367173;
// IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.NaturalLanguageClassifier/OnTrainClassifier
struct OnTrainClassifier_t2091787415;
// IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.NaturalLanguageClassifier/OnDeleteClassifier
struct OnDeleteClassifier_t3710463002;
// IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v3.PersonalityInsights/OnGetProfile
struct OnGetProfile_t2596486115;
// IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.LanguageTranslator/IdentifyCallback
struct IdentifyCallback_t682135812;
// IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.LanguageTranslator/GetLanguagesCallback
struct GetLanguagesCallback_t1996869611;
// IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.LanguageTranslator/GetModelCallback
struct GetModelCallback_t1483236201;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1572802995;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.SearchResponse
struct SearchResponse_t1678084193;
// IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.Classifier
struct Classifier_t1414011573;
// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.ListRankersPayload
struct ListRankersPayload_t4087700440;
// IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.Classifiers
struct Classifiers_t1537491400;
// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RankerStatusPayload
struct RankerStatusPayload_t2644736411;
// IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.ClassifyResult
struct ClassifyResult_t2166103447;
// IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v3.Profile
struct Profile_t3811251445;
// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.SolrConfigList
struct SolrConfigList_t3679635936;
// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.SolrClusterListResponse
struct SolrClusterListResponse_t1287774645;
// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.UploadResponse
struct UploadResponse_t429219006;
// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.CollectionsResponse
struct CollectionsResponse_t1627544196;
// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.IndexResponse
struct IndexResponse_t1068437599;
// IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v2.Profile
struct Profile_t4293889999;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef SOLRCLUSTERRESPONSE_T1008480713_H
#define SOLRCLUSTERRESPONSE_T1008480713_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.SolrClusterResponse
struct  SolrClusterResponse_t1008480713  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.SolrClusterResponse::<solr_cluster_id>k__BackingField
	String_t* ___U3Csolr_cluster_idU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.SolrClusterResponse::<cluster_name>k__BackingField
	String_t* ___U3Ccluster_nameU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.SolrClusterResponse::<cluster_size>k__BackingField
	String_t* ___U3Ccluster_sizeU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.SolrClusterResponse::<solr_cluster_status>k__BackingField
	String_t* ___U3Csolr_cluster_statusU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3Csolr_cluster_idU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SolrClusterResponse_t1008480713, ___U3Csolr_cluster_idU3Ek__BackingField_0)); }
	inline String_t* get_U3Csolr_cluster_idU3Ek__BackingField_0() const { return ___U3Csolr_cluster_idU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3Csolr_cluster_idU3Ek__BackingField_0() { return &___U3Csolr_cluster_idU3Ek__BackingField_0; }
	inline void set_U3Csolr_cluster_idU3Ek__BackingField_0(String_t* value)
	{
		___U3Csolr_cluster_idU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Csolr_cluster_idU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3Ccluster_nameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(SolrClusterResponse_t1008480713, ___U3Ccluster_nameU3Ek__BackingField_1)); }
	inline String_t* get_U3Ccluster_nameU3Ek__BackingField_1() const { return ___U3Ccluster_nameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3Ccluster_nameU3Ek__BackingField_1() { return &___U3Ccluster_nameU3Ek__BackingField_1; }
	inline void set_U3Ccluster_nameU3Ek__BackingField_1(String_t* value)
	{
		___U3Ccluster_nameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3Ccluster_nameU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3Ccluster_sizeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SolrClusterResponse_t1008480713, ___U3Ccluster_sizeU3Ek__BackingField_2)); }
	inline String_t* get_U3Ccluster_sizeU3Ek__BackingField_2() const { return ___U3Ccluster_sizeU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3Ccluster_sizeU3Ek__BackingField_2() { return &___U3Ccluster_sizeU3Ek__BackingField_2; }
	inline void set_U3Ccluster_sizeU3Ek__BackingField_2(String_t* value)
	{
		___U3Ccluster_sizeU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3Ccluster_sizeU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3Csolr_cluster_statusU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SolrClusterResponse_t1008480713, ___U3Csolr_cluster_statusU3Ek__BackingField_3)); }
	inline String_t* get_U3Csolr_cluster_statusU3Ek__BackingField_3() const { return ___U3Csolr_cluster_statusU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3Csolr_cluster_statusU3Ek__BackingField_3() { return &___U3Csolr_cluster_statusU3Ek__BackingField_3; }
	inline void set_U3Csolr_cluster_statusU3Ek__BackingField_3(String_t* value)
	{
		___U3Csolr_cluster_statusU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3Csolr_cluster_statusU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOLRCLUSTERRESPONSE_T1008480713_H
#ifndef CHECKSERVICESTATUS_T3235045572_H
#define CHECKSERVICESTATUS_T3235045572_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v3.PersonalityInsights/CheckServiceStatus
struct  CheckServiceStatus_t3235045572  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v3.PersonalityInsights IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v3.PersonalityInsights/CheckServiceStatus::m_Service
	PersonalityInsights_t2503525937 * ___m_Service_0;
	// IBM.Watson.DeveloperCloud.Services.ServiceStatus IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v3.PersonalityInsights/CheckServiceStatus::m_Callback
	ServiceStatus_t1443707987 * ___m_Callback_1;

public:
	inline static int32_t get_offset_of_m_Service_0() { return static_cast<int32_t>(offsetof(CheckServiceStatus_t3235045572, ___m_Service_0)); }
	inline PersonalityInsights_t2503525937 * get_m_Service_0() const { return ___m_Service_0; }
	inline PersonalityInsights_t2503525937 ** get_address_of_m_Service_0() { return &___m_Service_0; }
	inline void set_m_Service_0(PersonalityInsights_t2503525937 * value)
	{
		___m_Service_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Service_0), value);
	}

	inline static int32_t get_offset_of_m_Callback_1() { return static_cast<int32_t>(offsetof(CheckServiceStatus_t3235045572, ___m_Callback_1)); }
	inline ServiceStatus_t1443707987 * get_m_Callback_1() const { return ___m_Callback_1; }
	inline ServiceStatus_t1443707987 ** get_address_of_m_Callback_1() { return &___m_Callback_1; }
	inline void set_m_Callback_1(ServiceStatus_t1443707987 * value)
	{
		___m_Callback_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Callback_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHECKSERVICESTATUS_T3235045572_H
#ifndef SOLRCLUSTERLISTRESPONSE_T1287774645_H
#define SOLRCLUSTERLISTRESPONSE_T1287774645_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.SolrClusterListResponse
struct  SolrClusterListResponse_t1287774645  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.SolrClusterResponse[] IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.SolrClusterListResponse::<clusters>k__BackingField
	SolrClusterResponseU5BU5D_t3723086068* ___U3CclustersU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CclustersU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SolrClusterListResponse_t1287774645, ___U3CclustersU3Ek__BackingField_0)); }
	inline SolrClusterResponseU5BU5D_t3723086068* get_U3CclustersU3Ek__BackingField_0() const { return ___U3CclustersU3Ek__BackingField_0; }
	inline SolrClusterResponseU5BU5D_t3723086068** get_address_of_U3CclustersU3Ek__BackingField_0() { return &___U3CclustersU3Ek__BackingField_0; }
	inline void set_U3CclustersU3Ek__BackingField_0(SolrClusterResponseU5BU5D_t3723086068* value)
	{
		___U3CclustersU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CclustersU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOLRCLUSTERLISTRESPONSE_T1287774645_H
#ifndef SOLRCONFIGLIST_T3679635936_H
#define SOLRCONFIGLIST_T3679635936_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.SolrConfigList
struct  SolrConfigList_t3679635936  : public RuntimeObject
{
public:
	// System.String[] IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.SolrConfigList::<solr_configs>k__BackingField
	StringU5BU5D_t1642385972* ___U3Csolr_configsU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3Csolr_configsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SolrConfigList_t3679635936, ___U3Csolr_configsU3Ek__BackingField_0)); }
	inline StringU5BU5D_t1642385972* get_U3Csolr_configsU3Ek__BackingField_0() const { return ___U3Csolr_configsU3Ek__BackingField_0; }
	inline StringU5BU5D_t1642385972** get_address_of_U3Csolr_configsU3Ek__BackingField_0() { return &___U3Csolr_configsU3Ek__BackingField_0; }
	inline void set_U3Csolr_configsU3Ek__BackingField_0(StringU5BU5D_t1642385972* value)
	{
		___U3Csolr_configsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Csolr_configsU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOLRCONFIGLIST_T3679635936_H
#ifndef DELETERESPONSE_T2373142580_H
#define DELETERESPONSE_T2373142580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.DeleteResponse
struct  DeleteResponse_t2373142580  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.DeleteResponse::<message>k__BackingField
	String_t* ___U3CmessageU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.DeleteResponse::<statusCode>k__BackingField
	String_t* ___U3CstatusCodeU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmessageU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DeleteResponse_t2373142580, ___U3CmessageU3Ek__BackingField_0)); }
	inline String_t* get_U3CmessageU3Ek__BackingField_0() const { return ___U3CmessageU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CmessageU3Ek__BackingField_0() { return &___U3CmessageU3Ek__BackingField_0; }
	inline void set_U3CmessageU3Ek__BackingField_0(String_t* value)
	{
		___U3CmessageU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmessageU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CstatusCodeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(DeleteResponse_t2373142580, ___U3CstatusCodeU3Ek__BackingField_1)); }
	inline String_t* get_U3CstatusCodeU3Ek__BackingField_1() const { return ___U3CstatusCodeU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CstatusCodeU3Ek__BackingField_1() { return &___U3CstatusCodeU3Ek__BackingField_1; }
	inline void set_U3CstatusCodeU3Ek__BackingField_1(String_t* value)
	{
		___U3CstatusCodeU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstatusCodeU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELETERESPONSE_T2373142580_H
#ifndef MESSAGERESPONSEPAYLOAD_T3469683060_H
#define MESSAGERESPONSEPAYLOAD_T3469683060_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.MessageResponsePayload
struct  MessageResponsePayload_t3469683060  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.MessageResponsePayload::<message>k__BackingField
	String_t* ___U3CmessageU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CmessageU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MessageResponsePayload_t3469683060, ___U3CmessageU3Ek__BackingField_0)); }
	inline String_t* get_U3CmessageU3Ek__BackingField_0() const { return ___U3CmessageU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CmessageU3Ek__BackingField_0() { return &___U3CmessageU3Ek__BackingField_0; }
	inline void set_U3CmessageU3Ek__BackingField_0(String_t* value)
	{
		___U3CmessageU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmessageU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGERESPONSEPAYLOAD_T3469683060_H
#ifndef ERRORRESPONSEPAYLOAD_T3410979863_H
#define ERRORRESPONSEPAYLOAD_T3410979863_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.ErrorResponsePayload
struct  ErrorResponsePayload_t3410979863  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.ErrorResponsePayload::<msg>k__BackingField
	String_t* ___U3CmsgU3Ek__BackingField_0;
	// System.Int32 IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.ErrorResponsePayload::<code>k__BackingField
	int32_t ___U3CcodeU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmsgU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ErrorResponsePayload_t3410979863, ___U3CmsgU3Ek__BackingField_0)); }
	inline String_t* get_U3CmsgU3Ek__BackingField_0() const { return ___U3CmsgU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CmsgU3Ek__BackingField_0() { return &___U3CmsgU3Ek__BackingField_0; }
	inline void set_U3CmsgU3Ek__BackingField_0(String_t* value)
	{
		___U3CmsgU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmsgU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CcodeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ErrorResponsePayload_t3410979863, ___U3CcodeU3Ek__BackingField_1)); }
	inline int32_t get_U3CcodeU3Ek__BackingField_1() const { return ___U3CcodeU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CcodeU3Ek__BackingField_1() { return &___U3CcodeU3Ek__BackingField_1; }
	inline void set_U3CcodeU3Ek__BackingField_1(int32_t value)
	{
		___U3CcodeU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERRORRESPONSEPAYLOAD_T3410979863_H
#ifndef UPLOADRESPONSE_T429219006_H
#define UPLOADRESPONSE_T429219006_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.UploadResponse
struct  UploadResponse_t429219006  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.UploadResponse::<message>k__BackingField
	String_t* ___U3CmessageU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.UploadResponse::<statusCode>k__BackingField
	String_t* ___U3CstatusCodeU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmessageU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(UploadResponse_t429219006, ___U3CmessageU3Ek__BackingField_0)); }
	inline String_t* get_U3CmessageU3Ek__BackingField_0() const { return ___U3CmessageU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CmessageU3Ek__BackingField_0() { return &___U3CmessageU3Ek__BackingField_0; }
	inline void set_U3CmessageU3Ek__BackingField_0(String_t* value)
	{
		___U3CmessageU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmessageU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CstatusCodeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(UploadResponse_t429219006, ___U3CstatusCodeU3Ek__BackingField_1)); }
	inline String_t* get_U3CstatusCodeU3Ek__BackingField_1() const { return ___U3CstatusCodeU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CstatusCodeU3Ek__BackingField_1() { return &___U3CstatusCodeU3Ek__BackingField_1; }
	inline void set_U3CstatusCodeU3Ek__BackingField_1(String_t* value)
	{
		___U3CstatusCodeU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstatusCodeU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPLOADRESPONSE_T429219006_H
#ifndef WARNING_T31395408_H
#define WARNING_T31395408_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v3.Warning
struct  Warning_t31395408  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v3.Warning::<warning_id>k__BackingField
	String_t* ___U3Cwarning_idU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v3.Warning::<message>k__BackingField
	String_t* ___U3CmessageU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3Cwarning_idU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Warning_t31395408, ___U3Cwarning_idU3Ek__BackingField_0)); }
	inline String_t* get_U3Cwarning_idU3Ek__BackingField_0() const { return ___U3Cwarning_idU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3Cwarning_idU3Ek__BackingField_0() { return &___U3Cwarning_idU3Ek__BackingField_0; }
	inline void set_U3Cwarning_idU3Ek__BackingField_0(String_t* value)
	{
		___U3Cwarning_idU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cwarning_idU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CmessageU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Warning_t31395408, ___U3CmessageU3Ek__BackingField_1)); }
	inline String_t* get_U3CmessageU3Ek__BackingField_1() const { return ___U3CmessageU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CmessageU3Ek__BackingField_1() { return &___U3CmessageU3Ek__BackingField_1; }
	inline void set_U3CmessageU3Ek__BackingField_1(String_t* value)
	{
		___U3CmessageU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmessageU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WARNING_T31395408_H
#ifndef CONSUMPTIONPREFERENCESNODE_T1038535837_H
#define CONSUMPTIONPREFERENCESNODE_T1038535837_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v3.ConsumptionPreferencesNode
struct  ConsumptionPreferencesNode_t1038535837  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v3.ConsumptionPreferencesNode::<consumption_preference_id>k__BackingField
	String_t* ___U3Cconsumption_preference_idU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v3.ConsumptionPreferencesNode::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_1;
	// System.Double IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v3.ConsumptionPreferencesNode::<score>k__BackingField
	double ___U3CscoreU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3Cconsumption_preference_idU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ConsumptionPreferencesNode_t1038535837, ___U3Cconsumption_preference_idU3Ek__BackingField_0)); }
	inline String_t* get_U3Cconsumption_preference_idU3Ek__BackingField_0() const { return ___U3Cconsumption_preference_idU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3Cconsumption_preference_idU3Ek__BackingField_0() { return &___U3Cconsumption_preference_idU3Ek__BackingField_0; }
	inline void set_U3Cconsumption_preference_idU3Ek__BackingField_0(String_t* value)
	{
		___U3Cconsumption_preference_idU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cconsumption_preference_idU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ConsumptionPreferencesNode_t1038535837, ___U3CnameU3Ek__BackingField_1)); }
	inline String_t* get_U3CnameU3Ek__BackingField_1() const { return ___U3CnameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_1() { return &___U3CnameU3Ek__BackingField_1; }
	inline void set_U3CnameU3Ek__BackingField_1(String_t* value)
	{
		___U3CnameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CscoreU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ConsumptionPreferencesNode_t1038535837, ___U3CscoreU3Ek__BackingField_2)); }
	inline double get_U3CscoreU3Ek__BackingField_2() const { return ___U3CscoreU3Ek__BackingField_2; }
	inline double* get_address_of_U3CscoreU3Ek__BackingField_2() { return &___U3CscoreU3Ek__BackingField_2; }
	inline void set_U3CscoreU3Ek__BackingField_2(double value)
	{
		___U3CscoreU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSUMPTIONPREFERENCESNODE_T1038535837_H
#ifndef BEHAVIORNODE_T287073406_H
#define BEHAVIORNODE_T287073406_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v3.BehaviorNode
struct  BehaviorNode_t287073406  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v3.BehaviorNode::<trait_id>k__BackingField
	String_t* ___U3Ctrait_idU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v3.BehaviorNode::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v3.BehaviorNode::<category>k__BackingField
	String_t* ___U3CcategoryU3Ek__BackingField_2;
	// System.Double IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v3.BehaviorNode::<percentage>k__BackingField
	double ___U3CpercentageU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3Ctrait_idU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(BehaviorNode_t287073406, ___U3Ctrait_idU3Ek__BackingField_0)); }
	inline String_t* get_U3Ctrait_idU3Ek__BackingField_0() const { return ___U3Ctrait_idU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3Ctrait_idU3Ek__BackingField_0() { return &___U3Ctrait_idU3Ek__BackingField_0; }
	inline void set_U3Ctrait_idU3Ek__BackingField_0(String_t* value)
	{
		___U3Ctrait_idU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Ctrait_idU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(BehaviorNode_t287073406, ___U3CnameU3Ek__BackingField_1)); }
	inline String_t* get_U3CnameU3Ek__BackingField_1() const { return ___U3CnameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_1() { return &___U3CnameU3Ek__BackingField_1; }
	inline void set_U3CnameU3Ek__BackingField_1(String_t* value)
	{
		___U3CnameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CcategoryU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(BehaviorNode_t287073406, ___U3CcategoryU3Ek__BackingField_2)); }
	inline String_t* get_U3CcategoryU3Ek__BackingField_2() const { return ___U3CcategoryU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CcategoryU3Ek__BackingField_2() { return &___U3CcategoryU3Ek__BackingField_2; }
	inline void set_U3CcategoryU3Ek__BackingField_2(String_t* value)
	{
		___U3CcategoryU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcategoryU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CpercentageU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(BehaviorNode_t287073406, ___U3CpercentageU3Ek__BackingField_3)); }
	inline double get_U3CpercentageU3Ek__BackingField_3() const { return ___U3CpercentageU3Ek__BackingField_3; }
	inline double* get_address_of_U3CpercentageU3Ek__BackingField_3() { return &___U3CpercentageU3Ek__BackingField_3; }
	inline void set_U3CpercentageU3Ek__BackingField_3(double value)
	{
		___U3CpercentageU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIORNODE_T287073406_H
#ifndef CONSUMPTIONPREFERENCESCATEGORYNODE_T38182357_H
#define CONSUMPTIONPREFERENCESCATEGORYNODE_T38182357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v3.ConsumptionPreferencesCategoryNode
struct  ConsumptionPreferencesCategoryNode_t38182357  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v3.ConsumptionPreferencesCategoryNode::<consumption_preference_category_id>k__BackingField
	String_t* ___U3Cconsumption_preference_category_idU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v3.ConsumptionPreferencesCategoryNode::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_1;
	// IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v3.ConsumptionPreferencesNode[] IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v3.ConsumptionPreferencesCategoryNode::<consumption_preferences>k__BackingField
	ConsumptionPreferencesNodeU5BU5D_t4282278992* ___U3Cconsumption_preferencesU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3Cconsumption_preference_category_idU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ConsumptionPreferencesCategoryNode_t38182357, ___U3Cconsumption_preference_category_idU3Ek__BackingField_0)); }
	inline String_t* get_U3Cconsumption_preference_category_idU3Ek__BackingField_0() const { return ___U3Cconsumption_preference_category_idU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3Cconsumption_preference_category_idU3Ek__BackingField_0() { return &___U3Cconsumption_preference_category_idU3Ek__BackingField_0; }
	inline void set_U3Cconsumption_preference_category_idU3Ek__BackingField_0(String_t* value)
	{
		___U3Cconsumption_preference_category_idU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cconsumption_preference_category_idU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ConsumptionPreferencesCategoryNode_t38182357, ___U3CnameU3Ek__BackingField_1)); }
	inline String_t* get_U3CnameU3Ek__BackingField_1() const { return ___U3CnameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_1() { return &___U3CnameU3Ek__BackingField_1; }
	inline void set_U3CnameU3Ek__BackingField_1(String_t* value)
	{
		___U3CnameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3Cconsumption_preferencesU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ConsumptionPreferencesCategoryNode_t38182357, ___U3Cconsumption_preferencesU3Ek__BackingField_2)); }
	inline ConsumptionPreferencesNodeU5BU5D_t4282278992* get_U3Cconsumption_preferencesU3Ek__BackingField_2() const { return ___U3Cconsumption_preferencesU3Ek__BackingField_2; }
	inline ConsumptionPreferencesNodeU5BU5D_t4282278992** get_address_of_U3Cconsumption_preferencesU3Ek__BackingField_2() { return &___U3Cconsumption_preferencesU3Ek__BackingField_2; }
	inline void set_U3Cconsumption_preferencesU3Ek__BackingField_2(ConsumptionPreferencesNodeU5BU5D_t4282278992* value)
	{
		___U3Cconsumption_preferencesU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cconsumption_preferencesU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSUMPTIONPREFERENCESCATEGORYNODE_T38182357_H
#ifndef CONTENTTYPE_T2631042457_H
#define CONTENTTYPE_T2631042457_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v3.ContentType
struct  ContentType_t2631042457  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTENTTYPE_T2631042457_H
#ifndef PERSONALITYINSIGHTSVERSION_T1779906393_H
#define PERSONALITYINSIGHTSVERSION_T1779906393_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v3.PersonalityInsightsVersion
struct  PersonalityInsightsVersion_t1779906393  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PERSONALITYINSIGHTSVERSION_T1779906393_H
#ifndef PERSONALITYINSIGHTS_T2503525937_H
#define PERSONALITYINSIGHTS_T2503525937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v3.PersonalityInsights
struct  PersonalityInsights_t2503525937  : public RuntimeObject
{
public:

public:
};

struct PersonalityInsights_t2503525937_StaticFields
{
public:
	// FullSerializer.fsSerializer IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v3.PersonalityInsights::sm_Serializer
	fsSerializer_t4193731081 * ___sm_Serializer_1;

public:
	inline static int32_t get_offset_of_sm_Serializer_1() { return static_cast<int32_t>(offsetof(PersonalityInsights_t2503525937_StaticFields, ___sm_Serializer_1)); }
	inline fsSerializer_t4193731081 * get_sm_Serializer_1() const { return ___sm_Serializer_1; }
	inline fsSerializer_t4193731081 ** get_address_of_sm_Serializer_1() { return &___sm_Serializer_1; }
	inline void set_sm_Serializer_1(fsSerializer_t4193731081 * value)
	{
		___sm_Serializer_1 = value;
		Il2CppCodeGenWriteBarrier((&___sm_Serializer_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PERSONALITYINSIGHTS_T2503525937_H
#ifndef CONTENTLANGUAGE_T2096133991_H
#define CONTENTLANGUAGE_T2096133991_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v3.ContentLanguage
struct  ContentLanguage_t2096133991  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTENTLANGUAGE_T2096133991_H
#ifndef ACCEPTLANGUAGE_T2731908252_H
#define ACCEPTLANGUAGE_T2731908252_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v3.AcceptLanguage
struct  AcceptLanguage_t2731908252  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACCEPTLANGUAGE_T2731908252_H
#ifndef RANKERSTATUSPAYLOAD_T2644736411_H
#define RANKERSTATUSPAYLOAD_T2644736411_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RankerStatusPayload
struct  RankerStatusPayload_t2644736411  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RankerStatusPayload::<ranker_id>k__BackingField
	String_t* ___U3Cranker_idU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RankerStatusPayload::<url>k__BackingField
	String_t* ___U3CurlU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RankerStatusPayload::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RankerStatusPayload::<created>k__BackingField
	String_t* ___U3CcreatedU3Ek__BackingField_3;
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RankerStatusPayload::<status>k__BackingField
	String_t* ___U3CstatusU3Ek__BackingField_4;
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RankerStatusPayload::<status_description>k__BackingField
	String_t* ___U3Cstatus_descriptionU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3Cranker_idU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RankerStatusPayload_t2644736411, ___U3Cranker_idU3Ek__BackingField_0)); }
	inline String_t* get_U3Cranker_idU3Ek__BackingField_0() const { return ___U3Cranker_idU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3Cranker_idU3Ek__BackingField_0() { return &___U3Cranker_idU3Ek__BackingField_0; }
	inline void set_U3Cranker_idU3Ek__BackingField_0(String_t* value)
	{
		___U3Cranker_idU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cranker_idU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CurlU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RankerStatusPayload_t2644736411, ___U3CurlU3Ek__BackingField_1)); }
	inline String_t* get_U3CurlU3Ek__BackingField_1() const { return ___U3CurlU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CurlU3Ek__BackingField_1() { return &___U3CurlU3Ek__BackingField_1; }
	inline void set_U3CurlU3Ek__BackingField_1(String_t* value)
	{
		___U3CurlU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CurlU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(RankerStatusPayload_t2644736411, ___U3CnameU3Ek__BackingField_2)); }
	inline String_t* get_U3CnameU3Ek__BackingField_2() const { return ___U3CnameU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_2() { return &___U3CnameU3Ek__BackingField_2; }
	inline void set_U3CnameU3Ek__BackingField_2(String_t* value)
	{
		___U3CnameU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CcreatedU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(RankerStatusPayload_t2644736411, ___U3CcreatedU3Ek__BackingField_3)); }
	inline String_t* get_U3CcreatedU3Ek__BackingField_3() const { return ___U3CcreatedU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CcreatedU3Ek__BackingField_3() { return &___U3CcreatedU3Ek__BackingField_3; }
	inline void set_U3CcreatedU3Ek__BackingField_3(String_t* value)
	{
		___U3CcreatedU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcreatedU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CstatusU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(RankerStatusPayload_t2644736411, ___U3CstatusU3Ek__BackingField_4)); }
	inline String_t* get_U3CstatusU3Ek__BackingField_4() const { return ___U3CstatusU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CstatusU3Ek__BackingField_4() { return &___U3CstatusU3Ek__BackingField_4; }
	inline void set_U3CstatusU3Ek__BackingField_4(String_t* value)
	{
		___U3CstatusU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstatusU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3Cstatus_descriptionU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(RankerStatusPayload_t2644736411, ___U3Cstatus_descriptionU3Ek__BackingField_5)); }
	inline String_t* get_U3Cstatus_descriptionU3Ek__BackingField_5() const { return ___U3Cstatus_descriptionU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3Cstatus_descriptionU3Ek__BackingField_5() { return &___U3Cstatus_descriptionU3Ek__BackingField_5; }
	inline void set_U3Cstatus_descriptionU3Ek__BackingField_5(String_t* value)
	{
		___U3Cstatus_descriptionU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cstatus_descriptionU3Ek__BackingField_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANKERSTATUSPAYLOAD_T2644736411_H
#ifndef RANKERTRAININGMETADATAPAYLOAD_T1049631040_H
#define RANKERTRAININGMETADATAPAYLOAD_T1049631040_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RankerTrainingMetadataPayload
struct  RankerTrainingMetadataPayload_t1049631040  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RankerTrainingMetadataPayload::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RankerTrainingMetadataPayload_t1049631040, ___U3CnameU3Ek__BackingField_0)); }
	inline String_t* get_U3CnameU3Ek__BackingField_0() const { return ___U3CnameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_0() { return &___U3CnameU3Ek__BackingField_0; }
	inline void set_U3CnameU3Ek__BackingField_0(String_t* value)
	{
		___U3CnameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANKERTRAININGMETADATAPAYLOAD_T1049631040_H
#ifndef LISTRANKERSPAYLOAD_T4087700440_H
#define LISTRANKERSPAYLOAD_T4087700440_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.ListRankersPayload
struct  ListRankersPayload_t4087700440  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RankerInfoPayload[] IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.ListRankersPayload::<rankers>k__BackingField
	RankerInfoPayloadU5BU5D_t3973908944* ___U3CrankersU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CrankersU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ListRankersPayload_t4087700440, ___U3CrankersU3Ek__BackingField_0)); }
	inline RankerInfoPayloadU5BU5D_t3973908944* get_U3CrankersU3Ek__BackingField_0() const { return ___U3CrankersU3Ek__BackingField_0; }
	inline RankerInfoPayloadU5BU5D_t3973908944** get_address_of_U3CrankersU3Ek__BackingField_0() { return &___U3CrankersU3Ek__BackingField_0; }
	inline void set_U3CrankersU3Ek__BackingField_0(RankerInfoPayloadU5BU5D_t3973908944* value)
	{
		___U3CrankersU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrankersU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTRANKERSPAYLOAD_T4087700440_H
#ifndef RANKERINFOPAYLOAD_T1776809245_H
#define RANKERINFOPAYLOAD_T1776809245_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RankerInfoPayload
struct  RankerInfoPayload_t1776809245  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RankerInfoPayload::<ranker_id>k__BackingField
	String_t* ___U3Cranker_idU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RankerInfoPayload::<url>k__BackingField
	String_t* ___U3CurlU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RankerInfoPayload::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RankerInfoPayload::<created>k__BackingField
	String_t* ___U3CcreatedU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3Cranker_idU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RankerInfoPayload_t1776809245, ___U3Cranker_idU3Ek__BackingField_0)); }
	inline String_t* get_U3Cranker_idU3Ek__BackingField_0() const { return ___U3Cranker_idU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3Cranker_idU3Ek__BackingField_0() { return &___U3Cranker_idU3Ek__BackingField_0; }
	inline void set_U3Cranker_idU3Ek__BackingField_0(String_t* value)
	{
		___U3Cranker_idU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cranker_idU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CurlU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RankerInfoPayload_t1776809245, ___U3CurlU3Ek__BackingField_1)); }
	inline String_t* get_U3CurlU3Ek__BackingField_1() const { return ___U3CurlU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CurlU3Ek__BackingField_1() { return &___U3CurlU3Ek__BackingField_1; }
	inline void set_U3CurlU3Ek__BackingField_1(String_t* value)
	{
		___U3CurlU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CurlU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(RankerInfoPayload_t1776809245, ___U3CnameU3Ek__BackingField_2)); }
	inline String_t* get_U3CnameU3Ek__BackingField_2() const { return ___U3CnameU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_2() { return &___U3CnameU3Ek__BackingField_2; }
	inline void set_U3CnameU3Ek__BackingField_2(String_t* value)
	{
		___U3CnameU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CcreatedU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(RankerInfoPayload_t1776809245, ___U3CcreatedU3Ek__BackingField_3)); }
	inline String_t* get_U3CcreatedU3Ek__BackingField_3() const { return ___U3CcreatedU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CcreatedU3Ek__BackingField_3() { return &___U3CcreatedU3Ek__BackingField_3; }
	inline void set_U3CcreatedU3Ek__BackingField_3(String_t* value)
	{
		___U3CcreatedU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcreatedU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANKERINFOPAYLOAD_T1776809245_H
#ifndef RANKEDANSWER_T193891511_H
#define RANKEDANSWER_T193891511_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RankedAnswer
struct  RankedAnswer_t193891511  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RankedAnswer::<answer_id>k__BackingField
	String_t* ___U3Canswer_idU3Ek__BackingField_0;
	// System.Int32 IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RankedAnswer::<score>k__BackingField
	int32_t ___U3CscoreU3Ek__BackingField_1;
	// System.Double IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RankedAnswer::<confidence>k__BackingField
	double ___U3CconfidenceU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3Canswer_idU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RankedAnswer_t193891511, ___U3Canswer_idU3Ek__BackingField_0)); }
	inline String_t* get_U3Canswer_idU3Ek__BackingField_0() const { return ___U3Canswer_idU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3Canswer_idU3Ek__BackingField_0() { return &___U3Canswer_idU3Ek__BackingField_0; }
	inline void set_U3Canswer_idU3Ek__BackingField_0(String_t* value)
	{
		___U3Canswer_idU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Canswer_idU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CscoreU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RankedAnswer_t193891511, ___U3CscoreU3Ek__BackingField_1)); }
	inline int32_t get_U3CscoreU3Ek__BackingField_1() const { return ___U3CscoreU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CscoreU3Ek__BackingField_1() { return &___U3CscoreU3Ek__BackingField_1; }
	inline void set_U3CscoreU3Ek__BackingField_1(int32_t value)
	{
		___U3CscoreU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CconfidenceU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(RankedAnswer_t193891511, ___U3CconfidenceU3Ek__BackingField_2)); }
	inline double get_U3CconfidenceU3Ek__BackingField_2() const { return ___U3CconfidenceU3Ek__BackingField_2; }
	inline double* get_address_of_U3CconfidenceU3Ek__BackingField_2() { return &___U3CconfidenceU3Ek__BackingField_2; }
	inline void set_U3CconfidenceU3Ek__BackingField_2(double value)
	{
		___U3CconfidenceU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANKEDANSWER_T193891511_H
#ifndef RETRIEVEANDRANK_T1381045327_H
#define RETRIEVEANDRANK_T1381045327_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank
struct  RetrieveAndRank_t1381045327  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/LoadFileDelegate IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank::<LoadFile>k__BackingField
	LoadFileDelegate_t1720639326 * ___U3CLoadFileU3Ek__BackingField_14;
	// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/SaveFileDelegate IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank::<SaveFile>k__BackingField
	SaveFileDelegate_t3125227661 * ___U3CSaveFileU3Ek__BackingField_15;

public:
	inline static int32_t get_offset_of_U3CLoadFileU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(RetrieveAndRank_t1381045327, ___U3CLoadFileU3Ek__BackingField_14)); }
	inline LoadFileDelegate_t1720639326 * get_U3CLoadFileU3Ek__BackingField_14() const { return ___U3CLoadFileU3Ek__BackingField_14; }
	inline LoadFileDelegate_t1720639326 ** get_address_of_U3CLoadFileU3Ek__BackingField_14() { return &___U3CLoadFileU3Ek__BackingField_14; }
	inline void set_U3CLoadFileU3Ek__BackingField_14(LoadFileDelegate_t1720639326 * value)
	{
		___U3CLoadFileU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLoadFileU3Ek__BackingField_14), value);
	}

	inline static int32_t get_offset_of_U3CSaveFileU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(RetrieveAndRank_t1381045327, ___U3CSaveFileU3Ek__BackingField_15)); }
	inline SaveFileDelegate_t3125227661 * get_U3CSaveFileU3Ek__BackingField_15() const { return ___U3CSaveFileU3Ek__BackingField_15; }
	inline SaveFileDelegate_t3125227661 ** get_address_of_U3CSaveFileU3Ek__BackingField_15() { return &___U3CSaveFileU3Ek__BackingField_15; }
	inline void set_U3CSaveFileU3Ek__BackingField_15(SaveFileDelegate_t3125227661 * value)
	{
		___U3CSaveFileU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSaveFileU3Ek__BackingField_15), value);
	}
};

struct RetrieveAndRank_t1381045327_StaticFields
{
public:
	// FullSerializer.fsSerializer IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank::sm_Serializer
	fsSerializer_t4193731081 * ___sm_Serializer_1;

public:
	inline static int32_t get_offset_of_sm_Serializer_1() { return static_cast<int32_t>(offsetof(RetrieveAndRank_t1381045327_StaticFields, ___sm_Serializer_1)); }
	inline fsSerializer_t4193731081 * get_sm_Serializer_1() const { return ___sm_Serializer_1; }
	inline fsSerializer_t4193731081 ** get_address_of_sm_Serializer_1() { return &___sm_Serializer_1; }
	inline void set_sm_Serializer_1(fsSerializer_t4193731081 * value)
	{
		___sm_Serializer_1 = value;
		Il2CppCodeGenWriteBarrier((&___sm_Serializer_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RETRIEVEANDRANK_T1381045327_H
#ifndef RANKERERRORRESPONSEPAYLOAD_T2774706454_H
#define RANKERERRORRESPONSEPAYLOAD_T2774706454_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RankerErrorResponsePayload
struct  RankerErrorResponsePayload_t2774706454  : public RuntimeObject
{
public:
	// System.Int32 IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RankerErrorResponsePayload::<code>k__BackingField
	int32_t ___U3CcodeU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RankerErrorResponsePayload::<error>k__BackingField
	String_t* ___U3CerrorU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RankerErrorResponsePayload::<description>k__BackingField
	String_t* ___U3CdescriptionU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CcodeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RankerErrorResponsePayload_t2774706454, ___U3CcodeU3Ek__BackingField_0)); }
	inline int32_t get_U3CcodeU3Ek__BackingField_0() const { return ___U3CcodeU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CcodeU3Ek__BackingField_0() { return &___U3CcodeU3Ek__BackingField_0; }
	inline void set_U3CcodeU3Ek__BackingField_0(int32_t value)
	{
		___U3CcodeU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CerrorU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RankerErrorResponsePayload_t2774706454, ___U3CerrorU3Ek__BackingField_1)); }
	inline String_t* get_U3CerrorU3Ek__BackingField_1() const { return ___U3CerrorU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CerrorU3Ek__BackingField_1() { return &___U3CerrorU3Ek__BackingField_1; }
	inline void set_U3CerrorU3Ek__BackingField_1(String_t* value)
	{
		___U3CerrorU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CerrorU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CdescriptionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(RankerErrorResponsePayload_t2774706454, ___U3CdescriptionU3Ek__BackingField_2)); }
	inline String_t* get_U3CdescriptionU3Ek__BackingField_2() const { return ___U3CdescriptionU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CdescriptionU3Ek__BackingField_2() { return &___U3CdescriptionU3Ek__BackingField_2; }
	inline void set_U3CdescriptionU3Ek__BackingField_2(String_t* value)
	{
		___U3CdescriptionU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdescriptionU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANKERERRORRESPONSEPAYLOAD_T2774706454_H
#ifndef RANKEROUTPUTPAYLOAD_T247035662_H
#define RANKEROUTPUTPAYLOAD_T247035662_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RankerOutputPayload
struct  RankerOutputPayload_t247035662  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RankerOutputPayload::<ranker_id>k__BackingField
	String_t* ___U3Cranker_idU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RankerOutputPayload::<url>k__BackingField
	String_t* ___U3CurlU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RankerOutputPayload::<top_answer>k__BackingField
	String_t* ___U3Ctop_answerU3Ek__BackingField_2;
	// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RankedAnswer[] IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RankerOutputPayload::<answers>k__BackingField
	RankedAnswerU5BU5D_t2259955342* ___U3CanswersU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3Cranker_idU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RankerOutputPayload_t247035662, ___U3Cranker_idU3Ek__BackingField_0)); }
	inline String_t* get_U3Cranker_idU3Ek__BackingField_0() const { return ___U3Cranker_idU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3Cranker_idU3Ek__BackingField_0() { return &___U3Cranker_idU3Ek__BackingField_0; }
	inline void set_U3Cranker_idU3Ek__BackingField_0(String_t* value)
	{
		___U3Cranker_idU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cranker_idU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CurlU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RankerOutputPayload_t247035662, ___U3CurlU3Ek__BackingField_1)); }
	inline String_t* get_U3CurlU3Ek__BackingField_1() const { return ___U3CurlU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CurlU3Ek__BackingField_1() { return &___U3CurlU3Ek__BackingField_1; }
	inline void set_U3CurlU3Ek__BackingField_1(String_t* value)
	{
		___U3CurlU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CurlU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3Ctop_answerU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(RankerOutputPayload_t247035662, ___U3Ctop_answerU3Ek__BackingField_2)); }
	inline String_t* get_U3Ctop_answerU3Ek__BackingField_2() const { return ___U3Ctop_answerU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3Ctop_answerU3Ek__BackingField_2() { return &___U3Ctop_answerU3Ek__BackingField_2; }
	inline void set_U3Ctop_answerU3Ek__BackingField_2(String_t* value)
	{
		___U3Ctop_answerU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3Ctop_answerU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CanswersU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(RankerOutputPayload_t247035662, ___U3CanswersU3Ek__BackingField_3)); }
	inline RankedAnswerU5BU5D_t2259955342* get_U3CanswersU3Ek__BackingField_3() const { return ___U3CanswersU3Ek__BackingField_3; }
	inline RankedAnswerU5BU5D_t2259955342** get_address_of_U3CanswersU3Ek__BackingField_3() { return &___U3CanswersU3Ek__BackingField_3; }
	inline void set_U3CanswersU3Ek__BackingField_3(RankedAnswerU5BU5D_t2259955342* value)
	{
		___U3CanswersU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CanswersU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANKEROUTPUTPAYLOAD_T247035662_H
#ifndef CLUSTERINFO_T4006997306_H
#define CLUSTERINFO_T4006997306_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.ClusterInfo
struct  ClusterInfo_t4006997306  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.SolrClusterResponse IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.ClusterInfo::<Cluster>k__BackingField
	SolrClusterResponse_t1008480713 * ___U3CClusterU3Ek__BackingField_0;
	// System.String[] IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.ClusterInfo::<Configs>k__BackingField
	StringU5BU5D_t1642385972* ___U3CConfigsU3Ek__BackingField_1;
	// System.String[] IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.ClusterInfo::<Collections>k__BackingField
	StringU5BU5D_t1642385972* ___U3CCollectionsU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CClusterU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ClusterInfo_t4006997306, ___U3CClusterU3Ek__BackingField_0)); }
	inline SolrClusterResponse_t1008480713 * get_U3CClusterU3Ek__BackingField_0() const { return ___U3CClusterU3Ek__BackingField_0; }
	inline SolrClusterResponse_t1008480713 ** get_address_of_U3CClusterU3Ek__BackingField_0() { return &___U3CClusterU3Ek__BackingField_0; }
	inline void set_U3CClusterU3Ek__BackingField_0(SolrClusterResponse_t1008480713 * value)
	{
		___U3CClusterU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CClusterU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CConfigsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ClusterInfo_t4006997306, ___U3CConfigsU3Ek__BackingField_1)); }
	inline StringU5BU5D_t1642385972* get_U3CConfigsU3Ek__BackingField_1() const { return ___U3CConfigsU3Ek__BackingField_1; }
	inline StringU5BU5D_t1642385972** get_address_of_U3CConfigsU3Ek__BackingField_1() { return &___U3CConfigsU3Ek__BackingField_1; }
	inline void set_U3CConfigsU3Ek__BackingField_1(StringU5BU5D_t1642385972* value)
	{
		___U3CConfigsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CConfigsU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CCollectionsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ClusterInfo_t4006997306, ___U3CCollectionsU3Ek__BackingField_2)); }
	inline StringU5BU5D_t1642385972* get_U3CCollectionsU3Ek__BackingField_2() const { return ___U3CCollectionsU3Ek__BackingField_2; }
	inline StringU5BU5D_t1642385972** get_address_of_U3CCollectionsU3Ek__BackingField_2() { return &___U3CCollectionsU3Ek__BackingField_2; }
	inline void set_U3CCollectionsU3Ek__BackingField_2(StringU5BU5D_t1642385972* value)
	{
		___U3CCollectionsU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCollectionsU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLUSTERINFO_T4006997306_H
#ifndef COLLECTIONSACTION_T1867305463_H
#define COLLECTIONSACTION_T1867305463_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.CollectionsAction
struct  CollectionsAction_t1867305463  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTIONSACTION_T1867305463_H
#ifndef INDEXRESPONSE_T1068437599_H
#define INDEXRESPONSE_T1068437599_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.IndexResponse
struct  IndexResponse_t1068437599  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.ResponseHeader IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.IndexResponse::<responseHeader>k__BackingField
	ResponseHeader_t2485747570 * ___U3CresponseHeaderU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CresponseHeaderU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(IndexResponse_t1068437599, ___U3CresponseHeaderU3Ek__BackingField_0)); }
	inline ResponseHeader_t2485747570 * get_U3CresponseHeaderU3Ek__BackingField_0() const { return ___U3CresponseHeaderU3Ek__BackingField_0; }
	inline ResponseHeader_t2485747570 ** get_address_of_U3CresponseHeaderU3Ek__BackingField_0() { return &___U3CresponseHeaderU3Ek__BackingField_0; }
	inline void set_U3CresponseHeaderU3Ek__BackingField_0(ResponseHeader_t2485747570 * value)
	{
		___U3CresponseHeaderU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CresponseHeaderU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INDEXRESPONSE_T1068437599_H
#ifndef COLLECTIONSRESPONSE_T1627544196_H
#define COLLECTIONSRESPONSE_T1627544196_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.CollectionsResponse
struct  CollectionsResponse_t1627544196  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.ResponseHeader IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.CollectionsResponse::<responseHeader>k__BackingField
	ResponseHeader_t2485747570 * ___U3CresponseHeaderU3Ek__BackingField_0;
	// System.String[] IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.CollectionsResponse::<collections>k__BackingField
	StringU5BU5D_t1642385972* ___U3CcollectionsU3Ek__BackingField_1;
	// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.CollectionsResponse[] IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.CollectionsResponse::<response>k__BackingField
	CollectionsResponseU5BU5D_t1328740077* ___U3CresponseU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.CollectionsResponse::<core>k__BackingField
	String_t* ___U3CcoreU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CresponseHeaderU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CollectionsResponse_t1627544196, ___U3CresponseHeaderU3Ek__BackingField_0)); }
	inline ResponseHeader_t2485747570 * get_U3CresponseHeaderU3Ek__BackingField_0() const { return ___U3CresponseHeaderU3Ek__BackingField_0; }
	inline ResponseHeader_t2485747570 ** get_address_of_U3CresponseHeaderU3Ek__BackingField_0() { return &___U3CresponseHeaderU3Ek__BackingField_0; }
	inline void set_U3CresponseHeaderU3Ek__BackingField_0(ResponseHeader_t2485747570 * value)
	{
		___U3CresponseHeaderU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CresponseHeaderU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CcollectionsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CollectionsResponse_t1627544196, ___U3CcollectionsU3Ek__BackingField_1)); }
	inline StringU5BU5D_t1642385972* get_U3CcollectionsU3Ek__BackingField_1() const { return ___U3CcollectionsU3Ek__BackingField_1; }
	inline StringU5BU5D_t1642385972** get_address_of_U3CcollectionsU3Ek__BackingField_1() { return &___U3CcollectionsU3Ek__BackingField_1; }
	inline void set_U3CcollectionsU3Ek__BackingField_1(StringU5BU5D_t1642385972* value)
	{
		___U3CcollectionsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcollectionsU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CresponseU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CollectionsResponse_t1627544196, ___U3CresponseU3Ek__BackingField_2)); }
	inline CollectionsResponseU5BU5D_t1328740077* get_U3CresponseU3Ek__BackingField_2() const { return ___U3CresponseU3Ek__BackingField_2; }
	inline CollectionsResponseU5BU5D_t1328740077** get_address_of_U3CresponseU3Ek__BackingField_2() { return &___U3CresponseU3Ek__BackingField_2; }
	inline void set_U3CresponseU3Ek__BackingField_2(CollectionsResponseU5BU5D_t1328740077* value)
	{
		___U3CresponseU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CresponseU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CcoreU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(CollectionsResponse_t1627544196, ___U3CcoreU3Ek__BackingField_3)); }
	inline String_t* get_U3CcoreU3Ek__BackingField_3() const { return ___U3CcoreU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CcoreU3Ek__BackingField_3() { return &___U3CcoreU3Ek__BackingField_3; }
	inline void set_U3CcoreU3Ek__BackingField_3(String_t* value)
	{
		___U3CcoreU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcoreU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTIONSRESPONSE_T1627544196_H
#ifndef RESPONSEHEADER_T2485747570_H
#define RESPONSEHEADER_T2485747570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.ResponseHeader
struct  ResponseHeader_t2485747570  : public RuntimeObject
{
public:
	// System.Int32 IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.ResponseHeader::<status>k__BackingField
	int32_t ___U3CstatusU3Ek__BackingField_0;
	// System.Int32 IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.ResponseHeader::<QTime>k__BackingField
	int32_t ___U3CQTimeU3Ek__BackingField_1;
	// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.QueryParams IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.ResponseHeader::<_params>k__BackingField
	QueryParams_t3040774976 * ___U3C_paramsU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CstatusU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ResponseHeader_t2485747570, ___U3CstatusU3Ek__BackingField_0)); }
	inline int32_t get_U3CstatusU3Ek__BackingField_0() const { return ___U3CstatusU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CstatusU3Ek__BackingField_0() { return &___U3CstatusU3Ek__BackingField_0; }
	inline void set_U3CstatusU3Ek__BackingField_0(int32_t value)
	{
		___U3CstatusU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CQTimeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ResponseHeader_t2485747570, ___U3CQTimeU3Ek__BackingField_1)); }
	inline int32_t get_U3CQTimeU3Ek__BackingField_1() const { return ___U3CQTimeU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CQTimeU3Ek__BackingField_1() { return &___U3CQTimeU3Ek__BackingField_1; }
	inline void set_U3CQTimeU3Ek__BackingField_1(int32_t value)
	{
		___U3CQTimeU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3C_paramsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ResponseHeader_t2485747570, ___U3C_paramsU3Ek__BackingField_2)); }
	inline QueryParams_t3040774976 * get_U3C_paramsU3Ek__BackingField_2() const { return ___U3C_paramsU3Ek__BackingField_2; }
	inline QueryParams_t3040774976 ** get_address_of_U3C_paramsU3Ek__BackingField_2() { return &___U3C_paramsU3Ek__BackingField_2; }
	inline void set_U3C_paramsU3Ek__BackingField_2(QueryParams_t3040774976 * value)
	{
		___U3C_paramsU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3C_paramsU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESPONSEHEADER_T2485747570_H
#ifndef RESPONSE_T1432761593_H
#define RESPONSE_T1432761593_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.Response
struct  Response_t1432761593  : public RuntimeObject
{
public:
	// System.Int32 IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.Response::<numFound>k__BackingField
	int32_t ___U3CnumFoundU3Ek__BackingField_0;
	// System.Int32 IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.Response::<start>k__BackingField
	int32_t ___U3CstartU3Ek__BackingField_1;
	// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.Doc[] IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.Response::<docs>k__BackingField
	DocU5BU5D_t1574444043* ___U3CdocsU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CnumFoundU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Response_t1432761593, ___U3CnumFoundU3Ek__BackingField_0)); }
	inline int32_t get_U3CnumFoundU3Ek__BackingField_0() const { return ___U3CnumFoundU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CnumFoundU3Ek__BackingField_0() { return &___U3CnumFoundU3Ek__BackingField_0; }
	inline void set_U3CnumFoundU3Ek__BackingField_0(int32_t value)
	{
		___U3CnumFoundU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CstartU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Response_t1432761593, ___U3CstartU3Ek__BackingField_1)); }
	inline int32_t get_U3CstartU3Ek__BackingField_1() const { return ___U3CstartU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CstartU3Ek__BackingField_1() { return &___U3CstartU3Ek__BackingField_1; }
	inline void set_U3CstartU3Ek__BackingField_1(int32_t value)
	{
		___U3CstartU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CdocsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Response_t1432761593, ___U3CdocsU3Ek__BackingField_2)); }
	inline DocU5BU5D_t1574444043* get_U3CdocsU3Ek__BackingField_2() const { return ___U3CdocsU3Ek__BackingField_2; }
	inline DocU5BU5D_t1574444043** get_address_of_U3CdocsU3Ek__BackingField_2() { return &___U3CdocsU3Ek__BackingField_2; }
	inline void set_U3CdocsU3Ek__BackingField_2(DocU5BU5D_t1574444043* value)
	{
		___U3CdocsU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdocsU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESPONSE_T1432761593_H
#ifndef DOC_T3160830270_H
#define DOC_T3160830270_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.Doc
struct  Doc_t3160830270  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.Doc::<id>k__BackingField
	String_t* ___U3CidU3Ek__BackingField_0;
	// System.String[] IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.Doc::<body>k__BackingField
	StringU5BU5D_t1642385972* ___U3CbodyU3Ek__BackingField_1;
	// System.String[] IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.Doc::<title>k__BackingField
	StringU5BU5D_t1642385972* ___U3CtitleU3Ek__BackingField_2;
	// System.String[] IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.Doc::<author>k__BackingField
	StringU5BU5D_t1642385972* ___U3CauthorU3Ek__BackingField_3;
	// System.String[] IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.Doc::<bibliography>k__BackingField
	StringU5BU5D_t1642385972* ___U3CbibliographyU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CidU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Doc_t3160830270, ___U3CidU3Ek__BackingField_0)); }
	inline String_t* get_U3CidU3Ek__BackingField_0() const { return ___U3CidU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CidU3Ek__BackingField_0() { return &___U3CidU3Ek__BackingField_0; }
	inline void set_U3CidU3Ek__BackingField_0(String_t* value)
	{
		___U3CidU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CidU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CbodyU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Doc_t3160830270, ___U3CbodyU3Ek__BackingField_1)); }
	inline StringU5BU5D_t1642385972* get_U3CbodyU3Ek__BackingField_1() const { return ___U3CbodyU3Ek__BackingField_1; }
	inline StringU5BU5D_t1642385972** get_address_of_U3CbodyU3Ek__BackingField_1() { return &___U3CbodyU3Ek__BackingField_1; }
	inline void set_U3CbodyU3Ek__BackingField_1(StringU5BU5D_t1642385972* value)
	{
		___U3CbodyU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CbodyU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CtitleU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Doc_t3160830270, ___U3CtitleU3Ek__BackingField_2)); }
	inline StringU5BU5D_t1642385972* get_U3CtitleU3Ek__BackingField_2() const { return ___U3CtitleU3Ek__BackingField_2; }
	inline StringU5BU5D_t1642385972** get_address_of_U3CtitleU3Ek__BackingField_2() { return &___U3CtitleU3Ek__BackingField_2; }
	inline void set_U3CtitleU3Ek__BackingField_2(StringU5BU5D_t1642385972* value)
	{
		___U3CtitleU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtitleU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CauthorU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Doc_t3160830270, ___U3CauthorU3Ek__BackingField_3)); }
	inline StringU5BU5D_t1642385972* get_U3CauthorU3Ek__BackingField_3() const { return ___U3CauthorU3Ek__BackingField_3; }
	inline StringU5BU5D_t1642385972** get_address_of_U3CauthorU3Ek__BackingField_3() { return &___U3CauthorU3Ek__BackingField_3; }
	inline void set_U3CauthorU3Ek__BackingField_3(StringU5BU5D_t1642385972* value)
	{
		___U3CauthorU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CauthorU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CbibliographyU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Doc_t3160830270, ___U3CbibliographyU3Ek__BackingField_4)); }
	inline StringU5BU5D_t1642385972* get_U3CbibliographyU3Ek__BackingField_4() const { return ___U3CbibliographyU3Ek__BackingField_4; }
	inline StringU5BU5D_t1642385972** get_address_of_U3CbibliographyU3Ek__BackingField_4() { return &___U3CbibliographyU3Ek__BackingField_4; }
	inline void set_U3CbibliographyU3Ek__BackingField_4(StringU5BU5D_t1642385972* value)
	{
		___U3CbibliographyU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CbibliographyU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOC_T3160830270_H
#ifndef QUERYPARAMS_T3040774976_H
#define QUERYPARAMS_T3040774976_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.QueryParams
struct  QueryParams_t3040774976  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.QueryParams::<q>k__BackingField
	String_t* ___U3CqU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.QueryParams::<fl>k__BackingField
	String_t* ___U3CflU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.QueryParams::<wt>k__BackingField
	String_t* ___U3CwtU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CqU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(QueryParams_t3040774976, ___U3CqU3Ek__BackingField_0)); }
	inline String_t* get_U3CqU3Ek__BackingField_0() const { return ___U3CqU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CqU3Ek__BackingField_0() { return &___U3CqU3Ek__BackingField_0; }
	inline void set_U3CqU3Ek__BackingField_0(String_t* value)
	{
		___U3CqU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CqU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CflU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(QueryParams_t3040774976, ___U3CflU3Ek__BackingField_1)); }
	inline String_t* get_U3CflU3Ek__BackingField_1() const { return ___U3CflU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CflU3Ek__BackingField_1() { return &___U3CflU3Ek__BackingField_1; }
	inline void set_U3CflU3Ek__BackingField_1(String_t* value)
	{
		___U3CflU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CflU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CwtU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(QueryParams_t3040774976, ___U3CwtU3Ek__BackingField_2)); }
	inline String_t* get_U3CwtU3Ek__BackingField_2() const { return ___U3CwtU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CwtU3Ek__BackingField_2() { return &___U3CwtU3Ek__BackingField_2; }
	inline void set_U3CwtU3Ek__BackingField_2(String_t* value)
	{
		___U3CwtU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwtU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUERYPARAMS_T3040774976_H
#ifndef SEARCHRESPONSE_T1678084193_H
#define SEARCHRESPONSE_T1678084193_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.SearchResponse
struct  SearchResponse_t1678084193  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.ResponseHeader IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.SearchResponse::<responseHeader>k__BackingField
	ResponseHeader_t2485747570 * ___U3CresponseHeaderU3Ek__BackingField_0;
	// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.Response IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.SearchResponse::<response>k__BackingField
	Response_t1432761593 * ___U3CresponseU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CresponseHeaderU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SearchResponse_t1678084193, ___U3CresponseHeaderU3Ek__BackingField_0)); }
	inline ResponseHeader_t2485747570 * get_U3CresponseHeaderU3Ek__BackingField_0() const { return ___U3CresponseHeaderU3Ek__BackingField_0; }
	inline ResponseHeader_t2485747570 ** get_address_of_U3CresponseHeaderU3Ek__BackingField_0() { return &___U3CresponseHeaderU3Ek__BackingField_0; }
	inline void set_U3CresponseHeaderU3Ek__BackingField_0(ResponseHeader_t2485747570 * value)
	{
		___U3CresponseHeaderU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CresponseHeaderU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CresponseU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(SearchResponse_t1678084193, ___U3CresponseU3Ek__BackingField_1)); }
	inline Response_t1432761593 * get_U3CresponseU3Ek__BackingField_1() const { return ___U3CresponseU3Ek__BackingField_1; }
	inline Response_t1432761593 ** get_address_of_U3CresponseU3Ek__BackingField_1() { return &___U3CresponseU3Ek__BackingField_1; }
	inline void set_U3CresponseU3Ek__BackingField_1(Response_t1432761593 * value)
	{
		___U3CresponseU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CresponseU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEARCHRESPONSE_T1678084193_H
#ifndef DELETECONFIGRESPONSE_T793172430_H
#define DELETECONFIGRESPONSE_T793172430_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.DeleteConfigResponse
struct  DeleteConfigResponse_t793172430  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.DeleteConfigResponse::<message>k__BackingField
	String_t* ___U3CmessageU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.DeleteConfigResponse::<statusCode>k__BackingField
	String_t* ___U3CstatusCodeU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmessageU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DeleteConfigResponse_t793172430, ___U3CmessageU3Ek__BackingField_0)); }
	inline String_t* get_U3CmessageU3Ek__BackingField_0() const { return ___U3CmessageU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CmessageU3Ek__BackingField_0() { return &___U3CmessageU3Ek__BackingField_0; }
	inline void set_U3CmessageU3Ek__BackingField_0(String_t* value)
	{
		___U3CmessageU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmessageU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CstatusCodeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(DeleteConfigResponse_t793172430, ___U3CstatusCodeU3Ek__BackingField_1)); }
	inline String_t* get_U3CstatusCodeU3Ek__BackingField_1() const { return ___U3CstatusCodeU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CstatusCodeU3Ek__BackingField_1() { return &___U3CstatusCodeU3Ek__BackingField_1; }
	inline void set_U3CstatusCodeU3Ek__BackingField_1(String_t* value)
	{
		___U3CstatusCodeU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstatusCodeU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELETECONFIGRESPONSE_T793172430_H
#ifndef FINDCLASSIFIERREQ_T3159305857_H
#define FINDCLASSIFIERREQ_T3159305857_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.NaturalLanguageClassifier/FindClassifierReq
struct  FindClassifierReq_t3159305857  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.NaturalLanguageClassifier IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.NaturalLanguageClassifier/FindClassifierReq::<Service>k__BackingField
	NaturalLanguageClassifier_t2492034444 * ___U3CServiceU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.NaturalLanguageClassifier/FindClassifierReq::<ClassifierName>k__BackingField
	String_t* ___U3CClassifierNameU3Ek__BackingField_1;
	// IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.NaturalLanguageClassifier/OnFindClassifier IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.NaturalLanguageClassifier/FindClassifierReq::<Callback>k__BackingField
	OnFindClassifier_t2470508822 * ___U3CCallbackU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CServiceU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(FindClassifierReq_t3159305857, ___U3CServiceU3Ek__BackingField_0)); }
	inline NaturalLanguageClassifier_t2492034444 * get_U3CServiceU3Ek__BackingField_0() const { return ___U3CServiceU3Ek__BackingField_0; }
	inline NaturalLanguageClassifier_t2492034444 ** get_address_of_U3CServiceU3Ek__BackingField_0() { return &___U3CServiceU3Ek__BackingField_0; }
	inline void set_U3CServiceU3Ek__BackingField_0(NaturalLanguageClassifier_t2492034444 * value)
	{
		___U3CServiceU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CServiceU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CClassifierNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(FindClassifierReq_t3159305857, ___U3CClassifierNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CClassifierNameU3Ek__BackingField_1() const { return ___U3CClassifierNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CClassifierNameU3Ek__BackingField_1() { return &___U3CClassifierNameU3Ek__BackingField_1; }
	inline void set_U3CClassifierNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CClassifierNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CClassifierNameU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(FindClassifierReq_t3159305857, ___U3CCallbackU3Ek__BackingField_2)); }
	inline OnFindClassifier_t2470508822 * get_U3CCallbackU3Ek__BackingField_2() const { return ___U3CCallbackU3Ek__BackingField_2; }
	inline OnFindClassifier_t2470508822 ** get_address_of_U3CCallbackU3Ek__BackingField_2() { return &___U3CCallbackU3Ek__BackingField_2; }
	inline void set_U3CCallbackU3Ek__BackingField_2(OnFindClassifier_t2470508822 * value)
	{
		___U3CCallbackU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FINDCLASSIFIERREQ_T3159305857_H
#ifndef REQUEST_T466816980_H
#define REQUEST_T466816980_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request
struct  Request_t466816980  : public RuntimeObject
{
public:
	// System.Single IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Timeout>k__BackingField
	float ___U3CTimeoutU3Ek__BackingField_0;
	// System.Boolean IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Cancel>k__BackingField
	bool ___U3CCancelU3Ek__BackingField_1;
	// System.Boolean IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Delete>k__BackingField
	bool ___U3CDeleteU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Function>k__BackingField
	String_t* ___U3CFunctionU3Ek__BackingField_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Parameters>k__BackingField
	Dictionary_2_t309261261 * ___U3CParametersU3Ek__BackingField_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Headers>k__BackingField
	Dictionary_2_t3943999495 * ___U3CHeadersU3Ek__BackingField_5;
	// System.Byte[] IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Send>k__BackingField
	ByteU5BU5D_t3397334013* ___U3CSendU3Ek__BackingField_6;
	// System.Collections.Generic.Dictionary`2<System.String,IBM.Watson.DeveloperCloud.Connection.RESTConnector/Form> IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Forms>k__BackingField
	Dictionary_2_t2694055125 * ___U3CFormsU3Ek__BackingField_7;
	// IBM.Watson.DeveloperCloud.Connection.RESTConnector/ResponseEvent IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<OnResponse>k__BackingField
	ResponseEvent_t1616568356 * ___U3COnResponseU3Ek__BackingField_8;
	// IBM.Watson.DeveloperCloud.Connection.RESTConnector/ProgressEvent IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<OnDownloadProgress>k__BackingField
	ProgressEvent_t4185145044 * ___U3COnDownloadProgressU3Ek__BackingField_9;
	// IBM.Watson.DeveloperCloud.Connection.RESTConnector/ProgressEvent IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<OnUploadProgress>k__BackingField
	ProgressEvent_t4185145044 * ___U3COnUploadProgressU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of_U3CTimeoutU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CTimeoutU3Ek__BackingField_0)); }
	inline float get_U3CTimeoutU3Ek__BackingField_0() const { return ___U3CTimeoutU3Ek__BackingField_0; }
	inline float* get_address_of_U3CTimeoutU3Ek__BackingField_0() { return &___U3CTimeoutU3Ek__BackingField_0; }
	inline void set_U3CTimeoutU3Ek__BackingField_0(float value)
	{
		___U3CTimeoutU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CCancelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CCancelU3Ek__BackingField_1)); }
	inline bool get_U3CCancelU3Ek__BackingField_1() const { return ___U3CCancelU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CCancelU3Ek__BackingField_1() { return &___U3CCancelU3Ek__BackingField_1; }
	inline void set_U3CCancelU3Ek__BackingField_1(bool value)
	{
		___U3CCancelU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CDeleteU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CDeleteU3Ek__BackingField_2)); }
	inline bool get_U3CDeleteU3Ek__BackingField_2() const { return ___U3CDeleteU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CDeleteU3Ek__BackingField_2() { return &___U3CDeleteU3Ek__BackingField_2; }
	inline void set_U3CDeleteU3Ek__BackingField_2(bool value)
	{
		___U3CDeleteU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CFunctionU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CFunctionU3Ek__BackingField_3)); }
	inline String_t* get_U3CFunctionU3Ek__BackingField_3() const { return ___U3CFunctionU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CFunctionU3Ek__BackingField_3() { return &___U3CFunctionU3Ek__BackingField_3; }
	inline void set_U3CFunctionU3Ek__BackingField_3(String_t* value)
	{
		___U3CFunctionU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFunctionU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CParametersU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CParametersU3Ek__BackingField_4)); }
	inline Dictionary_2_t309261261 * get_U3CParametersU3Ek__BackingField_4() const { return ___U3CParametersU3Ek__BackingField_4; }
	inline Dictionary_2_t309261261 ** get_address_of_U3CParametersU3Ek__BackingField_4() { return &___U3CParametersU3Ek__BackingField_4; }
	inline void set_U3CParametersU3Ek__BackingField_4(Dictionary_2_t309261261 * value)
	{
		___U3CParametersU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CParametersU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CHeadersU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CHeadersU3Ek__BackingField_5)); }
	inline Dictionary_2_t3943999495 * get_U3CHeadersU3Ek__BackingField_5() const { return ___U3CHeadersU3Ek__BackingField_5; }
	inline Dictionary_2_t3943999495 ** get_address_of_U3CHeadersU3Ek__BackingField_5() { return &___U3CHeadersU3Ek__BackingField_5; }
	inline void set_U3CHeadersU3Ek__BackingField_5(Dictionary_2_t3943999495 * value)
	{
		___U3CHeadersU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CHeadersU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CSendU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CSendU3Ek__BackingField_6)); }
	inline ByteU5BU5D_t3397334013* get_U3CSendU3Ek__BackingField_6() const { return ___U3CSendU3Ek__BackingField_6; }
	inline ByteU5BU5D_t3397334013** get_address_of_U3CSendU3Ek__BackingField_6() { return &___U3CSendU3Ek__BackingField_6; }
	inline void set_U3CSendU3Ek__BackingField_6(ByteU5BU5D_t3397334013* value)
	{
		___U3CSendU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSendU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CFormsU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CFormsU3Ek__BackingField_7)); }
	inline Dictionary_2_t2694055125 * get_U3CFormsU3Ek__BackingField_7() const { return ___U3CFormsU3Ek__BackingField_7; }
	inline Dictionary_2_t2694055125 ** get_address_of_U3CFormsU3Ek__BackingField_7() { return &___U3CFormsU3Ek__BackingField_7; }
	inline void set_U3CFormsU3Ek__BackingField_7(Dictionary_2_t2694055125 * value)
	{
		___U3CFormsU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFormsU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3COnResponseU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3COnResponseU3Ek__BackingField_8)); }
	inline ResponseEvent_t1616568356 * get_U3COnResponseU3Ek__BackingField_8() const { return ___U3COnResponseU3Ek__BackingField_8; }
	inline ResponseEvent_t1616568356 ** get_address_of_U3COnResponseU3Ek__BackingField_8() { return &___U3COnResponseU3Ek__BackingField_8; }
	inline void set_U3COnResponseU3Ek__BackingField_8(ResponseEvent_t1616568356 * value)
	{
		___U3COnResponseU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3COnResponseU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3COnDownloadProgressU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3COnDownloadProgressU3Ek__BackingField_9)); }
	inline ProgressEvent_t4185145044 * get_U3COnDownloadProgressU3Ek__BackingField_9() const { return ___U3COnDownloadProgressU3Ek__BackingField_9; }
	inline ProgressEvent_t4185145044 ** get_address_of_U3COnDownloadProgressU3Ek__BackingField_9() { return &___U3COnDownloadProgressU3Ek__BackingField_9; }
	inline void set_U3COnDownloadProgressU3Ek__BackingField_9(ProgressEvent_t4185145044 * value)
	{
		___U3COnDownloadProgressU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3COnDownloadProgressU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3COnUploadProgressU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3COnUploadProgressU3Ek__BackingField_10)); }
	inline ProgressEvent_t4185145044 * get_U3COnUploadProgressU3Ek__BackingField_10() const { return ___U3COnUploadProgressU3Ek__BackingField_10; }
	inline ProgressEvent_t4185145044 ** get_address_of_U3COnUploadProgressU3Ek__BackingField_10() { return &___U3COnUploadProgressU3Ek__BackingField_10; }
	inline void set_U3COnUploadProgressU3Ek__BackingField_10(ProgressEvent_t4185145044 * value)
	{
		___U3COnUploadProgressU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3COnUploadProgressU3Ek__BackingField_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUEST_T466816980_H
#ifndef TRAITTREENODE_T694336958_H
#define TRAITTREENODE_T694336958_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v2.TraitTreeNode
struct  TraitTreeNode_t694336958  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v2.TraitTreeNode::<id>k__BackingField
	String_t* ___U3CidU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v2.TraitTreeNode::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v2.TraitTreeNode::<category>k__BackingField
	String_t* ___U3CcategoryU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v2.TraitTreeNode::<percentage>k__BackingField
	String_t* ___U3CpercentageU3Ek__BackingField_3;
	// System.String IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v2.TraitTreeNode::<sampling_error>k__BackingField
	String_t* ___U3Csampling_errorU3Ek__BackingField_4;
	// System.String IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v2.TraitTreeNode::<raw_score>k__BackingField
	String_t* ___U3Craw_scoreU3Ek__BackingField_5;
	// System.String IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v2.TraitTreeNode::<raw_sampling_error>k__BackingField
	String_t* ___U3Craw_sampling_errorU3Ek__BackingField_6;
	// IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v2.TraitTreeNode[] IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v2.TraitTreeNode::<children>k__BackingField
	TraitTreeNodeU5BU5D_t4145450379* ___U3CchildrenU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_U3CidU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TraitTreeNode_t694336958, ___U3CidU3Ek__BackingField_0)); }
	inline String_t* get_U3CidU3Ek__BackingField_0() const { return ___U3CidU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CidU3Ek__BackingField_0() { return &___U3CidU3Ek__BackingField_0; }
	inline void set_U3CidU3Ek__BackingField_0(String_t* value)
	{
		___U3CidU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CidU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TraitTreeNode_t694336958, ___U3CnameU3Ek__BackingField_1)); }
	inline String_t* get_U3CnameU3Ek__BackingField_1() const { return ___U3CnameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_1() { return &___U3CnameU3Ek__BackingField_1; }
	inline void set_U3CnameU3Ek__BackingField_1(String_t* value)
	{
		___U3CnameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CcategoryU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(TraitTreeNode_t694336958, ___U3CcategoryU3Ek__BackingField_2)); }
	inline String_t* get_U3CcategoryU3Ek__BackingField_2() const { return ___U3CcategoryU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CcategoryU3Ek__BackingField_2() { return &___U3CcategoryU3Ek__BackingField_2; }
	inline void set_U3CcategoryU3Ek__BackingField_2(String_t* value)
	{
		___U3CcategoryU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcategoryU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CpercentageU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(TraitTreeNode_t694336958, ___U3CpercentageU3Ek__BackingField_3)); }
	inline String_t* get_U3CpercentageU3Ek__BackingField_3() const { return ___U3CpercentageU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CpercentageU3Ek__BackingField_3() { return &___U3CpercentageU3Ek__BackingField_3; }
	inline void set_U3CpercentageU3Ek__BackingField_3(String_t* value)
	{
		___U3CpercentageU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpercentageU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3Csampling_errorU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(TraitTreeNode_t694336958, ___U3Csampling_errorU3Ek__BackingField_4)); }
	inline String_t* get_U3Csampling_errorU3Ek__BackingField_4() const { return ___U3Csampling_errorU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3Csampling_errorU3Ek__BackingField_4() { return &___U3Csampling_errorU3Ek__BackingField_4; }
	inline void set_U3Csampling_errorU3Ek__BackingField_4(String_t* value)
	{
		___U3Csampling_errorU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3Csampling_errorU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3Craw_scoreU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(TraitTreeNode_t694336958, ___U3Craw_scoreU3Ek__BackingField_5)); }
	inline String_t* get_U3Craw_scoreU3Ek__BackingField_5() const { return ___U3Craw_scoreU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3Craw_scoreU3Ek__BackingField_5() { return &___U3Craw_scoreU3Ek__BackingField_5; }
	inline void set_U3Craw_scoreU3Ek__BackingField_5(String_t* value)
	{
		___U3Craw_scoreU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3Craw_scoreU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3Craw_sampling_errorU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(TraitTreeNode_t694336958, ___U3Craw_sampling_errorU3Ek__BackingField_6)); }
	inline String_t* get_U3Craw_sampling_errorU3Ek__BackingField_6() const { return ___U3Craw_sampling_errorU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3Craw_sampling_errorU3Ek__BackingField_6() { return &___U3Craw_sampling_errorU3Ek__BackingField_6; }
	inline void set_U3Craw_sampling_errorU3Ek__BackingField_6(String_t* value)
	{
		___U3Craw_sampling_errorU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3Craw_sampling_errorU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CchildrenU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(TraitTreeNode_t694336958, ___U3CchildrenU3Ek__BackingField_7)); }
	inline TraitTreeNodeU5BU5D_t4145450379* get_U3CchildrenU3Ek__BackingField_7() const { return ___U3CchildrenU3Ek__BackingField_7; }
	inline TraitTreeNodeU5BU5D_t4145450379** get_address_of_U3CchildrenU3Ek__BackingField_7() { return &___U3CchildrenU3Ek__BackingField_7; }
	inline void set_U3CchildrenU3Ek__BackingField_7(TraitTreeNodeU5BU5D_t4145450379* value)
	{
		___U3CchildrenU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CchildrenU3Ek__BackingField_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRAITTREENODE_T694336958_H
#ifndef PROFILE_T4293889999_H
#define PROFILE_T4293889999_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v2.Profile
struct  Profile_t4293889999  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v2.TraitTreeNode IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v2.Profile::<tree>k__BackingField
	TraitTreeNode_t694336958 * ___U3CtreeU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v2.Profile::<id>k__BackingField
	String_t* ___U3CidU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v2.Profile::<source>k__BackingField
	String_t* ___U3CsourceU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v2.Profile::<processed_lang>k__BackingField
	String_t* ___U3Cprocessed_langU3Ek__BackingField_3;
	// System.String IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v2.Profile::<word_count>k__BackingField
	String_t* ___U3Cword_countU3Ek__BackingField_4;
	// System.String IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v2.Profile::<word_count_message>k__BackingField
	String_t* ___U3Cword_count_messageU3Ek__BackingField_5;
	// IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v2.Warning[] IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v2.Profile::<warnings>k__BackingField
	WarningU5BU5D_t523936175* ___U3CwarningsU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CtreeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Profile_t4293889999, ___U3CtreeU3Ek__BackingField_0)); }
	inline TraitTreeNode_t694336958 * get_U3CtreeU3Ek__BackingField_0() const { return ___U3CtreeU3Ek__BackingField_0; }
	inline TraitTreeNode_t694336958 ** get_address_of_U3CtreeU3Ek__BackingField_0() { return &___U3CtreeU3Ek__BackingField_0; }
	inline void set_U3CtreeU3Ek__BackingField_0(TraitTreeNode_t694336958 * value)
	{
		___U3CtreeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtreeU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CidU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Profile_t4293889999, ___U3CidU3Ek__BackingField_1)); }
	inline String_t* get_U3CidU3Ek__BackingField_1() const { return ___U3CidU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CidU3Ek__BackingField_1() { return &___U3CidU3Ek__BackingField_1; }
	inline void set_U3CidU3Ek__BackingField_1(String_t* value)
	{
		___U3CidU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CidU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CsourceU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Profile_t4293889999, ___U3CsourceU3Ek__BackingField_2)); }
	inline String_t* get_U3CsourceU3Ek__BackingField_2() const { return ___U3CsourceU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CsourceU3Ek__BackingField_2() { return &___U3CsourceU3Ek__BackingField_2; }
	inline void set_U3CsourceU3Ek__BackingField_2(String_t* value)
	{
		___U3CsourceU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsourceU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3Cprocessed_langU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Profile_t4293889999, ___U3Cprocessed_langU3Ek__BackingField_3)); }
	inline String_t* get_U3Cprocessed_langU3Ek__BackingField_3() const { return ___U3Cprocessed_langU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3Cprocessed_langU3Ek__BackingField_3() { return &___U3Cprocessed_langU3Ek__BackingField_3; }
	inline void set_U3Cprocessed_langU3Ek__BackingField_3(String_t* value)
	{
		___U3Cprocessed_langU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cprocessed_langU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3Cword_countU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Profile_t4293889999, ___U3Cword_countU3Ek__BackingField_4)); }
	inline String_t* get_U3Cword_countU3Ek__BackingField_4() const { return ___U3Cword_countU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3Cword_countU3Ek__BackingField_4() { return &___U3Cword_countU3Ek__BackingField_4; }
	inline void set_U3Cword_countU3Ek__BackingField_4(String_t* value)
	{
		___U3Cword_countU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cword_countU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3Cword_count_messageU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Profile_t4293889999, ___U3Cword_count_messageU3Ek__BackingField_5)); }
	inline String_t* get_U3Cword_count_messageU3Ek__BackingField_5() const { return ___U3Cword_count_messageU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3Cword_count_messageU3Ek__BackingField_5() { return &___U3Cword_count_messageU3Ek__BackingField_5; }
	inline void set_U3Cword_count_messageU3Ek__BackingField_5(String_t* value)
	{
		___U3Cword_count_messageU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cword_count_messageU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CwarningsU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Profile_t4293889999, ___U3CwarningsU3Ek__BackingField_6)); }
	inline WarningU5BU5D_t523936175* get_U3CwarningsU3Ek__BackingField_6() const { return ___U3CwarningsU3Ek__BackingField_6; }
	inline WarningU5BU5D_t523936175** get_address_of_U3CwarningsU3Ek__BackingField_6() { return &___U3CwarningsU3Ek__BackingField_6; }
	inline void set_U3CwarningsU3Ek__BackingField_6(WarningU5BU5D_t523936175* value)
	{
		___U3CwarningsU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwarningsU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROFILE_T4293889999_H
#ifndef CHECKSERVICESTATUS_T740786868_H
#define CHECKSERVICESTATUS_T740786868_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.NaturalLanguageClassifier/CheckServiceStatus
struct  CheckServiceStatus_t740786868  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.NaturalLanguageClassifier IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.NaturalLanguageClassifier/CheckServiceStatus::m_Service
	NaturalLanguageClassifier_t2492034444 * ___m_Service_0;
	// IBM.Watson.DeveloperCloud.Services.ServiceStatus IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.NaturalLanguageClassifier/CheckServiceStatus::m_Callback
	ServiceStatus_t1443707987 * ___m_Callback_1;
	// System.Int32 IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.NaturalLanguageClassifier/CheckServiceStatus::m_GetClassifierCount
	int32_t ___m_GetClassifierCount_2;
	// System.Int32 IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.NaturalLanguageClassifier/CheckServiceStatus::m_ClassifyCount
	int32_t ___m_ClassifyCount_3;

public:
	inline static int32_t get_offset_of_m_Service_0() { return static_cast<int32_t>(offsetof(CheckServiceStatus_t740786868, ___m_Service_0)); }
	inline NaturalLanguageClassifier_t2492034444 * get_m_Service_0() const { return ___m_Service_0; }
	inline NaturalLanguageClassifier_t2492034444 ** get_address_of_m_Service_0() { return &___m_Service_0; }
	inline void set_m_Service_0(NaturalLanguageClassifier_t2492034444 * value)
	{
		___m_Service_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Service_0), value);
	}

	inline static int32_t get_offset_of_m_Callback_1() { return static_cast<int32_t>(offsetof(CheckServiceStatus_t740786868, ___m_Callback_1)); }
	inline ServiceStatus_t1443707987 * get_m_Callback_1() const { return ___m_Callback_1; }
	inline ServiceStatus_t1443707987 ** get_address_of_m_Callback_1() { return &___m_Callback_1; }
	inline void set_m_Callback_1(ServiceStatus_t1443707987 * value)
	{
		___m_Callback_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Callback_1), value);
	}

	inline static int32_t get_offset_of_m_GetClassifierCount_2() { return static_cast<int32_t>(offsetof(CheckServiceStatus_t740786868, ___m_GetClassifierCount_2)); }
	inline int32_t get_m_GetClassifierCount_2() const { return ___m_GetClassifierCount_2; }
	inline int32_t* get_address_of_m_GetClassifierCount_2() { return &___m_GetClassifierCount_2; }
	inline void set_m_GetClassifierCount_2(int32_t value)
	{
		___m_GetClassifierCount_2 = value;
	}

	inline static int32_t get_offset_of_m_ClassifyCount_3() { return static_cast<int32_t>(offsetof(CheckServiceStatus_t740786868, ___m_ClassifyCount_3)); }
	inline int32_t get_m_ClassifyCount_3() const { return ___m_ClassifyCount_3; }
	inline int32_t* get_address_of_m_ClassifyCount_3() { return &___m_ClassifyCount_3; }
	inline void set_m_ClassifyCount_3(int32_t value)
	{
		___m_ClassifyCount_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHECKSERVICESTATUS_T740786868_H
#ifndef NATURALLANGUAGECLASSIFIER_T2492034444_H
#define NATURALLANGUAGECLASSIFIER_T2492034444_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.NaturalLanguageClassifier
struct  NaturalLanguageClassifier_t2492034444  : public RuntimeObject
{
public:
	// System.Boolean IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.NaturalLanguageClassifier::<DisableCache>k__BackingField
	bool ___U3CDisableCacheU3Ek__BackingField_0;
	// System.Collections.Generic.Dictionary`2<System.String,IBM.Watson.DeveloperCloud.Utilities.DataCache> IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.NaturalLanguageClassifier::m_ClassifyCache
	Dictionary_2_t1870152036 * ___m_ClassifyCache_3;

public:
	inline static int32_t get_offset_of_U3CDisableCacheU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(NaturalLanguageClassifier_t2492034444, ___U3CDisableCacheU3Ek__BackingField_0)); }
	inline bool get_U3CDisableCacheU3Ek__BackingField_0() const { return ___U3CDisableCacheU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CDisableCacheU3Ek__BackingField_0() { return &___U3CDisableCacheU3Ek__BackingField_0; }
	inline void set_U3CDisableCacheU3Ek__BackingField_0(bool value)
	{
		___U3CDisableCacheU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_m_ClassifyCache_3() { return static_cast<int32_t>(offsetof(NaturalLanguageClassifier_t2492034444, ___m_ClassifyCache_3)); }
	inline Dictionary_2_t1870152036 * get_m_ClassifyCache_3() const { return ___m_ClassifyCache_3; }
	inline Dictionary_2_t1870152036 ** get_address_of_m_ClassifyCache_3() { return &___m_ClassifyCache_3; }
	inline void set_m_ClassifyCache_3(Dictionary_2_t1870152036 * value)
	{
		___m_ClassifyCache_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_ClassifyCache_3), value);
	}
};

struct NaturalLanguageClassifier_t2492034444_StaticFields
{
public:
	// FullSerializer.fsSerializer IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.NaturalLanguageClassifier::sm_Serializer
	fsSerializer_t4193731081 * ___sm_Serializer_2;

public:
	inline static int32_t get_offset_of_sm_Serializer_2() { return static_cast<int32_t>(offsetof(NaturalLanguageClassifier_t2492034444_StaticFields, ___sm_Serializer_2)); }
	inline fsSerializer_t4193731081 * get_sm_Serializer_2() const { return ___sm_Serializer_2; }
	inline fsSerializer_t4193731081 ** get_address_of_sm_Serializer_2() { return &___sm_Serializer_2; }
	inline void set_sm_Serializer_2(fsSerializer_t4193731081 * value)
	{
		___sm_Serializer_2 = value;
		Il2CppCodeGenWriteBarrier((&___sm_Serializer_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATURALLANGUAGECLASSIFIER_T2492034444_H
#ifndef CLASSIFIER_T1414011573_H
#define CLASSIFIER_T1414011573_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.Classifier
struct  Classifier_t1414011573  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.Classifier::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.Classifier::<language>k__BackingField
	String_t* ___U3ClanguageU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.Classifier::<url>k__BackingField
	String_t* ___U3CurlU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.Classifier::<classifier_id>k__BackingField
	String_t* ___U3Cclassifier_idU3Ek__BackingField_3;
	// System.String IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.Classifier::<created>k__BackingField
	String_t* ___U3CcreatedU3Ek__BackingField_4;
	// System.String IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.Classifier::<status>k__BackingField
	String_t* ___U3CstatusU3Ek__BackingField_5;
	// System.String IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.Classifier::<status_description>k__BackingField
	String_t* ___U3Cstatus_descriptionU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Classifier_t1414011573, ___U3CnameU3Ek__BackingField_0)); }
	inline String_t* get_U3CnameU3Ek__BackingField_0() const { return ___U3CnameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_0() { return &___U3CnameU3Ek__BackingField_0; }
	inline void set_U3CnameU3Ek__BackingField_0(String_t* value)
	{
		___U3CnameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3ClanguageU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Classifier_t1414011573, ___U3ClanguageU3Ek__BackingField_1)); }
	inline String_t* get_U3ClanguageU3Ek__BackingField_1() const { return ___U3ClanguageU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3ClanguageU3Ek__BackingField_1() { return &___U3ClanguageU3Ek__BackingField_1; }
	inline void set_U3ClanguageU3Ek__BackingField_1(String_t* value)
	{
		___U3ClanguageU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClanguageU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CurlU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Classifier_t1414011573, ___U3CurlU3Ek__BackingField_2)); }
	inline String_t* get_U3CurlU3Ek__BackingField_2() const { return ___U3CurlU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CurlU3Ek__BackingField_2() { return &___U3CurlU3Ek__BackingField_2; }
	inline void set_U3CurlU3Ek__BackingField_2(String_t* value)
	{
		___U3CurlU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CurlU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3Cclassifier_idU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Classifier_t1414011573, ___U3Cclassifier_idU3Ek__BackingField_3)); }
	inline String_t* get_U3Cclassifier_idU3Ek__BackingField_3() const { return ___U3Cclassifier_idU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3Cclassifier_idU3Ek__BackingField_3() { return &___U3Cclassifier_idU3Ek__BackingField_3; }
	inline void set_U3Cclassifier_idU3Ek__BackingField_3(String_t* value)
	{
		___U3Cclassifier_idU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cclassifier_idU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CcreatedU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Classifier_t1414011573, ___U3CcreatedU3Ek__BackingField_4)); }
	inline String_t* get_U3CcreatedU3Ek__BackingField_4() const { return ___U3CcreatedU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CcreatedU3Ek__BackingField_4() { return &___U3CcreatedU3Ek__BackingField_4; }
	inline void set_U3CcreatedU3Ek__BackingField_4(String_t* value)
	{
		___U3CcreatedU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcreatedU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CstatusU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Classifier_t1414011573, ___U3CstatusU3Ek__BackingField_5)); }
	inline String_t* get_U3CstatusU3Ek__BackingField_5() const { return ___U3CstatusU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CstatusU3Ek__BackingField_5() { return &___U3CstatusU3Ek__BackingField_5; }
	inline void set_U3CstatusU3Ek__BackingField_5(String_t* value)
	{
		___U3CstatusU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstatusU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3Cstatus_descriptionU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Classifier_t1414011573, ___U3Cstatus_descriptionU3Ek__BackingField_6)); }
	inline String_t* get_U3Cstatus_descriptionU3Ek__BackingField_6() const { return ___U3Cstatus_descriptionU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3Cstatus_descriptionU3Ek__BackingField_6() { return &___U3Cstatus_descriptionU3Ek__BackingField_6; }
	inline void set_U3Cstatus_descriptionU3Ek__BackingField_6(String_t* value)
	{
		___U3Cstatus_descriptionU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cstatus_descriptionU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLASSIFIER_T1414011573_H
#ifndef CHECKSERVICESTATUS_T297802562_H
#define CHECKSERVICESTATUS_T297802562_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.LanguageTranslator/CheckServiceStatus
struct  CheckServiceStatus_t297802562  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.LanguageTranslator IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.LanguageTranslator/CheckServiceStatus::m_Service
	LanguageTranslator_t2981742234 * ___m_Service_0;
	// IBM.Watson.DeveloperCloud.Services.ServiceStatus IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.LanguageTranslator/CheckServiceStatus::m_Callback
	ServiceStatus_t1443707987 * ___m_Callback_1;

public:
	inline static int32_t get_offset_of_m_Service_0() { return static_cast<int32_t>(offsetof(CheckServiceStatus_t297802562, ___m_Service_0)); }
	inline LanguageTranslator_t2981742234 * get_m_Service_0() const { return ___m_Service_0; }
	inline LanguageTranslator_t2981742234 ** get_address_of_m_Service_0() { return &___m_Service_0; }
	inline void set_m_Service_0(LanguageTranslator_t2981742234 * value)
	{
		___m_Service_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Service_0), value);
	}

	inline static int32_t get_offset_of_m_Callback_1() { return static_cast<int32_t>(offsetof(CheckServiceStatus_t297802562, ___m_Callback_1)); }
	inline ServiceStatus_t1443707987 * get_m_Callback_1() const { return ___m_Callback_1; }
	inline ServiceStatus_t1443707987 ** get_address_of_m_Callback_1() { return &___m_Callback_1; }
	inline void set_m_Callback_1(ServiceStatus_t1443707987 * value)
	{
		___m_Callback_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Callback_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHECKSERVICESTATUS_T297802562_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef CLASSIFYRESULT_T2166103447_H
#define CLASSIFYRESULT_T2166103447_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.ClassifyResult
struct  ClassifyResult_t2166103447  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.ClassifyResult::<classifier_id>k__BackingField
	String_t* ___U3Cclassifier_idU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.ClassifyResult::<url>k__BackingField
	String_t* ___U3CurlU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.ClassifyResult::<text>k__BackingField
	String_t* ___U3CtextU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.ClassifyResult::<top_class>k__BackingField
	String_t* ___U3Ctop_classU3Ek__BackingField_3;
	// IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.Class[] IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.ClassifyResult::<classes>k__BackingField
	ClassU5BU5D_t3189134507* ___U3CclassesU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3Cclassifier_idU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ClassifyResult_t2166103447, ___U3Cclassifier_idU3Ek__BackingField_0)); }
	inline String_t* get_U3Cclassifier_idU3Ek__BackingField_0() const { return ___U3Cclassifier_idU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3Cclassifier_idU3Ek__BackingField_0() { return &___U3Cclassifier_idU3Ek__BackingField_0; }
	inline void set_U3Cclassifier_idU3Ek__BackingField_0(String_t* value)
	{
		___U3Cclassifier_idU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cclassifier_idU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CurlU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ClassifyResult_t2166103447, ___U3CurlU3Ek__BackingField_1)); }
	inline String_t* get_U3CurlU3Ek__BackingField_1() const { return ___U3CurlU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CurlU3Ek__BackingField_1() { return &___U3CurlU3Ek__BackingField_1; }
	inline void set_U3CurlU3Ek__BackingField_1(String_t* value)
	{
		___U3CurlU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CurlU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CtextU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ClassifyResult_t2166103447, ___U3CtextU3Ek__BackingField_2)); }
	inline String_t* get_U3CtextU3Ek__BackingField_2() const { return ___U3CtextU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CtextU3Ek__BackingField_2() { return &___U3CtextU3Ek__BackingField_2; }
	inline void set_U3CtextU3Ek__BackingField_2(String_t* value)
	{
		___U3CtextU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3Ctop_classU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ClassifyResult_t2166103447, ___U3Ctop_classU3Ek__BackingField_3)); }
	inline String_t* get_U3Ctop_classU3Ek__BackingField_3() const { return ___U3Ctop_classU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3Ctop_classU3Ek__BackingField_3() { return &___U3Ctop_classU3Ek__BackingField_3; }
	inline void set_U3Ctop_classU3Ek__BackingField_3(String_t* value)
	{
		___U3Ctop_classU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3Ctop_classU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CclassesU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(ClassifyResult_t2166103447, ___U3CclassesU3Ek__BackingField_4)); }
	inline ClassU5BU5D_t3189134507* get_U3CclassesU3Ek__BackingField_4() const { return ___U3CclassesU3Ek__BackingField_4; }
	inline ClassU5BU5D_t3189134507** get_address_of_U3CclassesU3Ek__BackingField_4() { return &___U3CclassesU3Ek__BackingField_4; }
	inline void set_U3CclassesU3Ek__BackingField_4(ClassU5BU5D_t3189134507* value)
	{
		___U3CclassesU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CclassesU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLASSIFYRESULT_T2166103447_H
#ifndef CLASS_T2988469534_H
#define CLASS_T2988469534_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.Class
struct  Class_t2988469534  : public RuntimeObject
{
public:
	// System.Double IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.Class::<confidence>k__BackingField
	double ___U3CconfidenceU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.Class::<class_name>k__BackingField
	String_t* ___U3Cclass_nameU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CconfidenceU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Class_t2988469534, ___U3CconfidenceU3Ek__BackingField_0)); }
	inline double get_U3CconfidenceU3Ek__BackingField_0() const { return ___U3CconfidenceU3Ek__BackingField_0; }
	inline double* get_address_of_U3CconfidenceU3Ek__BackingField_0() { return &___U3CconfidenceU3Ek__BackingField_0; }
	inline void set_U3CconfidenceU3Ek__BackingField_0(double value)
	{
		___U3CconfidenceU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3Cclass_nameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Class_t2988469534, ___U3Cclass_nameU3Ek__BackingField_1)); }
	inline String_t* get_U3Cclass_nameU3Ek__BackingField_1() const { return ___U3Cclass_nameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3Cclass_nameU3Ek__BackingField_1() { return &___U3Cclass_nameU3Ek__BackingField_1; }
	inline void set_U3Cclass_nameU3Ek__BackingField_1(String_t* value)
	{
		___U3Cclass_nameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cclass_nameU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLASS_T2988469534_H
#ifndef CLASSIFIERS_T1537491400_H
#define CLASSIFIERS_T1537491400_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.Classifiers
struct  Classifiers_t1537491400  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.Classifier[] IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.Classifiers::<classifiers>k__BackingField
	ClassifierU5BU5D_t2606195416* ___U3CclassifiersU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CclassifiersU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Classifiers_t1537491400, ___U3CclassifiersU3Ek__BackingField_0)); }
	inline ClassifierU5BU5D_t2606195416* get_U3CclassifiersU3Ek__BackingField_0() const { return ___U3CclassifiersU3Ek__BackingField_0; }
	inline ClassifierU5BU5D_t2606195416** get_address_of_U3CclassifiersU3Ek__BackingField_0() { return &___U3CclassifiersU3Ek__BackingField_0; }
	inline void set_U3CclassifiersU3Ek__BackingField_0(ClassifierU5BU5D_t2606195416* value)
	{
		___U3CclassifiersU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CclassifiersU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLASSIFIERS_T1537491400_H
#ifndef CONTENTITEM_T1958644056_H
#define CONTENTITEM_T1958644056_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v2.ContentItem
struct  ContentItem_t1958644056  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v2.ContentItem::<id>k__BackingField
	String_t* ___U3CidU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v2.ContentItem::<userid>k__BackingField
	String_t* ___U3CuseridU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v2.ContentItem::<sourceid>k__BackingField
	String_t* ___U3CsourceidU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v2.ContentItem::<created>k__BackingField
	String_t* ___U3CcreatedU3Ek__BackingField_3;
	// System.String IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v2.ContentItem::<updated>k__BackingField
	String_t* ___U3CupdatedU3Ek__BackingField_4;
	// System.String IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v2.ContentItem::<contenttype>k__BackingField
	String_t* ___U3CcontenttypeU3Ek__BackingField_5;
	// System.String IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v2.ContentItem::<language>k__BackingField
	String_t* ___U3ClanguageU3Ek__BackingField_6;
	// System.String IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v2.ContentItem::<content>k__BackingField
	String_t* ___U3CcontentU3Ek__BackingField_7;
	// System.String IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v2.ContentItem::<parentid>k__BackingField
	String_t* ___U3CparentidU3Ek__BackingField_8;
	// System.Boolean IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v2.ContentItem::<reply>k__BackingField
	bool ___U3CreplyU3Ek__BackingField_9;
	// System.Boolean IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v2.ContentItem::<forward>k__BackingField
	bool ___U3CforwardU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of_U3CidU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ContentItem_t1958644056, ___U3CidU3Ek__BackingField_0)); }
	inline String_t* get_U3CidU3Ek__BackingField_0() const { return ___U3CidU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CidU3Ek__BackingField_0() { return &___U3CidU3Ek__BackingField_0; }
	inline void set_U3CidU3Ek__BackingField_0(String_t* value)
	{
		___U3CidU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CidU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CuseridU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ContentItem_t1958644056, ___U3CuseridU3Ek__BackingField_1)); }
	inline String_t* get_U3CuseridU3Ek__BackingField_1() const { return ___U3CuseridU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CuseridU3Ek__BackingField_1() { return &___U3CuseridU3Ek__BackingField_1; }
	inline void set_U3CuseridU3Ek__BackingField_1(String_t* value)
	{
		___U3CuseridU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CuseridU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CsourceidU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ContentItem_t1958644056, ___U3CsourceidU3Ek__BackingField_2)); }
	inline String_t* get_U3CsourceidU3Ek__BackingField_2() const { return ___U3CsourceidU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CsourceidU3Ek__BackingField_2() { return &___U3CsourceidU3Ek__BackingField_2; }
	inline void set_U3CsourceidU3Ek__BackingField_2(String_t* value)
	{
		___U3CsourceidU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsourceidU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CcreatedU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ContentItem_t1958644056, ___U3CcreatedU3Ek__BackingField_3)); }
	inline String_t* get_U3CcreatedU3Ek__BackingField_3() const { return ___U3CcreatedU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CcreatedU3Ek__BackingField_3() { return &___U3CcreatedU3Ek__BackingField_3; }
	inline void set_U3CcreatedU3Ek__BackingField_3(String_t* value)
	{
		___U3CcreatedU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcreatedU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CupdatedU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(ContentItem_t1958644056, ___U3CupdatedU3Ek__BackingField_4)); }
	inline String_t* get_U3CupdatedU3Ek__BackingField_4() const { return ___U3CupdatedU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CupdatedU3Ek__BackingField_4() { return &___U3CupdatedU3Ek__BackingField_4; }
	inline void set_U3CupdatedU3Ek__BackingField_4(String_t* value)
	{
		___U3CupdatedU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CupdatedU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CcontenttypeU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(ContentItem_t1958644056, ___U3CcontenttypeU3Ek__BackingField_5)); }
	inline String_t* get_U3CcontenttypeU3Ek__BackingField_5() const { return ___U3CcontenttypeU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CcontenttypeU3Ek__BackingField_5() { return &___U3CcontenttypeU3Ek__BackingField_5; }
	inline void set_U3CcontenttypeU3Ek__BackingField_5(String_t* value)
	{
		___U3CcontenttypeU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcontenttypeU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3ClanguageU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(ContentItem_t1958644056, ___U3ClanguageU3Ek__BackingField_6)); }
	inline String_t* get_U3ClanguageU3Ek__BackingField_6() const { return ___U3ClanguageU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3ClanguageU3Ek__BackingField_6() { return &___U3ClanguageU3Ek__BackingField_6; }
	inline void set_U3ClanguageU3Ek__BackingField_6(String_t* value)
	{
		___U3ClanguageU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClanguageU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CcontentU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(ContentItem_t1958644056, ___U3CcontentU3Ek__BackingField_7)); }
	inline String_t* get_U3CcontentU3Ek__BackingField_7() const { return ___U3CcontentU3Ek__BackingField_7; }
	inline String_t** get_address_of_U3CcontentU3Ek__BackingField_7() { return &___U3CcontentU3Ek__BackingField_7; }
	inline void set_U3CcontentU3Ek__BackingField_7(String_t* value)
	{
		___U3CcontentU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcontentU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CparentidU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(ContentItem_t1958644056, ___U3CparentidU3Ek__BackingField_8)); }
	inline String_t* get_U3CparentidU3Ek__BackingField_8() const { return ___U3CparentidU3Ek__BackingField_8; }
	inline String_t** get_address_of_U3CparentidU3Ek__BackingField_8() { return &___U3CparentidU3Ek__BackingField_8; }
	inline void set_U3CparentidU3Ek__BackingField_8(String_t* value)
	{
		___U3CparentidU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CparentidU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CreplyU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(ContentItem_t1958644056, ___U3CreplyU3Ek__BackingField_9)); }
	inline bool get_U3CreplyU3Ek__BackingField_9() const { return ___U3CreplyU3Ek__BackingField_9; }
	inline bool* get_address_of_U3CreplyU3Ek__BackingField_9() { return &___U3CreplyU3Ek__BackingField_9; }
	inline void set_U3CreplyU3Ek__BackingField_9(bool value)
	{
		___U3CreplyU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CforwardU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(ContentItem_t1958644056, ___U3CforwardU3Ek__BackingField_10)); }
	inline bool get_U3CforwardU3Ek__BackingField_10() const { return ___U3CforwardU3Ek__BackingField_10; }
	inline bool* get_address_of_U3CforwardU3Ek__BackingField_10() { return &___U3CforwardU3Ek__BackingField_10; }
	inline void set_U3CforwardU3Ek__BackingField_10(bool value)
	{
		___U3CforwardU3Ek__BackingField_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTENTITEM_T1958644056_H
#ifndef CHECKSERVICESTATUS_T1948517221_H
#define CHECKSERVICESTATUS_T1948517221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v2.PersonalityInsights/CheckServiceStatus
struct  CheckServiceStatus_t1948517221  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v2.PersonalityInsights IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v2.PersonalityInsights/CheckServiceStatus::m_Service
	PersonalityInsights_t582046669 * ___m_Service_0;
	// IBM.Watson.DeveloperCloud.Services.ServiceStatus IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v2.PersonalityInsights/CheckServiceStatus::m_Callback
	ServiceStatus_t1443707987 * ___m_Callback_1;

public:
	inline static int32_t get_offset_of_m_Service_0() { return static_cast<int32_t>(offsetof(CheckServiceStatus_t1948517221, ___m_Service_0)); }
	inline PersonalityInsights_t582046669 * get_m_Service_0() const { return ___m_Service_0; }
	inline PersonalityInsights_t582046669 ** get_address_of_m_Service_0() { return &___m_Service_0; }
	inline void set_m_Service_0(PersonalityInsights_t582046669 * value)
	{
		___m_Service_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Service_0), value);
	}

	inline static int32_t get_offset_of_m_Callback_1() { return static_cast<int32_t>(offsetof(CheckServiceStatus_t1948517221, ___m_Callback_1)); }
	inline ServiceStatus_t1443707987 * get_m_Callback_1() const { return ___m_Callback_1; }
	inline ServiceStatus_t1443707987 ** get_address_of_m_Callback_1() { return &___m_Callback_1; }
	inline void set_m_Callback_1(ServiceStatus_t1443707987 * value)
	{
		___m_Callback_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Callback_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHECKSERVICESTATUS_T1948517221_H
#ifndef PERSONALITYINSIGHTS_T582046669_H
#define PERSONALITYINSIGHTS_T582046669_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v2.PersonalityInsights
struct  PersonalityInsights_t582046669  : public RuntimeObject
{
public:

public:
};

struct PersonalityInsights_t582046669_StaticFields
{
public:
	// FullSerializer.fsSerializer IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v2.PersonalityInsights::sm_Serializer
	fsSerializer_t4193731081 * ___sm_Serializer_1;

public:
	inline static int32_t get_offset_of_sm_Serializer_1() { return static_cast<int32_t>(offsetof(PersonalityInsights_t582046669_StaticFields, ___sm_Serializer_1)); }
	inline fsSerializer_t4193731081 * get_sm_Serializer_1() const { return ___sm_Serializer_1; }
	inline fsSerializer_t4193731081 ** get_address_of_sm_Serializer_1() { return &___sm_Serializer_1; }
	inline void set_sm_Serializer_1(fsSerializer_t4193731081 * value)
	{
		___sm_Serializer_1 = value;
		Il2CppCodeGenWriteBarrier((&___sm_Serializer_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PERSONALITYINSIGHTS_T582046669_H
#ifndef CONTENTTYPE_T3618577653_H
#define CONTENTTYPE_T3618577653_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v2.ContentType
struct  ContentType_t3618577653  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTENTTYPE_T3618577653_H
#ifndef LANGUAGE_T3866454500_H
#define LANGUAGE_T3866454500_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v2.Language
struct  Language_t3866454500  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LANGUAGE_T3866454500_H
#ifndef PROFILE_T3811251445_H
#define PROFILE_T3811251445_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v3.Profile
struct  Profile_t3811251445  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v3.Profile::<processed_language>k__BackingField
	String_t* ___U3Cprocessed_languageU3Ek__BackingField_0;
	// System.Int32 IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v3.Profile::<word_count>k__BackingField
	int32_t ___U3Cword_countU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v3.Profile::<word_count_message>k__BackingField
	String_t* ___U3Cword_count_messageU3Ek__BackingField_2;
	// IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v3.TraitTreeNode[] IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v3.Profile::<personality>k__BackingField
	TraitTreeNodeU5BU5D_t476229367* ___U3CpersonalityU3Ek__BackingField_3;
	// IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v3.TraitTreeNode[] IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v3.Profile::<values>k__BackingField
	TraitTreeNodeU5BU5D_t476229367* ___U3CvaluesU3Ek__BackingField_4;
	// IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v3.TraitTreeNode[] IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v3.Profile::<needs>k__BackingField
	TraitTreeNodeU5BU5D_t476229367* ___U3CneedsU3Ek__BackingField_5;
	// IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v3.BehaviorNode[] IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v3.Profile::<behavior>k__BackingField
	BehaviorNodeU5BU5D_t2246947787* ___U3CbehaviorU3Ek__BackingField_6;
	// IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v3.ConsumptionPreferencesCategoryNode[] IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v3.Profile::<consumption_preferences>k__BackingField
	ConsumptionPreferencesCategoryNodeU5BU5D_t273403704* ___U3Cconsumption_preferencesU3Ek__BackingField_7;
	// IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v3.Warning[] IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v3.Profile::<warning>k__BackingField
	WarningU5BU5D_t2063588977* ___U3CwarningU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of_U3Cprocessed_languageU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Profile_t3811251445, ___U3Cprocessed_languageU3Ek__BackingField_0)); }
	inline String_t* get_U3Cprocessed_languageU3Ek__BackingField_0() const { return ___U3Cprocessed_languageU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3Cprocessed_languageU3Ek__BackingField_0() { return &___U3Cprocessed_languageU3Ek__BackingField_0; }
	inline void set_U3Cprocessed_languageU3Ek__BackingField_0(String_t* value)
	{
		___U3Cprocessed_languageU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cprocessed_languageU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3Cword_countU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Profile_t3811251445, ___U3Cword_countU3Ek__BackingField_1)); }
	inline int32_t get_U3Cword_countU3Ek__BackingField_1() const { return ___U3Cword_countU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3Cword_countU3Ek__BackingField_1() { return &___U3Cword_countU3Ek__BackingField_1; }
	inline void set_U3Cword_countU3Ek__BackingField_1(int32_t value)
	{
		___U3Cword_countU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3Cword_count_messageU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Profile_t3811251445, ___U3Cword_count_messageU3Ek__BackingField_2)); }
	inline String_t* get_U3Cword_count_messageU3Ek__BackingField_2() const { return ___U3Cword_count_messageU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3Cword_count_messageU3Ek__BackingField_2() { return &___U3Cword_count_messageU3Ek__BackingField_2; }
	inline void set_U3Cword_count_messageU3Ek__BackingField_2(String_t* value)
	{
		___U3Cword_count_messageU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cword_count_messageU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CpersonalityU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Profile_t3811251445, ___U3CpersonalityU3Ek__BackingField_3)); }
	inline TraitTreeNodeU5BU5D_t476229367* get_U3CpersonalityU3Ek__BackingField_3() const { return ___U3CpersonalityU3Ek__BackingField_3; }
	inline TraitTreeNodeU5BU5D_t476229367** get_address_of_U3CpersonalityU3Ek__BackingField_3() { return &___U3CpersonalityU3Ek__BackingField_3; }
	inline void set_U3CpersonalityU3Ek__BackingField_3(TraitTreeNodeU5BU5D_t476229367* value)
	{
		___U3CpersonalityU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpersonalityU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CvaluesU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Profile_t3811251445, ___U3CvaluesU3Ek__BackingField_4)); }
	inline TraitTreeNodeU5BU5D_t476229367* get_U3CvaluesU3Ek__BackingField_4() const { return ___U3CvaluesU3Ek__BackingField_4; }
	inline TraitTreeNodeU5BU5D_t476229367** get_address_of_U3CvaluesU3Ek__BackingField_4() { return &___U3CvaluesU3Ek__BackingField_4; }
	inline void set_U3CvaluesU3Ek__BackingField_4(TraitTreeNodeU5BU5D_t476229367* value)
	{
		___U3CvaluesU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CvaluesU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CneedsU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Profile_t3811251445, ___U3CneedsU3Ek__BackingField_5)); }
	inline TraitTreeNodeU5BU5D_t476229367* get_U3CneedsU3Ek__BackingField_5() const { return ___U3CneedsU3Ek__BackingField_5; }
	inline TraitTreeNodeU5BU5D_t476229367** get_address_of_U3CneedsU3Ek__BackingField_5() { return &___U3CneedsU3Ek__BackingField_5; }
	inline void set_U3CneedsU3Ek__BackingField_5(TraitTreeNodeU5BU5D_t476229367* value)
	{
		___U3CneedsU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CneedsU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CbehaviorU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Profile_t3811251445, ___U3CbehaviorU3Ek__BackingField_6)); }
	inline BehaviorNodeU5BU5D_t2246947787* get_U3CbehaviorU3Ek__BackingField_6() const { return ___U3CbehaviorU3Ek__BackingField_6; }
	inline BehaviorNodeU5BU5D_t2246947787** get_address_of_U3CbehaviorU3Ek__BackingField_6() { return &___U3CbehaviorU3Ek__BackingField_6; }
	inline void set_U3CbehaviorU3Ek__BackingField_6(BehaviorNodeU5BU5D_t2246947787* value)
	{
		___U3CbehaviorU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CbehaviorU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3Cconsumption_preferencesU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(Profile_t3811251445, ___U3Cconsumption_preferencesU3Ek__BackingField_7)); }
	inline ConsumptionPreferencesCategoryNodeU5BU5D_t273403704* get_U3Cconsumption_preferencesU3Ek__BackingField_7() const { return ___U3Cconsumption_preferencesU3Ek__BackingField_7; }
	inline ConsumptionPreferencesCategoryNodeU5BU5D_t273403704** get_address_of_U3Cconsumption_preferencesU3Ek__BackingField_7() { return &___U3Cconsumption_preferencesU3Ek__BackingField_7; }
	inline void set_U3Cconsumption_preferencesU3Ek__BackingField_7(ConsumptionPreferencesCategoryNodeU5BU5D_t273403704* value)
	{
		___U3Cconsumption_preferencesU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cconsumption_preferencesU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CwarningU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(Profile_t3811251445, ___U3CwarningU3Ek__BackingField_8)); }
	inline WarningU5BU5D_t2063588977* get_U3CwarningU3Ek__BackingField_8() const { return ___U3CwarningU3Ek__BackingField_8; }
	inline WarningU5BU5D_t2063588977** get_address_of_U3CwarningU3Ek__BackingField_8() { return &___U3CwarningU3Ek__BackingField_8; }
	inline void set_U3CwarningU3Ek__BackingField_8(WarningU5BU5D_t2063588977* value)
	{
		___U3CwarningU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwarningU3Ek__BackingField_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROFILE_T3811251445_H
#ifndef WARNING_T514099562_H
#define WARNING_T514099562_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v2.Warning
struct  Warning_t514099562  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v2.Warning::<id>k__BackingField
	String_t* ___U3CidU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v2.Warning::<message>k__BackingField
	String_t* ___U3CmessageU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CidU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Warning_t514099562, ___U3CidU3Ek__BackingField_0)); }
	inline String_t* get_U3CidU3Ek__BackingField_0() const { return ___U3CidU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CidU3Ek__BackingField_0() { return &___U3CidU3Ek__BackingField_0; }
	inline void set_U3CidU3Ek__BackingField_0(String_t* value)
	{
		___U3CidU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CidU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CmessageU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Warning_t514099562, ___U3CmessageU3Ek__BackingField_1)); }
	inline String_t* get_U3CmessageU3Ek__BackingField_1() const { return ___U3CmessageU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CmessageU3Ek__BackingField_1() { return &___U3CmessageU3Ek__BackingField_1; }
	inline void set_U3CmessageU3Ek__BackingField_1(String_t* value)
	{
		___U3CmessageU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmessageU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WARNING_T514099562_H
#ifndef CONTENTLISTCONTAINER_T811063702_H
#define CONTENTLISTCONTAINER_T811063702_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v2.ContentListContainer
struct  ContentListContainer_t811063702  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v2.ContentItem[] IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v2.ContentListContainer::<contentItems>k__BackingField
	ContentItemU5BU5D_t836316745* ___U3CcontentItemsU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CcontentItemsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ContentListContainer_t811063702, ___U3CcontentItemsU3Ek__BackingField_0)); }
	inline ContentItemU5BU5D_t836316745* get_U3CcontentItemsU3Ek__BackingField_0() const { return ___U3CcontentItemsU3Ek__BackingField_0; }
	inline ContentItemU5BU5D_t836316745** get_address_of_U3CcontentItemsU3Ek__BackingField_0() { return &___U3CcontentItemsU3Ek__BackingField_0; }
	inline void set_U3CcontentItemsU3Ek__BackingField_0(ContentItemU5BU5D_t836316745* value)
	{
		___U3CcontentItemsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcontentItemsU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTENTLISTCONTAINER_T811063702_H
#ifndef TRAITTREENODE_T3511443394_H
#define TRAITTREENODE_T3511443394_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v3.TraitTreeNode
struct  TraitTreeNode_t3511443394  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v3.TraitTreeNode::<trait_id>k__BackingField
	String_t* ___U3Ctrait_idU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v3.TraitTreeNode::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v3.TraitTreeNode::<category>k__BackingField
	String_t* ___U3CcategoryU3Ek__BackingField_2;
	// System.Double IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v3.TraitTreeNode::<percentile>k__BackingField
	double ___U3CpercentileU3Ek__BackingField_3;
	// System.Double IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v3.TraitTreeNode::<raw_score>k__BackingField
	double ___U3Craw_scoreU3Ek__BackingField_4;
	// IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v3.TraitTreeNode[] IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v3.TraitTreeNode::<children>k__BackingField
	TraitTreeNodeU5BU5D_t476229367* ___U3CchildrenU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3Ctrait_idU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TraitTreeNode_t3511443394, ___U3Ctrait_idU3Ek__BackingField_0)); }
	inline String_t* get_U3Ctrait_idU3Ek__BackingField_0() const { return ___U3Ctrait_idU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3Ctrait_idU3Ek__BackingField_0() { return &___U3Ctrait_idU3Ek__BackingField_0; }
	inline void set_U3Ctrait_idU3Ek__BackingField_0(String_t* value)
	{
		___U3Ctrait_idU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Ctrait_idU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TraitTreeNode_t3511443394, ___U3CnameU3Ek__BackingField_1)); }
	inline String_t* get_U3CnameU3Ek__BackingField_1() const { return ___U3CnameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_1() { return &___U3CnameU3Ek__BackingField_1; }
	inline void set_U3CnameU3Ek__BackingField_1(String_t* value)
	{
		___U3CnameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CcategoryU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(TraitTreeNode_t3511443394, ___U3CcategoryU3Ek__BackingField_2)); }
	inline String_t* get_U3CcategoryU3Ek__BackingField_2() const { return ___U3CcategoryU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CcategoryU3Ek__BackingField_2() { return &___U3CcategoryU3Ek__BackingField_2; }
	inline void set_U3CcategoryU3Ek__BackingField_2(String_t* value)
	{
		___U3CcategoryU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcategoryU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CpercentileU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(TraitTreeNode_t3511443394, ___U3CpercentileU3Ek__BackingField_3)); }
	inline double get_U3CpercentileU3Ek__BackingField_3() const { return ___U3CpercentileU3Ek__BackingField_3; }
	inline double* get_address_of_U3CpercentileU3Ek__BackingField_3() { return &___U3CpercentileU3Ek__BackingField_3; }
	inline void set_U3CpercentileU3Ek__BackingField_3(double value)
	{
		___U3CpercentileU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3Craw_scoreU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(TraitTreeNode_t3511443394, ___U3Craw_scoreU3Ek__BackingField_4)); }
	inline double get_U3Craw_scoreU3Ek__BackingField_4() const { return ___U3Craw_scoreU3Ek__BackingField_4; }
	inline double* get_address_of_U3Craw_scoreU3Ek__BackingField_4() { return &___U3Craw_scoreU3Ek__BackingField_4; }
	inline void set_U3Craw_scoreU3Ek__BackingField_4(double value)
	{
		___U3Craw_scoreU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CchildrenU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(TraitTreeNode_t3511443394, ___U3CchildrenU3Ek__BackingField_5)); }
	inline TraitTreeNodeU5BU5D_t476229367* get_U3CchildrenU3Ek__BackingField_5() const { return ___U3CchildrenU3Ek__BackingField_5; }
	inline TraitTreeNodeU5BU5D_t476229367** get_address_of_U3CchildrenU3Ek__BackingField_5() { return &___U3CchildrenU3Ek__BackingField_5; }
	inline void set_U3CchildrenU3Ek__BackingField_5(TraitTreeNodeU5BU5D_t476229367* value)
	{
		___U3CchildrenU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CchildrenU3Ek__BackingField_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRAITTREENODE_T3511443394_H
#ifndef BOOLEAN_T3825574718_H
#define BOOLEAN_T3825574718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t3825574718 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t3825574718, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t3825574718_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t3825574718_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t3825574718_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T3825574718_H
#ifndef GETCLUSTERSREQUEST_T1824000023_H
#define GETCLUSTERSREQUEST_T1824000023_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/GetClustersRequest
struct  GetClustersRequest_t1824000023  : public Request_t466816980
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/GetClustersRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_11;
	// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/OnGetClusters IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/GetClustersRequest::<Callback>k__BackingField
	OnGetClusters_t1915248271 * ___U3CCallbackU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetClustersRequest_t1824000023, ___U3CDataU3Ek__BackingField_11)); }
	inline String_t* get_U3CDataU3Ek__BackingField_11() const { return ___U3CDataU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_11() { return &___U3CDataU3Ek__BackingField_11; }
	inline void set_U3CDataU3Ek__BackingField_11(String_t* value)
	{
		___U3CDataU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GetClustersRequest_t1824000023, ___U3CCallbackU3Ek__BackingField_12)); }
	inline OnGetClusters_t1915248271 * get_U3CCallbackU3Ek__BackingField_12() const { return ___U3CCallbackU3Ek__BackingField_12; }
	inline OnGetClusters_t1915248271 ** get_address_of_U3CCallbackU3Ek__BackingField_12() { return &___U3CCallbackU3Ek__BackingField_12; }
	inline void set_U3CCallbackU3Ek__BackingField_12(OnGetClusters_t1915248271 * value)
	{
		___U3CCallbackU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETCLUSTERSREQUEST_T1824000023_H
#ifndef GETCLUSTERREQUEST_T52709226_H
#define GETCLUSTERREQUEST_T52709226_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/GetClusterRequest
struct  GetClusterRequest_t52709226  : public Request_t466816980
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/GetClusterRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/GetClusterRequest::<ClusterID>k__BackingField
	String_t* ___U3CClusterIDU3Ek__BackingField_12;
	// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/OnGetCluster IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/GetClusterRequest::<Callback>k__BackingField
	OnGetCluster_t3066553756 * ___U3CCallbackU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetClusterRequest_t52709226, ___U3CDataU3Ek__BackingField_11)); }
	inline String_t* get_U3CDataU3Ek__BackingField_11() const { return ___U3CDataU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_11() { return &___U3CDataU3Ek__BackingField_11; }
	inline void set_U3CDataU3Ek__BackingField_11(String_t* value)
	{
		___U3CDataU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CClusterIDU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GetClusterRequest_t52709226, ___U3CClusterIDU3Ek__BackingField_12)); }
	inline String_t* get_U3CClusterIDU3Ek__BackingField_12() const { return ___U3CClusterIDU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CClusterIDU3Ek__BackingField_12() { return &___U3CClusterIDU3Ek__BackingField_12; }
	inline void set_U3CClusterIDU3Ek__BackingField_12(String_t* value)
	{
		___U3CClusterIDU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CClusterIDU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(GetClusterRequest_t52709226, ___U3CCallbackU3Ek__BackingField_13)); }
	inline OnGetCluster_t3066553756 * get_U3CCallbackU3Ek__BackingField_13() const { return ___U3CCallbackU3Ek__BackingField_13; }
	inline OnGetCluster_t3066553756 ** get_address_of_U3CCallbackU3Ek__BackingField_13() { return &___U3CCallbackU3Ek__BackingField_13; }
	inline void set_U3CCallbackU3Ek__BackingField_13(OnGetCluster_t3066553756 * value)
	{
		___U3CCallbackU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETCLUSTERREQUEST_T52709226_H
#ifndef GETCLUSTERCONFIGSREQUEST_T1411051725_H
#define GETCLUSTERCONFIGSREQUEST_T1411051725_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/GetClusterConfigsRequest
struct  GetClusterConfigsRequest_t1411051725  : public Request_t466816980
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/GetClusterConfigsRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/GetClusterConfigsRequest::<ClusterID>k__BackingField
	String_t* ___U3CClusterIDU3Ek__BackingField_12;
	// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/OnGetClusterConfigs IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/GetClusterConfigsRequest::<Callback>k__BackingField
	OnGetClusterConfigs_t22213349 * ___U3CCallbackU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetClusterConfigsRequest_t1411051725, ___U3CDataU3Ek__BackingField_11)); }
	inline String_t* get_U3CDataU3Ek__BackingField_11() const { return ___U3CDataU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_11() { return &___U3CDataU3Ek__BackingField_11; }
	inline void set_U3CDataU3Ek__BackingField_11(String_t* value)
	{
		___U3CDataU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CClusterIDU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GetClusterConfigsRequest_t1411051725, ___U3CClusterIDU3Ek__BackingField_12)); }
	inline String_t* get_U3CClusterIDU3Ek__BackingField_12() const { return ___U3CClusterIDU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CClusterIDU3Ek__BackingField_12() { return &___U3CClusterIDU3Ek__BackingField_12; }
	inline void set_U3CClusterIDU3Ek__BackingField_12(String_t* value)
	{
		___U3CClusterIDU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CClusterIDU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(GetClusterConfigsRequest_t1411051725, ___U3CCallbackU3Ek__BackingField_13)); }
	inline OnGetClusterConfigs_t22213349 * get_U3CCallbackU3Ek__BackingField_13() const { return ___U3CCallbackU3Ek__BackingField_13; }
	inline OnGetClusterConfigs_t22213349 ** get_address_of_U3CCallbackU3Ek__BackingField_13() { return &___U3CCallbackU3Ek__BackingField_13; }
	inline void set_U3CCallbackU3Ek__BackingField_13(OnGetClusterConfigs_t22213349 * value)
	{
		___U3CCallbackU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETCLUSTERCONFIGSREQUEST_T1411051725_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	IntPtr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline IntPtr_t get_Zero_1() const { return ___Zero_1; }
	inline IntPtr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(IntPtr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef DELETECLUSTERREQUEST_T929452623_H
#define DELETECLUSTERREQUEST_T929452623_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/DeleteClusterRequest
struct  DeleteClusterRequest_t929452623  : public Request_t466816980
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/DeleteClusterRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/DeleteClusterRequest::<ClusterID>k__BackingField
	String_t* ___U3CClusterIDU3Ek__BackingField_12;
	// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/OnDeleteCluster IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/DeleteClusterRequest::<Callback>k__BackingField
	OnDeleteCluster_t1281715199 * ___U3CCallbackU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(DeleteClusterRequest_t929452623, ___U3CDataU3Ek__BackingField_11)); }
	inline String_t* get_U3CDataU3Ek__BackingField_11() const { return ___U3CDataU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_11() { return &___U3CDataU3Ek__BackingField_11; }
	inline void set_U3CDataU3Ek__BackingField_11(String_t* value)
	{
		___U3CDataU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CClusterIDU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(DeleteClusterRequest_t929452623, ___U3CClusterIDU3Ek__BackingField_12)); }
	inline String_t* get_U3CClusterIDU3Ek__BackingField_12() const { return ___U3CClusterIDU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CClusterIDU3Ek__BackingField_12() { return &___U3CClusterIDU3Ek__BackingField_12; }
	inline void set_U3CClusterIDU3Ek__BackingField_12(String_t* value)
	{
		___U3CClusterIDU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CClusterIDU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(DeleteClusterRequest_t929452623, ___U3CCallbackU3Ek__BackingField_13)); }
	inline OnDeleteCluster_t1281715199 * get_U3CCallbackU3Ek__BackingField_13() const { return ___U3CCallbackU3Ek__BackingField_13; }
	inline OnDeleteCluster_t1281715199 ** get_address_of_U3CCallbackU3Ek__BackingField_13() { return &___U3CCallbackU3Ek__BackingField_13; }
	inline void set_U3CCallbackU3Ek__BackingField_13(OnDeleteCluster_t1281715199 * value)
	{
		___U3CCallbackU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELETECLUSTERREQUEST_T929452623_H
#ifndef UPLOADCLUSTERCONFIGREQUEST_T3639785499_H
#define UPLOADCLUSTERCONFIGREQUEST_T3639785499_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/UploadClusterConfigRequest
struct  UploadClusterConfigRequest_t3639785499  : public Request_t466816980
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/UploadClusterConfigRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/UploadClusterConfigRequest::<ClusterID>k__BackingField
	String_t* ___U3CClusterIDU3Ek__BackingField_12;
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/UploadClusterConfigRequest::<ConfigName>k__BackingField
	String_t* ___U3CConfigNameU3Ek__BackingField_13;
	// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/OnUploadClusterConfig IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/UploadClusterConfigRequest::<Callback>k__BackingField
	OnUploadClusterConfig_t589517099 * ___U3CCallbackU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(UploadClusterConfigRequest_t3639785499, ___U3CDataU3Ek__BackingField_11)); }
	inline String_t* get_U3CDataU3Ek__BackingField_11() const { return ___U3CDataU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_11() { return &___U3CDataU3Ek__BackingField_11; }
	inline void set_U3CDataU3Ek__BackingField_11(String_t* value)
	{
		___U3CDataU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CClusterIDU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(UploadClusterConfigRequest_t3639785499, ___U3CClusterIDU3Ek__BackingField_12)); }
	inline String_t* get_U3CClusterIDU3Ek__BackingField_12() const { return ___U3CClusterIDU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CClusterIDU3Ek__BackingField_12() { return &___U3CClusterIDU3Ek__BackingField_12; }
	inline void set_U3CClusterIDU3Ek__BackingField_12(String_t* value)
	{
		___U3CClusterIDU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CClusterIDU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CConfigNameU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(UploadClusterConfigRequest_t3639785499, ___U3CConfigNameU3Ek__BackingField_13)); }
	inline String_t* get_U3CConfigNameU3Ek__BackingField_13() const { return ___U3CConfigNameU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CConfigNameU3Ek__BackingField_13() { return &___U3CConfigNameU3Ek__BackingField_13; }
	inline void set_U3CConfigNameU3Ek__BackingField_13(String_t* value)
	{
		___U3CConfigNameU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CConfigNameU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(UploadClusterConfigRequest_t3639785499, ___U3CCallbackU3Ek__BackingField_14)); }
	inline OnUploadClusterConfig_t589517099 * get_U3CCallbackU3Ek__BackingField_14() const { return ___U3CCallbackU3Ek__BackingField_14; }
	inline OnUploadClusterConfig_t589517099 ** get_address_of_U3CCallbackU3Ek__BackingField_14() { return &___U3CCallbackU3Ek__BackingField_14; }
	inline void set_U3CCallbackU3Ek__BackingField_14(OnUploadClusterConfig_t589517099 * value)
	{
		___U3CCallbackU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPLOADCLUSTERCONFIGREQUEST_T3639785499_H
#ifndef GETCLUSTERCONFIGREQUEST_T1264100768_H
#define GETCLUSTERCONFIGREQUEST_T1264100768_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/GetClusterConfigRequest
struct  GetClusterConfigRequest_t1264100768  : public Request_t466816980
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/GetClusterConfigRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/GetClusterConfigRequest::<ClusterID>k__BackingField
	String_t* ___U3CClusterIDU3Ek__BackingField_12;
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/GetClusterConfigRequest::<ConfigName>k__BackingField
	String_t* ___U3CConfigNameU3Ek__BackingField_13;
	// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/OnGetClusterConfig IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/GetClusterConfigRequest::<Callback>k__BackingField
	OnGetClusterConfig_t1069230450 * ___U3CCallbackU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetClusterConfigRequest_t1264100768, ___U3CDataU3Ek__BackingField_11)); }
	inline String_t* get_U3CDataU3Ek__BackingField_11() const { return ___U3CDataU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_11() { return &___U3CDataU3Ek__BackingField_11; }
	inline void set_U3CDataU3Ek__BackingField_11(String_t* value)
	{
		___U3CDataU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CClusterIDU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GetClusterConfigRequest_t1264100768, ___U3CClusterIDU3Ek__BackingField_12)); }
	inline String_t* get_U3CClusterIDU3Ek__BackingField_12() const { return ___U3CClusterIDU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CClusterIDU3Ek__BackingField_12() { return &___U3CClusterIDU3Ek__BackingField_12; }
	inline void set_U3CClusterIDU3Ek__BackingField_12(String_t* value)
	{
		___U3CClusterIDU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CClusterIDU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CConfigNameU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(GetClusterConfigRequest_t1264100768, ___U3CConfigNameU3Ek__BackingField_13)); }
	inline String_t* get_U3CConfigNameU3Ek__BackingField_13() const { return ___U3CConfigNameU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CConfigNameU3Ek__BackingField_13() { return &___U3CConfigNameU3Ek__BackingField_13; }
	inline void set_U3CConfigNameU3Ek__BackingField_13(String_t* value)
	{
		___U3CConfigNameU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CConfigNameU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(GetClusterConfigRequest_t1264100768, ___U3CCallbackU3Ek__BackingField_14)); }
	inline OnGetClusterConfig_t1069230450 * get_U3CCallbackU3Ek__BackingField_14() const { return ___U3CCallbackU3Ek__BackingField_14; }
	inline OnGetClusterConfig_t1069230450 ** get_address_of_U3CCallbackU3Ek__BackingField_14() { return &___U3CCallbackU3Ek__BackingField_14; }
	inline void set_U3CCallbackU3Ek__BackingField_14(OnGetClusterConfig_t1069230450 * value)
	{
		___U3CCallbackU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETCLUSTERCONFIGREQUEST_T1264100768_H
#ifndef CREATECLUSTERREQUEST_T616269052_H
#define CREATECLUSTERREQUEST_T616269052_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/CreateClusterRequest
struct  CreateClusterRequest_t616269052  : public Request_t466816980
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/CreateClusterRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_11;
	// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/OnCreateCluster IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/CreateClusterRequest::<Callback>k__BackingField
	OnCreateCluster_t1489102388 * ___U3CCallbackU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(CreateClusterRequest_t616269052, ___U3CDataU3Ek__BackingField_11)); }
	inline String_t* get_U3CDataU3Ek__BackingField_11() const { return ___U3CDataU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_11() { return &___U3CDataU3Ek__BackingField_11; }
	inline void set_U3CDataU3Ek__BackingField_11(String_t* value)
	{
		___U3CDataU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(CreateClusterRequest_t616269052, ___U3CCallbackU3Ek__BackingField_12)); }
	inline OnCreateCluster_t1489102388 * get_U3CCallbackU3Ek__BackingField_12() const { return ___U3CCallbackU3Ek__BackingField_12; }
	inline OnCreateCluster_t1489102388 ** get_address_of_U3CCallbackU3Ek__BackingField_12() { return &___U3CCallbackU3Ek__BackingField_12; }
	inline void set_U3CCallbackU3Ek__BackingField_12(OnCreateCluster_t1489102388 * value)
	{
		___U3CCallbackU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREATECLUSTERREQUEST_T616269052_H
#ifndef DELETECLUSTERCONFIGREQUEST_T2137334505_H
#define DELETECLUSTERCONFIGREQUEST_T2137334505_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/DeleteClusterConfigRequest
struct  DeleteClusterConfigRequest_t2137334505  : public Request_t466816980
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/DeleteClusterConfigRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/DeleteClusterConfigRequest::<ClusterID>k__BackingField
	String_t* ___U3CClusterIDU3Ek__BackingField_12;
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/DeleteClusterConfigRequest::<ConfigID>k__BackingField
	String_t* ___U3CConfigIDU3Ek__BackingField_13;
	// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/OnDeleteClusterConfig IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/DeleteClusterConfigRequest::<Callback>k__BackingField
	OnDeleteClusterConfig_t2578936137 * ___U3CCallbackU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(DeleteClusterConfigRequest_t2137334505, ___U3CDataU3Ek__BackingField_11)); }
	inline String_t* get_U3CDataU3Ek__BackingField_11() const { return ___U3CDataU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_11() { return &___U3CDataU3Ek__BackingField_11; }
	inline void set_U3CDataU3Ek__BackingField_11(String_t* value)
	{
		___U3CDataU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CClusterIDU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(DeleteClusterConfigRequest_t2137334505, ___U3CClusterIDU3Ek__BackingField_12)); }
	inline String_t* get_U3CClusterIDU3Ek__BackingField_12() const { return ___U3CClusterIDU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CClusterIDU3Ek__BackingField_12() { return &___U3CClusterIDU3Ek__BackingField_12; }
	inline void set_U3CClusterIDU3Ek__BackingField_12(String_t* value)
	{
		___U3CClusterIDU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CClusterIDU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CConfigIDU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(DeleteClusterConfigRequest_t2137334505, ___U3CConfigIDU3Ek__BackingField_13)); }
	inline String_t* get_U3CConfigIDU3Ek__BackingField_13() const { return ___U3CConfigIDU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CConfigIDU3Ek__BackingField_13() { return &___U3CConfigIDU3Ek__BackingField_13; }
	inline void set_U3CConfigIDU3Ek__BackingField_13(String_t* value)
	{
		___U3CConfigIDU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CConfigIDU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(DeleteClusterConfigRequest_t2137334505, ___U3CCallbackU3Ek__BackingField_14)); }
	inline OnDeleteClusterConfig_t2578936137 * get_U3CCallbackU3Ek__BackingField_14() const { return ___U3CCallbackU3Ek__BackingField_14; }
	inline OnDeleteClusterConfig_t2578936137 ** get_address_of_U3CCallbackU3Ek__BackingField_14() { return &___U3CCallbackU3Ek__BackingField_14; }
	inline void set_U3CCallbackU3Ek__BackingField_14(OnDeleteClusterConfig_t2578936137 * value)
	{
		___U3CCallbackU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELETECLUSTERCONFIGREQUEST_T2137334505_H
#ifndef GETRANKERSREQUEST_T2633918666_H
#define GETRANKERSREQUEST_T2633918666_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/GetRankersRequest
struct  GetRankersRequest_t2633918666  : public Request_t466816980
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/GetRankersRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_11;
	// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/OnGetRankers IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/GetRankersRequest::<Callback>k__BackingField
	OnGetRankers_t1731100556 * ___U3CCallbackU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetRankersRequest_t2633918666, ___U3CDataU3Ek__BackingField_11)); }
	inline String_t* get_U3CDataU3Ek__BackingField_11() const { return ___U3CDataU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_11() { return &___U3CDataU3Ek__BackingField_11; }
	inline void set_U3CDataU3Ek__BackingField_11(String_t* value)
	{
		___U3CDataU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GetRankersRequest_t2633918666, ___U3CCallbackU3Ek__BackingField_12)); }
	inline OnGetRankers_t1731100556 * get_U3CCallbackU3Ek__BackingField_12() const { return ___U3CCallbackU3Ek__BackingField_12; }
	inline OnGetRankers_t1731100556 ** get_address_of_U3CCallbackU3Ek__BackingField_12() { return &___U3CCallbackU3Ek__BackingField_12; }
	inline void set_U3CCallbackU3Ek__BackingField_12(OnGetRankers_t1731100556 * value)
	{
		___U3CCallbackU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRANKERSREQUEST_T2633918666_H
#ifndef VOID_T1841601450_H
#define VOID_T1841601450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1841601450 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1841601450_H
#ifndef SEARCHREQUEST_T1893815408_H
#define SEARCHREQUEST_T1893815408_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/SearchRequest
struct  SearchRequest_t1893815408  : public Request_t466816980
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/SearchRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/SearchRequest::<ClusterID>k__BackingField
	String_t* ___U3CClusterIDU3Ek__BackingField_12;
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/SearchRequest::<CollectionName>k__BackingField
	String_t* ___U3CCollectionNameU3Ek__BackingField_13;
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/SearchRequest::<Query>k__BackingField
	String_t* ___U3CQueryU3Ek__BackingField_14;
	// System.String[] IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/SearchRequest::<Fl>k__BackingField
	StringU5BU5D_t1642385972* ___U3CFlU3Ek__BackingField_15;
	// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/OnSearch IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/SearchRequest::<Callback>k__BackingField
	OnSearch_t2575532994 * ___U3CCallbackU3Ek__BackingField_16;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(SearchRequest_t1893815408, ___U3CDataU3Ek__BackingField_11)); }
	inline String_t* get_U3CDataU3Ek__BackingField_11() const { return ___U3CDataU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_11() { return &___U3CDataU3Ek__BackingField_11; }
	inline void set_U3CDataU3Ek__BackingField_11(String_t* value)
	{
		___U3CDataU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CClusterIDU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(SearchRequest_t1893815408, ___U3CClusterIDU3Ek__BackingField_12)); }
	inline String_t* get_U3CClusterIDU3Ek__BackingField_12() const { return ___U3CClusterIDU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CClusterIDU3Ek__BackingField_12() { return &___U3CClusterIDU3Ek__BackingField_12; }
	inline void set_U3CClusterIDU3Ek__BackingField_12(String_t* value)
	{
		___U3CClusterIDU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CClusterIDU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CCollectionNameU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(SearchRequest_t1893815408, ___U3CCollectionNameU3Ek__BackingField_13)); }
	inline String_t* get_U3CCollectionNameU3Ek__BackingField_13() const { return ___U3CCollectionNameU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CCollectionNameU3Ek__BackingField_13() { return &___U3CCollectionNameU3Ek__BackingField_13; }
	inline void set_U3CCollectionNameU3Ek__BackingField_13(String_t* value)
	{
		___U3CCollectionNameU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCollectionNameU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CQueryU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(SearchRequest_t1893815408, ___U3CQueryU3Ek__BackingField_14)); }
	inline String_t* get_U3CQueryU3Ek__BackingField_14() const { return ___U3CQueryU3Ek__BackingField_14; }
	inline String_t** get_address_of_U3CQueryU3Ek__BackingField_14() { return &___U3CQueryU3Ek__BackingField_14; }
	inline void set_U3CQueryU3Ek__BackingField_14(String_t* value)
	{
		___U3CQueryU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CQueryU3Ek__BackingField_14), value);
	}

	inline static int32_t get_offset_of_U3CFlU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(SearchRequest_t1893815408, ___U3CFlU3Ek__BackingField_15)); }
	inline StringU5BU5D_t1642385972* get_U3CFlU3Ek__BackingField_15() const { return ___U3CFlU3Ek__BackingField_15; }
	inline StringU5BU5D_t1642385972** get_address_of_U3CFlU3Ek__BackingField_15() { return &___U3CFlU3Ek__BackingField_15; }
	inline void set_U3CFlU3Ek__BackingField_15(StringU5BU5D_t1642385972* value)
	{
		___U3CFlU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFlU3Ek__BackingField_15), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(SearchRequest_t1893815408, ___U3CCallbackU3Ek__BackingField_16)); }
	inline OnSearch_t2575532994 * get_U3CCallbackU3Ek__BackingField_16() const { return ___U3CCallbackU3Ek__BackingField_16; }
	inline OnSearch_t2575532994 ** get_address_of_U3CCallbackU3Ek__BackingField_16() { return &___U3CCallbackU3Ek__BackingField_16; }
	inline void set_U3CCallbackU3Ek__BackingField_16(OnSearch_t2575532994 * value)
	{
		___U3CCallbackU3Ek__BackingField_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEARCHREQUEST_T1893815408_H
#ifndef COLLECTIONREQUEST_T2133486888_H
#define COLLECTIONREQUEST_T2133486888_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/CollectionRequest
struct  CollectionRequest_t2133486888  : public Request_t466816980
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/CollectionRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_11;
	// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/OnCollections IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/CollectionRequest::<Callback>k__BackingField
	OnCollections_t2275029757 * ___U3CCallbackU3Ek__BackingField_12;
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/CollectionRequest::<ClusterID>k__BackingField
	String_t* ___U3CClusterIDU3Ek__BackingField_13;
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/CollectionRequest::<Action>k__BackingField
	String_t* ___U3CActionU3Ek__BackingField_14;
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/CollectionRequest::<CollectionName>k__BackingField
	String_t* ___U3CCollectionNameU3Ek__BackingField_15;
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/CollectionRequest::<ConfigName>k__BackingField
	String_t* ___U3CConfigNameU3Ek__BackingField_16;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(CollectionRequest_t2133486888, ___U3CDataU3Ek__BackingField_11)); }
	inline String_t* get_U3CDataU3Ek__BackingField_11() const { return ___U3CDataU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_11() { return &___U3CDataU3Ek__BackingField_11; }
	inline void set_U3CDataU3Ek__BackingField_11(String_t* value)
	{
		___U3CDataU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(CollectionRequest_t2133486888, ___U3CCallbackU3Ek__BackingField_12)); }
	inline OnCollections_t2275029757 * get_U3CCallbackU3Ek__BackingField_12() const { return ___U3CCallbackU3Ek__BackingField_12; }
	inline OnCollections_t2275029757 ** get_address_of_U3CCallbackU3Ek__BackingField_12() { return &___U3CCallbackU3Ek__BackingField_12; }
	inline void set_U3CCallbackU3Ek__BackingField_12(OnCollections_t2275029757 * value)
	{
		___U3CCallbackU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CClusterIDU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(CollectionRequest_t2133486888, ___U3CClusterIDU3Ek__BackingField_13)); }
	inline String_t* get_U3CClusterIDU3Ek__BackingField_13() const { return ___U3CClusterIDU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CClusterIDU3Ek__BackingField_13() { return &___U3CClusterIDU3Ek__BackingField_13; }
	inline void set_U3CClusterIDU3Ek__BackingField_13(String_t* value)
	{
		___U3CClusterIDU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CClusterIDU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CActionU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(CollectionRequest_t2133486888, ___U3CActionU3Ek__BackingField_14)); }
	inline String_t* get_U3CActionU3Ek__BackingField_14() const { return ___U3CActionU3Ek__BackingField_14; }
	inline String_t** get_address_of_U3CActionU3Ek__BackingField_14() { return &___U3CActionU3Ek__BackingField_14; }
	inline void set_U3CActionU3Ek__BackingField_14(String_t* value)
	{
		___U3CActionU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CActionU3Ek__BackingField_14), value);
	}

	inline static int32_t get_offset_of_U3CCollectionNameU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(CollectionRequest_t2133486888, ___U3CCollectionNameU3Ek__BackingField_15)); }
	inline String_t* get_U3CCollectionNameU3Ek__BackingField_15() const { return ___U3CCollectionNameU3Ek__BackingField_15; }
	inline String_t** get_address_of_U3CCollectionNameU3Ek__BackingField_15() { return &___U3CCollectionNameU3Ek__BackingField_15; }
	inline void set_U3CCollectionNameU3Ek__BackingField_15(String_t* value)
	{
		___U3CCollectionNameU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCollectionNameU3Ek__BackingField_15), value);
	}

	inline static int32_t get_offset_of_U3CConfigNameU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(CollectionRequest_t2133486888, ___U3CConfigNameU3Ek__BackingField_16)); }
	inline String_t* get_U3CConfigNameU3Ek__BackingField_16() const { return ___U3CConfigNameU3Ek__BackingField_16; }
	inline String_t** get_address_of_U3CConfigNameU3Ek__BackingField_16() { return &___U3CConfigNameU3Ek__BackingField_16; }
	inline void set_U3CConfigNameU3Ek__BackingField_16(String_t* value)
	{
		___U3CConfigNameU3Ek__BackingField_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CConfigNameU3Ek__BackingField_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTIONREQUEST_T2133486888_H
#ifndef INDEXDOCUMENTSREQUEST_T3942467882_H
#define INDEXDOCUMENTSREQUEST_T3942467882_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/IndexDocumentsRequest
struct  IndexDocumentsRequest_t3942467882  : public Request_t466816980
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/IndexDocumentsRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/IndexDocumentsRequest::<IndexDataPath>k__BackingField
	String_t* ___U3CIndexDataPathU3Ek__BackingField_12;
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/IndexDocumentsRequest::<ClusterID>k__BackingField
	String_t* ___U3CClusterIDU3Ek__BackingField_13;
	// System.String IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/IndexDocumentsRequest::<CollectionName>k__BackingField
	String_t* ___U3CCollectionNameU3Ek__BackingField_14;
	// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/OnIndexDocuments IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/IndexDocumentsRequest::<Callback>k__BackingField
	OnIndexDocuments_t1217349952 * ___U3CCallbackU3Ek__BackingField_15;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(IndexDocumentsRequest_t3942467882, ___U3CDataU3Ek__BackingField_11)); }
	inline String_t* get_U3CDataU3Ek__BackingField_11() const { return ___U3CDataU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_11() { return &___U3CDataU3Ek__BackingField_11; }
	inline void set_U3CDataU3Ek__BackingField_11(String_t* value)
	{
		___U3CDataU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CIndexDataPathU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(IndexDocumentsRequest_t3942467882, ___U3CIndexDataPathU3Ek__BackingField_12)); }
	inline String_t* get_U3CIndexDataPathU3Ek__BackingField_12() const { return ___U3CIndexDataPathU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CIndexDataPathU3Ek__BackingField_12() { return &___U3CIndexDataPathU3Ek__BackingField_12; }
	inline void set_U3CIndexDataPathU3Ek__BackingField_12(String_t* value)
	{
		___U3CIndexDataPathU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CIndexDataPathU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CClusterIDU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(IndexDocumentsRequest_t3942467882, ___U3CClusterIDU3Ek__BackingField_13)); }
	inline String_t* get_U3CClusterIDU3Ek__BackingField_13() const { return ___U3CClusterIDU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CClusterIDU3Ek__BackingField_13() { return &___U3CClusterIDU3Ek__BackingField_13; }
	inline void set_U3CClusterIDU3Ek__BackingField_13(String_t* value)
	{
		___U3CClusterIDU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CClusterIDU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CCollectionNameU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(IndexDocumentsRequest_t3942467882, ___U3CCollectionNameU3Ek__BackingField_14)); }
	inline String_t* get_U3CCollectionNameU3Ek__BackingField_14() const { return ___U3CCollectionNameU3Ek__BackingField_14; }
	inline String_t** get_address_of_U3CCollectionNameU3Ek__BackingField_14() { return &___U3CCollectionNameU3Ek__BackingField_14; }
	inline void set_U3CCollectionNameU3Ek__BackingField_14(String_t* value)
	{
		___U3CCollectionNameU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCollectionNameU3Ek__BackingField_14), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(IndexDocumentsRequest_t3942467882, ___U3CCallbackU3Ek__BackingField_15)); }
	inline OnIndexDocuments_t1217349952 * get_U3CCallbackU3Ek__BackingField_15() const { return ___U3CCallbackU3Ek__BackingField_15; }
	inline OnIndexDocuments_t1217349952 ** get_address_of_U3CCallbackU3Ek__BackingField_15() { return &___U3CCallbackU3Ek__BackingField_15; }
	inline void set_U3CCallbackU3Ek__BackingField_15(OnIndexDocuments_t1217349952 * value)
	{
		___U3CCallbackU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INDEXDOCUMENTSREQUEST_T3942467882_H
#ifndef CLASSIFYREQ_T2507420843_H
#define CLASSIFYREQ_T2507420843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.NaturalLanguageClassifier/ClassifyReq
struct  ClassifyReq_t2507420843  : public Request_t466816980
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.NaturalLanguageClassifier/ClassifyReq::<TextId>k__BackingField
	String_t* ___U3CTextIdU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.NaturalLanguageClassifier/ClassifyReq::<ClassiferId>k__BackingField
	String_t* ___U3CClassiferIdU3Ek__BackingField_12;
	// IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.NaturalLanguageClassifier/OnClassify IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.NaturalLanguageClassifier/ClassifyReq::<Callback>k__BackingField
	OnClassify_t2074661398 * ___U3CCallbackU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_U3CTextIdU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(ClassifyReq_t2507420843, ___U3CTextIdU3Ek__BackingField_11)); }
	inline String_t* get_U3CTextIdU3Ek__BackingField_11() const { return ___U3CTextIdU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CTextIdU3Ek__BackingField_11() { return &___U3CTextIdU3Ek__BackingField_11; }
	inline void set_U3CTextIdU3Ek__BackingField_11(String_t* value)
	{
		___U3CTextIdU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTextIdU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CClassiferIdU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(ClassifyReq_t2507420843, ___U3CClassiferIdU3Ek__BackingField_12)); }
	inline String_t* get_U3CClassiferIdU3Ek__BackingField_12() const { return ___U3CClassiferIdU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CClassiferIdU3Ek__BackingField_12() { return &___U3CClassiferIdU3Ek__BackingField_12; }
	inline void set_U3CClassiferIdU3Ek__BackingField_12(String_t* value)
	{
		___U3CClassiferIdU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CClassiferIdU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(ClassifyReq_t2507420843, ___U3CCallbackU3Ek__BackingField_13)); }
	inline OnClassify_t2074661398 * get_U3CCallbackU3Ek__BackingField_13() const { return ___U3CCallbackU3Ek__BackingField_13; }
	inline OnClassify_t2074661398 ** get_address_of_U3CCallbackU3Ek__BackingField_13() { return &___U3CCallbackU3Ek__BackingField_13; }
	inline void set_U3CCallbackU3Ek__BackingField_13(OnClassify_t2074661398 * value)
	{
		___U3CCallbackU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLASSIFYREQ_T2507420843_H
#ifndef GETCLASSIFIERSREQ_T965357529_H
#define GETCLASSIFIERSREQ_T965357529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.NaturalLanguageClassifier/GetClassifiersReq
struct  GetClassifiersReq_t965357529  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.NaturalLanguageClassifier/OnGetClassifiers IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.NaturalLanguageClassifier/GetClassifiersReq::<Callback>k__BackingField
	OnGetClassifiers_t1859727718 * ___U3CCallbackU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetClassifiersReq_t965357529, ___U3CCallbackU3Ek__BackingField_11)); }
	inline OnGetClassifiers_t1859727718 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline OnGetClassifiers_t1859727718 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(OnGetClassifiers_t1859727718 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETCLASSIFIERSREQ_T965357529_H
#ifndef GETPROFILEREQUEST_T3346160902_H
#define GETPROFILEREQUEST_T3346160902_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v2.PersonalityInsights/GetProfileRequest
struct  GetProfileRequest_t3346160902  : public Request_t466816980
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v2.PersonalityInsights/GetProfileRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_11;
	// IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v2.PersonalityInsights/OnGetProfile IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v2.PersonalityInsights/GetProfileRequest::<Callback>k__BackingField
	OnGetProfile_t2522047396 * ___U3CCallbackU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetProfileRequest_t3346160902, ___U3CDataU3Ek__BackingField_11)); }
	inline String_t* get_U3CDataU3Ek__BackingField_11() const { return ___U3CDataU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_11() { return &___U3CDataU3Ek__BackingField_11; }
	inline void set_U3CDataU3Ek__BackingField_11(String_t* value)
	{
		___U3CDataU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GetProfileRequest_t3346160902, ___U3CCallbackU3Ek__BackingField_12)); }
	inline OnGetProfile_t2522047396 * get_U3CCallbackU3Ek__BackingField_12() const { return ___U3CCallbackU3Ek__BackingField_12; }
	inline OnGetProfile_t2522047396 ** get_address_of_U3CCallbackU3Ek__BackingField_12() { return &___U3CCallbackU3Ek__BackingField_12; }
	inline void set_U3CCallbackU3Ek__BackingField_12(OnGetProfile_t2522047396 * value)
	{
		___U3CCallbackU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETPROFILEREQUEST_T3346160902_H
#ifndef GETCLASSIFIERREQ_T2957664814_H
#define GETCLASSIFIERREQ_T2957664814_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.NaturalLanguageClassifier/GetClassifierReq
struct  GetClassifierReq_t2957664814  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.NaturalLanguageClassifier/OnGetClassifier IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.NaturalLanguageClassifier/GetClassifierReq::<Callback>k__BackingField
	OnGetClassifier_t1783367173 * ___U3CCallbackU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetClassifierReq_t2957664814, ___U3CCallbackU3Ek__BackingField_11)); }
	inline OnGetClassifier_t1783367173 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline OnGetClassifier_t1783367173 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(OnGetClassifier_t1783367173 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETCLASSIFIERREQ_T2957664814_H
#ifndef TRAINCLASSIFIERREQ_T396738312_H
#define TRAINCLASSIFIERREQ_T396738312_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.NaturalLanguageClassifier/TrainClassifierReq
struct  TrainClassifierReq_t396738312  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.NaturalLanguageClassifier/OnTrainClassifier IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.NaturalLanguageClassifier/TrainClassifierReq::<Callback>k__BackingField
	OnTrainClassifier_t2091787415 * ___U3CCallbackU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(TrainClassifierReq_t396738312, ___U3CCallbackU3Ek__BackingField_11)); }
	inline OnTrainClassifier_t2091787415 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline OnTrainClassifier_t2091787415 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(OnTrainClassifier_t2091787415 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRAINCLASSIFIERREQ_T396738312_H
#ifndef DELETECLASSIFIERREQ_T4038740477_H
#define DELETECLASSIFIERREQ_T4038740477_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.NaturalLanguageClassifier/DeleteClassifierReq
struct  DeleteClassifierReq_t4038740477  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.NaturalLanguageClassifier/OnDeleteClassifier IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.NaturalLanguageClassifier/DeleteClassifierReq::<Callback>k__BackingField
	OnDeleteClassifier_t3710463002 * ___U3CCallbackU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(DeleteClassifierReq_t4038740477, ___U3CCallbackU3Ek__BackingField_11)); }
	inline OnDeleteClassifier_t3710463002 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline OnDeleteClassifier_t3710463002 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(OnDeleteClassifier_t3710463002 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELETECLASSIFIERREQ_T4038740477_H
#ifndef GETPROFILEREQUEST_T2942284135_H
#define GETPROFILEREQUEST_T2942284135_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v3.PersonalityInsights/GetProfileRequest
struct  GetProfileRequest_t2942284135  : public Request_t466816980
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v3.PersonalityInsights/GetProfileRequest::<Source>k__BackingField
	String_t* ___U3CSourceU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v3.PersonalityInsights/GetProfileRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_12;
	// IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v3.PersonalityInsights/OnGetProfile IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v3.PersonalityInsights/GetProfileRequest::<Callback>k__BackingField
	OnGetProfile_t2596486115 * ___U3CCallbackU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_U3CSourceU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetProfileRequest_t2942284135, ___U3CSourceU3Ek__BackingField_11)); }
	inline String_t* get_U3CSourceU3Ek__BackingField_11() const { return ___U3CSourceU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CSourceU3Ek__BackingField_11() { return &___U3CSourceU3Ek__BackingField_11; }
	inline void set_U3CSourceU3Ek__BackingField_11(String_t* value)
	{
		___U3CSourceU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSourceU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GetProfileRequest_t2942284135, ___U3CDataU3Ek__BackingField_12)); }
	inline String_t* get_U3CDataU3Ek__BackingField_12() const { return ___U3CDataU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_12() { return &___U3CDataU3Ek__BackingField_12; }
	inline void set_U3CDataU3Ek__BackingField_12(String_t* value)
	{
		___U3CDataU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(GetProfileRequest_t2942284135, ___U3CCallbackU3Ek__BackingField_13)); }
	inline OnGetProfile_t2596486115 * get_U3CCallbackU3Ek__BackingField_13() const { return ___U3CCallbackU3Ek__BackingField_13; }
	inline OnGetProfile_t2596486115 ** get_address_of_U3CCallbackU3Ek__BackingField_13() { return &___U3CCallbackU3Ek__BackingField_13; }
	inline void set_U3CCallbackU3Ek__BackingField_13(OnGetProfile_t2596486115 * value)
	{
		___U3CCallbackU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETPROFILEREQUEST_T2942284135_H
#ifndef IDENTIFYREQ_T1608333675_H
#define IDENTIFYREQ_T1608333675_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.LanguageTranslator/IdentifyReq
struct  IdentifyReq_t1608333675  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.LanguageTranslator/IdentifyCallback IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.LanguageTranslator/IdentifyReq::<Callback>k__BackingField
	IdentifyCallback_t682135812 * ___U3CCallbackU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(IdentifyReq_t1608333675, ___U3CCallbackU3Ek__BackingField_11)); }
	inline IdentifyCallback_t682135812 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline IdentifyCallback_t682135812 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(IdentifyCallback_t682135812 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IDENTIFYREQ_T1608333675_H
#ifndef GETLANGUAGESREQ_T3181628952_H
#define GETLANGUAGESREQ_T3181628952_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.LanguageTranslator/GetLanguagesReq
struct  GetLanguagesReq_t3181628952  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.LanguageTranslator/GetLanguagesCallback IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.LanguageTranslator/GetLanguagesReq::<Callback>k__BackingField
	GetLanguagesCallback_t1996869611 * ___U3CCallbackU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetLanguagesReq_t3181628952, ___U3CCallbackU3Ek__BackingField_11)); }
	inline GetLanguagesCallback_t1996869611 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline GetLanguagesCallback_t1996869611 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(GetLanguagesCallback_t1996869611 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETLANGUAGESREQ_T3181628952_H
#ifndef GETMODELREQ_T3469869554_H
#define GETMODELREQ_T3469869554_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.LanguageTranslator/GetModelReq
struct  GetModelReq_t3469869554  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.LanguageTranslator/GetModelCallback IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.LanguageTranslator/GetModelReq::<Callback>k__BackingField
	GetModelCallback_t1483236201 * ___U3CCallbackU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetModelReq_t3469869554, ___U3CCallbackU3Ek__BackingField_11)); }
	inline GetModelCallback_t1483236201 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline GetModelCallback_t1483236201 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(GetModelCallback_t1483236201 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETMODELREQ_T3469869554_H
#ifndef DELEGATE_T3022476291_H
#define DELEGATE_T3022476291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t3022476291  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	IntPtr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	IntPtr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	IntPtr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	IntPtr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1572802995 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___invoke_impl_1)); }
	inline IntPtr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline IntPtr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(IntPtr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_3)); }
	inline IntPtr_t get_method_3() const { return ___method_3; }
	inline IntPtr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(IntPtr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___delegate_trampoline_4)); }
	inline IntPtr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline IntPtr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(IntPtr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_code_5)); }
	inline IntPtr_t get_method_code_5() const { return ___method_code_5; }
	inline IntPtr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(IntPtr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___data_8)); }
	inline DelegateData_t1572802995 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1572802995 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1572802995 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T3022476291_H
#ifndef MULTICASTDELEGATE_T3201952435_H
#define MULTICASTDELEGATE_T3201952435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t3201952435  : public Delegate_t3022476291
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t3201952435 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t3201952435 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___prev_9)); }
	inline MulticastDelegate_t3201952435 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t3201952435 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t3201952435 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___kpm_next_10)); }
	inline MulticastDelegate_t3201952435 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t3201952435 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t3201952435 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T3201952435_H
#ifndef ONDELETECLASSIFIER_T3710463002_H
#define ONDELETECLASSIFIER_T3710463002_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.NaturalLanguageClassifier/OnDeleteClassifier
struct  OnDeleteClassifier_t3710463002  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDELETECLASSIFIER_T3710463002_H
#ifndef ONSEARCH_T2575532994_H
#define ONSEARCH_T2575532994_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/OnSearch
struct  OnSearch_t2575532994  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONSEARCH_T2575532994_H
#ifndef ONFINDCLASSIFIER_T2470508822_H
#define ONFINDCLASSIFIER_T2470508822_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.NaturalLanguageClassifier/OnFindClassifier
struct  OnFindClassifier_t2470508822  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONFINDCLASSIFIER_T2470508822_H
#ifndef LOADFILEDELEGATE_T1720639326_H
#define LOADFILEDELEGATE_T1720639326_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/LoadFileDelegate
struct  LoadFileDelegate_t1720639326  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADFILEDELEGATE_T1720639326_H
#ifndef ONGETRANKERS_T1731100556_H
#define ONGETRANKERS_T1731100556_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/OnGetRankers
struct  OnGetRankers_t1731100556  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONGETRANKERS_T1731100556_H
#ifndef ONGETCLASSIFIERS_T1859727718_H
#define ONGETCLASSIFIERS_T1859727718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.NaturalLanguageClassifier/OnGetClassifiers
struct  OnGetClassifiers_t1859727718  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONGETCLASSIFIERS_T1859727718_H
#ifndef ONTRAINCLASSIFIER_T2091787415_H
#define ONTRAINCLASSIFIER_T2091787415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.NaturalLanguageClassifier/OnTrainClassifier
struct  OnTrainClassifier_t2091787415  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONTRAINCLASSIFIER_T2091787415_H
#ifndef ONCREATERANKER_T496698577_H
#define ONCREATERANKER_T496698577_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/OnCreateRanker
struct  OnCreateRanker_t496698577  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONCREATERANKER_T496698577_H
#ifndef ONCLASSIFY_T2074661398_H
#define ONCLASSIFY_T2074661398_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.NaturalLanguageClassifier/OnClassify
struct  OnClassify_t2074661398  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONCLASSIFY_T2074661398_H
#ifndef ONGETCLASSIFIER_T1783367173_H
#define ONGETCLASSIFIER_T1783367173_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.NaturalLanguageClassifier/OnGetClassifier
struct  OnGetClassifier_t1783367173  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONGETCLASSIFIER_T1783367173_H
#ifndef ONGETPROFILE_T2596486115_H
#define ONGETPROFILE_T2596486115_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v3.PersonalityInsights/OnGetProfile
struct  OnGetProfile_t2596486115  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONGETPROFILE_T2596486115_H
#ifndef ONGETCLUSTER_T3066553756_H
#define ONGETCLUSTER_T3066553756_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/OnGetCluster
struct  OnGetCluster_t3066553756  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONGETCLUSTER_T3066553756_H
#ifndef ONGETCLUSTERCONFIGS_T22213349_H
#define ONGETCLUSTERCONFIGS_T22213349_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/OnGetClusterConfigs
struct  OnGetClusterConfigs_t22213349  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONGETCLUSTERCONFIGS_T22213349_H
#ifndef ONDELETECLUSTER_T1281715199_H
#define ONDELETECLUSTER_T1281715199_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/OnDeleteCluster
struct  OnDeleteCluster_t1281715199  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDELETECLUSTER_T1281715199_H
#ifndef SAVEFILEDELEGATE_T3125227661_H
#define SAVEFILEDELEGATE_T3125227661_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/SaveFileDelegate
struct  SaveFileDelegate_t3125227661  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAVEFILEDELEGATE_T3125227661_H
#ifndef ONGETCLUSTERS_T1915248271_H
#define ONGETCLUSTERS_T1915248271_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/OnGetClusters
struct  OnGetClusters_t1915248271  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONGETCLUSTERS_T1915248271_H
#ifndef ONCREATECLUSTER_T1489102388_H
#define ONCREATECLUSTER_T1489102388_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/OnCreateCluster
struct  OnCreateCluster_t1489102388  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONCREATECLUSTER_T1489102388_H
#ifndef ONUPLOADCLUSTERCONFIG_T589517099_H
#define ONUPLOADCLUSTERCONFIG_T589517099_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/OnUploadClusterConfig
struct  OnUploadClusterConfig_t589517099  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONUPLOADCLUSTERCONFIG_T589517099_H
#ifndef ONCOLLECTIONS_T2275029757_H
#define ONCOLLECTIONS_T2275029757_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/OnCollections
struct  OnCollections_t2275029757  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONCOLLECTIONS_T2275029757_H
#ifndef ONINDEXDOCUMENTS_T1217349952_H
#define ONINDEXDOCUMENTS_T1217349952_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/OnIndexDocuments
struct  OnIndexDocuments_t1217349952  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONINDEXDOCUMENTS_T1217349952_H
#ifndef ONSAVECLUSTERCONFIG_T1862884805_H
#define ONSAVECLUSTERCONFIG_T1862884805_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/OnSaveClusterConfig
struct  OnSaveClusterConfig_t1862884805  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONSAVECLUSTERCONFIG_T1862884805_H
#ifndef ONGETPROFILE_T2522047396_H
#define ONGETPROFILE_T2522047396_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v2.PersonalityInsights/OnGetProfile
struct  OnGetProfile_t2522047396  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONGETPROFILE_T2522047396_H
#ifndef ONDELETECLUSTERCONFIG_T2578936137_H
#define ONDELETECLUSTERCONFIG_T2578936137_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/OnDeleteClusterConfig
struct  OnDeleteClusterConfig_t2578936137  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDELETECLUSTERCONFIG_T2578936137_H
#ifndef ONGETCLUSTERCONFIG_T1069230450_H
#define ONGETCLUSTERCONFIG_T1069230450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank/OnGetClusterConfig
struct  OnGetClusterConfig_t1069230450  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONGETCLUSTERCONFIG_T1069230450_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2600 = { sizeof (GetModelReq_t3469869554), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2600[1] = 
{
	GetModelReq_t3469869554::get_offset_of_U3CCallbackU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2601 = { sizeof (GetLanguagesReq_t3181628952), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2601[1] = 
{
	GetLanguagesReq_t3181628952::get_offset_of_U3CCallbackU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2602 = { sizeof (IdentifyReq_t1608333675), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2602[1] = 
{
	IdentifyReq_t1608333675::get_offset_of_U3CCallbackU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2603 = { sizeof (CheckServiceStatus_t297802562), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2603[2] = 
{
	CheckServiceStatus_t297802562::get_offset_of_m_Service_0(),
	CheckServiceStatus_t297802562::get_offset_of_m_Callback_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2604 = { sizeof (Classifier_t1414011573), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2604[7] = 
{
	Classifier_t1414011573::get_offset_of_U3CnameU3Ek__BackingField_0(),
	Classifier_t1414011573::get_offset_of_U3ClanguageU3Ek__BackingField_1(),
	Classifier_t1414011573::get_offset_of_U3CurlU3Ek__BackingField_2(),
	Classifier_t1414011573::get_offset_of_U3Cclassifier_idU3Ek__BackingField_3(),
	Classifier_t1414011573::get_offset_of_U3CcreatedU3Ek__BackingField_4(),
	Classifier_t1414011573::get_offset_of_U3CstatusU3Ek__BackingField_5(),
	Classifier_t1414011573::get_offset_of_U3Cstatus_descriptionU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2605 = { sizeof (Classifiers_t1537491400), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2605[1] = 
{
	Classifiers_t1537491400::get_offset_of_U3CclassifiersU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2606 = { sizeof (Class_t2988469534), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2606[2] = 
{
	Class_t2988469534::get_offset_of_U3CconfidenceU3Ek__BackingField_0(),
	Class_t2988469534::get_offset_of_U3Cclass_nameU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2607 = { sizeof (ClassifyResult_t2166103447), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2607[5] = 
{
	ClassifyResult_t2166103447::get_offset_of_U3Cclassifier_idU3Ek__BackingField_0(),
	ClassifyResult_t2166103447::get_offset_of_U3CurlU3Ek__BackingField_1(),
	ClassifyResult_t2166103447::get_offset_of_U3CtextU3Ek__BackingField_2(),
	ClassifyResult_t2166103447::get_offset_of_U3Ctop_classU3Ek__BackingField_3(),
	ClassifyResult_t2166103447::get_offset_of_U3CclassesU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2608 = { sizeof (NaturalLanguageClassifier_t2492034444), -1, sizeof(NaturalLanguageClassifier_t2492034444_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2608[4] = 
{
	NaturalLanguageClassifier_t2492034444::get_offset_of_U3CDisableCacheU3Ek__BackingField_0(),
	0,
	NaturalLanguageClassifier_t2492034444_StaticFields::get_offset_of_sm_Serializer_2(),
	NaturalLanguageClassifier_t2492034444::get_offset_of_m_ClassifyCache_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2609 = { sizeof (OnGetClassifier_t1783367173), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2610 = { sizeof (OnTrainClassifier_t2091787415), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2611 = { sizeof (OnFindClassifier_t2470508822), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2612 = { sizeof (OnGetClassifiers_t1859727718), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2613 = { sizeof (OnClassify_t2074661398), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2614 = { sizeof (OnDeleteClassifier_t3710463002), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2615 = { sizeof (FindClassifierReq_t3159305857), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2615[3] = 
{
	FindClassifierReq_t3159305857::get_offset_of_U3CServiceU3Ek__BackingField_0(),
	FindClassifierReq_t3159305857::get_offset_of_U3CClassifierNameU3Ek__BackingField_1(),
	FindClassifierReq_t3159305857::get_offset_of_U3CCallbackU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2616 = { sizeof (GetClassifiersReq_t965357529), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2616[1] = 
{
	GetClassifiersReq_t965357529::get_offset_of_U3CCallbackU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2617 = { sizeof (GetClassifierReq_t2957664814), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2617[1] = 
{
	GetClassifierReq_t2957664814::get_offset_of_U3CCallbackU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2618 = { sizeof (TrainClassifierReq_t396738312), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2618[1] = 
{
	TrainClassifierReq_t396738312::get_offset_of_U3CCallbackU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2619 = { sizeof (DeleteClassifierReq_t4038740477), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2619[1] = 
{
	DeleteClassifierReq_t4038740477::get_offset_of_U3CCallbackU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2620 = { sizeof (ClassifyReq_t2507420843), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2620[3] = 
{
	ClassifyReq_t2507420843::get_offset_of_U3CTextIdU3Ek__BackingField_11(),
	ClassifyReq_t2507420843::get_offset_of_U3CClassiferIdU3Ek__BackingField_12(),
	ClassifyReq_t2507420843::get_offset_of_U3CCallbackU3Ek__BackingField_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2621 = { sizeof (CheckServiceStatus_t740786868), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2621[4] = 
{
	CheckServiceStatus_t740786868::get_offset_of_m_Service_0(),
	CheckServiceStatus_t740786868::get_offset_of_m_Callback_1(),
	CheckServiceStatus_t740786868::get_offset_of_m_GetClassifierCount_2(),
	CheckServiceStatus_t740786868::get_offset_of_m_ClassifyCount_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2622 = { sizeof (Profile_t4293889999), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2622[7] = 
{
	Profile_t4293889999::get_offset_of_U3CtreeU3Ek__BackingField_0(),
	Profile_t4293889999::get_offset_of_U3CidU3Ek__BackingField_1(),
	Profile_t4293889999::get_offset_of_U3CsourceU3Ek__BackingField_2(),
	Profile_t4293889999::get_offset_of_U3Cprocessed_langU3Ek__BackingField_3(),
	Profile_t4293889999::get_offset_of_U3Cword_countU3Ek__BackingField_4(),
	Profile_t4293889999::get_offset_of_U3Cword_count_messageU3Ek__BackingField_5(),
	Profile_t4293889999::get_offset_of_U3CwarningsU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2623 = { sizeof (TraitTreeNode_t694336958), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2623[8] = 
{
	TraitTreeNode_t694336958::get_offset_of_U3CidU3Ek__BackingField_0(),
	TraitTreeNode_t694336958::get_offset_of_U3CnameU3Ek__BackingField_1(),
	TraitTreeNode_t694336958::get_offset_of_U3CcategoryU3Ek__BackingField_2(),
	TraitTreeNode_t694336958::get_offset_of_U3CpercentageU3Ek__BackingField_3(),
	TraitTreeNode_t694336958::get_offset_of_U3Csampling_errorU3Ek__BackingField_4(),
	TraitTreeNode_t694336958::get_offset_of_U3Craw_scoreU3Ek__BackingField_5(),
	TraitTreeNode_t694336958::get_offset_of_U3Craw_sampling_errorU3Ek__BackingField_6(),
	TraitTreeNode_t694336958::get_offset_of_U3CchildrenU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2624 = { sizeof (Warning_t514099562), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2624[2] = 
{
	Warning_t514099562::get_offset_of_U3CidU3Ek__BackingField_0(),
	Warning_t514099562::get_offset_of_U3CmessageU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2625 = { sizeof (ContentListContainer_t811063702), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2625[1] = 
{
	ContentListContainer_t811063702::get_offset_of_U3CcontentItemsU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2626 = { sizeof (ContentItem_t1958644056), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2626[11] = 
{
	ContentItem_t1958644056::get_offset_of_U3CidU3Ek__BackingField_0(),
	ContentItem_t1958644056::get_offset_of_U3CuseridU3Ek__BackingField_1(),
	ContentItem_t1958644056::get_offset_of_U3CsourceidU3Ek__BackingField_2(),
	ContentItem_t1958644056::get_offset_of_U3CcreatedU3Ek__BackingField_3(),
	ContentItem_t1958644056::get_offset_of_U3CupdatedU3Ek__BackingField_4(),
	ContentItem_t1958644056::get_offset_of_U3CcontenttypeU3Ek__BackingField_5(),
	ContentItem_t1958644056::get_offset_of_U3ClanguageU3Ek__BackingField_6(),
	ContentItem_t1958644056::get_offset_of_U3CcontentU3Ek__BackingField_7(),
	ContentItem_t1958644056::get_offset_of_U3CparentidU3Ek__BackingField_8(),
	ContentItem_t1958644056::get_offset_of_U3CreplyU3Ek__BackingField_9(),
	ContentItem_t1958644056::get_offset_of_U3CforwardU3Ek__BackingField_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2627 = { sizeof (ContentType_t3618577653), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2627[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2628 = { sizeof (Language_t3866454500), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2628[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2629 = { sizeof (PersonalityInsights_t582046669), -1, sizeof(PersonalityInsights_t582046669_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2629[3] = 
{
	0,
	PersonalityInsights_t582046669_StaticFields::get_offset_of_sm_Serializer_1(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2630 = { sizeof (OnGetProfile_t2522047396), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2631 = { sizeof (GetProfileRequest_t3346160902), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2631[2] = 
{
	GetProfileRequest_t3346160902::get_offset_of_U3CDataU3Ek__BackingField_11(),
	GetProfileRequest_t3346160902::get_offset_of_U3CCallbackU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2632 = { sizeof (CheckServiceStatus_t1948517221), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2632[2] = 
{
	CheckServiceStatus_t1948517221::get_offset_of_m_Service_0(),
	CheckServiceStatus_t1948517221::get_offset_of_m_Callback_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2633 = { sizeof (Profile_t3811251445), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2633[9] = 
{
	Profile_t3811251445::get_offset_of_U3Cprocessed_languageU3Ek__BackingField_0(),
	Profile_t3811251445::get_offset_of_U3Cword_countU3Ek__BackingField_1(),
	Profile_t3811251445::get_offset_of_U3Cword_count_messageU3Ek__BackingField_2(),
	Profile_t3811251445::get_offset_of_U3CpersonalityU3Ek__BackingField_3(),
	Profile_t3811251445::get_offset_of_U3CvaluesU3Ek__BackingField_4(),
	Profile_t3811251445::get_offset_of_U3CneedsU3Ek__BackingField_5(),
	Profile_t3811251445::get_offset_of_U3CbehaviorU3Ek__BackingField_6(),
	Profile_t3811251445::get_offset_of_U3Cconsumption_preferencesU3Ek__BackingField_7(),
	Profile_t3811251445::get_offset_of_U3CwarningU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2634 = { sizeof (TraitTreeNode_t3511443394), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2634[6] = 
{
	TraitTreeNode_t3511443394::get_offset_of_U3Ctrait_idU3Ek__BackingField_0(),
	TraitTreeNode_t3511443394::get_offset_of_U3CnameU3Ek__BackingField_1(),
	TraitTreeNode_t3511443394::get_offset_of_U3CcategoryU3Ek__BackingField_2(),
	TraitTreeNode_t3511443394::get_offset_of_U3CpercentileU3Ek__BackingField_3(),
	TraitTreeNode_t3511443394::get_offset_of_U3Craw_scoreU3Ek__BackingField_4(),
	TraitTreeNode_t3511443394::get_offset_of_U3CchildrenU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2635 = { sizeof (BehaviorNode_t287073406), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2635[4] = 
{
	BehaviorNode_t287073406::get_offset_of_U3Ctrait_idU3Ek__BackingField_0(),
	BehaviorNode_t287073406::get_offset_of_U3CnameU3Ek__BackingField_1(),
	BehaviorNode_t287073406::get_offset_of_U3CcategoryU3Ek__BackingField_2(),
	BehaviorNode_t287073406::get_offset_of_U3CpercentageU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2636 = { sizeof (ConsumptionPreferencesCategoryNode_t38182357), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2636[3] = 
{
	ConsumptionPreferencesCategoryNode_t38182357::get_offset_of_U3Cconsumption_preference_category_idU3Ek__BackingField_0(),
	ConsumptionPreferencesCategoryNode_t38182357::get_offset_of_U3CnameU3Ek__BackingField_1(),
	ConsumptionPreferencesCategoryNode_t38182357::get_offset_of_U3Cconsumption_preferencesU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2637 = { sizeof (Warning_t31395408), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2637[2] = 
{
	Warning_t31395408::get_offset_of_U3Cwarning_idU3Ek__BackingField_0(),
	Warning_t31395408::get_offset_of_U3CmessageU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2638 = { sizeof (ConsumptionPreferencesNode_t1038535837), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2638[3] = 
{
	ConsumptionPreferencesNode_t1038535837::get_offset_of_U3Cconsumption_preference_idU3Ek__BackingField_0(),
	ConsumptionPreferencesNode_t1038535837::get_offset_of_U3CnameU3Ek__BackingField_1(),
	ConsumptionPreferencesNode_t1038535837::get_offset_of_U3CscoreU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2639 = { sizeof (ContentType_t2631042457), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2639[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2640 = { sizeof (ContentLanguage_t2096133991), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2640[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2641 = { sizeof (AcceptLanguage_t2731908252), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2641[11] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2642 = { sizeof (PersonalityInsightsVersion_t1779906393), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2642[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2643 = { sizeof (PersonalityInsights_t2503525937), -1, sizeof(PersonalityInsights_t2503525937_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2643[3] = 
{
	0,
	PersonalityInsights_t2503525937_StaticFields::get_offset_of_sm_Serializer_1(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2644 = { sizeof (OnGetProfile_t2596486115), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2645 = { sizeof (GetProfileRequest_t2942284135), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2645[3] = 
{
	GetProfileRequest_t2942284135::get_offset_of_U3CSourceU3Ek__BackingField_11(),
	GetProfileRequest_t2942284135::get_offset_of_U3CDataU3Ek__BackingField_12(),
	GetProfileRequest_t2942284135::get_offset_of_U3CCallbackU3Ek__BackingField_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2646 = { sizeof (CheckServiceStatus_t3235045572), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2646[2] = 
{
	CheckServiceStatus_t3235045572::get_offset_of_m_Service_0(),
	CheckServiceStatus_t3235045572::get_offset_of_m_Callback_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2647 = { sizeof (SolrClusterListResponse_t1287774645), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2647[1] = 
{
	SolrClusterListResponse_t1287774645::get_offset_of_U3CclustersU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2648 = { sizeof (SolrClusterResponse_t1008480713), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2648[4] = 
{
	SolrClusterResponse_t1008480713::get_offset_of_U3Csolr_cluster_idU3Ek__BackingField_0(),
	SolrClusterResponse_t1008480713::get_offset_of_U3Ccluster_nameU3Ek__BackingField_1(),
	SolrClusterResponse_t1008480713::get_offset_of_U3Ccluster_sizeU3Ek__BackingField_2(),
	SolrClusterResponse_t1008480713::get_offset_of_U3Csolr_cluster_statusU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2649 = { sizeof (SolrConfigList_t3679635936), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2649[1] = 
{
	SolrConfigList_t3679635936::get_offset_of_U3Csolr_configsU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2650 = { sizeof (ErrorResponsePayload_t3410979863), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2650[2] = 
{
	ErrorResponsePayload_t3410979863::get_offset_of_U3CmsgU3Ek__BackingField_0(),
	ErrorResponsePayload_t3410979863::get_offset_of_U3CcodeU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2651 = { sizeof (UploadResponse_t429219006), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2651[2] = 
{
	UploadResponse_t429219006::get_offset_of_U3CmessageU3Ek__BackingField_0(),
	UploadResponse_t429219006::get_offset_of_U3CstatusCodeU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2652 = { sizeof (DeleteResponse_t2373142580), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2652[2] = 
{
	DeleteResponse_t2373142580::get_offset_of_U3CmessageU3Ek__BackingField_0(),
	DeleteResponse_t2373142580::get_offset_of_U3CstatusCodeU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2653 = { sizeof (DeleteConfigResponse_t793172430), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2653[2] = 
{
	DeleteConfigResponse_t793172430::get_offset_of_U3CmessageU3Ek__BackingField_0(),
	DeleteConfigResponse_t793172430::get_offset_of_U3CstatusCodeU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2654 = { sizeof (MessageResponsePayload_t3469683060), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2654[1] = 
{
	MessageResponsePayload_t3469683060::get_offset_of_U3CmessageU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2655 = { sizeof (CollectionsResponse_t1627544196), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2655[4] = 
{
	CollectionsResponse_t1627544196::get_offset_of_U3CresponseHeaderU3Ek__BackingField_0(),
	CollectionsResponse_t1627544196::get_offset_of_U3CcollectionsU3Ek__BackingField_1(),
	CollectionsResponse_t1627544196::get_offset_of_U3CresponseU3Ek__BackingField_2(),
	CollectionsResponse_t1627544196::get_offset_of_U3CcoreU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2656 = { sizeof (ResponseHeader_t2485747570), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2656[3] = 
{
	ResponseHeader_t2485747570::get_offset_of_U3CstatusU3Ek__BackingField_0(),
	ResponseHeader_t2485747570::get_offset_of_U3CQTimeU3Ek__BackingField_1(),
	ResponseHeader_t2485747570::get_offset_of_U3C_paramsU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2657 = { sizeof (CollectionsAction_t1867305463), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2657[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2658 = { sizeof (IndexResponse_t1068437599), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2658[1] = 
{
	IndexResponse_t1068437599::get_offset_of_U3CresponseHeaderU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2659 = { sizeof (QueryParams_t3040774976), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2659[3] = 
{
	QueryParams_t3040774976::get_offset_of_U3CqU3Ek__BackingField_0(),
	QueryParams_t3040774976::get_offset_of_U3CflU3Ek__BackingField_1(),
	QueryParams_t3040774976::get_offset_of_U3CwtU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2660 = { sizeof (SearchResponse_t1678084193), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2660[2] = 
{
	SearchResponse_t1678084193::get_offset_of_U3CresponseHeaderU3Ek__BackingField_0(),
	SearchResponse_t1678084193::get_offset_of_U3CresponseU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2661 = { sizeof (Response_t1432761593), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2661[3] = 
{
	Response_t1432761593::get_offset_of_U3CnumFoundU3Ek__BackingField_0(),
	Response_t1432761593::get_offset_of_U3CstartU3Ek__BackingField_1(),
	Response_t1432761593::get_offset_of_U3CdocsU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2662 = { sizeof (Doc_t3160830270), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2662[5] = 
{
	Doc_t3160830270::get_offset_of_U3CidU3Ek__BackingField_0(),
	Doc_t3160830270::get_offset_of_U3CbodyU3Ek__BackingField_1(),
	Doc_t3160830270::get_offset_of_U3CtitleU3Ek__BackingField_2(),
	Doc_t3160830270::get_offset_of_U3CauthorU3Ek__BackingField_3(),
	Doc_t3160830270::get_offset_of_U3CbibliographyU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2663 = { sizeof (ClusterInfo_t4006997306), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2663[3] = 
{
	ClusterInfo_t4006997306::get_offset_of_U3CClusterU3Ek__BackingField_0(),
	ClusterInfo_t4006997306::get_offset_of_U3CConfigsU3Ek__BackingField_1(),
	ClusterInfo_t4006997306::get_offset_of_U3CCollectionsU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2664 = { sizeof (ListRankersPayload_t4087700440), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2664[1] = 
{
	ListRankersPayload_t4087700440::get_offset_of_U3CrankersU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2665 = { sizeof (RankerInfoPayload_t1776809245), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2665[4] = 
{
	RankerInfoPayload_t1776809245::get_offset_of_U3Cranker_idU3Ek__BackingField_0(),
	RankerInfoPayload_t1776809245::get_offset_of_U3CurlU3Ek__BackingField_1(),
	RankerInfoPayload_t1776809245::get_offset_of_U3CnameU3Ek__BackingField_2(),
	RankerInfoPayload_t1776809245::get_offset_of_U3CcreatedU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2666 = { sizeof (RankerStatusPayload_t2644736411), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2666[6] = 
{
	RankerStatusPayload_t2644736411::get_offset_of_U3Cranker_idU3Ek__BackingField_0(),
	RankerStatusPayload_t2644736411::get_offset_of_U3CurlU3Ek__BackingField_1(),
	RankerStatusPayload_t2644736411::get_offset_of_U3CnameU3Ek__BackingField_2(),
	RankerStatusPayload_t2644736411::get_offset_of_U3CcreatedU3Ek__BackingField_3(),
	RankerStatusPayload_t2644736411::get_offset_of_U3CstatusU3Ek__BackingField_4(),
	RankerStatusPayload_t2644736411::get_offset_of_U3Cstatus_descriptionU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2667 = { sizeof (RankerTrainingMetadataPayload_t1049631040), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2667[1] = 
{
	RankerTrainingMetadataPayload_t1049631040::get_offset_of_U3CnameU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2668 = { sizeof (RankerErrorResponsePayload_t2774706454), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2668[3] = 
{
	RankerErrorResponsePayload_t2774706454::get_offset_of_U3CcodeU3Ek__BackingField_0(),
	RankerErrorResponsePayload_t2774706454::get_offset_of_U3CerrorU3Ek__BackingField_1(),
	RankerErrorResponsePayload_t2774706454::get_offset_of_U3CdescriptionU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2669 = { sizeof (RankerOutputPayload_t247035662), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2669[4] = 
{
	RankerOutputPayload_t247035662::get_offset_of_U3Cranker_idU3Ek__BackingField_0(),
	RankerOutputPayload_t247035662::get_offset_of_U3CurlU3Ek__BackingField_1(),
	RankerOutputPayload_t247035662::get_offset_of_U3Ctop_answerU3Ek__BackingField_2(),
	RankerOutputPayload_t247035662::get_offset_of_U3CanswersU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2670 = { sizeof (RankedAnswer_t193891511), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2670[3] = 
{
	RankedAnswer_t193891511::get_offset_of_U3Canswer_idU3Ek__BackingField_0(),
	RankedAnswer_t193891511::get_offset_of_U3CscoreU3Ek__BackingField_1(),
	RankedAnswer_t193891511::get_offset_of_U3CconfidenceU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2671 = { sizeof (RetrieveAndRank_t1381045327), -1, sizeof(RetrieveAndRank_t1381045327_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2671[16] = 
{
	0,
	RetrieveAndRank_t1381045327_StaticFields::get_offset_of_sm_Serializer_1(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	RetrieveAndRank_t1381045327::get_offset_of_U3CLoadFileU3Ek__BackingField_14(),
	RetrieveAndRank_t1381045327::get_offset_of_U3CSaveFileU3Ek__BackingField_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2672 = { sizeof (LoadFileDelegate_t1720639326), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2673 = { sizeof (SaveFileDelegate_t3125227661), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2674 = { sizeof (OnGetClusters_t1915248271), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2675 = { sizeof (GetClustersRequest_t1824000023), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2675[2] = 
{
	GetClustersRequest_t1824000023::get_offset_of_U3CDataU3Ek__BackingField_11(),
	GetClustersRequest_t1824000023::get_offset_of_U3CCallbackU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2676 = { sizeof (OnCreateCluster_t1489102388), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2677 = { sizeof (CreateClusterRequest_t616269052), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2677[2] = 
{
	CreateClusterRequest_t616269052::get_offset_of_U3CDataU3Ek__BackingField_11(),
	CreateClusterRequest_t616269052::get_offset_of_U3CCallbackU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2678 = { sizeof (OnDeleteCluster_t1281715199), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2679 = { sizeof (DeleteClusterRequest_t929452623), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2679[3] = 
{
	DeleteClusterRequest_t929452623::get_offset_of_U3CDataU3Ek__BackingField_11(),
	DeleteClusterRequest_t929452623::get_offset_of_U3CClusterIDU3Ek__BackingField_12(),
	DeleteClusterRequest_t929452623::get_offset_of_U3CCallbackU3Ek__BackingField_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2680 = { sizeof (OnGetCluster_t3066553756), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2681 = { sizeof (GetClusterRequest_t52709226), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2681[3] = 
{
	GetClusterRequest_t52709226::get_offset_of_U3CDataU3Ek__BackingField_11(),
	GetClusterRequest_t52709226::get_offset_of_U3CClusterIDU3Ek__BackingField_12(),
	GetClusterRequest_t52709226::get_offset_of_U3CCallbackU3Ek__BackingField_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2682 = { sizeof (OnGetClusterConfigs_t22213349), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2683 = { sizeof (GetClusterConfigsRequest_t1411051725), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2683[3] = 
{
	GetClusterConfigsRequest_t1411051725::get_offset_of_U3CDataU3Ek__BackingField_11(),
	GetClusterConfigsRequest_t1411051725::get_offset_of_U3CClusterIDU3Ek__BackingField_12(),
	GetClusterConfigsRequest_t1411051725::get_offset_of_U3CCallbackU3Ek__BackingField_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2684 = { sizeof (OnDeleteClusterConfig_t2578936137), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2685 = { sizeof (DeleteClusterConfigRequest_t2137334505), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2685[4] = 
{
	DeleteClusterConfigRequest_t2137334505::get_offset_of_U3CDataU3Ek__BackingField_11(),
	DeleteClusterConfigRequest_t2137334505::get_offset_of_U3CClusterIDU3Ek__BackingField_12(),
	DeleteClusterConfigRequest_t2137334505::get_offset_of_U3CConfigIDU3Ek__BackingField_13(),
	DeleteClusterConfigRequest_t2137334505::get_offset_of_U3CCallbackU3Ek__BackingField_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2686 = { sizeof (OnGetClusterConfig_t1069230450), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2687 = { sizeof (GetClusterConfigRequest_t1264100768), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2687[4] = 
{
	GetClusterConfigRequest_t1264100768::get_offset_of_U3CDataU3Ek__BackingField_11(),
	GetClusterConfigRequest_t1264100768::get_offset_of_U3CClusterIDU3Ek__BackingField_12(),
	GetClusterConfigRequest_t1264100768::get_offset_of_U3CConfigNameU3Ek__BackingField_13(),
	GetClusterConfigRequest_t1264100768::get_offset_of_U3CCallbackU3Ek__BackingField_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2688 = { sizeof (OnSaveClusterConfig_t1862884805), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2689 = { sizeof (OnUploadClusterConfig_t589517099), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2690 = { sizeof (UploadClusterConfigRequest_t3639785499), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2690[4] = 
{
	UploadClusterConfigRequest_t3639785499::get_offset_of_U3CDataU3Ek__BackingField_11(),
	UploadClusterConfigRequest_t3639785499::get_offset_of_U3CClusterIDU3Ek__BackingField_12(),
	UploadClusterConfigRequest_t3639785499::get_offset_of_U3CConfigNameU3Ek__BackingField_13(),
	UploadClusterConfigRequest_t3639785499::get_offset_of_U3CCallbackU3Ek__BackingField_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2691 = { sizeof (OnCollections_t2275029757), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2692 = { sizeof (CollectionRequest_t2133486888), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2692[6] = 
{
	CollectionRequest_t2133486888::get_offset_of_U3CDataU3Ek__BackingField_11(),
	CollectionRequest_t2133486888::get_offset_of_U3CCallbackU3Ek__BackingField_12(),
	CollectionRequest_t2133486888::get_offset_of_U3CClusterIDU3Ek__BackingField_13(),
	CollectionRequest_t2133486888::get_offset_of_U3CActionU3Ek__BackingField_14(),
	CollectionRequest_t2133486888::get_offset_of_U3CCollectionNameU3Ek__BackingField_15(),
	CollectionRequest_t2133486888::get_offset_of_U3CConfigNameU3Ek__BackingField_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2693 = { sizeof (OnIndexDocuments_t1217349952), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2694 = { sizeof (IndexDocumentsRequest_t3942467882), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2694[5] = 
{
	IndexDocumentsRequest_t3942467882::get_offset_of_U3CDataU3Ek__BackingField_11(),
	IndexDocumentsRequest_t3942467882::get_offset_of_U3CIndexDataPathU3Ek__BackingField_12(),
	IndexDocumentsRequest_t3942467882::get_offset_of_U3CClusterIDU3Ek__BackingField_13(),
	IndexDocumentsRequest_t3942467882::get_offset_of_U3CCollectionNameU3Ek__BackingField_14(),
	IndexDocumentsRequest_t3942467882::get_offset_of_U3CCallbackU3Ek__BackingField_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2695 = { sizeof (OnSearch_t2575532994), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2696 = { sizeof (SearchRequest_t1893815408), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2696[6] = 
{
	SearchRequest_t1893815408::get_offset_of_U3CDataU3Ek__BackingField_11(),
	SearchRequest_t1893815408::get_offset_of_U3CClusterIDU3Ek__BackingField_12(),
	SearchRequest_t1893815408::get_offset_of_U3CCollectionNameU3Ek__BackingField_13(),
	SearchRequest_t1893815408::get_offset_of_U3CQueryU3Ek__BackingField_14(),
	SearchRequest_t1893815408::get_offset_of_U3CFlU3Ek__BackingField_15(),
	SearchRequest_t1893815408::get_offset_of_U3CCallbackU3Ek__BackingField_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2697 = { sizeof (OnGetRankers_t1731100556), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2698 = { sizeof (GetRankersRequest_t2633918666), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2698[2] = 
{
	GetRankersRequest_t2633918666::get_offset_of_U3CDataU3Ek__BackingField_11(),
	GetRankersRequest_t2633918666::get_offset_of_U3CCallbackU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2699 = { sizeof (OnCreateRanker_t496698577), sizeof(Il2CppMethodPointer), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
