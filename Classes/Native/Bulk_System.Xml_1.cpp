﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.Xml.XPath.XPathFunctionLang
struct XPathFunctionLang_t718178243;
// System.Xml.XPath.FunctionArguments
struct FunctionArguments_t2900945452;
// System.Xml.XPath.XPathFunction
struct XPathFunction_t759167395;
// System.Xml.XPath.XPathException
struct XPathException_t1503722168;
// System.String
struct String_t;
// System.Xml.XPath.Expression
struct Expression_t1283317256;
// System.Xml.XPath.BaseIterator
struct BaseIterator_t2454437973;
// System.Globalization.CultureInfo
struct CultureInfo_t3500843524;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.Xml.XPath.XPathFunctionLast
struct XPathFunctionLast_t2734600875;
// System.Xml.XPath.XPathFunctionLocalName
struct XPathFunctionLocalName_t3935594891;
// System.Xml.XPath.XPathFunctionName
struct XPathFunctionName_t1880977850;
// System.Xml.XPath.XPathFunctionNamespaceUri
struct XPathFunctionNamespaceUri_t448035136;
// System.Xml.XPath.XPathFunctionNormalizeSpace
struct XPathFunctionNormalizeSpace_t1288231156;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;
// System.Xml.XPath.XPathFunctionNot
struct XPathFunctionNot_t1631674504;
// System.Xml.XPath.XPathBooleanFunction
struct XPathBooleanFunction_t2511646183;
// System.Xml.XPath.XPathFunctionNumber
struct XPathFunctionNumber_t1210619362;
// System.Xml.XPath.XPathNumericFunction
struct XPathNumericFunction_t2367269690;
// System.Xml.XPath.ExprNumber
struct ExprNumber_t2687440122;
// System.Xml.XPath.XPathFunctionPosition
struct XPathFunctionPosition_t3711683936;
// System.Xml.XPath.XPathFunctionRound
struct XPathFunctionRound_t711107915;
// System.ArgumentNullException
struct ArgumentNullException_t628810857;
// System.ArgumentException
struct ArgumentException_t3259014390;
// System.Globalization.NumberFormatInfo
struct NumberFormatInfo_t104580544;
// System.IFormatProvider
struct IFormatProvider_t2849799027;
// System.Xml.XPath.XPathFunctionStartsWith
struct XPathFunctionStartsWith_t1855488236;
// System.Xml.XPath.XPathFunctionString
struct XPathFunctionString_t3963481524;
// System.Xml.XPath.XPathFunctionStringLength
struct XPathFunctionStringLength_t2821584396;
// System.Xml.XPath.XPathFunctionSubstring
struct XPathFunctionSubstring_t2124841904;
// System.Xml.XPath.XPathFunctionSubstringAfter
struct XPathFunctionSubstringAfter_t1560316700;
// System.Xml.XPath.XPathFunctionSubstringBefore
struct XPathFunctionSubstringBefore_t1797727619;
// System.Xml.XPath.XPathFunctionSum
struct XPathFunctionSum_t2438242634;
// System.Xml.XPath.XPathFunctionTranslate
struct XPathFunctionTranslate_t2720664953;
// System.Xml.XPath.XPathFunctionTrue
struct XPathFunctionTrue_t1672815575;
// System.Xml.XPath.XPathItem
struct XPathItem_t3130801258;
// System.Xml.XPath.XPathIteratorComparer
struct XPathIteratorComparer_t2522471076;
// System.Xml.XPath.XPathNavigator
struct XPathNavigator_t3981235968;
// System.Xml.XPath.XPathExpression
struct XPathExpression_t452251917;
// System.Xml.XPath.XPathNodeIterator
struct XPathNodeIterator_t3192332357;
// System.Xml.IXmlNamespaceResolver
struct IXmlNamespaceResolver_t3928241465;
// System.Xml.XPath.CompiledExpression
struct CompiledExpression_t3686330919;
// System.Xml.XPath.NullIterator
struct NullIterator_t2539636145;
// System.Collections.IEnumerable
struct IEnumerable_t2911409499;
// System.Xml.XPath.XPathNavigator/<EnumerateChildren>c__Iterator0
struct U3CEnumerateChildrenU3Ec__Iterator0_t1235609798;
// System.Xml.XPath.XPathNavigator/EnumerableIterator
struct EnumerableIterator_t1602910416;
// System.Xml.XPath.WrapperIterator
struct WrapperIterator_t2718786873;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;
// System.NotSupportedException
struct NotSupportedException_t1793819818;
// System.Xml.XPath.XPathNavigatorComparer
struct XPathNavigatorComparer_t2670709129;
// System.Xml.XPath.XPathNodeIterator/<GetEnumerator>c__Iterator2
struct U3CGetEnumeratorU3Ec__Iterator2_t959253998;
// System.Xml.XPath.XPathSortElement
struct XPathSortElement_t1040291071;
// System.Xml.XPath.XPathSorter
struct XPathSorter_t3491953490;
// System.Xml.XPath.XPathSorters
struct XPathSorters_t4019574815;
// System.Collections.ArrayList
struct ArrayList_t4252133567;
// System.Xml.XPath.ListIterator
struct ListIterator_t3804705054;
// System.Collections.IList
struct IList_t3321498491;
// System.Xml.Xsl.XsltContext
struct XsltContext_t2013960098;
// System.Xml.Xsl.IXsltContextVariable
struct IXsltContextVariable_t3385767465;
// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t1944712516;
// System.Xml.Xsl.IXsltContextFunction
struct IXsltContextFunction_t1467867933;
// System.Xml.XPath.XPathResultType[]
struct XPathResultTypeU5BU5D_t2966113519;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.UInt32[]
struct UInt32U5BU5D_t59386216;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Xml.XmlNamespaceManager/NsDecl[]
struct NsDeclU5BU5D_t228542166;
// System.Xml.XmlNameTable
struct XmlNameTable_t1345805268;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.IntPtr[]
struct IntPtrU5BU5D_t169632028;
// System.Collections.IDictionary
struct IDictionary_t596158605;
// System.Globalization.DateTimeFormatInfo
struct DateTimeFormatInfo_t2187473504;
// System.Globalization.TextInfo
struct TextInfo_t3620182823;
// System.Globalization.CompareInfo
struct CompareInfo_t2310920157;
// System.Globalization.Calendar[]
struct CalendarU5BU5D_t3654442685;
// System.Globalization.Calendar
struct Calendar_t585061108;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.Int32
struct Int32_t2071877448;
// System.Void
struct Void_t1841601450;
// System.Byte
struct Byte_t3683104436;
// System.Double
struct Double_t4078015681;
// System.UInt16
struct UInt16_t986882611;
// System.Collections.IComparer
struct IComparer_t3952557350;

extern RuntimeClass* XPathException_t1503722168_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1550245230;
extern const uint32_t XPathFunctionLang__ctor_m2307304492_MetadataUsageId;
extern RuntimeClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctionLang_Evaluate_m4182185167_MetadataUsageId;
extern RuntimeClass* CultureInfo_t3500843524_il2cpp_TypeInfo_var;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern RuntimeClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctionLang_EvaluateBoolean_m258778298_MetadataUsageId;
extern RuntimeClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1352971294;
extern Il2CppCodeGenString* _stringLiteral372029317;
extern const uint32_t XPathFunctionLang_ToString_m4056273282_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3500109105;
extern const uint32_t XPathFunctionLast__ctor_m2403937296_MetadataUsageId;
extern RuntimeClass* Double_t4078015681_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctionLast_Evaluate_m3244523513_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral4274021071;
extern const uint32_t XPathFunctionLast_ToString_m3535557470_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral4037561126;
extern const uint32_t XPathFunctionLocalName__ctor_m1211626624_MetadataUsageId;
extern const uint32_t XPathFunctionLocalName_Evaluate_m1416622637_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2822356663;
extern const uint32_t XPathFunctionLocalName_ToString_m2774210428_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2424985204;
extern const uint32_t XPathFunctionName__ctor_m429788115_MetadataUsageId;
extern const uint32_t XPathFunctionName_Evaluate_m1216738966_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2515777075;
extern const uint32_t XPathFunctionName_ToString_m973448361_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral790676781;
extern const uint32_t XPathFunctionNamespaceUri__ctor_m1544480789_MetadataUsageId;
extern const uint32_t XPathFunctionNamespaceUri_Evaluate_m388318582_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1879196646;
extern const uint32_t XPathFunctionNamespaceUri_ToString_m2211506483_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2744585421;
extern const uint32_t XPathFunctionNormalizeSpace__ctor_m3022371881_MetadataUsageId;
extern RuntimeClass* StringBuilder_t1221177846_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctionNormalizeSpace_Evaluate_m4131070162_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2732366470;
extern const uint32_t XPathFunctionNormalizeSpace_ToString_m383064191_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2157221977;
extern const uint32_t XPathFunctionNot__ctor_m3119931389_MetadataUsageId;
extern const uint32_t XPathFunctionNot_Evaluate_m2475008982_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1084865993;
extern const uint32_t XPathFunctionNot_ToString_m3232938039_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2993931250;
extern const uint32_t XPathFunctionNumber__ctor_m2280431139_MetadataUsageId;
extern RuntimeClass* ExprNumber_t2687440122_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctionNumber_Optimize_m2168727705_MetadataUsageId;
extern const uint32_t XPathFunctionNumber_Evaluate_m3881007260_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral4042911851;
extern const uint32_t XPathFunctionNumber_ToString_m841233809_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3441363790;
extern const uint32_t XPathFunctionPosition__ctor_m3434234893_MetadataUsageId;
extern const uint32_t XPathFunctionPosition_Evaluate_m1355803762_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1287036886;
extern const uint32_t XPathFunctionPosition_ToString_m1517479791_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral586463890;
extern const uint32_t XPathFunctionRound__ctor_m2903590640_MetadataUsageId;
extern const uint32_t XPathFunctionRound_Evaluate_m1674023549_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3236332770;
extern const uint32_t XPathFunctionRound_ToString_m4105471924_MetadataUsageId;
extern RuntimeClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern RuntimeClass* XPathNodeIterator_t3192332357_il2cpp_TypeInfo_var;
extern RuntimeClass* XPathNavigator_t3981235968_il2cpp_TypeInfo_var;
extern RuntimeClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctions_ToBoolean_m2866337351_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3323263070;
extern Il2CppCodeGenString* _stringLiteral2609877245;
extern const uint32_t XPathFunctions_ToString_m908387119_MetadataUsageId;
extern RuntimeClass* NumberFormatInfo_t104580544_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral55341327;
extern Il2CppCodeGenString* _stringLiteral487594890;
extern Il2CppCodeGenString* _stringLiteral372029424;
extern const uint32_t XPathFunctions_ToString_m2724627955_MetadataUsageId;
extern RuntimeClass* BaseIterator_t2454437973_il2cpp_TypeInfo_var;
extern RuntimeClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctions_ToNumber_m2010435277_MetadataUsageId;
extern RuntimeClass* XmlChar_t1369421061_il2cpp_TypeInfo_var;
extern RuntimeClass* Char_t3454481338_il2cpp_TypeInfo_var;
extern RuntimeClass* OverflowException_t1075868493_il2cpp_TypeInfo_var;
extern RuntimeClass* FormatException_t2948921286_il2cpp_TypeInfo_var;
extern const uint32_t XPathFunctions_ToNumber_m2754819889_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1938299717;
extern const uint32_t XPathFunctionStartsWith__ctor_m2733142657_MetadataUsageId;
extern const uint32_t XPathFunctionStartsWith_Evaluate_m1045380392_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral247280470;
extern Il2CppCodeGenString* _stringLiteral372029314;
extern const uint32_t XPathFunctionStartsWith_ToString_m3285628411_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2614059802;
extern const uint32_t XPathFunctionString__ctor_m408009393_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral910930429;
extern const uint32_t XPathFunctionString_ToString_m3904874255_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1405175745;
extern const uint32_t XPathFunctionStringLength__ctor_m4250833153_MetadataUsageId;
extern const uint32_t XPathFunctionStringLength_Evaluate_m1453954152_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1827829842;
extern const uint32_t XPathFunctionStringLength_ToString_m1068025283_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2290278570;
extern const uint32_t XPathFunctionSubstring__ctor_m159311037_MetadataUsageId;
extern const uint32_t XPathFunctionSubstring_Evaluate_m1994297472_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral808858977;
extern const uint32_t XPathFunctionSubstring_ToString_m282664695_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral903430261;
extern const uint32_t XPathFunctionSubstringAfter__ctor_m4176727017_MetadataUsageId;
extern const uint32_t XPathFunctionSubstringAfter_Evaluate_m2110056486_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1134393990;
extern const uint32_t XPathFunctionSubstringAfter_ToString_m2115706811_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2870964214;
extern const uint32_t XPathFunctionSubstringBefore__ctor_m3051703460_MetadataUsageId;
extern const uint32_t XPathFunctionSubstringBefore_Evaluate_m366996147_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2049758785;
extern const uint32_t XPathFunctionSubstringBefore_ToString_m2067029356_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral4210209539;
extern const uint32_t XPathFunctionSum__ctor_m104079019_MetadataUsageId;
extern const uint32_t XPathFunctionSum_Evaluate_m458405960_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1219380691;
extern const uint32_t XPathFunctionSum_ToString_m108877281_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2932583186;
extern const uint32_t XPathFunctionTranslate__ctor_m1312115182_MetadataUsageId;
extern const uint32_t XPathFunctionTranslate_Evaluate_m944986293_MetadataUsageId;
extern const uint32_t XPathFunctionTranslate_ToString_m1307664662_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2583897385;
extern const uint32_t XPathFunctionTrue__ctor_m3471779148_MetadataUsageId;
extern const uint32_t XPathFunctionTrue_Evaluate_m164285465_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2078431091;
extern const uint32_t XPathFunctionTrue_ToString_m1131923630_MetadataUsageId;
extern RuntimeClass* XPathIteratorComparer_t2522471076_il2cpp_TypeInfo_var;
extern const uint32_t XPathIteratorComparer__cctor_m1438945887_MetadataUsageId;
extern const uint32_t XPathIteratorComparer_Compare_m2647634021_MetadataUsageId;
extern RuntimeField* U3CPrivateImplementationDetailsU3E_t1486305141____U24U24fieldU2D18_9_FieldInfo_var;
extern const uint32_t XPathNavigator__cctor_m1668901011_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1165419350;
extern Il2CppCodeGenString* _stringLiteral3939301371;
extern const uint32_t XPathNavigator_get_XmlLang_m1158727745_MetadataUsageId;
extern const uint32_t XPathNavigator_MoveToAttribute_m3716628652_MetadataUsageId;
extern const uint32_t XPathNavigator_MoveToNamespace_m419780955_MetadataUsageId;
extern RuntimeClass* CompiledExpression_t3686330919_il2cpp_TypeInfo_var;
extern RuntimeClass* NullIterator_t2539636145_il2cpp_TypeInfo_var;
extern const uint32_t XPathNavigator_Select_m2045522580_MetadataUsageId;
extern RuntimeClass* U3CEnumerateChildrenU3Ec__Iterator0_t1235609798_il2cpp_TypeInfo_var;
extern const uint32_t XPathNavigator_EnumerateChildren_m1107095336_MetadataUsageId;
extern RuntimeClass* EnumerableIterator_t1602910416_il2cpp_TypeInfo_var;
extern RuntimeClass* WrapperIterator_t2718786873_il2cpp_TypeInfo_var;
extern const uint32_t XPathNavigator_SelectChildren_m4089331250_MetadataUsageId;
extern const uint32_t U3CEnumerateChildrenU3Ec__Iterator0_System_Collections_Generic_IEnumerableU3CobjectU3E_GetEnumerator_m1191949246_MetadataUsageId;
extern RuntimeClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CEnumerateChildrenU3Ec__Iterator0_Reset_m3144178776_MetadataUsageId;
extern const uint32_t EnumerableIterator_Clone_m4268561691_MetadataUsageId;
extern RuntimeClass* IEnumerable_t2911409499_il2cpp_TypeInfo_var;
extern RuntimeClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern const uint32_t EnumerableIterator_MoveNext_m2512854753_MetadataUsageId;
extern const uint32_t EnumerableIterator_get_Current_m1712718357_MetadataUsageId;
extern RuntimeClass* XPathNavigatorComparer_t2670709129_il2cpp_TypeInfo_var;
extern const uint32_t XPathNavigatorComparer__cctor_m240886484_MetadataUsageId;
extern const uint32_t XPathNavigatorComparer_System_Collections_IEqualityComparer_Equals_m1841324898_MetadataUsageId;
extern const uint32_t XPathNavigatorComparer_Compare_m950798190_MetadataUsageId;
extern RuntimeClass* U3CGetEnumeratorU3Ec__Iterator2_t959253998_il2cpp_TypeInfo_var;
extern const uint32_t XPathNodeIterator_GetEnumerator_m1555209557_MetadataUsageId;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator2_Reset_m3035138684_MetadataUsageId;
extern const uint32_t XPathNumericFunction_get_StaticValue_m766662601_MetadataUsageId;
extern const uint32_t XPathSorter_Evaluate_m4282430526_MetadataUsageId;
extern RuntimeClass* IComparer_t3952557350_il2cpp_TypeInfo_var;
extern const uint32_t XPathSorter_Compare_m1708989355_MetadataUsageId;
extern RuntimeClass* XPathSortElement_t1040291071_il2cpp_TypeInfo_var;
extern RuntimeClass* XPathSorter_t3491953490_il2cpp_TypeInfo_var;
extern const uint32_t XPathSorters_System_Collections_IComparer_Compare_m845991242_MetadataUsageId;
extern RuntimeClass* ArrayList_t4252133567_il2cpp_TypeInfo_var;
extern RuntimeClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern const uint32_t XPathSorters_ToSortElementList_m407179934_MetadataUsageId;
extern RuntimeClass* XPathNavigatorU5BU5D_t2529815809_il2cpp_TypeInfo_var;
extern RuntimeClass* ListIterator_t3804705054_il2cpp_TypeInfo_var;
extern const uint32_t XPathSorters_Sort_m3271859196_MetadataUsageId;

struct CharU5BU5D_t1328083999;
struct StringU5BU5D_t1642385972;
struct ObjectU5BU5D_t3614634134;
struct XPathNavigatorU5BU5D_t2529815809;
struct XPathResultTypeU5BU5D_t2966113519;


#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef XMLCHAR_T1369421061_H
#define XMLCHAR_T1369421061_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlChar
struct  XmlChar_t1369421061  : public RuntimeObject
{
public:

public:
};

struct XmlChar_t1369421061_StaticFields
{
public:
	// System.Char[] System.Xml.XmlChar::WhitespaceChars
	CharU5BU5D_t1328083999* ___WhitespaceChars_0;
	// System.Byte[] System.Xml.XmlChar::firstNamePages
	ByteU5BU5D_t3397334013* ___firstNamePages_1;
	// System.Byte[] System.Xml.XmlChar::namePages
	ByteU5BU5D_t3397334013* ___namePages_2;
	// System.UInt32[] System.Xml.XmlChar::nameBitmap
	UInt32U5BU5D_t59386216* ___nameBitmap_3;

public:
	inline static int32_t get_offset_of_WhitespaceChars_0() { return static_cast<int32_t>(offsetof(XmlChar_t1369421061_StaticFields, ___WhitespaceChars_0)); }
	inline CharU5BU5D_t1328083999* get_WhitespaceChars_0() const { return ___WhitespaceChars_0; }
	inline CharU5BU5D_t1328083999** get_address_of_WhitespaceChars_0() { return &___WhitespaceChars_0; }
	inline void set_WhitespaceChars_0(CharU5BU5D_t1328083999* value)
	{
		___WhitespaceChars_0 = value;
		Il2CppCodeGenWriteBarrier((&___WhitespaceChars_0), value);
	}

	inline static int32_t get_offset_of_firstNamePages_1() { return static_cast<int32_t>(offsetof(XmlChar_t1369421061_StaticFields, ___firstNamePages_1)); }
	inline ByteU5BU5D_t3397334013* get_firstNamePages_1() const { return ___firstNamePages_1; }
	inline ByteU5BU5D_t3397334013** get_address_of_firstNamePages_1() { return &___firstNamePages_1; }
	inline void set_firstNamePages_1(ByteU5BU5D_t3397334013* value)
	{
		___firstNamePages_1 = value;
		Il2CppCodeGenWriteBarrier((&___firstNamePages_1), value);
	}

	inline static int32_t get_offset_of_namePages_2() { return static_cast<int32_t>(offsetof(XmlChar_t1369421061_StaticFields, ___namePages_2)); }
	inline ByteU5BU5D_t3397334013* get_namePages_2() const { return ___namePages_2; }
	inline ByteU5BU5D_t3397334013** get_address_of_namePages_2() { return &___namePages_2; }
	inline void set_namePages_2(ByteU5BU5D_t3397334013* value)
	{
		___namePages_2 = value;
		Il2CppCodeGenWriteBarrier((&___namePages_2), value);
	}

	inline static int32_t get_offset_of_nameBitmap_3() { return static_cast<int32_t>(offsetof(XmlChar_t1369421061_StaticFields, ___nameBitmap_3)); }
	inline UInt32U5BU5D_t59386216* get_nameBitmap_3() const { return ___nameBitmap_3; }
	inline UInt32U5BU5D_t59386216** get_address_of_nameBitmap_3() { return &___nameBitmap_3; }
	inline void set_nameBitmap_3(UInt32U5BU5D_t59386216* value)
	{
		___nameBitmap_3 = value;
		Il2CppCodeGenWriteBarrier((&___nameBitmap_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLCHAR_T1369421061_H
#ifndef ARRAYLIST_T4252133567_H
#define ARRAYLIST_T4252133567_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.ArrayList
struct  ArrayList_t4252133567  : public RuntimeObject
{
public:
	// System.Int32 System.Collections.ArrayList::_size
	int32_t ____size_1;
	// System.Object[] System.Collections.ArrayList::_items
	ObjectU5BU5D_t3614634134* ____items_2;
	// System.Int32 System.Collections.ArrayList::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__size_1() { return static_cast<int32_t>(offsetof(ArrayList_t4252133567, ____size_1)); }
	inline int32_t get__size_1() const { return ____size_1; }
	inline int32_t* get_address_of__size_1() { return &____size_1; }
	inline void set__size_1(int32_t value)
	{
		____size_1 = value;
	}

	inline static int32_t get_offset_of__items_2() { return static_cast<int32_t>(offsetof(ArrayList_t4252133567, ____items_2)); }
	inline ObjectU5BU5D_t3614634134* get__items_2() const { return ____items_2; }
	inline ObjectU5BU5D_t3614634134** get_address_of__items_2() { return &____items_2; }
	inline void set__items_2(ObjectU5BU5D_t3614634134* value)
	{
		____items_2 = value;
		Il2CppCodeGenWriteBarrier((&____items_2), value);
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(ArrayList_t4252133567, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct ArrayList_t4252133567_StaticFields
{
public:
	// System.Object[] System.Collections.ArrayList::EmptyArray
	ObjectU5BU5D_t3614634134* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(ArrayList_t4252133567_StaticFields, ___EmptyArray_4)); }
	inline ObjectU5BU5D_t3614634134* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline ObjectU5BU5D_t3614634134** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(ObjectU5BU5D_t3614634134* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRAYLIST_T4252133567_H
#ifndef XPATHSORTERS_T4019574815_H
#define XPATHSORTERS_T4019574815_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathSorters
struct  XPathSorters_t4019574815  : public RuntimeObject
{
public:
	// System.Collections.ArrayList System.Xml.XPath.XPathSorters::_rgSorters
	ArrayList_t4252133567 * ____rgSorters_0;

public:
	inline static int32_t get_offset_of__rgSorters_0() { return static_cast<int32_t>(offsetof(XPathSorters_t4019574815, ____rgSorters_0)); }
	inline ArrayList_t4252133567 * get__rgSorters_0() const { return ____rgSorters_0; }
	inline ArrayList_t4252133567 ** get_address_of__rgSorters_0() { return &____rgSorters_0; }
	inline void set__rgSorters_0(ArrayList_t4252133567 * value)
	{
		____rgSorters_0 = value;
		Il2CppCodeGenWriteBarrier((&____rgSorters_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHSORTERS_T4019574815_H
#ifndef XPATHNAVIGATORCOMPARER_T2670709129_H
#define XPATHNAVIGATORCOMPARER_T2670709129_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathNavigatorComparer
struct  XPathNavigatorComparer_t2670709129  : public RuntimeObject
{
public:

public:
};

struct XPathNavigatorComparer_t2670709129_StaticFields
{
public:
	// System.Xml.XPath.XPathNavigatorComparer System.Xml.XPath.XPathNavigatorComparer::Instance
	XPathNavigatorComparer_t2670709129 * ___Instance_0;

public:
	inline static int32_t get_offset_of_Instance_0() { return static_cast<int32_t>(offsetof(XPathNavigatorComparer_t2670709129_StaticFields, ___Instance_0)); }
	inline XPathNavigatorComparer_t2670709129 * get_Instance_0() const { return ___Instance_0; }
	inline XPathNavigatorComparer_t2670709129 ** get_address_of_Instance_0() { return &___Instance_0; }
	inline void set_Instance_0(XPathNavigatorComparer_t2670709129 * value)
	{
		___Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHNAVIGATORCOMPARER_T2670709129_H
#ifndef U3CGETENUMERATORU3EC__ITERATOR2_T959253998_H
#define U3CGETENUMERATORU3EC__ITERATOR2_T959253998_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathNodeIterator/<GetEnumerator>c__Iterator2
struct  U3CGetEnumeratorU3Ec__Iterator2_t959253998  : public RuntimeObject
{
public:
	// System.Int32 System.Xml.XPath.XPathNodeIterator/<GetEnumerator>c__Iterator2::$PC
	int32_t ___U24PC_0;
	// System.Object System.Xml.XPath.XPathNodeIterator/<GetEnumerator>c__Iterator2::$current
	RuntimeObject * ___U24current_1;
	// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.XPathNodeIterator/<GetEnumerator>c__Iterator2::<>f__this
	XPathNodeIterator_t3192332357 * ___U3CU3Ef__this_2;

public:
	inline static int32_t get_offset_of_U24PC_0() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator2_t959253998, ___U24PC_0)); }
	inline int32_t get_U24PC_0() const { return ___U24PC_0; }
	inline int32_t* get_address_of_U24PC_0() { return &___U24PC_0; }
	inline void set_U24PC_0(int32_t value)
	{
		___U24PC_0 = value;
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator2_t959253998, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_2() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator2_t959253998, ___U3CU3Ef__this_2)); }
	inline XPathNodeIterator_t3192332357 * get_U3CU3Ef__this_2() const { return ___U3CU3Ef__this_2; }
	inline XPathNodeIterator_t3192332357 ** get_address_of_U3CU3Ef__this_2() { return &___U3CU3Ef__this_2; }
	inline void set_U3CU3Ef__this_2(XPathNodeIterator_t3192332357 * value)
	{
		___U3CU3Ef__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETENUMERATORU3EC__ITERATOR2_T959253998_H
#ifndef XPATHSORTELEMENT_T1040291071_H
#define XPATHSORTELEMENT_T1040291071_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathSortElement
struct  XPathSortElement_t1040291071  : public RuntimeObject
{
public:
	// System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathSortElement::Navigator
	XPathNavigator_t3981235968 * ___Navigator_0;
	// System.Object[] System.Xml.XPath.XPathSortElement::Values
	ObjectU5BU5D_t3614634134* ___Values_1;

public:
	inline static int32_t get_offset_of_Navigator_0() { return static_cast<int32_t>(offsetof(XPathSortElement_t1040291071, ___Navigator_0)); }
	inline XPathNavigator_t3981235968 * get_Navigator_0() const { return ___Navigator_0; }
	inline XPathNavigator_t3981235968 ** get_address_of_Navigator_0() { return &___Navigator_0; }
	inline void set_Navigator_0(XPathNavigator_t3981235968 * value)
	{
		___Navigator_0 = value;
		Il2CppCodeGenWriteBarrier((&___Navigator_0), value);
	}

	inline static int32_t get_offset_of_Values_1() { return static_cast<int32_t>(offsetof(XPathSortElement_t1040291071, ___Values_1)); }
	inline ObjectU5BU5D_t3614634134* get_Values_1() const { return ___Values_1; }
	inline ObjectU5BU5D_t3614634134** get_address_of_Values_1() { return &___Values_1; }
	inline void set_Values_1(ObjectU5BU5D_t3614634134* value)
	{
		___Values_1 = value;
		Il2CppCodeGenWriteBarrier((&___Values_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHSORTELEMENT_T1040291071_H
#ifndef XMLNAMESPACEMANAGER_T486731501_H
#define XMLNAMESPACEMANAGER_T486731501_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNamespaceManager
struct  XmlNamespaceManager_t486731501  : public RuntimeObject
{
public:
	// System.Xml.XmlNamespaceManager/NsDecl[] System.Xml.XmlNamespaceManager::decls
	NsDeclU5BU5D_t228542166* ___decls_0;
	// System.Int32 System.Xml.XmlNamespaceManager::declPos
	int32_t ___declPos_1;
	// System.String System.Xml.XmlNamespaceManager::defaultNamespace
	String_t* ___defaultNamespace_2;
	// System.Xml.XmlNameTable System.Xml.XmlNamespaceManager::nameTable
	XmlNameTable_t1345805268 * ___nameTable_3;
	// System.Boolean System.Xml.XmlNamespaceManager::internalAtomizedNames
	bool ___internalAtomizedNames_4;

public:
	inline static int32_t get_offset_of_decls_0() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t486731501, ___decls_0)); }
	inline NsDeclU5BU5D_t228542166* get_decls_0() const { return ___decls_0; }
	inline NsDeclU5BU5D_t228542166** get_address_of_decls_0() { return &___decls_0; }
	inline void set_decls_0(NsDeclU5BU5D_t228542166* value)
	{
		___decls_0 = value;
		Il2CppCodeGenWriteBarrier((&___decls_0), value);
	}

	inline static int32_t get_offset_of_declPos_1() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t486731501, ___declPos_1)); }
	inline int32_t get_declPos_1() const { return ___declPos_1; }
	inline int32_t* get_address_of_declPos_1() { return &___declPos_1; }
	inline void set_declPos_1(int32_t value)
	{
		___declPos_1 = value;
	}

	inline static int32_t get_offset_of_defaultNamespace_2() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t486731501, ___defaultNamespace_2)); }
	inline String_t* get_defaultNamespace_2() const { return ___defaultNamespace_2; }
	inline String_t** get_address_of_defaultNamespace_2() { return &___defaultNamespace_2; }
	inline void set_defaultNamespace_2(String_t* value)
	{
		___defaultNamespace_2 = value;
		Il2CppCodeGenWriteBarrier((&___defaultNamespace_2), value);
	}

	inline static int32_t get_offset_of_nameTable_3() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t486731501, ___nameTable_3)); }
	inline XmlNameTable_t1345805268 * get_nameTable_3() const { return ___nameTable_3; }
	inline XmlNameTable_t1345805268 ** get_address_of_nameTable_3() { return &___nameTable_3; }
	inline void set_nameTable_3(XmlNameTable_t1345805268 * value)
	{
		___nameTable_3 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_3), value);
	}

	inline static int32_t get_offset_of_internalAtomizedNames_4() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t486731501, ___internalAtomizedNames_4)); }
	inline bool get_internalAtomizedNames_4() const { return ___internalAtomizedNames_4; }
	inline bool* get_address_of_internalAtomizedNames_4() { return &___internalAtomizedNames_4; }
	inline void set_internalAtomizedNames_4(bool value)
	{
		___internalAtomizedNames_4 = value;
	}
};

struct XmlNamespaceManager_t486731501_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.XmlNamespaceManager::<>f__switch$map28
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map28_5;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map28_5() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t486731501_StaticFields, ___U3CU3Ef__switchU24map28_5)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map28_5() const { return ___U3CU3Ef__switchU24map28_5; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map28_5() { return &___U3CU3Ef__switchU24map28_5; }
	inline void set_U3CU3Ef__switchU24map28_5(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map28_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map28_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNAMESPACEMANAGER_T486731501_H
#ifndef XPATHFUNCTIONS_T976633782_H
#define XPATHFUNCTIONS_T976633782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathFunctions
struct  XPathFunctions_t976633782  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHFUNCTIONS_T976633782_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef XPATHITEM_T3130801258_H
#define XPATHITEM_T3130801258_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathItem
struct  XPathItem_t3130801258  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHITEM_T3130801258_H
#ifndef NUMBERFORMATINFO_T104580544_H
#define NUMBERFORMATINFO_T104580544_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Globalization.NumberFormatInfo
struct  NumberFormatInfo_t104580544  : public RuntimeObject
{
public:
	// System.Boolean System.Globalization.NumberFormatInfo::isReadOnly
	bool ___isReadOnly_0;
	// System.String System.Globalization.NumberFormatInfo::decimalFormats
	String_t* ___decimalFormats_1;
	// System.String System.Globalization.NumberFormatInfo::currencyFormats
	String_t* ___currencyFormats_2;
	// System.String System.Globalization.NumberFormatInfo::percentFormats
	String_t* ___percentFormats_3;
	// System.String System.Globalization.NumberFormatInfo::digitPattern
	String_t* ___digitPattern_4;
	// System.String System.Globalization.NumberFormatInfo::zeroPattern
	String_t* ___zeroPattern_5;
	// System.Int32 System.Globalization.NumberFormatInfo::currencyDecimalDigits
	int32_t ___currencyDecimalDigits_6;
	// System.String System.Globalization.NumberFormatInfo::currencyDecimalSeparator
	String_t* ___currencyDecimalSeparator_7;
	// System.String System.Globalization.NumberFormatInfo::currencyGroupSeparator
	String_t* ___currencyGroupSeparator_8;
	// System.Int32[] System.Globalization.NumberFormatInfo::currencyGroupSizes
	Int32U5BU5D_t3030399641* ___currencyGroupSizes_9;
	// System.Int32 System.Globalization.NumberFormatInfo::currencyNegativePattern
	int32_t ___currencyNegativePattern_10;
	// System.Int32 System.Globalization.NumberFormatInfo::currencyPositivePattern
	int32_t ___currencyPositivePattern_11;
	// System.String System.Globalization.NumberFormatInfo::currencySymbol
	String_t* ___currencySymbol_12;
	// System.String System.Globalization.NumberFormatInfo::nanSymbol
	String_t* ___nanSymbol_13;
	// System.String System.Globalization.NumberFormatInfo::negativeInfinitySymbol
	String_t* ___negativeInfinitySymbol_14;
	// System.String System.Globalization.NumberFormatInfo::negativeSign
	String_t* ___negativeSign_15;
	// System.Int32 System.Globalization.NumberFormatInfo::numberDecimalDigits
	int32_t ___numberDecimalDigits_16;
	// System.String System.Globalization.NumberFormatInfo::numberDecimalSeparator
	String_t* ___numberDecimalSeparator_17;
	// System.String System.Globalization.NumberFormatInfo::numberGroupSeparator
	String_t* ___numberGroupSeparator_18;
	// System.Int32[] System.Globalization.NumberFormatInfo::numberGroupSizes
	Int32U5BU5D_t3030399641* ___numberGroupSizes_19;
	// System.Int32 System.Globalization.NumberFormatInfo::numberNegativePattern
	int32_t ___numberNegativePattern_20;
	// System.Int32 System.Globalization.NumberFormatInfo::percentDecimalDigits
	int32_t ___percentDecimalDigits_21;
	// System.String System.Globalization.NumberFormatInfo::percentDecimalSeparator
	String_t* ___percentDecimalSeparator_22;
	// System.String System.Globalization.NumberFormatInfo::percentGroupSeparator
	String_t* ___percentGroupSeparator_23;
	// System.Int32[] System.Globalization.NumberFormatInfo::percentGroupSizes
	Int32U5BU5D_t3030399641* ___percentGroupSizes_24;
	// System.Int32 System.Globalization.NumberFormatInfo::percentNegativePattern
	int32_t ___percentNegativePattern_25;
	// System.Int32 System.Globalization.NumberFormatInfo::percentPositivePattern
	int32_t ___percentPositivePattern_26;
	// System.String System.Globalization.NumberFormatInfo::percentSymbol
	String_t* ___percentSymbol_27;
	// System.String System.Globalization.NumberFormatInfo::perMilleSymbol
	String_t* ___perMilleSymbol_28;
	// System.String System.Globalization.NumberFormatInfo::positiveInfinitySymbol
	String_t* ___positiveInfinitySymbol_29;
	// System.String System.Globalization.NumberFormatInfo::positiveSign
	String_t* ___positiveSign_30;
	// System.String System.Globalization.NumberFormatInfo::ansiCurrencySymbol
	String_t* ___ansiCurrencySymbol_31;
	// System.Int32 System.Globalization.NumberFormatInfo::m_dataItem
	int32_t ___m_dataItem_32;
	// System.Boolean System.Globalization.NumberFormatInfo::m_useUserOverride
	bool ___m_useUserOverride_33;
	// System.Boolean System.Globalization.NumberFormatInfo::validForParseAsNumber
	bool ___validForParseAsNumber_34;
	// System.Boolean System.Globalization.NumberFormatInfo::validForParseAsCurrency
	bool ___validForParseAsCurrency_35;
	// System.String[] System.Globalization.NumberFormatInfo::nativeDigits
	StringU5BU5D_t1642385972* ___nativeDigits_36;
	// System.Int32 System.Globalization.NumberFormatInfo::digitSubstitution
	int32_t ___digitSubstitution_37;

public:
	inline static int32_t get_offset_of_isReadOnly_0() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t104580544, ___isReadOnly_0)); }
	inline bool get_isReadOnly_0() const { return ___isReadOnly_0; }
	inline bool* get_address_of_isReadOnly_0() { return &___isReadOnly_0; }
	inline void set_isReadOnly_0(bool value)
	{
		___isReadOnly_0 = value;
	}

	inline static int32_t get_offset_of_decimalFormats_1() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t104580544, ___decimalFormats_1)); }
	inline String_t* get_decimalFormats_1() const { return ___decimalFormats_1; }
	inline String_t** get_address_of_decimalFormats_1() { return &___decimalFormats_1; }
	inline void set_decimalFormats_1(String_t* value)
	{
		___decimalFormats_1 = value;
		Il2CppCodeGenWriteBarrier((&___decimalFormats_1), value);
	}

	inline static int32_t get_offset_of_currencyFormats_2() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t104580544, ___currencyFormats_2)); }
	inline String_t* get_currencyFormats_2() const { return ___currencyFormats_2; }
	inline String_t** get_address_of_currencyFormats_2() { return &___currencyFormats_2; }
	inline void set_currencyFormats_2(String_t* value)
	{
		___currencyFormats_2 = value;
		Il2CppCodeGenWriteBarrier((&___currencyFormats_2), value);
	}

	inline static int32_t get_offset_of_percentFormats_3() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t104580544, ___percentFormats_3)); }
	inline String_t* get_percentFormats_3() const { return ___percentFormats_3; }
	inline String_t** get_address_of_percentFormats_3() { return &___percentFormats_3; }
	inline void set_percentFormats_3(String_t* value)
	{
		___percentFormats_3 = value;
		Il2CppCodeGenWriteBarrier((&___percentFormats_3), value);
	}

	inline static int32_t get_offset_of_digitPattern_4() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t104580544, ___digitPattern_4)); }
	inline String_t* get_digitPattern_4() const { return ___digitPattern_4; }
	inline String_t** get_address_of_digitPattern_4() { return &___digitPattern_4; }
	inline void set_digitPattern_4(String_t* value)
	{
		___digitPattern_4 = value;
		Il2CppCodeGenWriteBarrier((&___digitPattern_4), value);
	}

	inline static int32_t get_offset_of_zeroPattern_5() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t104580544, ___zeroPattern_5)); }
	inline String_t* get_zeroPattern_5() const { return ___zeroPattern_5; }
	inline String_t** get_address_of_zeroPattern_5() { return &___zeroPattern_5; }
	inline void set_zeroPattern_5(String_t* value)
	{
		___zeroPattern_5 = value;
		Il2CppCodeGenWriteBarrier((&___zeroPattern_5), value);
	}

	inline static int32_t get_offset_of_currencyDecimalDigits_6() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t104580544, ___currencyDecimalDigits_6)); }
	inline int32_t get_currencyDecimalDigits_6() const { return ___currencyDecimalDigits_6; }
	inline int32_t* get_address_of_currencyDecimalDigits_6() { return &___currencyDecimalDigits_6; }
	inline void set_currencyDecimalDigits_6(int32_t value)
	{
		___currencyDecimalDigits_6 = value;
	}

	inline static int32_t get_offset_of_currencyDecimalSeparator_7() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t104580544, ___currencyDecimalSeparator_7)); }
	inline String_t* get_currencyDecimalSeparator_7() const { return ___currencyDecimalSeparator_7; }
	inline String_t** get_address_of_currencyDecimalSeparator_7() { return &___currencyDecimalSeparator_7; }
	inline void set_currencyDecimalSeparator_7(String_t* value)
	{
		___currencyDecimalSeparator_7 = value;
		Il2CppCodeGenWriteBarrier((&___currencyDecimalSeparator_7), value);
	}

	inline static int32_t get_offset_of_currencyGroupSeparator_8() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t104580544, ___currencyGroupSeparator_8)); }
	inline String_t* get_currencyGroupSeparator_8() const { return ___currencyGroupSeparator_8; }
	inline String_t** get_address_of_currencyGroupSeparator_8() { return &___currencyGroupSeparator_8; }
	inline void set_currencyGroupSeparator_8(String_t* value)
	{
		___currencyGroupSeparator_8 = value;
		Il2CppCodeGenWriteBarrier((&___currencyGroupSeparator_8), value);
	}

	inline static int32_t get_offset_of_currencyGroupSizes_9() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t104580544, ___currencyGroupSizes_9)); }
	inline Int32U5BU5D_t3030399641* get_currencyGroupSizes_9() const { return ___currencyGroupSizes_9; }
	inline Int32U5BU5D_t3030399641** get_address_of_currencyGroupSizes_9() { return &___currencyGroupSizes_9; }
	inline void set_currencyGroupSizes_9(Int32U5BU5D_t3030399641* value)
	{
		___currencyGroupSizes_9 = value;
		Il2CppCodeGenWriteBarrier((&___currencyGroupSizes_9), value);
	}

	inline static int32_t get_offset_of_currencyNegativePattern_10() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t104580544, ___currencyNegativePattern_10)); }
	inline int32_t get_currencyNegativePattern_10() const { return ___currencyNegativePattern_10; }
	inline int32_t* get_address_of_currencyNegativePattern_10() { return &___currencyNegativePattern_10; }
	inline void set_currencyNegativePattern_10(int32_t value)
	{
		___currencyNegativePattern_10 = value;
	}

	inline static int32_t get_offset_of_currencyPositivePattern_11() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t104580544, ___currencyPositivePattern_11)); }
	inline int32_t get_currencyPositivePattern_11() const { return ___currencyPositivePattern_11; }
	inline int32_t* get_address_of_currencyPositivePattern_11() { return &___currencyPositivePattern_11; }
	inline void set_currencyPositivePattern_11(int32_t value)
	{
		___currencyPositivePattern_11 = value;
	}

	inline static int32_t get_offset_of_currencySymbol_12() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t104580544, ___currencySymbol_12)); }
	inline String_t* get_currencySymbol_12() const { return ___currencySymbol_12; }
	inline String_t** get_address_of_currencySymbol_12() { return &___currencySymbol_12; }
	inline void set_currencySymbol_12(String_t* value)
	{
		___currencySymbol_12 = value;
		Il2CppCodeGenWriteBarrier((&___currencySymbol_12), value);
	}

	inline static int32_t get_offset_of_nanSymbol_13() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t104580544, ___nanSymbol_13)); }
	inline String_t* get_nanSymbol_13() const { return ___nanSymbol_13; }
	inline String_t** get_address_of_nanSymbol_13() { return &___nanSymbol_13; }
	inline void set_nanSymbol_13(String_t* value)
	{
		___nanSymbol_13 = value;
		Il2CppCodeGenWriteBarrier((&___nanSymbol_13), value);
	}

	inline static int32_t get_offset_of_negativeInfinitySymbol_14() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t104580544, ___negativeInfinitySymbol_14)); }
	inline String_t* get_negativeInfinitySymbol_14() const { return ___negativeInfinitySymbol_14; }
	inline String_t** get_address_of_negativeInfinitySymbol_14() { return &___negativeInfinitySymbol_14; }
	inline void set_negativeInfinitySymbol_14(String_t* value)
	{
		___negativeInfinitySymbol_14 = value;
		Il2CppCodeGenWriteBarrier((&___negativeInfinitySymbol_14), value);
	}

	inline static int32_t get_offset_of_negativeSign_15() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t104580544, ___negativeSign_15)); }
	inline String_t* get_negativeSign_15() const { return ___negativeSign_15; }
	inline String_t** get_address_of_negativeSign_15() { return &___negativeSign_15; }
	inline void set_negativeSign_15(String_t* value)
	{
		___negativeSign_15 = value;
		Il2CppCodeGenWriteBarrier((&___negativeSign_15), value);
	}

	inline static int32_t get_offset_of_numberDecimalDigits_16() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t104580544, ___numberDecimalDigits_16)); }
	inline int32_t get_numberDecimalDigits_16() const { return ___numberDecimalDigits_16; }
	inline int32_t* get_address_of_numberDecimalDigits_16() { return &___numberDecimalDigits_16; }
	inline void set_numberDecimalDigits_16(int32_t value)
	{
		___numberDecimalDigits_16 = value;
	}

	inline static int32_t get_offset_of_numberDecimalSeparator_17() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t104580544, ___numberDecimalSeparator_17)); }
	inline String_t* get_numberDecimalSeparator_17() const { return ___numberDecimalSeparator_17; }
	inline String_t** get_address_of_numberDecimalSeparator_17() { return &___numberDecimalSeparator_17; }
	inline void set_numberDecimalSeparator_17(String_t* value)
	{
		___numberDecimalSeparator_17 = value;
		Il2CppCodeGenWriteBarrier((&___numberDecimalSeparator_17), value);
	}

	inline static int32_t get_offset_of_numberGroupSeparator_18() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t104580544, ___numberGroupSeparator_18)); }
	inline String_t* get_numberGroupSeparator_18() const { return ___numberGroupSeparator_18; }
	inline String_t** get_address_of_numberGroupSeparator_18() { return &___numberGroupSeparator_18; }
	inline void set_numberGroupSeparator_18(String_t* value)
	{
		___numberGroupSeparator_18 = value;
		Il2CppCodeGenWriteBarrier((&___numberGroupSeparator_18), value);
	}

	inline static int32_t get_offset_of_numberGroupSizes_19() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t104580544, ___numberGroupSizes_19)); }
	inline Int32U5BU5D_t3030399641* get_numberGroupSizes_19() const { return ___numberGroupSizes_19; }
	inline Int32U5BU5D_t3030399641** get_address_of_numberGroupSizes_19() { return &___numberGroupSizes_19; }
	inline void set_numberGroupSizes_19(Int32U5BU5D_t3030399641* value)
	{
		___numberGroupSizes_19 = value;
		Il2CppCodeGenWriteBarrier((&___numberGroupSizes_19), value);
	}

	inline static int32_t get_offset_of_numberNegativePattern_20() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t104580544, ___numberNegativePattern_20)); }
	inline int32_t get_numberNegativePattern_20() const { return ___numberNegativePattern_20; }
	inline int32_t* get_address_of_numberNegativePattern_20() { return &___numberNegativePattern_20; }
	inline void set_numberNegativePattern_20(int32_t value)
	{
		___numberNegativePattern_20 = value;
	}

	inline static int32_t get_offset_of_percentDecimalDigits_21() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t104580544, ___percentDecimalDigits_21)); }
	inline int32_t get_percentDecimalDigits_21() const { return ___percentDecimalDigits_21; }
	inline int32_t* get_address_of_percentDecimalDigits_21() { return &___percentDecimalDigits_21; }
	inline void set_percentDecimalDigits_21(int32_t value)
	{
		___percentDecimalDigits_21 = value;
	}

	inline static int32_t get_offset_of_percentDecimalSeparator_22() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t104580544, ___percentDecimalSeparator_22)); }
	inline String_t* get_percentDecimalSeparator_22() const { return ___percentDecimalSeparator_22; }
	inline String_t** get_address_of_percentDecimalSeparator_22() { return &___percentDecimalSeparator_22; }
	inline void set_percentDecimalSeparator_22(String_t* value)
	{
		___percentDecimalSeparator_22 = value;
		Il2CppCodeGenWriteBarrier((&___percentDecimalSeparator_22), value);
	}

	inline static int32_t get_offset_of_percentGroupSeparator_23() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t104580544, ___percentGroupSeparator_23)); }
	inline String_t* get_percentGroupSeparator_23() const { return ___percentGroupSeparator_23; }
	inline String_t** get_address_of_percentGroupSeparator_23() { return &___percentGroupSeparator_23; }
	inline void set_percentGroupSeparator_23(String_t* value)
	{
		___percentGroupSeparator_23 = value;
		Il2CppCodeGenWriteBarrier((&___percentGroupSeparator_23), value);
	}

	inline static int32_t get_offset_of_percentGroupSizes_24() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t104580544, ___percentGroupSizes_24)); }
	inline Int32U5BU5D_t3030399641* get_percentGroupSizes_24() const { return ___percentGroupSizes_24; }
	inline Int32U5BU5D_t3030399641** get_address_of_percentGroupSizes_24() { return &___percentGroupSizes_24; }
	inline void set_percentGroupSizes_24(Int32U5BU5D_t3030399641* value)
	{
		___percentGroupSizes_24 = value;
		Il2CppCodeGenWriteBarrier((&___percentGroupSizes_24), value);
	}

	inline static int32_t get_offset_of_percentNegativePattern_25() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t104580544, ___percentNegativePattern_25)); }
	inline int32_t get_percentNegativePattern_25() const { return ___percentNegativePattern_25; }
	inline int32_t* get_address_of_percentNegativePattern_25() { return &___percentNegativePattern_25; }
	inline void set_percentNegativePattern_25(int32_t value)
	{
		___percentNegativePattern_25 = value;
	}

	inline static int32_t get_offset_of_percentPositivePattern_26() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t104580544, ___percentPositivePattern_26)); }
	inline int32_t get_percentPositivePattern_26() const { return ___percentPositivePattern_26; }
	inline int32_t* get_address_of_percentPositivePattern_26() { return &___percentPositivePattern_26; }
	inline void set_percentPositivePattern_26(int32_t value)
	{
		___percentPositivePattern_26 = value;
	}

	inline static int32_t get_offset_of_percentSymbol_27() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t104580544, ___percentSymbol_27)); }
	inline String_t* get_percentSymbol_27() const { return ___percentSymbol_27; }
	inline String_t** get_address_of_percentSymbol_27() { return &___percentSymbol_27; }
	inline void set_percentSymbol_27(String_t* value)
	{
		___percentSymbol_27 = value;
		Il2CppCodeGenWriteBarrier((&___percentSymbol_27), value);
	}

	inline static int32_t get_offset_of_perMilleSymbol_28() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t104580544, ___perMilleSymbol_28)); }
	inline String_t* get_perMilleSymbol_28() const { return ___perMilleSymbol_28; }
	inline String_t** get_address_of_perMilleSymbol_28() { return &___perMilleSymbol_28; }
	inline void set_perMilleSymbol_28(String_t* value)
	{
		___perMilleSymbol_28 = value;
		Il2CppCodeGenWriteBarrier((&___perMilleSymbol_28), value);
	}

	inline static int32_t get_offset_of_positiveInfinitySymbol_29() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t104580544, ___positiveInfinitySymbol_29)); }
	inline String_t* get_positiveInfinitySymbol_29() const { return ___positiveInfinitySymbol_29; }
	inline String_t** get_address_of_positiveInfinitySymbol_29() { return &___positiveInfinitySymbol_29; }
	inline void set_positiveInfinitySymbol_29(String_t* value)
	{
		___positiveInfinitySymbol_29 = value;
		Il2CppCodeGenWriteBarrier((&___positiveInfinitySymbol_29), value);
	}

	inline static int32_t get_offset_of_positiveSign_30() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t104580544, ___positiveSign_30)); }
	inline String_t* get_positiveSign_30() const { return ___positiveSign_30; }
	inline String_t** get_address_of_positiveSign_30() { return &___positiveSign_30; }
	inline void set_positiveSign_30(String_t* value)
	{
		___positiveSign_30 = value;
		Il2CppCodeGenWriteBarrier((&___positiveSign_30), value);
	}

	inline static int32_t get_offset_of_ansiCurrencySymbol_31() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t104580544, ___ansiCurrencySymbol_31)); }
	inline String_t* get_ansiCurrencySymbol_31() const { return ___ansiCurrencySymbol_31; }
	inline String_t** get_address_of_ansiCurrencySymbol_31() { return &___ansiCurrencySymbol_31; }
	inline void set_ansiCurrencySymbol_31(String_t* value)
	{
		___ansiCurrencySymbol_31 = value;
		Il2CppCodeGenWriteBarrier((&___ansiCurrencySymbol_31), value);
	}

	inline static int32_t get_offset_of_m_dataItem_32() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t104580544, ___m_dataItem_32)); }
	inline int32_t get_m_dataItem_32() const { return ___m_dataItem_32; }
	inline int32_t* get_address_of_m_dataItem_32() { return &___m_dataItem_32; }
	inline void set_m_dataItem_32(int32_t value)
	{
		___m_dataItem_32 = value;
	}

	inline static int32_t get_offset_of_m_useUserOverride_33() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t104580544, ___m_useUserOverride_33)); }
	inline bool get_m_useUserOverride_33() const { return ___m_useUserOverride_33; }
	inline bool* get_address_of_m_useUserOverride_33() { return &___m_useUserOverride_33; }
	inline void set_m_useUserOverride_33(bool value)
	{
		___m_useUserOverride_33 = value;
	}

	inline static int32_t get_offset_of_validForParseAsNumber_34() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t104580544, ___validForParseAsNumber_34)); }
	inline bool get_validForParseAsNumber_34() const { return ___validForParseAsNumber_34; }
	inline bool* get_address_of_validForParseAsNumber_34() { return &___validForParseAsNumber_34; }
	inline void set_validForParseAsNumber_34(bool value)
	{
		___validForParseAsNumber_34 = value;
	}

	inline static int32_t get_offset_of_validForParseAsCurrency_35() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t104580544, ___validForParseAsCurrency_35)); }
	inline bool get_validForParseAsCurrency_35() const { return ___validForParseAsCurrency_35; }
	inline bool* get_address_of_validForParseAsCurrency_35() { return &___validForParseAsCurrency_35; }
	inline void set_validForParseAsCurrency_35(bool value)
	{
		___validForParseAsCurrency_35 = value;
	}

	inline static int32_t get_offset_of_nativeDigits_36() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t104580544, ___nativeDigits_36)); }
	inline StringU5BU5D_t1642385972* get_nativeDigits_36() const { return ___nativeDigits_36; }
	inline StringU5BU5D_t1642385972** get_address_of_nativeDigits_36() { return &___nativeDigits_36; }
	inline void set_nativeDigits_36(StringU5BU5D_t1642385972* value)
	{
		___nativeDigits_36 = value;
		Il2CppCodeGenWriteBarrier((&___nativeDigits_36), value);
	}

	inline static int32_t get_offset_of_digitSubstitution_37() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t104580544, ___digitSubstitution_37)); }
	inline int32_t get_digitSubstitution_37() const { return ___digitSubstitution_37; }
	inline int32_t* get_address_of_digitSubstitution_37() { return &___digitSubstitution_37; }
	inline void set_digitSubstitution_37(int32_t value)
	{
		___digitSubstitution_37 = value;
	}
};

struct NumberFormatInfo_t104580544_StaticFields
{
public:
	// System.String[] System.Globalization.NumberFormatInfo::invariantNativeDigits
	StringU5BU5D_t1642385972* ___invariantNativeDigits_38;

public:
	inline static int32_t get_offset_of_invariantNativeDigits_38() { return static_cast<int32_t>(offsetof(NumberFormatInfo_t104580544_StaticFields, ___invariantNativeDigits_38)); }
	inline StringU5BU5D_t1642385972* get_invariantNativeDigits_38() const { return ___invariantNativeDigits_38; }
	inline StringU5BU5D_t1642385972** get_address_of_invariantNativeDigits_38() { return &___invariantNativeDigits_38; }
	inline void set_invariantNativeDigits_38(StringU5BU5D_t1642385972* value)
	{
		___invariantNativeDigits_38 = value;
		Il2CppCodeGenWriteBarrier((&___invariantNativeDigits_38), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NUMBERFORMATINFO_T104580544_H
#ifndef XMLQUALIFIEDNAME_T1944712516_H
#define XMLQUALIFIEDNAME_T1944712516_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlQualifiedName
struct  XmlQualifiedName_t1944712516  : public RuntimeObject
{
public:
	// System.String System.Xml.XmlQualifiedName::name
	String_t* ___name_1;
	// System.String System.Xml.XmlQualifiedName::ns
	String_t* ___ns_2;
	// System.Int32 System.Xml.XmlQualifiedName::hash
	int32_t ___hash_3;

public:
	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(XmlQualifiedName_t1944712516, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}

	inline static int32_t get_offset_of_ns_2() { return static_cast<int32_t>(offsetof(XmlQualifiedName_t1944712516, ___ns_2)); }
	inline String_t* get_ns_2() const { return ___ns_2; }
	inline String_t** get_address_of_ns_2() { return &___ns_2; }
	inline void set_ns_2(String_t* value)
	{
		___ns_2 = value;
		Il2CppCodeGenWriteBarrier((&___ns_2), value);
	}

	inline static int32_t get_offset_of_hash_3() { return static_cast<int32_t>(offsetof(XmlQualifiedName_t1944712516, ___hash_3)); }
	inline int32_t get_hash_3() const { return ___hash_3; }
	inline int32_t* get_address_of_hash_3() { return &___hash_3; }
	inline void set_hash_3(int32_t value)
	{
		___hash_3 = value;
	}
};

struct XmlQualifiedName_t1944712516_StaticFields
{
public:
	// System.Xml.XmlQualifiedName System.Xml.XmlQualifiedName::Empty
	XmlQualifiedName_t1944712516 * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(XmlQualifiedName_t1944712516_StaticFields, ___Empty_0)); }
	inline XmlQualifiedName_t1944712516 * get_Empty_0() const { return ___Empty_0; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(XmlQualifiedName_t1944712516 * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLQUALIFIEDNAME_T1944712516_H
#ifndef STRINGBUILDER_T1221177846_H
#define STRINGBUILDER_T1221177846_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.StringBuilder
struct  StringBuilder_t1221177846  : public RuntimeObject
{
public:
	// System.Int32 System.Text.StringBuilder::_length
	int32_t ____length_1;
	// System.String System.Text.StringBuilder::_str
	String_t* ____str_2;
	// System.String System.Text.StringBuilder::_cached_str
	String_t* ____cached_str_3;
	// System.Int32 System.Text.StringBuilder::_maxCapacity
	int32_t ____maxCapacity_4;

public:
	inline static int32_t get_offset_of__length_1() { return static_cast<int32_t>(offsetof(StringBuilder_t1221177846, ____length_1)); }
	inline int32_t get__length_1() const { return ____length_1; }
	inline int32_t* get_address_of__length_1() { return &____length_1; }
	inline void set__length_1(int32_t value)
	{
		____length_1 = value;
	}

	inline static int32_t get_offset_of__str_2() { return static_cast<int32_t>(offsetof(StringBuilder_t1221177846, ____str_2)); }
	inline String_t* get__str_2() const { return ____str_2; }
	inline String_t** get_address_of__str_2() { return &____str_2; }
	inline void set__str_2(String_t* value)
	{
		____str_2 = value;
		Il2CppCodeGenWriteBarrier((&____str_2), value);
	}

	inline static int32_t get_offset_of__cached_str_3() { return static_cast<int32_t>(offsetof(StringBuilder_t1221177846, ____cached_str_3)); }
	inline String_t* get__cached_str_3() const { return ____cached_str_3; }
	inline String_t** get_address_of__cached_str_3() { return &____cached_str_3; }
	inline void set__cached_str_3(String_t* value)
	{
		____cached_str_3 = value;
		Il2CppCodeGenWriteBarrier((&____cached_str_3), value);
	}

	inline static int32_t get_offset_of__maxCapacity_4() { return static_cast<int32_t>(offsetof(StringBuilder_t1221177846, ____maxCapacity_4)); }
	inline int32_t get__maxCapacity_4() const { return ____maxCapacity_4; }
	inline int32_t* get_address_of__maxCapacity_4() { return &____maxCapacity_4; }
	inline void set__maxCapacity_4(int32_t value)
	{
		____maxCapacity_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGBUILDER_T1221177846_H
#ifndef FUNCTIONARGUMENTS_T2900945452_H
#define FUNCTIONARGUMENTS_T2900945452_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.FunctionArguments
struct  FunctionArguments_t2900945452  : public RuntimeObject
{
public:
	// System.Xml.XPath.Expression System.Xml.XPath.FunctionArguments::_arg
	Expression_t1283317256 * ____arg_0;
	// System.Xml.XPath.FunctionArguments System.Xml.XPath.FunctionArguments::_tail
	FunctionArguments_t2900945452 * ____tail_1;

public:
	inline static int32_t get_offset_of__arg_0() { return static_cast<int32_t>(offsetof(FunctionArguments_t2900945452, ____arg_0)); }
	inline Expression_t1283317256 * get__arg_0() const { return ____arg_0; }
	inline Expression_t1283317256 ** get_address_of__arg_0() { return &____arg_0; }
	inline void set__arg_0(Expression_t1283317256 * value)
	{
		____arg_0 = value;
		Il2CppCodeGenWriteBarrier((&____arg_0), value);
	}

	inline static int32_t get_offset_of__tail_1() { return static_cast<int32_t>(offsetof(FunctionArguments_t2900945452, ____tail_1)); }
	inline FunctionArguments_t2900945452 * get__tail_1() const { return ____tail_1; }
	inline FunctionArguments_t2900945452 ** get_address_of__tail_1() { return &____tail_1; }
	inline void set__tail_1(FunctionArguments_t2900945452 * value)
	{
		____tail_1 = value;
		Il2CppCodeGenWriteBarrier((&____tail_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNCTIONARGUMENTS_T2900945452_H
#ifndef EXCEPTION_T1927440687_H
#define EXCEPTION_T1927440687_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t1927440687  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t169632028* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t1927440687 * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t169632028* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t169632028** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t169632028* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___inner_exception_1)); }
	inline Exception_t1927440687 * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t1927440687 ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t1927440687 * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T1927440687_H
#ifndef XPATHITERATORCOMPARER_T2522471076_H
#define XPATHITERATORCOMPARER_T2522471076_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathIteratorComparer
struct  XPathIteratorComparer_t2522471076  : public RuntimeObject
{
public:

public:
};

struct XPathIteratorComparer_t2522471076_StaticFields
{
public:
	// System.Xml.XPath.XPathIteratorComparer System.Xml.XPath.XPathIteratorComparer::Instance
	XPathIteratorComparer_t2522471076 * ___Instance_0;

public:
	inline static int32_t get_offset_of_Instance_0() { return static_cast<int32_t>(offsetof(XPathIteratorComparer_t2522471076_StaticFields, ___Instance_0)); }
	inline XPathIteratorComparer_t2522471076 * get_Instance_0() const { return ___Instance_0; }
	inline XPathIteratorComparer_t2522471076 ** get_address_of_Instance_0() { return &___Instance_0; }
	inline void set_Instance_0(XPathIteratorComparer_t2522471076 * value)
	{
		___Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHITERATORCOMPARER_T2522471076_H
#ifndef XPATHEXPRESSION_T452251917_H
#define XPATHEXPRESSION_T452251917_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathExpression
struct  XPathExpression_t452251917  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHEXPRESSION_T452251917_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t1328083999* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t1328083999* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t1328083999** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t1328083999* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef EXPRESSION_T1283317256_H
#define EXPRESSION_T1283317256_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.Expression
struct  Expression_t1283317256  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRESSION_T1283317256_H
#ifndef CULTUREINFO_T3500843524_H
#define CULTUREINFO_T3500843524_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Globalization.CultureInfo
struct  CultureInfo_t3500843524  : public RuntimeObject
{
public:
	// System.Boolean System.Globalization.CultureInfo::m_isReadOnly
	bool ___m_isReadOnly_7;
	// System.Int32 System.Globalization.CultureInfo::cultureID
	int32_t ___cultureID_8;
	// System.Int32 System.Globalization.CultureInfo::parent_lcid
	int32_t ___parent_lcid_9;
	// System.Int32 System.Globalization.CultureInfo::specific_lcid
	int32_t ___specific_lcid_10;
	// System.Int32 System.Globalization.CultureInfo::datetime_index
	int32_t ___datetime_index_11;
	// System.Int32 System.Globalization.CultureInfo::number_index
	int32_t ___number_index_12;
	// System.Boolean System.Globalization.CultureInfo::m_useUserOverride
	bool ___m_useUserOverride_13;
	// System.Globalization.NumberFormatInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::numInfo
	NumberFormatInfo_t104580544 * ___numInfo_14;
	// System.Globalization.DateTimeFormatInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::dateTimeInfo
	DateTimeFormatInfo_t2187473504 * ___dateTimeInfo_15;
	// System.Globalization.TextInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::textInfo
	TextInfo_t3620182823 * ___textInfo_16;
	// System.String System.Globalization.CultureInfo::m_name
	String_t* ___m_name_17;
	// System.String System.Globalization.CultureInfo::displayname
	String_t* ___displayname_18;
	// System.String System.Globalization.CultureInfo::englishname
	String_t* ___englishname_19;
	// System.String System.Globalization.CultureInfo::nativename
	String_t* ___nativename_20;
	// System.String System.Globalization.CultureInfo::iso3lang
	String_t* ___iso3lang_21;
	// System.String System.Globalization.CultureInfo::iso2lang
	String_t* ___iso2lang_22;
	// System.String System.Globalization.CultureInfo::icu_name
	String_t* ___icu_name_23;
	// System.String System.Globalization.CultureInfo::win3lang
	String_t* ___win3lang_24;
	// System.String System.Globalization.CultureInfo::territory
	String_t* ___territory_25;
	// System.Globalization.CompareInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::compareInfo
	CompareInfo_t2310920157 * ___compareInfo_26;
	// System.Int32* System.Globalization.CultureInfo::calendar_data
	int32_t* ___calendar_data_27;
	// System.Void* System.Globalization.CultureInfo::textinfo_data
	void* ___textinfo_data_28;
	// System.Globalization.Calendar[] System.Globalization.CultureInfo::optional_calendars
	CalendarU5BU5D_t3654442685* ___optional_calendars_29;
	// System.Globalization.CultureInfo System.Globalization.CultureInfo::parent_culture
	CultureInfo_t3500843524 * ___parent_culture_30;
	// System.Int32 System.Globalization.CultureInfo::m_dataItem
	int32_t ___m_dataItem_31;
	// System.Globalization.Calendar System.Globalization.CultureInfo::calendar
	Calendar_t585061108 * ___calendar_32;
	// System.Boolean System.Globalization.CultureInfo::constructed
	bool ___constructed_33;
	// System.Byte[] System.Globalization.CultureInfo::cached_serialized_form
	ByteU5BU5D_t3397334013* ___cached_serialized_form_34;

public:
	inline static int32_t get_offset_of_m_isReadOnly_7() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524, ___m_isReadOnly_7)); }
	inline bool get_m_isReadOnly_7() const { return ___m_isReadOnly_7; }
	inline bool* get_address_of_m_isReadOnly_7() { return &___m_isReadOnly_7; }
	inline void set_m_isReadOnly_7(bool value)
	{
		___m_isReadOnly_7 = value;
	}

	inline static int32_t get_offset_of_cultureID_8() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524, ___cultureID_8)); }
	inline int32_t get_cultureID_8() const { return ___cultureID_8; }
	inline int32_t* get_address_of_cultureID_8() { return &___cultureID_8; }
	inline void set_cultureID_8(int32_t value)
	{
		___cultureID_8 = value;
	}

	inline static int32_t get_offset_of_parent_lcid_9() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524, ___parent_lcid_9)); }
	inline int32_t get_parent_lcid_9() const { return ___parent_lcid_9; }
	inline int32_t* get_address_of_parent_lcid_9() { return &___parent_lcid_9; }
	inline void set_parent_lcid_9(int32_t value)
	{
		___parent_lcid_9 = value;
	}

	inline static int32_t get_offset_of_specific_lcid_10() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524, ___specific_lcid_10)); }
	inline int32_t get_specific_lcid_10() const { return ___specific_lcid_10; }
	inline int32_t* get_address_of_specific_lcid_10() { return &___specific_lcid_10; }
	inline void set_specific_lcid_10(int32_t value)
	{
		___specific_lcid_10 = value;
	}

	inline static int32_t get_offset_of_datetime_index_11() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524, ___datetime_index_11)); }
	inline int32_t get_datetime_index_11() const { return ___datetime_index_11; }
	inline int32_t* get_address_of_datetime_index_11() { return &___datetime_index_11; }
	inline void set_datetime_index_11(int32_t value)
	{
		___datetime_index_11 = value;
	}

	inline static int32_t get_offset_of_number_index_12() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524, ___number_index_12)); }
	inline int32_t get_number_index_12() const { return ___number_index_12; }
	inline int32_t* get_address_of_number_index_12() { return &___number_index_12; }
	inline void set_number_index_12(int32_t value)
	{
		___number_index_12 = value;
	}

	inline static int32_t get_offset_of_m_useUserOverride_13() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524, ___m_useUserOverride_13)); }
	inline bool get_m_useUserOverride_13() const { return ___m_useUserOverride_13; }
	inline bool* get_address_of_m_useUserOverride_13() { return &___m_useUserOverride_13; }
	inline void set_m_useUserOverride_13(bool value)
	{
		___m_useUserOverride_13 = value;
	}

	inline static int32_t get_offset_of_numInfo_14() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524, ___numInfo_14)); }
	inline NumberFormatInfo_t104580544 * get_numInfo_14() const { return ___numInfo_14; }
	inline NumberFormatInfo_t104580544 ** get_address_of_numInfo_14() { return &___numInfo_14; }
	inline void set_numInfo_14(NumberFormatInfo_t104580544 * value)
	{
		___numInfo_14 = value;
		Il2CppCodeGenWriteBarrier((&___numInfo_14), value);
	}

	inline static int32_t get_offset_of_dateTimeInfo_15() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524, ___dateTimeInfo_15)); }
	inline DateTimeFormatInfo_t2187473504 * get_dateTimeInfo_15() const { return ___dateTimeInfo_15; }
	inline DateTimeFormatInfo_t2187473504 ** get_address_of_dateTimeInfo_15() { return &___dateTimeInfo_15; }
	inline void set_dateTimeInfo_15(DateTimeFormatInfo_t2187473504 * value)
	{
		___dateTimeInfo_15 = value;
		Il2CppCodeGenWriteBarrier((&___dateTimeInfo_15), value);
	}

	inline static int32_t get_offset_of_textInfo_16() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524, ___textInfo_16)); }
	inline TextInfo_t3620182823 * get_textInfo_16() const { return ___textInfo_16; }
	inline TextInfo_t3620182823 ** get_address_of_textInfo_16() { return &___textInfo_16; }
	inline void set_textInfo_16(TextInfo_t3620182823 * value)
	{
		___textInfo_16 = value;
		Il2CppCodeGenWriteBarrier((&___textInfo_16), value);
	}

	inline static int32_t get_offset_of_m_name_17() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524, ___m_name_17)); }
	inline String_t* get_m_name_17() const { return ___m_name_17; }
	inline String_t** get_address_of_m_name_17() { return &___m_name_17; }
	inline void set_m_name_17(String_t* value)
	{
		___m_name_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_name_17), value);
	}

	inline static int32_t get_offset_of_displayname_18() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524, ___displayname_18)); }
	inline String_t* get_displayname_18() const { return ___displayname_18; }
	inline String_t** get_address_of_displayname_18() { return &___displayname_18; }
	inline void set_displayname_18(String_t* value)
	{
		___displayname_18 = value;
		Il2CppCodeGenWriteBarrier((&___displayname_18), value);
	}

	inline static int32_t get_offset_of_englishname_19() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524, ___englishname_19)); }
	inline String_t* get_englishname_19() const { return ___englishname_19; }
	inline String_t** get_address_of_englishname_19() { return &___englishname_19; }
	inline void set_englishname_19(String_t* value)
	{
		___englishname_19 = value;
		Il2CppCodeGenWriteBarrier((&___englishname_19), value);
	}

	inline static int32_t get_offset_of_nativename_20() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524, ___nativename_20)); }
	inline String_t* get_nativename_20() const { return ___nativename_20; }
	inline String_t** get_address_of_nativename_20() { return &___nativename_20; }
	inline void set_nativename_20(String_t* value)
	{
		___nativename_20 = value;
		Il2CppCodeGenWriteBarrier((&___nativename_20), value);
	}

	inline static int32_t get_offset_of_iso3lang_21() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524, ___iso3lang_21)); }
	inline String_t* get_iso3lang_21() const { return ___iso3lang_21; }
	inline String_t** get_address_of_iso3lang_21() { return &___iso3lang_21; }
	inline void set_iso3lang_21(String_t* value)
	{
		___iso3lang_21 = value;
		Il2CppCodeGenWriteBarrier((&___iso3lang_21), value);
	}

	inline static int32_t get_offset_of_iso2lang_22() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524, ___iso2lang_22)); }
	inline String_t* get_iso2lang_22() const { return ___iso2lang_22; }
	inline String_t** get_address_of_iso2lang_22() { return &___iso2lang_22; }
	inline void set_iso2lang_22(String_t* value)
	{
		___iso2lang_22 = value;
		Il2CppCodeGenWriteBarrier((&___iso2lang_22), value);
	}

	inline static int32_t get_offset_of_icu_name_23() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524, ___icu_name_23)); }
	inline String_t* get_icu_name_23() const { return ___icu_name_23; }
	inline String_t** get_address_of_icu_name_23() { return &___icu_name_23; }
	inline void set_icu_name_23(String_t* value)
	{
		___icu_name_23 = value;
		Il2CppCodeGenWriteBarrier((&___icu_name_23), value);
	}

	inline static int32_t get_offset_of_win3lang_24() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524, ___win3lang_24)); }
	inline String_t* get_win3lang_24() const { return ___win3lang_24; }
	inline String_t** get_address_of_win3lang_24() { return &___win3lang_24; }
	inline void set_win3lang_24(String_t* value)
	{
		___win3lang_24 = value;
		Il2CppCodeGenWriteBarrier((&___win3lang_24), value);
	}

	inline static int32_t get_offset_of_territory_25() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524, ___territory_25)); }
	inline String_t* get_territory_25() const { return ___territory_25; }
	inline String_t** get_address_of_territory_25() { return &___territory_25; }
	inline void set_territory_25(String_t* value)
	{
		___territory_25 = value;
		Il2CppCodeGenWriteBarrier((&___territory_25), value);
	}

	inline static int32_t get_offset_of_compareInfo_26() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524, ___compareInfo_26)); }
	inline CompareInfo_t2310920157 * get_compareInfo_26() const { return ___compareInfo_26; }
	inline CompareInfo_t2310920157 ** get_address_of_compareInfo_26() { return &___compareInfo_26; }
	inline void set_compareInfo_26(CompareInfo_t2310920157 * value)
	{
		___compareInfo_26 = value;
		Il2CppCodeGenWriteBarrier((&___compareInfo_26), value);
	}

	inline static int32_t get_offset_of_calendar_data_27() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524, ___calendar_data_27)); }
	inline int32_t* get_calendar_data_27() const { return ___calendar_data_27; }
	inline int32_t** get_address_of_calendar_data_27() { return &___calendar_data_27; }
	inline void set_calendar_data_27(int32_t* value)
	{
		___calendar_data_27 = value;
	}

	inline static int32_t get_offset_of_textinfo_data_28() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524, ___textinfo_data_28)); }
	inline void* get_textinfo_data_28() const { return ___textinfo_data_28; }
	inline void** get_address_of_textinfo_data_28() { return &___textinfo_data_28; }
	inline void set_textinfo_data_28(void* value)
	{
		___textinfo_data_28 = value;
	}

	inline static int32_t get_offset_of_optional_calendars_29() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524, ___optional_calendars_29)); }
	inline CalendarU5BU5D_t3654442685* get_optional_calendars_29() const { return ___optional_calendars_29; }
	inline CalendarU5BU5D_t3654442685** get_address_of_optional_calendars_29() { return &___optional_calendars_29; }
	inline void set_optional_calendars_29(CalendarU5BU5D_t3654442685* value)
	{
		___optional_calendars_29 = value;
		Il2CppCodeGenWriteBarrier((&___optional_calendars_29), value);
	}

	inline static int32_t get_offset_of_parent_culture_30() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524, ___parent_culture_30)); }
	inline CultureInfo_t3500843524 * get_parent_culture_30() const { return ___parent_culture_30; }
	inline CultureInfo_t3500843524 ** get_address_of_parent_culture_30() { return &___parent_culture_30; }
	inline void set_parent_culture_30(CultureInfo_t3500843524 * value)
	{
		___parent_culture_30 = value;
		Il2CppCodeGenWriteBarrier((&___parent_culture_30), value);
	}

	inline static int32_t get_offset_of_m_dataItem_31() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524, ___m_dataItem_31)); }
	inline int32_t get_m_dataItem_31() const { return ___m_dataItem_31; }
	inline int32_t* get_address_of_m_dataItem_31() { return &___m_dataItem_31; }
	inline void set_m_dataItem_31(int32_t value)
	{
		___m_dataItem_31 = value;
	}

	inline static int32_t get_offset_of_calendar_32() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524, ___calendar_32)); }
	inline Calendar_t585061108 * get_calendar_32() const { return ___calendar_32; }
	inline Calendar_t585061108 ** get_address_of_calendar_32() { return &___calendar_32; }
	inline void set_calendar_32(Calendar_t585061108 * value)
	{
		___calendar_32 = value;
		Il2CppCodeGenWriteBarrier((&___calendar_32), value);
	}

	inline static int32_t get_offset_of_constructed_33() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524, ___constructed_33)); }
	inline bool get_constructed_33() const { return ___constructed_33; }
	inline bool* get_address_of_constructed_33() { return &___constructed_33; }
	inline void set_constructed_33(bool value)
	{
		___constructed_33 = value;
	}

	inline static int32_t get_offset_of_cached_serialized_form_34() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524, ___cached_serialized_form_34)); }
	inline ByteU5BU5D_t3397334013* get_cached_serialized_form_34() const { return ___cached_serialized_form_34; }
	inline ByteU5BU5D_t3397334013** get_address_of_cached_serialized_form_34() { return &___cached_serialized_form_34; }
	inline void set_cached_serialized_form_34(ByteU5BU5D_t3397334013* value)
	{
		___cached_serialized_form_34 = value;
		Il2CppCodeGenWriteBarrier((&___cached_serialized_form_34), value);
	}
};

struct CultureInfo_t3500843524_StaticFields
{
public:
	// System.Globalization.CultureInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::invariant_culture_info
	CultureInfo_t3500843524 * ___invariant_culture_info_4;
	// System.Object System.Globalization.CultureInfo::shared_table_lock
	RuntimeObject * ___shared_table_lock_5;
	// System.Int32 System.Globalization.CultureInfo::BootstrapCultureID
	int32_t ___BootstrapCultureID_6;
	// System.String System.Globalization.CultureInfo::MSG_READONLY
	String_t* ___MSG_READONLY_35;
	// System.Collections.Hashtable System.Globalization.CultureInfo::shared_by_number
	Hashtable_t909839986 * ___shared_by_number_36;
	// System.Collections.Hashtable System.Globalization.CultureInfo::shared_by_name
	Hashtable_t909839986 * ___shared_by_name_37;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Globalization.CultureInfo::<>f__switch$map19
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map19_38;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Globalization.CultureInfo::<>f__switch$map1A
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map1A_39;

public:
	inline static int32_t get_offset_of_invariant_culture_info_4() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524_StaticFields, ___invariant_culture_info_4)); }
	inline CultureInfo_t3500843524 * get_invariant_culture_info_4() const { return ___invariant_culture_info_4; }
	inline CultureInfo_t3500843524 ** get_address_of_invariant_culture_info_4() { return &___invariant_culture_info_4; }
	inline void set_invariant_culture_info_4(CultureInfo_t3500843524 * value)
	{
		___invariant_culture_info_4 = value;
		Il2CppCodeGenWriteBarrier((&___invariant_culture_info_4), value);
	}

	inline static int32_t get_offset_of_shared_table_lock_5() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524_StaticFields, ___shared_table_lock_5)); }
	inline RuntimeObject * get_shared_table_lock_5() const { return ___shared_table_lock_5; }
	inline RuntimeObject ** get_address_of_shared_table_lock_5() { return &___shared_table_lock_5; }
	inline void set_shared_table_lock_5(RuntimeObject * value)
	{
		___shared_table_lock_5 = value;
		Il2CppCodeGenWriteBarrier((&___shared_table_lock_5), value);
	}

	inline static int32_t get_offset_of_BootstrapCultureID_6() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524_StaticFields, ___BootstrapCultureID_6)); }
	inline int32_t get_BootstrapCultureID_6() const { return ___BootstrapCultureID_6; }
	inline int32_t* get_address_of_BootstrapCultureID_6() { return &___BootstrapCultureID_6; }
	inline void set_BootstrapCultureID_6(int32_t value)
	{
		___BootstrapCultureID_6 = value;
	}

	inline static int32_t get_offset_of_MSG_READONLY_35() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524_StaticFields, ___MSG_READONLY_35)); }
	inline String_t* get_MSG_READONLY_35() const { return ___MSG_READONLY_35; }
	inline String_t** get_address_of_MSG_READONLY_35() { return &___MSG_READONLY_35; }
	inline void set_MSG_READONLY_35(String_t* value)
	{
		___MSG_READONLY_35 = value;
		Il2CppCodeGenWriteBarrier((&___MSG_READONLY_35), value);
	}

	inline static int32_t get_offset_of_shared_by_number_36() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524_StaticFields, ___shared_by_number_36)); }
	inline Hashtable_t909839986 * get_shared_by_number_36() const { return ___shared_by_number_36; }
	inline Hashtable_t909839986 ** get_address_of_shared_by_number_36() { return &___shared_by_number_36; }
	inline void set_shared_by_number_36(Hashtable_t909839986 * value)
	{
		___shared_by_number_36 = value;
		Il2CppCodeGenWriteBarrier((&___shared_by_number_36), value);
	}

	inline static int32_t get_offset_of_shared_by_name_37() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524_StaticFields, ___shared_by_name_37)); }
	inline Hashtable_t909839986 * get_shared_by_name_37() const { return ___shared_by_name_37; }
	inline Hashtable_t909839986 ** get_address_of_shared_by_name_37() { return &___shared_by_name_37; }
	inline void set_shared_by_name_37(Hashtable_t909839986 * value)
	{
		___shared_by_name_37 = value;
		Il2CppCodeGenWriteBarrier((&___shared_by_name_37), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map19_38() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524_StaticFields, ___U3CU3Ef__switchU24map19_38)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map19_38() const { return ___U3CU3Ef__switchU24map19_38; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map19_38() { return &___U3CU3Ef__switchU24map19_38; }
	inline void set_U3CU3Ef__switchU24map19_38(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map19_38 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map19_38), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1A_39() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524_StaticFields, ___U3CU3Ef__switchU24map1A_39)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map1A_39() const { return ___U3CU3Ef__switchU24map1A_39; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map1A_39() { return &___U3CU3Ef__switchU24map1A_39; }
	inline void set_U3CU3Ef__switchU24map1A_39(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map1A_39 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map1A_39), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CULTUREINFO_T3500843524_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef XPATHNODEITERATOR_T3192332357_H
#define XPATHNODEITERATOR_T3192332357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathNodeIterator
struct  XPathNodeIterator_t3192332357  : public RuntimeObject
{
public:
	// System.Int32 System.Xml.XPath.XPathNodeIterator::_count
	int32_t ____count_0;

public:
	inline static int32_t get_offset_of__count_0() { return static_cast<int32_t>(offsetof(XPathNodeIterator_t3192332357, ____count_0)); }
	inline int32_t get__count_0() const { return ____count_0; }
	inline int32_t* get_address_of__count_0() { return &____count_0; }
	inline void set__count_0(int32_t value)
	{
		____count_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHNODEITERATOR_T3192332357_H
#ifndef U24ARRAYTYPEU2412_T3672778807_H
#define U24ARRAYTYPEU2412_T3672778807_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType$12
struct  U24ArrayTypeU2412_t3672778807 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU2412_t3672778807__padding[12];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU2412_T3672778807_H
#ifndef SYSTEMEXCEPTION_T3877406272_H
#define SYSTEMEXCEPTION_T3877406272_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t3877406272  : public Exception_t1927440687
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T3877406272_H
#ifndef ENUMERABLEITERATOR_T1602910416_H
#define ENUMERABLEITERATOR_T1602910416_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathNavigator/EnumerableIterator
struct  EnumerableIterator_t1602910416  : public XPathNodeIterator_t3192332357
{
public:
	// System.Collections.IEnumerable System.Xml.XPath.XPathNavigator/EnumerableIterator::source
	RuntimeObject* ___source_1;
	// System.Collections.IEnumerator System.Xml.XPath.XPathNavigator/EnumerableIterator::e
	RuntimeObject* ___e_2;
	// System.Int32 System.Xml.XPath.XPathNavigator/EnumerableIterator::pos
	int32_t ___pos_3;

public:
	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(EnumerableIterator_t1602910416, ___source_1)); }
	inline RuntimeObject* get_source_1() const { return ___source_1; }
	inline RuntimeObject** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(RuntimeObject* value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier((&___source_1), value);
	}

	inline static int32_t get_offset_of_e_2() { return static_cast<int32_t>(offsetof(EnumerableIterator_t1602910416, ___e_2)); }
	inline RuntimeObject* get_e_2() const { return ___e_2; }
	inline RuntimeObject** get_address_of_e_2() { return &___e_2; }
	inline void set_e_2(RuntimeObject* value)
	{
		___e_2 = value;
		Il2CppCodeGenWriteBarrier((&___e_2), value);
	}

	inline static int32_t get_offset_of_pos_3() { return static_cast<int32_t>(offsetof(EnumerableIterator_t1602910416, ___pos_3)); }
	inline int32_t get_pos_3() const { return ___pos_3; }
	inline int32_t* get_address_of_pos_3() { return &___pos_3; }
	inline void set_pos_3(int32_t value)
	{
		___pos_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERABLEITERATOR_T1602910416_H
#ifndef UINT32_T2149682021_H
#define UINT32_T2149682021_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt32
struct  UInt32_t2149682021 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(UInt32_t2149682021, ___m_value_2)); }
	inline uint32_t get_m_value_2() const { return ___m_value_2; }
	inline uint32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(uint32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32_T2149682021_H
#ifndef XSLTCONTEXT_T2013960098_H
#define XSLTCONTEXT_T2013960098_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Xsl.XsltContext
struct  XsltContext_t2013960098  : public XmlNamespaceManager_t486731501
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSLTCONTEXT_T2013960098_H
#ifndef COMPILEDEXPRESSION_T3686330919_H
#define COMPILEDEXPRESSION_T3686330919_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.CompiledExpression
struct  CompiledExpression_t3686330919  : public XPathExpression_t452251917
{
public:
	// System.Xml.IXmlNamespaceResolver System.Xml.XPath.CompiledExpression::_nsm
	RuntimeObject* ____nsm_0;
	// System.Xml.XPath.Expression System.Xml.XPath.CompiledExpression::_expr
	Expression_t1283317256 * ____expr_1;
	// System.Xml.XPath.XPathSorters System.Xml.XPath.CompiledExpression::_sorters
	XPathSorters_t4019574815 * ____sorters_2;
	// System.String System.Xml.XPath.CompiledExpression::rawExpression
	String_t* ___rawExpression_3;

public:
	inline static int32_t get_offset_of__nsm_0() { return static_cast<int32_t>(offsetof(CompiledExpression_t3686330919, ____nsm_0)); }
	inline RuntimeObject* get__nsm_0() const { return ____nsm_0; }
	inline RuntimeObject** get_address_of__nsm_0() { return &____nsm_0; }
	inline void set__nsm_0(RuntimeObject* value)
	{
		____nsm_0 = value;
		Il2CppCodeGenWriteBarrier((&____nsm_0), value);
	}

	inline static int32_t get_offset_of__expr_1() { return static_cast<int32_t>(offsetof(CompiledExpression_t3686330919, ____expr_1)); }
	inline Expression_t1283317256 * get__expr_1() const { return ____expr_1; }
	inline Expression_t1283317256 ** get_address_of__expr_1() { return &____expr_1; }
	inline void set__expr_1(Expression_t1283317256 * value)
	{
		____expr_1 = value;
		Il2CppCodeGenWriteBarrier((&____expr_1), value);
	}

	inline static int32_t get_offset_of__sorters_2() { return static_cast<int32_t>(offsetof(CompiledExpression_t3686330919, ____sorters_2)); }
	inline XPathSorters_t4019574815 * get__sorters_2() const { return ____sorters_2; }
	inline XPathSorters_t4019574815 ** get_address_of__sorters_2() { return &____sorters_2; }
	inline void set__sorters_2(XPathSorters_t4019574815 * value)
	{
		____sorters_2 = value;
		Il2CppCodeGenWriteBarrier((&____sorters_2), value);
	}

	inline static int32_t get_offset_of_rawExpression_3() { return static_cast<int32_t>(offsetof(CompiledExpression_t3686330919, ___rawExpression_3)); }
	inline String_t* get_rawExpression_3() const { return ___rawExpression_3; }
	inline String_t** get_address_of_rawExpression_3() { return &___rawExpression_3; }
	inline void set_rawExpression_3(String_t* value)
	{
		___rawExpression_3 = value;
		Il2CppCodeGenWriteBarrier((&___rawExpression_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPILEDEXPRESSION_T3686330919_H
#ifndef EXPRNUMBER_T2687440122_H
#define EXPRNUMBER_T2687440122_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.ExprNumber
struct  ExprNumber_t2687440122  : public Expression_t1283317256
{
public:
	// System.Double System.Xml.XPath.ExprNumber::_value
	double ____value_0;

public:
	inline static int32_t get_offset_of__value_0() { return static_cast<int32_t>(offsetof(ExprNumber_t2687440122, ____value_0)); }
	inline double get__value_0() const { return ____value_0; }
	inline double* get_address_of__value_0() { return &____value_0; }
	inline void set__value_0(double value)
	{
		____value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRNUMBER_T2687440122_H
#ifndef U24ARRAYTYPEU24256_T2038352957_H
#define U24ARRAYTYPEU24256_T2038352957_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType$256
struct  U24ArrayTypeU24256_t2038352957 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU24256_t2038352957__padding[256];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU24256_T2038352957_H
#ifndef CHAR_T3454481338_H
#define CHAR_T3454481338_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Char
struct  Char_t3454481338 
{
public:
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Char_t3454481338, ___m_value_2)); }
	inline Il2CppChar get_m_value_2() const { return ___m_value_2; }
	inline Il2CppChar* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(Il2CppChar value)
	{
		___m_value_2 = value;
	}
};

struct Char_t3454481338_StaticFields
{
public:
	// System.Byte* System.Char::category_data
	uint8_t* ___category_data_3;
	// System.Byte* System.Char::numeric_data
	uint8_t* ___numeric_data_4;
	// System.Double* System.Char::numeric_data_values
	double* ___numeric_data_values_5;
	// System.UInt16* System.Char::to_lower_data_low
	uint16_t* ___to_lower_data_low_6;
	// System.UInt16* System.Char::to_lower_data_high
	uint16_t* ___to_lower_data_high_7;
	// System.UInt16* System.Char::to_upper_data_low
	uint16_t* ___to_upper_data_low_8;
	// System.UInt16* System.Char::to_upper_data_high
	uint16_t* ___to_upper_data_high_9;

public:
	inline static int32_t get_offset_of_category_data_3() { return static_cast<int32_t>(offsetof(Char_t3454481338_StaticFields, ___category_data_3)); }
	inline uint8_t* get_category_data_3() const { return ___category_data_3; }
	inline uint8_t** get_address_of_category_data_3() { return &___category_data_3; }
	inline void set_category_data_3(uint8_t* value)
	{
		___category_data_3 = value;
	}

	inline static int32_t get_offset_of_numeric_data_4() { return static_cast<int32_t>(offsetof(Char_t3454481338_StaticFields, ___numeric_data_4)); }
	inline uint8_t* get_numeric_data_4() const { return ___numeric_data_4; }
	inline uint8_t** get_address_of_numeric_data_4() { return &___numeric_data_4; }
	inline void set_numeric_data_4(uint8_t* value)
	{
		___numeric_data_4 = value;
	}

	inline static int32_t get_offset_of_numeric_data_values_5() { return static_cast<int32_t>(offsetof(Char_t3454481338_StaticFields, ___numeric_data_values_5)); }
	inline double* get_numeric_data_values_5() const { return ___numeric_data_values_5; }
	inline double** get_address_of_numeric_data_values_5() { return &___numeric_data_values_5; }
	inline void set_numeric_data_values_5(double* value)
	{
		___numeric_data_values_5 = value;
	}

	inline static int32_t get_offset_of_to_lower_data_low_6() { return static_cast<int32_t>(offsetof(Char_t3454481338_StaticFields, ___to_lower_data_low_6)); }
	inline uint16_t* get_to_lower_data_low_6() const { return ___to_lower_data_low_6; }
	inline uint16_t** get_address_of_to_lower_data_low_6() { return &___to_lower_data_low_6; }
	inline void set_to_lower_data_low_6(uint16_t* value)
	{
		___to_lower_data_low_6 = value;
	}

	inline static int32_t get_offset_of_to_lower_data_high_7() { return static_cast<int32_t>(offsetof(Char_t3454481338_StaticFields, ___to_lower_data_high_7)); }
	inline uint16_t* get_to_lower_data_high_7() const { return ___to_lower_data_high_7; }
	inline uint16_t** get_address_of_to_lower_data_high_7() { return &___to_lower_data_high_7; }
	inline void set_to_lower_data_high_7(uint16_t* value)
	{
		___to_lower_data_high_7 = value;
	}

	inline static int32_t get_offset_of_to_upper_data_low_8() { return static_cast<int32_t>(offsetof(Char_t3454481338_StaticFields, ___to_upper_data_low_8)); }
	inline uint16_t* get_to_upper_data_low_8() const { return ___to_upper_data_low_8; }
	inline uint16_t** get_address_of_to_upper_data_low_8() { return &___to_upper_data_low_8; }
	inline void set_to_upper_data_low_8(uint16_t* value)
	{
		___to_upper_data_low_8 = value;
	}

	inline static int32_t get_offset_of_to_upper_data_high_9() { return static_cast<int32_t>(offsetof(Char_t3454481338_StaticFields, ___to_upper_data_high_9)); }
	inline uint16_t* get_to_upper_data_high_9() const { return ___to_upper_data_high_9; }
	inline uint16_t** get_address_of_to_upper_data_high_9() { return &___to_upper_data_high_9; }
	inline void set_to_upper_data_high_9(uint16_t* value)
	{
		___to_upper_data_high_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHAR_T3454481338_H
#ifndef INT32_T2071877448_H
#define INT32_T2071877448_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2071877448 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2071877448, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2071877448_H
#ifndef VOID_T1841601450_H
#define VOID_T1841601450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1841601450 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1841601450_H
#ifndef XPATHNAVIGATOR_T3981235968_H
#define XPATHNAVIGATOR_T3981235968_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathNavigator
struct  XPathNavigator_t3981235968  : public XPathItem_t3130801258
{
public:

public:
};

struct XPathNavigator_t3981235968_StaticFields
{
public:
	// System.Char[] System.Xml.XPath.XPathNavigator::escape_text_chars
	CharU5BU5D_t1328083999* ___escape_text_chars_0;
	// System.Char[] System.Xml.XPath.XPathNavigator::escape_attr_chars
	CharU5BU5D_t1328083999* ___escape_attr_chars_1;

public:
	inline static int32_t get_offset_of_escape_text_chars_0() { return static_cast<int32_t>(offsetof(XPathNavigator_t3981235968_StaticFields, ___escape_text_chars_0)); }
	inline CharU5BU5D_t1328083999* get_escape_text_chars_0() const { return ___escape_text_chars_0; }
	inline CharU5BU5D_t1328083999** get_address_of_escape_text_chars_0() { return &___escape_text_chars_0; }
	inline void set_escape_text_chars_0(CharU5BU5D_t1328083999* value)
	{
		___escape_text_chars_0 = value;
		Il2CppCodeGenWriteBarrier((&___escape_text_chars_0), value);
	}

	inline static int32_t get_offset_of_escape_attr_chars_1() { return static_cast<int32_t>(offsetof(XPathNavigator_t3981235968_StaticFields, ___escape_attr_chars_1)); }
	inline CharU5BU5D_t1328083999* get_escape_attr_chars_1() const { return ___escape_attr_chars_1; }
	inline CharU5BU5D_t1328083999** get_address_of_escape_attr_chars_1() { return &___escape_attr_chars_1; }
	inline void set_escape_attr_chars_1(CharU5BU5D_t1328083999* value)
	{
		___escape_attr_chars_1 = value;
		Il2CppCodeGenWriteBarrier((&___escape_attr_chars_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHNAVIGATOR_T3981235968_H
#ifndef U24ARRAYTYPEU241280_T628910058_H
#define U24ARRAYTYPEU241280_T628910058_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType$1280
struct  U24ArrayTypeU241280_t628910058 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU241280_t628910058__padding[1280];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU241280_T628910058_H
#ifndef BOOLEAN_T3825574718_H
#define BOOLEAN_T3825574718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t3825574718 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t3825574718, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t3825574718_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t3825574718_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t3825574718_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T3825574718_H
#ifndef XPATHFUNCTION_T759167395_H
#define XPATHFUNCTION_T759167395_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathFunction
struct  XPathFunction_t759167395  : public Expression_t1283317256
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHFUNCTION_T759167395_H
#ifndef BASEITERATOR_T2454437973_H
#define BASEITERATOR_T2454437973_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.BaseIterator
struct  BaseIterator_t2454437973  : public XPathNodeIterator_t3192332357
{
public:
	// System.Xml.IXmlNamespaceResolver System.Xml.XPath.BaseIterator::_nsm
	RuntimeObject* ____nsm_1;
	// System.Int32 System.Xml.XPath.BaseIterator::position
	int32_t ___position_2;

public:
	inline static int32_t get_offset_of__nsm_1() { return static_cast<int32_t>(offsetof(BaseIterator_t2454437973, ____nsm_1)); }
	inline RuntimeObject* get__nsm_1() const { return ____nsm_1; }
	inline RuntimeObject** get_address_of__nsm_1() { return &____nsm_1; }
	inline void set__nsm_1(RuntimeObject* value)
	{
		____nsm_1 = value;
		Il2CppCodeGenWriteBarrier((&____nsm_1), value);
	}

	inline static int32_t get_offset_of_position_2() { return static_cast<int32_t>(offsetof(BaseIterator_t2454437973, ___position_2)); }
	inline int32_t get_position_2() const { return ___position_2; }
	inline int32_t* get_address_of_position_2() { return &___position_2; }
	inline void set_position_2(int32_t value)
	{
		___position_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEITERATOR_T2454437973_H
#ifndef DOUBLE_T4078015681_H
#define DOUBLE_T4078015681_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Double
struct  Double_t4078015681 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_13;

public:
	inline static int32_t get_offset_of_m_value_13() { return static_cast<int32_t>(offsetof(Double_t4078015681, ___m_value_13)); }
	inline double get_m_value_13() const { return ___m_value_13; }
	inline double* get_address_of_m_value_13() { return &___m_value_13; }
	inline void set_m_value_13(double value)
	{
		___m_value_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLE_T4078015681_H
#ifndef U24ARRAYTYPEU24208_T1278838065_H
#define U24ARRAYTYPEU24208_T1278838065_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType$208
struct  U24ArrayTypeU24208_t1278838065 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU24208_t1278838065__padding[208];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU24208_T1278838065_H
#ifndef U24ARRAYTYPEU24236_T2844922008_H
#define U24ARRAYTYPEU24236_T2844922008_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType$236
struct  U24ArrayTypeU24236_t2844922008 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU24236_t2844922008__padding[236];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU24236_T2844922008_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t1328083999* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t1328083999* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t1328083999** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t1328083999* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	IntPtr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline IntPtr_t get_Zero_1() const { return ___Zero_1; }
	inline IntPtr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(IntPtr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef U24ARRAYTYPEU248_T1957337328_H
#define U24ARRAYTYPEU248_T1957337328_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType$8
struct  U24ArrayTypeU248_t1957337328 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU248_t1957337328__padding[8];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU248_T1957337328_H
#ifndef U24ARRAYTYPEU241532_T1327816026_H
#define U24ARRAYTYPEU241532_T1327816026_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType$1532
struct  U24ArrayTypeU241532_t1327816026 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU241532_t1327816026__padding[1532];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU241532_T1327816026_H
#ifndef U24ARRAYTYPEU2472_T3672778801_H
#define U24ARRAYTYPEU2472_T3672778801_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType$72
struct  U24ArrayTypeU2472_t3672778801 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU2472_t3672778801__padding[72];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU2472_T3672778801_H
#ifndef SIMPLEITERATOR_T833624542_H
#define SIMPLEITERATOR_T833624542_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.SimpleIterator
struct  SimpleIterator_t833624542  : public BaseIterator_t2454437973
{
public:
	// System.Xml.XPath.XPathNavigator System.Xml.XPath.SimpleIterator::_nav
	XPathNavigator_t3981235968 * ____nav_3;
	// System.Xml.XPath.XPathNavigator System.Xml.XPath.SimpleIterator::_current
	XPathNavigator_t3981235968 * ____current_4;
	// System.Boolean System.Xml.XPath.SimpleIterator::skipfirst
	bool ___skipfirst_5;

public:
	inline static int32_t get_offset_of__nav_3() { return static_cast<int32_t>(offsetof(SimpleIterator_t833624542, ____nav_3)); }
	inline XPathNavigator_t3981235968 * get__nav_3() const { return ____nav_3; }
	inline XPathNavigator_t3981235968 ** get_address_of__nav_3() { return &____nav_3; }
	inline void set__nav_3(XPathNavigator_t3981235968 * value)
	{
		____nav_3 = value;
		Il2CppCodeGenWriteBarrier((&____nav_3), value);
	}

	inline static int32_t get_offset_of__current_4() { return static_cast<int32_t>(offsetof(SimpleIterator_t833624542, ____current_4)); }
	inline XPathNavigator_t3981235968 * get__current_4() const { return ____current_4; }
	inline XPathNavigator_t3981235968 ** get_address_of__current_4() { return &____current_4; }
	inline void set__current_4(XPathNavigator_t3981235968 * value)
	{
		____current_4 = value;
		Il2CppCodeGenWriteBarrier((&____current_4), value);
	}

	inline static int32_t get_offset_of_skipfirst_5() { return static_cast<int32_t>(offsetof(SimpleIterator_t833624542, ___skipfirst_5)); }
	inline bool get_skipfirst_5() const { return ___skipfirst_5; }
	inline bool* get_address_of_skipfirst_5() { return &___skipfirst_5; }
	inline void set_skipfirst_5(bool value)
	{
		___skipfirst_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLEITERATOR_T833624542_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1486305141_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1486305141_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t1486305141  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType$208 <PrivateImplementationDetails>::$$field-0
	U24ArrayTypeU24208_t1278838065  ___U24U24fieldU2D0_0;
	// <PrivateImplementationDetails>/$ArrayType$208 <PrivateImplementationDetails>::$$field-1
	U24ArrayTypeU24208_t1278838065  ___U24U24fieldU2D1_1;
	// <PrivateImplementationDetails>/$ArrayType$236 <PrivateImplementationDetails>::$$field-2
	U24ArrayTypeU24236_t2844922008  ___U24U24fieldU2D2_2;
	// <PrivateImplementationDetails>/$ArrayType$72 <PrivateImplementationDetails>::$$field-3
	U24ArrayTypeU2472_t3672778801  ___U24U24fieldU2D3_3;
	// <PrivateImplementationDetails>/$ArrayType$236 <PrivateImplementationDetails>::$$field-4
	U24ArrayTypeU24236_t2844922008  ___U24U24fieldU2D4_4;
	// <PrivateImplementationDetails>/$ArrayType$236 <PrivateImplementationDetails>::$$field-5
	U24ArrayTypeU24236_t2844922008  ___U24U24fieldU2D5_5;
	// <PrivateImplementationDetails>/$ArrayType$72 <PrivateImplementationDetails>::$$field-6
	U24ArrayTypeU2472_t3672778801  ___U24U24fieldU2D6_6;
	// <PrivateImplementationDetails>/$ArrayType$1532 <PrivateImplementationDetails>::$$field-7
	U24ArrayTypeU241532_t1327816026  ___U24U24fieldU2D7_7;
	// <PrivateImplementationDetails>/$ArrayType$1532 <PrivateImplementationDetails>::$$field-8
	U24ArrayTypeU241532_t1327816026  ___U24U24fieldU2D8_8;
	// <PrivateImplementationDetails>/$ArrayType$12 <PrivateImplementationDetails>::$$field-18
	U24ArrayTypeU2412_t3672778807  ___U24U24fieldU2D18_9;
	// <PrivateImplementationDetails>/$ArrayType$8 <PrivateImplementationDetails>::$$field-25
	U24ArrayTypeU248_t1957337328  ___U24U24fieldU2D25_10;
	// <PrivateImplementationDetails>/$ArrayType$8 <PrivateImplementationDetails>::$$field-26
	U24ArrayTypeU248_t1957337328  ___U24U24fieldU2D26_11;
	// <PrivateImplementationDetails>/$ArrayType$256 <PrivateImplementationDetails>::$$field-27
	U24ArrayTypeU24256_t2038352957  ___U24U24fieldU2D27_12;
	// <PrivateImplementationDetails>/$ArrayType$256 <PrivateImplementationDetails>::$$field-28
	U24ArrayTypeU24256_t2038352957  ___U24U24fieldU2D28_13;
	// <PrivateImplementationDetails>/$ArrayType$1280 <PrivateImplementationDetails>::$$field-29
	U24ArrayTypeU241280_t628910058  ___U24U24fieldU2D29_14;

public:
	inline static int32_t get_offset_of_U24U24fieldU2D0_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields, ___U24U24fieldU2D0_0)); }
	inline U24ArrayTypeU24208_t1278838065  get_U24U24fieldU2D0_0() const { return ___U24U24fieldU2D0_0; }
	inline U24ArrayTypeU24208_t1278838065 * get_address_of_U24U24fieldU2D0_0() { return &___U24U24fieldU2D0_0; }
	inline void set_U24U24fieldU2D0_0(U24ArrayTypeU24208_t1278838065  value)
	{
		___U24U24fieldU2D0_0 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D1_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields, ___U24U24fieldU2D1_1)); }
	inline U24ArrayTypeU24208_t1278838065  get_U24U24fieldU2D1_1() const { return ___U24U24fieldU2D1_1; }
	inline U24ArrayTypeU24208_t1278838065 * get_address_of_U24U24fieldU2D1_1() { return &___U24U24fieldU2D1_1; }
	inline void set_U24U24fieldU2D1_1(U24ArrayTypeU24208_t1278838065  value)
	{
		___U24U24fieldU2D1_1 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D2_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields, ___U24U24fieldU2D2_2)); }
	inline U24ArrayTypeU24236_t2844922008  get_U24U24fieldU2D2_2() const { return ___U24U24fieldU2D2_2; }
	inline U24ArrayTypeU24236_t2844922008 * get_address_of_U24U24fieldU2D2_2() { return &___U24U24fieldU2D2_2; }
	inline void set_U24U24fieldU2D2_2(U24ArrayTypeU24236_t2844922008  value)
	{
		___U24U24fieldU2D2_2 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D3_3() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields, ___U24U24fieldU2D3_3)); }
	inline U24ArrayTypeU2472_t3672778801  get_U24U24fieldU2D3_3() const { return ___U24U24fieldU2D3_3; }
	inline U24ArrayTypeU2472_t3672778801 * get_address_of_U24U24fieldU2D3_3() { return &___U24U24fieldU2D3_3; }
	inline void set_U24U24fieldU2D3_3(U24ArrayTypeU2472_t3672778801  value)
	{
		___U24U24fieldU2D3_3 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D4_4() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields, ___U24U24fieldU2D4_4)); }
	inline U24ArrayTypeU24236_t2844922008  get_U24U24fieldU2D4_4() const { return ___U24U24fieldU2D4_4; }
	inline U24ArrayTypeU24236_t2844922008 * get_address_of_U24U24fieldU2D4_4() { return &___U24U24fieldU2D4_4; }
	inline void set_U24U24fieldU2D4_4(U24ArrayTypeU24236_t2844922008  value)
	{
		___U24U24fieldU2D4_4 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D5_5() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields, ___U24U24fieldU2D5_5)); }
	inline U24ArrayTypeU24236_t2844922008  get_U24U24fieldU2D5_5() const { return ___U24U24fieldU2D5_5; }
	inline U24ArrayTypeU24236_t2844922008 * get_address_of_U24U24fieldU2D5_5() { return &___U24U24fieldU2D5_5; }
	inline void set_U24U24fieldU2D5_5(U24ArrayTypeU24236_t2844922008  value)
	{
		___U24U24fieldU2D5_5 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D6_6() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields, ___U24U24fieldU2D6_6)); }
	inline U24ArrayTypeU2472_t3672778801  get_U24U24fieldU2D6_6() const { return ___U24U24fieldU2D6_6; }
	inline U24ArrayTypeU2472_t3672778801 * get_address_of_U24U24fieldU2D6_6() { return &___U24U24fieldU2D6_6; }
	inline void set_U24U24fieldU2D6_6(U24ArrayTypeU2472_t3672778801  value)
	{
		___U24U24fieldU2D6_6 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D7_7() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields, ___U24U24fieldU2D7_7)); }
	inline U24ArrayTypeU241532_t1327816026  get_U24U24fieldU2D7_7() const { return ___U24U24fieldU2D7_7; }
	inline U24ArrayTypeU241532_t1327816026 * get_address_of_U24U24fieldU2D7_7() { return &___U24U24fieldU2D7_7; }
	inline void set_U24U24fieldU2D7_7(U24ArrayTypeU241532_t1327816026  value)
	{
		___U24U24fieldU2D7_7 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D8_8() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields, ___U24U24fieldU2D8_8)); }
	inline U24ArrayTypeU241532_t1327816026  get_U24U24fieldU2D8_8() const { return ___U24U24fieldU2D8_8; }
	inline U24ArrayTypeU241532_t1327816026 * get_address_of_U24U24fieldU2D8_8() { return &___U24U24fieldU2D8_8; }
	inline void set_U24U24fieldU2D8_8(U24ArrayTypeU241532_t1327816026  value)
	{
		___U24U24fieldU2D8_8 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D18_9() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields, ___U24U24fieldU2D18_9)); }
	inline U24ArrayTypeU2412_t3672778807  get_U24U24fieldU2D18_9() const { return ___U24U24fieldU2D18_9; }
	inline U24ArrayTypeU2412_t3672778807 * get_address_of_U24U24fieldU2D18_9() { return &___U24U24fieldU2D18_9; }
	inline void set_U24U24fieldU2D18_9(U24ArrayTypeU2412_t3672778807  value)
	{
		___U24U24fieldU2D18_9 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D25_10() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields, ___U24U24fieldU2D25_10)); }
	inline U24ArrayTypeU248_t1957337328  get_U24U24fieldU2D25_10() const { return ___U24U24fieldU2D25_10; }
	inline U24ArrayTypeU248_t1957337328 * get_address_of_U24U24fieldU2D25_10() { return &___U24U24fieldU2D25_10; }
	inline void set_U24U24fieldU2D25_10(U24ArrayTypeU248_t1957337328  value)
	{
		___U24U24fieldU2D25_10 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D26_11() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields, ___U24U24fieldU2D26_11)); }
	inline U24ArrayTypeU248_t1957337328  get_U24U24fieldU2D26_11() const { return ___U24U24fieldU2D26_11; }
	inline U24ArrayTypeU248_t1957337328 * get_address_of_U24U24fieldU2D26_11() { return &___U24U24fieldU2D26_11; }
	inline void set_U24U24fieldU2D26_11(U24ArrayTypeU248_t1957337328  value)
	{
		___U24U24fieldU2D26_11 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D27_12() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields, ___U24U24fieldU2D27_12)); }
	inline U24ArrayTypeU24256_t2038352957  get_U24U24fieldU2D27_12() const { return ___U24U24fieldU2D27_12; }
	inline U24ArrayTypeU24256_t2038352957 * get_address_of_U24U24fieldU2D27_12() { return &___U24U24fieldU2D27_12; }
	inline void set_U24U24fieldU2D27_12(U24ArrayTypeU24256_t2038352957  value)
	{
		___U24U24fieldU2D27_12 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D28_13() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields, ___U24U24fieldU2D28_13)); }
	inline U24ArrayTypeU24256_t2038352957  get_U24U24fieldU2D28_13() const { return ___U24U24fieldU2D28_13; }
	inline U24ArrayTypeU24256_t2038352957 * get_address_of_U24U24fieldU2D28_13() { return &___U24U24fieldU2D28_13; }
	inline void set_U24U24fieldU2D28_13(U24ArrayTypeU24256_t2038352957  value)
	{
		___U24U24fieldU2D28_13 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D29_14() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields, ___U24U24fieldU2D29_14)); }
	inline U24ArrayTypeU241280_t628910058  get_U24U24fieldU2D29_14() const { return ___U24U24fieldU2D29_14; }
	inline U24ArrayTypeU241280_t628910058 * get_address_of_U24U24fieldU2D29_14() { return &___U24U24fieldU2D29_14; }
	inline void set_U24U24fieldU2D29_14(U24ArrayTypeU241280_t628910058  value)
	{
		___U24U24fieldU2D29_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1486305141_H
#ifndef LISTITERATOR_T3804705054_H
#define LISTITERATOR_T3804705054_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.ListIterator
struct  ListIterator_t3804705054  : public BaseIterator_t2454437973
{
public:
	// System.Collections.IList System.Xml.XPath.ListIterator::_list
	RuntimeObject* ____list_3;

public:
	inline static int32_t get_offset_of__list_3() { return static_cast<int32_t>(offsetof(ListIterator_t3804705054, ____list_3)); }
	inline RuntimeObject* get__list_3() const { return ____list_3; }
	inline RuntimeObject** get_address_of__list_3() { return &____list_3; }
	inline void set__list_3(RuntimeObject* value)
	{
		____list_3 = value;
		Il2CppCodeGenWriteBarrier((&____list_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTITERATOR_T3804705054_H
#ifndef ARITHMETICEXCEPTION_T3261462543_H
#define ARITHMETICEXCEPTION_T3261462543_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArithmeticException
struct  ArithmeticException_t3261462543  : public SystemException_t3877406272
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARITHMETICEXCEPTION_T3261462543_H
#ifndef XMLDATATYPE_T315095065_H
#define XMLDATATYPE_T315095065_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XmlDataType
struct  XmlDataType_t315095065 
{
public:
	// System.Int32 System.Xml.XPath.XmlDataType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XmlDataType_t315095065, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLDATATYPE_T315095065_H
#ifndef WRAPPERITERATOR_T2718786873_H
#define WRAPPERITERATOR_T2718786873_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.WrapperIterator
struct  WrapperIterator_t2718786873  : public BaseIterator_t2454437973
{
public:
	// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.WrapperIterator::iter
	XPathNodeIterator_t3192332357 * ___iter_3;

public:
	inline static int32_t get_offset_of_iter_3() { return static_cast<int32_t>(offsetof(WrapperIterator_t2718786873, ___iter_3)); }
	inline XPathNodeIterator_t3192332357 * get_iter_3() const { return ___iter_3; }
	inline XPathNodeIterator_t3192332357 ** get_address_of_iter_3() { return &___iter_3; }
	inline void set_iter_3(XPathNodeIterator_t3192332357 * value)
	{
		___iter_3 = value;
		Il2CppCodeGenWriteBarrier((&___iter_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRAPPERITERATOR_T2718786873_H
#ifndef NOTSUPPORTEDEXCEPTION_T1793819818_H
#define NOTSUPPORTEDEXCEPTION_T1793819818_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.NotSupportedException
struct  NotSupportedException_t1793819818  : public SystemException_t3877406272
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTSUPPORTEDEXCEPTION_T1793819818_H
#ifndef XPATHFUNCTIONPOSITION_T3711683936_H
#define XPATHFUNCTIONPOSITION_T3711683936_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathFunctionPosition
struct  XPathFunctionPosition_t3711683936  : public XPathFunction_t759167395
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHFUNCTIONPOSITION_T3711683936_H
#ifndef XPATHNUMERICFUNCTION_T2367269690_H
#define XPATHNUMERICFUNCTION_T2367269690_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathNumericFunction
struct  XPathNumericFunction_t2367269690  : public XPathFunction_t759167395
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHNUMERICFUNCTION_T2367269690_H
#ifndef XPATHBOOLEANFUNCTION_T2511646183_H
#define XPATHBOOLEANFUNCTION_T2511646183_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathBooleanFunction
struct  XPathBooleanFunction_t2511646183  : public XPathFunction_t759167395
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHBOOLEANFUNCTION_T2511646183_H
#ifndef XPATHNODETYPE_T817388867_H
#define XPATHNODETYPE_T817388867_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathNodeType
struct  XPathNodeType_t817388867 
{
public:
	// System.Int32 System.Xml.XPath.XPathNodeType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XPathNodeType_t817388867, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHNODETYPE_T817388867_H
#ifndef FORMATEXCEPTION_T2948921286_H
#define FORMATEXCEPTION_T2948921286_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.FormatException
struct  FormatException_t2948921286  : public SystemException_t3877406272
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMATEXCEPTION_T2948921286_H
#ifndef NUMBERSTYLES_T3408984435_H
#define NUMBERSTYLES_T3408984435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Globalization.NumberStyles
struct  NumberStyles_t3408984435 
{
public:
	// System.Int32 System.Globalization.NumberStyles::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(NumberStyles_t3408984435, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NUMBERSTYLES_T3408984435_H
#ifndef ARGUMENTEXCEPTION_T3259014390_H
#define ARGUMENTEXCEPTION_T3259014390_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArgumentException
struct  ArgumentException_t3259014390  : public SystemException_t3877406272
{
public:
	// System.String System.ArgumentException::param_name
	String_t* ___param_name_12;

public:
	inline static int32_t get_offset_of_param_name_12() { return static_cast<int32_t>(offsetof(ArgumentException_t3259014390, ___param_name_12)); }
	inline String_t* get_param_name_12() const { return ___param_name_12; }
	inline String_t** get_address_of_param_name_12() { return &___param_name_12; }
	inline void set_param_name_12(String_t* value)
	{
		___param_name_12 = value;
		Il2CppCodeGenWriteBarrier((&___param_name_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTEXCEPTION_T3259014390_H
#ifndef XPATHFUNCTIONNORMALIZESPACE_T1288231156_H
#define XPATHFUNCTIONNORMALIZESPACE_T1288231156_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathFunctionNormalizeSpace
struct  XPathFunctionNormalizeSpace_t1288231156  : public XPathFunction_t759167395
{
public:
	// System.Xml.XPath.Expression System.Xml.XPath.XPathFunctionNormalizeSpace::arg0
	Expression_t1283317256 * ___arg0_0;

public:
	inline static int32_t get_offset_of_arg0_0() { return static_cast<int32_t>(offsetof(XPathFunctionNormalizeSpace_t1288231156, ___arg0_0)); }
	inline Expression_t1283317256 * get_arg0_0() const { return ___arg0_0; }
	inline Expression_t1283317256 ** get_address_of_arg0_0() { return &___arg0_0; }
	inline void set_arg0_0(Expression_t1283317256 * value)
	{
		___arg0_0 = value;
		Il2CppCodeGenWriteBarrier((&___arg0_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHFUNCTIONNORMALIZESPACE_T1288231156_H
#ifndef XPATHRESULTTYPE_T1521569578_H
#define XPATHRESULTTYPE_T1521569578_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathResultType
struct  XPathResultType_t1521569578 
{
public:
	// System.Int32 System.Xml.XPath.XPathResultType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XPathResultType_t1521569578, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHRESULTTYPE_T1521569578_H
#ifndef XPATHEXCEPTION_T1503722168_H
#define XPATHEXCEPTION_T1503722168_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathException
struct  XPathException_t1503722168  : public SystemException_t3877406272
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHEXCEPTION_T1503722168_H
#ifndef XPATHFUNCTIONLANG_T718178243_H
#define XPATHFUNCTIONLANG_T718178243_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathFunctionLang
struct  XPathFunctionLang_t718178243  : public XPathFunction_t759167395
{
public:
	// System.Xml.XPath.Expression System.Xml.XPath.XPathFunctionLang::arg0
	Expression_t1283317256 * ___arg0_0;

public:
	inline static int32_t get_offset_of_arg0_0() { return static_cast<int32_t>(offsetof(XPathFunctionLang_t718178243, ___arg0_0)); }
	inline Expression_t1283317256 * get_arg0_0() const { return ___arg0_0; }
	inline Expression_t1283317256 ** get_address_of_arg0_0() { return &___arg0_0; }
	inline void set_arg0_0(Expression_t1283317256 * value)
	{
		___arg0_0 = value;
		Il2CppCodeGenWriteBarrier((&___arg0_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHFUNCTIONLANG_T718178243_H
#ifndef XPATHFUNCTIONLAST_T2734600875_H
#define XPATHFUNCTIONLAST_T2734600875_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathFunctionLast
struct  XPathFunctionLast_t2734600875  : public XPathFunction_t759167395
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHFUNCTIONLAST_T2734600875_H
#ifndef XPATHFUNCTIONNAMESPACEURI_T448035136_H
#define XPATHFUNCTIONNAMESPACEURI_T448035136_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathFunctionNamespaceUri
struct  XPathFunctionNamespaceUri_t448035136  : public XPathFunction_t759167395
{
public:
	// System.Xml.XPath.Expression System.Xml.XPath.XPathFunctionNamespaceUri::arg0
	Expression_t1283317256 * ___arg0_0;

public:
	inline static int32_t get_offset_of_arg0_0() { return static_cast<int32_t>(offsetof(XPathFunctionNamespaceUri_t448035136, ___arg0_0)); }
	inline Expression_t1283317256 * get_arg0_0() const { return ___arg0_0; }
	inline Expression_t1283317256 ** get_address_of_arg0_0() { return &___arg0_0; }
	inline void set_arg0_0(Expression_t1283317256 * value)
	{
		___arg0_0 = value;
		Il2CppCodeGenWriteBarrier((&___arg0_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHFUNCTIONNAMESPACEURI_T448035136_H
#ifndef XPATHFUNCTIONNAME_T1880977850_H
#define XPATHFUNCTIONNAME_T1880977850_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathFunctionName
struct  XPathFunctionName_t1880977850  : public XPathFunction_t759167395
{
public:
	// System.Xml.XPath.Expression System.Xml.XPath.XPathFunctionName::arg0
	Expression_t1283317256 * ___arg0_0;

public:
	inline static int32_t get_offset_of_arg0_0() { return static_cast<int32_t>(offsetof(XPathFunctionName_t1880977850, ___arg0_0)); }
	inline Expression_t1283317256 * get_arg0_0() const { return ___arg0_0; }
	inline Expression_t1283317256 ** get_address_of_arg0_0() { return &___arg0_0; }
	inline void set_arg0_0(Expression_t1283317256 * value)
	{
		___arg0_0 = value;
		Il2CppCodeGenWriteBarrier((&___arg0_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHFUNCTIONNAME_T1880977850_H
#ifndef XPATHFUNCTIONLOCALNAME_T3935594891_H
#define XPATHFUNCTIONLOCALNAME_T3935594891_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathFunctionLocalName
struct  XPathFunctionLocalName_t3935594891  : public XPathFunction_t759167395
{
public:
	// System.Xml.XPath.Expression System.Xml.XPath.XPathFunctionLocalName::arg0
	Expression_t1283317256 * ___arg0_0;

public:
	inline static int32_t get_offset_of_arg0_0() { return static_cast<int32_t>(offsetof(XPathFunctionLocalName_t3935594891, ___arg0_0)); }
	inline Expression_t1283317256 * get_arg0_0() const { return ___arg0_0; }
	inline Expression_t1283317256 ** get_address_of_arg0_0() { return &___arg0_0; }
	inline void set_arg0_0(Expression_t1283317256 * value)
	{
		___arg0_0 = value;
		Il2CppCodeGenWriteBarrier((&___arg0_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHFUNCTIONLOCALNAME_T3935594891_H
#ifndef XPATHFUNCTIONSUBSTRINGBEFORE_T1797727619_H
#define XPATHFUNCTIONSUBSTRINGBEFORE_T1797727619_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathFunctionSubstringBefore
struct  XPathFunctionSubstringBefore_t1797727619  : public XPathFunction_t759167395
{
public:
	// System.Xml.XPath.Expression System.Xml.XPath.XPathFunctionSubstringBefore::arg0
	Expression_t1283317256 * ___arg0_0;
	// System.Xml.XPath.Expression System.Xml.XPath.XPathFunctionSubstringBefore::arg1
	Expression_t1283317256 * ___arg1_1;

public:
	inline static int32_t get_offset_of_arg0_0() { return static_cast<int32_t>(offsetof(XPathFunctionSubstringBefore_t1797727619, ___arg0_0)); }
	inline Expression_t1283317256 * get_arg0_0() const { return ___arg0_0; }
	inline Expression_t1283317256 ** get_address_of_arg0_0() { return &___arg0_0; }
	inline void set_arg0_0(Expression_t1283317256 * value)
	{
		___arg0_0 = value;
		Il2CppCodeGenWriteBarrier((&___arg0_0), value);
	}

	inline static int32_t get_offset_of_arg1_1() { return static_cast<int32_t>(offsetof(XPathFunctionSubstringBefore_t1797727619, ___arg1_1)); }
	inline Expression_t1283317256 * get_arg1_1() const { return ___arg1_1; }
	inline Expression_t1283317256 ** get_address_of_arg1_1() { return &___arg1_1; }
	inline void set_arg1_1(Expression_t1283317256 * value)
	{
		___arg1_1 = value;
		Il2CppCodeGenWriteBarrier((&___arg1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHFUNCTIONSUBSTRINGBEFORE_T1797727619_H
#ifndef XPATHFUNCTIONSUBSTRINGAFTER_T1560316700_H
#define XPATHFUNCTIONSUBSTRINGAFTER_T1560316700_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathFunctionSubstringAfter
struct  XPathFunctionSubstringAfter_t1560316700  : public XPathFunction_t759167395
{
public:
	// System.Xml.XPath.Expression System.Xml.XPath.XPathFunctionSubstringAfter::arg0
	Expression_t1283317256 * ___arg0_0;
	// System.Xml.XPath.Expression System.Xml.XPath.XPathFunctionSubstringAfter::arg1
	Expression_t1283317256 * ___arg1_1;

public:
	inline static int32_t get_offset_of_arg0_0() { return static_cast<int32_t>(offsetof(XPathFunctionSubstringAfter_t1560316700, ___arg0_0)); }
	inline Expression_t1283317256 * get_arg0_0() const { return ___arg0_0; }
	inline Expression_t1283317256 ** get_address_of_arg0_0() { return &___arg0_0; }
	inline void set_arg0_0(Expression_t1283317256 * value)
	{
		___arg0_0 = value;
		Il2CppCodeGenWriteBarrier((&___arg0_0), value);
	}

	inline static int32_t get_offset_of_arg1_1() { return static_cast<int32_t>(offsetof(XPathFunctionSubstringAfter_t1560316700, ___arg1_1)); }
	inline Expression_t1283317256 * get_arg1_1() const { return ___arg1_1; }
	inline Expression_t1283317256 ** get_address_of_arg1_1() { return &___arg1_1; }
	inline void set_arg1_1(Expression_t1283317256 * value)
	{
		___arg1_1 = value;
		Il2CppCodeGenWriteBarrier((&___arg1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHFUNCTIONSUBSTRINGAFTER_T1560316700_H
#ifndef XPATHFUNCTIONSUBSTRING_T2124841904_H
#define XPATHFUNCTIONSUBSTRING_T2124841904_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathFunctionSubstring
struct  XPathFunctionSubstring_t2124841904  : public XPathFunction_t759167395
{
public:
	// System.Xml.XPath.Expression System.Xml.XPath.XPathFunctionSubstring::arg0
	Expression_t1283317256 * ___arg0_0;
	// System.Xml.XPath.Expression System.Xml.XPath.XPathFunctionSubstring::arg1
	Expression_t1283317256 * ___arg1_1;
	// System.Xml.XPath.Expression System.Xml.XPath.XPathFunctionSubstring::arg2
	Expression_t1283317256 * ___arg2_2;

public:
	inline static int32_t get_offset_of_arg0_0() { return static_cast<int32_t>(offsetof(XPathFunctionSubstring_t2124841904, ___arg0_0)); }
	inline Expression_t1283317256 * get_arg0_0() const { return ___arg0_0; }
	inline Expression_t1283317256 ** get_address_of_arg0_0() { return &___arg0_0; }
	inline void set_arg0_0(Expression_t1283317256 * value)
	{
		___arg0_0 = value;
		Il2CppCodeGenWriteBarrier((&___arg0_0), value);
	}

	inline static int32_t get_offset_of_arg1_1() { return static_cast<int32_t>(offsetof(XPathFunctionSubstring_t2124841904, ___arg1_1)); }
	inline Expression_t1283317256 * get_arg1_1() const { return ___arg1_1; }
	inline Expression_t1283317256 ** get_address_of_arg1_1() { return &___arg1_1; }
	inline void set_arg1_1(Expression_t1283317256 * value)
	{
		___arg1_1 = value;
		Il2CppCodeGenWriteBarrier((&___arg1_1), value);
	}

	inline static int32_t get_offset_of_arg2_2() { return static_cast<int32_t>(offsetof(XPathFunctionSubstring_t2124841904, ___arg2_2)); }
	inline Expression_t1283317256 * get_arg2_2() const { return ___arg2_2; }
	inline Expression_t1283317256 ** get_address_of_arg2_2() { return &___arg2_2; }
	inline void set_arg2_2(Expression_t1283317256 * value)
	{
		___arg2_2 = value;
		Il2CppCodeGenWriteBarrier((&___arg2_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHFUNCTIONSUBSTRING_T2124841904_H
#ifndef XPATHFUNCTIONTRANSLATE_T2720664953_H
#define XPATHFUNCTIONTRANSLATE_T2720664953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathFunctionTranslate
struct  XPathFunctionTranslate_t2720664953  : public XPathFunction_t759167395
{
public:
	// System.Xml.XPath.Expression System.Xml.XPath.XPathFunctionTranslate::arg0
	Expression_t1283317256 * ___arg0_0;
	// System.Xml.XPath.Expression System.Xml.XPath.XPathFunctionTranslate::arg1
	Expression_t1283317256 * ___arg1_1;
	// System.Xml.XPath.Expression System.Xml.XPath.XPathFunctionTranslate::arg2
	Expression_t1283317256 * ___arg2_2;

public:
	inline static int32_t get_offset_of_arg0_0() { return static_cast<int32_t>(offsetof(XPathFunctionTranslate_t2720664953, ___arg0_0)); }
	inline Expression_t1283317256 * get_arg0_0() const { return ___arg0_0; }
	inline Expression_t1283317256 ** get_address_of_arg0_0() { return &___arg0_0; }
	inline void set_arg0_0(Expression_t1283317256 * value)
	{
		___arg0_0 = value;
		Il2CppCodeGenWriteBarrier((&___arg0_0), value);
	}

	inline static int32_t get_offset_of_arg1_1() { return static_cast<int32_t>(offsetof(XPathFunctionTranslate_t2720664953, ___arg1_1)); }
	inline Expression_t1283317256 * get_arg1_1() const { return ___arg1_1; }
	inline Expression_t1283317256 ** get_address_of_arg1_1() { return &___arg1_1; }
	inline void set_arg1_1(Expression_t1283317256 * value)
	{
		___arg1_1 = value;
		Il2CppCodeGenWriteBarrier((&___arg1_1), value);
	}

	inline static int32_t get_offset_of_arg2_2() { return static_cast<int32_t>(offsetof(XPathFunctionTranslate_t2720664953, ___arg2_2)); }
	inline Expression_t1283317256 * get_arg2_2() const { return ___arg2_2; }
	inline Expression_t1283317256 ** get_address_of_arg2_2() { return &___arg2_2; }
	inline void set_arg2_2(Expression_t1283317256 * value)
	{
		___arg2_2 = value;
		Il2CppCodeGenWriteBarrier((&___arg2_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHFUNCTIONTRANSLATE_T2720664953_H
#ifndef RUNTIMEFIELDHANDLE_T2331729674_H
#define RUNTIMEFIELDHANDLE_T2331729674_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeFieldHandle
struct  RuntimeFieldHandle_t2331729674 
{
public:
	// System.IntPtr System.RuntimeFieldHandle::value
	IntPtr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeFieldHandle_t2331729674, ___value_0)); }
	inline IntPtr_t get_value_0() const { return ___value_0; }
	inline IntPtr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(IntPtr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEFIELDHANDLE_T2331729674_H
#ifndef XPATHNAMESPACESCOPE_T3601604274_H
#define XPATHNAMESPACESCOPE_T3601604274_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathNamespaceScope
struct  XPathNamespaceScope_t3601604274 
{
public:
	// System.Int32 System.Xml.XPath.XPathNamespaceScope::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XPathNamespaceScope_t3601604274, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHNAMESPACESCOPE_T3601604274_H
#ifndef XMLNODEORDER_T930453011_H
#define XMLNODEORDER_T930453011_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNodeOrder
struct  XmlNodeOrder_t930453011 
{
public:
	// System.Int32 System.Xml.XmlNodeOrder::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XmlNodeOrder_t930453011, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNODEORDER_T930453011_H
#ifndef XPATHFUNCTIONSTRINGLENGTH_T2821584396_H
#define XPATHFUNCTIONSTRINGLENGTH_T2821584396_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathFunctionStringLength
struct  XPathFunctionStringLength_t2821584396  : public XPathFunction_t759167395
{
public:
	// System.Xml.XPath.Expression System.Xml.XPath.XPathFunctionStringLength::arg0
	Expression_t1283317256 * ___arg0_0;

public:
	inline static int32_t get_offset_of_arg0_0() { return static_cast<int32_t>(offsetof(XPathFunctionStringLength_t2821584396, ___arg0_0)); }
	inline Expression_t1283317256 * get_arg0_0() const { return ___arg0_0; }
	inline Expression_t1283317256 ** get_address_of_arg0_0() { return &___arg0_0; }
	inline void set_arg0_0(Expression_t1283317256 * value)
	{
		___arg0_0 = value;
		Il2CppCodeGenWriteBarrier((&___arg0_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHFUNCTIONSTRINGLENGTH_T2821584396_H
#ifndef XPATHFUNCTIONSTARTSWITH_T1855488236_H
#define XPATHFUNCTIONSTARTSWITH_T1855488236_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathFunctionStartsWith
struct  XPathFunctionStartsWith_t1855488236  : public XPathFunction_t759167395
{
public:
	// System.Xml.XPath.Expression System.Xml.XPath.XPathFunctionStartsWith::arg0
	Expression_t1283317256 * ___arg0_0;
	// System.Xml.XPath.Expression System.Xml.XPath.XPathFunctionStartsWith::arg1
	Expression_t1283317256 * ___arg1_1;

public:
	inline static int32_t get_offset_of_arg0_0() { return static_cast<int32_t>(offsetof(XPathFunctionStartsWith_t1855488236, ___arg0_0)); }
	inline Expression_t1283317256 * get_arg0_0() const { return ___arg0_0; }
	inline Expression_t1283317256 ** get_address_of_arg0_0() { return &___arg0_0; }
	inline void set_arg0_0(Expression_t1283317256 * value)
	{
		___arg0_0 = value;
		Il2CppCodeGenWriteBarrier((&___arg0_0), value);
	}

	inline static int32_t get_offset_of_arg1_1() { return static_cast<int32_t>(offsetof(XPathFunctionStartsWith_t1855488236, ___arg1_1)); }
	inline Expression_t1283317256 * get_arg1_1() const { return ___arg1_1; }
	inline Expression_t1283317256 ** get_address_of_arg1_1() { return &___arg1_1; }
	inline void set_arg1_1(Expression_t1283317256 * value)
	{
		___arg1_1 = value;
		Il2CppCodeGenWriteBarrier((&___arg1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHFUNCTIONSTARTSWITH_T1855488236_H
#ifndef XPATHFUNCTIONSTRING_T3963481524_H
#define XPATHFUNCTIONSTRING_T3963481524_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathFunctionString
struct  XPathFunctionString_t3963481524  : public XPathFunction_t759167395
{
public:
	// System.Xml.XPath.Expression System.Xml.XPath.XPathFunctionString::arg0
	Expression_t1283317256 * ___arg0_0;

public:
	inline static int32_t get_offset_of_arg0_0() { return static_cast<int32_t>(offsetof(XPathFunctionString_t3963481524, ___arg0_0)); }
	inline Expression_t1283317256 * get_arg0_0() const { return ___arg0_0; }
	inline Expression_t1283317256 ** get_address_of_arg0_0() { return &___arg0_0; }
	inline void set_arg0_0(Expression_t1283317256 * value)
	{
		___arg0_0 = value;
		Il2CppCodeGenWriteBarrier((&___arg0_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHFUNCTIONSTRING_T3963481524_H
#ifndef XPATHSORTER_T3491953490_H
#define XPATHSORTER_T3491953490_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathSorter
struct  XPathSorter_t3491953490  : public RuntimeObject
{
public:
	// System.Xml.XPath.Expression System.Xml.XPath.XPathSorter::_expr
	Expression_t1283317256 * ____expr_0;
	// System.Collections.IComparer System.Xml.XPath.XPathSorter::_cmp
	RuntimeObject* ____cmp_1;
	// System.Xml.XPath.XmlDataType System.Xml.XPath.XPathSorter::_type
	int32_t ____type_2;

public:
	inline static int32_t get_offset_of__expr_0() { return static_cast<int32_t>(offsetof(XPathSorter_t3491953490, ____expr_0)); }
	inline Expression_t1283317256 * get__expr_0() const { return ____expr_0; }
	inline Expression_t1283317256 ** get_address_of__expr_0() { return &____expr_0; }
	inline void set__expr_0(Expression_t1283317256 * value)
	{
		____expr_0 = value;
		Il2CppCodeGenWriteBarrier((&____expr_0), value);
	}

	inline static int32_t get_offset_of__cmp_1() { return static_cast<int32_t>(offsetof(XPathSorter_t3491953490, ____cmp_1)); }
	inline RuntimeObject* get__cmp_1() const { return ____cmp_1; }
	inline RuntimeObject** get_address_of__cmp_1() { return &____cmp_1; }
	inline void set__cmp_1(RuntimeObject* value)
	{
		____cmp_1 = value;
		Il2CppCodeGenWriteBarrier((&____cmp_1), value);
	}

	inline static int32_t get_offset_of__type_2() { return static_cast<int32_t>(offsetof(XPathSorter_t3491953490, ____type_2)); }
	inline int32_t get__type_2() const { return ____type_2; }
	inline int32_t* get_address_of__type_2() { return &____type_2; }
	inline void set__type_2(int32_t value)
	{
		____type_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHSORTER_T3491953490_H
#ifndef XPATHFUNCTIONTRUE_T1672815575_H
#define XPATHFUNCTIONTRUE_T1672815575_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathFunctionTrue
struct  XPathFunctionTrue_t1672815575  : public XPathBooleanFunction_t2511646183
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHFUNCTIONTRUE_T1672815575_H
#ifndef XPATHFUNCTIONROUND_T711107915_H
#define XPATHFUNCTIONROUND_T711107915_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathFunctionRound
struct  XPathFunctionRound_t711107915  : public XPathNumericFunction_t2367269690
{
public:
	// System.Xml.XPath.Expression System.Xml.XPath.XPathFunctionRound::arg0
	Expression_t1283317256 * ___arg0_0;

public:
	inline static int32_t get_offset_of_arg0_0() { return static_cast<int32_t>(offsetof(XPathFunctionRound_t711107915, ___arg0_0)); }
	inline Expression_t1283317256 * get_arg0_0() const { return ___arg0_0; }
	inline Expression_t1283317256 ** get_address_of_arg0_0() { return &___arg0_0; }
	inline void set_arg0_0(Expression_t1283317256 * value)
	{
		___arg0_0 = value;
		Il2CppCodeGenWriteBarrier((&___arg0_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHFUNCTIONROUND_T711107915_H
#ifndef OVERFLOWEXCEPTION_T1075868493_H
#define OVERFLOWEXCEPTION_T1075868493_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.OverflowException
struct  OverflowException_t1075868493  : public ArithmeticException_t3261462543
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OVERFLOWEXCEPTION_T1075868493_H
#ifndef SELFITERATOR_T1886393192_H
#define SELFITERATOR_T1886393192_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.SelfIterator
struct  SelfIterator_t1886393192  : public SimpleIterator_t833624542
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELFITERATOR_T1886393192_H
#ifndef XPATHFUNCTIONNOT_T1631674504_H
#define XPATHFUNCTIONNOT_T1631674504_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathFunctionNot
struct  XPathFunctionNot_t1631674504  : public XPathBooleanFunction_t2511646183
{
public:
	// System.Xml.XPath.Expression System.Xml.XPath.XPathFunctionNot::arg0
	Expression_t1283317256 * ___arg0_0;

public:
	inline static int32_t get_offset_of_arg0_0() { return static_cast<int32_t>(offsetof(XPathFunctionNot_t1631674504, ___arg0_0)); }
	inline Expression_t1283317256 * get_arg0_0() const { return ___arg0_0; }
	inline Expression_t1283317256 ** get_address_of_arg0_0() { return &___arg0_0; }
	inline void set_arg0_0(Expression_t1283317256 * value)
	{
		___arg0_0 = value;
		Il2CppCodeGenWriteBarrier((&___arg0_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHFUNCTIONNOT_T1631674504_H
#ifndef ARGUMENTNULLEXCEPTION_T628810857_H
#define ARGUMENTNULLEXCEPTION_T628810857_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArgumentNullException
struct  ArgumentNullException_t628810857  : public ArgumentException_t3259014390
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTNULLEXCEPTION_T628810857_H
#ifndef XPATHFUNCTIONNUMBER_T1210619362_H
#define XPATHFUNCTIONNUMBER_T1210619362_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathFunctionNumber
struct  XPathFunctionNumber_t1210619362  : public XPathNumericFunction_t2367269690
{
public:
	// System.Xml.XPath.Expression System.Xml.XPath.XPathFunctionNumber::arg0
	Expression_t1283317256 * ___arg0_0;

public:
	inline static int32_t get_offset_of_arg0_0() { return static_cast<int32_t>(offsetof(XPathFunctionNumber_t1210619362, ___arg0_0)); }
	inline Expression_t1283317256 * get_arg0_0() const { return ___arg0_0; }
	inline Expression_t1283317256 ** get_address_of_arg0_0() { return &___arg0_0; }
	inline void set_arg0_0(Expression_t1283317256 * value)
	{
		___arg0_0 = value;
		Il2CppCodeGenWriteBarrier((&___arg0_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHFUNCTIONNUMBER_T1210619362_H
#ifndef XPATHFUNCTIONSUM_T2438242634_H
#define XPATHFUNCTIONSUM_T2438242634_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathFunctionSum
struct  XPathFunctionSum_t2438242634  : public XPathNumericFunction_t2367269690
{
public:
	// System.Xml.XPath.Expression System.Xml.XPath.XPathFunctionSum::arg0
	Expression_t1283317256 * ___arg0_0;

public:
	inline static int32_t get_offset_of_arg0_0() { return static_cast<int32_t>(offsetof(XPathFunctionSum_t2438242634, ___arg0_0)); }
	inline Expression_t1283317256 * get_arg0_0() const { return ___arg0_0; }
	inline Expression_t1283317256 ** get_address_of_arg0_0() { return &___arg0_0; }
	inline void set_arg0_0(Expression_t1283317256 * value)
	{
		___arg0_0 = value;
		Il2CppCodeGenWriteBarrier((&___arg0_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHFUNCTIONSUM_T2438242634_H
#ifndef U3CENUMERATECHILDRENU3EC__ITERATOR0_T1235609798_H
#define U3CENUMERATECHILDRENU3EC__ITERATOR0_T1235609798_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathNavigator/<EnumerateChildren>c__Iterator0
struct  U3CEnumerateChildrenU3Ec__Iterator0_t1235609798  : public RuntimeObject
{
public:
	// System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator/<EnumerateChildren>c__Iterator0::n
	XPathNavigator_t3981235968 * ___n_0;
	// System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator/<EnumerateChildren>c__Iterator0::<nav>__0
	XPathNavigator_t3981235968 * ___U3CnavU3E__0_1;
	// System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator/<EnumerateChildren>c__Iterator0::<nav2>__1
	XPathNavigator_t3981235968 * ___U3Cnav2U3E__1_2;
	// System.Xml.XPath.XPathNodeType System.Xml.XPath.XPathNavigator/<EnumerateChildren>c__Iterator0::type
	int32_t ___type_3;
	// System.Int32 System.Xml.XPath.XPathNavigator/<EnumerateChildren>c__Iterator0::$PC
	int32_t ___U24PC_4;
	// System.Object System.Xml.XPath.XPathNavigator/<EnumerateChildren>c__Iterator0::$current
	RuntimeObject * ___U24current_5;
	// System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator/<EnumerateChildren>c__Iterator0::<$>n
	XPathNavigator_t3981235968 * ___U3CU24U3En_6;
	// System.Xml.XPath.XPathNodeType System.Xml.XPath.XPathNavigator/<EnumerateChildren>c__Iterator0::<$>type
	int32_t ___U3CU24U3Etype_7;

public:
	inline static int32_t get_offset_of_n_0() { return static_cast<int32_t>(offsetof(U3CEnumerateChildrenU3Ec__Iterator0_t1235609798, ___n_0)); }
	inline XPathNavigator_t3981235968 * get_n_0() const { return ___n_0; }
	inline XPathNavigator_t3981235968 ** get_address_of_n_0() { return &___n_0; }
	inline void set_n_0(XPathNavigator_t3981235968 * value)
	{
		___n_0 = value;
		Il2CppCodeGenWriteBarrier((&___n_0), value);
	}

	inline static int32_t get_offset_of_U3CnavU3E__0_1() { return static_cast<int32_t>(offsetof(U3CEnumerateChildrenU3Ec__Iterator0_t1235609798, ___U3CnavU3E__0_1)); }
	inline XPathNavigator_t3981235968 * get_U3CnavU3E__0_1() const { return ___U3CnavU3E__0_1; }
	inline XPathNavigator_t3981235968 ** get_address_of_U3CnavU3E__0_1() { return &___U3CnavU3E__0_1; }
	inline void set_U3CnavU3E__0_1(XPathNavigator_t3981235968 * value)
	{
		___U3CnavU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnavU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3Cnav2U3E__1_2() { return static_cast<int32_t>(offsetof(U3CEnumerateChildrenU3Ec__Iterator0_t1235609798, ___U3Cnav2U3E__1_2)); }
	inline XPathNavigator_t3981235968 * get_U3Cnav2U3E__1_2() const { return ___U3Cnav2U3E__1_2; }
	inline XPathNavigator_t3981235968 ** get_address_of_U3Cnav2U3E__1_2() { return &___U3Cnav2U3E__1_2; }
	inline void set_U3Cnav2U3E__1_2(XPathNavigator_t3981235968 * value)
	{
		___U3Cnav2U3E__1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cnav2U3E__1_2), value);
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(U3CEnumerateChildrenU3Ec__Iterator0_t1235609798, ___type_3)); }
	inline int32_t get_type_3() const { return ___type_3; }
	inline int32_t* get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(int32_t value)
	{
		___type_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CEnumerateChildrenU3Ec__Iterator0_t1235609798, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CEnumerateChildrenU3Ec__Iterator0_t1235609798, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U3CU24U3En_6() { return static_cast<int32_t>(offsetof(U3CEnumerateChildrenU3Ec__Iterator0_t1235609798, ___U3CU24U3En_6)); }
	inline XPathNavigator_t3981235968 * get_U3CU24U3En_6() const { return ___U3CU24U3En_6; }
	inline XPathNavigator_t3981235968 ** get_address_of_U3CU24U3En_6() { return &___U3CU24U3En_6; }
	inline void set_U3CU24U3En_6(XPathNavigator_t3981235968 * value)
	{
		___U3CU24U3En_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU24U3En_6), value);
	}

	inline static int32_t get_offset_of_U3CU24U3Etype_7() { return static_cast<int32_t>(offsetof(U3CEnumerateChildrenU3Ec__Iterator0_t1235609798, ___U3CU24U3Etype_7)); }
	inline int32_t get_U3CU24U3Etype_7() const { return ___U3CU24U3Etype_7; }
	inline int32_t* get_address_of_U3CU24U3Etype_7() { return &___U3CU24U3Etype_7; }
	inline void set_U3CU24U3Etype_7(int32_t value)
	{
		___U3CU24U3Etype_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CENUMERATECHILDRENU3EC__ITERATOR0_T1235609798_H
#ifndef NULLITERATOR_T2539636145_H
#define NULLITERATOR_T2539636145_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.NullIterator
struct  NullIterator_t2539636145  : public SelfIterator_t1886393192
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLITERATOR_T2539636145_H
// System.Char[]
struct CharU5BU5D_t1328083999  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Il2CppChar m_Items[1];

public:
	inline Il2CppChar GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppChar value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Il2CppChar GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppChar value)
	{
		m_Items[index] = value;
	}
};
// System.String[]
struct StringU5BU5D_t1642385972  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Object[]
struct ObjectU5BU5D_t3614634134  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Xml.XPath.XPathNavigator[]
struct XPathNavigatorU5BU5D_t2529815809  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) XPathNavigator_t3981235968 * m_Items[1];

public:
	inline XPathNavigator_t3981235968 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline XPathNavigator_t3981235968 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, XPathNavigator_t3981235968 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline XPathNavigator_t3981235968 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline XPathNavigator_t3981235968 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, XPathNavigator_t3981235968 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Xml.XPath.XPathResultType[]
struct XPathResultTypeU5BU5D_t2966113519  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};



// System.Void System.Xml.XPath.XPathFunction::.ctor(System.Xml.XPath.FunctionArguments)
extern "C"  void XPathFunction__ctor_m327599860 (XPathFunction_t759167395 * __this, FunctionArguments_t2900945452 * ___args0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Xml.XPath.FunctionArguments System.Xml.XPath.FunctionArguments::get_Tail()
extern "C"  FunctionArguments_t2900945452 * FunctionArguments_get_Tail_m3947258239 (FunctionArguments_t2900945452 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XPath.XPathException::.ctor(System.String)
extern "C"  void XPathException__ctor_m3605407358 (XPathException_t1503722168 * __this, String_t* ___message0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Xml.XPath.Expression System.Xml.XPath.FunctionArguments::get_Arg()
extern "C"  Expression_t1283317256 * FunctionArguments_get_Arg_m1790516491 (FunctionArguments_t2900945452 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Globalization.CultureInfo System.Globalization.CultureInfo::get_InvariantCulture()
extern "C"  CultureInfo_t3500843524 * CultureInfo_get_InvariantCulture_m398972276 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::ToLower(System.Globalization.CultureInfo)
extern "C"  String_t* String_ToLower_m743194025 (String_t* __this, CultureInfo_t3500843524 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::op_Equality(System.String,System.String)
extern "C"  bool String_op_Equality_m1790663636 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String[] System.String::Split(System.Char[])
extern "C"  StringU5BU5D_t1642385972* String_Split_m3326265864 (String_t* __this, CharU5BU5D_t1328083999* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String[])
extern "C"  String_t* String_Concat_m626692867 (RuntimeObject * __this /* static, unused */, StringU5BU5D_t1642385972* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String,System.String)
extern "C"  String_t* String_Concat_m612901809 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, String_t* p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.StringBuilder::.ctor()
extern "C"  void StringBuilder__ctor_m3946851802 (StringBuilder_t1221177846 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Char System.String::get_Chars(System.Int32)
extern "C"  Il2CppChar String_get_Chars_m4230566705 (String_t* __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.StringBuilder::get_Length()
extern "C"  int32_t StringBuilder_get_Length_m1608241323 (StringBuilder_t1221177846 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.Char)
extern "C"  StringBuilder_t1221177846 * StringBuilder_Append_m3618697540 (StringBuilder_t1221177846 * __this, Il2CppChar p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.String::get_Length()
extern "C"  int32_t String_get_Length_m1606060069 (String_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Text.StringBuilder::ToString()
extern "C"  String_t* StringBuilder_ToString_m1507807375 (StringBuilder_t1221177846 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XPath.XPathBooleanFunction::.ctor(System.Xml.XPath.FunctionArguments)
extern "C"  void XPathBooleanFunction__ctor_m620012104 (XPathBooleanFunction_t2511646183 * __this, FunctionArguments_t2900945452 * ___args0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XPath.XPathNumericFunction::.ctor(System.Xml.XPath.FunctionArguments)
extern "C"  void XPathNumericFunction__ctor_m2271660427 (XPathNumericFunction_t2367269690 * __this, FunctionArguments_t2900945452 * ___args0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XPath.ExprNumber::.ctor(System.Double)
extern "C"  void ExprNumber__ctor_m2550519548 (ExprNumber_t2687440122 * __this, double ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Double System.Xml.XPath.XPathFunctions::ToNumber(System.String)
extern "C"  double XPathFunctions_ToNumber_m2754819889 (RuntimeObject * __this /* static, unused */, String_t* ___arg0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Double System.Xml.XPath.XPathFunctionRound::Round(System.Double)
extern "C"  double XPathFunctionRound_Round_m791118100 (XPathFunctionRound_t711107915 * __this, double ___arg0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentNullException::.ctor()
extern "C"  void ArgumentNullException__ctor_m911049464 (ArgumentNullException_t628810857 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Double::IsNaN(System.Double)
extern "C"  bool Double_IsNaN_m2289494211 (RuntimeObject * __this /* static, unused */, double p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XPath.XPathFunctions::ToBoolean(System.Object)
extern "C"  bool XPathFunctions_ToBoolean_m2866337351 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___arg0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentException::.ctor()
extern "C"  void ArgumentException__ctor_m2105824819 (ArgumentException_t3259014390 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XPath.XPathFunctions::ToString(System.Double)
extern "C"  String_t* XPathFunctions_ToString_m2724627955 (RuntimeObject * __this /* static, unused */, double ___d0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Globalization.NumberFormatInfo System.Globalization.NumberFormatInfo::get_InvariantInfo()
extern "C"  NumberFormatInfo_t104580544 * NumberFormatInfo_get_InvariantInfo_m2658215204 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Double::ToString(System.String,System.IFormatProvider)
extern "C"  String_t* Double_ToString_m1474956491 (double* __this, String_t* p0, RuntimeObject* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XPath.XPathFunctions::ToString(System.Object)
extern "C"  String_t* XPathFunctions_ToString_m908387119 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___arg0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Double System.Convert::ToDouble(System.Boolean)
extern "C"  double Convert_ToDouble_m204912726 (RuntimeObject * __this /* static, unused */, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Trim(System.Char[])
extern "C"  String_t* String_Trim_m3982520224 (String_t* __this, CharU5BU5D_t1328083999* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.Object,System.Object)
extern "C"  String_t* String_Concat_m56707527 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Double System.Double::Parse(System.String,System.Globalization.NumberStyles,System.IFormatProvider)
extern "C"  double Double_Parse_m2344589511 (RuntimeObject * __this /* static, unused */, String_t* p0, int32_t p1, RuntimeObject* p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::StartsWith(System.String)
extern "C"  bool String_StartsWith_m1841920685 (String_t* __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Double::IsNegativeInfinity(System.Double)
extern "C"  bool Double_IsNegativeInfinity_m1111913183 (RuntimeObject * __this /* static, unused */, double p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Substring(System.Int32)
extern "C"  String_t* String_Substring_m2032624251 (String_t* __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Substring(System.Int32,System.Int32)
extern "C"  String_t* String_Substring_m12482732 (String_t* __this, int32_t p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.String::IndexOf(System.String)
extern "C"  int32_t String_IndexOf_m4251815737 (String_t* __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.StringBuilder::.ctor(System.Int32)
extern "C"  void StringBuilder__ctor_m536337337 (StringBuilder_t1221177846 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.String::IndexOf(System.Char)
extern "C"  int32_t String_IndexOf_m2358239236 (String_t* __this, Il2CppChar p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m2551263788 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XPath.XPathIteratorComparer::.ctor()
extern "C"  void XPathIteratorComparer__ctor_m2708098576 (XPathIteratorComparer_t2522471076 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XPath.XPathItem::.ctor()
extern "C"  void XPathItem__ctor_m341749466 (XPathItem_t3130801258 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.CompilerServices.RuntimeHelpers::InitializeArray(System.Array,System.RuntimeFieldHandle)
extern "C"  void RuntimeHelpers_InitializeArray_m3920580167 (RuntimeObject * __this /* static, unused */, RuntimeArray * p0, RuntimeFieldHandle_t2331729674  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XPath.XPathNavigator::MoveToNextNamespace()
extern "C"  bool XPathNavigator_MoveToNextNamespace_m3661588194 (XPathNavigator_t3981235968 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Xml.XPath.XPathExpression System.Xml.XPath.XPathExpression::Compile(System.String)
extern "C"  XPathExpression_t452251917 * XPathExpression_Compile_m2615308077 (RuntimeObject * __this /* static, unused */, String_t* ___xpath0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XPath.XPathNavigator::MoveToFirstNamespace()
extern "C"  bool XPathNavigator_MoveToFirstNamespace_m3677738759 (XPathNavigator_t3981235968 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XPath.XPathNavigator::MoveToFirstImpl()
extern "C"  bool XPathNavigator_MoveToFirstImpl_m1135523502 (XPathNavigator_t3981235968 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.XPathNavigator::Select(System.Xml.XPath.XPathExpression,System.Xml.IXmlNamespaceResolver)
extern "C"  XPathNodeIterator_t3192332357 * XPathNavigator_Select_m2045522580 (XPathNavigator_t3981235968 * __this, XPathExpression_t452251917 * ___expr0, RuntimeObject* ___ctx1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Xml.IXmlNamespaceResolver System.Xml.XPath.CompiledExpression::get_NamespaceManager()
extern "C"  RuntimeObject* CompiledExpression_get_NamespaceManager_m4093303772 (CompiledExpression_t3686330919 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XPath.NullIterator::.ctor(System.Xml.XPath.XPathNavigator,System.Xml.IXmlNamespaceResolver)
extern "C"  void NullIterator__ctor_m2816370923 (NullIterator_t2539636145 * __this, XPathNavigator_t3981235968 * ___nav0, RuntimeObject* ___nsm1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.CompiledExpression::EvaluateNodeSet(System.Xml.XPath.BaseIterator)
extern "C"  XPathNodeIterator_t3192332357 * CompiledExpression_EvaluateNodeSet_m4253810175 (CompiledExpression_t3686330919 * __this, BaseIterator_t2454437973 * ___iter0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XPath.XPathNavigator/<EnumerateChildren>c__Iterator0::.ctor()
extern "C"  void U3CEnumerateChildrenU3Ec__Iterator0__ctor_m2964582031 (U3CEnumerateChildrenU3Ec__Iterator0_t1235609798 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerable System.Xml.XPath.XPathNavigator::EnumerateChildren(System.Xml.XPath.XPathNavigator,System.Xml.XPath.XPathNodeType)
extern "C"  RuntimeObject* XPathNavigator_EnumerateChildren_m1107095336 (RuntimeObject * __this /* static, unused */, XPathNavigator_t3981235968 * ___n0, int32_t ___type1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XPath.XPathNavigator/EnumerableIterator::.ctor(System.Collections.IEnumerable,System.Int32)
extern "C"  void EnumerableIterator__ctor_m970393097 (EnumerableIterator_t1602910416 * __this, RuntimeObject* ___source0, int32_t ___pos1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XPath.WrapperIterator::.ctor(System.Xml.XPath.XPathNodeIterator,System.Xml.IXmlNamespaceResolver)
extern "C"  void WrapperIterator__ctor_m904768110 (WrapperIterator_t2718786873 * __this, XPathNodeIterator_t3192332357 * ___iter0, RuntimeObject* ___nsm1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.Object> System.Xml.XPath.XPathNavigator/<EnumerateChildren>c__Iterator0::System.Collections.Generic.IEnumerable<object>.GetEnumerator()
extern "C"  RuntimeObject* U3CEnumerateChildrenU3Ec__Iterator0_System_Collections_Generic_IEnumerableU3CobjectU3E_GetEnumerator_m1191949246 (U3CEnumerateChildrenU3Ec__Iterator0_t1235609798 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Threading.Interlocked::CompareExchange(System.Int32&,System.Int32,System.Int32)
extern "C"  int32_t Interlocked_CompareExchange_m3339239614 (RuntimeObject * __this /* static, unused */, int32_t* p0, int32_t p1, int32_t p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.NotSupportedException::.ctor()
extern "C"  void NotSupportedException__ctor_m3232764727 (NotSupportedException_t1793819818 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XPath.XPathNodeIterator::.ctor()
extern "C"  void XPathNodeIterator__ctor_m2254908861 (XPathNodeIterator_t3192332357 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XPath.XPathNavigatorComparer::.ctor()
extern "C"  void XPathNavigatorComparer__ctor_m3253571973 (XPathNavigatorComparer_t2670709129 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XPath.XPathNodeIterator/<GetEnumerator>c__Iterator2::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator2__ctor_m3138811673 (U3CGetEnumeratorU3Ec__Iterator2_t959253998 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Xml.XPath.XPathSorter::Compare(System.Object,System.Object)
extern "C"  int32_t XPathSorter_Compare_m1708989355 (XPathSorter_t3491953490 * __this, RuntimeObject * ___o10, RuntimeObject * ___o21, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList System.Xml.XPath.XPathSorters::ToSortElementList(System.Xml.XPath.BaseIterator)
extern "C"  ArrayList_t4252133567 * XPathSorters_ToSortElementList_m407179934 (XPathSorters_t4019574815 * __this, BaseIterator_t2454437973 * ___iter0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Xml.IXmlNamespaceResolver System.Xml.XPath.BaseIterator::get_NamespaceManager()
extern "C"  RuntimeObject* BaseIterator_get_NamespaceManager_m625352966 (BaseIterator_t2454437973 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Xml.XPath.BaseIterator System.Xml.XPath.XPathSorters::Sort(System.Collections.ArrayList,System.Xml.IXmlNamespaceResolver)
extern "C"  BaseIterator_t2454437973 * XPathSorters_Sort_m3271859196 (XPathSorters_t4019574815 * __this, ArrayList_t4252133567 * ___rgElts0, RuntimeObject* ___nsm1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList::.ctor()
extern "C"  void ArrayList__ctor_m4012174379 (ArrayList_t4252133567 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XPath.XPathSortElement::.ctor()
extern "C"  void XPathSortElement__ctor_m4067665647 (XPathSortElement_t1040291071 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Object System.Xml.XPath.XPathSorter::Evaluate(System.Xml.XPath.BaseIterator)
extern "C"  RuntimeObject * XPathSorter_Evaluate_m4282430526 (XPathSorter_t3491953490 * __this, BaseIterator_t2454437973 * ___iter0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XPath.ListIterator::.ctor(System.Collections.IList,System.Xml.IXmlNamespaceResolver)
extern "C"  void ListIterator__ctor_m1817971076 (ListIterator_t3804705054 * __this, RuntimeObject* ___list0, RuntimeObject* ___nsm1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XmlQualifiedName::get_Namespace()
extern "C"  String_t* XmlQualifiedName_get_Namespace_m2734729190 (XmlQualifiedName_t1944712516 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XmlQualifiedName::get_Name()
extern "C"  String_t* XmlQualifiedName_get_Name_m4055250010 (XmlQualifiedName_t1944712516 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Xml.XPath.XPathFunctionLang::.ctor(System.Xml.XPath.FunctionArguments)
extern "C"  void XPathFunctionLang__ctor_m2307304492 (XPathFunctionLang_t718178243 * __this, FunctionArguments_t2900945452 * ___args0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionLang__ctor_m2307304492_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		FunctionArguments_t2900945452 * L_0 = ___args0;
		XPathFunction__ctor_m327599860(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t2900945452 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		FunctionArguments_t2900945452 * L_2 = ___args0;
		NullCheck(L_2);
		FunctionArguments_t2900945452 * L_3 = FunctionArguments_get_Tail_m3947258239(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0023;
		}
	}

IL_0018:
	{
		XPathException_t1503722168 * L_4 = (XPathException_t1503722168 *)il2cpp_codegen_object_new(XPathException_t1503722168_il2cpp_TypeInfo_var);
		XPathException__ctor_m3605407358(L_4, _stringLiteral1550245230, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0023:
	{
		FunctionArguments_t2900945452 * L_5 = ___args0;
		NullCheck(L_5);
		Expression_t1283317256 * L_6 = FunctionArguments_get_Arg_m1790516491(L_5, /*hidden argument*/NULL);
		__this->set_arg0_0(L_6);
		return;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.XPathFunctionLang::get_ReturnType()
extern "C"  int32_t XPathFunctionLang_get_ReturnType_m3866603946 (XPathFunctionLang_t718178243 * __this, const RuntimeMethod* method)
{
	{
		return (int32_t)(2);
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionLang::get_Peer()
extern "C"  bool XPathFunctionLang_get_Peer_m2957520604 (XPathFunctionLang_t718178243 * __this, const RuntimeMethod* method)
{
	{
		Expression_t1283317256 * L_0 = __this->get_arg0_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_0);
		return L_1;
	}
}
// System.Object System.Xml.XPath.XPathFunctionLang::Evaluate(System.Xml.XPath.BaseIterator)
extern "C"  RuntimeObject * XPathFunctionLang_Evaluate_m4182185167 (XPathFunctionLang_t718178243 * __this, BaseIterator_t2454437973 * ___iter0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionLang_Evaluate_m4182185167_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		BaseIterator_t2454437973 * L_0 = ___iter0;
		bool L_1 = VirtFuncInvoker1< bool, BaseIterator_t2454437973 * >::Invoke(18 /* System.Boolean System.Xml.XPath.XPathFunctionLang::EvaluateBoolean(System.Xml.XPath.BaseIterator) */, __this, L_0);
		bool L_2 = L_1;
		RuntimeObject * L_3 = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &L_2);
		return L_3;
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionLang::EvaluateBoolean(System.Xml.XPath.BaseIterator)
extern "C"  bool XPathFunctionLang_EvaluateBoolean_m258778298 (XPathFunctionLang_t718178243 * __this, BaseIterator_t2454437973 * ___iter0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionLang_EvaluateBoolean_m258778298_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	int32_t G_B3_0 = 0;
	{
		Expression_t1283317256 * L_0 = __this->get_arg0_0();
		BaseIterator_t2454437973 * L_1 = ___iter0;
		NullCheck(L_0);
		String_t* L_2 = VirtFuncInvoker1< String_t*, BaseIterator_t2454437973 * >::Invoke(17 /* System.String System.Xml.XPath.Expression::EvaluateString(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3500843524_il2cpp_TypeInfo_var);
		CultureInfo_t3500843524 * L_3 = CultureInfo_get_InvariantCulture_m398972276(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		String_t* L_4 = String_ToLower_m743194025(L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		BaseIterator_t2454437973 * L_5 = ___iter0;
		NullCheck(L_5);
		XPathNavigator_t3981235968 * L_6 = VirtFuncInvoker0< XPathNavigator_t3981235968 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_5);
		NullCheck(L_6);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(13 /* System.String System.Xml.XPath.XPathNavigator::get_XmlLang() */, L_6);
		CultureInfo_t3500843524 * L_8 = CultureInfo_get_InvariantCulture_m398972276(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_7);
		String_t* L_9 = String_ToLower_m743194025(L_7, L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		String_t* L_10 = V_0;
		String_t* L_11 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_12 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_0054;
		}
	}
	{
		String_t* L_13 = V_0;
		String_t* L_14 = V_1;
		CharU5BU5D_t1328083999* L_15 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_15);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)45));
		NullCheck(L_14);
		StringU5BU5D_t1642385972* L_16 = String_Split_m3326265864(L_14, L_15, /*hidden argument*/NULL);
		NullCheck(L_16);
		int32_t L_17 = 0;
		String_t* L_18 = (L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_19 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_13, L_18, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_19));
		goto IL_0055;
	}

IL_0054:
	{
		G_B3_0 = 1;
	}

IL_0055:
	{
		return (bool)G_B3_0;
	}
}
// System.String System.Xml.XPath.XPathFunctionLang::ToString()
extern "C"  String_t* XPathFunctionLang_ToString_m4056273282 (XPathFunctionLang_t718178243 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionLang_ToString_m4056273282_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		StringU5BU5D_t1642385972* L_0 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral1352971294);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral1352971294);
		StringU5BU5D_t1642385972* L_1 = L_0;
		Expression_t1283317256 * L_2 = __this->get_arg0_0();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_3);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_3);
		StringU5BU5D_t1642385972* L_4 = L_1;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, _stringLiteral372029317);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m626692867(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void System.Xml.XPath.XPathFunctionLast::.ctor(System.Xml.XPath.FunctionArguments)
extern "C"  void XPathFunctionLast__ctor_m2403937296 (XPathFunctionLast_t2734600875 * __this, FunctionArguments_t2900945452 * ___args0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionLast__ctor_m2403937296_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		FunctionArguments_t2900945452 * L_0 = ___args0;
		XPathFunction__ctor_m327599860(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t2900945452 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		XPathException_t1503722168 * L_2 = (XPathException_t1503722168 *)il2cpp_codegen_object_new(XPathException_t1503722168_il2cpp_TypeInfo_var);
		XPathException__ctor_m3605407358(L_2, _stringLiteral3500109105, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0018:
	{
		return;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.XPathFunctionLast::get_ReturnType()
extern "C"  int32_t XPathFunctionLast_get_ReturnType_m1927039466 (XPathFunctionLast_t2734600875 * __this, const RuntimeMethod* method)
{
	{
		return (int32_t)(0);
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionLast::get_Peer()
extern "C"  bool XPathFunctionLast_get_Peer_m4123466940 (XPathFunctionLast_t2734600875 * __this, const RuntimeMethod* method)
{
	{
		return (bool)1;
	}
}
// System.Object System.Xml.XPath.XPathFunctionLast::Evaluate(System.Xml.XPath.BaseIterator)
extern "C"  RuntimeObject * XPathFunctionLast_Evaluate_m3244523513 (XPathFunctionLast_t2734600875 * __this, BaseIterator_t2454437973 * ___iter0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionLast_Evaluate_m3244523513_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		BaseIterator_t2454437973 * L_0 = ___iter0;
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 System.Xml.XPath.XPathNodeIterator::get_Count() */, L_0);
		double L_2 = (((double)((double)L_1)));
		RuntimeObject * L_3 = Box(Double_t4078015681_il2cpp_TypeInfo_var, &L_2);
		return L_3;
	}
}
// System.String System.Xml.XPath.XPathFunctionLast::ToString()
extern "C"  String_t* XPathFunctionLast_ToString_m3535557470 (XPathFunctionLast_t2734600875 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionLast_ToString_m3535557470_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		return _stringLiteral4274021071;
	}
}
// System.Void System.Xml.XPath.XPathFunctionLocalName::.ctor(System.Xml.XPath.FunctionArguments)
extern "C"  void XPathFunctionLocalName__ctor_m1211626624 (XPathFunctionLocalName_t3935594891 * __this, FunctionArguments_t2900945452 * ___args0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionLocalName__ctor_m1211626624_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		FunctionArguments_t2900945452 * L_0 = ___args0;
		XPathFunction__ctor_m327599860(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t2900945452 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		FunctionArguments_t2900945452 * L_2 = ___args0;
		NullCheck(L_2);
		Expression_t1283317256 * L_3 = FunctionArguments_get_Arg_m1790516491(L_2, /*hidden argument*/NULL);
		__this->set_arg0_0(L_3);
		FunctionArguments_t2900945452 * L_4 = ___args0;
		NullCheck(L_4);
		FunctionArguments_t2900945452 * L_5 = FunctionArguments_get_Tail_m3947258239(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002f;
		}
	}
	{
		XPathException_t1503722168 * L_6 = (XPathException_t1503722168 *)il2cpp_codegen_object_new(XPathException_t1503722168_il2cpp_TypeInfo_var);
		XPathException__ctor_m3605407358(L_6, _stringLiteral4037561126, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_002f:
	{
		return;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.XPathFunctionLocalName::get_ReturnType()
extern "C"  int32_t XPathFunctionLocalName_get_ReturnType_m1282536342 (XPathFunctionLocalName_t3935594891 * __this, const RuntimeMethod* method)
{
	{
		return (int32_t)(1);
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionLocalName::get_Peer()
extern "C"  bool XPathFunctionLocalName_get_Peer_m1910621328 (XPathFunctionLocalName_t3935594891 * __this, const RuntimeMethod* method)
{
	int32_t G_B3_0 = 0;
	{
		Expression_t1283317256 * L_0 = __this->get_arg0_0();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		Expression_t1283317256 * L_1 = __this->get_arg0_0();
		NullCheck(L_1);
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_1);
		G_B3_0 = ((int32_t)(L_2));
		goto IL_001c;
	}

IL_001b:
	{
		G_B3_0 = 1;
	}

IL_001c:
	{
		return (bool)G_B3_0;
	}
}
// System.Object System.Xml.XPath.XPathFunctionLocalName::Evaluate(System.Xml.XPath.BaseIterator)
extern "C"  RuntimeObject * XPathFunctionLocalName_Evaluate_m1416622637 (XPathFunctionLocalName_t3935594891 * __this, BaseIterator_t2454437973 * ___iter0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionLocalName_Evaluate_m1416622637_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	BaseIterator_t2454437973 * V_0 = NULL;
	{
		Expression_t1283317256 * L_0 = __this->get_arg0_0();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		BaseIterator_t2454437973 * L_1 = ___iter0;
		NullCheck(L_1);
		XPathNavigator_t3981235968 * L_2 = VirtFuncInvoker0< XPathNavigator_t3981235968 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_1);
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String System.Xml.XPath.XPathNavigator::get_LocalName() */, L_2);
		return L_3;
	}

IL_0017:
	{
		Expression_t1283317256 * L_4 = __this->get_arg0_0();
		BaseIterator_t2454437973 * L_5 = ___iter0;
		NullCheck(L_4);
		BaseIterator_t2454437973 * L_6 = VirtFuncInvoker1< BaseIterator_t2454437973 *, BaseIterator_t2454437973 * >::Invoke(14 /* System.Xml.XPath.BaseIterator System.Xml.XPath.Expression::EvaluateNodeSet(System.Xml.XPath.BaseIterator) */, L_4, L_5);
		V_0 = L_6;
		BaseIterator_t2454437973 * L_7 = V_0;
		if (!L_7)
		{
			goto IL_0035;
		}
	}
	{
		BaseIterator_t2454437973 * L_8 = V_0;
		NullCheck(L_8);
		bool L_9 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.BaseIterator::MoveNext() */, L_8);
		if (L_9)
		{
			goto IL_003b;
		}
	}

IL_0035:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		return L_10;
	}

IL_003b:
	{
		BaseIterator_t2454437973 * L_11 = V_0;
		NullCheck(L_11);
		XPathNavigator_t3981235968 * L_12 = VirtFuncInvoker0< XPathNavigator_t3981235968 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_11);
		NullCheck(L_12);
		String_t* L_13 = VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String System.Xml.XPath.XPathNavigator::get_LocalName() */, L_12);
		return L_13;
	}
}
// System.String System.Xml.XPath.XPathFunctionLocalName::ToString()
extern "C"  String_t* XPathFunctionLocalName_ToString_m2774210428 (XPathFunctionLocalName_t3935594891 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionLocalName_ToString_m2774210428_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Expression_t1283317256 * L_0 = __this->get_arg0_0();
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral2822356663, L_1, _stringLiteral372029317, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void System.Xml.XPath.XPathFunctionName::.ctor(System.Xml.XPath.FunctionArguments)
extern "C"  void XPathFunctionName__ctor_m429788115 (XPathFunctionName_t1880977850 * __this, FunctionArguments_t2900945452 * ___args0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionName__ctor_m429788115_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		FunctionArguments_t2900945452 * L_0 = ___args0;
		XPathFunction__ctor_m327599860(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t2900945452 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		FunctionArguments_t2900945452 * L_2 = ___args0;
		NullCheck(L_2);
		Expression_t1283317256 * L_3 = FunctionArguments_get_Arg_m1790516491(L_2, /*hidden argument*/NULL);
		__this->set_arg0_0(L_3);
		FunctionArguments_t2900945452 * L_4 = ___args0;
		NullCheck(L_4);
		FunctionArguments_t2900945452 * L_5 = FunctionArguments_get_Tail_m3947258239(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002f;
		}
	}
	{
		XPathException_t1503722168 * L_6 = (XPathException_t1503722168 *)il2cpp_codegen_object_new(XPathException_t1503722168_il2cpp_TypeInfo_var);
		XPathException__ctor_m3605407358(L_6, _stringLiteral2424985204, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_002f:
	{
		return;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.XPathFunctionName::get_ReturnType()
extern "C"  int32_t XPathFunctionName_get_ReturnType_m2532855297 (XPathFunctionName_t1880977850 * __this, const RuntimeMethod* method)
{
	{
		return (int32_t)(1);
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionName::get_Peer()
extern "C"  bool XPathFunctionName_get_Peer_m477470885 (XPathFunctionName_t1880977850 * __this, const RuntimeMethod* method)
{
	int32_t G_B3_0 = 0;
	{
		Expression_t1283317256 * L_0 = __this->get_arg0_0();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		Expression_t1283317256 * L_1 = __this->get_arg0_0();
		NullCheck(L_1);
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_1);
		G_B3_0 = ((int32_t)(L_2));
		goto IL_001c;
	}

IL_001b:
	{
		G_B3_0 = 1;
	}

IL_001c:
	{
		return (bool)G_B3_0;
	}
}
// System.Object System.Xml.XPath.XPathFunctionName::Evaluate(System.Xml.XPath.BaseIterator)
extern "C"  RuntimeObject * XPathFunctionName_Evaluate_m1216738966 (XPathFunctionName_t1880977850 * __this, BaseIterator_t2454437973 * ___iter0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionName_Evaluate_m1216738966_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	BaseIterator_t2454437973 * V_0 = NULL;
	{
		Expression_t1283317256 * L_0 = __this->get_arg0_0();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		BaseIterator_t2454437973 * L_1 = ___iter0;
		NullCheck(L_1);
		XPathNavigator_t3981235968 * L_2 = VirtFuncInvoker0< XPathNavigator_t3981235968 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_1);
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(10 /* System.String System.Xml.XPath.XPathNavigator::get_Name() */, L_2);
		return L_3;
	}

IL_0017:
	{
		Expression_t1283317256 * L_4 = __this->get_arg0_0();
		BaseIterator_t2454437973 * L_5 = ___iter0;
		NullCheck(L_4);
		BaseIterator_t2454437973 * L_6 = VirtFuncInvoker1< BaseIterator_t2454437973 *, BaseIterator_t2454437973 * >::Invoke(14 /* System.Xml.XPath.BaseIterator System.Xml.XPath.Expression::EvaluateNodeSet(System.Xml.XPath.BaseIterator) */, L_4, L_5);
		V_0 = L_6;
		BaseIterator_t2454437973 * L_7 = V_0;
		if (!L_7)
		{
			goto IL_0035;
		}
	}
	{
		BaseIterator_t2454437973 * L_8 = V_0;
		NullCheck(L_8);
		bool L_9 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.BaseIterator::MoveNext() */, L_8);
		if (L_9)
		{
			goto IL_003b;
		}
	}

IL_0035:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		return L_10;
	}

IL_003b:
	{
		BaseIterator_t2454437973 * L_11 = V_0;
		NullCheck(L_11);
		XPathNavigator_t3981235968 * L_12 = VirtFuncInvoker0< XPathNavigator_t3981235968 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_11);
		NullCheck(L_12);
		String_t* L_13 = VirtFuncInvoker0< String_t* >::Invoke(10 /* System.String System.Xml.XPath.XPathNavigator::get_Name() */, L_12);
		return L_13;
	}
}
// System.String System.Xml.XPath.XPathFunctionName::ToString()
extern "C"  String_t* XPathFunctionName_ToString_m973448361 (XPathFunctionName_t1880977850 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionName_ToString_m973448361_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* G_B2_0 = NULL;
	String_t* G_B1_0 = NULL;
	String_t* G_B3_0 = NULL;
	String_t* G_B3_1 = NULL;
	{
		Expression_t1283317256 * L_0 = __this->get_arg0_0();
		G_B1_0 = _stringLiteral2515777075;
		if (!L_0)
		{
			G_B2_0 = _stringLiteral2515777075;
			goto IL_0020;
		}
	}
	{
		Expression_t1283317256 * L_1 = __this->get_arg0_0();
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_1);
		G_B3_0 = L_2;
		G_B3_1 = G_B1_0;
		goto IL_0025;
	}

IL_0020:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		G_B3_0 = L_3;
		G_B3_1 = G_B2_0;
	}

IL_0025:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m612901809(NULL /*static, unused*/, G_B3_1, G_B3_0, _stringLiteral372029317, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void System.Xml.XPath.XPathFunctionNamespaceUri::.ctor(System.Xml.XPath.FunctionArguments)
extern "C"  void XPathFunctionNamespaceUri__ctor_m1544480789 (XPathFunctionNamespaceUri_t448035136 * __this, FunctionArguments_t2900945452 * ___args0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionNamespaceUri__ctor_m1544480789_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		FunctionArguments_t2900945452 * L_0 = ___args0;
		XPathFunction__ctor_m327599860(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t2900945452 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		FunctionArguments_t2900945452 * L_2 = ___args0;
		NullCheck(L_2);
		Expression_t1283317256 * L_3 = FunctionArguments_get_Arg_m1790516491(L_2, /*hidden argument*/NULL);
		__this->set_arg0_0(L_3);
		FunctionArguments_t2900945452 * L_4 = ___args0;
		NullCheck(L_4);
		FunctionArguments_t2900945452 * L_5 = FunctionArguments_get_Tail_m3947258239(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002f;
		}
	}
	{
		XPathException_t1503722168 * L_6 = (XPathException_t1503722168 *)il2cpp_codegen_object_new(XPathException_t1503722168_il2cpp_TypeInfo_var);
		XPathException__ctor_m3605407358(L_6, _stringLiteral790676781, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_002f:
	{
		return;
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionNamespaceUri::get_Peer()
extern "C"  bool XPathFunctionNamespaceUri_get_Peer_m3198966399 (XPathFunctionNamespaceUri_t448035136 * __this, const RuntimeMethod* method)
{
	int32_t G_B3_0 = 0;
	{
		Expression_t1283317256 * L_0 = __this->get_arg0_0();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		Expression_t1283317256 * L_1 = __this->get_arg0_0();
		NullCheck(L_1);
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_1);
		G_B3_0 = ((int32_t)(L_2));
		goto IL_001c;
	}

IL_001b:
	{
		G_B3_0 = 1;
	}

IL_001c:
	{
		return (bool)G_B3_0;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.XPathFunctionNamespaceUri::get_ReturnType()
extern "C"  int32_t XPathFunctionNamespaceUri_get_ReturnType_m3927590639 (XPathFunctionNamespaceUri_t448035136 * __this, const RuntimeMethod* method)
{
	{
		return (int32_t)(1);
	}
}
// System.Object System.Xml.XPath.XPathFunctionNamespaceUri::Evaluate(System.Xml.XPath.BaseIterator)
extern "C"  RuntimeObject * XPathFunctionNamespaceUri_Evaluate_m388318582 (XPathFunctionNamespaceUri_t448035136 * __this, BaseIterator_t2454437973 * ___iter0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionNamespaceUri_Evaluate_m388318582_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	BaseIterator_t2454437973 * V_0 = NULL;
	{
		Expression_t1283317256 * L_0 = __this->get_arg0_0();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		BaseIterator_t2454437973 * L_1 = ___iter0;
		NullCheck(L_1);
		XPathNavigator_t3981235968 * L_2 = VirtFuncInvoker0< XPathNavigator_t3981235968 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_1);
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(11 /* System.String System.Xml.XPath.XPathNavigator::get_NamespaceURI() */, L_2);
		return L_3;
	}

IL_0017:
	{
		Expression_t1283317256 * L_4 = __this->get_arg0_0();
		BaseIterator_t2454437973 * L_5 = ___iter0;
		NullCheck(L_4);
		BaseIterator_t2454437973 * L_6 = VirtFuncInvoker1< BaseIterator_t2454437973 *, BaseIterator_t2454437973 * >::Invoke(14 /* System.Xml.XPath.BaseIterator System.Xml.XPath.Expression::EvaluateNodeSet(System.Xml.XPath.BaseIterator) */, L_4, L_5);
		V_0 = L_6;
		BaseIterator_t2454437973 * L_7 = V_0;
		if (!L_7)
		{
			goto IL_0035;
		}
	}
	{
		BaseIterator_t2454437973 * L_8 = V_0;
		NullCheck(L_8);
		bool L_9 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.BaseIterator::MoveNext() */, L_8);
		if (L_9)
		{
			goto IL_003b;
		}
	}

IL_0035:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		return L_10;
	}

IL_003b:
	{
		BaseIterator_t2454437973 * L_11 = V_0;
		NullCheck(L_11);
		XPathNavigator_t3981235968 * L_12 = VirtFuncInvoker0< XPathNavigator_t3981235968 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_11);
		NullCheck(L_12);
		String_t* L_13 = VirtFuncInvoker0< String_t* >::Invoke(11 /* System.String System.Xml.XPath.XPathNavigator::get_NamespaceURI() */, L_12);
		return L_13;
	}
}
// System.String System.Xml.XPath.XPathFunctionNamespaceUri::ToString()
extern "C"  String_t* XPathFunctionNamespaceUri_ToString_m2211506483 (XPathFunctionNamespaceUri_t448035136 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionNamespaceUri_ToString_m2211506483_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Expression_t1283317256 * L_0 = __this->get_arg0_0();
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral1879196646, L_1, _stringLiteral372029317, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void System.Xml.XPath.XPathFunctionNormalizeSpace::.ctor(System.Xml.XPath.FunctionArguments)
extern "C"  void XPathFunctionNormalizeSpace__ctor_m3022371881 (XPathFunctionNormalizeSpace_t1288231156 * __this, FunctionArguments_t2900945452 * ___args0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionNormalizeSpace__ctor_m3022371881_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		FunctionArguments_t2900945452 * L_0 = ___args0;
		XPathFunction__ctor_m327599860(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t2900945452 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		FunctionArguments_t2900945452 * L_2 = ___args0;
		NullCheck(L_2);
		Expression_t1283317256 * L_3 = FunctionArguments_get_Arg_m1790516491(L_2, /*hidden argument*/NULL);
		__this->set_arg0_0(L_3);
		FunctionArguments_t2900945452 * L_4 = ___args0;
		NullCheck(L_4);
		FunctionArguments_t2900945452 * L_5 = FunctionArguments_get_Tail_m3947258239(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002f;
		}
	}
	{
		XPathException_t1503722168 * L_6 = (XPathException_t1503722168 *)il2cpp_codegen_object_new(XPathException_t1503722168_il2cpp_TypeInfo_var);
		XPathException__ctor_m3605407358(L_6, _stringLiteral2744585421, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_002f:
	{
		return;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.XPathFunctionNormalizeSpace::get_ReturnType()
extern "C"  int32_t XPathFunctionNormalizeSpace_get_ReturnType_m533286203 (XPathFunctionNormalizeSpace_t1288231156 * __this, const RuntimeMethod* method)
{
	{
		return (int32_t)(1);
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionNormalizeSpace::get_Peer()
extern "C"  bool XPathFunctionNormalizeSpace_get_Peer_m2159179587 (XPathFunctionNormalizeSpace_t1288231156 * __this, const RuntimeMethod* method)
{
	int32_t G_B3_0 = 0;
	{
		Expression_t1283317256 * L_0 = __this->get_arg0_0();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		Expression_t1283317256 * L_1 = __this->get_arg0_0();
		NullCheck(L_1);
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_1);
		G_B3_0 = ((int32_t)(L_2));
		goto IL_001c;
	}

IL_001b:
	{
		G_B3_0 = 1;
	}

IL_001c:
	{
		return (bool)G_B3_0;
	}
}
// System.Object System.Xml.XPath.XPathFunctionNormalizeSpace::Evaluate(System.Xml.XPath.BaseIterator)
extern "C"  RuntimeObject * XPathFunctionNormalizeSpace_Evaluate_m4131070162 (XPathFunctionNormalizeSpace_t1288231156 * __this, BaseIterator_t2454437973 * ___iter0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionNormalizeSpace_Evaluate_m4131070162_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	StringBuilder_t1221177846 * V_1 = NULL;
	bool V_2 = false;
	int32_t V_3 = 0;
	Il2CppChar V_4 = 0x0;
	{
		Expression_t1283317256 * L_0 = __this->get_arg0_0();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Expression_t1283317256 * L_1 = __this->get_arg0_0();
		BaseIterator_t2454437973 * L_2 = ___iter0;
		NullCheck(L_1);
		String_t* L_3 = VirtFuncInvoker1< String_t*, BaseIterator_t2454437973 * >::Invoke(17 /* System.String System.Xml.XPath.Expression::EvaluateString(System.Xml.XPath.BaseIterator) */, L_1, L_2);
		V_0 = L_3;
		goto IL_0029;
	}

IL_001d:
	{
		BaseIterator_t2454437973 * L_4 = ___iter0;
		NullCheck(L_4);
		XPathNavigator_t3981235968 * L_5 = VirtFuncInvoker0< XPathNavigator_t3981235968 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_4);
		NullCheck(L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.Xml.XPath.XPathItem::get_Value() */, L_5);
		V_0 = L_6;
	}

IL_0029:
	{
		StringBuilder_t1221177846 * L_7 = (StringBuilder_t1221177846 *)il2cpp_codegen_object_new(StringBuilder_t1221177846_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3946851802(L_7, /*hidden argument*/NULL);
		V_1 = L_7;
		V_2 = (bool)0;
		V_3 = 0;
		goto IL_0096;
	}

IL_0038:
	{
		String_t* L_8 = V_0;
		int32_t L_9 = V_3;
		NullCheck(L_8);
		Il2CppChar L_10 = String_get_Chars_m4230566705(L_8, L_9, /*hidden argument*/NULL);
		V_4 = L_10;
		Il2CppChar L_11 = V_4;
		if ((((int32_t)L_11) == ((int32_t)((int32_t)32))))
		{
			goto IL_0065;
		}
	}
	{
		Il2CppChar L_12 = V_4;
		if ((((int32_t)L_12) == ((int32_t)((int32_t)9))))
		{
			goto IL_0065;
		}
	}
	{
		Il2CppChar L_13 = V_4;
		if ((((int32_t)L_13) == ((int32_t)((int32_t)13))))
		{
			goto IL_0065;
		}
	}
	{
		Il2CppChar L_14 = V_4;
		if ((!(((uint32_t)L_14) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_006c;
		}
	}

IL_0065:
	{
		V_2 = (bool)1;
		goto IL_0092;
	}

IL_006c:
	{
		bool L_15 = V_2;
		if (!L_15)
		{
			goto IL_0089;
		}
	}
	{
		V_2 = (bool)0;
		StringBuilder_t1221177846 * L_16 = V_1;
		NullCheck(L_16);
		int32_t L_17 = StringBuilder_get_Length_m1608241323(L_16, /*hidden argument*/NULL);
		if ((((int32_t)L_17) <= ((int32_t)0)))
		{
			goto IL_0089;
		}
	}
	{
		StringBuilder_t1221177846 * L_18 = V_1;
		NullCheck(L_18);
		StringBuilder_Append_m3618697540(L_18, ((int32_t)32), /*hidden argument*/NULL);
	}

IL_0089:
	{
		StringBuilder_t1221177846 * L_19 = V_1;
		Il2CppChar L_20 = V_4;
		NullCheck(L_19);
		StringBuilder_Append_m3618697540(L_19, L_20, /*hidden argument*/NULL);
	}

IL_0092:
	{
		int32_t L_21 = V_3;
		V_3 = ((int32_t)((int32_t)L_21+(int32_t)1));
	}

IL_0096:
	{
		int32_t L_22 = V_3;
		String_t* L_23 = V_0;
		NullCheck(L_23);
		int32_t L_24 = String_get_Length_m1606060069(L_23, /*hidden argument*/NULL);
		if ((((int32_t)L_22) < ((int32_t)L_24)))
		{
			goto IL_0038;
		}
	}
	{
		StringBuilder_t1221177846 * L_25 = V_1;
		NullCheck(L_25);
		String_t* L_26 = StringBuilder_ToString_m1507807375(L_25, /*hidden argument*/NULL);
		return L_26;
	}
}
// System.String System.Xml.XPath.XPathFunctionNormalizeSpace::ToString()
extern "C"  String_t* XPathFunctionNormalizeSpace_ToString_m383064191 (XPathFunctionNormalizeSpace_t1288231156 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionNormalizeSpace_ToString_m383064191_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1642385972* G_B2_1 = NULL;
	StringU5BU5D_t1642385972* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1642385972* G_B1_1 = NULL;
	StringU5BU5D_t1642385972* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1642385972* G_B3_2 = NULL;
	StringU5BU5D_t1642385972* G_B3_3 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral2732366470);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral2732366470);
		StringU5BU5D_t1642385972* L_1 = L_0;
		Expression_t1283317256 * L_2 = __this->get_arg0_0();
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_002b;
		}
	}
	{
		Expression_t1283317256 * L_3 = __this->get_arg0_0();
		NullCheck(L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_3);
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_0030;
	}

IL_002b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_0030:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1642385972* L_6 = G_B3_3;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral372029317);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m626692867(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void System.Xml.XPath.XPathFunctionNot::.ctor(System.Xml.XPath.FunctionArguments)
extern "C"  void XPathFunctionNot__ctor_m3119931389 (XPathFunctionNot_t1631674504 * __this, FunctionArguments_t2900945452 * ___args0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionNot__ctor_m3119931389_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		FunctionArguments_t2900945452 * L_0 = ___args0;
		XPathBooleanFunction__ctor_m620012104(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t2900945452 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		FunctionArguments_t2900945452 * L_2 = ___args0;
		NullCheck(L_2);
		FunctionArguments_t2900945452 * L_3 = FunctionArguments_get_Tail_m3947258239(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0023;
		}
	}

IL_0018:
	{
		XPathException_t1503722168 * L_4 = (XPathException_t1503722168 *)il2cpp_codegen_object_new(XPathException_t1503722168_il2cpp_TypeInfo_var);
		XPathException__ctor_m3605407358(L_4, _stringLiteral2157221977, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0023:
	{
		FunctionArguments_t2900945452 * L_5 = ___args0;
		NullCheck(L_5);
		Expression_t1283317256 * L_6 = FunctionArguments_get_Arg_m1790516491(L_5, /*hidden argument*/NULL);
		__this->set_arg0_0(L_6);
		return;
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionNot::get_Peer()
extern "C"  bool XPathFunctionNot_get_Peer_m3659900111 (XPathFunctionNot_t1631674504 * __this, const RuntimeMethod* method)
{
	{
		Expression_t1283317256 * L_0 = __this->get_arg0_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_0);
		return L_1;
	}
}
// System.Object System.Xml.XPath.XPathFunctionNot::Evaluate(System.Xml.XPath.BaseIterator)
extern "C"  RuntimeObject * XPathFunctionNot_Evaluate_m2475008982 (XPathFunctionNot_t1631674504 * __this, BaseIterator_t2454437973 * ___iter0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionNot_Evaluate_m2475008982_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Expression_t1283317256 * L_0 = __this->get_arg0_0();
		BaseIterator_t2454437973 * L_1 = ___iter0;
		NullCheck(L_0);
		bool L_2 = VirtFuncInvoker1< bool, BaseIterator_t2454437973 * >::Invoke(18 /* System.Boolean System.Xml.XPath.Expression::EvaluateBoolean(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		bool L_3 = ((bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0));
		RuntimeObject * L_4 = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &L_3);
		return L_4;
	}
}
// System.String System.Xml.XPath.XPathFunctionNot::ToString()
extern "C"  String_t* XPathFunctionNot_ToString_m3232938039 (XPathFunctionNot_t1631674504 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionNot_ToString_m3232938039_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		StringU5BU5D_t1642385972* L_0 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral1084865993);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral1084865993);
		StringU5BU5D_t1642385972* L_1 = L_0;
		Expression_t1283317256 * L_2 = __this->get_arg0_0();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_3);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_3);
		StringU5BU5D_t1642385972* L_4 = L_1;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, _stringLiteral372029317);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m626692867(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void System.Xml.XPath.XPathFunctionNumber::.ctor(System.Xml.XPath.FunctionArguments)
extern "C"  void XPathFunctionNumber__ctor_m2280431139 (XPathFunctionNumber_t1210619362 * __this, FunctionArguments_t2900945452 * ___args0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionNumber__ctor_m2280431139_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		FunctionArguments_t2900945452 * L_0 = ___args0;
		XPathNumericFunction__ctor_m2271660427(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t2900945452 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		FunctionArguments_t2900945452 * L_2 = ___args0;
		NullCheck(L_2);
		Expression_t1283317256 * L_3 = FunctionArguments_get_Arg_m1790516491(L_2, /*hidden argument*/NULL);
		__this->set_arg0_0(L_3);
		FunctionArguments_t2900945452 * L_4 = ___args0;
		NullCheck(L_4);
		FunctionArguments_t2900945452 * L_5 = FunctionArguments_get_Tail_m3947258239(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002f;
		}
	}
	{
		XPathException_t1503722168 * L_6 = (XPathException_t1503722168 *)il2cpp_codegen_object_new(XPathException_t1503722168_il2cpp_TypeInfo_var);
		XPathException__ctor_m3605407358(L_6, _stringLiteral2993931250, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_002f:
	{
		return;
	}
}
// System.Xml.XPath.Expression System.Xml.XPath.XPathFunctionNumber::Optimize()
extern "C"  Expression_t1283317256 * XPathFunctionNumber_Optimize_m2168727705 (XPathFunctionNumber_t1210619362 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionNumber_Optimize_m2168727705_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	XPathFunctionNumber_t1210619362 * G_B5_0 = NULL;
	{
		Expression_t1283317256 * L_0 = __this->get_arg0_0();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return __this;
	}

IL_000d:
	{
		Expression_t1283317256 * L_1 = __this->get_arg0_0();
		NullCheck(L_1);
		Expression_t1283317256 * L_2 = VirtFuncInvoker0< Expression_t1283317256 * >::Invoke(6 /* System.Xml.XPath.Expression System.Xml.XPath.Expression::Optimize() */, L_1);
		__this->set_arg0_0(L_2);
		Expression_t1283317256 * L_3 = __this->get_arg0_0();
		NullCheck(L_3);
		bool L_4 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Xml.XPath.Expression::get_HasStaticValue() */, L_3);
		if (L_4)
		{
			goto IL_0034;
		}
	}
	{
		G_B5_0 = __this;
		goto IL_003f;
	}

IL_0034:
	{
		double L_5 = VirtFuncInvoker0< double >::Invoke(10 /* System.Double System.Xml.XPath.XPathFunctionNumber::get_StaticValueAsNumber() */, __this);
		ExprNumber_t2687440122 * L_6 = (ExprNumber_t2687440122 *)il2cpp_codegen_object_new(ExprNumber_t2687440122_il2cpp_TypeInfo_var);
		ExprNumber__ctor_m2550519548(L_6, L_5, /*hidden argument*/NULL);
		G_B5_0 = ((XPathFunctionNumber_t1210619362 *)(L_6));
	}

IL_003f:
	{
		return G_B5_0;
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionNumber::get_HasStaticValue()
extern "C"  bool XPathFunctionNumber_get_HasStaticValue_m3540922994 (XPathFunctionNumber_t1210619362 * __this, const RuntimeMethod* method)
{
	int32_t G_B3_0 = 0;
	{
		Expression_t1283317256 * L_0 = __this->get_arg0_0();
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		Expression_t1283317256 * L_1 = __this->get_arg0_0();
		NullCheck(L_1);
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Xml.XPath.Expression::get_HasStaticValue() */, L_1);
		G_B3_0 = ((int32_t)(L_2));
		goto IL_0019;
	}

IL_0018:
	{
		G_B3_0 = 0;
	}

IL_0019:
	{
		return (bool)G_B3_0;
	}
}
// System.Double System.Xml.XPath.XPathFunctionNumber::get_StaticValueAsNumber()
extern "C"  double XPathFunctionNumber_get_StaticValueAsNumber_m3218526562 (XPathFunctionNumber_t1210619362 * __this, const RuntimeMethod* method)
{
	double G_B3_0 = 0.0;
	{
		Expression_t1283317256 * L_0 = __this->get_arg0_0();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		Expression_t1283317256 * L_1 = __this->get_arg0_0();
		NullCheck(L_1);
		double L_2 = VirtFuncInvoker0< double >::Invoke(10 /* System.Double System.Xml.XPath.Expression::get_StaticValueAsNumber() */, L_1);
		G_B3_0 = L_2;
		goto IL_0024;
	}

IL_001b:
	{
		G_B3_0 = (0.0);
	}

IL_0024:
	{
		return G_B3_0;
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionNumber::get_Peer()
extern "C"  bool XPathFunctionNumber_get_Peer_m572033157 (XPathFunctionNumber_t1210619362 * __this, const RuntimeMethod* method)
{
	int32_t G_B3_0 = 0;
	{
		Expression_t1283317256 * L_0 = __this->get_arg0_0();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		Expression_t1283317256 * L_1 = __this->get_arg0_0();
		NullCheck(L_1);
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_1);
		G_B3_0 = ((int32_t)(L_2));
		goto IL_001c;
	}

IL_001b:
	{
		G_B3_0 = 1;
	}

IL_001c:
	{
		return (bool)G_B3_0;
	}
}
// System.Object System.Xml.XPath.XPathFunctionNumber::Evaluate(System.Xml.XPath.BaseIterator)
extern "C"  RuntimeObject * XPathFunctionNumber_Evaluate_m3881007260 (XPathFunctionNumber_t1210619362 * __this, BaseIterator_t2454437973 * ___iter0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionNumber_Evaluate_m3881007260_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Expression_t1283317256 * L_0 = __this->get_arg0_0();
		if (L_0)
		{
			goto IL_0021;
		}
	}
	{
		BaseIterator_t2454437973 * L_1 = ___iter0;
		NullCheck(L_1);
		XPathNavigator_t3981235968 * L_2 = VirtFuncInvoker0< XPathNavigator_t3981235968 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_1);
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.Xml.XPath.XPathItem::get_Value() */, L_2);
		double L_4 = XPathFunctions_ToNumber_m2754819889(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		double L_5 = L_4;
		RuntimeObject * L_6 = Box(Double_t4078015681_il2cpp_TypeInfo_var, &L_5);
		return L_6;
	}

IL_0021:
	{
		Expression_t1283317256 * L_7 = __this->get_arg0_0();
		BaseIterator_t2454437973 * L_8 = ___iter0;
		NullCheck(L_7);
		double L_9 = VirtFuncInvoker1< double, BaseIterator_t2454437973 * >::Invoke(16 /* System.Double System.Xml.XPath.Expression::EvaluateNumber(System.Xml.XPath.BaseIterator) */, L_7, L_8);
		double L_10 = L_9;
		RuntimeObject * L_11 = Box(Double_t4078015681_il2cpp_TypeInfo_var, &L_10);
		return L_11;
	}
}
// System.String System.Xml.XPath.XPathFunctionNumber::ToString()
extern "C"  String_t* XPathFunctionNumber_ToString_m841233809 (XPathFunctionNumber_t1210619362 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionNumber_ToString_m841233809_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		StringU5BU5D_t1642385972* L_0 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral4042911851);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral4042911851);
		StringU5BU5D_t1642385972* L_1 = L_0;
		Expression_t1283317256 * L_2 = __this->get_arg0_0();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_3);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_3);
		StringU5BU5D_t1642385972* L_4 = L_1;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, _stringLiteral372029317);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m626692867(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void System.Xml.XPath.XPathFunctionPosition::.ctor(System.Xml.XPath.FunctionArguments)
extern "C"  void XPathFunctionPosition__ctor_m3434234893 (XPathFunctionPosition_t3711683936 * __this, FunctionArguments_t2900945452 * ___args0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionPosition__ctor_m3434234893_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		FunctionArguments_t2900945452 * L_0 = ___args0;
		XPathFunction__ctor_m327599860(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t2900945452 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		XPathException_t1503722168 * L_2 = (XPathException_t1503722168 *)il2cpp_codegen_object_new(XPathException_t1503722168_il2cpp_TypeInfo_var);
		XPathException__ctor_m3605407358(L_2, _stringLiteral3441363790, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0018:
	{
		return;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.XPathFunctionPosition::get_ReturnType()
extern "C"  int32_t XPathFunctionPosition_get_ReturnType_m3265774891 (XPathFunctionPosition_t3711683936 * __this, const RuntimeMethod* method)
{
	{
		return (int32_t)(0);
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionPosition::get_Peer()
extern "C"  bool XPathFunctionPosition_get_Peer_m2851184435 (XPathFunctionPosition_t3711683936 * __this, const RuntimeMethod* method)
{
	{
		return (bool)1;
	}
}
// System.Object System.Xml.XPath.XPathFunctionPosition::Evaluate(System.Xml.XPath.BaseIterator)
extern "C"  RuntimeObject * XPathFunctionPosition_Evaluate_m1355803762 (XPathFunctionPosition_t3711683936 * __this, BaseIterator_t2454437973 * ___iter0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionPosition_Evaluate_m1355803762_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		BaseIterator_t2454437973 * L_0 = ___iter0;
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.Xml.XPath.BaseIterator::get_CurrentPosition() */, L_0);
		double L_2 = (((double)((double)L_1)));
		RuntimeObject * L_3 = Box(Double_t4078015681_il2cpp_TypeInfo_var, &L_2);
		return L_3;
	}
}
// System.String System.Xml.XPath.XPathFunctionPosition::ToString()
extern "C"  String_t* XPathFunctionPosition_ToString_m1517479791 (XPathFunctionPosition_t3711683936 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionPosition_ToString_m1517479791_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		return _stringLiteral1287036886;
	}
}
// System.Void System.Xml.XPath.XPathFunctionRound::.ctor(System.Xml.XPath.FunctionArguments)
extern "C"  void XPathFunctionRound__ctor_m2903590640 (XPathFunctionRound_t711107915 * __this, FunctionArguments_t2900945452 * ___args0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionRound__ctor_m2903590640_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		FunctionArguments_t2900945452 * L_0 = ___args0;
		XPathNumericFunction__ctor_m2271660427(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t2900945452 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		FunctionArguments_t2900945452 * L_2 = ___args0;
		NullCheck(L_2);
		FunctionArguments_t2900945452 * L_3 = FunctionArguments_get_Tail_m3947258239(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0023;
		}
	}

IL_0018:
	{
		XPathException_t1503722168 * L_4 = (XPathException_t1503722168 *)il2cpp_codegen_object_new(XPathException_t1503722168_il2cpp_TypeInfo_var);
		XPathException__ctor_m3605407358(L_4, _stringLiteral586463890, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0023:
	{
		FunctionArguments_t2900945452 * L_5 = ___args0;
		NullCheck(L_5);
		Expression_t1283317256 * L_6 = FunctionArguments_get_Arg_m1790516491(L_5, /*hidden argument*/NULL);
		__this->set_arg0_0(L_6);
		return;
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionRound::get_HasStaticValue()
extern "C"  bool XPathFunctionRound_get_HasStaticValue_m2402119487 (XPathFunctionRound_t711107915 * __this, const RuntimeMethod* method)
{
	{
		Expression_t1283317256 * L_0 = __this->get_arg0_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Xml.XPath.Expression::get_HasStaticValue() */, L_0);
		return L_1;
	}
}
// System.Double System.Xml.XPath.XPathFunctionRound::get_StaticValueAsNumber()
extern "C"  double XPathFunctionRound_get_StaticValueAsNumber_m3567840505 (XPathFunctionRound_t711107915 * __this, const RuntimeMethod* method)
{
	double G_B3_0 = 0.0;
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Xml.XPath.XPathFunctionRound::get_HasStaticValue() */, __this);
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		Expression_t1283317256 * L_1 = __this->get_arg0_0();
		NullCheck(L_1);
		double L_2 = VirtFuncInvoker0< double >::Invoke(10 /* System.Double System.Xml.XPath.Expression::get_StaticValueAsNumber() */, L_1);
		double L_3 = XPathFunctionRound_Round_m791118100(__this, L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		goto IL_002a;
	}

IL_0021:
	{
		G_B3_0 = (0.0);
	}

IL_002a:
	{
		return G_B3_0;
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionRound::get_Peer()
extern "C"  bool XPathFunctionRound_get_Peer_m3775540112 (XPathFunctionRound_t711107915 * __this, const RuntimeMethod* method)
{
	{
		Expression_t1283317256 * L_0 = __this->get_arg0_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_0);
		return L_1;
	}
}
// System.Object System.Xml.XPath.XPathFunctionRound::Evaluate(System.Xml.XPath.BaseIterator)
extern "C"  RuntimeObject * XPathFunctionRound_Evaluate_m1674023549 (XPathFunctionRound_t711107915 * __this, BaseIterator_t2454437973 * ___iter0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionRound_Evaluate_m1674023549_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Expression_t1283317256 * L_0 = __this->get_arg0_0();
		BaseIterator_t2454437973 * L_1 = ___iter0;
		NullCheck(L_0);
		double L_2 = VirtFuncInvoker1< double, BaseIterator_t2454437973 * >::Invoke(16 /* System.Double System.Xml.XPath.Expression::EvaluateNumber(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		double L_3 = XPathFunctionRound_Round_m791118100(__this, L_2, /*hidden argument*/NULL);
		double L_4 = L_3;
		RuntimeObject * L_5 = Box(Double_t4078015681_il2cpp_TypeInfo_var, &L_4);
		return L_5;
	}
}
// System.Double System.Xml.XPath.XPathFunctionRound::Round(System.Double)
extern "C"  double XPathFunctionRound_Round_m791118100 (XPathFunctionRound_t711107915 * __this, double ___arg0, const RuntimeMethod* method)
{
	{
		double L_0 = ___arg0;
		if ((((double)L_0) < ((double)(-0.5))))
		{
			goto IL_001e;
		}
	}
	{
		double L_1 = ___arg0;
		if ((!(((double)L_1) > ((double)(0.0)))))
		{
			goto IL_002f;
		}
	}

IL_001e:
	{
		double L_2 = ___arg0;
		double L_3 = floor(((double)((double)L_2+(double)(0.5))));
		return L_3;
	}

IL_002f:
	{
		double L_4 = ___arg0;
		double L_5 = bankers_round(L_4);
		return L_5;
	}
}
// System.String System.Xml.XPath.XPathFunctionRound::ToString()
extern "C"  String_t* XPathFunctionRound_ToString_m4105471924 (XPathFunctionRound_t711107915 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionRound_ToString_m4105471924_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		StringU5BU5D_t1642385972* L_0 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral3236332770);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral3236332770);
		StringU5BU5D_t1642385972* L_1 = L_0;
		Expression_t1283317256 * L_2 = __this->get_arg0_0();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_3);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_3);
		StringU5BU5D_t1642385972* L_4 = L_1;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, _stringLiteral372029317);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m626692867(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Boolean System.Xml.XPath.XPathFunctions::ToBoolean(System.Object)
extern "C"  bool XPathFunctions_ToBoolean_m2866337351 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___arg0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctions_ToBoolean_m2866337351_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	double V_0 = 0.0;
	XPathNodeIterator_t3192332357 * V_1 = NULL;
	int32_t G_B8_0 = 0;
	{
		RuntimeObject * L_0 = ___arg0;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m911049464(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_000c:
	{
		RuntimeObject * L_2 = ___arg0;
		if (!((RuntimeObject *)IsInstSealed((RuntimeObject*)L_2, Boolean_t3825574718_il2cpp_TypeInfo_var)))
		{
			goto IL_001e;
		}
	}
	{
		RuntimeObject * L_3 = ___arg0;
		return ((*(bool*)((bool*)UnBox(L_3, Boolean_t3825574718_il2cpp_TypeInfo_var))));
	}

IL_001e:
	{
		RuntimeObject * L_4 = ___arg0;
		if (!((RuntimeObject *)IsInstSealed((RuntimeObject*)L_4, Double_t4078015681_il2cpp_TypeInfo_var)))
		{
			goto IL_004c;
		}
	}
	{
		RuntimeObject * L_5 = ___arg0;
		V_0 = ((*(double*)((double*)UnBox(L_5, Double_t4078015681_il2cpp_TypeInfo_var))));
		double L_6 = V_0;
		if ((((double)L_6) == ((double)(0.0))))
		{
			goto IL_004a;
		}
	}
	{
		double L_7 = V_0;
		bool L_8 = Double_IsNaN_m2289494211(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		G_B8_0 = ((((int32_t)L_8) == ((int32_t)0))? 1 : 0);
		goto IL_004b;
	}

IL_004a:
	{
		G_B8_0 = 0;
	}

IL_004b:
	{
		return (bool)G_B8_0;
	}

IL_004c:
	{
		RuntimeObject * L_9 = ___arg0;
		if (!((String_t*)IsInstSealed((RuntimeObject*)L_9, String_t_il2cpp_TypeInfo_var)))
		{
			goto IL_0069;
		}
	}
	{
		RuntimeObject * L_10 = ___arg0;
		NullCheck(((String_t*)CastclassSealed((RuntimeObject*)L_10, String_t_il2cpp_TypeInfo_var)));
		int32_t L_11 = String_get_Length_m1606060069(((String_t*)CastclassSealed((RuntimeObject*)L_10, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return (bool)((((int32_t)((((int32_t)L_11) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}

IL_0069:
	{
		RuntimeObject * L_12 = ___arg0;
		if (!((XPathNodeIterator_t3192332357 *)IsInstClass((RuntimeObject*)L_12, XPathNodeIterator_t3192332357_il2cpp_TypeInfo_var)))
		{
			goto IL_0082;
		}
	}
	{
		RuntimeObject * L_13 = ___arg0;
		V_1 = ((XPathNodeIterator_t3192332357 *)CastclassClass((RuntimeObject*)L_13, XPathNodeIterator_t3192332357_il2cpp_TypeInfo_var));
		XPathNodeIterator_t3192332357 * L_14 = V_1;
		NullCheck(L_14);
		bool L_15 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.XPathNodeIterator::MoveNext() */, L_14);
		return L_15;
	}

IL_0082:
	{
		RuntimeObject * L_16 = ___arg0;
		if (!((XPathNavigator_t3981235968 *)IsInstClass((RuntimeObject*)L_16, XPathNavigator_t3981235968_il2cpp_TypeInfo_var)))
		{
			goto IL_00a0;
		}
	}
	{
		RuntimeObject * L_17 = ___arg0;
		NullCheck(((XPathNavigator_t3981235968 *)CastclassClass((RuntimeObject*)L_17, XPathNavigator_t3981235968_il2cpp_TypeInfo_var)));
		XPathNodeIterator_t3192332357 * L_18 = VirtFuncInvoker1< XPathNodeIterator_t3192332357 *, int32_t >::Invoke(34 /* System.Xml.XPath.XPathNodeIterator System.Xml.XPath.XPathNavigator::SelectChildren(System.Xml.XPath.XPathNodeType) */, ((XPathNavigator_t3981235968 *)CastclassClass((RuntimeObject*)L_17, XPathNavigator_t3981235968_il2cpp_TypeInfo_var)), ((int32_t)9));
		bool L_19 = XPathFunctions_ToBoolean_m2866337351(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		return L_19;
	}

IL_00a0:
	{
		ArgumentException_t3259014390 * L_20 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_20, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_20);
	}
}
// System.Boolean System.Xml.XPath.XPathFunctions::ToBoolean(System.String)
extern "C"  bool XPathFunctions_ToBoolean_m309599523 (RuntimeObject * __this /* static, unused */, String_t* ___s0, const RuntimeMethod* method)
{
	int32_t G_B3_0 = 0;
	{
		String_t* L_0 = ___s0;
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		String_t* L_1 = ___s0;
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m1606060069(L_1, /*hidden argument*/NULL);
		G_B3_0 = ((((int32_t)L_2) > ((int32_t)0))? 1 : 0);
		goto IL_0012;
	}

IL_0011:
	{
		G_B3_0 = 0;
	}

IL_0012:
	{
		return (bool)G_B3_0;
	}
}
// System.String System.Xml.XPath.XPathFunctions::ToString(System.Object)
extern "C"  String_t* XPathFunctions_ToString_m908387119 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___arg0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctions_ToString_m908387119_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	XPathNodeIterator_t3192332357 * V_0 = NULL;
	String_t* G_B8_0 = NULL;
	{
		RuntimeObject * L_0 = ___arg0;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m911049464(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_000c:
	{
		RuntimeObject * L_2 = ___arg0;
		if (!((String_t*)IsInstSealed((RuntimeObject*)L_2, String_t_il2cpp_TypeInfo_var)))
		{
			goto IL_001e;
		}
	}
	{
		RuntimeObject * L_3 = ___arg0;
		return ((String_t*)CastclassSealed((RuntimeObject*)L_3, String_t_il2cpp_TypeInfo_var));
	}

IL_001e:
	{
		RuntimeObject * L_4 = ___arg0;
		if (!((RuntimeObject *)IsInstSealed((RuntimeObject*)L_4, Boolean_t3825574718_il2cpp_TypeInfo_var)))
		{
			goto IL_0044;
		}
	}
	{
		RuntimeObject * L_5 = ___arg0;
		if (!((*(bool*)((bool*)UnBox(L_5, Boolean_t3825574718_il2cpp_TypeInfo_var)))))
		{
			goto IL_003e;
		}
	}
	{
		G_B8_0 = _stringLiteral3323263070;
		goto IL_0043;
	}

IL_003e:
	{
		G_B8_0 = _stringLiteral2609877245;
	}

IL_0043:
	{
		return G_B8_0;
	}

IL_0044:
	{
		RuntimeObject * L_6 = ___arg0;
		if (!((RuntimeObject *)IsInstSealed((RuntimeObject*)L_6, Double_t4078015681_il2cpp_TypeInfo_var)))
		{
			goto IL_005b;
		}
	}
	{
		RuntimeObject * L_7 = ___arg0;
		String_t* L_8 = XPathFunctions_ToString_m2724627955(NULL /*static, unused*/, ((*(double*)((double*)UnBox(L_7, Double_t4078015681_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		return L_8;
	}

IL_005b:
	{
		RuntimeObject * L_9 = ___arg0;
		if (!((XPathNodeIterator_t3192332357 *)IsInstClass((RuntimeObject*)L_9, XPathNodeIterator_t3192332357_il2cpp_TypeInfo_var)))
		{
			goto IL_008a;
		}
	}
	{
		RuntimeObject * L_10 = ___arg0;
		V_0 = ((XPathNodeIterator_t3192332357 *)CastclassClass((RuntimeObject*)L_10, XPathNodeIterator_t3192332357_il2cpp_TypeInfo_var));
		XPathNodeIterator_t3192332357 * L_11 = V_0;
		NullCheck(L_11);
		bool L_12 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.XPathNodeIterator::MoveNext() */, L_11);
		if (L_12)
		{
			goto IL_007e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		return L_13;
	}

IL_007e:
	{
		XPathNodeIterator_t3192332357 * L_14 = V_0;
		NullCheck(L_14);
		XPathNavigator_t3981235968 * L_15 = VirtFuncInvoker0< XPathNavigator_t3981235968 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_14);
		NullCheck(L_15);
		String_t* L_16 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.Xml.XPath.XPathItem::get_Value() */, L_15);
		return L_16;
	}

IL_008a:
	{
		RuntimeObject * L_17 = ___arg0;
		if (!((XPathNavigator_t3981235968 *)IsInstClass((RuntimeObject*)L_17, XPathNavigator_t3981235968_il2cpp_TypeInfo_var)))
		{
			goto IL_00a1;
		}
	}
	{
		RuntimeObject * L_18 = ___arg0;
		NullCheck(((XPathNavigator_t3981235968 *)CastclassClass((RuntimeObject*)L_18, XPathNavigator_t3981235968_il2cpp_TypeInfo_var)));
		String_t* L_19 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.Xml.XPath.XPathItem::get_Value() */, ((XPathNavigator_t3981235968 *)CastclassClass((RuntimeObject*)L_18, XPathNavigator_t3981235968_il2cpp_TypeInfo_var)));
		return L_19;
	}

IL_00a1:
	{
		ArgumentException_t3259014390 * L_20 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_20, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_20);
	}
}
// System.String System.Xml.XPath.XPathFunctions::ToString(System.Double)
extern "C"  String_t* XPathFunctions_ToString_m2724627955 (RuntimeObject * __this /* static, unused */, double ___d0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctions_ToString_m2724627955_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		double L_0 = ___d0;
		if ((!(((double)L_0) == ((double)(-std::numeric_limits<double>::infinity())))))
		{
			goto IL_0015;
		}
	}
	{
		return _stringLiteral55341327;
	}

IL_0015:
	{
		double L_1 = ___d0;
		if ((!(((double)L_1) == ((double)(std::numeric_limits<double>::infinity())))))
		{
			goto IL_002a;
		}
	}
	{
		return _stringLiteral487594890;
	}

IL_002a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(NumberFormatInfo_t104580544_il2cpp_TypeInfo_var);
		NumberFormatInfo_t104580544 * L_2 = NumberFormatInfo_get_InvariantInfo_m2658215204(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_3 = Double_ToString_m1474956491((&___d0), _stringLiteral372029424, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Double System.Xml.XPath.XPathFunctions::ToNumber(System.Object)
extern "C"  double XPathFunctions_ToNumber_m2010435277 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___arg0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctions_ToNumber_m2010435277_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		RuntimeObject * L_0 = ___arg0;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m911049464(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_000c:
	{
		RuntimeObject * L_2 = ___arg0;
		if (((BaseIterator_t2454437973 *)IsInstClass((RuntimeObject*)L_2, BaseIterator_t2454437973_il2cpp_TypeInfo_var)))
		{
			goto IL_0022;
		}
	}
	{
		RuntimeObject * L_3 = ___arg0;
		if (!((XPathNavigator_t3981235968 *)IsInstClass((RuntimeObject*)L_3, XPathNavigator_t3981235968_il2cpp_TypeInfo_var)))
		{
			goto IL_002a;
		}
	}

IL_0022:
	{
		RuntimeObject * L_4 = ___arg0;
		String_t* L_5 = XPathFunctions_ToString_m908387119(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		___arg0 = L_5;
	}

IL_002a:
	{
		RuntimeObject * L_6 = ___arg0;
		if (!((String_t*)IsInstSealed((RuntimeObject*)L_6, String_t_il2cpp_TypeInfo_var)))
		{
			goto IL_0043;
		}
	}
	{
		RuntimeObject * L_7 = ___arg0;
		V_0 = ((String_t*)IsInstSealed((RuntimeObject*)L_7, String_t_il2cpp_TypeInfo_var));
		String_t* L_8 = V_0;
		double L_9 = XPathFunctions_ToNumber_m2754819889(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return L_9;
	}

IL_0043:
	{
		RuntimeObject * L_10 = ___arg0;
		if (!((RuntimeObject *)IsInstSealed((RuntimeObject*)L_10, Double_t4078015681_il2cpp_TypeInfo_var)))
		{
			goto IL_0055;
		}
	}
	{
		RuntimeObject * L_11 = ___arg0;
		return ((*(double*)((double*)UnBox(L_11, Double_t4078015681_il2cpp_TypeInfo_var))));
	}

IL_0055:
	{
		RuntimeObject * L_12 = ___arg0;
		if (!((RuntimeObject *)IsInstSealed((RuntimeObject*)L_12, Boolean_t3825574718_il2cpp_TypeInfo_var)))
		{
			goto IL_006c;
		}
	}
	{
		RuntimeObject * L_13 = ___arg0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		double L_14 = Convert_ToDouble_m204912726(NULL /*static, unused*/, ((*(bool*)((bool*)UnBox(L_13, Boolean_t3825574718_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		return L_14;
	}

IL_006c:
	{
		ArgumentException_t3259014390 * L_15 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_15, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_15);
	}
}
// System.Double System.Xml.XPath.XPathFunctions::ToNumber(System.String)
extern "C"  double XPathFunctions_ToNumber_m2754819889 (RuntimeObject * __this /* static, unused */, String_t* ___arg0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctions_ToNumber_m2754819889_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	double V_1 = 0.0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = ___arg0;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m911049464(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_000c:
	{
		String_t* L_2 = ___arg0;
		IL2CPP_RUNTIME_CLASS_INIT(XmlChar_t1369421061_il2cpp_TypeInfo_var);
		CharU5BU5D_t1328083999* L_3 = ((XmlChar_t1369421061_StaticFields*)il2cpp_codegen_static_fields_for(XmlChar_t1369421061_il2cpp_TypeInfo_var))->get_WhitespaceChars_0();
		NullCheck(L_2);
		String_t* L_4 = String_Trim_m3982520224(L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		String_t* L_5 = V_0;
		NullCheck(L_5);
		int32_t L_6 = String_get_Length_m1606060069(L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_002d;
		}
	}
	{
		return (std::numeric_limits<double>::quiet_NaN());
	}

IL_002d:
	try
	{ // begin try (depth: 1)
		{
			String_t* L_7 = V_0;
			NullCheck(L_7);
			Il2CppChar L_8 = String_get_Chars_m4230566705(L_7, 0, /*hidden argument*/NULL);
			if ((!(((uint32_t)L_8) == ((uint32_t)((int32_t)46)))))
			{
				goto IL_0049;
			}
		}

IL_003b:
		{
			Il2CppChar L_9 = ((Il2CppChar)((int32_t)46));
			RuntimeObject * L_10 = Box(Char_t3454481338_il2cpp_TypeInfo_var, &L_9);
			String_t* L_11 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_12 = String_Concat_m56707527(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
			V_0 = L_12;
		}

IL_0049:
		{
			String_t* L_13 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(NumberFormatInfo_t104580544_il2cpp_TypeInfo_var);
			NumberFormatInfo_t104580544 * L_14 = NumberFormatInfo_get_InvariantInfo_m2658215204(NULL /*static, unused*/, /*hidden argument*/NULL);
			double L_15 = Double_Parse_m2344589511(NULL /*static, unused*/, L_13, ((int32_t)39), L_14, /*hidden argument*/NULL);
			V_1 = L_15;
			goto IL_008b;
		}

IL_005c:
		{
			; // IL_005c: leave IL_008b
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (OverflowException_t1075868493_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0061;
		if(il2cpp_codegen_class_is_assignable_from (FormatException_t2948921286_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0076;
		throw e;
	}

CATCH_0061:
	{ // begin catch(System.OverflowException)
		{
			V_1 = (std::numeric_limits<double>::quiet_NaN());
			goto IL_008b;
		}

IL_0071:
		{
			; // IL_0071: leave IL_008b
		}
	} // end catch (depth: 1)

CATCH_0076:
	{ // begin catch(System.FormatException)
		{
			V_1 = (std::numeric_limits<double>::quiet_NaN());
			goto IL_008b;
		}

IL_0086:
		{
			; // IL_0086: leave IL_008b
		}
	} // end catch (depth: 1)

IL_008b:
	{
		double L_16 = V_1;
		return L_16;
	}
}
// System.Void System.Xml.XPath.XPathFunctionStartsWith::.ctor(System.Xml.XPath.FunctionArguments)
extern "C"  void XPathFunctionStartsWith__ctor_m2733142657 (XPathFunctionStartsWith_t1855488236 * __this, FunctionArguments_t2900945452 * ___args0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionStartsWith__ctor_m2733142657_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		FunctionArguments_t2900945452 * L_0 = ___args0;
		XPathFunction__ctor_m327599860(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t2900945452 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_0028;
		}
	}
	{
		FunctionArguments_t2900945452 * L_2 = ___args0;
		NullCheck(L_2);
		FunctionArguments_t2900945452 * L_3 = FunctionArguments_get_Tail_m3947258239(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0028;
		}
	}
	{
		FunctionArguments_t2900945452 * L_4 = ___args0;
		NullCheck(L_4);
		FunctionArguments_t2900945452 * L_5 = FunctionArguments_get_Tail_m3947258239(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		FunctionArguments_t2900945452 * L_6 = FunctionArguments_get_Tail_m3947258239(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0033;
		}
	}

IL_0028:
	{
		XPathException_t1503722168 * L_7 = (XPathException_t1503722168 *)il2cpp_codegen_object_new(XPathException_t1503722168_il2cpp_TypeInfo_var);
		XPathException__ctor_m3605407358(L_7, _stringLiteral1938299717, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_0033:
	{
		FunctionArguments_t2900945452 * L_8 = ___args0;
		NullCheck(L_8);
		Expression_t1283317256 * L_9 = FunctionArguments_get_Arg_m1790516491(L_8, /*hidden argument*/NULL);
		__this->set_arg0_0(L_9);
		FunctionArguments_t2900945452 * L_10 = ___args0;
		NullCheck(L_10);
		FunctionArguments_t2900945452 * L_11 = FunctionArguments_get_Tail_m3947258239(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		Expression_t1283317256 * L_12 = FunctionArguments_get_Arg_m1790516491(L_11, /*hidden argument*/NULL);
		__this->set_arg1_1(L_12);
		return;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.XPathFunctionStartsWith::get_ReturnType()
extern "C"  int32_t XPathFunctionStartsWith_get_ReturnType_m3707999567 (XPathFunctionStartsWith_t1855488236 * __this, const RuntimeMethod* method)
{
	{
		return (int32_t)(2);
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionStartsWith::get_Peer()
extern "C"  bool XPathFunctionStartsWith_get_Peer_m208855191 (XPathFunctionStartsWith_t1855488236 * __this, const RuntimeMethod* method)
{
	int32_t G_B3_0 = 0;
	{
		Expression_t1283317256 * L_0 = __this->get_arg0_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_0);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		Expression_t1283317256 * L_2 = __this->get_arg1_1();
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_2);
		G_B3_0 = ((int32_t)(L_3));
		goto IL_001e;
	}

IL_001d:
	{
		G_B3_0 = 0;
	}

IL_001e:
	{
		return (bool)G_B3_0;
	}
}
// System.Object System.Xml.XPath.XPathFunctionStartsWith::Evaluate(System.Xml.XPath.BaseIterator)
extern "C"  RuntimeObject * XPathFunctionStartsWith_Evaluate_m1045380392 (XPathFunctionStartsWith_t1855488236 * __this, BaseIterator_t2454437973 * ___iter0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionStartsWith_Evaluate_m1045380392_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Expression_t1283317256 * L_0 = __this->get_arg0_0();
		BaseIterator_t2454437973 * L_1 = ___iter0;
		NullCheck(L_0);
		String_t* L_2 = VirtFuncInvoker1< String_t*, BaseIterator_t2454437973 * >::Invoke(17 /* System.String System.Xml.XPath.Expression::EvaluateString(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		Expression_t1283317256 * L_3 = __this->get_arg1_1();
		BaseIterator_t2454437973 * L_4 = ___iter0;
		NullCheck(L_3);
		String_t* L_5 = VirtFuncInvoker1< String_t*, BaseIterator_t2454437973 * >::Invoke(17 /* System.String System.Xml.XPath.Expression::EvaluateString(System.Xml.XPath.BaseIterator) */, L_3, L_4);
		NullCheck(L_2);
		bool L_6 = String_StartsWith_m1841920685(L_2, L_5, /*hidden argument*/NULL);
		bool L_7 = L_6;
		RuntimeObject * L_8 = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &L_7);
		return L_8;
	}
}
// System.String System.Xml.XPath.XPathFunctionStartsWith::ToString()
extern "C"  String_t* XPathFunctionStartsWith_ToString_m3285628411 (XPathFunctionStartsWith_t1855488236 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionStartsWith_ToString_m3285628411_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		StringU5BU5D_t1642385972* L_0 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral247280470);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral247280470);
		StringU5BU5D_t1642385972* L_1 = L_0;
		Expression_t1283317256 * L_2 = __this->get_arg0_0();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_3);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_3);
		StringU5BU5D_t1642385972* L_4 = L_1;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, _stringLiteral372029314);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral372029314);
		StringU5BU5D_t1642385972* L_5 = L_4;
		Expression_t1283317256 * L_6 = __this->get_arg1_1();
		NullCheck(L_6);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_6);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_7);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_7);
		StringU5BU5D_t1642385972* L_8 = L_5;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, _stringLiteral372029317);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m626692867(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// System.Void System.Xml.XPath.XPathFunctionString::.ctor(System.Xml.XPath.FunctionArguments)
extern "C"  void XPathFunctionString__ctor_m408009393 (XPathFunctionString_t3963481524 * __this, FunctionArguments_t2900945452 * ___args0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionString__ctor_m408009393_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		FunctionArguments_t2900945452 * L_0 = ___args0;
		XPathFunction__ctor_m327599860(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t2900945452 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		FunctionArguments_t2900945452 * L_2 = ___args0;
		NullCheck(L_2);
		Expression_t1283317256 * L_3 = FunctionArguments_get_Arg_m1790516491(L_2, /*hidden argument*/NULL);
		__this->set_arg0_0(L_3);
		FunctionArguments_t2900945452 * L_4 = ___args0;
		NullCheck(L_4);
		FunctionArguments_t2900945452 * L_5 = FunctionArguments_get_Tail_m3947258239(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002f;
		}
	}
	{
		XPathException_t1503722168 * L_6 = (XPathException_t1503722168 *)il2cpp_codegen_object_new(XPathException_t1503722168_il2cpp_TypeInfo_var);
		XPathException__ctor_m3605407358(L_6, _stringLiteral2614059802, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_002f:
	{
		return;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.XPathFunctionString::get_ReturnType()
extern "C"  int32_t XPathFunctionString_get_ReturnType_m1026546691 (XPathFunctionString_t3963481524 * __this, const RuntimeMethod* method)
{
	{
		return (int32_t)(1);
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionString::get_Peer()
extern "C"  bool XPathFunctionString_get_Peer_m3451050091 (XPathFunctionString_t3963481524 * __this, const RuntimeMethod* method)
{
	int32_t G_B3_0 = 0;
	{
		Expression_t1283317256 * L_0 = __this->get_arg0_0();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		Expression_t1283317256 * L_1 = __this->get_arg0_0();
		NullCheck(L_1);
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_1);
		G_B3_0 = ((int32_t)(L_2));
		goto IL_001c;
	}

IL_001b:
	{
		G_B3_0 = 1;
	}

IL_001c:
	{
		return (bool)G_B3_0;
	}
}
// System.Object System.Xml.XPath.XPathFunctionString::Evaluate(System.Xml.XPath.BaseIterator)
extern "C"  RuntimeObject * XPathFunctionString_Evaluate_m1383694564 (XPathFunctionString_t3963481524 * __this, BaseIterator_t2454437973 * ___iter0, const RuntimeMethod* method)
{
	{
		Expression_t1283317256 * L_0 = __this->get_arg0_0();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		BaseIterator_t2454437973 * L_1 = ___iter0;
		NullCheck(L_1);
		XPathNavigator_t3981235968 * L_2 = VirtFuncInvoker0< XPathNavigator_t3981235968 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_1);
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.Xml.XPath.XPathItem::get_Value() */, L_2);
		return L_3;
	}

IL_0017:
	{
		Expression_t1283317256 * L_4 = __this->get_arg0_0();
		BaseIterator_t2454437973 * L_5 = ___iter0;
		NullCheck(L_4);
		String_t* L_6 = VirtFuncInvoker1< String_t*, BaseIterator_t2454437973 * >::Invoke(17 /* System.String System.Xml.XPath.Expression::EvaluateString(System.Xml.XPath.BaseIterator) */, L_4, L_5);
		return L_6;
	}
}
// System.String System.Xml.XPath.XPathFunctionString::ToString()
extern "C"  String_t* XPathFunctionString_ToString_m3904874255 (XPathFunctionString_t3963481524 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionString_ToString_m3904874255_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Expression_t1283317256 * L_0 = __this->get_arg0_0();
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral910930429, L_1, _stringLiteral372029317, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void System.Xml.XPath.XPathFunctionStringLength::.ctor(System.Xml.XPath.FunctionArguments)
extern "C"  void XPathFunctionStringLength__ctor_m4250833153 (XPathFunctionStringLength_t2821584396 * __this, FunctionArguments_t2900945452 * ___args0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionStringLength__ctor_m4250833153_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		FunctionArguments_t2900945452 * L_0 = ___args0;
		XPathFunction__ctor_m327599860(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t2900945452 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		FunctionArguments_t2900945452 * L_2 = ___args0;
		NullCheck(L_2);
		Expression_t1283317256 * L_3 = FunctionArguments_get_Arg_m1790516491(L_2, /*hidden argument*/NULL);
		__this->set_arg0_0(L_3);
		FunctionArguments_t2900945452 * L_4 = ___args0;
		NullCheck(L_4);
		FunctionArguments_t2900945452 * L_5 = FunctionArguments_get_Tail_m3947258239(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002f;
		}
	}
	{
		XPathException_t1503722168 * L_6 = (XPathException_t1503722168 *)il2cpp_codegen_object_new(XPathException_t1503722168_il2cpp_TypeInfo_var);
		XPathException__ctor_m3605407358(L_6, _stringLiteral1405175745, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_002f:
	{
		return;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.XPathFunctionStringLength::get_ReturnType()
extern "C"  int32_t XPathFunctionStringLength_get_ReturnType_m2819246327 (XPathFunctionStringLength_t2821584396 * __this, const RuntimeMethod* method)
{
	{
		return (int32_t)(0);
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionStringLength::get_Peer()
extern "C"  bool XPathFunctionStringLength_get_Peer_m940120679 (XPathFunctionStringLength_t2821584396 * __this, const RuntimeMethod* method)
{
	int32_t G_B3_0 = 0;
	{
		Expression_t1283317256 * L_0 = __this->get_arg0_0();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		Expression_t1283317256 * L_1 = __this->get_arg0_0();
		NullCheck(L_1);
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_1);
		G_B3_0 = ((int32_t)(L_2));
		goto IL_001c;
	}

IL_001b:
	{
		G_B3_0 = 1;
	}

IL_001c:
	{
		return (bool)G_B3_0;
	}
}
// System.Object System.Xml.XPath.XPathFunctionStringLength::Evaluate(System.Xml.XPath.BaseIterator)
extern "C"  RuntimeObject * XPathFunctionStringLength_Evaluate_m1453954152 (XPathFunctionStringLength_t2821584396 * __this, BaseIterator_t2454437973 * ___iter0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionStringLength_Evaluate_m1453954152_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		Expression_t1283317256 * L_0 = __this->get_arg0_0();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Expression_t1283317256 * L_1 = __this->get_arg0_0();
		BaseIterator_t2454437973 * L_2 = ___iter0;
		NullCheck(L_1);
		String_t* L_3 = VirtFuncInvoker1< String_t*, BaseIterator_t2454437973 * >::Invoke(17 /* System.String System.Xml.XPath.Expression::EvaluateString(System.Xml.XPath.BaseIterator) */, L_1, L_2);
		V_0 = L_3;
		goto IL_0029;
	}

IL_001d:
	{
		BaseIterator_t2454437973 * L_4 = ___iter0;
		NullCheck(L_4);
		XPathNavigator_t3981235968 * L_5 = VirtFuncInvoker0< XPathNavigator_t3981235968 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_4);
		NullCheck(L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.Xml.XPath.XPathItem::get_Value() */, L_5);
		V_0 = L_6;
	}

IL_0029:
	{
		String_t* L_7 = V_0;
		NullCheck(L_7);
		int32_t L_8 = String_get_Length_m1606060069(L_7, /*hidden argument*/NULL);
		double L_9 = (((double)((double)L_8)));
		RuntimeObject * L_10 = Box(Double_t4078015681_il2cpp_TypeInfo_var, &L_9);
		return L_10;
	}
}
// System.String System.Xml.XPath.XPathFunctionStringLength::ToString()
extern "C"  String_t* XPathFunctionStringLength_ToString_m1068025283 (XPathFunctionStringLength_t2821584396 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionStringLength_ToString_m1068025283_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		StringU5BU5D_t1642385972* L_0 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral1827829842);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral1827829842);
		StringU5BU5D_t1642385972* L_1 = L_0;
		Expression_t1283317256 * L_2 = __this->get_arg0_0();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_3);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_3);
		StringU5BU5D_t1642385972* L_4 = L_1;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, _stringLiteral372029317);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m626692867(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void System.Xml.XPath.XPathFunctionSubstring::.ctor(System.Xml.XPath.FunctionArguments)
extern "C"  void XPathFunctionSubstring__ctor_m159311037 (XPathFunctionSubstring_t2124841904 * __this, FunctionArguments_t2900945452 * ___args0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionSubstring__ctor_m159311037_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		FunctionArguments_t2900945452 * L_0 = ___args0;
		XPathFunction__ctor_m327599860(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t2900945452 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_003d;
		}
	}
	{
		FunctionArguments_t2900945452 * L_2 = ___args0;
		NullCheck(L_2);
		FunctionArguments_t2900945452 * L_3 = FunctionArguments_get_Tail_m3947258239(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_003d;
		}
	}
	{
		FunctionArguments_t2900945452 * L_4 = ___args0;
		NullCheck(L_4);
		FunctionArguments_t2900945452 * L_5 = FunctionArguments_get_Tail_m3947258239(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		FunctionArguments_t2900945452 * L_6 = FunctionArguments_get_Tail_m3947258239(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0048;
		}
	}
	{
		FunctionArguments_t2900945452 * L_7 = ___args0;
		NullCheck(L_7);
		FunctionArguments_t2900945452 * L_8 = FunctionArguments_get_Tail_m3947258239(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		FunctionArguments_t2900945452 * L_9 = FunctionArguments_get_Tail_m3947258239(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		FunctionArguments_t2900945452 * L_10 = FunctionArguments_get_Tail_m3947258239(L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0048;
		}
	}

IL_003d:
	{
		XPathException_t1503722168 * L_11 = (XPathException_t1503722168 *)il2cpp_codegen_object_new(XPathException_t1503722168_il2cpp_TypeInfo_var);
		XPathException__ctor_m3605407358(L_11, _stringLiteral2290278570, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
	}

IL_0048:
	{
		FunctionArguments_t2900945452 * L_12 = ___args0;
		NullCheck(L_12);
		Expression_t1283317256 * L_13 = FunctionArguments_get_Arg_m1790516491(L_12, /*hidden argument*/NULL);
		__this->set_arg0_0(L_13);
		FunctionArguments_t2900945452 * L_14 = ___args0;
		NullCheck(L_14);
		FunctionArguments_t2900945452 * L_15 = FunctionArguments_get_Tail_m3947258239(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		Expression_t1283317256 * L_16 = FunctionArguments_get_Arg_m1790516491(L_15, /*hidden argument*/NULL);
		__this->set_arg1_1(L_16);
		FunctionArguments_t2900945452 * L_17 = ___args0;
		NullCheck(L_17);
		FunctionArguments_t2900945452 * L_18 = FunctionArguments_get_Tail_m3947258239(L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		FunctionArguments_t2900945452 * L_19 = FunctionArguments_get_Tail_m3947258239(L_18, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_008b;
		}
	}
	{
		FunctionArguments_t2900945452 * L_20 = ___args0;
		NullCheck(L_20);
		FunctionArguments_t2900945452 * L_21 = FunctionArguments_get_Tail_m3947258239(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		FunctionArguments_t2900945452 * L_22 = FunctionArguments_get_Tail_m3947258239(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		Expression_t1283317256 * L_23 = FunctionArguments_get_Arg_m1790516491(L_22, /*hidden argument*/NULL);
		__this->set_arg2_2(L_23);
	}

IL_008b:
	{
		return;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.XPathFunctionSubstring::get_ReturnType()
extern "C"  int32_t XPathFunctionSubstring_get_ReturnType_m716534527 (XPathFunctionSubstring_t2124841904 * __this, const RuntimeMethod* method)
{
	{
		return (int32_t)(1);
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionSubstring::get_Peer()
extern "C"  bool XPathFunctionSubstring_get_Peer_m3657998975 (XPathFunctionSubstring_t2124841904 * __this, const RuntimeMethod* method)
{
	int32_t G_B5_0 = 0;
	int32_t G_B7_0 = 0;
	{
		Expression_t1283317256 * L_0 = __this->get_arg0_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_0);
		if (!L_1)
		{
			goto IL_003e;
		}
	}
	{
		Expression_t1283317256 * L_2 = __this->get_arg1_1();
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_2);
		if (!L_3)
		{
			goto IL_003e;
		}
	}
	{
		Expression_t1283317256 * L_4 = __this->get_arg2_2();
		if (!L_4)
		{
			goto IL_003b;
		}
	}
	{
		Expression_t1283317256 * L_5 = __this->get_arg2_2();
		NullCheck(L_5);
		bool L_6 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_5);
		G_B5_0 = ((int32_t)(L_6));
		goto IL_003c;
	}

IL_003b:
	{
		G_B5_0 = 1;
	}

IL_003c:
	{
		G_B7_0 = G_B5_0;
		goto IL_003f;
	}

IL_003e:
	{
		G_B7_0 = 0;
	}

IL_003f:
	{
		return (bool)G_B7_0;
	}
}
// System.Object System.Xml.XPath.XPathFunctionSubstring::Evaluate(System.Xml.XPath.BaseIterator)
extern "C"  RuntimeObject * XPathFunctionSubstring_Evaluate_m1994297472 (XPathFunctionSubstring_t2124841904 * __this, BaseIterator_t2454437973 * ___iter0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionSubstring_Evaluate_m1994297472_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	double V_1 = 0.0;
	double V_2 = 0.0;
	double V_3 = 0.0;
	{
		Expression_t1283317256 * L_0 = __this->get_arg0_0();
		BaseIterator_t2454437973 * L_1 = ___iter0;
		NullCheck(L_0);
		String_t* L_2 = VirtFuncInvoker1< String_t*, BaseIterator_t2454437973 * >::Invoke(17 /* System.String System.Xml.XPath.Expression::EvaluateString(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		V_0 = L_2;
		Expression_t1283317256 * L_3 = __this->get_arg1_1();
		BaseIterator_t2454437973 * L_4 = ___iter0;
		NullCheck(L_3);
		double L_5 = VirtFuncInvoker1< double, BaseIterator_t2454437973 * >::Invoke(16 /* System.Double System.Xml.XPath.Expression::EvaluateNumber(System.Xml.XPath.BaseIterator) */, L_3, L_4);
		double L_6 = bankers_round(L_5);
		V_1 = ((double)((double)L_6-(double)(1.0)));
		double L_7 = V_1;
		bool L_8 = Double_IsNaN_m2289494211(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_004c;
		}
	}
	{
		double L_9 = V_1;
		bool L_10 = Double_IsNegativeInfinity_m1111913183(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_004c;
		}
	}
	{
		double L_11 = V_1;
		String_t* L_12 = V_0;
		NullCheck(L_12);
		int32_t L_13 = String_get_Length_m1606060069(L_12, /*hidden argument*/NULL);
		if ((!(((double)L_11) >= ((double)(((double)((double)L_13)))))))
		{
			goto IL_0052;
		}
	}

IL_004c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		return L_14;
	}

IL_0052:
	{
		Expression_t1283317256 * L_15 = __this->get_arg2_2();
		if (L_15)
		{
			goto IL_007f;
		}
	}
	{
		double L_16 = V_1;
		if ((!(((double)L_16) < ((double)(0.0)))))
		{
			goto IL_0076;
		}
	}
	{
		V_1 = (0.0);
	}

IL_0076:
	{
		String_t* L_17 = V_0;
		double L_18 = V_1;
		NullCheck(L_17);
		String_t* L_19 = String_Substring_m2032624251(L_17, (((int32_t)((int32_t)L_18))), /*hidden argument*/NULL);
		return L_19;
	}

IL_007f:
	{
		Expression_t1283317256 * L_20 = __this->get_arg2_2();
		BaseIterator_t2454437973 * L_21 = ___iter0;
		NullCheck(L_20);
		double L_22 = VirtFuncInvoker1< double, BaseIterator_t2454437973 * >::Invoke(16 /* System.Double System.Xml.XPath.Expression::EvaluateNumber(System.Xml.XPath.BaseIterator) */, L_20, L_21);
		double L_23 = bankers_round(L_22);
		V_2 = L_23;
		double L_24 = V_2;
		bool L_25 = Double_IsNaN_m2289494211(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00a2;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_26 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		return L_26;
	}

IL_00a2:
	{
		double L_27 = V_1;
		if ((((double)L_27) < ((double)(0.0))))
		{
			goto IL_00c0;
		}
	}
	{
		double L_28 = V_2;
		if ((!(((double)L_28) < ((double)(0.0)))))
		{
			goto IL_00e3;
		}
	}

IL_00c0:
	{
		double L_29 = V_1;
		double L_30 = V_2;
		V_2 = ((double)((double)L_29+(double)L_30));
		double L_31 = V_2;
		if ((!(((double)L_31) <= ((double)(0.0)))))
		{
			goto IL_00d9;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		return L_32;
	}

IL_00d9:
	{
		V_1 = (0.0);
	}

IL_00e3:
	{
		String_t* L_33 = V_0;
		NullCheck(L_33);
		int32_t L_34 = String_get_Length_m1606060069(L_33, /*hidden argument*/NULL);
		double L_35 = V_1;
		V_3 = ((double)((double)(((double)((double)L_34)))-(double)L_35));
		double L_36 = V_2;
		double L_37 = V_3;
		if ((!(((double)L_36) > ((double)L_37))))
		{
			goto IL_00f6;
		}
	}
	{
		double L_38 = V_3;
		V_2 = L_38;
	}

IL_00f6:
	{
		String_t* L_39 = V_0;
		double L_40 = V_1;
		double L_41 = V_2;
		NullCheck(L_39);
		String_t* L_42 = String_Substring_m12482732(L_39, (((int32_t)((int32_t)L_40))), (((int32_t)((int32_t)L_41))), /*hidden argument*/NULL);
		return L_42;
	}
}
// System.String System.Xml.XPath.XPathFunctionSubstring::ToString()
extern "C"  String_t* XPathFunctionSubstring_ToString_m282664695 (XPathFunctionSubstring_t2124841904 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionSubstring_ToString_m282664695_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		StringU5BU5D_t1642385972* L_0 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)7));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral808858977);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral808858977);
		StringU5BU5D_t1642385972* L_1 = L_0;
		Expression_t1283317256 * L_2 = __this->get_arg0_0();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_3);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_3);
		StringU5BU5D_t1642385972* L_4 = L_1;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, _stringLiteral372029314);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral372029314);
		StringU5BU5D_t1642385972* L_5 = L_4;
		Expression_t1283317256 * L_6 = __this->get_arg1_1();
		NullCheck(L_6);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_6);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_7);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_7);
		StringU5BU5D_t1642385972* L_8 = L_5;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, _stringLiteral372029314);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029314);
		StringU5BU5D_t1642385972* L_9 = L_8;
		Expression_t1283317256 * L_10 = __this->get_arg2_2();
		NullCheck(L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_10);
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, L_11);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)L_11);
		StringU5BU5D_t1642385972* L_12 = L_9;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral372029317);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(6), (String_t*)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m626692867(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
// System.Void System.Xml.XPath.XPathFunctionSubstringAfter::.ctor(System.Xml.XPath.FunctionArguments)
extern "C"  void XPathFunctionSubstringAfter__ctor_m4176727017 (XPathFunctionSubstringAfter_t1560316700 * __this, FunctionArguments_t2900945452 * ___args0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionSubstringAfter__ctor_m4176727017_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		FunctionArguments_t2900945452 * L_0 = ___args0;
		XPathFunction__ctor_m327599860(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t2900945452 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_0028;
		}
	}
	{
		FunctionArguments_t2900945452 * L_2 = ___args0;
		NullCheck(L_2);
		FunctionArguments_t2900945452 * L_3 = FunctionArguments_get_Tail_m3947258239(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0028;
		}
	}
	{
		FunctionArguments_t2900945452 * L_4 = ___args0;
		NullCheck(L_4);
		FunctionArguments_t2900945452 * L_5 = FunctionArguments_get_Tail_m3947258239(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		FunctionArguments_t2900945452 * L_6 = FunctionArguments_get_Tail_m3947258239(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0033;
		}
	}

IL_0028:
	{
		XPathException_t1503722168 * L_7 = (XPathException_t1503722168 *)il2cpp_codegen_object_new(XPathException_t1503722168_il2cpp_TypeInfo_var);
		XPathException__ctor_m3605407358(L_7, _stringLiteral903430261, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_0033:
	{
		FunctionArguments_t2900945452 * L_8 = ___args0;
		NullCheck(L_8);
		Expression_t1283317256 * L_9 = FunctionArguments_get_Arg_m1790516491(L_8, /*hidden argument*/NULL);
		__this->set_arg0_0(L_9);
		FunctionArguments_t2900945452 * L_10 = ___args0;
		NullCheck(L_10);
		FunctionArguments_t2900945452 * L_11 = FunctionArguments_get_Tail_m3947258239(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		Expression_t1283317256 * L_12 = FunctionArguments_get_Arg_m1790516491(L_11, /*hidden argument*/NULL);
		__this->set_arg1_1(L_12);
		return;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.XPathFunctionSubstringAfter::get_ReturnType()
extern "C"  int32_t XPathFunctionSubstringAfter_get_ReturnType_m773333815 (XPathFunctionSubstringAfter_t1560316700 * __this, const RuntimeMethod* method)
{
	{
		return (int32_t)(1);
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionSubstringAfter::get_Peer()
extern "C"  bool XPathFunctionSubstringAfter_get_Peer_m2092456095 (XPathFunctionSubstringAfter_t1560316700 * __this, const RuntimeMethod* method)
{
	int32_t G_B3_0 = 0;
	{
		Expression_t1283317256 * L_0 = __this->get_arg0_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_0);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		Expression_t1283317256 * L_2 = __this->get_arg1_1();
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_2);
		G_B3_0 = ((int32_t)(L_3));
		goto IL_001e;
	}

IL_001d:
	{
		G_B3_0 = 0;
	}

IL_001e:
	{
		return (bool)G_B3_0;
	}
}
// System.Object System.Xml.XPath.XPathFunctionSubstringAfter::Evaluate(System.Xml.XPath.BaseIterator)
extern "C"  RuntimeObject * XPathFunctionSubstringAfter_Evaluate_m2110056486 (XPathFunctionSubstringAfter_t1560316700 * __this, BaseIterator_t2454437973 * ___iter0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionSubstringAfter_Evaluate_m2110056486_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	int32_t V_2 = 0;
	{
		Expression_t1283317256 * L_0 = __this->get_arg0_0();
		BaseIterator_t2454437973 * L_1 = ___iter0;
		NullCheck(L_0);
		String_t* L_2 = VirtFuncInvoker1< String_t*, BaseIterator_t2454437973 * >::Invoke(17 /* System.String System.Xml.XPath.Expression::EvaluateString(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		V_0 = L_2;
		Expression_t1283317256 * L_3 = __this->get_arg1_1();
		BaseIterator_t2454437973 * L_4 = ___iter0;
		NullCheck(L_3);
		String_t* L_5 = VirtFuncInvoker1< String_t*, BaseIterator_t2454437973 * >::Invoke(17 /* System.String System.Xml.XPath.Expression::EvaluateString(System.Xml.XPath.BaseIterator) */, L_3, L_4);
		V_1 = L_5;
		String_t* L_6 = V_0;
		String_t* L_7 = V_1;
		NullCheck(L_6);
		int32_t L_8 = String_IndexOf_m4251815737(L_6, L_7, /*hidden argument*/NULL);
		V_2 = L_8;
		int32_t L_9 = V_2;
		if ((((int32_t)L_9) >= ((int32_t)0)))
		{
			goto IL_002f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		return L_10;
	}

IL_002f:
	{
		String_t* L_11 = V_0;
		int32_t L_12 = V_2;
		String_t* L_13 = V_1;
		NullCheck(L_13);
		int32_t L_14 = String_get_Length_m1606060069(L_13, /*hidden argument*/NULL);
		NullCheck(L_11);
		String_t* L_15 = String_Substring_m2032624251(L_11, ((int32_t)((int32_t)L_12+(int32_t)L_14)), /*hidden argument*/NULL);
		return L_15;
	}
}
// System.String System.Xml.XPath.XPathFunctionSubstringAfter::ToString()
extern "C"  String_t* XPathFunctionSubstringAfter_ToString_m2115706811 (XPathFunctionSubstringAfter_t1560316700 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionSubstringAfter_ToString_m2115706811_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		StringU5BU5D_t1642385972* L_0 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral1134393990);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral1134393990);
		StringU5BU5D_t1642385972* L_1 = L_0;
		Expression_t1283317256 * L_2 = __this->get_arg0_0();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_3);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_3);
		StringU5BU5D_t1642385972* L_4 = L_1;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, _stringLiteral372029314);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral372029314);
		StringU5BU5D_t1642385972* L_5 = L_4;
		Expression_t1283317256 * L_6 = __this->get_arg1_1();
		NullCheck(L_6);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_6);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_7);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_7);
		StringU5BU5D_t1642385972* L_8 = L_5;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, _stringLiteral372029317);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m626692867(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// System.Void System.Xml.XPath.XPathFunctionSubstringBefore::.ctor(System.Xml.XPath.FunctionArguments)
extern "C"  void XPathFunctionSubstringBefore__ctor_m3051703460 (XPathFunctionSubstringBefore_t1797727619 * __this, FunctionArguments_t2900945452 * ___args0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionSubstringBefore__ctor_m3051703460_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		FunctionArguments_t2900945452 * L_0 = ___args0;
		XPathFunction__ctor_m327599860(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t2900945452 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_0028;
		}
	}
	{
		FunctionArguments_t2900945452 * L_2 = ___args0;
		NullCheck(L_2);
		FunctionArguments_t2900945452 * L_3 = FunctionArguments_get_Tail_m3947258239(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0028;
		}
	}
	{
		FunctionArguments_t2900945452 * L_4 = ___args0;
		NullCheck(L_4);
		FunctionArguments_t2900945452 * L_5 = FunctionArguments_get_Tail_m3947258239(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		FunctionArguments_t2900945452 * L_6 = FunctionArguments_get_Tail_m3947258239(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0033;
		}
	}

IL_0028:
	{
		XPathException_t1503722168 * L_7 = (XPathException_t1503722168 *)il2cpp_codegen_object_new(XPathException_t1503722168_il2cpp_TypeInfo_var);
		XPathException__ctor_m3605407358(L_7, _stringLiteral2870964214, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_0033:
	{
		FunctionArguments_t2900945452 * L_8 = ___args0;
		NullCheck(L_8);
		Expression_t1283317256 * L_9 = FunctionArguments_get_Arg_m1790516491(L_8, /*hidden argument*/NULL);
		__this->set_arg0_0(L_9);
		FunctionArguments_t2900945452 * L_10 = ___args0;
		NullCheck(L_10);
		FunctionArguments_t2900945452 * L_11 = FunctionArguments_get_Tail_m3947258239(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		Expression_t1283317256 * L_12 = FunctionArguments_get_Arg_m1790516491(L_11, /*hidden argument*/NULL);
		__this->set_arg1_1(L_12);
		return;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.XPathFunctionSubstringBefore::get_ReturnType()
extern "C"  int32_t XPathFunctionSubstringBefore_get_ReturnType_m2285774286 (XPathFunctionSubstringBefore_t1797727619 * __this, const RuntimeMethod* method)
{
	{
		return (int32_t)(1);
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionSubstringBefore::get_Peer()
extern "C"  bool XPathFunctionSubstringBefore_get_Peer_m2968056856 (XPathFunctionSubstringBefore_t1797727619 * __this, const RuntimeMethod* method)
{
	int32_t G_B3_0 = 0;
	{
		Expression_t1283317256 * L_0 = __this->get_arg0_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_0);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		Expression_t1283317256 * L_2 = __this->get_arg1_1();
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_2);
		G_B3_0 = ((int32_t)(L_3));
		goto IL_001e;
	}

IL_001d:
	{
		G_B3_0 = 0;
	}

IL_001e:
	{
		return (bool)G_B3_0;
	}
}
// System.Object System.Xml.XPath.XPathFunctionSubstringBefore::Evaluate(System.Xml.XPath.BaseIterator)
extern "C"  RuntimeObject * XPathFunctionSubstringBefore_Evaluate_m366996147 (XPathFunctionSubstringBefore_t1797727619 * __this, BaseIterator_t2454437973 * ___iter0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionSubstringBefore_Evaluate_m366996147_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	int32_t V_2 = 0;
	{
		Expression_t1283317256 * L_0 = __this->get_arg0_0();
		BaseIterator_t2454437973 * L_1 = ___iter0;
		NullCheck(L_0);
		String_t* L_2 = VirtFuncInvoker1< String_t*, BaseIterator_t2454437973 * >::Invoke(17 /* System.String System.Xml.XPath.Expression::EvaluateString(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		V_0 = L_2;
		Expression_t1283317256 * L_3 = __this->get_arg1_1();
		BaseIterator_t2454437973 * L_4 = ___iter0;
		NullCheck(L_3);
		String_t* L_5 = VirtFuncInvoker1< String_t*, BaseIterator_t2454437973 * >::Invoke(17 /* System.String System.Xml.XPath.Expression::EvaluateString(System.Xml.XPath.BaseIterator) */, L_3, L_4);
		V_1 = L_5;
		String_t* L_6 = V_0;
		String_t* L_7 = V_1;
		NullCheck(L_6);
		int32_t L_8 = String_IndexOf_m4251815737(L_6, L_7, /*hidden argument*/NULL);
		V_2 = L_8;
		int32_t L_9 = V_2;
		if ((((int32_t)L_9) > ((int32_t)0)))
		{
			goto IL_002f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		return L_10;
	}

IL_002f:
	{
		String_t* L_11 = V_0;
		int32_t L_12 = V_2;
		NullCheck(L_11);
		String_t* L_13 = String_Substring_m12482732(L_11, 0, L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
// System.String System.Xml.XPath.XPathFunctionSubstringBefore::ToString()
extern "C"  String_t* XPathFunctionSubstringBefore_ToString_m2067029356 (XPathFunctionSubstringBefore_t1797727619 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionSubstringBefore_ToString_m2067029356_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		StringU5BU5D_t1642385972* L_0 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral2049758785);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral2049758785);
		StringU5BU5D_t1642385972* L_1 = L_0;
		Expression_t1283317256 * L_2 = __this->get_arg0_0();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_3);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_3);
		StringU5BU5D_t1642385972* L_4 = L_1;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, _stringLiteral372029314);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral372029314);
		StringU5BU5D_t1642385972* L_5 = L_4;
		Expression_t1283317256 * L_6 = __this->get_arg1_1();
		NullCheck(L_6);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_6);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_7);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_7);
		StringU5BU5D_t1642385972* L_8 = L_5;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, _stringLiteral372029317);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m626692867(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// System.Void System.Xml.XPath.XPathFunctionSum::.ctor(System.Xml.XPath.FunctionArguments)
extern "C"  void XPathFunctionSum__ctor_m104079019 (XPathFunctionSum_t2438242634 * __this, FunctionArguments_t2900945452 * ___args0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionSum__ctor_m104079019_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		FunctionArguments_t2900945452 * L_0 = ___args0;
		XPathNumericFunction__ctor_m2271660427(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t2900945452 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		FunctionArguments_t2900945452 * L_2 = ___args0;
		NullCheck(L_2);
		FunctionArguments_t2900945452 * L_3 = FunctionArguments_get_Tail_m3947258239(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0023;
		}
	}

IL_0018:
	{
		XPathException_t1503722168 * L_4 = (XPathException_t1503722168 *)il2cpp_codegen_object_new(XPathException_t1503722168_il2cpp_TypeInfo_var);
		XPathException__ctor_m3605407358(L_4, _stringLiteral4210209539, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0023:
	{
		FunctionArguments_t2900945452 * L_5 = ___args0;
		NullCheck(L_5);
		Expression_t1283317256 * L_6 = FunctionArguments_get_Arg_m1790516491(L_5, /*hidden argument*/NULL);
		__this->set_arg0_0(L_6);
		return;
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionSum::get_Peer()
extern "C"  bool XPathFunctionSum_get_Peer_m2383150609 (XPathFunctionSum_t2438242634 * __this, const RuntimeMethod* method)
{
	{
		Expression_t1283317256 * L_0 = __this->get_arg0_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_0);
		return L_1;
	}
}
// System.Object System.Xml.XPath.XPathFunctionSum::Evaluate(System.Xml.XPath.BaseIterator)
extern "C"  RuntimeObject * XPathFunctionSum_Evaluate_m458405960 (XPathFunctionSum_t2438242634 * __this, BaseIterator_t2454437973 * ___iter0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionSum_Evaluate_m458405960_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	XPathNodeIterator_t3192332357 * V_0 = NULL;
	double V_1 = 0.0;
	{
		Expression_t1283317256 * L_0 = __this->get_arg0_0();
		BaseIterator_t2454437973 * L_1 = ___iter0;
		NullCheck(L_0);
		BaseIterator_t2454437973 * L_2 = VirtFuncInvoker1< BaseIterator_t2454437973 *, BaseIterator_t2454437973 * >::Invoke(14 /* System.Xml.XPath.BaseIterator System.Xml.XPath.Expression::EvaluateNodeSet(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		V_0 = L_2;
		V_1 = (0.0);
		goto IL_002f;
	}

IL_001c:
	{
		double L_3 = V_1;
		XPathNodeIterator_t3192332357 * L_4 = V_0;
		NullCheck(L_4);
		XPathNavigator_t3981235968 * L_5 = VirtFuncInvoker0< XPathNavigator_t3981235968 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_4);
		NullCheck(L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.Xml.XPath.XPathItem::get_Value() */, L_5);
		double L_7 = XPathFunctions_ToNumber_m2754819889(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		V_1 = ((double)((double)L_3+(double)L_7));
	}

IL_002f:
	{
		XPathNodeIterator_t3192332357 * L_8 = V_0;
		NullCheck(L_8);
		bool L_9 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.XPathNodeIterator::MoveNext() */, L_8);
		if (L_9)
		{
			goto IL_001c;
		}
	}
	{
		double L_10 = V_1;
		double L_11 = L_10;
		RuntimeObject * L_12 = Box(Double_t4078015681_il2cpp_TypeInfo_var, &L_11);
		return L_12;
	}
}
// System.String System.Xml.XPath.XPathFunctionSum::ToString()
extern "C"  String_t* XPathFunctionSum_ToString_m108877281 (XPathFunctionSum_t2438242634 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionSum_ToString_m108877281_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		StringU5BU5D_t1642385972* L_0 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral1219380691);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral1219380691);
		StringU5BU5D_t1642385972* L_1 = L_0;
		Expression_t1283317256 * L_2 = __this->get_arg0_0();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_3);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_3);
		StringU5BU5D_t1642385972* L_4 = L_1;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, _stringLiteral372029317);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m626692867(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void System.Xml.XPath.XPathFunctionTranslate::.ctor(System.Xml.XPath.FunctionArguments)
extern "C"  void XPathFunctionTranslate__ctor_m1312115182 (XPathFunctionTranslate_t2720664953 * __this, FunctionArguments_t2900945452 * ___args0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionTranslate__ctor_m1312115182_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		FunctionArguments_t2900945452 * L_0 = ___args0;
		XPathFunction__ctor_m327599860(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t2900945452 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_003d;
		}
	}
	{
		FunctionArguments_t2900945452 * L_2 = ___args0;
		NullCheck(L_2);
		FunctionArguments_t2900945452 * L_3 = FunctionArguments_get_Tail_m3947258239(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_003d;
		}
	}
	{
		FunctionArguments_t2900945452 * L_4 = ___args0;
		NullCheck(L_4);
		FunctionArguments_t2900945452 * L_5 = FunctionArguments_get_Tail_m3947258239(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		FunctionArguments_t2900945452 * L_6 = FunctionArguments_get_Tail_m3947258239(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_003d;
		}
	}
	{
		FunctionArguments_t2900945452 * L_7 = ___args0;
		NullCheck(L_7);
		FunctionArguments_t2900945452 * L_8 = FunctionArguments_get_Tail_m3947258239(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		FunctionArguments_t2900945452 * L_9 = FunctionArguments_get_Tail_m3947258239(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		FunctionArguments_t2900945452 * L_10 = FunctionArguments_get_Tail_m3947258239(L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0048;
		}
	}

IL_003d:
	{
		XPathException_t1503722168 * L_11 = (XPathException_t1503722168 *)il2cpp_codegen_object_new(XPathException_t1503722168_il2cpp_TypeInfo_var);
		XPathException__ctor_m3605407358(L_11, _stringLiteral2932583186, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
	}

IL_0048:
	{
		FunctionArguments_t2900945452 * L_12 = ___args0;
		NullCheck(L_12);
		Expression_t1283317256 * L_13 = FunctionArguments_get_Arg_m1790516491(L_12, /*hidden argument*/NULL);
		__this->set_arg0_0(L_13);
		FunctionArguments_t2900945452 * L_14 = ___args0;
		NullCheck(L_14);
		FunctionArguments_t2900945452 * L_15 = FunctionArguments_get_Tail_m3947258239(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		Expression_t1283317256 * L_16 = FunctionArguments_get_Arg_m1790516491(L_15, /*hidden argument*/NULL);
		__this->set_arg1_1(L_16);
		FunctionArguments_t2900945452 * L_17 = ___args0;
		NullCheck(L_17);
		FunctionArguments_t2900945452 * L_18 = FunctionArguments_get_Tail_m3947258239(L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		FunctionArguments_t2900945452 * L_19 = FunctionArguments_get_Tail_m3947258239(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		Expression_t1283317256 * L_20 = FunctionArguments_get_Arg_m1790516491(L_19, /*hidden argument*/NULL);
		__this->set_arg2_2(L_20);
		return;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.XPathFunctionTranslate::get_ReturnType()
extern "C"  int32_t XPathFunctionTranslate_get_ReturnType_m933147468 (XPathFunctionTranslate_t2720664953 * __this, const RuntimeMethod* method)
{
	{
		return (int32_t)(1);
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionTranslate::get_Peer()
extern "C"  bool XPathFunctionTranslate_get_Peer_m3798702666 (XPathFunctionTranslate_t2720664953 * __this, const RuntimeMethod* method)
{
	int32_t G_B4_0 = 0;
	{
		Expression_t1283317256 * L_0 = __this->get_arg0_0();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_0);
		if (!L_1)
		{
			goto IL_002d;
		}
	}
	{
		Expression_t1283317256 * L_2 = __this->get_arg1_1();
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_2);
		if (!L_3)
		{
			goto IL_002d;
		}
	}
	{
		Expression_t1283317256 * L_4 = __this->get_arg2_2();
		NullCheck(L_4);
		bool L_5 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Xml.XPath.Expression::get_Peer() */, L_4);
		G_B4_0 = ((int32_t)(L_5));
		goto IL_002e;
	}

IL_002d:
	{
		G_B4_0 = 0;
	}

IL_002e:
	{
		return (bool)G_B4_0;
	}
}
// System.Object System.Xml.XPath.XPathFunctionTranslate::Evaluate(System.Xml.XPath.BaseIterator)
extern "C"  RuntimeObject * XPathFunctionTranslate_Evaluate_m944986293 (XPathFunctionTranslate_t2720664953 * __this, BaseIterator_t2454437973 * ___iter0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionTranslate_Evaluate_m944986293_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	String_t* V_2 = NULL;
	StringBuilder_t1221177846 * V_3 = NULL;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	{
		Expression_t1283317256 * L_0 = __this->get_arg0_0();
		BaseIterator_t2454437973 * L_1 = ___iter0;
		NullCheck(L_0);
		String_t* L_2 = VirtFuncInvoker1< String_t*, BaseIterator_t2454437973 * >::Invoke(17 /* System.String System.Xml.XPath.Expression::EvaluateString(System.Xml.XPath.BaseIterator) */, L_0, L_1);
		V_0 = L_2;
		Expression_t1283317256 * L_3 = __this->get_arg1_1();
		BaseIterator_t2454437973 * L_4 = ___iter0;
		NullCheck(L_3);
		String_t* L_5 = VirtFuncInvoker1< String_t*, BaseIterator_t2454437973 * >::Invoke(17 /* System.String System.Xml.XPath.Expression::EvaluateString(System.Xml.XPath.BaseIterator) */, L_3, L_4);
		V_1 = L_5;
		Expression_t1283317256 * L_6 = __this->get_arg2_2();
		BaseIterator_t2454437973 * L_7 = ___iter0;
		NullCheck(L_6);
		String_t* L_8 = VirtFuncInvoker1< String_t*, BaseIterator_t2454437973 * >::Invoke(17 /* System.String System.Xml.XPath.Expression::EvaluateString(System.Xml.XPath.BaseIterator) */, L_6, L_7);
		V_2 = L_8;
		String_t* L_9 = V_0;
		NullCheck(L_9);
		int32_t L_10 = String_get_Length_m1606060069(L_9, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_11 = (StringBuilder_t1221177846 *)il2cpp_codegen_object_new(StringBuilder_t1221177846_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m536337337(L_11, L_10, /*hidden argument*/NULL);
		V_3 = L_11;
		V_4 = 0;
		String_t* L_12 = V_0;
		NullCheck(L_12);
		int32_t L_13 = String_get_Length_m1606060069(L_12, /*hidden argument*/NULL);
		V_5 = L_13;
		String_t* L_14 = V_2;
		NullCheck(L_14);
		int32_t L_15 = String_get_Length_m1606060069(L_14, /*hidden argument*/NULL);
		V_6 = L_15;
		goto IL_0095;
	}

IL_004b:
	{
		String_t* L_16 = V_1;
		String_t* L_17 = V_0;
		int32_t L_18 = V_4;
		NullCheck(L_17);
		Il2CppChar L_19 = String_get_Chars_m4230566705(L_17, L_18, /*hidden argument*/NULL);
		NullCheck(L_16);
		int32_t L_20 = String_IndexOf_m2358239236(L_16, L_19, /*hidden argument*/NULL);
		V_7 = L_20;
		int32_t L_21 = V_7;
		if ((((int32_t)L_21) == ((int32_t)(-1))))
		{
			goto IL_0080;
		}
	}
	{
		int32_t L_22 = V_7;
		int32_t L_23 = V_6;
		if ((((int32_t)L_22) >= ((int32_t)L_23)))
		{
			goto IL_007b;
		}
	}
	{
		StringBuilder_t1221177846 * L_24 = V_3;
		String_t* L_25 = V_2;
		int32_t L_26 = V_7;
		NullCheck(L_25);
		Il2CppChar L_27 = String_get_Chars_m4230566705(L_25, L_26, /*hidden argument*/NULL);
		NullCheck(L_24);
		StringBuilder_Append_m3618697540(L_24, L_27, /*hidden argument*/NULL);
	}

IL_007b:
	{
		goto IL_008f;
	}

IL_0080:
	{
		StringBuilder_t1221177846 * L_28 = V_3;
		String_t* L_29 = V_0;
		int32_t L_30 = V_4;
		NullCheck(L_29);
		Il2CppChar L_31 = String_get_Chars_m4230566705(L_29, L_30, /*hidden argument*/NULL);
		NullCheck(L_28);
		StringBuilder_Append_m3618697540(L_28, L_31, /*hidden argument*/NULL);
	}

IL_008f:
	{
		int32_t L_32 = V_4;
		V_4 = ((int32_t)((int32_t)L_32+(int32_t)1));
	}

IL_0095:
	{
		int32_t L_33 = V_4;
		int32_t L_34 = V_5;
		if ((((int32_t)L_33) < ((int32_t)L_34)))
		{
			goto IL_004b;
		}
	}
	{
		StringBuilder_t1221177846 * L_35 = V_3;
		NullCheck(L_35);
		String_t* L_36 = StringBuilder_ToString_m1507807375(L_35, /*hidden argument*/NULL);
		return L_36;
	}
}
// System.String System.Xml.XPath.XPathFunctionTranslate::ToString()
extern "C"  String_t* XPathFunctionTranslate_ToString_m1307664662 (XPathFunctionTranslate_t2720664953 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionTranslate_ToString_m1307664662_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		StringU5BU5D_t1642385972* L_0 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)7));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral1827829842);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral1827829842);
		StringU5BU5D_t1642385972* L_1 = L_0;
		Expression_t1283317256 * L_2 = __this->get_arg0_0();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_3);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_3);
		StringU5BU5D_t1642385972* L_4 = L_1;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, _stringLiteral372029314);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral372029314);
		StringU5BU5D_t1642385972* L_5 = L_4;
		Expression_t1283317256 * L_6 = __this->get_arg1_1();
		NullCheck(L_6);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_6);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_7);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_7);
		StringU5BU5D_t1642385972* L_8 = L_5;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, _stringLiteral372029314);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029314);
		StringU5BU5D_t1642385972* L_9 = L_8;
		Expression_t1283317256 * L_10 = __this->get_arg2_2();
		NullCheck(L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_10);
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, L_11);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)L_11);
		StringU5BU5D_t1642385972* L_12 = L_9;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral372029317);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(6), (String_t*)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m626692867(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
// System.Void System.Xml.XPath.XPathFunctionTrue::.ctor(System.Xml.XPath.FunctionArguments)
extern "C"  void XPathFunctionTrue__ctor_m3471779148 (XPathFunctionTrue_t1672815575 * __this, FunctionArguments_t2900945452 * ___args0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionTrue__ctor_m3471779148_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		FunctionArguments_t2900945452 * L_0 = ___args0;
		XPathBooleanFunction__ctor_m620012104(__this, L_0, /*hidden argument*/NULL);
		FunctionArguments_t2900945452 * L_1 = ___args0;
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		XPathException_t1503722168 * L_2 = (XPathException_t1503722168 *)il2cpp_codegen_object_new(XPathException_t1503722168_il2cpp_TypeInfo_var);
		XPathException__ctor_m3605407358(L_2, _stringLiteral2583897385, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0018:
	{
		return;
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionTrue::get_HasStaticValue()
extern "C"  bool XPathFunctionTrue_get_HasStaticValue_m3813180071 (XPathFunctionTrue_t1672815575 * __this, const RuntimeMethod* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionTrue::get_StaticValueAsBoolean()
extern "C"  bool XPathFunctionTrue_get_StaticValueAsBoolean_m1308627295 (XPathFunctionTrue_t1672815575 * __this, const RuntimeMethod* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Xml.XPath.XPathFunctionTrue::get_Peer()
extern "C"  bool XPathFunctionTrue_get_Peer_m2350064100 (XPathFunctionTrue_t1672815575 * __this, const RuntimeMethod* method)
{
	{
		return (bool)1;
	}
}
// System.Object System.Xml.XPath.XPathFunctionTrue::Evaluate(System.Xml.XPath.BaseIterator)
extern "C"  RuntimeObject * XPathFunctionTrue_Evaluate_m164285465 (XPathFunctionTrue_t1672815575 * __this, BaseIterator_t2454437973 * ___iter0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionTrue_Evaluate_m164285465_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ((bool)1);
		RuntimeObject * L_1 = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &L_0);
		return L_1;
	}
}
// System.String System.Xml.XPath.XPathFunctionTrue::ToString()
extern "C"  String_t* XPathFunctionTrue_ToString_m1131923630 (XPathFunctionTrue_t1672815575 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathFunctionTrue_ToString_m1131923630_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		return _stringLiteral2078431091;
	}
}
// System.Void System.Xml.XPath.XPathItem::.ctor()
extern "C"  void XPathItem__ctor_m341749466 (XPathItem_t3130801258 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.XPath.XPathIteratorComparer::.ctor()
extern "C"  void XPathIteratorComparer__ctor_m2708098576 (XPathIteratorComparer_t2522471076 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.XPath.XPathIteratorComparer::.cctor()
extern "C"  void XPathIteratorComparer__cctor_m1438945887 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathIteratorComparer__cctor_m1438945887_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		XPathIteratorComparer_t2522471076 * L_0 = (XPathIteratorComparer_t2522471076 *)il2cpp_codegen_object_new(XPathIteratorComparer_t2522471076_il2cpp_TypeInfo_var);
		XPathIteratorComparer__ctor_m2708098576(L_0, /*hidden argument*/NULL);
		((XPathIteratorComparer_t2522471076_StaticFields*)il2cpp_codegen_static_fields_for(XPathIteratorComparer_t2522471076_il2cpp_TypeInfo_var))->set_Instance_0(L_0);
		return;
	}
}
// System.Int32 System.Xml.XPath.XPathIteratorComparer::Compare(System.Object,System.Object)
extern "C"  int32_t XPathIteratorComparer_Compare_m2647634021 (XPathIteratorComparer_t2522471076 * __this, RuntimeObject * ___o10, RuntimeObject * ___o21, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathIteratorComparer_Compare_m2647634021_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	XPathNodeIterator_t3192332357 * V_0 = NULL;
	XPathNodeIterator_t3192332357 * V_1 = NULL;
	int32_t V_2 = 0;
	{
		RuntimeObject * L_0 = ___o10;
		V_0 = ((XPathNodeIterator_t3192332357 *)IsInstClass((RuntimeObject*)L_0, XPathNodeIterator_t3192332357_il2cpp_TypeInfo_var));
		RuntimeObject * L_1 = ___o21;
		V_1 = ((XPathNodeIterator_t3192332357 *)IsInstClass((RuntimeObject*)L_1, XPathNodeIterator_t3192332357_il2cpp_TypeInfo_var));
		XPathNodeIterator_t3192332357 * L_2 = V_0;
		if (L_2)
		{
			goto IL_0016;
		}
	}
	{
		return (-1);
	}

IL_0016:
	{
		XPathNodeIterator_t3192332357 * L_3 = V_1;
		if (L_3)
		{
			goto IL_001e;
		}
	}
	{
		return 1;
	}

IL_001e:
	{
		XPathNodeIterator_t3192332357 * L_4 = V_0;
		NullCheck(L_4);
		XPathNavigator_t3981235968 * L_5 = VirtFuncInvoker0< XPathNavigator_t3981235968 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_4);
		XPathNodeIterator_t3192332357 * L_6 = V_1;
		NullCheck(L_6);
		XPathNavigator_t3981235968 * L_7 = VirtFuncInvoker0< XPathNavigator_t3981235968 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_6);
		NullCheck(L_5);
		int32_t L_8 = VirtFuncInvoker1< int32_t, XPathNavigator_t3981235968 * >::Invoke(15 /* System.Xml.XmlNodeOrder System.Xml.XPath.XPathNavigator::ComparePosition(System.Xml.XPath.XPathNavigator) */, L_5, L_7);
		V_2 = L_8;
		int32_t L_9 = V_2;
		if ((((int32_t)L_9) == ((int32_t)1)))
		{
			goto IL_0045;
		}
	}
	{
		int32_t L_10 = V_2;
		if ((((int32_t)L_10) == ((int32_t)2)))
		{
			goto IL_0043;
		}
	}
	{
		goto IL_0047;
	}

IL_0043:
	{
		return 0;
	}

IL_0045:
	{
		return (-1);
	}

IL_0047:
	{
		return 1;
	}
}
// System.Void System.Xml.XPath.XPathNavigator::.ctor()
extern "C"  void XPathNavigator__ctor_m3760155520 (XPathNavigator_t3981235968 * __this, const RuntimeMethod* method)
{
	{
		XPathItem__ctor_m341749466(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.XPath.XPathNavigator::.cctor()
extern "C"  void XPathNavigator__cctor_m1668901011 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathNavigator__cctor_m1668901011_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		CharU5BU5D_t1328083999* L_0 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_0);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)38));
		CharU5BU5D_t1328083999* L_1 = L_0;
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppChar)((int32_t)60));
		CharU5BU5D_t1328083999* L_2 = L_1;
		NullCheck(L_2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppChar)((int32_t)62));
		((XPathNavigator_t3981235968_StaticFields*)il2cpp_codegen_static_fields_for(XPathNavigator_t3981235968_il2cpp_TypeInfo_var))->set_escape_text_chars_0(L_2);
		CharU5BU5D_t1328083999* L_3 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)6));
		RuntimeHelpers_InitializeArray_m3920580167(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_3, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t1486305141____U24U24fieldU2D18_9_FieldInfo_var), /*hidden argument*/NULL);
		((XPathNavigator_t3981235968_StaticFields*)il2cpp_codegen_static_fields_for(XPathNavigator_t3981235968_il2cpp_TypeInfo_var))->set_escape_attr_chars_1(L_3);
		return;
	}
}
// System.Object System.Xml.XPath.XPathNavigator::System.ICloneable.Clone()
extern "C"  RuntimeObject * XPathNavigator_System_ICloneable_Clone_m3220185747 (XPathNavigator_t3981235968 * __this, const RuntimeMethod* method)
{
	{
		XPathNavigator_t3981235968 * L_0 = VirtFuncInvoker0< XPathNavigator_t3981235968 * >::Invoke(14 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, __this);
		return L_0;
	}
}
// System.Boolean System.Xml.XPath.XPathNavigator::get_HasAttributes()
extern "C"  bool XPathNavigator_get_HasAttributes_m3776636292 (XPathNavigator_t3981235968 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(24 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToFirstAttribute() */, __this);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, __this);
		return (bool)1;
	}
}
// System.Boolean System.Xml.XPath.XPathNavigator::get_HasChildren()
extern "C"  bool XPathNavigator_get_HasChildren_m3105216106 (XPathNavigator_t3981235968 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(25 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToFirstChild() */, __this);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, __this);
		return (bool)1;
	}
}
// System.String System.Xml.XPath.XPathNavigator::get_XmlLang()
extern "C"  String_t* XPathNavigator_get_XmlLang_m1158727745 (XPathNavigator_t3981235968 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathNavigator_get_XmlLang_m1158727745_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	XPathNavigator_t3981235968 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		XPathNavigator_t3981235968 * L_0 = VirtFuncInvoker0< XPathNavigator_t3981235968 * >::Invoke(14 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, __this);
		V_0 = L_0;
		XPathNavigator_t3981235968 * L_1 = V_0;
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(12 /* System.Xml.XPath.XPathNodeType System.Xml.XPath.XPathNavigator::get_NodeType() */, L_1);
		V_1 = L_2;
		int32_t L_3 = V_1;
		if ((((int32_t)L_3) == ((int32_t)2)))
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_4 = V_1;
		if ((((int32_t)L_4) == ((int32_t)3)))
		{
			goto IL_0021;
		}
	}
	{
		goto IL_002d;
	}

IL_0021:
	{
		XPathNavigator_t3981235968 * L_5 = V_0;
		NullCheck(L_5);
		VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, L_5);
		goto IL_002d;
	}

IL_002d:
	{
		XPathNavigator_t3981235968 * L_6 = V_0;
		NullCheck(L_6);
		bool L_7 = VirtFuncInvoker2< bool, String_t*, String_t* >::Invoke(20 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToAttribute(System.String,System.String) */, L_6, _stringLiteral1165419350, _stringLiteral3939301371);
		if (!L_7)
		{
			goto IL_0049;
		}
	}
	{
		XPathNavigator_t3981235968 * L_8 = V_0;
		NullCheck(L_8);
		String_t* L_9 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.Xml.XPath.XPathItem::get_Value() */, L_8);
		return L_9;
	}

IL_0049:
	{
		XPathNavigator_t3981235968 * L_10 = V_0;
		NullCheck(L_10);
		bool L_11 = VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, L_10);
		if (L_11)
		{
			goto IL_002d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		return L_12;
	}
}
// System.Xml.XmlNodeOrder System.Xml.XPath.XPathNavigator::ComparePosition(System.Xml.XPath.XPathNavigator)
extern "C"  int32_t XPathNavigator_ComparePosition_m2001546875 (XPathNavigator_t3981235968 * __this, XPathNavigator_t3981235968 * ___nav0, const RuntimeMethod* method)
{
	XPathNavigator_t3981235968 * V_0 = NULL;
	XPathNavigator_t3981235968 * V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	{
		XPathNavigator_t3981235968 * L_0 = ___nav0;
		bool L_1 = VirtFuncInvoker1< bool, XPathNavigator_t3981235968 * >::Invoke(18 /* System.Boolean System.Xml.XPath.XPathNavigator::IsSamePosition(System.Xml.XPath.XPathNavigator) */, __this, L_0);
		if (!L_1)
		{
			goto IL_000e;
		}
	}
	{
		return (int32_t)(2);
	}

IL_000e:
	{
		XPathNavigator_t3981235968 * L_2 = ___nav0;
		bool L_3 = VirtFuncInvoker1< bool, XPathNavigator_t3981235968 * >::Invoke(17 /* System.Boolean System.Xml.XPath.XPathNavigator::IsDescendant(System.Xml.XPath.XPathNavigator) */, __this, L_2);
		if (!L_3)
		{
			goto IL_001c;
		}
	}
	{
		return (int32_t)(0);
	}

IL_001c:
	{
		XPathNavigator_t3981235968 * L_4 = ___nav0;
		NullCheck(L_4);
		bool L_5 = VirtFuncInvoker1< bool, XPathNavigator_t3981235968 * >::Invoke(17 /* System.Boolean System.Xml.XPath.XPathNavigator::IsDescendant(System.Xml.XPath.XPathNavigator) */, L_4, __this);
		if (!L_5)
		{
			goto IL_002a;
		}
	}
	{
		return (int32_t)(1);
	}

IL_002a:
	{
		XPathNavigator_t3981235968 * L_6 = VirtFuncInvoker0< XPathNavigator_t3981235968 * >::Invoke(14 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, __this);
		V_0 = L_6;
		XPathNavigator_t3981235968 * L_7 = ___nav0;
		NullCheck(L_7);
		XPathNavigator_t3981235968 * L_8 = VirtFuncInvoker0< XPathNavigator_t3981235968 * >::Invoke(14 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, L_7);
		V_1 = L_8;
		XPathNavigator_t3981235968 * L_9 = V_0;
		NullCheck(L_9);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Xml.XPath.XPathNavigator::MoveToRoot() */, L_9);
		XPathNavigator_t3981235968 * L_10 = V_1;
		NullCheck(L_10);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Xml.XPath.XPathNavigator::MoveToRoot() */, L_10);
		XPathNavigator_t3981235968 * L_11 = V_0;
		XPathNavigator_t3981235968 * L_12 = V_1;
		NullCheck(L_11);
		bool L_13 = VirtFuncInvoker1< bool, XPathNavigator_t3981235968 * >::Invoke(18 /* System.Boolean System.Xml.XPath.XPathNavigator::IsSamePosition(System.Xml.XPath.XPathNavigator) */, L_11, L_12);
		if (L_13)
		{
			goto IL_0052;
		}
	}
	{
		return (int32_t)(3);
	}

IL_0052:
	{
		XPathNavigator_t3981235968 * L_14 = V_0;
		NullCheck(L_14);
		VirtFuncInvoker1< bool, XPathNavigator_t3981235968 * >::Invoke(19 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveTo(System.Xml.XPath.XPathNavigator) */, L_14, __this);
		XPathNavigator_t3981235968 * L_15 = V_1;
		XPathNavigator_t3981235968 * L_16 = ___nav0;
		NullCheck(L_15);
		VirtFuncInvoker1< bool, XPathNavigator_t3981235968 * >::Invoke(19 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveTo(System.Xml.XPath.XPathNavigator) */, L_15, L_16);
		V_2 = 0;
		goto IL_006d;
	}

IL_0069:
	{
		int32_t L_17 = V_2;
		V_2 = ((int32_t)((int32_t)L_17+(int32_t)1));
	}

IL_006d:
	{
		XPathNavigator_t3981235968 * L_18 = V_0;
		NullCheck(L_18);
		bool L_19 = VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, L_18);
		if (L_19)
		{
			goto IL_0069;
		}
	}
	{
		XPathNavigator_t3981235968 * L_20 = V_0;
		NullCheck(L_20);
		VirtFuncInvoker1< bool, XPathNavigator_t3981235968 * >::Invoke(19 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveTo(System.Xml.XPath.XPathNavigator) */, L_20, __this);
		V_3 = 0;
		goto IL_008b;
	}

IL_0087:
	{
		int32_t L_21 = V_3;
		V_3 = ((int32_t)((int32_t)L_21+(int32_t)1));
	}

IL_008b:
	{
		XPathNavigator_t3981235968 * L_22 = V_1;
		NullCheck(L_22);
		bool L_23 = VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, L_22);
		if (L_23)
		{
			goto IL_0087;
		}
	}
	{
		XPathNavigator_t3981235968 * L_24 = V_1;
		XPathNavigator_t3981235968 * L_25 = ___nav0;
		NullCheck(L_24);
		VirtFuncInvoker1< bool, XPathNavigator_t3981235968 * >::Invoke(19 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveTo(System.Xml.XPath.XPathNavigator) */, L_24, L_25);
		int32_t L_26 = V_2;
		V_4 = L_26;
		goto IL_00b3;
	}

IL_00a6:
	{
		XPathNavigator_t3981235968 * L_27 = V_0;
		NullCheck(L_27);
		VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, L_27);
		int32_t L_28 = V_4;
		V_4 = ((int32_t)((int32_t)L_28-(int32_t)1));
	}

IL_00b3:
	{
		int32_t L_29 = V_4;
		int32_t L_30 = V_3;
		if ((((int32_t)L_29) > ((int32_t)L_30)))
		{
			goto IL_00a6;
		}
	}
	{
		int32_t L_31 = V_3;
		V_5 = L_31;
		goto IL_00d0;
	}

IL_00c3:
	{
		XPathNavigator_t3981235968 * L_32 = V_1;
		NullCheck(L_32);
		VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, L_32);
		int32_t L_33 = V_5;
		V_5 = ((int32_t)((int32_t)L_33-(int32_t)1));
	}

IL_00d0:
	{
		int32_t L_34 = V_5;
		int32_t L_35 = V_4;
		if ((((int32_t)L_34) > ((int32_t)L_35)))
		{
			goto IL_00c3;
		}
	}
	{
		goto IL_00f2;
	}

IL_00de:
	{
		XPathNavigator_t3981235968 * L_36 = V_0;
		NullCheck(L_36);
		VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, L_36);
		XPathNavigator_t3981235968 * L_37 = V_1;
		NullCheck(L_37);
		VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, L_37);
		int32_t L_38 = V_4;
		V_4 = ((int32_t)((int32_t)L_38-(int32_t)1));
	}

IL_00f2:
	{
		XPathNavigator_t3981235968 * L_39 = V_0;
		XPathNavigator_t3981235968 * L_40 = V_1;
		NullCheck(L_39);
		bool L_41 = VirtFuncInvoker1< bool, XPathNavigator_t3981235968 * >::Invoke(18 /* System.Boolean System.Xml.XPath.XPathNavigator::IsSamePosition(System.Xml.XPath.XPathNavigator) */, L_39, L_40);
		if (!L_41)
		{
			goto IL_00de;
		}
	}
	{
		XPathNavigator_t3981235968 * L_42 = V_0;
		NullCheck(L_42);
		VirtFuncInvoker1< bool, XPathNavigator_t3981235968 * >::Invoke(19 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveTo(System.Xml.XPath.XPathNavigator) */, L_42, __this);
		int32_t L_43 = V_2;
		V_6 = L_43;
		goto IL_011b;
	}

IL_010e:
	{
		XPathNavigator_t3981235968 * L_44 = V_0;
		NullCheck(L_44);
		VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, L_44);
		int32_t L_45 = V_6;
		V_6 = ((int32_t)((int32_t)L_45-(int32_t)1));
	}

IL_011b:
	{
		int32_t L_46 = V_6;
		int32_t L_47 = V_4;
		if ((((int32_t)L_46) > ((int32_t)((int32_t)((int32_t)L_47+(int32_t)1)))))
		{
			goto IL_010e;
		}
	}
	{
		XPathNavigator_t3981235968 * L_48 = V_1;
		XPathNavigator_t3981235968 * L_49 = ___nav0;
		NullCheck(L_48);
		VirtFuncInvoker1< bool, XPathNavigator_t3981235968 * >::Invoke(19 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveTo(System.Xml.XPath.XPathNavigator) */, L_48, L_49);
		int32_t L_50 = V_3;
		V_7 = L_50;
		goto IL_0143;
	}

IL_0136:
	{
		XPathNavigator_t3981235968 * L_51 = V_1;
		NullCheck(L_51);
		VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, L_51);
		int32_t L_52 = V_7;
		V_7 = ((int32_t)((int32_t)L_52-(int32_t)1));
	}

IL_0143:
	{
		int32_t L_53 = V_7;
		int32_t L_54 = V_4;
		if ((((int32_t)L_53) > ((int32_t)((int32_t)((int32_t)L_54+(int32_t)1)))))
		{
			goto IL_0136;
		}
	}
	{
		XPathNavigator_t3981235968 * L_55 = V_0;
		NullCheck(L_55);
		int32_t L_56 = VirtFuncInvoker0< int32_t >::Invoke(12 /* System.Xml.XPath.XPathNodeType System.Xml.XPath.XPathNavigator::get_NodeType() */, L_55);
		if ((!(((uint32_t)L_56) == ((uint32_t)3))))
		{
			goto IL_0188;
		}
	}
	{
		XPathNavigator_t3981235968 * L_57 = V_1;
		NullCheck(L_57);
		int32_t L_58 = VirtFuncInvoker0< int32_t >::Invoke(12 /* System.Xml.XPath.XPathNodeType System.Xml.XPath.XPathNavigator::get_NodeType() */, L_57);
		if ((((int32_t)L_58) == ((int32_t)3)))
		{
			goto IL_0168;
		}
	}
	{
		return (int32_t)(0);
	}

IL_0168:
	{
		goto IL_017b;
	}

IL_016d:
	{
		XPathNavigator_t3981235968 * L_59 = V_0;
		XPathNavigator_t3981235968 * L_60 = V_1;
		NullCheck(L_59);
		bool L_61 = VirtFuncInvoker1< bool, XPathNavigator_t3981235968 * >::Invoke(18 /* System.Boolean System.Xml.XPath.XPathNavigator::IsSamePosition(System.Xml.XPath.XPathNavigator) */, L_59, L_60);
		if (!L_61)
		{
			goto IL_017b;
		}
	}
	{
		return (int32_t)(0);
	}

IL_017b:
	{
		XPathNavigator_t3981235968 * L_62 = V_0;
		NullCheck(L_62);
		bool L_63 = XPathNavigator_MoveToNextNamespace_m3661588194(L_62, /*hidden argument*/NULL);
		if (L_63)
		{
			goto IL_016d;
		}
	}
	{
		return (int32_t)(1);
	}

IL_0188:
	{
		XPathNavigator_t3981235968 * L_64 = V_1;
		NullCheck(L_64);
		int32_t L_65 = VirtFuncInvoker0< int32_t >::Invoke(12 /* System.Xml.XPath.XPathNodeType System.Xml.XPath.XPathNavigator::get_NodeType() */, L_64);
		if ((!(((uint32_t)L_65) == ((uint32_t)3))))
		{
			goto IL_0196;
		}
	}
	{
		return (int32_t)(1);
	}

IL_0196:
	{
		XPathNavigator_t3981235968 * L_66 = V_0;
		NullCheck(L_66);
		int32_t L_67 = VirtFuncInvoker0< int32_t >::Invoke(12 /* System.Xml.XPath.XPathNodeType System.Xml.XPath.XPathNavigator::get_NodeType() */, L_66);
		if ((!(((uint32_t)L_67) == ((uint32_t)2))))
		{
			goto IL_01d0;
		}
	}
	{
		XPathNavigator_t3981235968 * L_68 = V_1;
		NullCheck(L_68);
		int32_t L_69 = VirtFuncInvoker0< int32_t >::Invoke(12 /* System.Xml.XPath.XPathNodeType System.Xml.XPath.XPathNavigator::get_NodeType() */, L_68);
		if ((((int32_t)L_69) == ((int32_t)2)))
		{
			goto IL_01b0;
		}
	}
	{
		return (int32_t)(0);
	}

IL_01b0:
	{
		goto IL_01c3;
	}

IL_01b5:
	{
		XPathNavigator_t3981235968 * L_70 = V_0;
		XPathNavigator_t3981235968 * L_71 = V_1;
		NullCheck(L_70);
		bool L_72 = VirtFuncInvoker1< bool, XPathNavigator_t3981235968 * >::Invoke(18 /* System.Boolean System.Xml.XPath.XPathNavigator::IsSamePosition(System.Xml.XPath.XPathNavigator) */, L_70, L_71);
		if (!L_72)
		{
			goto IL_01c3;
		}
	}
	{
		return (int32_t)(0);
	}

IL_01c3:
	{
		XPathNavigator_t3981235968 * L_73 = V_0;
		NullCheck(L_73);
		bool L_74 = VirtFuncInvoker0< bool >::Invoke(29 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToNextAttribute() */, L_73);
		if (L_74)
		{
			goto IL_01b5;
		}
	}
	{
		return (int32_t)(1);
	}

IL_01d0:
	{
		goto IL_01e3;
	}

IL_01d5:
	{
		XPathNavigator_t3981235968 * L_75 = V_0;
		XPathNavigator_t3981235968 * L_76 = V_1;
		NullCheck(L_75);
		bool L_77 = VirtFuncInvoker1< bool, XPathNavigator_t3981235968 * >::Invoke(18 /* System.Boolean System.Xml.XPath.XPathNavigator::IsSamePosition(System.Xml.XPath.XPathNavigator) */, L_75, L_76);
		if (!L_77)
		{
			goto IL_01e3;
		}
	}
	{
		return (int32_t)(0);
	}

IL_01e3:
	{
		XPathNavigator_t3981235968 * L_78 = V_0;
		NullCheck(L_78);
		bool L_79 = VirtFuncInvoker0< bool >::Invoke(28 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToNext() */, L_78);
		if (L_79)
		{
			goto IL_01d5;
		}
	}
	{
		return (int32_t)(1);
	}
}
// System.Xml.XPath.XPathExpression System.Xml.XPath.XPathNavigator::Compile(System.String)
extern "C"  XPathExpression_t452251917 * XPathNavigator_Compile_m2646144820 (XPathNavigator_t3981235968 * __this, String_t* ___xpath0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___xpath0;
		XPathExpression_t452251917 * L_1 = XPathExpression_Compile_m2615308077(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Xml.XPath.XPathNavigator::IsDescendant(System.Xml.XPath.XPathNavigator)
extern "C"  bool XPathNavigator_IsDescendant_m2690514350 (XPathNavigator_t3981235968 * __this, XPathNavigator_t3981235968 * ___nav0, const RuntimeMethod* method)
{
	{
		XPathNavigator_t3981235968 * L_0 = ___nav0;
		if (!L_0)
		{
			goto IL_002c;
		}
	}
	{
		XPathNavigator_t3981235968 * L_1 = ___nav0;
		NullCheck(L_1);
		XPathNavigator_t3981235968 * L_2 = VirtFuncInvoker0< XPathNavigator_t3981235968 * >::Invoke(14 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, L_1);
		___nav0 = L_2;
		goto IL_0021;
	}

IL_0013:
	{
		XPathNavigator_t3981235968 * L_3 = ___nav0;
		bool L_4 = VirtFuncInvoker1< bool, XPathNavigator_t3981235968 * >::Invoke(18 /* System.Boolean System.Xml.XPath.XPathNavigator::IsSamePosition(System.Xml.XPath.XPathNavigator) */, __this, L_3);
		if (!L_4)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		XPathNavigator_t3981235968 * L_5 = ___nav0;
		NullCheck(L_5);
		bool L_6 = VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, L_5);
		if (L_6)
		{
			goto IL_0013;
		}
	}

IL_002c:
	{
		return (bool)0;
	}
}
// System.Boolean System.Xml.XPath.XPathNavigator::MoveToAttribute(System.String,System.String)
extern "C"  bool XPathNavigator_MoveToAttribute_m3716628652 (XPathNavigator_t3981235968 * __this, String_t* ___localName0, String_t* ___namespaceURI1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathNavigator_MoveToAttribute_m3716628652_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(24 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToFirstAttribute() */, __this);
		if (!L_0)
		{
			goto IL_0041;
		}
	}

IL_000b:
	{
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String System.Xml.XPath.XPathNavigator::get_LocalName() */, __this);
		String_t* L_2 = ___localName0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002f;
		}
	}
	{
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(11 /* System.String System.Xml.XPath.XPathNavigator::get_NamespaceURI() */, __this);
		String_t* L_5 = ___namespaceURI1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_6 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_002f;
		}
	}
	{
		return (bool)1;
	}

IL_002f:
	{
		bool L_7 = VirtFuncInvoker0< bool >::Invoke(29 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToNextAttribute() */, __this);
		if (L_7)
		{
			goto IL_000b;
		}
	}
	{
		VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, __this);
	}

IL_0041:
	{
		return (bool)0;
	}
}
// System.Boolean System.Xml.XPath.XPathNavigator::MoveToNamespace(System.String)
extern "C"  bool XPathNavigator_MoveToNamespace_m419780955 (XPathNavigator_t3981235968 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathNavigator_MoveToNamespace_m419780955_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = XPathNavigator_MoveToFirstNamespace_m3677738759(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0030;
		}
	}

IL_000b:
	{
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String System.Xml.XPath.XPathNavigator::get_LocalName() */, __this);
		String_t* L_2 = ___name0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001e;
		}
	}
	{
		return (bool)1;
	}

IL_001e:
	{
		bool L_4 = XPathNavigator_MoveToNextNamespace_m3661588194(__this, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_000b;
		}
	}
	{
		VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, __this);
	}

IL_0030:
	{
		return (bool)0;
	}
}
// System.Boolean System.Xml.XPath.XPathNavigator::MoveToFirst()
extern "C"  bool XPathNavigator_MoveToFirst_m2987745696 (XPathNavigator_t3981235968 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = XPathNavigator_MoveToFirstImpl_m1135523502(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.Xml.XPath.XPathNavigator::MoveToRoot()
extern "C"  void XPathNavigator_MoveToRoot_m3740159050 (XPathNavigator_t3981235968 * __this, const RuntimeMethod* method)
{
	{
		goto IL_0005;
	}

IL_0005:
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, __this);
		if (L_0)
		{
			goto IL_0005;
		}
	}
	{
		return;
	}
}
// System.Boolean System.Xml.XPath.XPathNavigator::MoveToFirstImpl()
extern "C"  bool XPathNavigator_MoveToFirstImpl_m1135523502 (XPathNavigator_t3981235968 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(12 /* System.Xml.XPath.XPathNodeType System.Xml.XPath.XPathNavigator::get_NodeType() */, __this);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)2)))
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)3)))
		{
			goto IL_001a;
		}
	}
	{
		goto IL_001c;
	}

IL_001a:
	{
		return (bool)0;
	}

IL_001c:
	{
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, __this);
		if (L_3)
		{
			goto IL_0029;
		}
	}
	{
		return (bool)0;
	}

IL_0029:
	{
		VirtFuncInvoker0< bool >::Invoke(25 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToFirstChild() */, __this);
		return (bool)1;
	}
}
// System.Boolean System.Xml.XPath.XPathNavigator::MoveToFirstNamespace()
extern "C"  bool XPathNavigator_MoveToFirstNamespace_m3677738759 (XPathNavigator_t3981235968 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = VirtFuncInvoker1< bool, int32_t >::Invoke(26 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToFirstNamespace(System.Xml.XPath.XPathNamespaceScope) */, __this, 0);
		return L_0;
	}
}
// System.Boolean System.Xml.XPath.XPathNavigator::MoveToNextNamespace()
extern "C"  bool XPathNavigator_MoveToNextNamespace_m3661588194 (XPathNavigator_t3981235968 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = VirtFuncInvoker1< bool, int32_t >::Invoke(30 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToNextNamespace(System.Xml.XPath.XPathNamespaceScope) */, __this, 0);
		return L_0;
	}
}
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.XPathNavigator::Select(System.String)
extern "C"  XPathNodeIterator_t3192332357 * XPathNavigator_Select_m3233457121 (XPathNavigator_t3981235968 * __this, String_t* ___xpath0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___xpath0;
		XPathExpression_t452251917 * L_1 = VirtFuncInvoker1< XPathExpression_t452251917 *, String_t* >::Invoke(16 /* System.Xml.XPath.XPathExpression System.Xml.XPath.XPathNavigator::Compile(System.String) */, __this, L_0);
		XPathNodeIterator_t3192332357 * L_2 = VirtFuncInvoker1< XPathNodeIterator_t3192332357 *, XPathExpression_t452251917 * >::Invoke(33 /* System.Xml.XPath.XPathNodeIterator System.Xml.XPath.XPathNavigator::Select(System.Xml.XPath.XPathExpression) */, __this, L_1);
		return L_2;
	}
}
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.XPathNavigator::Select(System.Xml.XPath.XPathExpression)
extern "C"  XPathNodeIterator_t3192332357 * XPathNavigator_Select_m65000375 (XPathNavigator_t3981235968 * __this, XPathExpression_t452251917 * ___expr0, const RuntimeMethod* method)
{
	{
		XPathExpression_t452251917 * L_0 = ___expr0;
		XPathNodeIterator_t3192332357 * L_1 = XPathNavigator_Select_m2045522580(__this, L_0, (RuntimeObject*)NULL, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.XPathNavigator::Select(System.Xml.XPath.XPathExpression,System.Xml.IXmlNamespaceResolver)
extern "C"  XPathNodeIterator_t3192332357 * XPathNavigator_Select_m2045522580 (XPathNavigator_t3981235968 * __this, XPathExpression_t452251917 * ___expr0, RuntimeObject* ___ctx1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathNavigator_Select_m2045522580_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CompiledExpression_t3686330919 * V_0 = NULL;
	BaseIterator_t2454437973 * V_1 = NULL;
	{
		XPathExpression_t452251917 * L_0 = ___expr0;
		V_0 = ((CompiledExpression_t3686330919 *)CastclassClass((RuntimeObject*)L_0, CompiledExpression_t3686330919_il2cpp_TypeInfo_var));
		RuntimeObject* L_1 = ___ctx1;
		if (L_1)
		{
			goto IL_0015;
		}
	}
	{
		CompiledExpression_t3686330919 * L_2 = V_0;
		NullCheck(L_2);
		RuntimeObject* L_3 = CompiledExpression_get_NamespaceManager_m4093303772(L_2, /*hidden argument*/NULL);
		___ctx1 = L_3;
	}

IL_0015:
	{
		RuntimeObject* L_4 = ___ctx1;
		NullIterator_t2539636145 * L_5 = (NullIterator_t2539636145 *)il2cpp_codegen_object_new(NullIterator_t2539636145_il2cpp_TypeInfo_var);
		NullIterator__ctor_m2816370923(L_5, __this, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		CompiledExpression_t3686330919 * L_6 = V_0;
		BaseIterator_t2454437973 * L_7 = V_1;
		NullCheck(L_6);
		XPathNodeIterator_t3192332357 * L_8 = CompiledExpression_EvaluateNodeSet_m4253810175(L_6, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Collections.IEnumerable System.Xml.XPath.XPathNavigator::EnumerateChildren(System.Xml.XPath.XPathNavigator,System.Xml.XPath.XPathNodeType)
extern "C"  RuntimeObject* XPathNavigator_EnumerateChildren_m1107095336 (RuntimeObject * __this /* static, unused */, XPathNavigator_t3981235968 * ___n0, int32_t ___type1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathNavigator_EnumerateChildren_m1107095336_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CEnumerateChildrenU3Ec__Iterator0_t1235609798 * V_0 = NULL;
	{
		U3CEnumerateChildrenU3Ec__Iterator0_t1235609798 * L_0 = (U3CEnumerateChildrenU3Ec__Iterator0_t1235609798 *)il2cpp_codegen_object_new(U3CEnumerateChildrenU3Ec__Iterator0_t1235609798_il2cpp_TypeInfo_var);
		U3CEnumerateChildrenU3Ec__Iterator0__ctor_m2964582031(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CEnumerateChildrenU3Ec__Iterator0_t1235609798 * L_1 = V_0;
		XPathNavigator_t3981235968 * L_2 = ___n0;
		NullCheck(L_1);
		L_1->set_n_0(L_2);
		U3CEnumerateChildrenU3Ec__Iterator0_t1235609798 * L_3 = V_0;
		int32_t L_4 = ___type1;
		NullCheck(L_3);
		L_3->set_type_3(L_4);
		U3CEnumerateChildrenU3Ec__Iterator0_t1235609798 * L_5 = V_0;
		XPathNavigator_t3981235968 * L_6 = ___n0;
		NullCheck(L_5);
		L_5->set_U3CU24U3En_6(L_6);
		U3CEnumerateChildrenU3Ec__Iterator0_t1235609798 * L_7 = V_0;
		int32_t L_8 = ___type1;
		NullCheck(L_7);
		L_7->set_U3CU24U3Etype_7(L_8);
		U3CEnumerateChildrenU3Ec__Iterator0_t1235609798 * L_9 = V_0;
		U3CEnumerateChildrenU3Ec__Iterator0_t1235609798 * L_10 = L_9;
		NullCheck(L_10);
		L_10->set_U24PC_4(((int32_t)-2));
		return L_10;
	}
}
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.XPathNavigator::SelectChildren(System.Xml.XPath.XPathNodeType)
extern "C"  XPathNodeIterator_t3192332357 * XPathNavigator_SelectChildren_m4089331250 (XPathNavigator_t3981235968 * __this, int32_t ___type0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathNavigator_SelectChildren_m4089331250_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___type0;
		IL2CPP_RUNTIME_CLASS_INIT(XPathNavigator_t3981235968_il2cpp_TypeInfo_var);
		RuntimeObject* L_1 = XPathNavigator_EnumerateChildren_m1107095336(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		EnumerableIterator_t1602910416 * L_2 = (EnumerableIterator_t1602910416 *)il2cpp_codegen_object_new(EnumerableIterator_t1602910416_il2cpp_TypeInfo_var);
		EnumerableIterator__ctor_m970393097(L_2, L_1, 0, /*hidden argument*/NULL);
		WrapperIterator_t2718786873 * L_3 = (WrapperIterator_t2718786873 *)il2cpp_codegen_object_new(WrapperIterator_t2718786873_il2cpp_TypeInfo_var);
		WrapperIterator__ctor_m904768110(L_3, L_2, (RuntimeObject*)NULL, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.String System.Xml.XPath.XPathNavigator::ToString()
extern "C"  String_t* XPathNavigator_ToString_m921348895 (XPathNavigator_t3981235968 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.Xml.XPath.XPathItem::get_Value() */, __this);
		return L_0;
	}
}
// System.String System.Xml.XPath.XPathNavigator::LookupNamespace(System.String)
extern "C"  String_t* XPathNavigator_LookupNamespace_m2791382698 (XPathNavigator_t3981235968 * __this, String_t* ___prefix0, const RuntimeMethod* method)
{
	XPathNavigator_t3981235968 * V_0 = NULL;
	{
		XPathNavigator_t3981235968 * L_0 = VirtFuncInvoker0< XPathNavigator_t3981235968 * >::Invoke(14 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, __this);
		V_0 = L_0;
		XPathNavigator_t3981235968 * L_1 = V_0;
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(12 /* System.Xml.XPath.XPathNodeType System.Xml.XPath.XPathNavigator::get_NodeType() */, L_1);
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		XPathNavigator_t3981235968 * L_3 = V_0;
		NullCheck(L_3);
		VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, L_3);
	}

IL_001a:
	{
		XPathNavigator_t3981235968 * L_4 = V_0;
		String_t* L_5 = ___prefix0;
		NullCheck(L_4);
		bool L_6 = VirtFuncInvoker1< bool, String_t* >::Invoke(21 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToNamespace(System.String) */, L_4, L_5);
		if (!L_6)
		{
			goto IL_002d;
		}
	}
	{
		XPathNavigator_t3981235968 * L_7 = V_0;
		NullCheck(L_7);
		String_t* L_8 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.Xml.XPath.XPathItem::get_Value() */, L_7);
		return L_8;
	}

IL_002d:
	{
		return (String_t*)NULL;
	}
}
// System.Void System.Xml.XPath.XPathNavigator/<EnumerateChildren>c__Iterator0::.ctor()
extern "C"  void U3CEnumerateChildrenU3Ec__Iterator0__ctor_m2964582031 (U3CEnumerateChildrenU3Ec__Iterator0_t1235609798 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object System.Xml.XPath.XPathNavigator/<EnumerateChildren>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  RuntimeObject * U3CEnumerateChildrenU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m621572829 (U3CEnumerateChildrenU3Ec__Iterator0_t1235609798 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_5();
		return L_0;
	}
}
// System.Object System.Xml.XPath.XPathNavigator/<EnumerateChildren>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * U3CEnumerateChildrenU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2876953237 (U3CEnumerateChildrenU3Ec__Iterator0_t1235609798 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_5();
		return L_0;
	}
}
// System.Collections.IEnumerator System.Xml.XPath.XPathNavigator/<EnumerateChildren>c__Iterator0::System.Collections.IEnumerable.GetEnumerator()
extern "C"  RuntimeObject* U3CEnumerateChildrenU3Ec__Iterator0_System_Collections_IEnumerable_GetEnumerator_m2344063366 (U3CEnumerateChildrenU3Ec__Iterator0_t1235609798 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = U3CEnumerateChildrenU3Ec__Iterator0_System_Collections_Generic_IEnumerableU3CobjectU3E_GetEnumerator_m1191949246(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Object> System.Xml.XPath.XPathNavigator/<EnumerateChildren>c__Iterator0::System.Collections.Generic.IEnumerable<object>.GetEnumerator()
extern "C"  RuntimeObject* U3CEnumerateChildrenU3Ec__Iterator0_System_Collections_Generic_IEnumerableU3CobjectU3E_GetEnumerator_m1191949246 (U3CEnumerateChildrenU3Ec__Iterator0_t1235609798 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CEnumerateChildrenU3Ec__Iterator0_System_Collections_Generic_IEnumerableU3CobjectU3E_GetEnumerator_m1191949246_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CEnumerateChildrenU3Ec__Iterator0_t1235609798 * V_0 = NULL;
	{
		int32_t* L_0 = __this->get_address_of_U24PC_4();
		int32_t L_1 = Interlocked_CompareExchange_m3339239614(NULL /*static, unused*/, L_0, 0, ((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CEnumerateChildrenU3Ec__Iterator0_t1235609798 * L_2 = (U3CEnumerateChildrenU3Ec__Iterator0_t1235609798 *)il2cpp_codegen_object_new(U3CEnumerateChildrenU3Ec__Iterator0_t1235609798_il2cpp_TypeInfo_var);
		U3CEnumerateChildrenU3Ec__Iterator0__ctor_m2964582031(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		U3CEnumerateChildrenU3Ec__Iterator0_t1235609798 * L_3 = V_0;
		XPathNavigator_t3981235968 * L_4 = __this->get_U3CU24U3En_6();
		NullCheck(L_3);
		L_3->set_n_0(L_4);
		U3CEnumerateChildrenU3Ec__Iterator0_t1235609798 * L_5 = V_0;
		int32_t L_6 = __this->get_U3CU24U3Etype_7();
		NullCheck(L_5);
		L_5->set_type_3(L_6);
		U3CEnumerateChildrenU3Ec__Iterator0_t1235609798 * L_7 = V_0;
		return L_7;
	}
}
// System.Boolean System.Xml.XPath.XPathNavigator/<EnumerateChildren>c__Iterator0::MoveNext()
extern "C"  bool U3CEnumerateChildrenU3Ec__Iterator0_MoveNext_m2851675985 (U3CEnumerateChildrenU3Ec__Iterator0_t1235609798 * __this, const RuntimeMethod* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_4();
		V_0 = L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_00d4;
			}
		}
	}
	{
		goto IL_00eb;
	}

IL_0021:
	{
		XPathNavigator_t3981235968 * L_2 = __this->get_n_0();
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(25 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToFirstChild() */, L_2);
		if (L_3)
		{
			goto IL_0036;
		}
	}
	{
		goto IL_00eb;
	}

IL_0036:
	{
		XPathNavigator_t3981235968 * L_4 = __this->get_n_0();
		NullCheck(L_4);
		VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, L_4);
		XPathNavigator_t3981235968 * L_5 = __this->get_n_0();
		NullCheck(L_5);
		XPathNavigator_t3981235968 * L_6 = VirtFuncInvoker0< XPathNavigator_t3981235968 * >::Invoke(14 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, L_5);
		__this->set_U3CnavU3E__0_1(L_6);
		XPathNavigator_t3981235968 * L_7 = __this->get_U3CnavU3E__0_1();
		NullCheck(L_7);
		VirtFuncInvoker0< bool >::Invoke(25 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToFirstChild() */, L_7);
		__this->set_U3Cnav2U3E__1_2((XPathNavigator_t3981235968 *)NULL);
	}

IL_0066:
	{
		int32_t L_8 = __this->get_type_3();
		if ((((int32_t)L_8) == ((int32_t)((int32_t)9))))
		{
			goto IL_0089;
		}
	}
	{
		XPathNavigator_t3981235968 * L_9 = __this->get_U3CnavU3E__0_1();
		NullCheck(L_9);
		int32_t L_10 = VirtFuncInvoker0< int32_t >::Invoke(12 /* System.Xml.XPath.XPathNodeType System.Xml.XPath.XPathNavigator::get_NodeType() */, L_9);
		int32_t L_11 = __this->get_type_3();
		if ((!(((uint32_t)L_10) == ((uint32_t)L_11))))
		{
			goto IL_00d4;
		}
	}

IL_0089:
	{
		XPathNavigator_t3981235968 * L_12 = __this->get_U3Cnav2U3E__1_2();
		if (L_12)
		{
			goto IL_00aa;
		}
	}
	{
		XPathNavigator_t3981235968 * L_13 = __this->get_U3CnavU3E__0_1();
		NullCheck(L_13);
		XPathNavigator_t3981235968 * L_14 = VirtFuncInvoker0< XPathNavigator_t3981235968 * >::Invoke(14 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, L_13);
		__this->set_U3Cnav2U3E__1_2(L_14);
		goto IL_00bc;
	}

IL_00aa:
	{
		XPathNavigator_t3981235968 * L_15 = __this->get_U3Cnav2U3E__1_2();
		XPathNavigator_t3981235968 * L_16 = __this->get_U3CnavU3E__0_1();
		NullCheck(L_15);
		VirtFuncInvoker1< bool, XPathNavigator_t3981235968 * >::Invoke(19 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveTo(System.Xml.XPath.XPathNavigator) */, L_15, L_16);
	}

IL_00bc:
	{
		XPathNavigator_t3981235968 * L_17 = __this->get_U3Cnav2U3E__1_2();
		__this->set_U24current_5(L_17);
		__this->set_U24PC_4(1);
		goto IL_00ed;
	}

IL_00d4:
	{
		XPathNavigator_t3981235968 * L_18 = __this->get_U3CnavU3E__0_1();
		NullCheck(L_18);
		bool L_19 = VirtFuncInvoker0< bool >::Invoke(28 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToNext() */, L_18);
		if (L_19)
		{
			goto IL_0066;
		}
	}
	{
		__this->set_U24PC_4((-1));
	}

IL_00eb:
	{
		return (bool)0;
	}

IL_00ed:
	{
		return (bool)1;
	}
	// Dead block : IL_00ef: ldloc.1
}
// System.Void System.Xml.XPath.XPathNavigator/<EnumerateChildren>c__Iterator0::Dispose()
extern "C"  void U3CEnumerateChildrenU3Ec__Iterator0_Dispose_m3853171810 (U3CEnumerateChildrenU3Ec__Iterator0_t1235609798 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24PC_4((-1));
		return;
	}
}
// System.Void System.Xml.XPath.XPathNavigator/<EnumerateChildren>c__Iterator0::Reset()
extern "C"  void U3CEnumerateChildrenU3Ec__Iterator0_Reset_m3144178776 (U3CEnumerateChildrenU3Ec__Iterator0_t1235609798 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CEnumerateChildrenU3Ec__Iterator0_Reset_m3144178776_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Xml.XPath.XPathNavigator/EnumerableIterator::.ctor(System.Collections.IEnumerable,System.Int32)
extern "C"  void EnumerableIterator__ctor_m970393097 (EnumerableIterator_t1602910416 * __this, RuntimeObject* ___source0, int32_t ___pos1, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		XPathNodeIterator__ctor_m2254908861(__this, /*hidden argument*/NULL);
		RuntimeObject* L_0 = ___source0;
		__this->set_source_1(L_0);
		V_0 = 0;
		goto IL_001f;
	}

IL_0014:
	{
		VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.XPathNavigator/EnumerableIterator::MoveNext() */, __this);
		int32_t L_1 = V_0;
		V_0 = ((int32_t)((int32_t)L_1+(int32_t)1));
	}

IL_001f:
	{
		int32_t L_2 = V_0;
		int32_t L_3 = ___pos1;
		if ((((int32_t)L_2) < ((int32_t)L_3)))
		{
			goto IL_0014;
		}
	}
	{
		return;
	}
}
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.XPathNavigator/EnumerableIterator::Clone()
extern "C"  XPathNodeIterator_t3192332357 * EnumerableIterator_Clone_m4268561691 (EnumerableIterator_t1602910416 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EnumerableIterator_Clone_m4268561691_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = __this->get_source_1();
		int32_t L_1 = __this->get_pos_3();
		EnumerableIterator_t1602910416 * L_2 = (EnumerableIterator_t1602910416 *)il2cpp_codegen_object_new(EnumerableIterator_t1602910416_il2cpp_TypeInfo_var);
		EnumerableIterator__ctor_m970393097(L_2, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean System.Xml.XPath.XPathNavigator/EnumerableIterator::MoveNext()
extern "C"  bool EnumerableIterator_MoveNext_m2512854753 (EnumerableIterator_t1602910416 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EnumerableIterator_MoveNext_m2512854753_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = __this->get_e_2();
		if (L_0)
		{
			goto IL_001c;
		}
	}
	{
		RuntimeObject* L_1 = __this->get_source_1();
		NullCheck(L_1);
		RuntimeObject* L_2 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t2911409499_il2cpp_TypeInfo_var, L_1);
		__this->set_e_2(L_2);
	}

IL_001c:
	{
		RuntimeObject* L_3 = __this->get_e_2();
		NullCheck(L_3);
		bool L_4 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_3);
		if (L_4)
		{
			goto IL_002e;
		}
	}
	{
		return (bool)0;
	}

IL_002e:
	{
		int32_t L_5 = __this->get_pos_3();
		__this->set_pos_3(((int32_t)((int32_t)L_5+(int32_t)1)));
		return (bool)1;
	}
}
// System.Int32 System.Xml.XPath.XPathNavigator/EnumerableIterator::get_CurrentPosition()
extern "C"  int32_t EnumerableIterator_get_CurrentPosition_m3372306150 (EnumerableIterator_t1602910416 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_pos_3();
		return L_0;
	}
}
// System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator/EnumerableIterator::get_Current()
extern "C"  XPathNavigator_t3981235968 * EnumerableIterator_get_Current_m1712718357 (EnumerableIterator_t1602910416 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EnumerableIterator_get_Current_m1712718357_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	XPathNavigator_t3981235968 * G_B3_0 = NULL;
	{
		int32_t L_0 = __this->get_pos_3();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		G_B3_0 = ((XPathNavigator_t3981235968 *)(NULL));
		goto IL_0021;
	}

IL_0011:
	{
		RuntimeObject* L_1 = __this->get_e_2();
		NullCheck(L_1);
		RuntimeObject * L_2 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_1);
		G_B3_0 = ((XPathNavigator_t3981235968 *)CastclassClass((RuntimeObject*)L_2, XPathNavigator_t3981235968_il2cpp_TypeInfo_var));
	}

IL_0021:
	{
		return G_B3_0;
	}
}
// System.Void System.Xml.XPath.XPathNavigatorComparer::.ctor()
extern "C"  void XPathNavigatorComparer__ctor_m3253571973 (XPathNavigatorComparer_t2670709129 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.XPath.XPathNavigatorComparer::.cctor()
extern "C"  void XPathNavigatorComparer__cctor_m240886484 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathNavigatorComparer__cctor_m240886484_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		XPathNavigatorComparer_t2670709129 * L_0 = (XPathNavigatorComparer_t2670709129 *)il2cpp_codegen_object_new(XPathNavigatorComparer_t2670709129_il2cpp_TypeInfo_var);
		XPathNavigatorComparer__ctor_m3253571973(L_0, /*hidden argument*/NULL);
		((XPathNavigatorComparer_t2670709129_StaticFields*)il2cpp_codegen_static_fields_for(XPathNavigatorComparer_t2670709129_il2cpp_TypeInfo_var))->set_Instance_0(L_0);
		return;
	}
}
// System.Boolean System.Xml.XPath.XPathNavigatorComparer::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool XPathNavigatorComparer_System_Collections_IEqualityComparer_Equals_m1841324898 (XPathNavigatorComparer_t2670709129 * __this, RuntimeObject * ___o10, RuntimeObject * ___o21, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathNavigatorComparer_System_Collections_IEqualityComparer_Equals_m1841324898_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	XPathNavigator_t3981235968 * V_0 = NULL;
	XPathNavigator_t3981235968 * V_1 = NULL;
	int32_t G_B4_0 = 0;
	{
		RuntimeObject * L_0 = ___o10;
		V_0 = ((XPathNavigator_t3981235968 *)IsInstClass((RuntimeObject*)L_0, XPathNavigator_t3981235968_il2cpp_TypeInfo_var));
		RuntimeObject * L_1 = ___o21;
		V_1 = ((XPathNavigator_t3981235968 *)IsInstClass((RuntimeObject*)L_1, XPathNavigator_t3981235968_il2cpp_TypeInfo_var));
		XPathNavigator_t3981235968 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_0023;
		}
	}
	{
		XPathNavigator_t3981235968 * L_3 = V_1;
		if (!L_3)
		{
			goto IL_0023;
		}
	}
	{
		XPathNavigator_t3981235968 * L_4 = V_0;
		XPathNavigator_t3981235968 * L_5 = V_1;
		NullCheck(L_4);
		bool L_6 = VirtFuncInvoker1< bool, XPathNavigator_t3981235968 * >::Invoke(18 /* System.Boolean System.Xml.XPath.XPathNavigator::IsSamePosition(System.Xml.XPath.XPathNavigator) */, L_4, L_5);
		G_B4_0 = ((int32_t)(L_6));
		goto IL_0024;
	}

IL_0023:
	{
		G_B4_0 = 0;
	}

IL_0024:
	{
		return (bool)G_B4_0;
	}
}
// System.Int32 System.Xml.XPath.XPathNavigatorComparer::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t XPathNavigatorComparer_System_Collections_IEqualityComparer_GetHashCode_m2021225288 (XPathNavigatorComparer_t2670709129 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___obj0;
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_0);
		return L_1;
	}
}
// System.Int32 System.Xml.XPath.XPathNavigatorComparer::Compare(System.Object,System.Object)
extern "C"  int32_t XPathNavigatorComparer_Compare_m950798190 (XPathNavigatorComparer_t2670709129 * __this, RuntimeObject * ___o10, RuntimeObject * ___o21, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathNavigatorComparer_Compare_m950798190_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	XPathNavigator_t3981235968 * V_0 = NULL;
	XPathNavigator_t3981235968 * V_1 = NULL;
	int32_t V_2 = 0;
	{
		RuntimeObject * L_0 = ___o10;
		V_0 = ((XPathNavigator_t3981235968 *)IsInstClass((RuntimeObject*)L_0, XPathNavigator_t3981235968_il2cpp_TypeInfo_var));
		RuntimeObject * L_1 = ___o21;
		V_1 = ((XPathNavigator_t3981235968 *)IsInstClass((RuntimeObject*)L_1, XPathNavigator_t3981235968_il2cpp_TypeInfo_var));
		XPathNavigator_t3981235968 * L_2 = V_0;
		if (L_2)
		{
			goto IL_0016;
		}
	}
	{
		return (-1);
	}

IL_0016:
	{
		XPathNavigator_t3981235968 * L_3 = V_1;
		if (L_3)
		{
			goto IL_001e;
		}
	}
	{
		return 1;
	}

IL_001e:
	{
		XPathNavigator_t3981235968 * L_4 = V_0;
		XPathNavigator_t3981235968 * L_5 = V_1;
		NullCheck(L_4);
		int32_t L_6 = VirtFuncInvoker1< int32_t, XPathNavigator_t3981235968 * >::Invoke(15 /* System.Xml.XmlNodeOrder System.Xml.XPath.XPathNavigator::ComparePosition(System.Xml.XPath.XPathNavigator) */, L_4, L_5);
		V_2 = L_6;
		int32_t L_7 = V_2;
		if ((((int32_t)L_7) == ((int32_t)1)))
		{
			goto IL_003b;
		}
	}
	{
		int32_t L_8 = V_2;
		if ((((int32_t)L_8) == ((int32_t)2)))
		{
			goto IL_0039;
		}
	}
	{
		goto IL_003d;
	}

IL_0039:
	{
		return 0;
	}

IL_003b:
	{
		return 1;
	}

IL_003d:
	{
		return (-1);
	}
}
// System.Void System.Xml.XPath.XPathNodeIterator::.ctor()
extern "C"  void XPathNodeIterator__ctor_m2254908861 (XPathNodeIterator_t3192332357 * __this, const RuntimeMethod* method)
{
	{
		__this->set__count_0((-1));
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object System.Xml.XPath.XPathNodeIterator::System.ICloneable.Clone()
extern "C"  RuntimeObject * XPathNodeIterator_System_ICloneable_Clone_m176700494 (XPathNodeIterator_t3192332357 * __this, const RuntimeMethod* method)
{
	{
		XPathNodeIterator_t3192332357 * L_0 = VirtFuncInvoker0< XPathNodeIterator_t3192332357 * >::Invoke(9 /* System.Xml.XPath.XPathNodeIterator System.Xml.XPath.XPathNodeIterator::Clone() */, __this);
		return L_0;
	}
}
// System.Int32 System.Xml.XPath.XPathNodeIterator::get_Count()
extern "C"  int32_t XPathNodeIterator_get_Count_m1320026517 (XPathNodeIterator_t3192332357 * __this, const RuntimeMethod* method)
{
	XPathNodeIterator_t3192332357 * V_0 = NULL;
	{
		int32_t L_0 = __this->get__count_0();
		if ((!(((uint32_t)L_0) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		XPathNodeIterator_t3192332357 * L_1 = VirtFuncInvoker0< XPathNodeIterator_t3192332357 * >::Invoke(9 /* System.Xml.XPath.XPathNodeIterator System.Xml.XPath.XPathNodeIterator::Clone() */, __this);
		V_0 = L_1;
		goto IL_0018;
	}

IL_0018:
	{
		XPathNodeIterator_t3192332357 * L_2 = V_0;
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.XPathNodeIterator::MoveNext() */, L_2);
		if (L_3)
		{
			goto IL_0018;
		}
	}
	{
		XPathNodeIterator_t3192332357 * L_4 = V_0;
		NullCheck(L_4);
		int32_t L_5 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.Xml.XPath.XPathNodeIterator::get_CurrentPosition() */, L_4);
		__this->set__count_0(L_5);
	}

IL_002f:
	{
		int32_t L_6 = __this->get__count_0();
		return L_6;
	}
}
// System.Collections.IEnumerator System.Xml.XPath.XPathNodeIterator::GetEnumerator()
extern "C"  RuntimeObject* XPathNodeIterator_GetEnumerator_m1555209557 (XPathNodeIterator_t3192332357 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathNodeIterator_GetEnumerator_m1555209557_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CGetEnumeratorU3Ec__Iterator2_t959253998 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__Iterator2_t959253998 * L_0 = (U3CGetEnumeratorU3Ec__Iterator2_t959253998 *)il2cpp_codegen_object_new(U3CGetEnumeratorU3Ec__Iterator2_t959253998_il2cpp_TypeInfo_var);
		U3CGetEnumeratorU3Ec__Iterator2__ctor_m3138811673(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CGetEnumeratorU3Ec__Iterator2_t959253998 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_2(__this);
		U3CGetEnumeratorU3Ec__Iterator2_t959253998 * L_2 = V_0;
		return L_2;
	}
}
// System.Void System.Xml.XPath.XPathNodeIterator/<GetEnumerator>c__Iterator2::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator2__ctor_m3138811673 (U3CGetEnumeratorU3Ec__Iterator2_t959253998 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object System.Xml.XPath.XPathNodeIterator/<GetEnumerator>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  RuntimeObject * U3CGetEnumeratorU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m706015399 (U3CGetEnumeratorU3Ec__Iterator2_t959253998 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object System.Xml.XPath.XPathNodeIterator/<GetEnumerator>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * U3CGetEnumeratorU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m2717083791 (U3CGetEnumeratorU3Ec__Iterator2_t959253998 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Boolean System.Xml.XPath.XPathNodeIterator/<GetEnumerator>c__Iterator2::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator2_MoveNext_m2345571559 (U3CGetEnumeratorU3Ec__Iterator2_t959253998 * __this, const RuntimeMethod* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_0();
		V_0 = L_0;
		__this->set_U24PC_0((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_0043;
			}
		}
	}
	{
		goto IL_005a;
	}

IL_0021:
	{
		goto IL_0043;
	}

IL_0026:
	{
		XPathNodeIterator_t3192332357 * L_2 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_2);
		XPathNavigator_t3981235968 * L_3 = VirtFuncInvoker0< XPathNavigator_t3981235968 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_2);
		__this->set_U24current_1(L_3);
		__this->set_U24PC_0(1);
		goto IL_005c;
	}

IL_0043:
	{
		XPathNodeIterator_t3192332357 * L_4 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_4);
		bool L_5 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.XPathNodeIterator::MoveNext() */, L_4);
		if (L_5)
		{
			goto IL_0026;
		}
	}
	{
		__this->set_U24PC_0((-1));
	}

IL_005a:
	{
		return (bool)0;
	}

IL_005c:
	{
		return (bool)1;
	}
	// Dead block : IL_005e: ldloc.1
}
// System.Void System.Xml.XPath.XPathNodeIterator/<GetEnumerator>c__Iterator2::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator2_Dispose_m1981482874 (U3CGetEnumeratorU3Ec__Iterator2_t959253998 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24PC_0((-1));
		return;
	}
}
// System.Void System.Xml.XPath.XPathNodeIterator/<GetEnumerator>c__Iterator2::Reset()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator2_Reset_m3035138684 (U3CGetEnumeratorU3Ec__Iterator2_t959253998 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator2_Reset_m3035138684_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Xml.XPath.XPathNumericFunction::.ctor(System.Xml.XPath.FunctionArguments)
extern "C"  void XPathNumericFunction__ctor_m2271660427 (XPathNumericFunction_t2367269690 * __this, FunctionArguments_t2900945452 * ___args0, const RuntimeMethod* method)
{
	{
		FunctionArguments_t2900945452 * L_0 = ___args0;
		XPathFunction__ctor_m327599860(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Xml.XPath.XPathResultType System.Xml.XPath.XPathNumericFunction::get_ReturnType()
extern "C"  int32_t XPathNumericFunction_get_ReturnType_m980457913 (XPathNumericFunction_t2367269690 * __this, const RuntimeMethod* method)
{
	{
		return (int32_t)(0);
	}
}
// System.Object System.Xml.XPath.XPathNumericFunction::get_StaticValue()
extern "C"  RuntimeObject * XPathNumericFunction_get_StaticValue_m766662601 (XPathNumericFunction_t2367269690 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathNumericFunction_get_StaticValue_m766662601_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		double L_0 = VirtFuncInvoker0< double >::Invoke(10 /* System.Double System.Xml.XPath.Expression::get_StaticValueAsNumber() */, __this);
		double L_1 = L_0;
		RuntimeObject * L_2 = Box(Double_t4078015681_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Xml.XPath.XPathSortElement::.ctor()
extern "C"  void XPathSortElement__ctor_m4067665647 (XPathSortElement_t1040291071 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object System.Xml.XPath.XPathSorter::Evaluate(System.Xml.XPath.BaseIterator)
extern "C"  RuntimeObject * XPathSorter_Evaluate_m4282430526 (XPathSorter_t3491953490 * __this, BaseIterator_t2454437973 * ___iter0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathSorter_Evaluate_m4282430526_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get__type_2();
		if ((!(((uint32_t)L_0) == ((uint32_t)2))))
		{
			goto IL_001e;
		}
	}
	{
		Expression_t1283317256 * L_1 = __this->get__expr_0();
		BaseIterator_t2454437973 * L_2 = ___iter0;
		NullCheck(L_1);
		double L_3 = VirtFuncInvoker1< double, BaseIterator_t2454437973 * >::Invoke(16 /* System.Double System.Xml.XPath.Expression::EvaluateNumber(System.Xml.XPath.BaseIterator) */, L_1, L_2);
		double L_4 = L_3;
		RuntimeObject * L_5 = Box(Double_t4078015681_il2cpp_TypeInfo_var, &L_4);
		return L_5;
	}

IL_001e:
	{
		Expression_t1283317256 * L_6 = __this->get__expr_0();
		BaseIterator_t2454437973 * L_7 = ___iter0;
		NullCheck(L_6);
		String_t* L_8 = VirtFuncInvoker1< String_t*, BaseIterator_t2454437973 * >::Invoke(17 /* System.String System.Xml.XPath.Expression::EvaluateString(System.Xml.XPath.BaseIterator) */, L_6, L_7);
		return L_8;
	}
}
// System.Int32 System.Xml.XPath.XPathSorter::Compare(System.Object,System.Object)
extern "C"  int32_t XPathSorter_Compare_m1708989355 (XPathSorter_t3491953490 * __this, RuntimeObject * ___o10, RuntimeObject * ___o21, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathSorter_Compare_m1708989355_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = __this->get__cmp_1();
		RuntimeObject * L_1 = ___o10;
		RuntimeObject * L_2 = ___o21;
		NullCheck(L_0);
		int32_t L_3 = InterfaceFuncInvoker2< int32_t, RuntimeObject *, RuntimeObject * >::Invoke(0 /* System.Int32 System.Collections.IComparer::Compare(System.Object,System.Object) */, IComparer_t3952557350_il2cpp_TypeInfo_var, L_0, L_1, L_2);
		return L_3;
	}
}
// System.Int32 System.Xml.XPath.XPathSorters::System.Collections.IComparer.Compare(System.Object,System.Object)
extern "C"  int32_t XPathSorters_System_Collections_IComparer_Compare_m845991242 (XPathSorters_t4019574815 * __this, RuntimeObject * ___o10, RuntimeObject * ___o21, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathSorters_System_Collections_IComparer_Compare_m845991242_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	XPathSortElement_t1040291071 * V_0 = NULL;
	XPathSortElement_t1040291071 * V_1 = NULL;
	int32_t V_2 = 0;
	XPathSorter_t3491953490 * V_3 = NULL;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	{
		RuntimeObject * L_0 = ___o10;
		V_0 = ((XPathSortElement_t1040291071 *)CastclassClass((RuntimeObject*)L_0, XPathSortElement_t1040291071_il2cpp_TypeInfo_var));
		RuntimeObject * L_1 = ___o21;
		V_1 = ((XPathSortElement_t1040291071 *)CastclassClass((RuntimeObject*)L_1, XPathSortElement_t1040291071_il2cpp_TypeInfo_var));
		V_2 = 0;
		goto IL_004d;
	}

IL_0015:
	{
		ArrayList_t4252133567 * L_2 = __this->get__rgSorters_0();
		int32_t L_3 = V_2;
		NullCheck(L_2);
		RuntimeObject * L_4 = VirtFuncInvoker1< RuntimeObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_2, L_3);
		V_3 = ((XPathSorter_t3491953490 *)CastclassClass((RuntimeObject*)L_4, XPathSorter_t3491953490_il2cpp_TypeInfo_var));
		XPathSorter_t3491953490 * L_5 = V_3;
		XPathSortElement_t1040291071 * L_6 = V_0;
		NullCheck(L_6);
		ObjectU5BU5D_t3614634134* L_7 = L_6->get_Values_1();
		int32_t L_8 = V_2;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		RuntimeObject * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		XPathSortElement_t1040291071 * L_11 = V_1;
		NullCheck(L_11);
		ObjectU5BU5D_t3614634134* L_12 = L_11->get_Values_1();
		int32_t L_13 = V_2;
		NullCheck(L_12);
		int32_t L_14 = L_13;
		RuntimeObject * L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		NullCheck(L_5);
		int32_t L_16 = XPathSorter_Compare_m1708989355(L_5, L_10, L_15, /*hidden argument*/NULL);
		V_4 = L_16;
		int32_t L_17 = V_4;
		if (!L_17)
		{
			goto IL_0049;
		}
	}
	{
		int32_t L_18 = V_4;
		return L_18;
	}

IL_0049:
	{
		int32_t L_19 = V_2;
		V_2 = ((int32_t)((int32_t)L_19+(int32_t)1));
	}

IL_004d:
	{
		int32_t L_20 = V_2;
		ArrayList_t4252133567 * L_21 = __this->get__rgSorters_0();
		NullCheck(L_21);
		int32_t L_22 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_21);
		if ((((int32_t)L_20) < ((int32_t)L_22)))
		{
			goto IL_0015;
		}
	}
	{
		XPathSortElement_t1040291071 * L_23 = V_0;
		NullCheck(L_23);
		XPathNavigator_t3981235968 * L_24 = L_23->get_Navigator_0();
		XPathSortElement_t1040291071 * L_25 = V_1;
		NullCheck(L_25);
		XPathNavigator_t3981235968 * L_26 = L_25->get_Navigator_0();
		NullCheck(L_24);
		int32_t L_27 = VirtFuncInvoker1< int32_t, XPathNavigator_t3981235968 * >::Invoke(15 /* System.Xml.XmlNodeOrder System.Xml.XPath.XPathNavigator::ComparePosition(System.Xml.XPath.XPathNavigator) */, L_24, L_26);
		V_5 = L_27;
		int32_t L_28 = V_5;
		if ((((int32_t)L_28) == ((int32_t)1)))
		{
			goto IL_0088;
		}
	}
	{
		int32_t L_29 = V_5;
		if ((((int32_t)L_29) == ((int32_t)2)))
		{
			goto IL_0086;
		}
	}
	{
		goto IL_008a;
	}

IL_0086:
	{
		return 0;
	}

IL_0088:
	{
		return 1;
	}

IL_008a:
	{
		return (-1);
	}
}
// System.Xml.XPath.BaseIterator System.Xml.XPath.XPathSorters::Sort(System.Xml.XPath.BaseIterator)
extern "C"  BaseIterator_t2454437973 * XPathSorters_Sort_m3354109990 (XPathSorters_t4019574815 * __this, BaseIterator_t2454437973 * ___iter0, const RuntimeMethod* method)
{
	ArrayList_t4252133567 * V_0 = NULL;
	{
		BaseIterator_t2454437973 * L_0 = ___iter0;
		ArrayList_t4252133567 * L_1 = XPathSorters_ToSortElementList_m407179934(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		ArrayList_t4252133567 * L_2 = V_0;
		BaseIterator_t2454437973 * L_3 = ___iter0;
		NullCheck(L_3);
		RuntimeObject* L_4 = BaseIterator_get_NamespaceManager_m625352966(L_3, /*hidden argument*/NULL);
		BaseIterator_t2454437973 * L_5 = XPathSorters_Sort_m3271859196(__this, L_2, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Collections.ArrayList System.Xml.XPath.XPathSorters::ToSortElementList(System.Xml.XPath.BaseIterator)
extern "C"  ArrayList_t4252133567 * XPathSorters_ToSortElementList_m407179934 (XPathSorters_t4019574815 * __this, BaseIterator_t2454437973 * ___iter0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathSorters_ToSortElementList_m407179934_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ArrayList_t4252133567 * V_0 = NULL;
	int32_t V_1 = 0;
	XPathSortElement_t1040291071 * V_2 = NULL;
	int32_t V_3 = 0;
	XPathSorter_t3491953490 * V_4 = NULL;
	{
		ArrayList_t4252133567 * L_0 = (ArrayList_t4252133567 *)il2cpp_codegen_object_new(ArrayList_t4252133567_il2cpp_TypeInfo_var);
		ArrayList__ctor_m4012174379(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		ArrayList_t4252133567 * L_1 = __this->get__rgSorters_0();
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_1);
		V_1 = L_2;
		goto IL_0081;
	}

IL_0017:
	{
		XPathSortElement_t1040291071 * L_3 = (XPathSortElement_t1040291071 *)il2cpp_codegen_object_new(XPathSortElement_t1040291071_il2cpp_TypeInfo_var);
		XPathSortElement__ctor_m4067665647(L_3, /*hidden argument*/NULL);
		V_2 = L_3;
		XPathSortElement_t1040291071 * L_4 = V_2;
		BaseIterator_t2454437973 * L_5 = ___iter0;
		NullCheck(L_5);
		XPathNavigator_t3981235968 * L_6 = VirtFuncInvoker0< XPathNavigator_t3981235968 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_5);
		NullCheck(L_6);
		XPathNavigator_t3981235968 * L_7 = VirtFuncInvoker0< XPathNavigator_t3981235968 * >::Invoke(14 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, L_6);
		NullCheck(L_4);
		L_4->set_Navigator_0(L_7);
		XPathSortElement_t1040291071 * L_8 = V_2;
		int32_t L_9 = V_1;
		NullCheck(L_8);
		L_8->set_Values_1(((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)L_9)));
		V_3 = 0;
		goto IL_0068;
	}

IL_0041:
	{
		ArrayList_t4252133567 * L_10 = __this->get__rgSorters_0();
		int32_t L_11 = V_3;
		NullCheck(L_10);
		RuntimeObject * L_12 = VirtFuncInvoker1< RuntimeObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_10, L_11);
		V_4 = ((XPathSorter_t3491953490 *)CastclassClass((RuntimeObject*)L_12, XPathSorter_t3491953490_il2cpp_TypeInfo_var));
		XPathSortElement_t1040291071 * L_13 = V_2;
		NullCheck(L_13);
		ObjectU5BU5D_t3614634134* L_14 = L_13->get_Values_1();
		int32_t L_15 = V_3;
		XPathSorter_t3491953490 * L_16 = V_4;
		BaseIterator_t2454437973 * L_17 = ___iter0;
		NullCheck(L_16);
		RuntimeObject * L_18 = XPathSorter_Evaluate_m4282430526(L_16, L_17, /*hidden argument*/NULL);
		NullCheck(L_14);
		ArrayElementTypeCheck (L_14, L_18);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(L_15), (RuntimeObject *)L_18);
		int32_t L_19 = V_3;
		V_3 = ((int32_t)((int32_t)L_19+(int32_t)1));
	}

IL_0068:
	{
		int32_t L_20 = V_3;
		ArrayList_t4252133567 * L_21 = __this->get__rgSorters_0();
		NullCheck(L_21);
		int32_t L_22 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_21);
		if ((((int32_t)L_20) < ((int32_t)L_22)))
		{
			goto IL_0041;
		}
	}
	{
		ArrayList_t4252133567 * L_23 = V_0;
		XPathSortElement_t1040291071 * L_24 = V_2;
		NullCheck(L_23);
		VirtFuncInvoker1< int32_t, RuntimeObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_23, L_24);
	}

IL_0081:
	{
		BaseIterator_t2454437973 * L_25 = ___iter0;
		NullCheck(L_25);
		bool L_26 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.BaseIterator::MoveNext() */, L_25);
		if (L_26)
		{
			goto IL_0017;
		}
	}
	{
		ArrayList_t4252133567 * L_27 = V_0;
		return L_27;
	}
}
// System.Xml.XPath.BaseIterator System.Xml.XPath.XPathSorters::Sort(System.Collections.ArrayList,System.Xml.IXmlNamespaceResolver)
extern "C"  BaseIterator_t2454437973 * XPathSorters_Sort_m3271859196 (XPathSorters_t4019574815 * __this, ArrayList_t4252133567 * ___rgElts0, RuntimeObject* ___nsm1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XPathSorters_Sort_m3271859196_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	XPathNavigatorU5BU5D_t2529815809* V_0 = NULL;
	int32_t V_1 = 0;
	XPathSortElement_t1040291071 * V_2 = NULL;
	{
		ArrayList_t4252133567 * L_0 = ___rgElts0;
		NullCheck(L_0);
		VirtActionInvoker1< RuntimeObject* >::Invoke(46 /* System.Void System.Collections.ArrayList::Sort(System.Collections.IComparer) */, L_0, __this);
		ArrayList_t4252133567 * L_1 = ___rgElts0;
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_1);
		V_0 = ((XPathNavigatorU5BU5D_t2529815809*)SZArrayNew(XPathNavigatorU5BU5D_t2529815809_il2cpp_TypeInfo_var, (uint32_t)L_2));
		V_1 = 0;
		goto IL_0034;
	}

IL_001a:
	{
		ArrayList_t4252133567 * L_3 = ___rgElts0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		RuntimeObject * L_5 = VirtFuncInvoker1< RuntimeObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_3, L_4);
		V_2 = ((XPathSortElement_t1040291071 *)CastclassClass((RuntimeObject*)L_5, XPathSortElement_t1040291071_il2cpp_TypeInfo_var));
		XPathNavigatorU5BU5D_t2529815809* L_6 = V_0;
		int32_t L_7 = V_1;
		XPathSortElement_t1040291071 * L_8 = V_2;
		NullCheck(L_8);
		XPathNavigator_t3981235968 * L_9 = L_8->get_Navigator_0();
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_9);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(L_7), (XPathNavigator_t3981235968 *)L_9);
		int32_t L_10 = V_1;
		V_1 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0034:
	{
		int32_t L_11 = V_1;
		ArrayList_t4252133567 * L_12 = ___rgElts0;
		NullCheck(L_12);
		int32_t L_13 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_12);
		if ((((int32_t)L_11) < ((int32_t)L_13)))
		{
			goto IL_001a;
		}
	}
	{
		XPathNavigatorU5BU5D_t2529815809* L_14 = V_0;
		RuntimeObject* L_15 = ___nsm1;
		ListIterator_t3804705054 * L_16 = (ListIterator_t3804705054 *)il2cpp_codegen_object_new(ListIterator_t3804705054_il2cpp_TypeInfo_var);
		ListIterator__ctor_m1817971076(L_16, (RuntimeObject*)(RuntimeObject*)L_14, L_15, /*hidden argument*/NULL);
		return L_16;
	}
}
// System.Xml.Xsl.IXsltContextVariable System.Xml.Xsl.XsltContext::ResolveVariable(System.Xml.XmlQualifiedName)
extern "C"  RuntimeObject* XsltContext_ResolveVariable_m2369667327 (XsltContext_t2013960098 * __this, XmlQualifiedName_t1944712516 * ___name0, const RuntimeMethod* method)
{
	{
		XmlQualifiedName_t1944712516 * L_0 = ___name0;
		NullCheck(L_0);
		String_t* L_1 = XmlQualifiedName_get_Namespace_m2734729190(L_0, /*hidden argument*/NULL);
		String_t* L_2 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(9 /* System.String System.Xml.XmlNamespaceManager::LookupPrefix(System.String) */, __this, L_1);
		XmlQualifiedName_t1944712516 * L_3 = ___name0;
		NullCheck(L_3);
		String_t* L_4 = XmlQualifiedName_get_Name_m4055250010(L_3, /*hidden argument*/NULL);
		RuntimeObject* L_5 = VirtFuncInvoker2< RuntimeObject*, String_t*, String_t* >::Invoke(11 /* System.Xml.Xsl.IXsltContextVariable System.Xml.Xsl.XsltContext::ResolveVariable(System.String,System.String) */, __this, L_2, L_4);
		return L_5;
	}
}
// System.Xml.Xsl.IXsltContextFunction System.Xml.Xsl.XsltContext::ResolveFunction(System.Xml.XmlQualifiedName,System.Xml.XPath.XPathResultType[])
extern "C"  RuntimeObject* XsltContext_ResolveFunction_m50907708 (XsltContext_t2013960098 * __this, XmlQualifiedName_t1944712516 * ___name0, XPathResultTypeU5BU5D_t2966113519* ___argTypes1, const RuntimeMethod* method)
{
	{
		XmlQualifiedName_t1944712516 * L_0 = ___name0;
		NullCheck(L_0);
		String_t* L_1 = XmlQualifiedName_get_Name_m4055250010(L_0, /*hidden argument*/NULL);
		XmlQualifiedName_t1944712516 * L_2 = ___name0;
		NullCheck(L_2);
		String_t* L_3 = XmlQualifiedName_get_Namespace_m2734729190(L_2, /*hidden argument*/NULL);
		XPathResultTypeU5BU5D_t2966113519* L_4 = ___argTypes1;
		RuntimeObject* L_5 = VirtFuncInvoker3< RuntimeObject*, String_t*, String_t*, XPathResultTypeU5BU5D_t2966113519* >::Invoke(10 /* System.Xml.Xsl.IXsltContextFunction System.Xml.Xsl.XsltContext::ResolveFunction(System.String,System.String,System.Xml.XPath.XPathResultType[]) */, __this, L_1, L_3, L_4);
		return L_5;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
