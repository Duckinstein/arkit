﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.Text.StringBuilder
struct StringBuilder_t1221177846;
// System.String
struct String_t;
// TouchScript.TouchManagerInstance
struct TouchManagerInstance_t2629118981;
// WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey4
struct U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943;
// System.IO.Stream
struct Stream_t3255436806;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.IO.MemoryStream
struct MemoryStream_t743994179;
// System.Action`1<System.Byte[]>
struct Action_1_t3199133395;
// System.Action`1<System.Int64>
struct Action_1_t710877419;
// System.Action`1<System.Exception>
struct Action_1_t1729240069;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.Collections.Specialized.NameValueCollection
struct NameValueCollection_t3047564564;
// System.Version
struct Version_t1755874712;
// System.Action
struct Action_t3226471752;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// System.Collections.Generic.List`1<System.Byte>
struct List_1_t3052225568;
// System.Func`2<System.String,System.Boolean>
struct Func_2_t1989381442;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Func`2<System.Int32,System.Boolean>
struct Func_2_t3377395111;
// WebSocketSharp.Net.HttpListenerContext
struct HttpListenerContext_t994708409;
// System.Exception
struct Exception_t1927440687;
// System.Threading.ManualResetEvent
struct ManualResetEvent_t926074657;
// WebSocketSharp.Net.HttpListener
struct HttpListener_t4179429670;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.Collections.Generic.List`1<WebSocketSharp.Net.HttpListenerPrefix>
struct List_1_t4193866914;
// System.Net.IPEndPoint
struct IPEndPoint_t2615413766;
// System.Collections.Generic.Dictionary`2<WebSocketSharp.Net.HttpListenerPrefix,WebSocketSharp.Net.HttpListener>
struct Dictionary_2_t59835555;
// System.Net.Sockets.Socket
struct Socket_t3821512045;
// WebSocketSharp.Net.ServerSslConfiguration
struct ServerSslConfiguration_t204724213;
// System.Collections.Generic.Dictionary`2<WebSocketSharp.Net.HttpConnection,WebSocketSharp.Net.HttpConnection>
struct Dictionary_2_t3932468307;
// System.Collections.Generic.Dictionary`2<System.Net.IPAddress,System.Collections.Generic.Dictionary`2<System.Int32,WebSocketSharp.Net.EndPointListener>>
struct Dictionary_2_t3010034516;
// WebSocketSharp.Net.HttpListenerAsyncResult
struct HttpListenerAsyncResult_t3506939685;
// System.IntPtr[]
struct IntPtrU5BU5D_t169632028;
// System.Collections.IDictionary
struct IDictionary_t596158605;
// WebSocketSharp.Net.HttpConnection
struct HttpConnection_t2649486862;
// WebSocketSharp.Net.HttpListenerRequest
struct HttpListenerRequest_t2316381291;
// WebSocketSharp.Net.HttpListenerResponse
struct HttpListenerResponse_t2223360553;
// System.Security.Principal.IPrincipal
struct IPrincipal_t783141777;
// WebSocketSharp.Net.WebSockets.HttpListenerWebSocketContext
struct HttpListenerWebSocketContext_t1778866096;
// System.Net.IPAddress[]
struct IPAddressU5BU5D_t4087230954;
// System.Collections.Generic.List`1<WebSocketSharp.Net.Cookie>
struct List_1_t1195309592;
// System.Comparison`1<WebSocketSharp.Net.Cookie>
struct Comparison_1_t3087927311;
// System.Collections.Generic.HashSet`1<System.String>
struct HashSet_1_t362681087;
// TouchScript.Layers.UILayer
struct UILayer_t314035379;
// System.Text.Encoding
struct Encoding_t663144255;
// WebSocketSharp.Net.CookieCollection
struct CookieCollection_t4248997468;
// WebSocketSharp.Net.WebHeaderCollection
struct WebHeaderCollection_t1932982249;
// WebSocketSharp.Net.ResponseStream
struct ResponseStream_t3200689523;
// WebSocketSharp.Net.Cookie
struct Cookie_t1826188460;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.IDisposable
struct IDisposable_t2427283555;
// TouchScript.Layers.TouchLayer
struct TouchLayer_t2635439978;
// System.Void
struct Void_t1841601450;
// System.Func`2<System.String[],WebSocketSharp.HttpResponse>
struct Func_2_t1532221928;
// System.Func`2<System.String[],WebSocketSharp.HttpRequest>
struct Func_2_t557125244;
// System.Collections.Generic.IList`1<TouchScript.TouchPoint>
struct IList_1_t1500569684;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t2510243513;
// UnityEngine.Camera
struct Camera_t189460977;
// TouchScript.TouchPoint
struct TouchPoint_t959629083;
// WebSocketSharp.PayloadData
struct PayloadData_t3839327312;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.EventSystems.BaseRaycaster
struct BaseRaycaster_t2336171397;
// System.Uri
struct Uri_t19570940;
// UnityEngine.Collider2D
struct Collider2D_t646061738;
// UnityEngine.Collider
struct Collider_t3497673348;
// UnityEngine.Object
struct Object_t1021602117;
// WebSocketSharp.Net.ChunkStream
struct ChunkStream_t2067859643;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.Func`2<WebSocketSharp.Net.HttpListenerRequest,WebSocketSharp.Net.AuthenticationSchemes>
struct Func_2_t1102535652;
// System.Collections.Generic.List`1<WebSocketSharp.Net.HttpListenerContext>
struct List_1_t363829541;
// System.Collections.Generic.Dictionary`2<WebSocketSharp.Net.HttpListenerContext,WebSocketSharp.Net.HttpListenerContext>
struct Dictionary_2_t2381547847;
// WebSocketSharp.Logger
struct Logger_t2598199114;
// WebSocketSharp.Net.HttpListenerPrefixCollection
struct HttpListenerPrefixCollection_t3269177542;
// System.Func`2<System.Security.Principal.IIdentity,WebSocketSharp.Net.NetworkCredential>
struct Func_2_t353436505;
// System.Collections.Generic.List`1<WebSocketSharp.Net.HttpListenerAsyncResult>
struct List_1_t2876060817;
// System.Net.Security.LocalCertificateSelectionCallback
struct LocalCertificateSelectionCallback_t3696771181;
// System.Net.Security.RemoteCertificateValidationCallback
struct RemoteCertificateValidationCallback_t2756269959;
// UnityEngine.Canvas
struct Canvas_t209405766;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// System.Collections.Generic.List`1<WebSocketSharp.Net.Chunk>
struct List_1_t1673048283;
// WebSocketSharp.Net.RequestStream
struct RequestStream_t775716369;
// WebSocketSharp.Net.EndPointListener
struct EndPointListener_t3937551933;
// WebSocketSharp.Net.HttpListenerPrefix
struct HttpListenerPrefix_t529778486;
// System.Threading.Timer
struct Timer_t791717973;
// System.Threading.TimerCallback
struct TimerCallback_t1684927372;
// UnityEngine.Transform
struct Transform_t3275118058;
// System.Action`2<WebSocketSharp.LogData,System.String>
struct Action_2_t502883108;
// System.Diagnostics.StackFrame
struct StackFrame_t2050294881;
// System.Security.Cryptography.X509Certificates.X509CertificateCollection
struct X509CertificateCollection_t1197680765;
// TouchScript.InputSources.IInputSource
struct IInputSource_t3266560338;
// TouchScript.Tags
struct Tags_t1265380163;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;
// System.Collections.Generic.List`1<TouchScript.Layers.TouchLayer>
struct List_1_t2004561110;
// System.EventHandler`1<TouchScript.Layers.TouchLayerEventArgs>
struct EventHandler_1_t4133675533;
// TouchScript.Layers.ILayerDelegate
struct ILayerDelegate_t701621129;
// TouchScript.Layers.ProjectionParams
struct ProjectionParams_t2712959773;
// TouchScript.InputSources.ICoordinatesRemapper
struct ICoordinatesRemapper_t2162613424;
// TouchScript.InputSources.InputHandlers.MouseHandler
struct MouseHandler_t3116661769;
// TouchScript.InputSources.InputHandlers.TouchHandler
struct TouchHandler_t2521645213;
// System.Collections.Generic.List`1<TouchScript.Hit.HitTest>
struct List_1_t137760637;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_t3685274804;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1599784723;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t3466835263;
// System.Collections.Generic.Dictionary`2<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams>
struct Dictionary_2_t2765956170;
// System.EventHandler`1<TouchScript.TouchEventArgs>
struct EventHandler_1_t509234338;
// System.EventHandler
struct EventHandler_t277755526;
// TouchScript.Devices.Display.IDisplayDevice
struct IDisplayDevice_t2646363379;
// System.Collections.Generic.List`1<TouchScript.InputSources.IInputSource>
struct List_1_t2635681470;
// System.Collections.Generic.List`1<TouchScript.TouchPoint>
struct List_1_t328750215;
// System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.TouchPoint>
struct Dictionary_2_t4262422014;
// System.Collections.Generic.HashSet`1<System.Int32>
struct HashSet_1_t405338302;
// TouchScript.Utils.ObjectPool`1<TouchScript.TouchPoint>
struct ObjectPool_1_t312917930;
// TouchScript.Utils.ObjectPool`1<System.Collections.Generic.List`1<TouchScript.TouchPoint>>
struct ObjectPool_1_t3977006358;
// TouchScript.Utils.ObjectPool`1<System.Collections.Generic.List`1<System.Int32>>
struct ObjectPool_1_t794287427;
// System.Predicate`1<TouchScript.Layers.TouchLayer>
struct Predicate_1_t1078410093;
// System.Collections.Generic.List`1<UnityEngine.RaycastHit>
struct List_1_t3751268748;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t1079703083;
// System.Collections.Generic.List`1<UnityEngine.RaycastHit2D>
struct List_1_t3433029906;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef CLUSTERUTILS_T953614479_H
#define CLUSTERUTILS_T953614479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Utils.ClusterUtils
struct  ClusterUtils_t953614479  : public RuntimeObject
{
public:

public:
};

struct ClusterUtils_t953614479_StaticFields
{
public:
	// System.Text.StringBuilder TouchScript.Utils.ClusterUtils::hashString
	StringBuilder_t1221177846 * ___hashString_0;

public:
	inline static int32_t get_offset_of_hashString_0() { return static_cast<int32_t>(offsetof(ClusterUtils_t953614479_StaticFields, ___hashString_0)); }
	inline StringBuilder_t1221177846 * get_hashString_0() const { return ___hashString_0; }
	inline StringBuilder_t1221177846 ** get_address_of_hashString_0() { return &___hashString_0; }
	inline void set_hashString_0(StringBuilder_t1221177846 * value)
	{
		___hashString_0 = value;
		Il2CppCodeGenWriteBarrier((&___hashString_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLUSTERUTILS_T953614479_H
#ifndef GENERICIDENTITY_T607870731_H
#define GENERICIDENTITY_T607870731_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Principal.GenericIdentity
struct  GenericIdentity_t607870731  : public RuntimeObject
{
public:
	// System.String System.Security.Principal.GenericIdentity::m_name
	String_t* ___m_name_0;
	// System.String System.Security.Principal.GenericIdentity::m_type
	String_t* ___m_type_1;

public:
	inline static int32_t get_offset_of_m_name_0() { return static_cast<int32_t>(offsetof(GenericIdentity_t607870731, ___m_name_0)); }
	inline String_t* get_m_name_0() const { return ___m_name_0; }
	inline String_t** get_address_of_m_name_0() { return &___m_name_0; }
	inline void set_m_name_0(String_t* value)
	{
		___m_name_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_name_0), value);
	}

	inline static int32_t get_offset_of_m_type_1() { return static_cast<int32_t>(offsetof(GenericIdentity_t607870731, ___m_type_1)); }
	inline String_t* get_m_type_1() const { return ___m_type_1; }
	inline String_t** get_address_of_m_type_1() { return &___m_type_1; }
	inline void set_m_type_1(String_t* value)
	{
		___m_type_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_type_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICIDENTITY_T607870731_H
#ifndef EVENTHANDLEREXTENSIONS_T2256621884_H
#define EVENTHANDLEREXTENSIONS_T2256621884_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Utils.EventHandlerExtensions
struct  EventHandlerExtensions_t2256621884  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTHANDLEREXTENSIONS_T2256621884_H
#ifndef PROJECTIONUTILS_T1537418392_H
#define PROJECTIONUTILS_T1537418392_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Utils.ProjectionUtils
struct  ProjectionUtils_t1537418392  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROJECTIONUTILS_T1537418392_H
#ifndef TWOD_T1213408342_H
#define TWOD_T1213408342_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Utils.Geom.TwoD
struct  TwoD_t1213408342  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWOD_T1213408342_H
#ifndef U3CINTERNAL_ENDTOUCHU3EC__ANONSTOREY2_T1111470859_H
#define U3CINTERNAL_ENDTOUCHU3EC__ANONSTOREY2_T1111470859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.TouchManagerInstance/<INTERNAL_EndTouch>c__AnonStorey2
struct  U3CINTERNAL_EndTouchU3Ec__AnonStorey2_t1111470859  : public RuntimeObject
{
public:
	// System.Int32 TouchScript.TouchManagerInstance/<INTERNAL_EndTouch>c__AnonStorey2::id
	int32_t ___id_0;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(U3CINTERNAL_EndTouchU3Ec__AnonStorey2_t1111470859, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINTERNAL_ENDTOUCHU3EC__ANONSTOREY2_T1111470859_H
#ifndef U3CINTERNAL_MOVETOUCHU3EC__ANONSTOREY1_T1781908992_H
#define U3CINTERNAL_MOVETOUCHU3EC__ANONSTOREY1_T1781908992_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.TouchManagerInstance/<INTERNAL_MoveTouch>c__AnonStorey1
struct  U3CINTERNAL_MoveTouchU3Ec__AnonStorey1_t1781908992  : public RuntimeObject
{
public:
	// System.Int32 TouchScript.TouchManagerInstance/<INTERNAL_MoveTouch>c__AnonStorey1::id
	int32_t ___id_0;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(U3CINTERNAL_MoveTouchU3Ec__AnonStorey1_t1781908992, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINTERNAL_MOVETOUCHU3EC__ANONSTOREY1_T1781908992_H
#ifndef U3CINTERNAL_CANCELTOUCHU3EC__ANONSTOREY3_T4047882747_H
#define U3CINTERNAL_CANCELTOUCHU3EC__ANONSTOREY3_T4047882747_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.TouchManagerInstance/<INTERNAL_CancelTouch>c__AnonStorey3
struct  U3CINTERNAL_CancelTouchU3Ec__AnonStorey3_t4047882747  : public RuntimeObject
{
public:
	// System.Int32 TouchScript.TouchManagerInstance/<INTERNAL_CancelTouch>c__AnonStorey3::id
	int32_t ___id_0;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(U3CINTERNAL_CancelTouchU3Ec__AnonStorey3_t4047882747, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINTERNAL_CANCELTOUCHU3EC__ANONSTOREY3_T4047882747_H
#ifndef EVENTARGS_T3289624707_H
#define EVENTARGS_T3289624707_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventArgs
struct  EventArgs_t3289624707  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t3289624707_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t3289624707 * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t3289624707_StaticFields, ___Empty_0)); }
	inline EventArgs_t3289624707 * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t3289624707 ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t3289624707 * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTARGS_T3289624707_H
#ifndef U3CLATEAWAKEU3EC__ITERATOR0_T316551236_H
#define U3CLATEAWAKEU3EC__ITERATOR0_T316551236_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.TouchManagerInstance/<lateAwake>c__Iterator0
struct  U3ClateAwakeU3Ec__Iterator0_t316551236  : public RuntimeObject
{
public:
	// TouchScript.TouchManagerInstance TouchScript.TouchManagerInstance/<lateAwake>c__Iterator0::$this
	TouchManagerInstance_t2629118981 * ___U24this_0;
	// System.Object TouchScript.TouchManagerInstance/<lateAwake>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean TouchScript.TouchManagerInstance/<lateAwake>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 TouchScript.TouchManagerInstance/<lateAwake>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3ClateAwakeU3Ec__Iterator0_t316551236, ___U24this_0)); }
	inline TouchManagerInstance_t2629118981 * get_U24this_0() const { return ___U24this_0; }
	inline TouchManagerInstance_t2629118981 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(TouchManagerInstance_t2629118981 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3ClateAwakeU3Ec__Iterator0_t316551236, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3ClateAwakeU3Ec__Iterator0_t316551236, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3ClateAwakeU3Ec__Iterator0_t316551236, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLATEAWAKEU3EC__ITERATOR0_T316551236_H
#ifndef TOUCHUTILS_T3521695436_H
#define TOUCHUTILS_T3521695436_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Utils.TouchUtils
struct  TouchUtils_t3521695436  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHUTILS_T3521695436_H
#ifndef U3CREADBYTESASYNCU3EC__ANONSTOREY5_T4262448565_H
#define U3CREADBYTESASYNCU3EC__ANONSTOREY5_T4262448565_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey4/<ReadBytesAsync>c__AnonStorey5
struct  U3CReadBytesAsyncU3Ec__AnonStorey5_t4262448565  : public RuntimeObject
{
public:
	// System.Int64 WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey4/<ReadBytesAsync>c__AnonStorey5::len
	int64_t ___len_0;
	// WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey4 WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey4/<ReadBytesAsync>c__AnonStorey5::<>f__ref$4
	U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943 * ___U3CU3Ef__refU244_1;

public:
	inline static int32_t get_offset_of_len_0() { return static_cast<int32_t>(offsetof(U3CReadBytesAsyncU3Ec__AnonStorey5_t4262448565, ___len_0)); }
	inline int64_t get_len_0() const { return ___len_0; }
	inline int64_t* get_address_of_len_0() { return &___len_0; }
	inline void set_len_0(int64_t value)
	{
		___len_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU244_1() { return static_cast<int32_t>(offsetof(U3CReadBytesAsyncU3Ec__AnonStorey5_t4262448565, ___U3CU3Ef__refU244_1)); }
	inline U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943 * get_U3CU3Ef__refU244_1() const { return ___U3CU3Ef__refU244_1; }
	inline U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943 ** get_address_of_U3CU3Ef__refU244_1() { return &___U3CU3Ef__refU244_1; }
	inline void set_U3CU3Ef__refU244_1(U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943 * value)
	{
		___U3CU3Ef__refU244_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU244_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREADBYTESASYNCU3EC__ANONSTOREY5_T4262448565_H
#ifndef U3CREADBYTESASYNCU3EC__ANONSTOREY4_T1697116943_H
#define U3CREADBYTESASYNCU3EC__ANONSTOREY4_T1697116943_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey4
struct  U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943  : public RuntimeObject
{
public:
	// System.Int32 WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey4::bufferLength
	int32_t ___bufferLength_0;
	// System.IO.Stream WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey4::stream
	Stream_t3255436806 * ___stream_1;
	// System.Byte[] WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey4::buff
	ByteU5BU5D_t3397334013* ___buff_2;
	// System.IO.MemoryStream WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey4::dest
	MemoryStream_t743994179 * ___dest_3;
	// System.Action`1<System.Byte[]> WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey4::completed
	Action_1_t3199133395 * ___completed_4;
	// System.Action`1<System.Int64> WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey4::read
	Action_1_t710877419 * ___read_5;
	// System.Action`1<System.Exception> WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey4::error
	Action_1_t1729240069 * ___error_6;

public:
	inline static int32_t get_offset_of_bufferLength_0() { return static_cast<int32_t>(offsetof(U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943, ___bufferLength_0)); }
	inline int32_t get_bufferLength_0() const { return ___bufferLength_0; }
	inline int32_t* get_address_of_bufferLength_0() { return &___bufferLength_0; }
	inline void set_bufferLength_0(int32_t value)
	{
		___bufferLength_0 = value;
	}

	inline static int32_t get_offset_of_stream_1() { return static_cast<int32_t>(offsetof(U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943, ___stream_1)); }
	inline Stream_t3255436806 * get_stream_1() const { return ___stream_1; }
	inline Stream_t3255436806 ** get_address_of_stream_1() { return &___stream_1; }
	inline void set_stream_1(Stream_t3255436806 * value)
	{
		___stream_1 = value;
		Il2CppCodeGenWriteBarrier((&___stream_1), value);
	}

	inline static int32_t get_offset_of_buff_2() { return static_cast<int32_t>(offsetof(U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943, ___buff_2)); }
	inline ByteU5BU5D_t3397334013* get_buff_2() const { return ___buff_2; }
	inline ByteU5BU5D_t3397334013** get_address_of_buff_2() { return &___buff_2; }
	inline void set_buff_2(ByteU5BU5D_t3397334013* value)
	{
		___buff_2 = value;
		Il2CppCodeGenWriteBarrier((&___buff_2), value);
	}

	inline static int32_t get_offset_of_dest_3() { return static_cast<int32_t>(offsetof(U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943, ___dest_3)); }
	inline MemoryStream_t743994179 * get_dest_3() const { return ___dest_3; }
	inline MemoryStream_t743994179 ** get_address_of_dest_3() { return &___dest_3; }
	inline void set_dest_3(MemoryStream_t743994179 * value)
	{
		___dest_3 = value;
		Il2CppCodeGenWriteBarrier((&___dest_3), value);
	}

	inline static int32_t get_offset_of_completed_4() { return static_cast<int32_t>(offsetof(U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943, ___completed_4)); }
	inline Action_1_t3199133395 * get_completed_4() const { return ___completed_4; }
	inline Action_1_t3199133395 ** get_address_of_completed_4() { return &___completed_4; }
	inline void set_completed_4(Action_1_t3199133395 * value)
	{
		___completed_4 = value;
		Il2CppCodeGenWriteBarrier((&___completed_4), value);
	}

	inline static int32_t get_offset_of_read_5() { return static_cast<int32_t>(offsetof(U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943, ___read_5)); }
	inline Action_1_t710877419 * get_read_5() const { return ___read_5; }
	inline Action_1_t710877419 ** get_address_of_read_5() { return &___read_5; }
	inline void set_read_5(Action_1_t710877419 * value)
	{
		___read_5 = value;
		Il2CppCodeGenWriteBarrier((&___read_5), value);
	}

	inline static int32_t get_offset_of_error_6() { return static_cast<int32_t>(offsetof(U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943, ___error_6)); }
	inline Action_1_t1729240069 * get_error_6() const { return ___error_6; }
	inline Action_1_t1729240069 ** get_address_of_error_6() { return &___error_6; }
	inline void set_error_6(Action_1_t1729240069 * value)
	{
		___error_6 = value;
		Il2CppCodeGenWriteBarrier((&___error_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREADBYTESASYNCU3EC__ANONSTOREY4_T1697116943_H
#ifndef U3CSPLITHEADERVALUEU3EC__ITERATOR0_T2473694690_H
#define U3CSPLITHEADERVALUEU3EC__ITERATOR0_T2473694690_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Ext/<SplitHeaderValue>c__Iterator0
struct  U3CSplitHeaderValueU3Ec__Iterator0_t2473694690  : public RuntimeObject
{
public:
	// System.String WebSocketSharp.Ext/<SplitHeaderValue>c__Iterator0::value
	String_t* ___value_0;
	// System.Int32 WebSocketSharp.Ext/<SplitHeaderValue>c__Iterator0::<len>__0
	int32_t ___U3ClenU3E__0_1;
	// System.Char[] WebSocketSharp.Ext/<SplitHeaderValue>c__Iterator0::separators
	CharU5BU5D_t1328083999* ___separators_2;
	// System.String WebSocketSharp.Ext/<SplitHeaderValue>c__Iterator0::<seps>__0
	String_t* ___U3CsepsU3E__0_3;
	// System.Text.StringBuilder WebSocketSharp.Ext/<SplitHeaderValue>c__Iterator0::<buff>__0
	StringBuilder_t1221177846 * ___U3CbuffU3E__0_4;
	// System.Boolean WebSocketSharp.Ext/<SplitHeaderValue>c__Iterator0::<escaped>__0
	bool ___U3CescapedU3E__0_5;
	// System.Boolean WebSocketSharp.Ext/<SplitHeaderValue>c__Iterator0::<quoted>__0
	bool ___U3CquotedU3E__0_6;
	// System.Int32 WebSocketSharp.Ext/<SplitHeaderValue>c__Iterator0::<i>__1
	int32_t ___U3CiU3E__1_7;
	// System.Char WebSocketSharp.Ext/<SplitHeaderValue>c__Iterator0::<c>__2
	Il2CppChar ___U3CcU3E__2_8;
	// System.String WebSocketSharp.Ext/<SplitHeaderValue>c__Iterator0::$current
	String_t* ___U24current_9;
	// System.Boolean WebSocketSharp.Ext/<SplitHeaderValue>c__Iterator0::$disposing
	bool ___U24disposing_10;
	// System.Int32 WebSocketSharp.Ext/<SplitHeaderValue>c__Iterator0::$PC
	int32_t ___U24PC_11;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(U3CSplitHeaderValueU3Ec__Iterator0_t2473694690, ___value_0)); }
	inline String_t* get_value_0() const { return ___value_0; }
	inline String_t** get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(String_t* value)
	{
		___value_0 = value;
		Il2CppCodeGenWriteBarrier((&___value_0), value);
	}

	inline static int32_t get_offset_of_U3ClenU3E__0_1() { return static_cast<int32_t>(offsetof(U3CSplitHeaderValueU3Ec__Iterator0_t2473694690, ___U3ClenU3E__0_1)); }
	inline int32_t get_U3ClenU3E__0_1() const { return ___U3ClenU3E__0_1; }
	inline int32_t* get_address_of_U3ClenU3E__0_1() { return &___U3ClenU3E__0_1; }
	inline void set_U3ClenU3E__0_1(int32_t value)
	{
		___U3ClenU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_separators_2() { return static_cast<int32_t>(offsetof(U3CSplitHeaderValueU3Ec__Iterator0_t2473694690, ___separators_2)); }
	inline CharU5BU5D_t1328083999* get_separators_2() const { return ___separators_2; }
	inline CharU5BU5D_t1328083999** get_address_of_separators_2() { return &___separators_2; }
	inline void set_separators_2(CharU5BU5D_t1328083999* value)
	{
		___separators_2 = value;
		Il2CppCodeGenWriteBarrier((&___separators_2), value);
	}

	inline static int32_t get_offset_of_U3CsepsU3E__0_3() { return static_cast<int32_t>(offsetof(U3CSplitHeaderValueU3Ec__Iterator0_t2473694690, ___U3CsepsU3E__0_3)); }
	inline String_t* get_U3CsepsU3E__0_3() const { return ___U3CsepsU3E__0_3; }
	inline String_t** get_address_of_U3CsepsU3E__0_3() { return &___U3CsepsU3E__0_3; }
	inline void set_U3CsepsU3E__0_3(String_t* value)
	{
		___U3CsepsU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsepsU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U3CbuffU3E__0_4() { return static_cast<int32_t>(offsetof(U3CSplitHeaderValueU3Ec__Iterator0_t2473694690, ___U3CbuffU3E__0_4)); }
	inline StringBuilder_t1221177846 * get_U3CbuffU3E__0_4() const { return ___U3CbuffU3E__0_4; }
	inline StringBuilder_t1221177846 ** get_address_of_U3CbuffU3E__0_4() { return &___U3CbuffU3E__0_4; }
	inline void set_U3CbuffU3E__0_4(StringBuilder_t1221177846 * value)
	{
		___U3CbuffU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CbuffU3E__0_4), value);
	}

	inline static int32_t get_offset_of_U3CescapedU3E__0_5() { return static_cast<int32_t>(offsetof(U3CSplitHeaderValueU3Ec__Iterator0_t2473694690, ___U3CescapedU3E__0_5)); }
	inline bool get_U3CescapedU3E__0_5() const { return ___U3CescapedU3E__0_5; }
	inline bool* get_address_of_U3CescapedU3E__0_5() { return &___U3CescapedU3E__0_5; }
	inline void set_U3CescapedU3E__0_5(bool value)
	{
		___U3CescapedU3E__0_5 = value;
	}

	inline static int32_t get_offset_of_U3CquotedU3E__0_6() { return static_cast<int32_t>(offsetof(U3CSplitHeaderValueU3Ec__Iterator0_t2473694690, ___U3CquotedU3E__0_6)); }
	inline bool get_U3CquotedU3E__0_6() const { return ___U3CquotedU3E__0_6; }
	inline bool* get_address_of_U3CquotedU3E__0_6() { return &___U3CquotedU3E__0_6; }
	inline void set_U3CquotedU3E__0_6(bool value)
	{
		___U3CquotedU3E__0_6 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E__1_7() { return static_cast<int32_t>(offsetof(U3CSplitHeaderValueU3Ec__Iterator0_t2473694690, ___U3CiU3E__1_7)); }
	inline int32_t get_U3CiU3E__1_7() const { return ___U3CiU3E__1_7; }
	inline int32_t* get_address_of_U3CiU3E__1_7() { return &___U3CiU3E__1_7; }
	inline void set_U3CiU3E__1_7(int32_t value)
	{
		___U3CiU3E__1_7 = value;
	}

	inline static int32_t get_offset_of_U3CcU3E__2_8() { return static_cast<int32_t>(offsetof(U3CSplitHeaderValueU3Ec__Iterator0_t2473694690, ___U3CcU3E__2_8)); }
	inline Il2CppChar get_U3CcU3E__2_8() const { return ___U3CcU3E__2_8; }
	inline Il2CppChar* get_address_of_U3CcU3E__2_8() { return &___U3CcU3E__2_8; }
	inline void set_U3CcU3E__2_8(Il2CppChar value)
	{
		___U3CcU3E__2_8 = value;
	}

	inline static int32_t get_offset_of_U24current_9() { return static_cast<int32_t>(offsetof(U3CSplitHeaderValueU3Ec__Iterator0_t2473694690, ___U24current_9)); }
	inline String_t* get_U24current_9() const { return ___U24current_9; }
	inline String_t** get_address_of_U24current_9() { return &___U24current_9; }
	inline void set_U24current_9(String_t* value)
	{
		___U24current_9 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_9), value);
	}

	inline static int32_t get_offset_of_U24disposing_10() { return static_cast<int32_t>(offsetof(U3CSplitHeaderValueU3Ec__Iterator0_t2473694690, ___U24disposing_10)); }
	inline bool get_U24disposing_10() const { return ___U24disposing_10; }
	inline bool* get_address_of_U24disposing_10() { return &___U24disposing_10; }
	inline void set_U24disposing_10(bool value)
	{
		___U24disposing_10 = value;
	}

	inline static int32_t get_offset_of_U24PC_11() { return static_cast<int32_t>(offsetof(U3CSplitHeaderValueU3Ec__Iterator0_t2473694690, ___U24PC_11)); }
	inline int32_t get_U24PC_11() const { return ___U24PC_11; }
	inline int32_t* get_address_of_U24PC_11() { return &___U24PC_11; }
	inline void set_U24PC_11(int32_t value)
	{
		___U24PC_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSPLITHEADERVALUEU3EC__ITERATOR0_T2473694690_H
#ifndef HTTPBASE_T4283398485_H
#define HTTPBASE_T4283398485_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.HttpBase
struct  HttpBase_t4283398485  : public RuntimeObject
{
public:
	// System.Collections.Specialized.NameValueCollection WebSocketSharp.HttpBase::_headers
	NameValueCollection_t3047564564 * ____headers_0;
	// System.Version WebSocketSharp.HttpBase::_version
	Version_t1755874712 * ____version_2;
	// System.Byte[] WebSocketSharp.HttpBase::EntityBodyData
	ByteU5BU5D_t3397334013* ___EntityBodyData_3;

public:
	inline static int32_t get_offset_of__headers_0() { return static_cast<int32_t>(offsetof(HttpBase_t4283398485, ____headers_0)); }
	inline NameValueCollection_t3047564564 * get__headers_0() const { return ____headers_0; }
	inline NameValueCollection_t3047564564 ** get_address_of__headers_0() { return &____headers_0; }
	inline void set__headers_0(NameValueCollection_t3047564564 * value)
	{
		____headers_0 = value;
		Il2CppCodeGenWriteBarrier((&____headers_0), value);
	}

	inline static int32_t get_offset_of__version_2() { return static_cast<int32_t>(offsetof(HttpBase_t4283398485, ____version_2)); }
	inline Version_t1755874712 * get__version_2() const { return ____version_2; }
	inline Version_t1755874712 ** get_address_of__version_2() { return &____version_2; }
	inline void set__version_2(Version_t1755874712 * value)
	{
		____version_2 = value;
		Il2CppCodeGenWriteBarrier((&____version_2), value);
	}

	inline static int32_t get_offset_of_EntityBodyData_3() { return static_cast<int32_t>(offsetof(HttpBase_t4283398485, ___EntityBodyData_3)); }
	inline ByteU5BU5D_t3397334013* get_EntityBodyData_3() const { return ___EntityBodyData_3; }
	inline ByteU5BU5D_t3397334013** get_address_of_EntityBodyData_3() { return &___EntityBodyData_3; }
	inline void set_EntityBodyData_3(ByteU5BU5D_t3397334013* value)
	{
		___EntityBodyData_3 = value;
		Il2CppCodeGenWriteBarrier((&___EntityBodyData_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPBASE_T4283398485_H
#ifndef U3CWRITEBYTESASYNCU3EC__ANONSTOREY6_T896389242_H
#define U3CWRITEBYTESASYNCU3EC__ANONSTOREY6_T896389242_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Ext/<WriteBytesAsync>c__AnonStorey6
struct  U3CWriteBytesAsyncU3Ec__AnonStorey6_t896389242  : public RuntimeObject
{
public:
	// System.Action WebSocketSharp.Ext/<WriteBytesAsync>c__AnonStorey6::completed
	Action_t3226471752 * ___completed_0;
	// System.IO.MemoryStream WebSocketSharp.Ext/<WriteBytesAsync>c__AnonStorey6::input
	MemoryStream_t743994179 * ___input_1;
	// System.Action`1<System.Exception> WebSocketSharp.Ext/<WriteBytesAsync>c__AnonStorey6::error
	Action_1_t1729240069 * ___error_2;

public:
	inline static int32_t get_offset_of_completed_0() { return static_cast<int32_t>(offsetof(U3CWriteBytesAsyncU3Ec__AnonStorey6_t896389242, ___completed_0)); }
	inline Action_t3226471752 * get_completed_0() const { return ___completed_0; }
	inline Action_t3226471752 ** get_address_of_completed_0() { return &___completed_0; }
	inline void set_completed_0(Action_t3226471752 * value)
	{
		___completed_0 = value;
		Il2CppCodeGenWriteBarrier((&___completed_0), value);
	}

	inline static int32_t get_offset_of_input_1() { return static_cast<int32_t>(offsetof(U3CWriteBytesAsyncU3Ec__AnonStorey6_t896389242, ___input_1)); }
	inline MemoryStream_t743994179 * get_input_1() const { return ___input_1; }
	inline MemoryStream_t743994179 ** get_address_of_input_1() { return &___input_1; }
	inline void set_input_1(MemoryStream_t743994179 * value)
	{
		___input_1 = value;
		Il2CppCodeGenWriteBarrier((&___input_1), value);
	}

	inline static int32_t get_offset_of_error_2() { return static_cast<int32_t>(offsetof(U3CWriteBytesAsyncU3Ec__AnonStorey6_t896389242, ___error_2)); }
	inline Action_1_t1729240069 * get_error_2() const { return ___error_2; }
	inline Action_1_t1729240069 ** get_address_of_error_2() { return &___error_2; }
	inline void set_error_2(Action_1_t1729240069 * value)
	{
		___error_2 = value;
		Il2CppCodeGenWriteBarrier((&___error_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWRITEBYTESASYNCU3EC__ANONSTOREY6_T896389242_H
#ifndef U3CREADBYTESASYNCU3EC__ANONSTOREY3_T1697116946_H
#define U3CREADBYTESASYNCU3EC__ANONSTOREY3_T1697116946_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey3
struct  U3CReadBytesAsyncU3Ec__AnonStorey3_t1697116946  : public RuntimeObject
{
public:
	// System.IO.Stream WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey3::stream
	Stream_t3255436806 * ___stream_0;
	// System.Int32 WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey3::length
	int32_t ___length_1;
	// System.Action`1<System.Byte[]> WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey3::completed
	Action_1_t3199133395 * ___completed_2;
	// System.Byte[] WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey3::buff
	ByteU5BU5D_t3397334013* ___buff_3;
	// System.Int32 WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey3::offset
	int32_t ___offset_4;
	// System.AsyncCallback WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey3::callback
	AsyncCallback_t163412349 * ___callback_5;
	// System.Action`1<System.Exception> WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey3::error
	Action_1_t1729240069 * ___error_6;

public:
	inline static int32_t get_offset_of_stream_0() { return static_cast<int32_t>(offsetof(U3CReadBytesAsyncU3Ec__AnonStorey3_t1697116946, ___stream_0)); }
	inline Stream_t3255436806 * get_stream_0() const { return ___stream_0; }
	inline Stream_t3255436806 ** get_address_of_stream_0() { return &___stream_0; }
	inline void set_stream_0(Stream_t3255436806 * value)
	{
		___stream_0 = value;
		Il2CppCodeGenWriteBarrier((&___stream_0), value);
	}

	inline static int32_t get_offset_of_length_1() { return static_cast<int32_t>(offsetof(U3CReadBytesAsyncU3Ec__AnonStorey3_t1697116946, ___length_1)); }
	inline int32_t get_length_1() const { return ___length_1; }
	inline int32_t* get_address_of_length_1() { return &___length_1; }
	inline void set_length_1(int32_t value)
	{
		___length_1 = value;
	}

	inline static int32_t get_offset_of_completed_2() { return static_cast<int32_t>(offsetof(U3CReadBytesAsyncU3Ec__AnonStorey3_t1697116946, ___completed_2)); }
	inline Action_1_t3199133395 * get_completed_2() const { return ___completed_2; }
	inline Action_1_t3199133395 ** get_address_of_completed_2() { return &___completed_2; }
	inline void set_completed_2(Action_1_t3199133395 * value)
	{
		___completed_2 = value;
		Il2CppCodeGenWriteBarrier((&___completed_2), value);
	}

	inline static int32_t get_offset_of_buff_3() { return static_cast<int32_t>(offsetof(U3CReadBytesAsyncU3Ec__AnonStorey3_t1697116946, ___buff_3)); }
	inline ByteU5BU5D_t3397334013* get_buff_3() const { return ___buff_3; }
	inline ByteU5BU5D_t3397334013** get_address_of_buff_3() { return &___buff_3; }
	inline void set_buff_3(ByteU5BU5D_t3397334013* value)
	{
		___buff_3 = value;
		Il2CppCodeGenWriteBarrier((&___buff_3), value);
	}

	inline static int32_t get_offset_of_offset_4() { return static_cast<int32_t>(offsetof(U3CReadBytesAsyncU3Ec__AnonStorey3_t1697116946, ___offset_4)); }
	inline int32_t get_offset_4() const { return ___offset_4; }
	inline int32_t* get_address_of_offset_4() { return &___offset_4; }
	inline void set_offset_4(int32_t value)
	{
		___offset_4 = value;
	}

	inline static int32_t get_offset_of_callback_5() { return static_cast<int32_t>(offsetof(U3CReadBytesAsyncU3Ec__AnonStorey3_t1697116946, ___callback_5)); }
	inline AsyncCallback_t163412349 * get_callback_5() const { return ___callback_5; }
	inline AsyncCallback_t163412349 ** get_address_of_callback_5() { return &___callback_5; }
	inline void set_callback_5(AsyncCallback_t163412349 * value)
	{
		___callback_5 = value;
		Il2CppCodeGenWriteBarrier((&___callback_5), value);
	}

	inline static int32_t get_offset_of_error_6() { return static_cast<int32_t>(offsetof(U3CReadBytesAsyncU3Ec__AnonStorey3_t1697116946, ___error_6)); }
	inline Action_1_t1729240069 * get_error_6() const { return ___error_6; }
	inline Action_1_t1729240069 ** get_address_of_error_6() { return &___error_6; }
	inline void set_error_6(Action_1_t1729240069 * value)
	{
		___error_6 = value;
		Il2CppCodeGenWriteBarrier((&___error_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREADBYTESASYNCU3EC__ANONSTOREY3_T1697116946_H
#ifndef U3CREADHEADERSU3EC__ANONSTOREY0_T2579283176_H
#define U3CREADHEADERSU3EC__ANONSTOREY0_T2579283176_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.HttpBase/<readHeaders>c__AnonStorey0
struct  U3CreadHeadersU3Ec__AnonStorey0_t2579283176  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.Byte> WebSocketSharp.HttpBase/<readHeaders>c__AnonStorey0::buff
	List_1_t3052225568 * ___buff_0;
	// System.Int32 WebSocketSharp.HttpBase/<readHeaders>c__AnonStorey0::cnt
	int32_t ___cnt_1;

public:
	inline static int32_t get_offset_of_buff_0() { return static_cast<int32_t>(offsetof(U3CreadHeadersU3Ec__AnonStorey0_t2579283176, ___buff_0)); }
	inline List_1_t3052225568 * get_buff_0() const { return ___buff_0; }
	inline List_1_t3052225568 ** get_address_of_buff_0() { return &___buff_0; }
	inline void set_buff_0(List_1_t3052225568 * value)
	{
		___buff_0 = value;
		Il2CppCodeGenWriteBarrier((&___buff_0), value);
	}

	inline static int32_t get_offset_of_cnt_1() { return static_cast<int32_t>(offsetof(U3CreadHeadersU3Ec__AnonStorey0_t2579283176, ___cnt_1)); }
	inline int32_t get_cnt_1() const { return ___cnt_1; }
	inline int32_t* get_address_of_cnt_1() { return &___cnt_1; }
	inline void set_cnt_1(int32_t value)
	{
		___cnt_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREADHEADERSU3EC__ANONSTOREY0_T2579283176_H
#ifndef TRANSFORMUTILS_T390635157_H
#define TRANSFORMUTILS_T390635157_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Utils.TransformUtils
struct  TransformUtils_t390635157  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMUTILS_T390635157_H
#ifndef EXT_T870230697_H
#define EXT_T870230697_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Ext
struct  Ext_t870230697  : public RuntimeObject
{
public:

public:
};

struct Ext_t870230697_StaticFields
{
public:
	// System.Byte[] WebSocketSharp.Ext::_last
	ByteU5BU5D_t3397334013* ____last_0;
	// System.Func`2<System.String,System.Boolean> WebSocketSharp.Ext::<>f__am$cache0
	Func_2_t1989381442 * ___U3CU3Ef__amU24cache0_2;

public:
	inline static int32_t get_offset_of__last_0() { return static_cast<int32_t>(offsetof(Ext_t870230697_StaticFields, ____last_0)); }
	inline ByteU5BU5D_t3397334013* get__last_0() const { return ____last_0; }
	inline ByteU5BU5D_t3397334013** get_address_of__last_0() { return &____last_0; }
	inline void set__last_0(ByteU5BU5D_t3397334013* value)
	{
		____last_0 = value;
		Il2CppCodeGenWriteBarrier((&____last_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_2() { return static_cast<int32_t>(offsetof(Ext_t870230697_StaticFields, ___U3CU3Ef__amU24cache0_2)); }
	inline Func_2_t1989381442 * get_U3CU3Ef__amU24cache0_2() const { return ___U3CU3Ef__amU24cache0_2; }
	inline Func_2_t1989381442 ** get_address_of_U3CU3Ef__amU24cache0_2() { return &___U3CU3Ef__amU24cache0_2; }
	inline void set_U3CU3Ef__amU24cache0_2(Func_2_t1989381442 * value)
	{
		___U3CU3Ef__amU24cache0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXT_T870230697_H
#ifndef U3CCOPYTOASYNCU3EC__ANONSTOREY2_T620221128_H
#define U3CCOPYTOASYNCU3EC__ANONSTOREY2_T620221128_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Ext/<CopyToAsync>c__AnonStorey2
struct  U3CCopyToAsyncU3Ec__AnonStorey2_t620221128  : public RuntimeObject
{
public:
	// System.IO.Stream WebSocketSharp.Ext/<CopyToAsync>c__AnonStorey2::source
	Stream_t3255436806 * ___source_0;
	// System.Action WebSocketSharp.Ext/<CopyToAsync>c__AnonStorey2::completed
	Action_t3226471752 * ___completed_1;
	// System.IO.Stream WebSocketSharp.Ext/<CopyToAsync>c__AnonStorey2::destination
	Stream_t3255436806 * ___destination_2;
	// System.Byte[] WebSocketSharp.Ext/<CopyToAsync>c__AnonStorey2::buff
	ByteU5BU5D_t3397334013* ___buff_3;
	// System.Int32 WebSocketSharp.Ext/<CopyToAsync>c__AnonStorey2::bufferLength
	int32_t ___bufferLength_4;
	// System.AsyncCallback WebSocketSharp.Ext/<CopyToAsync>c__AnonStorey2::callback
	AsyncCallback_t163412349 * ___callback_5;
	// System.Action`1<System.Exception> WebSocketSharp.Ext/<CopyToAsync>c__AnonStorey2::error
	Action_1_t1729240069 * ___error_6;

public:
	inline static int32_t get_offset_of_source_0() { return static_cast<int32_t>(offsetof(U3CCopyToAsyncU3Ec__AnonStorey2_t620221128, ___source_0)); }
	inline Stream_t3255436806 * get_source_0() const { return ___source_0; }
	inline Stream_t3255436806 ** get_address_of_source_0() { return &___source_0; }
	inline void set_source_0(Stream_t3255436806 * value)
	{
		___source_0 = value;
		Il2CppCodeGenWriteBarrier((&___source_0), value);
	}

	inline static int32_t get_offset_of_completed_1() { return static_cast<int32_t>(offsetof(U3CCopyToAsyncU3Ec__AnonStorey2_t620221128, ___completed_1)); }
	inline Action_t3226471752 * get_completed_1() const { return ___completed_1; }
	inline Action_t3226471752 ** get_address_of_completed_1() { return &___completed_1; }
	inline void set_completed_1(Action_t3226471752 * value)
	{
		___completed_1 = value;
		Il2CppCodeGenWriteBarrier((&___completed_1), value);
	}

	inline static int32_t get_offset_of_destination_2() { return static_cast<int32_t>(offsetof(U3CCopyToAsyncU3Ec__AnonStorey2_t620221128, ___destination_2)); }
	inline Stream_t3255436806 * get_destination_2() const { return ___destination_2; }
	inline Stream_t3255436806 ** get_address_of_destination_2() { return &___destination_2; }
	inline void set_destination_2(Stream_t3255436806 * value)
	{
		___destination_2 = value;
		Il2CppCodeGenWriteBarrier((&___destination_2), value);
	}

	inline static int32_t get_offset_of_buff_3() { return static_cast<int32_t>(offsetof(U3CCopyToAsyncU3Ec__AnonStorey2_t620221128, ___buff_3)); }
	inline ByteU5BU5D_t3397334013* get_buff_3() const { return ___buff_3; }
	inline ByteU5BU5D_t3397334013** get_address_of_buff_3() { return &___buff_3; }
	inline void set_buff_3(ByteU5BU5D_t3397334013* value)
	{
		___buff_3 = value;
		Il2CppCodeGenWriteBarrier((&___buff_3), value);
	}

	inline static int32_t get_offset_of_bufferLength_4() { return static_cast<int32_t>(offsetof(U3CCopyToAsyncU3Ec__AnonStorey2_t620221128, ___bufferLength_4)); }
	inline int32_t get_bufferLength_4() const { return ___bufferLength_4; }
	inline int32_t* get_address_of_bufferLength_4() { return &___bufferLength_4; }
	inline void set_bufferLength_4(int32_t value)
	{
		___bufferLength_4 = value;
	}

	inline static int32_t get_offset_of_callback_5() { return static_cast<int32_t>(offsetof(U3CCopyToAsyncU3Ec__AnonStorey2_t620221128, ___callback_5)); }
	inline AsyncCallback_t163412349 * get_callback_5() const { return ___callback_5; }
	inline AsyncCallback_t163412349 ** get_address_of_callback_5() { return &___callback_5; }
	inline void set_callback_5(AsyncCallback_t163412349 * value)
	{
		___callback_5 = value;
		Il2CppCodeGenWriteBarrier((&___callback_5), value);
	}

	inline static int32_t get_offset_of_error_6() { return static_cast<int32_t>(offsetof(U3CCopyToAsyncU3Ec__AnonStorey2_t620221128, ___error_6)); }
	inline Action_1_t1729240069 * get_error_6() const { return ___error_6; }
	inline Action_1_t1729240069 ** get_address_of_error_6() { return &___error_6; }
	inline void set_error_6(Action_1_t1729240069 * value)
	{
		___error_6 = value;
		Il2CppCodeGenWriteBarrier((&___error_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOPYTOASYNCU3EC__ANONSTOREY2_T620221128_H
#ifndef U3CCONTAINSTWICEU3EC__ANONSTOREY1_T2127424492_H
#define U3CCONTAINSTWICEU3EC__ANONSTOREY1_T2127424492_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Ext/<ContainsTwice>c__AnonStorey1
struct  U3CContainsTwiceU3Ec__AnonStorey1_t2127424492  : public RuntimeObject
{
public:
	// System.Int32 WebSocketSharp.Ext/<ContainsTwice>c__AnonStorey1::len
	int32_t ___len_0;
	// System.String[] WebSocketSharp.Ext/<ContainsTwice>c__AnonStorey1::values
	StringU5BU5D_t1642385972* ___values_1;
	// System.Func`2<System.Int32,System.Boolean> WebSocketSharp.Ext/<ContainsTwice>c__AnonStorey1::contains
	Func_2_t3377395111 * ___contains_2;

public:
	inline static int32_t get_offset_of_len_0() { return static_cast<int32_t>(offsetof(U3CContainsTwiceU3Ec__AnonStorey1_t2127424492, ___len_0)); }
	inline int32_t get_len_0() const { return ___len_0; }
	inline int32_t* get_address_of_len_0() { return &___len_0; }
	inline void set_len_0(int32_t value)
	{
		___len_0 = value;
	}

	inline static int32_t get_offset_of_values_1() { return static_cast<int32_t>(offsetof(U3CContainsTwiceU3Ec__AnonStorey1_t2127424492, ___values_1)); }
	inline StringU5BU5D_t1642385972* get_values_1() const { return ___values_1; }
	inline StringU5BU5D_t1642385972** get_address_of_values_1() { return &___values_1; }
	inline void set_values_1(StringU5BU5D_t1642385972* value)
	{
		___values_1 = value;
		Il2CppCodeGenWriteBarrier((&___values_1), value);
	}

	inline static int32_t get_offset_of_contains_2() { return static_cast<int32_t>(offsetof(U3CContainsTwiceU3Ec__AnonStorey1_t2127424492, ___contains_2)); }
	inline Func_2_t3377395111 * get_contains_2() const { return ___contains_2; }
	inline Func_2_t3377395111 ** get_address_of_contains_2() { return &___contains_2; }
	inline void set_contains_2(Func_2_t3377395111 * value)
	{
		___contains_2 = value;
		Il2CppCodeGenWriteBarrier((&___contains_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCONTAINSTWICEU3EC__ANONSTOREY1_T2127424492_H
#ifndef HTTPLISTENERASYNCRESULT_T3506939685_H
#define HTTPLISTENERASYNCRESULT_T3506939685_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.HttpListenerAsyncResult
struct  HttpListenerAsyncResult_t3506939685  : public RuntimeObject
{
public:
	// System.AsyncCallback WebSocketSharp.Net.HttpListenerAsyncResult::_callback
	AsyncCallback_t163412349 * ____callback_0;
	// System.Boolean WebSocketSharp.Net.HttpListenerAsyncResult::_completed
	bool ____completed_1;
	// WebSocketSharp.Net.HttpListenerContext WebSocketSharp.Net.HttpListenerAsyncResult::_context
	HttpListenerContext_t994708409 * ____context_2;
	// System.Boolean WebSocketSharp.Net.HttpListenerAsyncResult::_endCalled
	bool ____endCalled_3;
	// System.Exception WebSocketSharp.Net.HttpListenerAsyncResult::_exception
	Exception_t1927440687 * ____exception_4;
	// System.Boolean WebSocketSharp.Net.HttpListenerAsyncResult::_inGet
	bool ____inGet_5;
	// System.Object WebSocketSharp.Net.HttpListenerAsyncResult::_state
	RuntimeObject * ____state_6;
	// System.Object WebSocketSharp.Net.HttpListenerAsyncResult::_sync
	RuntimeObject * ____sync_7;
	// System.Boolean WebSocketSharp.Net.HttpListenerAsyncResult::_syncCompleted
	bool ____syncCompleted_8;
	// System.Threading.ManualResetEvent WebSocketSharp.Net.HttpListenerAsyncResult::_waitHandle
	ManualResetEvent_t926074657 * ____waitHandle_9;

public:
	inline static int32_t get_offset_of__callback_0() { return static_cast<int32_t>(offsetof(HttpListenerAsyncResult_t3506939685, ____callback_0)); }
	inline AsyncCallback_t163412349 * get__callback_0() const { return ____callback_0; }
	inline AsyncCallback_t163412349 ** get_address_of__callback_0() { return &____callback_0; }
	inline void set__callback_0(AsyncCallback_t163412349 * value)
	{
		____callback_0 = value;
		Il2CppCodeGenWriteBarrier((&____callback_0), value);
	}

	inline static int32_t get_offset_of__completed_1() { return static_cast<int32_t>(offsetof(HttpListenerAsyncResult_t3506939685, ____completed_1)); }
	inline bool get__completed_1() const { return ____completed_1; }
	inline bool* get_address_of__completed_1() { return &____completed_1; }
	inline void set__completed_1(bool value)
	{
		____completed_1 = value;
	}

	inline static int32_t get_offset_of__context_2() { return static_cast<int32_t>(offsetof(HttpListenerAsyncResult_t3506939685, ____context_2)); }
	inline HttpListenerContext_t994708409 * get__context_2() const { return ____context_2; }
	inline HttpListenerContext_t994708409 ** get_address_of__context_2() { return &____context_2; }
	inline void set__context_2(HttpListenerContext_t994708409 * value)
	{
		____context_2 = value;
		Il2CppCodeGenWriteBarrier((&____context_2), value);
	}

	inline static int32_t get_offset_of__endCalled_3() { return static_cast<int32_t>(offsetof(HttpListenerAsyncResult_t3506939685, ____endCalled_3)); }
	inline bool get__endCalled_3() const { return ____endCalled_3; }
	inline bool* get_address_of__endCalled_3() { return &____endCalled_3; }
	inline void set__endCalled_3(bool value)
	{
		____endCalled_3 = value;
	}

	inline static int32_t get_offset_of__exception_4() { return static_cast<int32_t>(offsetof(HttpListenerAsyncResult_t3506939685, ____exception_4)); }
	inline Exception_t1927440687 * get__exception_4() const { return ____exception_4; }
	inline Exception_t1927440687 ** get_address_of__exception_4() { return &____exception_4; }
	inline void set__exception_4(Exception_t1927440687 * value)
	{
		____exception_4 = value;
		Il2CppCodeGenWriteBarrier((&____exception_4), value);
	}

	inline static int32_t get_offset_of__inGet_5() { return static_cast<int32_t>(offsetof(HttpListenerAsyncResult_t3506939685, ____inGet_5)); }
	inline bool get__inGet_5() const { return ____inGet_5; }
	inline bool* get_address_of__inGet_5() { return &____inGet_5; }
	inline void set__inGet_5(bool value)
	{
		____inGet_5 = value;
	}

	inline static int32_t get_offset_of__state_6() { return static_cast<int32_t>(offsetof(HttpListenerAsyncResult_t3506939685, ____state_6)); }
	inline RuntimeObject * get__state_6() const { return ____state_6; }
	inline RuntimeObject ** get_address_of__state_6() { return &____state_6; }
	inline void set__state_6(RuntimeObject * value)
	{
		____state_6 = value;
		Il2CppCodeGenWriteBarrier((&____state_6), value);
	}

	inline static int32_t get_offset_of__sync_7() { return static_cast<int32_t>(offsetof(HttpListenerAsyncResult_t3506939685, ____sync_7)); }
	inline RuntimeObject * get__sync_7() const { return ____sync_7; }
	inline RuntimeObject ** get_address_of__sync_7() { return &____sync_7; }
	inline void set__sync_7(RuntimeObject * value)
	{
		____sync_7 = value;
		Il2CppCodeGenWriteBarrier((&____sync_7), value);
	}

	inline static int32_t get_offset_of__syncCompleted_8() { return static_cast<int32_t>(offsetof(HttpListenerAsyncResult_t3506939685, ____syncCompleted_8)); }
	inline bool get__syncCompleted_8() const { return ____syncCompleted_8; }
	inline bool* get_address_of__syncCompleted_8() { return &____syncCompleted_8; }
	inline void set__syncCompleted_8(bool value)
	{
		____syncCompleted_8 = value;
	}

	inline static int32_t get_offset_of__waitHandle_9() { return static_cast<int32_t>(offsetof(HttpListenerAsyncResult_t3506939685, ____waitHandle_9)); }
	inline ManualResetEvent_t926074657 * get__waitHandle_9() const { return ____waitHandle_9; }
	inline ManualResetEvent_t926074657 ** get_address_of__waitHandle_9() { return &____waitHandle_9; }
	inline void set__waitHandle_9(ManualResetEvent_t926074657 * value)
	{
		____waitHandle_9 = value;
		Il2CppCodeGenWriteBarrier((&____waitHandle_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPLISTENERASYNCRESULT_T3506939685_H
#ifndef HTTPLISTENERPREFIXCOLLECTION_T3269177542_H
#define HTTPLISTENERPREFIXCOLLECTION_T3269177542_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.HttpListenerPrefixCollection
struct  HttpListenerPrefixCollection_t3269177542  : public RuntimeObject
{
public:
	// WebSocketSharp.Net.HttpListener WebSocketSharp.Net.HttpListenerPrefixCollection::_listener
	HttpListener_t4179429670 * ____listener_0;
	// System.Collections.Generic.List`1<System.String> WebSocketSharp.Net.HttpListenerPrefixCollection::_prefixes
	List_1_t1398341365 * ____prefixes_1;

public:
	inline static int32_t get_offset_of__listener_0() { return static_cast<int32_t>(offsetof(HttpListenerPrefixCollection_t3269177542, ____listener_0)); }
	inline HttpListener_t4179429670 * get__listener_0() const { return ____listener_0; }
	inline HttpListener_t4179429670 ** get_address_of__listener_0() { return &____listener_0; }
	inline void set__listener_0(HttpListener_t4179429670 * value)
	{
		____listener_0 = value;
		Il2CppCodeGenWriteBarrier((&____listener_0), value);
	}

	inline static int32_t get_offset_of__prefixes_1() { return static_cast<int32_t>(offsetof(HttpListenerPrefixCollection_t3269177542, ____prefixes_1)); }
	inline List_1_t1398341365 * get__prefixes_1() const { return ____prefixes_1; }
	inline List_1_t1398341365 ** get_address_of__prefixes_1() { return &____prefixes_1; }
	inline void set__prefixes_1(List_1_t1398341365 * value)
	{
		____prefixes_1 = value;
		Il2CppCodeGenWriteBarrier((&____prefixes_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPLISTENERPREFIXCOLLECTION_T3269177542_H
#ifndef PROJECTIONPARAMS_T2712959773_H
#define PROJECTIONPARAMS_T2712959773_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Layers.ProjectionParams
struct  ProjectionParams_t2712959773  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROJECTIONPARAMS_T2712959773_H
#ifndef ENDPOINTLISTENER_T3937551933_H
#define ENDPOINTLISTENER_T3937551933_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.EndPointListener
struct  EndPointListener_t3937551933  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<WebSocketSharp.Net.HttpListenerPrefix> WebSocketSharp.Net.EndPointListener::_all
	List_1_t4193866914 * ____all_0;
	// System.Net.IPEndPoint WebSocketSharp.Net.EndPointListener::_endpoint
	IPEndPoint_t2615413766 * ____endpoint_2;
	// System.Collections.Generic.Dictionary`2<WebSocketSharp.Net.HttpListenerPrefix,WebSocketSharp.Net.HttpListener> WebSocketSharp.Net.EndPointListener::_prefixes
	Dictionary_2_t59835555 * ____prefixes_3;
	// System.Boolean WebSocketSharp.Net.EndPointListener::_secure
	bool ____secure_4;
	// System.Net.Sockets.Socket WebSocketSharp.Net.EndPointListener::_socket
	Socket_t3821512045 * ____socket_5;
	// WebSocketSharp.Net.ServerSslConfiguration WebSocketSharp.Net.EndPointListener::_sslConfig
	ServerSslConfiguration_t204724213 * ____sslConfig_6;
	// System.Collections.Generic.List`1<WebSocketSharp.Net.HttpListenerPrefix> WebSocketSharp.Net.EndPointListener::_unhandled
	List_1_t4193866914 * ____unhandled_7;
	// System.Collections.Generic.Dictionary`2<WebSocketSharp.Net.HttpConnection,WebSocketSharp.Net.HttpConnection> WebSocketSharp.Net.EndPointListener::_unregistered
	Dictionary_2_t3932468307 * ____unregistered_8;
	// System.Object WebSocketSharp.Net.EndPointListener::_unregisteredSync
	RuntimeObject * ____unregisteredSync_9;

public:
	inline static int32_t get_offset_of__all_0() { return static_cast<int32_t>(offsetof(EndPointListener_t3937551933, ____all_0)); }
	inline List_1_t4193866914 * get__all_0() const { return ____all_0; }
	inline List_1_t4193866914 ** get_address_of__all_0() { return &____all_0; }
	inline void set__all_0(List_1_t4193866914 * value)
	{
		____all_0 = value;
		Il2CppCodeGenWriteBarrier((&____all_0), value);
	}

	inline static int32_t get_offset_of__endpoint_2() { return static_cast<int32_t>(offsetof(EndPointListener_t3937551933, ____endpoint_2)); }
	inline IPEndPoint_t2615413766 * get__endpoint_2() const { return ____endpoint_2; }
	inline IPEndPoint_t2615413766 ** get_address_of__endpoint_2() { return &____endpoint_2; }
	inline void set__endpoint_2(IPEndPoint_t2615413766 * value)
	{
		____endpoint_2 = value;
		Il2CppCodeGenWriteBarrier((&____endpoint_2), value);
	}

	inline static int32_t get_offset_of__prefixes_3() { return static_cast<int32_t>(offsetof(EndPointListener_t3937551933, ____prefixes_3)); }
	inline Dictionary_2_t59835555 * get__prefixes_3() const { return ____prefixes_3; }
	inline Dictionary_2_t59835555 ** get_address_of__prefixes_3() { return &____prefixes_3; }
	inline void set__prefixes_3(Dictionary_2_t59835555 * value)
	{
		____prefixes_3 = value;
		Il2CppCodeGenWriteBarrier((&____prefixes_3), value);
	}

	inline static int32_t get_offset_of__secure_4() { return static_cast<int32_t>(offsetof(EndPointListener_t3937551933, ____secure_4)); }
	inline bool get__secure_4() const { return ____secure_4; }
	inline bool* get_address_of__secure_4() { return &____secure_4; }
	inline void set__secure_4(bool value)
	{
		____secure_4 = value;
	}

	inline static int32_t get_offset_of__socket_5() { return static_cast<int32_t>(offsetof(EndPointListener_t3937551933, ____socket_5)); }
	inline Socket_t3821512045 * get__socket_5() const { return ____socket_5; }
	inline Socket_t3821512045 ** get_address_of__socket_5() { return &____socket_5; }
	inline void set__socket_5(Socket_t3821512045 * value)
	{
		____socket_5 = value;
		Il2CppCodeGenWriteBarrier((&____socket_5), value);
	}

	inline static int32_t get_offset_of__sslConfig_6() { return static_cast<int32_t>(offsetof(EndPointListener_t3937551933, ____sslConfig_6)); }
	inline ServerSslConfiguration_t204724213 * get__sslConfig_6() const { return ____sslConfig_6; }
	inline ServerSslConfiguration_t204724213 ** get_address_of__sslConfig_6() { return &____sslConfig_6; }
	inline void set__sslConfig_6(ServerSslConfiguration_t204724213 * value)
	{
		____sslConfig_6 = value;
		Il2CppCodeGenWriteBarrier((&____sslConfig_6), value);
	}

	inline static int32_t get_offset_of__unhandled_7() { return static_cast<int32_t>(offsetof(EndPointListener_t3937551933, ____unhandled_7)); }
	inline List_1_t4193866914 * get__unhandled_7() const { return ____unhandled_7; }
	inline List_1_t4193866914 ** get_address_of__unhandled_7() { return &____unhandled_7; }
	inline void set__unhandled_7(List_1_t4193866914 * value)
	{
		____unhandled_7 = value;
		Il2CppCodeGenWriteBarrier((&____unhandled_7), value);
	}

	inline static int32_t get_offset_of__unregistered_8() { return static_cast<int32_t>(offsetof(EndPointListener_t3937551933, ____unregistered_8)); }
	inline Dictionary_2_t3932468307 * get__unregistered_8() const { return ____unregistered_8; }
	inline Dictionary_2_t3932468307 ** get_address_of__unregistered_8() { return &____unregistered_8; }
	inline void set__unregistered_8(Dictionary_2_t3932468307 * value)
	{
		____unregistered_8 = value;
		Il2CppCodeGenWriteBarrier((&____unregistered_8), value);
	}

	inline static int32_t get_offset_of__unregisteredSync_9() { return static_cast<int32_t>(offsetof(EndPointListener_t3937551933, ____unregisteredSync_9)); }
	inline RuntimeObject * get__unregisteredSync_9() const { return ____unregisteredSync_9; }
	inline RuntimeObject ** get_address_of__unregisteredSync_9() { return &____unregisteredSync_9; }
	inline void set__unregisteredSync_9(RuntimeObject * value)
	{
		____unregisteredSync_9 = value;
		Il2CppCodeGenWriteBarrier((&____unregisteredSync_9), value);
	}
};

struct EndPointListener_t3937551933_StaticFields
{
public:
	// System.String WebSocketSharp.Net.EndPointListener::_defaultCertFolderPath
	String_t* ____defaultCertFolderPath_1;
	// System.AsyncCallback WebSocketSharp.Net.EndPointListener::<>f__mg$cache0
	AsyncCallback_t163412349 * ___U3CU3Ef__mgU24cache0_10;
	// System.AsyncCallback WebSocketSharp.Net.EndPointListener::<>f__mg$cache1
	AsyncCallback_t163412349 * ___U3CU3Ef__mgU24cache1_11;

public:
	inline static int32_t get_offset_of__defaultCertFolderPath_1() { return static_cast<int32_t>(offsetof(EndPointListener_t3937551933_StaticFields, ____defaultCertFolderPath_1)); }
	inline String_t* get__defaultCertFolderPath_1() const { return ____defaultCertFolderPath_1; }
	inline String_t** get_address_of__defaultCertFolderPath_1() { return &____defaultCertFolderPath_1; }
	inline void set__defaultCertFolderPath_1(String_t* value)
	{
		____defaultCertFolderPath_1 = value;
		Il2CppCodeGenWriteBarrier((&____defaultCertFolderPath_1), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_10() { return static_cast<int32_t>(offsetof(EndPointListener_t3937551933_StaticFields, ___U3CU3Ef__mgU24cache0_10)); }
	inline AsyncCallback_t163412349 * get_U3CU3Ef__mgU24cache0_10() const { return ___U3CU3Ef__mgU24cache0_10; }
	inline AsyncCallback_t163412349 ** get_address_of_U3CU3Ef__mgU24cache0_10() { return &___U3CU3Ef__mgU24cache0_10; }
	inline void set_U3CU3Ef__mgU24cache0_10(AsyncCallback_t163412349 * value)
	{
		___U3CU3Ef__mgU24cache0_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_10), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_11() { return static_cast<int32_t>(offsetof(EndPointListener_t3937551933_StaticFields, ___U3CU3Ef__mgU24cache1_11)); }
	inline AsyncCallback_t163412349 * get_U3CU3Ef__mgU24cache1_11() const { return ___U3CU3Ef__mgU24cache1_11; }
	inline AsyncCallback_t163412349 ** get_address_of_U3CU3Ef__mgU24cache1_11() { return &___U3CU3Ef__mgU24cache1_11; }
	inline void set_U3CU3Ef__mgU24cache1_11(AsyncCallback_t163412349 * value)
	{
		___U3CU3Ef__mgU24cache1_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENDPOINTLISTENER_T3937551933_H
#ifndef ENDPOINTMANAGER_T2259888518_H
#define ENDPOINTMANAGER_T2259888518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.EndPointManager
struct  EndPointManager_t2259888518  : public RuntimeObject
{
public:

public:
};

struct EndPointManager_t2259888518_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Net.IPAddress,System.Collections.Generic.Dictionary`2<System.Int32,WebSocketSharp.Net.EndPointListener>> WebSocketSharp.Net.EndPointManager::_addressToEndpoints
	Dictionary_2_t3010034516 * ____addressToEndpoints_0;

public:
	inline static int32_t get_offset_of__addressToEndpoints_0() { return static_cast<int32_t>(offsetof(EndPointManager_t2259888518_StaticFields, ____addressToEndpoints_0)); }
	inline Dictionary_2_t3010034516 * get__addressToEndpoints_0() const { return ____addressToEndpoints_0; }
	inline Dictionary_2_t3010034516 ** get_address_of__addressToEndpoints_0() { return &____addressToEndpoints_0; }
	inline void set__addressToEndpoints_0(Dictionary_2_t3010034516 * value)
	{
		____addressToEndpoints_0 = value;
		Il2CppCodeGenWriteBarrier((&____addressToEndpoints_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENDPOINTMANAGER_T2259888518_H
#ifndef U3CCOMPLETEU3EC__ANONSTOREY0_T2877245460_H
#define U3CCOMPLETEU3EC__ANONSTOREY0_T2877245460_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.HttpListenerAsyncResult/<complete>c__AnonStorey0
struct  U3CcompleteU3Ec__AnonStorey0_t2877245460  : public RuntimeObject
{
public:
	// System.AsyncCallback WebSocketSharp.Net.HttpListenerAsyncResult/<complete>c__AnonStorey0::callback
	AsyncCallback_t163412349 * ___callback_0;
	// WebSocketSharp.Net.HttpListenerAsyncResult WebSocketSharp.Net.HttpListenerAsyncResult/<complete>c__AnonStorey0::asyncResult
	HttpListenerAsyncResult_t3506939685 * ___asyncResult_1;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CcompleteU3Ec__AnonStorey0_t2877245460, ___callback_0)); }
	inline AsyncCallback_t163412349 * get_callback_0() const { return ___callback_0; }
	inline AsyncCallback_t163412349 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(AsyncCallback_t163412349 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}

	inline static int32_t get_offset_of_asyncResult_1() { return static_cast<int32_t>(offsetof(U3CcompleteU3Ec__AnonStorey0_t2877245460, ___asyncResult_1)); }
	inline HttpListenerAsyncResult_t3506939685 * get_asyncResult_1() const { return ___asyncResult_1; }
	inline HttpListenerAsyncResult_t3506939685 ** get_address_of_asyncResult_1() { return &___asyncResult_1; }
	inline void set_asyncResult_1(HttpListenerAsyncResult_t3506939685 * value)
	{
		___asyncResult_1 = value;
		Il2CppCodeGenWriteBarrier((&___asyncResult_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOMPLETEU3EC__ANONSTOREY0_T2877245460_H
#ifndef EXCEPTION_T1927440687_H
#define EXCEPTION_T1927440687_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t1927440687  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t169632028* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t1927440687 * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t169632028* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t169632028** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t169632028* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___inner_exception_1)); }
	inline Exception_t1927440687 * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t1927440687 ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t1927440687 * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T1927440687_H
#ifndef HTTPLISTENERCONTEXT_T994708409_H
#define HTTPLISTENERCONTEXT_T994708409_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.HttpListenerContext
struct  HttpListenerContext_t994708409  : public RuntimeObject
{
public:
	// WebSocketSharp.Net.HttpConnection WebSocketSharp.Net.HttpListenerContext::_connection
	HttpConnection_t2649486862 * ____connection_0;
	// System.String WebSocketSharp.Net.HttpListenerContext::_error
	String_t* ____error_1;
	// System.Int32 WebSocketSharp.Net.HttpListenerContext::_errorStatus
	int32_t ____errorStatus_2;
	// WebSocketSharp.Net.HttpListener WebSocketSharp.Net.HttpListenerContext::_listener
	HttpListener_t4179429670 * ____listener_3;
	// WebSocketSharp.Net.HttpListenerRequest WebSocketSharp.Net.HttpListenerContext::_request
	HttpListenerRequest_t2316381291 * ____request_4;
	// WebSocketSharp.Net.HttpListenerResponse WebSocketSharp.Net.HttpListenerContext::_response
	HttpListenerResponse_t2223360553 * ____response_5;
	// System.Security.Principal.IPrincipal WebSocketSharp.Net.HttpListenerContext::_user
	RuntimeObject* ____user_6;
	// WebSocketSharp.Net.WebSockets.HttpListenerWebSocketContext WebSocketSharp.Net.HttpListenerContext::_websocketContext
	HttpListenerWebSocketContext_t1778866096 * ____websocketContext_7;

public:
	inline static int32_t get_offset_of__connection_0() { return static_cast<int32_t>(offsetof(HttpListenerContext_t994708409, ____connection_0)); }
	inline HttpConnection_t2649486862 * get__connection_0() const { return ____connection_0; }
	inline HttpConnection_t2649486862 ** get_address_of__connection_0() { return &____connection_0; }
	inline void set__connection_0(HttpConnection_t2649486862 * value)
	{
		____connection_0 = value;
		Il2CppCodeGenWriteBarrier((&____connection_0), value);
	}

	inline static int32_t get_offset_of__error_1() { return static_cast<int32_t>(offsetof(HttpListenerContext_t994708409, ____error_1)); }
	inline String_t* get__error_1() const { return ____error_1; }
	inline String_t** get_address_of__error_1() { return &____error_1; }
	inline void set__error_1(String_t* value)
	{
		____error_1 = value;
		Il2CppCodeGenWriteBarrier((&____error_1), value);
	}

	inline static int32_t get_offset_of__errorStatus_2() { return static_cast<int32_t>(offsetof(HttpListenerContext_t994708409, ____errorStatus_2)); }
	inline int32_t get__errorStatus_2() const { return ____errorStatus_2; }
	inline int32_t* get_address_of__errorStatus_2() { return &____errorStatus_2; }
	inline void set__errorStatus_2(int32_t value)
	{
		____errorStatus_2 = value;
	}

	inline static int32_t get_offset_of__listener_3() { return static_cast<int32_t>(offsetof(HttpListenerContext_t994708409, ____listener_3)); }
	inline HttpListener_t4179429670 * get__listener_3() const { return ____listener_3; }
	inline HttpListener_t4179429670 ** get_address_of__listener_3() { return &____listener_3; }
	inline void set__listener_3(HttpListener_t4179429670 * value)
	{
		____listener_3 = value;
		Il2CppCodeGenWriteBarrier((&____listener_3), value);
	}

	inline static int32_t get_offset_of__request_4() { return static_cast<int32_t>(offsetof(HttpListenerContext_t994708409, ____request_4)); }
	inline HttpListenerRequest_t2316381291 * get__request_4() const { return ____request_4; }
	inline HttpListenerRequest_t2316381291 ** get_address_of__request_4() { return &____request_4; }
	inline void set__request_4(HttpListenerRequest_t2316381291 * value)
	{
		____request_4 = value;
		Il2CppCodeGenWriteBarrier((&____request_4), value);
	}

	inline static int32_t get_offset_of__response_5() { return static_cast<int32_t>(offsetof(HttpListenerContext_t994708409, ____response_5)); }
	inline HttpListenerResponse_t2223360553 * get__response_5() const { return ____response_5; }
	inline HttpListenerResponse_t2223360553 ** get_address_of__response_5() { return &____response_5; }
	inline void set__response_5(HttpListenerResponse_t2223360553 * value)
	{
		____response_5 = value;
		Il2CppCodeGenWriteBarrier((&____response_5), value);
	}

	inline static int32_t get_offset_of__user_6() { return static_cast<int32_t>(offsetof(HttpListenerContext_t994708409, ____user_6)); }
	inline RuntimeObject* get__user_6() const { return ____user_6; }
	inline RuntimeObject** get_address_of__user_6() { return &____user_6; }
	inline void set__user_6(RuntimeObject* value)
	{
		____user_6 = value;
		Il2CppCodeGenWriteBarrier((&____user_6), value);
	}

	inline static int32_t get_offset_of__websocketContext_7() { return static_cast<int32_t>(offsetof(HttpListenerContext_t994708409, ____websocketContext_7)); }
	inline HttpListenerWebSocketContext_t1778866096 * get__websocketContext_7() const { return ____websocketContext_7; }
	inline HttpListenerWebSocketContext_t1778866096 ** get_address_of__websocketContext_7() { return &____websocketContext_7; }
	inline void set__websocketContext_7(HttpListenerWebSocketContext_t1778866096 * value)
	{
		____websocketContext_7 = value;
		Il2CppCodeGenWriteBarrier((&____websocketContext_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPLISTENERCONTEXT_T994708409_H
#ifndef STREAM_T3255436806_H
#define STREAM_T3255436806_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream
struct  Stream_t3255436806  : public RuntimeObject
{
public:

public:
};

struct Stream_t3255436806_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_t3255436806 * ___Null_0;

public:
	inline static int32_t get_offset_of_Null_0() { return static_cast<int32_t>(offsetof(Stream_t3255436806_StaticFields, ___Null_0)); }
	inline Stream_t3255436806 * get_Null_0() const { return ___Null_0; }
	inline Stream_t3255436806 ** get_address_of_Null_0() { return &___Null_0; }
	inline void set_Null_0(Stream_t3255436806 * value)
	{
		___Null_0 = value;
		Il2CppCodeGenWriteBarrier((&___Null_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAM_T3255436806_H
#ifndef HTTPLISTENERPREFIX_T529778486_H
#define HTTPLISTENERPREFIX_T529778486_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.HttpListenerPrefix
struct  HttpListenerPrefix_t529778486  : public RuntimeObject
{
public:
	// System.Net.IPAddress[] WebSocketSharp.Net.HttpListenerPrefix::_addresses
	IPAddressU5BU5D_t4087230954* ____addresses_0;
	// System.String WebSocketSharp.Net.HttpListenerPrefix::_host
	String_t* ____host_1;
	// WebSocketSharp.Net.HttpListener WebSocketSharp.Net.HttpListenerPrefix::_listener
	HttpListener_t4179429670 * ____listener_2;
	// System.String WebSocketSharp.Net.HttpListenerPrefix::_original
	String_t* ____original_3;
	// System.String WebSocketSharp.Net.HttpListenerPrefix::_path
	String_t* ____path_4;
	// System.UInt16 WebSocketSharp.Net.HttpListenerPrefix::_port
	uint16_t ____port_5;
	// System.Boolean WebSocketSharp.Net.HttpListenerPrefix::_secure
	bool ____secure_6;

public:
	inline static int32_t get_offset_of__addresses_0() { return static_cast<int32_t>(offsetof(HttpListenerPrefix_t529778486, ____addresses_0)); }
	inline IPAddressU5BU5D_t4087230954* get__addresses_0() const { return ____addresses_0; }
	inline IPAddressU5BU5D_t4087230954** get_address_of__addresses_0() { return &____addresses_0; }
	inline void set__addresses_0(IPAddressU5BU5D_t4087230954* value)
	{
		____addresses_0 = value;
		Il2CppCodeGenWriteBarrier((&____addresses_0), value);
	}

	inline static int32_t get_offset_of__host_1() { return static_cast<int32_t>(offsetof(HttpListenerPrefix_t529778486, ____host_1)); }
	inline String_t* get__host_1() const { return ____host_1; }
	inline String_t** get_address_of__host_1() { return &____host_1; }
	inline void set__host_1(String_t* value)
	{
		____host_1 = value;
		Il2CppCodeGenWriteBarrier((&____host_1), value);
	}

	inline static int32_t get_offset_of__listener_2() { return static_cast<int32_t>(offsetof(HttpListenerPrefix_t529778486, ____listener_2)); }
	inline HttpListener_t4179429670 * get__listener_2() const { return ____listener_2; }
	inline HttpListener_t4179429670 ** get_address_of__listener_2() { return &____listener_2; }
	inline void set__listener_2(HttpListener_t4179429670 * value)
	{
		____listener_2 = value;
		Il2CppCodeGenWriteBarrier((&____listener_2), value);
	}

	inline static int32_t get_offset_of__original_3() { return static_cast<int32_t>(offsetof(HttpListenerPrefix_t529778486, ____original_3)); }
	inline String_t* get__original_3() const { return ____original_3; }
	inline String_t** get_address_of__original_3() { return &____original_3; }
	inline void set__original_3(String_t* value)
	{
		____original_3 = value;
		Il2CppCodeGenWriteBarrier((&____original_3), value);
	}

	inline static int32_t get_offset_of__path_4() { return static_cast<int32_t>(offsetof(HttpListenerPrefix_t529778486, ____path_4)); }
	inline String_t* get__path_4() const { return ____path_4; }
	inline String_t** get_address_of__path_4() { return &____path_4; }
	inline void set__path_4(String_t* value)
	{
		____path_4 = value;
		Il2CppCodeGenWriteBarrier((&____path_4), value);
	}

	inline static int32_t get_offset_of__port_5() { return static_cast<int32_t>(offsetof(HttpListenerPrefix_t529778486, ____port_5)); }
	inline uint16_t get__port_5() const { return ____port_5; }
	inline uint16_t* get_address_of__port_5() { return &____port_5; }
	inline void set__port_5(uint16_t value)
	{
		____port_5 = value;
	}

	inline static int32_t get_offset_of__secure_6() { return static_cast<int32_t>(offsetof(HttpListenerPrefix_t529778486, ____secure_6)); }
	inline bool get__secure_6() const { return ____secure_6; }
	inline bool* get_address_of__secure_6() { return &____secure_6; }
	inline void set__secure_6(bool value)
	{
		____secure_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPLISTENERPREFIX_T529778486_H
#ifndef COOKIECOLLECTION_T4248997468_H
#define COOKIECOLLECTION_T4248997468_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.CookieCollection
struct  CookieCollection_t4248997468  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<WebSocketSharp.Net.Cookie> WebSocketSharp.Net.CookieCollection::_list
	List_1_t1195309592 * ____list_0;
	// System.Object WebSocketSharp.Net.CookieCollection::_sync
	RuntimeObject * ____sync_1;

public:
	inline static int32_t get_offset_of__list_0() { return static_cast<int32_t>(offsetof(CookieCollection_t4248997468, ____list_0)); }
	inline List_1_t1195309592 * get__list_0() const { return ____list_0; }
	inline List_1_t1195309592 ** get_address_of__list_0() { return &____list_0; }
	inline void set__list_0(List_1_t1195309592 * value)
	{
		____list_0 = value;
		Il2CppCodeGenWriteBarrier((&____list_0), value);
	}

	inline static int32_t get_offset_of__sync_1() { return static_cast<int32_t>(offsetof(CookieCollection_t4248997468, ____sync_1)); }
	inline RuntimeObject * get__sync_1() const { return ____sync_1; }
	inline RuntimeObject ** get_address_of__sync_1() { return &____sync_1; }
	inline void set__sync_1(RuntimeObject * value)
	{
		____sync_1 = value;
		Il2CppCodeGenWriteBarrier((&____sync_1), value);
	}
};

struct CookieCollection_t4248997468_StaticFields
{
public:
	// System.Comparison`1<WebSocketSharp.Net.Cookie> WebSocketSharp.Net.CookieCollection::<>f__mg$cache0
	Comparison_1_t3087927311 * ___U3CU3Ef__mgU24cache0_2;
	// System.Comparison`1<WebSocketSharp.Net.Cookie> WebSocketSharp.Net.CookieCollection::<>f__mg$cache1
	Comparison_1_t3087927311 * ___U3CU3Ef__mgU24cache1_3;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_2() { return static_cast<int32_t>(offsetof(CookieCollection_t4248997468_StaticFields, ___U3CU3Ef__mgU24cache0_2)); }
	inline Comparison_1_t3087927311 * get_U3CU3Ef__mgU24cache0_2() const { return ___U3CU3Ef__mgU24cache0_2; }
	inline Comparison_1_t3087927311 ** get_address_of_U3CU3Ef__mgU24cache0_2() { return &___U3CU3Ef__mgU24cache0_2; }
	inline void set_U3CU3Ef__mgU24cache0_2(Comparison_1_t3087927311 * value)
	{
		___U3CU3Ef__mgU24cache0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_3() { return static_cast<int32_t>(offsetof(CookieCollection_t4248997468_StaticFields, ___U3CU3Ef__mgU24cache1_3)); }
	inline Comparison_1_t3087927311 * get_U3CU3Ef__mgU24cache1_3() const { return ___U3CU3Ef__mgU24cache1_3; }
	inline Comparison_1_t3087927311 ** get_address_of_U3CU3Ef__mgU24cache1_3() { return &___U3CU3Ef__mgU24cache1_3; }
	inline void set_U3CU3Ef__mgU24cache1_3(Comparison_1_t3087927311 * value)
	{
		___U3CU3Ef__mgU24cache1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COOKIECOLLECTION_T4248997468_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef CHUNK_T2303927151_H
#define CHUNK_T2303927151_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.Chunk
struct  Chunk_t2303927151  : public RuntimeObject
{
public:
	// System.Byte[] WebSocketSharp.Net.Chunk::_data
	ByteU5BU5D_t3397334013* ____data_0;
	// System.Int32 WebSocketSharp.Net.Chunk::_offset
	int32_t ____offset_1;

public:
	inline static int32_t get_offset_of__data_0() { return static_cast<int32_t>(offsetof(Chunk_t2303927151, ____data_0)); }
	inline ByteU5BU5D_t3397334013* get__data_0() const { return ____data_0; }
	inline ByteU5BU5D_t3397334013** get_address_of__data_0() { return &____data_0; }
	inline void set__data_0(ByteU5BU5D_t3397334013* value)
	{
		____data_0 = value;
		Il2CppCodeGenWriteBarrier((&____data_0), value);
	}

	inline static int32_t get_offset_of__offset_1() { return static_cast<int32_t>(offsetof(Chunk_t2303927151, ____offset_1)); }
	inline int32_t get__offset_1() const { return ____offset_1; }
	inline int32_t* get_address_of__offset_1() { return &____offset_1; }
	inline void set__offset_1(int32_t value)
	{
		____offset_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHUNK_T2303927151_H
#ifndef TAGS_T1265380163_H
#define TAGS_T1265380163_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Tags
struct  Tags_t1265380163  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.String> TouchScript.Tags::tagList
	List_1_t1398341365 * ___tagList_5;
	// System.Collections.Generic.HashSet`1<System.String> TouchScript.Tags::tags
	HashSet_1_t362681087 * ___tags_6;
	// System.String TouchScript.Tags::stringValue
	String_t* ___stringValue_7;

public:
	inline static int32_t get_offset_of_tagList_5() { return static_cast<int32_t>(offsetof(Tags_t1265380163, ___tagList_5)); }
	inline List_1_t1398341365 * get_tagList_5() const { return ___tagList_5; }
	inline List_1_t1398341365 ** get_address_of_tagList_5() { return &___tagList_5; }
	inline void set_tagList_5(List_1_t1398341365 * value)
	{
		___tagList_5 = value;
		Il2CppCodeGenWriteBarrier((&___tagList_5), value);
	}

	inline static int32_t get_offset_of_tags_6() { return static_cast<int32_t>(offsetof(Tags_t1265380163, ___tags_6)); }
	inline HashSet_1_t362681087 * get_tags_6() const { return ___tags_6; }
	inline HashSet_1_t362681087 ** get_address_of_tags_6() { return &___tags_6; }
	inline void set_tags_6(HashSet_1_t362681087 * value)
	{
		___tags_6 = value;
		Il2CppCodeGenWriteBarrier((&___tags_6), value);
	}

	inline static int32_t get_offset_of_stringValue_7() { return static_cast<int32_t>(offsetof(Tags_t1265380163, ___stringValue_7)); }
	inline String_t* get_stringValue_7() const { return ___stringValue_7; }
	inline String_t** get_address_of_stringValue_7() { return &___stringValue_7; }
	inline void set_stringValue_7(String_t* value)
	{
		___stringValue_7 = value;
		Il2CppCodeGenWriteBarrier((&___stringValue_7), value);
	}
};

struct Tags_t1265380163_StaticFields
{
public:
	// TouchScript.Tags TouchScript.Tags::EMPTY
	Tags_t1265380163 * ___EMPTY_4;

public:
	inline static int32_t get_offset_of_EMPTY_4() { return static_cast<int32_t>(offsetof(Tags_t1265380163_StaticFields, ___EMPTY_4)); }
	inline Tags_t1265380163 * get_EMPTY_4() const { return ___EMPTY_4; }
	inline Tags_t1265380163 ** get_address_of_EMPTY_4() { return &___EMPTY_4; }
	inline void set_EMPTY_4(Tags_t1265380163 * value)
	{
		___EMPTY_4 = value;
		Il2CppCodeGenWriteBarrier((&___EMPTY_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAGS_T1265380163_H
#ifndef U3CLATEAWAKEU3EC__ITERATOR0_T181594678_H
#define U3CLATEAWAKEU3EC__ITERATOR0_T181594678_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Layers.UILayer/<lateAwake>c__Iterator0
struct  U3ClateAwakeU3Ec__Iterator0_t181594678  : public RuntimeObject
{
public:
	// TouchScript.Layers.UILayer TouchScript.Layers.UILayer/<lateAwake>c__Iterator0::$this
	UILayer_t314035379 * ___U24this_0;
	// System.Object TouchScript.Layers.UILayer/<lateAwake>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean TouchScript.Layers.UILayer/<lateAwake>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 TouchScript.Layers.UILayer/<lateAwake>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3ClateAwakeU3Ec__Iterator0_t181594678, ___U24this_0)); }
	inline UILayer_t314035379 * get_U24this_0() const { return ___U24this_0; }
	inline UILayer_t314035379 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(UILayer_t314035379 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3ClateAwakeU3Ec__Iterator0_t181594678, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3ClateAwakeU3Ec__Iterator0_t181594678, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3ClateAwakeU3Ec__Iterator0_t181594678, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLATEAWAKEU3EC__ITERATOR0_T181594678_H
#ifndef U3CCLOSEU3EC__ANONSTOREY1_T1940037592_H
#define U3CCLOSEU3EC__ANONSTOREY1_T1940037592_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.HttpListenerResponse/<Close>c__AnonStorey1
struct  U3CCloseU3Ec__AnonStorey1_t1940037592  : public RuntimeObject
{
public:
	// System.IO.Stream WebSocketSharp.Net.HttpListenerResponse/<Close>c__AnonStorey1::output
	Stream_t3255436806 * ___output_0;
	// WebSocketSharp.Net.HttpListenerResponse WebSocketSharp.Net.HttpListenerResponse/<Close>c__AnonStorey1::$this
	HttpListenerResponse_t2223360553 * ___U24this_1;

public:
	inline static int32_t get_offset_of_output_0() { return static_cast<int32_t>(offsetof(U3CCloseU3Ec__AnonStorey1_t1940037592, ___output_0)); }
	inline Stream_t3255436806 * get_output_0() const { return ___output_0; }
	inline Stream_t3255436806 ** get_address_of_output_0() { return &___output_0; }
	inline void set_output_0(Stream_t3255436806 * value)
	{
		___output_0 = value;
		Il2CppCodeGenWriteBarrier((&___output_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CCloseU3Ec__AnonStorey1_t1940037592, ___U24this_1)); }
	inline HttpListenerResponse_t2223360553 * get_U24this_1() const { return ___U24this_1; }
	inline HttpListenerResponse_t2223360553 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(HttpListenerResponse_t2223360553 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCLOSEU3EC__ANONSTOREY1_T1940037592_H
#ifndef ATTRIBUTE_T542643598_H
#define ATTRIBUTE_T542643598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t542643598  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T542643598_H
#ifndef HTTPLISTENERRESPONSE_T2223360553_H
#define HTTPLISTENERRESPONSE_T2223360553_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.HttpListenerResponse
struct  HttpListenerResponse_t2223360553  : public RuntimeObject
{
public:
	// System.Boolean WebSocketSharp.Net.HttpListenerResponse::_closeConnection
	bool ____closeConnection_0;
	// System.Text.Encoding WebSocketSharp.Net.HttpListenerResponse::_contentEncoding
	Encoding_t663144255 * ____contentEncoding_1;
	// System.Int64 WebSocketSharp.Net.HttpListenerResponse::_contentLength
	int64_t ____contentLength_2;
	// System.String WebSocketSharp.Net.HttpListenerResponse::_contentType
	String_t* ____contentType_3;
	// WebSocketSharp.Net.HttpListenerContext WebSocketSharp.Net.HttpListenerResponse::_context
	HttpListenerContext_t994708409 * ____context_4;
	// WebSocketSharp.Net.CookieCollection WebSocketSharp.Net.HttpListenerResponse::_cookies
	CookieCollection_t4248997468 * ____cookies_5;
	// System.Boolean WebSocketSharp.Net.HttpListenerResponse::_disposed
	bool ____disposed_6;
	// WebSocketSharp.Net.WebHeaderCollection WebSocketSharp.Net.HttpListenerResponse::_headers
	WebHeaderCollection_t1932982249 * ____headers_7;
	// System.Boolean WebSocketSharp.Net.HttpListenerResponse::_headersSent
	bool ____headersSent_8;
	// System.Boolean WebSocketSharp.Net.HttpListenerResponse::_keepAlive
	bool ____keepAlive_9;
	// System.String WebSocketSharp.Net.HttpListenerResponse::_location
	String_t* ____location_10;
	// WebSocketSharp.Net.ResponseStream WebSocketSharp.Net.HttpListenerResponse::_outputStream
	ResponseStream_t3200689523 * ____outputStream_11;
	// System.Boolean WebSocketSharp.Net.HttpListenerResponse::_sendChunked
	bool ____sendChunked_12;
	// System.Int32 WebSocketSharp.Net.HttpListenerResponse::_statusCode
	int32_t ____statusCode_13;
	// System.String WebSocketSharp.Net.HttpListenerResponse::_statusDescription
	String_t* ____statusDescription_14;
	// System.Version WebSocketSharp.Net.HttpListenerResponse::_version
	Version_t1755874712 * ____version_15;

public:
	inline static int32_t get_offset_of__closeConnection_0() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t2223360553, ____closeConnection_0)); }
	inline bool get__closeConnection_0() const { return ____closeConnection_0; }
	inline bool* get_address_of__closeConnection_0() { return &____closeConnection_0; }
	inline void set__closeConnection_0(bool value)
	{
		____closeConnection_0 = value;
	}

	inline static int32_t get_offset_of__contentEncoding_1() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t2223360553, ____contentEncoding_1)); }
	inline Encoding_t663144255 * get__contentEncoding_1() const { return ____contentEncoding_1; }
	inline Encoding_t663144255 ** get_address_of__contentEncoding_1() { return &____contentEncoding_1; }
	inline void set__contentEncoding_1(Encoding_t663144255 * value)
	{
		____contentEncoding_1 = value;
		Il2CppCodeGenWriteBarrier((&____contentEncoding_1), value);
	}

	inline static int32_t get_offset_of__contentLength_2() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t2223360553, ____contentLength_2)); }
	inline int64_t get__contentLength_2() const { return ____contentLength_2; }
	inline int64_t* get_address_of__contentLength_2() { return &____contentLength_2; }
	inline void set__contentLength_2(int64_t value)
	{
		____contentLength_2 = value;
	}

	inline static int32_t get_offset_of__contentType_3() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t2223360553, ____contentType_3)); }
	inline String_t* get__contentType_3() const { return ____contentType_3; }
	inline String_t** get_address_of__contentType_3() { return &____contentType_3; }
	inline void set__contentType_3(String_t* value)
	{
		____contentType_3 = value;
		Il2CppCodeGenWriteBarrier((&____contentType_3), value);
	}

	inline static int32_t get_offset_of__context_4() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t2223360553, ____context_4)); }
	inline HttpListenerContext_t994708409 * get__context_4() const { return ____context_4; }
	inline HttpListenerContext_t994708409 ** get_address_of__context_4() { return &____context_4; }
	inline void set__context_4(HttpListenerContext_t994708409 * value)
	{
		____context_4 = value;
		Il2CppCodeGenWriteBarrier((&____context_4), value);
	}

	inline static int32_t get_offset_of__cookies_5() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t2223360553, ____cookies_5)); }
	inline CookieCollection_t4248997468 * get__cookies_5() const { return ____cookies_5; }
	inline CookieCollection_t4248997468 ** get_address_of__cookies_5() { return &____cookies_5; }
	inline void set__cookies_5(CookieCollection_t4248997468 * value)
	{
		____cookies_5 = value;
		Il2CppCodeGenWriteBarrier((&____cookies_5), value);
	}

	inline static int32_t get_offset_of__disposed_6() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t2223360553, ____disposed_6)); }
	inline bool get__disposed_6() const { return ____disposed_6; }
	inline bool* get_address_of__disposed_6() { return &____disposed_6; }
	inline void set__disposed_6(bool value)
	{
		____disposed_6 = value;
	}

	inline static int32_t get_offset_of__headers_7() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t2223360553, ____headers_7)); }
	inline WebHeaderCollection_t1932982249 * get__headers_7() const { return ____headers_7; }
	inline WebHeaderCollection_t1932982249 ** get_address_of__headers_7() { return &____headers_7; }
	inline void set__headers_7(WebHeaderCollection_t1932982249 * value)
	{
		____headers_7 = value;
		Il2CppCodeGenWriteBarrier((&____headers_7), value);
	}

	inline static int32_t get_offset_of__headersSent_8() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t2223360553, ____headersSent_8)); }
	inline bool get__headersSent_8() const { return ____headersSent_8; }
	inline bool* get_address_of__headersSent_8() { return &____headersSent_8; }
	inline void set__headersSent_8(bool value)
	{
		____headersSent_8 = value;
	}

	inline static int32_t get_offset_of__keepAlive_9() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t2223360553, ____keepAlive_9)); }
	inline bool get__keepAlive_9() const { return ____keepAlive_9; }
	inline bool* get_address_of__keepAlive_9() { return &____keepAlive_9; }
	inline void set__keepAlive_9(bool value)
	{
		____keepAlive_9 = value;
	}

	inline static int32_t get_offset_of__location_10() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t2223360553, ____location_10)); }
	inline String_t* get__location_10() const { return ____location_10; }
	inline String_t** get_address_of__location_10() { return &____location_10; }
	inline void set__location_10(String_t* value)
	{
		____location_10 = value;
		Il2CppCodeGenWriteBarrier((&____location_10), value);
	}

	inline static int32_t get_offset_of__outputStream_11() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t2223360553, ____outputStream_11)); }
	inline ResponseStream_t3200689523 * get__outputStream_11() const { return ____outputStream_11; }
	inline ResponseStream_t3200689523 ** get_address_of__outputStream_11() { return &____outputStream_11; }
	inline void set__outputStream_11(ResponseStream_t3200689523 * value)
	{
		____outputStream_11 = value;
		Il2CppCodeGenWriteBarrier((&____outputStream_11), value);
	}

	inline static int32_t get_offset_of__sendChunked_12() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t2223360553, ____sendChunked_12)); }
	inline bool get__sendChunked_12() const { return ____sendChunked_12; }
	inline bool* get_address_of__sendChunked_12() { return &____sendChunked_12; }
	inline void set__sendChunked_12(bool value)
	{
		____sendChunked_12 = value;
	}

	inline static int32_t get_offset_of__statusCode_13() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t2223360553, ____statusCode_13)); }
	inline int32_t get__statusCode_13() const { return ____statusCode_13; }
	inline int32_t* get_address_of__statusCode_13() { return &____statusCode_13; }
	inline void set__statusCode_13(int32_t value)
	{
		____statusCode_13 = value;
	}

	inline static int32_t get_offset_of__statusDescription_14() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t2223360553, ____statusDescription_14)); }
	inline String_t* get__statusDescription_14() const { return ____statusDescription_14; }
	inline String_t** get_address_of__statusDescription_14() { return &____statusDescription_14; }
	inline void set__statusDescription_14(String_t* value)
	{
		____statusDescription_14 = value;
		Il2CppCodeGenWriteBarrier((&____statusDescription_14), value);
	}

	inline static int32_t get_offset_of__version_15() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t2223360553, ____version_15)); }
	inline Version_t1755874712 * get__version_15() const { return ____version_15; }
	inline Version_t1755874712 ** get_address_of__version_15() { return &____version_15; }
	inline void set__version_15(Version_t1755874712 * value)
	{
		____version_15 = value;
		Il2CppCodeGenWriteBarrier((&____version_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPLISTENERRESPONSE_T2223360553_H
#ifndef U3CFINDCOOKIEU3EC__ITERATOR0_T531581468_H
#define U3CFINDCOOKIEU3EC__ITERATOR0_T531581468_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.HttpListenerResponse/<findCookie>c__Iterator0
struct  U3CfindCookieU3Ec__Iterator0_t531581468  : public RuntimeObject
{
public:
	// WebSocketSharp.Net.Cookie WebSocketSharp.Net.HttpListenerResponse/<findCookie>c__Iterator0::cookie
	Cookie_t1826188460 * ___cookie_0;
	// System.String WebSocketSharp.Net.HttpListenerResponse/<findCookie>c__Iterator0::<name>__0
	String_t* ___U3CnameU3E__0_1;
	// System.String WebSocketSharp.Net.HttpListenerResponse/<findCookie>c__Iterator0::<domain>__0
	String_t* ___U3CdomainU3E__0_2;
	// System.String WebSocketSharp.Net.HttpListenerResponse/<findCookie>c__Iterator0::<path>__0
	String_t* ___U3CpathU3E__0_3;
	// System.Collections.IEnumerator WebSocketSharp.Net.HttpListenerResponse/<findCookie>c__Iterator0::$locvar0
	RuntimeObject* ___U24locvar0_4;
	// WebSocketSharp.Net.Cookie WebSocketSharp.Net.HttpListenerResponse/<findCookie>c__Iterator0::<c>__1
	Cookie_t1826188460 * ___U3CcU3E__1_5;
	// System.IDisposable WebSocketSharp.Net.HttpListenerResponse/<findCookie>c__Iterator0::$locvar1
	RuntimeObject* ___U24locvar1_6;
	// WebSocketSharp.Net.HttpListenerResponse WebSocketSharp.Net.HttpListenerResponse/<findCookie>c__Iterator0::$this
	HttpListenerResponse_t2223360553 * ___U24this_7;
	// WebSocketSharp.Net.Cookie WebSocketSharp.Net.HttpListenerResponse/<findCookie>c__Iterator0::$current
	Cookie_t1826188460 * ___U24current_8;
	// System.Boolean WebSocketSharp.Net.HttpListenerResponse/<findCookie>c__Iterator0::$disposing
	bool ___U24disposing_9;
	// System.Int32 WebSocketSharp.Net.HttpListenerResponse/<findCookie>c__Iterator0::$PC
	int32_t ___U24PC_10;

public:
	inline static int32_t get_offset_of_cookie_0() { return static_cast<int32_t>(offsetof(U3CfindCookieU3Ec__Iterator0_t531581468, ___cookie_0)); }
	inline Cookie_t1826188460 * get_cookie_0() const { return ___cookie_0; }
	inline Cookie_t1826188460 ** get_address_of_cookie_0() { return &___cookie_0; }
	inline void set_cookie_0(Cookie_t1826188460 * value)
	{
		___cookie_0 = value;
		Il2CppCodeGenWriteBarrier((&___cookie_0), value);
	}

	inline static int32_t get_offset_of_U3CnameU3E__0_1() { return static_cast<int32_t>(offsetof(U3CfindCookieU3Ec__Iterator0_t531581468, ___U3CnameU3E__0_1)); }
	inline String_t* get_U3CnameU3E__0_1() const { return ___U3CnameU3E__0_1; }
	inline String_t** get_address_of_U3CnameU3E__0_1() { return &___U3CnameU3E__0_1; }
	inline void set_U3CnameU3E__0_1(String_t* value)
	{
		___U3CnameU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CdomainU3E__0_2() { return static_cast<int32_t>(offsetof(U3CfindCookieU3Ec__Iterator0_t531581468, ___U3CdomainU3E__0_2)); }
	inline String_t* get_U3CdomainU3E__0_2() const { return ___U3CdomainU3E__0_2; }
	inline String_t** get_address_of_U3CdomainU3E__0_2() { return &___U3CdomainU3E__0_2; }
	inline void set_U3CdomainU3E__0_2(String_t* value)
	{
		___U3CdomainU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdomainU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CpathU3E__0_3() { return static_cast<int32_t>(offsetof(U3CfindCookieU3Ec__Iterator0_t531581468, ___U3CpathU3E__0_3)); }
	inline String_t* get_U3CpathU3E__0_3() const { return ___U3CpathU3E__0_3; }
	inline String_t** get_address_of_U3CpathU3E__0_3() { return &___U3CpathU3E__0_3; }
	inline void set_U3CpathU3E__0_3(String_t* value)
	{
		___U3CpathU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpathU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U24locvar0_4() { return static_cast<int32_t>(offsetof(U3CfindCookieU3Ec__Iterator0_t531581468, ___U24locvar0_4)); }
	inline RuntimeObject* get_U24locvar0_4() const { return ___U24locvar0_4; }
	inline RuntimeObject** get_address_of_U24locvar0_4() { return &___U24locvar0_4; }
	inline void set_U24locvar0_4(RuntimeObject* value)
	{
		___U24locvar0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_4), value);
	}

	inline static int32_t get_offset_of_U3CcU3E__1_5() { return static_cast<int32_t>(offsetof(U3CfindCookieU3Ec__Iterator0_t531581468, ___U3CcU3E__1_5)); }
	inline Cookie_t1826188460 * get_U3CcU3E__1_5() const { return ___U3CcU3E__1_5; }
	inline Cookie_t1826188460 ** get_address_of_U3CcU3E__1_5() { return &___U3CcU3E__1_5; }
	inline void set_U3CcU3E__1_5(Cookie_t1826188460 * value)
	{
		___U3CcU3E__1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcU3E__1_5), value);
	}

	inline static int32_t get_offset_of_U24locvar1_6() { return static_cast<int32_t>(offsetof(U3CfindCookieU3Ec__Iterator0_t531581468, ___U24locvar1_6)); }
	inline RuntimeObject* get_U24locvar1_6() const { return ___U24locvar1_6; }
	inline RuntimeObject** get_address_of_U24locvar1_6() { return &___U24locvar1_6; }
	inline void set_U24locvar1_6(RuntimeObject* value)
	{
		___U24locvar1_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar1_6), value);
	}

	inline static int32_t get_offset_of_U24this_7() { return static_cast<int32_t>(offsetof(U3CfindCookieU3Ec__Iterator0_t531581468, ___U24this_7)); }
	inline HttpListenerResponse_t2223360553 * get_U24this_7() const { return ___U24this_7; }
	inline HttpListenerResponse_t2223360553 ** get_address_of_U24this_7() { return &___U24this_7; }
	inline void set_U24this_7(HttpListenerResponse_t2223360553 * value)
	{
		___U24this_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_7), value);
	}

	inline static int32_t get_offset_of_U24current_8() { return static_cast<int32_t>(offsetof(U3CfindCookieU3Ec__Iterator0_t531581468, ___U24current_8)); }
	inline Cookie_t1826188460 * get_U24current_8() const { return ___U24current_8; }
	inline Cookie_t1826188460 ** get_address_of_U24current_8() { return &___U24current_8; }
	inline void set_U24current_8(Cookie_t1826188460 * value)
	{
		___U24current_8 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_8), value);
	}

	inline static int32_t get_offset_of_U24disposing_9() { return static_cast<int32_t>(offsetof(U3CfindCookieU3Ec__Iterator0_t531581468, ___U24disposing_9)); }
	inline bool get_U24disposing_9() const { return ___U24disposing_9; }
	inline bool* get_address_of_U24disposing_9() { return &___U24disposing_9; }
	inline void set_U24disposing_9(bool value)
	{
		___U24disposing_9 = value;
	}

	inline static int32_t get_offset_of_U24PC_10() { return static_cast<int32_t>(offsetof(U3CfindCookieU3Ec__Iterator0_t531581468, ___U24PC_10)); }
	inline int32_t get_U24PC_10() const { return ___U24PC_10; }
	inline int32_t* get_address_of_U24PC_10() { return &___U24PC_10; }
	inline void set_U24PC_10(int32_t value)
	{
		___U24PC_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFINDCOOKIEU3EC__ITERATOR0_T531581468_H
#ifndef U3CLATEAWAKEU3EC__ITERATOR0_T3289977563_H
#define U3CLATEAWAKEU3EC__ITERATOR0_T3289977563_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Layers.TouchLayer/<lateAwake>c__Iterator0
struct  U3ClateAwakeU3Ec__Iterator0_t3289977563  : public RuntimeObject
{
public:
	// TouchScript.Layers.TouchLayer TouchScript.Layers.TouchLayer/<lateAwake>c__Iterator0::$this
	TouchLayer_t2635439978 * ___U24this_0;
	// System.Object TouchScript.Layers.TouchLayer/<lateAwake>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean TouchScript.Layers.TouchLayer/<lateAwake>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 TouchScript.Layers.TouchLayer/<lateAwake>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3ClateAwakeU3Ec__Iterator0_t3289977563, ___U24this_0)); }
	inline TouchLayer_t2635439978 * get_U24this_0() const { return ___U24this_0; }
	inline TouchLayer_t2635439978 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(TouchLayer_t2635439978 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3ClateAwakeU3Ec__Iterator0_t3289977563, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3ClateAwakeU3Ec__Iterator0_t3289977563, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3ClateAwakeU3Ec__Iterator0_t3289977563, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLATEAWAKEU3EC__ITERATOR0_T3289977563_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	IntPtr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline IntPtr_t get_Zero_1() const { return ___Zero_1; }
	inline IntPtr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(IntPtr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t1328083999* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t1328083999* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t1328083999** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t1328083999* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef HTTPDIGESTIDENTITY_T3711678560_H
#define HTTPDIGESTIDENTITY_T3711678560_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.HttpDigestIdentity
struct  HttpDigestIdentity_t3711678560  : public GenericIdentity_t607870731
{
public:
	// System.Collections.Specialized.NameValueCollection WebSocketSharp.Net.HttpDigestIdentity::_parameters
	NameValueCollection_t3047564564 * ____parameters_2;

public:
	inline static int32_t get_offset_of__parameters_2() { return static_cast<int32_t>(offsetof(HttpDigestIdentity_t3711678560, ____parameters_2)); }
	inline NameValueCollection_t3047564564 * get__parameters_2() const { return ____parameters_2; }
	inline NameValueCollection_t3047564564 ** get_address_of__parameters_2() { return &____parameters_2; }
	inline void set__parameters_2(NameValueCollection_t3047564564 * value)
	{
		____parameters_2 = value;
		Il2CppCodeGenWriteBarrier((&____parameters_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPDIGESTIDENTITY_T3711678560_H
#ifndef VECTOR2_T2243707579_H
#define VECTOR2_T2243707579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2243707579 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2243707579, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2243707579, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2243707579_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2243707579  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2243707579  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2243707579  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2243707579  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2243707579  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2243707579  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2243707579  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2243707579  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2243707579  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2243707579 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2243707579  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___oneVector_3)); }
	inline Vector2_t2243707579  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2243707579 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2243707579  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___upVector_4)); }
	inline Vector2_t2243707579  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2243707579 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2243707579  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___downVector_5)); }
	inline Vector2_t2243707579  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2243707579 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2243707579  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___leftVector_6)); }
	inline Vector2_t2243707579  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2243707579 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2243707579  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___rightVector_7)); }
	inline Vector2_t2243707579  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2243707579 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2243707579  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2243707579  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2243707579 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2243707579  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2243707579  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2243707579 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2243707579  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2243707579_H
#ifndef LAYERMASK_T3188175821_H
#define LAYERMASK_T3188175821_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LayerMask
struct  LayerMask_t3188175821 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t3188175821, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_T3188175821_H
#ifndef VECTOR3_T2243707580_H
#define VECTOR3_T2243707580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t2243707580 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t2243707580_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t2243707580  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t2243707580  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t2243707580  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t2243707580  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t2243707580  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t2243707580  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t2243707580  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t2243707580  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t2243707580  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t2243707580  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___zeroVector_4)); }
	inline Vector3_t2243707580  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t2243707580 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t2243707580  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___oneVector_5)); }
	inline Vector3_t2243707580  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t2243707580 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t2243707580  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___upVector_6)); }
	inline Vector3_t2243707580  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t2243707580 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t2243707580  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___downVector_7)); }
	inline Vector3_t2243707580  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t2243707580 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t2243707580  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___leftVector_8)); }
	inline Vector3_t2243707580  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t2243707580 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t2243707580  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___rightVector_9)); }
	inline Vector3_t2243707580  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t2243707580 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t2243707580  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___forwardVector_10)); }
	inline Vector3_t2243707580  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t2243707580 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t2243707580  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___backVector_11)); }
	inline Vector3_t2243707580  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t2243707580 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t2243707580  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t2243707580  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t2243707580 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t2243707580  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t2243707580  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t2243707580 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t2243707580  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T2243707580_H
#ifndef HTTPBASICIDENTITY_T296759650_H
#define HTTPBASICIDENTITY_T296759650_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.HttpBasicIdentity
struct  HttpBasicIdentity_t296759650  : public GenericIdentity_t607870731
{
public:
	// System.String WebSocketSharp.Net.HttpBasicIdentity::_password
	String_t* ____password_2;

public:
	inline static int32_t get_offset_of__password_2() { return static_cast<int32_t>(offsetof(HttpBasicIdentity_t296759650, ____password_2)); }
	inline String_t* get__password_2() const { return ____password_2; }
	inline String_t** get_address_of__password_2() { return &____password_2; }
	inline void set__password_2(String_t* value)
	{
		____password_2 = value;
		Il2CppCodeGenWriteBarrier((&____password_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPBASICIDENTITY_T296759650_H
#ifndef HTTPREQUEST_T1845443631_H
#define HTTPREQUEST_T1845443631_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.HttpRequest
struct  HttpRequest_t1845443631  : public HttpBase_t4283398485
{
public:
	// System.String WebSocketSharp.HttpRequest::_method
	String_t* ____method_5;
	// System.String WebSocketSharp.HttpRequest::_uri
	String_t* ____uri_6;
	// System.Boolean WebSocketSharp.HttpRequest::_websocketRequest
	bool ____websocketRequest_7;
	// System.Boolean WebSocketSharp.HttpRequest::_websocketRequestSet
	bool ____websocketRequestSet_8;

public:
	inline static int32_t get_offset_of__method_5() { return static_cast<int32_t>(offsetof(HttpRequest_t1845443631, ____method_5)); }
	inline String_t* get__method_5() const { return ____method_5; }
	inline String_t** get_address_of__method_5() { return &____method_5; }
	inline void set__method_5(String_t* value)
	{
		____method_5 = value;
		Il2CppCodeGenWriteBarrier((&____method_5), value);
	}

	inline static int32_t get_offset_of__uri_6() { return static_cast<int32_t>(offsetof(HttpRequest_t1845443631, ____uri_6)); }
	inline String_t* get__uri_6() const { return ____uri_6; }
	inline String_t** get_address_of__uri_6() { return &____uri_6; }
	inline void set__uri_6(String_t* value)
	{
		____uri_6 = value;
		Il2CppCodeGenWriteBarrier((&____uri_6), value);
	}

	inline static int32_t get_offset_of__websocketRequest_7() { return static_cast<int32_t>(offsetof(HttpRequest_t1845443631, ____websocketRequest_7)); }
	inline bool get__websocketRequest_7() const { return ____websocketRequest_7; }
	inline bool* get_address_of__websocketRequest_7() { return &____websocketRequest_7; }
	inline void set__websocketRequest_7(bool value)
	{
		____websocketRequest_7 = value;
	}

	inline static int32_t get_offset_of__websocketRequestSet_8() { return static_cast<int32_t>(offsetof(HttpRequest_t1845443631, ____websocketRequestSet_8)); }
	inline bool get__websocketRequestSet_8() const { return ____websocketRequestSet_8; }
	inline bool* get_address_of__websocketRequestSet_8() { return &____websocketRequestSet_8; }
	inline void set__websocketRequestSet_8(bool value)
	{
		____websocketRequestSet_8 = value;
	}
};

struct HttpRequest_t1845443631_StaticFields
{
public:
	// System.Func`2<System.String[],WebSocketSharp.HttpResponse> WebSocketSharp.HttpRequest::<>f__mg$cache0
	Func_2_t1532221928 * ___U3CU3Ef__mgU24cache0_9;
	// System.Func`2<System.String[],WebSocketSharp.HttpRequest> WebSocketSharp.HttpRequest::<>f__mg$cache1
	Func_2_t557125244 * ___U3CU3Ef__mgU24cache1_10;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_9() { return static_cast<int32_t>(offsetof(HttpRequest_t1845443631_StaticFields, ___U3CU3Ef__mgU24cache0_9)); }
	inline Func_2_t1532221928 * get_U3CU3Ef__mgU24cache0_9() const { return ___U3CU3Ef__mgU24cache0_9; }
	inline Func_2_t1532221928 ** get_address_of_U3CU3Ef__mgU24cache0_9() { return &___U3CU3Ef__mgU24cache0_9; }
	inline void set_U3CU3Ef__mgU24cache0_9(Func_2_t1532221928 * value)
	{
		___U3CU3Ef__mgU24cache0_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_9), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_10() { return static_cast<int32_t>(offsetof(HttpRequest_t1845443631_StaticFields, ___U3CU3Ef__mgU24cache1_10)); }
	inline Func_2_t557125244 * get_U3CU3Ef__mgU24cache1_10() const { return ___U3CU3Ef__mgU24cache1_10; }
	inline Func_2_t557125244 ** get_address_of_U3CU3Ef__mgU24cache1_10() { return &___U3CU3Ef__mgU24cache1_10; }
	inline void set_U3CU3Ef__mgU24cache1_10(Func_2_t557125244 * value)
	{
		___U3CU3Ef__mgU24cache1_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPREQUEST_T1845443631_H
#ifndef HTTPRESPONSE_T2820540315_H
#define HTTPRESPONSE_T2820540315_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.HttpResponse
struct  HttpResponse_t2820540315  : public HttpBase_t4283398485
{
public:
	// System.String WebSocketSharp.HttpResponse::_code
	String_t* ____code_5;
	// System.String WebSocketSharp.HttpResponse::_reason
	String_t* ____reason_6;

public:
	inline static int32_t get_offset_of__code_5() { return static_cast<int32_t>(offsetof(HttpResponse_t2820540315, ____code_5)); }
	inline String_t* get__code_5() const { return ____code_5; }
	inline String_t** get_address_of__code_5() { return &____code_5; }
	inline void set__code_5(String_t* value)
	{
		____code_5 = value;
		Il2CppCodeGenWriteBarrier((&____code_5), value);
	}

	inline static int32_t get_offset_of__reason_6() { return static_cast<int32_t>(offsetof(HttpResponse_t2820540315, ____reason_6)); }
	inline String_t* get__reason_6() const { return ____reason_6; }
	inline String_t** get_address_of__reason_6() { return &____reason_6; }
	inline void set__reason_6(String_t* value)
	{
		____reason_6 = value;
		Il2CppCodeGenWriteBarrier((&____reason_6), value);
	}
};

struct HttpResponse_t2820540315_StaticFields
{
public:
	// System.Func`2<System.String[],WebSocketSharp.HttpResponse> WebSocketSharp.HttpResponse::<>f__mg$cache0
	Func_2_t1532221928 * ___U3CU3Ef__mgU24cache0_7;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_7() { return static_cast<int32_t>(offsetof(HttpResponse_t2820540315_StaticFields, ___U3CU3Ef__mgU24cache0_7)); }
	inline Func_2_t1532221928 * get_U3CU3Ef__mgU24cache0_7() const { return ___U3CU3Ef__mgU24cache0_7; }
	inline Func_2_t1532221928 ** get_address_of_U3CU3Ef__mgU24cache0_7() { return &___U3CU3Ef__mgU24cache0_7; }
	inline void set_U3CU3Ef__mgU24cache0_7(Func_2_t1532221928 * value)
	{
		___U3CU3Ef__mgU24cache0_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPRESPONSE_T2820540315_H
#ifndef BOOLEAN_T3825574718_H
#define BOOLEAN_T3825574718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t3825574718 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t3825574718, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t3825574718_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t3825574718_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t3825574718_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T3825574718_H
#ifndef SYSTEMEXCEPTION_T3877406272_H
#define SYSTEMEXCEPTION_T3877406272_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t3877406272  : public Exception_t1927440687
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T3877406272_H
#ifndef TOUCHEVENTARGS_T1917927166_H
#define TOUCHEVENTARGS_T1917927166_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.TouchEventArgs
struct  TouchEventArgs_t1917927166  : public EventArgs_t3289624707
{
public:
	// System.Collections.Generic.IList`1<TouchScript.TouchPoint> TouchScript.TouchEventArgs::<Touches>k__BackingField
	RuntimeObject* ___U3CTouchesU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CTouchesU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TouchEventArgs_t1917927166, ___U3CTouchesU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CTouchesU3Ek__BackingField_1() const { return ___U3CTouchesU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CTouchesU3Ek__BackingField_1() { return &___U3CTouchesU3Ek__BackingField_1; }
	inline void set_U3CTouchesU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CTouchesU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTouchesU3Ek__BackingField_1), value);
	}
};

struct TouchEventArgs_t1917927166_StaticFields
{
public:
	// TouchScript.TouchEventArgs TouchScript.TouchEventArgs::instance
	TouchEventArgs_t1917927166 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(TouchEventArgs_t1917927166_StaticFields, ___instance_2)); }
	inline TouchEventArgs_t1917927166 * get_instance_2() const { return ___instance_2; }
	inline TouchEventArgs_t1917927166 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(TouchEventArgs_t1917927166 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHEVENTARGS_T1917927166_H
#ifndef GUID_T_H
#define GUID_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Guid
struct  Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_0;
	// System.Int16 System.Guid::_b
	int16_t ____b_1;
	// System.Int16 System.Guid::_c
	int16_t ____c_2;
	// System.Byte System.Guid::_d
	uint8_t ____d_3;
	// System.Byte System.Guid::_e
	uint8_t ____e_4;
	// System.Byte System.Guid::_f
	uint8_t ____f_5;
	// System.Byte System.Guid::_g
	uint8_t ____g_6;
	// System.Byte System.Guid::_h
	uint8_t ____h_7;
	// System.Byte System.Guid::_i
	uint8_t ____i_8;
	// System.Byte System.Guid::_j
	uint8_t ____j_9;
	// System.Byte System.Guid::_k
	uint8_t ____k_10;

public:
	inline static int32_t get_offset_of__a_0() { return static_cast<int32_t>(offsetof(Guid_t, ____a_0)); }
	inline int32_t get__a_0() const { return ____a_0; }
	inline int32_t* get_address_of__a_0() { return &____a_0; }
	inline void set__a_0(int32_t value)
	{
		____a_0 = value;
	}

	inline static int32_t get_offset_of__b_1() { return static_cast<int32_t>(offsetof(Guid_t, ____b_1)); }
	inline int16_t get__b_1() const { return ____b_1; }
	inline int16_t* get_address_of__b_1() { return &____b_1; }
	inline void set__b_1(int16_t value)
	{
		____b_1 = value;
	}

	inline static int32_t get_offset_of__c_2() { return static_cast<int32_t>(offsetof(Guid_t, ____c_2)); }
	inline int16_t get__c_2() const { return ____c_2; }
	inline int16_t* get_address_of__c_2() { return &____c_2; }
	inline void set__c_2(int16_t value)
	{
		____c_2 = value;
	}

	inline static int32_t get_offset_of__d_3() { return static_cast<int32_t>(offsetof(Guid_t, ____d_3)); }
	inline uint8_t get__d_3() const { return ____d_3; }
	inline uint8_t* get_address_of__d_3() { return &____d_3; }
	inline void set__d_3(uint8_t value)
	{
		____d_3 = value;
	}

	inline static int32_t get_offset_of__e_4() { return static_cast<int32_t>(offsetof(Guid_t, ____e_4)); }
	inline uint8_t get__e_4() const { return ____e_4; }
	inline uint8_t* get_address_of__e_4() { return &____e_4; }
	inline void set__e_4(uint8_t value)
	{
		____e_4 = value;
	}

	inline static int32_t get_offset_of__f_5() { return static_cast<int32_t>(offsetof(Guid_t, ____f_5)); }
	inline uint8_t get__f_5() const { return ____f_5; }
	inline uint8_t* get_address_of__f_5() { return &____f_5; }
	inline void set__f_5(uint8_t value)
	{
		____f_5 = value;
	}

	inline static int32_t get_offset_of__g_6() { return static_cast<int32_t>(offsetof(Guid_t, ____g_6)); }
	inline uint8_t get__g_6() const { return ____g_6; }
	inline uint8_t* get_address_of__g_6() { return &____g_6; }
	inline void set__g_6(uint8_t value)
	{
		____g_6 = value;
	}

	inline static int32_t get_offset_of__h_7() { return static_cast<int32_t>(offsetof(Guid_t, ____h_7)); }
	inline uint8_t get__h_7() const { return ____h_7; }
	inline uint8_t* get_address_of__h_7() { return &____h_7; }
	inline void set__h_7(uint8_t value)
	{
		____h_7 = value;
	}

	inline static int32_t get_offset_of__i_8() { return static_cast<int32_t>(offsetof(Guid_t, ____i_8)); }
	inline uint8_t get__i_8() const { return ____i_8; }
	inline uint8_t* get_address_of__i_8() { return &____i_8; }
	inline void set__i_8(uint8_t value)
	{
		____i_8 = value;
	}

	inline static int32_t get_offset_of__j_9() { return static_cast<int32_t>(offsetof(Guid_t, ____j_9)); }
	inline uint8_t get__j_9() const { return ____j_9; }
	inline uint8_t* get_address_of__j_9() { return &____j_9; }
	inline void set__j_9(uint8_t value)
	{
		____j_9 = value;
	}

	inline static int32_t get_offset_of__k_10() { return static_cast<int32_t>(offsetof(Guid_t, ____k_10)); }
	inline uint8_t get__k_10() const { return ____k_10; }
	inline uint8_t* get_address_of__k_10() { return &____k_10; }
	inline void set__k_10(uint8_t value)
	{
		____k_10 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_11;
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_12;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t2510243513 * ____rng_13;

public:
	inline static int32_t get_offset_of_Empty_11() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_11)); }
	inline Guid_t  get_Empty_11() const { return ___Empty_11; }
	inline Guid_t * get_address_of_Empty_11() { return &___Empty_11; }
	inline void set_Empty_11(Guid_t  value)
	{
		___Empty_11 = value;
	}

	inline static int32_t get_offset_of__rngAccess_12() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_12)); }
	inline RuntimeObject * get__rngAccess_12() const { return ____rngAccess_12; }
	inline RuntimeObject ** get_address_of__rngAccess_12() { return &____rngAccess_12; }
	inline void set__rngAccess_12(RuntimeObject * value)
	{
		____rngAccess_12 = value;
		Il2CppCodeGenWriteBarrier((&____rngAccess_12), value);
	}

	inline static int32_t get_offset_of__rng_13() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_13)); }
	inline RandomNumberGenerator_t2510243513 * get__rng_13() const { return ____rng_13; }
	inline RandomNumberGenerator_t2510243513 ** get_address_of__rng_13() { return &____rng_13; }
	inline void set__rng_13(RandomNumberGenerator_t2510243513 * value)
	{
		____rng_13 = value;
		Il2CppCodeGenWriteBarrier((&____rng_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUID_T_H
#ifndef CAMERAPROJECTIONPARAMS_T285425166_H
#define CAMERAPROJECTIONPARAMS_T285425166_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Layers.CameraProjectionParams
struct  CameraProjectionParams_t285425166  : public ProjectionParams_t2712959773
{
public:
	// UnityEngine.Camera TouchScript.Layers.CameraProjectionParams::camera
	Camera_t189460977 * ___camera_0;

public:
	inline static int32_t get_offset_of_camera_0() { return static_cast<int32_t>(offsetof(CameraProjectionParams_t285425166, ___camera_0)); }
	inline Camera_t189460977 * get_camera_0() const { return ___camera_0; }
	inline Camera_t189460977 ** get_address_of_camera_0() { return &___camera_0; }
	inline void set_camera_0(Camera_t189460977 * value)
	{
		___camera_0 = value;
		Il2CppCodeGenWriteBarrier((&___camera_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAPROJECTIONPARAMS_T285425166_H
#ifndef TIMESPAN_T3430258949_H
#define TIMESPAN_T3430258949_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t3430258949 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_8;

public:
	inline static int32_t get_offset_of__ticks_8() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949, ____ticks_8)); }
	inline int64_t get__ticks_8() const { return ____ticks_8; }
	inline int64_t* get_address_of__ticks_8() { return &____ticks_8; }
	inline void set__ticks_8(int64_t value)
	{
		____ticks_8 = value;
	}
};

struct TimeSpan_t3430258949_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t3430258949  ___MaxValue_5;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t3430258949  ___MinValue_6;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t3430258949  ___Zero_7;

public:
	inline static int32_t get_offset_of_MaxValue_5() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949_StaticFields, ___MaxValue_5)); }
	inline TimeSpan_t3430258949  get_MaxValue_5() const { return ___MaxValue_5; }
	inline TimeSpan_t3430258949 * get_address_of_MaxValue_5() { return &___MaxValue_5; }
	inline void set_MaxValue_5(TimeSpan_t3430258949  value)
	{
		___MaxValue_5 = value;
	}

	inline static int32_t get_offset_of_MinValue_6() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949_StaticFields, ___MinValue_6)); }
	inline TimeSpan_t3430258949  get_MinValue_6() const { return ___MinValue_6; }
	inline TimeSpan_t3430258949 * get_address_of_MinValue_6() { return &___MinValue_6; }
	inline void set_MinValue_6(TimeSpan_t3430258949  value)
	{
		___MinValue_6 = value;
	}

	inline static int32_t get_offset_of_Zero_7() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949_StaticFields, ___Zero_7)); }
	inline TimeSpan_t3430258949  get_Zero_7() const { return ___Zero_7; }
	inline TimeSpan_t3430258949 * get_address_of_Zero_7() { return &___Zero_7; }
	inline void set_Zero_7(TimeSpan_t3430258949  value)
	{
		___Zero_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T3430258949_H
#ifndef PROPERTYATTRIBUTE_T2606999759_H
#define PROPERTYATTRIBUTE_T2606999759_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyAttribute
struct  PropertyAttribute_t2606999759  : public Attribute_t542643598
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYATTRIBUTE_T2606999759_H
#ifndef REQUESTSTREAM_T775716369_H
#define REQUESTSTREAM_T775716369_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.RequestStream
struct  RequestStream_t775716369  : public Stream_t3255436806
{
public:
	// System.Int64 WebSocketSharp.Net.RequestStream::_bodyLeft
	int64_t ____bodyLeft_1;
	// System.Byte[] WebSocketSharp.Net.RequestStream::_buffer
	ByteU5BU5D_t3397334013* ____buffer_2;
	// System.Int32 WebSocketSharp.Net.RequestStream::_count
	int32_t ____count_3;
	// System.Boolean WebSocketSharp.Net.RequestStream::_disposed
	bool ____disposed_4;
	// System.Int32 WebSocketSharp.Net.RequestStream::_offset
	int32_t ____offset_5;
	// System.IO.Stream WebSocketSharp.Net.RequestStream::_stream
	Stream_t3255436806 * ____stream_6;

public:
	inline static int32_t get_offset_of__bodyLeft_1() { return static_cast<int32_t>(offsetof(RequestStream_t775716369, ____bodyLeft_1)); }
	inline int64_t get__bodyLeft_1() const { return ____bodyLeft_1; }
	inline int64_t* get_address_of__bodyLeft_1() { return &____bodyLeft_1; }
	inline void set__bodyLeft_1(int64_t value)
	{
		____bodyLeft_1 = value;
	}

	inline static int32_t get_offset_of__buffer_2() { return static_cast<int32_t>(offsetof(RequestStream_t775716369, ____buffer_2)); }
	inline ByteU5BU5D_t3397334013* get__buffer_2() const { return ____buffer_2; }
	inline ByteU5BU5D_t3397334013** get_address_of__buffer_2() { return &____buffer_2; }
	inline void set__buffer_2(ByteU5BU5D_t3397334013* value)
	{
		____buffer_2 = value;
		Il2CppCodeGenWriteBarrier((&____buffer_2), value);
	}

	inline static int32_t get_offset_of__count_3() { return static_cast<int32_t>(offsetof(RequestStream_t775716369, ____count_3)); }
	inline int32_t get__count_3() const { return ____count_3; }
	inline int32_t* get_address_of__count_3() { return &____count_3; }
	inline void set__count_3(int32_t value)
	{
		____count_3 = value;
	}

	inline static int32_t get_offset_of__disposed_4() { return static_cast<int32_t>(offsetof(RequestStream_t775716369, ____disposed_4)); }
	inline bool get__disposed_4() const { return ____disposed_4; }
	inline bool* get_address_of__disposed_4() { return &____disposed_4; }
	inline void set__disposed_4(bool value)
	{
		____disposed_4 = value;
	}

	inline static int32_t get_offset_of__offset_5() { return static_cast<int32_t>(offsetof(RequestStream_t775716369, ____offset_5)); }
	inline int32_t get__offset_5() const { return ____offset_5; }
	inline int32_t* get_address_of__offset_5() { return &____offset_5; }
	inline void set__offset_5(int32_t value)
	{
		____offset_5 = value;
	}

	inline static int32_t get_offset_of__stream_6() { return static_cast<int32_t>(offsetof(RequestStream_t775716369, ____stream_6)); }
	inline Stream_t3255436806 * get__stream_6() const { return ____stream_6; }
	inline Stream_t3255436806 ** get_address_of__stream_6() { return &____stream_6; }
	inline void set__stream_6(Stream_t3255436806 * value)
	{
		____stream_6 = value;
		Il2CppCodeGenWriteBarrier((&____stream_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUESTSTREAM_T775716369_H
#ifndef ERROREVENTARGS_T502222999_H
#define ERROREVENTARGS_T502222999_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.ErrorEventArgs
struct  ErrorEventArgs_t502222999  : public EventArgs_t3289624707
{
public:
	// System.Exception WebSocketSharp.ErrorEventArgs::_exception
	Exception_t1927440687 * ____exception_1;
	// System.String WebSocketSharp.ErrorEventArgs::_message
	String_t* ____message_2;

public:
	inline static int32_t get_offset_of__exception_1() { return static_cast<int32_t>(offsetof(ErrorEventArgs_t502222999, ____exception_1)); }
	inline Exception_t1927440687 * get__exception_1() const { return ____exception_1; }
	inline Exception_t1927440687 ** get_address_of__exception_1() { return &____exception_1; }
	inline void set__exception_1(Exception_t1927440687 * value)
	{
		____exception_1 = value;
		Il2CppCodeGenWriteBarrier((&____exception_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(ErrorEventArgs_t502222999, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERROREVENTARGS_T502222999_H
#ifndef TOUCHLAYEREVENTARGS_T1247401065_H
#define TOUCHLAYEREVENTARGS_T1247401065_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Layers.TouchLayerEventArgs
struct  TouchLayerEventArgs_t1247401065  : public EventArgs_t3289624707
{
public:
	// TouchScript.TouchPoint TouchScript.Layers.TouchLayerEventArgs::<Touch>k__BackingField
	TouchPoint_t959629083 * ___U3CTouchU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CTouchU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TouchLayerEventArgs_t1247401065, ___U3CTouchU3Ek__BackingField_1)); }
	inline TouchPoint_t959629083 * get_U3CTouchU3Ek__BackingField_1() const { return ___U3CTouchU3Ek__BackingField_1; }
	inline TouchPoint_t959629083 ** get_address_of_U3CTouchU3Ek__BackingField_1() { return &___U3CTouchU3Ek__BackingField_1; }
	inline void set_U3CTouchU3Ek__BackingField_1(TouchPoint_t959629083 * value)
	{
		___U3CTouchU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTouchU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHLAYEREVENTARGS_T1247401065_H
#ifndef CLOSEEVENTARGS_T344507773_H
#define CLOSEEVENTARGS_T344507773_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.CloseEventArgs
struct  CloseEventArgs_t344507773  : public EventArgs_t3289624707
{
public:
	// System.Boolean WebSocketSharp.CloseEventArgs::_clean
	bool ____clean_1;
	// System.UInt16 WebSocketSharp.CloseEventArgs::_code
	uint16_t ____code_2;
	// WebSocketSharp.PayloadData WebSocketSharp.CloseEventArgs::_payloadData
	PayloadData_t3839327312 * ____payloadData_3;
	// System.String WebSocketSharp.CloseEventArgs::_reason
	String_t* ____reason_4;

public:
	inline static int32_t get_offset_of__clean_1() { return static_cast<int32_t>(offsetof(CloseEventArgs_t344507773, ____clean_1)); }
	inline bool get__clean_1() const { return ____clean_1; }
	inline bool* get_address_of__clean_1() { return &____clean_1; }
	inline void set__clean_1(bool value)
	{
		____clean_1 = value;
	}

	inline static int32_t get_offset_of__code_2() { return static_cast<int32_t>(offsetof(CloseEventArgs_t344507773, ____code_2)); }
	inline uint16_t get__code_2() const { return ____code_2; }
	inline uint16_t* get_address_of__code_2() { return &____code_2; }
	inline void set__code_2(uint16_t value)
	{
		____code_2 = value;
	}

	inline static int32_t get_offset_of__payloadData_3() { return static_cast<int32_t>(offsetof(CloseEventArgs_t344507773, ____payloadData_3)); }
	inline PayloadData_t3839327312 * get__payloadData_3() const { return ____payloadData_3; }
	inline PayloadData_t3839327312 ** get_address_of__payloadData_3() { return &____payloadData_3; }
	inline void set__payloadData_3(PayloadData_t3839327312 * value)
	{
		____payloadData_3 = value;
		Il2CppCodeGenWriteBarrier((&____payloadData_3), value);
	}

	inline static int32_t get_offset_of__reason_4() { return static_cast<int32_t>(offsetof(CloseEventArgs_t344507773, ____reason_4)); }
	inline String_t* get__reason_4() const { return ____reason_4; }
	inline String_t** get_address_of__reason_4() { return &____reason_4; }
	inline void set__reason_4(String_t* value)
	{
		____reason_4 = value;
		Il2CppCodeGenWriteBarrier((&____reason_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLOSEEVENTARGS_T344507773_H
#ifndef RAYCASTRESULT_T21186376_H
#define RAYCASTRESULT_T21186376_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.RaycastResult
struct  RaycastResult_t21186376 
{
public:
	// UnityEngine.GameObject UnityEngine.EventSystems.RaycastResult::m_GameObject
	GameObject_t1756533147 * ___m_GameObject_0;
	// UnityEngine.EventSystems.BaseRaycaster UnityEngine.EventSystems.RaycastResult::module
	BaseRaycaster_t2336171397 * ___module_1;
	// System.Single UnityEngine.EventSystems.RaycastResult::distance
	float ___distance_2;
	// System.Single UnityEngine.EventSystems.RaycastResult::index
	float ___index_3;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::depth
	int32_t ___depth_4;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::sortingLayer
	int32_t ___sortingLayer_5;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::sortingOrder
	int32_t ___sortingOrder_6;
	// UnityEngine.Vector3 UnityEngine.EventSystems.RaycastResult::worldPosition
	Vector3_t2243707580  ___worldPosition_7;
	// UnityEngine.Vector3 UnityEngine.EventSystems.RaycastResult::worldNormal
	Vector3_t2243707580  ___worldNormal_8;
	// UnityEngine.Vector2 UnityEngine.EventSystems.RaycastResult::screenPosition
	Vector2_t2243707579  ___screenPosition_9;

public:
	inline static int32_t get_offset_of_m_GameObject_0() { return static_cast<int32_t>(offsetof(RaycastResult_t21186376, ___m_GameObject_0)); }
	inline GameObject_t1756533147 * get_m_GameObject_0() const { return ___m_GameObject_0; }
	inline GameObject_t1756533147 ** get_address_of_m_GameObject_0() { return &___m_GameObject_0; }
	inline void set_m_GameObject_0(GameObject_t1756533147 * value)
	{
		___m_GameObject_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_GameObject_0), value);
	}

	inline static int32_t get_offset_of_module_1() { return static_cast<int32_t>(offsetof(RaycastResult_t21186376, ___module_1)); }
	inline BaseRaycaster_t2336171397 * get_module_1() const { return ___module_1; }
	inline BaseRaycaster_t2336171397 ** get_address_of_module_1() { return &___module_1; }
	inline void set_module_1(BaseRaycaster_t2336171397 * value)
	{
		___module_1 = value;
		Il2CppCodeGenWriteBarrier((&___module_1), value);
	}

	inline static int32_t get_offset_of_distance_2() { return static_cast<int32_t>(offsetof(RaycastResult_t21186376, ___distance_2)); }
	inline float get_distance_2() const { return ___distance_2; }
	inline float* get_address_of_distance_2() { return &___distance_2; }
	inline void set_distance_2(float value)
	{
		___distance_2 = value;
	}

	inline static int32_t get_offset_of_index_3() { return static_cast<int32_t>(offsetof(RaycastResult_t21186376, ___index_3)); }
	inline float get_index_3() const { return ___index_3; }
	inline float* get_address_of_index_3() { return &___index_3; }
	inline void set_index_3(float value)
	{
		___index_3 = value;
	}

	inline static int32_t get_offset_of_depth_4() { return static_cast<int32_t>(offsetof(RaycastResult_t21186376, ___depth_4)); }
	inline int32_t get_depth_4() const { return ___depth_4; }
	inline int32_t* get_address_of_depth_4() { return &___depth_4; }
	inline void set_depth_4(int32_t value)
	{
		___depth_4 = value;
	}

	inline static int32_t get_offset_of_sortingLayer_5() { return static_cast<int32_t>(offsetof(RaycastResult_t21186376, ___sortingLayer_5)); }
	inline int32_t get_sortingLayer_5() const { return ___sortingLayer_5; }
	inline int32_t* get_address_of_sortingLayer_5() { return &___sortingLayer_5; }
	inline void set_sortingLayer_5(int32_t value)
	{
		___sortingLayer_5 = value;
	}

	inline static int32_t get_offset_of_sortingOrder_6() { return static_cast<int32_t>(offsetof(RaycastResult_t21186376, ___sortingOrder_6)); }
	inline int32_t get_sortingOrder_6() const { return ___sortingOrder_6; }
	inline int32_t* get_address_of_sortingOrder_6() { return &___sortingOrder_6; }
	inline void set_sortingOrder_6(int32_t value)
	{
		___sortingOrder_6 = value;
	}

	inline static int32_t get_offset_of_worldPosition_7() { return static_cast<int32_t>(offsetof(RaycastResult_t21186376, ___worldPosition_7)); }
	inline Vector3_t2243707580  get_worldPosition_7() const { return ___worldPosition_7; }
	inline Vector3_t2243707580 * get_address_of_worldPosition_7() { return &___worldPosition_7; }
	inline void set_worldPosition_7(Vector3_t2243707580  value)
	{
		___worldPosition_7 = value;
	}

	inline static int32_t get_offset_of_worldNormal_8() { return static_cast<int32_t>(offsetof(RaycastResult_t21186376, ___worldNormal_8)); }
	inline Vector3_t2243707580  get_worldNormal_8() const { return ___worldNormal_8; }
	inline Vector3_t2243707580 * get_address_of_worldNormal_8() { return &___worldNormal_8; }
	inline void set_worldNormal_8(Vector3_t2243707580  value)
	{
		___worldNormal_8 = value;
	}

	inline static int32_t get_offset_of_screenPosition_9() { return static_cast<int32_t>(offsetof(RaycastResult_t21186376, ___screenPosition_9)); }
	inline Vector2_t2243707579  get_screenPosition_9() const { return ___screenPosition_9; }
	inline Vector2_t2243707579 * get_address_of_screenPosition_9() { return &___screenPosition_9; }
	inline void set_screenPosition_9(Vector2_t2243707579  value)
	{
		___screenPosition_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.EventSystems.RaycastResult
struct RaycastResult_t21186376_marshaled_pinvoke
{
	GameObject_t1756533147 * ___m_GameObject_0;
	BaseRaycaster_t2336171397 * ___module_1;
	float ___distance_2;
	float ___index_3;
	int32_t ___depth_4;
	int32_t ___sortingLayer_5;
	int32_t ___sortingOrder_6;
	Vector3_t2243707580  ___worldPosition_7;
	Vector3_t2243707580  ___worldNormal_8;
	Vector2_t2243707579  ___screenPosition_9;
};
// Native definition for COM marshalling of UnityEngine.EventSystems.RaycastResult
struct RaycastResult_t21186376_marshaled_com
{
	GameObject_t1756533147 * ___m_GameObject_0;
	BaseRaycaster_t2336171397 * ___module_1;
	float ___distance_2;
	float ___index_3;
	int32_t ___depth_4;
	int32_t ___sortingLayer_5;
	int32_t ___sortingOrder_6;
	Vector3_t2243707580  ___worldPosition_7;
	Vector3_t2243707580  ___worldNormal_8;
	Vector2_t2243707579  ___screenPosition_9;
};
#endif // RAYCASTRESULT_T21186376_H
#ifndef HTTPLISTENERREQUEST_T2316381291_H
#define HTTPLISTENERREQUEST_T2316381291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.HttpListenerRequest
struct  HttpListenerRequest_t2316381291  : public RuntimeObject
{
public:
	// System.String[] WebSocketSharp.Net.HttpListenerRequest::_acceptTypes
	StringU5BU5D_t1642385972* ____acceptTypes_1;
	// System.Boolean WebSocketSharp.Net.HttpListenerRequest::_chunked
	bool ____chunked_2;
	// System.Text.Encoding WebSocketSharp.Net.HttpListenerRequest::_contentEncoding
	Encoding_t663144255 * ____contentEncoding_3;
	// System.Int64 WebSocketSharp.Net.HttpListenerRequest::_contentLength
	int64_t ____contentLength_4;
	// System.Boolean WebSocketSharp.Net.HttpListenerRequest::_contentLengthSet
	bool ____contentLengthSet_5;
	// WebSocketSharp.Net.HttpListenerContext WebSocketSharp.Net.HttpListenerRequest::_context
	HttpListenerContext_t994708409 * ____context_6;
	// WebSocketSharp.Net.CookieCollection WebSocketSharp.Net.HttpListenerRequest::_cookies
	CookieCollection_t4248997468 * ____cookies_7;
	// WebSocketSharp.Net.WebHeaderCollection WebSocketSharp.Net.HttpListenerRequest::_headers
	WebHeaderCollection_t1932982249 * ____headers_8;
	// System.Guid WebSocketSharp.Net.HttpListenerRequest::_identifier
	Guid_t  ____identifier_9;
	// System.IO.Stream WebSocketSharp.Net.HttpListenerRequest::_inputStream
	Stream_t3255436806 * ____inputStream_10;
	// System.Boolean WebSocketSharp.Net.HttpListenerRequest::_keepAlive
	bool ____keepAlive_11;
	// System.Boolean WebSocketSharp.Net.HttpListenerRequest::_keepAliveSet
	bool ____keepAliveSet_12;
	// System.String WebSocketSharp.Net.HttpListenerRequest::_method
	String_t* ____method_13;
	// System.Collections.Specialized.NameValueCollection WebSocketSharp.Net.HttpListenerRequest::_queryString
	NameValueCollection_t3047564564 * ____queryString_14;
	// System.Uri WebSocketSharp.Net.HttpListenerRequest::_referer
	Uri_t19570940 * ____referer_15;
	// System.String WebSocketSharp.Net.HttpListenerRequest::_uri
	String_t* ____uri_16;
	// System.Uri WebSocketSharp.Net.HttpListenerRequest::_url
	Uri_t19570940 * ____url_17;
	// System.String[] WebSocketSharp.Net.HttpListenerRequest::_userLanguages
	StringU5BU5D_t1642385972* ____userLanguages_18;
	// System.Version WebSocketSharp.Net.HttpListenerRequest::_version
	Version_t1755874712 * ____version_19;
	// System.Boolean WebSocketSharp.Net.HttpListenerRequest::_websocketRequest
	bool ____websocketRequest_20;
	// System.Boolean WebSocketSharp.Net.HttpListenerRequest::_websocketRequestSet
	bool ____websocketRequestSet_21;

public:
	inline static int32_t get_offset_of__acceptTypes_1() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t2316381291, ____acceptTypes_1)); }
	inline StringU5BU5D_t1642385972* get__acceptTypes_1() const { return ____acceptTypes_1; }
	inline StringU5BU5D_t1642385972** get_address_of__acceptTypes_1() { return &____acceptTypes_1; }
	inline void set__acceptTypes_1(StringU5BU5D_t1642385972* value)
	{
		____acceptTypes_1 = value;
		Il2CppCodeGenWriteBarrier((&____acceptTypes_1), value);
	}

	inline static int32_t get_offset_of__chunked_2() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t2316381291, ____chunked_2)); }
	inline bool get__chunked_2() const { return ____chunked_2; }
	inline bool* get_address_of__chunked_2() { return &____chunked_2; }
	inline void set__chunked_2(bool value)
	{
		____chunked_2 = value;
	}

	inline static int32_t get_offset_of__contentEncoding_3() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t2316381291, ____contentEncoding_3)); }
	inline Encoding_t663144255 * get__contentEncoding_3() const { return ____contentEncoding_3; }
	inline Encoding_t663144255 ** get_address_of__contentEncoding_3() { return &____contentEncoding_3; }
	inline void set__contentEncoding_3(Encoding_t663144255 * value)
	{
		____contentEncoding_3 = value;
		Il2CppCodeGenWriteBarrier((&____contentEncoding_3), value);
	}

	inline static int32_t get_offset_of__contentLength_4() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t2316381291, ____contentLength_4)); }
	inline int64_t get__contentLength_4() const { return ____contentLength_4; }
	inline int64_t* get_address_of__contentLength_4() { return &____contentLength_4; }
	inline void set__contentLength_4(int64_t value)
	{
		____contentLength_4 = value;
	}

	inline static int32_t get_offset_of__contentLengthSet_5() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t2316381291, ____contentLengthSet_5)); }
	inline bool get__contentLengthSet_5() const { return ____contentLengthSet_5; }
	inline bool* get_address_of__contentLengthSet_5() { return &____contentLengthSet_5; }
	inline void set__contentLengthSet_5(bool value)
	{
		____contentLengthSet_5 = value;
	}

	inline static int32_t get_offset_of__context_6() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t2316381291, ____context_6)); }
	inline HttpListenerContext_t994708409 * get__context_6() const { return ____context_6; }
	inline HttpListenerContext_t994708409 ** get_address_of__context_6() { return &____context_6; }
	inline void set__context_6(HttpListenerContext_t994708409 * value)
	{
		____context_6 = value;
		Il2CppCodeGenWriteBarrier((&____context_6), value);
	}

	inline static int32_t get_offset_of__cookies_7() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t2316381291, ____cookies_7)); }
	inline CookieCollection_t4248997468 * get__cookies_7() const { return ____cookies_7; }
	inline CookieCollection_t4248997468 ** get_address_of__cookies_7() { return &____cookies_7; }
	inline void set__cookies_7(CookieCollection_t4248997468 * value)
	{
		____cookies_7 = value;
		Il2CppCodeGenWriteBarrier((&____cookies_7), value);
	}

	inline static int32_t get_offset_of__headers_8() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t2316381291, ____headers_8)); }
	inline WebHeaderCollection_t1932982249 * get__headers_8() const { return ____headers_8; }
	inline WebHeaderCollection_t1932982249 ** get_address_of__headers_8() { return &____headers_8; }
	inline void set__headers_8(WebHeaderCollection_t1932982249 * value)
	{
		____headers_8 = value;
		Il2CppCodeGenWriteBarrier((&____headers_8), value);
	}

	inline static int32_t get_offset_of__identifier_9() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t2316381291, ____identifier_9)); }
	inline Guid_t  get__identifier_9() const { return ____identifier_9; }
	inline Guid_t * get_address_of__identifier_9() { return &____identifier_9; }
	inline void set__identifier_9(Guid_t  value)
	{
		____identifier_9 = value;
	}

	inline static int32_t get_offset_of__inputStream_10() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t2316381291, ____inputStream_10)); }
	inline Stream_t3255436806 * get__inputStream_10() const { return ____inputStream_10; }
	inline Stream_t3255436806 ** get_address_of__inputStream_10() { return &____inputStream_10; }
	inline void set__inputStream_10(Stream_t3255436806 * value)
	{
		____inputStream_10 = value;
		Il2CppCodeGenWriteBarrier((&____inputStream_10), value);
	}

	inline static int32_t get_offset_of__keepAlive_11() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t2316381291, ____keepAlive_11)); }
	inline bool get__keepAlive_11() const { return ____keepAlive_11; }
	inline bool* get_address_of__keepAlive_11() { return &____keepAlive_11; }
	inline void set__keepAlive_11(bool value)
	{
		____keepAlive_11 = value;
	}

	inline static int32_t get_offset_of__keepAliveSet_12() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t2316381291, ____keepAliveSet_12)); }
	inline bool get__keepAliveSet_12() const { return ____keepAliveSet_12; }
	inline bool* get_address_of__keepAliveSet_12() { return &____keepAliveSet_12; }
	inline void set__keepAliveSet_12(bool value)
	{
		____keepAliveSet_12 = value;
	}

	inline static int32_t get_offset_of__method_13() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t2316381291, ____method_13)); }
	inline String_t* get__method_13() const { return ____method_13; }
	inline String_t** get_address_of__method_13() { return &____method_13; }
	inline void set__method_13(String_t* value)
	{
		____method_13 = value;
		Il2CppCodeGenWriteBarrier((&____method_13), value);
	}

	inline static int32_t get_offset_of__queryString_14() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t2316381291, ____queryString_14)); }
	inline NameValueCollection_t3047564564 * get__queryString_14() const { return ____queryString_14; }
	inline NameValueCollection_t3047564564 ** get_address_of__queryString_14() { return &____queryString_14; }
	inline void set__queryString_14(NameValueCollection_t3047564564 * value)
	{
		____queryString_14 = value;
		Il2CppCodeGenWriteBarrier((&____queryString_14), value);
	}

	inline static int32_t get_offset_of__referer_15() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t2316381291, ____referer_15)); }
	inline Uri_t19570940 * get__referer_15() const { return ____referer_15; }
	inline Uri_t19570940 ** get_address_of__referer_15() { return &____referer_15; }
	inline void set__referer_15(Uri_t19570940 * value)
	{
		____referer_15 = value;
		Il2CppCodeGenWriteBarrier((&____referer_15), value);
	}

	inline static int32_t get_offset_of__uri_16() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t2316381291, ____uri_16)); }
	inline String_t* get__uri_16() const { return ____uri_16; }
	inline String_t** get_address_of__uri_16() { return &____uri_16; }
	inline void set__uri_16(String_t* value)
	{
		____uri_16 = value;
		Il2CppCodeGenWriteBarrier((&____uri_16), value);
	}

	inline static int32_t get_offset_of__url_17() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t2316381291, ____url_17)); }
	inline Uri_t19570940 * get__url_17() const { return ____url_17; }
	inline Uri_t19570940 ** get_address_of__url_17() { return &____url_17; }
	inline void set__url_17(Uri_t19570940 * value)
	{
		____url_17 = value;
		Il2CppCodeGenWriteBarrier((&____url_17), value);
	}

	inline static int32_t get_offset_of__userLanguages_18() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t2316381291, ____userLanguages_18)); }
	inline StringU5BU5D_t1642385972* get__userLanguages_18() const { return ____userLanguages_18; }
	inline StringU5BU5D_t1642385972** get_address_of__userLanguages_18() { return &____userLanguages_18; }
	inline void set__userLanguages_18(StringU5BU5D_t1642385972* value)
	{
		____userLanguages_18 = value;
		Il2CppCodeGenWriteBarrier((&____userLanguages_18), value);
	}

	inline static int32_t get_offset_of__version_19() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t2316381291, ____version_19)); }
	inline Version_t1755874712 * get__version_19() const { return ____version_19; }
	inline Version_t1755874712 ** get_address_of__version_19() { return &____version_19; }
	inline void set__version_19(Version_t1755874712 * value)
	{
		____version_19 = value;
		Il2CppCodeGenWriteBarrier((&____version_19), value);
	}

	inline static int32_t get_offset_of__websocketRequest_20() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t2316381291, ____websocketRequest_20)); }
	inline bool get__websocketRequest_20() const { return ____websocketRequest_20; }
	inline bool* get_address_of__websocketRequest_20() { return &____websocketRequest_20; }
	inline void set__websocketRequest_20(bool value)
	{
		____websocketRequest_20 = value;
	}

	inline static int32_t get_offset_of__websocketRequestSet_21() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t2316381291, ____websocketRequestSet_21)); }
	inline bool get__websocketRequestSet_21() const { return ____websocketRequestSet_21; }
	inline bool* get_address_of__websocketRequestSet_21() { return &____websocketRequestSet_21; }
	inline void set__websocketRequestSet_21(bool value)
	{
		____websocketRequestSet_21 = value;
	}
};

struct HttpListenerRequest_t2316381291_StaticFields
{
public:
	// System.Byte[] WebSocketSharp.Net.HttpListenerRequest::_100continue
	ByteU5BU5D_t3397334013* ____100continue_0;

public:
	inline static int32_t get_offset_of__100continue_0() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t2316381291_StaticFields, ____100continue_0)); }
	inline ByteU5BU5D_t3397334013* get__100continue_0() const { return ____100continue_0; }
	inline ByteU5BU5D_t3397334013** get_address_of__100continue_0() { return &____100continue_0; }
	inline void set__100continue_0(ByteU5BU5D_t3397334013* value)
	{
		____100continue_0 = value;
		Il2CppCodeGenWriteBarrier((&____100continue_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPLISTENERREQUEST_T2316381291_H
#ifndef RAYCASTHIT2D_T4063908774_H
#define RAYCASTHIT2D_T4063908774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit2D
struct  RaycastHit2D_t4063908774 
{
public:
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Centroid
	Vector2_t2243707579  ___m_Centroid_0;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Point
	Vector2_t2243707579  ___m_Point_1;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Normal
	Vector2_t2243707579  ___m_Normal_2;
	// System.Single UnityEngine.RaycastHit2D::m_Distance
	float ___m_Distance_3;
	// System.Single UnityEngine.RaycastHit2D::m_Fraction
	float ___m_Fraction_4;
	// UnityEngine.Collider2D UnityEngine.RaycastHit2D::m_Collider
	Collider2D_t646061738 * ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Centroid_0() { return static_cast<int32_t>(offsetof(RaycastHit2D_t4063908774, ___m_Centroid_0)); }
	inline Vector2_t2243707579  get_m_Centroid_0() const { return ___m_Centroid_0; }
	inline Vector2_t2243707579 * get_address_of_m_Centroid_0() { return &___m_Centroid_0; }
	inline void set_m_Centroid_0(Vector2_t2243707579  value)
	{
		___m_Centroid_0 = value;
	}

	inline static int32_t get_offset_of_m_Point_1() { return static_cast<int32_t>(offsetof(RaycastHit2D_t4063908774, ___m_Point_1)); }
	inline Vector2_t2243707579  get_m_Point_1() const { return ___m_Point_1; }
	inline Vector2_t2243707579 * get_address_of_m_Point_1() { return &___m_Point_1; }
	inline void set_m_Point_1(Vector2_t2243707579  value)
	{
		___m_Point_1 = value;
	}

	inline static int32_t get_offset_of_m_Normal_2() { return static_cast<int32_t>(offsetof(RaycastHit2D_t4063908774, ___m_Normal_2)); }
	inline Vector2_t2243707579  get_m_Normal_2() const { return ___m_Normal_2; }
	inline Vector2_t2243707579 * get_address_of_m_Normal_2() { return &___m_Normal_2; }
	inline void set_m_Normal_2(Vector2_t2243707579  value)
	{
		___m_Normal_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit2D_t4063908774, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_Fraction_4() { return static_cast<int32_t>(offsetof(RaycastHit2D_t4063908774, ___m_Fraction_4)); }
	inline float get_m_Fraction_4() const { return ___m_Fraction_4; }
	inline float* get_address_of_m_Fraction_4() { return &___m_Fraction_4; }
	inline void set_m_Fraction_4(float value)
	{
		___m_Fraction_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit2D_t4063908774, ___m_Collider_5)); }
	inline Collider2D_t646061738 * get_m_Collider_5() const { return ___m_Collider_5; }
	inline Collider2D_t646061738 ** get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(Collider2D_t646061738 * value)
	{
		___m_Collider_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Collider_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.RaycastHit2D
struct RaycastHit2D_t4063908774_marshaled_pinvoke
{
	Vector2_t2243707579  ___m_Centroid_0;
	Vector2_t2243707579  ___m_Point_1;
	Vector2_t2243707579  ___m_Normal_2;
	float ___m_Distance_3;
	float ___m_Fraction_4;
	Collider2D_t646061738 * ___m_Collider_5;
};
// Native definition for COM marshalling of UnityEngine.RaycastHit2D
struct RaycastHit2D_t4063908774_marshaled_com
{
	Vector2_t2243707579  ___m_Centroid_0;
	Vector2_t2243707579  ___m_Point_1;
	Vector2_t2243707579  ___m_Normal_2;
	float ___m_Distance_3;
	float ___m_Fraction_4;
	Collider2D_t646061738 * ___m_Collider_5;
};
#endif // RAYCASTHIT2D_T4063908774_H
#ifndef EXTERNALEXCEPTION_T1252662682_H
#define EXTERNALEXCEPTION_T1252662682_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.ExternalException
struct  ExternalException_t1252662682  : public SystemException_t3877406272
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTERNALEXCEPTION_T1252662682_H
#ifndef OBJECT_T1021602117_H
#define OBJECT_T1021602117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1021602117  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	IntPtr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1021602117, ___m_CachedPtr_0)); }
	inline IntPtr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline IntPtr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(IntPtr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1021602117_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1021602117_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1021602117_H
#ifndef DATETIMEKIND_T2186819611_H
#define DATETIMEKIND_T2186819611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeKind
struct  DateTimeKind_t2186819611 
{
public:
	// System.Int32 System.DateTimeKind::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeKind_t2186819611, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEKIND_T2186819611_H
#ifndef SSLPROTOCOLS_T894678499_H
#define SSLPROTOCOLS_T894678499_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Authentication.SslProtocols
struct  SslProtocols_t894678499 
{
public:
	// System.Int32 System.Security.Authentication.SslProtocols::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SslProtocols_t894678499, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SSLPROTOCOLS_T894678499_H
#ifndef FORMATEXCEPTION_T2948921286_H
#define FORMATEXCEPTION_T2948921286_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.FormatException
struct  FormatException_t2948921286  : public SystemException_t3877406272
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMATEXCEPTION_T2948921286_H
#ifndef INPUTSTATE_T2016389849_H
#define INPUTSTATE_T2016389849_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.InputState
struct  InputState_t2016389849 
{
public:
	// System.Int32 WebSocketSharp.Net.InputState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InputState_t2016389849, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTSTATE_T2016389849_H
#ifndef INPUTCHUNKSTATE_T126533044_H
#define INPUTCHUNKSTATE_T126533044_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.InputChunkState
struct  InputChunkState_t126533044 
{
public:
	// System.Int32 WebSocketSharp.Net.InputChunkState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InputChunkState_t126533044, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTCHUNKSTATE_T126533044_H
#ifndef OPCODE_T2313788840_H
#define OPCODE_T2313788840_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Opcode
struct  Opcode_t2313788840 
{
public:
	// System.Byte WebSocketSharp.Opcode::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Opcode_t2313788840, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPCODE_T2313788840_H
#ifndef RENDERMODE_T4280533217_H
#define RENDERMODE_T4280533217_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderMode
struct  RenderMode_t4280533217 
{
public:
	// System.Int32 UnityEngine.RenderMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RenderMode_t4280533217, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERMODE_T4280533217_H
#ifndef LINESTATE_T825951569_H
#define LINESTATE_T825951569_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.LineState
struct  LineState_t825951569 
{
public:
	// System.Int32 WebSocketSharp.Net.LineState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LineState_t825951569, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINESTATE_T825951569_H
#ifndef TOUCHHITTYPE_T1472696400_H
#define TOUCHHITTYPE_T1472696400_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Hit.TouchHit/TouchHitType
struct  TouchHitType_t1472696400 
{
public:
	// System.Int32 TouchScript.Hit.TouchHit/TouchHitType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchHitType_t1472696400, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHHITTYPE_T1472696400_H
#ifndef RAYCASTHIT_T87180320_H
#define RAYCASTHIT_T87180320_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit
struct  RaycastHit_t87180320 
{
public:
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_t2243707580  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_t2243707580  ___m_Normal_1;
	// System.Int32 UnityEngine.RaycastHit::m_FaceID
	int32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_t2243707579  ___m_UV_4;
	// UnityEngine.Collider UnityEngine.RaycastHit::m_Collider
	Collider_t3497673348 * ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(RaycastHit_t87180320, ___m_Point_0)); }
	inline Vector3_t2243707580  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_t2243707580 * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_t2243707580  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(RaycastHit_t87180320, ___m_Normal_1)); }
	inline Vector3_t2243707580  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_t2243707580 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_t2243707580  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_FaceID_2() { return static_cast<int32_t>(offsetof(RaycastHit_t87180320, ___m_FaceID_2)); }
	inline int32_t get_m_FaceID_2() const { return ___m_FaceID_2; }
	inline int32_t* get_address_of_m_FaceID_2() { return &___m_FaceID_2; }
	inline void set_m_FaceID_2(int32_t value)
	{
		___m_FaceID_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit_t87180320, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_UV_4() { return static_cast<int32_t>(offsetof(RaycastHit_t87180320, ___m_UV_4)); }
	inline Vector2_t2243707579  get_m_UV_4() const { return ___m_UV_4; }
	inline Vector2_t2243707579 * get_address_of_m_UV_4() { return &___m_UV_4; }
	inline void set_m_UV_4(Vector2_t2243707579  value)
	{
		___m_UV_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit_t87180320, ___m_Collider_5)); }
	inline Collider_t3497673348 * get_m_Collider_5() const { return ___m_Collider_5; }
	inline Collider_t3497673348 ** get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(Collider_t3497673348 * value)
	{
		___m_Collider_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Collider_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.RaycastHit
struct RaycastHit_t87180320_marshaled_pinvoke
{
	Vector3_t2243707580  ___m_Point_0;
	Vector3_t2243707580  ___m_Normal_1;
	int32_t ___m_FaceID_2;
	float ___m_Distance_3;
	Vector2_t2243707579  ___m_UV_4;
	Collider_t3497673348 * ___m_Collider_5;
};
// Native definition for COM marshalling of UnityEngine.RaycastHit
struct RaycastHit_t87180320_marshaled_com
{
	Vector3_t2243707580  ___m_Point_0;
	Vector3_t2243707580  ___m_Normal_1;
	int32_t ___m_FaceID_2;
	float ___m_Distance_3;
	Vector2_t2243707579  ___m_UV_4;
	Collider_t3497673348 * ___m_Collider_5;
};
#endif // RAYCASTHIT_T87180320_H
#ifndef HTTPREQUESTHEADER_T1796288394_H
#define HTTPREQUESTHEADER_T1796288394_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.HttpRequestHeader
struct  HttpRequestHeader_t1796288394 
{
public:
	// System.Int32 WebSocketSharp.Net.HttpRequestHeader::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HttpRequestHeader_t1796288394, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPREQUESTHEADER_T1796288394_H
#ifndef HTTPSTATUSCODE_T2661820989_H
#define HTTPSTATUSCODE_T2661820989_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.HttpStatusCode
struct  HttpStatusCode_t2661820989 
{
public:
	// System.Int32 WebSocketSharp.Net.HttpStatusCode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HttpStatusCode_t2661820989, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPSTATUSCODE_T2661820989_H
#ifndef HTTPRESPONSEHEADER_T1325853500_H
#define HTTPRESPONSEHEADER_T1325853500_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.HttpResponseHeader
struct  HttpResponseHeader_t1325853500 
{
public:
	// System.Int32 WebSocketSharp.Net.HttpResponseHeader::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HttpResponseHeader_t1325853500, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPRESPONSEHEADER_T1325853500_H
#ifndef NULLTOGGLEATTRIBUTE_T2456566677_H
#define NULLTOGGLEATTRIBUTE_T2456566677_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Utils.Attributes.NullToggleAttribute
struct  NullToggleAttribute_t2456566677  : public PropertyAttribute_t2606999759
{
public:
	// System.Int32 TouchScript.Utils.Attributes.NullToggleAttribute::NullIntValue
	int32_t ___NullIntValue_0;
	// System.Single TouchScript.Utils.Attributes.NullToggleAttribute::NullFloatValue
	float ___NullFloatValue_1;
	// UnityEngine.Object TouchScript.Utils.Attributes.NullToggleAttribute::NullObjectValue
	Object_t1021602117 * ___NullObjectValue_2;

public:
	inline static int32_t get_offset_of_NullIntValue_0() { return static_cast<int32_t>(offsetof(NullToggleAttribute_t2456566677, ___NullIntValue_0)); }
	inline int32_t get_NullIntValue_0() const { return ___NullIntValue_0; }
	inline int32_t* get_address_of_NullIntValue_0() { return &___NullIntValue_0; }
	inline void set_NullIntValue_0(int32_t value)
	{
		___NullIntValue_0 = value;
	}

	inline static int32_t get_offset_of_NullFloatValue_1() { return static_cast<int32_t>(offsetof(NullToggleAttribute_t2456566677, ___NullFloatValue_1)); }
	inline float get_NullFloatValue_1() const { return ___NullFloatValue_1; }
	inline float* get_address_of_NullFloatValue_1() { return &___NullFloatValue_1; }
	inline void set_NullFloatValue_1(float value)
	{
		___NullFloatValue_1 = value;
	}

	inline static int32_t get_offset_of_NullObjectValue_2() { return static_cast<int32_t>(offsetof(NullToggleAttribute_t2456566677, ___NullObjectValue_2)); }
	inline Object_t1021602117 * get_NullObjectValue_2() const { return ___NullObjectValue_2; }
	inline Object_t1021602117 ** get_address_of_NullObjectValue_2() { return &___NullObjectValue_2; }
	inline void set_NullObjectValue_2(Object_t1021602117 * value)
	{
		___NullObjectValue_2 = value;
		Il2CppCodeGenWriteBarrier((&___NullObjectValue_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLTOGGLEATTRIBUTE_T2456566677_H
#ifndef TOGGLELEFTATTRIBUTE_T3639526933_H
#define TOGGLELEFTATTRIBUTE_T3639526933_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Utils.Attributes.ToggleLeftAttribute
struct  ToggleLeftAttribute_t3639526933  : public PropertyAttribute_t2606999759
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOGGLELEFTATTRIBUTE_T3639526933_H
#ifndef MESSAGENAME_T2757950695_H
#define MESSAGENAME_T2757950695_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.TouchManager/MessageName
struct  MessageName_t2757950695 
{
public:
	// System.Int32 TouchScript.TouchManager/MessageName::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MessageName_t2757950695, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGENAME_T2757950695_H
#ifndef AUTHENTICATIONSCHEMES_T29593226_H
#define AUTHENTICATIONSCHEMES_T29593226_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.AuthenticationSchemes
struct  AuthenticationSchemes_t29593226 
{
public:
	// System.Int32 WebSocketSharp.Net.AuthenticationSchemes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AuthenticationSchemes_t29593226, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTHENTICATIONSCHEMES_T29593226_H
#ifndef MESSAGETYPE_T1628650752_H
#define MESSAGETYPE_T1628650752_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.TouchManager/MessageType
struct  MessageType_t1628650752 
{
public:
	// System.Int32 TouchScript.TouchManager/MessageType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MessageType_t1628650752, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGETYPE_T1628650752_H
#ifndef COMPRESSIONMETHOD_T4066553457_H
#define COMPRESSIONMETHOD_T4066553457_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.CompressionMethod
struct  CompressionMethod_t4066553457 
{
public:
	// System.Byte WebSocketSharp.CompressionMethod::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CompressionMethod_t4066553457, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPRESSIONMETHOD_T4066553457_H
#ifndef FIN_T2752139063_H
#define FIN_T2752139063_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Fin
struct  Fin_t2752139063 
{
public:
	// System.Byte WebSocketSharp.Fin::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Fin_t2752139063, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIN_T2752139063_H
#ifndef BYTEORDER_T469806806_H
#define BYTEORDER_T469806806_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.ByteOrder
struct  ByteOrder_t469806806 
{
public:
	// System.Int32 WebSocketSharp.ByteOrder::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ByteOrder_t469806806, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTEORDER_T469806806_H
#ifndef MASK_T1111889066_H
#define MASK_T1111889066_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Mask
struct  Mask_t1111889066 
{
public:
	// System.Byte WebSocketSharp.Mask::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Mask_t1111889066, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASK_T1111889066_H
#ifndef LOGLEVEL_T2748531832_H
#define LOGLEVEL_T2748531832_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.LogLevel
struct  LogLevel_t2748531832 
{
public:
	// System.Int32 WebSocketSharp.LogLevel::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LogLevel_t2748531832, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGLEVEL_T2748531832_H
#ifndef WINDOWS7TOUCHAPITYPE_T3091725811_H
#define WINDOWS7TOUCHAPITYPE_T3091725811_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.InputSources.StandardInput/Windows7TouchAPIType
struct  Windows7TouchAPIType_t3091725811 
{
public:
	// System.Int32 TouchScript.InputSources.StandardInput/Windows7TouchAPIType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Windows7TouchAPIType_t3091725811, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WINDOWS7TOUCHAPITYPE_T3091725811_H
#ifndef U3CSORTHITSU3EC__ANONSTOREY0_T2253667628_H
#define U3CSORTHITSU3EC__ANONSTOREY0_T2253667628_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Layers.CameraLayer/<sortHits>c__AnonStorey0
struct  U3CsortHitsU3Ec__AnonStorey0_t2253667628  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 TouchScript.Layers.CameraLayer/<sortHits>c__AnonStorey0::cameraPos
	Vector3_t2243707580  ___cameraPos_0;

public:
	inline static int32_t get_offset_of_cameraPos_0() { return static_cast<int32_t>(offsetof(U3CsortHitsU3Ec__AnonStorey0_t2253667628, ___cameraPos_0)); }
	inline Vector3_t2243707580  get_cameraPos_0() const { return ___cameraPos_0; }
	inline Vector3_t2243707580 * get_address_of_cameraPos_0() { return &___cameraPos_0; }
	inline void set_cameraPos_0(Vector3_t2243707580  value)
	{
		___cameraPos_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSORTHITSU3EC__ANONSTOREY0_T2253667628_H
#ifndef WINDOWS8TOUCHAPITYPE_T4008807668_H
#define WINDOWS8TOUCHAPITYPE_T4008807668_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.InputSources.StandardInput/Windows8TouchAPIType
struct  Windows8TouchAPIType_t4008807668 
{
public:
	// System.Int32 TouchScript.InputSources.StandardInput/Windows8TouchAPIType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Windows8TouchAPIType_t4008807668, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WINDOWS8TOUCHAPITYPE_T4008807668_H
#ifndef CLOSESTATUSCODE_T2945181741_H
#define CLOSESTATUSCODE_T2945181741_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.CloseStatusCode
struct  CloseStatusCode_t2945181741 
{
public:
	// System.UInt16 WebSocketSharp.CloseStatusCode::value__
	uint16_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CloseStatusCode_t2945181741, ___value___1)); }
	inline uint16_t get_value___1() const { return ___value___1; }
	inline uint16_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint16_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLOSESTATUSCODE_T2945181741_H
#ifndef HTTPHEADERTYPE_T1518115223_H
#define HTTPHEADERTYPE_T1518115223_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.HttpHeaderType
struct  HttpHeaderType_t1518115223 
{
public:
	// System.Int32 WebSocketSharp.Net.HttpHeaderType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HttpHeaderType_t1518115223, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPHEADERTYPE_T1518115223_H
#ifndef CHUNKEDREQUESTSTREAM_T3311534059_H
#define CHUNKEDREQUESTSTREAM_T3311534059_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.ChunkedRequestStream
struct  ChunkedRequestStream_t3311534059  : public RequestStream_t775716369
{
public:
	// WebSocketSharp.Net.HttpListenerContext WebSocketSharp.Net.ChunkedRequestStream::_context
	HttpListenerContext_t994708409 * ____context_8;
	// WebSocketSharp.Net.ChunkStream WebSocketSharp.Net.ChunkedRequestStream::_decoder
	ChunkStream_t2067859643 * ____decoder_9;
	// System.Boolean WebSocketSharp.Net.ChunkedRequestStream::_disposed
	bool ____disposed_10;
	// System.Boolean WebSocketSharp.Net.ChunkedRequestStream::_noMoreData
	bool ____noMoreData_11;

public:
	inline static int32_t get_offset_of__context_8() { return static_cast<int32_t>(offsetof(ChunkedRequestStream_t3311534059, ____context_8)); }
	inline HttpListenerContext_t994708409 * get__context_8() const { return ____context_8; }
	inline HttpListenerContext_t994708409 ** get_address_of__context_8() { return &____context_8; }
	inline void set__context_8(HttpListenerContext_t994708409 * value)
	{
		____context_8 = value;
		Il2CppCodeGenWriteBarrier((&____context_8), value);
	}

	inline static int32_t get_offset_of__decoder_9() { return static_cast<int32_t>(offsetof(ChunkedRequestStream_t3311534059, ____decoder_9)); }
	inline ChunkStream_t2067859643 * get__decoder_9() const { return ____decoder_9; }
	inline ChunkStream_t2067859643 ** get_address_of__decoder_9() { return &____decoder_9; }
	inline void set__decoder_9(ChunkStream_t2067859643 * value)
	{
		____decoder_9 = value;
		Il2CppCodeGenWriteBarrier((&____decoder_9), value);
	}

	inline static int32_t get_offset_of__disposed_10() { return static_cast<int32_t>(offsetof(ChunkedRequestStream_t3311534059, ____disposed_10)); }
	inline bool get__disposed_10() const { return ____disposed_10; }
	inline bool* get_address_of__disposed_10() { return &____disposed_10; }
	inline void set__disposed_10(bool value)
	{
		____disposed_10 = value;
	}

	inline static int32_t get_offset_of__noMoreData_11() { return static_cast<int32_t>(offsetof(ChunkedRequestStream_t3311534059, ____noMoreData_11)); }
	inline bool get__noMoreData_11() const { return ____noMoreData_11; }
	inline bool* get_address_of__noMoreData_11() { return &____noMoreData_11; }
	inline void set__noMoreData_11(bool value)
	{
		____noMoreData_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHUNKEDREQUESTSTREAM_T3311534059_H
#ifndef LAYERHITRESULT_T1590288664_H
#define LAYERHITRESULT_T1590288664_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Layers.TouchLayer/LayerHitResult
struct  LayerHitResult_t1590288664 
{
public:
	// System.Int32 TouchScript.Layers.TouchLayer/LayerHitResult::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LayerHitResult_t1590288664, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERHITRESULT_T1590288664_H
#ifndef LAYERTYPE_T777199562_H
#define LAYERTYPE_T777199562_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Layers.FullscreenLayer/LayerType
struct  LayerType_t777199562 
{
public:
	// System.Int32 TouchScript.Layers.FullscreenLayer/LayerType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LayerType_t777199562, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERTYPE_T777199562_H
#ifndef WIN32EXCEPTION_T1708275760_H
#define WIN32EXCEPTION_T1708275760_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.Win32Exception
struct  Win32Exception_t1708275760  : public ExternalException_t1252662682
{
public:
	// System.Int32 System.ComponentModel.Win32Exception::native_error_code
	int32_t ___native_error_code_11;

public:
	inline static int32_t get_offset_of_native_error_code_11() { return static_cast<int32_t>(offsetof(Win32Exception_t1708275760, ___native_error_code_11)); }
	inline int32_t get_native_error_code_11() const { return ___native_error_code_11; }
	inline int32_t* get_address_of_native_error_code_11() { return &___native_error_code_11; }
	inline void set_native_error_code_11(int32_t value)
	{
		___native_error_code_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WIN32EXCEPTION_T1708275760_H
#ifndef DATETIME_T693205669_H
#define DATETIME_T693205669_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t693205669 
{
public:
	// System.TimeSpan System.DateTime::ticks
	TimeSpan_t3430258949  ___ticks_10;
	// System.DateTimeKind System.DateTime::kind
	int32_t ___kind_11;

public:
	inline static int32_t get_offset_of_ticks_10() { return static_cast<int32_t>(offsetof(DateTime_t693205669, ___ticks_10)); }
	inline TimeSpan_t3430258949  get_ticks_10() const { return ___ticks_10; }
	inline TimeSpan_t3430258949 * get_address_of_ticks_10() { return &___ticks_10; }
	inline void set_ticks_10(TimeSpan_t3430258949  value)
	{
		___ticks_10 = value;
	}

	inline static int32_t get_offset_of_kind_11() { return static_cast<int32_t>(offsetof(DateTime_t693205669, ___kind_11)); }
	inline int32_t get_kind_11() const { return ___kind_11; }
	inline int32_t* get_address_of_kind_11() { return &___kind_11; }
	inline void set_kind_11(int32_t value)
	{
		___kind_11 = value;
	}
};

struct DateTime_t693205669_StaticFields
{
public:
	// System.DateTime System.DateTime::MaxValue
	DateTime_t693205669  ___MaxValue_12;
	// System.DateTime System.DateTime::MinValue
	DateTime_t693205669  ___MinValue_13;
	// System.String[] System.DateTime::ParseTimeFormats
	StringU5BU5D_t1642385972* ___ParseTimeFormats_14;
	// System.String[] System.DateTime::ParseYearDayMonthFormats
	StringU5BU5D_t1642385972* ___ParseYearDayMonthFormats_15;
	// System.String[] System.DateTime::ParseYearMonthDayFormats
	StringU5BU5D_t1642385972* ___ParseYearMonthDayFormats_16;
	// System.String[] System.DateTime::ParseDayMonthYearFormats
	StringU5BU5D_t1642385972* ___ParseDayMonthYearFormats_17;
	// System.String[] System.DateTime::ParseMonthDayYearFormats
	StringU5BU5D_t1642385972* ___ParseMonthDayYearFormats_18;
	// System.String[] System.DateTime::MonthDayShortFormats
	StringU5BU5D_t1642385972* ___MonthDayShortFormats_19;
	// System.String[] System.DateTime::DayMonthShortFormats
	StringU5BU5D_t1642385972* ___DayMonthShortFormats_20;
	// System.Int32[] System.DateTime::daysmonth
	Int32U5BU5D_t3030399641* ___daysmonth_21;
	// System.Int32[] System.DateTime::daysmonthleap
	Int32U5BU5D_t3030399641* ___daysmonthleap_22;
	// System.Object System.DateTime::to_local_time_span_object
	RuntimeObject * ___to_local_time_span_object_23;
	// System.Int64 System.DateTime::last_now
	int64_t ___last_now_24;

public:
	inline static int32_t get_offset_of_MaxValue_12() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___MaxValue_12)); }
	inline DateTime_t693205669  get_MaxValue_12() const { return ___MaxValue_12; }
	inline DateTime_t693205669 * get_address_of_MaxValue_12() { return &___MaxValue_12; }
	inline void set_MaxValue_12(DateTime_t693205669  value)
	{
		___MaxValue_12 = value;
	}

	inline static int32_t get_offset_of_MinValue_13() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___MinValue_13)); }
	inline DateTime_t693205669  get_MinValue_13() const { return ___MinValue_13; }
	inline DateTime_t693205669 * get_address_of_MinValue_13() { return &___MinValue_13; }
	inline void set_MinValue_13(DateTime_t693205669  value)
	{
		___MinValue_13 = value;
	}

	inline static int32_t get_offset_of_ParseTimeFormats_14() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseTimeFormats_14)); }
	inline StringU5BU5D_t1642385972* get_ParseTimeFormats_14() const { return ___ParseTimeFormats_14; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseTimeFormats_14() { return &___ParseTimeFormats_14; }
	inline void set_ParseTimeFormats_14(StringU5BU5D_t1642385972* value)
	{
		___ParseTimeFormats_14 = value;
		Il2CppCodeGenWriteBarrier((&___ParseTimeFormats_14), value);
	}

	inline static int32_t get_offset_of_ParseYearDayMonthFormats_15() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseYearDayMonthFormats_15)); }
	inline StringU5BU5D_t1642385972* get_ParseYearDayMonthFormats_15() const { return ___ParseYearDayMonthFormats_15; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseYearDayMonthFormats_15() { return &___ParseYearDayMonthFormats_15; }
	inline void set_ParseYearDayMonthFormats_15(StringU5BU5D_t1642385972* value)
	{
		___ParseYearDayMonthFormats_15 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearDayMonthFormats_15), value);
	}

	inline static int32_t get_offset_of_ParseYearMonthDayFormats_16() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseYearMonthDayFormats_16)); }
	inline StringU5BU5D_t1642385972* get_ParseYearMonthDayFormats_16() const { return ___ParseYearMonthDayFormats_16; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseYearMonthDayFormats_16() { return &___ParseYearMonthDayFormats_16; }
	inline void set_ParseYearMonthDayFormats_16(StringU5BU5D_t1642385972* value)
	{
		___ParseYearMonthDayFormats_16 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearMonthDayFormats_16), value);
	}

	inline static int32_t get_offset_of_ParseDayMonthYearFormats_17() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseDayMonthYearFormats_17)); }
	inline StringU5BU5D_t1642385972* get_ParseDayMonthYearFormats_17() const { return ___ParseDayMonthYearFormats_17; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseDayMonthYearFormats_17() { return &___ParseDayMonthYearFormats_17; }
	inline void set_ParseDayMonthYearFormats_17(StringU5BU5D_t1642385972* value)
	{
		___ParseDayMonthYearFormats_17 = value;
		Il2CppCodeGenWriteBarrier((&___ParseDayMonthYearFormats_17), value);
	}

	inline static int32_t get_offset_of_ParseMonthDayYearFormats_18() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseMonthDayYearFormats_18)); }
	inline StringU5BU5D_t1642385972* get_ParseMonthDayYearFormats_18() const { return ___ParseMonthDayYearFormats_18; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseMonthDayYearFormats_18() { return &___ParseMonthDayYearFormats_18; }
	inline void set_ParseMonthDayYearFormats_18(StringU5BU5D_t1642385972* value)
	{
		___ParseMonthDayYearFormats_18 = value;
		Il2CppCodeGenWriteBarrier((&___ParseMonthDayYearFormats_18), value);
	}

	inline static int32_t get_offset_of_MonthDayShortFormats_19() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___MonthDayShortFormats_19)); }
	inline StringU5BU5D_t1642385972* get_MonthDayShortFormats_19() const { return ___MonthDayShortFormats_19; }
	inline StringU5BU5D_t1642385972** get_address_of_MonthDayShortFormats_19() { return &___MonthDayShortFormats_19; }
	inline void set_MonthDayShortFormats_19(StringU5BU5D_t1642385972* value)
	{
		___MonthDayShortFormats_19 = value;
		Il2CppCodeGenWriteBarrier((&___MonthDayShortFormats_19), value);
	}

	inline static int32_t get_offset_of_DayMonthShortFormats_20() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___DayMonthShortFormats_20)); }
	inline StringU5BU5D_t1642385972* get_DayMonthShortFormats_20() const { return ___DayMonthShortFormats_20; }
	inline StringU5BU5D_t1642385972** get_address_of_DayMonthShortFormats_20() { return &___DayMonthShortFormats_20; }
	inline void set_DayMonthShortFormats_20(StringU5BU5D_t1642385972* value)
	{
		___DayMonthShortFormats_20 = value;
		Il2CppCodeGenWriteBarrier((&___DayMonthShortFormats_20), value);
	}

	inline static int32_t get_offset_of_daysmonth_21() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___daysmonth_21)); }
	inline Int32U5BU5D_t3030399641* get_daysmonth_21() const { return ___daysmonth_21; }
	inline Int32U5BU5D_t3030399641** get_address_of_daysmonth_21() { return &___daysmonth_21; }
	inline void set_daysmonth_21(Int32U5BU5D_t3030399641* value)
	{
		___daysmonth_21 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonth_21), value);
	}

	inline static int32_t get_offset_of_daysmonthleap_22() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___daysmonthleap_22)); }
	inline Int32U5BU5D_t3030399641* get_daysmonthleap_22() const { return ___daysmonthleap_22; }
	inline Int32U5BU5D_t3030399641** get_address_of_daysmonthleap_22() { return &___daysmonthleap_22; }
	inline void set_daysmonthleap_22(Int32U5BU5D_t3030399641* value)
	{
		___daysmonthleap_22 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonthleap_22), value);
	}

	inline static int32_t get_offset_of_to_local_time_span_object_23() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___to_local_time_span_object_23)); }
	inline RuntimeObject * get_to_local_time_span_object_23() const { return ___to_local_time_span_object_23; }
	inline RuntimeObject ** get_address_of_to_local_time_span_object_23() { return &___to_local_time_span_object_23; }
	inline void set_to_local_time_span_object_23(RuntimeObject * value)
	{
		___to_local_time_span_object_23 = value;
		Il2CppCodeGenWriteBarrier((&___to_local_time_span_object_23), value);
	}

	inline static int32_t get_offset_of_last_now_24() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___last_now_24)); }
	inline int64_t get_last_now_24() const { return ___last_now_24; }
	inline int64_t* get_address_of_last_now_24() { return &___last_now_24; }
	inline void set_last_now_24(int64_t value)
	{
		___last_now_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T693205669_H
#ifndef HTTPLISTENER_T4179429670_H
#define HTTPLISTENER_T4179429670_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.HttpListener
struct  HttpListener_t4179429670  : public RuntimeObject
{
public:
	// WebSocketSharp.Net.AuthenticationSchemes WebSocketSharp.Net.HttpListener::_authSchemes
	int32_t ____authSchemes_0;
	// System.Func`2<WebSocketSharp.Net.HttpListenerRequest,WebSocketSharp.Net.AuthenticationSchemes> WebSocketSharp.Net.HttpListener::_authSchemeSelector
	Func_2_t1102535652 * ____authSchemeSelector_1;
	// System.String WebSocketSharp.Net.HttpListener::_certFolderPath
	String_t* ____certFolderPath_2;
	// System.Collections.Generic.Dictionary`2<WebSocketSharp.Net.HttpConnection,WebSocketSharp.Net.HttpConnection> WebSocketSharp.Net.HttpListener::_connections
	Dictionary_2_t3932468307 * ____connections_3;
	// System.Object WebSocketSharp.Net.HttpListener::_connectionsSync
	RuntimeObject * ____connectionsSync_4;
	// System.Collections.Generic.List`1<WebSocketSharp.Net.HttpListenerContext> WebSocketSharp.Net.HttpListener::_ctxQueue
	List_1_t363829541 * ____ctxQueue_5;
	// System.Object WebSocketSharp.Net.HttpListener::_ctxQueueSync
	RuntimeObject * ____ctxQueueSync_6;
	// System.Collections.Generic.Dictionary`2<WebSocketSharp.Net.HttpListenerContext,WebSocketSharp.Net.HttpListenerContext> WebSocketSharp.Net.HttpListener::_ctxRegistry
	Dictionary_2_t2381547847 * ____ctxRegistry_7;
	// System.Object WebSocketSharp.Net.HttpListener::_ctxRegistrySync
	RuntimeObject * ____ctxRegistrySync_8;
	// System.Boolean WebSocketSharp.Net.HttpListener::_disposed
	bool ____disposed_10;
	// System.Boolean WebSocketSharp.Net.HttpListener::_ignoreWriteExceptions
	bool ____ignoreWriteExceptions_11;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) WebSocketSharp.Net.HttpListener::_listening
	bool ____listening_12;
	// WebSocketSharp.Logger WebSocketSharp.Net.HttpListener::_logger
	Logger_t2598199114 * ____logger_13;
	// WebSocketSharp.Net.HttpListenerPrefixCollection WebSocketSharp.Net.HttpListener::_prefixes
	HttpListenerPrefixCollection_t3269177542 * ____prefixes_14;
	// System.String WebSocketSharp.Net.HttpListener::_realm
	String_t* ____realm_15;
	// System.Boolean WebSocketSharp.Net.HttpListener::_reuseAddress
	bool ____reuseAddress_16;
	// WebSocketSharp.Net.ServerSslConfiguration WebSocketSharp.Net.HttpListener::_sslConfig
	ServerSslConfiguration_t204724213 * ____sslConfig_17;
	// System.Func`2<System.Security.Principal.IIdentity,WebSocketSharp.Net.NetworkCredential> WebSocketSharp.Net.HttpListener::_userCredFinder
	Func_2_t353436505 * ____userCredFinder_18;
	// System.Collections.Generic.List`1<WebSocketSharp.Net.HttpListenerAsyncResult> WebSocketSharp.Net.HttpListener::_waitQueue
	List_1_t2876060817 * ____waitQueue_19;
	// System.Object WebSocketSharp.Net.HttpListener::_waitQueueSync
	RuntimeObject * ____waitQueueSync_20;

public:
	inline static int32_t get_offset_of__authSchemes_0() { return static_cast<int32_t>(offsetof(HttpListener_t4179429670, ____authSchemes_0)); }
	inline int32_t get__authSchemes_0() const { return ____authSchemes_0; }
	inline int32_t* get_address_of__authSchemes_0() { return &____authSchemes_0; }
	inline void set__authSchemes_0(int32_t value)
	{
		____authSchemes_0 = value;
	}

	inline static int32_t get_offset_of__authSchemeSelector_1() { return static_cast<int32_t>(offsetof(HttpListener_t4179429670, ____authSchemeSelector_1)); }
	inline Func_2_t1102535652 * get__authSchemeSelector_1() const { return ____authSchemeSelector_1; }
	inline Func_2_t1102535652 ** get_address_of__authSchemeSelector_1() { return &____authSchemeSelector_1; }
	inline void set__authSchemeSelector_1(Func_2_t1102535652 * value)
	{
		____authSchemeSelector_1 = value;
		Il2CppCodeGenWriteBarrier((&____authSchemeSelector_1), value);
	}

	inline static int32_t get_offset_of__certFolderPath_2() { return static_cast<int32_t>(offsetof(HttpListener_t4179429670, ____certFolderPath_2)); }
	inline String_t* get__certFolderPath_2() const { return ____certFolderPath_2; }
	inline String_t** get_address_of__certFolderPath_2() { return &____certFolderPath_2; }
	inline void set__certFolderPath_2(String_t* value)
	{
		____certFolderPath_2 = value;
		Il2CppCodeGenWriteBarrier((&____certFolderPath_2), value);
	}

	inline static int32_t get_offset_of__connections_3() { return static_cast<int32_t>(offsetof(HttpListener_t4179429670, ____connections_3)); }
	inline Dictionary_2_t3932468307 * get__connections_3() const { return ____connections_3; }
	inline Dictionary_2_t3932468307 ** get_address_of__connections_3() { return &____connections_3; }
	inline void set__connections_3(Dictionary_2_t3932468307 * value)
	{
		____connections_3 = value;
		Il2CppCodeGenWriteBarrier((&____connections_3), value);
	}

	inline static int32_t get_offset_of__connectionsSync_4() { return static_cast<int32_t>(offsetof(HttpListener_t4179429670, ____connectionsSync_4)); }
	inline RuntimeObject * get__connectionsSync_4() const { return ____connectionsSync_4; }
	inline RuntimeObject ** get_address_of__connectionsSync_4() { return &____connectionsSync_4; }
	inline void set__connectionsSync_4(RuntimeObject * value)
	{
		____connectionsSync_4 = value;
		Il2CppCodeGenWriteBarrier((&____connectionsSync_4), value);
	}

	inline static int32_t get_offset_of__ctxQueue_5() { return static_cast<int32_t>(offsetof(HttpListener_t4179429670, ____ctxQueue_5)); }
	inline List_1_t363829541 * get__ctxQueue_5() const { return ____ctxQueue_5; }
	inline List_1_t363829541 ** get_address_of__ctxQueue_5() { return &____ctxQueue_5; }
	inline void set__ctxQueue_5(List_1_t363829541 * value)
	{
		____ctxQueue_5 = value;
		Il2CppCodeGenWriteBarrier((&____ctxQueue_5), value);
	}

	inline static int32_t get_offset_of__ctxQueueSync_6() { return static_cast<int32_t>(offsetof(HttpListener_t4179429670, ____ctxQueueSync_6)); }
	inline RuntimeObject * get__ctxQueueSync_6() const { return ____ctxQueueSync_6; }
	inline RuntimeObject ** get_address_of__ctxQueueSync_6() { return &____ctxQueueSync_6; }
	inline void set__ctxQueueSync_6(RuntimeObject * value)
	{
		____ctxQueueSync_6 = value;
		Il2CppCodeGenWriteBarrier((&____ctxQueueSync_6), value);
	}

	inline static int32_t get_offset_of__ctxRegistry_7() { return static_cast<int32_t>(offsetof(HttpListener_t4179429670, ____ctxRegistry_7)); }
	inline Dictionary_2_t2381547847 * get__ctxRegistry_7() const { return ____ctxRegistry_7; }
	inline Dictionary_2_t2381547847 ** get_address_of__ctxRegistry_7() { return &____ctxRegistry_7; }
	inline void set__ctxRegistry_7(Dictionary_2_t2381547847 * value)
	{
		____ctxRegistry_7 = value;
		Il2CppCodeGenWriteBarrier((&____ctxRegistry_7), value);
	}

	inline static int32_t get_offset_of__ctxRegistrySync_8() { return static_cast<int32_t>(offsetof(HttpListener_t4179429670, ____ctxRegistrySync_8)); }
	inline RuntimeObject * get__ctxRegistrySync_8() const { return ____ctxRegistrySync_8; }
	inline RuntimeObject ** get_address_of__ctxRegistrySync_8() { return &____ctxRegistrySync_8; }
	inline void set__ctxRegistrySync_8(RuntimeObject * value)
	{
		____ctxRegistrySync_8 = value;
		Il2CppCodeGenWriteBarrier((&____ctxRegistrySync_8), value);
	}

	inline static int32_t get_offset_of__disposed_10() { return static_cast<int32_t>(offsetof(HttpListener_t4179429670, ____disposed_10)); }
	inline bool get__disposed_10() const { return ____disposed_10; }
	inline bool* get_address_of__disposed_10() { return &____disposed_10; }
	inline void set__disposed_10(bool value)
	{
		____disposed_10 = value;
	}

	inline static int32_t get_offset_of__ignoreWriteExceptions_11() { return static_cast<int32_t>(offsetof(HttpListener_t4179429670, ____ignoreWriteExceptions_11)); }
	inline bool get__ignoreWriteExceptions_11() const { return ____ignoreWriteExceptions_11; }
	inline bool* get_address_of__ignoreWriteExceptions_11() { return &____ignoreWriteExceptions_11; }
	inline void set__ignoreWriteExceptions_11(bool value)
	{
		____ignoreWriteExceptions_11 = value;
	}

	inline static int32_t get_offset_of__listening_12() { return static_cast<int32_t>(offsetof(HttpListener_t4179429670, ____listening_12)); }
	inline bool get__listening_12() const { return ____listening_12; }
	inline bool* get_address_of__listening_12() { return &____listening_12; }
	inline void set__listening_12(bool value)
	{
		____listening_12 = value;
	}

	inline static int32_t get_offset_of__logger_13() { return static_cast<int32_t>(offsetof(HttpListener_t4179429670, ____logger_13)); }
	inline Logger_t2598199114 * get__logger_13() const { return ____logger_13; }
	inline Logger_t2598199114 ** get_address_of__logger_13() { return &____logger_13; }
	inline void set__logger_13(Logger_t2598199114 * value)
	{
		____logger_13 = value;
		Il2CppCodeGenWriteBarrier((&____logger_13), value);
	}

	inline static int32_t get_offset_of__prefixes_14() { return static_cast<int32_t>(offsetof(HttpListener_t4179429670, ____prefixes_14)); }
	inline HttpListenerPrefixCollection_t3269177542 * get__prefixes_14() const { return ____prefixes_14; }
	inline HttpListenerPrefixCollection_t3269177542 ** get_address_of__prefixes_14() { return &____prefixes_14; }
	inline void set__prefixes_14(HttpListenerPrefixCollection_t3269177542 * value)
	{
		____prefixes_14 = value;
		Il2CppCodeGenWriteBarrier((&____prefixes_14), value);
	}

	inline static int32_t get_offset_of__realm_15() { return static_cast<int32_t>(offsetof(HttpListener_t4179429670, ____realm_15)); }
	inline String_t* get__realm_15() const { return ____realm_15; }
	inline String_t** get_address_of__realm_15() { return &____realm_15; }
	inline void set__realm_15(String_t* value)
	{
		____realm_15 = value;
		Il2CppCodeGenWriteBarrier((&____realm_15), value);
	}

	inline static int32_t get_offset_of__reuseAddress_16() { return static_cast<int32_t>(offsetof(HttpListener_t4179429670, ____reuseAddress_16)); }
	inline bool get__reuseAddress_16() const { return ____reuseAddress_16; }
	inline bool* get_address_of__reuseAddress_16() { return &____reuseAddress_16; }
	inline void set__reuseAddress_16(bool value)
	{
		____reuseAddress_16 = value;
	}

	inline static int32_t get_offset_of__sslConfig_17() { return static_cast<int32_t>(offsetof(HttpListener_t4179429670, ____sslConfig_17)); }
	inline ServerSslConfiguration_t204724213 * get__sslConfig_17() const { return ____sslConfig_17; }
	inline ServerSslConfiguration_t204724213 ** get_address_of__sslConfig_17() { return &____sslConfig_17; }
	inline void set__sslConfig_17(ServerSslConfiguration_t204724213 * value)
	{
		____sslConfig_17 = value;
		Il2CppCodeGenWriteBarrier((&____sslConfig_17), value);
	}

	inline static int32_t get_offset_of__userCredFinder_18() { return static_cast<int32_t>(offsetof(HttpListener_t4179429670, ____userCredFinder_18)); }
	inline Func_2_t353436505 * get__userCredFinder_18() const { return ____userCredFinder_18; }
	inline Func_2_t353436505 ** get_address_of__userCredFinder_18() { return &____userCredFinder_18; }
	inline void set__userCredFinder_18(Func_2_t353436505 * value)
	{
		____userCredFinder_18 = value;
		Il2CppCodeGenWriteBarrier((&____userCredFinder_18), value);
	}

	inline static int32_t get_offset_of__waitQueue_19() { return static_cast<int32_t>(offsetof(HttpListener_t4179429670, ____waitQueue_19)); }
	inline List_1_t2876060817 * get__waitQueue_19() const { return ____waitQueue_19; }
	inline List_1_t2876060817 ** get_address_of__waitQueue_19() { return &____waitQueue_19; }
	inline void set__waitQueue_19(List_1_t2876060817 * value)
	{
		____waitQueue_19 = value;
		Il2CppCodeGenWriteBarrier((&____waitQueue_19), value);
	}

	inline static int32_t get_offset_of__waitQueueSync_20() { return static_cast<int32_t>(offsetof(HttpListener_t4179429670, ____waitQueueSync_20)); }
	inline RuntimeObject * get__waitQueueSync_20() const { return ____waitQueueSync_20; }
	inline RuntimeObject ** get_address_of__waitQueueSync_20() { return &____waitQueueSync_20; }
	inline void set__waitQueueSync_20(RuntimeObject * value)
	{
		____waitQueueSync_20 = value;
		Il2CppCodeGenWriteBarrier((&____waitQueueSync_20), value);
	}
};

struct HttpListener_t4179429670_StaticFields
{
public:
	// System.String WebSocketSharp.Net.HttpListener::_defaultRealm
	String_t* ____defaultRealm_9;

public:
	inline static int32_t get_offset_of__defaultRealm_9() { return static_cast<int32_t>(offsetof(HttpListener_t4179429670_StaticFields, ____defaultRealm_9)); }
	inline String_t* get__defaultRealm_9() const { return ____defaultRealm_9; }
	inline String_t** get_address_of__defaultRealm_9() { return &____defaultRealm_9; }
	inline void set__defaultRealm_9(String_t* value)
	{
		____defaultRealm_9 = value;
		Il2CppCodeGenWriteBarrier((&____defaultRealm_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPLISTENER_T4179429670_H
#ifndef COMPONENT_T3819376471_H
#define COMPONENT_T3819376471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t3819376471  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T3819376471_H
#ifndef SSLCONFIGURATION_T760772650_H
#define SSLCONFIGURATION_T760772650_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.SslConfiguration
struct  SslConfiguration_t760772650  : public RuntimeObject
{
public:
	// System.Net.Security.LocalCertificateSelectionCallback WebSocketSharp.Net.SslConfiguration::_certSelectionCallback
	LocalCertificateSelectionCallback_t3696771181 * ____certSelectionCallback_0;
	// System.Net.Security.RemoteCertificateValidationCallback WebSocketSharp.Net.SslConfiguration::_certValidationCallback
	RemoteCertificateValidationCallback_t2756269959 * ____certValidationCallback_1;
	// System.Boolean WebSocketSharp.Net.SslConfiguration::_checkCertRevocation
	bool ____checkCertRevocation_2;
	// System.Security.Authentication.SslProtocols WebSocketSharp.Net.SslConfiguration::_enabledProtocols
	int32_t ____enabledProtocols_3;

public:
	inline static int32_t get_offset_of__certSelectionCallback_0() { return static_cast<int32_t>(offsetof(SslConfiguration_t760772650, ____certSelectionCallback_0)); }
	inline LocalCertificateSelectionCallback_t3696771181 * get__certSelectionCallback_0() const { return ____certSelectionCallback_0; }
	inline LocalCertificateSelectionCallback_t3696771181 ** get_address_of__certSelectionCallback_0() { return &____certSelectionCallback_0; }
	inline void set__certSelectionCallback_0(LocalCertificateSelectionCallback_t3696771181 * value)
	{
		____certSelectionCallback_0 = value;
		Il2CppCodeGenWriteBarrier((&____certSelectionCallback_0), value);
	}

	inline static int32_t get_offset_of__certValidationCallback_1() { return static_cast<int32_t>(offsetof(SslConfiguration_t760772650, ____certValidationCallback_1)); }
	inline RemoteCertificateValidationCallback_t2756269959 * get__certValidationCallback_1() const { return ____certValidationCallback_1; }
	inline RemoteCertificateValidationCallback_t2756269959 ** get_address_of__certValidationCallback_1() { return &____certValidationCallback_1; }
	inline void set__certValidationCallback_1(RemoteCertificateValidationCallback_t2756269959 * value)
	{
		____certValidationCallback_1 = value;
		Il2CppCodeGenWriteBarrier((&____certValidationCallback_1), value);
	}

	inline static int32_t get_offset_of__checkCertRevocation_2() { return static_cast<int32_t>(offsetof(SslConfiguration_t760772650, ____checkCertRevocation_2)); }
	inline bool get__checkCertRevocation_2() const { return ____checkCertRevocation_2; }
	inline bool* get_address_of__checkCertRevocation_2() { return &____checkCertRevocation_2; }
	inline void set__checkCertRevocation_2(bool value)
	{
		____checkCertRevocation_2 = value;
	}

	inline static int32_t get_offset_of__enabledProtocols_3() { return static_cast<int32_t>(offsetof(SslConfiguration_t760772650, ____enabledProtocols_3)); }
	inline int32_t get__enabledProtocols_3() const { return ____enabledProtocols_3; }
	inline int32_t* get_address_of__enabledProtocols_3() { return &____enabledProtocols_3; }
	inline void set__enabledProtocols_3(int32_t value)
	{
		____enabledProtocols_3 = value;
	}
};

struct SslConfiguration_t760772650_StaticFields
{
public:
	// System.Net.Security.LocalCertificateSelectionCallback WebSocketSharp.Net.SslConfiguration::<>f__am$cache0
	LocalCertificateSelectionCallback_t3696771181 * ___U3CU3Ef__amU24cache0_4;
	// System.Net.Security.RemoteCertificateValidationCallback WebSocketSharp.Net.SslConfiguration::<>f__am$cache1
	RemoteCertificateValidationCallback_t2756269959 * ___U3CU3Ef__amU24cache1_5;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_4() { return static_cast<int32_t>(offsetof(SslConfiguration_t760772650_StaticFields, ___U3CU3Ef__amU24cache0_4)); }
	inline LocalCertificateSelectionCallback_t3696771181 * get_U3CU3Ef__amU24cache0_4() const { return ___U3CU3Ef__amU24cache0_4; }
	inline LocalCertificateSelectionCallback_t3696771181 ** get_address_of_U3CU3Ef__amU24cache0_4() { return &___U3CU3Ef__amU24cache0_4; }
	inline void set_U3CU3Ef__amU24cache0_4(LocalCertificateSelectionCallback_t3696771181 * value)
	{
		___U3CU3Ef__amU24cache0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_5() { return static_cast<int32_t>(offsetof(SslConfiguration_t760772650_StaticFields, ___U3CU3Ef__amU24cache1_5)); }
	inline RemoteCertificateValidationCallback_t2756269959 * get_U3CU3Ef__amU24cache1_5() const { return ___U3CU3Ef__amU24cache1_5; }
	inline RemoteCertificateValidationCallback_t2756269959 ** get_address_of_U3CU3Ef__amU24cache1_5() { return &___U3CU3Ef__amU24cache1_5; }
	inline void set_U3CU3Ef__amU24cache1_5(RemoteCertificateValidationCallback_t2756269959 * value)
	{
		___U3CU3Ef__amU24cache1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SSLCONFIGURATION_T760772650_H
#ifndef CANVASPROJECTIONPARAMS_T880996221_H
#define CANVASPROJECTIONPARAMS_T880996221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Layers.CanvasProjectionParams
struct  CanvasProjectionParams_t880996221  : public ProjectionParams_t2712959773
{
public:
	// UnityEngine.Canvas TouchScript.Layers.CanvasProjectionParams::canvas
	Canvas_t209405766 * ___canvas_0;
	// UnityEngine.RectTransform TouchScript.Layers.CanvasProjectionParams::rect
	RectTransform_t3349966182 * ___rect_1;
	// UnityEngine.RenderMode TouchScript.Layers.CanvasProjectionParams::mode
	int32_t ___mode_2;
	// UnityEngine.Camera TouchScript.Layers.CanvasProjectionParams::camera
	Camera_t189460977 * ___camera_3;

public:
	inline static int32_t get_offset_of_canvas_0() { return static_cast<int32_t>(offsetof(CanvasProjectionParams_t880996221, ___canvas_0)); }
	inline Canvas_t209405766 * get_canvas_0() const { return ___canvas_0; }
	inline Canvas_t209405766 ** get_address_of_canvas_0() { return &___canvas_0; }
	inline void set_canvas_0(Canvas_t209405766 * value)
	{
		___canvas_0 = value;
		Il2CppCodeGenWriteBarrier((&___canvas_0), value);
	}

	inline static int32_t get_offset_of_rect_1() { return static_cast<int32_t>(offsetof(CanvasProjectionParams_t880996221, ___rect_1)); }
	inline RectTransform_t3349966182 * get_rect_1() const { return ___rect_1; }
	inline RectTransform_t3349966182 ** get_address_of_rect_1() { return &___rect_1; }
	inline void set_rect_1(RectTransform_t3349966182 * value)
	{
		___rect_1 = value;
		Il2CppCodeGenWriteBarrier((&___rect_1), value);
	}

	inline static int32_t get_offset_of_mode_2() { return static_cast<int32_t>(offsetof(CanvasProjectionParams_t880996221, ___mode_2)); }
	inline int32_t get_mode_2() const { return ___mode_2; }
	inline int32_t* get_address_of_mode_2() { return &___mode_2; }
	inline void set_mode_2(int32_t value)
	{
		___mode_2 = value;
	}

	inline static int32_t get_offset_of_camera_3() { return static_cast<int32_t>(offsetof(CanvasProjectionParams_t880996221, ___camera_3)); }
	inline Camera_t189460977 * get_camera_3() const { return ___camera_3; }
	inline Camera_t189460977 ** get_address_of_camera_3() { return &___camera_3; }
	inline void set_camera_3(Camera_t189460977 * value)
	{
		___camera_3 = value;
		Il2CppCodeGenWriteBarrier((&___camera_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANVASPROJECTIONPARAMS_T880996221_H
#ifndef COOKIEEXCEPTION_T780982235_H
#define COOKIEEXCEPTION_T780982235_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.CookieException
struct  CookieException_t780982235  : public FormatException_t2948921286
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COOKIEEXCEPTION_T780982235_H
#ifndef AUTHENTICATIONBASE_T909684845_H
#define AUTHENTICATIONBASE_T909684845_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.AuthenticationBase
struct  AuthenticationBase_t909684845  : public RuntimeObject
{
public:
	// WebSocketSharp.Net.AuthenticationSchemes WebSocketSharp.Net.AuthenticationBase::_scheme
	int32_t ____scheme_0;
	// System.Collections.Specialized.NameValueCollection WebSocketSharp.Net.AuthenticationBase::Parameters
	NameValueCollection_t3047564564 * ___Parameters_1;

public:
	inline static int32_t get_offset_of__scheme_0() { return static_cast<int32_t>(offsetof(AuthenticationBase_t909684845, ____scheme_0)); }
	inline int32_t get__scheme_0() const { return ____scheme_0; }
	inline int32_t* get_address_of__scheme_0() { return &____scheme_0; }
	inline void set__scheme_0(int32_t value)
	{
		____scheme_0 = value;
	}

	inline static int32_t get_offset_of_Parameters_1() { return static_cast<int32_t>(offsetof(AuthenticationBase_t909684845, ___Parameters_1)); }
	inline NameValueCollection_t3047564564 * get_Parameters_1() const { return ___Parameters_1; }
	inline NameValueCollection_t3047564564 ** get_address_of_Parameters_1() { return &___Parameters_1; }
	inline void set_Parameters_1(NameValueCollection_t3047564564 * value)
	{
		___Parameters_1 = value;
		Il2CppCodeGenWriteBarrier((&___Parameters_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTHENTICATIONBASE_T909684845_H
#ifndef CHUNKSTREAM_T2067859643_H
#define CHUNKSTREAM_T2067859643_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.ChunkStream
struct  ChunkStream_t2067859643  : public RuntimeObject
{
public:
	// System.Int32 WebSocketSharp.Net.ChunkStream::_chunkRead
	int32_t ____chunkRead_0;
	// System.Int32 WebSocketSharp.Net.ChunkStream::_chunkSize
	int32_t ____chunkSize_1;
	// System.Collections.Generic.List`1<WebSocketSharp.Net.Chunk> WebSocketSharp.Net.ChunkStream::_chunks
	List_1_t1673048283 * ____chunks_2;
	// System.Boolean WebSocketSharp.Net.ChunkStream::_gotIt
	bool ____gotIt_3;
	// WebSocketSharp.Net.WebHeaderCollection WebSocketSharp.Net.ChunkStream::_headers
	WebHeaderCollection_t1932982249 * ____headers_4;
	// System.Text.StringBuilder WebSocketSharp.Net.ChunkStream::_saved
	StringBuilder_t1221177846 * ____saved_5;
	// System.Boolean WebSocketSharp.Net.ChunkStream::_sawCr
	bool ____sawCr_6;
	// WebSocketSharp.Net.InputChunkState WebSocketSharp.Net.ChunkStream::_state
	int32_t ____state_7;
	// System.Int32 WebSocketSharp.Net.ChunkStream::_trailerState
	int32_t ____trailerState_8;

public:
	inline static int32_t get_offset_of__chunkRead_0() { return static_cast<int32_t>(offsetof(ChunkStream_t2067859643, ____chunkRead_0)); }
	inline int32_t get__chunkRead_0() const { return ____chunkRead_0; }
	inline int32_t* get_address_of__chunkRead_0() { return &____chunkRead_0; }
	inline void set__chunkRead_0(int32_t value)
	{
		____chunkRead_0 = value;
	}

	inline static int32_t get_offset_of__chunkSize_1() { return static_cast<int32_t>(offsetof(ChunkStream_t2067859643, ____chunkSize_1)); }
	inline int32_t get__chunkSize_1() const { return ____chunkSize_1; }
	inline int32_t* get_address_of__chunkSize_1() { return &____chunkSize_1; }
	inline void set__chunkSize_1(int32_t value)
	{
		____chunkSize_1 = value;
	}

	inline static int32_t get_offset_of__chunks_2() { return static_cast<int32_t>(offsetof(ChunkStream_t2067859643, ____chunks_2)); }
	inline List_1_t1673048283 * get__chunks_2() const { return ____chunks_2; }
	inline List_1_t1673048283 ** get_address_of__chunks_2() { return &____chunks_2; }
	inline void set__chunks_2(List_1_t1673048283 * value)
	{
		____chunks_2 = value;
		Il2CppCodeGenWriteBarrier((&____chunks_2), value);
	}

	inline static int32_t get_offset_of__gotIt_3() { return static_cast<int32_t>(offsetof(ChunkStream_t2067859643, ____gotIt_3)); }
	inline bool get__gotIt_3() const { return ____gotIt_3; }
	inline bool* get_address_of__gotIt_3() { return &____gotIt_3; }
	inline void set__gotIt_3(bool value)
	{
		____gotIt_3 = value;
	}

	inline static int32_t get_offset_of__headers_4() { return static_cast<int32_t>(offsetof(ChunkStream_t2067859643, ____headers_4)); }
	inline WebHeaderCollection_t1932982249 * get__headers_4() const { return ____headers_4; }
	inline WebHeaderCollection_t1932982249 ** get_address_of__headers_4() { return &____headers_4; }
	inline void set__headers_4(WebHeaderCollection_t1932982249 * value)
	{
		____headers_4 = value;
		Il2CppCodeGenWriteBarrier((&____headers_4), value);
	}

	inline static int32_t get_offset_of__saved_5() { return static_cast<int32_t>(offsetof(ChunkStream_t2067859643, ____saved_5)); }
	inline StringBuilder_t1221177846 * get__saved_5() const { return ____saved_5; }
	inline StringBuilder_t1221177846 ** get_address_of__saved_5() { return &____saved_5; }
	inline void set__saved_5(StringBuilder_t1221177846 * value)
	{
		____saved_5 = value;
		Il2CppCodeGenWriteBarrier((&____saved_5), value);
	}

	inline static int32_t get_offset_of__sawCr_6() { return static_cast<int32_t>(offsetof(ChunkStream_t2067859643, ____sawCr_6)); }
	inline bool get__sawCr_6() const { return ____sawCr_6; }
	inline bool* get_address_of__sawCr_6() { return &____sawCr_6; }
	inline void set__sawCr_6(bool value)
	{
		____sawCr_6 = value;
	}

	inline static int32_t get_offset_of__state_7() { return static_cast<int32_t>(offsetof(ChunkStream_t2067859643, ____state_7)); }
	inline int32_t get__state_7() const { return ____state_7; }
	inline int32_t* get_address_of__state_7() { return &____state_7; }
	inline void set__state_7(int32_t value)
	{
		____state_7 = value;
	}

	inline static int32_t get_offset_of__trailerState_8() { return static_cast<int32_t>(offsetof(ChunkStream_t2067859643, ____trailerState_8)); }
	inline int32_t get__trailerState_8() const { return ____trailerState_8; }
	inline int32_t* get_address_of__trailerState_8() { return &____trailerState_8; }
	inline void set__trailerState_8(int32_t value)
	{
		____trailerState_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHUNKSTREAM_T2067859643_H
#ifndef HTTPCONNECTION_T2649486862_H
#define HTTPCONNECTION_T2649486862_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.HttpConnection
struct  HttpConnection_t2649486862  : public RuntimeObject
{
public:
	// System.Byte[] WebSocketSharp.Net.HttpConnection::_buffer
	ByteU5BU5D_t3397334013* ____buffer_0;
	// WebSocketSharp.Net.HttpListenerContext WebSocketSharp.Net.HttpConnection::_context
	HttpListenerContext_t994708409 * ____context_2;
	// System.Boolean WebSocketSharp.Net.HttpConnection::_contextRegistered
	bool ____contextRegistered_3;
	// System.Text.StringBuilder WebSocketSharp.Net.HttpConnection::_currentLine
	StringBuilder_t1221177846 * ____currentLine_4;
	// WebSocketSharp.Net.InputState WebSocketSharp.Net.HttpConnection::_inputState
	int32_t ____inputState_5;
	// WebSocketSharp.Net.RequestStream WebSocketSharp.Net.HttpConnection::_inputStream
	RequestStream_t775716369 * ____inputStream_6;
	// WebSocketSharp.Net.HttpListener WebSocketSharp.Net.HttpConnection::_lastListener
	HttpListener_t4179429670 * ____lastListener_7;
	// WebSocketSharp.Net.LineState WebSocketSharp.Net.HttpConnection::_lineState
	int32_t ____lineState_8;
	// WebSocketSharp.Net.EndPointListener WebSocketSharp.Net.HttpConnection::_listener
	EndPointListener_t3937551933 * ____listener_9;
	// WebSocketSharp.Net.ResponseStream WebSocketSharp.Net.HttpConnection::_outputStream
	ResponseStream_t3200689523 * ____outputStream_10;
	// System.Int32 WebSocketSharp.Net.HttpConnection::_position
	int32_t ____position_11;
	// WebSocketSharp.Net.HttpListenerPrefix WebSocketSharp.Net.HttpConnection::_prefix
	HttpListenerPrefix_t529778486 * ____prefix_12;
	// System.IO.MemoryStream WebSocketSharp.Net.HttpConnection::_requestBuffer
	MemoryStream_t743994179 * ____requestBuffer_13;
	// System.Int32 WebSocketSharp.Net.HttpConnection::_reuses
	int32_t ____reuses_14;
	// System.Boolean WebSocketSharp.Net.HttpConnection::_secure
	bool ____secure_15;
	// System.Net.Sockets.Socket WebSocketSharp.Net.HttpConnection::_socket
	Socket_t3821512045 * ____socket_16;
	// System.IO.Stream WebSocketSharp.Net.HttpConnection::_stream
	Stream_t3255436806 * ____stream_17;
	// System.Object WebSocketSharp.Net.HttpConnection::_sync
	RuntimeObject * ____sync_18;
	// System.Int32 WebSocketSharp.Net.HttpConnection::_timeout
	int32_t ____timeout_19;
	// System.Threading.Timer WebSocketSharp.Net.HttpConnection::_timer
	Timer_t791717973 * ____timer_20;

public:
	inline static int32_t get_offset_of__buffer_0() { return static_cast<int32_t>(offsetof(HttpConnection_t2649486862, ____buffer_0)); }
	inline ByteU5BU5D_t3397334013* get__buffer_0() const { return ____buffer_0; }
	inline ByteU5BU5D_t3397334013** get_address_of__buffer_0() { return &____buffer_0; }
	inline void set__buffer_0(ByteU5BU5D_t3397334013* value)
	{
		____buffer_0 = value;
		Il2CppCodeGenWriteBarrier((&____buffer_0), value);
	}

	inline static int32_t get_offset_of__context_2() { return static_cast<int32_t>(offsetof(HttpConnection_t2649486862, ____context_2)); }
	inline HttpListenerContext_t994708409 * get__context_2() const { return ____context_2; }
	inline HttpListenerContext_t994708409 ** get_address_of__context_2() { return &____context_2; }
	inline void set__context_2(HttpListenerContext_t994708409 * value)
	{
		____context_2 = value;
		Il2CppCodeGenWriteBarrier((&____context_2), value);
	}

	inline static int32_t get_offset_of__contextRegistered_3() { return static_cast<int32_t>(offsetof(HttpConnection_t2649486862, ____contextRegistered_3)); }
	inline bool get__contextRegistered_3() const { return ____contextRegistered_3; }
	inline bool* get_address_of__contextRegistered_3() { return &____contextRegistered_3; }
	inline void set__contextRegistered_3(bool value)
	{
		____contextRegistered_3 = value;
	}

	inline static int32_t get_offset_of__currentLine_4() { return static_cast<int32_t>(offsetof(HttpConnection_t2649486862, ____currentLine_4)); }
	inline StringBuilder_t1221177846 * get__currentLine_4() const { return ____currentLine_4; }
	inline StringBuilder_t1221177846 ** get_address_of__currentLine_4() { return &____currentLine_4; }
	inline void set__currentLine_4(StringBuilder_t1221177846 * value)
	{
		____currentLine_4 = value;
		Il2CppCodeGenWriteBarrier((&____currentLine_4), value);
	}

	inline static int32_t get_offset_of__inputState_5() { return static_cast<int32_t>(offsetof(HttpConnection_t2649486862, ____inputState_5)); }
	inline int32_t get__inputState_5() const { return ____inputState_5; }
	inline int32_t* get_address_of__inputState_5() { return &____inputState_5; }
	inline void set__inputState_5(int32_t value)
	{
		____inputState_5 = value;
	}

	inline static int32_t get_offset_of__inputStream_6() { return static_cast<int32_t>(offsetof(HttpConnection_t2649486862, ____inputStream_6)); }
	inline RequestStream_t775716369 * get__inputStream_6() const { return ____inputStream_6; }
	inline RequestStream_t775716369 ** get_address_of__inputStream_6() { return &____inputStream_6; }
	inline void set__inputStream_6(RequestStream_t775716369 * value)
	{
		____inputStream_6 = value;
		Il2CppCodeGenWriteBarrier((&____inputStream_6), value);
	}

	inline static int32_t get_offset_of__lastListener_7() { return static_cast<int32_t>(offsetof(HttpConnection_t2649486862, ____lastListener_7)); }
	inline HttpListener_t4179429670 * get__lastListener_7() const { return ____lastListener_7; }
	inline HttpListener_t4179429670 ** get_address_of__lastListener_7() { return &____lastListener_7; }
	inline void set__lastListener_7(HttpListener_t4179429670 * value)
	{
		____lastListener_7 = value;
		Il2CppCodeGenWriteBarrier((&____lastListener_7), value);
	}

	inline static int32_t get_offset_of__lineState_8() { return static_cast<int32_t>(offsetof(HttpConnection_t2649486862, ____lineState_8)); }
	inline int32_t get__lineState_8() const { return ____lineState_8; }
	inline int32_t* get_address_of__lineState_8() { return &____lineState_8; }
	inline void set__lineState_8(int32_t value)
	{
		____lineState_8 = value;
	}

	inline static int32_t get_offset_of__listener_9() { return static_cast<int32_t>(offsetof(HttpConnection_t2649486862, ____listener_9)); }
	inline EndPointListener_t3937551933 * get__listener_9() const { return ____listener_9; }
	inline EndPointListener_t3937551933 ** get_address_of__listener_9() { return &____listener_9; }
	inline void set__listener_9(EndPointListener_t3937551933 * value)
	{
		____listener_9 = value;
		Il2CppCodeGenWriteBarrier((&____listener_9), value);
	}

	inline static int32_t get_offset_of__outputStream_10() { return static_cast<int32_t>(offsetof(HttpConnection_t2649486862, ____outputStream_10)); }
	inline ResponseStream_t3200689523 * get__outputStream_10() const { return ____outputStream_10; }
	inline ResponseStream_t3200689523 ** get_address_of__outputStream_10() { return &____outputStream_10; }
	inline void set__outputStream_10(ResponseStream_t3200689523 * value)
	{
		____outputStream_10 = value;
		Il2CppCodeGenWriteBarrier((&____outputStream_10), value);
	}

	inline static int32_t get_offset_of__position_11() { return static_cast<int32_t>(offsetof(HttpConnection_t2649486862, ____position_11)); }
	inline int32_t get__position_11() const { return ____position_11; }
	inline int32_t* get_address_of__position_11() { return &____position_11; }
	inline void set__position_11(int32_t value)
	{
		____position_11 = value;
	}

	inline static int32_t get_offset_of__prefix_12() { return static_cast<int32_t>(offsetof(HttpConnection_t2649486862, ____prefix_12)); }
	inline HttpListenerPrefix_t529778486 * get__prefix_12() const { return ____prefix_12; }
	inline HttpListenerPrefix_t529778486 ** get_address_of__prefix_12() { return &____prefix_12; }
	inline void set__prefix_12(HttpListenerPrefix_t529778486 * value)
	{
		____prefix_12 = value;
		Il2CppCodeGenWriteBarrier((&____prefix_12), value);
	}

	inline static int32_t get_offset_of__requestBuffer_13() { return static_cast<int32_t>(offsetof(HttpConnection_t2649486862, ____requestBuffer_13)); }
	inline MemoryStream_t743994179 * get__requestBuffer_13() const { return ____requestBuffer_13; }
	inline MemoryStream_t743994179 ** get_address_of__requestBuffer_13() { return &____requestBuffer_13; }
	inline void set__requestBuffer_13(MemoryStream_t743994179 * value)
	{
		____requestBuffer_13 = value;
		Il2CppCodeGenWriteBarrier((&____requestBuffer_13), value);
	}

	inline static int32_t get_offset_of__reuses_14() { return static_cast<int32_t>(offsetof(HttpConnection_t2649486862, ____reuses_14)); }
	inline int32_t get__reuses_14() const { return ____reuses_14; }
	inline int32_t* get_address_of__reuses_14() { return &____reuses_14; }
	inline void set__reuses_14(int32_t value)
	{
		____reuses_14 = value;
	}

	inline static int32_t get_offset_of__secure_15() { return static_cast<int32_t>(offsetof(HttpConnection_t2649486862, ____secure_15)); }
	inline bool get__secure_15() const { return ____secure_15; }
	inline bool* get_address_of__secure_15() { return &____secure_15; }
	inline void set__secure_15(bool value)
	{
		____secure_15 = value;
	}

	inline static int32_t get_offset_of__socket_16() { return static_cast<int32_t>(offsetof(HttpConnection_t2649486862, ____socket_16)); }
	inline Socket_t3821512045 * get__socket_16() const { return ____socket_16; }
	inline Socket_t3821512045 ** get_address_of__socket_16() { return &____socket_16; }
	inline void set__socket_16(Socket_t3821512045 * value)
	{
		____socket_16 = value;
		Il2CppCodeGenWriteBarrier((&____socket_16), value);
	}

	inline static int32_t get_offset_of__stream_17() { return static_cast<int32_t>(offsetof(HttpConnection_t2649486862, ____stream_17)); }
	inline Stream_t3255436806 * get__stream_17() const { return ____stream_17; }
	inline Stream_t3255436806 ** get_address_of__stream_17() { return &____stream_17; }
	inline void set__stream_17(Stream_t3255436806 * value)
	{
		____stream_17 = value;
		Il2CppCodeGenWriteBarrier((&____stream_17), value);
	}

	inline static int32_t get_offset_of__sync_18() { return static_cast<int32_t>(offsetof(HttpConnection_t2649486862, ____sync_18)); }
	inline RuntimeObject * get__sync_18() const { return ____sync_18; }
	inline RuntimeObject ** get_address_of__sync_18() { return &____sync_18; }
	inline void set__sync_18(RuntimeObject * value)
	{
		____sync_18 = value;
		Il2CppCodeGenWriteBarrier((&____sync_18), value);
	}

	inline static int32_t get_offset_of__timeout_19() { return static_cast<int32_t>(offsetof(HttpConnection_t2649486862, ____timeout_19)); }
	inline int32_t get__timeout_19() const { return ____timeout_19; }
	inline int32_t* get_address_of__timeout_19() { return &____timeout_19; }
	inline void set__timeout_19(int32_t value)
	{
		____timeout_19 = value;
	}

	inline static int32_t get_offset_of__timer_20() { return static_cast<int32_t>(offsetof(HttpConnection_t2649486862, ____timer_20)); }
	inline Timer_t791717973 * get__timer_20() const { return ____timer_20; }
	inline Timer_t791717973 ** get_address_of__timer_20() { return &____timer_20; }
	inline void set__timer_20(Timer_t791717973 * value)
	{
		____timer_20 = value;
		Il2CppCodeGenWriteBarrier((&____timer_20), value);
	}
};

struct HttpConnection_t2649486862_StaticFields
{
public:
	// System.Threading.TimerCallback WebSocketSharp.Net.HttpConnection::<>f__mg$cache0
	TimerCallback_t1684927372 * ___U3CU3Ef__mgU24cache0_21;
	// System.AsyncCallback WebSocketSharp.Net.HttpConnection::<>f__mg$cache1
	AsyncCallback_t163412349 * ___U3CU3Ef__mgU24cache1_22;
	// System.AsyncCallback WebSocketSharp.Net.HttpConnection::<>f__mg$cache2
	AsyncCallback_t163412349 * ___U3CU3Ef__mgU24cache2_23;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_21() { return static_cast<int32_t>(offsetof(HttpConnection_t2649486862_StaticFields, ___U3CU3Ef__mgU24cache0_21)); }
	inline TimerCallback_t1684927372 * get_U3CU3Ef__mgU24cache0_21() const { return ___U3CU3Ef__mgU24cache0_21; }
	inline TimerCallback_t1684927372 ** get_address_of_U3CU3Ef__mgU24cache0_21() { return &___U3CU3Ef__mgU24cache0_21; }
	inline void set_U3CU3Ef__mgU24cache0_21(TimerCallback_t1684927372 * value)
	{
		___U3CU3Ef__mgU24cache0_21 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_21), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_22() { return static_cast<int32_t>(offsetof(HttpConnection_t2649486862_StaticFields, ___U3CU3Ef__mgU24cache1_22)); }
	inline AsyncCallback_t163412349 * get_U3CU3Ef__mgU24cache1_22() const { return ___U3CU3Ef__mgU24cache1_22; }
	inline AsyncCallback_t163412349 ** get_address_of_U3CU3Ef__mgU24cache1_22() { return &___U3CU3Ef__mgU24cache1_22; }
	inline void set_U3CU3Ef__mgU24cache1_22(AsyncCallback_t163412349 * value)
	{
		___U3CU3Ef__mgU24cache1_22 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_22), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2_23() { return static_cast<int32_t>(offsetof(HttpConnection_t2649486862_StaticFields, ___U3CU3Ef__mgU24cache2_23)); }
	inline AsyncCallback_t163412349 * get_U3CU3Ef__mgU24cache2_23() const { return ___U3CU3Ef__mgU24cache2_23; }
	inline AsyncCallback_t163412349 ** get_address_of_U3CU3Ef__mgU24cache2_23() { return &___U3CU3Ef__mgU24cache2_23; }
	inline void set_U3CU3Ef__mgU24cache2_23(AsyncCallback_t163412349 * value)
	{
		___U3CU3Ef__mgU24cache2_23 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache2_23), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPCONNECTION_T2649486862_H
#ifndef HTTPHEADERINFO_T2096319561_H
#define HTTPHEADERINFO_T2096319561_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.HttpHeaderInfo
struct  HttpHeaderInfo_t2096319561  : public RuntimeObject
{
public:
	// System.String WebSocketSharp.Net.HttpHeaderInfo::_name
	String_t* ____name_0;
	// WebSocketSharp.Net.HttpHeaderType WebSocketSharp.Net.HttpHeaderInfo::_type
	int32_t ____type_1;

public:
	inline static int32_t get_offset_of__name_0() { return static_cast<int32_t>(offsetof(HttpHeaderInfo_t2096319561, ____name_0)); }
	inline String_t* get__name_0() const { return ____name_0; }
	inline String_t** get_address_of__name_0() { return &____name_0; }
	inline void set__name_0(String_t* value)
	{
		____name_0 = value;
		Il2CppCodeGenWriteBarrier((&____name_0), value);
	}

	inline static int32_t get_offset_of__type_1() { return static_cast<int32_t>(offsetof(HttpHeaderInfo_t2096319561, ____type_1)); }
	inline int32_t get__type_1() const { return ____type_1; }
	inline int32_t* get_address_of__type_1() { return &____type_1; }
	inline void set__type_1(int32_t value)
	{
		____type_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPHEADERINFO_T2096319561_H
#ifndef MESSAGEEVENTARGS_T2890051726_H
#define MESSAGEEVENTARGS_T2890051726_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.MessageEventArgs
struct  MessageEventArgs_t2890051726  : public EventArgs_t3289624707
{
public:
	// System.String WebSocketSharp.MessageEventArgs::_data
	String_t* ____data_1;
	// System.Boolean WebSocketSharp.MessageEventArgs::_dataSet
	bool ____dataSet_2;
	// WebSocketSharp.Opcode WebSocketSharp.MessageEventArgs::_opcode
	uint8_t ____opcode_3;
	// System.Byte[] WebSocketSharp.MessageEventArgs::_rawData
	ByteU5BU5D_t3397334013* ____rawData_4;

public:
	inline static int32_t get_offset_of__data_1() { return static_cast<int32_t>(offsetof(MessageEventArgs_t2890051726, ____data_1)); }
	inline String_t* get__data_1() const { return ____data_1; }
	inline String_t** get_address_of__data_1() { return &____data_1; }
	inline void set__data_1(String_t* value)
	{
		____data_1 = value;
		Il2CppCodeGenWriteBarrier((&____data_1), value);
	}

	inline static int32_t get_offset_of__dataSet_2() { return static_cast<int32_t>(offsetof(MessageEventArgs_t2890051726, ____dataSet_2)); }
	inline bool get__dataSet_2() const { return ____dataSet_2; }
	inline bool* get_address_of__dataSet_2() { return &____dataSet_2; }
	inline void set__dataSet_2(bool value)
	{
		____dataSet_2 = value;
	}

	inline static int32_t get_offset_of__opcode_3() { return static_cast<int32_t>(offsetof(MessageEventArgs_t2890051726, ____opcode_3)); }
	inline uint8_t get__opcode_3() const { return ____opcode_3; }
	inline uint8_t* get_address_of__opcode_3() { return &____opcode_3; }
	inline void set__opcode_3(uint8_t value)
	{
		____opcode_3 = value;
	}

	inline static int32_t get_offset_of__rawData_4() { return static_cast<int32_t>(offsetof(MessageEventArgs_t2890051726, ____rawData_4)); }
	inline ByteU5BU5D_t3397334013* get__rawData_4() const { return ____rawData_4; }
	inline ByteU5BU5D_t3397334013** get_address_of__rawData_4() { return &____rawData_4; }
	inline void set__rawData_4(ByteU5BU5D_t3397334013* value)
	{
		____rawData_4 = value;
		Il2CppCodeGenWriteBarrier((&____rawData_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGEEVENTARGS_T2890051726_H
#ifndef TOUCHHIT_T4186847494_H
#define TOUCHHIT_T4186847494_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Hit.TouchHit
struct  TouchHit_t4186847494 
{
public:
	// TouchScript.Hit.TouchHit/TouchHitType TouchScript.Hit.TouchHit::type
	int32_t ___type_0;
	// UnityEngine.Transform TouchScript.Hit.TouchHit::transform
	Transform_t3275118058 * ___transform_1;
	// UnityEngine.RaycastHit TouchScript.Hit.TouchHit::raycastHit
	RaycastHit_t87180320  ___raycastHit_2;
	// UnityEngine.RaycastHit2D TouchScript.Hit.TouchHit::raycastHit2D
	RaycastHit2D_t4063908774  ___raycastHit2D_3;
	// UnityEngine.EventSystems.RaycastResult TouchScript.Hit.TouchHit::raycastResult
	RaycastResult_t21186376  ___raycastResult_4;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(TouchHit_t4186847494, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_transform_1() { return static_cast<int32_t>(offsetof(TouchHit_t4186847494, ___transform_1)); }
	inline Transform_t3275118058 * get_transform_1() const { return ___transform_1; }
	inline Transform_t3275118058 ** get_address_of_transform_1() { return &___transform_1; }
	inline void set_transform_1(Transform_t3275118058 * value)
	{
		___transform_1 = value;
		Il2CppCodeGenWriteBarrier((&___transform_1), value);
	}

	inline static int32_t get_offset_of_raycastHit_2() { return static_cast<int32_t>(offsetof(TouchHit_t4186847494, ___raycastHit_2)); }
	inline RaycastHit_t87180320  get_raycastHit_2() const { return ___raycastHit_2; }
	inline RaycastHit_t87180320 * get_address_of_raycastHit_2() { return &___raycastHit_2; }
	inline void set_raycastHit_2(RaycastHit_t87180320  value)
	{
		___raycastHit_2 = value;
	}

	inline static int32_t get_offset_of_raycastHit2D_3() { return static_cast<int32_t>(offsetof(TouchHit_t4186847494, ___raycastHit2D_3)); }
	inline RaycastHit2D_t4063908774  get_raycastHit2D_3() const { return ___raycastHit2D_3; }
	inline RaycastHit2D_t4063908774 * get_address_of_raycastHit2D_3() { return &___raycastHit2D_3; }
	inline void set_raycastHit2D_3(RaycastHit2D_t4063908774  value)
	{
		___raycastHit2D_3 = value;
	}

	inline static int32_t get_offset_of_raycastResult_4() { return static_cast<int32_t>(offsetof(TouchHit_t4186847494, ___raycastResult_4)); }
	inline RaycastResult_t21186376  get_raycastResult_4() const { return ___raycastResult_4; }
	inline RaycastResult_t21186376 * get_address_of_raycastResult_4() { return &___raycastResult_4; }
	inline void set_raycastResult_4(RaycastResult_t21186376  value)
	{
		___raycastResult_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TouchScript.Hit.TouchHit
struct TouchHit_t4186847494_marshaled_pinvoke
{
	int32_t ___type_0;
	Transform_t3275118058 * ___transform_1;
	RaycastHit_t87180320_marshaled_pinvoke ___raycastHit_2;
	RaycastHit2D_t4063908774_marshaled_pinvoke ___raycastHit2D_3;
	RaycastResult_t21186376_marshaled_pinvoke ___raycastResult_4;
};
// Native definition for COM marshalling of TouchScript.Hit.TouchHit
struct TouchHit_t4186847494_marshaled_com
{
	int32_t ___type_0;
	Transform_t3275118058 * ___transform_1;
	RaycastHit_t87180320_marshaled_com ___raycastHit_2;
	RaycastHit2D_t4063908774_marshaled_com ___raycastHit2D_3;
	RaycastResult_t21186376_marshaled_com ___raycastResult_4;
};
#endif // TOUCHHIT_T4186847494_H
#ifndef LOGGER_T2598199114_H
#define LOGGER_T2598199114_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Logger
struct  Logger_t2598199114  : public RuntimeObject
{
public:
	// System.String modreq(System.Runtime.CompilerServices.IsVolatile) WebSocketSharp.Logger::_file
	String_t* ____file_0;
	// WebSocketSharp.LogLevel modreq(System.Runtime.CompilerServices.IsVolatile) WebSocketSharp.Logger::_level
	int32_t ____level_1;
	// System.Action`2<WebSocketSharp.LogData,System.String> WebSocketSharp.Logger::_output
	Action_2_t502883108 * ____output_2;
	// System.Object WebSocketSharp.Logger::_sync
	RuntimeObject * ____sync_3;

public:
	inline static int32_t get_offset_of__file_0() { return static_cast<int32_t>(offsetof(Logger_t2598199114, ____file_0)); }
	inline String_t* get__file_0() const { return ____file_0; }
	inline String_t** get_address_of__file_0() { return &____file_0; }
	inline void set__file_0(String_t* value)
	{
		____file_0 = value;
		Il2CppCodeGenWriteBarrier((&____file_0), value);
	}

	inline static int32_t get_offset_of__level_1() { return static_cast<int32_t>(offsetof(Logger_t2598199114, ____level_1)); }
	inline int32_t get__level_1() const { return ____level_1; }
	inline int32_t* get_address_of__level_1() { return &____level_1; }
	inline void set__level_1(int32_t value)
	{
		____level_1 = value;
	}

	inline static int32_t get_offset_of__output_2() { return static_cast<int32_t>(offsetof(Logger_t2598199114, ____output_2)); }
	inline Action_2_t502883108 * get__output_2() const { return ____output_2; }
	inline Action_2_t502883108 ** get_address_of__output_2() { return &____output_2; }
	inline void set__output_2(Action_2_t502883108 * value)
	{
		____output_2 = value;
		Il2CppCodeGenWriteBarrier((&____output_2), value);
	}

	inline static int32_t get_offset_of__sync_3() { return static_cast<int32_t>(offsetof(Logger_t2598199114, ____sync_3)); }
	inline RuntimeObject * get__sync_3() const { return ____sync_3; }
	inline RuntimeObject ** get_address_of__sync_3() { return &____sync_3; }
	inline void set__sync_3(RuntimeObject * value)
	{
		____sync_3 = value;
		Il2CppCodeGenWriteBarrier((&____sync_3), value);
	}
};

struct Logger_t2598199114_StaticFields
{
public:
	// System.Action`2<WebSocketSharp.LogData,System.String> WebSocketSharp.Logger::<>f__mg$cache0
	Action_2_t502883108 * ___U3CU3Ef__mgU24cache0_4;
	// System.Action`2<WebSocketSharp.LogData,System.String> WebSocketSharp.Logger::<>f__mg$cache1
	Action_2_t502883108 * ___U3CU3Ef__mgU24cache1_5;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_4() { return static_cast<int32_t>(offsetof(Logger_t2598199114_StaticFields, ___U3CU3Ef__mgU24cache0_4)); }
	inline Action_2_t502883108 * get_U3CU3Ef__mgU24cache0_4() const { return ___U3CU3Ef__mgU24cache0_4; }
	inline Action_2_t502883108 ** get_address_of_U3CU3Ef__mgU24cache0_4() { return &___U3CU3Ef__mgU24cache0_4; }
	inline void set_U3CU3Ef__mgU24cache0_4(Action_2_t502883108 * value)
	{
		___U3CU3Ef__mgU24cache0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_5() { return static_cast<int32_t>(offsetof(Logger_t2598199114_StaticFields, ___U3CU3Ef__mgU24cache1_5)); }
	inline Action_2_t502883108 * get_U3CU3Ef__mgU24cache1_5() const { return ___U3CU3Ef__mgU24cache1_5; }
	inline Action_2_t502883108 ** get_address_of_U3CU3Ef__mgU24cache1_5() { return &___U3CU3Ef__mgU24cache1_5; }
	inline void set_U3CU3Ef__mgU24cache1_5(Action_2_t502883108 * value)
	{
		___U3CU3Ef__mgU24cache1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGGER_T2598199114_H
#ifndef LOGDATA_T4095822710_H
#define LOGDATA_T4095822710_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.LogData
struct  LogData_t4095822710  : public RuntimeObject
{
public:
	// System.Diagnostics.StackFrame WebSocketSharp.LogData::_caller
	StackFrame_t2050294881 * ____caller_0;
	// System.DateTime WebSocketSharp.LogData::_date
	DateTime_t693205669  ____date_1;
	// WebSocketSharp.LogLevel WebSocketSharp.LogData::_level
	int32_t ____level_2;
	// System.String WebSocketSharp.LogData::_message
	String_t* ____message_3;

public:
	inline static int32_t get_offset_of__caller_0() { return static_cast<int32_t>(offsetof(LogData_t4095822710, ____caller_0)); }
	inline StackFrame_t2050294881 * get__caller_0() const { return ____caller_0; }
	inline StackFrame_t2050294881 ** get_address_of__caller_0() { return &____caller_0; }
	inline void set__caller_0(StackFrame_t2050294881 * value)
	{
		____caller_0 = value;
		Il2CppCodeGenWriteBarrier((&____caller_0), value);
	}

	inline static int32_t get_offset_of__date_1() { return static_cast<int32_t>(offsetof(LogData_t4095822710, ____date_1)); }
	inline DateTime_t693205669  get__date_1() const { return ____date_1; }
	inline DateTime_t693205669 * get_address_of__date_1() { return &____date_1; }
	inline void set__date_1(DateTime_t693205669  value)
	{
		____date_1 = value;
	}

	inline static int32_t get_offset_of__level_2() { return static_cast<int32_t>(offsetof(LogData_t4095822710, ____level_2)); }
	inline int32_t get__level_2() const { return ____level_2; }
	inline int32_t* get_address_of__level_2() { return &____level_2; }
	inline void set__level_2(int32_t value)
	{
		____level_2 = value;
	}

	inline static int32_t get_offset_of__message_3() { return static_cast<int32_t>(offsetof(LogData_t4095822710, ____message_3)); }
	inline String_t* get__message_3() const { return ____message_3; }
	inline String_t** get_address_of__message_3() { return &____message_3; }
	inline void set__message_3(String_t* value)
	{
		____message_3 = value;
		Il2CppCodeGenWriteBarrier((&____message_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGDATA_T4095822710_H
#ifndef AUTHENTICATIONRESPONSE_T1212723231_H
#define AUTHENTICATIONRESPONSE_T1212723231_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.AuthenticationResponse
struct  AuthenticationResponse_t1212723231  : public AuthenticationBase_t909684845
{
public:
	// System.UInt32 WebSocketSharp.Net.AuthenticationResponse::_nonceCount
	uint32_t ____nonceCount_2;

public:
	inline static int32_t get_offset_of__nonceCount_2() { return static_cast<int32_t>(offsetof(AuthenticationResponse_t1212723231, ____nonceCount_2)); }
	inline uint32_t get__nonceCount_2() const { return ____nonceCount_2; }
	inline uint32_t* get_address_of__nonceCount_2() { return &____nonceCount_2; }
	inline void set__nonceCount_2(uint32_t value)
	{
		____nonceCount_2 = value;
	}
};

struct AuthenticationResponse_t1212723231_StaticFields
{
public:
	// System.Func`2<System.String,System.Boolean> WebSocketSharp.Net.AuthenticationResponse::<>f__am$cache0
	Func_2_t1989381442 * ___U3CU3Ef__amU24cache0_3;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_3() { return static_cast<int32_t>(offsetof(AuthenticationResponse_t1212723231_StaticFields, ___U3CU3Ef__amU24cache0_3)); }
	inline Func_2_t1989381442 * get_U3CU3Ef__amU24cache0_3() const { return ___U3CU3Ef__amU24cache0_3; }
	inline Func_2_t1989381442 ** get_address_of_U3CU3Ef__amU24cache0_3() { return &___U3CU3Ef__amU24cache0_3; }
	inline void set_U3CU3Ef__amU24cache0_3(Func_2_t1989381442 * value)
	{
		___U3CU3Ef__amU24cache0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTHENTICATIONRESPONSE_T1212723231_H
#ifndef CLIENTSSLCONFIGURATION_T1159130081_H
#define CLIENTSSLCONFIGURATION_T1159130081_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.ClientSslConfiguration
struct  ClientSslConfiguration_t1159130081  : public SslConfiguration_t760772650
{
public:
	// System.Security.Cryptography.X509Certificates.X509CertificateCollection WebSocketSharp.Net.ClientSslConfiguration::_certs
	X509CertificateCollection_t1197680765 * ____certs_6;
	// System.String WebSocketSharp.Net.ClientSslConfiguration::_host
	String_t* ____host_7;

public:
	inline static int32_t get_offset_of__certs_6() { return static_cast<int32_t>(offsetof(ClientSslConfiguration_t1159130081, ____certs_6)); }
	inline X509CertificateCollection_t1197680765 * get__certs_6() const { return ____certs_6; }
	inline X509CertificateCollection_t1197680765 ** get_address_of__certs_6() { return &____certs_6; }
	inline void set__certs_6(X509CertificateCollection_t1197680765 * value)
	{
		____certs_6 = value;
		Il2CppCodeGenWriteBarrier((&____certs_6), value);
	}

	inline static int32_t get_offset_of__host_7() { return static_cast<int32_t>(offsetof(ClientSslConfiguration_t1159130081, ____host_7)); }
	inline String_t* get__host_7() const { return ____host_7; }
	inline String_t** get_address_of__host_7() { return &____host_7; }
	inline void set__host_7(String_t* value)
	{
		____host_7 = value;
		Il2CppCodeGenWriteBarrier((&____host_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIENTSSLCONFIGURATION_T1159130081_H
#ifndef HTTPLISTENEREXCEPTION_T543138973_H
#define HTTPLISTENEREXCEPTION_T543138973_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.HttpListenerException
struct  HttpListenerException_t543138973  : public Win32Exception_t1708275760
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPLISTENEREXCEPTION_T543138973_H
#ifndef COOKIE_T1826188460_H
#define COOKIE_T1826188460_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.Cookie
struct  Cookie_t1826188460  : public RuntimeObject
{
public:
	// System.String WebSocketSharp.Net.Cookie::_comment
	String_t* ____comment_0;
	// System.Uri WebSocketSharp.Net.Cookie::_commentUri
	Uri_t19570940 * ____commentUri_1;
	// System.Boolean WebSocketSharp.Net.Cookie::_discard
	bool ____discard_2;
	// System.String WebSocketSharp.Net.Cookie::_domain
	String_t* ____domain_3;
	// System.DateTime WebSocketSharp.Net.Cookie::_expires
	DateTime_t693205669  ____expires_4;
	// System.Boolean WebSocketSharp.Net.Cookie::_httpOnly
	bool ____httpOnly_5;
	// System.String WebSocketSharp.Net.Cookie::_name
	String_t* ____name_6;
	// System.String WebSocketSharp.Net.Cookie::_path
	String_t* ____path_7;
	// System.String WebSocketSharp.Net.Cookie::_port
	String_t* ____port_8;
	// System.Int32[] WebSocketSharp.Net.Cookie::_ports
	Int32U5BU5D_t3030399641* ____ports_9;
	// System.Boolean WebSocketSharp.Net.Cookie::_secure
	bool ____secure_12;
	// System.DateTime WebSocketSharp.Net.Cookie::_timestamp
	DateTime_t693205669  ____timestamp_13;
	// System.String WebSocketSharp.Net.Cookie::_value
	String_t* ____value_14;
	// System.Int32 WebSocketSharp.Net.Cookie::_version
	int32_t ____version_15;
	// System.Boolean WebSocketSharp.Net.Cookie::<ExactDomain>k__BackingField
	bool ___U3CExactDomainU3Ek__BackingField_16;

public:
	inline static int32_t get_offset_of__comment_0() { return static_cast<int32_t>(offsetof(Cookie_t1826188460, ____comment_0)); }
	inline String_t* get__comment_0() const { return ____comment_0; }
	inline String_t** get_address_of__comment_0() { return &____comment_0; }
	inline void set__comment_0(String_t* value)
	{
		____comment_0 = value;
		Il2CppCodeGenWriteBarrier((&____comment_0), value);
	}

	inline static int32_t get_offset_of__commentUri_1() { return static_cast<int32_t>(offsetof(Cookie_t1826188460, ____commentUri_1)); }
	inline Uri_t19570940 * get__commentUri_1() const { return ____commentUri_1; }
	inline Uri_t19570940 ** get_address_of__commentUri_1() { return &____commentUri_1; }
	inline void set__commentUri_1(Uri_t19570940 * value)
	{
		____commentUri_1 = value;
		Il2CppCodeGenWriteBarrier((&____commentUri_1), value);
	}

	inline static int32_t get_offset_of__discard_2() { return static_cast<int32_t>(offsetof(Cookie_t1826188460, ____discard_2)); }
	inline bool get__discard_2() const { return ____discard_2; }
	inline bool* get_address_of__discard_2() { return &____discard_2; }
	inline void set__discard_2(bool value)
	{
		____discard_2 = value;
	}

	inline static int32_t get_offset_of__domain_3() { return static_cast<int32_t>(offsetof(Cookie_t1826188460, ____domain_3)); }
	inline String_t* get__domain_3() const { return ____domain_3; }
	inline String_t** get_address_of__domain_3() { return &____domain_3; }
	inline void set__domain_3(String_t* value)
	{
		____domain_3 = value;
		Il2CppCodeGenWriteBarrier((&____domain_3), value);
	}

	inline static int32_t get_offset_of__expires_4() { return static_cast<int32_t>(offsetof(Cookie_t1826188460, ____expires_4)); }
	inline DateTime_t693205669  get__expires_4() const { return ____expires_4; }
	inline DateTime_t693205669 * get_address_of__expires_4() { return &____expires_4; }
	inline void set__expires_4(DateTime_t693205669  value)
	{
		____expires_4 = value;
	}

	inline static int32_t get_offset_of__httpOnly_5() { return static_cast<int32_t>(offsetof(Cookie_t1826188460, ____httpOnly_5)); }
	inline bool get__httpOnly_5() const { return ____httpOnly_5; }
	inline bool* get_address_of__httpOnly_5() { return &____httpOnly_5; }
	inline void set__httpOnly_5(bool value)
	{
		____httpOnly_5 = value;
	}

	inline static int32_t get_offset_of__name_6() { return static_cast<int32_t>(offsetof(Cookie_t1826188460, ____name_6)); }
	inline String_t* get__name_6() const { return ____name_6; }
	inline String_t** get_address_of__name_6() { return &____name_6; }
	inline void set__name_6(String_t* value)
	{
		____name_6 = value;
		Il2CppCodeGenWriteBarrier((&____name_6), value);
	}

	inline static int32_t get_offset_of__path_7() { return static_cast<int32_t>(offsetof(Cookie_t1826188460, ____path_7)); }
	inline String_t* get__path_7() const { return ____path_7; }
	inline String_t** get_address_of__path_7() { return &____path_7; }
	inline void set__path_7(String_t* value)
	{
		____path_7 = value;
		Il2CppCodeGenWriteBarrier((&____path_7), value);
	}

	inline static int32_t get_offset_of__port_8() { return static_cast<int32_t>(offsetof(Cookie_t1826188460, ____port_8)); }
	inline String_t* get__port_8() const { return ____port_8; }
	inline String_t** get_address_of__port_8() { return &____port_8; }
	inline void set__port_8(String_t* value)
	{
		____port_8 = value;
		Il2CppCodeGenWriteBarrier((&____port_8), value);
	}

	inline static int32_t get_offset_of__ports_9() { return static_cast<int32_t>(offsetof(Cookie_t1826188460, ____ports_9)); }
	inline Int32U5BU5D_t3030399641* get__ports_9() const { return ____ports_9; }
	inline Int32U5BU5D_t3030399641** get_address_of__ports_9() { return &____ports_9; }
	inline void set__ports_9(Int32U5BU5D_t3030399641* value)
	{
		____ports_9 = value;
		Il2CppCodeGenWriteBarrier((&____ports_9), value);
	}

	inline static int32_t get_offset_of__secure_12() { return static_cast<int32_t>(offsetof(Cookie_t1826188460, ____secure_12)); }
	inline bool get__secure_12() const { return ____secure_12; }
	inline bool* get_address_of__secure_12() { return &____secure_12; }
	inline void set__secure_12(bool value)
	{
		____secure_12 = value;
	}

	inline static int32_t get_offset_of__timestamp_13() { return static_cast<int32_t>(offsetof(Cookie_t1826188460, ____timestamp_13)); }
	inline DateTime_t693205669  get__timestamp_13() const { return ____timestamp_13; }
	inline DateTime_t693205669 * get_address_of__timestamp_13() { return &____timestamp_13; }
	inline void set__timestamp_13(DateTime_t693205669  value)
	{
		____timestamp_13 = value;
	}

	inline static int32_t get_offset_of__value_14() { return static_cast<int32_t>(offsetof(Cookie_t1826188460, ____value_14)); }
	inline String_t* get__value_14() const { return ____value_14; }
	inline String_t** get_address_of__value_14() { return &____value_14; }
	inline void set__value_14(String_t* value)
	{
		____value_14 = value;
		Il2CppCodeGenWriteBarrier((&____value_14), value);
	}

	inline static int32_t get_offset_of__version_15() { return static_cast<int32_t>(offsetof(Cookie_t1826188460, ____version_15)); }
	inline int32_t get__version_15() const { return ____version_15; }
	inline int32_t* get_address_of__version_15() { return &____version_15; }
	inline void set__version_15(int32_t value)
	{
		____version_15 = value;
	}

	inline static int32_t get_offset_of_U3CExactDomainU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(Cookie_t1826188460, ___U3CExactDomainU3Ek__BackingField_16)); }
	inline bool get_U3CExactDomainU3Ek__BackingField_16() const { return ___U3CExactDomainU3Ek__BackingField_16; }
	inline bool* get_address_of_U3CExactDomainU3Ek__BackingField_16() { return &___U3CExactDomainU3Ek__BackingField_16; }
	inline void set_U3CExactDomainU3Ek__BackingField_16(bool value)
	{
		___U3CExactDomainU3Ek__BackingField_16 = value;
	}
};

struct Cookie_t1826188460_StaticFields
{
public:
	// System.Char[] WebSocketSharp.Net.Cookie::_reservedCharsForName
	CharU5BU5D_t1328083999* ____reservedCharsForName_10;
	// System.Char[] WebSocketSharp.Net.Cookie::_reservedCharsForValue
	CharU5BU5D_t1328083999* ____reservedCharsForValue_11;

public:
	inline static int32_t get_offset_of__reservedCharsForName_10() { return static_cast<int32_t>(offsetof(Cookie_t1826188460_StaticFields, ____reservedCharsForName_10)); }
	inline CharU5BU5D_t1328083999* get__reservedCharsForName_10() const { return ____reservedCharsForName_10; }
	inline CharU5BU5D_t1328083999** get_address_of__reservedCharsForName_10() { return &____reservedCharsForName_10; }
	inline void set__reservedCharsForName_10(CharU5BU5D_t1328083999* value)
	{
		____reservedCharsForName_10 = value;
		Il2CppCodeGenWriteBarrier((&____reservedCharsForName_10), value);
	}

	inline static int32_t get_offset_of__reservedCharsForValue_11() { return static_cast<int32_t>(offsetof(Cookie_t1826188460_StaticFields, ____reservedCharsForValue_11)); }
	inline CharU5BU5D_t1328083999* get__reservedCharsForValue_11() const { return ____reservedCharsForValue_11; }
	inline CharU5BU5D_t1328083999** get_address_of__reservedCharsForValue_11() { return &____reservedCharsForValue_11; }
	inline void set__reservedCharsForValue_11(CharU5BU5D_t1328083999* value)
	{
		____reservedCharsForValue_11 = value;
		Il2CppCodeGenWriteBarrier((&____reservedCharsForValue_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COOKIE_T1826188460_H
#ifndef BEHAVIOUR_T955675639_H
#define BEHAVIOUR_T955675639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t955675639  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T955675639_H
#ifndef TOUCHPOINT_T959629083_H
#define TOUCHPOINT_T959629083_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.TouchPoint
struct  TouchPoint_t959629083  : public RuntimeObject
{
public:
	// System.Int32 TouchScript.TouchPoint::<Id>k__BackingField
	int32_t ___U3CIdU3Ek__BackingField_0;
	// UnityEngine.Transform TouchScript.TouchPoint::<Target>k__BackingField
	Transform_t3275118058 * ___U3CTargetU3Ek__BackingField_1;
	// UnityEngine.Vector2 TouchScript.TouchPoint::<PreviousPosition>k__BackingField
	Vector2_t2243707579  ___U3CPreviousPositionU3Ek__BackingField_2;
	// TouchScript.Hit.TouchHit TouchScript.TouchPoint::<Hit>k__BackingField
	TouchHit_t4186847494  ___U3CHitU3Ek__BackingField_3;
	// TouchScript.Layers.TouchLayer TouchScript.TouchPoint::<Layer>k__BackingField
	TouchLayer_t2635439978 * ___U3CLayerU3Ek__BackingField_4;
	// TouchScript.InputSources.IInputSource TouchScript.TouchPoint::<InputSource>k__BackingField
	RuntimeObject* ___U3CInputSourceU3Ek__BackingField_5;
	// TouchScript.Tags TouchScript.TouchPoint::<Tags>k__BackingField
	Tags_t1265380163 * ___U3CTagsU3Ek__BackingField_6;
	// System.Int32 TouchScript.TouchPoint::refCount
	int32_t ___refCount_7;
	// UnityEngine.Vector2 TouchScript.TouchPoint::position
	Vector2_t2243707579  ___position_8;
	// UnityEngine.Vector2 TouchScript.TouchPoint::newPosition
	Vector2_t2243707579  ___newPosition_9;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> TouchScript.TouchPoint::properties
	Dictionary_2_t309261261 * ___properties_10;

public:
	inline static int32_t get_offset_of_U3CIdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TouchPoint_t959629083, ___U3CIdU3Ek__BackingField_0)); }
	inline int32_t get_U3CIdU3Ek__BackingField_0() const { return ___U3CIdU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CIdU3Ek__BackingField_0() { return &___U3CIdU3Ek__BackingField_0; }
	inline void set_U3CIdU3Ek__BackingField_0(int32_t value)
	{
		___U3CIdU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CTargetU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TouchPoint_t959629083, ___U3CTargetU3Ek__BackingField_1)); }
	inline Transform_t3275118058 * get_U3CTargetU3Ek__BackingField_1() const { return ___U3CTargetU3Ek__BackingField_1; }
	inline Transform_t3275118058 ** get_address_of_U3CTargetU3Ek__BackingField_1() { return &___U3CTargetU3Ek__BackingField_1; }
	inline void set_U3CTargetU3Ek__BackingField_1(Transform_t3275118058 * value)
	{
		___U3CTargetU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTargetU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CPreviousPositionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(TouchPoint_t959629083, ___U3CPreviousPositionU3Ek__BackingField_2)); }
	inline Vector2_t2243707579  get_U3CPreviousPositionU3Ek__BackingField_2() const { return ___U3CPreviousPositionU3Ek__BackingField_2; }
	inline Vector2_t2243707579 * get_address_of_U3CPreviousPositionU3Ek__BackingField_2() { return &___U3CPreviousPositionU3Ek__BackingField_2; }
	inline void set_U3CPreviousPositionU3Ek__BackingField_2(Vector2_t2243707579  value)
	{
		___U3CPreviousPositionU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CHitU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(TouchPoint_t959629083, ___U3CHitU3Ek__BackingField_3)); }
	inline TouchHit_t4186847494  get_U3CHitU3Ek__BackingField_3() const { return ___U3CHitU3Ek__BackingField_3; }
	inline TouchHit_t4186847494 * get_address_of_U3CHitU3Ek__BackingField_3() { return &___U3CHitU3Ek__BackingField_3; }
	inline void set_U3CHitU3Ek__BackingField_3(TouchHit_t4186847494  value)
	{
		___U3CHitU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CLayerU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(TouchPoint_t959629083, ___U3CLayerU3Ek__BackingField_4)); }
	inline TouchLayer_t2635439978 * get_U3CLayerU3Ek__BackingField_4() const { return ___U3CLayerU3Ek__BackingField_4; }
	inline TouchLayer_t2635439978 ** get_address_of_U3CLayerU3Ek__BackingField_4() { return &___U3CLayerU3Ek__BackingField_4; }
	inline void set_U3CLayerU3Ek__BackingField_4(TouchLayer_t2635439978 * value)
	{
		___U3CLayerU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLayerU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CInputSourceU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(TouchPoint_t959629083, ___U3CInputSourceU3Ek__BackingField_5)); }
	inline RuntimeObject* get_U3CInputSourceU3Ek__BackingField_5() const { return ___U3CInputSourceU3Ek__BackingField_5; }
	inline RuntimeObject** get_address_of_U3CInputSourceU3Ek__BackingField_5() { return &___U3CInputSourceU3Ek__BackingField_5; }
	inline void set_U3CInputSourceU3Ek__BackingField_5(RuntimeObject* value)
	{
		___U3CInputSourceU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInputSourceU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CTagsU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(TouchPoint_t959629083, ___U3CTagsU3Ek__BackingField_6)); }
	inline Tags_t1265380163 * get_U3CTagsU3Ek__BackingField_6() const { return ___U3CTagsU3Ek__BackingField_6; }
	inline Tags_t1265380163 ** get_address_of_U3CTagsU3Ek__BackingField_6() { return &___U3CTagsU3Ek__BackingField_6; }
	inline void set_U3CTagsU3Ek__BackingField_6(Tags_t1265380163 * value)
	{
		___U3CTagsU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTagsU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_refCount_7() { return static_cast<int32_t>(offsetof(TouchPoint_t959629083, ___refCount_7)); }
	inline int32_t get_refCount_7() const { return ___refCount_7; }
	inline int32_t* get_address_of_refCount_7() { return &___refCount_7; }
	inline void set_refCount_7(int32_t value)
	{
		___refCount_7 = value;
	}

	inline static int32_t get_offset_of_position_8() { return static_cast<int32_t>(offsetof(TouchPoint_t959629083, ___position_8)); }
	inline Vector2_t2243707579  get_position_8() const { return ___position_8; }
	inline Vector2_t2243707579 * get_address_of_position_8() { return &___position_8; }
	inline void set_position_8(Vector2_t2243707579  value)
	{
		___position_8 = value;
	}

	inline static int32_t get_offset_of_newPosition_9() { return static_cast<int32_t>(offsetof(TouchPoint_t959629083, ___newPosition_9)); }
	inline Vector2_t2243707579  get_newPosition_9() const { return ___newPosition_9; }
	inline Vector2_t2243707579 * get_address_of_newPosition_9() { return &___newPosition_9; }
	inline void set_newPosition_9(Vector2_t2243707579  value)
	{
		___newPosition_9 = value;
	}

	inline static int32_t get_offset_of_properties_10() { return static_cast<int32_t>(offsetof(TouchPoint_t959629083, ___properties_10)); }
	inline Dictionary_2_t309261261 * get_properties_10() const { return ___properties_10; }
	inline Dictionary_2_t309261261 ** get_address_of_properties_10() { return &___properties_10; }
	inline void set_properties_10(Dictionary_2_t309261261 * value)
	{
		___properties_10 = value;
		Il2CppCodeGenWriteBarrier((&___properties_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHPOINT_T959629083_H
#ifndef AUTHENTICATIONCHALLENGE_T1146723439_H
#define AUTHENTICATIONCHALLENGE_T1146723439_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.AuthenticationChallenge
struct  AuthenticationChallenge_t1146723439  : public AuthenticationBase_t909684845
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTHENTICATIONCHALLENGE_T1146723439_H
#ifndef MONOBEHAVIOUR_T1158329972_H
#define MONOBEHAVIOUR_T1158329972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t1158329972  : public Behaviour_t955675639
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T1158329972_H
#ifndef DEBUGGABLEMONOBEHAVIOUR_T3136086048_H
#define DEBUGGABLEMONOBEHAVIOUR_T3136086048_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.DebuggableMonoBehaviour
struct  DebuggableMonoBehaviour_t3136086048  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGGABLEMONOBEHAVIOUR_T3136086048_H
#ifndef TOUCHMANAGER_T3980263048_H
#define TOUCHMANAGER_T3980263048_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.TouchManager
struct  TouchManager_t3980263048  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Object TouchScript.TouchManager::displayDevice
	Object_t1021602117 * ___displayDevice_6;
	// System.Boolean TouchScript.TouchManager::shouldCreateCameraLayer
	bool ___shouldCreateCameraLayer_7;
	// System.Boolean TouchScript.TouchManager::shouldCreateStandardInput
	bool ___shouldCreateStandardInput_8;
	// System.Boolean TouchScript.TouchManager::useSendMessage
	bool ___useSendMessage_9;
	// TouchScript.TouchManager/MessageType TouchScript.TouchManager::sendMessageEvents
	int32_t ___sendMessageEvents_10;
	// UnityEngine.GameObject TouchScript.TouchManager::sendMessageTarget
	GameObject_t1756533147 * ___sendMessageTarget_11;
	// System.Collections.Generic.List`1<TouchScript.Layers.TouchLayer> TouchScript.TouchManager::layers
	List_1_t2004561110 * ___layers_12;

public:
	inline static int32_t get_offset_of_displayDevice_6() { return static_cast<int32_t>(offsetof(TouchManager_t3980263048, ___displayDevice_6)); }
	inline Object_t1021602117 * get_displayDevice_6() const { return ___displayDevice_6; }
	inline Object_t1021602117 ** get_address_of_displayDevice_6() { return &___displayDevice_6; }
	inline void set_displayDevice_6(Object_t1021602117 * value)
	{
		___displayDevice_6 = value;
		Il2CppCodeGenWriteBarrier((&___displayDevice_6), value);
	}

	inline static int32_t get_offset_of_shouldCreateCameraLayer_7() { return static_cast<int32_t>(offsetof(TouchManager_t3980263048, ___shouldCreateCameraLayer_7)); }
	inline bool get_shouldCreateCameraLayer_7() const { return ___shouldCreateCameraLayer_7; }
	inline bool* get_address_of_shouldCreateCameraLayer_7() { return &___shouldCreateCameraLayer_7; }
	inline void set_shouldCreateCameraLayer_7(bool value)
	{
		___shouldCreateCameraLayer_7 = value;
	}

	inline static int32_t get_offset_of_shouldCreateStandardInput_8() { return static_cast<int32_t>(offsetof(TouchManager_t3980263048, ___shouldCreateStandardInput_8)); }
	inline bool get_shouldCreateStandardInput_8() const { return ___shouldCreateStandardInput_8; }
	inline bool* get_address_of_shouldCreateStandardInput_8() { return &___shouldCreateStandardInput_8; }
	inline void set_shouldCreateStandardInput_8(bool value)
	{
		___shouldCreateStandardInput_8 = value;
	}

	inline static int32_t get_offset_of_useSendMessage_9() { return static_cast<int32_t>(offsetof(TouchManager_t3980263048, ___useSendMessage_9)); }
	inline bool get_useSendMessage_9() const { return ___useSendMessage_9; }
	inline bool* get_address_of_useSendMessage_9() { return &___useSendMessage_9; }
	inline void set_useSendMessage_9(bool value)
	{
		___useSendMessage_9 = value;
	}

	inline static int32_t get_offset_of_sendMessageEvents_10() { return static_cast<int32_t>(offsetof(TouchManager_t3980263048, ___sendMessageEvents_10)); }
	inline int32_t get_sendMessageEvents_10() const { return ___sendMessageEvents_10; }
	inline int32_t* get_address_of_sendMessageEvents_10() { return &___sendMessageEvents_10; }
	inline void set_sendMessageEvents_10(int32_t value)
	{
		___sendMessageEvents_10 = value;
	}

	inline static int32_t get_offset_of_sendMessageTarget_11() { return static_cast<int32_t>(offsetof(TouchManager_t3980263048, ___sendMessageTarget_11)); }
	inline GameObject_t1756533147 * get_sendMessageTarget_11() const { return ___sendMessageTarget_11; }
	inline GameObject_t1756533147 ** get_address_of_sendMessageTarget_11() { return &___sendMessageTarget_11; }
	inline void set_sendMessageTarget_11(GameObject_t1756533147 * value)
	{
		___sendMessageTarget_11 = value;
		Il2CppCodeGenWriteBarrier((&___sendMessageTarget_11), value);
	}

	inline static int32_t get_offset_of_layers_12() { return static_cast<int32_t>(offsetof(TouchManager_t3980263048, ___layers_12)); }
	inline List_1_t2004561110 * get_layers_12() const { return ___layers_12; }
	inline List_1_t2004561110 ** get_address_of_layers_12() { return &___layers_12; }
	inline void set_layers_12(List_1_t2004561110 * value)
	{
		___layers_12 = value;
		Il2CppCodeGenWriteBarrier((&___layers_12), value);
	}
};

struct TouchManager_t3980263048_StaticFields
{
public:
	// UnityEngine.Vector2 TouchScript.TouchManager::INVALID_POSITION
	Vector2_t2243707579  ___INVALID_POSITION_4;
	// System.Version TouchScript.TouchManager::VERSION
	Version_t1755874712 * ___VERSION_5;

public:
	inline static int32_t get_offset_of_INVALID_POSITION_4() { return static_cast<int32_t>(offsetof(TouchManager_t3980263048_StaticFields, ___INVALID_POSITION_4)); }
	inline Vector2_t2243707579  get_INVALID_POSITION_4() const { return ___INVALID_POSITION_4; }
	inline Vector2_t2243707579 * get_address_of_INVALID_POSITION_4() { return &___INVALID_POSITION_4; }
	inline void set_INVALID_POSITION_4(Vector2_t2243707579  value)
	{
		___INVALID_POSITION_4 = value;
	}

	inline static int32_t get_offset_of_VERSION_5() { return static_cast<int32_t>(offsetof(TouchManager_t3980263048_StaticFields, ___VERSION_5)); }
	inline Version_t1755874712 * get_VERSION_5() const { return ___VERSION_5; }
	inline Version_t1755874712 ** get_address_of_VERSION_5() { return &___VERSION_5; }
	inline void set_VERSION_5(Version_t1755874712 * value)
	{
		___VERSION_5 = value;
		Il2CppCodeGenWriteBarrier((&___VERSION_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHMANAGER_T3980263048_H
#ifndef TOUCHLAYER_T2635439978_H
#define TOUCHLAYER_T2635439978_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Layers.TouchLayer
struct  TouchLayer_t2635439978  : public MonoBehaviour_t1158329972
{
public:
	// System.EventHandler`1<TouchScript.Layers.TouchLayerEventArgs> TouchScript.Layers.TouchLayer::touchBeganInvoker
	EventHandler_1_t4133675533 * ___touchBeganInvoker_2;
	// System.String TouchScript.Layers.TouchLayer::Name
	String_t* ___Name_3;
	// TouchScript.Layers.ILayerDelegate TouchScript.Layers.TouchLayer::<Delegate>k__BackingField
	RuntimeObject* ___U3CDelegateU3Ek__BackingField_4;
	// TouchScript.Layers.ProjectionParams TouchScript.Layers.TouchLayer::layerProjectionParams
	ProjectionParams_t2712959773 * ___layerProjectionParams_5;

public:
	inline static int32_t get_offset_of_touchBeganInvoker_2() { return static_cast<int32_t>(offsetof(TouchLayer_t2635439978, ___touchBeganInvoker_2)); }
	inline EventHandler_1_t4133675533 * get_touchBeganInvoker_2() const { return ___touchBeganInvoker_2; }
	inline EventHandler_1_t4133675533 ** get_address_of_touchBeganInvoker_2() { return &___touchBeganInvoker_2; }
	inline void set_touchBeganInvoker_2(EventHandler_1_t4133675533 * value)
	{
		___touchBeganInvoker_2 = value;
		Il2CppCodeGenWriteBarrier((&___touchBeganInvoker_2), value);
	}

	inline static int32_t get_offset_of_Name_3() { return static_cast<int32_t>(offsetof(TouchLayer_t2635439978, ___Name_3)); }
	inline String_t* get_Name_3() const { return ___Name_3; }
	inline String_t** get_address_of_Name_3() { return &___Name_3; }
	inline void set_Name_3(String_t* value)
	{
		___Name_3 = value;
		Il2CppCodeGenWriteBarrier((&___Name_3), value);
	}

	inline static int32_t get_offset_of_U3CDelegateU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(TouchLayer_t2635439978, ___U3CDelegateU3Ek__BackingField_4)); }
	inline RuntimeObject* get_U3CDelegateU3Ek__BackingField_4() const { return ___U3CDelegateU3Ek__BackingField_4; }
	inline RuntimeObject** get_address_of_U3CDelegateU3Ek__BackingField_4() { return &___U3CDelegateU3Ek__BackingField_4; }
	inline void set_U3CDelegateU3Ek__BackingField_4(RuntimeObject* value)
	{
		___U3CDelegateU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDelegateU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_layerProjectionParams_5() { return static_cast<int32_t>(offsetof(TouchLayer_t2635439978, ___layerProjectionParams_5)); }
	inline ProjectionParams_t2712959773 * get_layerProjectionParams_5() const { return ___layerProjectionParams_5; }
	inline ProjectionParams_t2712959773 ** get_address_of_layerProjectionParams_5() { return &___layerProjectionParams_5; }
	inline void set_layerProjectionParams_5(ProjectionParams_t2712959773 * value)
	{
		___layerProjectionParams_5 = value;
		Il2CppCodeGenWriteBarrier((&___layerProjectionParams_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHLAYER_T2635439978_H
#ifndef INPUTSOURCE_T3078263767_H
#define INPUTSOURCE_T3078263767_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.InputSources.InputSource
struct  InputSource_t3078263767  : public MonoBehaviour_t1158329972
{
public:
	// TouchScript.InputSources.ICoordinatesRemapper TouchScript.InputSources.InputSource::<CoordinatesRemapper>k__BackingField
	RuntimeObject* ___U3CCoordinatesRemapperU3Ek__BackingField_2;
	// System.Boolean TouchScript.InputSources.InputSource::advancedProps
	bool ___advancedProps_3;
	// TouchScript.TouchManagerInstance TouchScript.InputSources.InputSource::manager
	TouchManagerInstance_t2629118981 * ___manager_4;

public:
	inline static int32_t get_offset_of_U3CCoordinatesRemapperU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(InputSource_t3078263767, ___U3CCoordinatesRemapperU3Ek__BackingField_2)); }
	inline RuntimeObject* get_U3CCoordinatesRemapperU3Ek__BackingField_2() const { return ___U3CCoordinatesRemapperU3Ek__BackingField_2; }
	inline RuntimeObject** get_address_of_U3CCoordinatesRemapperU3Ek__BackingField_2() { return &___U3CCoordinatesRemapperU3Ek__BackingField_2; }
	inline void set_U3CCoordinatesRemapperU3Ek__BackingField_2(RuntimeObject* value)
	{
		___U3CCoordinatesRemapperU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCoordinatesRemapperU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_advancedProps_3() { return static_cast<int32_t>(offsetof(InputSource_t3078263767, ___advancedProps_3)); }
	inline bool get_advancedProps_3() const { return ___advancedProps_3; }
	inline bool* get_address_of_advancedProps_3() { return &___advancedProps_3; }
	inline void set_advancedProps_3(bool value)
	{
		___advancedProps_3 = value;
	}

	inline static int32_t get_offset_of_manager_4() { return static_cast<int32_t>(offsetof(InputSource_t3078263767, ___manager_4)); }
	inline TouchManagerInstance_t2629118981 * get_manager_4() const { return ___manager_4; }
	inline TouchManagerInstance_t2629118981 ** get_address_of_manager_4() { return &___manager_4; }
	inline void set_manager_4(TouchManagerInstance_t2629118981 * value)
	{
		___manager_4 = value;
		Il2CppCodeGenWriteBarrier((&___manager_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTSOURCE_T3078263767_H
#ifndef STANDARDINPUT_T4102879489_H
#define STANDARDINPUT_T4102879489_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.InputSources.StandardInput
struct  StandardInput_t4102879489  : public InputSource_t3078263767
{
public:
	// TouchScript.Tags TouchScript.InputSources.StandardInput::TouchTags
	Tags_t1265380163 * ___TouchTags_5;
	// TouchScript.Tags TouchScript.InputSources.StandardInput::MouseTags
	Tags_t1265380163 * ___MouseTags_6;
	// TouchScript.Tags TouchScript.InputSources.StandardInput::PenTags
	Tags_t1265380163 * ___PenTags_7;
	// TouchScript.InputSources.StandardInput/Windows8TouchAPIType TouchScript.InputSources.StandardInput::Windows8Touch
	int32_t ___Windows8Touch_8;
	// TouchScript.InputSources.StandardInput/Windows7TouchAPIType TouchScript.InputSources.StandardInput::Windows7Touch
	int32_t ___Windows7Touch_9;
	// System.Boolean TouchScript.InputSources.StandardInput::WebPlayerTouch
	bool ___WebPlayerTouch_10;
	// System.Boolean TouchScript.InputSources.StandardInput::WebGLTouch
	bool ___WebGLTouch_11;
	// System.Boolean TouchScript.InputSources.StandardInput::Windows8Mouse
	bool ___Windows8Mouse_12;
	// System.Boolean TouchScript.InputSources.StandardInput::Windows7Mouse
	bool ___Windows7Mouse_13;
	// System.Boolean TouchScript.InputSources.StandardInput::UniversalWindowsMouse
	bool ___UniversalWindowsMouse_14;
	// TouchScript.InputSources.InputHandlers.MouseHandler TouchScript.InputSources.StandardInput::mouseHandler
	MouseHandler_t3116661769 * ___mouseHandler_15;
	// TouchScript.InputSources.InputHandlers.TouchHandler TouchScript.InputSources.StandardInput::touchHandler
	TouchHandler_t2521645213 * ___touchHandler_16;

public:
	inline static int32_t get_offset_of_TouchTags_5() { return static_cast<int32_t>(offsetof(StandardInput_t4102879489, ___TouchTags_5)); }
	inline Tags_t1265380163 * get_TouchTags_5() const { return ___TouchTags_5; }
	inline Tags_t1265380163 ** get_address_of_TouchTags_5() { return &___TouchTags_5; }
	inline void set_TouchTags_5(Tags_t1265380163 * value)
	{
		___TouchTags_5 = value;
		Il2CppCodeGenWriteBarrier((&___TouchTags_5), value);
	}

	inline static int32_t get_offset_of_MouseTags_6() { return static_cast<int32_t>(offsetof(StandardInput_t4102879489, ___MouseTags_6)); }
	inline Tags_t1265380163 * get_MouseTags_6() const { return ___MouseTags_6; }
	inline Tags_t1265380163 ** get_address_of_MouseTags_6() { return &___MouseTags_6; }
	inline void set_MouseTags_6(Tags_t1265380163 * value)
	{
		___MouseTags_6 = value;
		Il2CppCodeGenWriteBarrier((&___MouseTags_6), value);
	}

	inline static int32_t get_offset_of_PenTags_7() { return static_cast<int32_t>(offsetof(StandardInput_t4102879489, ___PenTags_7)); }
	inline Tags_t1265380163 * get_PenTags_7() const { return ___PenTags_7; }
	inline Tags_t1265380163 ** get_address_of_PenTags_7() { return &___PenTags_7; }
	inline void set_PenTags_7(Tags_t1265380163 * value)
	{
		___PenTags_7 = value;
		Il2CppCodeGenWriteBarrier((&___PenTags_7), value);
	}

	inline static int32_t get_offset_of_Windows8Touch_8() { return static_cast<int32_t>(offsetof(StandardInput_t4102879489, ___Windows8Touch_8)); }
	inline int32_t get_Windows8Touch_8() const { return ___Windows8Touch_8; }
	inline int32_t* get_address_of_Windows8Touch_8() { return &___Windows8Touch_8; }
	inline void set_Windows8Touch_8(int32_t value)
	{
		___Windows8Touch_8 = value;
	}

	inline static int32_t get_offset_of_Windows7Touch_9() { return static_cast<int32_t>(offsetof(StandardInput_t4102879489, ___Windows7Touch_9)); }
	inline int32_t get_Windows7Touch_9() const { return ___Windows7Touch_9; }
	inline int32_t* get_address_of_Windows7Touch_9() { return &___Windows7Touch_9; }
	inline void set_Windows7Touch_9(int32_t value)
	{
		___Windows7Touch_9 = value;
	}

	inline static int32_t get_offset_of_WebPlayerTouch_10() { return static_cast<int32_t>(offsetof(StandardInput_t4102879489, ___WebPlayerTouch_10)); }
	inline bool get_WebPlayerTouch_10() const { return ___WebPlayerTouch_10; }
	inline bool* get_address_of_WebPlayerTouch_10() { return &___WebPlayerTouch_10; }
	inline void set_WebPlayerTouch_10(bool value)
	{
		___WebPlayerTouch_10 = value;
	}

	inline static int32_t get_offset_of_WebGLTouch_11() { return static_cast<int32_t>(offsetof(StandardInput_t4102879489, ___WebGLTouch_11)); }
	inline bool get_WebGLTouch_11() const { return ___WebGLTouch_11; }
	inline bool* get_address_of_WebGLTouch_11() { return &___WebGLTouch_11; }
	inline void set_WebGLTouch_11(bool value)
	{
		___WebGLTouch_11 = value;
	}

	inline static int32_t get_offset_of_Windows8Mouse_12() { return static_cast<int32_t>(offsetof(StandardInput_t4102879489, ___Windows8Mouse_12)); }
	inline bool get_Windows8Mouse_12() const { return ___Windows8Mouse_12; }
	inline bool* get_address_of_Windows8Mouse_12() { return &___Windows8Mouse_12; }
	inline void set_Windows8Mouse_12(bool value)
	{
		___Windows8Mouse_12 = value;
	}

	inline static int32_t get_offset_of_Windows7Mouse_13() { return static_cast<int32_t>(offsetof(StandardInput_t4102879489, ___Windows7Mouse_13)); }
	inline bool get_Windows7Mouse_13() const { return ___Windows7Mouse_13; }
	inline bool* get_address_of_Windows7Mouse_13() { return &___Windows7Mouse_13; }
	inline void set_Windows7Mouse_13(bool value)
	{
		___Windows7Mouse_13 = value;
	}

	inline static int32_t get_offset_of_UniversalWindowsMouse_14() { return static_cast<int32_t>(offsetof(StandardInput_t4102879489, ___UniversalWindowsMouse_14)); }
	inline bool get_UniversalWindowsMouse_14() const { return ___UniversalWindowsMouse_14; }
	inline bool* get_address_of_UniversalWindowsMouse_14() { return &___UniversalWindowsMouse_14; }
	inline void set_UniversalWindowsMouse_14(bool value)
	{
		___UniversalWindowsMouse_14 = value;
	}

	inline static int32_t get_offset_of_mouseHandler_15() { return static_cast<int32_t>(offsetof(StandardInput_t4102879489, ___mouseHandler_15)); }
	inline MouseHandler_t3116661769 * get_mouseHandler_15() const { return ___mouseHandler_15; }
	inline MouseHandler_t3116661769 ** get_address_of_mouseHandler_15() { return &___mouseHandler_15; }
	inline void set_mouseHandler_15(MouseHandler_t3116661769 * value)
	{
		___mouseHandler_15 = value;
		Il2CppCodeGenWriteBarrier((&___mouseHandler_15), value);
	}

	inline static int32_t get_offset_of_touchHandler_16() { return static_cast<int32_t>(offsetof(StandardInput_t4102879489, ___touchHandler_16)); }
	inline TouchHandler_t2521645213 * get_touchHandler_16() const { return ___touchHandler_16; }
	inline TouchHandler_t2521645213 ** get_address_of_touchHandler_16() { return &___touchHandler_16; }
	inline void set_touchHandler_16(TouchHandler_t2521645213 * value)
	{
		___touchHandler_16 = value;
		Il2CppCodeGenWriteBarrier((&___touchHandler_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STANDARDINPUT_T4102879489_H
#ifndef MOUSEINPUT_T301708067_H
#define MOUSEINPUT_T301708067_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.InputSources.MouseInput
struct  MouseInput_t301708067  : public InputSource_t3078263767
{
public:
	// System.Boolean TouchScript.InputSources.MouseInput::DisableOnMobilePlatforms
	bool ___DisableOnMobilePlatforms_5;
	// TouchScript.Tags TouchScript.InputSources.MouseInput::Tags
	Tags_t1265380163 * ___Tags_6;
	// TouchScript.InputSources.InputHandlers.MouseHandler TouchScript.InputSources.MouseInput::mouseHandler
	MouseHandler_t3116661769 * ___mouseHandler_7;

public:
	inline static int32_t get_offset_of_DisableOnMobilePlatforms_5() { return static_cast<int32_t>(offsetof(MouseInput_t301708067, ___DisableOnMobilePlatforms_5)); }
	inline bool get_DisableOnMobilePlatforms_5() const { return ___DisableOnMobilePlatforms_5; }
	inline bool* get_address_of_DisableOnMobilePlatforms_5() { return &___DisableOnMobilePlatforms_5; }
	inline void set_DisableOnMobilePlatforms_5(bool value)
	{
		___DisableOnMobilePlatforms_5 = value;
	}

	inline static int32_t get_offset_of_Tags_6() { return static_cast<int32_t>(offsetof(MouseInput_t301708067, ___Tags_6)); }
	inline Tags_t1265380163 * get_Tags_6() const { return ___Tags_6; }
	inline Tags_t1265380163 ** get_address_of_Tags_6() { return &___Tags_6; }
	inline void set_Tags_6(Tags_t1265380163 * value)
	{
		___Tags_6 = value;
		Il2CppCodeGenWriteBarrier((&___Tags_6), value);
	}

	inline static int32_t get_offset_of_mouseHandler_7() { return static_cast<int32_t>(offsetof(MouseInput_t301708067, ___mouseHandler_7)); }
	inline MouseHandler_t3116661769 * get_mouseHandler_7() const { return ___mouseHandler_7; }
	inline MouseHandler_t3116661769 ** get_address_of_mouseHandler_7() { return &___mouseHandler_7; }
	inline void set_mouseHandler_7(MouseHandler_t3116661769 * value)
	{
		___mouseHandler_7 = value;
		Il2CppCodeGenWriteBarrier((&___mouseHandler_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSEINPUT_T301708067_H
#ifndef FULLSCREENLAYER_T1925731828_H
#define FULLSCREENLAYER_T1925731828_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Layers.FullscreenLayer
struct  FullscreenLayer_t1925731828  : public TouchLayer_t2635439978
{
public:
	// TouchScript.Layers.FullscreenLayer/LayerType TouchScript.Layers.FullscreenLayer::type
	int32_t ___type_6;
	// UnityEngine.Camera TouchScript.Layers.FullscreenLayer::_camera
	Camera_t189460977 * ____camera_7;
	// UnityEngine.Transform TouchScript.Layers.FullscreenLayer::cameraTransform
	Transform_t3275118058 * ___cameraTransform_8;
	// System.Collections.Generic.List`1<TouchScript.Hit.HitTest> TouchScript.Layers.FullscreenLayer::tmpHitTestList
	List_1_t137760637 * ___tmpHitTestList_9;

public:
	inline static int32_t get_offset_of_type_6() { return static_cast<int32_t>(offsetof(FullscreenLayer_t1925731828, ___type_6)); }
	inline int32_t get_type_6() const { return ___type_6; }
	inline int32_t* get_address_of_type_6() { return &___type_6; }
	inline void set_type_6(int32_t value)
	{
		___type_6 = value;
	}

	inline static int32_t get_offset_of__camera_7() { return static_cast<int32_t>(offsetof(FullscreenLayer_t1925731828, ____camera_7)); }
	inline Camera_t189460977 * get__camera_7() const { return ____camera_7; }
	inline Camera_t189460977 ** get_address_of__camera_7() { return &____camera_7; }
	inline void set__camera_7(Camera_t189460977 * value)
	{
		____camera_7 = value;
		Il2CppCodeGenWriteBarrier((&____camera_7), value);
	}

	inline static int32_t get_offset_of_cameraTransform_8() { return static_cast<int32_t>(offsetof(FullscreenLayer_t1925731828, ___cameraTransform_8)); }
	inline Transform_t3275118058 * get_cameraTransform_8() const { return ___cameraTransform_8; }
	inline Transform_t3275118058 ** get_address_of_cameraTransform_8() { return &___cameraTransform_8; }
	inline void set_cameraTransform_8(Transform_t3275118058 * value)
	{
		___cameraTransform_8 = value;
		Il2CppCodeGenWriteBarrier((&___cameraTransform_8), value);
	}

	inline static int32_t get_offset_of_tmpHitTestList_9() { return static_cast<int32_t>(offsetof(FullscreenLayer_t1925731828, ___tmpHitTestList_9)); }
	inline List_1_t137760637 * get_tmpHitTestList_9() const { return ___tmpHitTestList_9; }
	inline List_1_t137760637 ** get_address_of_tmpHitTestList_9() { return &___tmpHitTestList_9; }
	inline void set_tmpHitTestList_9(List_1_t137760637 * value)
	{
		___tmpHitTestList_9 = value;
		Il2CppCodeGenWriteBarrier((&___tmpHitTestList_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FULLSCREENLAYER_T1925731828_H
#ifndef UILAYER_T314035379_H
#define UILAYER_T314035379_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Layers.UILayer
struct  UILayer_t314035379  : public TouchLayer_t2635439978
{
public:
	// UnityEngine.LayerMask TouchScript.Layers.UILayer::layerMask
	LayerMask_t3188175821  ___layerMask_7;
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult> TouchScript.Layers.UILayer::raycastResultCache
	List_1_t3685274804 * ___raycastResultCache_8;
	// System.Collections.Generic.List`1<TouchScript.Hit.HitTest> TouchScript.Layers.UILayer::tmpHitTestList
	List_1_t137760637 * ___tmpHitTestList_9;
	// UnityEngine.EventSystems.PointerEventData TouchScript.Layers.UILayer::pointerDataCache
	PointerEventData_t1599784723 * ___pointerDataCache_10;
	// UnityEngine.EventSystems.EventSystem TouchScript.Layers.UILayer::eventSystem
	EventSystem_t3466835263 * ___eventSystem_11;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Canvas,TouchScript.Layers.ProjectionParams> TouchScript.Layers.UILayer::projectionParamsCache
	Dictionary_2_t2765956170 * ___projectionParamsCache_12;

public:
	inline static int32_t get_offset_of_layerMask_7() { return static_cast<int32_t>(offsetof(UILayer_t314035379, ___layerMask_7)); }
	inline LayerMask_t3188175821  get_layerMask_7() const { return ___layerMask_7; }
	inline LayerMask_t3188175821 * get_address_of_layerMask_7() { return &___layerMask_7; }
	inline void set_layerMask_7(LayerMask_t3188175821  value)
	{
		___layerMask_7 = value;
	}

	inline static int32_t get_offset_of_raycastResultCache_8() { return static_cast<int32_t>(offsetof(UILayer_t314035379, ___raycastResultCache_8)); }
	inline List_1_t3685274804 * get_raycastResultCache_8() const { return ___raycastResultCache_8; }
	inline List_1_t3685274804 ** get_address_of_raycastResultCache_8() { return &___raycastResultCache_8; }
	inline void set_raycastResultCache_8(List_1_t3685274804 * value)
	{
		___raycastResultCache_8 = value;
		Il2CppCodeGenWriteBarrier((&___raycastResultCache_8), value);
	}

	inline static int32_t get_offset_of_tmpHitTestList_9() { return static_cast<int32_t>(offsetof(UILayer_t314035379, ___tmpHitTestList_9)); }
	inline List_1_t137760637 * get_tmpHitTestList_9() const { return ___tmpHitTestList_9; }
	inline List_1_t137760637 ** get_address_of_tmpHitTestList_9() { return &___tmpHitTestList_9; }
	inline void set_tmpHitTestList_9(List_1_t137760637 * value)
	{
		___tmpHitTestList_9 = value;
		Il2CppCodeGenWriteBarrier((&___tmpHitTestList_9), value);
	}

	inline static int32_t get_offset_of_pointerDataCache_10() { return static_cast<int32_t>(offsetof(UILayer_t314035379, ___pointerDataCache_10)); }
	inline PointerEventData_t1599784723 * get_pointerDataCache_10() const { return ___pointerDataCache_10; }
	inline PointerEventData_t1599784723 ** get_address_of_pointerDataCache_10() { return &___pointerDataCache_10; }
	inline void set_pointerDataCache_10(PointerEventData_t1599784723 * value)
	{
		___pointerDataCache_10 = value;
		Il2CppCodeGenWriteBarrier((&___pointerDataCache_10), value);
	}

	inline static int32_t get_offset_of_eventSystem_11() { return static_cast<int32_t>(offsetof(UILayer_t314035379, ___eventSystem_11)); }
	inline EventSystem_t3466835263 * get_eventSystem_11() const { return ___eventSystem_11; }
	inline EventSystem_t3466835263 ** get_address_of_eventSystem_11() { return &___eventSystem_11; }
	inline void set_eventSystem_11(EventSystem_t3466835263 * value)
	{
		___eventSystem_11 = value;
		Il2CppCodeGenWriteBarrier((&___eventSystem_11), value);
	}

	inline static int32_t get_offset_of_projectionParamsCache_12() { return static_cast<int32_t>(offsetof(UILayer_t314035379, ___projectionParamsCache_12)); }
	inline Dictionary_2_t2765956170 * get_projectionParamsCache_12() const { return ___projectionParamsCache_12; }
	inline Dictionary_2_t2765956170 ** get_address_of_projectionParamsCache_12() { return &___projectionParamsCache_12; }
	inline void set_projectionParamsCache_12(Dictionary_2_t2765956170 * value)
	{
		___projectionParamsCache_12 = value;
		Il2CppCodeGenWriteBarrier((&___projectionParamsCache_12), value);
	}
};

struct UILayer_t314035379_StaticFields
{
public:
	// TouchScript.Layers.UILayer TouchScript.Layers.UILayer::instance
	UILayer_t314035379 * ___instance_6;

public:
	inline static int32_t get_offset_of_instance_6() { return static_cast<int32_t>(offsetof(UILayer_t314035379_StaticFields, ___instance_6)); }
	inline UILayer_t314035379 * get_instance_6() const { return ___instance_6; }
	inline UILayer_t314035379 ** get_address_of_instance_6() { return &___instance_6; }
	inline void set_instance_6(UILayer_t314035379 * value)
	{
		___instance_6 = value;
		Il2CppCodeGenWriteBarrier((&___instance_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UILAYER_T314035379_H
#ifndef TOUCHMANAGERINSTANCE_T2629118981_H
#define TOUCHMANAGERINSTANCE_T2629118981_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.TouchManagerInstance
struct  TouchManagerInstance_t2629118981  : public DebuggableMonoBehaviour_t3136086048
{
public:
	// System.EventHandler`1<TouchScript.TouchEventArgs> TouchScript.TouchManagerInstance::touchesBeganInvoker
	EventHandler_1_t509234338 * ___touchesBeganInvoker_2;
	// System.EventHandler`1<TouchScript.TouchEventArgs> TouchScript.TouchManagerInstance::touchesMovedInvoker
	EventHandler_1_t509234338 * ___touchesMovedInvoker_3;
	// System.EventHandler`1<TouchScript.TouchEventArgs> TouchScript.TouchManagerInstance::touchesEndedInvoker
	EventHandler_1_t509234338 * ___touchesEndedInvoker_4;
	// System.EventHandler`1<TouchScript.TouchEventArgs> TouchScript.TouchManagerInstance::touchesCancelledInvoker
	EventHandler_1_t509234338 * ___touchesCancelledInvoker_5;
	// System.EventHandler TouchScript.TouchManagerInstance::frameStartedInvoker
	EventHandler_t277755526 * ___frameStartedInvoker_6;
	// System.EventHandler TouchScript.TouchManagerInstance::frameFinishedInvoker
	EventHandler_t277755526 * ___frameFinishedInvoker_7;
	// System.Boolean TouchScript.TouchManagerInstance::shouldCreateCameraLayer
	bool ___shouldCreateCameraLayer_10;
	// System.Boolean TouchScript.TouchManagerInstance::shouldCreateStandardInput
	bool ___shouldCreateStandardInput_11;
	// TouchScript.Devices.Display.IDisplayDevice TouchScript.TouchManagerInstance::displayDevice
	RuntimeObject* ___displayDevice_12;
	// System.Single TouchScript.TouchManagerInstance::dpi
	float ___dpi_13;
	// System.Single TouchScript.TouchManagerInstance::dotsPerCentimeter
	float ___dotsPerCentimeter_14;
	// System.Collections.Generic.List`1<TouchScript.Layers.TouchLayer> TouchScript.TouchManagerInstance::layers
	List_1_t2004561110 * ___layers_15;
	// System.Int32 TouchScript.TouchManagerInstance::layerCount
	int32_t ___layerCount_16;
	// System.Collections.Generic.List`1<TouchScript.InputSources.IInputSource> TouchScript.TouchManagerInstance::inputs
	List_1_t2635681470 * ___inputs_17;
	// System.Int32 TouchScript.TouchManagerInstance::inputCount
	int32_t ___inputCount_18;
	// System.Collections.Generic.List`1<TouchScript.TouchPoint> TouchScript.TouchManagerInstance::touches
	List_1_t328750215 * ___touches_19;
	// System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.TouchPoint> TouchScript.TouchManagerInstance::idToTouch
	Dictionary_2_t4262422014 * ___idToTouch_20;
	// System.Collections.Generic.List`1<TouchScript.TouchPoint> TouchScript.TouchManagerInstance::touchesBegan
	List_1_t328750215 * ___touchesBegan_21;
	// System.Collections.Generic.HashSet`1<System.Int32> TouchScript.TouchManagerInstance::touchesUpdated
	HashSet_1_t405338302 * ___touchesUpdated_22;
	// System.Collections.Generic.HashSet`1<System.Int32> TouchScript.TouchManagerInstance::touchesEnded
	HashSet_1_t405338302 * ___touchesEnded_23;
	// System.Collections.Generic.HashSet`1<System.Int32> TouchScript.TouchManagerInstance::touchesCancelled
	HashSet_1_t405338302 * ___touchesCancelled_24;
	// System.Int32 TouchScript.TouchManagerInstance::nextTouchId
	int32_t ___nextTouchId_28;
	// System.Object TouchScript.TouchManagerInstance::touchLock
	RuntimeObject * ___touchLock_29;

public:
	inline static int32_t get_offset_of_touchesBeganInvoker_2() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2629118981, ___touchesBeganInvoker_2)); }
	inline EventHandler_1_t509234338 * get_touchesBeganInvoker_2() const { return ___touchesBeganInvoker_2; }
	inline EventHandler_1_t509234338 ** get_address_of_touchesBeganInvoker_2() { return &___touchesBeganInvoker_2; }
	inline void set_touchesBeganInvoker_2(EventHandler_1_t509234338 * value)
	{
		___touchesBeganInvoker_2 = value;
		Il2CppCodeGenWriteBarrier((&___touchesBeganInvoker_2), value);
	}

	inline static int32_t get_offset_of_touchesMovedInvoker_3() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2629118981, ___touchesMovedInvoker_3)); }
	inline EventHandler_1_t509234338 * get_touchesMovedInvoker_3() const { return ___touchesMovedInvoker_3; }
	inline EventHandler_1_t509234338 ** get_address_of_touchesMovedInvoker_3() { return &___touchesMovedInvoker_3; }
	inline void set_touchesMovedInvoker_3(EventHandler_1_t509234338 * value)
	{
		___touchesMovedInvoker_3 = value;
		Il2CppCodeGenWriteBarrier((&___touchesMovedInvoker_3), value);
	}

	inline static int32_t get_offset_of_touchesEndedInvoker_4() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2629118981, ___touchesEndedInvoker_4)); }
	inline EventHandler_1_t509234338 * get_touchesEndedInvoker_4() const { return ___touchesEndedInvoker_4; }
	inline EventHandler_1_t509234338 ** get_address_of_touchesEndedInvoker_4() { return &___touchesEndedInvoker_4; }
	inline void set_touchesEndedInvoker_4(EventHandler_1_t509234338 * value)
	{
		___touchesEndedInvoker_4 = value;
		Il2CppCodeGenWriteBarrier((&___touchesEndedInvoker_4), value);
	}

	inline static int32_t get_offset_of_touchesCancelledInvoker_5() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2629118981, ___touchesCancelledInvoker_5)); }
	inline EventHandler_1_t509234338 * get_touchesCancelledInvoker_5() const { return ___touchesCancelledInvoker_5; }
	inline EventHandler_1_t509234338 ** get_address_of_touchesCancelledInvoker_5() { return &___touchesCancelledInvoker_5; }
	inline void set_touchesCancelledInvoker_5(EventHandler_1_t509234338 * value)
	{
		___touchesCancelledInvoker_5 = value;
		Il2CppCodeGenWriteBarrier((&___touchesCancelledInvoker_5), value);
	}

	inline static int32_t get_offset_of_frameStartedInvoker_6() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2629118981, ___frameStartedInvoker_6)); }
	inline EventHandler_t277755526 * get_frameStartedInvoker_6() const { return ___frameStartedInvoker_6; }
	inline EventHandler_t277755526 ** get_address_of_frameStartedInvoker_6() { return &___frameStartedInvoker_6; }
	inline void set_frameStartedInvoker_6(EventHandler_t277755526 * value)
	{
		___frameStartedInvoker_6 = value;
		Il2CppCodeGenWriteBarrier((&___frameStartedInvoker_6), value);
	}

	inline static int32_t get_offset_of_frameFinishedInvoker_7() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2629118981, ___frameFinishedInvoker_7)); }
	inline EventHandler_t277755526 * get_frameFinishedInvoker_7() const { return ___frameFinishedInvoker_7; }
	inline EventHandler_t277755526 ** get_address_of_frameFinishedInvoker_7() { return &___frameFinishedInvoker_7; }
	inline void set_frameFinishedInvoker_7(EventHandler_t277755526 * value)
	{
		___frameFinishedInvoker_7 = value;
		Il2CppCodeGenWriteBarrier((&___frameFinishedInvoker_7), value);
	}

	inline static int32_t get_offset_of_shouldCreateCameraLayer_10() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2629118981, ___shouldCreateCameraLayer_10)); }
	inline bool get_shouldCreateCameraLayer_10() const { return ___shouldCreateCameraLayer_10; }
	inline bool* get_address_of_shouldCreateCameraLayer_10() { return &___shouldCreateCameraLayer_10; }
	inline void set_shouldCreateCameraLayer_10(bool value)
	{
		___shouldCreateCameraLayer_10 = value;
	}

	inline static int32_t get_offset_of_shouldCreateStandardInput_11() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2629118981, ___shouldCreateStandardInput_11)); }
	inline bool get_shouldCreateStandardInput_11() const { return ___shouldCreateStandardInput_11; }
	inline bool* get_address_of_shouldCreateStandardInput_11() { return &___shouldCreateStandardInput_11; }
	inline void set_shouldCreateStandardInput_11(bool value)
	{
		___shouldCreateStandardInput_11 = value;
	}

	inline static int32_t get_offset_of_displayDevice_12() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2629118981, ___displayDevice_12)); }
	inline RuntimeObject* get_displayDevice_12() const { return ___displayDevice_12; }
	inline RuntimeObject** get_address_of_displayDevice_12() { return &___displayDevice_12; }
	inline void set_displayDevice_12(RuntimeObject* value)
	{
		___displayDevice_12 = value;
		Il2CppCodeGenWriteBarrier((&___displayDevice_12), value);
	}

	inline static int32_t get_offset_of_dpi_13() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2629118981, ___dpi_13)); }
	inline float get_dpi_13() const { return ___dpi_13; }
	inline float* get_address_of_dpi_13() { return &___dpi_13; }
	inline void set_dpi_13(float value)
	{
		___dpi_13 = value;
	}

	inline static int32_t get_offset_of_dotsPerCentimeter_14() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2629118981, ___dotsPerCentimeter_14)); }
	inline float get_dotsPerCentimeter_14() const { return ___dotsPerCentimeter_14; }
	inline float* get_address_of_dotsPerCentimeter_14() { return &___dotsPerCentimeter_14; }
	inline void set_dotsPerCentimeter_14(float value)
	{
		___dotsPerCentimeter_14 = value;
	}

	inline static int32_t get_offset_of_layers_15() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2629118981, ___layers_15)); }
	inline List_1_t2004561110 * get_layers_15() const { return ___layers_15; }
	inline List_1_t2004561110 ** get_address_of_layers_15() { return &___layers_15; }
	inline void set_layers_15(List_1_t2004561110 * value)
	{
		___layers_15 = value;
		Il2CppCodeGenWriteBarrier((&___layers_15), value);
	}

	inline static int32_t get_offset_of_layerCount_16() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2629118981, ___layerCount_16)); }
	inline int32_t get_layerCount_16() const { return ___layerCount_16; }
	inline int32_t* get_address_of_layerCount_16() { return &___layerCount_16; }
	inline void set_layerCount_16(int32_t value)
	{
		___layerCount_16 = value;
	}

	inline static int32_t get_offset_of_inputs_17() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2629118981, ___inputs_17)); }
	inline List_1_t2635681470 * get_inputs_17() const { return ___inputs_17; }
	inline List_1_t2635681470 ** get_address_of_inputs_17() { return &___inputs_17; }
	inline void set_inputs_17(List_1_t2635681470 * value)
	{
		___inputs_17 = value;
		Il2CppCodeGenWriteBarrier((&___inputs_17), value);
	}

	inline static int32_t get_offset_of_inputCount_18() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2629118981, ___inputCount_18)); }
	inline int32_t get_inputCount_18() const { return ___inputCount_18; }
	inline int32_t* get_address_of_inputCount_18() { return &___inputCount_18; }
	inline void set_inputCount_18(int32_t value)
	{
		___inputCount_18 = value;
	}

	inline static int32_t get_offset_of_touches_19() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2629118981, ___touches_19)); }
	inline List_1_t328750215 * get_touches_19() const { return ___touches_19; }
	inline List_1_t328750215 ** get_address_of_touches_19() { return &___touches_19; }
	inline void set_touches_19(List_1_t328750215 * value)
	{
		___touches_19 = value;
		Il2CppCodeGenWriteBarrier((&___touches_19), value);
	}

	inline static int32_t get_offset_of_idToTouch_20() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2629118981, ___idToTouch_20)); }
	inline Dictionary_2_t4262422014 * get_idToTouch_20() const { return ___idToTouch_20; }
	inline Dictionary_2_t4262422014 ** get_address_of_idToTouch_20() { return &___idToTouch_20; }
	inline void set_idToTouch_20(Dictionary_2_t4262422014 * value)
	{
		___idToTouch_20 = value;
		Il2CppCodeGenWriteBarrier((&___idToTouch_20), value);
	}

	inline static int32_t get_offset_of_touchesBegan_21() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2629118981, ___touchesBegan_21)); }
	inline List_1_t328750215 * get_touchesBegan_21() const { return ___touchesBegan_21; }
	inline List_1_t328750215 ** get_address_of_touchesBegan_21() { return &___touchesBegan_21; }
	inline void set_touchesBegan_21(List_1_t328750215 * value)
	{
		___touchesBegan_21 = value;
		Il2CppCodeGenWriteBarrier((&___touchesBegan_21), value);
	}

	inline static int32_t get_offset_of_touchesUpdated_22() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2629118981, ___touchesUpdated_22)); }
	inline HashSet_1_t405338302 * get_touchesUpdated_22() const { return ___touchesUpdated_22; }
	inline HashSet_1_t405338302 ** get_address_of_touchesUpdated_22() { return &___touchesUpdated_22; }
	inline void set_touchesUpdated_22(HashSet_1_t405338302 * value)
	{
		___touchesUpdated_22 = value;
		Il2CppCodeGenWriteBarrier((&___touchesUpdated_22), value);
	}

	inline static int32_t get_offset_of_touchesEnded_23() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2629118981, ___touchesEnded_23)); }
	inline HashSet_1_t405338302 * get_touchesEnded_23() const { return ___touchesEnded_23; }
	inline HashSet_1_t405338302 ** get_address_of_touchesEnded_23() { return &___touchesEnded_23; }
	inline void set_touchesEnded_23(HashSet_1_t405338302 * value)
	{
		___touchesEnded_23 = value;
		Il2CppCodeGenWriteBarrier((&___touchesEnded_23), value);
	}

	inline static int32_t get_offset_of_touchesCancelled_24() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2629118981, ___touchesCancelled_24)); }
	inline HashSet_1_t405338302 * get_touchesCancelled_24() const { return ___touchesCancelled_24; }
	inline HashSet_1_t405338302 ** get_address_of_touchesCancelled_24() { return &___touchesCancelled_24; }
	inline void set_touchesCancelled_24(HashSet_1_t405338302 * value)
	{
		___touchesCancelled_24 = value;
		Il2CppCodeGenWriteBarrier((&___touchesCancelled_24), value);
	}

	inline static int32_t get_offset_of_nextTouchId_28() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2629118981, ___nextTouchId_28)); }
	inline int32_t get_nextTouchId_28() const { return ___nextTouchId_28; }
	inline int32_t* get_address_of_nextTouchId_28() { return &___nextTouchId_28; }
	inline void set_nextTouchId_28(int32_t value)
	{
		___nextTouchId_28 = value;
	}

	inline static int32_t get_offset_of_touchLock_29() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2629118981, ___touchLock_29)); }
	inline RuntimeObject * get_touchLock_29() const { return ___touchLock_29; }
	inline RuntimeObject ** get_address_of_touchLock_29() { return &___touchLock_29; }
	inline void set_touchLock_29(RuntimeObject * value)
	{
		___touchLock_29 = value;
		Il2CppCodeGenWriteBarrier((&___touchLock_29), value);
	}
};

struct TouchManagerInstance_t2629118981_StaticFields
{
public:
	// System.Boolean TouchScript.TouchManagerInstance::shuttingDown
	bool ___shuttingDown_8;
	// TouchScript.TouchManagerInstance TouchScript.TouchManagerInstance::instance
	TouchManagerInstance_t2629118981 * ___instance_9;
	// TouchScript.Utils.ObjectPool`1<TouchScript.TouchPoint> TouchScript.TouchManagerInstance::touchPointPool
	ObjectPool_1_t312917930 * ___touchPointPool_25;
	// TouchScript.Utils.ObjectPool`1<System.Collections.Generic.List`1<TouchScript.TouchPoint>> TouchScript.TouchManagerInstance::touchPointListPool
	ObjectPool_1_t3977006358 * ___touchPointListPool_26;
	// TouchScript.Utils.ObjectPool`1<System.Collections.Generic.List`1<System.Int32>> TouchScript.TouchManagerInstance::intListPool
	ObjectPool_1_t794287427 * ___intListPool_27;
	// System.Predicate`1<TouchScript.Layers.TouchLayer> TouchScript.TouchManagerInstance::<>f__am$cache0
	Predicate_1_t1078410093 * ___U3CU3Ef__amU24cache0_30;

public:
	inline static int32_t get_offset_of_shuttingDown_8() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2629118981_StaticFields, ___shuttingDown_8)); }
	inline bool get_shuttingDown_8() const { return ___shuttingDown_8; }
	inline bool* get_address_of_shuttingDown_8() { return &___shuttingDown_8; }
	inline void set_shuttingDown_8(bool value)
	{
		___shuttingDown_8 = value;
	}

	inline static int32_t get_offset_of_instance_9() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2629118981_StaticFields, ___instance_9)); }
	inline TouchManagerInstance_t2629118981 * get_instance_9() const { return ___instance_9; }
	inline TouchManagerInstance_t2629118981 ** get_address_of_instance_9() { return &___instance_9; }
	inline void set_instance_9(TouchManagerInstance_t2629118981 * value)
	{
		___instance_9 = value;
		Il2CppCodeGenWriteBarrier((&___instance_9), value);
	}

	inline static int32_t get_offset_of_touchPointPool_25() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2629118981_StaticFields, ___touchPointPool_25)); }
	inline ObjectPool_1_t312917930 * get_touchPointPool_25() const { return ___touchPointPool_25; }
	inline ObjectPool_1_t312917930 ** get_address_of_touchPointPool_25() { return &___touchPointPool_25; }
	inline void set_touchPointPool_25(ObjectPool_1_t312917930 * value)
	{
		___touchPointPool_25 = value;
		Il2CppCodeGenWriteBarrier((&___touchPointPool_25), value);
	}

	inline static int32_t get_offset_of_touchPointListPool_26() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2629118981_StaticFields, ___touchPointListPool_26)); }
	inline ObjectPool_1_t3977006358 * get_touchPointListPool_26() const { return ___touchPointListPool_26; }
	inline ObjectPool_1_t3977006358 ** get_address_of_touchPointListPool_26() { return &___touchPointListPool_26; }
	inline void set_touchPointListPool_26(ObjectPool_1_t3977006358 * value)
	{
		___touchPointListPool_26 = value;
		Il2CppCodeGenWriteBarrier((&___touchPointListPool_26), value);
	}

	inline static int32_t get_offset_of_intListPool_27() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2629118981_StaticFields, ___intListPool_27)); }
	inline ObjectPool_1_t794287427 * get_intListPool_27() const { return ___intListPool_27; }
	inline ObjectPool_1_t794287427 ** get_address_of_intListPool_27() { return &___intListPool_27; }
	inline void set_intListPool_27(ObjectPool_1_t794287427 * value)
	{
		___intListPool_27 = value;
		Il2CppCodeGenWriteBarrier((&___intListPool_27), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_30() { return static_cast<int32_t>(offsetof(TouchManagerInstance_t2629118981_StaticFields, ___U3CU3Ef__amU24cache0_30)); }
	inline Predicate_1_t1078410093 * get_U3CU3Ef__amU24cache0_30() const { return ___U3CU3Ef__amU24cache0_30; }
	inline Predicate_1_t1078410093 ** get_address_of_U3CU3Ef__amU24cache0_30() { return &___U3CU3Ef__amU24cache0_30; }
	inline void set_U3CU3Ef__amU24cache0_30(Predicate_1_t1078410093 * value)
	{
		___U3CU3Ef__amU24cache0_30 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHMANAGERINSTANCE_T2629118981_H
#ifndef CAMERALAYERBASE_T3857323967_H
#define CAMERALAYERBASE_T3857323967_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Layers.CameraLayerBase
struct  CameraLayerBase_t3857323967  : public TouchLayer_t2635439978
{
public:
	// UnityEngine.LayerMask TouchScript.Layers.CameraLayerBase::layerMask
	LayerMask_t3188175821  ___layerMask_6;
	// UnityEngine.Camera TouchScript.Layers.CameraLayerBase::_camera
	Camera_t189460977 * ____camera_7;

public:
	inline static int32_t get_offset_of_layerMask_6() { return static_cast<int32_t>(offsetof(CameraLayerBase_t3857323967, ___layerMask_6)); }
	inline LayerMask_t3188175821  get_layerMask_6() const { return ___layerMask_6; }
	inline LayerMask_t3188175821 * get_address_of_layerMask_6() { return &___layerMask_6; }
	inline void set_layerMask_6(LayerMask_t3188175821  value)
	{
		___layerMask_6 = value;
	}

	inline static int32_t get_offset_of__camera_7() { return static_cast<int32_t>(offsetof(CameraLayerBase_t3857323967, ____camera_7)); }
	inline Camera_t189460977 * get__camera_7() const { return ____camera_7; }
	inline Camera_t189460977 ** get_address_of__camera_7() { return &____camera_7; }
	inline void set__camera_7(Camera_t189460977 * value)
	{
		____camera_7 = value;
		Il2CppCodeGenWriteBarrier((&____camera_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERALAYERBASE_T3857323967_H
#ifndef CAMERALAYER_T464507322_H
#define CAMERALAYER_T464507322_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Layers.CameraLayer
struct  CameraLayer_t464507322  : public CameraLayerBase_t3857323967
{
public:
	// System.Collections.Generic.List`1<UnityEngine.RaycastHit> TouchScript.Layers.CameraLayer::sortedHits
	List_1_t3751268748 * ___sortedHits_8;
	// UnityEngine.Transform TouchScript.Layers.CameraLayer::cachedTransform
	Transform_t3275118058 * ___cachedTransform_9;
	// System.Collections.Generic.List`1<TouchScript.Hit.HitTest> TouchScript.Layers.CameraLayer::tmpHitTestList
	List_1_t137760637 * ___tmpHitTestList_10;

public:
	inline static int32_t get_offset_of_sortedHits_8() { return static_cast<int32_t>(offsetof(CameraLayer_t464507322, ___sortedHits_8)); }
	inline List_1_t3751268748 * get_sortedHits_8() const { return ___sortedHits_8; }
	inline List_1_t3751268748 ** get_address_of_sortedHits_8() { return &___sortedHits_8; }
	inline void set_sortedHits_8(List_1_t3751268748 * value)
	{
		___sortedHits_8 = value;
		Il2CppCodeGenWriteBarrier((&___sortedHits_8), value);
	}

	inline static int32_t get_offset_of_cachedTransform_9() { return static_cast<int32_t>(offsetof(CameraLayer_t464507322, ___cachedTransform_9)); }
	inline Transform_t3275118058 * get_cachedTransform_9() const { return ___cachedTransform_9; }
	inline Transform_t3275118058 ** get_address_of_cachedTransform_9() { return &___cachedTransform_9; }
	inline void set_cachedTransform_9(Transform_t3275118058 * value)
	{
		___cachedTransform_9 = value;
		Il2CppCodeGenWriteBarrier((&___cachedTransform_9), value);
	}

	inline static int32_t get_offset_of_tmpHitTestList_10() { return static_cast<int32_t>(offsetof(CameraLayer_t464507322, ___tmpHitTestList_10)); }
	inline List_1_t137760637 * get_tmpHitTestList_10() const { return ___tmpHitTestList_10; }
	inline List_1_t137760637 ** get_address_of_tmpHitTestList_10() { return &___tmpHitTestList_10; }
	inline void set_tmpHitTestList_10(List_1_t137760637 * value)
	{
		___tmpHitTestList_10 = value;
		Il2CppCodeGenWriteBarrier((&___tmpHitTestList_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERALAYER_T464507322_H
#ifndef CAMERALAYER2D_T3366536156_H
#define CAMERALAYER2D_T3366536156_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Layers.CameraLayer2D
struct  CameraLayer2D_t3366536156  : public CameraLayerBase_t3857323967
{
public:
	// System.Int32[] TouchScript.Layers.CameraLayer2D::layerIds
	Int32U5BU5D_t3030399641* ___layerIds_8;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> TouchScript.Layers.CameraLayer2D::layerById
	Dictionary_2_t1079703083 * ___layerById_9;
	// System.Collections.Generic.List`1<UnityEngine.RaycastHit2D> TouchScript.Layers.CameraLayer2D::sortedHits
	List_1_t3433029906 * ___sortedHits_10;
	// System.Collections.Generic.List`1<TouchScript.Hit.HitTest> TouchScript.Layers.CameraLayer2D::tmpHitTestList
	List_1_t137760637 * ___tmpHitTestList_11;

public:
	inline static int32_t get_offset_of_layerIds_8() { return static_cast<int32_t>(offsetof(CameraLayer2D_t3366536156, ___layerIds_8)); }
	inline Int32U5BU5D_t3030399641* get_layerIds_8() const { return ___layerIds_8; }
	inline Int32U5BU5D_t3030399641** get_address_of_layerIds_8() { return &___layerIds_8; }
	inline void set_layerIds_8(Int32U5BU5D_t3030399641* value)
	{
		___layerIds_8 = value;
		Il2CppCodeGenWriteBarrier((&___layerIds_8), value);
	}

	inline static int32_t get_offset_of_layerById_9() { return static_cast<int32_t>(offsetof(CameraLayer2D_t3366536156, ___layerById_9)); }
	inline Dictionary_2_t1079703083 * get_layerById_9() const { return ___layerById_9; }
	inline Dictionary_2_t1079703083 ** get_address_of_layerById_9() { return &___layerById_9; }
	inline void set_layerById_9(Dictionary_2_t1079703083 * value)
	{
		___layerById_9 = value;
		Il2CppCodeGenWriteBarrier((&___layerById_9), value);
	}

	inline static int32_t get_offset_of_sortedHits_10() { return static_cast<int32_t>(offsetof(CameraLayer2D_t3366536156, ___sortedHits_10)); }
	inline List_1_t3433029906 * get_sortedHits_10() const { return ___sortedHits_10; }
	inline List_1_t3433029906 ** get_address_of_sortedHits_10() { return &___sortedHits_10; }
	inline void set_sortedHits_10(List_1_t3433029906 * value)
	{
		___sortedHits_10 = value;
		Il2CppCodeGenWriteBarrier((&___sortedHits_10), value);
	}

	inline static int32_t get_offset_of_tmpHitTestList_11() { return static_cast<int32_t>(offsetof(CameraLayer2D_t3366536156, ___tmpHitTestList_11)); }
	inline List_1_t137760637 * get_tmpHitTestList_11() const { return ___tmpHitTestList_11; }
	inline List_1_t137760637 ** get_address_of_tmpHitTestList_11() { return &___tmpHitTestList_11; }
	inline void set_tmpHitTestList_11(List_1_t137760637 * value)
	{
		___tmpHitTestList_11 = value;
		Il2CppCodeGenWriteBarrier((&___tmpHitTestList_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERALAYER2D_T3366536156_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3200 = { sizeof (MouseInput_t301708067), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3200[3] = 
{
	MouseInput_t301708067::get_offset_of_DisableOnMobilePlatforms_5(),
	MouseInput_t301708067::get_offset_of_Tags_6(),
	MouseInput_t301708067::get_offset_of_mouseHandler_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3201 = { sizeof (StandardInput_t4102879489), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3201[12] = 
{
	StandardInput_t4102879489::get_offset_of_TouchTags_5(),
	StandardInput_t4102879489::get_offset_of_MouseTags_6(),
	StandardInput_t4102879489::get_offset_of_PenTags_7(),
	StandardInput_t4102879489::get_offset_of_Windows8Touch_8(),
	StandardInput_t4102879489::get_offset_of_Windows7Touch_9(),
	StandardInput_t4102879489::get_offset_of_WebPlayerTouch_10(),
	StandardInput_t4102879489::get_offset_of_WebGLTouch_11(),
	StandardInput_t4102879489::get_offset_of_Windows8Mouse_12(),
	StandardInput_t4102879489::get_offset_of_Windows7Mouse_13(),
	StandardInput_t4102879489::get_offset_of_UniversalWindowsMouse_14(),
	StandardInput_t4102879489::get_offset_of_mouseHandler_15(),
	StandardInput_t4102879489::get_offset_of_touchHandler_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3202 = { sizeof (Windows8TouchAPIType_t4008807668)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3202[5] = 
{
	Windows8TouchAPIType_t4008807668::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3203 = { sizeof (Windows7TouchAPIType_t3091725811)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3203[4] = 
{
	Windows7TouchAPIType_t3091725811::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3204 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3205 = { sizeof (TouchEventArgs_t1917927166), -1, sizeof(TouchEventArgs_t1917927166_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3205[2] = 
{
	TouchEventArgs_t1917927166::get_offset_of_U3CTouchesU3Ek__BackingField_1(),
	TouchEventArgs_t1917927166_StaticFields::get_offset_of_instance_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3206 = { sizeof (CameraLayer_t464507322), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3206[3] = 
{
	CameraLayer_t464507322::get_offset_of_sortedHits_8(),
	CameraLayer_t464507322::get_offset_of_cachedTransform_9(),
	CameraLayer_t464507322::get_offset_of_tmpHitTestList_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3207 = { sizeof (U3CsortHitsU3Ec__AnonStorey0_t2253667628), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3207[1] = 
{
	U3CsortHitsU3Ec__AnonStorey0_t2253667628::get_offset_of_cameraPos_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3208 = { sizeof (CameraLayer2D_t3366536156), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3208[4] = 
{
	CameraLayer2D_t3366536156::get_offset_of_layerIds_8(),
	CameraLayer2D_t3366536156::get_offset_of_layerById_9(),
	CameraLayer2D_t3366536156::get_offset_of_sortedHits_10(),
	CameraLayer2D_t3366536156::get_offset_of_tmpHitTestList_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3209 = { sizeof (CameraLayerBase_t3857323967), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3209[2] = 
{
	CameraLayerBase_t3857323967::get_offset_of_layerMask_6(),
	CameraLayerBase_t3857323967::get_offset_of__camera_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3210 = { sizeof (FullscreenLayer_t1925731828), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3210[4] = 
{
	FullscreenLayer_t1925731828::get_offset_of_type_6(),
	FullscreenLayer_t1925731828::get_offset_of__camera_7(),
	FullscreenLayer_t1925731828::get_offset_of_cameraTransform_8(),
	FullscreenLayer_t1925731828::get_offset_of_tmpHitTestList_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3211 = { sizeof (LayerType_t777199562)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3211[4] = 
{
	LayerType_t777199562::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3212 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3213 = { sizeof (ProjectionParams_t2712959773), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3214 = { sizeof (CameraProjectionParams_t285425166), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3214[1] = 
{
	CameraProjectionParams_t285425166::get_offset_of_camera_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3215 = { sizeof (CanvasProjectionParams_t880996221), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3215[4] = 
{
	CanvasProjectionParams_t880996221::get_offset_of_canvas_0(),
	CanvasProjectionParams_t880996221::get_offset_of_rect_1(),
	CanvasProjectionParams_t880996221::get_offset_of_mode_2(),
	CanvasProjectionParams_t880996221::get_offset_of_camera_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3216 = { sizeof (TouchLayer_t2635439978), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3216[4] = 
{
	TouchLayer_t2635439978::get_offset_of_touchBeganInvoker_2(),
	TouchLayer_t2635439978::get_offset_of_Name_3(),
	TouchLayer_t2635439978::get_offset_of_U3CDelegateU3Ek__BackingField_4(),
	TouchLayer_t2635439978::get_offset_of_layerProjectionParams_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3217 = { sizeof (LayerHitResult_t1590288664)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3217[4] = 
{
	LayerHitResult_t1590288664::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3218 = { sizeof (U3ClateAwakeU3Ec__Iterator0_t3289977563), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3218[4] = 
{
	U3ClateAwakeU3Ec__Iterator0_t3289977563::get_offset_of_U24this_0(),
	U3ClateAwakeU3Ec__Iterator0_t3289977563::get_offset_of_U24current_1(),
	U3ClateAwakeU3Ec__Iterator0_t3289977563::get_offset_of_U24disposing_2(),
	U3ClateAwakeU3Ec__Iterator0_t3289977563::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3219 = { sizeof (TouchLayerEventArgs_t1247401065), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3219[1] = 
{
	TouchLayerEventArgs_t1247401065::get_offset_of_U3CTouchU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3220 = { sizeof (UILayer_t314035379), -1, sizeof(UILayer_t314035379_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3220[7] = 
{
	UILayer_t314035379_StaticFields::get_offset_of_instance_6(),
	UILayer_t314035379::get_offset_of_layerMask_7(),
	UILayer_t314035379::get_offset_of_raycastResultCache_8(),
	UILayer_t314035379::get_offset_of_tmpHitTestList_9(),
	UILayer_t314035379::get_offset_of_pointerDataCache_10(),
	UILayer_t314035379::get_offset_of_eventSystem_11(),
	UILayer_t314035379::get_offset_of_projectionParamsCache_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3221 = { sizeof (U3ClateAwakeU3Ec__Iterator0_t181594678), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3221[4] = 
{
	U3ClateAwakeU3Ec__Iterator0_t181594678::get_offset_of_U24this_0(),
	U3ClateAwakeU3Ec__Iterator0_t181594678::get_offset_of_U24current_1(),
	U3ClateAwakeU3Ec__Iterator0_t181594678::get_offset_of_U24disposing_2(),
	U3ClateAwakeU3Ec__Iterator0_t181594678::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3222 = { sizeof (Tags_t1265380163), -1, sizeof(Tags_t1265380163_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3222[8] = 
{
	0,
	0,
	0,
	0,
	Tags_t1265380163_StaticFields::get_offset_of_EMPTY_4(),
	Tags_t1265380163::get_offset_of_tagList_5(),
	Tags_t1265380163::get_offset_of_tags_6(),
	Tags_t1265380163::get_offset_of_stringValue_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3223 = { sizeof (TouchManager_t3980263048), -1, sizeof(TouchManager_t3980263048_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3223[11] = 
{
	0,
	0,
	TouchManager_t3980263048_StaticFields::get_offset_of_INVALID_POSITION_4(),
	TouchManager_t3980263048_StaticFields::get_offset_of_VERSION_5(),
	TouchManager_t3980263048::get_offset_of_displayDevice_6(),
	TouchManager_t3980263048::get_offset_of_shouldCreateCameraLayer_7(),
	TouchManager_t3980263048::get_offset_of_shouldCreateStandardInput_8(),
	TouchManager_t3980263048::get_offset_of_useSendMessage_9(),
	TouchManager_t3980263048::get_offset_of_sendMessageEvents_10(),
	TouchManager_t3980263048::get_offset_of_sendMessageTarget_11(),
	TouchManager_t3980263048::get_offset_of_layers_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3224 = { sizeof (MessageType_t1628650752)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3224[7] = 
{
	MessageType_t1628650752::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3225 = { sizeof (MessageName_t2757950695)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3225[7] = 
{
	MessageName_t2757950695::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3226 = { sizeof (TouchManagerInstance_t2629118981), -1, sizeof(TouchManagerInstance_t2629118981_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3226[29] = 
{
	TouchManagerInstance_t2629118981::get_offset_of_touchesBeganInvoker_2(),
	TouchManagerInstance_t2629118981::get_offset_of_touchesMovedInvoker_3(),
	TouchManagerInstance_t2629118981::get_offset_of_touchesEndedInvoker_4(),
	TouchManagerInstance_t2629118981::get_offset_of_touchesCancelledInvoker_5(),
	TouchManagerInstance_t2629118981::get_offset_of_frameStartedInvoker_6(),
	TouchManagerInstance_t2629118981::get_offset_of_frameFinishedInvoker_7(),
	TouchManagerInstance_t2629118981_StaticFields::get_offset_of_shuttingDown_8(),
	TouchManagerInstance_t2629118981_StaticFields::get_offset_of_instance_9(),
	TouchManagerInstance_t2629118981::get_offset_of_shouldCreateCameraLayer_10(),
	TouchManagerInstance_t2629118981::get_offset_of_shouldCreateStandardInput_11(),
	TouchManagerInstance_t2629118981::get_offset_of_displayDevice_12(),
	TouchManagerInstance_t2629118981::get_offset_of_dpi_13(),
	TouchManagerInstance_t2629118981::get_offset_of_dotsPerCentimeter_14(),
	TouchManagerInstance_t2629118981::get_offset_of_layers_15(),
	TouchManagerInstance_t2629118981::get_offset_of_layerCount_16(),
	TouchManagerInstance_t2629118981::get_offset_of_inputs_17(),
	TouchManagerInstance_t2629118981::get_offset_of_inputCount_18(),
	TouchManagerInstance_t2629118981::get_offset_of_touches_19(),
	TouchManagerInstance_t2629118981::get_offset_of_idToTouch_20(),
	TouchManagerInstance_t2629118981::get_offset_of_touchesBegan_21(),
	TouchManagerInstance_t2629118981::get_offset_of_touchesUpdated_22(),
	TouchManagerInstance_t2629118981::get_offset_of_touchesEnded_23(),
	TouchManagerInstance_t2629118981::get_offset_of_touchesCancelled_24(),
	TouchManagerInstance_t2629118981_StaticFields::get_offset_of_touchPointPool_25(),
	TouchManagerInstance_t2629118981_StaticFields::get_offset_of_touchPointListPool_26(),
	TouchManagerInstance_t2629118981_StaticFields::get_offset_of_intListPool_27(),
	TouchManagerInstance_t2629118981::get_offset_of_nextTouchId_28(),
	TouchManagerInstance_t2629118981::get_offset_of_touchLock_29(),
	TouchManagerInstance_t2629118981_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3227 = { sizeof (U3CINTERNAL_MoveTouchU3Ec__AnonStorey1_t1781908992), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3227[1] = 
{
	U3CINTERNAL_MoveTouchU3Ec__AnonStorey1_t1781908992::get_offset_of_id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3228 = { sizeof (U3CINTERNAL_EndTouchU3Ec__AnonStorey2_t1111470859), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3228[1] = 
{
	U3CINTERNAL_EndTouchU3Ec__AnonStorey2_t1111470859::get_offset_of_id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3229 = { sizeof (U3CINTERNAL_CancelTouchU3Ec__AnonStorey3_t4047882747), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3229[1] = 
{
	U3CINTERNAL_CancelTouchU3Ec__AnonStorey3_t4047882747::get_offset_of_id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3230 = { sizeof (U3ClateAwakeU3Ec__Iterator0_t316551236), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3230[4] = 
{
	U3ClateAwakeU3Ec__Iterator0_t316551236::get_offset_of_U24this_0(),
	U3ClateAwakeU3Ec__Iterator0_t316551236::get_offset_of_U24current_1(),
	U3ClateAwakeU3Ec__Iterator0_t316551236::get_offset_of_U24disposing_2(),
	U3ClateAwakeU3Ec__Iterator0_t316551236::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3231 = { sizeof (TouchPoint_t959629083), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3231[11] = 
{
	TouchPoint_t959629083::get_offset_of_U3CIdU3Ek__BackingField_0(),
	TouchPoint_t959629083::get_offset_of_U3CTargetU3Ek__BackingField_1(),
	TouchPoint_t959629083::get_offset_of_U3CPreviousPositionU3Ek__BackingField_2(),
	TouchPoint_t959629083::get_offset_of_U3CHitU3Ek__BackingField_3(),
	TouchPoint_t959629083::get_offset_of_U3CLayerU3Ek__BackingField_4(),
	TouchPoint_t959629083::get_offset_of_U3CInputSourceU3Ek__BackingField_5(),
	TouchPoint_t959629083::get_offset_of_U3CTagsU3Ek__BackingField_6(),
	TouchPoint_t959629083::get_offset_of_refCount_7(),
	TouchPoint_t959629083::get_offset_of_position_8(),
	TouchPoint_t959629083::get_offset_of_newPosition_9(),
	TouchPoint_t959629083::get_offset_of_properties_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3232 = { sizeof (NullToggleAttribute_t2456566677), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3232[3] = 
{
	NullToggleAttribute_t2456566677::get_offset_of_NullIntValue_0(),
	NullToggleAttribute_t2456566677::get_offset_of_NullFloatValue_1(),
	NullToggleAttribute_t2456566677::get_offset_of_NullObjectValue_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3233 = { sizeof (ToggleLeftAttribute_t3639526933), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3234 = { sizeof (ClusterUtils_t953614479), -1, sizeof(ClusterUtils_t953614479_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3234[1] = 
{
	ClusterUtils_t953614479_StaticFields::get_offset_of_hashString_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3235 = { sizeof (EventHandlerExtensions_t2256621884), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3236 = { sizeof (TwoD_t1213408342), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3237 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3237[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3238 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3239 = { sizeof (ProjectionUtils_t1537418392), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3240 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3240[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3241 = { sizeof (TouchUtils_t3521695436), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3242 = { sizeof (TransformUtils_t390635157), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3243 = { sizeof (ByteOrder_t469806806)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3243[3] = 
{
	ByteOrder_t469806806::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3244 = { sizeof (CloseEventArgs_t344507773), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3244[4] = 
{
	CloseEventArgs_t344507773::get_offset_of__clean_1(),
	CloseEventArgs_t344507773::get_offset_of__code_2(),
	CloseEventArgs_t344507773::get_offset_of__payloadData_3(),
	CloseEventArgs_t344507773::get_offset_of__reason_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3245 = { sizeof (CloseStatusCode_t2945181741)+ sizeof (RuntimeObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3245[14] = 
{
	CloseStatusCode_t2945181741::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3246 = { sizeof (CompressionMethod_t4066553457)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3246[3] = 
{
	CompressionMethod_t4066553457::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3247 = { sizeof (ErrorEventArgs_t502222999), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3247[2] = 
{
	ErrorEventArgs_t502222999::get_offset_of__exception_1(),
	ErrorEventArgs_t502222999::get_offset_of__message_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3248 = { sizeof (Ext_t870230697), -1, sizeof(Ext_t870230697_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3248[3] = 
{
	Ext_t870230697_StaticFields::get_offset_of__last_0(),
	0,
	Ext_t870230697_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3249 = { sizeof (U3CContainsTwiceU3Ec__AnonStorey1_t2127424492), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3249[3] = 
{
	U3CContainsTwiceU3Ec__AnonStorey1_t2127424492::get_offset_of_len_0(),
	U3CContainsTwiceU3Ec__AnonStorey1_t2127424492::get_offset_of_values_1(),
	U3CContainsTwiceU3Ec__AnonStorey1_t2127424492::get_offset_of_contains_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3250 = { sizeof (U3CCopyToAsyncU3Ec__AnonStorey2_t620221128), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3250[7] = 
{
	U3CCopyToAsyncU3Ec__AnonStorey2_t620221128::get_offset_of_source_0(),
	U3CCopyToAsyncU3Ec__AnonStorey2_t620221128::get_offset_of_completed_1(),
	U3CCopyToAsyncU3Ec__AnonStorey2_t620221128::get_offset_of_destination_2(),
	U3CCopyToAsyncU3Ec__AnonStorey2_t620221128::get_offset_of_buff_3(),
	U3CCopyToAsyncU3Ec__AnonStorey2_t620221128::get_offset_of_bufferLength_4(),
	U3CCopyToAsyncU3Ec__AnonStorey2_t620221128::get_offset_of_callback_5(),
	U3CCopyToAsyncU3Ec__AnonStorey2_t620221128::get_offset_of_error_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3251 = { sizeof (U3CReadBytesAsyncU3Ec__AnonStorey3_t1697116946), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3251[7] = 
{
	U3CReadBytesAsyncU3Ec__AnonStorey3_t1697116946::get_offset_of_stream_0(),
	U3CReadBytesAsyncU3Ec__AnonStorey3_t1697116946::get_offset_of_length_1(),
	U3CReadBytesAsyncU3Ec__AnonStorey3_t1697116946::get_offset_of_completed_2(),
	U3CReadBytesAsyncU3Ec__AnonStorey3_t1697116946::get_offset_of_buff_3(),
	U3CReadBytesAsyncU3Ec__AnonStorey3_t1697116946::get_offset_of_offset_4(),
	U3CReadBytesAsyncU3Ec__AnonStorey3_t1697116946::get_offset_of_callback_5(),
	U3CReadBytesAsyncU3Ec__AnonStorey3_t1697116946::get_offset_of_error_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3252 = { sizeof (U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3252[7] = 
{
	U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943::get_offset_of_bufferLength_0(),
	U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943::get_offset_of_stream_1(),
	U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943::get_offset_of_buff_2(),
	U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943::get_offset_of_dest_3(),
	U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943::get_offset_of_completed_4(),
	U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943::get_offset_of_read_5(),
	U3CReadBytesAsyncU3Ec__AnonStorey4_t1697116943::get_offset_of_error_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3253 = { sizeof (U3CReadBytesAsyncU3Ec__AnonStorey5_t4262448565), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3253[2] = 
{
	U3CReadBytesAsyncU3Ec__AnonStorey5_t4262448565::get_offset_of_len_0(),
	U3CReadBytesAsyncU3Ec__AnonStorey5_t4262448565::get_offset_of_U3CU3Ef__refU244_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3254 = { sizeof (U3CSplitHeaderValueU3Ec__Iterator0_t2473694690), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3254[12] = 
{
	U3CSplitHeaderValueU3Ec__Iterator0_t2473694690::get_offset_of_value_0(),
	U3CSplitHeaderValueU3Ec__Iterator0_t2473694690::get_offset_of_U3ClenU3E__0_1(),
	U3CSplitHeaderValueU3Ec__Iterator0_t2473694690::get_offset_of_separators_2(),
	U3CSplitHeaderValueU3Ec__Iterator0_t2473694690::get_offset_of_U3CsepsU3E__0_3(),
	U3CSplitHeaderValueU3Ec__Iterator0_t2473694690::get_offset_of_U3CbuffU3E__0_4(),
	U3CSplitHeaderValueU3Ec__Iterator0_t2473694690::get_offset_of_U3CescapedU3E__0_5(),
	U3CSplitHeaderValueU3Ec__Iterator0_t2473694690::get_offset_of_U3CquotedU3E__0_6(),
	U3CSplitHeaderValueU3Ec__Iterator0_t2473694690::get_offset_of_U3CiU3E__1_7(),
	U3CSplitHeaderValueU3Ec__Iterator0_t2473694690::get_offset_of_U3CcU3E__2_8(),
	U3CSplitHeaderValueU3Ec__Iterator0_t2473694690::get_offset_of_U24current_9(),
	U3CSplitHeaderValueU3Ec__Iterator0_t2473694690::get_offset_of_U24disposing_10(),
	U3CSplitHeaderValueU3Ec__Iterator0_t2473694690::get_offset_of_U24PC_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3255 = { sizeof (U3CWriteBytesAsyncU3Ec__AnonStorey6_t896389242), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3255[3] = 
{
	U3CWriteBytesAsyncU3Ec__AnonStorey6_t896389242::get_offset_of_completed_0(),
	U3CWriteBytesAsyncU3Ec__AnonStorey6_t896389242::get_offset_of_input_1(),
	U3CWriteBytesAsyncU3Ec__AnonStorey6_t896389242::get_offset_of_error_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3256 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3256[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3257 = { sizeof (Fin_t2752139063)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3257[3] = 
{
	Fin_t2752139063::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3258 = { sizeof (HttpBase_t4283398485), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3258[5] = 
{
	HttpBase_t4283398485::get_offset_of__headers_0(),
	0,
	HttpBase_t4283398485::get_offset_of__version_2(),
	HttpBase_t4283398485::get_offset_of_EntityBodyData_3(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3259 = { sizeof (U3CreadHeadersU3Ec__AnonStorey0_t2579283176), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3259[2] = 
{
	U3CreadHeadersU3Ec__AnonStorey0_t2579283176::get_offset_of_buff_0(),
	U3CreadHeadersU3Ec__AnonStorey0_t2579283176::get_offset_of_cnt_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3260 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3260[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3261 = { sizeof (HttpRequest_t1845443631), -1, sizeof(HttpRequest_t1845443631_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3261[6] = 
{
	HttpRequest_t1845443631::get_offset_of__method_5(),
	HttpRequest_t1845443631::get_offset_of__uri_6(),
	HttpRequest_t1845443631::get_offset_of__websocketRequest_7(),
	HttpRequest_t1845443631::get_offset_of__websocketRequestSet_8(),
	HttpRequest_t1845443631_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_9(),
	HttpRequest_t1845443631_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3262 = { sizeof (HttpResponse_t2820540315), -1, sizeof(HttpResponse_t2820540315_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3262[3] = 
{
	HttpResponse_t2820540315::get_offset_of__code_5(),
	HttpResponse_t2820540315::get_offset_of__reason_6(),
	HttpResponse_t2820540315_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3263 = { sizeof (LogData_t4095822710), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3263[4] = 
{
	LogData_t4095822710::get_offset_of__caller_0(),
	LogData_t4095822710::get_offset_of__date_1(),
	LogData_t4095822710::get_offset_of__level_2(),
	LogData_t4095822710::get_offset_of__message_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3264 = { sizeof (Logger_t2598199114), -1, sizeof(Logger_t2598199114_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3264[6] = 
{
	Logger_t2598199114::get_offset_of__file_0(),
	Logger_t2598199114::get_offset_of__level_1(),
	Logger_t2598199114::get_offset_of__output_2(),
	Logger_t2598199114::get_offset_of__sync_3(),
	Logger_t2598199114_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_4(),
	Logger_t2598199114_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3265 = { sizeof (LogLevel_t2748531832)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3265[7] = 
{
	LogLevel_t2748531832::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3266 = { sizeof (Mask_t1111889066)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3266[3] = 
{
	Mask_t1111889066::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3267 = { sizeof (MessageEventArgs_t2890051726), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3267[4] = 
{
	MessageEventArgs_t2890051726::get_offset_of__data_1(),
	MessageEventArgs_t2890051726::get_offset_of__dataSet_2(),
	MessageEventArgs_t2890051726::get_offset_of__opcode_3(),
	MessageEventArgs_t2890051726::get_offset_of__rawData_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3268 = { sizeof (AuthenticationBase_t909684845), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3268[2] = 
{
	AuthenticationBase_t909684845::get_offset_of__scheme_0(),
	AuthenticationBase_t909684845::get_offset_of_Parameters_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3269 = { sizeof (AuthenticationChallenge_t1146723439), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3270 = { sizeof (AuthenticationResponse_t1212723231), -1, sizeof(AuthenticationResponse_t1212723231_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3270[2] = 
{
	AuthenticationResponse_t1212723231::get_offset_of__nonceCount_2(),
	AuthenticationResponse_t1212723231_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3271 = { sizeof (AuthenticationSchemes_t29593226)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3271[5] = 
{
	AuthenticationSchemes_t29593226::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3272 = { sizeof (Chunk_t2303927151), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3272[2] = 
{
	Chunk_t2303927151::get_offset_of__data_0(),
	Chunk_t2303927151::get_offset_of__offset_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3273 = { sizeof (ChunkedRequestStream_t3311534059), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3273[5] = 
{
	0,
	ChunkedRequestStream_t3311534059::get_offset_of__context_8(),
	ChunkedRequestStream_t3311534059::get_offset_of__decoder_9(),
	ChunkedRequestStream_t3311534059::get_offset_of__disposed_10(),
	ChunkedRequestStream_t3311534059::get_offset_of__noMoreData_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3274 = { sizeof (ChunkStream_t2067859643), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3274[9] = 
{
	ChunkStream_t2067859643::get_offset_of__chunkRead_0(),
	ChunkStream_t2067859643::get_offset_of__chunkSize_1(),
	ChunkStream_t2067859643::get_offset_of__chunks_2(),
	ChunkStream_t2067859643::get_offset_of__gotIt_3(),
	ChunkStream_t2067859643::get_offset_of__headers_4(),
	ChunkStream_t2067859643::get_offset_of__saved_5(),
	ChunkStream_t2067859643::get_offset_of__sawCr_6(),
	ChunkStream_t2067859643::get_offset_of__state_7(),
	ChunkStream_t2067859643::get_offset_of__trailerState_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3275 = { sizeof (ClientSslConfiguration_t1159130081), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3275[2] = 
{
	ClientSslConfiguration_t1159130081::get_offset_of__certs_6(),
	ClientSslConfiguration_t1159130081::get_offset_of__host_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3276 = { sizeof (Cookie_t1826188460), -1, sizeof(Cookie_t1826188460_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3276[17] = 
{
	Cookie_t1826188460::get_offset_of__comment_0(),
	Cookie_t1826188460::get_offset_of__commentUri_1(),
	Cookie_t1826188460::get_offset_of__discard_2(),
	Cookie_t1826188460::get_offset_of__domain_3(),
	Cookie_t1826188460::get_offset_of__expires_4(),
	Cookie_t1826188460::get_offset_of__httpOnly_5(),
	Cookie_t1826188460::get_offset_of__name_6(),
	Cookie_t1826188460::get_offset_of__path_7(),
	Cookie_t1826188460::get_offset_of__port_8(),
	Cookie_t1826188460::get_offset_of__ports_9(),
	Cookie_t1826188460_StaticFields::get_offset_of__reservedCharsForName_10(),
	Cookie_t1826188460_StaticFields::get_offset_of__reservedCharsForValue_11(),
	Cookie_t1826188460::get_offset_of__secure_12(),
	Cookie_t1826188460::get_offset_of__timestamp_13(),
	Cookie_t1826188460::get_offset_of__value_14(),
	Cookie_t1826188460::get_offset_of__version_15(),
	Cookie_t1826188460::get_offset_of_U3CExactDomainU3Ek__BackingField_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3277 = { sizeof (CookieCollection_t4248997468), -1, sizeof(CookieCollection_t4248997468_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3277[4] = 
{
	CookieCollection_t4248997468::get_offset_of__list_0(),
	CookieCollection_t4248997468::get_offset_of__sync_1(),
	CookieCollection_t4248997468_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_2(),
	CookieCollection_t4248997468_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3278 = { sizeof (CookieException_t780982235), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3279 = { sizeof (EndPointListener_t3937551933), -1, sizeof(EndPointListener_t3937551933_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3279[12] = 
{
	EndPointListener_t3937551933::get_offset_of__all_0(),
	EndPointListener_t3937551933_StaticFields::get_offset_of__defaultCertFolderPath_1(),
	EndPointListener_t3937551933::get_offset_of__endpoint_2(),
	EndPointListener_t3937551933::get_offset_of__prefixes_3(),
	EndPointListener_t3937551933::get_offset_of__secure_4(),
	EndPointListener_t3937551933::get_offset_of__socket_5(),
	EndPointListener_t3937551933::get_offset_of__sslConfig_6(),
	EndPointListener_t3937551933::get_offset_of__unhandled_7(),
	EndPointListener_t3937551933::get_offset_of__unregistered_8(),
	EndPointListener_t3937551933::get_offset_of__unregisteredSync_9(),
	EndPointListener_t3937551933_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_10(),
	EndPointListener_t3937551933_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3280 = { sizeof (EndPointManager_t2259888518), -1, sizeof(EndPointManager_t2259888518_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3280[1] = 
{
	EndPointManager_t2259888518_StaticFields::get_offset_of__addressToEndpoints_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3281 = { sizeof (HttpBasicIdentity_t296759650), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3281[1] = 
{
	HttpBasicIdentity_t296759650::get_offset_of__password_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3282 = { sizeof (HttpConnection_t2649486862), -1, sizeof(HttpConnection_t2649486862_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3282[24] = 
{
	HttpConnection_t2649486862::get_offset_of__buffer_0(),
	0,
	HttpConnection_t2649486862::get_offset_of__context_2(),
	HttpConnection_t2649486862::get_offset_of__contextRegistered_3(),
	HttpConnection_t2649486862::get_offset_of__currentLine_4(),
	HttpConnection_t2649486862::get_offset_of__inputState_5(),
	HttpConnection_t2649486862::get_offset_of__inputStream_6(),
	HttpConnection_t2649486862::get_offset_of__lastListener_7(),
	HttpConnection_t2649486862::get_offset_of__lineState_8(),
	HttpConnection_t2649486862::get_offset_of__listener_9(),
	HttpConnection_t2649486862::get_offset_of__outputStream_10(),
	HttpConnection_t2649486862::get_offset_of__position_11(),
	HttpConnection_t2649486862::get_offset_of__prefix_12(),
	HttpConnection_t2649486862::get_offset_of__requestBuffer_13(),
	HttpConnection_t2649486862::get_offset_of__reuses_14(),
	HttpConnection_t2649486862::get_offset_of__secure_15(),
	HttpConnection_t2649486862::get_offset_of__socket_16(),
	HttpConnection_t2649486862::get_offset_of__stream_17(),
	HttpConnection_t2649486862::get_offset_of__sync_18(),
	HttpConnection_t2649486862::get_offset_of__timeout_19(),
	HttpConnection_t2649486862::get_offset_of__timer_20(),
	HttpConnection_t2649486862_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_21(),
	HttpConnection_t2649486862_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_22(),
	HttpConnection_t2649486862_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3283 = { sizeof (HttpDigestIdentity_t3711678560), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3283[1] = 
{
	HttpDigestIdentity_t3711678560::get_offset_of__parameters_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3284 = { sizeof (HttpHeaderInfo_t2096319561), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3284[2] = 
{
	HttpHeaderInfo_t2096319561::get_offset_of__name_0(),
	HttpHeaderInfo_t2096319561::get_offset_of__type_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3285 = { sizeof (HttpHeaderType_t1518115223)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3285[8] = 
{
	HttpHeaderType_t1518115223::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3286 = { sizeof (HttpListener_t4179429670), -1, sizeof(HttpListener_t4179429670_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3286[21] = 
{
	HttpListener_t4179429670::get_offset_of__authSchemes_0(),
	HttpListener_t4179429670::get_offset_of__authSchemeSelector_1(),
	HttpListener_t4179429670::get_offset_of__certFolderPath_2(),
	HttpListener_t4179429670::get_offset_of__connections_3(),
	HttpListener_t4179429670::get_offset_of__connectionsSync_4(),
	HttpListener_t4179429670::get_offset_of__ctxQueue_5(),
	HttpListener_t4179429670::get_offset_of__ctxQueueSync_6(),
	HttpListener_t4179429670::get_offset_of__ctxRegistry_7(),
	HttpListener_t4179429670::get_offset_of__ctxRegistrySync_8(),
	HttpListener_t4179429670_StaticFields::get_offset_of__defaultRealm_9(),
	HttpListener_t4179429670::get_offset_of__disposed_10(),
	HttpListener_t4179429670::get_offset_of__ignoreWriteExceptions_11(),
	HttpListener_t4179429670::get_offset_of__listening_12(),
	HttpListener_t4179429670::get_offset_of__logger_13(),
	HttpListener_t4179429670::get_offset_of__prefixes_14(),
	HttpListener_t4179429670::get_offset_of__realm_15(),
	HttpListener_t4179429670::get_offset_of__reuseAddress_16(),
	HttpListener_t4179429670::get_offset_of__sslConfig_17(),
	HttpListener_t4179429670::get_offset_of__userCredFinder_18(),
	HttpListener_t4179429670::get_offset_of__waitQueue_19(),
	HttpListener_t4179429670::get_offset_of__waitQueueSync_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3287 = { sizeof (HttpListenerAsyncResult_t3506939685), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3287[10] = 
{
	HttpListenerAsyncResult_t3506939685::get_offset_of__callback_0(),
	HttpListenerAsyncResult_t3506939685::get_offset_of__completed_1(),
	HttpListenerAsyncResult_t3506939685::get_offset_of__context_2(),
	HttpListenerAsyncResult_t3506939685::get_offset_of__endCalled_3(),
	HttpListenerAsyncResult_t3506939685::get_offset_of__exception_4(),
	HttpListenerAsyncResult_t3506939685::get_offset_of__inGet_5(),
	HttpListenerAsyncResult_t3506939685::get_offset_of__state_6(),
	HttpListenerAsyncResult_t3506939685::get_offset_of__sync_7(),
	HttpListenerAsyncResult_t3506939685::get_offset_of__syncCompleted_8(),
	HttpListenerAsyncResult_t3506939685::get_offset_of__waitHandle_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3288 = { sizeof (U3CcompleteU3Ec__AnonStorey0_t2877245460), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3288[2] = 
{
	U3CcompleteU3Ec__AnonStorey0_t2877245460::get_offset_of_callback_0(),
	U3CcompleteU3Ec__AnonStorey0_t2877245460::get_offset_of_asyncResult_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3289 = { sizeof (HttpListenerContext_t994708409), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3289[8] = 
{
	HttpListenerContext_t994708409::get_offset_of__connection_0(),
	HttpListenerContext_t994708409::get_offset_of__error_1(),
	HttpListenerContext_t994708409::get_offset_of__errorStatus_2(),
	HttpListenerContext_t994708409::get_offset_of__listener_3(),
	HttpListenerContext_t994708409::get_offset_of__request_4(),
	HttpListenerContext_t994708409::get_offset_of__response_5(),
	HttpListenerContext_t994708409::get_offset_of__user_6(),
	HttpListenerContext_t994708409::get_offset_of__websocketContext_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3290 = { sizeof (HttpListenerException_t543138973), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3291 = { sizeof (HttpListenerPrefix_t529778486), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3291[7] = 
{
	HttpListenerPrefix_t529778486::get_offset_of__addresses_0(),
	HttpListenerPrefix_t529778486::get_offset_of__host_1(),
	HttpListenerPrefix_t529778486::get_offset_of__listener_2(),
	HttpListenerPrefix_t529778486::get_offset_of__original_3(),
	HttpListenerPrefix_t529778486::get_offset_of__path_4(),
	HttpListenerPrefix_t529778486::get_offset_of__port_5(),
	HttpListenerPrefix_t529778486::get_offset_of__secure_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3292 = { sizeof (HttpListenerPrefixCollection_t3269177542), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3292[2] = 
{
	HttpListenerPrefixCollection_t3269177542::get_offset_of__listener_0(),
	HttpListenerPrefixCollection_t3269177542::get_offset_of__prefixes_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3293 = { sizeof (HttpListenerRequest_t2316381291), -1, sizeof(HttpListenerRequest_t2316381291_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3293[22] = 
{
	HttpListenerRequest_t2316381291_StaticFields::get_offset_of__100continue_0(),
	HttpListenerRequest_t2316381291::get_offset_of__acceptTypes_1(),
	HttpListenerRequest_t2316381291::get_offset_of__chunked_2(),
	HttpListenerRequest_t2316381291::get_offset_of__contentEncoding_3(),
	HttpListenerRequest_t2316381291::get_offset_of__contentLength_4(),
	HttpListenerRequest_t2316381291::get_offset_of__contentLengthSet_5(),
	HttpListenerRequest_t2316381291::get_offset_of__context_6(),
	HttpListenerRequest_t2316381291::get_offset_of__cookies_7(),
	HttpListenerRequest_t2316381291::get_offset_of__headers_8(),
	HttpListenerRequest_t2316381291::get_offset_of__identifier_9(),
	HttpListenerRequest_t2316381291::get_offset_of__inputStream_10(),
	HttpListenerRequest_t2316381291::get_offset_of__keepAlive_11(),
	HttpListenerRequest_t2316381291::get_offset_of__keepAliveSet_12(),
	HttpListenerRequest_t2316381291::get_offset_of__method_13(),
	HttpListenerRequest_t2316381291::get_offset_of__queryString_14(),
	HttpListenerRequest_t2316381291::get_offset_of__referer_15(),
	HttpListenerRequest_t2316381291::get_offset_of__uri_16(),
	HttpListenerRequest_t2316381291::get_offset_of__url_17(),
	HttpListenerRequest_t2316381291::get_offset_of__userLanguages_18(),
	HttpListenerRequest_t2316381291::get_offset_of__version_19(),
	HttpListenerRequest_t2316381291::get_offset_of__websocketRequest_20(),
	HttpListenerRequest_t2316381291::get_offset_of__websocketRequestSet_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3294 = { sizeof (HttpListenerResponse_t2223360553), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3294[16] = 
{
	HttpListenerResponse_t2223360553::get_offset_of__closeConnection_0(),
	HttpListenerResponse_t2223360553::get_offset_of__contentEncoding_1(),
	HttpListenerResponse_t2223360553::get_offset_of__contentLength_2(),
	HttpListenerResponse_t2223360553::get_offset_of__contentType_3(),
	HttpListenerResponse_t2223360553::get_offset_of__context_4(),
	HttpListenerResponse_t2223360553::get_offset_of__cookies_5(),
	HttpListenerResponse_t2223360553::get_offset_of__disposed_6(),
	HttpListenerResponse_t2223360553::get_offset_of__headers_7(),
	HttpListenerResponse_t2223360553::get_offset_of__headersSent_8(),
	HttpListenerResponse_t2223360553::get_offset_of__keepAlive_9(),
	HttpListenerResponse_t2223360553::get_offset_of__location_10(),
	HttpListenerResponse_t2223360553::get_offset_of__outputStream_11(),
	HttpListenerResponse_t2223360553::get_offset_of__sendChunked_12(),
	HttpListenerResponse_t2223360553::get_offset_of__statusCode_13(),
	HttpListenerResponse_t2223360553::get_offset_of__statusDescription_14(),
	HttpListenerResponse_t2223360553::get_offset_of__version_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3295 = { sizeof (U3CfindCookieU3Ec__Iterator0_t531581468), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3295[11] = 
{
	U3CfindCookieU3Ec__Iterator0_t531581468::get_offset_of_cookie_0(),
	U3CfindCookieU3Ec__Iterator0_t531581468::get_offset_of_U3CnameU3E__0_1(),
	U3CfindCookieU3Ec__Iterator0_t531581468::get_offset_of_U3CdomainU3E__0_2(),
	U3CfindCookieU3Ec__Iterator0_t531581468::get_offset_of_U3CpathU3E__0_3(),
	U3CfindCookieU3Ec__Iterator0_t531581468::get_offset_of_U24locvar0_4(),
	U3CfindCookieU3Ec__Iterator0_t531581468::get_offset_of_U3CcU3E__1_5(),
	U3CfindCookieU3Ec__Iterator0_t531581468::get_offset_of_U24locvar1_6(),
	U3CfindCookieU3Ec__Iterator0_t531581468::get_offset_of_U24this_7(),
	U3CfindCookieU3Ec__Iterator0_t531581468::get_offset_of_U24current_8(),
	U3CfindCookieU3Ec__Iterator0_t531581468::get_offset_of_U24disposing_9(),
	U3CfindCookieU3Ec__Iterator0_t531581468::get_offset_of_U24PC_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3296 = { sizeof (U3CCloseU3Ec__AnonStorey1_t1940037592), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3296[2] = 
{
	U3CCloseU3Ec__AnonStorey1_t1940037592::get_offset_of_output_0(),
	U3CCloseU3Ec__AnonStorey1_t1940037592::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3297 = { sizeof (HttpRequestHeader_t1796288394)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3297[46] = 
{
	HttpRequestHeader_t1796288394::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3298 = { sizeof (HttpResponseHeader_t1325853500)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3298[35] = 
{
	HttpResponseHeader_t1325853500::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3299 = { sizeof (HttpStatusCode_t2661820989)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3299[47] = 
{
	HttpStatusCode_t2661820989::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
