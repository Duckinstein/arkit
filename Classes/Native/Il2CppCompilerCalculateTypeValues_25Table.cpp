﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// IBM.Watson.DeveloperCloud.Services.DocumentConversion.v1.DocumentConversion
struct DocumentConversion_t1610681923;
// IBM.Watson.DeveloperCloud.Services.ServiceStatus
struct ServiceStatus_t1443707987;
// System.String
struct String_t;
// FullSerializer.fsSerializer
struct fsSerializer_t4193731081;
// IBM.Watson.DeveloperCloud.Services.DocumentConversion.v1.DocumentConversion/LoadFileDelegate
struct LoadFileDelegate_t484637430;
// IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.Language[]
struct LanguageU5BU5D_t2790361649;
// IBM.Watson.DeveloperCloud.Services.DocumentConversion.v1.Metadata[]
struct MetadataU5BU5D_t2549442342;
// IBM.Watson.DeveloperCloud.Services.DocumentConversion.v1.AnswerUnit[]
struct AnswerUnitU5BU5D_t702690105;
// IBM.Watson.DeveloperCloud.Services.DocumentConversion.v1.Warning[]
struct WarningU5BU5D_t573465225;
// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery
struct Discovery_t1478121420;
// IBM.Watson.DeveloperCloud.Services.DocumentConversion.v1.Content[]
struct ContentU5BU5D_t3098126528;
// IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.Translation[]
struct TranslationU5BU5D_t2644088720;
// IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.TranslationModel[]
struct TranslationModelU5BU5D_t1028387267;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Collections.Generic.Dictionary`2<System.String,IBM.Watson.DeveloperCloud.Connection.RESTConnector/Form>
struct Dictionary_2_t2694055125;
// IBM.Watson.DeveloperCloud.Connection.RESTConnector/ResponseEvent
struct ResponseEvent_t1616568356;
// IBM.Watson.DeveloperCloud.Connection.RESTConnector/ProgressEvent
struct ProgressEvent_t4185145044;
// IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.TranslationModel[]
struct TranslationModelU5BU5D_t662426443;
// IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.Translation[]
struct TranslationU5BU5D_t1094179602;
// IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.Language[]
struct LanguageU5BU5D_t1980958365;
// IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.LanguageTranslation
struct LanguageTranslation_t3099415401;
// IBM.Watson.DeveloperCloud.Services.Discovery.v1.AggregationTerm
struct AggregationTerm_t3655396846;
// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Notice[]
struct NoticeU5BU5D_t1969629099;
// IBM.Watson.DeveloperCloud.Services.Discovery.v1.QueryResult[]
struct QueryResultU5BU5D_t189578170;
// IBM.Watson.DeveloperCloud.Services.Discovery.v1.QueryAggregation
struct QueryAggregation_t1214220176;
// IBM.Watson.DeveloperCloud.Services.Discovery.v1.AggregationResult
struct AggregationResult_t3787092725;
// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Field[]
struct FieldU5BU5D_t2037695783;
// System.Void
struct Void_t1841601450;
// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/OnGetEnvironments
struct OnGetEnvironments_t613121798;
// IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.LanguageTranslation/TranslateCallback
struct TranslateCallback_t149709672;
// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/OnGetEnvironment
struct OnGetEnvironment_t3415272147;
// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/OnDeleteEnvironment
struct OnDeleteEnvironment_t1826514308;
// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/OnAddEnvironment
struct OnAddEnvironment_t731364804;
// IBM.Watson.DeveloperCloud.Services.DocumentConversion.v1.DocumentConversion/OnConvertDocument
struct OnConvertDocument_t1041817288;
// IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.LanguageTranslation/GetModelsCallback
struct GetModelsCallback_t1383378020;
// IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.LanguageTranslator/TranslateCallback
struct TranslateCallback_t3218931436;
// IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.LanguageTranslator/GetModelsCallback
struct GetModelsCallback_t2850854704;
// IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.LanguageTranslation/GetLanguagesCallback
struct GetLanguagesCallback_t833615519;
// IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.LanguageTranslation/GetModelCallback
struct GetModelCallback_t3256244769;
// System.Char[]
struct CharU5BU5D_t1328083999;
// IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.LanguageTranslation/IdentifyCallback
struct IdentifyCallback_t942722308;
// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/OnGetConfigurations
struct OnGetConfigurations_t2431280213;
// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/OnGetCollection
struct OnGetCollection_t85885496;
// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/OnAddDocument
struct OnAddDocument_t2879918214;
// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/OnGetConfiguration
struct OnGetConfiguration_t613203618;
// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/OnDeleteConfiguration
struct OnDeleteConfiguration_t1574428889;
// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/OnQuery
struct OnQuery_t887364716;
// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/OnDeleteDocument
struct OnDeleteDocument_t3959738954;
// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/OnGetFields
struct OnGetFields_t307794883;
// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/OnUpdateDocument
struct OnUpdateDocument_t4020936920;
// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/OnDeleteCollection
struct OnDeleteCollection_t603439291;
// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/OnGetCollections
struct OnGetCollections_t2546296547;
// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/OnAddConfiguration
struct OnAddConfiguration_t2751378613;
// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/OnPreviewConfiguration
struct OnPreviewConfiguration_t1311234390;
// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/OnAddCollection
struct OnAddCollection_t1214285067;
// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/OnGetDocument
struct OnGetDocument_t1915942165;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1572802995;
// IBM.Watson.DeveloperCloud.Services.Discovery.v1.CollectionRef
struct CollectionRef_t3248558799;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.TranslationModels
struct TranslationModels_t18565733;
// IBM.Watson.DeveloperCloud.Services.Discovery.v1.GetFieldsResponse
struct GetFieldsResponse_t2235887816;
// IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.Languages
struct Languages_t1673966175;
// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Environment
struct Environment_t3491688169;
// IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.Translations
struct Translations_t2818370350;
// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Collection
struct Collection_t3355621610;
// IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.TranslationModel
struct TranslationModel_t2730767334;
// IBM.Watson.DeveloperCloud.Services.Discovery.v1.GetCollectionsResponse
struct GetCollectionsResponse_t825581884;
// IBM.Watson.DeveloperCloud.Services.Discovery.v1.DocumentAccepted
struct DocumentAccepted_t4162415286;
// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Configuration
struct Configuration_t2574692812;
// IBM.Watson.DeveloperCloud.Services.Discovery.v1.TestDocument
struct TestDocument_t1009335169;
// IBM.Watson.DeveloperCloud.Services.Discovery.v1.GetEnvironmentsResponse
struct GetEnvironmentsResponse_t3378403827;
// IBM.Watson.DeveloperCloud.Services.DocumentConversion.v1.ConvertedDocument
struct ConvertedDocument_t1813677595;
// IBM.Watson.DeveloperCloud.Services.Discovery.v1.QueryResponse
struct QueryResponse_t507320049;
// IBM.Watson.DeveloperCloud.Services.Discovery.v1.GetConfigurationsResponse
struct GetConfigurationsResponse_t3984912378;
// IBM.Watson.DeveloperCloud.Services.Discovery.v1.DocumentStatus
struct DocumentStatus_t2219045527;
// IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.TranslationModel
struct TranslationModel_t4027325182;
// IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.TranslationModels
struct TranslationModels_t2926535245;
// IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.Languages
struct Languages_t4109093315;
// IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.Translations
struct Translations_t3057738030;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef CHECKSERVICESTATUS_T1899240418_H
#define CHECKSERVICESTATUS_T1899240418_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.DocumentConversion.v1.DocumentConversion/CheckServiceStatus
struct  CheckServiceStatus_t1899240418  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.DocumentConversion.v1.DocumentConversion IBM.Watson.DeveloperCloud.Services.DocumentConversion.v1.DocumentConversion/CheckServiceStatus::m_Service
	DocumentConversion_t1610681923 * ___m_Service_0;
	// IBM.Watson.DeveloperCloud.Services.ServiceStatus IBM.Watson.DeveloperCloud.Services.DocumentConversion.v1.DocumentConversion/CheckServiceStatus::m_Callback
	ServiceStatus_t1443707987 * ___m_Callback_1;

public:
	inline static int32_t get_offset_of_m_Service_0() { return static_cast<int32_t>(offsetof(CheckServiceStatus_t1899240418, ___m_Service_0)); }
	inline DocumentConversion_t1610681923 * get_m_Service_0() const { return ___m_Service_0; }
	inline DocumentConversion_t1610681923 ** get_address_of_m_Service_0() { return &___m_Service_0; }
	inline void set_m_Service_0(DocumentConversion_t1610681923 * value)
	{
		___m_Service_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Service_0), value);
	}

	inline static int32_t get_offset_of_m_Callback_1() { return static_cast<int32_t>(offsetof(CheckServiceStatus_t1899240418, ___m_Callback_1)); }
	inline ServiceStatus_t1443707987 * get_m_Callback_1() const { return ___m_Callback_1; }
	inline ServiceStatus_t1443707987 ** get_address_of_m_Callback_1() { return &___m_Callback_1; }
	inline void set_m_Callback_1(ServiceStatus_t1443707987 * value)
	{
		___m_Callback_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Callback_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHECKSERVICESTATUS_T1899240418_H
#ifndef VERSION_T1541338264_H
#define VERSION_T1541338264_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.DocumentConversion.v1.Version
struct  Version_t1541338264  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERSION_T1541338264_H
#ifndef DOCUMENTCONVERSION_T1610681923_H
#define DOCUMENTCONVERSION_T1610681923_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.DocumentConversion.v1.DocumentConversion
struct  DocumentConversion_t1610681923  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.DocumentConversion.v1.DocumentConversion/LoadFileDelegate IBM.Watson.DeveloperCloud.Services.DocumentConversion.v1.DocumentConversion::<LoadFile>k__BackingField
	LoadFileDelegate_t484637430 * ___U3CLoadFileU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CLoadFileU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(DocumentConversion_t1610681923, ___U3CLoadFileU3Ek__BackingField_3)); }
	inline LoadFileDelegate_t484637430 * get_U3CLoadFileU3Ek__BackingField_3() const { return ___U3CLoadFileU3Ek__BackingField_3; }
	inline LoadFileDelegate_t484637430 ** get_address_of_U3CLoadFileU3Ek__BackingField_3() { return &___U3CLoadFileU3Ek__BackingField_3; }
	inline void set_U3CLoadFileU3Ek__BackingField_3(LoadFileDelegate_t484637430 * value)
	{
		___U3CLoadFileU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLoadFileU3Ek__BackingField_3), value);
	}
};

struct DocumentConversion_t1610681923_StaticFields
{
public:
	// FullSerializer.fsSerializer IBM.Watson.DeveloperCloud.Services.DocumentConversion.v1.DocumentConversion::sm_Serializer
	fsSerializer_t4193731081 * ___sm_Serializer_1;

public:
	inline static int32_t get_offset_of_sm_Serializer_1() { return static_cast<int32_t>(offsetof(DocumentConversion_t1610681923_StaticFields, ___sm_Serializer_1)); }
	inline fsSerializer_t4193731081 * get_sm_Serializer_1() const { return ___sm_Serializer_1; }
	inline fsSerializer_t4193731081 ** get_address_of_sm_Serializer_1() { return &___sm_Serializer_1; }
	inline void set_sm_Serializer_1(fsSerializer_t4193731081 * value)
	{
		___sm_Serializer_1 = value;
		Il2CppCodeGenWriteBarrier((&___sm_Serializer_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOCUMENTCONVERSION_T1610681923_H
#ifndef SERVICEHELPER_T233431317_H
#define SERVICEHELPER_T233431317_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.ServiceHelper
struct  ServiceHelper_t233431317  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERVICEHELPER_T233431317_H
#ifndef TRANSLATION_T1796874115_H
#define TRANSLATION_T1796874115_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.Translation
struct  Translation_t1796874115  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.Translation::<translation>k__BackingField
	String_t* ___U3CtranslationU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CtranslationU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Translation_t1796874115, ___U3CtranslationU3Ek__BackingField_0)); }
	inline String_t* get_U3CtranslationU3Ek__BackingField_0() const { return ___U3CtranslationU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CtranslationU3Ek__BackingField_0() { return &___U3CtranslationU3Ek__BackingField_0; }
	inline void set_U3CtranslationU3Ek__BackingField_0(String_t* value)
	{
		___U3CtranslationU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtranslationU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSLATION_T1796874115_H
#ifndef LANGUAGES_T4109093315_H
#define LANGUAGES_T4109093315_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.Languages
struct  Languages_t4109093315  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.Language[] IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.Languages::<languages>k__BackingField
	LanguageU5BU5D_t2790361649* ___U3ClanguagesU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3ClanguagesU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Languages_t4109093315, ___U3ClanguagesU3Ek__BackingField_0)); }
	inline LanguageU5BU5D_t2790361649* get_U3ClanguagesU3Ek__BackingField_0() const { return ___U3ClanguagesU3Ek__BackingField_0; }
	inline LanguageU5BU5D_t2790361649** get_address_of_U3ClanguagesU3Ek__BackingField_0() { return &___U3ClanguagesU3Ek__BackingField_0; }
	inline void set_U3ClanguagesU3Ek__BackingField_0(LanguageU5BU5D_t2790361649* value)
	{
		___U3ClanguagesU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClanguagesU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LANGUAGES_T4109093315_H
#ifndef LANGUAGE_T834895248_H
#define LANGUAGE_T834895248_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.Language
struct  Language_t834895248  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.Language::<language>k__BackingField
	String_t* ___U3ClanguageU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.Language::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3ClanguageU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Language_t834895248, ___U3ClanguageU3Ek__BackingField_0)); }
	inline String_t* get_U3ClanguageU3Ek__BackingField_0() const { return ___U3ClanguageU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3ClanguageU3Ek__BackingField_0() { return &___U3ClanguageU3Ek__BackingField_0; }
	inline void set_U3ClanguageU3Ek__BackingField_0(String_t* value)
	{
		___U3ClanguageU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClanguageU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Language_t834895248, ___U3CnameU3Ek__BackingField_1)); }
	inline String_t* get_U3CnameU3Ek__BackingField_1() const { return ___U3CnameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_1() { return &___U3CnameU3Ek__BackingField_1; }
	inline void set_U3CnameU3Ek__BackingField_1(String_t* value)
	{
		___U3CnameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LANGUAGE_T834895248_H
#ifndef METADATA_T1932344831_H
#define METADATA_T1932344831_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.DocumentConversion.v1.Metadata
struct  Metadata_t1932344831  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.DocumentConversion.v1.Metadata::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.DocumentConversion.v1.Metadata::<content>k__BackingField
	String_t* ___U3CcontentU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Metadata_t1932344831, ___U3CnameU3Ek__BackingField_0)); }
	inline String_t* get_U3CnameU3Ek__BackingField_0() const { return ___U3CnameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_0() { return &___U3CnameU3Ek__BackingField_0; }
	inline void set_U3CnameU3Ek__BackingField_0(String_t* value)
	{
		___U3CnameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CcontentU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Metadata_t1932344831, ___U3CcontentU3Ek__BackingField_1)); }
	inline String_t* get_U3CcontentU3Ek__BackingField_1() const { return ___U3CcontentU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CcontentU3Ek__BackingField_1() { return &___U3CcontentU3Ek__BackingField_1; }
	inline void set_U3CcontentU3Ek__BackingField_1(String_t* value)
	{
		___U3CcontentU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcontentU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METADATA_T1932344831_H
#ifndef CONVERTEDDOCUMENT_T1813677595_H
#define CONVERTEDDOCUMENT_T1813677595_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.DocumentConversion.v1.ConvertedDocument
struct  ConvertedDocument_t1813677595  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.DocumentConversion.v1.ConvertedDocument::<source_document_id>k__BackingField
	String_t* ___U3Csource_document_idU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.DocumentConversion.v1.ConvertedDocument::<timestamp>k__BackingField
	String_t* ___U3CtimestampU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.DocumentConversion.v1.ConvertedDocument::<media_type_detected>k__BackingField
	String_t* ___U3Cmedia_type_detectedU3Ek__BackingField_2;
	// IBM.Watson.DeveloperCloud.Services.DocumentConversion.v1.Metadata[] IBM.Watson.DeveloperCloud.Services.DocumentConversion.v1.ConvertedDocument::<metadata>k__BackingField
	MetadataU5BU5D_t2549442342* ___U3CmetadataU3Ek__BackingField_3;
	// IBM.Watson.DeveloperCloud.Services.DocumentConversion.v1.AnswerUnit[] IBM.Watson.DeveloperCloud.Services.DocumentConversion.v1.ConvertedDocument::<answer_units>k__BackingField
	AnswerUnitU5BU5D_t702690105* ___U3Canswer_unitsU3Ek__BackingField_4;
	// IBM.Watson.DeveloperCloud.Services.DocumentConversion.v1.Warning[] IBM.Watson.DeveloperCloud.Services.DocumentConversion.v1.ConvertedDocument::<warnings>k__BackingField
	WarningU5BU5D_t573465225* ___U3CwarningsU3Ek__BackingField_5;
	// System.String IBM.Watson.DeveloperCloud.Services.DocumentConversion.v1.ConvertedDocument::<textContent>k__BackingField
	String_t* ___U3CtextContentU3Ek__BackingField_6;
	// System.String IBM.Watson.DeveloperCloud.Services.DocumentConversion.v1.ConvertedDocument::<htmlContent>k__BackingField
	String_t* ___U3ChtmlContentU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_U3Csource_document_idU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ConvertedDocument_t1813677595, ___U3Csource_document_idU3Ek__BackingField_0)); }
	inline String_t* get_U3Csource_document_idU3Ek__BackingField_0() const { return ___U3Csource_document_idU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3Csource_document_idU3Ek__BackingField_0() { return &___U3Csource_document_idU3Ek__BackingField_0; }
	inline void set_U3Csource_document_idU3Ek__BackingField_0(String_t* value)
	{
		___U3Csource_document_idU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Csource_document_idU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CtimestampU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ConvertedDocument_t1813677595, ___U3CtimestampU3Ek__BackingField_1)); }
	inline String_t* get_U3CtimestampU3Ek__BackingField_1() const { return ___U3CtimestampU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CtimestampU3Ek__BackingField_1() { return &___U3CtimestampU3Ek__BackingField_1; }
	inline void set_U3CtimestampU3Ek__BackingField_1(String_t* value)
	{
		___U3CtimestampU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtimestampU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3Cmedia_type_detectedU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ConvertedDocument_t1813677595, ___U3Cmedia_type_detectedU3Ek__BackingField_2)); }
	inline String_t* get_U3Cmedia_type_detectedU3Ek__BackingField_2() const { return ___U3Cmedia_type_detectedU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3Cmedia_type_detectedU3Ek__BackingField_2() { return &___U3Cmedia_type_detectedU3Ek__BackingField_2; }
	inline void set_U3Cmedia_type_detectedU3Ek__BackingField_2(String_t* value)
	{
		___U3Cmedia_type_detectedU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cmedia_type_detectedU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CmetadataU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ConvertedDocument_t1813677595, ___U3CmetadataU3Ek__BackingField_3)); }
	inline MetadataU5BU5D_t2549442342* get_U3CmetadataU3Ek__BackingField_3() const { return ___U3CmetadataU3Ek__BackingField_3; }
	inline MetadataU5BU5D_t2549442342** get_address_of_U3CmetadataU3Ek__BackingField_3() { return &___U3CmetadataU3Ek__BackingField_3; }
	inline void set_U3CmetadataU3Ek__BackingField_3(MetadataU5BU5D_t2549442342* value)
	{
		___U3CmetadataU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmetadataU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3Canswer_unitsU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(ConvertedDocument_t1813677595, ___U3Canswer_unitsU3Ek__BackingField_4)); }
	inline AnswerUnitU5BU5D_t702690105* get_U3Canswer_unitsU3Ek__BackingField_4() const { return ___U3Canswer_unitsU3Ek__BackingField_4; }
	inline AnswerUnitU5BU5D_t702690105** get_address_of_U3Canswer_unitsU3Ek__BackingField_4() { return &___U3Canswer_unitsU3Ek__BackingField_4; }
	inline void set_U3Canswer_unitsU3Ek__BackingField_4(AnswerUnitU5BU5D_t702690105* value)
	{
		___U3Canswer_unitsU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3Canswer_unitsU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CwarningsU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(ConvertedDocument_t1813677595, ___U3CwarningsU3Ek__BackingField_5)); }
	inline WarningU5BU5D_t573465225* get_U3CwarningsU3Ek__BackingField_5() const { return ___U3CwarningsU3Ek__BackingField_5; }
	inline WarningU5BU5D_t573465225** get_address_of_U3CwarningsU3Ek__BackingField_5() { return &___U3CwarningsU3Ek__BackingField_5; }
	inline void set_U3CwarningsU3Ek__BackingField_5(WarningU5BU5D_t573465225* value)
	{
		___U3CwarningsU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwarningsU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CtextContentU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(ConvertedDocument_t1813677595, ___U3CtextContentU3Ek__BackingField_6)); }
	inline String_t* get_U3CtextContentU3Ek__BackingField_6() const { return ___U3CtextContentU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CtextContentU3Ek__BackingField_6() { return &___U3CtextContentU3Ek__BackingField_6; }
	inline void set_U3CtextContentU3Ek__BackingField_6(String_t* value)
	{
		___U3CtextContentU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextContentU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3ChtmlContentU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(ConvertedDocument_t1813677595, ___U3ChtmlContentU3Ek__BackingField_7)); }
	inline String_t* get_U3ChtmlContentU3Ek__BackingField_7() const { return ___U3ChtmlContentU3Ek__BackingField_7; }
	inline String_t** get_address_of_U3ChtmlContentU3Ek__BackingField_7() { return &___U3ChtmlContentU3Ek__BackingField_7; }
	inline void set_U3ChtmlContentU3Ek__BackingField_7(String_t* value)
	{
		___U3ChtmlContentU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3ChtmlContentU3Ek__BackingField_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONVERTEDDOCUMENT_T1813677595_H
#ifndef CHECKSERVICESTATUS_T3621665888_H
#define CHECKSERVICESTATUS_T3621665888_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/CheckServiceStatus
struct  CheckServiceStatus_t3621665888  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/CheckServiceStatus::m_Service
	Discovery_t1478121420 * ___m_Service_0;
	// IBM.Watson.DeveloperCloud.Services.ServiceStatus IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/CheckServiceStatus::m_Callback
	ServiceStatus_t1443707987 * ___m_Callback_1;

public:
	inline static int32_t get_offset_of_m_Service_0() { return static_cast<int32_t>(offsetof(CheckServiceStatus_t3621665888, ___m_Service_0)); }
	inline Discovery_t1478121420 * get_m_Service_0() const { return ___m_Service_0; }
	inline Discovery_t1478121420 ** get_address_of_m_Service_0() { return &___m_Service_0; }
	inline void set_m_Service_0(Discovery_t1478121420 * value)
	{
		___m_Service_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Service_0), value);
	}

	inline static int32_t get_offset_of_m_Callback_1() { return static_cast<int32_t>(offsetof(CheckServiceStatus_t3621665888, ___m_Callback_1)); }
	inline ServiceStatus_t1443707987 * get_m_Callback_1() const { return ___m_Callback_1; }
	inline ServiceStatus_t1443707987 ** get_address_of_m_Callback_1() { return &___m_Callback_1; }
	inline void set_m_Callback_1(ServiceStatus_t1443707987 * value)
	{
		___m_Callback_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Callback_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHECKSERVICESTATUS_T3621665888_H
#ifndef ANSWERUNIT_T3274674984_H
#define ANSWERUNIT_T3274674984_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.DocumentConversion.v1.AnswerUnit
struct  AnswerUnit_t3274674984  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.DocumentConversion.v1.AnswerUnit::<id>k__BackingField
	String_t* ___U3CidU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.DocumentConversion.v1.AnswerUnit::<type>k__BackingField
	String_t* ___U3CtypeU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.DocumentConversion.v1.AnswerUnit::<parent_id>k__BackingField
	String_t* ___U3Cparent_idU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Services.DocumentConversion.v1.AnswerUnit::<title>k__BackingField
	String_t* ___U3CtitleU3Ek__BackingField_3;
	// System.String IBM.Watson.DeveloperCloud.Services.DocumentConversion.v1.AnswerUnit::<direction>k__BackingField
	String_t* ___U3CdirectionU3Ek__BackingField_4;
	// IBM.Watson.DeveloperCloud.Services.DocumentConversion.v1.Content[] IBM.Watson.DeveloperCloud.Services.DocumentConversion.v1.AnswerUnit::<content>k__BackingField
	ContentU5BU5D_t3098126528* ___U3CcontentU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CidU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AnswerUnit_t3274674984, ___U3CidU3Ek__BackingField_0)); }
	inline String_t* get_U3CidU3Ek__BackingField_0() const { return ___U3CidU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CidU3Ek__BackingField_0() { return &___U3CidU3Ek__BackingField_0; }
	inline void set_U3CidU3Ek__BackingField_0(String_t* value)
	{
		___U3CidU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CidU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CtypeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AnswerUnit_t3274674984, ___U3CtypeU3Ek__BackingField_1)); }
	inline String_t* get_U3CtypeU3Ek__BackingField_1() const { return ___U3CtypeU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CtypeU3Ek__BackingField_1() { return &___U3CtypeU3Ek__BackingField_1; }
	inline void set_U3CtypeU3Ek__BackingField_1(String_t* value)
	{
		___U3CtypeU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtypeU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3Cparent_idU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AnswerUnit_t3274674984, ___U3Cparent_idU3Ek__BackingField_2)); }
	inline String_t* get_U3Cparent_idU3Ek__BackingField_2() const { return ___U3Cparent_idU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3Cparent_idU3Ek__BackingField_2() { return &___U3Cparent_idU3Ek__BackingField_2; }
	inline void set_U3Cparent_idU3Ek__BackingField_2(String_t* value)
	{
		___U3Cparent_idU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cparent_idU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CtitleU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AnswerUnit_t3274674984, ___U3CtitleU3Ek__BackingField_3)); }
	inline String_t* get_U3CtitleU3Ek__BackingField_3() const { return ___U3CtitleU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CtitleU3Ek__BackingField_3() { return &___U3CtitleU3Ek__BackingField_3; }
	inline void set_U3CtitleU3Ek__BackingField_3(String_t* value)
	{
		___U3CtitleU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtitleU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CdirectionU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AnswerUnit_t3274674984, ___U3CdirectionU3Ek__BackingField_4)); }
	inline String_t* get_U3CdirectionU3Ek__BackingField_4() const { return ___U3CdirectionU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CdirectionU3Ek__BackingField_4() { return &___U3CdirectionU3Ek__BackingField_4; }
	inline void set_U3CdirectionU3Ek__BackingField_4(String_t* value)
	{
		___U3CdirectionU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdirectionU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CcontentU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(AnswerUnit_t3274674984, ___U3CcontentU3Ek__BackingField_5)); }
	inline ContentU5BU5D_t3098126528* get_U3CcontentU3Ek__BackingField_5() const { return ___U3CcontentU3Ek__BackingField_5; }
	inline ContentU5BU5D_t3098126528** get_address_of_U3CcontentU3Ek__BackingField_5() { return &___U3CcontentU3Ek__BackingField_5; }
	inline void set_U3CcontentU3Ek__BackingField_5(ContentU5BU5D_t3098126528* value)
	{
		___U3CcontentU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcontentU3Ek__BackingField_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANSWERUNIT_T3274674984_H
#ifndef CONVERSIONTARGET_T2856393115_H
#define CONVERSIONTARGET_T2856393115_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.DocumentConversion.v1.ConversionTarget
struct  ConversionTarget_t2856393115  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONVERSIONTARGET_T2856393115_H
#ifndef WARNING_T1238678040_H
#define WARNING_T1238678040_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.DocumentConversion.v1.Warning
struct  Warning_t1238678040  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.DocumentConversion.v1.Warning::<code>k__BackingField
	String_t* ___U3CcodeU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.DocumentConversion.v1.Warning::<error>k__BackingField
	String_t* ___U3CerrorU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CcodeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Warning_t1238678040, ___U3CcodeU3Ek__BackingField_0)); }
	inline String_t* get_U3CcodeU3Ek__BackingField_0() const { return ___U3CcodeU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CcodeU3Ek__BackingField_0() { return &___U3CcodeU3Ek__BackingField_0; }
	inline void set_U3CcodeU3Ek__BackingField_0(String_t* value)
	{
		___U3CcodeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcodeU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CerrorU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Warning_t1238678040, ___U3CerrorU3Ek__BackingField_1)); }
	inline String_t* get_U3CerrorU3Ek__BackingField_1() const { return ___U3CerrorU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CerrorU3Ek__BackingField_1() { return &___U3CerrorU3Ek__BackingField_1; }
	inline void set_U3CerrorU3Ek__BackingField_1(String_t* value)
	{
		___U3CerrorU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CerrorU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WARNING_T1238678040_H
#ifndef CONTENT_T2184047853_H
#define CONTENT_T2184047853_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.DocumentConversion.v1.Content
struct  Content_t2184047853  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.DocumentConversion.v1.Content::<media_type>k__BackingField
	String_t* ___U3Cmedia_typeU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.DocumentConversion.v1.Content::<text>k__BackingField
	String_t* ___U3CtextU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3Cmedia_typeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Content_t2184047853, ___U3Cmedia_typeU3Ek__BackingField_0)); }
	inline String_t* get_U3Cmedia_typeU3Ek__BackingField_0() const { return ___U3Cmedia_typeU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3Cmedia_typeU3Ek__BackingField_0() { return &___U3Cmedia_typeU3Ek__BackingField_0; }
	inline void set_U3Cmedia_typeU3Ek__BackingField_0(String_t* value)
	{
		___U3Cmedia_typeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cmedia_typeU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CtextU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Content_t2184047853, ___U3CtextU3Ek__BackingField_1)); }
	inline String_t* get_U3CtextU3Ek__BackingField_1() const { return ___U3CtextU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CtextU3Ek__BackingField_1() { return &___U3CtextU3Ek__BackingField_1; }
	inline void set_U3CtextU3Ek__BackingField_1(String_t* value)
	{
		___U3CtextU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTENT_T2184047853_H
#ifndef TRANSLATIONMODEL_T2730767334_H
#define TRANSLATIONMODEL_T2730767334_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.TranslationModel
struct  TranslationModel_t2730767334  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.TranslationModel::<model_id>k__BackingField
	String_t* ___U3Cmodel_idU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.TranslationModel::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.TranslationModel::<source>k__BackingField
	String_t* ___U3CsourceU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.TranslationModel::<target>k__BackingField
	String_t* ___U3CtargetU3Ek__BackingField_3;
	// System.String IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.TranslationModel::<base_model_id>k__BackingField
	String_t* ___U3Cbase_model_idU3Ek__BackingField_4;
	// System.String IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.TranslationModel::<domain>k__BackingField
	String_t* ___U3CdomainU3Ek__BackingField_5;
	// System.Boolean IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.TranslationModel::<customizable>k__BackingField
	bool ___U3CcustomizableU3Ek__BackingField_6;
	// System.Boolean IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.TranslationModel::<default>k__BackingField
	bool ___U3CdefaultU3Ek__BackingField_7;
	// System.String IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.TranslationModel::<owner>k__BackingField
	String_t* ___U3CownerU3Ek__BackingField_8;
	// System.String IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.TranslationModel::<status>k__BackingField
	String_t* ___U3CstatusU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of_U3Cmodel_idU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TranslationModel_t2730767334, ___U3Cmodel_idU3Ek__BackingField_0)); }
	inline String_t* get_U3Cmodel_idU3Ek__BackingField_0() const { return ___U3Cmodel_idU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3Cmodel_idU3Ek__BackingField_0() { return &___U3Cmodel_idU3Ek__BackingField_0; }
	inline void set_U3Cmodel_idU3Ek__BackingField_0(String_t* value)
	{
		___U3Cmodel_idU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cmodel_idU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TranslationModel_t2730767334, ___U3CnameU3Ek__BackingField_1)); }
	inline String_t* get_U3CnameU3Ek__BackingField_1() const { return ___U3CnameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_1() { return &___U3CnameU3Ek__BackingField_1; }
	inline void set_U3CnameU3Ek__BackingField_1(String_t* value)
	{
		___U3CnameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CsourceU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(TranslationModel_t2730767334, ___U3CsourceU3Ek__BackingField_2)); }
	inline String_t* get_U3CsourceU3Ek__BackingField_2() const { return ___U3CsourceU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CsourceU3Ek__BackingField_2() { return &___U3CsourceU3Ek__BackingField_2; }
	inline void set_U3CsourceU3Ek__BackingField_2(String_t* value)
	{
		___U3CsourceU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsourceU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CtargetU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(TranslationModel_t2730767334, ___U3CtargetU3Ek__BackingField_3)); }
	inline String_t* get_U3CtargetU3Ek__BackingField_3() const { return ___U3CtargetU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CtargetU3Ek__BackingField_3() { return &___U3CtargetU3Ek__BackingField_3; }
	inline void set_U3CtargetU3Ek__BackingField_3(String_t* value)
	{
		___U3CtargetU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtargetU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3Cbase_model_idU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(TranslationModel_t2730767334, ___U3Cbase_model_idU3Ek__BackingField_4)); }
	inline String_t* get_U3Cbase_model_idU3Ek__BackingField_4() const { return ___U3Cbase_model_idU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3Cbase_model_idU3Ek__BackingField_4() { return &___U3Cbase_model_idU3Ek__BackingField_4; }
	inline void set_U3Cbase_model_idU3Ek__BackingField_4(String_t* value)
	{
		___U3Cbase_model_idU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cbase_model_idU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CdomainU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(TranslationModel_t2730767334, ___U3CdomainU3Ek__BackingField_5)); }
	inline String_t* get_U3CdomainU3Ek__BackingField_5() const { return ___U3CdomainU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CdomainU3Ek__BackingField_5() { return &___U3CdomainU3Ek__BackingField_5; }
	inline void set_U3CdomainU3Ek__BackingField_5(String_t* value)
	{
		___U3CdomainU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdomainU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CcustomizableU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(TranslationModel_t2730767334, ___U3CcustomizableU3Ek__BackingField_6)); }
	inline bool get_U3CcustomizableU3Ek__BackingField_6() const { return ___U3CcustomizableU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CcustomizableU3Ek__BackingField_6() { return &___U3CcustomizableU3Ek__BackingField_6; }
	inline void set_U3CcustomizableU3Ek__BackingField_6(bool value)
	{
		___U3CcustomizableU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CdefaultU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(TranslationModel_t2730767334, ___U3CdefaultU3Ek__BackingField_7)); }
	inline bool get_U3CdefaultU3Ek__BackingField_7() const { return ___U3CdefaultU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CdefaultU3Ek__BackingField_7() { return &___U3CdefaultU3Ek__BackingField_7; }
	inline void set_U3CdefaultU3Ek__BackingField_7(bool value)
	{
		___U3CdefaultU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CownerU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(TranslationModel_t2730767334, ___U3CownerU3Ek__BackingField_8)); }
	inline String_t* get_U3CownerU3Ek__BackingField_8() const { return ___U3CownerU3Ek__BackingField_8; }
	inline String_t** get_address_of_U3CownerU3Ek__BackingField_8() { return &___U3CownerU3Ek__BackingField_8; }
	inline void set_U3CownerU3Ek__BackingField_8(String_t* value)
	{
		___U3CownerU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CownerU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CstatusU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(TranslationModel_t2730767334, ___U3CstatusU3Ek__BackingField_9)); }
	inline String_t* get_U3CstatusU3Ek__BackingField_9() const { return ___U3CstatusU3Ek__BackingField_9; }
	inline String_t** get_address_of_U3CstatusU3Ek__BackingField_9() { return &___U3CstatusU3Ek__BackingField_9; }
	inline void set_U3CstatusU3Ek__BackingField_9(String_t* value)
	{
		___U3CstatusU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstatusU3Ek__BackingField_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSLATIONMODEL_T2730767334_H
#ifndef TRANSLATIONS_T2818370350_H
#define TRANSLATIONS_T2818370350_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.Translations
struct  Translations_t2818370350  : public RuntimeObject
{
public:
	// System.Int64 IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.Translations::<word_count>k__BackingField
	int64_t ___U3Cword_countU3Ek__BackingField_0;
	// System.Int64 IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.Translations::<character_count>k__BackingField
	int64_t ___U3Ccharacter_countU3Ek__BackingField_1;
	// IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.Translation[] IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.Translations::<translations>k__BackingField
	TranslationU5BU5D_t2644088720* ___U3CtranslationsU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3Cword_countU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Translations_t2818370350, ___U3Cword_countU3Ek__BackingField_0)); }
	inline int64_t get_U3Cword_countU3Ek__BackingField_0() const { return ___U3Cword_countU3Ek__BackingField_0; }
	inline int64_t* get_address_of_U3Cword_countU3Ek__BackingField_0() { return &___U3Cword_countU3Ek__BackingField_0; }
	inline void set_U3Cword_countU3Ek__BackingField_0(int64_t value)
	{
		___U3Cword_countU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3Ccharacter_countU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Translations_t2818370350, ___U3Ccharacter_countU3Ek__BackingField_1)); }
	inline int64_t get_U3Ccharacter_countU3Ek__BackingField_1() const { return ___U3Ccharacter_countU3Ek__BackingField_1; }
	inline int64_t* get_address_of_U3Ccharacter_countU3Ek__BackingField_1() { return &___U3Ccharacter_countU3Ek__BackingField_1; }
	inline void set_U3Ccharacter_countU3Ek__BackingField_1(int64_t value)
	{
		___U3Ccharacter_countU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CtranslationsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Translations_t2818370350, ___U3CtranslationsU3Ek__BackingField_2)); }
	inline TranslationU5BU5D_t2644088720* get_U3CtranslationsU3Ek__BackingField_2() const { return ___U3CtranslationsU3Ek__BackingField_2; }
	inline TranslationU5BU5D_t2644088720** get_address_of_U3CtranslationsU3Ek__BackingField_2() { return &___U3CtranslationsU3Ek__BackingField_2; }
	inline void set_U3CtranslationsU3Ek__BackingField_2(TranslationU5BU5D_t2644088720* value)
	{
		___U3CtranslationsU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtranslationsU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSLATIONS_T2818370350_H
#ifndef TRANSLATION_T308652637_H
#define TRANSLATION_T308652637_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.Translation
struct  Translation_t308652637  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.Translation::<translation>k__BackingField
	String_t* ___U3CtranslationU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CtranslationU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Translation_t308652637, ___U3CtranslationU3Ek__BackingField_0)); }
	inline String_t* get_U3CtranslationU3Ek__BackingField_0() const { return ___U3CtranslationU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CtranslationU3Ek__BackingField_0() { return &___U3CtranslationU3Ek__BackingField_0; }
	inline void set_U3CtranslationU3Ek__BackingField_0(String_t* value)
	{
		___U3CtranslationU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtranslationU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSLATION_T308652637_H
#ifndef TRANSLATIONMODELS_T18565733_H
#define TRANSLATIONMODELS_T18565733_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.TranslationModels
struct  TranslationModels_t18565733  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.TranslationModel[] IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.TranslationModels::<models>k__BackingField
	TranslationModelU5BU5D_t1028387267* ___U3CmodelsU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CmodelsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TranslationModels_t18565733, ___U3CmodelsU3Ek__BackingField_0)); }
	inline TranslationModelU5BU5D_t1028387267* get_U3CmodelsU3Ek__BackingField_0() const { return ___U3CmodelsU3Ek__BackingField_0; }
	inline TranslationModelU5BU5D_t1028387267** get_address_of_U3CmodelsU3Ek__BackingField_0() { return &___U3CmodelsU3Ek__BackingField_0; }
	inline void set_U3CmodelsU3Ek__BackingField_0(TranslationModelU5BU5D_t1028387267* value)
	{
		___U3CmodelsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmodelsU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSLATIONMODELS_T18565733_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef REQUEST_T466816980_H
#define REQUEST_T466816980_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request
struct  Request_t466816980  : public RuntimeObject
{
public:
	// System.Single IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Timeout>k__BackingField
	float ___U3CTimeoutU3Ek__BackingField_0;
	// System.Boolean IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Cancel>k__BackingField
	bool ___U3CCancelU3Ek__BackingField_1;
	// System.Boolean IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Delete>k__BackingField
	bool ___U3CDeleteU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Function>k__BackingField
	String_t* ___U3CFunctionU3Ek__BackingField_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Parameters>k__BackingField
	Dictionary_2_t309261261 * ___U3CParametersU3Ek__BackingField_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Headers>k__BackingField
	Dictionary_2_t3943999495 * ___U3CHeadersU3Ek__BackingField_5;
	// System.Byte[] IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Send>k__BackingField
	ByteU5BU5D_t3397334013* ___U3CSendU3Ek__BackingField_6;
	// System.Collections.Generic.Dictionary`2<System.String,IBM.Watson.DeveloperCloud.Connection.RESTConnector/Form> IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Forms>k__BackingField
	Dictionary_2_t2694055125 * ___U3CFormsU3Ek__BackingField_7;
	// IBM.Watson.DeveloperCloud.Connection.RESTConnector/ResponseEvent IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<OnResponse>k__BackingField
	ResponseEvent_t1616568356 * ___U3COnResponseU3Ek__BackingField_8;
	// IBM.Watson.DeveloperCloud.Connection.RESTConnector/ProgressEvent IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<OnDownloadProgress>k__BackingField
	ProgressEvent_t4185145044 * ___U3COnDownloadProgressU3Ek__BackingField_9;
	// IBM.Watson.DeveloperCloud.Connection.RESTConnector/ProgressEvent IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<OnUploadProgress>k__BackingField
	ProgressEvent_t4185145044 * ___U3COnUploadProgressU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of_U3CTimeoutU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CTimeoutU3Ek__BackingField_0)); }
	inline float get_U3CTimeoutU3Ek__BackingField_0() const { return ___U3CTimeoutU3Ek__BackingField_0; }
	inline float* get_address_of_U3CTimeoutU3Ek__BackingField_0() { return &___U3CTimeoutU3Ek__BackingField_0; }
	inline void set_U3CTimeoutU3Ek__BackingField_0(float value)
	{
		___U3CTimeoutU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CCancelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CCancelU3Ek__BackingField_1)); }
	inline bool get_U3CCancelU3Ek__BackingField_1() const { return ___U3CCancelU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CCancelU3Ek__BackingField_1() { return &___U3CCancelU3Ek__BackingField_1; }
	inline void set_U3CCancelU3Ek__BackingField_1(bool value)
	{
		___U3CCancelU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CDeleteU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CDeleteU3Ek__BackingField_2)); }
	inline bool get_U3CDeleteU3Ek__BackingField_2() const { return ___U3CDeleteU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CDeleteU3Ek__BackingField_2() { return &___U3CDeleteU3Ek__BackingField_2; }
	inline void set_U3CDeleteU3Ek__BackingField_2(bool value)
	{
		___U3CDeleteU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CFunctionU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CFunctionU3Ek__BackingField_3)); }
	inline String_t* get_U3CFunctionU3Ek__BackingField_3() const { return ___U3CFunctionU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CFunctionU3Ek__BackingField_3() { return &___U3CFunctionU3Ek__BackingField_3; }
	inline void set_U3CFunctionU3Ek__BackingField_3(String_t* value)
	{
		___U3CFunctionU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFunctionU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CParametersU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CParametersU3Ek__BackingField_4)); }
	inline Dictionary_2_t309261261 * get_U3CParametersU3Ek__BackingField_4() const { return ___U3CParametersU3Ek__BackingField_4; }
	inline Dictionary_2_t309261261 ** get_address_of_U3CParametersU3Ek__BackingField_4() { return &___U3CParametersU3Ek__BackingField_4; }
	inline void set_U3CParametersU3Ek__BackingField_4(Dictionary_2_t309261261 * value)
	{
		___U3CParametersU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CParametersU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CHeadersU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CHeadersU3Ek__BackingField_5)); }
	inline Dictionary_2_t3943999495 * get_U3CHeadersU3Ek__BackingField_5() const { return ___U3CHeadersU3Ek__BackingField_5; }
	inline Dictionary_2_t3943999495 ** get_address_of_U3CHeadersU3Ek__BackingField_5() { return &___U3CHeadersU3Ek__BackingField_5; }
	inline void set_U3CHeadersU3Ek__BackingField_5(Dictionary_2_t3943999495 * value)
	{
		___U3CHeadersU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CHeadersU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CSendU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CSendU3Ek__BackingField_6)); }
	inline ByteU5BU5D_t3397334013* get_U3CSendU3Ek__BackingField_6() const { return ___U3CSendU3Ek__BackingField_6; }
	inline ByteU5BU5D_t3397334013** get_address_of_U3CSendU3Ek__BackingField_6() { return &___U3CSendU3Ek__BackingField_6; }
	inline void set_U3CSendU3Ek__BackingField_6(ByteU5BU5D_t3397334013* value)
	{
		___U3CSendU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSendU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CFormsU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CFormsU3Ek__BackingField_7)); }
	inline Dictionary_2_t2694055125 * get_U3CFormsU3Ek__BackingField_7() const { return ___U3CFormsU3Ek__BackingField_7; }
	inline Dictionary_2_t2694055125 ** get_address_of_U3CFormsU3Ek__BackingField_7() { return &___U3CFormsU3Ek__BackingField_7; }
	inline void set_U3CFormsU3Ek__BackingField_7(Dictionary_2_t2694055125 * value)
	{
		___U3CFormsU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFormsU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3COnResponseU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3COnResponseU3Ek__BackingField_8)); }
	inline ResponseEvent_t1616568356 * get_U3COnResponseU3Ek__BackingField_8() const { return ___U3COnResponseU3Ek__BackingField_8; }
	inline ResponseEvent_t1616568356 ** get_address_of_U3COnResponseU3Ek__BackingField_8() { return &___U3COnResponseU3Ek__BackingField_8; }
	inline void set_U3COnResponseU3Ek__BackingField_8(ResponseEvent_t1616568356 * value)
	{
		___U3COnResponseU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3COnResponseU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3COnDownloadProgressU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3COnDownloadProgressU3Ek__BackingField_9)); }
	inline ProgressEvent_t4185145044 * get_U3COnDownloadProgressU3Ek__BackingField_9() const { return ___U3COnDownloadProgressU3Ek__BackingField_9; }
	inline ProgressEvent_t4185145044 ** get_address_of_U3COnDownloadProgressU3Ek__BackingField_9() { return &___U3COnDownloadProgressU3Ek__BackingField_9; }
	inline void set_U3COnDownloadProgressU3Ek__BackingField_9(ProgressEvent_t4185145044 * value)
	{
		___U3COnDownloadProgressU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3COnDownloadProgressU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3COnUploadProgressU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3COnUploadProgressU3Ek__BackingField_10)); }
	inline ProgressEvent_t4185145044 * get_U3COnUploadProgressU3Ek__BackingField_10() const { return ___U3COnUploadProgressU3Ek__BackingField_10; }
	inline ProgressEvent_t4185145044 ** get_address_of_U3COnUploadProgressU3Ek__BackingField_10() { return &___U3COnUploadProgressU3Ek__BackingField_10; }
	inline void set_U3COnUploadProgressU3Ek__BackingField_10(ProgressEvent_t4185145044 * value)
	{
		___U3COnUploadProgressU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3COnUploadProgressU3Ek__BackingField_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUEST_T466816980_H
#ifndef LANGUAGETRANSLATOR_T2981742234_H
#define LANGUAGETRANSLATOR_T2981742234_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.LanguageTranslator
struct  LanguageTranslator_t2981742234  : public RuntimeObject
{
public:

public:
};

struct LanguageTranslator_t2981742234_StaticFields
{
public:
	// FullSerializer.fsSerializer IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.LanguageTranslator::sm_Serializer
	fsSerializer_t4193731081 * ___sm_Serializer_1;

public:
	inline static int32_t get_offset_of_sm_Serializer_1() { return static_cast<int32_t>(offsetof(LanguageTranslator_t2981742234_StaticFields, ___sm_Serializer_1)); }
	inline fsSerializer_t4193731081 * get_sm_Serializer_1() const { return ___sm_Serializer_1; }
	inline fsSerializer_t4193731081 ** get_address_of_sm_Serializer_1() { return &___sm_Serializer_1; }
	inline void set_sm_Serializer_1(fsSerializer_t4193731081 * value)
	{
		___sm_Serializer_1 = value;
		Il2CppCodeGenWriteBarrier((&___sm_Serializer_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LANGUAGETRANSLATOR_T2981742234_H
#ifndef TRANSLATIONMODELS_T2926535245_H
#define TRANSLATIONMODELS_T2926535245_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.TranslationModels
struct  TranslationModels_t2926535245  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.TranslationModel[] IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.TranslationModels::<models>k__BackingField
	TranslationModelU5BU5D_t662426443* ___U3CmodelsU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CmodelsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TranslationModels_t2926535245, ___U3CmodelsU3Ek__BackingField_0)); }
	inline TranslationModelU5BU5D_t662426443* get_U3CmodelsU3Ek__BackingField_0() const { return ___U3CmodelsU3Ek__BackingField_0; }
	inline TranslationModelU5BU5D_t662426443** get_address_of_U3CmodelsU3Ek__BackingField_0() { return &___U3CmodelsU3Ek__BackingField_0; }
	inline void set_U3CmodelsU3Ek__BackingField_0(TranslationModelU5BU5D_t662426443* value)
	{
		___U3CmodelsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmodelsU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSLATIONMODELS_T2926535245_H
#ifndef TRANSLATIONMODEL_T4027325182_H
#define TRANSLATIONMODEL_T4027325182_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.TranslationModel
struct  TranslationModel_t4027325182  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.TranslationModel::<model_id>k__BackingField
	String_t* ___U3Cmodel_idU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.TranslationModel::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.TranslationModel::<source>k__BackingField
	String_t* ___U3CsourceU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.TranslationModel::<target>k__BackingField
	String_t* ___U3CtargetU3Ek__BackingField_3;
	// System.String IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.TranslationModel::<base_model_id>k__BackingField
	String_t* ___U3Cbase_model_idU3Ek__BackingField_4;
	// System.String IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.TranslationModel::<domain>k__BackingField
	String_t* ___U3CdomainU3Ek__BackingField_5;
	// System.Boolean IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.TranslationModel::<customizable>k__BackingField
	bool ___U3CcustomizableU3Ek__BackingField_6;
	// System.Boolean IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.TranslationModel::<default>k__BackingField
	bool ___U3CdefaultU3Ek__BackingField_7;
	// System.String IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.TranslationModel::<owner>k__BackingField
	String_t* ___U3CownerU3Ek__BackingField_8;
	// System.String IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.TranslationModel::<status>k__BackingField
	String_t* ___U3CstatusU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of_U3Cmodel_idU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TranslationModel_t4027325182, ___U3Cmodel_idU3Ek__BackingField_0)); }
	inline String_t* get_U3Cmodel_idU3Ek__BackingField_0() const { return ___U3Cmodel_idU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3Cmodel_idU3Ek__BackingField_0() { return &___U3Cmodel_idU3Ek__BackingField_0; }
	inline void set_U3Cmodel_idU3Ek__BackingField_0(String_t* value)
	{
		___U3Cmodel_idU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cmodel_idU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TranslationModel_t4027325182, ___U3CnameU3Ek__BackingField_1)); }
	inline String_t* get_U3CnameU3Ek__BackingField_1() const { return ___U3CnameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_1() { return &___U3CnameU3Ek__BackingField_1; }
	inline void set_U3CnameU3Ek__BackingField_1(String_t* value)
	{
		___U3CnameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CsourceU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(TranslationModel_t4027325182, ___U3CsourceU3Ek__BackingField_2)); }
	inline String_t* get_U3CsourceU3Ek__BackingField_2() const { return ___U3CsourceU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CsourceU3Ek__BackingField_2() { return &___U3CsourceU3Ek__BackingField_2; }
	inline void set_U3CsourceU3Ek__BackingField_2(String_t* value)
	{
		___U3CsourceU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsourceU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CtargetU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(TranslationModel_t4027325182, ___U3CtargetU3Ek__BackingField_3)); }
	inline String_t* get_U3CtargetU3Ek__BackingField_3() const { return ___U3CtargetU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CtargetU3Ek__BackingField_3() { return &___U3CtargetU3Ek__BackingField_3; }
	inline void set_U3CtargetU3Ek__BackingField_3(String_t* value)
	{
		___U3CtargetU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtargetU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3Cbase_model_idU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(TranslationModel_t4027325182, ___U3Cbase_model_idU3Ek__BackingField_4)); }
	inline String_t* get_U3Cbase_model_idU3Ek__BackingField_4() const { return ___U3Cbase_model_idU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3Cbase_model_idU3Ek__BackingField_4() { return &___U3Cbase_model_idU3Ek__BackingField_4; }
	inline void set_U3Cbase_model_idU3Ek__BackingField_4(String_t* value)
	{
		___U3Cbase_model_idU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cbase_model_idU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CdomainU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(TranslationModel_t4027325182, ___U3CdomainU3Ek__BackingField_5)); }
	inline String_t* get_U3CdomainU3Ek__BackingField_5() const { return ___U3CdomainU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CdomainU3Ek__BackingField_5() { return &___U3CdomainU3Ek__BackingField_5; }
	inline void set_U3CdomainU3Ek__BackingField_5(String_t* value)
	{
		___U3CdomainU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdomainU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CcustomizableU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(TranslationModel_t4027325182, ___U3CcustomizableU3Ek__BackingField_6)); }
	inline bool get_U3CcustomizableU3Ek__BackingField_6() const { return ___U3CcustomizableU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CcustomizableU3Ek__BackingField_6() { return &___U3CcustomizableU3Ek__BackingField_6; }
	inline void set_U3CcustomizableU3Ek__BackingField_6(bool value)
	{
		___U3CcustomizableU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CdefaultU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(TranslationModel_t4027325182, ___U3CdefaultU3Ek__BackingField_7)); }
	inline bool get_U3CdefaultU3Ek__BackingField_7() const { return ___U3CdefaultU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CdefaultU3Ek__BackingField_7() { return &___U3CdefaultU3Ek__BackingField_7; }
	inline void set_U3CdefaultU3Ek__BackingField_7(bool value)
	{
		___U3CdefaultU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CownerU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(TranslationModel_t4027325182, ___U3CownerU3Ek__BackingField_8)); }
	inline String_t* get_U3CownerU3Ek__BackingField_8() const { return ___U3CownerU3Ek__BackingField_8; }
	inline String_t** get_address_of_U3CownerU3Ek__BackingField_8() { return &___U3CownerU3Ek__BackingField_8; }
	inline void set_U3CownerU3Ek__BackingField_8(String_t* value)
	{
		___U3CownerU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CownerU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CstatusU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(TranslationModel_t4027325182, ___U3CstatusU3Ek__BackingField_9)); }
	inline String_t* get_U3CstatusU3Ek__BackingField_9() const { return ___U3CstatusU3Ek__BackingField_9; }
	inline String_t** get_address_of_U3CstatusU3Ek__BackingField_9() { return &___U3CstatusU3Ek__BackingField_9; }
	inline void set_U3CstatusU3Ek__BackingField_9(String_t* value)
	{
		___U3CstatusU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstatusU3Ek__BackingField_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSLATIONMODEL_T4027325182_H
#ifndef TRANSLATIONS_T3057738030_H
#define TRANSLATIONS_T3057738030_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.Translations
struct  Translations_t3057738030  : public RuntimeObject
{
public:
	// System.Int64 IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.Translations::<word_count>k__BackingField
	int64_t ___U3Cword_countU3Ek__BackingField_0;
	// System.Int64 IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.Translations::<character_count>k__BackingField
	int64_t ___U3Ccharacter_countU3Ek__BackingField_1;
	// IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.Translation[] IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.Translations::<translations>k__BackingField
	TranslationU5BU5D_t1094179602* ___U3CtranslationsU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3Cword_countU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Translations_t3057738030, ___U3Cword_countU3Ek__BackingField_0)); }
	inline int64_t get_U3Cword_countU3Ek__BackingField_0() const { return ___U3Cword_countU3Ek__BackingField_0; }
	inline int64_t* get_address_of_U3Cword_countU3Ek__BackingField_0() { return &___U3Cword_countU3Ek__BackingField_0; }
	inline void set_U3Cword_countU3Ek__BackingField_0(int64_t value)
	{
		___U3Cword_countU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3Ccharacter_countU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Translations_t3057738030, ___U3Ccharacter_countU3Ek__BackingField_1)); }
	inline int64_t get_U3Ccharacter_countU3Ek__BackingField_1() const { return ___U3Ccharacter_countU3Ek__BackingField_1; }
	inline int64_t* get_address_of_U3Ccharacter_countU3Ek__BackingField_1() { return &___U3Ccharacter_countU3Ek__BackingField_1; }
	inline void set_U3Ccharacter_countU3Ek__BackingField_1(int64_t value)
	{
		___U3Ccharacter_countU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CtranslationsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Translations_t3057738030, ___U3CtranslationsU3Ek__BackingField_2)); }
	inline TranslationU5BU5D_t1094179602* get_U3CtranslationsU3Ek__BackingField_2() const { return ___U3CtranslationsU3Ek__BackingField_2; }
	inline TranslationU5BU5D_t1094179602** get_address_of_U3CtranslationsU3Ek__BackingField_2() { return &___U3CtranslationsU3Ek__BackingField_2; }
	inline void set_U3CtranslationsU3Ek__BackingField_2(TranslationU5BU5D_t1094179602* value)
	{
		___U3CtranslationsU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtranslationsU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSLATIONS_T3057738030_H
#ifndef LANGUAGETRANSLATION_T3099415401_H
#define LANGUAGETRANSLATION_T3099415401_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.LanguageTranslation
struct  LanguageTranslation_t3099415401  : public RuntimeObject
{
public:

public:
};

struct LanguageTranslation_t3099415401_StaticFields
{
public:
	// FullSerializer.fsSerializer IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.LanguageTranslation::sm_Serializer
	fsSerializer_t4193731081 * ___sm_Serializer_1;

public:
	inline static int32_t get_offset_of_sm_Serializer_1() { return static_cast<int32_t>(offsetof(LanguageTranslation_t3099415401_StaticFields, ___sm_Serializer_1)); }
	inline fsSerializer_t4193731081 * get_sm_Serializer_1() const { return ___sm_Serializer_1; }
	inline fsSerializer_t4193731081 ** get_address_of_sm_Serializer_1() { return &___sm_Serializer_1; }
	inline void set_sm_Serializer_1(fsSerializer_t4193731081 * value)
	{
		___sm_Serializer_1 = value;
		Il2CppCodeGenWriteBarrier((&___sm_Serializer_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LANGUAGETRANSLATION_T3099415401_H
#ifndef LANGUAGES_T1673966175_H
#define LANGUAGES_T1673966175_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.Languages
struct  Languages_t1673966175  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.Language[] IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.Languages::<languages>k__BackingField
	LanguageU5BU5D_t1980958365* ___U3ClanguagesU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3ClanguagesU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Languages_t1673966175, ___U3ClanguagesU3Ek__BackingField_0)); }
	inline LanguageU5BU5D_t1980958365* get_U3ClanguagesU3Ek__BackingField_0() const { return ___U3ClanguagesU3Ek__BackingField_0; }
	inline LanguageU5BU5D_t1980958365** get_address_of_U3ClanguagesU3Ek__BackingField_0() { return &___U3ClanguagesU3Ek__BackingField_0; }
	inline void set_U3ClanguagesU3Ek__BackingField_0(LanguageU5BU5D_t1980958365* value)
	{
		___U3ClanguagesU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClanguagesU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LANGUAGES_T1673966175_H
#ifndef LANGUAGE_T2171379860_H
#define LANGUAGE_T2171379860_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.Language
struct  Language_t2171379860  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.Language::<language>k__BackingField
	String_t* ___U3ClanguageU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.Language::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3ClanguageU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Language_t2171379860, ___U3ClanguageU3Ek__BackingField_0)); }
	inline String_t* get_U3ClanguageU3Ek__BackingField_0() const { return ___U3ClanguageU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3ClanguageU3Ek__BackingField_0() { return &___U3ClanguageU3Ek__BackingField_0; }
	inline void set_U3ClanguageU3Ek__BackingField_0(String_t* value)
	{
		___U3ClanguageU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClanguageU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Language_t2171379860, ___U3CnameU3Ek__BackingField_1)); }
	inline String_t* get_U3CnameU3Ek__BackingField_1() const { return ___U3CnameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_1() { return &___U3CnameU3Ek__BackingField_1; }
	inline void set_U3CnameU3Ek__BackingField_1(String_t* value)
	{
		___U3CnameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LANGUAGE_T2171379860_H
#ifndef CHECKSERVICESTATUS_T3931118342_H
#define CHECKSERVICESTATUS_T3931118342_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.LanguageTranslation/CheckServiceStatus
struct  CheckServiceStatus_t3931118342  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.LanguageTranslation IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.LanguageTranslation/CheckServiceStatus::m_Service
	LanguageTranslation_t3099415401 * ___m_Service_0;
	// IBM.Watson.DeveloperCloud.Services.ServiceStatus IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.LanguageTranslation/CheckServiceStatus::m_Callback
	ServiceStatus_t1443707987 * ___m_Callback_1;

public:
	inline static int32_t get_offset_of_m_Service_0() { return static_cast<int32_t>(offsetof(CheckServiceStatus_t3931118342, ___m_Service_0)); }
	inline LanguageTranslation_t3099415401 * get_m_Service_0() const { return ___m_Service_0; }
	inline LanguageTranslation_t3099415401 ** get_address_of_m_Service_0() { return &___m_Service_0; }
	inline void set_m_Service_0(LanguageTranslation_t3099415401 * value)
	{
		___m_Service_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Service_0), value);
	}

	inline static int32_t get_offset_of_m_Callback_1() { return static_cast<int32_t>(offsetof(CheckServiceStatus_t3931118342, ___m_Callback_1)); }
	inline ServiceStatus_t1443707987 * get_m_Callback_1() const { return ___m_Callback_1; }
	inline ServiceStatus_t1443707987 ** get_address_of_m_Callback_1() { return &___m_Callback_1; }
	inline void set_m_Callback_1(ServiceStatus_t1443707987 * value)
	{
		___m_Callback_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Callback_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHECKSERVICESTATUS_T3931118342_H
#ifndef QUERYRESULT_T1852366331_H
#define QUERYRESULT_T1852366331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.QueryResult
struct  QueryResult_t1852366331  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.QueryResult::<id>k__BackingField
	String_t* ___U3CidU3Ek__BackingField_0;
	// System.Double IBM.Watson.DeveloperCloud.Services.Discovery.v1.QueryResult::<score>k__BackingField
	double ___U3CscoreU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CidU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(QueryResult_t1852366331, ___U3CidU3Ek__BackingField_0)); }
	inline String_t* get_U3CidU3Ek__BackingField_0() const { return ___U3CidU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CidU3Ek__BackingField_0() { return &___U3CidU3Ek__BackingField_0; }
	inline void set_U3CidU3Ek__BackingField_0(String_t* value)
	{
		___U3CidU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CidU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CscoreU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(QueryResult_t1852366331, ___U3CscoreU3Ek__BackingField_1)); }
	inline double get_U3CscoreU3Ek__BackingField_1() const { return ___U3CscoreU3Ek__BackingField_1; }
	inline double* get_address_of_U3CscoreU3Ek__BackingField_1() { return &___U3CscoreU3Ek__BackingField_1; }
	inline void set_U3CscoreU3Ek__BackingField_1(double value)
	{
		___U3CscoreU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUERYRESULT_T1852366331_H
#ifndef QUERYAGGREGATION_T1214220176_H
#define QUERYAGGREGATION_T1214220176_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.QueryAggregation
struct  QueryAggregation_t1214220176  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.Discovery.v1.AggregationTerm IBM.Watson.DeveloperCloud.Services.Discovery.v1.QueryAggregation::<term>k__BackingField
	AggregationTerm_t3655396846 * ___U3CtermU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CtermU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(QueryAggregation_t1214220176, ___U3CtermU3Ek__BackingField_0)); }
	inline AggregationTerm_t3655396846 * get_U3CtermU3Ek__BackingField_0() const { return ___U3CtermU3Ek__BackingField_0; }
	inline AggregationTerm_t3655396846 ** get_address_of_U3CtermU3Ek__BackingField_0() { return &___U3CtermU3Ek__BackingField_0; }
	inline void set_U3CtermU3Ek__BackingField_0(AggregationTerm_t3655396846 * value)
	{
		___U3CtermU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtermU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUERYAGGREGATION_T1214220176_H
#ifndef DOCUMENTSTATUS_T2219045527_H
#define DOCUMENTSTATUS_T2219045527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.DocumentStatus
struct  DocumentStatus_t2219045527  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.DocumentStatus::<document_id>k__BackingField
	String_t* ___U3Cdocument_idU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.DocumentStatus::<configuration_id>k__BackingField
	String_t* ___U3Cconfiguration_idU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.DocumentStatus::<created>k__BackingField
	String_t* ___U3CcreatedU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.DocumentStatus::<updated>k__BackingField
	String_t* ___U3CupdatedU3Ek__BackingField_3;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.DocumentStatus::<status>k__BackingField
	String_t* ___U3CstatusU3Ek__BackingField_4;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.DocumentStatus::<status_description>k__BackingField
	String_t* ___U3Cstatus_descriptionU3Ek__BackingField_5;
	// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Notice[] IBM.Watson.DeveloperCloud.Services.Discovery.v1.DocumentStatus::<notices>k__BackingField
	NoticeU5BU5D_t1969629099* ___U3CnoticesU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3Cdocument_idU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DocumentStatus_t2219045527, ___U3Cdocument_idU3Ek__BackingField_0)); }
	inline String_t* get_U3Cdocument_idU3Ek__BackingField_0() const { return ___U3Cdocument_idU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3Cdocument_idU3Ek__BackingField_0() { return &___U3Cdocument_idU3Ek__BackingField_0; }
	inline void set_U3Cdocument_idU3Ek__BackingField_0(String_t* value)
	{
		___U3Cdocument_idU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cdocument_idU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3Cconfiguration_idU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(DocumentStatus_t2219045527, ___U3Cconfiguration_idU3Ek__BackingField_1)); }
	inline String_t* get_U3Cconfiguration_idU3Ek__BackingField_1() const { return ___U3Cconfiguration_idU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3Cconfiguration_idU3Ek__BackingField_1() { return &___U3Cconfiguration_idU3Ek__BackingField_1; }
	inline void set_U3Cconfiguration_idU3Ek__BackingField_1(String_t* value)
	{
		___U3Cconfiguration_idU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cconfiguration_idU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CcreatedU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(DocumentStatus_t2219045527, ___U3CcreatedU3Ek__BackingField_2)); }
	inline String_t* get_U3CcreatedU3Ek__BackingField_2() const { return ___U3CcreatedU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CcreatedU3Ek__BackingField_2() { return &___U3CcreatedU3Ek__BackingField_2; }
	inline void set_U3CcreatedU3Ek__BackingField_2(String_t* value)
	{
		___U3CcreatedU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcreatedU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CupdatedU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(DocumentStatus_t2219045527, ___U3CupdatedU3Ek__BackingField_3)); }
	inline String_t* get_U3CupdatedU3Ek__BackingField_3() const { return ___U3CupdatedU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CupdatedU3Ek__BackingField_3() { return &___U3CupdatedU3Ek__BackingField_3; }
	inline void set_U3CupdatedU3Ek__BackingField_3(String_t* value)
	{
		___U3CupdatedU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CupdatedU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CstatusU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(DocumentStatus_t2219045527, ___U3CstatusU3Ek__BackingField_4)); }
	inline String_t* get_U3CstatusU3Ek__BackingField_4() const { return ___U3CstatusU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CstatusU3Ek__BackingField_4() { return &___U3CstatusU3Ek__BackingField_4; }
	inline void set_U3CstatusU3Ek__BackingField_4(String_t* value)
	{
		___U3CstatusU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstatusU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3Cstatus_descriptionU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(DocumentStatus_t2219045527, ___U3Cstatus_descriptionU3Ek__BackingField_5)); }
	inline String_t* get_U3Cstatus_descriptionU3Ek__BackingField_5() const { return ___U3Cstatus_descriptionU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3Cstatus_descriptionU3Ek__BackingField_5() { return &___U3Cstatus_descriptionU3Ek__BackingField_5; }
	inline void set_U3Cstatus_descriptionU3Ek__BackingField_5(String_t* value)
	{
		___U3Cstatus_descriptionU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cstatus_descriptionU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CnoticesU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(DocumentStatus_t2219045527, ___U3CnoticesU3Ek__BackingField_6)); }
	inline NoticeU5BU5D_t1969629099* get_U3CnoticesU3Ek__BackingField_6() const { return ___U3CnoticesU3Ek__BackingField_6; }
	inline NoticeU5BU5D_t1969629099** get_address_of_U3CnoticesU3Ek__BackingField_6() { return &___U3CnoticesU3Ek__BackingField_6; }
	inline void set_U3CnoticesU3Ek__BackingField_6(NoticeU5BU5D_t1969629099* value)
	{
		___U3CnoticesU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnoticesU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOCUMENTSTATUS_T2219045527_H
#ifndef QUERYRESPONSE_T507320049_H
#define QUERYRESPONSE_T507320049_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.QueryResponse
struct  QueryResponse_t507320049  : public RuntimeObject
{
public:
	// System.Double IBM.Watson.DeveloperCloud.Services.Discovery.v1.QueryResponse::<matching_results>k__BackingField
	double ___U3Cmatching_resultsU3Ek__BackingField_0;
	// IBM.Watson.DeveloperCloud.Services.Discovery.v1.QueryResult[] IBM.Watson.DeveloperCloud.Services.Discovery.v1.QueryResponse::<results>k__BackingField
	QueryResultU5BU5D_t189578170* ___U3CresultsU3Ek__BackingField_1;
	// IBM.Watson.DeveloperCloud.Services.Discovery.v1.QueryAggregation IBM.Watson.DeveloperCloud.Services.Discovery.v1.QueryResponse::<aggregations>k__BackingField
	QueryAggregation_t1214220176 * ___U3CaggregationsU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3Cmatching_resultsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(QueryResponse_t507320049, ___U3Cmatching_resultsU3Ek__BackingField_0)); }
	inline double get_U3Cmatching_resultsU3Ek__BackingField_0() const { return ___U3Cmatching_resultsU3Ek__BackingField_0; }
	inline double* get_address_of_U3Cmatching_resultsU3Ek__BackingField_0() { return &___U3Cmatching_resultsU3Ek__BackingField_0; }
	inline void set_U3Cmatching_resultsU3Ek__BackingField_0(double value)
	{
		___U3Cmatching_resultsU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CresultsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(QueryResponse_t507320049, ___U3CresultsU3Ek__BackingField_1)); }
	inline QueryResultU5BU5D_t189578170* get_U3CresultsU3Ek__BackingField_1() const { return ___U3CresultsU3Ek__BackingField_1; }
	inline QueryResultU5BU5D_t189578170** get_address_of_U3CresultsU3Ek__BackingField_1() { return &___U3CresultsU3Ek__BackingField_1; }
	inline void set_U3CresultsU3Ek__BackingField_1(QueryResultU5BU5D_t189578170* value)
	{
		___U3CresultsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CresultsU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CaggregationsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(QueryResponse_t507320049, ___U3CaggregationsU3Ek__BackingField_2)); }
	inline QueryAggregation_t1214220176 * get_U3CaggregationsU3Ek__BackingField_2() const { return ___U3CaggregationsU3Ek__BackingField_2; }
	inline QueryAggregation_t1214220176 ** get_address_of_U3CaggregationsU3Ek__BackingField_2() { return &___U3CaggregationsU3Ek__BackingField_2; }
	inline void set_U3CaggregationsU3Ek__BackingField_2(QueryAggregation_t1214220176 * value)
	{
		___U3CaggregationsU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CaggregationsU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUERYRESPONSE_T507320049_H
#ifndef DISCOVERYVERSION_T2429697012_H
#define DISCOVERYVERSION_T2429697012_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.DiscoveryVersion
struct  DiscoveryVersion_t2429697012  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISCOVERYVERSION_T2429697012_H
#ifndef DISCOVERY_T1478121420_H
#define DISCOVERY_T1478121420_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery
struct  Discovery_t1478121420  : public RuntimeObject
{
public:

public:
};

struct Discovery_t1478121420_StaticFields
{
public:
	// FullSerializer.fsSerializer IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery::sm_Serializer
	fsSerializer_t4193731081 * ___sm_Serializer_1;

public:
	inline static int32_t get_offset_of_sm_Serializer_1() { return static_cast<int32_t>(offsetof(Discovery_t1478121420_StaticFields, ___sm_Serializer_1)); }
	inline fsSerializer_t4193731081 * get_sm_Serializer_1() const { return ___sm_Serializer_1; }
	inline fsSerializer_t4193731081 ** get_address_of_sm_Serializer_1() { return &___sm_Serializer_1; }
	inline void set_sm_Serializer_1(fsSerializer_t4193731081 * value)
	{
		___sm_Serializer_1 = value;
		Il2CppCodeGenWriteBarrier((&___sm_Serializer_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISCOVERY_T1478121420_H
#ifndef AGGREGATIONTERM_T3655396846_H
#define AGGREGATIONTERM_T3655396846_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.AggregationTerm
struct  AggregationTerm_t3655396846  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.Discovery.v1.AggregationResult IBM.Watson.DeveloperCloud.Services.Discovery.v1.AggregationTerm::<results>k__BackingField
	AggregationResult_t3787092725 * ___U3CresultsU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CresultsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AggregationTerm_t3655396846, ___U3CresultsU3Ek__BackingField_0)); }
	inline AggregationResult_t3787092725 * get_U3CresultsU3Ek__BackingField_0() const { return ___U3CresultsU3Ek__BackingField_0; }
	inline AggregationResult_t3787092725 ** get_address_of_U3CresultsU3Ek__BackingField_0() { return &___U3CresultsU3Ek__BackingField_0; }
	inline void set_U3CresultsU3Ek__BackingField_0(AggregationResult_t3787092725 * value)
	{
		___U3CresultsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CresultsU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AGGREGATIONTERM_T3655396846_H
#ifndef AGGREGATIONRESULT_T3787092725_H
#define AGGREGATIONRESULT_T3787092725_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.AggregationResult
struct  AggregationResult_t3787092725  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.AggregationResult::<key>k__BackingField
	String_t* ___U3CkeyU3Ek__BackingField_0;
	// System.Double IBM.Watson.DeveloperCloud.Services.Discovery.v1.AggregationResult::<matching_results>k__BackingField
	double ___U3Cmatching_resultsU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CkeyU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AggregationResult_t3787092725, ___U3CkeyU3Ek__BackingField_0)); }
	inline String_t* get_U3CkeyU3Ek__BackingField_0() const { return ___U3CkeyU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CkeyU3Ek__BackingField_0() { return &___U3CkeyU3Ek__BackingField_0; }
	inline void set_U3CkeyU3Ek__BackingField_0(String_t* value)
	{
		___U3CkeyU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CkeyU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3Cmatching_resultsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AggregationResult_t3787092725, ___U3Cmatching_resultsU3Ek__BackingField_1)); }
	inline double get_U3Cmatching_resultsU3Ek__BackingField_1() const { return ___U3Cmatching_resultsU3Ek__BackingField_1; }
	inline double* get_address_of_U3Cmatching_resultsU3Ek__BackingField_1() { return &___U3Cmatching_resultsU3Ek__BackingField_1; }
	inline void set_U3Cmatching_resultsU3Ek__BackingField_1(double value)
	{
		___U3Cmatching_resultsU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AGGREGATIONRESULT_T3787092725_H
#ifndef DELETEDOCUMENTRESPONSE_T2376718965_H
#define DELETEDOCUMENTRESPONSE_T2376718965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.DeleteDocumentResponse
struct  DeleteDocumentResponse_t2376718965  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.DeleteDocumentResponse::<document_id>k__BackingField
	String_t* ___U3Cdocument_idU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.DeleteDocumentResponse::<status>k__BackingField
	String_t* ___U3CstatusU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3Cdocument_idU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DeleteDocumentResponse_t2376718965, ___U3Cdocument_idU3Ek__BackingField_0)); }
	inline String_t* get_U3Cdocument_idU3Ek__BackingField_0() const { return ___U3Cdocument_idU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3Cdocument_idU3Ek__BackingField_0() { return &___U3Cdocument_idU3Ek__BackingField_0; }
	inline void set_U3Cdocument_idU3Ek__BackingField_0(String_t* value)
	{
		___U3Cdocument_idU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cdocument_idU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CstatusU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(DeleteDocumentResponse_t2376718965, ___U3CstatusU3Ek__BackingField_1)); }
	inline String_t* get_U3CstatusU3Ek__BackingField_1() const { return ___U3CstatusU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CstatusU3Ek__BackingField_1() { return &___U3CstatusU3Ek__BackingField_1; }
	inline void set_U3CstatusU3Ek__BackingField_1(String_t* value)
	{
		___U3CstatusU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstatusU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELETEDOCUMENTRESPONSE_T2376718965_H
#ifndef DOCUMENTACCEPTED_T4162415286_H
#define DOCUMENTACCEPTED_T4162415286_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.DocumentAccepted
struct  DocumentAccepted_t4162415286  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.DocumentAccepted::<document_id>k__BackingField
	String_t* ___U3Cdocument_idU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.DocumentAccepted::<status>k__BackingField
	String_t* ___U3CstatusU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3Cdocument_idU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DocumentAccepted_t4162415286, ___U3Cdocument_idU3Ek__BackingField_0)); }
	inline String_t* get_U3Cdocument_idU3Ek__BackingField_0() const { return ___U3Cdocument_idU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3Cdocument_idU3Ek__BackingField_0() { return &___U3Cdocument_idU3Ek__BackingField_0; }
	inline void set_U3Cdocument_idU3Ek__BackingField_0(String_t* value)
	{
		___U3Cdocument_idU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cdocument_idU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CstatusU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(DocumentAccepted_t4162415286, ___U3CstatusU3Ek__BackingField_1)); }
	inline String_t* get_U3CstatusU3Ek__BackingField_1() const { return ___U3CstatusU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CstatusU3Ek__BackingField_1() { return &___U3CstatusU3Ek__BackingField_1; }
	inline void set_U3CstatusU3Ek__BackingField_1(String_t* value)
	{
		___U3CstatusU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstatusU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOCUMENTACCEPTED_T4162415286_H
#ifndef GETFIELDSRESPONSE_T2235887816_H
#define GETFIELDSRESPONSE_T2235887816_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.GetFieldsResponse
struct  GetFieldsResponse_t2235887816  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Field[] IBM.Watson.DeveloperCloud.Services.Discovery.v1.GetFieldsResponse::<fields>k__BackingField
	FieldU5BU5D_t2037695783* ___U3CfieldsU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CfieldsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(GetFieldsResponse_t2235887816, ___U3CfieldsU3Ek__BackingField_0)); }
	inline FieldU5BU5D_t2037695783* get_U3CfieldsU3Ek__BackingField_0() const { return ___U3CfieldsU3Ek__BackingField_0; }
	inline FieldU5BU5D_t2037695783** get_address_of_U3CfieldsU3Ek__BackingField_0() { return &___U3CfieldsU3Ek__BackingField_0; }
	inline void set_U3CfieldsU3Ek__BackingField_0(FieldU5BU5D_t2037695783* value)
	{
		___U3CfieldsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CfieldsU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETFIELDSRESPONSE_T2235887816_H
#ifndef FIELD_T3865657682_H
#define FIELD_T3865657682_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Field
struct  Field_t3865657682  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Field::<field>k__BackingField
	String_t* ___U3CfieldU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Field::<type>k__BackingField
	String_t* ___U3CtypeU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CfieldU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Field_t3865657682, ___U3CfieldU3Ek__BackingField_0)); }
	inline String_t* get_U3CfieldU3Ek__BackingField_0() const { return ___U3CfieldU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CfieldU3Ek__BackingField_0() { return &___U3CfieldU3Ek__BackingField_0; }
	inline void set_U3CfieldU3Ek__BackingField_0(String_t* value)
	{
		___U3CfieldU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CfieldU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CtypeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Field_t3865657682, ___U3CtypeU3Ek__BackingField_1)); }
	inline String_t* get_U3CtypeU3Ek__BackingField_1() const { return ___U3CtypeU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CtypeU3Ek__BackingField_1() { return &___U3CtypeU3Ek__BackingField_1; }
	inline void set_U3CtypeU3Ek__BackingField_1(String_t* value)
	{
		___U3CtypeU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtypeU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIELD_T3865657682_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	IntPtr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline IntPtr_t get_Zero_1() const { return ___Zero_1; }
	inline IntPtr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(IntPtr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef GETENVIRONMENTSREQUEST_T266851542_H
#define GETENVIRONMENTSREQUEST_T266851542_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/GetEnvironmentsRequest
struct  GetEnvironmentsRequest_t266851542  : public Request_t466816980
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/GetEnvironmentsRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_11;
	// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/OnGetEnvironments IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/GetEnvironmentsRequest::<Callback>k__BackingField
	OnGetEnvironments_t613121798 * ___U3CCallbackU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetEnvironmentsRequest_t266851542, ___U3CDataU3Ek__BackingField_11)); }
	inline String_t* get_U3CDataU3Ek__BackingField_11() const { return ___U3CDataU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_11() { return &___U3CDataU3Ek__BackingField_11; }
	inline void set_U3CDataU3Ek__BackingField_11(String_t* value)
	{
		___U3CDataU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GetEnvironmentsRequest_t266851542, ___U3CCallbackU3Ek__BackingField_12)); }
	inline OnGetEnvironments_t613121798 * get_U3CCallbackU3Ek__BackingField_12() const { return ___U3CCallbackU3Ek__BackingField_12; }
	inline OnGetEnvironments_t613121798 ** get_address_of_U3CCallbackU3Ek__BackingField_12() { return &___U3CCallbackU3Ek__BackingField_12; }
	inline void set_U3CCallbackU3Ek__BackingField_12(OnGetEnvironments_t613121798 * value)
	{
		___U3CCallbackU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETENVIRONMENTSREQUEST_T266851542_H
#ifndef TRANSLATEREQ_T3687846705_H
#define TRANSLATEREQ_T3687846705_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.LanguageTranslation/TranslateReq
struct  TranslateReq_t3687846705  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.LanguageTranslation/TranslateCallback IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.LanguageTranslation/TranslateReq::<Callback>k__BackingField
	TranslateCallback_t149709672 * ___U3CCallbackU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(TranslateReq_t3687846705, ___U3CCallbackU3Ek__BackingField_11)); }
	inline TranslateCallback_t149709672 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline TranslateCallback_t149709672 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(TranslateCallback_t149709672 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSLATEREQ_T3687846705_H
#ifndef GETENVIRONMENTREQUEST_T4050074335_H
#define GETENVIRONMENTREQUEST_T4050074335_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/GetEnvironmentRequest
struct  GetEnvironmentRequest_t4050074335  : public Request_t466816980
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/GetEnvironmentRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/GetEnvironmentRequest::<EnvironmentID>k__BackingField
	String_t* ___U3CEnvironmentIDU3Ek__BackingField_12;
	// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/OnGetEnvironment IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/GetEnvironmentRequest::<Callback>k__BackingField
	OnGetEnvironment_t3415272147 * ___U3CCallbackU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetEnvironmentRequest_t4050074335, ___U3CDataU3Ek__BackingField_11)); }
	inline String_t* get_U3CDataU3Ek__BackingField_11() const { return ___U3CDataU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_11() { return &___U3CDataU3Ek__BackingField_11; }
	inline void set_U3CDataU3Ek__BackingField_11(String_t* value)
	{
		___U3CDataU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CEnvironmentIDU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GetEnvironmentRequest_t4050074335, ___U3CEnvironmentIDU3Ek__BackingField_12)); }
	inline String_t* get_U3CEnvironmentIDU3Ek__BackingField_12() const { return ___U3CEnvironmentIDU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CEnvironmentIDU3Ek__BackingField_12() { return &___U3CEnvironmentIDU3Ek__BackingField_12; }
	inline void set_U3CEnvironmentIDU3Ek__BackingField_12(String_t* value)
	{
		___U3CEnvironmentIDU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEnvironmentIDU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(GetEnvironmentRequest_t4050074335, ___U3CCallbackU3Ek__BackingField_13)); }
	inline OnGetEnvironment_t3415272147 * get_U3CCallbackU3Ek__BackingField_13() const { return ___U3CCallbackU3Ek__BackingField_13; }
	inline OnGetEnvironment_t3415272147 ** get_address_of_U3CCallbackU3Ek__BackingField_13() { return &___U3CCallbackU3Ek__BackingField_13; }
	inline void set_U3CCallbackU3Ek__BackingField_13(OnGetEnvironment_t3415272147 * value)
	{
		___U3CCallbackU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETENVIRONMENTREQUEST_T4050074335_H
#ifndef DELETEENVIRONMENTREQUEST_T677322444_H
#define DELETEENVIRONMENTREQUEST_T677322444_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/DeleteEnvironmentRequest
struct  DeleteEnvironmentRequest_t677322444  : public Request_t466816980
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/DeleteEnvironmentRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/DeleteEnvironmentRequest::<EnvironmentID>k__BackingField
	String_t* ___U3CEnvironmentIDU3Ek__BackingField_12;
	// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/OnDeleteEnvironment IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/DeleteEnvironmentRequest::<Callback>k__BackingField
	OnDeleteEnvironment_t1826514308 * ___U3CCallbackU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(DeleteEnvironmentRequest_t677322444, ___U3CDataU3Ek__BackingField_11)); }
	inline String_t* get_U3CDataU3Ek__BackingField_11() const { return ___U3CDataU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_11() { return &___U3CDataU3Ek__BackingField_11; }
	inline void set_U3CDataU3Ek__BackingField_11(String_t* value)
	{
		___U3CDataU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CEnvironmentIDU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(DeleteEnvironmentRequest_t677322444, ___U3CEnvironmentIDU3Ek__BackingField_12)); }
	inline String_t* get_U3CEnvironmentIDU3Ek__BackingField_12() const { return ___U3CEnvironmentIDU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CEnvironmentIDU3Ek__BackingField_12() { return &___U3CEnvironmentIDU3Ek__BackingField_12; }
	inline void set_U3CEnvironmentIDU3Ek__BackingField_12(String_t* value)
	{
		___U3CEnvironmentIDU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEnvironmentIDU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(DeleteEnvironmentRequest_t677322444, ___U3CCallbackU3Ek__BackingField_13)); }
	inline OnDeleteEnvironment_t1826514308 * get_U3CCallbackU3Ek__BackingField_13() const { return ___U3CCallbackU3Ek__BackingField_13; }
	inline OnDeleteEnvironment_t1826514308 ** get_address_of_U3CCallbackU3Ek__BackingField_13() { return &___U3CCallbackU3Ek__BackingField_13; }
	inline void set_U3CCallbackU3Ek__BackingField_13(OnDeleteEnvironment_t1826514308 * value)
	{
		___U3CCallbackU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELETEENVIRONMENTREQUEST_T677322444_H
#ifndef ADDENVIRONMENTREQUEST_T3169458642_H
#define ADDENVIRONMENTREQUEST_T3169458642_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/AddEnvironmentRequest
struct  AddEnvironmentRequest_t3169458642  : public Request_t466816980
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/AddEnvironmentRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_11;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/AddEnvironmentRequest::<AddEnvironmentData>k__BackingField
	Dictionary_2_t309261261 * ___U3CAddEnvironmentDataU3Ek__BackingField_12;
	// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/OnAddEnvironment IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/AddEnvironmentRequest::<Callback>k__BackingField
	OnAddEnvironment_t731364804 * ___U3CCallbackU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(AddEnvironmentRequest_t3169458642, ___U3CDataU3Ek__BackingField_11)); }
	inline String_t* get_U3CDataU3Ek__BackingField_11() const { return ___U3CDataU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_11() { return &___U3CDataU3Ek__BackingField_11; }
	inline void set_U3CDataU3Ek__BackingField_11(String_t* value)
	{
		___U3CDataU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CAddEnvironmentDataU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(AddEnvironmentRequest_t3169458642, ___U3CAddEnvironmentDataU3Ek__BackingField_12)); }
	inline Dictionary_2_t309261261 * get_U3CAddEnvironmentDataU3Ek__BackingField_12() const { return ___U3CAddEnvironmentDataU3Ek__BackingField_12; }
	inline Dictionary_2_t309261261 ** get_address_of_U3CAddEnvironmentDataU3Ek__BackingField_12() { return &___U3CAddEnvironmentDataU3Ek__BackingField_12; }
	inline void set_U3CAddEnvironmentDataU3Ek__BackingField_12(Dictionary_2_t309261261 * value)
	{
		___U3CAddEnvironmentDataU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAddEnvironmentDataU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(AddEnvironmentRequest_t3169458642, ___U3CCallbackU3Ek__BackingField_13)); }
	inline OnAddEnvironment_t731364804 * get_U3CCallbackU3Ek__BackingField_13() const { return ___U3CCallbackU3Ek__BackingField_13; }
	inline OnAddEnvironment_t731364804 ** get_address_of_U3CCallbackU3Ek__BackingField_13() { return &___U3CCallbackU3Ek__BackingField_13; }
	inline void set_U3CCallbackU3Ek__BackingField_13(OnAddEnvironment_t731364804 * value)
	{
		___U3CCallbackU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDENVIRONMENTREQUEST_T3169458642_H
#ifndef CONVERTDOCUMENTREQUEST_T3520475208_H
#define CONVERTDOCUMENTREQUEST_T3520475208_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.DocumentConversion.v1.DocumentConversion/ConvertDocumentRequest
struct  ConvertDocumentRequest_t3520475208  : public Request_t466816980
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.DocumentConversion.v1.DocumentConversion/ConvertDocumentRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.DocumentConversion.v1.DocumentConversion/ConvertDocumentRequest::<ConversionTarget>k__BackingField
	String_t* ___U3CConversionTargetU3Ek__BackingField_12;
	// IBM.Watson.DeveloperCloud.Services.DocumentConversion.v1.DocumentConversion/OnConvertDocument IBM.Watson.DeveloperCloud.Services.DocumentConversion.v1.DocumentConversion/ConvertDocumentRequest::<Callback>k__BackingField
	OnConvertDocument_t1041817288 * ___U3CCallbackU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(ConvertDocumentRequest_t3520475208, ___U3CDataU3Ek__BackingField_11)); }
	inline String_t* get_U3CDataU3Ek__BackingField_11() const { return ___U3CDataU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_11() { return &___U3CDataU3Ek__BackingField_11; }
	inline void set_U3CDataU3Ek__BackingField_11(String_t* value)
	{
		___U3CDataU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CConversionTargetU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(ConvertDocumentRequest_t3520475208, ___U3CConversionTargetU3Ek__BackingField_12)); }
	inline String_t* get_U3CConversionTargetU3Ek__BackingField_12() const { return ___U3CConversionTargetU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CConversionTargetU3Ek__BackingField_12() { return &___U3CConversionTargetU3Ek__BackingField_12; }
	inline void set_U3CConversionTargetU3Ek__BackingField_12(String_t* value)
	{
		___U3CConversionTargetU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CConversionTargetU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(ConvertDocumentRequest_t3520475208, ___U3CCallbackU3Ek__BackingField_13)); }
	inline OnConvertDocument_t1041817288 * get_U3CCallbackU3Ek__BackingField_13() const { return ___U3CCallbackU3Ek__BackingField_13; }
	inline OnConvertDocument_t1041817288 ** get_address_of_U3CCallbackU3Ek__BackingField_13() { return &___U3CCallbackU3Ek__BackingField_13; }
	inline void set_U3CCallbackU3Ek__BackingField_13(OnConvertDocument_t1041817288 * value)
	{
		___U3CCallbackU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONVERTDOCUMENTREQUEST_T3520475208_H
#ifndef GETMODELSREQ_T3205312709_H
#define GETMODELSREQ_T3205312709_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.LanguageTranslation/GetModelsReq
struct  GetModelsReq_t3205312709  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.LanguageTranslation/GetModelsCallback IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.LanguageTranslation/GetModelsReq::<Callback>k__BackingField
	GetModelsCallback_t1383378020 * ___U3CCallbackU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetModelsReq_t3205312709, ___U3CCallbackU3Ek__BackingField_11)); }
	inline GetModelsCallback_t1383378020 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline GetModelsCallback_t1383378020 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(GetModelsCallback_t1383378020 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETMODELSREQ_T3205312709_H
#ifndef VOID_T1841601450_H
#define VOID_T1841601450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1841601450 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1841601450_H
#ifndef BOOLEAN_T3825574718_H
#define BOOLEAN_T3825574718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t3825574718 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t3825574718, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t3825574718_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t3825574718_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t3825574718_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T3825574718_H
#ifndef TRANSLATEREQ_T1466796057_H
#define TRANSLATEREQ_T1466796057_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.LanguageTranslator/TranslateReq
struct  TranslateReq_t1466796057  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.LanguageTranslator/TranslateCallback IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.LanguageTranslator/TranslateReq::<Callback>k__BackingField
	TranslateCallback_t3218931436 * ___U3CCallbackU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(TranslateReq_t1466796057, ___U3CCallbackU3Ek__BackingField_11)); }
	inline TranslateCallback_t3218931436 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline TranslateCallback_t3218931436 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(TranslateCallback_t3218931436 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSLATEREQ_T1466796057_H
#ifndef GETMODELSREQ_T974379077_H
#define GETMODELSREQ_T974379077_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.LanguageTranslator/GetModelsReq
struct  GetModelsReq_t974379077  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.LanguageTranslator/GetModelsCallback IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.LanguageTranslator/GetModelsReq::<Callback>k__BackingField
	GetModelsCallback_t2850854704 * ___U3CCallbackU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetModelsReq_t974379077, ___U3CCallbackU3Ek__BackingField_11)); }
	inline GetModelsCallback_t2850854704 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline GetModelsCallback_t2850854704 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(GetModelsCallback_t2850854704 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETMODELSREQ_T974379077_H
#ifndef GETLANGUAGESREQ_T3770216304_H
#define GETLANGUAGESREQ_T3770216304_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.LanguageTranslation/GetLanguagesReq
struct  GetLanguagesReq_t3770216304  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.LanguageTranslation/GetLanguagesCallback IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.LanguageTranslation/GetLanguagesReq::<Callback>k__BackingField
	GetLanguagesCallback_t833615519 * ___U3CCallbackU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetLanguagesReq_t3770216304, ___U3CCallbackU3Ek__BackingField_11)); }
	inline GetLanguagesCallback_t833615519 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline GetLanguagesCallback_t833615519 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(GetLanguagesCallback_t833615519 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETLANGUAGESREQ_T3770216304_H
#ifndef GETMODELREQ_T2857391638_H
#define GETMODELREQ_T2857391638_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.LanguageTranslation/GetModelReq
struct  GetModelReq_t2857391638  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.LanguageTranslation/GetModelCallback IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.LanguageTranslation/GetModelReq::<Callback>k__BackingField
	GetModelCallback_t3256244769 * ___U3CCallbackU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetModelReq_t2857391638, ___U3CCallbackU3Ek__BackingField_11)); }
	inline GetModelCallback_t3256244769 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline GetModelCallback_t3256244769 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(GetModelCallback_t3256244769 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETMODELREQ_T2857391638_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t1328083999* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t1328083999* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t1328083999** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t1328083999* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef IDENTIFYREQ_T3760833055_H
#define IDENTIFYREQ_T3760833055_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.LanguageTranslation/IdentifyReq
struct  IdentifyReq_t3760833055  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.LanguageTranslation/IdentifyCallback IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.LanguageTranslation/IdentifyReq::<Callback>k__BackingField
	IdentifyCallback_t942722308 * ___U3CCallbackU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(IdentifyReq_t3760833055, ___U3CCallbackU3Ek__BackingField_11)); }
	inline IdentifyCallback_t942722308 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline IdentifyCallback_t942722308 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(IdentifyCallback_t942722308 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IDENTIFYREQ_T3760833055_H
#ifndef GETCONFIGURATIONSREQUEST_T3894976669_H
#define GETCONFIGURATIONSREQUEST_T3894976669_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/GetConfigurationsRequest
struct  GetConfigurationsRequest_t3894976669  : public Request_t466816980
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/GetConfigurationsRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/GetConfigurationsRequest::<EnvironmentID>k__BackingField
	String_t* ___U3CEnvironmentIDU3Ek__BackingField_12;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/GetConfigurationsRequest::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_13;
	// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/OnGetConfigurations IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/GetConfigurationsRequest::<Callback>k__BackingField
	OnGetConfigurations_t2431280213 * ___U3CCallbackU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetConfigurationsRequest_t3894976669, ___U3CDataU3Ek__BackingField_11)); }
	inline String_t* get_U3CDataU3Ek__BackingField_11() const { return ___U3CDataU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_11() { return &___U3CDataU3Ek__BackingField_11; }
	inline void set_U3CDataU3Ek__BackingField_11(String_t* value)
	{
		___U3CDataU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CEnvironmentIDU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GetConfigurationsRequest_t3894976669, ___U3CEnvironmentIDU3Ek__BackingField_12)); }
	inline String_t* get_U3CEnvironmentIDU3Ek__BackingField_12() const { return ___U3CEnvironmentIDU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CEnvironmentIDU3Ek__BackingField_12() { return &___U3CEnvironmentIDU3Ek__BackingField_12; }
	inline void set_U3CEnvironmentIDU3Ek__BackingField_12(String_t* value)
	{
		___U3CEnvironmentIDU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEnvironmentIDU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(GetConfigurationsRequest_t3894976669, ___U3CNameU3Ek__BackingField_13)); }
	inline String_t* get_U3CNameU3Ek__BackingField_13() const { return ___U3CNameU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_13() { return &___U3CNameU3Ek__BackingField_13; }
	inline void set_U3CNameU3Ek__BackingField_13(String_t* value)
	{
		___U3CNameU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(GetConfigurationsRequest_t3894976669, ___U3CCallbackU3Ek__BackingField_14)); }
	inline OnGetConfigurations_t2431280213 * get_U3CCallbackU3Ek__BackingField_14() const { return ___U3CCallbackU3Ek__BackingField_14; }
	inline OnGetConfigurations_t2431280213 ** get_address_of_U3CCallbackU3Ek__BackingField_14() { return &___U3CCallbackU3Ek__BackingField_14; }
	inline void set_U3CCallbackU3Ek__BackingField_14(OnGetConfigurations_t2431280213 * value)
	{
		___U3CCallbackU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETCONFIGURATIONSREQUEST_T3894976669_H
#ifndef GETCOLLECTIONREQUEST_T3877445384_H
#define GETCOLLECTIONREQUEST_T3877445384_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/GetCollectionRequest
struct  GetCollectionRequest_t3877445384  : public Request_t466816980
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/GetCollectionRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/GetCollectionRequest::<EnvironmentID>k__BackingField
	String_t* ___U3CEnvironmentIDU3Ek__BackingField_12;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/GetCollectionRequest::<CollectionID>k__BackingField
	String_t* ___U3CCollectionIDU3Ek__BackingField_13;
	// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/OnGetCollection IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/GetCollectionRequest::<Callback>k__BackingField
	OnGetCollection_t85885496 * ___U3CCallbackU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetCollectionRequest_t3877445384, ___U3CDataU3Ek__BackingField_11)); }
	inline String_t* get_U3CDataU3Ek__BackingField_11() const { return ___U3CDataU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_11() { return &___U3CDataU3Ek__BackingField_11; }
	inline void set_U3CDataU3Ek__BackingField_11(String_t* value)
	{
		___U3CDataU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CEnvironmentIDU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GetCollectionRequest_t3877445384, ___U3CEnvironmentIDU3Ek__BackingField_12)); }
	inline String_t* get_U3CEnvironmentIDU3Ek__BackingField_12() const { return ___U3CEnvironmentIDU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CEnvironmentIDU3Ek__BackingField_12() { return &___U3CEnvironmentIDU3Ek__BackingField_12; }
	inline void set_U3CEnvironmentIDU3Ek__BackingField_12(String_t* value)
	{
		___U3CEnvironmentIDU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEnvironmentIDU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CCollectionIDU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(GetCollectionRequest_t3877445384, ___U3CCollectionIDU3Ek__BackingField_13)); }
	inline String_t* get_U3CCollectionIDU3Ek__BackingField_13() const { return ___U3CCollectionIDU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CCollectionIDU3Ek__BackingField_13() { return &___U3CCollectionIDU3Ek__BackingField_13; }
	inline void set_U3CCollectionIDU3Ek__BackingField_13(String_t* value)
	{
		___U3CCollectionIDU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCollectionIDU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(GetCollectionRequest_t3877445384, ___U3CCallbackU3Ek__BackingField_14)); }
	inline OnGetCollection_t85885496 * get_U3CCallbackU3Ek__BackingField_14() const { return ___U3CCallbackU3Ek__BackingField_14; }
	inline OnGetCollection_t85885496 ** get_address_of_U3CCallbackU3Ek__BackingField_14() { return &___U3CCallbackU3Ek__BackingField_14; }
	inline void set_U3CCallbackU3Ek__BackingField_14(OnGetCollection_t85885496 * value)
	{
		___U3CCallbackU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETCOLLECTIONREQUEST_T3877445384_H
#ifndef ADDDOCUMENTREQUEST_T3529010302_H
#define ADDDOCUMENTREQUEST_T3529010302_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/AddDocumentRequest
struct  AddDocumentRequest_t3529010302  : public Request_t466816980
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/AddDocumentRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/AddDocumentRequest::<EnvironmentID>k__BackingField
	String_t* ___U3CEnvironmentIDU3Ek__BackingField_12;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/AddDocumentRequest::<CollectionID>k__BackingField
	String_t* ___U3CCollectionIDU3Ek__BackingField_13;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/AddDocumentRequest::<ConfigurationID>k__BackingField
	String_t* ___U3CConfigurationIDU3Ek__BackingField_14;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/AddDocumentRequest::<Configuration>k__BackingField
	String_t* ___U3CConfigurationU3Ek__BackingField_15;
	// System.Byte[] IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/AddDocumentRequest::<ContentData>k__BackingField
	ByteU5BU5D_t3397334013* ___U3CContentDataU3Ek__BackingField_16;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/AddDocumentRequest::<ContentMimeType>k__BackingField
	String_t* ___U3CContentMimeTypeU3Ek__BackingField_17;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/AddDocumentRequest::<Metadata>k__BackingField
	String_t* ___U3CMetadataU3Ek__BackingField_18;
	// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/OnAddDocument IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/AddDocumentRequest::<Callback>k__BackingField
	OnAddDocument_t2879918214 * ___U3CCallbackU3Ek__BackingField_19;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(AddDocumentRequest_t3529010302, ___U3CDataU3Ek__BackingField_11)); }
	inline String_t* get_U3CDataU3Ek__BackingField_11() const { return ___U3CDataU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_11() { return &___U3CDataU3Ek__BackingField_11; }
	inline void set_U3CDataU3Ek__BackingField_11(String_t* value)
	{
		___U3CDataU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CEnvironmentIDU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(AddDocumentRequest_t3529010302, ___U3CEnvironmentIDU3Ek__BackingField_12)); }
	inline String_t* get_U3CEnvironmentIDU3Ek__BackingField_12() const { return ___U3CEnvironmentIDU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CEnvironmentIDU3Ek__BackingField_12() { return &___U3CEnvironmentIDU3Ek__BackingField_12; }
	inline void set_U3CEnvironmentIDU3Ek__BackingField_12(String_t* value)
	{
		___U3CEnvironmentIDU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEnvironmentIDU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CCollectionIDU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(AddDocumentRequest_t3529010302, ___U3CCollectionIDU3Ek__BackingField_13)); }
	inline String_t* get_U3CCollectionIDU3Ek__BackingField_13() const { return ___U3CCollectionIDU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CCollectionIDU3Ek__BackingField_13() { return &___U3CCollectionIDU3Ek__BackingField_13; }
	inline void set_U3CCollectionIDU3Ek__BackingField_13(String_t* value)
	{
		___U3CCollectionIDU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCollectionIDU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CConfigurationIDU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(AddDocumentRequest_t3529010302, ___U3CConfigurationIDU3Ek__BackingField_14)); }
	inline String_t* get_U3CConfigurationIDU3Ek__BackingField_14() const { return ___U3CConfigurationIDU3Ek__BackingField_14; }
	inline String_t** get_address_of_U3CConfigurationIDU3Ek__BackingField_14() { return &___U3CConfigurationIDU3Ek__BackingField_14; }
	inline void set_U3CConfigurationIDU3Ek__BackingField_14(String_t* value)
	{
		___U3CConfigurationIDU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CConfigurationIDU3Ek__BackingField_14), value);
	}

	inline static int32_t get_offset_of_U3CConfigurationU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(AddDocumentRequest_t3529010302, ___U3CConfigurationU3Ek__BackingField_15)); }
	inline String_t* get_U3CConfigurationU3Ek__BackingField_15() const { return ___U3CConfigurationU3Ek__BackingField_15; }
	inline String_t** get_address_of_U3CConfigurationU3Ek__BackingField_15() { return &___U3CConfigurationU3Ek__BackingField_15; }
	inline void set_U3CConfigurationU3Ek__BackingField_15(String_t* value)
	{
		___U3CConfigurationU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CConfigurationU3Ek__BackingField_15), value);
	}

	inline static int32_t get_offset_of_U3CContentDataU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(AddDocumentRequest_t3529010302, ___U3CContentDataU3Ek__BackingField_16)); }
	inline ByteU5BU5D_t3397334013* get_U3CContentDataU3Ek__BackingField_16() const { return ___U3CContentDataU3Ek__BackingField_16; }
	inline ByteU5BU5D_t3397334013** get_address_of_U3CContentDataU3Ek__BackingField_16() { return &___U3CContentDataU3Ek__BackingField_16; }
	inline void set_U3CContentDataU3Ek__BackingField_16(ByteU5BU5D_t3397334013* value)
	{
		___U3CContentDataU3Ek__BackingField_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CContentDataU3Ek__BackingField_16), value);
	}

	inline static int32_t get_offset_of_U3CContentMimeTypeU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(AddDocumentRequest_t3529010302, ___U3CContentMimeTypeU3Ek__BackingField_17)); }
	inline String_t* get_U3CContentMimeTypeU3Ek__BackingField_17() const { return ___U3CContentMimeTypeU3Ek__BackingField_17; }
	inline String_t** get_address_of_U3CContentMimeTypeU3Ek__BackingField_17() { return &___U3CContentMimeTypeU3Ek__BackingField_17; }
	inline void set_U3CContentMimeTypeU3Ek__BackingField_17(String_t* value)
	{
		___U3CContentMimeTypeU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CContentMimeTypeU3Ek__BackingField_17), value);
	}

	inline static int32_t get_offset_of_U3CMetadataU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(AddDocumentRequest_t3529010302, ___U3CMetadataU3Ek__BackingField_18)); }
	inline String_t* get_U3CMetadataU3Ek__BackingField_18() const { return ___U3CMetadataU3Ek__BackingField_18; }
	inline String_t** get_address_of_U3CMetadataU3Ek__BackingField_18() { return &___U3CMetadataU3Ek__BackingField_18; }
	inline void set_U3CMetadataU3Ek__BackingField_18(String_t* value)
	{
		___U3CMetadataU3Ek__BackingField_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMetadataU3Ek__BackingField_18), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(AddDocumentRequest_t3529010302, ___U3CCallbackU3Ek__BackingField_19)); }
	inline OnAddDocument_t2879918214 * get_U3CCallbackU3Ek__BackingField_19() const { return ___U3CCallbackU3Ek__BackingField_19; }
	inline OnAddDocument_t2879918214 ** get_address_of_U3CCallbackU3Ek__BackingField_19() { return &___U3CCallbackU3Ek__BackingField_19; }
	inline void set_U3CCallbackU3Ek__BackingField_19(OnAddDocument_t2879918214 * value)
	{
		___U3CCallbackU3Ek__BackingField_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDDOCUMENTREQUEST_T3529010302_H
#ifndef GETCONFIGURATIONREQUEST_T1348715152_H
#define GETCONFIGURATIONREQUEST_T1348715152_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/GetConfigurationRequest
struct  GetConfigurationRequest_t1348715152  : public Request_t466816980
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/GetConfigurationRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/GetConfigurationRequest::<EnvironmentID>k__BackingField
	String_t* ___U3CEnvironmentIDU3Ek__BackingField_12;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/GetConfigurationRequest::<ConfigurationID>k__BackingField
	String_t* ___U3CConfigurationIDU3Ek__BackingField_13;
	// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/OnGetConfiguration IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/GetConfigurationRequest::<Callback>k__BackingField
	OnGetConfiguration_t613203618 * ___U3CCallbackU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetConfigurationRequest_t1348715152, ___U3CDataU3Ek__BackingField_11)); }
	inline String_t* get_U3CDataU3Ek__BackingField_11() const { return ___U3CDataU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_11() { return &___U3CDataU3Ek__BackingField_11; }
	inline void set_U3CDataU3Ek__BackingField_11(String_t* value)
	{
		___U3CDataU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CEnvironmentIDU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GetConfigurationRequest_t1348715152, ___U3CEnvironmentIDU3Ek__BackingField_12)); }
	inline String_t* get_U3CEnvironmentIDU3Ek__BackingField_12() const { return ___U3CEnvironmentIDU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CEnvironmentIDU3Ek__BackingField_12() { return &___U3CEnvironmentIDU3Ek__BackingField_12; }
	inline void set_U3CEnvironmentIDU3Ek__BackingField_12(String_t* value)
	{
		___U3CEnvironmentIDU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEnvironmentIDU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CConfigurationIDU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(GetConfigurationRequest_t1348715152, ___U3CConfigurationIDU3Ek__BackingField_13)); }
	inline String_t* get_U3CConfigurationIDU3Ek__BackingField_13() const { return ___U3CConfigurationIDU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CConfigurationIDU3Ek__BackingField_13() { return &___U3CConfigurationIDU3Ek__BackingField_13; }
	inline void set_U3CConfigurationIDU3Ek__BackingField_13(String_t* value)
	{
		___U3CConfigurationIDU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CConfigurationIDU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(GetConfigurationRequest_t1348715152, ___U3CCallbackU3Ek__BackingField_14)); }
	inline OnGetConfiguration_t613203618 * get_U3CCallbackU3Ek__BackingField_14() const { return ___U3CCallbackU3Ek__BackingField_14; }
	inline OnGetConfiguration_t613203618 ** get_address_of_U3CCallbackU3Ek__BackingField_14() { return &___U3CCallbackU3Ek__BackingField_14; }
	inline void set_U3CCallbackU3Ek__BackingField_14(OnGetConfiguration_t613203618 * value)
	{
		___U3CCallbackU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETCONFIGURATIONREQUEST_T1348715152_H
#ifndef DELETECONFIGURATIONREQUEST_T1501683641_H
#define DELETECONFIGURATIONREQUEST_T1501683641_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/DeleteConfigurationRequest
struct  DeleteConfigurationRequest_t1501683641  : public Request_t466816980
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/DeleteConfigurationRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/DeleteConfigurationRequest::<EnvironmentID>k__BackingField
	String_t* ___U3CEnvironmentIDU3Ek__BackingField_12;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/DeleteConfigurationRequest::<ConfigurationID>k__BackingField
	String_t* ___U3CConfigurationIDU3Ek__BackingField_13;
	// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/OnDeleteConfiguration IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/DeleteConfigurationRequest::<Callback>k__BackingField
	OnDeleteConfiguration_t1574428889 * ___U3CCallbackU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(DeleteConfigurationRequest_t1501683641, ___U3CDataU3Ek__BackingField_11)); }
	inline String_t* get_U3CDataU3Ek__BackingField_11() const { return ___U3CDataU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_11() { return &___U3CDataU3Ek__BackingField_11; }
	inline void set_U3CDataU3Ek__BackingField_11(String_t* value)
	{
		___U3CDataU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CEnvironmentIDU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(DeleteConfigurationRequest_t1501683641, ___U3CEnvironmentIDU3Ek__BackingField_12)); }
	inline String_t* get_U3CEnvironmentIDU3Ek__BackingField_12() const { return ___U3CEnvironmentIDU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CEnvironmentIDU3Ek__BackingField_12() { return &___U3CEnvironmentIDU3Ek__BackingField_12; }
	inline void set_U3CEnvironmentIDU3Ek__BackingField_12(String_t* value)
	{
		___U3CEnvironmentIDU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEnvironmentIDU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CConfigurationIDU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(DeleteConfigurationRequest_t1501683641, ___U3CConfigurationIDU3Ek__BackingField_13)); }
	inline String_t* get_U3CConfigurationIDU3Ek__BackingField_13() const { return ___U3CConfigurationIDU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CConfigurationIDU3Ek__BackingField_13() { return &___U3CConfigurationIDU3Ek__BackingField_13; }
	inline void set_U3CConfigurationIDU3Ek__BackingField_13(String_t* value)
	{
		___U3CConfigurationIDU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CConfigurationIDU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(DeleteConfigurationRequest_t1501683641, ___U3CCallbackU3Ek__BackingField_14)); }
	inline OnDeleteConfiguration_t1574428889 * get_U3CCallbackU3Ek__BackingField_14() const { return ___U3CCallbackU3Ek__BackingField_14; }
	inline OnDeleteConfiguration_t1574428889 ** get_address_of_U3CCallbackU3Ek__BackingField_14() { return &___U3CCallbackU3Ek__BackingField_14; }
	inline void set_U3CCallbackU3Ek__BackingField_14(OnDeleteConfiguration_t1574428889 * value)
	{
		___U3CCallbackU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELETECONFIGURATIONREQUEST_T1501683641_H
#ifndef QUERYREQUEST_T3555347148_H
#define QUERYREQUEST_T3555347148_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/QueryRequest
struct  QueryRequest_t3555347148  : public Request_t466816980
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/QueryRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/QueryRequest::<EnvironmentID>k__BackingField
	String_t* ___U3CEnvironmentIDU3Ek__BackingField_12;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/QueryRequest::<CollectionID>k__BackingField
	String_t* ___U3CCollectionIDU3Ek__BackingField_13;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/QueryRequest::<Filter>k__BackingField
	String_t* ___U3CFilterU3Ek__BackingField_14;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/QueryRequest::<Query>k__BackingField
	String_t* ___U3CQueryU3Ek__BackingField_15;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/QueryRequest::<Aggregation>k__BackingField
	String_t* ___U3CAggregationU3Ek__BackingField_16;
	// System.Int32 IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/QueryRequest::<Count>k__BackingField
	int32_t ___U3CCountU3Ek__BackingField_17;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/QueryRequest::<Return>k__BackingField
	String_t* ___U3CReturnU3Ek__BackingField_18;
	// System.Int32 IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/QueryRequest::<Offset>k__BackingField
	int32_t ___U3COffsetU3Ek__BackingField_19;
	// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/OnQuery IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/QueryRequest::<Callback>k__BackingField
	OnQuery_t887364716 * ___U3CCallbackU3Ek__BackingField_20;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(QueryRequest_t3555347148, ___U3CDataU3Ek__BackingField_11)); }
	inline String_t* get_U3CDataU3Ek__BackingField_11() const { return ___U3CDataU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_11() { return &___U3CDataU3Ek__BackingField_11; }
	inline void set_U3CDataU3Ek__BackingField_11(String_t* value)
	{
		___U3CDataU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CEnvironmentIDU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(QueryRequest_t3555347148, ___U3CEnvironmentIDU3Ek__BackingField_12)); }
	inline String_t* get_U3CEnvironmentIDU3Ek__BackingField_12() const { return ___U3CEnvironmentIDU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CEnvironmentIDU3Ek__BackingField_12() { return &___U3CEnvironmentIDU3Ek__BackingField_12; }
	inline void set_U3CEnvironmentIDU3Ek__BackingField_12(String_t* value)
	{
		___U3CEnvironmentIDU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEnvironmentIDU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CCollectionIDU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(QueryRequest_t3555347148, ___U3CCollectionIDU3Ek__BackingField_13)); }
	inline String_t* get_U3CCollectionIDU3Ek__BackingField_13() const { return ___U3CCollectionIDU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CCollectionIDU3Ek__BackingField_13() { return &___U3CCollectionIDU3Ek__BackingField_13; }
	inline void set_U3CCollectionIDU3Ek__BackingField_13(String_t* value)
	{
		___U3CCollectionIDU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCollectionIDU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CFilterU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(QueryRequest_t3555347148, ___U3CFilterU3Ek__BackingField_14)); }
	inline String_t* get_U3CFilterU3Ek__BackingField_14() const { return ___U3CFilterU3Ek__BackingField_14; }
	inline String_t** get_address_of_U3CFilterU3Ek__BackingField_14() { return &___U3CFilterU3Ek__BackingField_14; }
	inline void set_U3CFilterU3Ek__BackingField_14(String_t* value)
	{
		___U3CFilterU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFilterU3Ek__BackingField_14), value);
	}

	inline static int32_t get_offset_of_U3CQueryU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(QueryRequest_t3555347148, ___U3CQueryU3Ek__BackingField_15)); }
	inline String_t* get_U3CQueryU3Ek__BackingField_15() const { return ___U3CQueryU3Ek__BackingField_15; }
	inline String_t** get_address_of_U3CQueryU3Ek__BackingField_15() { return &___U3CQueryU3Ek__BackingField_15; }
	inline void set_U3CQueryU3Ek__BackingField_15(String_t* value)
	{
		___U3CQueryU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CQueryU3Ek__BackingField_15), value);
	}

	inline static int32_t get_offset_of_U3CAggregationU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(QueryRequest_t3555347148, ___U3CAggregationU3Ek__BackingField_16)); }
	inline String_t* get_U3CAggregationU3Ek__BackingField_16() const { return ___U3CAggregationU3Ek__BackingField_16; }
	inline String_t** get_address_of_U3CAggregationU3Ek__BackingField_16() { return &___U3CAggregationU3Ek__BackingField_16; }
	inline void set_U3CAggregationU3Ek__BackingField_16(String_t* value)
	{
		___U3CAggregationU3Ek__BackingField_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAggregationU3Ek__BackingField_16), value);
	}

	inline static int32_t get_offset_of_U3CCountU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(QueryRequest_t3555347148, ___U3CCountU3Ek__BackingField_17)); }
	inline int32_t get_U3CCountU3Ek__BackingField_17() const { return ___U3CCountU3Ek__BackingField_17; }
	inline int32_t* get_address_of_U3CCountU3Ek__BackingField_17() { return &___U3CCountU3Ek__BackingField_17; }
	inline void set_U3CCountU3Ek__BackingField_17(int32_t value)
	{
		___U3CCountU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3CReturnU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(QueryRequest_t3555347148, ___U3CReturnU3Ek__BackingField_18)); }
	inline String_t* get_U3CReturnU3Ek__BackingField_18() const { return ___U3CReturnU3Ek__BackingField_18; }
	inline String_t** get_address_of_U3CReturnU3Ek__BackingField_18() { return &___U3CReturnU3Ek__BackingField_18; }
	inline void set_U3CReturnU3Ek__BackingField_18(String_t* value)
	{
		___U3CReturnU3Ek__BackingField_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CReturnU3Ek__BackingField_18), value);
	}

	inline static int32_t get_offset_of_U3COffsetU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(QueryRequest_t3555347148, ___U3COffsetU3Ek__BackingField_19)); }
	inline int32_t get_U3COffsetU3Ek__BackingField_19() const { return ___U3COffsetU3Ek__BackingField_19; }
	inline int32_t* get_address_of_U3COffsetU3Ek__BackingField_19() { return &___U3COffsetU3Ek__BackingField_19; }
	inline void set_U3COffsetU3Ek__BackingField_19(int32_t value)
	{
		___U3COffsetU3Ek__BackingField_19 = value;
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(QueryRequest_t3555347148, ___U3CCallbackU3Ek__BackingField_20)); }
	inline OnQuery_t887364716 * get_U3CCallbackU3Ek__BackingField_20() const { return ___U3CCallbackU3Ek__BackingField_20; }
	inline OnQuery_t887364716 ** get_address_of_U3CCallbackU3Ek__BackingField_20() { return &___U3CCallbackU3Ek__BackingField_20; }
	inline void set_U3CCallbackU3Ek__BackingField_20(OnQuery_t887364716 * value)
	{
		___U3CCallbackU3Ek__BackingField_20 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUERYREQUEST_T3555347148_H
#ifndef DELETEDOCUMENTREQUEST_T2235071592_H
#define DELETEDOCUMENTREQUEST_T2235071592_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/DeleteDocumentRequest
struct  DeleteDocumentRequest_t2235071592  : public Request_t466816980
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/DeleteDocumentRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/DeleteDocumentRequest::<EnvironmentID>k__BackingField
	String_t* ___U3CEnvironmentIDU3Ek__BackingField_12;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/DeleteDocumentRequest::<CollectionID>k__BackingField
	String_t* ___U3CCollectionIDU3Ek__BackingField_13;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/DeleteDocumentRequest::<DocumentID>k__BackingField
	String_t* ___U3CDocumentIDU3Ek__BackingField_14;
	// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/OnDeleteDocument IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/DeleteDocumentRequest::<Callback>k__BackingField
	OnDeleteDocument_t3959738954 * ___U3CCallbackU3Ek__BackingField_15;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(DeleteDocumentRequest_t2235071592, ___U3CDataU3Ek__BackingField_11)); }
	inline String_t* get_U3CDataU3Ek__BackingField_11() const { return ___U3CDataU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_11() { return &___U3CDataU3Ek__BackingField_11; }
	inline void set_U3CDataU3Ek__BackingField_11(String_t* value)
	{
		___U3CDataU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CEnvironmentIDU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(DeleteDocumentRequest_t2235071592, ___U3CEnvironmentIDU3Ek__BackingField_12)); }
	inline String_t* get_U3CEnvironmentIDU3Ek__BackingField_12() const { return ___U3CEnvironmentIDU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CEnvironmentIDU3Ek__BackingField_12() { return &___U3CEnvironmentIDU3Ek__BackingField_12; }
	inline void set_U3CEnvironmentIDU3Ek__BackingField_12(String_t* value)
	{
		___U3CEnvironmentIDU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEnvironmentIDU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CCollectionIDU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(DeleteDocumentRequest_t2235071592, ___U3CCollectionIDU3Ek__BackingField_13)); }
	inline String_t* get_U3CCollectionIDU3Ek__BackingField_13() const { return ___U3CCollectionIDU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CCollectionIDU3Ek__BackingField_13() { return &___U3CCollectionIDU3Ek__BackingField_13; }
	inline void set_U3CCollectionIDU3Ek__BackingField_13(String_t* value)
	{
		___U3CCollectionIDU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCollectionIDU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CDocumentIDU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(DeleteDocumentRequest_t2235071592, ___U3CDocumentIDU3Ek__BackingField_14)); }
	inline String_t* get_U3CDocumentIDU3Ek__BackingField_14() const { return ___U3CDocumentIDU3Ek__BackingField_14; }
	inline String_t** get_address_of_U3CDocumentIDU3Ek__BackingField_14() { return &___U3CDocumentIDU3Ek__BackingField_14; }
	inline void set_U3CDocumentIDU3Ek__BackingField_14(String_t* value)
	{
		___U3CDocumentIDU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDocumentIDU3Ek__BackingField_14), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(DeleteDocumentRequest_t2235071592, ___U3CCallbackU3Ek__BackingField_15)); }
	inline OnDeleteDocument_t3959738954 * get_U3CCallbackU3Ek__BackingField_15() const { return ___U3CCallbackU3Ek__BackingField_15; }
	inline OnDeleteDocument_t3959738954 ** get_address_of_U3CCallbackU3Ek__BackingField_15() { return &___U3CCallbackU3Ek__BackingField_15; }
	inline void set_U3CCallbackU3Ek__BackingField_15(OnDeleteDocument_t3959738954 * value)
	{
		___U3CCallbackU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELETEDOCUMENTREQUEST_T2235071592_H
#ifndef GETFIELDSREQUEST_T991558571_H
#define GETFIELDSREQUEST_T991558571_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/GetFieldsRequest
struct  GetFieldsRequest_t991558571  : public Request_t466816980
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/GetFieldsRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/GetFieldsRequest::<EnvironmentID>k__BackingField
	String_t* ___U3CEnvironmentIDU3Ek__BackingField_12;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/GetFieldsRequest::<CollectionID>k__BackingField
	String_t* ___U3CCollectionIDU3Ek__BackingField_13;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/GetFieldsRequest::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_14;
	// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/OnGetFields IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/GetFieldsRequest::<Callback>k__BackingField
	OnGetFields_t307794883 * ___U3CCallbackU3Ek__BackingField_15;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetFieldsRequest_t991558571, ___U3CDataU3Ek__BackingField_11)); }
	inline String_t* get_U3CDataU3Ek__BackingField_11() const { return ___U3CDataU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_11() { return &___U3CDataU3Ek__BackingField_11; }
	inline void set_U3CDataU3Ek__BackingField_11(String_t* value)
	{
		___U3CDataU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CEnvironmentIDU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GetFieldsRequest_t991558571, ___U3CEnvironmentIDU3Ek__BackingField_12)); }
	inline String_t* get_U3CEnvironmentIDU3Ek__BackingField_12() const { return ___U3CEnvironmentIDU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CEnvironmentIDU3Ek__BackingField_12() { return &___U3CEnvironmentIDU3Ek__BackingField_12; }
	inline void set_U3CEnvironmentIDU3Ek__BackingField_12(String_t* value)
	{
		___U3CEnvironmentIDU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEnvironmentIDU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CCollectionIDU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(GetFieldsRequest_t991558571, ___U3CCollectionIDU3Ek__BackingField_13)); }
	inline String_t* get_U3CCollectionIDU3Ek__BackingField_13() const { return ___U3CCollectionIDU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CCollectionIDU3Ek__BackingField_13() { return &___U3CCollectionIDU3Ek__BackingField_13; }
	inline void set_U3CCollectionIDU3Ek__BackingField_13(String_t* value)
	{
		___U3CCollectionIDU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCollectionIDU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(GetFieldsRequest_t991558571, ___U3CNameU3Ek__BackingField_14)); }
	inline String_t* get_U3CNameU3Ek__BackingField_14() const { return ___U3CNameU3Ek__BackingField_14; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_14() { return &___U3CNameU3Ek__BackingField_14; }
	inline void set_U3CNameU3Ek__BackingField_14(String_t* value)
	{
		___U3CNameU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_14), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(GetFieldsRequest_t991558571, ___U3CCallbackU3Ek__BackingField_15)); }
	inline OnGetFields_t307794883 * get_U3CCallbackU3Ek__BackingField_15() const { return ___U3CCallbackU3Ek__BackingField_15; }
	inline OnGetFields_t307794883 ** get_address_of_U3CCallbackU3Ek__BackingField_15() { return &___U3CCallbackU3Ek__BackingField_15; }
	inline void set_U3CCallbackU3Ek__BackingField_15(OnGetFields_t307794883 * value)
	{
		___U3CCallbackU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETFIELDSREQUEST_T991558571_H
#ifndef UPDATEDOCUMENTREQUEST_T2438806938_H
#define UPDATEDOCUMENTREQUEST_T2438806938_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/UpdateDocumentRequest
struct  UpdateDocumentRequest_t2438806938  : public Request_t466816980
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/UpdateDocumentRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/UpdateDocumentRequest::<EnvironmentID>k__BackingField
	String_t* ___U3CEnvironmentIDU3Ek__BackingField_12;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/UpdateDocumentRequest::<CollectionID>k__BackingField
	String_t* ___U3CCollectionIDU3Ek__BackingField_13;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/UpdateDocumentRequest::<DocumentID>k__BackingField
	String_t* ___U3CDocumentIDU3Ek__BackingField_14;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/UpdateDocumentRequest::<ConfigurationID>k__BackingField
	String_t* ___U3CConfigurationIDU3Ek__BackingField_15;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/UpdateDocumentRequest::<Configuration>k__BackingField
	String_t* ___U3CConfigurationU3Ek__BackingField_16;
	// System.Byte[] IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/UpdateDocumentRequest::<ContentData>k__BackingField
	ByteU5BU5D_t3397334013* ___U3CContentDataU3Ek__BackingField_17;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/UpdateDocumentRequest::<ContentMimeType>k__BackingField
	String_t* ___U3CContentMimeTypeU3Ek__BackingField_18;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/UpdateDocumentRequest::<Metadata>k__BackingField
	String_t* ___U3CMetadataU3Ek__BackingField_19;
	// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/OnUpdateDocument IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/UpdateDocumentRequest::<Callback>k__BackingField
	OnUpdateDocument_t4020936920 * ___U3CCallbackU3Ek__BackingField_20;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(UpdateDocumentRequest_t2438806938, ___U3CDataU3Ek__BackingField_11)); }
	inline String_t* get_U3CDataU3Ek__BackingField_11() const { return ___U3CDataU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_11() { return &___U3CDataU3Ek__BackingField_11; }
	inline void set_U3CDataU3Ek__BackingField_11(String_t* value)
	{
		___U3CDataU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CEnvironmentIDU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(UpdateDocumentRequest_t2438806938, ___U3CEnvironmentIDU3Ek__BackingField_12)); }
	inline String_t* get_U3CEnvironmentIDU3Ek__BackingField_12() const { return ___U3CEnvironmentIDU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CEnvironmentIDU3Ek__BackingField_12() { return &___U3CEnvironmentIDU3Ek__BackingField_12; }
	inline void set_U3CEnvironmentIDU3Ek__BackingField_12(String_t* value)
	{
		___U3CEnvironmentIDU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEnvironmentIDU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CCollectionIDU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(UpdateDocumentRequest_t2438806938, ___U3CCollectionIDU3Ek__BackingField_13)); }
	inline String_t* get_U3CCollectionIDU3Ek__BackingField_13() const { return ___U3CCollectionIDU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CCollectionIDU3Ek__BackingField_13() { return &___U3CCollectionIDU3Ek__BackingField_13; }
	inline void set_U3CCollectionIDU3Ek__BackingField_13(String_t* value)
	{
		___U3CCollectionIDU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCollectionIDU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CDocumentIDU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(UpdateDocumentRequest_t2438806938, ___U3CDocumentIDU3Ek__BackingField_14)); }
	inline String_t* get_U3CDocumentIDU3Ek__BackingField_14() const { return ___U3CDocumentIDU3Ek__BackingField_14; }
	inline String_t** get_address_of_U3CDocumentIDU3Ek__BackingField_14() { return &___U3CDocumentIDU3Ek__BackingField_14; }
	inline void set_U3CDocumentIDU3Ek__BackingField_14(String_t* value)
	{
		___U3CDocumentIDU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDocumentIDU3Ek__BackingField_14), value);
	}

	inline static int32_t get_offset_of_U3CConfigurationIDU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(UpdateDocumentRequest_t2438806938, ___U3CConfigurationIDU3Ek__BackingField_15)); }
	inline String_t* get_U3CConfigurationIDU3Ek__BackingField_15() const { return ___U3CConfigurationIDU3Ek__BackingField_15; }
	inline String_t** get_address_of_U3CConfigurationIDU3Ek__BackingField_15() { return &___U3CConfigurationIDU3Ek__BackingField_15; }
	inline void set_U3CConfigurationIDU3Ek__BackingField_15(String_t* value)
	{
		___U3CConfigurationIDU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CConfigurationIDU3Ek__BackingField_15), value);
	}

	inline static int32_t get_offset_of_U3CConfigurationU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(UpdateDocumentRequest_t2438806938, ___U3CConfigurationU3Ek__BackingField_16)); }
	inline String_t* get_U3CConfigurationU3Ek__BackingField_16() const { return ___U3CConfigurationU3Ek__BackingField_16; }
	inline String_t** get_address_of_U3CConfigurationU3Ek__BackingField_16() { return &___U3CConfigurationU3Ek__BackingField_16; }
	inline void set_U3CConfigurationU3Ek__BackingField_16(String_t* value)
	{
		___U3CConfigurationU3Ek__BackingField_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CConfigurationU3Ek__BackingField_16), value);
	}

	inline static int32_t get_offset_of_U3CContentDataU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(UpdateDocumentRequest_t2438806938, ___U3CContentDataU3Ek__BackingField_17)); }
	inline ByteU5BU5D_t3397334013* get_U3CContentDataU3Ek__BackingField_17() const { return ___U3CContentDataU3Ek__BackingField_17; }
	inline ByteU5BU5D_t3397334013** get_address_of_U3CContentDataU3Ek__BackingField_17() { return &___U3CContentDataU3Ek__BackingField_17; }
	inline void set_U3CContentDataU3Ek__BackingField_17(ByteU5BU5D_t3397334013* value)
	{
		___U3CContentDataU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CContentDataU3Ek__BackingField_17), value);
	}

	inline static int32_t get_offset_of_U3CContentMimeTypeU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(UpdateDocumentRequest_t2438806938, ___U3CContentMimeTypeU3Ek__BackingField_18)); }
	inline String_t* get_U3CContentMimeTypeU3Ek__BackingField_18() const { return ___U3CContentMimeTypeU3Ek__BackingField_18; }
	inline String_t** get_address_of_U3CContentMimeTypeU3Ek__BackingField_18() { return &___U3CContentMimeTypeU3Ek__BackingField_18; }
	inline void set_U3CContentMimeTypeU3Ek__BackingField_18(String_t* value)
	{
		___U3CContentMimeTypeU3Ek__BackingField_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CContentMimeTypeU3Ek__BackingField_18), value);
	}

	inline static int32_t get_offset_of_U3CMetadataU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(UpdateDocumentRequest_t2438806938, ___U3CMetadataU3Ek__BackingField_19)); }
	inline String_t* get_U3CMetadataU3Ek__BackingField_19() const { return ___U3CMetadataU3Ek__BackingField_19; }
	inline String_t** get_address_of_U3CMetadataU3Ek__BackingField_19() { return &___U3CMetadataU3Ek__BackingField_19; }
	inline void set_U3CMetadataU3Ek__BackingField_19(String_t* value)
	{
		___U3CMetadataU3Ek__BackingField_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMetadataU3Ek__BackingField_19), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(UpdateDocumentRequest_t2438806938, ___U3CCallbackU3Ek__BackingField_20)); }
	inline OnUpdateDocument_t4020936920 * get_U3CCallbackU3Ek__BackingField_20() const { return ___U3CCallbackU3Ek__BackingField_20; }
	inline OnUpdateDocument_t4020936920 ** get_address_of_U3CCallbackU3Ek__BackingField_20() { return &___U3CCallbackU3Ek__BackingField_20; }
	inline void set_U3CCallbackU3Ek__BackingField_20(OnUpdateDocument_t4020936920 * value)
	{
		___U3CCallbackU3Ek__BackingField_20 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATEDOCUMENTREQUEST_T2438806938_H
#ifndef DELETECOLLECTIONREQUEST_T1933527959_H
#define DELETECOLLECTIONREQUEST_T1933527959_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/DeleteCollectionRequest
struct  DeleteCollectionRequest_t1933527959  : public Request_t466816980
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/DeleteCollectionRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/DeleteCollectionRequest::<EnvironmentID>k__BackingField
	String_t* ___U3CEnvironmentIDU3Ek__BackingField_12;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/DeleteCollectionRequest::<CollectionID>k__BackingField
	String_t* ___U3CCollectionIDU3Ek__BackingField_13;
	// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/OnDeleteCollection IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/DeleteCollectionRequest::<Callback>k__BackingField
	OnDeleteCollection_t603439291 * ___U3CCallbackU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(DeleteCollectionRequest_t1933527959, ___U3CDataU3Ek__BackingField_11)); }
	inline String_t* get_U3CDataU3Ek__BackingField_11() const { return ___U3CDataU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_11() { return &___U3CDataU3Ek__BackingField_11; }
	inline void set_U3CDataU3Ek__BackingField_11(String_t* value)
	{
		___U3CDataU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CEnvironmentIDU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(DeleteCollectionRequest_t1933527959, ___U3CEnvironmentIDU3Ek__BackingField_12)); }
	inline String_t* get_U3CEnvironmentIDU3Ek__BackingField_12() const { return ___U3CEnvironmentIDU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CEnvironmentIDU3Ek__BackingField_12() { return &___U3CEnvironmentIDU3Ek__BackingField_12; }
	inline void set_U3CEnvironmentIDU3Ek__BackingField_12(String_t* value)
	{
		___U3CEnvironmentIDU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEnvironmentIDU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CCollectionIDU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(DeleteCollectionRequest_t1933527959, ___U3CCollectionIDU3Ek__BackingField_13)); }
	inline String_t* get_U3CCollectionIDU3Ek__BackingField_13() const { return ___U3CCollectionIDU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CCollectionIDU3Ek__BackingField_13() { return &___U3CCollectionIDU3Ek__BackingField_13; }
	inline void set_U3CCollectionIDU3Ek__BackingField_13(String_t* value)
	{
		___U3CCollectionIDU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCollectionIDU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(DeleteCollectionRequest_t1933527959, ___U3CCallbackU3Ek__BackingField_14)); }
	inline OnDeleteCollection_t603439291 * get_U3CCallbackU3Ek__BackingField_14() const { return ___U3CCallbackU3Ek__BackingField_14; }
	inline OnDeleteCollection_t603439291 ** get_address_of_U3CCallbackU3Ek__BackingField_14() { return &___U3CCallbackU3Ek__BackingField_14; }
	inline void set_U3CCallbackU3Ek__BackingField_14(OnDeleteCollection_t603439291 * value)
	{
		___U3CCallbackU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELETECOLLECTIONREQUEST_T1933527959_H
#ifndef GETCOLLECTIONSREQUEST_T324656879_H
#define GETCOLLECTIONSREQUEST_T324656879_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/GetCollectionsRequest
struct  GetCollectionsRequest_t324656879  : public Request_t466816980
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/GetCollectionsRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/GetCollectionsRequest::<EnvironmentID>k__BackingField
	String_t* ___U3CEnvironmentIDU3Ek__BackingField_12;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/GetCollectionsRequest::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_13;
	// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/OnGetCollections IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/GetCollectionsRequest::<Callback>k__BackingField
	OnGetCollections_t2546296547 * ___U3CCallbackU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetCollectionsRequest_t324656879, ___U3CDataU3Ek__BackingField_11)); }
	inline String_t* get_U3CDataU3Ek__BackingField_11() const { return ___U3CDataU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_11() { return &___U3CDataU3Ek__BackingField_11; }
	inline void set_U3CDataU3Ek__BackingField_11(String_t* value)
	{
		___U3CDataU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CEnvironmentIDU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GetCollectionsRequest_t324656879, ___U3CEnvironmentIDU3Ek__BackingField_12)); }
	inline String_t* get_U3CEnvironmentIDU3Ek__BackingField_12() const { return ___U3CEnvironmentIDU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CEnvironmentIDU3Ek__BackingField_12() { return &___U3CEnvironmentIDU3Ek__BackingField_12; }
	inline void set_U3CEnvironmentIDU3Ek__BackingField_12(String_t* value)
	{
		___U3CEnvironmentIDU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEnvironmentIDU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(GetCollectionsRequest_t324656879, ___U3CNameU3Ek__BackingField_13)); }
	inline String_t* get_U3CNameU3Ek__BackingField_13() const { return ___U3CNameU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_13() { return &___U3CNameU3Ek__BackingField_13; }
	inline void set_U3CNameU3Ek__BackingField_13(String_t* value)
	{
		___U3CNameU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(GetCollectionsRequest_t324656879, ___U3CCallbackU3Ek__BackingField_14)); }
	inline OnGetCollections_t2546296547 * get_U3CCallbackU3Ek__BackingField_14() const { return ___U3CCallbackU3Ek__BackingField_14; }
	inline OnGetCollections_t2546296547 ** get_address_of_U3CCallbackU3Ek__BackingField_14() { return &___U3CCallbackU3Ek__BackingField_14; }
	inline void set_U3CCallbackU3Ek__BackingField_14(OnGetCollections_t2546296547 * value)
	{
		___U3CCallbackU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETCOLLECTIONSREQUEST_T324656879_H
#ifndef ADDCONFIGURATIONREQUEST_T972289441_H
#define ADDCONFIGURATIONREQUEST_T972289441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/AddConfigurationRequest
struct  AddConfigurationRequest_t972289441  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/OnAddConfiguration IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/AddConfigurationRequest::<Callback>k__BackingField
	OnAddConfiguration_t2751378613 * ___U3CCallbackU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/AddConfigurationRequest::<EnvironmentID>k__BackingField
	String_t* ___U3CEnvironmentIDU3Ek__BackingField_12;
	// System.Byte[] IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/AddConfigurationRequest::<ConfigurationJsonData>k__BackingField
	ByteU5BU5D_t3397334013* ___U3CConfigurationJsonDataU3Ek__BackingField_13;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/AddConfigurationRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(AddConfigurationRequest_t972289441, ___U3CCallbackU3Ek__BackingField_11)); }
	inline OnAddConfiguration_t2751378613 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline OnAddConfiguration_t2751378613 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(OnAddConfiguration_t2751378613 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CEnvironmentIDU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(AddConfigurationRequest_t972289441, ___U3CEnvironmentIDU3Ek__BackingField_12)); }
	inline String_t* get_U3CEnvironmentIDU3Ek__BackingField_12() const { return ___U3CEnvironmentIDU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CEnvironmentIDU3Ek__BackingField_12() { return &___U3CEnvironmentIDU3Ek__BackingField_12; }
	inline void set_U3CEnvironmentIDU3Ek__BackingField_12(String_t* value)
	{
		___U3CEnvironmentIDU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEnvironmentIDU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CConfigurationJsonDataU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(AddConfigurationRequest_t972289441, ___U3CConfigurationJsonDataU3Ek__BackingField_13)); }
	inline ByteU5BU5D_t3397334013* get_U3CConfigurationJsonDataU3Ek__BackingField_13() const { return ___U3CConfigurationJsonDataU3Ek__BackingField_13; }
	inline ByteU5BU5D_t3397334013** get_address_of_U3CConfigurationJsonDataU3Ek__BackingField_13() { return &___U3CConfigurationJsonDataU3Ek__BackingField_13; }
	inline void set_U3CConfigurationJsonDataU3Ek__BackingField_13(ByteU5BU5D_t3397334013* value)
	{
		___U3CConfigurationJsonDataU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CConfigurationJsonDataU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(AddConfigurationRequest_t972289441, ___U3CDataU3Ek__BackingField_14)); }
	inline String_t* get_U3CDataU3Ek__BackingField_14() const { return ___U3CDataU3Ek__BackingField_14; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_14() { return &___U3CDataU3Ek__BackingField_14; }
	inline void set_U3CDataU3Ek__BackingField_14(String_t* value)
	{
		___U3CDataU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDCONFIGURATIONREQUEST_T972289441_H
#ifndef PREVIEWCONFIGURATIONREQUEST_T1392293340_H
#define PREVIEWCONFIGURATIONREQUEST_T1392293340_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/PreviewConfigurationRequest
struct  PreviewConfigurationRequest_t1392293340  : public Request_t466816980
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/PreviewConfigurationRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/PreviewConfigurationRequest::<EnvironmentID>k__BackingField
	String_t* ___U3CEnvironmentIDU3Ek__BackingField_12;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/PreviewConfigurationRequest::<ConfigurationID>k__BackingField
	String_t* ___U3CConfigurationIDU3Ek__BackingField_13;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/PreviewConfigurationRequest::<ConfigurationFilePath>k__BackingField
	String_t* ___U3CConfigurationFilePathU3Ek__BackingField_14;
	// System.Byte[] IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/PreviewConfigurationRequest::<ContentData>k__BackingField
	ByteU5BU5D_t3397334013* ___U3CContentDataU3Ek__BackingField_15;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/PreviewConfigurationRequest::<Metadata>k__BackingField
	String_t* ___U3CMetadataU3Ek__BackingField_16;
	// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/OnPreviewConfiguration IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/PreviewConfigurationRequest::<Callback>k__BackingField
	OnPreviewConfiguration_t1311234390 * ___U3CCallbackU3Ek__BackingField_17;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(PreviewConfigurationRequest_t1392293340, ___U3CDataU3Ek__BackingField_11)); }
	inline String_t* get_U3CDataU3Ek__BackingField_11() const { return ___U3CDataU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_11() { return &___U3CDataU3Ek__BackingField_11; }
	inline void set_U3CDataU3Ek__BackingField_11(String_t* value)
	{
		___U3CDataU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CEnvironmentIDU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(PreviewConfigurationRequest_t1392293340, ___U3CEnvironmentIDU3Ek__BackingField_12)); }
	inline String_t* get_U3CEnvironmentIDU3Ek__BackingField_12() const { return ___U3CEnvironmentIDU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CEnvironmentIDU3Ek__BackingField_12() { return &___U3CEnvironmentIDU3Ek__BackingField_12; }
	inline void set_U3CEnvironmentIDU3Ek__BackingField_12(String_t* value)
	{
		___U3CEnvironmentIDU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEnvironmentIDU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CConfigurationIDU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(PreviewConfigurationRequest_t1392293340, ___U3CConfigurationIDU3Ek__BackingField_13)); }
	inline String_t* get_U3CConfigurationIDU3Ek__BackingField_13() const { return ___U3CConfigurationIDU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CConfigurationIDU3Ek__BackingField_13() { return &___U3CConfigurationIDU3Ek__BackingField_13; }
	inline void set_U3CConfigurationIDU3Ek__BackingField_13(String_t* value)
	{
		___U3CConfigurationIDU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CConfigurationIDU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CConfigurationFilePathU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(PreviewConfigurationRequest_t1392293340, ___U3CConfigurationFilePathU3Ek__BackingField_14)); }
	inline String_t* get_U3CConfigurationFilePathU3Ek__BackingField_14() const { return ___U3CConfigurationFilePathU3Ek__BackingField_14; }
	inline String_t** get_address_of_U3CConfigurationFilePathU3Ek__BackingField_14() { return &___U3CConfigurationFilePathU3Ek__BackingField_14; }
	inline void set_U3CConfigurationFilePathU3Ek__BackingField_14(String_t* value)
	{
		___U3CConfigurationFilePathU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CConfigurationFilePathU3Ek__BackingField_14), value);
	}

	inline static int32_t get_offset_of_U3CContentDataU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(PreviewConfigurationRequest_t1392293340, ___U3CContentDataU3Ek__BackingField_15)); }
	inline ByteU5BU5D_t3397334013* get_U3CContentDataU3Ek__BackingField_15() const { return ___U3CContentDataU3Ek__BackingField_15; }
	inline ByteU5BU5D_t3397334013** get_address_of_U3CContentDataU3Ek__BackingField_15() { return &___U3CContentDataU3Ek__BackingField_15; }
	inline void set_U3CContentDataU3Ek__BackingField_15(ByteU5BU5D_t3397334013* value)
	{
		___U3CContentDataU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CContentDataU3Ek__BackingField_15), value);
	}

	inline static int32_t get_offset_of_U3CMetadataU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(PreviewConfigurationRequest_t1392293340, ___U3CMetadataU3Ek__BackingField_16)); }
	inline String_t* get_U3CMetadataU3Ek__BackingField_16() const { return ___U3CMetadataU3Ek__BackingField_16; }
	inline String_t** get_address_of_U3CMetadataU3Ek__BackingField_16() { return &___U3CMetadataU3Ek__BackingField_16; }
	inline void set_U3CMetadataU3Ek__BackingField_16(String_t* value)
	{
		___U3CMetadataU3Ek__BackingField_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMetadataU3Ek__BackingField_16), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(PreviewConfigurationRequest_t1392293340, ___U3CCallbackU3Ek__BackingField_17)); }
	inline OnPreviewConfiguration_t1311234390 * get_U3CCallbackU3Ek__BackingField_17() const { return ___U3CCallbackU3Ek__BackingField_17; }
	inline OnPreviewConfiguration_t1311234390 ** get_address_of_U3CCallbackU3Ek__BackingField_17() { return &___U3CCallbackU3Ek__BackingField_17; }
	inline void set_U3CCallbackU3Ek__BackingField_17(OnPreviewConfiguration_t1311234390 * value)
	{
		___U3CCallbackU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREVIEWCONFIGURATIONREQUEST_T1392293340_H
#ifndef ADDCOLLECTIONREQUEST_T1345327075_H
#define ADDCOLLECTIONREQUEST_T1345327075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/AddCollectionRequest
struct  AddCollectionRequest_t1345327075  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/OnAddCollection IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/AddCollectionRequest::<Callback>k__BackingField
	OnAddCollection_t1214285067 * ___U3CCallbackU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/AddCollectionRequest::<EnvironmentID>k__BackingField
	String_t* ___U3CEnvironmentIDU3Ek__BackingField_12;
	// System.Byte[] IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/AddCollectionRequest::<CollectionJsonData>k__BackingField
	ByteU5BU5D_t3397334013* ___U3CCollectionJsonDataU3Ek__BackingField_13;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/AddCollectionRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(AddCollectionRequest_t1345327075, ___U3CCallbackU3Ek__BackingField_11)); }
	inline OnAddCollection_t1214285067 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline OnAddCollection_t1214285067 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(OnAddCollection_t1214285067 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CEnvironmentIDU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(AddCollectionRequest_t1345327075, ___U3CEnvironmentIDU3Ek__BackingField_12)); }
	inline String_t* get_U3CEnvironmentIDU3Ek__BackingField_12() const { return ___U3CEnvironmentIDU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CEnvironmentIDU3Ek__BackingField_12() { return &___U3CEnvironmentIDU3Ek__BackingField_12; }
	inline void set_U3CEnvironmentIDU3Ek__BackingField_12(String_t* value)
	{
		___U3CEnvironmentIDU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEnvironmentIDU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CCollectionJsonDataU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(AddCollectionRequest_t1345327075, ___U3CCollectionJsonDataU3Ek__BackingField_13)); }
	inline ByteU5BU5D_t3397334013* get_U3CCollectionJsonDataU3Ek__BackingField_13() const { return ___U3CCollectionJsonDataU3Ek__BackingField_13; }
	inline ByteU5BU5D_t3397334013** get_address_of_U3CCollectionJsonDataU3Ek__BackingField_13() { return &___U3CCollectionJsonDataU3Ek__BackingField_13; }
	inline void set_U3CCollectionJsonDataU3Ek__BackingField_13(ByteU5BU5D_t3397334013* value)
	{
		___U3CCollectionJsonDataU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCollectionJsonDataU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(AddCollectionRequest_t1345327075, ___U3CDataU3Ek__BackingField_14)); }
	inline String_t* get_U3CDataU3Ek__BackingField_14() const { return ___U3CDataU3Ek__BackingField_14; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_14() { return &___U3CDataU3Ek__BackingField_14; }
	inline void set_U3CDataU3Ek__BackingField_14(String_t* value)
	{
		___U3CDataU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDCOLLECTIONREQUEST_T1345327075_H
#ifndef GETDOCUMENTREQUEST_T205494485_H
#define GETDOCUMENTREQUEST_T205494485_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/GetDocumentRequest
struct  GetDocumentRequest_t205494485  : public Request_t466816980
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/GetDocumentRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/GetDocumentRequest::<EnvironmentID>k__BackingField
	String_t* ___U3CEnvironmentIDU3Ek__BackingField_12;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/GetDocumentRequest::<CollectionID>k__BackingField
	String_t* ___U3CCollectionIDU3Ek__BackingField_13;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/GetDocumentRequest::<DocumentID>k__BackingField
	String_t* ___U3CDocumentIDU3Ek__BackingField_14;
	// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/OnGetDocument IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/GetDocumentRequest::<Callback>k__BackingField
	OnGetDocument_t1915942165 * ___U3CCallbackU3Ek__BackingField_15;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetDocumentRequest_t205494485, ___U3CDataU3Ek__BackingField_11)); }
	inline String_t* get_U3CDataU3Ek__BackingField_11() const { return ___U3CDataU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_11() { return &___U3CDataU3Ek__BackingField_11; }
	inline void set_U3CDataU3Ek__BackingField_11(String_t* value)
	{
		___U3CDataU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CEnvironmentIDU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GetDocumentRequest_t205494485, ___U3CEnvironmentIDU3Ek__BackingField_12)); }
	inline String_t* get_U3CEnvironmentIDU3Ek__BackingField_12() const { return ___U3CEnvironmentIDU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CEnvironmentIDU3Ek__BackingField_12() { return &___U3CEnvironmentIDU3Ek__BackingField_12; }
	inline void set_U3CEnvironmentIDU3Ek__BackingField_12(String_t* value)
	{
		___U3CEnvironmentIDU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEnvironmentIDU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CCollectionIDU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(GetDocumentRequest_t205494485, ___U3CCollectionIDU3Ek__BackingField_13)); }
	inline String_t* get_U3CCollectionIDU3Ek__BackingField_13() const { return ___U3CCollectionIDU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CCollectionIDU3Ek__BackingField_13() { return &___U3CCollectionIDU3Ek__BackingField_13; }
	inline void set_U3CCollectionIDU3Ek__BackingField_13(String_t* value)
	{
		___U3CCollectionIDU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCollectionIDU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CDocumentIDU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(GetDocumentRequest_t205494485, ___U3CDocumentIDU3Ek__BackingField_14)); }
	inline String_t* get_U3CDocumentIDU3Ek__BackingField_14() const { return ___U3CDocumentIDU3Ek__BackingField_14; }
	inline String_t** get_address_of_U3CDocumentIDU3Ek__BackingField_14() { return &___U3CDocumentIDU3Ek__BackingField_14; }
	inline void set_U3CDocumentIDU3Ek__BackingField_14(String_t* value)
	{
		___U3CDocumentIDU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDocumentIDU3Ek__BackingField_14), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(GetDocumentRequest_t205494485, ___U3CCallbackU3Ek__BackingField_15)); }
	inline OnGetDocument_t1915942165 * get_U3CCallbackU3Ek__BackingField_15() const { return ___U3CCallbackU3Ek__BackingField_15; }
	inline OnGetDocument_t1915942165 ** get_address_of_U3CCallbackU3Ek__BackingField_15() { return &___U3CCallbackU3Ek__BackingField_15; }
	inline void set_U3CCallbackU3Ek__BackingField_15(OnGetDocument_t1915942165 * value)
	{
		___U3CCallbackU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETDOCUMENTREQUEST_T205494485_H
#ifndef TYPEFILTER_T2978171441_H
#define TYPEFILTER_T2978171441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.LanguageTranslator/TypeFilter
struct  TypeFilter_t2978171441 
{
public:
	// System.Int32 IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.LanguageTranslator/TypeFilter::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TypeFilter_t2978171441, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEFILTER_T2978171441_H
#ifndef DELEGATE_T3022476291_H
#define DELEGATE_T3022476291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t3022476291  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	IntPtr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	IntPtr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	IntPtr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	IntPtr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1572802995 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___invoke_impl_1)); }
	inline IntPtr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline IntPtr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(IntPtr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_3)); }
	inline IntPtr_t get_method_3() const { return ___method_3; }
	inline IntPtr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(IntPtr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___delegate_trampoline_4)); }
	inline IntPtr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline IntPtr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(IntPtr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_code_5)); }
	inline IntPtr_t get_method_code_5() const { return ___method_code_5; }
	inline IntPtr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(IntPtr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___data_8)); }
	inline DelegateData_t1572802995 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1572802995 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1572802995 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T3022476291_H
#ifndef TYPEFILTER_T283187097_H
#define TYPEFILTER_T283187097_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.LanguageTranslation/TypeFilter
struct  TypeFilter_t283187097 
{
public:
	// System.Int32 IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.LanguageTranslation/TypeFilter::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TypeFilter_t283187097, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEFILTER_T283187097_H
#ifndef MULTICASTDELEGATE_T3201952435_H
#define MULTICASTDELEGATE_T3201952435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t3201952435  : public Delegate_t3022476291
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t3201952435 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t3201952435 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___prev_9)); }
	inline MulticastDelegate_t3201952435 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t3201952435 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t3201952435 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___kpm_next_10)); }
	inline MulticastDelegate_t3201952435 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t3201952435 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t3201952435 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T3201952435_H
#ifndef ONADDCOLLECTION_T1214285067_H
#define ONADDCOLLECTION_T1214285067_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/OnAddCollection
struct  OnAddCollection_t1214285067  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONADDCOLLECTION_T1214285067_H
#ifndef GETMODELSCALLBACK_T2850854704_H
#define GETMODELSCALLBACK_T2850854704_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.LanguageTranslator/GetModelsCallback
struct  GetModelsCallback_t2850854704  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETMODELSCALLBACK_T2850854704_H
#ifndef ONGETFIELDS_T307794883_H
#define ONGETFIELDS_T307794883_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/OnGetFields
struct  OnGetFields_t307794883  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONGETFIELDS_T307794883_H
#ifndef GETLANGUAGESCALLBACK_T1996869611_H
#define GETLANGUAGESCALLBACK_T1996869611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.LanguageTranslator/GetLanguagesCallback
struct  GetLanguagesCallback_t1996869611  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETLANGUAGESCALLBACK_T1996869611_H
#ifndef ONADDENVIRONMENT_T731364804_H
#define ONADDENVIRONMENT_T731364804_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/OnAddEnvironment
struct  OnAddEnvironment_t731364804  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONADDENVIRONMENT_T731364804_H
#ifndef ONDELETECOLLECTION_T603439291_H
#define ONDELETECOLLECTION_T603439291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/OnDeleteCollection
struct  OnDeleteCollection_t603439291  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDELETECOLLECTION_T603439291_H
#ifndef TRANSLATECALLBACK_T3218931436_H
#define TRANSLATECALLBACK_T3218931436_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.LanguageTranslator/TranslateCallback
struct  TranslateCallback_t3218931436  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSLATECALLBACK_T3218931436_H
#ifndef IDENTIFYCALLBACK_T682135812_H
#define IDENTIFYCALLBACK_T682135812_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.LanguageTranslator/IdentifyCallback
struct  IdentifyCallback_t682135812  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IDENTIFYCALLBACK_T682135812_H
#ifndef ONGETCOLLECTION_T85885496_H
#define ONGETCOLLECTION_T85885496_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/OnGetCollection
struct  OnGetCollection_t85885496  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONGETCOLLECTION_T85885496_H
#ifndef GETMODELCALLBACK_T1483236201_H
#define GETMODELCALLBACK_T1483236201_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.LanguageTranslator/GetModelCallback
struct  GetModelCallback_t1483236201  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETMODELCALLBACK_T1483236201_H
#ifndef ONGETCOLLECTIONS_T2546296547_H
#define ONGETCOLLECTIONS_T2546296547_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/OnGetCollections
struct  OnGetCollections_t2546296547  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONGETCOLLECTIONS_T2546296547_H
#ifndef ONADDDOCUMENT_T2879918214_H
#define ONADDDOCUMENT_T2879918214_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/OnAddDocument
struct  OnAddDocument_t2879918214  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONADDDOCUMENT_T2879918214_H
#ifndef ONGETENVIRONMENT_T3415272147_H
#define ONGETENVIRONMENT_T3415272147_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/OnGetEnvironment
struct  OnGetEnvironment_t3415272147  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONGETENVIRONMENT_T3415272147_H
#ifndef ONGETCONFIGURATION_T613203618_H
#define ONGETCONFIGURATION_T613203618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/OnGetConfiguration
struct  OnGetConfiguration_t613203618  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONGETCONFIGURATION_T613203618_H
#ifndef ONDELETECONFIGURATION_T1574428889_H
#define ONDELETECONFIGURATION_T1574428889_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/OnDeleteConfiguration
struct  OnDeleteConfiguration_t1574428889  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDELETECONFIGURATION_T1574428889_H
#ifndef ONPREVIEWCONFIGURATION_T1311234390_H
#define ONPREVIEWCONFIGURATION_T1311234390_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/OnPreviewConfiguration
struct  OnPreviewConfiguration_t1311234390  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONPREVIEWCONFIGURATION_T1311234390_H
#ifndef ONGETENVIRONMENTS_T613121798_H
#define ONGETENVIRONMENTS_T613121798_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/OnGetEnvironments
struct  OnGetEnvironments_t613121798  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONGETENVIRONMENTS_T613121798_H
#ifndef LOADFILEDELEGATE_T484637430_H
#define LOADFILEDELEGATE_T484637430_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.DocumentConversion.v1.DocumentConversion/LoadFileDelegate
struct  LoadFileDelegate_t484637430  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADFILEDELEGATE_T484637430_H
#ifndef ONCONVERTDOCUMENT_T1041817288_H
#define ONCONVERTDOCUMENT_T1041817288_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.DocumentConversion.v1.DocumentConversion/OnConvertDocument
struct  OnConvertDocument_t1041817288  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONCONVERTDOCUMENT_T1041817288_H
#ifndef ONADDCONFIGURATION_T2751378613_H
#define ONADDCONFIGURATION_T2751378613_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/OnAddConfiguration
struct  OnAddConfiguration_t2751378613  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONADDCONFIGURATION_T2751378613_H
#ifndef ONDELETEENVIRONMENT_T1826514308_H
#define ONDELETEENVIRONMENT_T1826514308_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/OnDeleteEnvironment
struct  OnDeleteEnvironment_t1826514308  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDELETEENVIRONMENT_T1826514308_H
#ifndef SERVICESTATUS_T1443707987_H
#define SERVICESTATUS_T1443707987_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.ServiceStatus
struct  ServiceStatus_t1443707987  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERVICESTATUS_T1443707987_H
#ifndef ONUPDATEDOCUMENT_T4020936920_H
#define ONUPDATEDOCUMENT_T4020936920_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/OnUpdateDocument
struct  OnUpdateDocument_t4020936920  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONUPDATEDOCUMENT_T4020936920_H
#ifndef ONQUERY_T887364716_H
#define ONQUERY_T887364716_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/OnQuery
struct  OnQuery_t887364716  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONQUERY_T887364716_H
#ifndef ONGETCONFIGURATIONS_T2431280213_H
#define ONGETCONFIGURATIONS_T2431280213_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/OnGetConfigurations
struct  OnGetConfigurations_t2431280213  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONGETCONFIGURATIONS_T2431280213_H
#ifndef ONDELETEDOCUMENT_T3959738954_H
#define ONDELETEDOCUMENT_T3959738954_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/OnDeleteDocument
struct  OnDeleteDocument_t3959738954  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDELETEDOCUMENT_T3959738954_H
#ifndef ONGETDOCUMENT_T1915942165_H
#define ONGETDOCUMENT_T1915942165_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery/OnGetDocument
struct  OnGetDocument_t1915942165  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONGETDOCUMENT_T1915942165_H
#ifndef GETMODELCALLBACK_T3256244769_H
#define GETMODELCALLBACK_T3256244769_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.LanguageTranslation/GetModelCallback
struct  GetModelCallback_t3256244769  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETMODELCALLBACK_T3256244769_H
#ifndef GETMODELSCALLBACK_T1383378020_H
#define GETMODELSCALLBACK_T1383378020_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.LanguageTranslation/GetModelsCallback
struct  GetModelsCallback_t1383378020  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETMODELSCALLBACK_T1383378020_H
#ifndef GETLANGUAGESCALLBACK_T833615519_H
#define GETLANGUAGESCALLBACK_T833615519_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.LanguageTranslation/GetLanguagesCallback
struct  GetLanguagesCallback_t833615519  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETLANGUAGESCALLBACK_T833615519_H
#ifndef TRANSLATECALLBACK_T149709672_H
#define TRANSLATECALLBACK_T149709672_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.LanguageTranslation/TranslateCallback
struct  TranslateCallback_t149709672  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSLATECALLBACK_T149709672_H
#ifndef IDENTIFYCALLBACK_T942722308_H
#define IDENTIFYCALLBACK_T942722308_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.LanguageTranslation/IdentifyCallback
struct  IdentifyCallback_t942722308  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IDENTIFYCALLBACK_T942722308_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2500 = { sizeof (GetFieldsResponse_t2235887816), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2500[1] = 
{
	GetFieldsResponse_t2235887816::get_offset_of_U3CfieldsU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2501 = { sizeof (Field_t3865657682), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2501[2] = 
{
	Field_t3865657682::get_offset_of_U3CfieldU3Ek__BackingField_0(),
	Field_t3865657682::get_offset_of_U3CtypeU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2502 = { sizeof (DocumentAccepted_t4162415286), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2502[2] = 
{
	DocumentAccepted_t4162415286::get_offset_of_U3Cdocument_idU3Ek__BackingField_0(),
	DocumentAccepted_t4162415286::get_offset_of_U3CstatusU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2503 = { sizeof (DeleteDocumentResponse_t2376718965), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2503[2] = 
{
	DeleteDocumentResponse_t2376718965::get_offset_of_U3Cdocument_idU3Ek__BackingField_0(),
	DeleteDocumentResponse_t2376718965::get_offset_of_U3CstatusU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2504 = { sizeof (DocumentStatus_t2219045527), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2504[7] = 
{
	DocumentStatus_t2219045527::get_offset_of_U3Cdocument_idU3Ek__BackingField_0(),
	DocumentStatus_t2219045527::get_offset_of_U3Cconfiguration_idU3Ek__BackingField_1(),
	DocumentStatus_t2219045527::get_offset_of_U3CcreatedU3Ek__BackingField_2(),
	DocumentStatus_t2219045527::get_offset_of_U3CupdatedU3Ek__BackingField_3(),
	DocumentStatus_t2219045527::get_offset_of_U3CstatusU3Ek__BackingField_4(),
	DocumentStatus_t2219045527::get_offset_of_U3Cstatus_descriptionU3Ek__BackingField_5(),
	DocumentStatus_t2219045527::get_offset_of_U3CnoticesU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2505 = { sizeof (QueryResponse_t507320049), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2505[3] = 
{
	QueryResponse_t507320049::get_offset_of_U3Cmatching_resultsU3Ek__BackingField_0(),
	QueryResponse_t507320049::get_offset_of_U3CresultsU3Ek__BackingField_1(),
	QueryResponse_t507320049::get_offset_of_U3CaggregationsU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2506 = { sizeof (QueryResult_t1852366331), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2506[2] = 
{
	QueryResult_t1852366331::get_offset_of_U3CidU3Ek__BackingField_0(),
	QueryResult_t1852366331::get_offset_of_U3CscoreU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2507 = { sizeof (QueryAggregation_t1214220176), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2507[1] = 
{
	QueryAggregation_t1214220176::get_offset_of_U3CtermU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2508 = { sizeof (AggregationTerm_t3655396846), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2508[1] = 
{
	AggregationTerm_t3655396846::get_offset_of_U3CresultsU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2509 = { sizeof (AggregationResult_t3787092725), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2509[2] = 
{
	AggregationResult_t3787092725::get_offset_of_U3CkeyU3Ek__BackingField_0(),
	AggregationResult_t3787092725::get_offset_of_U3Cmatching_resultsU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2510 = { sizeof (DiscoveryVersion_t2429697012), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2510[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2511 = { sizeof (Discovery_t1478121420), -1, sizeof(Discovery_t1478121420_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2511[14] = 
{
	0,
	Discovery_t1478121420_StaticFields::get_offset_of_sm_Serializer_1(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2512 = { sizeof (OnGetEnvironments_t613121798), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2513 = { sizeof (GetEnvironmentsRequest_t266851542), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2513[2] = 
{
	GetEnvironmentsRequest_t266851542::get_offset_of_U3CDataU3Ek__BackingField_11(),
	GetEnvironmentsRequest_t266851542::get_offset_of_U3CCallbackU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2514 = { sizeof (OnAddEnvironment_t731364804), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2515 = { sizeof (AddEnvironmentRequest_t3169458642), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2515[3] = 
{
	AddEnvironmentRequest_t3169458642::get_offset_of_U3CDataU3Ek__BackingField_11(),
	AddEnvironmentRequest_t3169458642::get_offset_of_U3CAddEnvironmentDataU3Ek__BackingField_12(),
	AddEnvironmentRequest_t3169458642::get_offset_of_U3CCallbackU3Ek__BackingField_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2516 = { sizeof (OnGetEnvironment_t3415272147), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2517 = { sizeof (GetEnvironmentRequest_t4050074335), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2517[3] = 
{
	GetEnvironmentRequest_t4050074335::get_offset_of_U3CDataU3Ek__BackingField_11(),
	GetEnvironmentRequest_t4050074335::get_offset_of_U3CEnvironmentIDU3Ek__BackingField_12(),
	GetEnvironmentRequest_t4050074335::get_offset_of_U3CCallbackU3Ek__BackingField_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2518 = { sizeof (OnDeleteEnvironment_t1826514308), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2519 = { sizeof (DeleteEnvironmentRequest_t677322444), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2519[3] = 
{
	DeleteEnvironmentRequest_t677322444::get_offset_of_U3CDataU3Ek__BackingField_11(),
	DeleteEnvironmentRequest_t677322444::get_offset_of_U3CEnvironmentIDU3Ek__BackingField_12(),
	DeleteEnvironmentRequest_t677322444::get_offset_of_U3CCallbackU3Ek__BackingField_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2520 = { sizeof (OnGetConfigurations_t2431280213), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2521 = { sizeof (GetConfigurationsRequest_t3894976669), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2521[4] = 
{
	GetConfigurationsRequest_t3894976669::get_offset_of_U3CDataU3Ek__BackingField_11(),
	GetConfigurationsRequest_t3894976669::get_offset_of_U3CEnvironmentIDU3Ek__BackingField_12(),
	GetConfigurationsRequest_t3894976669::get_offset_of_U3CNameU3Ek__BackingField_13(),
	GetConfigurationsRequest_t3894976669::get_offset_of_U3CCallbackU3Ek__BackingField_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2522 = { sizeof (OnAddConfiguration_t2751378613), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2523 = { sizeof (AddConfigurationRequest_t972289441), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2523[4] = 
{
	AddConfigurationRequest_t972289441::get_offset_of_U3CCallbackU3Ek__BackingField_11(),
	AddConfigurationRequest_t972289441::get_offset_of_U3CEnvironmentIDU3Ek__BackingField_12(),
	AddConfigurationRequest_t972289441::get_offset_of_U3CConfigurationJsonDataU3Ek__BackingField_13(),
	AddConfigurationRequest_t972289441::get_offset_of_U3CDataU3Ek__BackingField_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2524 = { sizeof (OnGetConfiguration_t613203618), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2525 = { sizeof (GetConfigurationRequest_t1348715152), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2525[4] = 
{
	GetConfigurationRequest_t1348715152::get_offset_of_U3CDataU3Ek__BackingField_11(),
	GetConfigurationRequest_t1348715152::get_offset_of_U3CEnvironmentIDU3Ek__BackingField_12(),
	GetConfigurationRequest_t1348715152::get_offset_of_U3CConfigurationIDU3Ek__BackingField_13(),
	GetConfigurationRequest_t1348715152::get_offset_of_U3CCallbackU3Ek__BackingField_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2526 = { sizeof (OnDeleteConfiguration_t1574428889), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2527 = { sizeof (DeleteConfigurationRequest_t1501683641), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2527[4] = 
{
	DeleteConfigurationRequest_t1501683641::get_offset_of_U3CDataU3Ek__BackingField_11(),
	DeleteConfigurationRequest_t1501683641::get_offset_of_U3CEnvironmentIDU3Ek__BackingField_12(),
	DeleteConfigurationRequest_t1501683641::get_offset_of_U3CConfigurationIDU3Ek__BackingField_13(),
	DeleteConfigurationRequest_t1501683641::get_offset_of_U3CCallbackU3Ek__BackingField_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2528 = { sizeof (OnPreviewConfiguration_t1311234390), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2529 = { sizeof (PreviewConfigurationRequest_t1392293340), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2529[7] = 
{
	PreviewConfigurationRequest_t1392293340::get_offset_of_U3CDataU3Ek__BackingField_11(),
	PreviewConfigurationRequest_t1392293340::get_offset_of_U3CEnvironmentIDU3Ek__BackingField_12(),
	PreviewConfigurationRequest_t1392293340::get_offset_of_U3CConfigurationIDU3Ek__BackingField_13(),
	PreviewConfigurationRequest_t1392293340::get_offset_of_U3CConfigurationFilePathU3Ek__BackingField_14(),
	PreviewConfigurationRequest_t1392293340::get_offset_of_U3CContentDataU3Ek__BackingField_15(),
	PreviewConfigurationRequest_t1392293340::get_offset_of_U3CMetadataU3Ek__BackingField_16(),
	PreviewConfigurationRequest_t1392293340::get_offset_of_U3CCallbackU3Ek__BackingField_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2530 = { sizeof (OnGetCollections_t2546296547), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2531 = { sizeof (GetCollectionsRequest_t324656879), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2531[4] = 
{
	GetCollectionsRequest_t324656879::get_offset_of_U3CDataU3Ek__BackingField_11(),
	GetCollectionsRequest_t324656879::get_offset_of_U3CEnvironmentIDU3Ek__BackingField_12(),
	GetCollectionsRequest_t324656879::get_offset_of_U3CNameU3Ek__BackingField_13(),
	GetCollectionsRequest_t324656879::get_offset_of_U3CCallbackU3Ek__BackingField_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2532 = { sizeof (OnAddCollection_t1214285067), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2533 = { sizeof (AddCollectionRequest_t1345327075), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2533[4] = 
{
	AddCollectionRequest_t1345327075::get_offset_of_U3CCallbackU3Ek__BackingField_11(),
	AddCollectionRequest_t1345327075::get_offset_of_U3CEnvironmentIDU3Ek__BackingField_12(),
	AddCollectionRequest_t1345327075::get_offset_of_U3CCollectionJsonDataU3Ek__BackingField_13(),
	AddCollectionRequest_t1345327075::get_offset_of_U3CDataU3Ek__BackingField_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2534 = { sizeof (OnGetCollection_t85885496), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2535 = { sizeof (GetCollectionRequest_t3877445384), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2535[4] = 
{
	GetCollectionRequest_t3877445384::get_offset_of_U3CDataU3Ek__BackingField_11(),
	GetCollectionRequest_t3877445384::get_offset_of_U3CEnvironmentIDU3Ek__BackingField_12(),
	GetCollectionRequest_t3877445384::get_offset_of_U3CCollectionIDU3Ek__BackingField_13(),
	GetCollectionRequest_t3877445384::get_offset_of_U3CCallbackU3Ek__BackingField_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2536 = { sizeof (OnDeleteCollection_t603439291), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2537 = { sizeof (DeleteCollectionRequest_t1933527959), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2537[4] = 
{
	DeleteCollectionRequest_t1933527959::get_offset_of_U3CDataU3Ek__BackingField_11(),
	DeleteCollectionRequest_t1933527959::get_offset_of_U3CEnvironmentIDU3Ek__BackingField_12(),
	DeleteCollectionRequest_t1933527959::get_offset_of_U3CCollectionIDU3Ek__BackingField_13(),
	DeleteCollectionRequest_t1933527959::get_offset_of_U3CCallbackU3Ek__BackingField_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2538 = { sizeof (OnGetFields_t307794883), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2539 = { sizeof (GetFieldsRequest_t991558571), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2539[5] = 
{
	GetFieldsRequest_t991558571::get_offset_of_U3CDataU3Ek__BackingField_11(),
	GetFieldsRequest_t991558571::get_offset_of_U3CEnvironmentIDU3Ek__BackingField_12(),
	GetFieldsRequest_t991558571::get_offset_of_U3CCollectionIDU3Ek__BackingField_13(),
	GetFieldsRequest_t991558571::get_offset_of_U3CNameU3Ek__BackingField_14(),
	GetFieldsRequest_t991558571::get_offset_of_U3CCallbackU3Ek__BackingField_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2540 = { sizeof (OnAddDocument_t2879918214), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2541 = { sizeof (AddDocumentRequest_t3529010302), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2541[9] = 
{
	AddDocumentRequest_t3529010302::get_offset_of_U3CDataU3Ek__BackingField_11(),
	AddDocumentRequest_t3529010302::get_offset_of_U3CEnvironmentIDU3Ek__BackingField_12(),
	AddDocumentRequest_t3529010302::get_offset_of_U3CCollectionIDU3Ek__BackingField_13(),
	AddDocumentRequest_t3529010302::get_offset_of_U3CConfigurationIDU3Ek__BackingField_14(),
	AddDocumentRequest_t3529010302::get_offset_of_U3CConfigurationU3Ek__BackingField_15(),
	AddDocumentRequest_t3529010302::get_offset_of_U3CContentDataU3Ek__BackingField_16(),
	AddDocumentRequest_t3529010302::get_offset_of_U3CContentMimeTypeU3Ek__BackingField_17(),
	AddDocumentRequest_t3529010302::get_offset_of_U3CMetadataU3Ek__BackingField_18(),
	AddDocumentRequest_t3529010302::get_offset_of_U3CCallbackU3Ek__BackingField_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2542 = { sizeof (OnDeleteDocument_t3959738954), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2543 = { sizeof (DeleteDocumentRequest_t2235071592), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2543[5] = 
{
	DeleteDocumentRequest_t2235071592::get_offset_of_U3CDataU3Ek__BackingField_11(),
	DeleteDocumentRequest_t2235071592::get_offset_of_U3CEnvironmentIDU3Ek__BackingField_12(),
	DeleteDocumentRequest_t2235071592::get_offset_of_U3CCollectionIDU3Ek__BackingField_13(),
	DeleteDocumentRequest_t2235071592::get_offset_of_U3CDocumentIDU3Ek__BackingField_14(),
	DeleteDocumentRequest_t2235071592::get_offset_of_U3CCallbackU3Ek__BackingField_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2544 = { sizeof (OnGetDocument_t1915942165), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2545 = { sizeof (GetDocumentRequest_t205494485), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2545[5] = 
{
	GetDocumentRequest_t205494485::get_offset_of_U3CDataU3Ek__BackingField_11(),
	GetDocumentRequest_t205494485::get_offset_of_U3CEnvironmentIDU3Ek__BackingField_12(),
	GetDocumentRequest_t205494485::get_offset_of_U3CCollectionIDU3Ek__BackingField_13(),
	GetDocumentRequest_t205494485::get_offset_of_U3CDocumentIDU3Ek__BackingField_14(),
	GetDocumentRequest_t205494485::get_offset_of_U3CCallbackU3Ek__BackingField_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2546 = { sizeof (OnUpdateDocument_t4020936920), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2547 = { sizeof (UpdateDocumentRequest_t2438806938), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2547[10] = 
{
	UpdateDocumentRequest_t2438806938::get_offset_of_U3CDataU3Ek__BackingField_11(),
	UpdateDocumentRequest_t2438806938::get_offset_of_U3CEnvironmentIDU3Ek__BackingField_12(),
	UpdateDocumentRequest_t2438806938::get_offset_of_U3CCollectionIDU3Ek__BackingField_13(),
	UpdateDocumentRequest_t2438806938::get_offset_of_U3CDocumentIDU3Ek__BackingField_14(),
	UpdateDocumentRequest_t2438806938::get_offset_of_U3CConfigurationIDU3Ek__BackingField_15(),
	UpdateDocumentRequest_t2438806938::get_offset_of_U3CConfigurationU3Ek__BackingField_16(),
	UpdateDocumentRequest_t2438806938::get_offset_of_U3CContentDataU3Ek__BackingField_17(),
	UpdateDocumentRequest_t2438806938::get_offset_of_U3CContentMimeTypeU3Ek__BackingField_18(),
	UpdateDocumentRequest_t2438806938::get_offset_of_U3CMetadataU3Ek__BackingField_19(),
	UpdateDocumentRequest_t2438806938::get_offset_of_U3CCallbackU3Ek__BackingField_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2548 = { sizeof (OnQuery_t887364716), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2549 = { sizeof (QueryRequest_t3555347148), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2549[10] = 
{
	QueryRequest_t3555347148::get_offset_of_U3CDataU3Ek__BackingField_11(),
	QueryRequest_t3555347148::get_offset_of_U3CEnvironmentIDU3Ek__BackingField_12(),
	QueryRequest_t3555347148::get_offset_of_U3CCollectionIDU3Ek__BackingField_13(),
	QueryRequest_t3555347148::get_offset_of_U3CFilterU3Ek__BackingField_14(),
	QueryRequest_t3555347148::get_offset_of_U3CQueryU3Ek__BackingField_15(),
	QueryRequest_t3555347148::get_offset_of_U3CAggregationU3Ek__BackingField_16(),
	QueryRequest_t3555347148::get_offset_of_U3CCountU3Ek__BackingField_17(),
	QueryRequest_t3555347148::get_offset_of_U3CReturnU3Ek__BackingField_18(),
	QueryRequest_t3555347148::get_offset_of_U3COffsetU3Ek__BackingField_19(),
	QueryRequest_t3555347148::get_offset_of_U3CCallbackU3Ek__BackingField_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2550 = { sizeof (CheckServiceStatus_t3621665888), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2550[2] = 
{
	CheckServiceStatus_t3621665888::get_offset_of_m_Service_0(),
	CheckServiceStatus_t3621665888::get_offset_of_m_Callback_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2551 = { sizeof (ConvertedDocument_t1813677595), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2551[8] = 
{
	ConvertedDocument_t1813677595::get_offset_of_U3Csource_document_idU3Ek__BackingField_0(),
	ConvertedDocument_t1813677595::get_offset_of_U3CtimestampU3Ek__BackingField_1(),
	ConvertedDocument_t1813677595::get_offset_of_U3Cmedia_type_detectedU3Ek__BackingField_2(),
	ConvertedDocument_t1813677595::get_offset_of_U3CmetadataU3Ek__BackingField_3(),
	ConvertedDocument_t1813677595::get_offset_of_U3Canswer_unitsU3Ek__BackingField_4(),
	ConvertedDocument_t1813677595::get_offset_of_U3CwarningsU3Ek__BackingField_5(),
	ConvertedDocument_t1813677595::get_offset_of_U3CtextContentU3Ek__BackingField_6(),
	ConvertedDocument_t1813677595::get_offset_of_U3ChtmlContentU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2552 = { sizeof (Metadata_t1932344831), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2552[2] = 
{
	Metadata_t1932344831::get_offset_of_U3CnameU3Ek__BackingField_0(),
	Metadata_t1932344831::get_offset_of_U3CcontentU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2553 = { sizeof (AnswerUnit_t3274674984), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2553[6] = 
{
	AnswerUnit_t3274674984::get_offset_of_U3CidU3Ek__BackingField_0(),
	AnswerUnit_t3274674984::get_offset_of_U3CtypeU3Ek__BackingField_1(),
	AnswerUnit_t3274674984::get_offset_of_U3Cparent_idU3Ek__BackingField_2(),
	AnswerUnit_t3274674984::get_offset_of_U3CtitleU3Ek__BackingField_3(),
	AnswerUnit_t3274674984::get_offset_of_U3CdirectionU3Ek__BackingField_4(),
	AnswerUnit_t3274674984::get_offset_of_U3CcontentU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2554 = { sizeof (Content_t2184047853), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2554[2] = 
{
	Content_t2184047853::get_offset_of_U3Cmedia_typeU3Ek__BackingField_0(),
	Content_t2184047853::get_offset_of_U3CtextU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2555 = { sizeof (Warning_t1238678040), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2555[2] = 
{
	Warning_t1238678040::get_offset_of_U3CcodeU3Ek__BackingField_0(),
	Warning_t1238678040::get_offset_of_U3CerrorU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2556 = { sizeof (ConversionTarget_t2856393115), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2556[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2557 = { sizeof (Version_t1541338264), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2557[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2558 = { sizeof (DocumentConversion_t1610681923), -1, sizeof(DocumentConversion_t1610681923_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2558[4] = 
{
	0,
	DocumentConversion_t1610681923_StaticFields::get_offset_of_sm_Serializer_1(),
	0,
	DocumentConversion_t1610681923::get_offset_of_U3CLoadFileU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2559 = { sizeof (OnConvertDocument_t1041817288), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2560 = { sizeof (LoadFileDelegate_t484637430), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2561 = { sizeof (ConvertDocumentRequest_t3520475208), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2561[3] = 
{
	ConvertDocumentRequest_t3520475208::get_offset_of_U3CDataU3Ek__BackingField_11(),
	ConvertDocumentRequest_t3520475208::get_offset_of_U3CConversionTargetU3Ek__BackingField_12(),
	ConvertDocumentRequest_t3520475208::get_offset_of_U3CCallbackU3Ek__BackingField_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2562 = { sizeof (CheckServiceStatus_t1899240418), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2562[2] = 
{
	CheckServiceStatus_t1899240418::get_offset_of_m_Service_0(),
	CheckServiceStatus_t1899240418::get_offset_of_m_Callback_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2563 = { sizeof (ServiceStatus_t1443707987), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2564 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2565 = { sizeof (ServiceHelper_t233431317), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2566 = { sizeof (Language_t834895248), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2566[2] = 
{
	Language_t834895248::get_offset_of_U3ClanguageU3Ek__BackingField_0(),
	Language_t834895248::get_offset_of_U3CnameU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2567 = { sizeof (Languages_t4109093315), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2567[1] = 
{
	Languages_t4109093315::get_offset_of_U3ClanguagesU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2568 = { sizeof (Translation_t1796874115), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2568[1] = 
{
	Translation_t1796874115::get_offset_of_U3CtranslationU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2569 = { sizeof (Translations_t3057738030), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2569[3] = 
{
	Translations_t3057738030::get_offset_of_U3Cword_countU3Ek__BackingField_0(),
	Translations_t3057738030::get_offset_of_U3Ccharacter_countU3Ek__BackingField_1(),
	Translations_t3057738030::get_offset_of_U3CtranslationsU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2570 = { sizeof (TranslationModel_t4027325182), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2570[10] = 
{
	TranslationModel_t4027325182::get_offset_of_U3Cmodel_idU3Ek__BackingField_0(),
	TranslationModel_t4027325182::get_offset_of_U3CnameU3Ek__BackingField_1(),
	TranslationModel_t4027325182::get_offset_of_U3CsourceU3Ek__BackingField_2(),
	TranslationModel_t4027325182::get_offset_of_U3CtargetU3Ek__BackingField_3(),
	TranslationModel_t4027325182::get_offset_of_U3Cbase_model_idU3Ek__BackingField_4(),
	TranslationModel_t4027325182::get_offset_of_U3CdomainU3Ek__BackingField_5(),
	TranslationModel_t4027325182::get_offset_of_U3CcustomizableU3Ek__BackingField_6(),
	TranslationModel_t4027325182::get_offset_of_U3CdefaultU3Ek__BackingField_7(),
	TranslationModel_t4027325182::get_offset_of_U3CownerU3Ek__BackingField_8(),
	TranslationModel_t4027325182::get_offset_of_U3CstatusU3Ek__BackingField_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2571 = { sizeof (TranslationModels_t2926535245), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2571[1] = 
{
	TranslationModels_t2926535245::get_offset_of_U3CmodelsU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2572 = { sizeof (LanguageTranslation_t3099415401), -1, sizeof(LanguageTranslation_t3099415401_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2572[2] = 
{
	0,
	LanguageTranslation_t3099415401_StaticFields::get_offset_of_sm_Serializer_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2573 = { sizeof (GetModelsCallback_t1383378020), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2574 = { sizeof (GetModelCallback_t3256244769), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2575 = { sizeof (GetLanguagesCallback_t833615519), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2576 = { sizeof (IdentifyCallback_t942722308), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2577 = { sizeof (TranslateCallback_t149709672), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2578 = { sizeof (TranslateReq_t3687846705), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2578[1] = 
{
	TranslateReq_t3687846705::get_offset_of_U3CCallbackU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2579 = { sizeof (TypeFilter_t283187097)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2579[4] = 
{
	TypeFilter_t283187097::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2580 = { sizeof (GetModelsReq_t3205312709), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2580[1] = 
{
	GetModelsReq_t3205312709::get_offset_of_U3CCallbackU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2581 = { sizeof (GetModelReq_t2857391638), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2581[1] = 
{
	GetModelReq_t2857391638::get_offset_of_U3CCallbackU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2582 = { sizeof (GetLanguagesReq_t3770216304), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2582[1] = 
{
	GetLanguagesReq_t3770216304::get_offset_of_U3CCallbackU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2583 = { sizeof (IdentifyReq_t3760833055), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2583[1] = 
{
	IdentifyReq_t3760833055::get_offset_of_U3CCallbackU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2584 = { sizeof (CheckServiceStatus_t3931118342), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2584[2] = 
{
	CheckServiceStatus_t3931118342::get_offset_of_m_Service_0(),
	CheckServiceStatus_t3931118342::get_offset_of_m_Callback_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2585 = { sizeof (Language_t2171379860), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2585[2] = 
{
	Language_t2171379860::get_offset_of_U3ClanguageU3Ek__BackingField_0(),
	Language_t2171379860::get_offset_of_U3CnameU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2586 = { sizeof (Languages_t1673966175), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2586[1] = 
{
	Languages_t1673966175::get_offset_of_U3ClanguagesU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2587 = { sizeof (Translation_t308652637), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2587[1] = 
{
	Translation_t308652637::get_offset_of_U3CtranslationU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2588 = { sizeof (Translations_t2818370350), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2588[3] = 
{
	Translations_t2818370350::get_offset_of_U3Cword_countU3Ek__BackingField_0(),
	Translations_t2818370350::get_offset_of_U3Ccharacter_countU3Ek__BackingField_1(),
	Translations_t2818370350::get_offset_of_U3CtranslationsU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2589 = { sizeof (TranslationModel_t2730767334), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2589[10] = 
{
	TranslationModel_t2730767334::get_offset_of_U3Cmodel_idU3Ek__BackingField_0(),
	TranslationModel_t2730767334::get_offset_of_U3CnameU3Ek__BackingField_1(),
	TranslationModel_t2730767334::get_offset_of_U3CsourceU3Ek__BackingField_2(),
	TranslationModel_t2730767334::get_offset_of_U3CtargetU3Ek__BackingField_3(),
	TranslationModel_t2730767334::get_offset_of_U3Cbase_model_idU3Ek__BackingField_4(),
	TranslationModel_t2730767334::get_offset_of_U3CdomainU3Ek__BackingField_5(),
	TranslationModel_t2730767334::get_offset_of_U3CcustomizableU3Ek__BackingField_6(),
	TranslationModel_t2730767334::get_offset_of_U3CdefaultU3Ek__BackingField_7(),
	TranslationModel_t2730767334::get_offset_of_U3CownerU3Ek__BackingField_8(),
	TranslationModel_t2730767334::get_offset_of_U3CstatusU3Ek__BackingField_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2590 = { sizeof (TranslationModels_t18565733), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2590[1] = 
{
	TranslationModels_t18565733::get_offset_of_U3CmodelsU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2591 = { sizeof (LanguageTranslator_t2981742234), -1, sizeof(LanguageTranslator_t2981742234_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2591[2] = 
{
	0,
	LanguageTranslator_t2981742234_StaticFields::get_offset_of_sm_Serializer_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2592 = { sizeof (GetModelsCallback_t2850854704), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2593 = { sizeof (GetModelCallback_t1483236201), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2594 = { sizeof (GetLanguagesCallback_t1996869611), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2595 = { sizeof (IdentifyCallback_t682135812), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2596 = { sizeof (TranslateCallback_t3218931436), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2597 = { sizeof (TranslateReq_t1466796057), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2597[1] = 
{
	TranslateReq_t1466796057::get_offset_of_U3CCallbackU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2598 = { sizeof (TypeFilter_t2978171441)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2598[4] = 
{
	TypeFilter_t2978171441::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2599 = { sizeof (GetModelsReq_t974379077), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2599[1] = 
{
	GetModelsReq_t974379077::get_offset_of_U3CCallbackU3Ek__BackingField_11(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
