﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.String
struct String_t;
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Position
struct Position_t3597491459;
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Anchor[]
struct AnchorU5BU5D_t2762965888;
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Config
struct Config_t1927135816;
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Node[]
struct NodeU5BU5D_t283985611;
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Map
struct Map_t1914475838;
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Solution[]
struct SolutionU5BU5D_t2153136770;
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ErrorInfoNoCode
struct ErrorInfoNoCode_t2062371116;
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.TextRecogOneWord[]
struct TextRecogOneWordU5BU5D_t1008406524;
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Drivers
struct Drivers_t1652706281;
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Params
struct Params_t3159428742;
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column[]
struct ColumnU5BU5D_t4200576441;
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Option[]
struct OptionU5BU5D_t3811736648;
// System.String[]
struct StringU5BU5D_t1642385972;
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Range
struct Range_t2396059803;
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.ApplicationData
struct ApplicationData_t1885408412;
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.ApplicationDataValue
struct ApplicationDataValue_t1721671971;
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.Location
struct Location_t328870245;
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyTopLevelSingle[]
struct ClassifyTopLevelSingleU5BU5D_t3685983982;
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.WarningInfo[]
struct WarningInfoU5BU5D_t1932079499;
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyPerClassifier[]
struct ClassifyPerClassifierU5BU5D_t1895762155;
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.OneFaceResult[]
struct OneFaceResultU5BU5D_t4008704067;
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics
struct TradeoffAnalytics_t100002595;
// IBM.Watson.DeveloperCloud.Services.ServiceStatus
struct ServiceStatus_t1443707987;
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.FacesTopLevelSingle[]
struct FacesTopLevelSingleU5BU5D_t1422738560;
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassResult[]
struct ClassResultU5BU5D_t2895066752;
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.StatusCause
struct StatusCause_t3777693303;
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.Age
struct Age_t582693723;
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.Gender
struct Gender_t3451852799;
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.FaceLocation
struct FaceLocation_t2324903414;
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.Identity
struct Identity_t2022392156;
// FullSerializer.fsSerializer
struct fsSerializer_t4193731081;
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.TextRecogTopLevelSingle[]
struct TextRecogTopLevelSingleU5BU5D_t247892373;
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.SimilarImageConfig[]
struct SimilarImageConfigU5BU5D_t273269715;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Collections.Generic.Dictionary`2<System.String,IBM.Watson.DeveloperCloud.Connection.RESTConnector/Form>
struct Dictionary_2_t2694055125;
// IBM.Watson.DeveloperCloud.Connection.RESTConnector/ResponseEvent
struct ResponseEvent_t1616568356;
// IBM.Watson.DeveloperCloud.Connection.RESTConnector/ProgressEvent
struct ProgressEvent_t4185145044;
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.CollectionImagesConfig[]
struct CollectionImagesConfigU5BU5D_t666950595;
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/LoadFileDelegate
struct LoadFileDelegate_t3728218844;
// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneCategory[]
struct ToneCategoryU5BU5D_t3565596459;
// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.Tone[]
struct ToneU5BU5D_t4171600311;
// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzer
struct ToneAnalyzer_t1356110496;
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Problem
struct Problem_t2814813345;
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Resolution
struct Resolution_t3867497320;
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.GetClassifiersPerClassifierBrief[]
struct GetClassifiersPerClassifierBriefU5BU5D_t1088138063;
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.CreateCollection[]
struct CreateCollectionU5BU5D_t2645069445;
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.GetCollectionsBrief[]
struct GetCollectionsBriefU5BU5D_t3322171774;
// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.DocumentTone
struct DocumentTone_t2537937785;
// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.SentenceTone[]
struct SentenceToneU5BU5D_t255016994;
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.Class[]
struct ClassU5BU5D_t1647736209;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech
struct TextToSpeech_t3349357562;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordsCallback
struct AddCustomizationWordsCallback_t2578578585;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Words
struct Words_t2948214629;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordsCallback
struct GetCustomizationWordsCallback_t2709615664;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordCallback
struct GetCustomizationWordCallback_t2485962781;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/OnDeleteCustomizationWordCallback
struct OnDeleteCustomizationWordCallback_t2544578463;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/CreateCustomizationCallback
struct CreateCustomizationCallback_t3071584865;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomVoice
struct CustomVoice_t330695553;
// System.Char[]
struct CharU5BU5D_t1328083999;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationsCallback
struct GetCustomizationsCallback_t201646696;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/UpdateCustomizationCallback
struct UpdateCustomizationCallback_t3026233620;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomVoiceUpdate
struct CustomVoiceUpdate_t1706248840;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationCallback
struct GetCustomizationCallback_t4165371535;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/OnDeleteCustomizationCallback
struct OnDeleteCustomizationCallback_t2862971265;
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics/OnDilemma
struct OnDilemma_t401820821;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordCallback
struct AddCustomizationWordCallback_t3775539180;
// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzer/OnGetToneAnalyzed
struct OnGetToneAnalyzed_t3813655652;
// System.Void
struct Void_t1841601450;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1572802995;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetPronunciationCallback
struct GetPronunciationCallback_t2622344657;
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.GetClassifiersPerClassifierVerbose
struct GetClassifiersPerClassifierVerbose_t3597794938;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.FacesTopLevelMultiple
struct FacesTopLevelMultiple_t351289709;
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyTopLevelMultiple
struct ClassifyTopLevelMultiple_t317282463;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Pronunciation
struct Pronunciation_t2711344207;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Customization
struct Customization_t2261013253;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Customizations
struct Customizations_t482380184;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomizationID
struct CustomizationID_t344279642;
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.DilemmasResponse
struct DilemmasResponse_t3767705817;
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.GetClassifiersTopLevelBrief
struct GetClassifiersTopLevelBrief_t3177285335;
// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzerResponse
struct ToneAnalyzerResponse_t3391014235;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Translation
struct Translation_t2174867539;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ANCHOR_T1470404909_H
#define ANCHOR_T1470404909_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Anchor
struct  Anchor_t1470404909  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Anchor::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_0;
	// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Position IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Anchor::<position>k__BackingField
	Position_t3597491459 * ___U3CpositionU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Anchor_t1470404909, ___U3CnameU3Ek__BackingField_0)); }
	inline String_t* get_U3CnameU3Ek__BackingField_0() const { return ___U3CnameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_0() { return &___U3CnameU3Ek__BackingField_0; }
	inline void set_U3CnameU3Ek__BackingField_0(String_t* value)
	{
		___U3CnameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CpositionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Anchor_t1470404909, ___U3CpositionU3Ek__BackingField_1)); }
	inline Position_t3597491459 * get_U3CpositionU3Ek__BackingField_1() const { return ___U3CpositionU3Ek__BackingField_1; }
	inline Position_t3597491459 ** get_address_of_U3CpositionU3Ek__BackingField_1() { return &___U3CpositionU3Ek__BackingField_1; }
	inline void set_U3CpositionU3Ek__BackingField_1(Position_t3597491459 * value)
	{
		___U3CpositionU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpositionU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANCHOR_T1470404909_H
#ifndef MAP_T1914475838_H
#define MAP_T1914475838_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Map
struct  Map_t1914475838  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Anchor[] IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Map::<anchors>k__BackingField
	AnchorU5BU5D_t2762965888* ___U3CanchorsU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Map::<comments>k__BackingField
	String_t* ___U3CcommentsU3Ek__BackingField_1;
	// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Config IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Map::<config>k__BackingField
	Config_t1927135816 * ___U3CconfigU3Ek__BackingField_2;
	// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Node[] IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Map::<nodes>k__BackingField
	NodeU5BU5D_t283985611* ___U3CnodesU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CanchorsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Map_t1914475838, ___U3CanchorsU3Ek__BackingField_0)); }
	inline AnchorU5BU5D_t2762965888* get_U3CanchorsU3Ek__BackingField_0() const { return ___U3CanchorsU3Ek__BackingField_0; }
	inline AnchorU5BU5D_t2762965888** get_address_of_U3CanchorsU3Ek__BackingField_0() { return &___U3CanchorsU3Ek__BackingField_0; }
	inline void set_U3CanchorsU3Ek__BackingField_0(AnchorU5BU5D_t2762965888* value)
	{
		___U3CanchorsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CanchorsU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CcommentsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Map_t1914475838, ___U3CcommentsU3Ek__BackingField_1)); }
	inline String_t* get_U3CcommentsU3Ek__BackingField_1() const { return ___U3CcommentsU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CcommentsU3Ek__BackingField_1() { return &___U3CcommentsU3Ek__BackingField_1; }
	inline void set_U3CcommentsU3Ek__BackingField_1(String_t* value)
	{
		___U3CcommentsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcommentsU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CconfigU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Map_t1914475838, ___U3CconfigU3Ek__BackingField_2)); }
	inline Config_t1927135816 * get_U3CconfigU3Ek__BackingField_2() const { return ___U3CconfigU3Ek__BackingField_2; }
	inline Config_t1927135816 ** get_address_of_U3CconfigU3Ek__BackingField_2() { return &___U3CconfigU3Ek__BackingField_2; }
	inline void set_U3CconfigU3Ek__BackingField_2(Config_t1927135816 * value)
	{
		___U3CconfigU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CconfigU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CnodesU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Map_t1914475838, ___U3CnodesU3Ek__BackingField_3)); }
	inline NodeU5BU5D_t283985611* get_U3CnodesU3Ek__BackingField_3() const { return ___U3CnodesU3Ek__BackingField_3; }
	inline NodeU5BU5D_t283985611** get_address_of_U3CnodesU3Ek__BackingField_3() { return &___U3CnodesU3Ek__BackingField_3; }
	inline void set_U3CnodesU3Ek__BackingField_3(NodeU5BU5D_t283985611* value)
	{
		___U3CnodesU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnodesU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAP_T1914475838_H
#ifndef RESOLUTION_T3867497320_H
#define RESOLUTION_T3867497320_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Resolution
struct  Resolution_t3867497320  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Map IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Resolution::<map>k__BackingField
	Map_t1914475838 * ___U3CmapU3Ek__BackingField_0;
	// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Solution[] IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Resolution::<solutions>k__BackingField
	SolutionU5BU5D_t2153136770* ___U3CsolutionsU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmapU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Resolution_t3867497320, ___U3CmapU3Ek__BackingField_0)); }
	inline Map_t1914475838 * get_U3CmapU3Ek__BackingField_0() const { return ___U3CmapU3Ek__BackingField_0; }
	inline Map_t1914475838 ** get_address_of_U3CmapU3Ek__BackingField_0() { return &___U3CmapU3Ek__BackingField_0; }
	inline void set_U3CmapU3Ek__BackingField_0(Map_t1914475838 * value)
	{
		___U3CmapU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmapU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CsolutionsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Resolution_t3867497320, ___U3CsolutionsU3Ek__BackingField_1)); }
	inline SolutionU5BU5D_t2153136770* get_U3CsolutionsU3Ek__BackingField_1() const { return ___U3CsolutionsU3Ek__BackingField_1; }
	inline SolutionU5BU5D_t2153136770** get_address_of_U3CsolutionsU3Ek__BackingField_1() { return &___U3CsolutionsU3Ek__BackingField_1; }
	inline void set_U3CsolutionsU3Ek__BackingField_1(SolutionU5BU5D_t2153136770* value)
	{
		___U3CsolutionsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsolutionsU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESOLUTION_T3867497320_H
#ifndef TEXTRECOGTOPLEVELSINGLE_T1839648508_H
#define TEXTRECOGTOPLEVELSINGLE_T1839648508_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.TextRecogTopLevelSingle
struct  TextRecogTopLevelSingle_t1839648508  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.TextRecogTopLevelSingle::<source_url>k__BackingField
	String_t* ___U3Csource_urlU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.TextRecogTopLevelSingle::<resolved_url>k__BackingField
	String_t* ___U3Cresolved_urlU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.TextRecogTopLevelSingle::<image>k__BackingField
	String_t* ___U3CimageU3Ek__BackingField_2;
	// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ErrorInfoNoCode IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.TextRecogTopLevelSingle::<error>k__BackingField
	ErrorInfoNoCode_t2062371116 * ___U3CerrorU3Ek__BackingField_3;
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.TextRecogTopLevelSingle::<text>k__BackingField
	String_t* ___U3CtextU3Ek__BackingField_4;
	// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.TextRecogOneWord[] IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.TextRecogTopLevelSingle::<words>k__BackingField
	TextRecogOneWordU5BU5D_t1008406524* ___U3CwordsU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3Csource_urlU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TextRecogTopLevelSingle_t1839648508, ___U3Csource_urlU3Ek__BackingField_0)); }
	inline String_t* get_U3Csource_urlU3Ek__BackingField_0() const { return ___U3Csource_urlU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3Csource_urlU3Ek__BackingField_0() { return &___U3Csource_urlU3Ek__BackingField_0; }
	inline void set_U3Csource_urlU3Ek__BackingField_0(String_t* value)
	{
		___U3Csource_urlU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Csource_urlU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3Cresolved_urlU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TextRecogTopLevelSingle_t1839648508, ___U3Cresolved_urlU3Ek__BackingField_1)); }
	inline String_t* get_U3Cresolved_urlU3Ek__BackingField_1() const { return ___U3Cresolved_urlU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3Cresolved_urlU3Ek__BackingField_1() { return &___U3Cresolved_urlU3Ek__BackingField_1; }
	inline void set_U3Cresolved_urlU3Ek__BackingField_1(String_t* value)
	{
		___U3Cresolved_urlU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cresolved_urlU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CimageU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(TextRecogTopLevelSingle_t1839648508, ___U3CimageU3Ek__BackingField_2)); }
	inline String_t* get_U3CimageU3Ek__BackingField_2() const { return ___U3CimageU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CimageU3Ek__BackingField_2() { return &___U3CimageU3Ek__BackingField_2; }
	inline void set_U3CimageU3Ek__BackingField_2(String_t* value)
	{
		___U3CimageU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CimageU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CerrorU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(TextRecogTopLevelSingle_t1839648508, ___U3CerrorU3Ek__BackingField_3)); }
	inline ErrorInfoNoCode_t2062371116 * get_U3CerrorU3Ek__BackingField_3() const { return ___U3CerrorU3Ek__BackingField_3; }
	inline ErrorInfoNoCode_t2062371116 ** get_address_of_U3CerrorU3Ek__BackingField_3() { return &___U3CerrorU3Ek__BackingField_3; }
	inline void set_U3CerrorU3Ek__BackingField_3(ErrorInfoNoCode_t2062371116 * value)
	{
		___U3CerrorU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CerrorU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CtextU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(TextRecogTopLevelSingle_t1839648508, ___U3CtextU3Ek__BackingField_4)); }
	inline String_t* get_U3CtextU3Ek__BackingField_4() const { return ___U3CtextU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CtextU3Ek__BackingField_4() { return &___U3CtextU3Ek__BackingField_4; }
	inline void set_U3CtextU3Ek__BackingField_4(String_t* value)
	{
		___U3CtextU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CwordsU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(TextRecogTopLevelSingle_t1839648508, ___U3CwordsU3Ek__BackingField_5)); }
	inline TextRecogOneWordU5BU5D_t1008406524* get_U3CwordsU3Ek__BackingField_5() const { return ___U3CwordsU3Ek__BackingField_5; }
	inline TextRecogOneWordU5BU5D_t1008406524** get_address_of_U3CwordsU3Ek__BackingField_5() { return &___U3CwordsU3Ek__BackingField_5; }
	inline void set_U3CwordsU3Ek__BackingField_5(TextRecogOneWordU5BU5D_t1008406524* value)
	{
		___U3CwordsU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwordsU3Ek__BackingField_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTRECOGTOPLEVELSINGLE_T1839648508_H
#ifndef PARAMS_T3159428742_H
#define PARAMS_T3159428742_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Params
struct  Params_t3159428742  : public RuntimeObject
{
public:
	// System.Double IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Params::<alpha_init>k__BackingField
	double ___U3Calpha_initU3Ek__BackingField_0;
	// System.Double IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Params::<anchor_epoch>k__BackingField
	double ___U3Canchor_epochU3Ek__BackingField_1;
	// System.Double IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Params::<map_size>k__BackingField
	double ___U3Cmap_sizeU3Ek__BackingField_2;
	// System.Double IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Params::<rAnchor>k__BackingField
	double ___U3CrAnchorU3Ek__BackingField_3;
	// System.Double IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Params::<rFinish>k__BackingField
	double ___U3CrFinishU3Ek__BackingField_4;
	// System.Double IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Params::<rInit>k__BackingField
	double ___U3CrInitU3Ek__BackingField_5;
	// System.Double IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Params::<seed>k__BackingField
	double ___U3CseedU3Ek__BackingField_6;
	// System.Double IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Params::<training_period>k__BackingField
	double ___U3Ctraining_periodU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_U3Calpha_initU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Params_t3159428742, ___U3Calpha_initU3Ek__BackingField_0)); }
	inline double get_U3Calpha_initU3Ek__BackingField_0() const { return ___U3Calpha_initU3Ek__BackingField_0; }
	inline double* get_address_of_U3Calpha_initU3Ek__BackingField_0() { return &___U3Calpha_initU3Ek__BackingField_0; }
	inline void set_U3Calpha_initU3Ek__BackingField_0(double value)
	{
		___U3Calpha_initU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3Canchor_epochU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Params_t3159428742, ___U3Canchor_epochU3Ek__BackingField_1)); }
	inline double get_U3Canchor_epochU3Ek__BackingField_1() const { return ___U3Canchor_epochU3Ek__BackingField_1; }
	inline double* get_address_of_U3Canchor_epochU3Ek__BackingField_1() { return &___U3Canchor_epochU3Ek__BackingField_1; }
	inline void set_U3Canchor_epochU3Ek__BackingField_1(double value)
	{
		___U3Canchor_epochU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3Cmap_sizeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Params_t3159428742, ___U3Cmap_sizeU3Ek__BackingField_2)); }
	inline double get_U3Cmap_sizeU3Ek__BackingField_2() const { return ___U3Cmap_sizeU3Ek__BackingField_2; }
	inline double* get_address_of_U3Cmap_sizeU3Ek__BackingField_2() { return &___U3Cmap_sizeU3Ek__BackingField_2; }
	inline void set_U3Cmap_sizeU3Ek__BackingField_2(double value)
	{
		___U3Cmap_sizeU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CrAnchorU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Params_t3159428742, ___U3CrAnchorU3Ek__BackingField_3)); }
	inline double get_U3CrAnchorU3Ek__BackingField_3() const { return ___U3CrAnchorU3Ek__BackingField_3; }
	inline double* get_address_of_U3CrAnchorU3Ek__BackingField_3() { return &___U3CrAnchorU3Ek__BackingField_3; }
	inline void set_U3CrAnchorU3Ek__BackingField_3(double value)
	{
		___U3CrAnchorU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CrFinishU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Params_t3159428742, ___U3CrFinishU3Ek__BackingField_4)); }
	inline double get_U3CrFinishU3Ek__BackingField_4() const { return ___U3CrFinishU3Ek__BackingField_4; }
	inline double* get_address_of_U3CrFinishU3Ek__BackingField_4() { return &___U3CrFinishU3Ek__BackingField_4; }
	inline void set_U3CrFinishU3Ek__BackingField_4(double value)
	{
		___U3CrFinishU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CrInitU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Params_t3159428742, ___U3CrInitU3Ek__BackingField_5)); }
	inline double get_U3CrInitU3Ek__BackingField_5() const { return ___U3CrInitU3Ek__BackingField_5; }
	inline double* get_address_of_U3CrInitU3Ek__BackingField_5() { return &___U3CrInitU3Ek__BackingField_5; }
	inline void set_U3CrInitU3Ek__BackingField_5(double value)
	{
		___U3CrInitU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CseedU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Params_t3159428742, ___U3CseedU3Ek__BackingField_6)); }
	inline double get_U3CseedU3Ek__BackingField_6() const { return ___U3CseedU3Ek__BackingField_6; }
	inline double* get_address_of_U3CseedU3Ek__BackingField_6() { return &___U3CseedU3Ek__BackingField_6; }
	inline void set_U3CseedU3Ek__BackingField_6(double value)
	{
		___U3CseedU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3Ctraining_periodU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(Params_t3159428742, ___U3Ctraining_periodU3Ek__BackingField_7)); }
	inline double get_U3Ctraining_periodU3Ek__BackingField_7() const { return ___U3Ctraining_periodU3Ek__BackingField_7; }
	inline double* get_address_of_U3Ctraining_periodU3Ek__BackingField_7() { return &___U3Ctraining_periodU3Ek__BackingField_7; }
	inline void set_U3Ctraining_periodU3Ek__BackingField_7(double value)
	{
		___U3Ctraining_periodU3Ek__BackingField_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMS_T3159428742_H
#ifndef POSITION_T3597491459_H
#define POSITION_T3597491459_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Position
struct  Position_t3597491459  : public RuntimeObject
{
public:
	// System.Double IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Position::<x>k__BackingField
	double ___U3CxU3Ek__BackingField_0;
	// System.Double IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Position::<y>k__BackingField
	double ___U3CyU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CxU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Position_t3597491459, ___U3CxU3Ek__BackingField_0)); }
	inline double get_U3CxU3Ek__BackingField_0() const { return ___U3CxU3Ek__BackingField_0; }
	inline double* get_address_of_U3CxU3Ek__BackingField_0() { return &___U3CxU3Ek__BackingField_0; }
	inline void set_U3CxU3Ek__BackingField_0(double value)
	{
		___U3CxU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CyU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Position_t3597491459, ___U3CyU3Ek__BackingField_1)); }
	inline double get_U3CyU3Ek__BackingField_1() const { return ___U3CyU3Ek__BackingField_1; }
	inline double* get_address_of_U3CyU3Ek__BackingField_1() { return &___U3CyU3Ek__BackingField_1; }
	inline void set_U3CyU3Ek__BackingField_1(double value)
	{
		___U3CyU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSITION_T3597491459_H
#ifndef CONFIG_T1927135816_H
#define CONFIG_T1927135816_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Config
struct  Config_t1927135816  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Drivers IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Config::<drivers>k__BackingField
	Drivers_t1652706281 * ___U3CdriversU3Ek__BackingField_0;
	// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Params IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Config::<params>k__BackingField
	Params_t3159428742 * ___U3CparamsU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CdriversU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Config_t1927135816, ___U3CdriversU3Ek__BackingField_0)); }
	inline Drivers_t1652706281 * get_U3CdriversU3Ek__BackingField_0() const { return ___U3CdriversU3Ek__BackingField_0; }
	inline Drivers_t1652706281 ** get_address_of_U3CdriversU3Ek__BackingField_0() { return &___U3CdriversU3Ek__BackingField_0; }
	inline void set_U3CdriversU3Ek__BackingField_0(Drivers_t1652706281 * value)
	{
		___U3CdriversU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdriversU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CparamsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Config_t1927135816, ___U3CparamsU3Ek__BackingField_1)); }
	inline Params_t3159428742 * get_U3CparamsU3Ek__BackingField_1() const { return ___U3CparamsU3Ek__BackingField_1; }
	inline Params_t3159428742 ** get_address_of_U3CparamsU3Ek__BackingField_1() { return &___U3CparamsU3Ek__BackingField_1; }
	inline void set_U3CparamsU3Ek__BackingField_1(Params_t3159428742 * value)
	{
		___U3CparamsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CparamsU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIG_T1927135816_H
#ifndef APPLICATIONDATAVALUE_T1721671971_H
#define APPLICATIONDATAVALUE_T1721671971_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.ApplicationDataValue
struct  ApplicationDataValue_t1721671971  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPLICATIONDATAVALUE_T1721671971_H
#ifndef RANGE_T2396059803_H
#define RANGE_T2396059803_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Range
struct  Range_t2396059803  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANGE_T2396059803_H
#ifndef RECOGNIZETEXTPARAMETERS_T1331035907_H
#define RECOGNIZETEXTPARAMETERS_T1331035907_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.RecognizeTextParameters
struct  RecognizeTextParameters_t1331035907  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.RecognizeTextParameters::<url>k__BackingField
	String_t* ___U3CurlU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CurlU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RecognizeTextParameters_t1331035907, ___U3CurlU3Ek__BackingField_0)); }
	inline String_t* get_U3CurlU3Ek__BackingField_0() const { return ___U3CurlU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CurlU3Ek__BackingField_0() { return &___U3CurlU3Ek__BackingField_0; }
	inline void set_U3CurlU3Ek__BackingField_0(String_t* value)
	{
		___U3CurlU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CurlU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECOGNIZETEXTPARAMETERS_T1331035907_H
#ifndef PROBLEM_T2814813345_H
#define PROBLEM_T2814813345_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Problem
struct  Problem_t2814813345  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column[] IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Problem::<columns>k__BackingField
	ColumnU5BU5D_t4200576441* ___U3CcolumnsU3Ek__BackingField_0;
	// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Option[] IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Problem::<options>k__BackingField
	OptionU5BU5D_t3811736648* ___U3CoptionsU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Problem::<subject>k__BackingField
	String_t* ___U3CsubjectU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CcolumnsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Problem_t2814813345, ___U3CcolumnsU3Ek__BackingField_0)); }
	inline ColumnU5BU5D_t4200576441* get_U3CcolumnsU3Ek__BackingField_0() const { return ___U3CcolumnsU3Ek__BackingField_0; }
	inline ColumnU5BU5D_t4200576441** get_address_of_U3CcolumnsU3Ek__BackingField_0() { return &___U3CcolumnsU3Ek__BackingField_0; }
	inline void set_U3CcolumnsU3Ek__BackingField_0(ColumnU5BU5D_t4200576441* value)
	{
		___U3CcolumnsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcolumnsU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CoptionsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Problem_t2814813345, ___U3CoptionsU3Ek__BackingField_1)); }
	inline OptionU5BU5D_t3811736648* get_U3CoptionsU3Ek__BackingField_1() const { return ___U3CoptionsU3Ek__BackingField_1; }
	inline OptionU5BU5D_t3811736648** get_address_of_U3CoptionsU3Ek__BackingField_1() { return &___U3CoptionsU3Ek__BackingField_1; }
	inline void set_U3CoptionsU3Ek__BackingField_1(OptionU5BU5D_t3811736648* value)
	{
		___U3CoptionsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CoptionsU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CsubjectU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Problem_t2814813345, ___U3CsubjectU3Ek__BackingField_2)); }
	inline String_t* get_U3CsubjectU3Ek__BackingField_2() const { return ___U3CsubjectU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CsubjectU3Ek__BackingField_2() { return &___U3CsubjectU3Ek__BackingField_2; }
	inline void set_U3CsubjectU3Ek__BackingField_2(String_t* value)
	{
		___U3CsubjectU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsubjectU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROBLEM_T2814813345_H
#ifndef COLUMN_T2612486824_H
#define COLUMN_T2612486824_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column
struct  Column_t2612486824  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column::<key>k__BackingField
	String_t* ___U3CkeyU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column::<type>k__BackingField
	String_t* ___U3CtypeU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column::<description>k__BackingField
	String_t* ___U3CdescriptionU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column::<format>k__BackingField
	String_t* ___U3CformatU3Ek__BackingField_3;
	// System.String IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column::<full_name>k__BackingField
	String_t* ___U3Cfull_nameU3Ek__BackingField_4;
	// System.String IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column::<goal>k__BackingField
	String_t* ___U3CgoalU3Ek__BackingField_5;
	// System.Int32 IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column::<insignificant_loss>k__BackingField
	int32_t ___U3Cinsignificant_lossU3Ek__BackingField_6;
	// System.Boolean IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column::<is_objective>k__BackingField
	bool ___U3Cis_objectiveU3Ek__BackingField_7;
	// System.String[] IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column::<preference>k__BackingField
	StringU5BU5D_t1642385972* ___U3CpreferenceU3Ek__BackingField_8;
	// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Range IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column::<range>k__BackingField
	Range_t2396059803 * ___U3CrangeU3Ek__BackingField_9;
	// System.Int64 IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column::<significant_gain>k__BackingField
	int64_t ___U3Csignificant_gainU3Ek__BackingField_10;
	// System.Int64 IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column::<significant_loss>k__BackingField
	int64_t ___U3Csignificant_lossU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_U3CkeyU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Column_t2612486824, ___U3CkeyU3Ek__BackingField_0)); }
	inline String_t* get_U3CkeyU3Ek__BackingField_0() const { return ___U3CkeyU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CkeyU3Ek__BackingField_0() { return &___U3CkeyU3Ek__BackingField_0; }
	inline void set_U3CkeyU3Ek__BackingField_0(String_t* value)
	{
		___U3CkeyU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CkeyU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CtypeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Column_t2612486824, ___U3CtypeU3Ek__BackingField_1)); }
	inline String_t* get_U3CtypeU3Ek__BackingField_1() const { return ___U3CtypeU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CtypeU3Ek__BackingField_1() { return &___U3CtypeU3Ek__BackingField_1; }
	inline void set_U3CtypeU3Ek__BackingField_1(String_t* value)
	{
		___U3CtypeU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtypeU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CdescriptionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Column_t2612486824, ___U3CdescriptionU3Ek__BackingField_2)); }
	inline String_t* get_U3CdescriptionU3Ek__BackingField_2() const { return ___U3CdescriptionU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CdescriptionU3Ek__BackingField_2() { return &___U3CdescriptionU3Ek__BackingField_2; }
	inline void set_U3CdescriptionU3Ek__BackingField_2(String_t* value)
	{
		___U3CdescriptionU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdescriptionU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CformatU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Column_t2612486824, ___U3CformatU3Ek__BackingField_3)); }
	inline String_t* get_U3CformatU3Ek__BackingField_3() const { return ___U3CformatU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CformatU3Ek__BackingField_3() { return &___U3CformatU3Ek__BackingField_3; }
	inline void set_U3CformatU3Ek__BackingField_3(String_t* value)
	{
		___U3CformatU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CformatU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3Cfull_nameU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Column_t2612486824, ___U3Cfull_nameU3Ek__BackingField_4)); }
	inline String_t* get_U3Cfull_nameU3Ek__BackingField_4() const { return ___U3Cfull_nameU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3Cfull_nameU3Ek__BackingField_4() { return &___U3Cfull_nameU3Ek__BackingField_4; }
	inline void set_U3Cfull_nameU3Ek__BackingField_4(String_t* value)
	{
		___U3Cfull_nameU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cfull_nameU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CgoalU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Column_t2612486824, ___U3CgoalU3Ek__BackingField_5)); }
	inline String_t* get_U3CgoalU3Ek__BackingField_5() const { return ___U3CgoalU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CgoalU3Ek__BackingField_5() { return &___U3CgoalU3Ek__BackingField_5; }
	inline void set_U3CgoalU3Ek__BackingField_5(String_t* value)
	{
		___U3CgoalU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CgoalU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3Cinsignificant_lossU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Column_t2612486824, ___U3Cinsignificant_lossU3Ek__BackingField_6)); }
	inline int32_t get_U3Cinsignificant_lossU3Ek__BackingField_6() const { return ___U3Cinsignificant_lossU3Ek__BackingField_6; }
	inline int32_t* get_address_of_U3Cinsignificant_lossU3Ek__BackingField_6() { return &___U3Cinsignificant_lossU3Ek__BackingField_6; }
	inline void set_U3Cinsignificant_lossU3Ek__BackingField_6(int32_t value)
	{
		___U3Cinsignificant_lossU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3Cis_objectiveU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(Column_t2612486824, ___U3Cis_objectiveU3Ek__BackingField_7)); }
	inline bool get_U3Cis_objectiveU3Ek__BackingField_7() const { return ___U3Cis_objectiveU3Ek__BackingField_7; }
	inline bool* get_address_of_U3Cis_objectiveU3Ek__BackingField_7() { return &___U3Cis_objectiveU3Ek__BackingField_7; }
	inline void set_U3Cis_objectiveU3Ek__BackingField_7(bool value)
	{
		___U3Cis_objectiveU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CpreferenceU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(Column_t2612486824, ___U3CpreferenceU3Ek__BackingField_8)); }
	inline StringU5BU5D_t1642385972* get_U3CpreferenceU3Ek__BackingField_8() const { return ___U3CpreferenceU3Ek__BackingField_8; }
	inline StringU5BU5D_t1642385972** get_address_of_U3CpreferenceU3Ek__BackingField_8() { return &___U3CpreferenceU3Ek__BackingField_8; }
	inline void set_U3CpreferenceU3Ek__BackingField_8(StringU5BU5D_t1642385972* value)
	{
		___U3CpreferenceU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpreferenceU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CrangeU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(Column_t2612486824, ___U3CrangeU3Ek__BackingField_9)); }
	inline Range_t2396059803 * get_U3CrangeU3Ek__BackingField_9() const { return ___U3CrangeU3Ek__BackingField_9; }
	inline Range_t2396059803 ** get_address_of_U3CrangeU3Ek__BackingField_9() { return &___U3CrangeU3Ek__BackingField_9; }
	inline void set_U3CrangeU3Ek__BackingField_9(Range_t2396059803 * value)
	{
		___U3CrangeU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrangeU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3Csignificant_gainU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(Column_t2612486824, ___U3Csignificant_gainU3Ek__BackingField_10)); }
	inline int64_t get_U3Csignificant_gainU3Ek__BackingField_10() const { return ___U3Csignificant_gainU3Ek__BackingField_10; }
	inline int64_t* get_address_of_U3Csignificant_gainU3Ek__BackingField_10() { return &___U3Csignificant_gainU3Ek__BackingField_10; }
	inline void set_U3Csignificant_gainU3Ek__BackingField_10(int64_t value)
	{
		___U3Csignificant_gainU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3Csignificant_lossU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(Column_t2612486824, ___U3Csignificant_lossU3Ek__BackingField_11)); }
	inline int64_t get_U3Csignificant_lossU3Ek__BackingField_11() const { return ___U3Csignificant_lossU3Ek__BackingField_11; }
	inline int64_t* get_address_of_U3Csignificant_lossU3Ek__BackingField_11() { return &___U3Csignificant_lossU3Ek__BackingField_11; }
	inline void set_U3Csignificant_lossU3Ek__BackingField_11(int64_t value)
	{
		___U3Csignificant_lossU3Ek__BackingField_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLUMN_T2612486824_H
#ifndef OPTION_T1775127045_H
#define OPTION_T1775127045_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Option
struct  Option_t1775127045  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.ApplicationData IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Option::<app_data>k__BackingField
	ApplicationData_t1885408412 * ___U3Capp_dataU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Option::<description_html>k__BackingField
	String_t* ___U3Cdescription_htmlU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Option::<key>k__BackingField
	String_t* ___U3CkeyU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Option::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_3;
	// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.ApplicationDataValue IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Option::<values>k__BackingField
	ApplicationDataValue_t1721671971 * ___U3CvaluesU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3Capp_dataU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Option_t1775127045, ___U3Capp_dataU3Ek__BackingField_0)); }
	inline ApplicationData_t1885408412 * get_U3Capp_dataU3Ek__BackingField_0() const { return ___U3Capp_dataU3Ek__BackingField_0; }
	inline ApplicationData_t1885408412 ** get_address_of_U3Capp_dataU3Ek__BackingField_0() { return &___U3Capp_dataU3Ek__BackingField_0; }
	inline void set_U3Capp_dataU3Ek__BackingField_0(ApplicationData_t1885408412 * value)
	{
		___U3Capp_dataU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Capp_dataU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3Cdescription_htmlU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Option_t1775127045, ___U3Cdescription_htmlU3Ek__BackingField_1)); }
	inline String_t* get_U3Cdescription_htmlU3Ek__BackingField_1() const { return ___U3Cdescription_htmlU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3Cdescription_htmlU3Ek__BackingField_1() { return &___U3Cdescription_htmlU3Ek__BackingField_1; }
	inline void set_U3Cdescription_htmlU3Ek__BackingField_1(String_t* value)
	{
		___U3Cdescription_htmlU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cdescription_htmlU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CkeyU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Option_t1775127045, ___U3CkeyU3Ek__BackingField_2)); }
	inline String_t* get_U3CkeyU3Ek__BackingField_2() const { return ___U3CkeyU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CkeyU3Ek__BackingField_2() { return &___U3CkeyU3Ek__BackingField_2; }
	inline void set_U3CkeyU3Ek__BackingField_2(String_t* value)
	{
		___U3CkeyU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CkeyU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Option_t1775127045, ___U3CnameU3Ek__BackingField_3)); }
	inline String_t* get_U3CnameU3Ek__BackingField_3() const { return ___U3CnameU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_3() { return &___U3CnameU3Ek__BackingField_3; }
	inline void set_U3CnameU3Ek__BackingField_3(String_t* value)
	{
		___U3CnameU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CvaluesU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Option_t1775127045, ___U3CvaluesU3Ek__BackingField_4)); }
	inline ApplicationDataValue_t1721671971 * get_U3CvaluesU3Ek__BackingField_4() const { return ___U3CvaluesU3Ek__BackingField_4; }
	inline ApplicationDataValue_t1721671971 ** get_address_of_U3CvaluesU3Ek__BackingField_4() { return &___U3CvaluesU3Ek__BackingField_4; }
	inline void set_U3CvaluesU3Ek__BackingField_4(ApplicationDataValue_t1721671971 * value)
	{
		___U3CvaluesU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CvaluesU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPTION_T1775127045_H
#ifndef APPLICATIONDATA_T1885408412_H
#define APPLICATIONDATA_T1885408412_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.ApplicationData
struct  ApplicationData_t1885408412  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPLICATIONDATA_T1885408412_H
#ifndef LOCATION_T328870245_H
#define LOCATION_T328870245_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.Location
struct  Location_t328870245  : public RuntimeObject
{
public:
	// System.Double IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.Location::<width>k__BackingField
	double ___U3CwidthU3Ek__BackingField_0;
	// System.Double IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.Location::<height>k__BackingField
	double ___U3CheightU3Ek__BackingField_1;
	// System.Double IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.Location::<left>k__BackingField
	double ___U3CleftU3Ek__BackingField_2;
	// System.Double IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.Location::<top>k__BackingField
	double ___U3CtopU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CwidthU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Location_t328870245, ___U3CwidthU3Ek__BackingField_0)); }
	inline double get_U3CwidthU3Ek__BackingField_0() const { return ___U3CwidthU3Ek__BackingField_0; }
	inline double* get_address_of_U3CwidthU3Ek__BackingField_0() { return &___U3CwidthU3Ek__BackingField_0; }
	inline void set_U3CwidthU3Ek__BackingField_0(double value)
	{
		___U3CwidthU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CheightU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Location_t328870245, ___U3CheightU3Ek__BackingField_1)); }
	inline double get_U3CheightU3Ek__BackingField_1() const { return ___U3CheightU3Ek__BackingField_1; }
	inline double* get_address_of_U3CheightU3Ek__BackingField_1() { return &___U3CheightU3Ek__BackingField_1; }
	inline void set_U3CheightU3Ek__BackingField_1(double value)
	{
		___U3CheightU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CleftU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Location_t328870245, ___U3CleftU3Ek__BackingField_2)); }
	inline double get_U3CleftU3Ek__BackingField_2() const { return ___U3CleftU3Ek__BackingField_2; }
	inline double* get_address_of_U3CleftU3Ek__BackingField_2() { return &___U3CleftU3Ek__BackingField_2; }
	inline void set_U3CleftU3Ek__BackingField_2(double value)
	{
		___U3CleftU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CtopU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Location_t328870245, ___U3CtopU3Ek__BackingField_3)); }
	inline double get_U3CtopU3Ek__BackingField_3() const { return ___U3CtopU3Ek__BackingField_3; }
	inline double* get_address_of_U3CtopU3Ek__BackingField_3() { return &___U3CtopU3Ek__BackingField_3; }
	inline void set_U3CtopU3Ek__BackingField_3(double value)
	{
		___U3CtopU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCATION_T328870245_H
#ifndef TEXTRECOGONEWORD_T2687074657_H
#define TEXTRECOGONEWORD_T2687074657_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.TextRecogOneWord
struct  TextRecogOneWord_t2687074657  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.TextRecogOneWord::<word>k__BackingField
	String_t* ___U3CwordU3Ek__BackingField_0;
	// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.Location IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.TextRecogOneWord::<location>k__BackingField
	Location_t328870245 * ___U3ClocationU3Ek__BackingField_1;
	// System.Double IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.TextRecogOneWord::<score>k__BackingField
	double ___U3CscoreU3Ek__BackingField_2;
	// System.Double IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.TextRecogOneWord::<line_number>k__BackingField
	double ___U3Cline_numberU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CwordU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TextRecogOneWord_t2687074657, ___U3CwordU3Ek__BackingField_0)); }
	inline String_t* get_U3CwordU3Ek__BackingField_0() const { return ___U3CwordU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CwordU3Ek__BackingField_0() { return &___U3CwordU3Ek__BackingField_0; }
	inline void set_U3CwordU3Ek__BackingField_0(String_t* value)
	{
		___U3CwordU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwordU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3ClocationU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TextRecogOneWord_t2687074657, ___U3ClocationU3Ek__BackingField_1)); }
	inline Location_t328870245 * get_U3ClocationU3Ek__BackingField_1() const { return ___U3ClocationU3Ek__BackingField_1; }
	inline Location_t328870245 ** get_address_of_U3ClocationU3Ek__BackingField_1() { return &___U3ClocationU3Ek__BackingField_1; }
	inline void set_U3ClocationU3Ek__BackingField_1(Location_t328870245 * value)
	{
		___U3ClocationU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClocationU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CscoreU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(TextRecogOneWord_t2687074657, ___U3CscoreU3Ek__BackingField_2)); }
	inline double get_U3CscoreU3Ek__BackingField_2() const { return ___U3CscoreU3Ek__BackingField_2; }
	inline double* get_address_of_U3CscoreU3Ek__BackingField_2() { return &___U3CscoreU3Ek__BackingField_2; }
	inline void set_U3CscoreU3Ek__BackingField_2(double value)
	{
		___U3CscoreU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3Cline_numberU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(TextRecogOneWord_t2687074657, ___U3Cline_numberU3Ek__BackingField_3)); }
	inline double get_U3Cline_numberU3Ek__BackingField_3() const { return ___U3Cline_numberU3Ek__BackingField_3; }
	inline double* get_address_of_U3Cline_numberU3Ek__BackingField_3() { return &___U3Cline_numberU3Ek__BackingField_3; }
	inline void set_U3Cline_numberU3Ek__BackingField_3(double value)
	{
		___U3Cline_numberU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTRECOGONEWORD_T2687074657_H
#ifndef CLASSIFYTOPLEVELMULTIPLE_T317282463_H
#define CLASSIFYTOPLEVELMULTIPLE_T317282463_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyTopLevelMultiple
struct  ClassifyTopLevelMultiple_t317282463  : public RuntimeObject
{
public:
	// System.Int32 IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyTopLevelMultiple::<images_processed>k__BackingField
	int32_t ___U3Cimages_processedU3Ek__BackingField_0;
	// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyTopLevelSingle[] IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyTopLevelMultiple::<images>k__BackingField
	ClassifyTopLevelSingleU5BU5D_t3685983982* ___U3CimagesU3Ek__BackingField_1;
	// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.WarningInfo[] IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyTopLevelMultiple::<warnings>k__BackingField
	WarningInfoU5BU5D_t1932079499* ___U3CwarningsU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3Cimages_processedU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ClassifyTopLevelMultiple_t317282463, ___U3Cimages_processedU3Ek__BackingField_0)); }
	inline int32_t get_U3Cimages_processedU3Ek__BackingField_0() const { return ___U3Cimages_processedU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3Cimages_processedU3Ek__BackingField_0() { return &___U3Cimages_processedU3Ek__BackingField_0; }
	inline void set_U3Cimages_processedU3Ek__BackingField_0(int32_t value)
	{
		___U3Cimages_processedU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CimagesU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ClassifyTopLevelMultiple_t317282463, ___U3CimagesU3Ek__BackingField_1)); }
	inline ClassifyTopLevelSingleU5BU5D_t3685983982* get_U3CimagesU3Ek__BackingField_1() const { return ___U3CimagesU3Ek__BackingField_1; }
	inline ClassifyTopLevelSingleU5BU5D_t3685983982** get_address_of_U3CimagesU3Ek__BackingField_1() { return &___U3CimagesU3Ek__BackingField_1; }
	inline void set_U3CimagesU3Ek__BackingField_1(ClassifyTopLevelSingleU5BU5D_t3685983982* value)
	{
		___U3CimagesU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CimagesU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CwarningsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ClassifyTopLevelMultiple_t317282463, ___U3CwarningsU3Ek__BackingField_2)); }
	inline WarningInfoU5BU5D_t1932079499* get_U3CwarningsU3Ek__BackingField_2() const { return ___U3CwarningsU3Ek__BackingField_2; }
	inline WarningInfoU5BU5D_t1932079499** get_address_of_U3CwarningsU3Ek__BackingField_2() { return &___U3CwarningsU3Ek__BackingField_2; }
	inline void set_U3CwarningsU3Ek__BackingField_2(WarningInfoU5BU5D_t1932079499* value)
	{
		___U3CwarningsU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwarningsU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLASSIFYTOPLEVELMULTIPLE_T317282463_H
#ifndef CLASSIFYTOPLEVELSINGLE_T3733844951_H
#define CLASSIFYTOPLEVELSINGLE_T3733844951_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyTopLevelSingle
struct  ClassifyTopLevelSingle_t3733844951  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyTopLevelSingle::<source_url>k__BackingField
	String_t* ___U3Csource_urlU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyTopLevelSingle::<resolved_url>k__BackingField
	String_t* ___U3Cresolved_urlU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyTopLevelSingle::<image>k__BackingField
	String_t* ___U3CimageU3Ek__BackingField_2;
	// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ErrorInfoNoCode IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyTopLevelSingle::<error>k__BackingField
	ErrorInfoNoCode_t2062371116 * ___U3CerrorU3Ek__BackingField_3;
	// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyPerClassifier[] IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyTopLevelSingle::<classifiers>k__BackingField
	ClassifyPerClassifierU5BU5D_t1895762155* ___U3CclassifiersU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3Csource_urlU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ClassifyTopLevelSingle_t3733844951, ___U3Csource_urlU3Ek__BackingField_0)); }
	inline String_t* get_U3Csource_urlU3Ek__BackingField_0() const { return ___U3Csource_urlU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3Csource_urlU3Ek__BackingField_0() { return &___U3Csource_urlU3Ek__BackingField_0; }
	inline void set_U3Csource_urlU3Ek__BackingField_0(String_t* value)
	{
		___U3Csource_urlU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Csource_urlU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3Cresolved_urlU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ClassifyTopLevelSingle_t3733844951, ___U3Cresolved_urlU3Ek__BackingField_1)); }
	inline String_t* get_U3Cresolved_urlU3Ek__BackingField_1() const { return ___U3Cresolved_urlU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3Cresolved_urlU3Ek__BackingField_1() { return &___U3Cresolved_urlU3Ek__BackingField_1; }
	inline void set_U3Cresolved_urlU3Ek__BackingField_1(String_t* value)
	{
		___U3Cresolved_urlU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cresolved_urlU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CimageU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ClassifyTopLevelSingle_t3733844951, ___U3CimageU3Ek__BackingField_2)); }
	inline String_t* get_U3CimageU3Ek__BackingField_2() const { return ___U3CimageU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CimageU3Ek__BackingField_2() { return &___U3CimageU3Ek__BackingField_2; }
	inline void set_U3CimageU3Ek__BackingField_2(String_t* value)
	{
		___U3CimageU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CimageU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CerrorU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ClassifyTopLevelSingle_t3733844951, ___U3CerrorU3Ek__BackingField_3)); }
	inline ErrorInfoNoCode_t2062371116 * get_U3CerrorU3Ek__BackingField_3() const { return ___U3CerrorU3Ek__BackingField_3; }
	inline ErrorInfoNoCode_t2062371116 ** get_address_of_U3CerrorU3Ek__BackingField_3() { return &___U3CerrorU3Ek__BackingField_3; }
	inline void set_U3CerrorU3Ek__BackingField_3(ErrorInfoNoCode_t2062371116 * value)
	{
		___U3CerrorU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CerrorU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CclassifiersU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(ClassifyTopLevelSingle_t3733844951, ___U3CclassifiersU3Ek__BackingField_4)); }
	inline ClassifyPerClassifierU5BU5D_t1895762155* get_U3CclassifiersU3Ek__BackingField_4() const { return ___U3CclassifiersU3Ek__BackingField_4; }
	inline ClassifyPerClassifierU5BU5D_t1895762155** get_address_of_U3CclassifiersU3Ek__BackingField_4() { return &___U3CclassifiersU3Ek__BackingField_4; }
	inline void set_U3CclassifiersU3Ek__BackingField_4(ClassifyPerClassifierU5BU5D_t1895762155* value)
	{
		___U3CclassifiersU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CclassifiersU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLASSIFYTOPLEVELSINGLE_T3733844951_H
#ifndef FACESTOPLEVELSINGLE_T2140351533_H
#define FACESTOPLEVELSINGLE_T2140351533_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.FacesTopLevelSingle
struct  FacesTopLevelSingle_t2140351533  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.FacesTopLevelSingle::<source_url>k__BackingField
	String_t* ___U3Csource_urlU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.FacesTopLevelSingle::<resolved_url>k__BackingField
	String_t* ___U3Cresolved_urlU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.FacesTopLevelSingle::<image>k__BackingField
	String_t* ___U3CimageU3Ek__BackingField_2;
	// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ErrorInfoNoCode IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.FacesTopLevelSingle::<error>k__BackingField
	ErrorInfoNoCode_t2062371116 * ___U3CerrorU3Ek__BackingField_3;
	// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.OneFaceResult[] IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.FacesTopLevelSingle::<faces>k__BackingField
	OneFaceResultU5BU5D_t4008704067* ___U3CfacesU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3Csource_urlU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(FacesTopLevelSingle_t2140351533, ___U3Csource_urlU3Ek__BackingField_0)); }
	inline String_t* get_U3Csource_urlU3Ek__BackingField_0() const { return ___U3Csource_urlU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3Csource_urlU3Ek__BackingField_0() { return &___U3Csource_urlU3Ek__BackingField_0; }
	inline void set_U3Csource_urlU3Ek__BackingField_0(String_t* value)
	{
		___U3Csource_urlU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Csource_urlU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3Cresolved_urlU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(FacesTopLevelSingle_t2140351533, ___U3Cresolved_urlU3Ek__BackingField_1)); }
	inline String_t* get_U3Cresolved_urlU3Ek__BackingField_1() const { return ___U3Cresolved_urlU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3Cresolved_urlU3Ek__BackingField_1() { return &___U3Cresolved_urlU3Ek__BackingField_1; }
	inline void set_U3Cresolved_urlU3Ek__BackingField_1(String_t* value)
	{
		___U3Cresolved_urlU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cresolved_urlU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CimageU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(FacesTopLevelSingle_t2140351533, ___U3CimageU3Ek__BackingField_2)); }
	inline String_t* get_U3CimageU3Ek__BackingField_2() const { return ___U3CimageU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CimageU3Ek__BackingField_2() { return &___U3CimageU3Ek__BackingField_2; }
	inline void set_U3CimageU3Ek__BackingField_2(String_t* value)
	{
		___U3CimageU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CimageU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CerrorU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(FacesTopLevelSingle_t2140351533, ___U3CerrorU3Ek__BackingField_3)); }
	inline ErrorInfoNoCode_t2062371116 * get_U3CerrorU3Ek__BackingField_3() const { return ___U3CerrorU3Ek__BackingField_3; }
	inline ErrorInfoNoCode_t2062371116 ** get_address_of_U3CerrorU3Ek__BackingField_3() { return &___U3CerrorU3Ek__BackingField_3; }
	inline void set_U3CerrorU3Ek__BackingField_3(ErrorInfoNoCode_t2062371116 * value)
	{
		___U3CerrorU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CerrorU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CfacesU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(FacesTopLevelSingle_t2140351533, ___U3CfacesU3Ek__BackingField_4)); }
	inline OneFaceResultU5BU5D_t4008704067* get_U3CfacesU3Ek__BackingField_4() const { return ___U3CfacesU3Ek__BackingField_4; }
	inline OneFaceResultU5BU5D_t4008704067** get_address_of_U3CfacesU3Ek__BackingField_4() { return &___U3CfacesU3Ek__BackingField_4; }
	inline void set_U3CfacesU3Ek__BackingField_4(OneFaceResultU5BU5D_t4008704067* value)
	{
		___U3CfacesU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CfacesU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACESTOPLEVELSINGLE_T2140351533_H
#ifndef CHECKSERVICESTATUS_T584740610_H
#define CHECKSERVICESTATUS_T584740610_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics/CheckServiceStatus
struct  CheckServiceStatus_t584740610  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics/CheckServiceStatus::m_Service
	TradeoffAnalytics_t100002595 * ___m_Service_0;
	// IBM.Watson.DeveloperCloud.Services.ServiceStatus IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics/CheckServiceStatus::m_Callback
	ServiceStatus_t1443707987 * ___m_Callback_1;

public:
	inline static int32_t get_offset_of_m_Service_0() { return static_cast<int32_t>(offsetof(CheckServiceStatus_t584740610, ___m_Service_0)); }
	inline TradeoffAnalytics_t100002595 * get_m_Service_0() const { return ___m_Service_0; }
	inline TradeoffAnalytics_t100002595 ** get_address_of_m_Service_0() { return &___m_Service_0; }
	inline void set_m_Service_0(TradeoffAnalytics_t100002595 * value)
	{
		___m_Service_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Service_0), value);
	}

	inline static int32_t get_offset_of_m_Callback_1() { return static_cast<int32_t>(offsetof(CheckServiceStatus_t584740610, ___m_Callback_1)); }
	inline ServiceStatus_t1443707987 * get_m_Callback_1() const { return ___m_Callback_1; }
	inline ServiceStatus_t1443707987 ** get_address_of_m_Callback_1() { return &___m_Callback_1; }
	inline void set_m_Callback_1(ServiceStatus_t1443707987 * value)
	{
		___m_Callback_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Callback_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHECKSERVICESTATUS_T584740610_H
#ifndef CLASSIFYPARAMETERS_T933043320_H
#define CLASSIFYPARAMETERS_T933043320_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyParameters
struct  ClassifyParameters_t933043320  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyParameters::<url>k__BackingField
	String_t* ___U3CurlU3Ek__BackingField_0;
	// System.String[] IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyParameters::<classifier_ids>k__BackingField
	StringU5BU5D_t1642385972* ___U3Cclassifier_idsU3Ek__BackingField_1;
	// System.String[] IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyParameters::<owners>k__BackingField
	StringU5BU5D_t1642385972* ___U3CownersU3Ek__BackingField_2;
	// System.Single IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyParameters::<threshold>k__BackingField
	float ___U3CthresholdU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CurlU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ClassifyParameters_t933043320, ___U3CurlU3Ek__BackingField_0)); }
	inline String_t* get_U3CurlU3Ek__BackingField_0() const { return ___U3CurlU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CurlU3Ek__BackingField_0() { return &___U3CurlU3Ek__BackingField_0; }
	inline void set_U3CurlU3Ek__BackingField_0(String_t* value)
	{
		___U3CurlU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CurlU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3Cclassifier_idsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ClassifyParameters_t933043320, ___U3Cclassifier_idsU3Ek__BackingField_1)); }
	inline StringU5BU5D_t1642385972* get_U3Cclassifier_idsU3Ek__BackingField_1() const { return ___U3Cclassifier_idsU3Ek__BackingField_1; }
	inline StringU5BU5D_t1642385972** get_address_of_U3Cclassifier_idsU3Ek__BackingField_1() { return &___U3Cclassifier_idsU3Ek__BackingField_1; }
	inline void set_U3Cclassifier_idsU3Ek__BackingField_1(StringU5BU5D_t1642385972* value)
	{
		___U3Cclassifier_idsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cclassifier_idsU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CownersU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ClassifyParameters_t933043320, ___U3CownersU3Ek__BackingField_2)); }
	inline StringU5BU5D_t1642385972* get_U3CownersU3Ek__BackingField_2() const { return ___U3CownersU3Ek__BackingField_2; }
	inline StringU5BU5D_t1642385972** get_address_of_U3CownersU3Ek__BackingField_2() { return &___U3CownersU3Ek__BackingField_2; }
	inline void set_U3CownersU3Ek__BackingField_2(StringU5BU5D_t1642385972* value)
	{
		___U3CownersU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CownersU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CthresholdU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ClassifyParameters_t933043320, ___U3CthresholdU3Ek__BackingField_3)); }
	inline float get_U3CthresholdU3Ek__BackingField_3() const { return ___U3CthresholdU3Ek__BackingField_3; }
	inline float* get_address_of_U3CthresholdU3Ek__BackingField_3() { return &___U3CthresholdU3Ek__BackingField_3; }
	inline void set_U3CthresholdU3Ek__BackingField_3(float value)
	{
		___U3CthresholdU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLASSIFYPARAMETERS_T933043320_H
#ifndef FACESTOPLEVELMULTIPLE_T351289709_H
#define FACESTOPLEVELMULTIPLE_T351289709_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.FacesTopLevelMultiple
struct  FacesTopLevelMultiple_t351289709  : public RuntimeObject
{
public:
	// System.Int32 IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.FacesTopLevelMultiple::<images_processed>k__BackingField
	int32_t ___U3Cimages_processedU3Ek__BackingField_0;
	// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.FacesTopLevelSingle[] IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.FacesTopLevelMultiple::<images>k__BackingField
	FacesTopLevelSingleU5BU5D_t1422738560* ___U3CimagesU3Ek__BackingField_1;
	// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.WarningInfo[] IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.FacesTopLevelMultiple::<warnings>k__BackingField
	WarningInfoU5BU5D_t1932079499* ___U3CwarningsU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3Cimages_processedU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(FacesTopLevelMultiple_t351289709, ___U3Cimages_processedU3Ek__BackingField_0)); }
	inline int32_t get_U3Cimages_processedU3Ek__BackingField_0() const { return ___U3Cimages_processedU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3Cimages_processedU3Ek__BackingField_0() { return &___U3Cimages_processedU3Ek__BackingField_0; }
	inline void set_U3Cimages_processedU3Ek__BackingField_0(int32_t value)
	{
		___U3Cimages_processedU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CimagesU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(FacesTopLevelMultiple_t351289709, ___U3CimagesU3Ek__BackingField_1)); }
	inline FacesTopLevelSingleU5BU5D_t1422738560* get_U3CimagesU3Ek__BackingField_1() const { return ___U3CimagesU3Ek__BackingField_1; }
	inline FacesTopLevelSingleU5BU5D_t1422738560** get_address_of_U3CimagesU3Ek__BackingField_1() { return &___U3CimagesU3Ek__BackingField_1; }
	inline void set_U3CimagesU3Ek__BackingField_1(FacesTopLevelSingleU5BU5D_t1422738560* value)
	{
		___U3CimagesU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CimagesU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CwarningsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(FacesTopLevelMultiple_t351289709, ___U3CwarningsU3Ek__BackingField_2)); }
	inline WarningInfoU5BU5D_t1932079499* get_U3CwarningsU3Ek__BackingField_2() const { return ___U3CwarningsU3Ek__BackingField_2; }
	inline WarningInfoU5BU5D_t1932079499** get_address_of_U3CwarningsU3Ek__BackingField_2() { return &___U3CwarningsU3Ek__BackingField_2; }
	inline void set_U3CwarningsU3Ek__BackingField_2(WarningInfoU5BU5D_t1932079499* value)
	{
		___U3CwarningsU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwarningsU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACESTOPLEVELMULTIPLE_T351289709_H
#ifndef CLASSIFYPERCLASSIFIER_T2357796318_H
#define CLASSIFYPERCLASSIFIER_T2357796318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyPerClassifier
struct  ClassifyPerClassifier_t2357796318  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyPerClassifier::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyPerClassifier::<classifier_id>k__BackingField
	String_t* ___U3Cclassifier_idU3Ek__BackingField_1;
	// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassResult[] IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassifyPerClassifier::<classes>k__BackingField
	ClassResultU5BU5D_t2895066752* ___U3CclassesU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ClassifyPerClassifier_t2357796318, ___U3CnameU3Ek__BackingField_0)); }
	inline String_t* get_U3CnameU3Ek__BackingField_0() const { return ___U3CnameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_0() { return &___U3CnameU3Ek__BackingField_0; }
	inline void set_U3CnameU3Ek__BackingField_0(String_t* value)
	{
		___U3CnameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3Cclassifier_idU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ClassifyPerClassifier_t2357796318, ___U3Cclassifier_idU3Ek__BackingField_1)); }
	inline String_t* get_U3Cclassifier_idU3Ek__BackingField_1() const { return ___U3Cclassifier_idU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3Cclassifier_idU3Ek__BackingField_1() { return &___U3Cclassifier_idU3Ek__BackingField_1; }
	inline void set_U3Cclassifier_idU3Ek__BackingField_1(String_t* value)
	{
		___U3Cclassifier_idU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cclassifier_idU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CclassesU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ClassifyPerClassifier_t2357796318, ___U3CclassesU3Ek__BackingField_2)); }
	inline ClassResultU5BU5D_t2895066752* get_U3CclassesU3Ek__BackingField_2() const { return ___U3CclassesU3Ek__BackingField_2; }
	inline ClassResultU5BU5D_t2895066752** get_address_of_U3CclassesU3Ek__BackingField_2() { return &___U3CclassesU3Ek__BackingField_2; }
	inline void set_U3CclassesU3Ek__BackingField_2(ClassResultU5BU5D_t2895066752* value)
	{
		___U3CclassesU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CclassesU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLASSIFYPERCLASSIFIER_T2357796318_H
#ifndef CLASSRESULT_T3791036973_H
#define CLASSRESULT_T3791036973_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassResult
struct  ClassResult_t3791036973  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassResult::<m_class>k__BackingField
	String_t* ___U3Cm_classU3Ek__BackingField_0;
	// System.Double IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassResult::<score>k__BackingField
	double ___U3CscoreU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ClassResult::<type_hierarchy>k__BackingField
	String_t* ___U3Ctype_hierarchyU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3Cm_classU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ClassResult_t3791036973, ___U3Cm_classU3Ek__BackingField_0)); }
	inline String_t* get_U3Cm_classU3Ek__BackingField_0() const { return ___U3Cm_classU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3Cm_classU3Ek__BackingField_0() { return &___U3Cm_classU3Ek__BackingField_0; }
	inline void set_U3Cm_classU3Ek__BackingField_0(String_t* value)
	{
		___U3Cm_classU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cm_classU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CscoreU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ClassResult_t3791036973, ___U3CscoreU3Ek__BackingField_1)); }
	inline double get_U3CscoreU3Ek__BackingField_1() const { return ___U3CscoreU3Ek__BackingField_1; }
	inline double* get_address_of_U3CscoreU3Ek__BackingField_1() { return &___U3CscoreU3Ek__BackingField_1; }
	inline void set_U3CscoreU3Ek__BackingField_1(double value)
	{
		___U3CscoreU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3Ctype_hierarchyU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ClassResult_t3791036973, ___U3Ctype_hierarchyU3Ek__BackingField_2)); }
	inline String_t* get_U3Ctype_hierarchyU3Ek__BackingField_2() const { return ___U3Ctype_hierarchyU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3Ctype_hierarchyU3Ek__BackingField_2() { return &___U3Ctype_hierarchyU3Ek__BackingField_2; }
	inline void set_U3Ctype_hierarchyU3Ek__BackingField_2(String_t* value)
	{
		___U3Ctype_hierarchyU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3Ctype_hierarchyU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLASSRESULT_T3791036973_H
#ifndef SOLUTION_T204511443_H
#define SOLUTION_T204511443_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Solution
struct  Solution_t204511443  : public RuntimeObject
{
public:
	// System.String[] IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Solution::<shadow_me>k__BackingField
	StringU5BU5D_t1642385972* ___U3Cshadow_meU3Ek__BackingField_0;
	// System.String[] IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Solution::<shadows>k__BackingField
	StringU5BU5D_t1642385972* ___U3CshadowsU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Solution::<solution_ref>k__BackingField
	String_t* ___U3Csolution_refU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Solution::<status>k__BackingField
	String_t* ___U3CstatusU3Ek__BackingField_3;
	// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.StatusCause IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Solution::<status_cause>k__BackingField
	StatusCause_t3777693303 * ___U3Cstatus_causeU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3Cshadow_meU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Solution_t204511443, ___U3Cshadow_meU3Ek__BackingField_0)); }
	inline StringU5BU5D_t1642385972* get_U3Cshadow_meU3Ek__BackingField_0() const { return ___U3Cshadow_meU3Ek__BackingField_0; }
	inline StringU5BU5D_t1642385972** get_address_of_U3Cshadow_meU3Ek__BackingField_0() { return &___U3Cshadow_meU3Ek__BackingField_0; }
	inline void set_U3Cshadow_meU3Ek__BackingField_0(StringU5BU5D_t1642385972* value)
	{
		___U3Cshadow_meU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cshadow_meU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CshadowsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Solution_t204511443, ___U3CshadowsU3Ek__BackingField_1)); }
	inline StringU5BU5D_t1642385972* get_U3CshadowsU3Ek__BackingField_1() const { return ___U3CshadowsU3Ek__BackingField_1; }
	inline StringU5BU5D_t1642385972** get_address_of_U3CshadowsU3Ek__BackingField_1() { return &___U3CshadowsU3Ek__BackingField_1; }
	inline void set_U3CshadowsU3Ek__BackingField_1(StringU5BU5D_t1642385972* value)
	{
		___U3CshadowsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CshadowsU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3Csolution_refU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Solution_t204511443, ___U3Csolution_refU3Ek__BackingField_2)); }
	inline String_t* get_U3Csolution_refU3Ek__BackingField_2() const { return ___U3Csolution_refU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3Csolution_refU3Ek__BackingField_2() { return &___U3Csolution_refU3Ek__BackingField_2; }
	inline void set_U3Csolution_refU3Ek__BackingField_2(String_t* value)
	{
		___U3Csolution_refU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3Csolution_refU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CstatusU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Solution_t204511443, ___U3CstatusU3Ek__BackingField_3)); }
	inline String_t* get_U3CstatusU3Ek__BackingField_3() const { return ___U3CstatusU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CstatusU3Ek__BackingField_3() { return &___U3CstatusU3Ek__BackingField_3; }
	inline void set_U3CstatusU3Ek__BackingField_3(String_t* value)
	{
		___U3CstatusU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstatusU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3Cstatus_causeU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Solution_t204511443, ___U3Cstatus_causeU3Ek__BackingField_4)); }
	inline StatusCause_t3777693303 * get_U3Cstatus_causeU3Ek__BackingField_4() const { return ___U3Cstatus_causeU3Ek__BackingField_4; }
	inline StatusCause_t3777693303 ** get_address_of_U3Cstatus_causeU3Ek__BackingField_4() { return &___U3Cstatus_causeU3Ek__BackingField_4; }
	inline void set_U3Cstatus_causeU3Ek__BackingField_4(StatusCause_t3777693303 * value)
	{
		___U3Cstatus_causeU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cstatus_causeU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOLUTION_T204511443_H
#ifndef STATUSCAUSE_T3777693303_H
#define STATUSCAUSE_T3777693303_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.StatusCause
struct  StatusCause_t3777693303  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.StatusCause::<error_code>k__BackingField
	String_t* ___U3Cerror_codeU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.StatusCause::<message>k__BackingField
	String_t* ___U3CmessageU3Ek__BackingField_1;
	// System.String[] IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.StatusCause::<tokens>k__BackingField
	StringU5BU5D_t1642385972* ___U3CtokensU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3Cerror_codeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StatusCause_t3777693303, ___U3Cerror_codeU3Ek__BackingField_0)); }
	inline String_t* get_U3Cerror_codeU3Ek__BackingField_0() const { return ___U3Cerror_codeU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3Cerror_codeU3Ek__BackingField_0() { return &___U3Cerror_codeU3Ek__BackingField_0; }
	inline void set_U3Cerror_codeU3Ek__BackingField_0(String_t* value)
	{
		___U3Cerror_codeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cerror_codeU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CmessageU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(StatusCause_t3777693303, ___U3CmessageU3Ek__BackingField_1)); }
	inline String_t* get_U3CmessageU3Ek__BackingField_1() const { return ___U3CmessageU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CmessageU3Ek__BackingField_1() { return &___U3CmessageU3Ek__BackingField_1; }
	inline void set_U3CmessageU3Ek__BackingField_1(String_t* value)
	{
		___U3CmessageU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmessageU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CtokensU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(StatusCause_t3777693303, ___U3CtokensU3Ek__BackingField_2)); }
	inline StringU5BU5D_t1642385972* get_U3CtokensU3Ek__BackingField_2() const { return ___U3CtokensU3Ek__BackingField_2; }
	inline StringU5BU5D_t1642385972** get_address_of_U3CtokensU3Ek__BackingField_2() { return &___U3CtokensU3Ek__BackingField_2; }
	inline void set_U3CtokensU3Ek__BackingField_2(StringU5BU5D_t1642385972* value)
	{
		___U3CtokensU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtokensU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATUSCAUSE_T3777693303_H
#ifndef METRICS_T236865991_H
#define METRICS_T236865991_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Metrics
struct  Metrics_t236865991  : public RuntimeObject
{
public:
	// System.Double IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Metrics::<final_kappa>k__BackingField
	double ___U3Cfinal_kappaU3Ek__BackingField_0;
	// System.Double IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Metrics::<kappa>k__BackingField
	double ___U3CkappaU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3Cfinal_kappaU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Metrics_t236865991, ___U3Cfinal_kappaU3Ek__BackingField_0)); }
	inline double get_U3Cfinal_kappaU3Ek__BackingField_0() const { return ___U3Cfinal_kappaU3Ek__BackingField_0; }
	inline double* get_address_of_U3Cfinal_kappaU3Ek__BackingField_0() { return &___U3Cfinal_kappaU3Ek__BackingField_0; }
	inline void set_U3Cfinal_kappaU3Ek__BackingField_0(double value)
	{
		___U3Cfinal_kappaU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CkappaU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Metrics_t236865991, ___U3CkappaU3Ek__BackingField_1)); }
	inline double get_U3CkappaU3Ek__BackingField_1() const { return ___U3CkappaU3Ek__BackingField_1; }
	inline double* get_address_of_U3CkappaU3Ek__BackingField_1() { return &___U3CkappaU3Ek__BackingField_1; }
	inline void set_U3CkappaU3Ek__BackingField_1(double value)
	{
		___U3CkappaU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METRICS_T236865991_H
#ifndef NODE_T3986951550_H
#define NODE_T3986951550_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Node
struct  Node_t3986951550  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Position IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Node::<coordinates>k__BackingField
	Position_t3597491459 * ___U3CcoordinatesU3Ek__BackingField_0;
	// System.String[] IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Node::<solution_refs>k__BackingField
	StringU5BU5D_t1642385972* ___U3Csolution_refsU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CcoordinatesU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Node_t3986951550, ___U3CcoordinatesU3Ek__BackingField_0)); }
	inline Position_t3597491459 * get_U3CcoordinatesU3Ek__BackingField_0() const { return ___U3CcoordinatesU3Ek__BackingField_0; }
	inline Position_t3597491459 ** get_address_of_U3CcoordinatesU3Ek__BackingField_0() { return &___U3CcoordinatesU3Ek__BackingField_0; }
	inline void set_U3CcoordinatesU3Ek__BackingField_0(Position_t3597491459 * value)
	{
		___U3CcoordinatesU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcoordinatesU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3Csolution_refsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Node_t3986951550, ___U3Csolution_refsU3Ek__BackingField_1)); }
	inline StringU5BU5D_t1642385972* get_U3Csolution_refsU3Ek__BackingField_1() const { return ___U3Csolution_refsU3Ek__BackingField_1; }
	inline StringU5BU5D_t1642385972** get_address_of_U3Csolution_refsU3Ek__BackingField_1() { return &___U3Csolution_refsU3Ek__BackingField_1; }
	inline void set_U3Csolution_refsU3Ek__BackingField_1(StringU5BU5D_t1642385972* value)
	{
		___U3Csolution_refsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3Csolution_refsU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NODE_T3986951550_H
#ifndef DETECTFACESPARAMETERS_T3278171735_H
#define DETECTFACESPARAMETERS_T3278171735_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.DetectFacesParameters
struct  DetectFacesParameters_t3278171735  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.DetectFacesParameters::<url>k__BackingField
	String_t* ___U3CurlU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CurlU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DetectFacesParameters_t3278171735, ___U3CurlU3Ek__BackingField_0)); }
	inline String_t* get_U3CurlU3Ek__BackingField_0() const { return ___U3CurlU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CurlU3Ek__BackingField_0() { return &___U3CurlU3Ek__BackingField_0; }
	inline void set_U3CurlU3Ek__BackingField_0(String_t* value)
	{
		___U3CurlU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CurlU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DETECTFACESPARAMETERS_T3278171735_H
#ifndef ONEFACERESULT_T2674708326_H
#define ONEFACERESULT_T2674708326_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.OneFaceResult
struct  OneFaceResult_t2674708326  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.Age IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.OneFaceResult::<age>k__BackingField
	Age_t582693723 * ___U3CageU3Ek__BackingField_0;
	// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.Gender IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.OneFaceResult::<gender>k__BackingField
	Gender_t3451852799 * ___U3CgenderU3Ek__BackingField_1;
	// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.FaceLocation IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.OneFaceResult::<face_location>k__BackingField
	FaceLocation_t2324903414 * ___U3Cface_locationU3Ek__BackingField_2;
	// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.Identity IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.OneFaceResult::<identity>k__BackingField
	Identity_t2022392156 * ___U3CidentityU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CageU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(OneFaceResult_t2674708326, ___U3CageU3Ek__BackingField_0)); }
	inline Age_t582693723 * get_U3CageU3Ek__BackingField_0() const { return ___U3CageU3Ek__BackingField_0; }
	inline Age_t582693723 ** get_address_of_U3CageU3Ek__BackingField_0() { return &___U3CageU3Ek__BackingField_0; }
	inline void set_U3CageU3Ek__BackingField_0(Age_t582693723 * value)
	{
		___U3CageU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CageU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CgenderU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(OneFaceResult_t2674708326, ___U3CgenderU3Ek__BackingField_1)); }
	inline Gender_t3451852799 * get_U3CgenderU3Ek__BackingField_1() const { return ___U3CgenderU3Ek__BackingField_1; }
	inline Gender_t3451852799 ** get_address_of_U3CgenderU3Ek__BackingField_1() { return &___U3CgenderU3Ek__BackingField_1; }
	inline void set_U3CgenderU3Ek__BackingField_1(Gender_t3451852799 * value)
	{
		___U3CgenderU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CgenderU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3Cface_locationU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(OneFaceResult_t2674708326, ___U3Cface_locationU3Ek__BackingField_2)); }
	inline FaceLocation_t2324903414 * get_U3Cface_locationU3Ek__BackingField_2() const { return ___U3Cface_locationU3Ek__BackingField_2; }
	inline FaceLocation_t2324903414 ** get_address_of_U3Cface_locationU3Ek__BackingField_2() { return &___U3Cface_locationU3Ek__BackingField_2; }
	inline void set_U3Cface_locationU3Ek__BackingField_2(FaceLocation_t2324903414 * value)
	{
		___U3Cface_locationU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cface_locationU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CidentityU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(OneFaceResult_t2674708326, ___U3CidentityU3Ek__BackingField_3)); }
	inline Identity_t2022392156 * get_U3CidentityU3Ek__BackingField_3() const { return ___U3CidentityU3Ek__BackingField_3; }
	inline Identity_t2022392156 ** get_address_of_U3CidentityU3Ek__BackingField_3() { return &___U3CidentityU3Ek__BackingField_3; }
	inline void set_U3CidentityU3Ek__BackingField_3(Identity_t2022392156 * value)
	{
		___U3CidentityU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CidentityU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONEFACERESULT_T2674708326_H
#ifndef TRADEOFFANALYTICS_T100002595_H
#define TRADEOFFANALYTICS_T100002595_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics
struct  TradeoffAnalytics_t100002595  : public RuntimeObject
{
public:

public:
};

struct TradeoffAnalytics_t100002595_StaticFields
{
public:
	// FullSerializer.fsSerializer IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics::sm_Serializer
	fsSerializer_t4193731081 * ___sm_Serializer_1;

public:
	inline static int32_t get_offset_of_sm_Serializer_1() { return static_cast<int32_t>(offsetof(TradeoffAnalytics_t100002595_StaticFields, ___sm_Serializer_1)); }
	inline fsSerializer_t4193731081 * get_sm_Serializer_1() const { return ___sm_Serializer_1; }
	inline fsSerializer_t4193731081 ** get_address_of_sm_Serializer_1() { return &___sm_Serializer_1; }
	inline void set_sm_Serializer_1(fsSerializer_t4193731081 * value)
	{
		___sm_Serializer_1 = value;
		Il2CppCodeGenWriteBarrier((&___sm_Serializer_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRADEOFFANALYTICS_T100002595_H
#ifndef TEXTRECOGTOPLEVELMULTIPLE_T1284479682_H
#define TEXTRECOGTOPLEVELMULTIPLE_T1284479682_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.TextRecogTopLevelMultiple
struct  TextRecogTopLevelMultiple_t1284479682  : public RuntimeObject
{
public:
	// System.Int32 IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.TextRecogTopLevelMultiple::<images_processed>k__BackingField
	int32_t ___U3Cimages_processedU3Ek__BackingField_0;
	// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.TextRecogTopLevelSingle[] IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.TextRecogTopLevelMultiple::<images>k__BackingField
	TextRecogTopLevelSingleU5BU5D_t247892373* ___U3CimagesU3Ek__BackingField_1;
	// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.WarningInfo[] IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.TextRecogTopLevelMultiple::<warnings>k__BackingField
	WarningInfoU5BU5D_t1932079499* ___U3CwarningsU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3Cimages_processedU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TextRecogTopLevelMultiple_t1284479682, ___U3Cimages_processedU3Ek__BackingField_0)); }
	inline int32_t get_U3Cimages_processedU3Ek__BackingField_0() const { return ___U3Cimages_processedU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3Cimages_processedU3Ek__BackingField_0() { return &___U3Cimages_processedU3Ek__BackingField_0; }
	inline void set_U3Cimages_processedU3Ek__BackingField_0(int32_t value)
	{
		___U3Cimages_processedU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CimagesU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TextRecogTopLevelMultiple_t1284479682, ___U3CimagesU3Ek__BackingField_1)); }
	inline TextRecogTopLevelSingleU5BU5D_t247892373* get_U3CimagesU3Ek__BackingField_1() const { return ___U3CimagesU3Ek__BackingField_1; }
	inline TextRecogTopLevelSingleU5BU5D_t247892373** get_address_of_U3CimagesU3Ek__BackingField_1() { return &___U3CimagesU3Ek__BackingField_1; }
	inline void set_U3CimagesU3Ek__BackingField_1(TextRecogTopLevelSingleU5BU5D_t247892373* value)
	{
		___U3CimagesU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CimagesU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CwarningsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(TextRecogTopLevelMultiple_t1284479682, ___U3CwarningsU3Ek__BackingField_2)); }
	inline WarningInfoU5BU5D_t1932079499* get_U3CwarningsU3Ek__BackingField_2() const { return ___U3CwarningsU3Ek__BackingField_2; }
	inline WarningInfoU5BU5D_t1932079499** get_address_of_U3CwarningsU3Ek__BackingField_2() { return &___U3CwarningsU3Ek__BackingField_2; }
	inline void set_U3CwarningsU3Ek__BackingField_2(WarningInfoU5BU5D_t1932079499* value)
	{
		___U3CwarningsU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwarningsU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTRECOGTOPLEVELMULTIPLE_T1284479682_H
#ifndef SIMILARIMAGECONFIG_T3734626070_H
#define SIMILARIMAGECONFIG_T3734626070_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.SimilarImageConfig
struct  SimilarImageConfig_t3734626070  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.SimilarImageConfig::<image_id>k__BackingField
	String_t* ___U3Cimage_idU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.SimilarImageConfig::<created>k__BackingField
	String_t* ___U3CcreatedU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.SimilarImageConfig::<image_file>k__BackingField
	String_t* ___U3Cimage_fileU3Ek__BackingField_2;
	// System.Object IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.SimilarImageConfig::<metadata>k__BackingField
	RuntimeObject * ___U3CmetadataU3Ek__BackingField_3;
	// System.Single IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.SimilarImageConfig::<score>k__BackingField
	float ___U3CscoreU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3Cimage_idU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SimilarImageConfig_t3734626070, ___U3Cimage_idU3Ek__BackingField_0)); }
	inline String_t* get_U3Cimage_idU3Ek__BackingField_0() const { return ___U3Cimage_idU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3Cimage_idU3Ek__BackingField_0() { return &___U3Cimage_idU3Ek__BackingField_0; }
	inline void set_U3Cimage_idU3Ek__BackingField_0(String_t* value)
	{
		___U3Cimage_idU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cimage_idU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CcreatedU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(SimilarImageConfig_t3734626070, ___U3CcreatedU3Ek__BackingField_1)); }
	inline String_t* get_U3CcreatedU3Ek__BackingField_1() const { return ___U3CcreatedU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CcreatedU3Ek__BackingField_1() { return &___U3CcreatedU3Ek__BackingField_1; }
	inline void set_U3CcreatedU3Ek__BackingField_1(String_t* value)
	{
		___U3CcreatedU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcreatedU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3Cimage_fileU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SimilarImageConfig_t3734626070, ___U3Cimage_fileU3Ek__BackingField_2)); }
	inline String_t* get_U3Cimage_fileU3Ek__BackingField_2() const { return ___U3Cimage_fileU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3Cimage_fileU3Ek__BackingField_2() { return &___U3Cimage_fileU3Ek__BackingField_2; }
	inline void set_U3Cimage_fileU3Ek__BackingField_2(String_t* value)
	{
		___U3Cimage_fileU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cimage_fileU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CmetadataU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SimilarImageConfig_t3734626070, ___U3CmetadataU3Ek__BackingField_3)); }
	inline RuntimeObject * get_U3CmetadataU3Ek__BackingField_3() const { return ___U3CmetadataU3Ek__BackingField_3; }
	inline RuntimeObject ** get_address_of_U3CmetadataU3Ek__BackingField_3() { return &___U3CmetadataU3Ek__BackingField_3; }
	inline void set_U3CmetadataU3Ek__BackingField_3(RuntimeObject * value)
	{
		___U3CmetadataU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmetadataU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CscoreU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(SimilarImageConfig_t3734626070, ___U3CscoreU3Ek__BackingField_4)); }
	inline float get_U3CscoreU3Ek__BackingField_4() const { return ___U3CscoreU3Ek__BackingField_4; }
	inline float* get_address_of_U3CscoreU3Ek__BackingField_4() { return &___U3CscoreU3Ek__BackingField_4; }
	inline void set_U3CscoreU3Ek__BackingField_4(float value)
	{
		___U3CscoreU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMILARIMAGECONFIG_T3734626070_H
#ifndef SIMILARIMAGESCONFIG_T2576720653_H
#define SIMILARIMAGESCONFIG_T2576720653_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.SimilarImagesConfig
struct  SimilarImagesConfig_t2576720653  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.SimilarImageConfig[] IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.SimilarImagesConfig::<similar_images>k__BackingField
	SimilarImageConfigU5BU5D_t273269715* ___U3Csimilar_imagesU3Ek__BackingField_0;
	// System.Int32 IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.SimilarImagesConfig::<images_processed>k__BackingField
	int32_t ___U3Cimages_processedU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3Csimilar_imagesU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SimilarImagesConfig_t2576720653, ___U3Csimilar_imagesU3Ek__BackingField_0)); }
	inline SimilarImageConfigU5BU5D_t273269715* get_U3Csimilar_imagesU3Ek__BackingField_0() const { return ___U3Csimilar_imagesU3Ek__BackingField_0; }
	inline SimilarImageConfigU5BU5D_t273269715** get_address_of_U3Csimilar_imagesU3Ek__BackingField_0() { return &___U3Csimilar_imagesU3Ek__BackingField_0; }
	inline void set_U3Csimilar_imagesU3Ek__BackingField_0(SimilarImageConfigU5BU5D_t273269715* value)
	{
		___U3Csimilar_imagesU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Csimilar_imagesU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3Cimages_processedU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(SimilarImagesConfig_t2576720653, ___U3Cimages_processedU3Ek__BackingField_1)); }
	inline int32_t get_U3Cimages_processedU3Ek__BackingField_1() const { return ___U3Cimages_processedU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3Cimages_processedU3Ek__BackingField_1() { return &___U3Cimages_processedU3Ek__BackingField_1; }
	inline void set_U3Cimages_processedU3Ek__BackingField_1(int32_t value)
	{
		___U3Cimages_processedU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMILARIMAGESCONFIG_T2576720653_H
#ifndef ERRORINFONOCODE_T2062371116_H
#define ERRORINFONOCODE_T2062371116_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ErrorInfoNoCode
struct  ErrorInfoNoCode_t2062371116  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ErrorInfoNoCode::<error_id>k__BackingField
	String_t* ___U3Cerror_idU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.ErrorInfoNoCode::<description>k__BackingField
	String_t* ___U3CdescriptionU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3Cerror_idU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ErrorInfoNoCode_t2062371116, ___U3Cerror_idU3Ek__BackingField_0)); }
	inline String_t* get_U3Cerror_idU3Ek__BackingField_0() const { return ___U3Cerror_idU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3Cerror_idU3Ek__BackingField_0() { return &___U3Cerror_idU3Ek__BackingField_0; }
	inline void set_U3Cerror_idU3Ek__BackingField_0(String_t* value)
	{
		___U3Cerror_idU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cerror_idU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CdescriptionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ErrorInfoNoCode_t2062371116, ___U3CdescriptionU3Ek__BackingField_1)); }
	inline String_t* get_U3CdescriptionU3Ek__BackingField_1() const { return ___U3CdescriptionU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CdescriptionU3Ek__BackingField_1() { return &___U3CdescriptionU3Ek__BackingField_1; }
	inline void set_U3CdescriptionU3Ek__BackingField_1(String_t* value)
	{
		___U3CdescriptionU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdescriptionU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERRORINFONOCODE_T2062371116_H
#ifndef WARNINGINFO_T624028094_H
#define WARNINGINFO_T624028094_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.WarningInfo
struct  WarningInfo_t624028094  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.WarningInfo::<warning_id>k__BackingField
	String_t* ___U3Cwarning_idU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.WarningInfo::<description>k__BackingField
	String_t* ___U3CdescriptionU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3Cwarning_idU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(WarningInfo_t624028094, ___U3Cwarning_idU3Ek__BackingField_0)); }
	inline String_t* get_U3Cwarning_idU3Ek__BackingField_0() const { return ___U3Cwarning_idU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3Cwarning_idU3Ek__BackingField_0() { return &___U3Cwarning_idU3Ek__BackingField_0; }
	inline void set_U3Cwarning_idU3Ek__BackingField_0(String_t* value)
	{
		___U3Cwarning_idU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cwarning_idU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CdescriptionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(WarningInfo_t624028094, ___U3CdescriptionU3Ek__BackingField_1)); }
	inline String_t* get_U3CdescriptionU3Ek__BackingField_1() const { return ___U3CdescriptionU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CdescriptionU3Ek__BackingField_1() { return &___U3CdescriptionU3Ek__BackingField_1; }
	inline void set_U3CdescriptionU3Ek__BackingField_1(String_t* value)
	{
		___U3CdescriptionU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdescriptionU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WARNINGINFO_T624028094_H
#ifndef GETCOLLECTIONSBRIEF_T2166116487_H
#define GETCOLLECTIONSBRIEF_T2166116487_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.GetCollectionsBrief
struct  GetCollectionsBrief_t2166116487  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.GetCollectionsBrief::<image_id>k__BackingField
	String_t* ___U3Cimage_idU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.GetCollectionsBrief::<created>k__BackingField
	String_t* ___U3CcreatedU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.GetCollectionsBrief::<image_file>k__BackingField
	String_t* ___U3Cimage_fileU3Ek__BackingField_2;
	// System.Object IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.GetCollectionsBrief::<metadata>k__BackingField
	RuntimeObject * ___U3CmetadataU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3Cimage_idU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(GetCollectionsBrief_t2166116487, ___U3Cimage_idU3Ek__BackingField_0)); }
	inline String_t* get_U3Cimage_idU3Ek__BackingField_0() const { return ___U3Cimage_idU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3Cimage_idU3Ek__BackingField_0() { return &___U3Cimage_idU3Ek__BackingField_0; }
	inline void set_U3Cimage_idU3Ek__BackingField_0(String_t* value)
	{
		___U3Cimage_idU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cimage_idU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CcreatedU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(GetCollectionsBrief_t2166116487, ___U3CcreatedU3Ek__BackingField_1)); }
	inline String_t* get_U3CcreatedU3Ek__BackingField_1() const { return ___U3CcreatedU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CcreatedU3Ek__BackingField_1() { return &___U3CcreatedU3Ek__BackingField_1; }
	inline void set_U3CcreatedU3Ek__BackingField_1(String_t* value)
	{
		___U3CcreatedU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcreatedU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3Cimage_fileU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(GetCollectionsBrief_t2166116487, ___U3Cimage_fileU3Ek__BackingField_2)); }
	inline String_t* get_U3Cimage_fileU3Ek__BackingField_2() const { return ___U3Cimage_fileU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3Cimage_fileU3Ek__BackingField_2() { return &___U3Cimage_fileU3Ek__BackingField_2; }
	inline void set_U3Cimage_fileU3Ek__BackingField_2(String_t* value)
	{
		___U3Cimage_fileU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cimage_fileU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CmetadataU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(GetCollectionsBrief_t2166116487, ___U3CmetadataU3Ek__BackingField_3)); }
	inline RuntimeObject * get_U3CmetadataU3Ek__BackingField_3() const { return ___U3CmetadataU3Ek__BackingField_3; }
	inline RuntimeObject ** get_address_of_U3CmetadataU3Ek__BackingField_3() { return &___U3CmetadataU3Ek__BackingField_3; }
	inline void set_U3CmetadataU3Ek__BackingField_3(RuntimeObject * value)
	{
		___U3CmetadataU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmetadataU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETCOLLECTIONSBRIEF_T2166116487_H
#ifndef REQUEST_T466816980_H
#define REQUEST_T466816980_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request
struct  Request_t466816980  : public RuntimeObject
{
public:
	// System.Single IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Timeout>k__BackingField
	float ___U3CTimeoutU3Ek__BackingField_0;
	// System.Boolean IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Cancel>k__BackingField
	bool ___U3CCancelU3Ek__BackingField_1;
	// System.Boolean IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Delete>k__BackingField
	bool ___U3CDeleteU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Function>k__BackingField
	String_t* ___U3CFunctionU3Ek__BackingField_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Parameters>k__BackingField
	Dictionary_2_t309261261 * ___U3CParametersU3Ek__BackingField_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Headers>k__BackingField
	Dictionary_2_t3943999495 * ___U3CHeadersU3Ek__BackingField_5;
	// System.Byte[] IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Send>k__BackingField
	ByteU5BU5D_t3397334013* ___U3CSendU3Ek__BackingField_6;
	// System.Collections.Generic.Dictionary`2<System.String,IBM.Watson.DeveloperCloud.Connection.RESTConnector/Form> IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Forms>k__BackingField
	Dictionary_2_t2694055125 * ___U3CFormsU3Ek__BackingField_7;
	// IBM.Watson.DeveloperCloud.Connection.RESTConnector/ResponseEvent IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<OnResponse>k__BackingField
	ResponseEvent_t1616568356 * ___U3COnResponseU3Ek__BackingField_8;
	// IBM.Watson.DeveloperCloud.Connection.RESTConnector/ProgressEvent IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<OnDownloadProgress>k__BackingField
	ProgressEvent_t4185145044 * ___U3COnDownloadProgressU3Ek__BackingField_9;
	// IBM.Watson.DeveloperCloud.Connection.RESTConnector/ProgressEvent IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<OnUploadProgress>k__BackingField
	ProgressEvent_t4185145044 * ___U3COnUploadProgressU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of_U3CTimeoutU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CTimeoutU3Ek__BackingField_0)); }
	inline float get_U3CTimeoutU3Ek__BackingField_0() const { return ___U3CTimeoutU3Ek__BackingField_0; }
	inline float* get_address_of_U3CTimeoutU3Ek__BackingField_0() { return &___U3CTimeoutU3Ek__BackingField_0; }
	inline void set_U3CTimeoutU3Ek__BackingField_0(float value)
	{
		___U3CTimeoutU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CCancelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CCancelU3Ek__BackingField_1)); }
	inline bool get_U3CCancelU3Ek__BackingField_1() const { return ___U3CCancelU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CCancelU3Ek__BackingField_1() { return &___U3CCancelU3Ek__BackingField_1; }
	inline void set_U3CCancelU3Ek__BackingField_1(bool value)
	{
		___U3CCancelU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CDeleteU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CDeleteU3Ek__BackingField_2)); }
	inline bool get_U3CDeleteU3Ek__BackingField_2() const { return ___U3CDeleteU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CDeleteU3Ek__BackingField_2() { return &___U3CDeleteU3Ek__BackingField_2; }
	inline void set_U3CDeleteU3Ek__BackingField_2(bool value)
	{
		___U3CDeleteU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CFunctionU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CFunctionU3Ek__BackingField_3)); }
	inline String_t* get_U3CFunctionU3Ek__BackingField_3() const { return ___U3CFunctionU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CFunctionU3Ek__BackingField_3() { return &___U3CFunctionU3Ek__BackingField_3; }
	inline void set_U3CFunctionU3Ek__BackingField_3(String_t* value)
	{
		___U3CFunctionU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFunctionU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CParametersU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CParametersU3Ek__BackingField_4)); }
	inline Dictionary_2_t309261261 * get_U3CParametersU3Ek__BackingField_4() const { return ___U3CParametersU3Ek__BackingField_4; }
	inline Dictionary_2_t309261261 ** get_address_of_U3CParametersU3Ek__BackingField_4() { return &___U3CParametersU3Ek__BackingField_4; }
	inline void set_U3CParametersU3Ek__BackingField_4(Dictionary_2_t309261261 * value)
	{
		___U3CParametersU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CParametersU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CHeadersU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CHeadersU3Ek__BackingField_5)); }
	inline Dictionary_2_t3943999495 * get_U3CHeadersU3Ek__BackingField_5() const { return ___U3CHeadersU3Ek__BackingField_5; }
	inline Dictionary_2_t3943999495 ** get_address_of_U3CHeadersU3Ek__BackingField_5() { return &___U3CHeadersU3Ek__BackingField_5; }
	inline void set_U3CHeadersU3Ek__BackingField_5(Dictionary_2_t3943999495 * value)
	{
		___U3CHeadersU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CHeadersU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CSendU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CSendU3Ek__BackingField_6)); }
	inline ByteU5BU5D_t3397334013* get_U3CSendU3Ek__BackingField_6() const { return ___U3CSendU3Ek__BackingField_6; }
	inline ByteU5BU5D_t3397334013** get_address_of_U3CSendU3Ek__BackingField_6() { return &___U3CSendU3Ek__BackingField_6; }
	inline void set_U3CSendU3Ek__BackingField_6(ByteU5BU5D_t3397334013* value)
	{
		___U3CSendU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSendU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CFormsU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CFormsU3Ek__BackingField_7)); }
	inline Dictionary_2_t2694055125 * get_U3CFormsU3Ek__BackingField_7() const { return ___U3CFormsU3Ek__BackingField_7; }
	inline Dictionary_2_t2694055125 ** get_address_of_U3CFormsU3Ek__BackingField_7() { return &___U3CFormsU3Ek__BackingField_7; }
	inline void set_U3CFormsU3Ek__BackingField_7(Dictionary_2_t2694055125 * value)
	{
		___U3CFormsU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFormsU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3COnResponseU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3COnResponseU3Ek__BackingField_8)); }
	inline ResponseEvent_t1616568356 * get_U3COnResponseU3Ek__BackingField_8() const { return ___U3COnResponseU3Ek__BackingField_8; }
	inline ResponseEvent_t1616568356 ** get_address_of_U3COnResponseU3Ek__BackingField_8() { return &___U3COnResponseU3Ek__BackingField_8; }
	inline void set_U3COnResponseU3Ek__BackingField_8(ResponseEvent_t1616568356 * value)
	{
		___U3COnResponseU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3COnResponseU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3COnDownloadProgressU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3COnDownloadProgressU3Ek__BackingField_9)); }
	inline ProgressEvent_t4185145044 * get_U3COnDownloadProgressU3Ek__BackingField_9() const { return ___U3COnDownloadProgressU3Ek__BackingField_9; }
	inline ProgressEvent_t4185145044 ** get_address_of_U3COnDownloadProgressU3Ek__BackingField_9() { return &___U3COnDownloadProgressU3Ek__BackingField_9; }
	inline void set_U3COnDownloadProgressU3Ek__BackingField_9(ProgressEvent_t4185145044 * value)
	{
		___U3COnDownloadProgressU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3COnDownloadProgressU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3COnUploadProgressU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3COnUploadProgressU3Ek__BackingField_10)); }
	inline ProgressEvent_t4185145044 * get_U3COnUploadProgressU3Ek__BackingField_10() const { return ___U3COnUploadProgressU3Ek__BackingField_10; }
	inline ProgressEvent_t4185145044 ** get_address_of_U3COnUploadProgressU3Ek__BackingField_10() { return &___U3COnUploadProgressU3Ek__BackingField_10; }
	inline void set_U3COnUploadProgressU3Ek__BackingField_10(ProgressEvent_t4185145044 * value)
	{
		___U3COnUploadProgressU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3COnUploadProgressU3Ek__BackingField_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUEST_T466816980_H
#ifndef COLLECTIONIMAGESCONFIG_T1273369062_H
#define COLLECTIONIMAGESCONFIG_T1273369062_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.CollectionImagesConfig
struct  CollectionImagesConfig_t1273369062  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.CollectionImagesConfig::<image_id>k__BackingField
	String_t* ___U3Cimage_idU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.CollectionImagesConfig::<created>k__BackingField
	String_t* ___U3CcreatedU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.CollectionImagesConfig::<image_file>k__BackingField
	String_t* ___U3Cimage_fileU3Ek__BackingField_2;
	// System.Object IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.CollectionImagesConfig::<metadata>k__BackingField
	RuntimeObject * ___U3CmetadataU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3Cimage_idU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CollectionImagesConfig_t1273369062, ___U3Cimage_idU3Ek__BackingField_0)); }
	inline String_t* get_U3Cimage_idU3Ek__BackingField_0() const { return ___U3Cimage_idU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3Cimage_idU3Ek__BackingField_0() { return &___U3Cimage_idU3Ek__BackingField_0; }
	inline void set_U3Cimage_idU3Ek__BackingField_0(String_t* value)
	{
		___U3Cimage_idU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cimage_idU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CcreatedU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CollectionImagesConfig_t1273369062, ___U3CcreatedU3Ek__BackingField_1)); }
	inline String_t* get_U3CcreatedU3Ek__BackingField_1() const { return ___U3CcreatedU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CcreatedU3Ek__BackingField_1() { return &___U3CcreatedU3Ek__BackingField_1; }
	inline void set_U3CcreatedU3Ek__BackingField_1(String_t* value)
	{
		___U3CcreatedU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcreatedU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3Cimage_fileU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CollectionImagesConfig_t1273369062, ___U3Cimage_fileU3Ek__BackingField_2)); }
	inline String_t* get_U3Cimage_fileU3Ek__BackingField_2() const { return ___U3Cimage_fileU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3Cimage_fileU3Ek__BackingField_2() { return &___U3Cimage_fileU3Ek__BackingField_2; }
	inline void set_U3Cimage_fileU3Ek__BackingField_2(String_t* value)
	{
		___U3Cimage_fileU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cimage_fileU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CmetadataU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(CollectionImagesConfig_t1273369062, ___U3CmetadataU3Ek__BackingField_3)); }
	inline RuntimeObject * get_U3CmetadataU3Ek__BackingField_3() const { return ___U3CmetadataU3Ek__BackingField_3; }
	inline RuntimeObject ** get_address_of_U3CmetadataU3Ek__BackingField_3() { return &___U3CmetadataU3Ek__BackingField_3; }
	inline void set_U3CmetadataU3Ek__BackingField_3(RuntimeObject * value)
	{
		___U3CmetadataU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmetadataU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTIONIMAGESCONFIG_T1273369062_H
#ifndef COLLECTIONSCONFIG_T2493163663_H
#define COLLECTIONSCONFIG_T2493163663_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.CollectionsConfig
struct  CollectionsConfig_t2493163663  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.CollectionImagesConfig[] IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.CollectionsConfig::<images>k__BackingField
	CollectionImagesConfigU5BU5D_t666950595* ___U3CimagesU3Ek__BackingField_0;
	// System.Int32 IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.CollectionsConfig::<images_processed>k__BackingField
	int32_t ___U3Cimages_processedU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CimagesU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CollectionsConfig_t2493163663, ___U3CimagesU3Ek__BackingField_0)); }
	inline CollectionImagesConfigU5BU5D_t666950595* get_U3CimagesU3Ek__BackingField_0() const { return ___U3CimagesU3Ek__BackingField_0; }
	inline CollectionImagesConfigU5BU5D_t666950595** get_address_of_U3CimagesU3Ek__BackingField_0() { return &___U3CimagesU3Ek__BackingField_0; }
	inline void set_U3CimagesU3Ek__BackingField_0(CollectionImagesConfigU5BU5D_t666950595* value)
	{
		___U3CimagesU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CimagesU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3Cimages_processedU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CollectionsConfig_t2493163663, ___U3Cimages_processedU3Ek__BackingField_1)); }
	inline int32_t get_U3Cimages_processedU3Ek__BackingField_1() const { return ___U3Cimages_processedU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3Cimages_processedU3Ek__BackingField_1() { return &___U3Cimages_processedU3Ek__BackingField_1; }
	inline void set_U3Cimages_processedU3Ek__BackingField_1(int32_t value)
	{
		___U3Cimages_processedU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTIONSCONFIG_T2493163663_H
#ifndef VISUALRECOGNITIONVERSION_T883645903_H
#define VISUALRECOGNITIONVERSION_T883645903_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognitionVersion
struct  VisualRecognitionVersion_t883645903  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VISUALRECOGNITIONVERSION_T883645903_H
#ifndef IDENTITY_T2022392156_H
#define IDENTITY_T2022392156_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.Identity
struct  Identity_t2022392156  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.Identity::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_0;
	// System.Double IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.Identity::<score>k__BackingField
	double ___U3CscoreU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.Identity::<type_hierarchy>k__BackingField
	String_t* ___U3Ctype_hierarchyU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Identity_t2022392156, ___U3CnameU3Ek__BackingField_0)); }
	inline String_t* get_U3CnameU3Ek__BackingField_0() const { return ___U3CnameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_0() { return &___U3CnameU3Ek__BackingField_0; }
	inline void set_U3CnameU3Ek__BackingField_0(String_t* value)
	{
		___U3CnameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CscoreU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Identity_t2022392156, ___U3CscoreU3Ek__BackingField_1)); }
	inline double get_U3CscoreU3Ek__BackingField_1() const { return ___U3CscoreU3Ek__BackingField_1; }
	inline double* get_address_of_U3CscoreU3Ek__BackingField_1() { return &___U3CscoreU3Ek__BackingField_1; }
	inline void set_U3CscoreU3Ek__BackingField_1(double value)
	{
		___U3CscoreU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3Ctype_hierarchyU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Identity_t2022392156, ___U3Ctype_hierarchyU3Ek__BackingField_2)); }
	inline String_t* get_U3Ctype_hierarchyU3Ek__BackingField_2() const { return ___U3Ctype_hierarchyU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3Ctype_hierarchyU3Ek__BackingField_2() { return &___U3Ctype_hierarchyU3Ek__BackingField_2; }
	inline void set_U3Ctype_hierarchyU3Ek__BackingField_2(String_t* value)
	{
		___U3Ctype_hierarchyU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3Ctype_hierarchyU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IDENTITY_T2022392156_H
#ifndef DRIVERS_T1652706281_H
#define DRIVERS_T1652706281_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Drivers
struct  Drivers_t1652706281  : public RuntimeObject
{
public:
	// System.Double IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Drivers::<alpha_init>k__BackingField
	double ___U3Calpha_initU3Ek__BackingField_0;
	// System.Double IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Drivers::<data_multiplier>k__BackingField
	double ___U3Cdata_multiplierU3Ek__BackingField_1;
	// System.Double IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Drivers::<max_map_size>k__BackingField
	double ___U3Cmax_map_sizeU3Ek__BackingField_2;
	// System.Double IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Drivers::<r_anchor_init>k__BackingField
	double ___U3Cr_anchor_initU3Ek__BackingField_3;
	// System.Double IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Drivers::<r_fin>k__BackingField
	double ___U3Cr_finU3Ek__BackingField_4;
	// System.Double IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Drivers::<r_init>k__BackingField
	double ___U3Cr_initU3Ek__BackingField_5;
	// System.Double IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Drivers::<training_anchors>k__BackingField
	double ___U3Ctraining_anchorsU3Ek__BackingField_6;
	// System.Double IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Drivers::<training_length>k__BackingField
	double ___U3Ctraining_lengthU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_U3Calpha_initU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Drivers_t1652706281, ___U3Calpha_initU3Ek__BackingField_0)); }
	inline double get_U3Calpha_initU3Ek__BackingField_0() const { return ___U3Calpha_initU3Ek__BackingField_0; }
	inline double* get_address_of_U3Calpha_initU3Ek__BackingField_0() { return &___U3Calpha_initU3Ek__BackingField_0; }
	inline void set_U3Calpha_initU3Ek__BackingField_0(double value)
	{
		___U3Calpha_initU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3Cdata_multiplierU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Drivers_t1652706281, ___U3Cdata_multiplierU3Ek__BackingField_1)); }
	inline double get_U3Cdata_multiplierU3Ek__BackingField_1() const { return ___U3Cdata_multiplierU3Ek__BackingField_1; }
	inline double* get_address_of_U3Cdata_multiplierU3Ek__BackingField_1() { return &___U3Cdata_multiplierU3Ek__BackingField_1; }
	inline void set_U3Cdata_multiplierU3Ek__BackingField_1(double value)
	{
		___U3Cdata_multiplierU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3Cmax_map_sizeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Drivers_t1652706281, ___U3Cmax_map_sizeU3Ek__BackingField_2)); }
	inline double get_U3Cmax_map_sizeU3Ek__BackingField_2() const { return ___U3Cmax_map_sizeU3Ek__BackingField_2; }
	inline double* get_address_of_U3Cmax_map_sizeU3Ek__BackingField_2() { return &___U3Cmax_map_sizeU3Ek__BackingField_2; }
	inline void set_U3Cmax_map_sizeU3Ek__BackingField_2(double value)
	{
		___U3Cmax_map_sizeU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3Cr_anchor_initU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Drivers_t1652706281, ___U3Cr_anchor_initU3Ek__BackingField_3)); }
	inline double get_U3Cr_anchor_initU3Ek__BackingField_3() const { return ___U3Cr_anchor_initU3Ek__BackingField_3; }
	inline double* get_address_of_U3Cr_anchor_initU3Ek__BackingField_3() { return &___U3Cr_anchor_initU3Ek__BackingField_3; }
	inline void set_U3Cr_anchor_initU3Ek__BackingField_3(double value)
	{
		___U3Cr_anchor_initU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3Cr_finU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Drivers_t1652706281, ___U3Cr_finU3Ek__BackingField_4)); }
	inline double get_U3Cr_finU3Ek__BackingField_4() const { return ___U3Cr_finU3Ek__BackingField_4; }
	inline double* get_address_of_U3Cr_finU3Ek__BackingField_4() { return &___U3Cr_finU3Ek__BackingField_4; }
	inline void set_U3Cr_finU3Ek__BackingField_4(double value)
	{
		___U3Cr_finU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3Cr_initU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Drivers_t1652706281, ___U3Cr_initU3Ek__BackingField_5)); }
	inline double get_U3Cr_initU3Ek__BackingField_5() const { return ___U3Cr_initU3Ek__BackingField_5; }
	inline double* get_address_of_U3Cr_initU3Ek__BackingField_5() { return &___U3Cr_initU3Ek__BackingField_5; }
	inline void set_U3Cr_initU3Ek__BackingField_5(double value)
	{
		___U3Cr_initU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3Ctraining_anchorsU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Drivers_t1652706281, ___U3Ctraining_anchorsU3Ek__BackingField_6)); }
	inline double get_U3Ctraining_anchorsU3Ek__BackingField_6() const { return ___U3Ctraining_anchorsU3Ek__BackingField_6; }
	inline double* get_address_of_U3Ctraining_anchorsU3Ek__BackingField_6() { return &___U3Ctraining_anchorsU3Ek__BackingField_6; }
	inline void set_U3Ctraining_anchorsU3Ek__BackingField_6(double value)
	{
		___U3Ctraining_anchorsU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3Ctraining_lengthU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(Drivers_t1652706281, ___U3Ctraining_lengthU3Ek__BackingField_7)); }
	inline double get_U3Ctraining_lengthU3Ek__BackingField_7() const { return ___U3Ctraining_lengthU3Ek__BackingField_7; }
	inline double* get_address_of_U3Ctraining_lengthU3Ek__BackingField_7() { return &___U3Ctraining_lengthU3Ek__BackingField_7; }
	inline void set_U3Ctraining_lengthU3Ek__BackingField_7(double value)
	{
		___U3Ctraining_lengthU3Ek__BackingField_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRIVERS_T1652706281_H
#ifndef VISUALRECOGNITION_T3119563755_H
#define VISUALRECOGNITION_T3119563755_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition
struct  VisualRecognition_t3119563755  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/LoadFileDelegate IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition::<LoadFile>k__BackingField
	LoadFileDelegate_t3728218844 * ___U3CLoadFileU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CLoadFileU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(VisualRecognition_t3119563755, ___U3CLoadFileU3Ek__BackingField_0)); }
	inline LoadFileDelegate_t3728218844 * get_U3CLoadFileU3Ek__BackingField_0() const { return ___U3CLoadFileU3Ek__BackingField_0; }
	inline LoadFileDelegate_t3728218844 ** get_address_of_U3CLoadFileU3Ek__BackingField_0() { return &___U3CLoadFileU3Ek__BackingField_0; }
	inline void set_U3CLoadFileU3Ek__BackingField_0(LoadFileDelegate_t3728218844 * value)
	{
		___U3CLoadFileU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLoadFileU3Ek__BackingField_0), value);
	}
};

struct VisualRecognition_t3119563755_StaticFields
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition::mp_ApiKey
	String_t* ___mp_ApiKey_12;
	// FullSerializer.fsSerializer IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition::sm_Serializer
	fsSerializer_t4193731081 * ___sm_Serializer_13;

public:
	inline static int32_t get_offset_of_mp_ApiKey_12() { return static_cast<int32_t>(offsetof(VisualRecognition_t3119563755_StaticFields, ___mp_ApiKey_12)); }
	inline String_t* get_mp_ApiKey_12() const { return ___mp_ApiKey_12; }
	inline String_t** get_address_of_mp_ApiKey_12() { return &___mp_ApiKey_12; }
	inline void set_mp_ApiKey_12(String_t* value)
	{
		___mp_ApiKey_12 = value;
		Il2CppCodeGenWriteBarrier((&___mp_ApiKey_12), value);
	}

	inline static int32_t get_offset_of_sm_Serializer_13() { return static_cast<int32_t>(offsetof(VisualRecognition_t3119563755_StaticFields, ___sm_Serializer_13)); }
	inline fsSerializer_t4193731081 * get_sm_Serializer_13() const { return ___sm_Serializer_13; }
	inline fsSerializer_t4193731081 ** get_address_of_sm_Serializer_13() { return &___sm_Serializer_13; }
	inline void set_sm_Serializer_13(fsSerializer_t4193731081 * value)
	{
		___sm_Serializer_13 = value;
		Il2CppCodeGenWriteBarrier((&___sm_Serializer_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VISUALRECOGNITION_T3119563755_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef AGE_T582693723_H
#define AGE_T582693723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.Age
struct  Age_t582693723  : public RuntimeObject
{
public:
	// System.Int32 IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.Age::<min>k__BackingField
	int32_t ___U3CminU3Ek__BackingField_0;
	// System.Int32 IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.Age::<max>k__BackingField
	int32_t ___U3CmaxU3Ek__BackingField_1;
	// System.Double IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.Age::<score>k__BackingField
	double ___U3CscoreU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CminU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Age_t582693723, ___U3CminU3Ek__BackingField_0)); }
	inline int32_t get_U3CminU3Ek__BackingField_0() const { return ___U3CminU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CminU3Ek__BackingField_0() { return &___U3CminU3Ek__BackingField_0; }
	inline void set_U3CminU3Ek__BackingField_0(int32_t value)
	{
		___U3CminU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CmaxU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Age_t582693723, ___U3CmaxU3Ek__BackingField_1)); }
	inline int32_t get_U3CmaxU3Ek__BackingField_1() const { return ___U3CmaxU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CmaxU3Ek__BackingField_1() { return &___U3CmaxU3Ek__BackingField_1; }
	inline void set_U3CmaxU3Ek__BackingField_1(int32_t value)
	{
		___U3CmaxU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CscoreU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Age_t582693723, ___U3CscoreU3Ek__BackingField_2)); }
	inline double get_U3CscoreU3Ek__BackingField_2() const { return ___U3CscoreU3Ek__BackingField_2; }
	inline double* get_address_of_U3CscoreU3Ek__BackingField_2() { return &___U3CscoreU3Ek__BackingField_2; }
	inline void set_U3CscoreU3Ek__BackingField_2(double value)
	{
		___U3CscoreU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AGE_T582693723_H
#ifndef FACELOCATION_T2324903414_H
#define FACELOCATION_T2324903414_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.FaceLocation
struct  FaceLocation_t2324903414  : public RuntimeObject
{
public:
	// System.Double IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.FaceLocation::<width>k__BackingField
	double ___U3CwidthU3Ek__BackingField_0;
	// System.Double IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.FaceLocation::<height>k__BackingField
	double ___U3CheightU3Ek__BackingField_1;
	// System.Double IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.FaceLocation::<left>k__BackingField
	double ___U3CleftU3Ek__BackingField_2;
	// System.Double IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.FaceLocation::<top>k__BackingField
	double ___U3CtopU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CwidthU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(FaceLocation_t2324903414, ___U3CwidthU3Ek__BackingField_0)); }
	inline double get_U3CwidthU3Ek__BackingField_0() const { return ___U3CwidthU3Ek__BackingField_0; }
	inline double* get_address_of_U3CwidthU3Ek__BackingField_0() { return &___U3CwidthU3Ek__BackingField_0; }
	inline void set_U3CwidthU3Ek__BackingField_0(double value)
	{
		___U3CwidthU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CheightU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(FaceLocation_t2324903414, ___U3CheightU3Ek__BackingField_1)); }
	inline double get_U3CheightU3Ek__BackingField_1() const { return ___U3CheightU3Ek__BackingField_1; }
	inline double* get_address_of_U3CheightU3Ek__BackingField_1() { return &___U3CheightU3Ek__BackingField_1; }
	inline void set_U3CheightU3Ek__BackingField_1(double value)
	{
		___U3CheightU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CleftU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(FaceLocation_t2324903414, ___U3CleftU3Ek__BackingField_2)); }
	inline double get_U3CleftU3Ek__BackingField_2() const { return ___U3CleftU3Ek__BackingField_2; }
	inline double* get_address_of_U3CleftU3Ek__BackingField_2() { return &___U3CleftU3Ek__BackingField_2; }
	inline void set_U3CleftU3Ek__BackingField_2(double value)
	{
		___U3CleftU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CtopU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(FaceLocation_t2324903414, ___U3CtopU3Ek__BackingField_3)); }
	inline double get_U3CtopU3Ek__BackingField_3() const { return ___U3CtopU3Ek__BackingField_3; }
	inline double* get_address_of_U3CtopU3Ek__BackingField_3() { return &___U3CtopU3Ek__BackingField_3; }
	inline void set_U3CtopU3Ek__BackingField_3(double value)
	{
		___U3CtopU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACELOCATION_T2324903414_H
#ifndef GENDER_T3451852799_H
#define GENDER_T3451852799_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.Gender
struct  Gender_t3451852799  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.Gender::<gender>k__BackingField
	String_t* ___U3CgenderU3Ek__BackingField_0;
	// System.Double IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.Gender::<score>k__BackingField
	double ___U3CscoreU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CgenderU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Gender_t3451852799, ___U3CgenderU3Ek__BackingField_0)); }
	inline String_t* get_U3CgenderU3Ek__BackingField_0() const { return ___U3CgenderU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CgenderU3Ek__BackingField_0() { return &___U3CgenderU3Ek__BackingField_0; }
	inline void set_U3CgenderU3Ek__BackingField_0(String_t* value)
	{
		___U3CgenderU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CgenderU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CscoreU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Gender_t3451852799, ___U3CscoreU3Ek__BackingField_1)); }
	inline double get_U3CscoreU3Ek__BackingField_1() const { return ___U3CscoreU3Ek__BackingField_1; }
	inline double* get_address_of_U3CscoreU3Ek__BackingField_1() { return &___U3CscoreU3Ek__BackingField_1; }
	inline void set_U3CscoreU3Ek__BackingField_1(double value)
	{
		___U3CscoreU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENDER_T3451852799_H
#ifndef SENTENCETONE_T3234952115_H
#define SENTENCETONE_T3234952115_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.SentenceTone
struct  SentenceTone_t3234952115  : public RuntimeObject
{
public:
	// System.Int32 IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.SentenceTone::<sentence_id>k__BackingField
	int32_t ___U3Csentence_idU3Ek__BackingField_0;
	// System.Int32 IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.SentenceTone::<input_from>k__BackingField
	int32_t ___U3Cinput_fromU3Ek__BackingField_1;
	// System.Int32 IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.SentenceTone::<input_to>k__BackingField
	int32_t ___U3Cinput_toU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.SentenceTone::<text>k__BackingField
	String_t* ___U3CtextU3Ek__BackingField_3;
	// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneCategory[] IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.SentenceTone::<tone_categories>k__BackingField
	ToneCategoryU5BU5D_t3565596459* ___U3Ctone_categoriesU3Ek__BackingField_4;
	// System.Double IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.SentenceTone::m_HighestScore
	double ___m_HighestScore_5;
	// System.String IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.SentenceTone::m_HighestScoreToneName
	String_t* ___m_HighestScoreToneName_6;
	// System.String IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.SentenceTone::m_HighestScoreToneCategoryName
	String_t* ___m_HighestScoreToneCategoryName_7;

public:
	inline static int32_t get_offset_of_U3Csentence_idU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SentenceTone_t3234952115, ___U3Csentence_idU3Ek__BackingField_0)); }
	inline int32_t get_U3Csentence_idU3Ek__BackingField_0() const { return ___U3Csentence_idU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3Csentence_idU3Ek__BackingField_0() { return &___U3Csentence_idU3Ek__BackingField_0; }
	inline void set_U3Csentence_idU3Ek__BackingField_0(int32_t value)
	{
		___U3Csentence_idU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3Cinput_fromU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(SentenceTone_t3234952115, ___U3Cinput_fromU3Ek__BackingField_1)); }
	inline int32_t get_U3Cinput_fromU3Ek__BackingField_1() const { return ___U3Cinput_fromU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3Cinput_fromU3Ek__BackingField_1() { return &___U3Cinput_fromU3Ek__BackingField_1; }
	inline void set_U3Cinput_fromU3Ek__BackingField_1(int32_t value)
	{
		___U3Cinput_fromU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3Cinput_toU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SentenceTone_t3234952115, ___U3Cinput_toU3Ek__BackingField_2)); }
	inline int32_t get_U3Cinput_toU3Ek__BackingField_2() const { return ___U3Cinput_toU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3Cinput_toU3Ek__BackingField_2() { return &___U3Cinput_toU3Ek__BackingField_2; }
	inline void set_U3Cinput_toU3Ek__BackingField_2(int32_t value)
	{
		___U3Cinput_toU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CtextU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SentenceTone_t3234952115, ___U3CtextU3Ek__BackingField_3)); }
	inline String_t* get_U3CtextU3Ek__BackingField_3() const { return ___U3CtextU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CtextU3Ek__BackingField_3() { return &___U3CtextU3Ek__BackingField_3; }
	inline void set_U3CtextU3Ek__BackingField_3(String_t* value)
	{
		___U3CtextU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3Ctone_categoriesU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(SentenceTone_t3234952115, ___U3Ctone_categoriesU3Ek__BackingField_4)); }
	inline ToneCategoryU5BU5D_t3565596459* get_U3Ctone_categoriesU3Ek__BackingField_4() const { return ___U3Ctone_categoriesU3Ek__BackingField_4; }
	inline ToneCategoryU5BU5D_t3565596459** get_address_of_U3Ctone_categoriesU3Ek__BackingField_4() { return &___U3Ctone_categoriesU3Ek__BackingField_4; }
	inline void set_U3Ctone_categoriesU3Ek__BackingField_4(ToneCategoryU5BU5D_t3565596459* value)
	{
		___U3Ctone_categoriesU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3Ctone_categoriesU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_m_HighestScore_5() { return static_cast<int32_t>(offsetof(SentenceTone_t3234952115, ___m_HighestScore_5)); }
	inline double get_m_HighestScore_5() const { return ___m_HighestScore_5; }
	inline double* get_address_of_m_HighestScore_5() { return &___m_HighestScore_5; }
	inline void set_m_HighestScore_5(double value)
	{
		___m_HighestScore_5 = value;
	}

	inline static int32_t get_offset_of_m_HighestScoreToneName_6() { return static_cast<int32_t>(offsetof(SentenceTone_t3234952115, ___m_HighestScoreToneName_6)); }
	inline String_t* get_m_HighestScoreToneName_6() const { return ___m_HighestScoreToneName_6; }
	inline String_t** get_address_of_m_HighestScoreToneName_6() { return &___m_HighestScoreToneName_6; }
	inline void set_m_HighestScoreToneName_6(String_t* value)
	{
		___m_HighestScoreToneName_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_HighestScoreToneName_6), value);
	}

	inline static int32_t get_offset_of_m_HighestScoreToneCategoryName_7() { return static_cast<int32_t>(offsetof(SentenceTone_t3234952115, ___m_HighestScoreToneCategoryName_7)); }
	inline String_t* get_m_HighestScoreToneCategoryName_7() const { return ___m_HighestScoreToneCategoryName_7; }
	inline String_t** get_address_of_m_HighestScoreToneCategoryName_7() { return &___m_HighestScoreToneCategoryName_7; }
	inline void set_m_HighestScoreToneCategoryName_7(String_t* value)
	{
		___m_HighestScoreToneCategoryName_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_HighestScoreToneCategoryName_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SENTENCETONE_T3234952115_H
#ifndef TONEANALYZER_T1356110496_H
#define TONEANALYZER_T1356110496_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzer
struct  ToneAnalyzer_t1356110496  : public RuntimeObject
{
public:

public:
};

struct ToneAnalyzer_t1356110496_StaticFields
{
public:
	// FullSerializer.fsSerializer IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzer::sm_Serializer
	fsSerializer_t4193731081 * ___sm_Serializer_1;

public:
	inline static int32_t get_offset_of_sm_Serializer_1() { return static_cast<int32_t>(offsetof(ToneAnalyzer_t1356110496_StaticFields, ___sm_Serializer_1)); }
	inline fsSerializer_t4193731081 * get_sm_Serializer_1() const { return ___sm_Serializer_1; }
	inline fsSerializer_t4193731081 ** get_address_of_sm_Serializer_1() { return &___sm_Serializer_1; }
	inline void set_sm_Serializer_1(fsSerializer_t4193731081 * value)
	{
		___sm_Serializer_1 = value;
		Il2CppCodeGenWriteBarrier((&___sm_Serializer_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TONEANALYZER_T1356110496_H
#ifndef TONECATEGORY_T3724297374_H
#define TONECATEGORY_T3724297374_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneCategory
struct  ToneCategory_t3724297374  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneCategory::<category_id>k__BackingField
	String_t* ___U3Ccategory_idU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneCategory::<category_name>k__BackingField
	String_t* ___U3Ccategory_nameU3Ek__BackingField_1;
	// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.Tone[] IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneCategory::<tones>k__BackingField
	ToneU5BU5D_t4171600311* ___U3CtonesU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3Ccategory_idU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ToneCategory_t3724297374, ___U3Ccategory_idU3Ek__BackingField_0)); }
	inline String_t* get_U3Ccategory_idU3Ek__BackingField_0() const { return ___U3Ccategory_idU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3Ccategory_idU3Ek__BackingField_0() { return &___U3Ccategory_idU3Ek__BackingField_0; }
	inline void set_U3Ccategory_idU3Ek__BackingField_0(String_t* value)
	{
		___U3Ccategory_idU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Ccategory_idU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3Ccategory_nameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ToneCategory_t3724297374, ___U3Ccategory_nameU3Ek__BackingField_1)); }
	inline String_t* get_U3Ccategory_nameU3Ek__BackingField_1() const { return ___U3Ccategory_nameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3Ccategory_nameU3Ek__BackingField_1() { return &___U3Ccategory_nameU3Ek__BackingField_1; }
	inline void set_U3Ccategory_nameU3Ek__BackingField_1(String_t* value)
	{
		___U3Ccategory_nameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3Ccategory_nameU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CtonesU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ToneCategory_t3724297374, ___U3CtonesU3Ek__BackingField_2)); }
	inline ToneU5BU5D_t4171600311* get_U3CtonesU3Ek__BackingField_2() const { return ___U3CtonesU3Ek__BackingField_2; }
	inline ToneU5BU5D_t4171600311** get_address_of_U3CtonesU3Ek__BackingField_2() { return &___U3CtonesU3Ek__BackingField_2; }
	inline void set_U3CtonesU3Ek__BackingField_2(ToneU5BU5D_t4171600311* value)
	{
		___U3CtonesU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtonesU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TONECATEGORY_T3724297374_H
#ifndef TONE_T1138009090_H
#define TONE_T1138009090_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.Tone
struct  Tone_t1138009090  : public RuntimeObject
{
public:
	// System.Double IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.Tone::<score>k__BackingField
	double ___U3CscoreU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.Tone::<tone_id>k__BackingField
	String_t* ___U3Ctone_idU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.Tone::<tone_name>k__BackingField
	String_t* ___U3Ctone_nameU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CscoreU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Tone_t1138009090, ___U3CscoreU3Ek__BackingField_0)); }
	inline double get_U3CscoreU3Ek__BackingField_0() const { return ___U3CscoreU3Ek__BackingField_0; }
	inline double* get_address_of_U3CscoreU3Ek__BackingField_0() { return &___U3CscoreU3Ek__BackingField_0; }
	inline void set_U3CscoreU3Ek__BackingField_0(double value)
	{
		___U3CscoreU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3Ctone_idU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Tone_t1138009090, ___U3Ctone_idU3Ek__BackingField_1)); }
	inline String_t* get_U3Ctone_idU3Ek__BackingField_1() const { return ___U3Ctone_idU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3Ctone_idU3Ek__BackingField_1() { return &___U3Ctone_idU3Ek__BackingField_1; }
	inline void set_U3Ctone_idU3Ek__BackingField_1(String_t* value)
	{
		___U3Ctone_idU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3Ctone_idU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3Ctone_nameU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Tone_t1138009090, ___U3Ctone_nameU3Ek__BackingField_2)); }
	inline String_t* get_U3Ctone_nameU3Ek__BackingField_2() const { return ___U3Ctone_nameU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3Ctone_nameU3Ek__BackingField_2() { return &___U3Ctone_nameU3Ek__BackingField_2; }
	inline void set_U3Ctone_nameU3Ek__BackingField_2(String_t* value)
	{
		___U3Ctone_nameU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3Ctone_nameU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TONE_T1138009090_H
#ifndef CHECKSERVICESTATUS_T1108037484_H
#define CHECKSERVICESTATUS_T1108037484_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzer/CheckServiceStatus
struct  CheckServiceStatus_t1108037484  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzer IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzer/CheckServiceStatus::m_Service
	ToneAnalyzer_t1356110496 * ___m_Service_0;
	// IBM.Watson.DeveloperCloud.Services.ServiceStatus IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzer/CheckServiceStatus::m_Callback
	ServiceStatus_t1443707987 * ___m_Callback_1;

public:
	inline static int32_t get_offset_of_m_Service_0() { return static_cast<int32_t>(offsetof(CheckServiceStatus_t1108037484, ___m_Service_0)); }
	inline ToneAnalyzer_t1356110496 * get_m_Service_0() const { return ___m_Service_0; }
	inline ToneAnalyzer_t1356110496 ** get_address_of_m_Service_0() { return &___m_Service_0; }
	inline void set_m_Service_0(ToneAnalyzer_t1356110496 * value)
	{
		___m_Service_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Service_0), value);
	}

	inline static int32_t get_offset_of_m_Callback_1() { return static_cast<int32_t>(offsetof(CheckServiceStatus_t1108037484, ___m_Callback_1)); }
	inline ServiceStatus_t1443707987 * get_m_Callback_1() const { return ___m_Callback_1; }
	inline ServiceStatus_t1443707987 ** get_address_of_m_Callback_1() { return &___m_Callback_1; }
	inline void set_m_Callback_1(ServiceStatus_t1443707987 * value)
	{
		___m_Callback_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Callback_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHECKSERVICESTATUS_T1108037484_H
#ifndef DILEMMASRESPONSE_T3767705817_H
#define DILEMMASRESPONSE_T3767705817_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.DilemmasResponse
struct  DilemmasResponse_t3767705817  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Problem IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.DilemmasResponse::<problem>k__BackingField
	Problem_t2814813345 * ___U3CproblemU3Ek__BackingField_0;
	// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Resolution IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.DilemmasResponse::<resolution>k__BackingField
	Resolution_t3867497320 * ___U3CresolutionU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CproblemU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DilemmasResponse_t3767705817, ___U3CproblemU3Ek__BackingField_0)); }
	inline Problem_t2814813345 * get_U3CproblemU3Ek__BackingField_0() const { return ___U3CproblemU3Ek__BackingField_0; }
	inline Problem_t2814813345 ** get_address_of_U3CproblemU3Ek__BackingField_0() { return &___U3CproblemU3Ek__BackingField_0; }
	inline void set_U3CproblemU3Ek__BackingField_0(Problem_t2814813345 * value)
	{
		___U3CproblemU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CproblemU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CresolutionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(DilemmasResponse_t3767705817, ___U3CresolutionU3Ek__BackingField_1)); }
	inline Resolution_t3867497320 * get_U3CresolutionU3Ek__BackingField_1() const { return ___U3CresolutionU3Ek__BackingField_1; }
	inline Resolution_t3867497320 ** get_address_of_U3CresolutionU3Ek__BackingField_1() { return &___U3CresolutionU3Ek__BackingField_1; }
	inline void set_U3CresolutionU3Ek__BackingField_1(Resolution_t3867497320 * value)
	{
		___U3CresolutionU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CresolutionU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DILEMMASRESPONSE_T3767705817_H
#ifndef GETCLASSIFIERSPERCLASSIFIERBRIEF_T3946924106_H
#define GETCLASSIFIERSPERCLASSIFIERBRIEF_T3946924106_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.GetClassifiersPerClassifierBrief
struct  GetClassifiersPerClassifierBrief_t3946924106  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.GetClassifiersPerClassifierBrief::<classifier_id>k__BackingField
	String_t* ___U3Cclassifier_idU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.GetClassifiersPerClassifierBrief::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3Cclassifier_idU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(GetClassifiersPerClassifierBrief_t3946924106, ___U3Cclassifier_idU3Ek__BackingField_0)); }
	inline String_t* get_U3Cclassifier_idU3Ek__BackingField_0() const { return ___U3Cclassifier_idU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3Cclassifier_idU3Ek__BackingField_0() { return &___U3Cclassifier_idU3Ek__BackingField_0; }
	inline void set_U3Cclassifier_idU3Ek__BackingField_0(String_t* value)
	{
		___U3Cclassifier_idU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cclassifier_idU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(GetClassifiersPerClassifierBrief_t3946924106, ___U3CnameU3Ek__BackingField_1)); }
	inline String_t* get_U3CnameU3Ek__BackingField_1() const { return ___U3CnameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_1() { return &___U3CnameU3Ek__BackingField_1; }
	inline void set_U3CnameU3Ek__BackingField_1(String_t* value)
	{
		___U3CnameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETCLASSIFIERSPERCLASSIFIERBRIEF_T3946924106_H
#ifndef GETCLASSIFIERSTOPLEVELBRIEF_T3177285335_H
#define GETCLASSIFIERSTOPLEVELBRIEF_T3177285335_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.GetClassifiersTopLevelBrief
struct  GetClassifiersTopLevelBrief_t3177285335  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.GetClassifiersPerClassifierBrief[] IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.GetClassifiersTopLevelBrief::<classifiers>k__BackingField
	GetClassifiersPerClassifierBriefU5BU5D_t1088138063* ___U3CclassifiersU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CclassifiersU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(GetClassifiersTopLevelBrief_t3177285335, ___U3CclassifiersU3Ek__BackingField_0)); }
	inline GetClassifiersPerClassifierBriefU5BU5D_t1088138063* get_U3CclassifiersU3Ek__BackingField_0() const { return ___U3CclassifiersU3Ek__BackingField_0; }
	inline GetClassifiersPerClassifierBriefU5BU5D_t1088138063** get_address_of_U3CclassifiersU3Ek__BackingField_0() { return &___U3CclassifiersU3Ek__BackingField_0; }
	inline void set_U3CclassifiersU3Ek__BackingField_0(GetClassifiersPerClassifierBriefU5BU5D_t1088138063* value)
	{
		___U3CclassifiersU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CclassifiersU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETCLASSIFIERSTOPLEVELBRIEF_T3177285335_H
#ifndef GETCOLLECTIONS_T2586322795_H
#define GETCOLLECTIONS_T2586322795_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.GetCollections
struct  GetCollections_t2586322795  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.CreateCollection[] IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.GetCollections::collections
	CreateCollectionU5BU5D_t2645069445* ___collections_0;

public:
	inline static int32_t get_offset_of_collections_0() { return static_cast<int32_t>(offsetof(GetCollections_t2586322795, ___collections_0)); }
	inline CreateCollectionU5BU5D_t2645069445* get_collections_0() const { return ___collections_0; }
	inline CreateCollectionU5BU5D_t2645069445** get_address_of_collections_0() { return &___collections_0; }
	inline void set_collections_0(CreateCollectionU5BU5D_t2645069445* value)
	{
		___collections_0 = value;
		Il2CppCodeGenWriteBarrier((&___collections_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETCOLLECTIONS_T2586322795_H
#ifndef CLASS_T4054986160_H
#define CLASS_T4054986160_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.Class
struct  Class_t4054986160  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.Class::<m_Class>k__BackingField
	String_t* ___U3Cm_ClassU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3Cm_ClassU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Class_t4054986160, ___U3Cm_ClassU3Ek__BackingField_0)); }
	inline String_t* get_U3Cm_ClassU3Ek__BackingField_0() const { return ___U3Cm_ClassU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3Cm_ClassU3Ek__BackingField_0() { return &___U3Cm_ClassU3Ek__BackingField_0; }
	inline void set_U3Cm_ClassU3Ek__BackingField_0(String_t* value)
	{
		___U3Cm_ClassU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cm_ClassU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLASS_T4054986160_H
#ifndef GETCOLLECTIONIMAGES_T1669456110_H
#define GETCOLLECTIONIMAGES_T1669456110_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.GetCollectionImages
struct  GetCollectionImages_t1669456110  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.GetCollectionsBrief[] IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.GetCollectionImages::<images>k__BackingField
	GetCollectionsBriefU5BU5D_t3322171774* ___U3CimagesU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CimagesU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(GetCollectionImages_t1669456110, ___U3CimagesU3Ek__BackingField_0)); }
	inline GetCollectionsBriefU5BU5D_t3322171774* get_U3CimagesU3Ek__BackingField_0() const { return ___U3CimagesU3Ek__BackingField_0; }
	inline GetCollectionsBriefU5BU5D_t3322171774** get_address_of_U3CimagesU3Ek__BackingField_0() { return &___U3CimagesU3Ek__BackingField_0; }
	inline void set_U3CimagesU3Ek__BackingField_0(GetCollectionsBriefU5BU5D_t3322171774* value)
	{
		___U3CimagesU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CimagesU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETCOLLECTIONIMAGES_T1669456110_H
#ifndef CREATECOLLECTION_T3312638156_H
#define CREATECOLLECTION_T3312638156_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.CreateCollection
struct  CreateCollection_t3312638156  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.CreateCollection::<collection_id>k__BackingField
	String_t* ___U3Ccollection_idU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.CreateCollection::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.CreateCollection::<created>k__BackingField
	String_t* ___U3CcreatedU3Ek__BackingField_2;
	// System.Int32 IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.CreateCollection::<images>k__BackingField
	int32_t ___U3CimagesU3Ek__BackingField_3;
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.CreateCollection::<status>k__BackingField
	String_t* ___U3CstatusU3Ek__BackingField_4;
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.CreateCollection::<capacity>k__BackingField
	String_t* ___U3CcapacityU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3Ccollection_idU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CreateCollection_t3312638156, ___U3Ccollection_idU3Ek__BackingField_0)); }
	inline String_t* get_U3Ccollection_idU3Ek__BackingField_0() const { return ___U3Ccollection_idU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3Ccollection_idU3Ek__BackingField_0() { return &___U3Ccollection_idU3Ek__BackingField_0; }
	inline void set_U3Ccollection_idU3Ek__BackingField_0(String_t* value)
	{
		___U3Ccollection_idU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Ccollection_idU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CreateCollection_t3312638156, ___U3CnameU3Ek__BackingField_1)); }
	inline String_t* get_U3CnameU3Ek__BackingField_1() const { return ___U3CnameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_1() { return &___U3CnameU3Ek__BackingField_1; }
	inline void set_U3CnameU3Ek__BackingField_1(String_t* value)
	{
		___U3CnameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CcreatedU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CreateCollection_t3312638156, ___U3CcreatedU3Ek__BackingField_2)); }
	inline String_t* get_U3CcreatedU3Ek__BackingField_2() const { return ___U3CcreatedU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CcreatedU3Ek__BackingField_2() { return &___U3CcreatedU3Ek__BackingField_2; }
	inline void set_U3CcreatedU3Ek__BackingField_2(String_t* value)
	{
		___U3CcreatedU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcreatedU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CimagesU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(CreateCollection_t3312638156, ___U3CimagesU3Ek__BackingField_3)); }
	inline int32_t get_U3CimagesU3Ek__BackingField_3() const { return ___U3CimagesU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CimagesU3Ek__BackingField_3() { return &___U3CimagesU3Ek__BackingField_3; }
	inline void set_U3CimagesU3Ek__BackingField_3(int32_t value)
	{
		___U3CimagesU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CstatusU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(CreateCollection_t3312638156, ___U3CstatusU3Ek__BackingField_4)); }
	inline String_t* get_U3CstatusU3Ek__BackingField_4() const { return ___U3CstatusU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CstatusU3Ek__BackingField_4() { return &___U3CstatusU3Ek__BackingField_4; }
	inline void set_U3CstatusU3Ek__BackingField_4(String_t* value)
	{
		___U3CstatusU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstatusU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CcapacityU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(CreateCollection_t3312638156, ___U3CcapacityU3Ek__BackingField_5)); }
	inline String_t* get_U3CcapacityU3Ek__BackingField_5() const { return ___U3CcapacityU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CcapacityU3Ek__BackingField_5() { return &___U3CcapacityU3Ek__BackingField_5; }
	inline void set_U3CcapacityU3Ek__BackingField_5(String_t* value)
	{
		___U3CcapacityU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcapacityU3Ek__BackingField_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREATECOLLECTION_T3312638156_H
#ifndef TONEANALYZERRESPONSE_T3391014235_H
#define TONEANALYZERRESPONSE_T3391014235_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzerResponse
struct  ToneAnalyzerResponse_t3391014235  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.DocumentTone IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzerResponse::<document_tone>k__BackingField
	DocumentTone_t2537937785 * ___U3Cdocument_toneU3Ek__BackingField_0;
	// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.SentenceTone[] IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzerResponse::<sentences_tone>k__BackingField
	SentenceToneU5BU5D_t255016994* ___U3Csentences_toneU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3Cdocument_toneU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ToneAnalyzerResponse_t3391014235, ___U3Cdocument_toneU3Ek__BackingField_0)); }
	inline DocumentTone_t2537937785 * get_U3Cdocument_toneU3Ek__BackingField_0() const { return ___U3Cdocument_toneU3Ek__BackingField_0; }
	inline DocumentTone_t2537937785 ** get_address_of_U3Cdocument_toneU3Ek__BackingField_0() { return &___U3Cdocument_toneU3Ek__BackingField_0; }
	inline void set_U3Cdocument_toneU3Ek__BackingField_0(DocumentTone_t2537937785 * value)
	{
		___U3Cdocument_toneU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cdocument_toneU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3Csentences_toneU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ToneAnalyzerResponse_t3391014235, ___U3Csentences_toneU3Ek__BackingField_1)); }
	inline SentenceToneU5BU5D_t255016994* get_U3Csentences_toneU3Ek__BackingField_1() const { return ___U3Csentences_toneU3Ek__BackingField_1; }
	inline SentenceToneU5BU5D_t255016994** get_address_of_U3Csentences_toneU3Ek__BackingField_1() { return &___U3Csentences_toneU3Ek__BackingField_1; }
	inline void set_U3Csentences_toneU3Ek__BackingField_1(SentenceToneU5BU5D_t255016994* value)
	{
		___U3Csentences_toneU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3Csentences_toneU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TONEANALYZERRESPONSE_T3391014235_H
#ifndef DOCUMENTTONE_T2537937785_H
#define DOCUMENTTONE_T2537937785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.DocumentTone
struct  DocumentTone_t2537937785  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneCategory[] IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.DocumentTone::<tone_categories>k__BackingField
	ToneCategoryU5BU5D_t3565596459* ___U3Ctone_categoriesU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3Ctone_categoriesU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DocumentTone_t2537937785, ___U3Ctone_categoriesU3Ek__BackingField_0)); }
	inline ToneCategoryU5BU5D_t3565596459* get_U3Ctone_categoriesU3Ek__BackingField_0() const { return ___U3Ctone_categoriesU3Ek__BackingField_0; }
	inline ToneCategoryU5BU5D_t3565596459** get_address_of_U3Ctone_categoriesU3Ek__BackingField_0() { return &___U3Ctone_categoriesU3Ek__BackingField_0; }
	inline void set_U3Ctone_categoriesU3Ek__BackingField_0(ToneCategoryU5BU5D_t3565596459* value)
	{
		___U3Ctone_categoriesU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Ctone_categoriesU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOCUMENTTONE_T2537937785_H
#ifndef GETCLASSIFIERSPERCLASSIFIERVERBOSE_T3597794938_H
#define GETCLASSIFIERSPERCLASSIFIERVERBOSE_T3597794938_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.GetClassifiersPerClassifierVerbose
struct  GetClassifiersPerClassifierVerbose_t3597794938  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.GetClassifiersPerClassifierVerbose::<classifier_id>k__BackingField
	String_t* ___U3Cclassifier_idU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.GetClassifiersPerClassifierVerbose::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.GetClassifiersPerClassifierVerbose::<owner>k__BackingField
	String_t* ___U3CownerU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.GetClassifiersPerClassifierVerbose::<status>k__BackingField
	String_t* ___U3CstatusU3Ek__BackingField_3;
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.GetClassifiersPerClassifierVerbose::<explanation>k__BackingField
	String_t* ___U3CexplanationU3Ek__BackingField_4;
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.GetClassifiersPerClassifierVerbose::<created>k__BackingField
	String_t* ___U3CcreatedU3Ek__BackingField_5;
	// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.Class[] IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.GetClassifiersPerClassifierVerbose::<classes>k__BackingField
	ClassU5BU5D_t1647736209* ___U3CclassesU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3Cclassifier_idU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(GetClassifiersPerClassifierVerbose_t3597794938, ___U3Cclassifier_idU3Ek__BackingField_0)); }
	inline String_t* get_U3Cclassifier_idU3Ek__BackingField_0() const { return ___U3Cclassifier_idU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3Cclassifier_idU3Ek__BackingField_0() { return &___U3Cclassifier_idU3Ek__BackingField_0; }
	inline void set_U3Cclassifier_idU3Ek__BackingField_0(String_t* value)
	{
		___U3Cclassifier_idU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cclassifier_idU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(GetClassifiersPerClassifierVerbose_t3597794938, ___U3CnameU3Ek__BackingField_1)); }
	inline String_t* get_U3CnameU3Ek__BackingField_1() const { return ___U3CnameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_1() { return &___U3CnameU3Ek__BackingField_1; }
	inline void set_U3CnameU3Ek__BackingField_1(String_t* value)
	{
		___U3CnameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CownerU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(GetClassifiersPerClassifierVerbose_t3597794938, ___U3CownerU3Ek__BackingField_2)); }
	inline String_t* get_U3CownerU3Ek__BackingField_2() const { return ___U3CownerU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CownerU3Ek__BackingField_2() { return &___U3CownerU3Ek__BackingField_2; }
	inline void set_U3CownerU3Ek__BackingField_2(String_t* value)
	{
		___U3CownerU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CownerU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CstatusU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(GetClassifiersPerClassifierVerbose_t3597794938, ___U3CstatusU3Ek__BackingField_3)); }
	inline String_t* get_U3CstatusU3Ek__BackingField_3() const { return ___U3CstatusU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CstatusU3Ek__BackingField_3() { return &___U3CstatusU3Ek__BackingField_3; }
	inline void set_U3CstatusU3Ek__BackingField_3(String_t* value)
	{
		___U3CstatusU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstatusU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CexplanationU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(GetClassifiersPerClassifierVerbose_t3597794938, ___U3CexplanationU3Ek__BackingField_4)); }
	inline String_t* get_U3CexplanationU3Ek__BackingField_4() const { return ___U3CexplanationU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CexplanationU3Ek__BackingField_4() { return &___U3CexplanationU3Ek__BackingField_4; }
	inline void set_U3CexplanationU3Ek__BackingField_4(String_t* value)
	{
		___U3CexplanationU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CexplanationU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CcreatedU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(GetClassifiersPerClassifierVerbose_t3597794938, ___U3CcreatedU3Ek__BackingField_5)); }
	inline String_t* get_U3CcreatedU3Ek__BackingField_5() const { return ___U3CcreatedU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CcreatedU3Ek__BackingField_5() { return &___U3CcreatedU3Ek__BackingField_5; }
	inline void set_U3CcreatedU3Ek__BackingField_5(String_t* value)
	{
		___U3CcreatedU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcreatedU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CclassesU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(GetClassifiersPerClassifierVerbose_t3597794938, ___U3CclassesU3Ek__BackingField_6)); }
	inline ClassU5BU5D_t1647736209* get_U3CclassesU3Ek__BackingField_6() const { return ___U3CclassesU3Ek__BackingField_6; }
	inline ClassU5BU5D_t1647736209** get_address_of_U3CclassesU3Ek__BackingField_6() { return &___U3CclassesU3Ek__BackingField_6; }
	inline void set_U3CclassesU3Ek__BackingField_6(ClassU5BU5D_t1647736209* value)
	{
		___U3CclassesU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CclassesU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETCLASSIFIERSPERCLASSIFIERVERBOSE_T3597794938_H
#ifndef CHECKSERVICESTATUS_T3651453794_H
#define CHECKSERVICESTATUS_T3651453794_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/CheckServiceStatus
struct  CheckServiceStatus_t3651453794  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/CheckServiceStatus::m_Service
	TextToSpeech_t3349357562 * ___m_Service_0;
	// IBM.Watson.DeveloperCloud.Services.ServiceStatus IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/CheckServiceStatus::m_Callback
	ServiceStatus_t1443707987 * ___m_Callback_1;

public:
	inline static int32_t get_offset_of_m_Service_0() { return static_cast<int32_t>(offsetof(CheckServiceStatus_t3651453794, ___m_Service_0)); }
	inline TextToSpeech_t3349357562 * get_m_Service_0() const { return ___m_Service_0; }
	inline TextToSpeech_t3349357562 ** get_address_of_m_Service_0() { return &___m_Service_0; }
	inline void set_m_Service_0(TextToSpeech_t3349357562 * value)
	{
		___m_Service_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Service_0), value);
	}

	inline static int32_t get_offset_of_m_Callback_1() { return static_cast<int32_t>(offsetof(CheckServiceStatus_t3651453794, ___m_Callback_1)); }
	inline ServiceStatus_t1443707987 * get_m_Callback_1() const { return ___m_Callback_1; }
	inline ServiceStatus_t1443707987 ** get_address_of_m_Callback_1() { return &___m_Callback_1; }
	inline void set_m_Callback_1(ServiceStatus_t1443707987 * value)
	{
		___m_Callback_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Callback_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHECKSERVICESTATUS_T3651453794_H
#ifndef ADDCUSTOMIZATIONWORDSREQUEST_T993580433_H
#define ADDCUSTOMIZATIONWORDSREQUEST_T993580433_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordsRequest
struct  AddCustomizationWordsRequest_t993580433  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordsCallback IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordsRequest::<Callback>k__BackingField
	AddCustomizationWordsCallback_t2578578585 * ___U3CCallbackU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordsRequest::<CustomizationID>k__BackingField
	String_t* ___U3CCustomizationIDU3Ek__BackingField_12;
	// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Words IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordsRequest::<Words>k__BackingField
	Words_t2948214629 * ___U3CWordsU3Ek__BackingField_13;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordsRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(AddCustomizationWordsRequest_t993580433, ___U3CCallbackU3Ek__BackingField_11)); }
	inline AddCustomizationWordsCallback_t2578578585 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline AddCustomizationWordsCallback_t2578578585 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(AddCustomizationWordsCallback_t2578578585 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCustomizationIDU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(AddCustomizationWordsRequest_t993580433, ___U3CCustomizationIDU3Ek__BackingField_12)); }
	inline String_t* get_U3CCustomizationIDU3Ek__BackingField_12() const { return ___U3CCustomizationIDU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CCustomizationIDU3Ek__BackingField_12() { return &___U3CCustomizationIDU3Ek__BackingField_12; }
	inline void set_U3CCustomizationIDU3Ek__BackingField_12(String_t* value)
	{
		___U3CCustomizationIDU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCustomizationIDU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CWordsU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(AddCustomizationWordsRequest_t993580433, ___U3CWordsU3Ek__BackingField_13)); }
	inline Words_t2948214629 * get_U3CWordsU3Ek__BackingField_13() const { return ___U3CWordsU3Ek__BackingField_13; }
	inline Words_t2948214629 ** get_address_of_U3CWordsU3Ek__BackingField_13() { return &___U3CWordsU3Ek__BackingField_13; }
	inline void set_U3CWordsU3Ek__BackingField_13(Words_t2948214629 * value)
	{
		___U3CWordsU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CWordsU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(AddCustomizationWordsRequest_t993580433, ___U3CDataU3Ek__BackingField_14)); }
	inline String_t* get_U3CDataU3Ek__BackingField_14() const { return ___U3CDataU3Ek__BackingField_14; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_14() { return &___U3CDataU3Ek__BackingField_14; }
	inline void set_U3CDataU3Ek__BackingField_14(String_t* value)
	{
		___U3CDataU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDCUSTOMIZATIONWORDSREQUEST_T993580433_H
#ifndef GETCUSTOMIZATIONWORDSREQUEST_T110549728_H
#define GETCUSTOMIZATIONWORDSREQUEST_T110549728_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordsRequest
struct  GetCustomizationWordsRequest_t110549728  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordsCallback IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordsRequest::<Callback>k__BackingField
	GetCustomizationWordsCallback_t2709615664 * ___U3CCallbackU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordsRequest::<CustomizationID>k__BackingField
	String_t* ___U3CCustomizationIDU3Ek__BackingField_12;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordsRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetCustomizationWordsRequest_t110549728, ___U3CCallbackU3Ek__BackingField_11)); }
	inline GetCustomizationWordsCallback_t2709615664 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline GetCustomizationWordsCallback_t2709615664 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(GetCustomizationWordsCallback_t2709615664 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCustomizationIDU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GetCustomizationWordsRequest_t110549728, ___U3CCustomizationIDU3Ek__BackingField_12)); }
	inline String_t* get_U3CCustomizationIDU3Ek__BackingField_12() const { return ___U3CCustomizationIDU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CCustomizationIDU3Ek__BackingField_12() { return &___U3CCustomizationIDU3Ek__BackingField_12; }
	inline void set_U3CCustomizationIDU3Ek__BackingField_12(String_t* value)
	{
		___U3CCustomizationIDU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCustomizationIDU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(GetCustomizationWordsRequest_t110549728, ___U3CDataU3Ek__BackingField_13)); }
	inline String_t* get_U3CDataU3Ek__BackingField_13() const { return ___U3CDataU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_13() { return &___U3CDataU3Ek__BackingField_13; }
	inline void set_U3CDataU3Ek__BackingField_13(String_t* value)
	{
		___U3CDataU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETCUSTOMIZATIONWORDSREQUEST_T110549728_H
#ifndef BOOLEAN_T3825574718_H
#define BOOLEAN_T3825574718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t3825574718 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t3825574718, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t3825574718_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t3825574718_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t3825574718_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T3825574718_H
#ifndef GETCUSTOMIZATIONWORDREQUEST_T1280810157_H
#define GETCUSTOMIZATIONWORDREQUEST_T1280810157_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordRequest
struct  GetCustomizationWordRequest_t1280810157  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordCallback IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordRequest::<Callback>k__BackingField
	GetCustomizationWordCallback_t2485962781 * ___U3CCallbackU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordRequest::<CustomizationID>k__BackingField
	String_t* ___U3CCustomizationIDU3Ek__BackingField_12;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordRequest::<Word>k__BackingField
	String_t* ___U3CWordU3Ek__BackingField_13;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetCustomizationWordRequest_t1280810157, ___U3CCallbackU3Ek__BackingField_11)); }
	inline GetCustomizationWordCallback_t2485962781 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline GetCustomizationWordCallback_t2485962781 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(GetCustomizationWordCallback_t2485962781 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCustomizationIDU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GetCustomizationWordRequest_t1280810157, ___U3CCustomizationIDU3Ek__BackingField_12)); }
	inline String_t* get_U3CCustomizationIDU3Ek__BackingField_12() const { return ___U3CCustomizationIDU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CCustomizationIDU3Ek__BackingField_12() { return &___U3CCustomizationIDU3Ek__BackingField_12; }
	inline void set_U3CCustomizationIDU3Ek__BackingField_12(String_t* value)
	{
		___U3CCustomizationIDU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCustomizationIDU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CWordU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(GetCustomizationWordRequest_t1280810157, ___U3CWordU3Ek__BackingField_13)); }
	inline String_t* get_U3CWordU3Ek__BackingField_13() const { return ___U3CWordU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CWordU3Ek__BackingField_13() { return &___U3CWordU3Ek__BackingField_13; }
	inline void set_U3CWordU3Ek__BackingField_13(String_t* value)
	{
		___U3CWordU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CWordU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(GetCustomizationWordRequest_t1280810157, ___U3CDataU3Ek__BackingField_14)); }
	inline String_t* get_U3CDataU3Ek__BackingField_14() const { return ___U3CDataU3Ek__BackingField_14; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_14() { return &___U3CDataU3Ek__BackingField_14; }
	inline void set_U3CDataU3Ek__BackingField_14(String_t* value)
	{
		___U3CDataU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETCUSTOMIZATIONWORDREQUEST_T1280810157_H
#ifndef VOID_T1841601450_H
#define VOID_T1841601450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1841601450 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1841601450_H
#ifndef DELETECUSTOMIZATIONWORDREQUEST_T3182066146_H
#define DELETECUSTOMIZATIONWORDREQUEST_T3182066146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/DeleteCustomizationWordRequest
struct  DeleteCustomizationWordRequest_t3182066146  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/OnDeleteCustomizationWordCallback IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/DeleteCustomizationWordRequest::<Callback>k__BackingField
	OnDeleteCustomizationWordCallback_t2544578463 * ___U3CCallbackU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/DeleteCustomizationWordRequest::<CustomizationID>k__BackingField
	String_t* ___U3CCustomizationIDU3Ek__BackingField_12;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/DeleteCustomizationWordRequest::<Word>k__BackingField
	String_t* ___U3CWordU3Ek__BackingField_13;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/DeleteCustomizationWordRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(DeleteCustomizationWordRequest_t3182066146, ___U3CCallbackU3Ek__BackingField_11)); }
	inline OnDeleteCustomizationWordCallback_t2544578463 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline OnDeleteCustomizationWordCallback_t2544578463 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(OnDeleteCustomizationWordCallback_t2544578463 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCustomizationIDU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(DeleteCustomizationWordRequest_t3182066146, ___U3CCustomizationIDU3Ek__BackingField_12)); }
	inline String_t* get_U3CCustomizationIDU3Ek__BackingField_12() const { return ___U3CCustomizationIDU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CCustomizationIDU3Ek__BackingField_12() { return &___U3CCustomizationIDU3Ek__BackingField_12; }
	inline void set_U3CCustomizationIDU3Ek__BackingField_12(String_t* value)
	{
		___U3CCustomizationIDU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCustomizationIDU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CWordU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(DeleteCustomizationWordRequest_t3182066146, ___U3CWordU3Ek__BackingField_13)); }
	inline String_t* get_U3CWordU3Ek__BackingField_13() const { return ___U3CWordU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CWordU3Ek__BackingField_13() { return &___U3CWordU3Ek__BackingField_13; }
	inline void set_U3CWordU3Ek__BackingField_13(String_t* value)
	{
		___U3CWordU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CWordU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(DeleteCustomizationWordRequest_t3182066146, ___U3CDataU3Ek__BackingField_14)); }
	inline String_t* get_U3CDataU3Ek__BackingField_14() const { return ___U3CDataU3Ek__BackingField_14; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_14() { return &___U3CDataU3Ek__BackingField_14; }
	inline void set_U3CDataU3Ek__BackingField_14(String_t* value)
	{
		___U3CDataU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELETECUSTOMIZATIONWORDREQUEST_T3182066146_H
#ifndef CREATECUSTOMIZATIONREQUEST_T295745401_H
#define CREATECUSTOMIZATIONREQUEST_T295745401_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/CreateCustomizationRequest
struct  CreateCustomizationRequest_t295745401  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/CreateCustomizationCallback IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/CreateCustomizationRequest::<Callback>k__BackingField
	CreateCustomizationCallback_t3071584865 * ___U3CCallbackU3Ek__BackingField_11;
	// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomVoice IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/CreateCustomizationRequest::<CustomVoice>k__BackingField
	CustomVoice_t330695553 * ___U3CCustomVoiceU3Ek__BackingField_12;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/CreateCustomizationRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(CreateCustomizationRequest_t295745401, ___U3CCallbackU3Ek__BackingField_11)); }
	inline CreateCustomizationCallback_t3071584865 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline CreateCustomizationCallback_t3071584865 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(CreateCustomizationCallback_t3071584865 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCustomVoiceU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(CreateCustomizationRequest_t295745401, ___U3CCustomVoiceU3Ek__BackingField_12)); }
	inline CustomVoice_t330695553 * get_U3CCustomVoiceU3Ek__BackingField_12() const { return ___U3CCustomVoiceU3Ek__BackingField_12; }
	inline CustomVoice_t330695553 ** get_address_of_U3CCustomVoiceU3Ek__BackingField_12() { return &___U3CCustomVoiceU3Ek__BackingField_12; }
	inline void set_U3CCustomVoiceU3Ek__BackingField_12(CustomVoice_t330695553 * value)
	{
		___U3CCustomVoiceU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCustomVoiceU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(CreateCustomizationRequest_t295745401, ___U3CDataU3Ek__BackingField_13)); }
	inline String_t* get_U3CDataU3Ek__BackingField_13() const { return ___U3CDataU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_13() { return &___U3CDataU3Ek__BackingField_13; }
	inline void set_U3CDataU3Ek__BackingField_13(String_t* value)
	{
		___U3CDataU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREATECUSTOMIZATIONREQUEST_T295745401_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t1328083999* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t1328083999* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t1328083999** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t1328083999* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef GETCUSTOMIZATIONSREQ_T2206589443_H
#define GETCUSTOMIZATIONSREQ_T2206589443_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationsReq
struct  GetCustomizationsReq_t2206589443  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationsCallback IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationsReq::<Callback>k__BackingField
	GetCustomizationsCallback_t201646696 * ___U3CCallbackU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationsReq::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetCustomizationsReq_t2206589443, ___U3CCallbackU3Ek__BackingField_11)); }
	inline GetCustomizationsCallback_t201646696 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline GetCustomizationsCallback_t201646696 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(GetCustomizationsCallback_t201646696 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GetCustomizationsReq_t2206589443, ___U3CDataU3Ek__BackingField_12)); }
	inline String_t* get_U3CDataU3Ek__BackingField_12() const { return ___U3CDataU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_12() { return &___U3CDataU3Ek__BackingField_12; }
	inline void set_U3CDataU3Ek__BackingField_12(String_t* value)
	{
		___U3CDataU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETCUSTOMIZATIONSREQ_T2206589443_H
#ifndef UPDATECUSTOMIZATIONREQUEST_T1669343124_H
#define UPDATECUSTOMIZATIONREQUEST_T1669343124_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/UpdateCustomizationRequest
struct  UpdateCustomizationRequest_t1669343124  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/UpdateCustomizationCallback IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/UpdateCustomizationRequest::<Callback>k__BackingField
	UpdateCustomizationCallback_t3026233620 * ___U3CCallbackU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/UpdateCustomizationRequest::<CustomizationID>k__BackingField
	String_t* ___U3CCustomizationIDU3Ek__BackingField_12;
	// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomVoiceUpdate IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/UpdateCustomizationRequest::<CustomVoiceUpdate>k__BackingField
	CustomVoiceUpdate_t1706248840 * ___U3CCustomVoiceUpdateU3Ek__BackingField_13;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/UpdateCustomizationRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(UpdateCustomizationRequest_t1669343124, ___U3CCallbackU3Ek__BackingField_11)); }
	inline UpdateCustomizationCallback_t3026233620 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline UpdateCustomizationCallback_t3026233620 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(UpdateCustomizationCallback_t3026233620 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCustomizationIDU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(UpdateCustomizationRequest_t1669343124, ___U3CCustomizationIDU3Ek__BackingField_12)); }
	inline String_t* get_U3CCustomizationIDU3Ek__BackingField_12() const { return ___U3CCustomizationIDU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CCustomizationIDU3Ek__BackingField_12() { return &___U3CCustomizationIDU3Ek__BackingField_12; }
	inline void set_U3CCustomizationIDU3Ek__BackingField_12(String_t* value)
	{
		___U3CCustomizationIDU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCustomizationIDU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CCustomVoiceUpdateU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(UpdateCustomizationRequest_t1669343124, ___U3CCustomVoiceUpdateU3Ek__BackingField_13)); }
	inline CustomVoiceUpdate_t1706248840 * get_U3CCustomVoiceUpdateU3Ek__BackingField_13() const { return ___U3CCustomVoiceUpdateU3Ek__BackingField_13; }
	inline CustomVoiceUpdate_t1706248840 ** get_address_of_U3CCustomVoiceUpdateU3Ek__BackingField_13() { return &___U3CCustomVoiceUpdateU3Ek__BackingField_13; }
	inline void set_U3CCustomVoiceUpdateU3Ek__BackingField_13(CustomVoiceUpdate_t1706248840 * value)
	{
		___U3CCustomVoiceUpdateU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCustomVoiceUpdateU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(UpdateCustomizationRequest_t1669343124, ___U3CDataU3Ek__BackingField_14)); }
	inline String_t* get_U3CDataU3Ek__BackingField_14() const { return ___U3CDataU3Ek__BackingField_14; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_14() { return &___U3CDataU3Ek__BackingField_14; }
	inline void set_U3CDataU3Ek__BackingField_14(String_t* value)
	{
		___U3CDataU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATECUSTOMIZATIONREQUEST_T1669343124_H
#ifndef GETCUSTOMIZATIONREQUEST_T1351603463_H
#define GETCUSTOMIZATIONREQUEST_T1351603463_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationRequest
struct  GetCustomizationRequest_t1351603463  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationCallback IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationRequest::<Callback>k__BackingField
	GetCustomizationCallback_t4165371535 * ___U3CCallbackU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationRequest::<CustomizationID>k__BackingField
	String_t* ___U3CCustomizationIDU3Ek__BackingField_12;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetCustomizationRequest_t1351603463, ___U3CCallbackU3Ek__BackingField_11)); }
	inline GetCustomizationCallback_t4165371535 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline GetCustomizationCallback_t4165371535 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(GetCustomizationCallback_t4165371535 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCustomizationIDU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GetCustomizationRequest_t1351603463, ___U3CCustomizationIDU3Ek__BackingField_12)); }
	inline String_t* get_U3CCustomizationIDU3Ek__BackingField_12() const { return ___U3CCustomizationIDU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CCustomizationIDU3Ek__BackingField_12() { return &___U3CCustomizationIDU3Ek__BackingField_12; }
	inline void set_U3CCustomizationIDU3Ek__BackingField_12(String_t* value)
	{
		___U3CCustomizationIDU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCustomizationIDU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(GetCustomizationRequest_t1351603463, ___U3CDataU3Ek__BackingField_13)); }
	inline String_t* get_U3CDataU3Ek__BackingField_13() const { return ___U3CDataU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_13() { return &___U3CDataU3Ek__BackingField_13; }
	inline void set_U3CDataU3Ek__BackingField_13(String_t* value)
	{
		___U3CDataU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETCUSTOMIZATIONREQUEST_T1351603463_H
#ifndef DELETECUSTOMIZATIONREQUEST_T4044506578_H
#define DELETECUSTOMIZATIONREQUEST_T4044506578_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/DeleteCustomizationRequest
struct  DeleteCustomizationRequest_t4044506578  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/OnDeleteCustomizationCallback IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/DeleteCustomizationRequest::<Callback>k__BackingField
	OnDeleteCustomizationCallback_t2862971265 * ___U3CCallbackU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/DeleteCustomizationRequest::<CustomizationID>k__BackingField
	String_t* ___U3CCustomizationIDU3Ek__BackingField_12;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/DeleteCustomizationRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(DeleteCustomizationRequest_t4044506578, ___U3CCallbackU3Ek__BackingField_11)); }
	inline OnDeleteCustomizationCallback_t2862971265 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline OnDeleteCustomizationCallback_t2862971265 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(OnDeleteCustomizationCallback_t2862971265 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCustomizationIDU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(DeleteCustomizationRequest_t4044506578, ___U3CCustomizationIDU3Ek__BackingField_12)); }
	inline String_t* get_U3CCustomizationIDU3Ek__BackingField_12() const { return ___U3CCustomizationIDU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CCustomizationIDU3Ek__BackingField_12() { return &___U3CCustomizationIDU3Ek__BackingField_12; }
	inline void set_U3CCustomizationIDU3Ek__BackingField_12(String_t* value)
	{
		___U3CCustomizationIDU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCustomizationIDU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(DeleteCustomizationRequest_t4044506578, ___U3CDataU3Ek__BackingField_13)); }
	inline String_t* get_U3CDataU3Ek__BackingField_13() const { return ___U3CDataU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_13() { return &___U3CDataU3Ek__BackingField_13; }
	inline void set_U3CDataU3Ek__BackingField_13(String_t* value)
	{
		___U3CDataU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELETECUSTOMIZATIONREQUEST_T4044506578_H
#ifndef VALUERANGE_T543481982_H
#define VALUERANGE_T543481982_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.ValueRange
struct  ValueRange_t543481982  : public Range_t2396059803
{
public:
	// System.Double IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.ValueRange::<low>k__BackingField
	double ___U3ClowU3Ek__BackingField_0;
	// System.Double IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.ValueRange::<high>k__BackingField
	double ___U3ChighU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3ClowU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ValueRange_t543481982, ___U3ClowU3Ek__BackingField_0)); }
	inline double get_U3ClowU3Ek__BackingField_0() const { return ___U3ClowU3Ek__BackingField_0; }
	inline double* get_address_of_U3ClowU3Ek__BackingField_0() { return &___U3ClowU3Ek__BackingField_0; }
	inline void set_U3ClowU3Ek__BackingField_0(double value)
	{
		___U3ClowU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3ChighU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ValueRange_t543481982, ___U3ChighU3Ek__BackingField_1)); }
	inline double get_U3ChighU3Ek__BackingField_1() const { return ___U3ChighU3Ek__BackingField_1; }
	inline double* get_address_of_U3ChighU3Ek__BackingField_1() { return &___U3ChighU3Ek__BackingField_1; }
	inline void set_U3ChighU3Ek__BackingField_1(double value)
	{
		___U3ChighU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALUERANGE_T543481982_H
#ifndef DATERANGE_T3049499159_H
#define DATERANGE_T3049499159_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.DateRange
struct  DateRange_t3049499159  : public Range_t2396059803
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.DateRange::<low>k__BackingField
	String_t* ___U3ClowU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.DateRange::<high>k__BackingField
	String_t* ___U3ChighU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3ClowU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DateRange_t3049499159, ___U3ClowU3Ek__BackingField_0)); }
	inline String_t* get_U3ClowU3Ek__BackingField_0() const { return ___U3ClowU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3ClowU3Ek__BackingField_0() { return &___U3ClowU3Ek__BackingField_0; }
	inline void set_U3ClowU3Ek__BackingField_0(String_t* value)
	{
		___U3ClowU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClowU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3ChighU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(DateRange_t3049499159, ___U3ChighU3Ek__BackingField_1)); }
	inline String_t* get_U3ChighU3Ek__BackingField_1() const { return ___U3ChighU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3ChighU3Ek__BackingField_1() { return &___U3ChighU3Ek__BackingField_1; }
	inline void set_U3ChighU3Ek__BackingField_1(String_t* value)
	{
		___U3ChighU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3ChighU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATERANGE_T3049499159_H
#ifndef PINGDATA_T2961942429_H
#define PINGDATA_T2961942429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics/PingData
struct  PingData_t2961942429  : public ApplicationData_t1885408412
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PINGDATA_T2961942429_H
#ifndef GETDILEMMAREQUEST_T37205599_H
#define GETDILEMMAREQUEST_T37205599_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics/GetDilemmaRequest
struct  GetDilemmaRequest_t37205599  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics/OnDilemma IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics/GetDilemmaRequest::<Callback>k__BackingField
	OnDilemma_t401820821 * ___U3CCallbackU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetDilemmaRequest_t37205599, ___U3CCallbackU3Ek__BackingField_11)); }
	inline OnDilemma_t401820821 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline OnDilemma_t401820821 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(OnDilemma_t401820821 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETDILEMMAREQUEST_T37205599_H
#ifndef PINGDATAVALUE_T526342856_H
#define PINGDATAVALUE_T526342856_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics/PingDataValue
struct  PingDataValue_t526342856  : public ApplicationDataValue_t1721671971
{
public:
	// System.Double IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics/PingDataValue::<ping>k__BackingField
	double ___U3CpingU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CpingU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(PingDataValue_t526342856, ___U3CpingU3Ek__BackingField_0)); }
	inline double get_U3CpingU3Ek__BackingField_0() const { return ___U3CpingU3Ek__BackingField_0; }
	inline double* get_address_of_U3CpingU3Ek__BackingField_0() { return &___U3CpingU3Ek__BackingField_0; }
	inline void set_U3CpingU3Ek__BackingField_0(double value)
	{
		___U3CpingU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PINGDATAVALUE_T526342856_H
#ifndef CATEGORICALRANGE_T943840333_H
#define CATEGORICALRANGE_T943840333_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.CategoricalRange
struct  CategoricalRange_t943840333  : public Range_t2396059803
{
public:
	// System.String[] IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.CategoricalRange::<keys>k__BackingField
	StringU5BU5D_t1642385972* ___U3CkeysU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CkeysU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CategoricalRange_t943840333, ___U3CkeysU3Ek__BackingField_0)); }
	inline StringU5BU5D_t1642385972* get_U3CkeysU3Ek__BackingField_0() const { return ___U3CkeysU3Ek__BackingField_0; }
	inline StringU5BU5D_t1642385972** get_address_of_U3CkeysU3Ek__BackingField_0() { return &___U3CkeysU3Ek__BackingField_0; }
	inline void set_U3CkeysU3Ek__BackingField_0(StringU5BU5D_t1642385972* value)
	{
		___U3CkeysU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CkeysU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CATEGORICALRANGE_T943840333_H
#ifndef ADDCUSTOMIZATIONWORDREQUEST_T864480132_H
#define ADDCUSTOMIZATIONWORDREQUEST_T864480132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordRequest
struct  AddCustomizationWordRequest_t864480132  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordCallback IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordRequest::<Callback>k__BackingField
	AddCustomizationWordCallback_t3775539180 * ___U3CCallbackU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordRequest::<CustomizationID>k__BackingField
	String_t* ___U3CCustomizationIDU3Ek__BackingField_12;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordRequest::<Word>k__BackingField
	String_t* ___U3CWordU3Ek__BackingField_13;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordRequest::<Translation>k__BackingField
	String_t* ___U3CTranslationU3Ek__BackingField_14;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_15;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(AddCustomizationWordRequest_t864480132, ___U3CCallbackU3Ek__BackingField_11)); }
	inline AddCustomizationWordCallback_t3775539180 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline AddCustomizationWordCallback_t3775539180 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(AddCustomizationWordCallback_t3775539180 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCustomizationIDU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(AddCustomizationWordRequest_t864480132, ___U3CCustomizationIDU3Ek__BackingField_12)); }
	inline String_t* get_U3CCustomizationIDU3Ek__BackingField_12() const { return ___U3CCustomizationIDU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CCustomizationIDU3Ek__BackingField_12() { return &___U3CCustomizationIDU3Ek__BackingField_12; }
	inline void set_U3CCustomizationIDU3Ek__BackingField_12(String_t* value)
	{
		___U3CCustomizationIDU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCustomizationIDU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CWordU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(AddCustomizationWordRequest_t864480132, ___U3CWordU3Ek__BackingField_13)); }
	inline String_t* get_U3CWordU3Ek__BackingField_13() const { return ___U3CWordU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CWordU3Ek__BackingField_13() { return &___U3CWordU3Ek__BackingField_13; }
	inline void set_U3CWordU3Ek__BackingField_13(String_t* value)
	{
		___U3CWordU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CWordU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CTranslationU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(AddCustomizationWordRequest_t864480132, ___U3CTranslationU3Ek__BackingField_14)); }
	inline String_t* get_U3CTranslationU3Ek__BackingField_14() const { return ___U3CTranslationU3Ek__BackingField_14; }
	inline String_t** get_address_of_U3CTranslationU3Ek__BackingField_14() { return &___U3CTranslationU3Ek__BackingField_14; }
	inline void set_U3CTranslationU3Ek__BackingField_14(String_t* value)
	{
		___U3CTranslationU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTranslationU3Ek__BackingField_14), value);
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(AddCustomizationWordRequest_t864480132, ___U3CDataU3Ek__BackingField_15)); }
	inline String_t* get_U3CDataU3Ek__BackingField_15() const { return ___U3CDataU3Ek__BackingField_15; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_15() { return &___U3CDataU3Ek__BackingField_15; }
	inline void set_U3CDataU3Ek__BackingField_15(String_t* value)
	{
		___U3CDataU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDCUSTOMIZATIONWORDREQUEST_T864480132_H
#ifndef GETTONEANALYZERREQUEST_T1769916618_H
#define GETTONEANALYZERREQUEST_T1769916618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzer/GetToneAnalyzerRequest
struct  GetToneAnalyzerRequest_t1769916618  : public Request_t466816980
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzer/GetToneAnalyzerRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_11;
	// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzer/OnGetToneAnalyzed IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzer/GetToneAnalyzerRequest::<Callback>k__BackingField
	OnGetToneAnalyzed_t3813655652 * ___U3CCallbackU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetToneAnalyzerRequest_t1769916618, ___U3CDataU3Ek__BackingField_11)); }
	inline String_t* get_U3CDataU3Ek__BackingField_11() const { return ___U3CDataU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_11() { return &___U3CDataU3Ek__BackingField_11; }
	inline void set_U3CDataU3Ek__BackingField_11(String_t* value)
	{
		___U3CDataU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GetToneAnalyzerRequest_t1769916618, ___U3CCallbackU3Ek__BackingField_12)); }
	inline OnGetToneAnalyzed_t3813655652 * get_U3CCallbackU3Ek__BackingField_12() const { return ___U3CCallbackU3Ek__BackingField_12; }
	inline OnGetToneAnalyzed_t3813655652 ** get_address_of_U3CCallbackU3Ek__BackingField_12() { return &___U3CCallbackU3Ek__BackingField_12; }
	inline void set_U3CCallbackU3Ek__BackingField_12(OnGetToneAnalyzed_t3813655652 * value)
	{
		___U3CCallbackU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETTONEANALYZERREQUEST_T1769916618_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	IntPtr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline IntPtr_t get_Zero_1() const { return ___Zero_1; }
	inline IntPtr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(IntPtr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef DELEGATE_T3022476291_H
#define DELEGATE_T3022476291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t3022476291  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	IntPtr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	IntPtr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	IntPtr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	IntPtr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1572802995 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___invoke_impl_1)); }
	inline IntPtr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline IntPtr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(IntPtr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_3)); }
	inline IntPtr_t get_method_3() const { return ___method_3; }
	inline IntPtr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(IntPtr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___delegate_trampoline_4)); }
	inline IntPtr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline IntPtr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(IntPtr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_code_5)); }
	inline IntPtr_t get_method_code_5() const { return ___method_code_5; }
	inline IntPtr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(IntPtr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___data_8)); }
	inline DelegateData_t1572802995 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1572802995 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1572802995 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T3022476291_H
#ifndef VOICETYPE_T981025524_H
#define VOICETYPE_T981025524_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.VoiceType
struct  VoiceType_t981025524 
{
public:
	// System.Int32 IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.VoiceType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(VoiceType_t981025524, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOICETYPE_T981025524_H
#ifndef GETPRONUNCIATIONREQ_T2533372478_H
#define GETPRONUNCIATIONREQ_T2533372478_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetPronunciationReq
struct  GetPronunciationReq_t2533372478  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetPronunciationCallback IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetPronunciationReq::<Callback>k__BackingField
	GetPronunciationCallback_t2622344657 * ___U3CCallbackU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetPronunciationReq::<Text>k__BackingField
	String_t* ___U3CTextU3Ek__BackingField_12;
	// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.VoiceType IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetPronunciationReq::<Voice>k__BackingField
	int32_t ___U3CVoiceU3Ek__BackingField_13;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetPronunciationReq::<Format>k__BackingField
	String_t* ___U3CFormatU3Ek__BackingField_14;
	// System.String IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetPronunciationReq::<Customization_ID>k__BackingField
	String_t* ___U3CCustomization_IDU3Ek__BackingField_15;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetPronunciationReq_t2533372478, ___U3CCallbackU3Ek__BackingField_11)); }
	inline GetPronunciationCallback_t2622344657 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline GetPronunciationCallback_t2622344657 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(GetPronunciationCallback_t2622344657 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CTextU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GetPronunciationReq_t2533372478, ___U3CTextU3Ek__BackingField_12)); }
	inline String_t* get_U3CTextU3Ek__BackingField_12() const { return ___U3CTextU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CTextU3Ek__BackingField_12() { return &___U3CTextU3Ek__BackingField_12; }
	inline void set_U3CTextU3Ek__BackingField_12(String_t* value)
	{
		___U3CTextU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTextU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CVoiceU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(GetPronunciationReq_t2533372478, ___U3CVoiceU3Ek__BackingField_13)); }
	inline int32_t get_U3CVoiceU3Ek__BackingField_13() const { return ___U3CVoiceU3Ek__BackingField_13; }
	inline int32_t* get_address_of_U3CVoiceU3Ek__BackingField_13() { return &___U3CVoiceU3Ek__BackingField_13; }
	inline void set_U3CVoiceU3Ek__BackingField_13(int32_t value)
	{
		___U3CVoiceU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CFormatU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(GetPronunciationReq_t2533372478, ___U3CFormatU3Ek__BackingField_14)); }
	inline String_t* get_U3CFormatU3Ek__BackingField_14() const { return ___U3CFormatU3Ek__BackingField_14; }
	inline String_t** get_address_of_U3CFormatU3Ek__BackingField_14() { return &___U3CFormatU3Ek__BackingField_14; }
	inline void set_U3CFormatU3Ek__BackingField_14(String_t* value)
	{
		___U3CFormatU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFormatU3Ek__BackingField_14), value);
	}

	inline static int32_t get_offset_of_U3CCustomization_IDU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(GetPronunciationReq_t2533372478, ___U3CCustomization_IDU3Ek__BackingField_15)); }
	inline String_t* get_U3CCustomization_IDU3Ek__BackingField_15() const { return ___U3CCustomization_IDU3Ek__BackingField_15; }
	inline String_t** get_address_of_U3CCustomization_IDU3Ek__BackingField_15() { return &___U3CCustomization_IDU3Ek__BackingField_15; }
	inline void set_U3CCustomization_IDU3Ek__BackingField_15(String_t* value)
	{
		___U3CCustomization_IDU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCustomization_IDU3Ek__BackingField_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETPRONUNCIATIONREQ_T2533372478_H
#ifndef MULTICASTDELEGATE_T3201952435_H
#define MULTICASTDELEGATE_T3201952435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t3201952435  : public Delegate_t3022476291
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t3201952435 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t3201952435 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___prev_9)); }
	inline MulticastDelegate_t3201952435 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t3201952435 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t3201952435 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___kpm_next_10)); }
	inline MulticastDelegate_t3201952435 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t3201952435 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t3201952435 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T3201952435_H
#ifndef ONTRAINCLASSIFIER_T294819893_H
#define ONTRAINCLASSIFIER_T294819893_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/OnTrainClassifier
struct  OnTrainClassifier_t294819893  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONTRAINCLASSIFIER_T294819893_H
#ifndef ONDELETECLASSIFIER_T917631666_H
#define ONDELETECLASSIFIER_T917631666_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/OnDeleteClassifier
struct  OnDeleteClassifier_t917631666  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDELETECLASSIFIER_T917631666_H
#ifndef ONDETECTFACES_T4020255763_H
#define ONDETECTFACES_T4020255763_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/OnDetectFaces
struct  OnDetectFaces_t4020255763  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDETECTFACES_T4020255763_H
#ifndef ONCLASSIFY_T311067478_H
#define ONCLASSIFY_T311067478_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/OnClassify
struct  OnClassify_t311067478  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONCLASSIFY_T311067478_H
#ifndef GETPRONUNCIATIONCALLBACK_T2622344657_H
#define GETPRONUNCIATIONCALLBACK_T2622344657_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetPronunciationCallback
struct  GetPronunciationCallback_t2622344657  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETPRONUNCIATIONCALLBACK_T2622344657_H
#ifndef ONGETCLASSIFIER_T713440359_H
#define ONGETCLASSIFIER_T713440359_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/OnGetClassifier
struct  OnGetClassifier_t713440359  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONGETCLASSIFIER_T713440359_H
#ifndef UPDATECUSTOMIZATIONCALLBACK_T3026233620_H
#define UPDATECUSTOMIZATIONCALLBACK_T3026233620_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/UpdateCustomizationCallback
struct  UpdateCustomizationCallback_t3026233620  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATECUSTOMIZATIONCALLBACK_T3026233620_H
#ifndef GETCUSTOMIZATIONWORDSCALLBACK_T2709615664_H
#define GETCUSTOMIZATIONWORDSCALLBACK_T2709615664_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordsCallback
struct  GetCustomizationWordsCallback_t2709615664  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETCUSTOMIZATIONWORDSCALLBACK_T2709615664_H
#ifndef ADDCUSTOMIZATIONWORDSCALLBACK_T2578578585_H
#define ADDCUSTOMIZATIONWORDSCALLBACK_T2578578585_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordsCallback
struct  AddCustomizationWordsCallback_t2578578585  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDCUSTOMIZATIONWORDSCALLBACK_T2578578585_H
#ifndef GETCUSTOMIZATIONCALLBACK_T4165371535_H
#define GETCUSTOMIZATIONCALLBACK_T4165371535_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationCallback
struct  GetCustomizationCallback_t4165371535  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETCUSTOMIZATIONCALLBACK_T4165371535_H
#ifndef GETCUSTOMIZATIONSCALLBACK_T201646696_H
#define GETCUSTOMIZATIONSCALLBACK_T201646696_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationsCallback
struct  GetCustomizationsCallback_t201646696  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETCUSTOMIZATIONSCALLBACK_T201646696_H
#ifndef CREATECUSTOMIZATIONCALLBACK_T3071584865_H
#define CREATECUSTOMIZATIONCALLBACK_T3071584865_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/CreateCustomizationCallback
struct  CreateCustomizationCallback_t3071584865  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREATECUSTOMIZATIONCALLBACK_T3071584865_H
#ifndef ONDELETECUSTOMIZATIONCALLBACK_T2862971265_H
#define ONDELETECUSTOMIZATIONCALLBACK_T2862971265_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/OnDeleteCustomizationCallback
struct  OnDeleteCustomizationCallback_t2862971265  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDELETECUSTOMIZATIONCALLBACK_T2862971265_H
#ifndef ONDILEMMA_T401820821_H
#define ONDILEMMA_T401820821_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics/OnDilemma
struct  OnDilemma_t401820821  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDILEMMA_T401820821_H
#ifndef ONFINDCLASSIFIER_T2896727766_H
#define ONFINDCLASSIFIER_T2896727766_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/OnFindClassifier
struct  OnFindClassifier_t2896727766  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONFINDCLASSIFIER_T2896727766_H
#ifndef ONGETCLASSIFIERS_T226395526_H
#define ONGETCLASSIFIERS_T226395526_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/OnGetClassifiers
struct  OnGetClassifiers_t226395526  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONGETCLASSIFIERS_T226395526_H
#ifndef ONGETTONEANALYZED_T3813655652_H
#define ONGETTONEANALYZED_T3813655652_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzer/OnGetToneAnalyzed
struct  OnGetToneAnalyzed_t3813655652  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONGETTONEANALYZED_T3813655652_H
#ifndef ONDELETECUSTOMIZATIONWORDCALLBACK_T2544578463_H
#define ONDELETECUSTOMIZATIONWORDCALLBACK_T2544578463_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/OnDeleteCustomizationWordCallback
struct  OnDeleteCustomizationWordCallback_t2544578463  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDELETECUSTOMIZATIONWORDCALLBACK_T2544578463_H
#ifndef GETCUSTOMIZATIONWORDCALLBACK_T2485962781_H
#define GETCUSTOMIZATIONWORDCALLBACK_T2485962781_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/GetCustomizationWordCallback
struct  GetCustomizationWordCallback_t2485962781  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETCUSTOMIZATIONWORDCALLBACK_T2485962781_H
#ifndef ADDCUSTOMIZATIONWORDCALLBACK_T3775539180_H
#define ADDCUSTOMIZATIONWORDCALLBACK_T3775539180_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech/AddCustomizationWordCallback
struct  AddCustomizationWordCallback_t3775539180  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDCUSTOMIZATIONWORDCALLBACK_T3775539180_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2800 = { sizeof (GetPronunciationCallback_t2622344657), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2801 = { sizeof (GetPronunciationReq_t2533372478), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2801[5] = 
{
	GetPronunciationReq_t2533372478::get_offset_of_U3CCallbackU3Ek__BackingField_11(),
	GetPronunciationReq_t2533372478::get_offset_of_U3CTextU3Ek__BackingField_12(),
	GetPronunciationReq_t2533372478::get_offset_of_U3CVoiceU3Ek__BackingField_13(),
	GetPronunciationReq_t2533372478::get_offset_of_U3CFormatU3Ek__BackingField_14(),
	GetPronunciationReq_t2533372478::get_offset_of_U3CCustomization_IDU3Ek__BackingField_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2802 = { sizeof (GetCustomizationsCallback_t201646696), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2803 = { sizeof (GetCustomizationsReq_t2206589443), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2803[2] = 
{
	GetCustomizationsReq_t2206589443::get_offset_of_U3CCallbackU3Ek__BackingField_11(),
	GetCustomizationsReq_t2206589443::get_offset_of_U3CDataU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2804 = { sizeof (CreateCustomizationCallback_t3071584865), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2805 = { sizeof (CreateCustomizationRequest_t295745401), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2805[3] = 
{
	CreateCustomizationRequest_t295745401::get_offset_of_U3CCallbackU3Ek__BackingField_11(),
	CreateCustomizationRequest_t295745401::get_offset_of_U3CCustomVoiceU3Ek__BackingField_12(),
	CreateCustomizationRequest_t295745401::get_offset_of_U3CDataU3Ek__BackingField_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2806 = { sizeof (OnDeleteCustomizationCallback_t2862971265), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2807 = { sizeof (DeleteCustomizationRequest_t4044506578), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2807[3] = 
{
	DeleteCustomizationRequest_t4044506578::get_offset_of_U3CCallbackU3Ek__BackingField_11(),
	DeleteCustomizationRequest_t4044506578::get_offset_of_U3CCustomizationIDU3Ek__BackingField_12(),
	DeleteCustomizationRequest_t4044506578::get_offset_of_U3CDataU3Ek__BackingField_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2808 = { sizeof (GetCustomizationCallback_t4165371535), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2809 = { sizeof (GetCustomizationRequest_t1351603463), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2809[3] = 
{
	GetCustomizationRequest_t1351603463::get_offset_of_U3CCallbackU3Ek__BackingField_11(),
	GetCustomizationRequest_t1351603463::get_offset_of_U3CCustomizationIDU3Ek__BackingField_12(),
	GetCustomizationRequest_t1351603463::get_offset_of_U3CDataU3Ek__BackingField_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2810 = { sizeof (UpdateCustomizationCallback_t3026233620), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2811 = { sizeof (UpdateCustomizationRequest_t1669343124), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2811[4] = 
{
	UpdateCustomizationRequest_t1669343124::get_offset_of_U3CCallbackU3Ek__BackingField_11(),
	UpdateCustomizationRequest_t1669343124::get_offset_of_U3CCustomizationIDU3Ek__BackingField_12(),
	UpdateCustomizationRequest_t1669343124::get_offset_of_U3CCustomVoiceUpdateU3Ek__BackingField_13(),
	UpdateCustomizationRequest_t1669343124::get_offset_of_U3CDataU3Ek__BackingField_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2812 = { sizeof (GetCustomizationWordsCallback_t2709615664), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2813 = { sizeof (GetCustomizationWordsRequest_t110549728), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2813[3] = 
{
	GetCustomizationWordsRequest_t110549728::get_offset_of_U3CCallbackU3Ek__BackingField_11(),
	GetCustomizationWordsRequest_t110549728::get_offset_of_U3CCustomizationIDU3Ek__BackingField_12(),
	GetCustomizationWordsRequest_t110549728::get_offset_of_U3CDataU3Ek__BackingField_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2814 = { sizeof (AddCustomizationWordsCallback_t2578578585), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2815 = { sizeof (AddCustomizationWordsRequest_t993580433), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2815[4] = 
{
	AddCustomizationWordsRequest_t993580433::get_offset_of_U3CCallbackU3Ek__BackingField_11(),
	AddCustomizationWordsRequest_t993580433::get_offset_of_U3CCustomizationIDU3Ek__BackingField_12(),
	AddCustomizationWordsRequest_t993580433::get_offset_of_U3CWordsU3Ek__BackingField_13(),
	AddCustomizationWordsRequest_t993580433::get_offset_of_U3CDataU3Ek__BackingField_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2816 = { sizeof (OnDeleteCustomizationWordCallback_t2544578463), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2817 = { sizeof (DeleteCustomizationWordRequest_t3182066146), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2817[4] = 
{
	DeleteCustomizationWordRequest_t3182066146::get_offset_of_U3CCallbackU3Ek__BackingField_11(),
	DeleteCustomizationWordRequest_t3182066146::get_offset_of_U3CCustomizationIDU3Ek__BackingField_12(),
	DeleteCustomizationWordRequest_t3182066146::get_offset_of_U3CWordU3Ek__BackingField_13(),
	DeleteCustomizationWordRequest_t3182066146::get_offset_of_U3CDataU3Ek__BackingField_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2818 = { sizeof (GetCustomizationWordCallback_t2485962781), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2819 = { sizeof (GetCustomizationWordRequest_t1280810157), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2819[4] = 
{
	GetCustomizationWordRequest_t1280810157::get_offset_of_U3CCallbackU3Ek__BackingField_11(),
	GetCustomizationWordRequest_t1280810157::get_offset_of_U3CCustomizationIDU3Ek__BackingField_12(),
	GetCustomizationWordRequest_t1280810157::get_offset_of_U3CWordU3Ek__BackingField_13(),
	GetCustomizationWordRequest_t1280810157::get_offset_of_U3CDataU3Ek__BackingField_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2820 = { sizeof (AddCustomizationWordCallback_t3775539180), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2821 = { sizeof (AddCustomizationWordRequest_t864480132), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2821[5] = 
{
	AddCustomizationWordRequest_t864480132::get_offset_of_U3CCallbackU3Ek__BackingField_11(),
	AddCustomizationWordRequest_t864480132::get_offset_of_U3CCustomizationIDU3Ek__BackingField_12(),
	AddCustomizationWordRequest_t864480132::get_offset_of_U3CWordU3Ek__BackingField_13(),
	AddCustomizationWordRequest_t864480132::get_offset_of_U3CTranslationU3Ek__BackingField_14(),
	AddCustomizationWordRequest_t864480132::get_offset_of_U3CDataU3Ek__BackingField_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2822 = { sizeof (CheckServiceStatus_t3651453794), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2822[2] = 
{
	CheckServiceStatus_t3651453794::get_offset_of_m_Service_0(),
	CheckServiceStatus_t3651453794::get_offset_of_m_Callback_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2823 = { sizeof (ToneAnalyzerResponse_t3391014235), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2823[2] = 
{
	ToneAnalyzerResponse_t3391014235::get_offset_of_U3Cdocument_toneU3Ek__BackingField_0(),
	ToneAnalyzerResponse_t3391014235::get_offset_of_U3Csentences_toneU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2824 = { sizeof (DocumentTone_t2537937785), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2824[1] = 
{
	DocumentTone_t2537937785::get_offset_of_U3Ctone_categoriesU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2825 = { sizeof (ToneCategory_t3724297374), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2825[3] = 
{
	ToneCategory_t3724297374::get_offset_of_U3Ccategory_idU3Ek__BackingField_0(),
	ToneCategory_t3724297374::get_offset_of_U3Ccategory_nameU3Ek__BackingField_1(),
	ToneCategory_t3724297374::get_offset_of_U3CtonesU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2826 = { sizeof (Tone_t1138009090), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2826[3] = 
{
	Tone_t1138009090::get_offset_of_U3CscoreU3Ek__BackingField_0(),
	Tone_t1138009090::get_offset_of_U3Ctone_idU3Ek__BackingField_1(),
	Tone_t1138009090::get_offset_of_U3Ctone_nameU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2827 = { sizeof (SentenceTone_t3234952115), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2827[8] = 
{
	SentenceTone_t3234952115::get_offset_of_U3Csentence_idU3Ek__BackingField_0(),
	SentenceTone_t3234952115::get_offset_of_U3Cinput_fromU3Ek__BackingField_1(),
	SentenceTone_t3234952115::get_offset_of_U3Cinput_toU3Ek__BackingField_2(),
	SentenceTone_t3234952115::get_offset_of_U3CtextU3Ek__BackingField_3(),
	SentenceTone_t3234952115::get_offset_of_U3Ctone_categoriesU3Ek__BackingField_4(),
	SentenceTone_t3234952115::get_offset_of_m_HighestScore_5(),
	SentenceTone_t3234952115::get_offset_of_m_HighestScoreToneName_6(),
	SentenceTone_t3234952115::get_offset_of_m_HighestScoreToneCategoryName_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2828 = { sizeof (ToneAnalyzer_t1356110496), -1, sizeof(ToneAnalyzer_t1356110496_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2828[3] = 
{
	0,
	ToneAnalyzer_t1356110496_StaticFields::get_offset_of_sm_Serializer_1(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2829 = { sizeof (OnGetToneAnalyzed_t3813655652), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2830 = { sizeof (GetToneAnalyzerRequest_t1769916618), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2830[2] = 
{
	GetToneAnalyzerRequest_t1769916618::get_offset_of_U3CDataU3Ek__BackingField_11(),
	GetToneAnalyzerRequest_t1769916618::get_offset_of_U3CCallbackU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2831 = { sizeof (CheckServiceStatus_t1108037484), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2831[2] = 
{
	CheckServiceStatus_t1108037484::get_offset_of_m_Service_0(),
	CheckServiceStatus_t1108037484::get_offset_of_m_Callback_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2832 = { sizeof (DilemmasResponse_t3767705817), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2832[2] = 
{
	DilemmasResponse_t3767705817::get_offset_of_U3CproblemU3Ek__BackingField_0(),
	DilemmasResponse_t3767705817::get_offset_of_U3CresolutionU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2833 = { sizeof (Problem_t2814813345), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2833[3] = 
{
	Problem_t2814813345::get_offset_of_U3CcolumnsU3Ek__BackingField_0(),
	Problem_t2814813345::get_offset_of_U3CoptionsU3Ek__BackingField_1(),
	Problem_t2814813345::get_offset_of_U3CsubjectU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2834 = { sizeof (Column_t2612486824), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2834[12] = 
{
	Column_t2612486824::get_offset_of_U3CkeyU3Ek__BackingField_0(),
	Column_t2612486824::get_offset_of_U3CtypeU3Ek__BackingField_1(),
	Column_t2612486824::get_offset_of_U3CdescriptionU3Ek__BackingField_2(),
	Column_t2612486824::get_offset_of_U3CformatU3Ek__BackingField_3(),
	Column_t2612486824::get_offset_of_U3Cfull_nameU3Ek__BackingField_4(),
	Column_t2612486824::get_offset_of_U3CgoalU3Ek__BackingField_5(),
	Column_t2612486824::get_offset_of_U3Cinsignificant_lossU3Ek__BackingField_6(),
	Column_t2612486824::get_offset_of_U3Cis_objectiveU3Ek__BackingField_7(),
	Column_t2612486824::get_offset_of_U3CpreferenceU3Ek__BackingField_8(),
	Column_t2612486824::get_offset_of_U3CrangeU3Ek__BackingField_9(),
	Column_t2612486824::get_offset_of_U3Csignificant_gainU3Ek__BackingField_10(),
	Column_t2612486824::get_offset_of_U3Csignificant_lossU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2835 = { sizeof (Range_t2396059803), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2836 = { sizeof (CategoricalRange_t943840333), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2836[1] = 
{
	CategoricalRange_t943840333::get_offset_of_U3CkeysU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2837 = { sizeof (DateRange_t3049499159), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2837[2] = 
{
	DateRange_t3049499159::get_offset_of_U3ClowU3Ek__BackingField_0(),
	DateRange_t3049499159::get_offset_of_U3ChighU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2838 = { sizeof (ValueRange_t543481982), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2838[2] = 
{
	ValueRange_t543481982::get_offset_of_U3ClowU3Ek__BackingField_0(),
	ValueRange_t543481982::get_offset_of_U3ChighU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2839 = { sizeof (Option_t1775127045), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2839[5] = 
{
	Option_t1775127045::get_offset_of_U3Capp_dataU3Ek__BackingField_0(),
	Option_t1775127045::get_offset_of_U3Cdescription_htmlU3Ek__BackingField_1(),
	Option_t1775127045::get_offset_of_U3CkeyU3Ek__BackingField_2(),
	Option_t1775127045::get_offset_of_U3CnameU3Ek__BackingField_3(),
	Option_t1775127045::get_offset_of_U3CvaluesU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2840 = { sizeof (ApplicationData_t1885408412), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2841 = { sizeof (ApplicationDataValue_t1721671971), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2842 = { sizeof (Resolution_t3867497320), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2842[2] = 
{
	Resolution_t3867497320::get_offset_of_U3CmapU3Ek__BackingField_0(),
	Resolution_t3867497320::get_offset_of_U3CsolutionsU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2843 = { sizeof (Map_t1914475838), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2843[4] = 
{
	Map_t1914475838::get_offset_of_U3CanchorsU3Ek__BackingField_0(),
	Map_t1914475838::get_offset_of_U3CcommentsU3Ek__BackingField_1(),
	Map_t1914475838::get_offset_of_U3CconfigU3Ek__BackingField_2(),
	Map_t1914475838::get_offset_of_U3CnodesU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2844 = { sizeof (Anchor_t1470404909), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2844[2] = 
{
	Anchor_t1470404909::get_offset_of_U3CnameU3Ek__BackingField_0(),
	Anchor_t1470404909::get_offset_of_U3CpositionU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2845 = { sizeof (Position_t3597491459), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2845[2] = 
{
	Position_t3597491459::get_offset_of_U3CxU3Ek__BackingField_0(),
	Position_t3597491459::get_offset_of_U3CyU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2846 = { sizeof (Config_t1927135816), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2846[2] = 
{
	Config_t1927135816::get_offset_of_U3CdriversU3Ek__BackingField_0(),
	Config_t1927135816::get_offset_of_U3CparamsU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2847 = { sizeof (Drivers_t1652706281), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2847[8] = 
{
	Drivers_t1652706281::get_offset_of_U3Calpha_initU3Ek__BackingField_0(),
	Drivers_t1652706281::get_offset_of_U3Cdata_multiplierU3Ek__BackingField_1(),
	Drivers_t1652706281::get_offset_of_U3Cmax_map_sizeU3Ek__BackingField_2(),
	Drivers_t1652706281::get_offset_of_U3Cr_anchor_initU3Ek__BackingField_3(),
	Drivers_t1652706281::get_offset_of_U3Cr_finU3Ek__BackingField_4(),
	Drivers_t1652706281::get_offset_of_U3Cr_initU3Ek__BackingField_5(),
	Drivers_t1652706281::get_offset_of_U3Ctraining_anchorsU3Ek__BackingField_6(),
	Drivers_t1652706281::get_offset_of_U3Ctraining_lengthU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2848 = { sizeof (Params_t3159428742), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2848[8] = 
{
	Params_t3159428742::get_offset_of_U3Calpha_initU3Ek__BackingField_0(),
	Params_t3159428742::get_offset_of_U3Canchor_epochU3Ek__BackingField_1(),
	Params_t3159428742::get_offset_of_U3Cmap_sizeU3Ek__BackingField_2(),
	Params_t3159428742::get_offset_of_U3CrAnchorU3Ek__BackingField_3(),
	Params_t3159428742::get_offset_of_U3CrFinishU3Ek__BackingField_4(),
	Params_t3159428742::get_offset_of_U3CrInitU3Ek__BackingField_5(),
	Params_t3159428742::get_offset_of_U3CseedU3Ek__BackingField_6(),
	Params_t3159428742::get_offset_of_U3Ctraining_periodU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2849 = { sizeof (Metrics_t236865991), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2849[2] = 
{
	Metrics_t236865991::get_offset_of_U3Cfinal_kappaU3Ek__BackingField_0(),
	Metrics_t236865991::get_offset_of_U3CkappaU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2850 = { sizeof (Node_t3986951550), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2850[2] = 
{
	Node_t3986951550::get_offset_of_U3CcoordinatesU3Ek__BackingField_0(),
	Node_t3986951550::get_offset_of_U3Csolution_refsU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2851 = { sizeof (Solution_t204511443), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2851[5] = 
{
	Solution_t204511443::get_offset_of_U3Cshadow_meU3Ek__BackingField_0(),
	Solution_t204511443::get_offset_of_U3CshadowsU3Ek__BackingField_1(),
	Solution_t204511443::get_offset_of_U3Csolution_refU3Ek__BackingField_2(),
	Solution_t204511443::get_offset_of_U3CstatusU3Ek__BackingField_3(),
	Solution_t204511443::get_offset_of_U3Cstatus_causeU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2852 = { sizeof (StatusCause_t3777693303), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2852[3] = 
{
	StatusCause_t3777693303::get_offset_of_U3Cerror_codeU3Ek__BackingField_0(),
	StatusCause_t3777693303::get_offset_of_U3CmessageU3Ek__BackingField_1(),
	StatusCause_t3777693303::get_offset_of_U3CtokensU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2853 = { sizeof (TradeoffAnalytics_t100002595), -1, sizeof(TradeoffAnalytics_t100002595_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2853[3] = 
{
	0,
	TradeoffAnalytics_t100002595_StaticFields::get_offset_of_sm_Serializer_1(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2854 = { sizeof (OnDilemma_t401820821), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2855 = { sizeof (GetDilemmaRequest_t37205599), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2855[1] = 
{
	GetDilemmaRequest_t37205599::get_offset_of_U3CCallbackU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2856 = { sizeof (PingDataValue_t526342856), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2856[1] = 
{
	PingDataValue_t526342856::get_offset_of_U3CpingU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2857 = { sizeof (PingData_t2961942429), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2858 = { sizeof (CheckServiceStatus_t584740610), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2858[2] = 
{
	CheckServiceStatus_t584740610::get_offset_of_m_Service_0(),
	CheckServiceStatus_t584740610::get_offset_of_m_Callback_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2859 = { sizeof (ClassifyTopLevelMultiple_t317282463), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2859[3] = 
{
	ClassifyTopLevelMultiple_t317282463::get_offset_of_U3Cimages_processedU3Ek__BackingField_0(),
	ClassifyTopLevelMultiple_t317282463::get_offset_of_U3CimagesU3Ek__BackingField_1(),
	ClassifyTopLevelMultiple_t317282463::get_offset_of_U3CwarningsU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2860 = { sizeof (ClassifyTopLevelSingle_t3733844951), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2860[5] = 
{
	ClassifyTopLevelSingle_t3733844951::get_offset_of_U3Csource_urlU3Ek__BackingField_0(),
	ClassifyTopLevelSingle_t3733844951::get_offset_of_U3Cresolved_urlU3Ek__BackingField_1(),
	ClassifyTopLevelSingle_t3733844951::get_offset_of_U3CimageU3Ek__BackingField_2(),
	ClassifyTopLevelSingle_t3733844951::get_offset_of_U3CerrorU3Ek__BackingField_3(),
	ClassifyTopLevelSingle_t3733844951::get_offset_of_U3CclassifiersU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2861 = { sizeof (ClassifyPerClassifier_t2357796318), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2861[3] = 
{
	ClassifyPerClassifier_t2357796318::get_offset_of_U3CnameU3Ek__BackingField_0(),
	ClassifyPerClassifier_t2357796318::get_offset_of_U3Cclassifier_idU3Ek__BackingField_1(),
	ClassifyPerClassifier_t2357796318::get_offset_of_U3CclassesU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2862 = { sizeof (ClassResult_t3791036973), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2862[3] = 
{
	ClassResult_t3791036973::get_offset_of_U3Cm_classU3Ek__BackingField_0(),
	ClassResult_t3791036973::get_offset_of_U3CscoreU3Ek__BackingField_1(),
	ClassResult_t3791036973::get_offset_of_U3Ctype_hierarchyU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2863 = { sizeof (ClassifyParameters_t933043320), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2863[4] = 
{
	ClassifyParameters_t933043320::get_offset_of_U3CurlU3Ek__BackingField_0(),
	ClassifyParameters_t933043320::get_offset_of_U3Cclassifier_idsU3Ek__BackingField_1(),
	ClassifyParameters_t933043320::get_offset_of_U3CownersU3Ek__BackingField_2(),
	ClassifyParameters_t933043320::get_offset_of_U3CthresholdU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2864 = { sizeof (FacesTopLevelMultiple_t351289709), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2864[3] = 
{
	FacesTopLevelMultiple_t351289709::get_offset_of_U3Cimages_processedU3Ek__BackingField_0(),
	FacesTopLevelMultiple_t351289709::get_offset_of_U3CimagesU3Ek__BackingField_1(),
	FacesTopLevelMultiple_t351289709::get_offset_of_U3CwarningsU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2865 = { sizeof (FacesTopLevelSingle_t2140351533), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2865[5] = 
{
	FacesTopLevelSingle_t2140351533::get_offset_of_U3Csource_urlU3Ek__BackingField_0(),
	FacesTopLevelSingle_t2140351533::get_offset_of_U3Cresolved_urlU3Ek__BackingField_1(),
	FacesTopLevelSingle_t2140351533::get_offset_of_U3CimageU3Ek__BackingField_2(),
	FacesTopLevelSingle_t2140351533::get_offset_of_U3CerrorU3Ek__BackingField_3(),
	FacesTopLevelSingle_t2140351533::get_offset_of_U3CfacesU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2866 = { sizeof (OneFaceResult_t2674708326), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2866[4] = 
{
	OneFaceResult_t2674708326::get_offset_of_U3CageU3Ek__BackingField_0(),
	OneFaceResult_t2674708326::get_offset_of_U3CgenderU3Ek__BackingField_1(),
	OneFaceResult_t2674708326::get_offset_of_U3Cface_locationU3Ek__BackingField_2(),
	OneFaceResult_t2674708326::get_offset_of_U3CidentityU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2867 = { sizeof (DetectFacesParameters_t3278171735), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2867[1] = 
{
	DetectFacesParameters_t3278171735::get_offset_of_U3CurlU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2868 = { sizeof (TextRecogTopLevelMultiple_t1284479682), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2868[3] = 
{
	TextRecogTopLevelMultiple_t1284479682::get_offset_of_U3Cimages_processedU3Ek__BackingField_0(),
	TextRecogTopLevelMultiple_t1284479682::get_offset_of_U3CimagesU3Ek__BackingField_1(),
	TextRecogTopLevelMultiple_t1284479682::get_offset_of_U3CwarningsU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2869 = { sizeof (TextRecogTopLevelSingle_t1839648508), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2869[6] = 
{
	TextRecogTopLevelSingle_t1839648508::get_offset_of_U3Csource_urlU3Ek__BackingField_0(),
	TextRecogTopLevelSingle_t1839648508::get_offset_of_U3Cresolved_urlU3Ek__BackingField_1(),
	TextRecogTopLevelSingle_t1839648508::get_offset_of_U3CimageU3Ek__BackingField_2(),
	TextRecogTopLevelSingle_t1839648508::get_offset_of_U3CerrorU3Ek__BackingField_3(),
	TextRecogTopLevelSingle_t1839648508::get_offset_of_U3CtextU3Ek__BackingField_4(),
	TextRecogTopLevelSingle_t1839648508::get_offset_of_U3CwordsU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2870 = { sizeof (TextRecogOneWord_t2687074657), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2870[4] = 
{
	TextRecogOneWord_t2687074657::get_offset_of_U3CwordU3Ek__BackingField_0(),
	TextRecogOneWord_t2687074657::get_offset_of_U3ClocationU3Ek__BackingField_1(),
	TextRecogOneWord_t2687074657::get_offset_of_U3CscoreU3Ek__BackingField_2(),
	TextRecogOneWord_t2687074657::get_offset_of_U3Cline_numberU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2871 = { sizeof (Location_t328870245), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2871[4] = 
{
	Location_t328870245::get_offset_of_U3CwidthU3Ek__BackingField_0(),
	Location_t328870245::get_offset_of_U3CheightU3Ek__BackingField_1(),
	Location_t328870245::get_offset_of_U3CleftU3Ek__BackingField_2(),
	Location_t328870245::get_offset_of_U3CtopU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2872 = { sizeof (RecognizeTextParameters_t1331035907), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2872[1] = 
{
	RecognizeTextParameters_t1331035907::get_offset_of_U3CurlU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2873 = { sizeof (GetClassifiersTopLevelBrief_t3177285335), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2873[1] = 
{
	GetClassifiersTopLevelBrief_t3177285335::get_offset_of_U3CclassifiersU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2874 = { sizeof (GetClassifiersPerClassifierBrief_t3946924106), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2874[2] = 
{
	GetClassifiersPerClassifierBrief_t3946924106::get_offset_of_U3Cclassifier_idU3Ek__BackingField_0(),
	GetClassifiersPerClassifierBrief_t3946924106::get_offset_of_U3CnameU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2875 = { sizeof (GetClassifiersPerClassifierVerbose_t3597794938), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2875[7] = 
{
	GetClassifiersPerClassifierVerbose_t3597794938::get_offset_of_U3Cclassifier_idU3Ek__BackingField_0(),
	GetClassifiersPerClassifierVerbose_t3597794938::get_offset_of_U3CnameU3Ek__BackingField_1(),
	GetClassifiersPerClassifierVerbose_t3597794938::get_offset_of_U3CownerU3Ek__BackingField_2(),
	GetClassifiersPerClassifierVerbose_t3597794938::get_offset_of_U3CstatusU3Ek__BackingField_3(),
	GetClassifiersPerClassifierVerbose_t3597794938::get_offset_of_U3CexplanationU3Ek__BackingField_4(),
	GetClassifiersPerClassifierVerbose_t3597794938::get_offset_of_U3CcreatedU3Ek__BackingField_5(),
	GetClassifiersPerClassifierVerbose_t3597794938::get_offset_of_U3CclassesU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2876 = { sizeof (Class_t4054986160), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2876[1] = 
{
	Class_t4054986160::get_offset_of_U3Cm_ClassU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2877 = { sizeof (GetCollections_t2586322795), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2877[1] = 
{
	GetCollections_t2586322795::get_offset_of_collections_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2878 = { sizeof (CreateCollection_t3312638156), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2878[6] = 
{
	CreateCollection_t3312638156::get_offset_of_U3Ccollection_idU3Ek__BackingField_0(),
	CreateCollection_t3312638156::get_offset_of_U3CnameU3Ek__BackingField_1(),
	CreateCollection_t3312638156::get_offset_of_U3CcreatedU3Ek__BackingField_2(),
	CreateCollection_t3312638156::get_offset_of_U3CimagesU3Ek__BackingField_3(),
	CreateCollection_t3312638156::get_offset_of_U3CstatusU3Ek__BackingField_4(),
	CreateCollection_t3312638156::get_offset_of_U3CcapacityU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2879 = { sizeof (GetCollectionImages_t1669456110), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2879[1] = 
{
	GetCollectionImages_t1669456110::get_offset_of_U3CimagesU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2880 = { sizeof (GetCollectionsBrief_t2166116487), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2880[4] = 
{
	GetCollectionsBrief_t2166116487::get_offset_of_U3Cimage_idU3Ek__BackingField_0(),
	GetCollectionsBrief_t2166116487::get_offset_of_U3CcreatedU3Ek__BackingField_1(),
	GetCollectionsBrief_t2166116487::get_offset_of_U3Cimage_fileU3Ek__BackingField_2(),
	GetCollectionsBrief_t2166116487::get_offset_of_U3CmetadataU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2881 = { sizeof (CollectionsConfig_t2493163663), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2881[2] = 
{
	CollectionsConfig_t2493163663::get_offset_of_U3CimagesU3Ek__BackingField_0(),
	CollectionsConfig_t2493163663::get_offset_of_U3Cimages_processedU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2882 = { sizeof (CollectionImagesConfig_t1273369062), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2882[4] = 
{
	CollectionImagesConfig_t1273369062::get_offset_of_U3Cimage_idU3Ek__BackingField_0(),
	CollectionImagesConfig_t1273369062::get_offset_of_U3CcreatedU3Ek__BackingField_1(),
	CollectionImagesConfig_t1273369062::get_offset_of_U3Cimage_fileU3Ek__BackingField_2(),
	CollectionImagesConfig_t1273369062::get_offset_of_U3CmetadataU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2883 = { sizeof (SimilarImagesConfig_t2576720653), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2883[2] = 
{
	SimilarImagesConfig_t2576720653::get_offset_of_U3Csimilar_imagesU3Ek__BackingField_0(),
	SimilarImagesConfig_t2576720653::get_offset_of_U3Cimages_processedU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2884 = { sizeof (SimilarImageConfig_t3734626070), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2884[5] = 
{
	SimilarImageConfig_t3734626070::get_offset_of_U3Cimage_idU3Ek__BackingField_0(),
	SimilarImageConfig_t3734626070::get_offset_of_U3CcreatedU3Ek__BackingField_1(),
	SimilarImageConfig_t3734626070::get_offset_of_U3Cimage_fileU3Ek__BackingField_2(),
	SimilarImageConfig_t3734626070::get_offset_of_U3CmetadataU3Ek__BackingField_3(),
	SimilarImageConfig_t3734626070::get_offset_of_U3CscoreU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2885 = { sizeof (WarningInfo_t624028094), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2885[2] = 
{
	WarningInfo_t624028094::get_offset_of_U3Cwarning_idU3Ek__BackingField_0(),
	WarningInfo_t624028094::get_offset_of_U3CdescriptionU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2886 = { sizeof (ErrorInfoNoCode_t2062371116), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2886[2] = 
{
	ErrorInfoNoCode_t2062371116::get_offset_of_U3Cerror_idU3Ek__BackingField_0(),
	ErrorInfoNoCode_t2062371116::get_offset_of_U3CdescriptionU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2887 = { sizeof (Age_t582693723), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2887[3] = 
{
	Age_t582693723::get_offset_of_U3CminU3Ek__BackingField_0(),
	Age_t582693723::get_offset_of_U3CmaxU3Ek__BackingField_1(),
	Age_t582693723::get_offset_of_U3CscoreU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2888 = { sizeof (Gender_t3451852799), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2888[2] = 
{
	Gender_t3451852799::get_offset_of_U3CgenderU3Ek__BackingField_0(),
	Gender_t3451852799::get_offset_of_U3CscoreU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2889 = { sizeof (FaceLocation_t2324903414), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2889[4] = 
{
	FaceLocation_t2324903414::get_offset_of_U3CwidthU3Ek__BackingField_0(),
	FaceLocation_t2324903414::get_offset_of_U3CheightU3Ek__BackingField_1(),
	FaceLocation_t2324903414::get_offset_of_U3CleftU3Ek__BackingField_2(),
	FaceLocation_t2324903414::get_offset_of_U3CtopU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2890 = { sizeof (Identity_t2022392156), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2890[3] = 
{
	Identity_t2022392156::get_offset_of_U3CnameU3Ek__BackingField_0(),
	Identity_t2022392156::get_offset_of_U3CscoreU3Ek__BackingField_1(),
	Identity_t2022392156::get_offset_of_U3Ctype_hierarchyU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2891 = { sizeof (VisualRecognitionVersion_t883645903), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2891[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2892 = { sizeof (VisualRecognition_t3119563755), -1, sizeof(VisualRecognition_t3119563755_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2892[15] = 
{
	VisualRecognition_t3119563755::get_offset_of_U3CLoadFileU3Ek__BackingField_0(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	VisualRecognition_t3119563755_StaticFields::get_offset_of_mp_ApiKey_12(),
	VisualRecognition_t3119563755_StaticFields::get_offset_of_sm_Serializer_13(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2893 = { sizeof (OnFindClassifier_t2896727766), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2894 = { sizeof (OnGetClassifiers_t226395526), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2895 = { sizeof (OnGetClassifier_t713440359), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2896 = { sizeof (OnDeleteClassifier_t917631666), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2897 = { sizeof (OnTrainClassifier_t294819893), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2898 = { sizeof (OnClassify_t311067478), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2899 = { sizeof (OnDetectFaces_t4020255763), sizeof(Il2CppMethodPointer), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
