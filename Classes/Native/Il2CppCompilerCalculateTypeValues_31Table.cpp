﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// TouchScript.Gestures.LongPressGesture
struct LongPressGesture_t656184630;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;
// System.Collections.Generic.Dictionary`2<System.Type,FullSerializer.Internal.fsOption`1<FullSerializer.Internal.fsVersionedType>>
struct Dictionary_2_t2909366393;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Type
struct Type_t;
// System.String
struct String_t;
// System.IO.StringReader
struct StringReader_t1480123486;
// System.IntPtr[]
struct IntPtrU5BU5D_t169632028;
// System.Collections.IDictionary
struct IDictionary_t596158605;
// FullSerializer.fsSerializer
struct fsSerializer_t4193731081;
// System.Func`2<FullSerializer.fsDataType,System.String>
struct Func_2_t1180654441;
// TouchScript.Gestures.TapGesture
struct TapGesture_t556401502;
// System.Func`4<UnityEngine.Vector2,TouchScript.Tags,System.Boolean,TouchScript.TouchPoint>
struct Func_4_t1475708928;
// System.Action`2<System.Int32,UnityEngine.Vector2>
struct Action_2_t1542075644;
// System.Action`1<System.Int32>
struct Action_1_t1873676830;
// TouchScript.Tags
struct Tags_t1265380163;
// System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState>
struct Dictionary_2_t1739907934;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;
// System.Collections.Generic.List`1<TouchScript.TouchPoint>
struct List_1_t328750215;
// System.Reflection.MethodInfo[]
struct MethodInfoU5BU5D_t152480188;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Collections.Generic.HashSet`1<System.String>
struct HashSet_1_t362681087;
// System.Collections.Generic.Dictionary`2<System.Type,FullSerializer.fsBaseConverter>
struct Dictionary_2_t3179035323;
// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.List`1<FullSerializer.fsObjectProcessor>>
struct Dictionary_2_t3992733373;
// System.Collections.Generic.List`1<FullSerializer.fsConverter>
struct List_1_t4130846565;
// System.Collections.Generic.Dictionary`2<System.Type,FullSerializer.fsDirectConverter>
struct Dictionary_2_t2700818715;
// System.Collections.Generic.List`1<FullSerializer.fsObjectProcessor>
struct List_1_t2055375476;
// FullSerializer.Internal.fsCyclicReferenceManager
struct fsCyclicReferenceManager_t1995018378;
// FullSerializer.fsSerializer/fsLazyCycleDefinitionWriter
struct fsLazyCycleDefinitionWriter_t2327014926;
// FullSerializer.fsContext
struct fsContext_t2896355488;
// System.Collections.Generic.Dictionary`2<System.Int32,FullSerializer.fsData>
struct Dictionary_2_t1591631240;
// System.Collections.Generic.HashSet`1<System.Int32>
struct HashSet_1_t405338302;
// System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
struct Dictionary_2_t1663937576;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Object>
struct Dictionary_2_t1697274930;
// System.Reflection.FieldInfo
struct FieldInfo_t;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t1902082073;
// System.Void
struct Void_t1841601450;
// System.Char[]
struct CharU5BU5D_t1328083999;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1599784723;
// TouchScript.TouchPoint
struct TouchPoint_t959629083;
// FullSerializer.Internal.fsVersionedType[]
struct fsVersionedTypeU5BU5D_t1849274963;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// UnityEngine.Collider2D
struct Collider2D_t646061738;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.EventSystems.BaseRaycaster
struct BaseRaycaster_t2336171397;
// UnityEngine.Collider
struct Collider_t3497673348;
// System.Collections.Generic.Dictionary`2<System.Type,FullSerializer.fsMetaType>
struct Dictionary_2_t909189527;
// FullSerializer.Internal.fsMetaProperty[]
struct fsMetaPropertyU5BU5D_t4057973332;
// UnityEngine.Transform
struct Transform_t3275118058;
// System.Type[]
struct TypeU5BU5D_t1664964607;
// System.Collections.Generic.IDictionary`2<FullSerializer.Internal.fsPortableReflection/AttributeQuery,System.Attribute>
struct IDictionary_2_t2678704838;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// TouchScript.Behaviors.Visualizer.TouchProxyBase
struct TouchProxyBase_t4188753234;
// TouchScript.Utils.ObjectPool`1<TouchScript.Behaviors.Visualizer.TouchProxyBase>
struct ObjectPool_1_t3542042081;
// System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Behaviors.Visualizer.TouchProxyBase>
struct Dictionary_2_t3196578869;
// TouchScript.IGestureDelegate
struct IGestureDelegate_t4252506175;
// System.Collections.Generic.List`1<TouchScript.Gestures.Gesture>
struct List_1_t1721427117;
// System.Action`2<TouchScript.Gestures.Gesture,System.Collections.Generic.IList`1<TouchScript.TouchPoint>>
struct Action_2_t2136039064;
// System.Action`1<UnityEngine.Transform>
struct Action_1_t3076917440;
// System.Collections.Generic.Dictionary`2<UnityEngine.Transform,System.Collections.Generic.List`1<TouchScript.TouchPoint>>
struct Dictionary_2_t70405120;
// System.Collections.Generic.Dictionary`2<TouchScript.Gestures.Gesture,System.Collections.Generic.List`1<TouchScript.TouchPoint>>
struct Dictionary_2_t673677165;
// TouchScript.Utils.ObjectPool`1<System.Collections.Generic.List`1<TouchScript.Gestures.Gesture>>
struct ObjectPool_1_t1074715964;
// TouchScript.Utils.ObjectPool`1<System.Collections.Generic.List`1<TouchScript.TouchPoint>>
struct ObjectPool_1_t3977006358;
// TouchScript.Utils.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Transform>>
struct ObjectPool_1_t1997528037;
// TouchScript.InputSources.ICoordinatesRemapper
struct ICoordinatesRemapper_t2162613424;
// TouchScript.TouchManagerInstance
struct TouchManagerInstance_t2629118981;
// System.Collections.Generic.List`1<TouchScript.Gestures.ITransformGesture>
struct List_1_t909705622;
// System.Collections.Generic.List`1<TouchScript.InputSources.TuioObjectMapping>
struct List_1_t3450048260;
// TUIOsharp.TuioServer
struct TuioServer_t595520884;
// TUIOsharp.DataProcessors.CursorProcessor
struct CursorProcessor_t1785954004;
// TUIOsharp.DataProcessors.ObjectProcessor
struct ObjectProcessor_t221569383;
// TUIOsharp.DataProcessors.BlobProcessor
struct BlobProcessor_t3603341577;
// System.Collections.Generic.Dictionary`2<TUIOsharp.Entities.TuioCursor,TouchScript.TouchPoint>
struct Dictionary_2_t2117679327;
// System.Collections.Generic.Dictionary`2<TUIOsharp.Entities.TuioBlob,TouchScript.TouchPoint>
struct Dictionary_2_t855216920;
// System.Collections.Generic.Dictionary`2<TUIOsharp.Entities.TuioObject,TouchScript.TouchPoint>
struct Dictionary_2_t788444728;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_t3685274804;
// UnityEngine.EventSystems.AxisEventData
struct AxisEventData_t1524870173;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t3466835263;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t2681005625;
// UnityEngine.EventSystems.BaseInput
struct BaseInput_t621514313;
// UnityEngine.UI.Text
struct Text_t356221433;
// TouchScript.InputSources.InputHandlers.TouchHandler
struct TouchHandler_t2521645213;
// System.EventHandler`1<TouchScript.Gestures.GestureStateChangeEventArgs>
struct EventHandler_1_t2091288363;
// System.EventHandler`1<System.EventArgs>
struct EventHandler_1_t1880931879;
// TouchScript.ITouchManager
struct ITouchManager_t2552034033;
// TouchScript.Layers.TouchLayer
struct TouchLayer_t2635439978;
// System.Collections.ObjectModel.ReadOnlyCollection`1<TouchScript.TouchPoint>
struct ReadOnlyCollection_1_t1145414775;
// TouchScript.Utils.TimedSequence`1<TouchScript.TouchPoint>
struct TimedSequence_1_t2737250283;
// TouchScript.GestureManagerInstance
struct GestureManagerInstance_t505647059;
// TouchScript.Utils.TimedSequence`1<UnityEngine.Vector2>
struct TimedSequence_1_t4021328779;
// System.EventHandler`1<TouchScript.Gestures.MetaGestureEventArgs>
struct EventHandler_1_t3142866083;
// System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData>
struct Dictionary_2_t2888425610;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.EventSystems.PointerEventData>
struct Dictionary_2_t607610358;
// TouchScript.Clusters.Clusters
struct Clusters_t4089973855;

struct fsVersionedType_t654750358_marshaled_pinvoke;
struct fsVersionedType_t654750358_marshaled_com;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CWAITU3EC__ITERATOR0_T3884913881_H
#define U3CWAITU3EC__ITERATOR0_T3884913881_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Gestures.LongPressGesture/<wait>c__Iterator0
struct  U3CwaitU3Ec__Iterator0_t3884913881  : public RuntimeObject
{
public:
	// System.Single TouchScript.Gestures.LongPressGesture/<wait>c__Iterator0::<targetTime>__0
	float ___U3CtargetTimeU3E__0_0;
	// TouchScript.Gestures.LongPressGesture TouchScript.Gestures.LongPressGesture/<wait>c__Iterator0::$this
	LongPressGesture_t656184630 * ___U24this_1;
	// System.Object TouchScript.Gestures.LongPressGesture/<wait>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean TouchScript.Gestures.LongPressGesture/<wait>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 TouchScript.Gestures.LongPressGesture/<wait>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CtargetTimeU3E__0_0() { return static_cast<int32_t>(offsetof(U3CwaitU3Ec__Iterator0_t3884913881, ___U3CtargetTimeU3E__0_0)); }
	inline float get_U3CtargetTimeU3E__0_0() const { return ___U3CtargetTimeU3E__0_0; }
	inline float* get_address_of_U3CtargetTimeU3E__0_0() { return &___U3CtargetTimeU3E__0_0; }
	inline void set_U3CtargetTimeU3E__0_0(float value)
	{
		___U3CtargetTimeU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CwaitU3Ec__Iterator0_t3884913881, ___U24this_1)); }
	inline LongPressGesture_t656184630 * get_U24this_1() const { return ___U24this_1; }
	inline LongPressGesture_t656184630 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(LongPressGesture_t656184630 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CwaitU3Ec__Iterator0_t3884913881, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CwaitU3Ec__Iterator0_t3884913881, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CwaitU3Ec__Iterator0_t3884913881, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITU3EC__ITERATOR0_T3884913881_H
#ifndef U3CCANSERIALIZEPROPERTYU3EC__ANONSTOREY1_T3778365949_H
#define U3CCANSERIALIZEPROPERTYU3EC__ANONSTOREY1_T3778365949_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsMetaType/<CanSerializeProperty>c__AnonStorey1
struct  U3CCanSerializePropertyU3Ec__AnonStorey1_t3778365949  : public RuntimeObject
{
public:
	// System.Reflection.PropertyInfo FullSerializer.fsMetaType/<CanSerializeProperty>c__AnonStorey1::property
	PropertyInfo_t * ___property_0;

public:
	inline static int32_t get_offset_of_property_0() { return static_cast<int32_t>(offsetof(U3CCanSerializePropertyU3Ec__AnonStorey1_t3778365949, ___property_0)); }
	inline PropertyInfo_t * get_property_0() const { return ___property_0; }
	inline PropertyInfo_t ** get_address_of_property_0() { return &___property_0; }
	inline void set_property_0(PropertyInfo_t * value)
	{
		___property_0 = value;
		Il2CppCodeGenWriteBarrier((&___property_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCANSERIALIZEPROPERTYU3EC__ANONSTOREY1_T3778365949_H
#ifndef FSREFLECTIONUTILITY_T889872004_H
#define FSREFLECTIONUTILITY_T889872004_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.Internal.fsReflectionUtility
struct  fsReflectionUtility_t889872004  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSREFLECTIONUTILITY_T889872004_H
#ifndef FSTYPELOOKUP_T2250544765_H
#define FSTYPELOOKUP_T2250544765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.Internal.fsTypeLookup
struct  fsTypeLookup_t2250544765  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSTYPELOOKUP_T2250544765_H
#ifndef JSON_T4020371770_H
#define JSON_T4020371770_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiniJSON.Json
struct  Json_t4020371770  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSON_T4020371770_H
#ifndef U3CCSHARPNAMEU3EC__ANONSTOREY0_T3839091716_H
#define U3CCSHARPNAMEU3EC__ANONSTOREY0_T3839091716_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsTypeExtensions/<CSharpName>c__AnonStorey0
struct  U3CCSharpNameU3Ec__AnonStorey0_t3839091716  : public RuntimeObject
{
public:
	// System.Boolean FullSerializer.fsTypeExtensions/<CSharpName>c__AnonStorey0::includeNamespace
	bool ___includeNamespace_0;

public:
	inline static int32_t get_offset_of_includeNamespace_0() { return static_cast<int32_t>(offsetof(U3CCSharpNameU3Ec__AnonStorey0_t3839091716, ___includeNamespace_0)); }
	inline bool get_includeNamespace_0() const { return ___includeNamespace_0; }
	inline bool* get_address_of_includeNamespace_0() { return &___includeNamespace_0; }
	inline void set_includeNamespace_0(bool value)
	{
		___includeNamespace_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCSHARPNAMEU3EC__ANONSTOREY0_T3839091716_H
#ifndef FSTYPEEXTENSIONS_T260565267_H
#define FSTYPEEXTENSIONS_T260565267_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsTypeExtensions
struct  fsTypeExtensions_t260565267  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSTYPEEXTENSIONS_T260565267_H
#ifndef FSVERSIONMANAGER_T2158286414_H
#define FSVERSIONMANAGER_T2158286414_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.Internal.fsVersionManager
struct  fsVersionManager_t2158286414  : public RuntimeObject
{
public:

public:
};

struct fsVersionManager_t2158286414_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,FullSerializer.Internal.fsOption`1<FullSerializer.Internal.fsVersionedType>> FullSerializer.Internal.fsVersionManager::_cache
	Dictionary_2_t2909366393 * ____cache_0;

public:
	inline static int32_t get_offset_of__cache_0() { return static_cast<int32_t>(offsetof(fsVersionManager_t2158286414_StaticFields, ____cache_0)); }
	inline Dictionary_2_t2909366393 * get__cache_0() const { return ____cache_0; }
	inline Dictionary_2_t2909366393 ** get_address_of__cache_0() { return &____cache_0; }
	inline void set__cache_0(Dictionary_2_t2909366393 * value)
	{
		____cache_0 = value;
		Il2CppCodeGenWriteBarrier((&____cache_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSVERSIONMANAGER_T2158286414_H
#ifndef U3CCOLLECTPROPERTIESU3EC__ANONSTOREY0_T1756394196_H
#define U3CCOLLECTPROPERTIESU3EC__ANONSTOREY0_T1756394196_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsMetaType/<CollectProperties>c__AnonStorey0
struct  U3CCollectPropertiesU3Ec__AnonStorey0_t1756394196  : public RuntimeObject
{
public:
	// System.Reflection.MemberInfo FullSerializer.fsMetaType/<CollectProperties>c__AnonStorey0::member
	MemberInfo_t * ___member_0;

public:
	inline static int32_t get_offset_of_member_0() { return static_cast<int32_t>(offsetof(U3CCollectPropertiesU3Ec__AnonStorey0_t1756394196, ___member_0)); }
	inline MemberInfo_t * get_member_0() const { return ___member_0; }
	inline MemberInfo_t ** get_address_of_member_0() { return &___member_0; }
	inline void set_member_0(MemberInfo_t * value)
	{
		___member_0 = value;
		Il2CppCodeGenWriteBarrier((&___member_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOLLECTPROPERTIESU3EC__ANONSTOREY0_T1756394196_H
#ifndef FSMETAPROPERTY_T2249223145_H
#define FSMETAPROPERTY_T2249223145_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.Internal.fsMetaProperty
struct  fsMetaProperty_t2249223145  : public RuntimeObject
{
public:
	// System.Reflection.MemberInfo FullSerializer.Internal.fsMetaProperty::_memberInfo
	MemberInfo_t * ____memberInfo_0;
	// System.Type FullSerializer.Internal.fsMetaProperty::<StorageType>k__BackingField
	Type_t * ___U3CStorageTypeU3Ek__BackingField_1;
	// System.Boolean FullSerializer.Internal.fsMetaProperty::<CanRead>k__BackingField
	bool ___U3CCanReadU3Ek__BackingField_2;
	// System.Boolean FullSerializer.Internal.fsMetaProperty::<CanWrite>k__BackingField
	bool ___U3CCanWriteU3Ek__BackingField_3;
	// System.String FullSerializer.Internal.fsMetaProperty::<JsonName>k__BackingField
	String_t* ___U3CJsonNameU3Ek__BackingField_4;
	// System.String FullSerializer.Internal.fsMetaProperty::<MemberName>k__BackingField
	String_t* ___U3CMemberNameU3Ek__BackingField_5;
	// System.Boolean FullSerializer.Internal.fsMetaProperty::<IsPublic>k__BackingField
	bool ___U3CIsPublicU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of__memberInfo_0() { return static_cast<int32_t>(offsetof(fsMetaProperty_t2249223145, ____memberInfo_0)); }
	inline MemberInfo_t * get__memberInfo_0() const { return ____memberInfo_0; }
	inline MemberInfo_t ** get_address_of__memberInfo_0() { return &____memberInfo_0; }
	inline void set__memberInfo_0(MemberInfo_t * value)
	{
		____memberInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&____memberInfo_0), value);
	}

	inline static int32_t get_offset_of_U3CStorageTypeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(fsMetaProperty_t2249223145, ___U3CStorageTypeU3Ek__BackingField_1)); }
	inline Type_t * get_U3CStorageTypeU3Ek__BackingField_1() const { return ___U3CStorageTypeU3Ek__BackingField_1; }
	inline Type_t ** get_address_of_U3CStorageTypeU3Ek__BackingField_1() { return &___U3CStorageTypeU3Ek__BackingField_1; }
	inline void set_U3CStorageTypeU3Ek__BackingField_1(Type_t * value)
	{
		___U3CStorageTypeU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CStorageTypeU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CCanReadU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(fsMetaProperty_t2249223145, ___U3CCanReadU3Ek__BackingField_2)); }
	inline bool get_U3CCanReadU3Ek__BackingField_2() const { return ___U3CCanReadU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CCanReadU3Ek__BackingField_2() { return &___U3CCanReadU3Ek__BackingField_2; }
	inline void set_U3CCanReadU3Ek__BackingField_2(bool value)
	{
		___U3CCanReadU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CCanWriteU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(fsMetaProperty_t2249223145, ___U3CCanWriteU3Ek__BackingField_3)); }
	inline bool get_U3CCanWriteU3Ek__BackingField_3() const { return ___U3CCanWriteU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CCanWriteU3Ek__BackingField_3() { return &___U3CCanWriteU3Ek__BackingField_3; }
	inline void set_U3CCanWriteU3Ek__BackingField_3(bool value)
	{
		___U3CCanWriteU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CJsonNameU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(fsMetaProperty_t2249223145, ___U3CJsonNameU3Ek__BackingField_4)); }
	inline String_t* get_U3CJsonNameU3Ek__BackingField_4() const { return ___U3CJsonNameU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CJsonNameU3Ek__BackingField_4() { return &___U3CJsonNameU3Ek__BackingField_4; }
	inline void set_U3CJsonNameU3Ek__BackingField_4(String_t* value)
	{
		___U3CJsonNameU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CJsonNameU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CMemberNameU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(fsMetaProperty_t2249223145, ___U3CMemberNameU3Ek__BackingField_5)); }
	inline String_t* get_U3CMemberNameU3Ek__BackingField_5() const { return ___U3CMemberNameU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CMemberNameU3Ek__BackingField_5() { return &___U3CMemberNameU3Ek__BackingField_5; }
	inline void set_U3CMemberNameU3Ek__BackingField_5(String_t* value)
	{
		___U3CMemberNameU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMemberNameU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CIsPublicU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(fsMetaProperty_t2249223145, ___U3CIsPublicU3Ek__BackingField_6)); }
	inline bool get_U3CIsPublicU3Ek__BackingField_6() const { return ___U3CIsPublicU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CIsPublicU3Ek__BackingField_6() { return &___U3CIsPublicU3Ek__BackingField_6; }
	inline void set_U3CIsPublicU3Ek__BackingField_6(bool value)
	{
		___U3CIsPublicU3Ek__BackingField_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSMETAPROPERTY_T2249223145_H
#ifndef PARSER_T1915358011_H
#define PARSER_T1915358011_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiniJSON.Json/Parser
struct  Parser_t1915358011  : public RuntimeObject
{
public:
	// System.IO.StringReader MiniJSON.Json/Parser::json
	StringReader_t1480123486 * ___json_1;

public:
	inline static int32_t get_offset_of_json_1() { return static_cast<int32_t>(offsetof(Parser_t1915358011, ___json_1)); }
	inline StringReader_t1480123486 * get_json_1() const { return ___json_1; }
	inline StringReader_t1480123486 ** get_address_of_json_1() { return &___json_1; }
	inline void set_json_1(StringReader_t1480123486 * value)
	{
		___json_1 = value;
		Il2CppCodeGenWriteBarrier((&___json_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARSER_T1915358011_H
#ifndef EXCEPTION_T1927440687_H
#define EXCEPTION_T1927440687_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t1927440687  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t169632028* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t1927440687 * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t169632028* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t169632028** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t169632028* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___inner_exception_1)); }
	inline Exception_t1927440687 * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t1927440687 ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t1927440687 * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T1927440687_H
#ifndef ATTRIBUTE_T542643598_H
#define ATTRIBUTE_T542643598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t542643598  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T542643598_H
#ifndef FSBASECONVERTER_T1241677426_H
#define FSBASECONVERTER_T1241677426_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsBaseConverter
struct  fsBaseConverter_t1241677426  : public RuntimeObject
{
public:
	// FullSerializer.fsSerializer FullSerializer.fsBaseConverter::Serializer
	fsSerializer_t4193731081 * ___Serializer_0;

public:
	inline static int32_t get_offset_of_Serializer_0() { return static_cast<int32_t>(offsetof(fsBaseConverter_t1241677426, ___Serializer_0)); }
	inline fsSerializer_t4193731081 * get_Serializer_0() const { return ___Serializer_0; }
	inline fsSerializer_t4193731081 ** get_address_of_Serializer_0() { return &___Serializer_0; }
	inline void set_Serializer_0(fsSerializer_t4193731081 * value)
	{
		___Serializer_0 = value;
		Il2CppCodeGenWriteBarrier((&___Serializer_0), value);
	}
};

struct fsBaseConverter_t1241677426_StaticFields
{
public:
	// System.Func`2<FullSerializer.fsDataType,System.String> FullSerializer.fsBaseConverter::<>f__am$cache0
	Func_2_t1180654441 * ___U3CU3Ef__amU24cache0_1;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_1() { return static_cast<int32_t>(offsetof(fsBaseConverter_t1241677426_StaticFields, ___U3CU3Ef__amU24cache0_1)); }
	inline Func_2_t1180654441 * get_U3CU3Ef__amU24cache0_1() const { return ___U3CU3Ef__amU24cache0_1; }
	inline Func_2_t1180654441 ** get_address_of_U3CU3Ef__amU24cache0_1() { return &___U3CU3Ef__amU24cache0_1; }
	inline void set_U3CU3Ef__amU24cache0_1(Func_2_t1180654441 * value)
	{
		___U3CU3Ef__amU24cache0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSBASECONVERTER_T1241677426_H
#ifndef U3CWAITU3EC__ITERATOR0_T3077308673_H
#define U3CWAITU3EC__ITERATOR0_T3077308673_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Gestures.TapGesture/<wait>c__Iterator0
struct  U3CwaitU3Ec__Iterator0_t3077308673  : public RuntimeObject
{
public:
	// System.Single TouchScript.Gestures.TapGesture/<wait>c__Iterator0::<targetTime>__0
	float ___U3CtargetTimeU3E__0_0;
	// TouchScript.Gestures.TapGesture TouchScript.Gestures.TapGesture/<wait>c__Iterator0::$this
	TapGesture_t556401502 * ___U24this_1;
	// System.Object TouchScript.Gestures.TapGesture/<wait>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean TouchScript.Gestures.TapGesture/<wait>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 TouchScript.Gestures.TapGesture/<wait>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CtargetTimeU3E__0_0() { return static_cast<int32_t>(offsetof(U3CwaitU3Ec__Iterator0_t3077308673, ___U3CtargetTimeU3E__0_0)); }
	inline float get_U3CtargetTimeU3E__0_0() const { return ___U3CtargetTimeU3E__0_0; }
	inline float* get_address_of_U3CtargetTimeU3E__0_0() { return &___U3CtargetTimeU3E__0_0; }
	inline void set_U3CtargetTimeU3E__0_0(float value)
	{
		___U3CtargetTimeU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CwaitU3Ec__Iterator0_t3077308673, ___U24this_1)); }
	inline TapGesture_t556401502 * get_U24this_1() const { return ___U24this_1; }
	inline TapGesture_t556401502 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(TapGesture_t556401502 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CwaitU3Ec__Iterator0_t3077308673, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CwaitU3Ec__Iterator0_t3077308673, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CwaitU3Ec__Iterator0_t3077308673, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITU3EC__ITERATOR0_T3077308673_H
#ifndef TOUCHHANDLER_T2521645213_H
#define TOUCHHANDLER_T2521645213_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.InputSources.InputHandlers.TouchHandler
struct  TouchHandler_t2521645213  : public RuntimeObject
{
public:
	// System.Func`4<UnityEngine.Vector2,TouchScript.Tags,System.Boolean,TouchScript.TouchPoint> TouchScript.InputSources.InputHandlers.TouchHandler::beginTouch
	Func_4_t1475708928 * ___beginTouch_0;
	// System.Action`2<System.Int32,UnityEngine.Vector2> TouchScript.InputSources.InputHandlers.TouchHandler::moveTouch
	Action_2_t1542075644 * ___moveTouch_1;
	// System.Action`1<System.Int32> TouchScript.InputSources.InputHandlers.TouchHandler::endTouch
	Action_1_t1873676830 * ___endTouch_2;
	// System.Action`1<System.Int32> TouchScript.InputSources.InputHandlers.TouchHandler::cancelTouch
	Action_1_t1873676830 * ___cancelTouch_3;
	// TouchScript.Tags TouchScript.InputSources.InputHandlers.TouchHandler::tags
	Tags_t1265380163 * ___tags_4;
	// System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.InputSources.InputHandlers.TouchHandler/TouchState> TouchScript.InputSources.InputHandlers.TouchHandler::systemToInternalId
	Dictionary_2_t1739907934 * ___systemToInternalId_5;
	// System.Int32 TouchScript.InputSources.InputHandlers.TouchHandler::touchesNum
	int32_t ___touchesNum_6;

public:
	inline static int32_t get_offset_of_beginTouch_0() { return static_cast<int32_t>(offsetof(TouchHandler_t2521645213, ___beginTouch_0)); }
	inline Func_4_t1475708928 * get_beginTouch_0() const { return ___beginTouch_0; }
	inline Func_4_t1475708928 ** get_address_of_beginTouch_0() { return &___beginTouch_0; }
	inline void set_beginTouch_0(Func_4_t1475708928 * value)
	{
		___beginTouch_0 = value;
		Il2CppCodeGenWriteBarrier((&___beginTouch_0), value);
	}

	inline static int32_t get_offset_of_moveTouch_1() { return static_cast<int32_t>(offsetof(TouchHandler_t2521645213, ___moveTouch_1)); }
	inline Action_2_t1542075644 * get_moveTouch_1() const { return ___moveTouch_1; }
	inline Action_2_t1542075644 ** get_address_of_moveTouch_1() { return &___moveTouch_1; }
	inline void set_moveTouch_1(Action_2_t1542075644 * value)
	{
		___moveTouch_1 = value;
		Il2CppCodeGenWriteBarrier((&___moveTouch_1), value);
	}

	inline static int32_t get_offset_of_endTouch_2() { return static_cast<int32_t>(offsetof(TouchHandler_t2521645213, ___endTouch_2)); }
	inline Action_1_t1873676830 * get_endTouch_2() const { return ___endTouch_2; }
	inline Action_1_t1873676830 ** get_address_of_endTouch_2() { return &___endTouch_2; }
	inline void set_endTouch_2(Action_1_t1873676830 * value)
	{
		___endTouch_2 = value;
		Il2CppCodeGenWriteBarrier((&___endTouch_2), value);
	}

	inline static int32_t get_offset_of_cancelTouch_3() { return static_cast<int32_t>(offsetof(TouchHandler_t2521645213, ___cancelTouch_3)); }
	inline Action_1_t1873676830 * get_cancelTouch_3() const { return ___cancelTouch_3; }
	inline Action_1_t1873676830 ** get_address_of_cancelTouch_3() { return &___cancelTouch_3; }
	inline void set_cancelTouch_3(Action_1_t1873676830 * value)
	{
		___cancelTouch_3 = value;
		Il2CppCodeGenWriteBarrier((&___cancelTouch_3), value);
	}

	inline static int32_t get_offset_of_tags_4() { return static_cast<int32_t>(offsetof(TouchHandler_t2521645213, ___tags_4)); }
	inline Tags_t1265380163 * get_tags_4() const { return ___tags_4; }
	inline Tags_t1265380163 ** get_address_of_tags_4() { return &___tags_4; }
	inline void set_tags_4(Tags_t1265380163 * value)
	{
		___tags_4 = value;
		Il2CppCodeGenWriteBarrier((&___tags_4), value);
	}

	inline static int32_t get_offset_of_systemToInternalId_5() { return static_cast<int32_t>(offsetof(TouchHandler_t2521645213, ___systemToInternalId_5)); }
	inline Dictionary_2_t1739907934 * get_systemToInternalId_5() const { return ___systemToInternalId_5; }
	inline Dictionary_2_t1739907934 ** get_address_of_systemToInternalId_5() { return &___systemToInternalId_5; }
	inline void set_systemToInternalId_5(Dictionary_2_t1739907934 * value)
	{
		___systemToInternalId_5 = value;
		Il2CppCodeGenWriteBarrier((&___systemToInternalId_5), value);
	}

	inline static int32_t get_offset_of_touchesNum_6() { return static_cast<int32_t>(offsetof(TouchHandler_t2521645213, ___touchesNum_6)); }
	inline int32_t get_touchesNum_6() const { return ___touchesNum_6; }
	inline int32_t* get_address_of_touchesNum_6() { return &___touchesNum_6; }
	inline void set_touchesNum_6(int32_t value)
	{
		___touchesNum_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHHANDLER_T2521645213_H
#ifndef EVENTARGS_T3289624707_H
#define EVENTARGS_T3289624707_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventArgs
struct  EventArgs_t3289624707  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t3289624707_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t3289624707 * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t3289624707_StaticFields, ___Empty_0)); }
	inline EventArgs_t3289624707 * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t3289624707 ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t3289624707 * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTARGS_T3289624707_H
#ifndef SERIALIZER_T4088787656_H
#define SERIALIZER_T4088787656_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiniJSON.Json/Serializer
struct  Serializer_t4088787656  : public RuntimeObject
{
public:
	// System.Text.StringBuilder MiniJSON.Json/Serializer::builder
	StringBuilder_t1221177846 * ___builder_0;

public:
	inline static int32_t get_offset_of_builder_0() { return static_cast<int32_t>(offsetof(Serializer_t4088787656, ___builder_0)); }
	inline StringBuilder_t1221177846 * get_builder_0() const { return ___builder_0; }
	inline StringBuilder_t1221177846 ** get_address_of_builder_0() { return &___builder_0; }
	inline void set_builder_0(StringBuilder_t1221177846 * value)
	{
		___builder_0 = value;
		Il2CppCodeGenWriteBarrier((&___builder_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZER_T4088787656_H
#ifndef TUIOOBJECTMAPPING_T4080927128_H
#define TUIOOBJECTMAPPING_T4080927128_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.InputSources.TuioObjectMapping
struct  TuioObjectMapping_t4080927128  : public RuntimeObject
{
public:
	// System.Int32 TouchScript.InputSources.TuioObjectMapping::Id
	int32_t ___Id_0;
	// System.String TouchScript.InputSources.TuioObjectMapping::Tag
	String_t* ___Tag_1;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(TuioObjectMapping_t4080927128, ___Id_0)); }
	inline int32_t get_Id_0() const { return ___Id_0; }
	inline int32_t* get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(int32_t value)
	{
		___Id_0 = value;
	}

	inline static int32_t get_offset_of_Tag_1() { return static_cast<int32_t>(offsetof(TuioObjectMapping_t4080927128, ___Tag_1)); }
	inline String_t* get_Tag_1() const { return ___Tag_1; }
	inline String_t** get_address_of_Tag_1() { return &___Tag_1; }
	inline void set_Tag_1(String_t* value)
	{
		___Tag_1 = value;
		Il2CppCodeGenWriteBarrier((&___Tag_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUIOOBJECTMAPPING_T4080927128_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef CLUSTERS_T4089973855_H
#define CLUSTERS_T4089973855_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Clusters.Clusters
struct  Clusters_t4089973855  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<TouchScript.TouchPoint> TouchScript.Clusters.Clusters::points
	List_1_t328750215 * ___points_2;
	// System.Boolean TouchScript.Clusters.Clusters::dirty
	bool ___dirty_3;
	// System.Collections.Generic.List`1<TouchScript.TouchPoint> TouchScript.Clusters.Clusters::cluster1
	List_1_t328750215 * ___cluster1_4;
	// System.Collections.Generic.List`1<TouchScript.TouchPoint> TouchScript.Clusters.Clusters::cluster2
	List_1_t328750215 * ___cluster2_5;
	// System.Single TouchScript.Clusters.Clusters::minPointDistance
	float ___minPointDistance_6;
	// System.Single TouchScript.Clusters.Clusters::minPointDistanceSqr
	float ___minPointDistanceSqr_7;
	// System.Boolean TouchScript.Clusters.Clusters::hasClusters
	bool ___hasClusters_8;

public:
	inline static int32_t get_offset_of_points_2() { return static_cast<int32_t>(offsetof(Clusters_t4089973855, ___points_2)); }
	inline List_1_t328750215 * get_points_2() const { return ___points_2; }
	inline List_1_t328750215 ** get_address_of_points_2() { return &___points_2; }
	inline void set_points_2(List_1_t328750215 * value)
	{
		___points_2 = value;
		Il2CppCodeGenWriteBarrier((&___points_2), value);
	}

	inline static int32_t get_offset_of_dirty_3() { return static_cast<int32_t>(offsetof(Clusters_t4089973855, ___dirty_3)); }
	inline bool get_dirty_3() const { return ___dirty_3; }
	inline bool* get_address_of_dirty_3() { return &___dirty_3; }
	inline void set_dirty_3(bool value)
	{
		___dirty_3 = value;
	}

	inline static int32_t get_offset_of_cluster1_4() { return static_cast<int32_t>(offsetof(Clusters_t4089973855, ___cluster1_4)); }
	inline List_1_t328750215 * get_cluster1_4() const { return ___cluster1_4; }
	inline List_1_t328750215 ** get_address_of_cluster1_4() { return &___cluster1_4; }
	inline void set_cluster1_4(List_1_t328750215 * value)
	{
		___cluster1_4 = value;
		Il2CppCodeGenWriteBarrier((&___cluster1_4), value);
	}

	inline static int32_t get_offset_of_cluster2_5() { return static_cast<int32_t>(offsetof(Clusters_t4089973855, ___cluster2_5)); }
	inline List_1_t328750215 * get_cluster2_5() const { return ___cluster2_5; }
	inline List_1_t328750215 ** get_address_of_cluster2_5() { return &___cluster2_5; }
	inline void set_cluster2_5(List_1_t328750215 * value)
	{
		___cluster2_5 = value;
		Il2CppCodeGenWriteBarrier((&___cluster2_5), value);
	}

	inline static int32_t get_offset_of_minPointDistance_6() { return static_cast<int32_t>(offsetof(Clusters_t4089973855, ___minPointDistance_6)); }
	inline float get_minPointDistance_6() const { return ___minPointDistance_6; }
	inline float* get_address_of_minPointDistance_6() { return &___minPointDistance_6; }
	inline void set_minPointDistance_6(float value)
	{
		___minPointDistance_6 = value;
	}

	inline static int32_t get_offset_of_minPointDistanceSqr_7() { return static_cast<int32_t>(offsetof(Clusters_t4089973855, ___minPointDistanceSqr_7)); }
	inline float get_minPointDistanceSqr_7() const { return ___minPointDistanceSqr_7; }
	inline float* get_address_of_minPointDistanceSqr_7() { return &___minPointDistanceSqr_7; }
	inline void set_minPointDistanceSqr_7(float value)
	{
		___minPointDistanceSqr_7 = value;
	}

	inline static int32_t get_offset_of_hasClusters_8() { return static_cast<int32_t>(offsetof(Clusters_t4089973855, ___hasClusters_8)); }
	inline bool get_hasClusters_8() const { return ___hasClusters_8; }
	inline bool* get_address_of_hasClusters_8() { return &___hasClusters_8; }
	inline void set_hasClusters_8(bool value)
	{
		___hasClusters_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLUSTERS_T4089973855_H
#ifndef U3CGETFLATTENEDMETHODSU3EC__ITERATOR0_T3009184096_H
#define U3CGETFLATTENEDMETHODSU3EC__ITERATOR0_T3009184096_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.Internal.fsPortableReflection/<GetFlattenedMethods>c__Iterator0
struct  U3CGetFlattenedMethodsU3Ec__Iterator0_t3009184096  : public RuntimeObject
{
public:
	// System.Type FullSerializer.Internal.fsPortableReflection/<GetFlattenedMethods>c__Iterator0::type
	Type_t * ___type_0;
	// System.Reflection.MethodInfo[] FullSerializer.Internal.fsPortableReflection/<GetFlattenedMethods>c__Iterator0::<methods>__1
	MethodInfoU5BU5D_t152480188* ___U3CmethodsU3E__1_1;
	// System.Int32 FullSerializer.Internal.fsPortableReflection/<GetFlattenedMethods>c__Iterator0::<i>__2
	int32_t ___U3CiU3E__2_2;
	// System.String FullSerializer.Internal.fsPortableReflection/<GetFlattenedMethods>c__Iterator0::methodName
	String_t* ___methodName_3;
	// System.Reflection.MethodInfo FullSerializer.Internal.fsPortableReflection/<GetFlattenedMethods>c__Iterator0::$current
	MethodInfo_t * ___U24current_4;
	// System.Boolean FullSerializer.Internal.fsPortableReflection/<GetFlattenedMethods>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Type FullSerializer.Internal.fsPortableReflection/<GetFlattenedMethods>c__Iterator0::<$>type
	Type_t * ___U3CU24U3Etype_6;
	// System.Int32 FullSerializer.Internal.fsPortableReflection/<GetFlattenedMethods>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(U3CGetFlattenedMethodsU3Ec__Iterator0_t3009184096, ___type_0)); }
	inline Type_t * get_type_0() const { return ___type_0; }
	inline Type_t ** get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(Type_t * value)
	{
		___type_0 = value;
		Il2CppCodeGenWriteBarrier((&___type_0), value);
	}

	inline static int32_t get_offset_of_U3CmethodsU3E__1_1() { return static_cast<int32_t>(offsetof(U3CGetFlattenedMethodsU3Ec__Iterator0_t3009184096, ___U3CmethodsU3E__1_1)); }
	inline MethodInfoU5BU5D_t152480188* get_U3CmethodsU3E__1_1() const { return ___U3CmethodsU3E__1_1; }
	inline MethodInfoU5BU5D_t152480188** get_address_of_U3CmethodsU3E__1_1() { return &___U3CmethodsU3E__1_1; }
	inline void set_U3CmethodsU3E__1_1(MethodInfoU5BU5D_t152480188* value)
	{
		___U3CmethodsU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmethodsU3E__1_1), value);
	}

	inline static int32_t get_offset_of_U3CiU3E__2_2() { return static_cast<int32_t>(offsetof(U3CGetFlattenedMethodsU3Ec__Iterator0_t3009184096, ___U3CiU3E__2_2)); }
	inline int32_t get_U3CiU3E__2_2() const { return ___U3CiU3E__2_2; }
	inline int32_t* get_address_of_U3CiU3E__2_2() { return &___U3CiU3E__2_2; }
	inline void set_U3CiU3E__2_2(int32_t value)
	{
		___U3CiU3E__2_2 = value;
	}

	inline static int32_t get_offset_of_methodName_3() { return static_cast<int32_t>(offsetof(U3CGetFlattenedMethodsU3Ec__Iterator0_t3009184096, ___methodName_3)); }
	inline String_t* get_methodName_3() const { return ___methodName_3; }
	inline String_t** get_address_of_methodName_3() { return &___methodName_3; }
	inline void set_methodName_3(String_t* value)
	{
		___methodName_3 = value;
		Il2CppCodeGenWriteBarrier((&___methodName_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CGetFlattenedMethodsU3Ec__Iterator0_t3009184096, ___U24current_4)); }
	inline MethodInfo_t * get_U24current_4() const { return ___U24current_4; }
	inline MethodInfo_t ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(MethodInfo_t * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CGetFlattenedMethodsU3Ec__Iterator0_t3009184096, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3Etype_6() { return static_cast<int32_t>(offsetof(U3CGetFlattenedMethodsU3Ec__Iterator0_t3009184096, ___U3CU24U3Etype_6)); }
	inline Type_t * get_U3CU24U3Etype_6() const { return ___U3CU24U3Etype_6; }
	inline Type_t ** get_address_of_U3CU24U3Etype_6() { return &___U3CU24U3Etype_6; }
	inline void set_U3CU24U3Etype_6(Type_t * value)
	{
		___U3CU24U3Etype_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU24U3Etype_6), value);
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CGetFlattenedMethodsU3Ec__Iterator0_t3009184096, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETFLATTENEDMETHODSU3EC__ITERATOR0_T3009184096_H
#ifndef FSSERIALIZER_T4193731081_H
#define FSSERIALIZER_T4193731081_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsSerializer
struct  fsSerializer_t4193731081  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,FullSerializer.fsBaseConverter> FullSerializer.fsSerializer::_cachedConverters
	Dictionary_2_t3179035323 * ____cachedConverters_6;
	// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.List`1<FullSerializer.fsObjectProcessor>> FullSerializer.fsSerializer::_cachedProcessors
	Dictionary_2_t3992733373 * ____cachedProcessors_7;
	// System.Collections.Generic.List`1<FullSerializer.fsConverter> FullSerializer.fsSerializer::_availableConverters
	List_1_t4130846565 * ____availableConverters_8;
	// System.Collections.Generic.Dictionary`2<System.Type,FullSerializer.fsDirectConverter> FullSerializer.fsSerializer::_availableDirectConverters
	Dictionary_2_t2700818715 * ____availableDirectConverters_9;
	// System.Collections.Generic.List`1<FullSerializer.fsObjectProcessor> FullSerializer.fsSerializer::_processors
	List_1_t2055375476 * ____processors_10;
	// FullSerializer.Internal.fsCyclicReferenceManager FullSerializer.fsSerializer::_references
	fsCyclicReferenceManager_t1995018378 * ____references_11;
	// FullSerializer.fsSerializer/fsLazyCycleDefinitionWriter FullSerializer.fsSerializer::_lazyReferenceWriter
	fsLazyCycleDefinitionWriter_t2327014926 * ____lazyReferenceWriter_12;
	// FullSerializer.fsContext FullSerializer.fsSerializer::Context
	fsContext_t2896355488 * ___Context_13;

public:
	inline static int32_t get_offset_of__cachedConverters_6() { return static_cast<int32_t>(offsetof(fsSerializer_t4193731081, ____cachedConverters_6)); }
	inline Dictionary_2_t3179035323 * get__cachedConverters_6() const { return ____cachedConverters_6; }
	inline Dictionary_2_t3179035323 ** get_address_of__cachedConverters_6() { return &____cachedConverters_6; }
	inline void set__cachedConverters_6(Dictionary_2_t3179035323 * value)
	{
		____cachedConverters_6 = value;
		Il2CppCodeGenWriteBarrier((&____cachedConverters_6), value);
	}

	inline static int32_t get_offset_of__cachedProcessors_7() { return static_cast<int32_t>(offsetof(fsSerializer_t4193731081, ____cachedProcessors_7)); }
	inline Dictionary_2_t3992733373 * get__cachedProcessors_7() const { return ____cachedProcessors_7; }
	inline Dictionary_2_t3992733373 ** get_address_of__cachedProcessors_7() { return &____cachedProcessors_7; }
	inline void set__cachedProcessors_7(Dictionary_2_t3992733373 * value)
	{
		____cachedProcessors_7 = value;
		Il2CppCodeGenWriteBarrier((&____cachedProcessors_7), value);
	}

	inline static int32_t get_offset_of__availableConverters_8() { return static_cast<int32_t>(offsetof(fsSerializer_t4193731081, ____availableConverters_8)); }
	inline List_1_t4130846565 * get__availableConverters_8() const { return ____availableConverters_8; }
	inline List_1_t4130846565 ** get_address_of__availableConverters_8() { return &____availableConverters_8; }
	inline void set__availableConverters_8(List_1_t4130846565 * value)
	{
		____availableConverters_8 = value;
		Il2CppCodeGenWriteBarrier((&____availableConverters_8), value);
	}

	inline static int32_t get_offset_of__availableDirectConverters_9() { return static_cast<int32_t>(offsetof(fsSerializer_t4193731081, ____availableDirectConverters_9)); }
	inline Dictionary_2_t2700818715 * get__availableDirectConverters_9() const { return ____availableDirectConverters_9; }
	inline Dictionary_2_t2700818715 ** get_address_of__availableDirectConverters_9() { return &____availableDirectConverters_9; }
	inline void set__availableDirectConverters_9(Dictionary_2_t2700818715 * value)
	{
		____availableDirectConverters_9 = value;
		Il2CppCodeGenWriteBarrier((&____availableDirectConverters_9), value);
	}

	inline static int32_t get_offset_of__processors_10() { return static_cast<int32_t>(offsetof(fsSerializer_t4193731081, ____processors_10)); }
	inline List_1_t2055375476 * get__processors_10() const { return ____processors_10; }
	inline List_1_t2055375476 ** get_address_of__processors_10() { return &____processors_10; }
	inline void set__processors_10(List_1_t2055375476 * value)
	{
		____processors_10 = value;
		Il2CppCodeGenWriteBarrier((&____processors_10), value);
	}

	inline static int32_t get_offset_of__references_11() { return static_cast<int32_t>(offsetof(fsSerializer_t4193731081, ____references_11)); }
	inline fsCyclicReferenceManager_t1995018378 * get__references_11() const { return ____references_11; }
	inline fsCyclicReferenceManager_t1995018378 ** get_address_of__references_11() { return &____references_11; }
	inline void set__references_11(fsCyclicReferenceManager_t1995018378 * value)
	{
		____references_11 = value;
		Il2CppCodeGenWriteBarrier((&____references_11), value);
	}

	inline static int32_t get_offset_of__lazyReferenceWriter_12() { return static_cast<int32_t>(offsetof(fsSerializer_t4193731081, ____lazyReferenceWriter_12)); }
	inline fsLazyCycleDefinitionWriter_t2327014926 * get__lazyReferenceWriter_12() const { return ____lazyReferenceWriter_12; }
	inline fsLazyCycleDefinitionWriter_t2327014926 ** get_address_of__lazyReferenceWriter_12() { return &____lazyReferenceWriter_12; }
	inline void set__lazyReferenceWriter_12(fsLazyCycleDefinitionWriter_t2327014926 * value)
	{
		____lazyReferenceWriter_12 = value;
		Il2CppCodeGenWriteBarrier((&____lazyReferenceWriter_12), value);
	}

	inline static int32_t get_offset_of_Context_13() { return static_cast<int32_t>(offsetof(fsSerializer_t4193731081, ___Context_13)); }
	inline fsContext_t2896355488 * get_Context_13() const { return ___Context_13; }
	inline fsContext_t2896355488 ** get_address_of_Context_13() { return &___Context_13; }
	inline void set_Context_13(fsContext_t2896355488 * value)
	{
		___Context_13 = value;
		Il2CppCodeGenWriteBarrier((&___Context_13), value);
	}
};

struct fsSerializer_t4193731081_StaticFields
{
public:
	// System.Collections.Generic.HashSet`1<System.String> FullSerializer.fsSerializer::_reservedKeywords
	HashSet_1_t362681087 * ____reservedKeywords_0;

public:
	inline static int32_t get_offset_of__reservedKeywords_0() { return static_cast<int32_t>(offsetof(fsSerializer_t4193731081_StaticFields, ____reservedKeywords_0)); }
	inline HashSet_1_t362681087 * get__reservedKeywords_0() const { return ____reservedKeywords_0; }
	inline HashSet_1_t362681087 ** get_address_of__reservedKeywords_0() { return &____reservedKeywords_0; }
	inline void set__reservedKeywords_0(HashSet_1_t362681087 * value)
	{
		____reservedKeywords_0 = value;
		Il2CppCodeGenWriteBarrier((&____reservedKeywords_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSSERIALIZER_T4193731081_H
#ifndef FSLAZYCYCLEDEFINITIONWRITER_T2327014926_H
#define FSLAZYCYCLEDEFINITIONWRITER_T2327014926_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsSerializer/fsLazyCycleDefinitionWriter
struct  fsLazyCycleDefinitionWriter_t2327014926  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,FullSerializer.fsData> FullSerializer.fsSerializer/fsLazyCycleDefinitionWriter::_pendingDefinitions
	Dictionary_2_t1591631240 * ____pendingDefinitions_0;
	// System.Collections.Generic.HashSet`1<System.Int32> FullSerializer.fsSerializer/fsLazyCycleDefinitionWriter::_references
	HashSet_1_t405338302 * ____references_1;

public:
	inline static int32_t get_offset_of__pendingDefinitions_0() { return static_cast<int32_t>(offsetof(fsLazyCycleDefinitionWriter_t2327014926, ____pendingDefinitions_0)); }
	inline Dictionary_2_t1591631240 * get__pendingDefinitions_0() const { return ____pendingDefinitions_0; }
	inline Dictionary_2_t1591631240 ** get_address_of__pendingDefinitions_0() { return &____pendingDefinitions_0; }
	inline void set__pendingDefinitions_0(Dictionary_2_t1591631240 * value)
	{
		____pendingDefinitions_0 = value;
		Il2CppCodeGenWriteBarrier((&____pendingDefinitions_0), value);
	}

	inline static int32_t get_offset_of__references_1() { return static_cast<int32_t>(offsetof(fsLazyCycleDefinitionWriter_t2327014926, ____references_1)); }
	inline HashSet_1_t405338302 * get__references_1() const { return ____references_1; }
	inline HashSet_1_t405338302 ** get_address_of__references_1() { return &____references_1; }
	inline void set__references_1(HashSet_1_t405338302 * value)
	{
		____references_1 = value;
		Il2CppCodeGenWriteBarrier((&____references_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSLAZYCYCLEDEFINITIONWRITER_T2327014926_H
#ifndef FSCYCLICREFERENCEMANAGER_T1995018378_H
#define FSCYCLICREFERENCEMANAGER_T1995018378_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.Internal.fsCyclicReferenceManager
struct  fsCyclicReferenceManager_t1995018378  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.Object,System.Int32> FullSerializer.Internal.fsCyclicReferenceManager::_objectIds
	Dictionary_2_t1663937576 * ____objectIds_0;
	// System.Int32 FullSerializer.Internal.fsCyclicReferenceManager::_nextId
	int32_t ____nextId_1;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Object> FullSerializer.Internal.fsCyclicReferenceManager::_marked
	Dictionary_2_t1697274930 * ____marked_2;
	// System.Int32 FullSerializer.Internal.fsCyclicReferenceManager::_depth
	int32_t ____depth_3;

public:
	inline static int32_t get_offset_of__objectIds_0() { return static_cast<int32_t>(offsetof(fsCyclicReferenceManager_t1995018378, ____objectIds_0)); }
	inline Dictionary_2_t1663937576 * get__objectIds_0() const { return ____objectIds_0; }
	inline Dictionary_2_t1663937576 ** get_address_of__objectIds_0() { return &____objectIds_0; }
	inline void set__objectIds_0(Dictionary_2_t1663937576 * value)
	{
		____objectIds_0 = value;
		Il2CppCodeGenWriteBarrier((&____objectIds_0), value);
	}

	inline static int32_t get_offset_of__nextId_1() { return static_cast<int32_t>(offsetof(fsCyclicReferenceManager_t1995018378, ____nextId_1)); }
	inline int32_t get__nextId_1() const { return ____nextId_1; }
	inline int32_t* get_address_of__nextId_1() { return &____nextId_1; }
	inline void set__nextId_1(int32_t value)
	{
		____nextId_1 = value;
	}

	inline static int32_t get_offset_of__marked_2() { return static_cast<int32_t>(offsetof(fsCyclicReferenceManager_t1995018378, ____marked_2)); }
	inline Dictionary_2_t1697274930 * get__marked_2() const { return ____marked_2; }
	inline Dictionary_2_t1697274930 ** get_address_of__marked_2() { return &____marked_2; }
	inline void set__marked_2(Dictionary_2_t1697274930 * value)
	{
		____marked_2 = value;
		Il2CppCodeGenWriteBarrier((&____marked_2), value);
	}

	inline static int32_t get_offset_of__depth_3() { return static_cast<int32_t>(offsetof(fsCyclicReferenceManager_t1995018378, ____depth_3)); }
	inline int32_t get__depth_3() const { return ____depth_3; }
	inline int32_t* get_address_of__depth_3() { return &____depth_3; }
	inline void set__depth_3(int32_t value)
	{
		____depth_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSCYCLICREFERENCEMANAGER_T1995018378_H
#ifndef FSDATA_T2583805605_H
#define FSDATA_T2583805605_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsData
struct  fsData_t2583805605  : public RuntimeObject
{
public:
	// System.Object FullSerializer.fsData::_value
	RuntimeObject * ____value_0;

public:
	inline static int32_t get_offset_of__value_0() { return static_cast<int32_t>(offsetof(fsData_t2583805605, ____value_0)); }
	inline RuntimeObject * get__value_0() const { return ____value_0; }
	inline RuntimeObject ** get_address_of__value_0() { return &____value_0; }
	inline void set__value_0(RuntimeObject * value)
	{
		____value_0 = value;
		Il2CppCodeGenWriteBarrier((&____value_0), value);
	}
};

struct fsData_t2583805605_StaticFields
{
public:
	// FullSerializer.fsData FullSerializer.fsData::True
	fsData_t2583805605 * ___True_1;
	// FullSerializer.fsData FullSerializer.fsData::False
	fsData_t2583805605 * ___False_2;
	// FullSerializer.fsData FullSerializer.fsData::Null
	fsData_t2583805605 * ___Null_3;

public:
	inline static int32_t get_offset_of_True_1() { return static_cast<int32_t>(offsetof(fsData_t2583805605_StaticFields, ___True_1)); }
	inline fsData_t2583805605 * get_True_1() const { return ___True_1; }
	inline fsData_t2583805605 ** get_address_of_True_1() { return &___True_1; }
	inline void set_True_1(fsData_t2583805605 * value)
	{
		___True_1 = value;
		Il2CppCodeGenWriteBarrier((&___True_1), value);
	}

	inline static int32_t get_offset_of_False_2() { return static_cast<int32_t>(offsetof(fsData_t2583805605_StaticFields, ___False_2)); }
	inline fsData_t2583805605 * get_False_2() const { return ___False_2; }
	inline fsData_t2583805605 ** get_address_of_False_2() { return &___False_2; }
	inline void set_False_2(fsData_t2583805605 * value)
	{
		___False_2 = value;
		Il2CppCodeGenWriteBarrier((&___False_2), value);
	}

	inline static int32_t get_offset_of_Null_3() { return static_cast<int32_t>(offsetof(fsData_t2583805605_StaticFields, ___Null_3)); }
	inline fsData_t2583805605 * get_Null_3() const { return ___Null_3; }
	inline fsData_t2583805605 ** get_address_of_Null_3() { return &___Null_3; }
	inline void set_Null_3(fsData_t2583805605 * value)
	{
		___Null_3 = value;
		Il2CppCodeGenWriteBarrier((&___Null_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSDATA_T2583805605_H
#ifndef FSJSONPARSER_T1906440848_H
#define FSJSONPARSER_T1906440848_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsJsonParser
struct  fsJsonParser_t1906440848  : public RuntimeObject
{
public:
	// System.Int32 FullSerializer.fsJsonParser::_start
	int32_t ____start_0;
	// System.String FullSerializer.fsJsonParser::_input
	String_t* ____input_1;
	// System.Text.StringBuilder FullSerializer.fsJsonParser::_cachedStringBuilder
	StringBuilder_t1221177846 * ____cachedStringBuilder_2;

public:
	inline static int32_t get_offset_of__start_0() { return static_cast<int32_t>(offsetof(fsJsonParser_t1906440848, ____start_0)); }
	inline int32_t get__start_0() const { return ____start_0; }
	inline int32_t* get_address_of__start_0() { return &____start_0; }
	inline void set__start_0(int32_t value)
	{
		____start_0 = value;
	}

	inline static int32_t get_offset_of__input_1() { return static_cast<int32_t>(offsetof(fsJsonParser_t1906440848, ____input_1)); }
	inline String_t* get__input_1() const { return ____input_1; }
	inline String_t** get_address_of__input_1() { return &____input_1; }
	inline void set__input_1(String_t* value)
	{
		____input_1 = value;
		Il2CppCodeGenWriteBarrier((&____input_1), value);
	}

	inline static int32_t get_offset_of__cachedStringBuilder_2() { return static_cast<int32_t>(offsetof(fsJsonParser_t1906440848, ____cachedStringBuilder_2)); }
	inline StringBuilder_t1221177846 * get__cachedStringBuilder_2() const { return ____cachedStringBuilder_2; }
	inline StringBuilder_t1221177846 ** get_address_of__cachedStringBuilder_2() { return &____cachedStringBuilder_2; }
	inline void set__cachedStringBuilder_2(StringBuilder_t1221177846 * value)
	{
		____cachedStringBuilder_2 = value;
		Il2CppCodeGenWriteBarrier((&____cachedStringBuilder_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSJSONPARSER_T1906440848_H
#ifndef FSJSONPRINTER_T3780364767_H
#define FSJSONPRINTER_T3780364767_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsJsonPrinter
struct  fsJsonPrinter_t3780364767  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSJSONPRINTER_T3780364767_H
#ifndef FSOBJECTPROCESSOR_T2686254344_H
#define FSOBJECTPROCESSOR_T2686254344_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsObjectProcessor
struct  fsObjectProcessor_t2686254344  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSOBJECTPROCESSOR_T2686254344_H
#ifndef U3CCANSERIALIZEFIELDU3EC__ANONSTOREY2_T2547514815_H
#define U3CCANSERIALIZEFIELDU3EC__ANONSTOREY2_T2547514815_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsMetaType/<CanSerializeField>c__AnonStorey2
struct  U3CCanSerializeFieldU3Ec__AnonStorey2_t2547514815  : public RuntimeObject
{
public:
	// System.Reflection.FieldInfo FullSerializer.fsMetaType/<CanSerializeField>c__AnonStorey2::field
	FieldInfo_t * ___field_0;

public:
	inline static int32_t get_offset_of_field_0() { return static_cast<int32_t>(offsetof(U3CCanSerializeFieldU3Ec__AnonStorey2_t2547514815, ___field_0)); }
	inline FieldInfo_t * get_field_0() const { return ___field_0; }
	inline FieldInfo_t ** get_address_of_field_0() { return &___field_0; }
	inline void set_field_0(FieldInfo_t * value)
	{
		___field_0 = value;
		Il2CppCodeGenWriteBarrier((&___field_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCANSERIALIZEFIELDU3EC__ANONSTOREY2_T2547514815_H
#ifndef ATTRIBUTEQUERYCOMPARATOR_T4230352628_H
#define ATTRIBUTEQUERYCOMPARATOR_T4230352628_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.Internal.fsPortableReflection/AttributeQueryComparator
struct  AttributeQueryComparator_t4230352628  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTEQUERYCOMPARATOR_T4230352628_H
#ifndef FSOPTION_T670623048_H
#define FSOPTION_T670623048_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.Internal.fsOption
struct  fsOption_t670623048  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSOPTION_T670623048_H
#ifndef OBJECTREFERENCEEQUALITYCOMPARATOR_T3520131135_H
#define OBJECTREFERENCEEQUALITYCOMPARATOR_T3520131135_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.Internal.fsCyclicReferenceManager/ObjectReferenceEqualityComparator
struct  ObjectReferenceEqualityComparator_t3520131135  : public RuntimeObject
{
public:

public:
};

struct ObjectReferenceEqualityComparator_t3520131135_StaticFields
{
public:
	// System.Collections.Generic.IEqualityComparer`1<System.Object> FullSerializer.Internal.fsCyclicReferenceManager/ObjectReferenceEqualityComparator::Instance
	RuntimeObject* ___Instance_0;

public:
	inline static int32_t get_offset_of_Instance_0() { return static_cast<int32_t>(offsetof(ObjectReferenceEqualityComparator_t3520131135_StaticFields, ___Instance_0)); }
	inline RuntimeObject* get_Instance_0() const { return ___Instance_0; }
	inline RuntimeObject** get_address_of_Instance_0() { return &___Instance_0; }
	inline void set_Instance_0(RuntimeObject* value)
	{
		___Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTREFERENCEEQUALITYCOMPARATOR_T3520131135_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	IntPtr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline IntPtr_t get_Zero_1() const { return ___Zero_1; }
	inline IntPtr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(IntPtr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef FSIGNOREATTRIBUTE_T839965207_H
#define FSIGNOREATTRIBUTE_T839965207_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsIgnoreAttribute
struct  fsIgnoreAttribute_t839965207  : public Attribute_t542643598
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSIGNOREATTRIBUTE_T839965207_H
#ifndef NULLABLE_1_T2088641033_H
#define NULLABLE_1_T2088641033_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Boolean>
struct  Nullable_1_t2088641033 
{
public:
	// T System.Nullable`1::value
	bool ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t2088641033, ___value_0)); }
	inline bool get_value_0() const { return ___value_0; }
	inline bool* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(bool value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t2088641033, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T2088641033_H
#ifndef FSSERIALIZATIONCALLBACKPROCESSOR_T357373768_H
#define FSSERIALIZATIONCALLBACKPROCESSOR_T357373768_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.Internal.fsSerializationCallbackProcessor
struct  fsSerializationCallbackProcessor_t357373768  : public fsObjectProcessor_t2686254344
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSSERIALIZATIONCALLBACKPROCESSOR_T357373768_H
#ifndef FSDUPLICATEVERSIONNAMEEXCEPTION_T4001675182_H
#define FSDUPLICATEVERSIONNAMEEXCEPTION_T4001675182_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsDuplicateVersionNameException
struct  fsDuplicateVersionNameException_t4001675182  : public Exception_t1927440687
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSDUPLICATEVERSIONNAMEEXCEPTION_T4001675182_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t1328083999* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t1328083999* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t1328083999** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t1328083999* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef TOUCHDATA_T3880599975_H
#define TOUCHDATA_T3880599975_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Gestures.UI.UIGesture/TouchData
struct  TouchData_t3880599975 
{
public:
	// System.Boolean TouchScript.Gestures.UI.UIGesture/TouchData::OnTarget
	bool ___OnTarget_0;
	// UnityEngine.EventSystems.PointerEventData TouchScript.Gestures.UI.UIGesture/TouchData::Data
	PointerEventData_t1599784723 * ___Data_1;

public:
	inline static int32_t get_offset_of_OnTarget_0() { return static_cast<int32_t>(offsetof(TouchData_t3880599975, ___OnTarget_0)); }
	inline bool get_OnTarget_0() const { return ___OnTarget_0; }
	inline bool* get_address_of_OnTarget_0() { return &___OnTarget_0; }
	inline void set_OnTarget_0(bool value)
	{
		___OnTarget_0 = value;
	}

	inline static int32_t get_offset_of_Data_1() { return static_cast<int32_t>(offsetof(TouchData_t3880599975, ___Data_1)); }
	inline PointerEventData_t1599784723 * get_Data_1() const { return ___Data_1; }
	inline PointerEventData_t1599784723 ** get_address_of_Data_1() { return &___Data_1; }
	inline void set_Data_1(PointerEventData_t1599784723 * value)
	{
		___Data_1 = value;
		Il2CppCodeGenWriteBarrier((&___Data_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TouchScript.Gestures.UI.UIGesture/TouchData
struct TouchData_t3880599975_marshaled_pinvoke
{
	int32_t ___OnTarget_0;
	PointerEventData_t1599784723 * ___Data_1;
};
// Native definition for COM marshalling of TouchScript.Gestures.UI.UIGesture/TouchData
struct TouchData_t3880599975_marshaled_com
{
	int32_t ___OnTarget_0;
	PointerEventData_t1599784723 * ___Data_1;
};
#endif // TOUCHDATA_T3880599975_H
#ifndef METAGESTUREEVENTARGS_T256591615_H
#define METAGESTUREEVENTARGS_T256591615_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Gestures.MetaGestureEventArgs
struct  MetaGestureEventArgs_t256591615  : public EventArgs_t3289624707
{
public:
	// TouchScript.TouchPoint TouchScript.Gestures.MetaGestureEventArgs::<Touch>k__BackingField
	TouchPoint_t959629083 * ___U3CTouchU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CTouchU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(MetaGestureEventArgs_t256591615, ___U3CTouchU3Ek__BackingField_1)); }
	inline TouchPoint_t959629083 * get_U3CTouchU3Ek__BackingField_1() const { return ___U3CTouchU3Ek__BackingField_1; }
	inline TouchPoint_t959629083 ** get_address_of_U3CTouchU3Ek__BackingField_1() { return &___U3CTouchU3Ek__BackingField_1; }
	inline void set_U3CTouchU3Ek__BackingField_1(TouchPoint_t959629083 * value)
	{
		___U3CTouchU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTouchU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METAGESTUREEVENTARGS_T256591615_H
#ifndef FSMISSINGVERSIONCONSTRUCTOREXCEPTION_T849263018_H
#define FSMISSINGVERSIONCONSTRUCTOREXCEPTION_T849263018_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsMissingVersionConstructorException
struct  fsMissingVersionConstructorException_t849263018  : public Exception_t1927440687
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSMISSINGVERSIONCONSTRUCTOREXCEPTION_T849263018_H
#ifndef FSDIRECTCONVERTER_T763460818_H
#define FSDIRECTCONVERTER_T763460818_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsDirectConverter
struct  fsDirectConverter_t763460818  : public fsBaseConverter_t1241677426
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSDIRECTCONVERTER_T763460818_H
#ifndef FSPROPERTYATTRIBUTE_T4237399860_H
#define FSPROPERTYATTRIBUTE_T4237399860_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsPropertyAttribute
struct  fsPropertyAttribute_t4237399860  : public Attribute_t542643598
{
public:
	// System.String FullSerializer.fsPropertyAttribute::Name
	String_t* ___Name_0;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(fsPropertyAttribute_t4237399860, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSPROPERTYATTRIBUTE_T4237399860_H
#ifndef ATTRIBUTEQUERY_T604298480_H
#define ATTRIBUTEQUERY_T604298480_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.Internal.fsPortableReflection/AttributeQuery
struct  AttributeQuery_t604298480 
{
public:
	// System.Reflection.MemberInfo FullSerializer.Internal.fsPortableReflection/AttributeQuery::MemberInfo
	MemberInfo_t * ___MemberInfo_0;
	// System.Type FullSerializer.Internal.fsPortableReflection/AttributeQuery::AttributeType
	Type_t * ___AttributeType_1;

public:
	inline static int32_t get_offset_of_MemberInfo_0() { return static_cast<int32_t>(offsetof(AttributeQuery_t604298480, ___MemberInfo_0)); }
	inline MemberInfo_t * get_MemberInfo_0() const { return ___MemberInfo_0; }
	inline MemberInfo_t ** get_address_of_MemberInfo_0() { return &___MemberInfo_0; }
	inline void set_MemberInfo_0(MemberInfo_t * value)
	{
		___MemberInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___MemberInfo_0), value);
	}

	inline static int32_t get_offset_of_AttributeType_1() { return static_cast<int32_t>(offsetof(AttributeQuery_t604298480, ___AttributeType_1)); }
	inline Type_t * get_AttributeType_1() const { return ___AttributeType_1; }
	inline Type_t ** get_address_of_AttributeType_1() { return &___AttributeType_1; }
	inline void set_AttributeType_1(Type_t * value)
	{
		___AttributeType_1 = value;
		Il2CppCodeGenWriteBarrier((&___AttributeType_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of FullSerializer.Internal.fsPortableReflection/AttributeQuery
struct AttributeQuery_t604298480_marshaled_pinvoke
{
	MemberInfo_t * ___MemberInfo_0;
	Type_t * ___AttributeType_1;
};
// Native definition for COM marshalling of FullSerializer.Internal.fsPortableReflection/AttributeQuery
struct AttributeQuery_t604298480_marshaled_com
{
	MemberInfo_t * ___MemberInfo_0;
	Type_t * ___AttributeType_1;
};
#endif // ATTRIBUTEQUERY_T604298480_H
#ifndef FSVERSIONEDTYPE_T654750358_H
#define FSVERSIONEDTYPE_T654750358_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.Internal.fsVersionedType
struct  fsVersionedType_t654750358 
{
public:
	// FullSerializer.Internal.fsVersionedType[] FullSerializer.Internal.fsVersionedType::Ancestors
	fsVersionedTypeU5BU5D_t1849274963* ___Ancestors_0;
	// System.String FullSerializer.Internal.fsVersionedType::VersionString
	String_t* ___VersionString_1;
	// System.Type FullSerializer.Internal.fsVersionedType::ModelType
	Type_t * ___ModelType_2;

public:
	inline static int32_t get_offset_of_Ancestors_0() { return static_cast<int32_t>(offsetof(fsVersionedType_t654750358, ___Ancestors_0)); }
	inline fsVersionedTypeU5BU5D_t1849274963* get_Ancestors_0() const { return ___Ancestors_0; }
	inline fsVersionedTypeU5BU5D_t1849274963** get_address_of_Ancestors_0() { return &___Ancestors_0; }
	inline void set_Ancestors_0(fsVersionedTypeU5BU5D_t1849274963* value)
	{
		___Ancestors_0 = value;
		Il2CppCodeGenWriteBarrier((&___Ancestors_0), value);
	}

	inline static int32_t get_offset_of_VersionString_1() { return static_cast<int32_t>(offsetof(fsVersionedType_t654750358, ___VersionString_1)); }
	inline String_t* get_VersionString_1() const { return ___VersionString_1; }
	inline String_t** get_address_of_VersionString_1() { return &___VersionString_1; }
	inline void set_VersionString_1(String_t* value)
	{
		___VersionString_1 = value;
		Il2CppCodeGenWriteBarrier((&___VersionString_1), value);
	}

	inline static int32_t get_offset_of_ModelType_2() { return static_cast<int32_t>(offsetof(fsVersionedType_t654750358, ___ModelType_2)); }
	inline Type_t * get_ModelType_2() const { return ___ModelType_2; }
	inline Type_t ** get_address_of_ModelType_2() { return &___ModelType_2; }
	inline void set_ModelType_2(Type_t * value)
	{
		___ModelType_2 = value;
		Il2CppCodeGenWriteBarrier((&___ModelType_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of FullSerializer.Internal.fsVersionedType
struct fsVersionedType_t654750358_marshaled_pinvoke
{
	fsVersionedType_t654750358_marshaled_pinvoke* ___Ancestors_0;
	char* ___VersionString_1;
	Type_t * ___ModelType_2;
};
// Native definition for COM marshalling of FullSerializer.Internal.fsVersionedType
struct fsVersionedType_t654750358_marshaled_com
{
	fsVersionedType_t654750358_marshaled_com* ___Ancestors_0;
	Il2CppChar* ___VersionString_1;
	Type_t * ___ModelType_2;
};
#endif // FSVERSIONEDTYPE_T654750358_H
#ifndef VECTOR3_T2243707580_H
#define VECTOR3_T2243707580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t2243707580 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t2243707580_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t2243707580  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t2243707580  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t2243707580  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t2243707580  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t2243707580  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t2243707580  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t2243707580  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t2243707580  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t2243707580  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t2243707580  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___zeroVector_4)); }
	inline Vector3_t2243707580  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t2243707580 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t2243707580  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___oneVector_5)); }
	inline Vector3_t2243707580  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t2243707580 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t2243707580  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___upVector_6)); }
	inline Vector3_t2243707580  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t2243707580 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t2243707580  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___downVector_7)); }
	inline Vector3_t2243707580  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t2243707580 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t2243707580  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___leftVector_8)); }
	inline Vector3_t2243707580  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t2243707580 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t2243707580  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___rightVector_9)); }
	inline Vector3_t2243707580  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t2243707580 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t2243707580  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___forwardVector_10)); }
	inline Vector3_t2243707580  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t2243707580 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t2243707580  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___backVector_11)); }
	inline Vector3_t2243707580  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t2243707580 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t2243707580  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t2243707580  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t2243707580 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t2243707580  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t2243707580  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t2243707580 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t2243707580  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T2243707580_H
#ifndef FSRESULT_T862419890_H
#define FSRESULT_T862419890_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsResult
struct  fsResult_t862419890 
{
public:
	// System.Boolean FullSerializer.fsResult::_success
	bool ____success_1;
	// System.Collections.Generic.List`1<System.String> FullSerializer.fsResult::_messages
	List_1_t1398341365 * ____messages_2;

public:
	inline static int32_t get_offset_of__success_1() { return static_cast<int32_t>(offsetof(fsResult_t862419890, ____success_1)); }
	inline bool get__success_1() const { return ____success_1; }
	inline bool* get_address_of__success_1() { return &____success_1; }
	inline void set__success_1(bool value)
	{
		____success_1 = value;
	}

	inline static int32_t get_offset_of__messages_2() { return static_cast<int32_t>(offsetof(fsResult_t862419890, ____messages_2)); }
	inline List_1_t1398341365 * get__messages_2() const { return ____messages_2; }
	inline List_1_t1398341365 ** get_address_of__messages_2() { return &____messages_2; }
	inline void set__messages_2(List_1_t1398341365 * value)
	{
		____messages_2 = value;
		Il2CppCodeGenWriteBarrier((&____messages_2), value);
	}
};

struct fsResult_t862419890_StaticFields
{
public:
	// System.String[] FullSerializer.fsResult::EmptyStringArray
	StringU5BU5D_t1642385972* ___EmptyStringArray_0;
	// FullSerializer.fsResult FullSerializer.fsResult::Success
	fsResult_t862419890  ___Success_3;

public:
	inline static int32_t get_offset_of_EmptyStringArray_0() { return static_cast<int32_t>(offsetof(fsResult_t862419890_StaticFields, ___EmptyStringArray_0)); }
	inline StringU5BU5D_t1642385972* get_EmptyStringArray_0() const { return ___EmptyStringArray_0; }
	inline StringU5BU5D_t1642385972** get_address_of_EmptyStringArray_0() { return &___EmptyStringArray_0; }
	inline void set_EmptyStringArray_0(StringU5BU5D_t1642385972* value)
	{
		___EmptyStringArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyStringArray_0), value);
	}

	inline static int32_t get_offset_of_Success_3() { return static_cast<int32_t>(offsetof(fsResult_t862419890_StaticFields, ___Success_3)); }
	inline fsResult_t862419890  get_Success_3() const { return ___Success_3; }
	inline fsResult_t862419890 * get_address_of_Success_3() { return &___Success_3; }
	inline void set_Success_3(fsResult_t862419890  value)
	{
		___Success_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of FullSerializer.fsResult
struct fsResult_t862419890_marshaled_pinvoke
{
	int32_t ____success_1;
	List_1_t1398341365 * ____messages_2;
};
// Native definition for COM marshalling of FullSerializer.fsResult
struct fsResult_t862419890_marshaled_com
{
	int32_t ____success_1;
	List_1_t1398341365 * ____messages_2;
};
#endif // FSRESULT_T862419890_H
#ifndef VECTOR2_T2243707579_H
#define VECTOR2_T2243707579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2243707579 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2243707579, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2243707579, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2243707579_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2243707579  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2243707579  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2243707579  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2243707579  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2243707579  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2243707579  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2243707579  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2243707579  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2243707579  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2243707579 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2243707579  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___oneVector_3)); }
	inline Vector2_t2243707579  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2243707579 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2243707579  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___upVector_4)); }
	inline Vector2_t2243707579  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2243707579 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2243707579  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___downVector_5)); }
	inline Vector2_t2243707579  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2243707579 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2243707579  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___leftVector_6)); }
	inline Vector2_t2243707579  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2243707579 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2243707579  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___rightVector_7)); }
	inline Vector2_t2243707579  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2243707579 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2243707579  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2243707579  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2243707579 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2243707579  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2243707579  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2243707579 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2243707579  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2243707579_H
#ifndef OBJECTHITRESULT_T3057876522_H
#define OBJECTHITRESULT_T3057876522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Hit.HitTest/ObjectHitResult
struct  ObjectHitResult_t3057876522 
{
public:
	// System.Int32 TouchScript.Hit.HitTest/ObjectHitResult::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ObjectHitResult_t3057876522, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTHITRESULT_T3057876522_H
#ifndef PROJECTIONTYPE_T462923329_H
#define PROJECTIONTYPE_T462923329_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Gestures.TransformGesture/ProjectionType
struct  ProjectionType_t462923329 
{
public:
	// System.Int32 TouchScript.Gestures.TransformGesture/ProjectionType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ProjectionType_t462923329, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROJECTIONTYPE_T462923329_H
#ifndef RAYCASTHIT2D_T4063908774_H
#define RAYCASTHIT2D_T4063908774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit2D
struct  RaycastHit2D_t4063908774 
{
public:
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Centroid
	Vector2_t2243707579  ___m_Centroid_0;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Point
	Vector2_t2243707579  ___m_Point_1;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Normal
	Vector2_t2243707579  ___m_Normal_2;
	// System.Single UnityEngine.RaycastHit2D::m_Distance
	float ___m_Distance_3;
	// System.Single UnityEngine.RaycastHit2D::m_Fraction
	float ___m_Fraction_4;
	// UnityEngine.Collider2D UnityEngine.RaycastHit2D::m_Collider
	Collider2D_t646061738 * ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Centroid_0() { return static_cast<int32_t>(offsetof(RaycastHit2D_t4063908774, ___m_Centroid_0)); }
	inline Vector2_t2243707579  get_m_Centroid_0() const { return ___m_Centroid_0; }
	inline Vector2_t2243707579 * get_address_of_m_Centroid_0() { return &___m_Centroid_0; }
	inline void set_m_Centroid_0(Vector2_t2243707579  value)
	{
		___m_Centroid_0 = value;
	}

	inline static int32_t get_offset_of_m_Point_1() { return static_cast<int32_t>(offsetof(RaycastHit2D_t4063908774, ___m_Point_1)); }
	inline Vector2_t2243707579  get_m_Point_1() const { return ___m_Point_1; }
	inline Vector2_t2243707579 * get_address_of_m_Point_1() { return &___m_Point_1; }
	inline void set_m_Point_1(Vector2_t2243707579  value)
	{
		___m_Point_1 = value;
	}

	inline static int32_t get_offset_of_m_Normal_2() { return static_cast<int32_t>(offsetof(RaycastHit2D_t4063908774, ___m_Normal_2)); }
	inline Vector2_t2243707579  get_m_Normal_2() const { return ___m_Normal_2; }
	inline Vector2_t2243707579 * get_address_of_m_Normal_2() { return &___m_Normal_2; }
	inline void set_m_Normal_2(Vector2_t2243707579  value)
	{
		___m_Normal_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit2D_t4063908774, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_Fraction_4() { return static_cast<int32_t>(offsetof(RaycastHit2D_t4063908774, ___m_Fraction_4)); }
	inline float get_m_Fraction_4() const { return ___m_Fraction_4; }
	inline float* get_address_of_m_Fraction_4() { return &___m_Fraction_4; }
	inline void set_m_Fraction_4(float value)
	{
		___m_Fraction_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit2D_t4063908774, ___m_Collider_5)); }
	inline Collider2D_t646061738 * get_m_Collider_5() const { return ___m_Collider_5; }
	inline Collider2D_t646061738 ** get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(Collider2D_t646061738 * value)
	{
		___m_Collider_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Collider_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.RaycastHit2D
struct RaycastHit2D_t4063908774_marshaled_pinvoke
{
	Vector2_t2243707579  ___m_Centroid_0;
	Vector2_t2243707579  ___m_Point_1;
	Vector2_t2243707579  ___m_Normal_2;
	float ___m_Distance_3;
	float ___m_Fraction_4;
	Collider2D_t646061738 * ___m_Collider_5;
};
// Native definition for COM marshalling of UnityEngine.RaycastHit2D
struct RaycastHit2D_t4063908774_marshaled_com
{
	Vector2_t2243707579  ___m_Centroid_0;
	Vector2_t2243707579  ___m_Point_1;
	Vector2_t2243707579  ___m_Normal_2;
	float ___m_Distance_3;
	float ___m_Fraction_4;
	Collider2D_t646061738 * ___m_Collider_5;
};
#endif // RAYCASTHIT2D_T4063908774_H
#ifndef MOVEDIRECTION_T1406276862_H
#define MOVEDIRECTION_T1406276862_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.MoveDirection
struct  MoveDirection_t1406276862 
{
public:
	// System.Int32 UnityEngine.EventSystems.MoveDirection::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MoveDirection_t1406276862, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVEDIRECTION_T1406276862_H
#ifndef PROJECTIONTYPE_T510080709_H
#define PROJECTIONTYPE_T510080709_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Gestures.PinnedTransformGesture/ProjectionType
struct  ProjectionType_t510080709 
{
public:
	// System.Int32 TouchScript.Gestures.PinnedTransformGesture/ProjectionType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ProjectionType_t510080709, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROJECTIONTYPE_T510080709_H
#ifndef RAYCASTRESULT_T21186376_H
#define RAYCASTRESULT_T21186376_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.RaycastResult
struct  RaycastResult_t21186376 
{
public:
	// UnityEngine.GameObject UnityEngine.EventSystems.RaycastResult::m_GameObject
	GameObject_t1756533147 * ___m_GameObject_0;
	// UnityEngine.EventSystems.BaseRaycaster UnityEngine.EventSystems.RaycastResult::module
	BaseRaycaster_t2336171397 * ___module_1;
	// System.Single UnityEngine.EventSystems.RaycastResult::distance
	float ___distance_2;
	// System.Single UnityEngine.EventSystems.RaycastResult::index
	float ___index_3;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::depth
	int32_t ___depth_4;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::sortingLayer
	int32_t ___sortingLayer_5;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::sortingOrder
	int32_t ___sortingOrder_6;
	// UnityEngine.Vector3 UnityEngine.EventSystems.RaycastResult::worldPosition
	Vector3_t2243707580  ___worldPosition_7;
	// UnityEngine.Vector3 UnityEngine.EventSystems.RaycastResult::worldNormal
	Vector3_t2243707580  ___worldNormal_8;
	// UnityEngine.Vector2 UnityEngine.EventSystems.RaycastResult::screenPosition
	Vector2_t2243707579  ___screenPosition_9;

public:
	inline static int32_t get_offset_of_m_GameObject_0() { return static_cast<int32_t>(offsetof(RaycastResult_t21186376, ___m_GameObject_0)); }
	inline GameObject_t1756533147 * get_m_GameObject_0() const { return ___m_GameObject_0; }
	inline GameObject_t1756533147 ** get_address_of_m_GameObject_0() { return &___m_GameObject_0; }
	inline void set_m_GameObject_0(GameObject_t1756533147 * value)
	{
		___m_GameObject_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_GameObject_0), value);
	}

	inline static int32_t get_offset_of_module_1() { return static_cast<int32_t>(offsetof(RaycastResult_t21186376, ___module_1)); }
	inline BaseRaycaster_t2336171397 * get_module_1() const { return ___module_1; }
	inline BaseRaycaster_t2336171397 ** get_address_of_module_1() { return &___module_1; }
	inline void set_module_1(BaseRaycaster_t2336171397 * value)
	{
		___module_1 = value;
		Il2CppCodeGenWriteBarrier((&___module_1), value);
	}

	inline static int32_t get_offset_of_distance_2() { return static_cast<int32_t>(offsetof(RaycastResult_t21186376, ___distance_2)); }
	inline float get_distance_2() const { return ___distance_2; }
	inline float* get_address_of_distance_2() { return &___distance_2; }
	inline void set_distance_2(float value)
	{
		___distance_2 = value;
	}

	inline static int32_t get_offset_of_index_3() { return static_cast<int32_t>(offsetof(RaycastResult_t21186376, ___index_3)); }
	inline float get_index_3() const { return ___index_3; }
	inline float* get_address_of_index_3() { return &___index_3; }
	inline void set_index_3(float value)
	{
		___index_3 = value;
	}

	inline static int32_t get_offset_of_depth_4() { return static_cast<int32_t>(offsetof(RaycastResult_t21186376, ___depth_4)); }
	inline int32_t get_depth_4() const { return ___depth_4; }
	inline int32_t* get_address_of_depth_4() { return &___depth_4; }
	inline void set_depth_4(int32_t value)
	{
		___depth_4 = value;
	}

	inline static int32_t get_offset_of_sortingLayer_5() { return static_cast<int32_t>(offsetof(RaycastResult_t21186376, ___sortingLayer_5)); }
	inline int32_t get_sortingLayer_5() const { return ___sortingLayer_5; }
	inline int32_t* get_address_of_sortingLayer_5() { return &___sortingLayer_5; }
	inline void set_sortingLayer_5(int32_t value)
	{
		___sortingLayer_5 = value;
	}

	inline static int32_t get_offset_of_sortingOrder_6() { return static_cast<int32_t>(offsetof(RaycastResult_t21186376, ___sortingOrder_6)); }
	inline int32_t get_sortingOrder_6() const { return ___sortingOrder_6; }
	inline int32_t* get_address_of_sortingOrder_6() { return &___sortingOrder_6; }
	inline void set_sortingOrder_6(int32_t value)
	{
		___sortingOrder_6 = value;
	}

	inline static int32_t get_offset_of_worldPosition_7() { return static_cast<int32_t>(offsetof(RaycastResult_t21186376, ___worldPosition_7)); }
	inline Vector3_t2243707580  get_worldPosition_7() const { return ___worldPosition_7; }
	inline Vector3_t2243707580 * get_address_of_worldPosition_7() { return &___worldPosition_7; }
	inline void set_worldPosition_7(Vector3_t2243707580  value)
	{
		___worldPosition_7 = value;
	}

	inline static int32_t get_offset_of_worldNormal_8() { return static_cast<int32_t>(offsetof(RaycastResult_t21186376, ___worldNormal_8)); }
	inline Vector3_t2243707580  get_worldNormal_8() const { return ___worldNormal_8; }
	inline Vector3_t2243707580 * get_address_of_worldNormal_8() { return &___worldNormal_8; }
	inline void set_worldNormal_8(Vector3_t2243707580  value)
	{
		___worldNormal_8 = value;
	}

	inline static int32_t get_offset_of_screenPosition_9() { return static_cast<int32_t>(offsetof(RaycastResult_t21186376, ___screenPosition_9)); }
	inline Vector2_t2243707579  get_screenPosition_9() const { return ___screenPosition_9; }
	inline Vector2_t2243707579 * get_address_of_screenPosition_9() { return &___screenPosition_9; }
	inline void set_screenPosition_9(Vector2_t2243707579  value)
	{
		___screenPosition_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.EventSystems.RaycastResult
struct RaycastResult_t21186376_marshaled_pinvoke
{
	GameObject_t1756533147 * ___m_GameObject_0;
	BaseRaycaster_t2336171397 * ___module_1;
	float ___distance_2;
	float ___index_3;
	int32_t ___depth_4;
	int32_t ___sortingLayer_5;
	int32_t ___sortingOrder_6;
	Vector3_t2243707580  ___worldPosition_7;
	Vector3_t2243707580  ___worldNormal_8;
	Vector2_t2243707579  ___screenPosition_9;
};
// Native definition for COM marshalling of UnityEngine.EventSystems.RaycastResult
struct RaycastResult_t21186376_marshaled_com
{
	GameObject_t1756533147 * ___m_GameObject_0;
	BaseRaycaster_t2336171397 * ___module_1;
	float ___distance_2;
	float ___index_3;
	int32_t ___depth_4;
	int32_t ___sortingLayer_5;
	int32_t ___sortingOrder_6;
	Vector3_t2243707580  ___worldPosition_7;
	Vector3_t2243707580  ___worldNormal_8;
	Vector2_t2243707579  ___screenPosition_9;
};
#endif // RAYCASTRESULT_T21186376_H
#ifndef TOUCHPHASE_T2458120420_H
#define TOUCHPHASE_T2458120420_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchPhase
struct  TouchPhase_t2458120420 
{
public:
	// System.Int32 UnityEngine.TouchPhase::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchPhase_t2458120420, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHPHASE_T2458120420_H
#ifndef PLANE_T3727654732_H
#define PLANE_T3727654732_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Plane
struct  Plane_t3727654732 
{
public:
	// UnityEngine.Vector3 UnityEngine.Plane::m_Normal
	Vector3_t2243707580  ___m_Normal_0;
	// System.Single UnityEngine.Plane::m_Distance
	float ___m_Distance_1;

public:
	inline static int32_t get_offset_of_m_Normal_0() { return static_cast<int32_t>(offsetof(Plane_t3727654732, ___m_Normal_0)); }
	inline Vector3_t2243707580  get_m_Normal_0() const { return ___m_Normal_0; }
	inline Vector3_t2243707580 * get_address_of_m_Normal_0() { return &___m_Normal_0; }
	inline void set_m_Normal_0(Vector3_t2243707580  value)
	{
		___m_Normal_0 = value;
	}

	inline static int32_t get_offset_of_m_Distance_1() { return static_cast<int32_t>(offsetof(Plane_t3727654732, ___m_Distance_1)); }
	inline float get_m_Distance_1() const { return ___m_Distance_1; }
	inline float* get_address_of_m_Distance_1() { return &___m_Distance_1; }
	inline void set_m_Distance_1(float value)
	{
		___m_Distance_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLANE_T3727654732_H
#ifndef OBJECT_T1021602117_H
#define OBJECT_T1021602117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1021602117  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	IntPtr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1021602117, ___m_CachedPtr_0)); }
	inline IntPtr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline IntPtr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(IntPtr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1021602117_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1021602117_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1021602117_H
#ifndef RAYCASTHIT_T87180320_H
#define RAYCASTHIT_T87180320_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit
struct  RaycastHit_t87180320 
{
public:
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_t2243707580  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_t2243707580  ___m_Normal_1;
	// System.Int32 UnityEngine.RaycastHit::m_FaceID
	int32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_t2243707579  ___m_UV_4;
	// UnityEngine.Collider UnityEngine.RaycastHit::m_Collider
	Collider_t3497673348 * ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(RaycastHit_t87180320, ___m_Point_0)); }
	inline Vector3_t2243707580  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_t2243707580 * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_t2243707580  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(RaycastHit_t87180320, ___m_Normal_1)); }
	inline Vector3_t2243707580  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_t2243707580 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_t2243707580  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_FaceID_2() { return static_cast<int32_t>(offsetof(RaycastHit_t87180320, ___m_FaceID_2)); }
	inline int32_t get_m_FaceID_2() const { return ___m_FaceID_2; }
	inline int32_t* get_address_of_m_FaceID_2() { return &___m_FaceID_2; }
	inline void set_m_FaceID_2(int32_t value)
	{
		___m_FaceID_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit_t87180320, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_UV_4() { return static_cast<int32_t>(offsetof(RaycastHit_t87180320, ___m_UV_4)); }
	inline Vector2_t2243707579  get_m_UV_4() const { return ___m_UV_4; }
	inline Vector2_t2243707579 * get_address_of_m_UV_4() { return &___m_UV_4; }
	inline void set_m_UV_4(Vector2_t2243707579  value)
	{
		___m_UV_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit_t87180320, ___m_Collider_5)); }
	inline Collider_t3497673348 * get_m_Collider_5() const { return ___m_Collider_5; }
	inline Collider_t3497673348 ** get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(Collider_t3497673348 * value)
	{
		___m_Collider_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Collider_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.RaycastHit
struct RaycastHit_t87180320_marshaled_pinvoke
{
	Vector3_t2243707580  ___m_Point_0;
	Vector3_t2243707580  ___m_Normal_1;
	int32_t ___m_FaceID_2;
	float ___m_Distance_3;
	Vector2_t2243707579  ___m_UV_4;
	Collider_t3497673348 * ___m_Collider_5;
};
// Native definition for COM marshalling of UnityEngine.RaycastHit
struct RaycastHit_t87180320_marshaled_com
{
	Vector3_t2243707580  ___m_Point_0;
	Vector3_t2243707580  ___m_Normal_1;
	int32_t ___m_FaceID_2;
	float ___m_Distance_3;
	Vector2_t2243707579  ___m_UV_4;
	Collider_t3497673348 * ___m_Collider_5;
};
#endif // RAYCASTHIT_T87180320_H
#ifndef TOUCHHITTYPE_T1472696400_H
#define TOUCHHITTYPE_T1472696400_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Hit.TouchHit/TouchHitType
struct  TouchHitType_t1472696400 
{
public:
	// System.Int32 TouchScript.Hit.TouchHit/TouchHitType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchHitType_t1472696400, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHHITTYPE_T1472696400_H
#ifndef MOUSEHANDLER_T3116661769_H
#define MOUSEHANDLER_T3116661769_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.InputSources.InputHandlers.MouseHandler
struct  MouseHandler_t3116661769  : public RuntimeObject
{
public:
	// System.Func`4<UnityEngine.Vector2,TouchScript.Tags,System.Boolean,TouchScript.TouchPoint> TouchScript.InputSources.InputHandlers.MouseHandler::beginTouch
	Func_4_t1475708928 * ___beginTouch_0;
	// System.Action`2<System.Int32,UnityEngine.Vector2> TouchScript.InputSources.InputHandlers.MouseHandler::moveTouch
	Action_2_t1542075644 * ___moveTouch_1;
	// System.Action`1<System.Int32> TouchScript.InputSources.InputHandlers.MouseHandler::endTouch
	Action_1_t1873676830 * ___endTouch_2;
	// System.Action`1<System.Int32> TouchScript.InputSources.InputHandlers.MouseHandler::cancelTouch
	Action_1_t1873676830 * ___cancelTouch_3;
	// TouchScript.Tags TouchScript.InputSources.InputHandlers.MouseHandler::tags
	Tags_t1265380163 * ___tags_4;
	// System.Int32 TouchScript.InputSources.InputHandlers.MouseHandler::mousePointId
	int32_t ___mousePointId_5;
	// System.Int32 TouchScript.InputSources.InputHandlers.MouseHandler::fakeMousePointId
	int32_t ___fakeMousePointId_6;
	// UnityEngine.Vector3 TouchScript.InputSources.InputHandlers.MouseHandler::mousePointPos
	Vector3_t2243707580  ___mousePointPos_7;

public:
	inline static int32_t get_offset_of_beginTouch_0() { return static_cast<int32_t>(offsetof(MouseHandler_t3116661769, ___beginTouch_0)); }
	inline Func_4_t1475708928 * get_beginTouch_0() const { return ___beginTouch_0; }
	inline Func_4_t1475708928 ** get_address_of_beginTouch_0() { return &___beginTouch_0; }
	inline void set_beginTouch_0(Func_4_t1475708928 * value)
	{
		___beginTouch_0 = value;
		Il2CppCodeGenWriteBarrier((&___beginTouch_0), value);
	}

	inline static int32_t get_offset_of_moveTouch_1() { return static_cast<int32_t>(offsetof(MouseHandler_t3116661769, ___moveTouch_1)); }
	inline Action_2_t1542075644 * get_moveTouch_1() const { return ___moveTouch_1; }
	inline Action_2_t1542075644 ** get_address_of_moveTouch_1() { return &___moveTouch_1; }
	inline void set_moveTouch_1(Action_2_t1542075644 * value)
	{
		___moveTouch_1 = value;
		Il2CppCodeGenWriteBarrier((&___moveTouch_1), value);
	}

	inline static int32_t get_offset_of_endTouch_2() { return static_cast<int32_t>(offsetof(MouseHandler_t3116661769, ___endTouch_2)); }
	inline Action_1_t1873676830 * get_endTouch_2() const { return ___endTouch_2; }
	inline Action_1_t1873676830 ** get_address_of_endTouch_2() { return &___endTouch_2; }
	inline void set_endTouch_2(Action_1_t1873676830 * value)
	{
		___endTouch_2 = value;
		Il2CppCodeGenWriteBarrier((&___endTouch_2), value);
	}

	inline static int32_t get_offset_of_cancelTouch_3() { return static_cast<int32_t>(offsetof(MouseHandler_t3116661769, ___cancelTouch_3)); }
	inline Action_1_t1873676830 * get_cancelTouch_3() const { return ___cancelTouch_3; }
	inline Action_1_t1873676830 ** get_address_of_cancelTouch_3() { return &___cancelTouch_3; }
	inline void set_cancelTouch_3(Action_1_t1873676830 * value)
	{
		___cancelTouch_3 = value;
		Il2CppCodeGenWriteBarrier((&___cancelTouch_3), value);
	}

	inline static int32_t get_offset_of_tags_4() { return static_cast<int32_t>(offsetof(MouseHandler_t3116661769, ___tags_4)); }
	inline Tags_t1265380163 * get_tags_4() const { return ___tags_4; }
	inline Tags_t1265380163 ** get_address_of_tags_4() { return &___tags_4; }
	inline void set_tags_4(Tags_t1265380163 * value)
	{
		___tags_4 = value;
		Il2CppCodeGenWriteBarrier((&___tags_4), value);
	}

	inline static int32_t get_offset_of_mousePointId_5() { return static_cast<int32_t>(offsetof(MouseHandler_t3116661769, ___mousePointId_5)); }
	inline int32_t get_mousePointId_5() const { return ___mousePointId_5; }
	inline int32_t* get_address_of_mousePointId_5() { return &___mousePointId_5; }
	inline void set_mousePointId_5(int32_t value)
	{
		___mousePointId_5 = value;
	}

	inline static int32_t get_offset_of_fakeMousePointId_6() { return static_cast<int32_t>(offsetof(MouseHandler_t3116661769, ___fakeMousePointId_6)); }
	inline int32_t get_fakeMousePointId_6() const { return ___fakeMousePointId_6; }
	inline int32_t* get_address_of_fakeMousePointId_6() { return &___fakeMousePointId_6; }
	inline void set_fakeMousePointId_6(int32_t value)
	{
		___fakeMousePointId_6 = value;
	}

	inline static int32_t get_offset_of_mousePointPos_7() { return static_cast<int32_t>(offsetof(MouseHandler_t3116661769, ___mousePointPos_7)); }
	inline Vector3_t2243707580  get_mousePointPos_7() const { return ___mousePointPos_7; }
	inline Vector3_t2243707580 * get_address_of_mousePointPos_7() { return &___mousePointPos_7; }
	inline void set_mousePointPos_7(Vector3_t2243707580  value)
	{
		___mousePointPos_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSEHANDLER_T3116661769_H
#ifndef BINDINGFLAGS_T1082350898_H
#define BINDINGFLAGS_T1082350898_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_t1082350898 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BindingFlags_t1082350898, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_T1082350898_H
#ifndef TOUCHESNUMSTATE_T594133898_H
#define TOUCHESNUMSTATE_T594133898_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Gestures.Gesture/TouchesNumState
struct  TouchesNumState_t594133898 
{
public:
	// System.Int32 TouchScript.Gestures.Gesture/TouchesNumState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchesNumState_t594133898, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHESNUMSTATE_T594133898_H
#ifndef GESTUREDIRECTION_T1726119045_H
#define GESTUREDIRECTION_T1726119045_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Gestures.FlickGesture/GestureDirection
struct  GestureDirection_t1726119045 
{
public:
	// System.Int32 TouchScript.Gestures.FlickGesture/GestureDirection::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GestureDirection_t1726119045, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GESTUREDIRECTION_T1726119045_H
#ifndef TRANSFORMTYPE_T94296928_H
#define TRANSFORMTYPE_T94296928_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Gestures.Base.TransformGestureBase/TransformType
struct  TransformType_t94296928 
{
public:
	// System.Int32 TouchScript.Gestures.Base.TransformGestureBase/TransformType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TransformType_t94296928, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMTYPE_T94296928_H
#ifndef FSMETATYPE_T3266798926_H
#define FSMETATYPE_T3266798926_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsMetaType
struct  fsMetaType_t3266798926  : public RuntimeObject
{
public:
	// System.Type FullSerializer.fsMetaType::ReflectedType
	Type_t * ___ReflectedType_1;
	// System.Boolean FullSerializer.fsMetaType::_hasEmittedAotData
	bool ____hasEmittedAotData_2;
	// FullSerializer.Internal.fsMetaProperty[] FullSerializer.fsMetaType::<Properties>k__BackingField
	fsMetaPropertyU5BU5D_t4057973332* ___U3CPropertiesU3Ek__BackingField_3;
	// System.Nullable`1<System.Boolean> FullSerializer.fsMetaType::_hasDefaultConstructorCache
	Nullable_1_t2088641033  ____hasDefaultConstructorCache_4;
	// System.Boolean FullSerializer.fsMetaType::_isDefaultConstructorPublic
	bool ____isDefaultConstructorPublic_5;

public:
	inline static int32_t get_offset_of_ReflectedType_1() { return static_cast<int32_t>(offsetof(fsMetaType_t3266798926, ___ReflectedType_1)); }
	inline Type_t * get_ReflectedType_1() const { return ___ReflectedType_1; }
	inline Type_t ** get_address_of_ReflectedType_1() { return &___ReflectedType_1; }
	inline void set_ReflectedType_1(Type_t * value)
	{
		___ReflectedType_1 = value;
		Il2CppCodeGenWriteBarrier((&___ReflectedType_1), value);
	}

	inline static int32_t get_offset_of__hasEmittedAotData_2() { return static_cast<int32_t>(offsetof(fsMetaType_t3266798926, ____hasEmittedAotData_2)); }
	inline bool get__hasEmittedAotData_2() const { return ____hasEmittedAotData_2; }
	inline bool* get_address_of__hasEmittedAotData_2() { return &____hasEmittedAotData_2; }
	inline void set__hasEmittedAotData_2(bool value)
	{
		____hasEmittedAotData_2 = value;
	}

	inline static int32_t get_offset_of_U3CPropertiesU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(fsMetaType_t3266798926, ___U3CPropertiesU3Ek__BackingField_3)); }
	inline fsMetaPropertyU5BU5D_t4057973332* get_U3CPropertiesU3Ek__BackingField_3() const { return ___U3CPropertiesU3Ek__BackingField_3; }
	inline fsMetaPropertyU5BU5D_t4057973332** get_address_of_U3CPropertiesU3Ek__BackingField_3() { return &___U3CPropertiesU3Ek__BackingField_3; }
	inline void set_U3CPropertiesU3Ek__BackingField_3(fsMetaPropertyU5BU5D_t4057973332* value)
	{
		___U3CPropertiesU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPropertiesU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of__hasDefaultConstructorCache_4() { return static_cast<int32_t>(offsetof(fsMetaType_t3266798926, ____hasDefaultConstructorCache_4)); }
	inline Nullable_1_t2088641033  get__hasDefaultConstructorCache_4() const { return ____hasDefaultConstructorCache_4; }
	inline Nullable_1_t2088641033 * get_address_of__hasDefaultConstructorCache_4() { return &____hasDefaultConstructorCache_4; }
	inline void set__hasDefaultConstructorCache_4(Nullable_1_t2088641033  value)
	{
		____hasDefaultConstructorCache_4 = value;
	}

	inline static int32_t get_offset_of__isDefaultConstructorPublic_5() { return static_cast<int32_t>(offsetof(fsMetaType_t3266798926, ____isDefaultConstructorPublic_5)); }
	inline bool get__isDefaultConstructorPublic_5() const { return ____isDefaultConstructorPublic_5; }
	inline bool* get_address_of__isDefaultConstructorPublic_5() { return &____isDefaultConstructorPublic_5; }
	inline void set__isDefaultConstructorPublic_5(bool value)
	{
		____isDefaultConstructorPublic_5 = value;
	}
};

struct fsMetaType_t3266798926_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,FullSerializer.fsMetaType> FullSerializer.fsMetaType::_metaTypes
	Dictionary_2_t909189527 * ____metaTypes_0;

public:
	inline static int32_t get_offset_of__metaTypes_0() { return static_cast<int32_t>(offsetof(fsMetaType_t3266798926_StaticFields, ____metaTypes_0)); }
	inline Dictionary_2_t909189527 * get__metaTypes_0() const { return ____metaTypes_0; }
	inline Dictionary_2_t909189527 ** get_address_of__metaTypes_0() { return &____metaTypes_0; }
	inline void set__metaTypes_0(Dictionary_2_t909189527 * value)
	{
		____metaTypes_0 = value;
		Il2CppCodeGenWriteBarrier((&____metaTypes_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSMETATYPE_T3266798926_H
#ifndef INPUTTYPE_T948942308_H
#define INPUTTYPE_T948942308_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.InputSources.TuioInput/InputType
struct  InputType_t948942308 
{
public:
	// System.Int32 TouchScript.InputSources.TuioInput/InputType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InputType_t948942308, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTTYPE_T948942308_H
#ifndef TOKEN_T2182318091_H
#define TOKEN_T2182318091_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiniJSON.Json/Parser/TOKEN
struct  TOKEN_t2182318091 
{
public:
	// System.Int32 MiniJSON.Json/Parser/TOKEN::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TOKEN_t2182318091, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOKEN_T2182318091_H
#ifndef FSDATATYPE_T1645355485_H
#define FSDATATYPE_T1645355485_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsDataType
struct  fsDataType_t1645355485 
{
public:
	// System.Int32 FullSerializer.fsDataType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(fsDataType_t1645355485, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSDATATYPE_T1645355485_H
#ifndef GESTURESTATE_T2128095272_H
#define GESTURESTATE_T2128095272_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Gestures.Gesture/GestureState
struct  GestureState_t2128095272 
{
public:
	// System.Int32 TouchScript.Gestures.Gesture/GestureState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GestureState_t2128095272, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GESTURESTATE_T2128095272_H
#ifndef FSMEMBERSERIALIZATION_T691367231_H
#define FSMEMBERSERIALIZATION_T691367231_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsMemberSerialization
struct  fsMemberSerialization_t691367231 
{
public:
	// System.Int32 FullSerializer.fsMemberSerialization::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(fsMemberSerialization_t691367231, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSMEMBERSERIALIZATION_T691367231_H
#ifndef TRANSFORMTYPE_T724632304_H
#define TRANSFORMTYPE_T724632304_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Gestures.Base.PinnedTrasformGestureBase/TransformType
struct  TransformType_t724632304 
{
public:
	// System.Int32 TouchScript.Gestures.Base.PinnedTrasformGestureBase/TransformType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TransformType_t724632304, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMTYPE_T724632304_H
#ifndef SCRIPTABLEOBJECT_T1975622470_H
#define SCRIPTABLEOBJECT_T1975622470_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t1975622470  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t1975622470_marshaled_pinvoke : public Object_t1021602117_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t1975622470_marshaled_com : public Object_t1021602117_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T1975622470_H
#ifndef GESTURESTATECHANGEEVENTARGS_T3499981191_H
#define GESTURESTATECHANGEEVENTARGS_T3499981191_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Gestures.GestureStateChangeEventArgs
struct  GestureStateChangeEventArgs_t3499981191  : public EventArgs_t3289624707
{
public:
	// TouchScript.Gestures.Gesture/GestureState TouchScript.Gestures.GestureStateChangeEventArgs::<PreviousState>k__BackingField
	int32_t ___U3CPreviousStateU3Ek__BackingField_1;
	// TouchScript.Gestures.Gesture/GestureState TouchScript.Gestures.GestureStateChangeEventArgs::<State>k__BackingField
	int32_t ___U3CStateU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CPreviousStateU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(GestureStateChangeEventArgs_t3499981191, ___U3CPreviousStateU3Ek__BackingField_1)); }
	inline int32_t get_U3CPreviousStateU3Ek__BackingField_1() const { return ___U3CPreviousStateU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CPreviousStateU3Ek__BackingField_1() { return &___U3CPreviousStateU3Ek__BackingField_1; }
	inline void set_U3CPreviousStateU3Ek__BackingField_1(int32_t value)
	{
		___U3CPreviousStateU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CStateU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(GestureStateChangeEventArgs_t3499981191, ___U3CStateU3Ek__BackingField_2)); }
	inline int32_t get_U3CStateU3Ek__BackingField_2() const { return ___U3CStateU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CStateU3Ek__BackingField_2() { return &___U3CStateU3Ek__BackingField_2; }
	inline void set_U3CStateU3Ek__BackingField_2(int32_t value)
	{
		___U3CStateU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GESTURESTATECHANGEEVENTARGS_T3499981191_H
#ifndef TOUCHHIT_T4186847494_H
#define TOUCHHIT_T4186847494_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Hit.TouchHit
struct  TouchHit_t4186847494 
{
public:
	// TouchScript.Hit.TouchHit/TouchHitType TouchScript.Hit.TouchHit::type
	int32_t ___type_0;
	// UnityEngine.Transform TouchScript.Hit.TouchHit::transform
	Transform_t3275118058 * ___transform_1;
	// UnityEngine.RaycastHit TouchScript.Hit.TouchHit::raycastHit
	RaycastHit_t87180320  ___raycastHit_2;
	// UnityEngine.RaycastHit2D TouchScript.Hit.TouchHit::raycastHit2D
	RaycastHit2D_t4063908774  ___raycastHit2D_3;
	// UnityEngine.EventSystems.RaycastResult TouchScript.Hit.TouchHit::raycastResult
	RaycastResult_t21186376  ___raycastResult_4;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(TouchHit_t4186847494, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_transform_1() { return static_cast<int32_t>(offsetof(TouchHit_t4186847494, ___transform_1)); }
	inline Transform_t3275118058 * get_transform_1() const { return ___transform_1; }
	inline Transform_t3275118058 ** get_address_of_transform_1() { return &___transform_1; }
	inline void set_transform_1(Transform_t3275118058 * value)
	{
		___transform_1 = value;
		Il2CppCodeGenWriteBarrier((&___transform_1), value);
	}

	inline static int32_t get_offset_of_raycastHit_2() { return static_cast<int32_t>(offsetof(TouchHit_t4186847494, ___raycastHit_2)); }
	inline RaycastHit_t87180320  get_raycastHit_2() const { return ___raycastHit_2; }
	inline RaycastHit_t87180320 * get_address_of_raycastHit_2() { return &___raycastHit_2; }
	inline void set_raycastHit_2(RaycastHit_t87180320  value)
	{
		___raycastHit_2 = value;
	}

	inline static int32_t get_offset_of_raycastHit2D_3() { return static_cast<int32_t>(offsetof(TouchHit_t4186847494, ___raycastHit2D_3)); }
	inline RaycastHit2D_t4063908774  get_raycastHit2D_3() const { return ___raycastHit2D_3; }
	inline RaycastHit2D_t4063908774 * get_address_of_raycastHit2D_3() { return &___raycastHit2D_3; }
	inline void set_raycastHit2D_3(RaycastHit2D_t4063908774  value)
	{
		___raycastHit2D_3 = value;
	}

	inline static int32_t get_offset_of_raycastResult_4() { return static_cast<int32_t>(offsetof(TouchHit_t4186847494, ___raycastResult_4)); }
	inline RaycastResult_t21186376  get_raycastResult_4() const { return ___raycastResult_4; }
	inline RaycastResult_t21186376 * get_address_of_raycastResult_4() { return &___raycastResult_4; }
	inline void set_raycastResult_4(RaycastResult_t21186376  value)
	{
		___raycastResult_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TouchScript.Hit.TouchHit
struct TouchHit_t4186847494_marshaled_pinvoke
{
	int32_t ___type_0;
	Transform_t3275118058 * ___transform_1;
	RaycastHit_t87180320_marshaled_pinvoke ___raycastHit_2;
	RaycastHit2D_t4063908774_marshaled_pinvoke ___raycastHit2D_3;
	RaycastResult_t21186376_marshaled_pinvoke ___raycastResult_4;
};
// Native definition for COM marshalling of TouchScript.Hit.TouchHit
struct TouchHit_t4186847494_marshaled_com
{
	int32_t ___type_0;
	Transform_t3275118058 * ___transform_1;
	RaycastHit_t87180320_marshaled_com ___raycastHit_2;
	RaycastHit2D_t4063908774_marshaled_com ___raycastHit2D_3;
	RaycastResult_t21186376_marshaled_com ___raycastResult_4;
};
#endif // TOUCHHIT_T4186847494_H
#ifndef TOUCHSTATE_T2732082299_H
#define TOUCHSTATE_T2732082299_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.InputSources.InputHandlers.TouchHandler/TouchState
struct  TouchState_t2732082299 
{
public:
	// System.Int32 TouchScript.InputSources.InputHandlers.TouchHandler/TouchState::Id
	int32_t ___Id_0;
	// UnityEngine.TouchPhase TouchScript.InputSources.InputHandlers.TouchHandler/TouchState::Phase
	int32_t ___Phase_1;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(TouchState_t2732082299, ___Id_0)); }
	inline int32_t get_Id_0() const { return ___Id_0; }
	inline int32_t* get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(int32_t value)
	{
		___Id_0 = value;
	}

	inline static int32_t get_offset_of_Phase_1() { return static_cast<int32_t>(offsetof(TouchState_t2732082299, ___Phase_1)); }
	inline int32_t get_Phase_1() const { return ___Phase_1; }
	inline int32_t* get_address_of_Phase_1() { return &___Phase_1; }
	inline void set_Phase_1(int32_t value)
	{
		___Phase_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHSTATE_T2732082299_H
#ifndef FSPORTABLEREFLECTION_T2923195415_H
#define FSPORTABLEREFLECTION_T2923195415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.Internal.fsPortableReflection
struct  fsPortableReflection_t2923195415  : public RuntimeObject
{
public:

public:
};

struct fsPortableReflection_t2923195415_StaticFields
{
public:
	// System.Type[] FullSerializer.Internal.fsPortableReflection::EmptyTypes
	TypeU5BU5D_t1664964607* ___EmptyTypes_0;
	// System.Collections.Generic.IDictionary`2<FullSerializer.Internal.fsPortableReflection/AttributeQuery,System.Attribute> FullSerializer.Internal.fsPortableReflection::_cachedAttributeQueries
	RuntimeObject* ____cachedAttributeQueries_1;
	// System.Reflection.BindingFlags FullSerializer.Internal.fsPortableReflection::DeclaredFlags
	int32_t ___DeclaredFlags_2;

public:
	inline static int32_t get_offset_of_EmptyTypes_0() { return static_cast<int32_t>(offsetof(fsPortableReflection_t2923195415_StaticFields, ___EmptyTypes_0)); }
	inline TypeU5BU5D_t1664964607* get_EmptyTypes_0() const { return ___EmptyTypes_0; }
	inline TypeU5BU5D_t1664964607** get_address_of_EmptyTypes_0() { return &___EmptyTypes_0; }
	inline void set_EmptyTypes_0(TypeU5BU5D_t1664964607* value)
	{
		___EmptyTypes_0 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_0), value);
	}

	inline static int32_t get_offset_of__cachedAttributeQueries_1() { return static_cast<int32_t>(offsetof(fsPortableReflection_t2923195415_StaticFields, ____cachedAttributeQueries_1)); }
	inline RuntimeObject* get__cachedAttributeQueries_1() const { return ____cachedAttributeQueries_1; }
	inline RuntimeObject** get_address_of__cachedAttributeQueries_1() { return &____cachedAttributeQueries_1; }
	inline void set__cachedAttributeQueries_1(RuntimeObject* value)
	{
		____cachedAttributeQueries_1 = value;
		Il2CppCodeGenWriteBarrier((&____cachedAttributeQueries_1), value);
	}

	inline static int32_t get_offset_of_DeclaredFlags_2() { return static_cast<int32_t>(offsetof(fsPortableReflection_t2923195415_StaticFields, ___DeclaredFlags_2)); }
	inline int32_t get_DeclaredFlags_2() const { return ___DeclaredFlags_2; }
	inline int32_t* get_address_of_DeclaredFlags_2() { return &___DeclaredFlags_2; }
	inline void set_DeclaredFlags_2(int32_t value)
	{
		___DeclaredFlags_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSPORTABLEREFLECTION_T2923195415_H
#ifndef COMPONENT_T3819376471_H
#define COMPONENT_T3819376471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t3819376471  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T3819376471_H
#ifndef FSOBJECTATTRIBUTE_T2382840370_H
#define FSOBJECTATTRIBUTE_T2382840370_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsObjectAttribute
struct  fsObjectAttribute_t2382840370  : public Attribute_t542643598
{
public:
	// System.Type[] FullSerializer.fsObjectAttribute::PreviousModels
	TypeU5BU5D_t1664964607* ___PreviousModels_0;
	// System.String FullSerializer.fsObjectAttribute::VersionString
	String_t* ___VersionString_1;
	// FullSerializer.fsMemberSerialization FullSerializer.fsObjectAttribute::MemberSerialization
	int32_t ___MemberSerialization_2;
	// System.Type FullSerializer.fsObjectAttribute::Converter
	Type_t * ___Converter_3;
	// System.Type FullSerializer.fsObjectAttribute::Processor
	Type_t * ___Processor_4;

public:
	inline static int32_t get_offset_of_PreviousModels_0() { return static_cast<int32_t>(offsetof(fsObjectAttribute_t2382840370, ___PreviousModels_0)); }
	inline TypeU5BU5D_t1664964607* get_PreviousModels_0() const { return ___PreviousModels_0; }
	inline TypeU5BU5D_t1664964607** get_address_of_PreviousModels_0() { return &___PreviousModels_0; }
	inline void set_PreviousModels_0(TypeU5BU5D_t1664964607* value)
	{
		___PreviousModels_0 = value;
		Il2CppCodeGenWriteBarrier((&___PreviousModels_0), value);
	}

	inline static int32_t get_offset_of_VersionString_1() { return static_cast<int32_t>(offsetof(fsObjectAttribute_t2382840370, ___VersionString_1)); }
	inline String_t* get_VersionString_1() const { return ___VersionString_1; }
	inline String_t** get_address_of_VersionString_1() { return &___VersionString_1; }
	inline void set_VersionString_1(String_t* value)
	{
		___VersionString_1 = value;
		Il2CppCodeGenWriteBarrier((&___VersionString_1), value);
	}

	inline static int32_t get_offset_of_MemberSerialization_2() { return static_cast<int32_t>(offsetof(fsObjectAttribute_t2382840370, ___MemberSerialization_2)); }
	inline int32_t get_MemberSerialization_2() const { return ___MemberSerialization_2; }
	inline int32_t* get_address_of_MemberSerialization_2() { return &___MemberSerialization_2; }
	inline void set_MemberSerialization_2(int32_t value)
	{
		___MemberSerialization_2 = value;
	}

	inline static int32_t get_offset_of_Converter_3() { return static_cast<int32_t>(offsetof(fsObjectAttribute_t2382840370, ___Converter_3)); }
	inline Type_t * get_Converter_3() const { return ___Converter_3; }
	inline Type_t ** get_address_of_Converter_3() { return &___Converter_3; }
	inline void set_Converter_3(Type_t * value)
	{
		___Converter_3 = value;
		Il2CppCodeGenWriteBarrier((&___Converter_3), value);
	}

	inline static int32_t get_offset_of_Processor_4() { return static_cast<int32_t>(offsetof(fsObjectAttribute_t2382840370, ___Processor_4)); }
	inline Type_t * get_Processor_4() const { return ___Processor_4; }
	inline Type_t ** get_address_of_Processor_4() { return &___Processor_4; }
	inline void set_Processor_4(Type_t * value)
	{
		___Processor_4 = value;
		Il2CppCodeGenWriteBarrier((&___Processor_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSOBJECTATTRIBUTE_T2382840370_H
#ifndef BEHAVIOUR_T955675639_H
#define BEHAVIOUR_T955675639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t955675639  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T955675639_H
#ifndef DISPLAYDEVICE_T1478475236_H
#define DISPLAYDEVICE_T1478475236_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Devices.Display.DisplayDevice
struct  DisplayDevice_t1478475236  : public ScriptableObject_t1975622470
{
public:
	// System.String TouchScript.Devices.Display.DisplayDevice::name
	String_t* ___name_2;
	// System.Single TouchScript.Devices.Display.DisplayDevice::dpi
	float ___dpi_3;

public:
	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(DisplayDevice_t1478475236, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((&___name_2), value);
	}

	inline static int32_t get_offset_of_dpi_3() { return static_cast<int32_t>(offsetof(DisplayDevice_t1478475236, ___dpi_3)); }
	inline float get_dpi_3() const { return ___dpi_3; }
	inline float* get_address_of_dpi_3() { return &___dpi_3; }
	inline void set_dpi_3(float value)
	{
		___dpi_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISPLAYDEVICE_T1478475236_H
#ifndef GENERICDISPLAYDEVICE_T579608737_H
#define GENERICDISPLAYDEVICE_T579608737_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Devices.Display.GenericDisplayDevice
struct  GenericDisplayDevice_t579608737  : public DisplayDevice_t1478475236
{
public:

public:
};

struct GenericDisplayDevice_t579608737_StaticFields
{
public:
	// System.Nullable`1<System.Boolean> TouchScript.Devices.Display.GenericDisplayDevice::isLaptop
	Nullable_1_t2088641033  ___isLaptop_4;

public:
	inline static int32_t get_offset_of_isLaptop_4() { return static_cast<int32_t>(offsetof(GenericDisplayDevice_t579608737_StaticFields, ___isLaptop_4)); }
	inline Nullable_1_t2088641033  get_isLaptop_4() const { return ___isLaptop_4; }
	inline Nullable_1_t2088641033 * get_address_of_isLaptop_4() { return &___isLaptop_4; }
	inline void set_isLaptop_4(Nullable_1_t2088641033  value)
	{
		___isLaptop_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICDISPLAYDEVICE_T579608737_H
#ifndef MONOBEHAVIOUR_T1158329972_H
#define MONOBEHAVIOUR_T1158329972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t1158329972  : public Behaviour_t955675639
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T1158329972_H
#ifndef DEBUGGABLEMONOBEHAVIOUR_T3136086048_H
#define DEBUGGABLEMONOBEHAVIOUR_T3136086048_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.DebuggableMonoBehaviour
struct  DebuggableMonoBehaviour_t3136086048  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGGABLEMONOBEHAVIOUR_T3136086048_H
#ifndef TOUCHPROXYBASE_T4188753234_H
#define TOUCHPROXYBASE_T4188753234_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Behaviors.Visualizer.TouchProxyBase
struct  TouchProxyBase_t4188753234  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean TouchScript.Behaviors.Visualizer.TouchProxyBase::<ShowTouchId>k__BackingField
	bool ___U3CShowTouchIdU3Ek__BackingField_2;
	// System.Boolean TouchScript.Behaviors.Visualizer.TouchProxyBase::<ShowTags>k__BackingField
	bool ___U3CShowTagsU3Ek__BackingField_3;
	// UnityEngine.RectTransform TouchScript.Behaviors.Visualizer.TouchProxyBase::rect
	RectTransform_t3349966182 * ___rect_4;
	// System.Int32 TouchScript.Behaviors.Visualizer.TouchProxyBase::size
	int32_t ___size_5;

public:
	inline static int32_t get_offset_of_U3CShowTouchIdU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(TouchProxyBase_t4188753234, ___U3CShowTouchIdU3Ek__BackingField_2)); }
	inline bool get_U3CShowTouchIdU3Ek__BackingField_2() const { return ___U3CShowTouchIdU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CShowTouchIdU3Ek__BackingField_2() { return &___U3CShowTouchIdU3Ek__BackingField_2; }
	inline void set_U3CShowTouchIdU3Ek__BackingField_2(bool value)
	{
		___U3CShowTouchIdU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CShowTagsU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(TouchProxyBase_t4188753234, ___U3CShowTagsU3Ek__BackingField_3)); }
	inline bool get_U3CShowTagsU3Ek__BackingField_3() const { return ___U3CShowTagsU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CShowTagsU3Ek__BackingField_3() { return &___U3CShowTagsU3Ek__BackingField_3; }
	inline void set_U3CShowTagsU3Ek__BackingField_3(bool value)
	{
		___U3CShowTagsU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_rect_4() { return static_cast<int32_t>(offsetof(TouchProxyBase_t4188753234, ___rect_4)); }
	inline RectTransform_t3349966182 * get_rect_4() const { return ___rect_4; }
	inline RectTransform_t3349966182 ** get_address_of_rect_4() { return &___rect_4; }
	inline void set_rect_4(RectTransform_t3349966182 * value)
	{
		___rect_4 = value;
		Il2CppCodeGenWriteBarrier((&___rect_4), value);
	}

	inline static int32_t get_offset_of_size_5() { return static_cast<int32_t>(offsetof(TouchProxyBase_t4188753234, ___size_5)); }
	inline int32_t get_size_5() const { return ___size_5; }
	inline int32_t* get_address_of_size_5() { return &___size_5; }
	inline void set_size_5(int32_t value)
	{
		___size_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHPROXYBASE_T4188753234_H
#ifndef TOUCHVISUALIZER_T2264027661_H
#define TOUCHVISUALIZER_T2264027661_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Behaviors.Visualizer.TouchVisualizer
struct  TouchVisualizer_t2264027661  : public MonoBehaviour_t1158329972
{
public:
	// TouchScript.Behaviors.Visualizer.TouchProxyBase TouchScript.Behaviors.Visualizer.TouchVisualizer::touchProxy
	TouchProxyBase_t4188753234 * ___touchProxy_2;
	// System.Boolean TouchScript.Behaviors.Visualizer.TouchVisualizer::showTouchId
	bool ___showTouchId_3;
	// System.Boolean TouchScript.Behaviors.Visualizer.TouchVisualizer::showTags
	bool ___showTags_4;
	// System.Boolean TouchScript.Behaviors.Visualizer.TouchVisualizer::useDPI
	bool ___useDPI_5;
	// System.Single TouchScript.Behaviors.Visualizer.TouchVisualizer::touchSize
	float ___touchSize_6;
	// System.Int32 TouchScript.Behaviors.Visualizer.TouchVisualizer::defaultSize
	int32_t ___defaultSize_7;
	// UnityEngine.RectTransform TouchScript.Behaviors.Visualizer.TouchVisualizer::rect
	RectTransform_t3349966182 * ___rect_8;
	// TouchScript.Utils.ObjectPool`1<TouchScript.Behaviors.Visualizer.TouchProxyBase> TouchScript.Behaviors.Visualizer.TouchVisualizer::pool
	ObjectPool_1_t3542042081 * ___pool_9;
	// System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Behaviors.Visualizer.TouchProxyBase> TouchScript.Behaviors.Visualizer.TouchVisualizer::proxies
	Dictionary_2_t3196578869 * ___proxies_10;

public:
	inline static int32_t get_offset_of_touchProxy_2() { return static_cast<int32_t>(offsetof(TouchVisualizer_t2264027661, ___touchProxy_2)); }
	inline TouchProxyBase_t4188753234 * get_touchProxy_2() const { return ___touchProxy_2; }
	inline TouchProxyBase_t4188753234 ** get_address_of_touchProxy_2() { return &___touchProxy_2; }
	inline void set_touchProxy_2(TouchProxyBase_t4188753234 * value)
	{
		___touchProxy_2 = value;
		Il2CppCodeGenWriteBarrier((&___touchProxy_2), value);
	}

	inline static int32_t get_offset_of_showTouchId_3() { return static_cast<int32_t>(offsetof(TouchVisualizer_t2264027661, ___showTouchId_3)); }
	inline bool get_showTouchId_3() const { return ___showTouchId_3; }
	inline bool* get_address_of_showTouchId_3() { return &___showTouchId_3; }
	inline void set_showTouchId_3(bool value)
	{
		___showTouchId_3 = value;
	}

	inline static int32_t get_offset_of_showTags_4() { return static_cast<int32_t>(offsetof(TouchVisualizer_t2264027661, ___showTags_4)); }
	inline bool get_showTags_4() const { return ___showTags_4; }
	inline bool* get_address_of_showTags_4() { return &___showTags_4; }
	inline void set_showTags_4(bool value)
	{
		___showTags_4 = value;
	}

	inline static int32_t get_offset_of_useDPI_5() { return static_cast<int32_t>(offsetof(TouchVisualizer_t2264027661, ___useDPI_5)); }
	inline bool get_useDPI_5() const { return ___useDPI_5; }
	inline bool* get_address_of_useDPI_5() { return &___useDPI_5; }
	inline void set_useDPI_5(bool value)
	{
		___useDPI_5 = value;
	}

	inline static int32_t get_offset_of_touchSize_6() { return static_cast<int32_t>(offsetof(TouchVisualizer_t2264027661, ___touchSize_6)); }
	inline float get_touchSize_6() const { return ___touchSize_6; }
	inline float* get_address_of_touchSize_6() { return &___touchSize_6; }
	inline void set_touchSize_6(float value)
	{
		___touchSize_6 = value;
	}

	inline static int32_t get_offset_of_defaultSize_7() { return static_cast<int32_t>(offsetof(TouchVisualizer_t2264027661, ___defaultSize_7)); }
	inline int32_t get_defaultSize_7() const { return ___defaultSize_7; }
	inline int32_t* get_address_of_defaultSize_7() { return &___defaultSize_7; }
	inline void set_defaultSize_7(int32_t value)
	{
		___defaultSize_7 = value;
	}

	inline static int32_t get_offset_of_rect_8() { return static_cast<int32_t>(offsetof(TouchVisualizer_t2264027661, ___rect_8)); }
	inline RectTransform_t3349966182 * get_rect_8() const { return ___rect_8; }
	inline RectTransform_t3349966182 ** get_address_of_rect_8() { return &___rect_8; }
	inline void set_rect_8(RectTransform_t3349966182 * value)
	{
		___rect_8 = value;
		Il2CppCodeGenWriteBarrier((&___rect_8), value);
	}

	inline static int32_t get_offset_of_pool_9() { return static_cast<int32_t>(offsetof(TouchVisualizer_t2264027661, ___pool_9)); }
	inline ObjectPool_1_t3542042081 * get_pool_9() const { return ___pool_9; }
	inline ObjectPool_1_t3542042081 ** get_address_of_pool_9() { return &___pool_9; }
	inline void set_pool_9(ObjectPool_1_t3542042081 * value)
	{
		___pool_9 = value;
		Il2CppCodeGenWriteBarrier((&___pool_9), value);
	}

	inline static int32_t get_offset_of_proxies_10() { return static_cast<int32_t>(offsetof(TouchVisualizer_t2264027661, ___proxies_10)); }
	inline Dictionary_2_t3196578869 * get_proxies_10() const { return ___proxies_10; }
	inline Dictionary_2_t3196578869 ** get_address_of_proxies_10() { return &___proxies_10; }
	inline void set_proxies_10(Dictionary_2_t3196578869 * value)
	{
		___proxies_10 = value;
		Il2CppCodeGenWriteBarrier((&___proxies_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHVISUALIZER_T2264027661_H
#ifndef GESTUREMANAGER_T1999815568_H
#define GESTUREMANAGER_T1999815568_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.GestureManager
struct  GestureManager_t1999815568  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GESTUREMANAGER_T1999815568_H
#ifndef GESTUREMANAGERINSTANCE_T505647059_H
#define GESTUREMANAGERINSTANCE_T505647059_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.GestureManagerInstance
struct  GestureManagerInstance_t505647059  : public MonoBehaviour_t1158329972
{
public:
	// TouchScript.IGestureDelegate TouchScript.GestureManagerInstance::<GlobalGestureDelegate>k__BackingField
	RuntimeObject* ___U3CGlobalGestureDelegateU3Ek__BackingField_2;
	// System.Collections.Generic.List`1<TouchScript.Gestures.Gesture> TouchScript.GestureManagerInstance::gesturesToReset
	List_1_t1721427117 * ___gesturesToReset_5;
	// System.Action`2<TouchScript.Gestures.Gesture,System.Collections.Generic.IList`1<TouchScript.TouchPoint>> TouchScript.GestureManagerInstance::_updateBegan
	Action_2_t2136039064 * ____updateBegan_6;
	// System.Action`2<TouchScript.Gestures.Gesture,System.Collections.Generic.IList`1<TouchScript.TouchPoint>> TouchScript.GestureManagerInstance::_updateMoved
	Action_2_t2136039064 * ____updateMoved_7;
	// System.Action`2<TouchScript.Gestures.Gesture,System.Collections.Generic.IList`1<TouchScript.TouchPoint>> TouchScript.GestureManagerInstance::_updateEnded
	Action_2_t2136039064 * ____updateEnded_8;
	// System.Action`2<TouchScript.Gestures.Gesture,System.Collections.Generic.IList`1<TouchScript.TouchPoint>> TouchScript.GestureManagerInstance::_updateCancelled
	Action_2_t2136039064 * ____updateCancelled_9;
	// System.Action`1<UnityEngine.Transform> TouchScript.GestureManagerInstance::_processTarget
	Action_1_t3076917440 * ____processTarget_10;
	// System.Action`1<UnityEngine.Transform> TouchScript.GestureManagerInstance::_processTargetBegan
	Action_1_t3076917440 * ____processTargetBegan_11;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Transform,System.Collections.Generic.List`1<TouchScript.TouchPoint>> TouchScript.GestureManagerInstance::targetTouches
	Dictionary_2_t70405120 * ___targetTouches_12;
	// System.Collections.Generic.Dictionary`2<TouchScript.Gestures.Gesture,System.Collections.Generic.List`1<TouchScript.TouchPoint>> TouchScript.GestureManagerInstance::gestureTouches
	Dictionary_2_t673677165 * ___gestureTouches_13;
	// System.Collections.Generic.List`1<TouchScript.Gestures.Gesture> TouchScript.GestureManagerInstance::activeGestures
	List_1_t1721427117 * ___activeGestures_14;

public:
	inline static int32_t get_offset_of_U3CGlobalGestureDelegateU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(GestureManagerInstance_t505647059, ___U3CGlobalGestureDelegateU3Ek__BackingField_2)); }
	inline RuntimeObject* get_U3CGlobalGestureDelegateU3Ek__BackingField_2() const { return ___U3CGlobalGestureDelegateU3Ek__BackingField_2; }
	inline RuntimeObject** get_address_of_U3CGlobalGestureDelegateU3Ek__BackingField_2() { return &___U3CGlobalGestureDelegateU3Ek__BackingField_2; }
	inline void set_U3CGlobalGestureDelegateU3Ek__BackingField_2(RuntimeObject* value)
	{
		___U3CGlobalGestureDelegateU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGlobalGestureDelegateU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_gesturesToReset_5() { return static_cast<int32_t>(offsetof(GestureManagerInstance_t505647059, ___gesturesToReset_5)); }
	inline List_1_t1721427117 * get_gesturesToReset_5() const { return ___gesturesToReset_5; }
	inline List_1_t1721427117 ** get_address_of_gesturesToReset_5() { return &___gesturesToReset_5; }
	inline void set_gesturesToReset_5(List_1_t1721427117 * value)
	{
		___gesturesToReset_5 = value;
		Il2CppCodeGenWriteBarrier((&___gesturesToReset_5), value);
	}

	inline static int32_t get_offset_of__updateBegan_6() { return static_cast<int32_t>(offsetof(GestureManagerInstance_t505647059, ____updateBegan_6)); }
	inline Action_2_t2136039064 * get__updateBegan_6() const { return ____updateBegan_6; }
	inline Action_2_t2136039064 ** get_address_of__updateBegan_6() { return &____updateBegan_6; }
	inline void set__updateBegan_6(Action_2_t2136039064 * value)
	{
		____updateBegan_6 = value;
		Il2CppCodeGenWriteBarrier((&____updateBegan_6), value);
	}

	inline static int32_t get_offset_of__updateMoved_7() { return static_cast<int32_t>(offsetof(GestureManagerInstance_t505647059, ____updateMoved_7)); }
	inline Action_2_t2136039064 * get__updateMoved_7() const { return ____updateMoved_7; }
	inline Action_2_t2136039064 ** get_address_of__updateMoved_7() { return &____updateMoved_7; }
	inline void set__updateMoved_7(Action_2_t2136039064 * value)
	{
		____updateMoved_7 = value;
		Il2CppCodeGenWriteBarrier((&____updateMoved_7), value);
	}

	inline static int32_t get_offset_of__updateEnded_8() { return static_cast<int32_t>(offsetof(GestureManagerInstance_t505647059, ____updateEnded_8)); }
	inline Action_2_t2136039064 * get__updateEnded_8() const { return ____updateEnded_8; }
	inline Action_2_t2136039064 ** get_address_of__updateEnded_8() { return &____updateEnded_8; }
	inline void set__updateEnded_8(Action_2_t2136039064 * value)
	{
		____updateEnded_8 = value;
		Il2CppCodeGenWriteBarrier((&____updateEnded_8), value);
	}

	inline static int32_t get_offset_of__updateCancelled_9() { return static_cast<int32_t>(offsetof(GestureManagerInstance_t505647059, ____updateCancelled_9)); }
	inline Action_2_t2136039064 * get__updateCancelled_9() const { return ____updateCancelled_9; }
	inline Action_2_t2136039064 ** get_address_of__updateCancelled_9() { return &____updateCancelled_9; }
	inline void set__updateCancelled_9(Action_2_t2136039064 * value)
	{
		____updateCancelled_9 = value;
		Il2CppCodeGenWriteBarrier((&____updateCancelled_9), value);
	}

	inline static int32_t get_offset_of__processTarget_10() { return static_cast<int32_t>(offsetof(GestureManagerInstance_t505647059, ____processTarget_10)); }
	inline Action_1_t3076917440 * get__processTarget_10() const { return ____processTarget_10; }
	inline Action_1_t3076917440 ** get_address_of__processTarget_10() { return &____processTarget_10; }
	inline void set__processTarget_10(Action_1_t3076917440 * value)
	{
		____processTarget_10 = value;
		Il2CppCodeGenWriteBarrier((&____processTarget_10), value);
	}

	inline static int32_t get_offset_of__processTargetBegan_11() { return static_cast<int32_t>(offsetof(GestureManagerInstance_t505647059, ____processTargetBegan_11)); }
	inline Action_1_t3076917440 * get__processTargetBegan_11() const { return ____processTargetBegan_11; }
	inline Action_1_t3076917440 ** get_address_of__processTargetBegan_11() { return &____processTargetBegan_11; }
	inline void set__processTargetBegan_11(Action_1_t3076917440 * value)
	{
		____processTargetBegan_11 = value;
		Il2CppCodeGenWriteBarrier((&____processTargetBegan_11), value);
	}

	inline static int32_t get_offset_of_targetTouches_12() { return static_cast<int32_t>(offsetof(GestureManagerInstance_t505647059, ___targetTouches_12)); }
	inline Dictionary_2_t70405120 * get_targetTouches_12() const { return ___targetTouches_12; }
	inline Dictionary_2_t70405120 ** get_address_of_targetTouches_12() { return &___targetTouches_12; }
	inline void set_targetTouches_12(Dictionary_2_t70405120 * value)
	{
		___targetTouches_12 = value;
		Il2CppCodeGenWriteBarrier((&___targetTouches_12), value);
	}

	inline static int32_t get_offset_of_gestureTouches_13() { return static_cast<int32_t>(offsetof(GestureManagerInstance_t505647059, ___gestureTouches_13)); }
	inline Dictionary_2_t673677165 * get_gestureTouches_13() const { return ___gestureTouches_13; }
	inline Dictionary_2_t673677165 ** get_address_of_gestureTouches_13() { return &___gestureTouches_13; }
	inline void set_gestureTouches_13(Dictionary_2_t673677165 * value)
	{
		___gestureTouches_13 = value;
		Il2CppCodeGenWriteBarrier((&___gestureTouches_13), value);
	}

	inline static int32_t get_offset_of_activeGestures_14() { return static_cast<int32_t>(offsetof(GestureManagerInstance_t505647059, ___activeGestures_14)); }
	inline List_1_t1721427117 * get_activeGestures_14() const { return ___activeGestures_14; }
	inline List_1_t1721427117 ** get_address_of_activeGestures_14() { return &___activeGestures_14; }
	inline void set_activeGestures_14(List_1_t1721427117 * value)
	{
		___activeGestures_14 = value;
		Il2CppCodeGenWriteBarrier((&___activeGestures_14), value);
	}
};

struct GestureManagerInstance_t505647059_StaticFields
{
public:
	// TouchScript.GestureManagerInstance TouchScript.GestureManagerInstance::instance
	GestureManagerInstance_t505647059 * ___instance_3;
	// System.Boolean TouchScript.GestureManagerInstance::shuttingDown
	bool ___shuttingDown_4;
	// TouchScript.Utils.ObjectPool`1<System.Collections.Generic.List`1<TouchScript.Gestures.Gesture>> TouchScript.GestureManagerInstance::gestureListPool
	ObjectPool_1_t1074715964 * ___gestureListPool_15;
	// TouchScript.Utils.ObjectPool`1<System.Collections.Generic.List`1<TouchScript.TouchPoint>> TouchScript.GestureManagerInstance::touchListPool
	ObjectPool_1_t3977006358 * ___touchListPool_16;
	// TouchScript.Utils.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Transform>> TouchScript.GestureManagerInstance::transformListPool
	ObjectPool_1_t1997528037 * ___transformListPool_17;

public:
	inline static int32_t get_offset_of_instance_3() { return static_cast<int32_t>(offsetof(GestureManagerInstance_t505647059_StaticFields, ___instance_3)); }
	inline GestureManagerInstance_t505647059 * get_instance_3() const { return ___instance_3; }
	inline GestureManagerInstance_t505647059 ** get_address_of_instance_3() { return &___instance_3; }
	inline void set_instance_3(GestureManagerInstance_t505647059 * value)
	{
		___instance_3 = value;
		Il2CppCodeGenWriteBarrier((&___instance_3), value);
	}

	inline static int32_t get_offset_of_shuttingDown_4() { return static_cast<int32_t>(offsetof(GestureManagerInstance_t505647059_StaticFields, ___shuttingDown_4)); }
	inline bool get_shuttingDown_4() const { return ___shuttingDown_4; }
	inline bool* get_address_of_shuttingDown_4() { return &___shuttingDown_4; }
	inline void set_shuttingDown_4(bool value)
	{
		___shuttingDown_4 = value;
	}

	inline static int32_t get_offset_of_gestureListPool_15() { return static_cast<int32_t>(offsetof(GestureManagerInstance_t505647059_StaticFields, ___gestureListPool_15)); }
	inline ObjectPool_1_t1074715964 * get_gestureListPool_15() const { return ___gestureListPool_15; }
	inline ObjectPool_1_t1074715964 ** get_address_of_gestureListPool_15() { return &___gestureListPool_15; }
	inline void set_gestureListPool_15(ObjectPool_1_t1074715964 * value)
	{
		___gestureListPool_15 = value;
		Il2CppCodeGenWriteBarrier((&___gestureListPool_15), value);
	}

	inline static int32_t get_offset_of_touchListPool_16() { return static_cast<int32_t>(offsetof(GestureManagerInstance_t505647059_StaticFields, ___touchListPool_16)); }
	inline ObjectPool_1_t3977006358 * get_touchListPool_16() const { return ___touchListPool_16; }
	inline ObjectPool_1_t3977006358 ** get_address_of_touchListPool_16() { return &___touchListPool_16; }
	inline void set_touchListPool_16(ObjectPool_1_t3977006358 * value)
	{
		___touchListPool_16 = value;
		Il2CppCodeGenWriteBarrier((&___touchListPool_16), value);
	}

	inline static int32_t get_offset_of_transformListPool_17() { return static_cast<int32_t>(offsetof(GestureManagerInstance_t505647059_StaticFields, ___transformListPool_17)); }
	inline ObjectPool_1_t1997528037 * get_transformListPool_17() const { return ___transformListPool_17; }
	inline ObjectPool_1_t1997528037 ** get_address_of_transformListPool_17() { return &___transformListPool_17; }
	inline void set_transformListPool_17(ObjectPool_1_t1997528037 * value)
	{
		___transformListPool_17 = value;
		Il2CppCodeGenWriteBarrier((&___transformListPool_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GESTUREMANAGERINSTANCE_T505647059_H
#ifndef UIBEHAVIOUR_T3960014691_H
#define UIBEHAVIOUR_T3960014691_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3960014691  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3960014691_H
#ifndef HITTEST_T768639505_H
#define HITTEST_T768639505_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Hit.HitTest
struct  HitTest_t768639505  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HITTEST_T768639505_H
#ifndef INPUTSOURCE_T3078263767_H
#define INPUTSOURCE_T3078263767_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.InputSources.InputSource
struct  InputSource_t3078263767  : public MonoBehaviour_t1158329972
{
public:
	// TouchScript.InputSources.ICoordinatesRemapper TouchScript.InputSources.InputSource::<CoordinatesRemapper>k__BackingField
	RuntimeObject* ___U3CCoordinatesRemapperU3Ek__BackingField_2;
	// System.Boolean TouchScript.InputSources.InputSource::advancedProps
	bool ___advancedProps_3;
	// TouchScript.TouchManagerInstance TouchScript.InputSources.InputSource::manager
	TouchManagerInstance_t2629118981 * ___manager_4;

public:
	inline static int32_t get_offset_of_U3CCoordinatesRemapperU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(InputSource_t3078263767, ___U3CCoordinatesRemapperU3Ek__BackingField_2)); }
	inline RuntimeObject* get_U3CCoordinatesRemapperU3Ek__BackingField_2() const { return ___U3CCoordinatesRemapperU3Ek__BackingField_2; }
	inline RuntimeObject** get_address_of_U3CCoordinatesRemapperU3Ek__BackingField_2() { return &___U3CCoordinatesRemapperU3Ek__BackingField_2; }
	inline void set_U3CCoordinatesRemapperU3Ek__BackingField_2(RuntimeObject* value)
	{
		___U3CCoordinatesRemapperU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCoordinatesRemapperU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_advancedProps_3() { return static_cast<int32_t>(offsetof(InputSource_t3078263767, ___advancedProps_3)); }
	inline bool get_advancedProps_3() const { return ___advancedProps_3; }
	inline bool* get_address_of_advancedProps_3() { return &___advancedProps_3; }
	inline void set_advancedProps_3(bool value)
	{
		___advancedProps_3 = value;
	}

	inline static int32_t get_offset_of_manager_4() { return static_cast<int32_t>(offsetof(InputSource_t3078263767, ___manager_4)); }
	inline TouchManagerInstance_t2629118981 * get_manager_4() const { return ___manager_4; }
	inline TouchManagerInstance_t2629118981 ** get_address_of_manager_4() { return &___manager_4; }
	inline void set_manager_4(TouchManagerInstance_t2629118981 * value)
	{
		___manager_4 = value;
		Il2CppCodeGenWriteBarrier((&___manager_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTSOURCE_T3078263767_H
#ifndef TRANSFORMER_T946377179_H
#define TRANSFORMER_T946377179_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Behaviors.Transformer
struct  Transformer_t946377179  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform TouchScript.Behaviors.Transformer::cachedTransform
	Transform_t3275118058 * ___cachedTransform_2;
	// System.Collections.Generic.List`1<TouchScript.Gestures.ITransformGesture> TouchScript.Behaviors.Transformer::gestures
	List_1_t909705622 * ___gestures_3;

public:
	inline static int32_t get_offset_of_cachedTransform_2() { return static_cast<int32_t>(offsetof(Transformer_t946377179, ___cachedTransform_2)); }
	inline Transform_t3275118058 * get_cachedTransform_2() const { return ___cachedTransform_2; }
	inline Transform_t3275118058 ** get_address_of_cachedTransform_2() { return &___cachedTransform_2; }
	inline void set_cachedTransform_2(Transform_t3275118058 * value)
	{
		___cachedTransform_2 = value;
		Il2CppCodeGenWriteBarrier((&___cachedTransform_2), value);
	}

	inline static int32_t get_offset_of_gestures_3() { return static_cast<int32_t>(offsetof(Transformer_t946377179, ___gestures_3)); }
	inline List_1_t909705622 * get_gestures_3() const { return ___gestures_3; }
	inline List_1_t909705622 ** get_address_of_gestures_3() { return &___gestures_3; }
	inline void set_gestures_3(List_1_t909705622 * value)
	{
		___gestures_3 = value;
		Il2CppCodeGenWriteBarrier((&___gestures_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMER_T946377179_H
#ifndef TUIOINPUT_T352576185_H
#define TUIOINPUT_T352576185_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.InputSources.TuioInput
struct  TuioInput_t352576185  : public InputSource_t3078263767
{
public:
	// System.Int32 TouchScript.InputSources.TuioInput::tuioPort
	int32_t ___tuioPort_6;
	// TouchScript.InputSources.TuioInput/InputType TouchScript.InputSources.TuioInput::supportedInputs
	int32_t ___supportedInputs_7;
	// System.Collections.Generic.List`1<TouchScript.InputSources.TuioObjectMapping> TouchScript.InputSources.TuioInput::tuioObjectMappings
	List_1_t3450048260 * ___tuioObjectMappings_8;
	// TouchScript.Tags TouchScript.InputSources.TuioInput::cursorTags
	Tags_t1265380163 * ___cursorTags_9;
	// TouchScript.Tags TouchScript.InputSources.TuioInput::blobTags
	Tags_t1265380163 * ___blobTags_10;
	// TouchScript.Tags TouchScript.InputSources.TuioInput::objectTags
	Tags_t1265380163 * ___objectTags_11;
	// TUIOsharp.TuioServer TouchScript.InputSources.TuioInput::server
	TuioServer_t595520884 * ___server_12;
	// TUIOsharp.DataProcessors.CursorProcessor TouchScript.InputSources.TuioInput::cursorProcessor
	CursorProcessor_t1785954004 * ___cursorProcessor_13;
	// TUIOsharp.DataProcessors.ObjectProcessor TouchScript.InputSources.TuioInput::objectProcessor
	ObjectProcessor_t221569383 * ___objectProcessor_14;
	// TUIOsharp.DataProcessors.BlobProcessor TouchScript.InputSources.TuioInput::blobProcessor
	BlobProcessor_t3603341577 * ___blobProcessor_15;
	// System.Collections.Generic.Dictionary`2<TUIOsharp.Entities.TuioCursor,TouchScript.TouchPoint> TouchScript.InputSources.TuioInput::cursorToInternalId
	Dictionary_2_t2117679327 * ___cursorToInternalId_16;
	// System.Collections.Generic.Dictionary`2<TUIOsharp.Entities.TuioBlob,TouchScript.TouchPoint> TouchScript.InputSources.TuioInput::blobToInternalId
	Dictionary_2_t855216920 * ___blobToInternalId_17;
	// System.Collections.Generic.Dictionary`2<TUIOsharp.Entities.TuioObject,TouchScript.TouchPoint> TouchScript.InputSources.TuioInput::objectToInternalId
	Dictionary_2_t788444728 * ___objectToInternalId_18;
	// System.Int32 TouchScript.InputSources.TuioInput::screenWidth
	int32_t ___screenWidth_19;
	// System.Int32 TouchScript.InputSources.TuioInput::screenHeight
	int32_t ___screenHeight_20;

public:
	inline static int32_t get_offset_of_tuioPort_6() { return static_cast<int32_t>(offsetof(TuioInput_t352576185, ___tuioPort_6)); }
	inline int32_t get_tuioPort_6() const { return ___tuioPort_6; }
	inline int32_t* get_address_of_tuioPort_6() { return &___tuioPort_6; }
	inline void set_tuioPort_6(int32_t value)
	{
		___tuioPort_6 = value;
	}

	inline static int32_t get_offset_of_supportedInputs_7() { return static_cast<int32_t>(offsetof(TuioInput_t352576185, ___supportedInputs_7)); }
	inline int32_t get_supportedInputs_7() const { return ___supportedInputs_7; }
	inline int32_t* get_address_of_supportedInputs_7() { return &___supportedInputs_7; }
	inline void set_supportedInputs_7(int32_t value)
	{
		___supportedInputs_7 = value;
	}

	inline static int32_t get_offset_of_tuioObjectMappings_8() { return static_cast<int32_t>(offsetof(TuioInput_t352576185, ___tuioObjectMappings_8)); }
	inline List_1_t3450048260 * get_tuioObjectMappings_8() const { return ___tuioObjectMappings_8; }
	inline List_1_t3450048260 ** get_address_of_tuioObjectMappings_8() { return &___tuioObjectMappings_8; }
	inline void set_tuioObjectMappings_8(List_1_t3450048260 * value)
	{
		___tuioObjectMappings_8 = value;
		Il2CppCodeGenWriteBarrier((&___tuioObjectMappings_8), value);
	}

	inline static int32_t get_offset_of_cursorTags_9() { return static_cast<int32_t>(offsetof(TuioInput_t352576185, ___cursorTags_9)); }
	inline Tags_t1265380163 * get_cursorTags_9() const { return ___cursorTags_9; }
	inline Tags_t1265380163 ** get_address_of_cursorTags_9() { return &___cursorTags_9; }
	inline void set_cursorTags_9(Tags_t1265380163 * value)
	{
		___cursorTags_9 = value;
		Il2CppCodeGenWriteBarrier((&___cursorTags_9), value);
	}

	inline static int32_t get_offset_of_blobTags_10() { return static_cast<int32_t>(offsetof(TuioInput_t352576185, ___blobTags_10)); }
	inline Tags_t1265380163 * get_blobTags_10() const { return ___blobTags_10; }
	inline Tags_t1265380163 ** get_address_of_blobTags_10() { return &___blobTags_10; }
	inline void set_blobTags_10(Tags_t1265380163 * value)
	{
		___blobTags_10 = value;
		Il2CppCodeGenWriteBarrier((&___blobTags_10), value);
	}

	inline static int32_t get_offset_of_objectTags_11() { return static_cast<int32_t>(offsetof(TuioInput_t352576185, ___objectTags_11)); }
	inline Tags_t1265380163 * get_objectTags_11() const { return ___objectTags_11; }
	inline Tags_t1265380163 ** get_address_of_objectTags_11() { return &___objectTags_11; }
	inline void set_objectTags_11(Tags_t1265380163 * value)
	{
		___objectTags_11 = value;
		Il2CppCodeGenWriteBarrier((&___objectTags_11), value);
	}

	inline static int32_t get_offset_of_server_12() { return static_cast<int32_t>(offsetof(TuioInput_t352576185, ___server_12)); }
	inline TuioServer_t595520884 * get_server_12() const { return ___server_12; }
	inline TuioServer_t595520884 ** get_address_of_server_12() { return &___server_12; }
	inline void set_server_12(TuioServer_t595520884 * value)
	{
		___server_12 = value;
		Il2CppCodeGenWriteBarrier((&___server_12), value);
	}

	inline static int32_t get_offset_of_cursorProcessor_13() { return static_cast<int32_t>(offsetof(TuioInput_t352576185, ___cursorProcessor_13)); }
	inline CursorProcessor_t1785954004 * get_cursorProcessor_13() const { return ___cursorProcessor_13; }
	inline CursorProcessor_t1785954004 ** get_address_of_cursorProcessor_13() { return &___cursorProcessor_13; }
	inline void set_cursorProcessor_13(CursorProcessor_t1785954004 * value)
	{
		___cursorProcessor_13 = value;
		Il2CppCodeGenWriteBarrier((&___cursorProcessor_13), value);
	}

	inline static int32_t get_offset_of_objectProcessor_14() { return static_cast<int32_t>(offsetof(TuioInput_t352576185, ___objectProcessor_14)); }
	inline ObjectProcessor_t221569383 * get_objectProcessor_14() const { return ___objectProcessor_14; }
	inline ObjectProcessor_t221569383 ** get_address_of_objectProcessor_14() { return &___objectProcessor_14; }
	inline void set_objectProcessor_14(ObjectProcessor_t221569383 * value)
	{
		___objectProcessor_14 = value;
		Il2CppCodeGenWriteBarrier((&___objectProcessor_14), value);
	}

	inline static int32_t get_offset_of_blobProcessor_15() { return static_cast<int32_t>(offsetof(TuioInput_t352576185, ___blobProcessor_15)); }
	inline BlobProcessor_t3603341577 * get_blobProcessor_15() const { return ___blobProcessor_15; }
	inline BlobProcessor_t3603341577 ** get_address_of_blobProcessor_15() { return &___blobProcessor_15; }
	inline void set_blobProcessor_15(BlobProcessor_t3603341577 * value)
	{
		___blobProcessor_15 = value;
		Il2CppCodeGenWriteBarrier((&___blobProcessor_15), value);
	}

	inline static int32_t get_offset_of_cursorToInternalId_16() { return static_cast<int32_t>(offsetof(TuioInput_t352576185, ___cursorToInternalId_16)); }
	inline Dictionary_2_t2117679327 * get_cursorToInternalId_16() const { return ___cursorToInternalId_16; }
	inline Dictionary_2_t2117679327 ** get_address_of_cursorToInternalId_16() { return &___cursorToInternalId_16; }
	inline void set_cursorToInternalId_16(Dictionary_2_t2117679327 * value)
	{
		___cursorToInternalId_16 = value;
		Il2CppCodeGenWriteBarrier((&___cursorToInternalId_16), value);
	}

	inline static int32_t get_offset_of_blobToInternalId_17() { return static_cast<int32_t>(offsetof(TuioInput_t352576185, ___blobToInternalId_17)); }
	inline Dictionary_2_t855216920 * get_blobToInternalId_17() const { return ___blobToInternalId_17; }
	inline Dictionary_2_t855216920 ** get_address_of_blobToInternalId_17() { return &___blobToInternalId_17; }
	inline void set_blobToInternalId_17(Dictionary_2_t855216920 * value)
	{
		___blobToInternalId_17 = value;
		Il2CppCodeGenWriteBarrier((&___blobToInternalId_17), value);
	}

	inline static int32_t get_offset_of_objectToInternalId_18() { return static_cast<int32_t>(offsetof(TuioInput_t352576185, ___objectToInternalId_18)); }
	inline Dictionary_2_t788444728 * get_objectToInternalId_18() const { return ___objectToInternalId_18; }
	inline Dictionary_2_t788444728 ** get_address_of_objectToInternalId_18() { return &___objectToInternalId_18; }
	inline void set_objectToInternalId_18(Dictionary_2_t788444728 * value)
	{
		___objectToInternalId_18 = value;
		Il2CppCodeGenWriteBarrier((&___objectToInternalId_18), value);
	}

	inline static int32_t get_offset_of_screenWidth_19() { return static_cast<int32_t>(offsetof(TuioInput_t352576185, ___screenWidth_19)); }
	inline int32_t get_screenWidth_19() const { return ___screenWidth_19; }
	inline int32_t* get_address_of_screenWidth_19() { return &___screenWidth_19; }
	inline void set_screenWidth_19(int32_t value)
	{
		___screenWidth_19 = value;
	}

	inline static int32_t get_offset_of_screenHeight_20() { return static_cast<int32_t>(offsetof(TuioInput_t352576185, ___screenHeight_20)); }
	inline int32_t get_screenHeight_20() const { return ___screenHeight_20; }
	inline int32_t* get_address_of_screenHeight_20() { return &___screenHeight_20; }
	inline void set_screenHeight_20(int32_t value)
	{
		___screenHeight_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUIOINPUT_T352576185_H
#ifndef BASEINPUTMODULE_T1295781545_H
#define BASEINPUTMODULE_T1295781545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.BaseInputModule
struct  BaseInputModule_t1295781545  : public UIBehaviour_t3960014691
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult> UnityEngine.EventSystems.BaseInputModule::m_RaycastResultCache
	List_1_t3685274804 * ___m_RaycastResultCache_2;
	// UnityEngine.EventSystems.AxisEventData UnityEngine.EventSystems.BaseInputModule::m_AxisEventData
	AxisEventData_t1524870173 * ___m_AxisEventData_3;
	// UnityEngine.EventSystems.EventSystem UnityEngine.EventSystems.BaseInputModule::m_EventSystem
	EventSystem_t3466835263 * ___m_EventSystem_4;
	// UnityEngine.EventSystems.BaseEventData UnityEngine.EventSystems.BaseInputModule::m_BaseEventData
	BaseEventData_t2681005625 * ___m_BaseEventData_5;
	// UnityEngine.EventSystems.BaseInput UnityEngine.EventSystems.BaseInputModule::m_InputOverride
	BaseInput_t621514313 * ___m_InputOverride_6;
	// UnityEngine.EventSystems.BaseInput UnityEngine.EventSystems.BaseInputModule::m_DefaultInput
	BaseInput_t621514313 * ___m_DefaultInput_7;

public:
	inline static int32_t get_offset_of_m_RaycastResultCache_2() { return static_cast<int32_t>(offsetof(BaseInputModule_t1295781545, ___m_RaycastResultCache_2)); }
	inline List_1_t3685274804 * get_m_RaycastResultCache_2() const { return ___m_RaycastResultCache_2; }
	inline List_1_t3685274804 ** get_address_of_m_RaycastResultCache_2() { return &___m_RaycastResultCache_2; }
	inline void set_m_RaycastResultCache_2(List_1_t3685274804 * value)
	{
		___m_RaycastResultCache_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_RaycastResultCache_2), value);
	}

	inline static int32_t get_offset_of_m_AxisEventData_3() { return static_cast<int32_t>(offsetof(BaseInputModule_t1295781545, ___m_AxisEventData_3)); }
	inline AxisEventData_t1524870173 * get_m_AxisEventData_3() const { return ___m_AxisEventData_3; }
	inline AxisEventData_t1524870173 ** get_address_of_m_AxisEventData_3() { return &___m_AxisEventData_3; }
	inline void set_m_AxisEventData_3(AxisEventData_t1524870173 * value)
	{
		___m_AxisEventData_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_AxisEventData_3), value);
	}

	inline static int32_t get_offset_of_m_EventSystem_4() { return static_cast<int32_t>(offsetof(BaseInputModule_t1295781545, ___m_EventSystem_4)); }
	inline EventSystem_t3466835263 * get_m_EventSystem_4() const { return ___m_EventSystem_4; }
	inline EventSystem_t3466835263 ** get_address_of_m_EventSystem_4() { return &___m_EventSystem_4; }
	inline void set_m_EventSystem_4(EventSystem_t3466835263 * value)
	{
		___m_EventSystem_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventSystem_4), value);
	}

	inline static int32_t get_offset_of_m_BaseEventData_5() { return static_cast<int32_t>(offsetof(BaseInputModule_t1295781545, ___m_BaseEventData_5)); }
	inline BaseEventData_t2681005625 * get_m_BaseEventData_5() const { return ___m_BaseEventData_5; }
	inline BaseEventData_t2681005625 ** get_address_of_m_BaseEventData_5() { return &___m_BaseEventData_5; }
	inline void set_m_BaseEventData_5(BaseEventData_t2681005625 * value)
	{
		___m_BaseEventData_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_BaseEventData_5), value);
	}

	inline static int32_t get_offset_of_m_InputOverride_6() { return static_cast<int32_t>(offsetof(BaseInputModule_t1295781545, ___m_InputOverride_6)); }
	inline BaseInput_t621514313 * get_m_InputOverride_6() const { return ___m_InputOverride_6; }
	inline BaseInput_t621514313 ** get_address_of_m_InputOverride_6() { return &___m_InputOverride_6; }
	inline void set_m_InputOverride_6(BaseInput_t621514313 * value)
	{
		___m_InputOverride_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_InputOverride_6), value);
	}

	inline static int32_t get_offset_of_m_DefaultInput_7() { return static_cast<int32_t>(offsetof(BaseInputModule_t1295781545, ___m_DefaultInput_7)); }
	inline BaseInput_t621514313 * get_m_DefaultInput_7() const { return ___m_DefaultInput_7; }
	inline BaseInput_t621514313 ** get_address_of_m_DefaultInput_7() { return &___m_DefaultInput_7; }
	inline void set_m_DefaultInput_7(BaseInput_t621514313 * value)
	{
		___m_DefaultInput_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_DefaultInput_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEINPUTMODULE_T1295781545_H
#ifndef TOUCHPROXY_T1093150977_H
#define TOUCHPROXY_T1093150977_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Behaviors.Visualizer.TouchProxy
struct  TouchProxy_t1093150977  : public TouchProxyBase_t4188753234
{
public:
	// UnityEngine.UI.Text TouchScript.Behaviors.Visualizer.TouchProxy::Text
	Text_t356221433 * ___Text_6;
	// System.Text.StringBuilder TouchScript.Behaviors.Visualizer.TouchProxy::stringBuilder
	StringBuilder_t1221177846 * ___stringBuilder_7;

public:
	inline static int32_t get_offset_of_Text_6() { return static_cast<int32_t>(offsetof(TouchProxy_t1093150977, ___Text_6)); }
	inline Text_t356221433 * get_Text_6() const { return ___Text_6; }
	inline Text_t356221433 ** get_address_of_Text_6() { return &___Text_6; }
	inline void set_Text_6(Text_t356221433 * value)
	{
		___Text_6 = value;
		Il2CppCodeGenWriteBarrier((&___Text_6), value);
	}

	inline static int32_t get_offset_of_stringBuilder_7() { return static_cast<int32_t>(offsetof(TouchProxy_t1093150977, ___stringBuilder_7)); }
	inline StringBuilder_t1221177846 * get_stringBuilder_7() const { return ___stringBuilder_7; }
	inline StringBuilder_t1221177846 ** get_address_of_stringBuilder_7() { return &___stringBuilder_7; }
	inline void set_stringBuilder_7(StringBuilder_t1221177846 * value)
	{
		___stringBuilder_7 = value;
		Il2CppCodeGenWriteBarrier((&___stringBuilder_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHPROXY_T1093150977_H
#ifndef UNTOUCHABLE_T2967803006_H
#define UNTOUCHABLE_T2967803006_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Hit.Untouchable
struct  Untouchable_t2967803006  : public HitTest_t768639505
{
public:
	// System.Boolean TouchScript.Hit.Untouchable::DiscardTouch
	bool ___DiscardTouch_2;

public:
	inline static int32_t get_offset_of_DiscardTouch_2() { return static_cast<int32_t>(offsetof(Untouchable_t2967803006, ___DiscardTouch_2)); }
	inline bool get_DiscardTouch_2() const { return ___DiscardTouch_2; }
	inline bool* get_address_of_DiscardTouch_2() { return &___DiscardTouch_2; }
	inline void set_DiscardTouch_2(bool value)
	{
		___DiscardTouch_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNTOUCHABLE_T2967803006_H
#ifndef MOBILEINPUT_T1619040116_H
#define MOBILEINPUT_T1619040116_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.InputSources.MobileInput
struct  MobileInput_t1619040116  : public InputSource_t3078263767
{
public:
	// System.Boolean TouchScript.InputSources.MobileInput::DisableOnNonTouchPlatforms
	bool ___DisableOnNonTouchPlatforms_5;
	// TouchScript.Tags TouchScript.InputSources.MobileInput::Tags
	Tags_t1265380163 * ___Tags_6;
	// TouchScript.InputSources.InputHandlers.TouchHandler TouchScript.InputSources.MobileInput::touchHandler
	TouchHandler_t2521645213 * ___touchHandler_7;

public:
	inline static int32_t get_offset_of_DisableOnNonTouchPlatforms_5() { return static_cast<int32_t>(offsetof(MobileInput_t1619040116, ___DisableOnNonTouchPlatforms_5)); }
	inline bool get_DisableOnNonTouchPlatforms_5() const { return ___DisableOnNonTouchPlatforms_5; }
	inline bool* get_address_of_DisableOnNonTouchPlatforms_5() { return &___DisableOnNonTouchPlatforms_5; }
	inline void set_DisableOnNonTouchPlatforms_5(bool value)
	{
		___DisableOnNonTouchPlatforms_5 = value;
	}

	inline static int32_t get_offset_of_Tags_6() { return static_cast<int32_t>(offsetof(MobileInput_t1619040116, ___Tags_6)); }
	inline Tags_t1265380163 * get_Tags_6() const { return ___Tags_6; }
	inline Tags_t1265380163 ** get_address_of_Tags_6() { return &___Tags_6; }
	inline void set_Tags_6(Tags_t1265380163 * value)
	{
		___Tags_6 = value;
		Il2CppCodeGenWriteBarrier((&___Tags_6), value);
	}

	inline static int32_t get_offset_of_touchHandler_7() { return static_cast<int32_t>(offsetof(MobileInput_t1619040116, ___touchHandler_7)); }
	inline TouchHandler_t2521645213 * get_touchHandler_7() const { return ___touchHandler_7; }
	inline TouchHandler_t2521645213 ** get_address_of_touchHandler_7() { return &___touchHandler_7; }
	inline void set_touchHandler_7(TouchHandler_t2521645213 * value)
	{
		___touchHandler_7 = value;
		Il2CppCodeGenWriteBarrier((&___touchHandler_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOBILEINPUT_T1619040116_H
#ifndef GESTURE_T2352305985_H
#define GESTURE_T2352305985_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Gestures.Gesture
struct  Gesture_t2352305985  : public DebuggableMonoBehaviour_t3136086048
{
public:
	// System.EventHandler`1<TouchScript.Gestures.GestureStateChangeEventArgs> TouchScript.Gestures.Gesture::stateChangedInvoker
	EventHandler_1_t2091288363 * ___stateChangedInvoker_4;
	// System.EventHandler`1<System.EventArgs> TouchScript.Gestures.Gesture::cancelledInvoker
	EventHandler_1_t1880931879 * ___cancelledInvoker_5;
	// TouchScript.Gestures.Gesture/GestureState TouchScript.Gestures.Gesture::<PreviousState>k__BackingField
	int32_t ___U3CPreviousStateU3Ek__BackingField_6;
	// TouchScript.IGestureDelegate TouchScript.Gestures.Gesture::<Delegate>k__BackingField
	RuntimeObject* ___U3CDelegateU3Ek__BackingField_7;
	// TouchScript.ITouchManager TouchScript.Gestures.Gesture::<touchManager>k__BackingField
	RuntimeObject* ___U3CtouchManagerU3Ek__BackingField_8;
	// TouchScript.Gestures.Gesture/TouchesNumState TouchScript.Gestures.Gesture::<touchesNumState>k__BackingField
	int32_t ___U3CtouchesNumStateU3Ek__BackingField_9;
	// System.Collections.Generic.List`1<TouchScript.TouchPoint> TouchScript.Gestures.Gesture::activeTouches
	List_1_t328750215 * ___activeTouches_10;
	// UnityEngine.Transform TouchScript.Gestures.Gesture::cachedTransform
	Transform_t3275118058 * ___cachedTransform_11;
	// System.Boolean TouchScript.Gestures.Gesture::advancedProps
	bool ___advancedProps_12;
	// System.Int32 TouchScript.Gestures.Gesture::minTouches
	int32_t ___minTouches_13;
	// System.Int32 TouchScript.Gestures.Gesture::maxTouches
	int32_t ___maxTouches_14;
	// System.Boolean TouchScript.Gestures.Gesture::combineTouches
	bool ___combineTouches_15;
	// System.Single TouchScript.Gestures.Gesture::combineTouchesInterval
	float ___combineTouchesInterval_16;
	// System.Boolean TouchScript.Gestures.Gesture::useSendMessage
	bool ___useSendMessage_17;
	// System.Boolean TouchScript.Gestures.Gesture::sendStateChangeMessages
	bool ___sendStateChangeMessages_18;
	// UnityEngine.GameObject TouchScript.Gestures.Gesture::sendMessageTarget
	GameObject_t1756533147 * ___sendMessageTarget_19;
	// TouchScript.Gestures.Gesture TouchScript.Gestures.Gesture::requireGestureToFail
	Gesture_t2352305985 * ___requireGestureToFail_20;
	// System.Collections.Generic.List`1<TouchScript.Gestures.Gesture> TouchScript.Gestures.Gesture::friendlyGestures
	List_1_t1721427117 * ___friendlyGestures_21;
	// System.Int32 TouchScript.Gestures.Gesture::numTouches
	int32_t ___numTouches_22;
	// TouchScript.Layers.TouchLayer TouchScript.Gestures.Gesture::layer
	TouchLayer_t2635439978 * ___layer_23;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<TouchScript.TouchPoint> TouchScript.Gestures.Gesture::readonlyActiveTouches
	ReadOnlyCollection_1_t1145414775 * ___readonlyActiveTouches_24;
	// TouchScript.Utils.TimedSequence`1<TouchScript.TouchPoint> TouchScript.Gestures.Gesture::touchSequence
	TimedSequence_1_t2737250283 * ___touchSequence_25;
	// TouchScript.GestureManagerInstance TouchScript.Gestures.Gesture::gestureManagerInstance
	GestureManagerInstance_t505647059 * ___gestureManagerInstance_26;
	// TouchScript.Gestures.Gesture/GestureState TouchScript.Gestures.Gesture::delayedStateChange
	int32_t ___delayedStateChange_27;
	// System.Boolean TouchScript.Gestures.Gesture::requiredGestureFailed
	bool ___requiredGestureFailed_28;
	// TouchScript.Gestures.Gesture/GestureState TouchScript.Gestures.Gesture::state
	int32_t ___state_29;
	// UnityEngine.Vector2 TouchScript.Gestures.Gesture::cachedScreenPosition
	Vector2_t2243707579  ___cachedScreenPosition_30;
	// UnityEngine.Vector2 TouchScript.Gestures.Gesture::cachedPreviousScreenPosition
	Vector2_t2243707579  ___cachedPreviousScreenPosition_31;

public:
	inline static int32_t get_offset_of_stateChangedInvoker_4() { return static_cast<int32_t>(offsetof(Gesture_t2352305985, ___stateChangedInvoker_4)); }
	inline EventHandler_1_t2091288363 * get_stateChangedInvoker_4() const { return ___stateChangedInvoker_4; }
	inline EventHandler_1_t2091288363 ** get_address_of_stateChangedInvoker_4() { return &___stateChangedInvoker_4; }
	inline void set_stateChangedInvoker_4(EventHandler_1_t2091288363 * value)
	{
		___stateChangedInvoker_4 = value;
		Il2CppCodeGenWriteBarrier((&___stateChangedInvoker_4), value);
	}

	inline static int32_t get_offset_of_cancelledInvoker_5() { return static_cast<int32_t>(offsetof(Gesture_t2352305985, ___cancelledInvoker_5)); }
	inline EventHandler_1_t1880931879 * get_cancelledInvoker_5() const { return ___cancelledInvoker_5; }
	inline EventHandler_1_t1880931879 ** get_address_of_cancelledInvoker_5() { return &___cancelledInvoker_5; }
	inline void set_cancelledInvoker_5(EventHandler_1_t1880931879 * value)
	{
		___cancelledInvoker_5 = value;
		Il2CppCodeGenWriteBarrier((&___cancelledInvoker_5), value);
	}

	inline static int32_t get_offset_of_U3CPreviousStateU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Gesture_t2352305985, ___U3CPreviousStateU3Ek__BackingField_6)); }
	inline int32_t get_U3CPreviousStateU3Ek__BackingField_6() const { return ___U3CPreviousStateU3Ek__BackingField_6; }
	inline int32_t* get_address_of_U3CPreviousStateU3Ek__BackingField_6() { return &___U3CPreviousStateU3Ek__BackingField_6; }
	inline void set_U3CPreviousStateU3Ek__BackingField_6(int32_t value)
	{
		___U3CPreviousStateU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CDelegateU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(Gesture_t2352305985, ___U3CDelegateU3Ek__BackingField_7)); }
	inline RuntimeObject* get_U3CDelegateU3Ek__BackingField_7() const { return ___U3CDelegateU3Ek__BackingField_7; }
	inline RuntimeObject** get_address_of_U3CDelegateU3Ek__BackingField_7() { return &___U3CDelegateU3Ek__BackingField_7; }
	inline void set_U3CDelegateU3Ek__BackingField_7(RuntimeObject* value)
	{
		___U3CDelegateU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDelegateU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CtouchManagerU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(Gesture_t2352305985, ___U3CtouchManagerU3Ek__BackingField_8)); }
	inline RuntimeObject* get_U3CtouchManagerU3Ek__BackingField_8() const { return ___U3CtouchManagerU3Ek__BackingField_8; }
	inline RuntimeObject** get_address_of_U3CtouchManagerU3Ek__BackingField_8() { return &___U3CtouchManagerU3Ek__BackingField_8; }
	inline void set_U3CtouchManagerU3Ek__BackingField_8(RuntimeObject* value)
	{
		___U3CtouchManagerU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtouchManagerU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CtouchesNumStateU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(Gesture_t2352305985, ___U3CtouchesNumStateU3Ek__BackingField_9)); }
	inline int32_t get_U3CtouchesNumStateU3Ek__BackingField_9() const { return ___U3CtouchesNumStateU3Ek__BackingField_9; }
	inline int32_t* get_address_of_U3CtouchesNumStateU3Ek__BackingField_9() { return &___U3CtouchesNumStateU3Ek__BackingField_9; }
	inline void set_U3CtouchesNumStateU3Ek__BackingField_9(int32_t value)
	{
		___U3CtouchesNumStateU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_activeTouches_10() { return static_cast<int32_t>(offsetof(Gesture_t2352305985, ___activeTouches_10)); }
	inline List_1_t328750215 * get_activeTouches_10() const { return ___activeTouches_10; }
	inline List_1_t328750215 ** get_address_of_activeTouches_10() { return &___activeTouches_10; }
	inline void set_activeTouches_10(List_1_t328750215 * value)
	{
		___activeTouches_10 = value;
		Il2CppCodeGenWriteBarrier((&___activeTouches_10), value);
	}

	inline static int32_t get_offset_of_cachedTransform_11() { return static_cast<int32_t>(offsetof(Gesture_t2352305985, ___cachedTransform_11)); }
	inline Transform_t3275118058 * get_cachedTransform_11() const { return ___cachedTransform_11; }
	inline Transform_t3275118058 ** get_address_of_cachedTransform_11() { return &___cachedTransform_11; }
	inline void set_cachedTransform_11(Transform_t3275118058 * value)
	{
		___cachedTransform_11 = value;
		Il2CppCodeGenWriteBarrier((&___cachedTransform_11), value);
	}

	inline static int32_t get_offset_of_advancedProps_12() { return static_cast<int32_t>(offsetof(Gesture_t2352305985, ___advancedProps_12)); }
	inline bool get_advancedProps_12() const { return ___advancedProps_12; }
	inline bool* get_address_of_advancedProps_12() { return &___advancedProps_12; }
	inline void set_advancedProps_12(bool value)
	{
		___advancedProps_12 = value;
	}

	inline static int32_t get_offset_of_minTouches_13() { return static_cast<int32_t>(offsetof(Gesture_t2352305985, ___minTouches_13)); }
	inline int32_t get_minTouches_13() const { return ___minTouches_13; }
	inline int32_t* get_address_of_minTouches_13() { return &___minTouches_13; }
	inline void set_minTouches_13(int32_t value)
	{
		___minTouches_13 = value;
	}

	inline static int32_t get_offset_of_maxTouches_14() { return static_cast<int32_t>(offsetof(Gesture_t2352305985, ___maxTouches_14)); }
	inline int32_t get_maxTouches_14() const { return ___maxTouches_14; }
	inline int32_t* get_address_of_maxTouches_14() { return &___maxTouches_14; }
	inline void set_maxTouches_14(int32_t value)
	{
		___maxTouches_14 = value;
	}

	inline static int32_t get_offset_of_combineTouches_15() { return static_cast<int32_t>(offsetof(Gesture_t2352305985, ___combineTouches_15)); }
	inline bool get_combineTouches_15() const { return ___combineTouches_15; }
	inline bool* get_address_of_combineTouches_15() { return &___combineTouches_15; }
	inline void set_combineTouches_15(bool value)
	{
		___combineTouches_15 = value;
	}

	inline static int32_t get_offset_of_combineTouchesInterval_16() { return static_cast<int32_t>(offsetof(Gesture_t2352305985, ___combineTouchesInterval_16)); }
	inline float get_combineTouchesInterval_16() const { return ___combineTouchesInterval_16; }
	inline float* get_address_of_combineTouchesInterval_16() { return &___combineTouchesInterval_16; }
	inline void set_combineTouchesInterval_16(float value)
	{
		___combineTouchesInterval_16 = value;
	}

	inline static int32_t get_offset_of_useSendMessage_17() { return static_cast<int32_t>(offsetof(Gesture_t2352305985, ___useSendMessage_17)); }
	inline bool get_useSendMessage_17() const { return ___useSendMessage_17; }
	inline bool* get_address_of_useSendMessage_17() { return &___useSendMessage_17; }
	inline void set_useSendMessage_17(bool value)
	{
		___useSendMessage_17 = value;
	}

	inline static int32_t get_offset_of_sendStateChangeMessages_18() { return static_cast<int32_t>(offsetof(Gesture_t2352305985, ___sendStateChangeMessages_18)); }
	inline bool get_sendStateChangeMessages_18() const { return ___sendStateChangeMessages_18; }
	inline bool* get_address_of_sendStateChangeMessages_18() { return &___sendStateChangeMessages_18; }
	inline void set_sendStateChangeMessages_18(bool value)
	{
		___sendStateChangeMessages_18 = value;
	}

	inline static int32_t get_offset_of_sendMessageTarget_19() { return static_cast<int32_t>(offsetof(Gesture_t2352305985, ___sendMessageTarget_19)); }
	inline GameObject_t1756533147 * get_sendMessageTarget_19() const { return ___sendMessageTarget_19; }
	inline GameObject_t1756533147 ** get_address_of_sendMessageTarget_19() { return &___sendMessageTarget_19; }
	inline void set_sendMessageTarget_19(GameObject_t1756533147 * value)
	{
		___sendMessageTarget_19 = value;
		Il2CppCodeGenWriteBarrier((&___sendMessageTarget_19), value);
	}

	inline static int32_t get_offset_of_requireGestureToFail_20() { return static_cast<int32_t>(offsetof(Gesture_t2352305985, ___requireGestureToFail_20)); }
	inline Gesture_t2352305985 * get_requireGestureToFail_20() const { return ___requireGestureToFail_20; }
	inline Gesture_t2352305985 ** get_address_of_requireGestureToFail_20() { return &___requireGestureToFail_20; }
	inline void set_requireGestureToFail_20(Gesture_t2352305985 * value)
	{
		___requireGestureToFail_20 = value;
		Il2CppCodeGenWriteBarrier((&___requireGestureToFail_20), value);
	}

	inline static int32_t get_offset_of_friendlyGestures_21() { return static_cast<int32_t>(offsetof(Gesture_t2352305985, ___friendlyGestures_21)); }
	inline List_1_t1721427117 * get_friendlyGestures_21() const { return ___friendlyGestures_21; }
	inline List_1_t1721427117 ** get_address_of_friendlyGestures_21() { return &___friendlyGestures_21; }
	inline void set_friendlyGestures_21(List_1_t1721427117 * value)
	{
		___friendlyGestures_21 = value;
		Il2CppCodeGenWriteBarrier((&___friendlyGestures_21), value);
	}

	inline static int32_t get_offset_of_numTouches_22() { return static_cast<int32_t>(offsetof(Gesture_t2352305985, ___numTouches_22)); }
	inline int32_t get_numTouches_22() const { return ___numTouches_22; }
	inline int32_t* get_address_of_numTouches_22() { return &___numTouches_22; }
	inline void set_numTouches_22(int32_t value)
	{
		___numTouches_22 = value;
	}

	inline static int32_t get_offset_of_layer_23() { return static_cast<int32_t>(offsetof(Gesture_t2352305985, ___layer_23)); }
	inline TouchLayer_t2635439978 * get_layer_23() const { return ___layer_23; }
	inline TouchLayer_t2635439978 ** get_address_of_layer_23() { return &___layer_23; }
	inline void set_layer_23(TouchLayer_t2635439978 * value)
	{
		___layer_23 = value;
		Il2CppCodeGenWriteBarrier((&___layer_23), value);
	}

	inline static int32_t get_offset_of_readonlyActiveTouches_24() { return static_cast<int32_t>(offsetof(Gesture_t2352305985, ___readonlyActiveTouches_24)); }
	inline ReadOnlyCollection_1_t1145414775 * get_readonlyActiveTouches_24() const { return ___readonlyActiveTouches_24; }
	inline ReadOnlyCollection_1_t1145414775 ** get_address_of_readonlyActiveTouches_24() { return &___readonlyActiveTouches_24; }
	inline void set_readonlyActiveTouches_24(ReadOnlyCollection_1_t1145414775 * value)
	{
		___readonlyActiveTouches_24 = value;
		Il2CppCodeGenWriteBarrier((&___readonlyActiveTouches_24), value);
	}

	inline static int32_t get_offset_of_touchSequence_25() { return static_cast<int32_t>(offsetof(Gesture_t2352305985, ___touchSequence_25)); }
	inline TimedSequence_1_t2737250283 * get_touchSequence_25() const { return ___touchSequence_25; }
	inline TimedSequence_1_t2737250283 ** get_address_of_touchSequence_25() { return &___touchSequence_25; }
	inline void set_touchSequence_25(TimedSequence_1_t2737250283 * value)
	{
		___touchSequence_25 = value;
		Il2CppCodeGenWriteBarrier((&___touchSequence_25), value);
	}

	inline static int32_t get_offset_of_gestureManagerInstance_26() { return static_cast<int32_t>(offsetof(Gesture_t2352305985, ___gestureManagerInstance_26)); }
	inline GestureManagerInstance_t505647059 * get_gestureManagerInstance_26() const { return ___gestureManagerInstance_26; }
	inline GestureManagerInstance_t505647059 ** get_address_of_gestureManagerInstance_26() { return &___gestureManagerInstance_26; }
	inline void set_gestureManagerInstance_26(GestureManagerInstance_t505647059 * value)
	{
		___gestureManagerInstance_26 = value;
		Il2CppCodeGenWriteBarrier((&___gestureManagerInstance_26), value);
	}

	inline static int32_t get_offset_of_delayedStateChange_27() { return static_cast<int32_t>(offsetof(Gesture_t2352305985, ___delayedStateChange_27)); }
	inline int32_t get_delayedStateChange_27() const { return ___delayedStateChange_27; }
	inline int32_t* get_address_of_delayedStateChange_27() { return &___delayedStateChange_27; }
	inline void set_delayedStateChange_27(int32_t value)
	{
		___delayedStateChange_27 = value;
	}

	inline static int32_t get_offset_of_requiredGestureFailed_28() { return static_cast<int32_t>(offsetof(Gesture_t2352305985, ___requiredGestureFailed_28)); }
	inline bool get_requiredGestureFailed_28() const { return ___requiredGestureFailed_28; }
	inline bool* get_address_of_requiredGestureFailed_28() { return &___requiredGestureFailed_28; }
	inline void set_requiredGestureFailed_28(bool value)
	{
		___requiredGestureFailed_28 = value;
	}

	inline static int32_t get_offset_of_state_29() { return static_cast<int32_t>(offsetof(Gesture_t2352305985, ___state_29)); }
	inline int32_t get_state_29() const { return ___state_29; }
	inline int32_t* get_address_of_state_29() { return &___state_29; }
	inline void set_state_29(int32_t value)
	{
		___state_29 = value;
	}

	inline static int32_t get_offset_of_cachedScreenPosition_30() { return static_cast<int32_t>(offsetof(Gesture_t2352305985, ___cachedScreenPosition_30)); }
	inline Vector2_t2243707579  get_cachedScreenPosition_30() const { return ___cachedScreenPosition_30; }
	inline Vector2_t2243707579 * get_address_of_cachedScreenPosition_30() { return &___cachedScreenPosition_30; }
	inline void set_cachedScreenPosition_30(Vector2_t2243707579  value)
	{
		___cachedScreenPosition_30 = value;
	}

	inline static int32_t get_offset_of_cachedPreviousScreenPosition_31() { return static_cast<int32_t>(offsetof(Gesture_t2352305985, ___cachedPreviousScreenPosition_31)); }
	inline Vector2_t2243707579  get_cachedPreviousScreenPosition_31() const { return ___cachedPreviousScreenPosition_31; }
	inline Vector2_t2243707579 * get_address_of_cachedPreviousScreenPosition_31() { return &___cachedPreviousScreenPosition_31; }
	inline void set_cachedPreviousScreenPosition_31(Vector2_t2243707579  value)
	{
		___cachedPreviousScreenPosition_31 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GESTURE_T2352305985_H
#ifndef RELEASEGESTURE_T248506278_H
#define RELEASEGESTURE_T248506278_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Gestures.ReleaseGesture
struct  ReleaseGesture_t248506278  : public Gesture_t2352305985
{
public:
	// System.EventHandler`1<System.EventArgs> TouchScript.Gestures.ReleaseGesture::releasedInvoker
	EventHandler_1_t1880931879 * ___releasedInvoker_33;
	// System.Boolean TouchScript.Gestures.ReleaseGesture::ignoreChildren
	bool ___ignoreChildren_34;

public:
	inline static int32_t get_offset_of_releasedInvoker_33() { return static_cast<int32_t>(offsetof(ReleaseGesture_t248506278, ___releasedInvoker_33)); }
	inline EventHandler_1_t1880931879 * get_releasedInvoker_33() const { return ___releasedInvoker_33; }
	inline EventHandler_1_t1880931879 ** get_address_of_releasedInvoker_33() { return &___releasedInvoker_33; }
	inline void set_releasedInvoker_33(EventHandler_1_t1880931879 * value)
	{
		___releasedInvoker_33 = value;
		Il2CppCodeGenWriteBarrier((&___releasedInvoker_33), value);
	}

	inline static int32_t get_offset_of_ignoreChildren_34() { return static_cast<int32_t>(offsetof(ReleaseGesture_t248506278, ___ignoreChildren_34)); }
	inline bool get_ignoreChildren_34() const { return ___ignoreChildren_34; }
	inline bool* get_address_of_ignoreChildren_34() { return &___ignoreChildren_34; }
	inline void set_ignoreChildren_34(bool value)
	{
		___ignoreChildren_34 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RELEASEGESTURE_T248506278_H
#ifndef TAPGESTURE_T556401502_H
#define TAPGESTURE_T556401502_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Gestures.TapGesture
struct  TapGesture_t556401502  : public Gesture_t2352305985
{
public:
	// System.EventHandler`1<System.EventArgs> TouchScript.Gestures.TapGesture::tappedInvoker
	EventHandler_1_t1880931879 * ___tappedInvoker_33;
	// System.Int32 TouchScript.Gestures.TapGesture::numberOfTapsRequired
	int32_t ___numberOfTapsRequired_34;
	// System.Single TouchScript.Gestures.TapGesture::timeLimit
	float ___timeLimit_35;
	// System.Single TouchScript.Gestures.TapGesture::distanceLimit
	float ___distanceLimit_36;
	// System.Single TouchScript.Gestures.TapGesture::distanceLimitInPixelsSquared
	float ___distanceLimitInPixelsSquared_37;
	// System.Boolean TouchScript.Gestures.TapGesture::isActive
	bool ___isActive_38;
	// System.Int32 TouchScript.Gestures.TapGesture::tapsDone
	int32_t ___tapsDone_39;
	// UnityEngine.Vector2 TouchScript.Gestures.TapGesture::startPosition
	Vector2_t2243707579  ___startPosition_40;
	// UnityEngine.Vector2 TouchScript.Gestures.TapGesture::totalMovement
	Vector2_t2243707579  ___totalMovement_41;

public:
	inline static int32_t get_offset_of_tappedInvoker_33() { return static_cast<int32_t>(offsetof(TapGesture_t556401502, ___tappedInvoker_33)); }
	inline EventHandler_1_t1880931879 * get_tappedInvoker_33() const { return ___tappedInvoker_33; }
	inline EventHandler_1_t1880931879 ** get_address_of_tappedInvoker_33() { return &___tappedInvoker_33; }
	inline void set_tappedInvoker_33(EventHandler_1_t1880931879 * value)
	{
		___tappedInvoker_33 = value;
		Il2CppCodeGenWriteBarrier((&___tappedInvoker_33), value);
	}

	inline static int32_t get_offset_of_numberOfTapsRequired_34() { return static_cast<int32_t>(offsetof(TapGesture_t556401502, ___numberOfTapsRequired_34)); }
	inline int32_t get_numberOfTapsRequired_34() const { return ___numberOfTapsRequired_34; }
	inline int32_t* get_address_of_numberOfTapsRequired_34() { return &___numberOfTapsRequired_34; }
	inline void set_numberOfTapsRequired_34(int32_t value)
	{
		___numberOfTapsRequired_34 = value;
	}

	inline static int32_t get_offset_of_timeLimit_35() { return static_cast<int32_t>(offsetof(TapGesture_t556401502, ___timeLimit_35)); }
	inline float get_timeLimit_35() const { return ___timeLimit_35; }
	inline float* get_address_of_timeLimit_35() { return &___timeLimit_35; }
	inline void set_timeLimit_35(float value)
	{
		___timeLimit_35 = value;
	}

	inline static int32_t get_offset_of_distanceLimit_36() { return static_cast<int32_t>(offsetof(TapGesture_t556401502, ___distanceLimit_36)); }
	inline float get_distanceLimit_36() const { return ___distanceLimit_36; }
	inline float* get_address_of_distanceLimit_36() { return &___distanceLimit_36; }
	inline void set_distanceLimit_36(float value)
	{
		___distanceLimit_36 = value;
	}

	inline static int32_t get_offset_of_distanceLimitInPixelsSquared_37() { return static_cast<int32_t>(offsetof(TapGesture_t556401502, ___distanceLimitInPixelsSquared_37)); }
	inline float get_distanceLimitInPixelsSquared_37() const { return ___distanceLimitInPixelsSquared_37; }
	inline float* get_address_of_distanceLimitInPixelsSquared_37() { return &___distanceLimitInPixelsSquared_37; }
	inline void set_distanceLimitInPixelsSquared_37(float value)
	{
		___distanceLimitInPixelsSquared_37 = value;
	}

	inline static int32_t get_offset_of_isActive_38() { return static_cast<int32_t>(offsetof(TapGesture_t556401502, ___isActive_38)); }
	inline bool get_isActive_38() const { return ___isActive_38; }
	inline bool* get_address_of_isActive_38() { return &___isActive_38; }
	inline void set_isActive_38(bool value)
	{
		___isActive_38 = value;
	}

	inline static int32_t get_offset_of_tapsDone_39() { return static_cast<int32_t>(offsetof(TapGesture_t556401502, ___tapsDone_39)); }
	inline int32_t get_tapsDone_39() const { return ___tapsDone_39; }
	inline int32_t* get_address_of_tapsDone_39() { return &___tapsDone_39; }
	inline void set_tapsDone_39(int32_t value)
	{
		___tapsDone_39 = value;
	}

	inline static int32_t get_offset_of_startPosition_40() { return static_cast<int32_t>(offsetof(TapGesture_t556401502, ___startPosition_40)); }
	inline Vector2_t2243707579  get_startPosition_40() const { return ___startPosition_40; }
	inline Vector2_t2243707579 * get_address_of_startPosition_40() { return &___startPosition_40; }
	inline void set_startPosition_40(Vector2_t2243707579  value)
	{
		___startPosition_40 = value;
	}

	inline static int32_t get_offset_of_totalMovement_41() { return static_cast<int32_t>(offsetof(TapGesture_t556401502, ___totalMovement_41)); }
	inline Vector2_t2243707579  get_totalMovement_41() const { return ___totalMovement_41; }
	inline Vector2_t2243707579 * get_address_of_totalMovement_41() { return &___totalMovement_41; }
	inline void set_totalMovement_41(Vector2_t2243707579  value)
	{
		___totalMovement_41 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAPGESTURE_T556401502_H
#ifndef FLICKGESTURE_T402498036_H
#define FLICKGESTURE_T402498036_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Gestures.FlickGesture
struct  FlickGesture_t402498036  : public Gesture_t2352305985
{
public:
	// System.EventHandler`1<System.EventArgs> TouchScript.Gestures.FlickGesture::flickedInvoker
	EventHandler_1_t1880931879 * ___flickedInvoker_33;
	// UnityEngine.Vector2 TouchScript.Gestures.FlickGesture::<ScreenFlickVector>k__BackingField
	Vector2_t2243707579  ___U3CScreenFlickVectorU3Ek__BackingField_34;
	// System.Single TouchScript.Gestures.FlickGesture::<ScreenFlickTime>k__BackingField
	float ___U3CScreenFlickTimeU3Ek__BackingField_35;
	// System.Single TouchScript.Gestures.FlickGesture::flickTime
	float ___flickTime_36;
	// System.Single TouchScript.Gestures.FlickGesture::minDistance
	float ___minDistance_37;
	// System.Single TouchScript.Gestures.FlickGesture::movementThreshold
	float ___movementThreshold_38;
	// TouchScript.Gestures.FlickGesture/GestureDirection TouchScript.Gestures.FlickGesture::direction
	int32_t ___direction_39;
	// System.Boolean TouchScript.Gestures.FlickGesture::moving
	bool ___moving_40;
	// UnityEngine.Vector2 TouchScript.Gestures.FlickGesture::movementBuffer
	Vector2_t2243707579  ___movementBuffer_41;
	// System.Boolean TouchScript.Gestures.FlickGesture::isActive
	bool ___isActive_42;
	// TouchScript.Utils.TimedSequence`1<UnityEngine.Vector2> TouchScript.Gestures.FlickGesture::deltaSequence
	TimedSequence_1_t4021328779 * ___deltaSequence_43;

public:
	inline static int32_t get_offset_of_flickedInvoker_33() { return static_cast<int32_t>(offsetof(FlickGesture_t402498036, ___flickedInvoker_33)); }
	inline EventHandler_1_t1880931879 * get_flickedInvoker_33() const { return ___flickedInvoker_33; }
	inline EventHandler_1_t1880931879 ** get_address_of_flickedInvoker_33() { return &___flickedInvoker_33; }
	inline void set_flickedInvoker_33(EventHandler_1_t1880931879 * value)
	{
		___flickedInvoker_33 = value;
		Il2CppCodeGenWriteBarrier((&___flickedInvoker_33), value);
	}

	inline static int32_t get_offset_of_U3CScreenFlickVectorU3Ek__BackingField_34() { return static_cast<int32_t>(offsetof(FlickGesture_t402498036, ___U3CScreenFlickVectorU3Ek__BackingField_34)); }
	inline Vector2_t2243707579  get_U3CScreenFlickVectorU3Ek__BackingField_34() const { return ___U3CScreenFlickVectorU3Ek__BackingField_34; }
	inline Vector2_t2243707579 * get_address_of_U3CScreenFlickVectorU3Ek__BackingField_34() { return &___U3CScreenFlickVectorU3Ek__BackingField_34; }
	inline void set_U3CScreenFlickVectorU3Ek__BackingField_34(Vector2_t2243707579  value)
	{
		___U3CScreenFlickVectorU3Ek__BackingField_34 = value;
	}

	inline static int32_t get_offset_of_U3CScreenFlickTimeU3Ek__BackingField_35() { return static_cast<int32_t>(offsetof(FlickGesture_t402498036, ___U3CScreenFlickTimeU3Ek__BackingField_35)); }
	inline float get_U3CScreenFlickTimeU3Ek__BackingField_35() const { return ___U3CScreenFlickTimeU3Ek__BackingField_35; }
	inline float* get_address_of_U3CScreenFlickTimeU3Ek__BackingField_35() { return &___U3CScreenFlickTimeU3Ek__BackingField_35; }
	inline void set_U3CScreenFlickTimeU3Ek__BackingField_35(float value)
	{
		___U3CScreenFlickTimeU3Ek__BackingField_35 = value;
	}

	inline static int32_t get_offset_of_flickTime_36() { return static_cast<int32_t>(offsetof(FlickGesture_t402498036, ___flickTime_36)); }
	inline float get_flickTime_36() const { return ___flickTime_36; }
	inline float* get_address_of_flickTime_36() { return &___flickTime_36; }
	inline void set_flickTime_36(float value)
	{
		___flickTime_36 = value;
	}

	inline static int32_t get_offset_of_minDistance_37() { return static_cast<int32_t>(offsetof(FlickGesture_t402498036, ___minDistance_37)); }
	inline float get_minDistance_37() const { return ___minDistance_37; }
	inline float* get_address_of_minDistance_37() { return &___minDistance_37; }
	inline void set_minDistance_37(float value)
	{
		___minDistance_37 = value;
	}

	inline static int32_t get_offset_of_movementThreshold_38() { return static_cast<int32_t>(offsetof(FlickGesture_t402498036, ___movementThreshold_38)); }
	inline float get_movementThreshold_38() const { return ___movementThreshold_38; }
	inline float* get_address_of_movementThreshold_38() { return &___movementThreshold_38; }
	inline void set_movementThreshold_38(float value)
	{
		___movementThreshold_38 = value;
	}

	inline static int32_t get_offset_of_direction_39() { return static_cast<int32_t>(offsetof(FlickGesture_t402498036, ___direction_39)); }
	inline int32_t get_direction_39() const { return ___direction_39; }
	inline int32_t* get_address_of_direction_39() { return &___direction_39; }
	inline void set_direction_39(int32_t value)
	{
		___direction_39 = value;
	}

	inline static int32_t get_offset_of_moving_40() { return static_cast<int32_t>(offsetof(FlickGesture_t402498036, ___moving_40)); }
	inline bool get_moving_40() const { return ___moving_40; }
	inline bool* get_address_of_moving_40() { return &___moving_40; }
	inline void set_moving_40(bool value)
	{
		___moving_40 = value;
	}

	inline static int32_t get_offset_of_movementBuffer_41() { return static_cast<int32_t>(offsetof(FlickGesture_t402498036, ___movementBuffer_41)); }
	inline Vector2_t2243707579  get_movementBuffer_41() const { return ___movementBuffer_41; }
	inline Vector2_t2243707579 * get_address_of_movementBuffer_41() { return &___movementBuffer_41; }
	inline void set_movementBuffer_41(Vector2_t2243707579  value)
	{
		___movementBuffer_41 = value;
	}

	inline static int32_t get_offset_of_isActive_42() { return static_cast<int32_t>(offsetof(FlickGesture_t402498036, ___isActive_42)); }
	inline bool get_isActive_42() const { return ___isActive_42; }
	inline bool* get_address_of_isActive_42() { return &___isActive_42; }
	inline void set_isActive_42(bool value)
	{
		___isActive_42 = value;
	}

	inline static int32_t get_offset_of_deltaSequence_43() { return static_cast<int32_t>(offsetof(FlickGesture_t402498036, ___deltaSequence_43)); }
	inline TimedSequence_1_t4021328779 * get_deltaSequence_43() const { return ___deltaSequence_43; }
	inline TimedSequence_1_t4021328779 ** get_address_of_deltaSequence_43() { return &___deltaSequence_43; }
	inline void set_deltaSequence_43(TimedSequence_1_t4021328779 * value)
	{
		___deltaSequence_43 = value;
		Il2CppCodeGenWriteBarrier((&___deltaSequence_43), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLICKGESTURE_T402498036_H
#ifndef METAGESTURE_T1110051842_H
#define METAGESTURE_T1110051842_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Gestures.MetaGesture
struct  MetaGesture_t1110051842  : public Gesture_t2352305985
{
public:
	// System.EventHandler`1<TouchScript.Gestures.MetaGestureEventArgs> TouchScript.Gestures.MetaGesture::touchBeganInvoker
	EventHandler_1_t3142866083 * ___touchBeganInvoker_36;
	// System.EventHandler`1<TouchScript.Gestures.MetaGestureEventArgs> TouchScript.Gestures.MetaGesture::touchMovedInvoker
	EventHandler_1_t3142866083 * ___touchMovedInvoker_37;
	// System.EventHandler`1<TouchScript.Gestures.MetaGestureEventArgs> TouchScript.Gestures.MetaGesture::touchEndedInvoker
	EventHandler_1_t3142866083 * ___touchEndedInvoker_38;
	// System.EventHandler`1<TouchScript.Gestures.MetaGestureEventArgs> TouchScript.Gestures.MetaGesture::touchCancelledInvoker
	EventHandler_1_t3142866083 * ___touchCancelledInvoker_39;

public:
	inline static int32_t get_offset_of_touchBeganInvoker_36() { return static_cast<int32_t>(offsetof(MetaGesture_t1110051842, ___touchBeganInvoker_36)); }
	inline EventHandler_1_t3142866083 * get_touchBeganInvoker_36() const { return ___touchBeganInvoker_36; }
	inline EventHandler_1_t3142866083 ** get_address_of_touchBeganInvoker_36() { return &___touchBeganInvoker_36; }
	inline void set_touchBeganInvoker_36(EventHandler_1_t3142866083 * value)
	{
		___touchBeganInvoker_36 = value;
		Il2CppCodeGenWriteBarrier((&___touchBeganInvoker_36), value);
	}

	inline static int32_t get_offset_of_touchMovedInvoker_37() { return static_cast<int32_t>(offsetof(MetaGesture_t1110051842, ___touchMovedInvoker_37)); }
	inline EventHandler_1_t3142866083 * get_touchMovedInvoker_37() const { return ___touchMovedInvoker_37; }
	inline EventHandler_1_t3142866083 ** get_address_of_touchMovedInvoker_37() { return &___touchMovedInvoker_37; }
	inline void set_touchMovedInvoker_37(EventHandler_1_t3142866083 * value)
	{
		___touchMovedInvoker_37 = value;
		Il2CppCodeGenWriteBarrier((&___touchMovedInvoker_37), value);
	}

	inline static int32_t get_offset_of_touchEndedInvoker_38() { return static_cast<int32_t>(offsetof(MetaGesture_t1110051842, ___touchEndedInvoker_38)); }
	inline EventHandler_1_t3142866083 * get_touchEndedInvoker_38() const { return ___touchEndedInvoker_38; }
	inline EventHandler_1_t3142866083 ** get_address_of_touchEndedInvoker_38() { return &___touchEndedInvoker_38; }
	inline void set_touchEndedInvoker_38(EventHandler_1_t3142866083 * value)
	{
		___touchEndedInvoker_38 = value;
		Il2CppCodeGenWriteBarrier((&___touchEndedInvoker_38), value);
	}

	inline static int32_t get_offset_of_touchCancelledInvoker_39() { return static_cast<int32_t>(offsetof(MetaGesture_t1110051842, ___touchCancelledInvoker_39)); }
	inline EventHandler_1_t3142866083 * get_touchCancelledInvoker_39() const { return ___touchCancelledInvoker_39; }
	inline EventHandler_1_t3142866083 ** get_address_of_touchCancelledInvoker_39() { return &___touchCancelledInvoker_39; }
	inline void set_touchCancelledInvoker_39(EventHandler_1_t3142866083 * value)
	{
		___touchCancelledInvoker_39 = value;
		Il2CppCodeGenWriteBarrier((&___touchCancelledInvoker_39), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METAGESTURE_T1110051842_H
#ifndef LONGPRESSGESTURE_T656184630_H
#define LONGPRESSGESTURE_T656184630_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Gestures.LongPressGesture
struct  LongPressGesture_t656184630  : public Gesture_t2352305985
{
public:
	// System.EventHandler`1<System.EventArgs> TouchScript.Gestures.LongPressGesture::longPressedInvoker
	EventHandler_1_t1880931879 * ___longPressedInvoker_33;
	// System.Single TouchScript.Gestures.LongPressGesture::timeToPress
	float ___timeToPress_34;
	// System.Single TouchScript.Gestures.LongPressGesture::distanceLimit
	float ___distanceLimit_35;
	// System.Single TouchScript.Gestures.LongPressGesture::distanceLimitInPixelsSquared
	float ___distanceLimitInPixelsSquared_36;
	// UnityEngine.Vector2 TouchScript.Gestures.LongPressGesture::totalMovement
	Vector2_t2243707579  ___totalMovement_37;

public:
	inline static int32_t get_offset_of_longPressedInvoker_33() { return static_cast<int32_t>(offsetof(LongPressGesture_t656184630, ___longPressedInvoker_33)); }
	inline EventHandler_1_t1880931879 * get_longPressedInvoker_33() const { return ___longPressedInvoker_33; }
	inline EventHandler_1_t1880931879 ** get_address_of_longPressedInvoker_33() { return &___longPressedInvoker_33; }
	inline void set_longPressedInvoker_33(EventHandler_1_t1880931879 * value)
	{
		___longPressedInvoker_33 = value;
		Il2CppCodeGenWriteBarrier((&___longPressedInvoker_33), value);
	}

	inline static int32_t get_offset_of_timeToPress_34() { return static_cast<int32_t>(offsetof(LongPressGesture_t656184630, ___timeToPress_34)); }
	inline float get_timeToPress_34() const { return ___timeToPress_34; }
	inline float* get_address_of_timeToPress_34() { return &___timeToPress_34; }
	inline void set_timeToPress_34(float value)
	{
		___timeToPress_34 = value;
	}

	inline static int32_t get_offset_of_distanceLimit_35() { return static_cast<int32_t>(offsetof(LongPressGesture_t656184630, ___distanceLimit_35)); }
	inline float get_distanceLimit_35() const { return ___distanceLimit_35; }
	inline float* get_address_of_distanceLimit_35() { return &___distanceLimit_35; }
	inline void set_distanceLimit_35(float value)
	{
		___distanceLimit_35 = value;
	}

	inline static int32_t get_offset_of_distanceLimitInPixelsSquared_36() { return static_cast<int32_t>(offsetof(LongPressGesture_t656184630, ___distanceLimitInPixelsSquared_36)); }
	inline float get_distanceLimitInPixelsSquared_36() const { return ___distanceLimitInPixelsSquared_36; }
	inline float* get_address_of_distanceLimitInPixelsSquared_36() { return &___distanceLimitInPixelsSquared_36; }
	inline void set_distanceLimitInPixelsSquared_36(float value)
	{
		___distanceLimitInPixelsSquared_36 = value;
	}

	inline static int32_t get_offset_of_totalMovement_37() { return static_cast<int32_t>(offsetof(LongPressGesture_t656184630, ___totalMovement_37)); }
	inline Vector2_t2243707579  get_totalMovement_37() const { return ___totalMovement_37; }
	inline Vector2_t2243707579 * get_address_of_totalMovement_37() { return &___totalMovement_37; }
	inline void set_totalMovement_37(Vector2_t2243707579  value)
	{
		___totalMovement_37 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LONGPRESSGESTURE_T656184630_H
#ifndef PRESSGESTURE_T582183752_H
#define PRESSGESTURE_T582183752_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Gestures.PressGesture
struct  PressGesture_t582183752  : public Gesture_t2352305985
{
public:
	// System.EventHandler`1<System.EventArgs> TouchScript.Gestures.PressGesture::pressedInvoker
	EventHandler_1_t1880931879 * ___pressedInvoker_33;
	// System.Boolean TouchScript.Gestures.PressGesture::ignoreChildren
	bool ___ignoreChildren_34;

public:
	inline static int32_t get_offset_of_pressedInvoker_33() { return static_cast<int32_t>(offsetof(PressGesture_t582183752, ___pressedInvoker_33)); }
	inline EventHandler_1_t1880931879 * get_pressedInvoker_33() const { return ___pressedInvoker_33; }
	inline EventHandler_1_t1880931879 ** get_address_of_pressedInvoker_33() { return &___pressedInvoker_33; }
	inline void set_pressedInvoker_33(EventHandler_1_t1880931879 * value)
	{
		___pressedInvoker_33 = value;
		Il2CppCodeGenWriteBarrier((&___pressedInvoker_33), value);
	}

	inline static int32_t get_offset_of_ignoreChildren_34() { return static_cast<int32_t>(offsetof(PressGesture_t582183752, ___ignoreChildren_34)); }
	inline bool get_ignoreChildren_34() const { return ___ignoreChildren_34; }
	inline bool* get_address_of_ignoreChildren_34() { return &___ignoreChildren_34; }
	inline void set_ignoreChildren_34(bool value)
	{
		___ignoreChildren_34 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRESSGESTURE_T582183752_H
#ifndef PINNEDTRASFORMGESTUREBASE_T3366405746_H
#define PINNEDTRASFORMGESTUREBASE_T3366405746_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Gestures.Base.PinnedTrasformGestureBase
struct  PinnedTrasformGestureBase_t3366405746  : public Gesture_t2352305985
{
public:
	// System.EventHandler`1<System.EventArgs> TouchScript.Gestures.Base.PinnedTrasformGestureBase::transformStartedInvoker
	EventHandler_1_t1880931879 * ___transformStartedInvoker_35;
	// System.EventHandler`1<System.EventArgs> TouchScript.Gestures.Base.PinnedTrasformGestureBase::transformedInvoker
	EventHandler_1_t1880931879 * ___transformedInvoker_36;
	// System.EventHandler`1<System.EventArgs> TouchScript.Gestures.Base.PinnedTrasformGestureBase::transformCompletedInvoker
	EventHandler_1_t1880931879 * ___transformCompletedInvoker_37;
	// System.Single TouchScript.Gestures.Base.PinnedTrasformGestureBase::screenTransformPixelThreshold
	float ___screenTransformPixelThreshold_38;
	// System.Single TouchScript.Gestures.Base.PinnedTrasformGestureBase::screenTransformPixelThresholdSquared
	float ___screenTransformPixelThresholdSquared_39;
	// UnityEngine.Collider TouchScript.Gestures.Base.PinnedTrasformGestureBase::cachedCollider
	Collider_t3497673348 * ___cachedCollider_40;
	// System.Single TouchScript.Gestures.Base.PinnedTrasformGestureBase::deltaRotation
	float ___deltaRotation_41;
	// System.Single TouchScript.Gestures.Base.PinnedTrasformGestureBase::deltaScale
	float ___deltaScale_42;
	// UnityEngine.Vector2 TouchScript.Gestures.Base.PinnedTrasformGestureBase::screenPixelTranslationBuffer
	Vector2_t2243707579  ___screenPixelTranslationBuffer_43;
	// System.Single TouchScript.Gestures.Base.PinnedTrasformGestureBase::screenPixelRotationBuffer
	float ___screenPixelRotationBuffer_44;
	// System.Single TouchScript.Gestures.Base.PinnedTrasformGestureBase::angleBuffer
	float ___angleBuffer_45;
	// System.Single TouchScript.Gestures.Base.PinnedTrasformGestureBase::screenPixelScalingBuffer
	float ___screenPixelScalingBuffer_46;
	// System.Single TouchScript.Gestures.Base.PinnedTrasformGestureBase::scaleBuffer
	float ___scaleBuffer_47;
	// System.Boolean TouchScript.Gestures.Base.PinnedTrasformGestureBase::isTransforming
	bool ___isTransforming_48;
	// TouchScript.Gestures.Base.PinnedTrasformGestureBase/TransformType TouchScript.Gestures.Base.PinnedTrasformGestureBase::type
	int32_t ___type_49;
	// System.Single TouchScript.Gestures.Base.PinnedTrasformGestureBase::screenTransformThreshold
	float ___screenTransformThreshold_50;

public:
	inline static int32_t get_offset_of_transformStartedInvoker_35() { return static_cast<int32_t>(offsetof(PinnedTrasformGestureBase_t3366405746, ___transformStartedInvoker_35)); }
	inline EventHandler_1_t1880931879 * get_transformStartedInvoker_35() const { return ___transformStartedInvoker_35; }
	inline EventHandler_1_t1880931879 ** get_address_of_transformStartedInvoker_35() { return &___transformStartedInvoker_35; }
	inline void set_transformStartedInvoker_35(EventHandler_1_t1880931879 * value)
	{
		___transformStartedInvoker_35 = value;
		Il2CppCodeGenWriteBarrier((&___transformStartedInvoker_35), value);
	}

	inline static int32_t get_offset_of_transformedInvoker_36() { return static_cast<int32_t>(offsetof(PinnedTrasformGestureBase_t3366405746, ___transformedInvoker_36)); }
	inline EventHandler_1_t1880931879 * get_transformedInvoker_36() const { return ___transformedInvoker_36; }
	inline EventHandler_1_t1880931879 ** get_address_of_transformedInvoker_36() { return &___transformedInvoker_36; }
	inline void set_transformedInvoker_36(EventHandler_1_t1880931879 * value)
	{
		___transformedInvoker_36 = value;
		Il2CppCodeGenWriteBarrier((&___transformedInvoker_36), value);
	}

	inline static int32_t get_offset_of_transformCompletedInvoker_37() { return static_cast<int32_t>(offsetof(PinnedTrasformGestureBase_t3366405746, ___transformCompletedInvoker_37)); }
	inline EventHandler_1_t1880931879 * get_transformCompletedInvoker_37() const { return ___transformCompletedInvoker_37; }
	inline EventHandler_1_t1880931879 ** get_address_of_transformCompletedInvoker_37() { return &___transformCompletedInvoker_37; }
	inline void set_transformCompletedInvoker_37(EventHandler_1_t1880931879 * value)
	{
		___transformCompletedInvoker_37 = value;
		Il2CppCodeGenWriteBarrier((&___transformCompletedInvoker_37), value);
	}

	inline static int32_t get_offset_of_screenTransformPixelThreshold_38() { return static_cast<int32_t>(offsetof(PinnedTrasformGestureBase_t3366405746, ___screenTransformPixelThreshold_38)); }
	inline float get_screenTransformPixelThreshold_38() const { return ___screenTransformPixelThreshold_38; }
	inline float* get_address_of_screenTransformPixelThreshold_38() { return &___screenTransformPixelThreshold_38; }
	inline void set_screenTransformPixelThreshold_38(float value)
	{
		___screenTransformPixelThreshold_38 = value;
	}

	inline static int32_t get_offset_of_screenTransformPixelThresholdSquared_39() { return static_cast<int32_t>(offsetof(PinnedTrasformGestureBase_t3366405746, ___screenTransformPixelThresholdSquared_39)); }
	inline float get_screenTransformPixelThresholdSquared_39() const { return ___screenTransformPixelThresholdSquared_39; }
	inline float* get_address_of_screenTransformPixelThresholdSquared_39() { return &___screenTransformPixelThresholdSquared_39; }
	inline void set_screenTransformPixelThresholdSquared_39(float value)
	{
		___screenTransformPixelThresholdSquared_39 = value;
	}

	inline static int32_t get_offset_of_cachedCollider_40() { return static_cast<int32_t>(offsetof(PinnedTrasformGestureBase_t3366405746, ___cachedCollider_40)); }
	inline Collider_t3497673348 * get_cachedCollider_40() const { return ___cachedCollider_40; }
	inline Collider_t3497673348 ** get_address_of_cachedCollider_40() { return &___cachedCollider_40; }
	inline void set_cachedCollider_40(Collider_t3497673348 * value)
	{
		___cachedCollider_40 = value;
		Il2CppCodeGenWriteBarrier((&___cachedCollider_40), value);
	}

	inline static int32_t get_offset_of_deltaRotation_41() { return static_cast<int32_t>(offsetof(PinnedTrasformGestureBase_t3366405746, ___deltaRotation_41)); }
	inline float get_deltaRotation_41() const { return ___deltaRotation_41; }
	inline float* get_address_of_deltaRotation_41() { return &___deltaRotation_41; }
	inline void set_deltaRotation_41(float value)
	{
		___deltaRotation_41 = value;
	}

	inline static int32_t get_offset_of_deltaScale_42() { return static_cast<int32_t>(offsetof(PinnedTrasformGestureBase_t3366405746, ___deltaScale_42)); }
	inline float get_deltaScale_42() const { return ___deltaScale_42; }
	inline float* get_address_of_deltaScale_42() { return &___deltaScale_42; }
	inline void set_deltaScale_42(float value)
	{
		___deltaScale_42 = value;
	}

	inline static int32_t get_offset_of_screenPixelTranslationBuffer_43() { return static_cast<int32_t>(offsetof(PinnedTrasformGestureBase_t3366405746, ___screenPixelTranslationBuffer_43)); }
	inline Vector2_t2243707579  get_screenPixelTranslationBuffer_43() const { return ___screenPixelTranslationBuffer_43; }
	inline Vector2_t2243707579 * get_address_of_screenPixelTranslationBuffer_43() { return &___screenPixelTranslationBuffer_43; }
	inline void set_screenPixelTranslationBuffer_43(Vector2_t2243707579  value)
	{
		___screenPixelTranslationBuffer_43 = value;
	}

	inline static int32_t get_offset_of_screenPixelRotationBuffer_44() { return static_cast<int32_t>(offsetof(PinnedTrasformGestureBase_t3366405746, ___screenPixelRotationBuffer_44)); }
	inline float get_screenPixelRotationBuffer_44() const { return ___screenPixelRotationBuffer_44; }
	inline float* get_address_of_screenPixelRotationBuffer_44() { return &___screenPixelRotationBuffer_44; }
	inline void set_screenPixelRotationBuffer_44(float value)
	{
		___screenPixelRotationBuffer_44 = value;
	}

	inline static int32_t get_offset_of_angleBuffer_45() { return static_cast<int32_t>(offsetof(PinnedTrasformGestureBase_t3366405746, ___angleBuffer_45)); }
	inline float get_angleBuffer_45() const { return ___angleBuffer_45; }
	inline float* get_address_of_angleBuffer_45() { return &___angleBuffer_45; }
	inline void set_angleBuffer_45(float value)
	{
		___angleBuffer_45 = value;
	}

	inline static int32_t get_offset_of_screenPixelScalingBuffer_46() { return static_cast<int32_t>(offsetof(PinnedTrasformGestureBase_t3366405746, ___screenPixelScalingBuffer_46)); }
	inline float get_screenPixelScalingBuffer_46() const { return ___screenPixelScalingBuffer_46; }
	inline float* get_address_of_screenPixelScalingBuffer_46() { return &___screenPixelScalingBuffer_46; }
	inline void set_screenPixelScalingBuffer_46(float value)
	{
		___screenPixelScalingBuffer_46 = value;
	}

	inline static int32_t get_offset_of_scaleBuffer_47() { return static_cast<int32_t>(offsetof(PinnedTrasformGestureBase_t3366405746, ___scaleBuffer_47)); }
	inline float get_scaleBuffer_47() const { return ___scaleBuffer_47; }
	inline float* get_address_of_scaleBuffer_47() { return &___scaleBuffer_47; }
	inline void set_scaleBuffer_47(float value)
	{
		___scaleBuffer_47 = value;
	}

	inline static int32_t get_offset_of_isTransforming_48() { return static_cast<int32_t>(offsetof(PinnedTrasformGestureBase_t3366405746, ___isTransforming_48)); }
	inline bool get_isTransforming_48() const { return ___isTransforming_48; }
	inline bool* get_address_of_isTransforming_48() { return &___isTransforming_48; }
	inline void set_isTransforming_48(bool value)
	{
		___isTransforming_48 = value;
	}

	inline static int32_t get_offset_of_type_49() { return static_cast<int32_t>(offsetof(PinnedTrasformGestureBase_t3366405746, ___type_49)); }
	inline int32_t get_type_49() const { return ___type_49; }
	inline int32_t* get_address_of_type_49() { return &___type_49; }
	inline void set_type_49(int32_t value)
	{
		___type_49 = value;
	}

	inline static int32_t get_offset_of_screenTransformThreshold_50() { return static_cast<int32_t>(offsetof(PinnedTrasformGestureBase_t3366405746, ___screenTransformThreshold_50)); }
	inline float get_screenTransformThreshold_50() const { return ___screenTransformThreshold_50; }
	inline float* get_address_of_screenTransformThreshold_50() { return &___screenTransformThreshold_50; }
	inline void set_screenTransformThreshold_50(float value)
	{
		___screenTransformThreshold_50 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PINNEDTRASFORMGESTUREBASE_T3366405746_H
#ifndef TRANSFORMGESTUREBASE_T91965108_H
#define TRANSFORMGESTUREBASE_T91965108_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Gestures.Base.TransformGestureBase
struct  TransformGestureBase_t91965108  : public Gesture_t2352305985
{
public:
	// System.EventHandler`1<System.EventArgs> TouchScript.Gestures.Base.TransformGestureBase::transformStartedInvoker
	EventHandler_1_t1880931879 * ___transformStartedInvoker_35;
	// System.EventHandler`1<System.EventArgs> TouchScript.Gestures.Base.TransformGestureBase::transformedInvoker
	EventHandler_1_t1880931879 * ___transformedInvoker_36;
	// System.EventHandler`1<System.EventArgs> TouchScript.Gestures.Base.TransformGestureBase::transformCompletedInvoker
	EventHandler_1_t1880931879 * ___transformCompletedInvoker_37;
	// System.Single TouchScript.Gestures.Base.TransformGestureBase::minScreenPointsPixelDistance
	float ___minScreenPointsPixelDistance_38;
	// System.Single TouchScript.Gestures.Base.TransformGestureBase::minScreenPointsPixelDistanceSquared
	float ___minScreenPointsPixelDistanceSquared_39;
	// System.Single TouchScript.Gestures.Base.TransformGestureBase::screenTransformPixelThreshold
	float ___screenTransformPixelThreshold_40;
	// System.Single TouchScript.Gestures.Base.TransformGestureBase::screenTransformPixelThresholdSquared
	float ___screenTransformPixelThresholdSquared_41;
	// UnityEngine.Vector3 TouchScript.Gestures.Base.TransformGestureBase::deltaPosition
	Vector3_t2243707580  ___deltaPosition_42;
	// System.Single TouchScript.Gestures.Base.TransformGestureBase::deltaRotation
	float ___deltaRotation_43;
	// System.Single TouchScript.Gestures.Base.TransformGestureBase::deltaScale
	float ___deltaScale_44;
	// UnityEngine.Vector2 TouchScript.Gestures.Base.TransformGestureBase::screenPixelTranslationBuffer
	Vector2_t2243707579  ___screenPixelTranslationBuffer_45;
	// System.Single TouchScript.Gestures.Base.TransformGestureBase::screenPixelRotationBuffer
	float ___screenPixelRotationBuffer_46;
	// System.Single TouchScript.Gestures.Base.TransformGestureBase::angleBuffer
	float ___angleBuffer_47;
	// System.Single TouchScript.Gestures.Base.TransformGestureBase::screenPixelScalingBuffer
	float ___screenPixelScalingBuffer_48;
	// System.Single TouchScript.Gestures.Base.TransformGestureBase::scaleBuffer
	float ___scaleBuffer_49;
	// System.Boolean TouchScript.Gestures.Base.TransformGestureBase::isTransforming
	bool ___isTransforming_50;
	// TouchScript.Gestures.Base.TransformGestureBase/TransformType TouchScript.Gestures.Base.TransformGestureBase::type
	int32_t ___type_51;
	// System.Single TouchScript.Gestures.Base.TransformGestureBase::minScreenPointsDistance
	float ___minScreenPointsDistance_52;
	// System.Single TouchScript.Gestures.Base.TransformGestureBase::screenTransformThreshold
	float ___screenTransformThreshold_53;

public:
	inline static int32_t get_offset_of_transformStartedInvoker_35() { return static_cast<int32_t>(offsetof(TransformGestureBase_t91965108, ___transformStartedInvoker_35)); }
	inline EventHandler_1_t1880931879 * get_transformStartedInvoker_35() const { return ___transformStartedInvoker_35; }
	inline EventHandler_1_t1880931879 ** get_address_of_transformStartedInvoker_35() { return &___transformStartedInvoker_35; }
	inline void set_transformStartedInvoker_35(EventHandler_1_t1880931879 * value)
	{
		___transformStartedInvoker_35 = value;
		Il2CppCodeGenWriteBarrier((&___transformStartedInvoker_35), value);
	}

	inline static int32_t get_offset_of_transformedInvoker_36() { return static_cast<int32_t>(offsetof(TransformGestureBase_t91965108, ___transformedInvoker_36)); }
	inline EventHandler_1_t1880931879 * get_transformedInvoker_36() const { return ___transformedInvoker_36; }
	inline EventHandler_1_t1880931879 ** get_address_of_transformedInvoker_36() { return &___transformedInvoker_36; }
	inline void set_transformedInvoker_36(EventHandler_1_t1880931879 * value)
	{
		___transformedInvoker_36 = value;
		Il2CppCodeGenWriteBarrier((&___transformedInvoker_36), value);
	}

	inline static int32_t get_offset_of_transformCompletedInvoker_37() { return static_cast<int32_t>(offsetof(TransformGestureBase_t91965108, ___transformCompletedInvoker_37)); }
	inline EventHandler_1_t1880931879 * get_transformCompletedInvoker_37() const { return ___transformCompletedInvoker_37; }
	inline EventHandler_1_t1880931879 ** get_address_of_transformCompletedInvoker_37() { return &___transformCompletedInvoker_37; }
	inline void set_transformCompletedInvoker_37(EventHandler_1_t1880931879 * value)
	{
		___transformCompletedInvoker_37 = value;
		Il2CppCodeGenWriteBarrier((&___transformCompletedInvoker_37), value);
	}

	inline static int32_t get_offset_of_minScreenPointsPixelDistance_38() { return static_cast<int32_t>(offsetof(TransformGestureBase_t91965108, ___minScreenPointsPixelDistance_38)); }
	inline float get_minScreenPointsPixelDistance_38() const { return ___minScreenPointsPixelDistance_38; }
	inline float* get_address_of_minScreenPointsPixelDistance_38() { return &___minScreenPointsPixelDistance_38; }
	inline void set_minScreenPointsPixelDistance_38(float value)
	{
		___minScreenPointsPixelDistance_38 = value;
	}

	inline static int32_t get_offset_of_minScreenPointsPixelDistanceSquared_39() { return static_cast<int32_t>(offsetof(TransformGestureBase_t91965108, ___minScreenPointsPixelDistanceSquared_39)); }
	inline float get_minScreenPointsPixelDistanceSquared_39() const { return ___minScreenPointsPixelDistanceSquared_39; }
	inline float* get_address_of_minScreenPointsPixelDistanceSquared_39() { return &___minScreenPointsPixelDistanceSquared_39; }
	inline void set_minScreenPointsPixelDistanceSquared_39(float value)
	{
		___minScreenPointsPixelDistanceSquared_39 = value;
	}

	inline static int32_t get_offset_of_screenTransformPixelThreshold_40() { return static_cast<int32_t>(offsetof(TransformGestureBase_t91965108, ___screenTransformPixelThreshold_40)); }
	inline float get_screenTransformPixelThreshold_40() const { return ___screenTransformPixelThreshold_40; }
	inline float* get_address_of_screenTransformPixelThreshold_40() { return &___screenTransformPixelThreshold_40; }
	inline void set_screenTransformPixelThreshold_40(float value)
	{
		___screenTransformPixelThreshold_40 = value;
	}

	inline static int32_t get_offset_of_screenTransformPixelThresholdSquared_41() { return static_cast<int32_t>(offsetof(TransformGestureBase_t91965108, ___screenTransformPixelThresholdSquared_41)); }
	inline float get_screenTransformPixelThresholdSquared_41() const { return ___screenTransformPixelThresholdSquared_41; }
	inline float* get_address_of_screenTransformPixelThresholdSquared_41() { return &___screenTransformPixelThresholdSquared_41; }
	inline void set_screenTransformPixelThresholdSquared_41(float value)
	{
		___screenTransformPixelThresholdSquared_41 = value;
	}

	inline static int32_t get_offset_of_deltaPosition_42() { return static_cast<int32_t>(offsetof(TransformGestureBase_t91965108, ___deltaPosition_42)); }
	inline Vector3_t2243707580  get_deltaPosition_42() const { return ___deltaPosition_42; }
	inline Vector3_t2243707580 * get_address_of_deltaPosition_42() { return &___deltaPosition_42; }
	inline void set_deltaPosition_42(Vector3_t2243707580  value)
	{
		___deltaPosition_42 = value;
	}

	inline static int32_t get_offset_of_deltaRotation_43() { return static_cast<int32_t>(offsetof(TransformGestureBase_t91965108, ___deltaRotation_43)); }
	inline float get_deltaRotation_43() const { return ___deltaRotation_43; }
	inline float* get_address_of_deltaRotation_43() { return &___deltaRotation_43; }
	inline void set_deltaRotation_43(float value)
	{
		___deltaRotation_43 = value;
	}

	inline static int32_t get_offset_of_deltaScale_44() { return static_cast<int32_t>(offsetof(TransformGestureBase_t91965108, ___deltaScale_44)); }
	inline float get_deltaScale_44() const { return ___deltaScale_44; }
	inline float* get_address_of_deltaScale_44() { return &___deltaScale_44; }
	inline void set_deltaScale_44(float value)
	{
		___deltaScale_44 = value;
	}

	inline static int32_t get_offset_of_screenPixelTranslationBuffer_45() { return static_cast<int32_t>(offsetof(TransformGestureBase_t91965108, ___screenPixelTranslationBuffer_45)); }
	inline Vector2_t2243707579  get_screenPixelTranslationBuffer_45() const { return ___screenPixelTranslationBuffer_45; }
	inline Vector2_t2243707579 * get_address_of_screenPixelTranslationBuffer_45() { return &___screenPixelTranslationBuffer_45; }
	inline void set_screenPixelTranslationBuffer_45(Vector2_t2243707579  value)
	{
		___screenPixelTranslationBuffer_45 = value;
	}

	inline static int32_t get_offset_of_screenPixelRotationBuffer_46() { return static_cast<int32_t>(offsetof(TransformGestureBase_t91965108, ___screenPixelRotationBuffer_46)); }
	inline float get_screenPixelRotationBuffer_46() const { return ___screenPixelRotationBuffer_46; }
	inline float* get_address_of_screenPixelRotationBuffer_46() { return &___screenPixelRotationBuffer_46; }
	inline void set_screenPixelRotationBuffer_46(float value)
	{
		___screenPixelRotationBuffer_46 = value;
	}

	inline static int32_t get_offset_of_angleBuffer_47() { return static_cast<int32_t>(offsetof(TransformGestureBase_t91965108, ___angleBuffer_47)); }
	inline float get_angleBuffer_47() const { return ___angleBuffer_47; }
	inline float* get_address_of_angleBuffer_47() { return &___angleBuffer_47; }
	inline void set_angleBuffer_47(float value)
	{
		___angleBuffer_47 = value;
	}

	inline static int32_t get_offset_of_screenPixelScalingBuffer_48() { return static_cast<int32_t>(offsetof(TransformGestureBase_t91965108, ___screenPixelScalingBuffer_48)); }
	inline float get_screenPixelScalingBuffer_48() const { return ___screenPixelScalingBuffer_48; }
	inline float* get_address_of_screenPixelScalingBuffer_48() { return &___screenPixelScalingBuffer_48; }
	inline void set_screenPixelScalingBuffer_48(float value)
	{
		___screenPixelScalingBuffer_48 = value;
	}

	inline static int32_t get_offset_of_scaleBuffer_49() { return static_cast<int32_t>(offsetof(TransformGestureBase_t91965108, ___scaleBuffer_49)); }
	inline float get_scaleBuffer_49() const { return ___scaleBuffer_49; }
	inline float* get_address_of_scaleBuffer_49() { return &___scaleBuffer_49; }
	inline void set_scaleBuffer_49(float value)
	{
		___scaleBuffer_49 = value;
	}

	inline static int32_t get_offset_of_isTransforming_50() { return static_cast<int32_t>(offsetof(TransformGestureBase_t91965108, ___isTransforming_50)); }
	inline bool get_isTransforming_50() const { return ___isTransforming_50; }
	inline bool* get_address_of_isTransforming_50() { return &___isTransforming_50; }
	inline void set_isTransforming_50(bool value)
	{
		___isTransforming_50 = value;
	}

	inline static int32_t get_offset_of_type_51() { return static_cast<int32_t>(offsetof(TransformGestureBase_t91965108, ___type_51)); }
	inline int32_t get_type_51() const { return ___type_51; }
	inline int32_t* get_address_of_type_51() { return &___type_51; }
	inline void set_type_51(int32_t value)
	{
		___type_51 = value;
	}

	inline static int32_t get_offset_of_minScreenPointsDistance_52() { return static_cast<int32_t>(offsetof(TransformGestureBase_t91965108, ___minScreenPointsDistance_52)); }
	inline float get_minScreenPointsDistance_52() const { return ___minScreenPointsDistance_52; }
	inline float* get_address_of_minScreenPointsDistance_52() { return &___minScreenPointsDistance_52; }
	inline void set_minScreenPointsDistance_52(float value)
	{
		___minScreenPointsDistance_52 = value;
	}

	inline static int32_t get_offset_of_screenTransformThreshold_53() { return static_cast<int32_t>(offsetof(TransformGestureBase_t91965108, ___screenTransformThreshold_53)); }
	inline float get_screenTransformThreshold_53() const { return ___screenTransformThreshold_53; }
	inline float* get_address_of_screenTransformThreshold_53() { return &___screenTransformThreshold_53; }
	inline void set_screenTransformThreshold_53(float value)
	{
		___screenTransformThreshold_53 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMGESTUREBASE_T91965108_H
#ifndef UIGESTURE_T3400500675_H
#define UIGESTURE_T3400500675_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Gestures.UI.UIGesture
struct  UIGesture_t3400500675  : public Gesture_t2352305985
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,TouchScript.Gestures.UI.UIGesture/TouchData> TouchScript.Gestures.UI.UIGesture::pointerData
	Dictionary_2_t2888425610 * ___pointerData_32;

public:
	inline static int32_t get_offset_of_pointerData_32() { return static_cast<int32_t>(offsetof(UIGesture_t3400500675, ___pointerData_32)); }
	inline Dictionary_2_t2888425610 * get_pointerData_32() const { return ___pointerData_32; }
	inline Dictionary_2_t2888425610 ** get_address_of_pointerData_32() { return &___pointerData_32; }
	inline void set_pointerData_32(Dictionary_2_t2888425610 * value)
	{
		___pointerData_32 = value;
		Il2CppCodeGenWriteBarrier((&___pointerData_32), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIGESTURE_T3400500675_H
#ifndef TOUCHSCRIPTINPUTMODULE_T3561165032_H
#define TOUCHSCRIPTINPUTMODULE_T3561165032_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Behaviors.TouchScriptInputModule
struct  TouchScriptInputModule_t3561165032  : public BaseInputModule_t1295781545
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.EventSystems.PointerEventData> TouchScript.Behaviors.TouchScriptInputModule::pointerEvents
	Dictionary_2_t607610358 * ___pointerEvents_8;
	// System.String TouchScript.Behaviors.TouchScriptInputModule::horizontalAxis
	String_t* ___horizontalAxis_9;
	// System.String TouchScript.Behaviors.TouchScriptInputModule::verticalAxis
	String_t* ___verticalAxis_10;
	// System.String TouchScript.Behaviors.TouchScriptInputModule::submitButton
	String_t* ___submitButton_11;
	// System.String TouchScript.Behaviors.TouchScriptInputModule::cancelButton
	String_t* ___cancelButton_12;
	// System.Single TouchScript.Behaviors.TouchScriptInputModule::inputActionsPerSecond
	float ___inputActionsPerSecond_13;
	// System.Single TouchScript.Behaviors.TouchScriptInputModule::repeatDelay
	float ___repeatDelay_14;
	// System.Single TouchScript.Behaviors.TouchScriptInputModule::nextActionTime
	float ___nextActionTime_15;
	// UnityEngine.EventSystems.MoveDirection TouchScript.Behaviors.TouchScriptInputModule::lastMoveDirection
	int32_t ___lastMoveDirection_16;
	// System.Single TouchScript.Behaviors.TouchScriptInputModule::lastMoveStartTime
	float ___lastMoveStartTime_17;

public:
	inline static int32_t get_offset_of_pointerEvents_8() { return static_cast<int32_t>(offsetof(TouchScriptInputModule_t3561165032, ___pointerEvents_8)); }
	inline Dictionary_2_t607610358 * get_pointerEvents_8() const { return ___pointerEvents_8; }
	inline Dictionary_2_t607610358 ** get_address_of_pointerEvents_8() { return &___pointerEvents_8; }
	inline void set_pointerEvents_8(Dictionary_2_t607610358 * value)
	{
		___pointerEvents_8 = value;
		Il2CppCodeGenWriteBarrier((&___pointerEvents_8), value);
	}

	inline static int32_t get_offset_of_horizontalAxis_9() { return static_cast<int32_t>(offsetof(TouchScriptInputModule_t3561165032, ___horizontalAxis_9)); }
	inline String_t* get_horizontalAxis_9() const { return ___horizontalAxis_9; }
	inline String_t** get_address_of_horizontalAxis_9() { return &___horizontalAxis_9; }
	inline void set_horizontalAxis_9(String_t* value)
	{
		___horizontalAxis_9 = value;
		Il2CppCodeGenWriteBarrier((&___horizontalAxis_9), value);
	}

	inline static int32_t get_offset_of_verticalAxis_10() { return static_cast<int32_t>(offsetof(TouchScriptInputModule_t3561165032, ___verticalAxis_10)); }
	inline String_t* get_verticalAxis_10() const { return ___verticalAxis_10; }
	inline String_t** get_address_of_verticalAxis_10() { return &___verticalAxis_10; }
	inline void set_verticalAxis_10(String_t* value)
	{
		___verticalAxis_10 = value;
		Il2CppCodeGenWriteBarrier((&___verticalAxis_10), value);
	}

	inline static int32_t get_offset_of_submitButton_11() { return static_cast<int32_t>(offsetof(TouchScriptInputModule_t3561165032, ___submitButton_11)); }
	inline String_t* get_submitButton_11() const { return ___submitButton_11; }
	inline String_t** get_address_of_submitButton_11() { return &___submitButton_11; }
	inline void set_submitButton_11(String_t* value)
	{
		___submitButton_11 = value;
		Il2CppCodeGenWriteBarrier((&___submitButton_11), value);
	}

	inline static int32_t get_offset_of_cancelButton_12() { return static_cast<int32_t>(offsetof(TouchScriptInputModule_t3561165032, ___cancelButton_12)); }
	inline String_t* get_cancelButton_12() const { return ___cancelButton_12; }
	inline String_t** get_address_of_cancelButton_12() { return &___cancelButton_12; }
	inline void set_cancelButton_12(String_t* value)
	{
		___cancelButton_12 = value;
		Il2CppCodeGenWriteBarrier((&___cancelButton_12), value);
	}

	inline static int32_t get_offset_of_inputActionsPerSecond_13() { return static_cast<int32_t>(offsetof(TouchScriptInputModule_t3561165032, ___inputActionsPerSecond_13)); }
	inline float get_inputActionsPerSecond_13() const { return ___inputActionsPerSecond_13; }
	inline float* get_address_of_inputActionsPerSecond_13() { return &___inputActionsPerSecond_13; }
	inline void set_inputActionsPerSecond_13(float value)
	{
		___inputActionsPerSecond_13 = value;
	}

	inline static int32_t get_offset_of_repeatDelay_14() { return static_cast<int32_t>(offsetof(TouchScriptInputModule_t3561165032, ___repeatDelay_14)); }
	inline float get_repeatDelay_14() const { return ___repeatDelay_14; }
	inline float* get_address_of_repeatDelay_14() { return &___repeatDelay_14; }
	inline void set_repeatDelay_14(float value)
	{
		___repeatDelay_14 = value;
	}

	inline static int32_t get_offset_of_nextActionTime_15() { return static_cast<int32_t>(offsetof(TouchScriptInputModule_t3561165032, ___nextActionTime_15)); }
	inline float get_nextActionTime_15() const { return ___nextActionTime_15; }
	inline float* get_address_of_nextActionTime_15() { return &___nextActionTime_15; }
	inline void set_nextActionTime_15(float value)
	{
		___nextActionTime_15 = value;
	}

	inline static int32_t get_offset_of_lastMoveDirection_16() { return static_cast<int32_t>(offsetof(TouchScriptInputModule_t3561165032, ___lastMoveDirection_16)); }
	inline int32_t get_lastMoveDirection_16() const { return ___lastMoveDirection_16; }
	inline int32_t* get_address_of_lastMoveDirection_16() { return &___lastMoveDirection_16; }
	inline void set_lastMoveDirection_16(int32_t value)
	{
		___lastMoveDirection_16 = value;
	}

	inline static int32_t get_offset_of_lastMoveStartTime_17() { return static_cast<int32_t>(offsetof(TouchScriptInputModule_t3561165032, ___lastMoveStartTime_17)); }
	inline float get_lastMoveStartTime_17() const { return ___lastMoveStartTime_17; }
	inline float* get_address_of_lastMoveStartTime_17() { return &___lastMoveStartTime_17; }
	inline void set_lastMoveStartTime_17(float value)
	{
		___lastMoveStartTime_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHSCRIPTINPUTMODULE_T3561165032_H
#ifndef PINNEDTRANSFORMGESTURE_T2478931093_H
#define PINNEDTRANSFORMGESTURE_T2478931093_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Gestures.PinnedTransformGesture
struct  PinnedTransformGesture_t2478931093  : public PinnedTrasformGestureBase_t3366405746
{
public:
	// TouchScript.Gestures.PinnedTransformGesture/ProjectionType TouchScript.Gestures.PinnedTransformGesture::projection
	int32_t ___projection_51;
	// UnityEngine.Vector3 TouchScript.Gestures.PinnedTransformGesture::projectionPlaneNormal
	Vector3_t2243707580  ___projectionPlaneNormal_52;
	// TouchScript.Layers.TouchLayer TouchScript.Gestures.PinnedTransformGesture::projectionLayer
	TouchLayer_t2635439978 * ___projectionLayer_53;
	// UnityEngine.Plane TouchScript.Gestures.PinnedTransformGesture::transformPlane
	Plane_t3727654732  ___transformPlane_54;

public:
	inline static int32_t get_offset_of_projection_51() { return static_cast<int32_t>(offsetof(PinnedTransformGesture_t2478931093, ___projection_51)); }
	inline int32_t get_projection_51() const { return ___projection_51; }
	inline int32_t* get_address_of_projection_51() { return &___projection_51; }
	inline void set_projection_51(int32_t value)
	{
		___projection_51 = value;
	}

	inline static int32_t get_offset_of_projectionPlaneNormal_52() { return static_cast<int32_t>(offsetof(PinnedTransformGesture_t2478931093, ___projectionPlaneNormal_52)); }
	inline Vector3_t2243707580  get_projectionPlaneNormal_52() const { return ___projectionPlaneNormal_52; }
	inline Vector3_t2243707580 * get_address_of_projectionPlaneNormal_52() { return &___projectionPlaneNormal_52; }
	inline void set_projectionPlaneNormal_52(Vector3_t2243707580  value)
	{
		___projectionPlaneNormal_52 = value;
	}

	inline static int32_t get_offset_of_projectionLayer_53() { return static_cast<int32_t>(offsetof(PinnedTransformGesture_t2478931093, ___projectionLayer_53)); }
	inline TouchLayer_t2635439978 * get_projectionLayer_53() const { return ___projectionLayer_53; }
	inline TouchLayer_t2635439978 ** get_address_of_projectionLayer_53() { return &___projectionLayer_53; }
	inline void set_projectionLayer_53(TouchLayer_t2635439978 * value)
	{
		___projectionLayer_53 = value;
		Il2CppCodeGenWriteBarrier((&___projectionLayer_53), value);
	}

	inline static int32_t get_offset_of_transformPlane_54() { return static_cast<int32_t>(offsetof(PinnedTransformGesture_t2478931093, ___transformPlane_54)); }
	inline Plane_t3727654732  get_transformPlane_54() const { return ___transformPlane_54; }
	inline Plane_t3727654732 * get_address_of_transformPlane_54() { return &___transformPlane_54; }
	inline void set_transformPlane_54(Plane_t3727654732  value)
	{
		___transformPlane_54 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PINNEDTRANSFORMGESTURE_T2478931093_H
#ifndef SCREENTRANSFORMGESTURE_T3088562929_H
#define SCREENTRANSFORMGESTURE_T3088562929_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Gestures.ScreenTransformGesture
struct  ScreenTransformGesture_t3088562929  : public TransformGestureBase_t91965108
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENTRANSFORMGESTURE_T3088562929_H
#ifndef TRANSFORMGESTURE_T780972309_H
#define TRANSFORMGESTURE_T780972309_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Gestures.TransformGesture
struct  TransformGesture_t780972309  : public TransformGestureBase_t91965108
{
public:
	// TouchScript.Gestures.TransformGesture/ProjectionType TouchScript.Gestures.TransformGesture::projection
	int32_t ___projection_54;
	// UnityEngine.Vector3 TouchScript.Gestures.TransformGesture::projectionPlaneNormal
	Vector3_t2243707580  ___projectionPlaneNormal_55;
	// TouchScript.Layers.TouchLayer TouchScript.Gestures.TransformGesture::projectionLayer
	TouchLayer_t2635439978 * ___projectionLayer_56;
	// UnityEngine.Plane TouchScript.Gestures.TransformGesture::transformPlane
	Plane_t3727654732  ___transformPlane_57;

public:
	inline static int32_t get_offset_of_projection_54() { return static_cast<int32_t>(offsetof(TransformGesture_t780972309, ___projection_54)); }
	inline int32_t get_projection_54() const { return ___projection_54; }
	inline int32_t* get_address_of_projection_54() { return &___projection_54; }
	inline void set_projection_54(int32_t value)
	{
		___projection_54 = value;
	}

	inline static int32_t get_offset_of_projectionPlaneNormal_55() { return static_cast<int32_t>(offsetof(TransformGesture_t780972309, ___projectionPlaneNormal_55)); }
	inline Vector3_t2243707580  get_projectionPlaneNormal_55() const { return ___projectionPlaneNormal_55; }
	inline Vector3_t2243707580 * get_address_of_projectionPlaneNormal_55() { return &___projectionPlaneNormal_55; }
	inline void set_projectionPlaneNormal_55(Vector3_t2243707580  value)
	{
		___projectionPlaneNormal_55 = value;
	}

	inline static int32_t get_offset_of_projectionLayer_56() { return static_cast<int32_t>(offsetof(TransformGesture_t780972309, ___projectionLayer_56)); }
	inline TouchLayer_t2635439978 * get_projectionLayer_56() const { return ___projectionLayer_56; }
	inline TouchLayer_t2635439978 ** get_address_of_projectionLayer_56() { return &___projectionLayer_56; }
	inline void set_projectionLayer_56(TouchLayer_t2635439978 * value)
	{
		___projectionLayer_56 = value;
		Il2CppCodeGenWriteBarrier((&___projectionLayer_56), value);
	}

	inline static int32_t get_offset_of_transformPlane_57() { return static_cast<int32_t>(offsetof(TransformGesture_t780972309, ___transformPlane_57)); }
	inline Plane_t3727654732  get_transformPlane_57() const { return ___transformPlane_57; }
	inline Plane_t3727654732 * get_address_of_transformPlane_57() { return &___transformPlane_57; }
	inline void set_transformPlane_57(Plane_t3727654732  value)
	{
		___transformPlane_57 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMGESTURE_T780972309_H
#ifndef CLUSTEREDSCREENTRANSFORMGESTURE_T1866155674_H
#define CLUSTEREDSCREENTRANSFORMGESTURE_T1866155674_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Gestures.Clustered.ClusteredScreenTransformGesture
struct  ClusteredScreenTransformGesture_t1866155674  : public ScreenTransformGesture_t3088562929
{
public:
	// TouchScript.Clusters.Clusters TouchScript.Gestures.Clustered.ClusteredScreenTransformGesture::clusters
	Clusters_t4089973855 * ___clusters_54;

public:
	inline static int32_t get_offset_of_clusters_54() { return static_cast<int32_t>(offsetof(ClusteredScreenTransformGesture_t1866155674, ___clusters_54)); }
	inline Clusters_t4089973855 * get_clusters_54() const { return ___clusters_54; }
	inline Clusters_t4089973855 ** get_address_of_clusters_54() { return &___clusters_54; }
	inline void set_clusters_54(Clusters_t4089973855 * value)
	{
		___clusters_54 = value;
		Il2CppCodeGenWriteBarrier((&___clusters_54), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLUSTEREDSCREENTRANSFORMGESTURE_T1866155674_H
#ifndef CLUSTEREDPINNEDTRANSFORMGESTURE_T2370140594_H
#define CLUSTEREDPINNEDTRANSFORMGESTURE_T2370140594_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Gestures.Clustered.ClusteredPinnedTransformGesture
struct  ClusteredPinnedTransformGesture_t2370140594  : public PinnedTransformGesture_t2478931093
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLUSTEREDPINNEDTRANSFORMGESTURE_T2370140594_H
#ifndef CLUSTEREDTRANSFORMGESTURE_T1600768702_H
#define CLUSTEREDTRANSFORMGESTURE_T1600768702_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchScript.Gestures.Clustered.ClusteredTransformGesture
struct  ClusteredTransformGesture_t1600768702  : public TransformGesture_t780972309
{
public:
	// TouchScript.Clusters.Clusters TouchScript.Gestures.Clustered.ClusteredTransformGesture::clusters
	Clusters_t4089973855 * ___clusters_58;

public:
	inline static int32_t get_offset_of_clusters_58() { return static_cast<int32_t>(offsetof(ClusteredTransformGesture_t1600768702, ___clusters_58)); }
	inline Clusters_t4089973855 * get_clusters_58() const { return ___clusters_58; }
	inline Clusters_t4089973855 ** get_address_of_clusters_58() { return &___clusters_58; }
	inline void set_clusters_58(Clusters_t4089973855 * value)
	{
		___clusters_58 = value;
		Il2CppCodeGenWriteBarrier((&___clusters_58), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLUSTEREDTRANSFORMGESTURE_T1600768702_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3100 = { sizeof (fsDataType_t1645355485)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3100[8] = 
{
	fsDataType_t1645355485::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3101 = { sizeof (fsData_t2583805605), -1, sizeof(fsData_t2583805605_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3101[4] = 
{
	fsData_t2583805605::get_offset_of__value_0(),
	fsData_t2583805605_StaticFields::get_offset_of_True_1(),
	fsData_t2583805605_StaticFields::get_offset_of_False_2(),
	fsData_t2583805605_StaticFields::get_offset_of_Null_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3102 = { sizeof (fsDirectConverter_t763460818), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3103 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3104 = { sizeof (fsMissingVersionConstructorException_t849263018), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3105 = { sizeof (fsDuplicateVersionNameException_t4001675182), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3106 = { sizeof (fsIgnoreAttribute_t839965207), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3107 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3108 = { sizeof (fsSerializationCallbackProcessor_t357373768), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3109 = { sizeof (fsJsonParser_t1906440848), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3109[3] = 
{
	fsJsonParser_t1906440848::get_offset_of__start_0(),
	fsJsonParser_t1906440848::get_offset_of__input_1(),
	fsJsonParser_t1906440848::get_offset_of__cachedStringBuilder_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3110 = { sizeof (fsJsonPrinter_t3780364767), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3111 = { sizeof (fsMemberSerialization_t691367231)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3111[4] = 
{
	fsMemberSerialization_t691367231::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3112 = { sizeof (fsObjectAttribute_t2382840370), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3112[5] = 
{
	fsObjectAttribute_t2382840370::get_offset_of_PreviousModels_0(),
	fsObjectAttribute_t2382840370::get_offset_of_VersionString_1(),
	fsObjectAttribute_t2382840370::get_offset_of_MemberSerialization_2(),
	fsObjectAttribute_t2382840370::get_offset_of_Converter_3(),
	fsObjectAttribute_t2382840370::get_offset_of_Processor_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3113 = { sizeof (fsObjectProcessor_t2686254344), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3114 = { sizeof (fsPropertyAttribute_t4237399860), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3114[1] = 
{
	fsPropertyAttribute_t4237399860::get_offset_of_Name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3115 = { sizeof (fsResult_t862419890)+ sizeof (RuntimeObject), -1, sizeof(fsResult_t862419890_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3115[4] = 
{
	fsResult_t862419890_StaticFields::get_offset_of_EmptyStringArray_0(),
	fsResult_t862419890::get_offset_of__success_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	fsResult_t862419890::get_offset_of__messages_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	fsResult_t862419890_StaticFields::get_offset_of_Success_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3116 = { sizeof (fsSerializer_t4193731081), -1, sizeof(fsSerializer_t4193731081_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3116[14] = 
{
	fsSerializer_t4193731081_StaticFields::get_offset_of__reservedKeywords_0(),
	0,
	0,
	0,
	0,
	0,
	fsSerializer_t4193731081::get_offset_of__cachedConverters_6(),
	fsSerializer_t4193731081::get_offset_of__cachedProcessors_7(),
	fsSerializer_t4193731081::get_offset_of__availableConverters_8(),
	fsSerializer_t4193731081::get_offset_of__availableDirectConverters_9(),
	fsSerializer_t4193731081::get_offset_of__processors_10(),
	fsSerializer_t4193731081::get_offset_of__references_11(),
	fsSerializer_t4193731081::get_offset_of__lazyReferenceWriter_12(),
	fsSerializer_t4193731081::get_offset_of_Context_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3117 = { sizeof (fsLazyCycleDefinitionWriter_t2327014926), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3117[2] = 
{
	fsLazyCycleDefinitionWriter_t2327014926::get_offset_of__pendingDefinitions_0(),
	fsLazyCycleDefinitionWriter_t2327014926::get_offset_of__references_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3118 = { sizeof (fsCyclicReferenceManager_t1995018378), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3118[4] = 
{
	fsCyclicReferenceManager_t1995018378::get_offset_of__objectIds_0(),
	fsCyclicReferenceManager_t1995018378::get_offset_of__nextId_1(),
	fsCyclicReferenceManager_t1995018378::get_offset_of__marked_2(),
	fsCyclicReferenceManager_t1995018378::get_offset_of__depth_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3119 = { sizeof (ObjectReferenceEqualityComparator_t3520131135), -1, sizeof(ObjectReferenceEqualityComparator_t3520131135_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3119[1] = 
{
	ObjectReferenceEqualityComparator_t3520131135_StaticFields::get_offset_of_Instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3120 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3120[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3121 = { sizeof (fsOption_t670623048), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3122 = { sizeof (fsPortableReflection_t2923195415), -1, sizeof(fsPortableReflection_t2923195415_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3122[3] = 
{
	fsPortableReflection_t2923195415_StaticFields::get_offset_of_EmptyTypes_0(),
	fsPortableReflection_t2923195415_StaticFields::get_offset_of__cachedAttributeQueries_1(),
	fsPortableReflection_t2923195415_StaticFields::get_offset_of_DeclaredFlags_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3123 = { sizeof (AttributeQuery_t604298480)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3123[2] = 
{
	AttributeQuery_t604298480::get_offset_of_MemberInfo_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AttributeQuery_t604298480::get_offset_of_AttributeType_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3124 = { sizeof (AttributeQueryComparator_t4230352628), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3125 = { sizeof (U3CGetFlattenedMethodsU3Ec__Iterator0_t3009184096), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3125[8] = 
{
	U3CGetFlattenedMethodsU3Ec__Iterator0_t3009184096::get_offset_of_type_0(),
	U3CGetFlattenedMethodsU3Ec__Iterator0_t3009184096::get_offset_of_U3CmethodsU3E__1_1(),
	U3CGetFlattenedMethodsU3Ec__Iterator0_t3009184096::get_offset_of_U3CiU3E__2_2(),
	U3CGetFlattenedMethodsU3Ec__Iterator0_t3009184096::get_offset_of_methodName_3(),
	U3CGetFlattenedMethodsU3Ec__Iterator0_t3009184096::get_offset_of_U24current_4(),
	U3CGetFlattenedMethodsU3Ec__Iterator0_t3009184096::get_offset_of_U24disposing_5(),
	U3CGetFlattenedMethodsU3Ec__Iterator0_t3009184096::get_offset_of_U3CU24U3Etype_6(),
	U3CGetFlattenedMethodsU3Ec__Iterator0_t3009184096::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3126 = { sizeof (fsTypeExtensions_t260565267), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3127 = { sizeof (U3CCSharpNameU3Ec__AnonStorey0_t3839091716), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3127[1] = 
{
	U3CCSharpNameU3Ec__AnonStorey0_t3839091716::get_offset_of_includeNamespace_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3128 = { sizeof (fsVersionedType_t654750358)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3128[3] = 
{
	fsVersionedType_t654750358::get_offset_of_Ancestors_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	fsVersionedType_t654750358::get_offset_of_VersionString_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	fsVersionedType_t654750358::get_offset_of_ModelType_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3129 = { sizeof (fsVersionManager_t2158286414), -1, sizeof(fsVersionManager_t2158286414_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3129[1] = 
{
	fsVersionManager_t2158286414_StaticFields::get_offset_of__cache_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3130 = { sizeof (fsMetaProperty_t2249223145), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3130[7] = 
{
	fsMetaProperty_t2249223145::get_offset_of__memberInfo_0(),
	fsMetaProperty_t2249223145::get_offset_of_U3CStorageTypeU3Ek__BackingField_1(),
	fsMetaProperty_t2249223145::get_offset_of_U3CCanReadU3Ek__BackingField_2(),
	fsMetaProperty_t2249223145::get_offset_of_U3CCanWriteU3Ek__BackingField_3(),
	fsMetaProperty_t2249223145::get_offset_of_U3CJsonNameU3Ek__BackingField_4(),
	fsMetaProperty_t2249223145::get_offset_of_U3CMemberNameU3Ek__BackingField_5(),
	fsMetaProperty_t2249223145::get_offset_of_U3CIsPublicU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3131 = { sizeof (fsMetaType_t3266798926), -1, sizeof(fsMetaType_t3266798926_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3131[6] = 
{
	fsMetaType_t3266798926_StaticFields::get_offset_of__metaTypes_0(),
	fsMetaType_t3266798926::get_offset_of_ReflectedType_1(),
	fsMetaType_t3266798926::get_offset_of__hasEmittedAotData_2(),
	fsMetaType_t3266798926::get_offset_of_U3CPropertiesU3Ek__BackingField_3(),
	fsMetaType_t3266798926::get_offset_of__hasDefaultConstructorCache_4(),
	fsMetaType_t3266798926::get_offset_of__isDefaultConstructorPublic_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3132 = { sizeof (U3CCollectPropertiesU3Ec__AnonStorey0_t1756394196), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3132[1] = 
{
	U3CCollectPropertiesU3Ec__AnonStorey0_t1756394196::get_offset_of_member_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3133 = { sizeof (U3CCanSerializePropertyU3Ec__AnonStorey1_t3778365949), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3133[1] = 
{
	U3CCanSerializePropertyU3Ec__AnonStorey1_t3778365949::get_offset_of_property_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3134 = { sizeof (U3CCanSerializeFieldU3Ec__AnonStorey2_t2547514815), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3134[1] = 
{
	U3CCanSerializeFieldU3Ec__AnonStorey2_t2547514815::get_offset_of_field_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3135 = { sizeof (fsReflectionUtility_t889872004), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3136 = { sizeof (fsTypeLookup_t2250544765), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3137 = { sizeof (Json_t4020371770), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3138 = { sizeof (Parser_t1915358011), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3138[2] = 
{
	0,
	Parser_t1915358011::get_offset_of_json_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3139 = { sizeof (TOKEN_t2182318091)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3139[13] = 
{
	TOKEN_t2182318091::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3140 = { sizeof (Serializer_t4088787656), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3140[1] = 
{
	Serializer_t4088787656::get_offset_of_builder_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3141 = { sizeof (TuioInput_t352576185), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3141[16] = 
{
	0,
	TuioInput_t352576185::get_offset_of_tuioPort_6(),
	TuioInput_t352576185::get_offset_of_supportedInputs_7(),
	TuioInput_t352576185::get_offset_of_tuioObjectMappings_8(),
	TuioInput_t352576185::get_offset_of_cursorTags_9(),
	TuioInput_t352576185::get_offset_of_blobTags_10(),
	TuioInput_t352576185::get_offset_of_objectTags_11(),
	TuioInput_t352576185::get_offset_of_server_12(),
	TuioInput_t352576185::get_offset_of_cursorProcessor_13(),
	TuioInput_t352576185::get_offset_of_objectProcessor_14(),
	TuioInput_t352576185::get_offset_of_blobProcessor_15(),
	TuioInput_t352576185::get_offset_of_cursorToInternalId_16(),
	TuioInput_t352576185::get_offset_of_blobToInternalId_17(),
	TuioInput_t352576185::get_offset_of_objectToInternalId_18(),
	TuioInput_t352576185::get_offset_of_screenWidth_19(),
	TuioInput_t352576185::get_offset_of_screenHeight_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3142 = { sizeof (InputType_t948942308)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3142[4] = 
{
	InputType_t948942308::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3143 = { sizeof (TuioObjectMapping_t4080927128), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3143[2] = 
{
	TuioObjectMapping_t4080927128::get_offset_of_Id_0(),
	TuioObjectMapping_t4080927128::get_offset_of_Tag_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3144 = { sizeof (TouchScriptInputModule_t3561165032), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3144[10] = 
{
	TouchScriptInputModule_t3561165032::get_offset_of_pointerEvents_8(),
	TouchScriptInputModule_t3561165032::get_offset_of_horizontalAxis_9(),
	TouchScriptInputModule_t3561165032::get_offset_of_verticalAxis_10(),
	TouchScriptInputModule_t3561165032::get_offset_of_submitButton_11(),
	TouchScriptInputModule_t3561165032::get_offset_of_cancelButton_12(),
	TouchScriptInputModule_t3561165032::get_offset_of_inputActionsPerSecond_13(),
	TouchScriptInputModule_t3561165032::get_offset_of_repeatDelay_14(),
	TouchScriptInputModule_t3561165032::get_offset_of_nextActionTime_15(),
	TouchScriptInputModule_t3561165032::get_offset_of_lastMoveDirection_16(),
	TouchScriptInputModule_t3561165032::get_offset_of_lastMoveStartTime_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3145 = { sizeof (Transformer_t946377179), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3145[2] = 
{
	Transformer_t946377179::get_offset_of_cachedTransform_2(),
	Transformer_t946377179::get_offset_of_gestures_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3146 = { sizeof (TouchProxy_t1093150977), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3146[2] = 
{
	TouchProxy_t1093150977::get_offset_of_Text_6(),
	TouchProxy_t1093150977::get_offset_of_stringBuilder_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3147 = { sizeof (TouchProxyBase_t4188753234), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3147[4] = 
{
	TouchProxyBase_t4188753234::get_offset_of_U3CShowTouchIdU3Ek__BackingField_2(),
	TouchProxyBase_t4188753234::get_offset_of_U3CShowTagsU3Ek__BackingField_3(),
	TouchProxyBase_t4188753234::get_offset_of_rect_4(),
	TouchProxyBase_t4188753234::get_offset_of_size_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3148 = { sizeof (TouchVisualizer_t2264027661), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3148[9] = 
{
	TouchVisualizer_t2264027661::get_offset_of_touchProxy_2(),
	TouchVisualizer_t2264027661::get_offset_of_showTouchId_3(),
	TouchVisualizer_t2264027661::get_offset_of_showTags_4(),
	TouchVisualizer_t2264027661::get_offset_of_useDPI_5(),
	TouchVisualizer_t2264027661::get_offset_of_touchSize_6(),
	TouchVisualizer_t2264027661::get_offset_of_defaultSize_7(),
	TouchVisualizer_t2264027661::get_offset_of_rect_8(),
	TouchVisualizer_t2264027661::get_offset_of_pool_9(),
	TouchVisualizer_t2264027661::get_offset_of_proxies_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3149 = { sizeof (Clusters_t4089973855), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3149[9] = 
{
	0,
	0,
	Clusters_t4089973855::get_offset_of_points_2(),
	Clusters_t4089973855::get_offset_of_dirty_3(),
	Clusters_t4089973855::get_offset_of_cluster1_4(),
	Clusters_t4089973855::get_offset_of_cluster2_5(),
	Clusters_t4089973855::get_offset_of_minPointDistance_6(),
	Clusters_t4089973855::get_offset_of_minPointDistanceSqr_7(),
	Clusters_t4089973855::get_offset_of_hasClusters_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3150 = { sizeof (DebuggableMonoBehaviour_t3136086048), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3151 = { sizeof (DisplayDevice_t1478475236), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3151[2] = 
{
	DisplayDevice_t1478475236::get_offset_of_name_2(),
	DisplayDevice_t1478475236::get_offset_of_dpi_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3152 = { sizeof (GenericDisplayDevice_t579608737), -1, sizeof(GenericDisplayDevice_t579608737_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3152[1] = 
{
	GenericDisplayDevice_t579608737_StaticFields::get_offset_of_isLaptop_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3153 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3154 = { sizeof (GestureManager_t1999815568), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3155 = { sizeof (GestureManagerInstance_t505647059), -1, sizeof(GestureManagerInstance_t505647059_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3155[16] = 
{
	GestureManagerInstance_t505647059::get_offset_of_U3CGlobalGestureDelegateU3Ek__BackingField_2(),
	GestureManagerInstance_t505647059_StaticFields::get_offset_of_instance_3(),
	GestureManagerInstance_t505647059_StaticFields::get_offset_of_shuttingDown_4(),
	GestureManagerInstance_t505647059::get_offset_of_gesturesToReset_5(),
	GestureManagerInstance_t505647059::get_offset_of__updateBegan_6(),
	GestureManagerInstance_t505647059::get_offset_of__updateMoved_7(),
	GestureManagerInstance_t505647059::get_offset_of__updateEnded_8(),
	GestureManagerInstance_t505647059::get_offset_of__updateCancelled_9(),
	GestureManagerInstance_t505647059::get_offset_of__processTarget_10(),
	GestureManagerInstance_t505647059::get_offset_of__processTargetBegan_11(),
	GestureManagerInstance_t505647059::get_offset_of_targetTouches_12(),
	GestureManagerInstance_t505647059::get_offset_of_gestureTouches_13(),
	GestureManagerInstance_t505647059::get_offset_of_activeGestures_14(),
	GestureManagerInstance_t505647059_StaticFields::get_offset_of_gestureListPool_15(),
	GestureManagerInstance_t505647059_StaticFields::get_offset_of_touchListPool_16(),
	GestureManagerInstance_t505647059_StaticFields::get_offset_of_transformListPool_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3156 = { sizeof (PinnedTrasformGestureBase_t3366405746), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3156[19] = 
{
	0,
	0,
	0,
	PinnedTrasformGestureBase_t3366405746::get_offset_of_transformStartedInvoker_35(),
	PinnedTrasformGestureBase_t3366405746::get_offset_of_transformedInvoker_36(),
	PinnedTrasformGestureBase_t3366405746::get_offset_of_transformCompletedInvoker_37(),
	PinnedTrasformGestureBase_t3366405746::get_offset_of_screenTransformPixelThreshold_38(),
	PinnedTrasformGestureBase_t3366405746::get_offset_of_screenTransformPixelThresholdSquared_39(),
	PinnedTrasformGestureBase_t3366405746::get_offset_of_cachedCollider_40(),
	PinnedTrasformGestureBase_t3366405746::get_offset_of_deltaRotation_41(),
	PinnedTrasformGestureBase_t3366405746::get_offset_of_deltaScale_42(),
	PinnedTrasformGestureBase_t3366405746::get_offset_of_screenPixelTranslationBuffer_43(),
	PinnedTrasformGestureBase_t3366405746::get_offset_of_screenPixelRotationBuffer_44(),
	PinnedTrasformGestureBase_t3366405746::get_offset_of_angleBuffer_45(),
	PinnedTrasformGestureBase_t3366405746::get_offset_of_screenPixelScalingBuffer_46(),
	PinnedTrasformGestureBase_t3366405746::get_offset_of_scaleBuffer_47(),
	PinnedTrasformGestureBase_t3366405746::get_offset_of_isTransforming_48(),
	PinnedTrasformGestureBase_t3366405746::get_offset_of_type_49(),
	PinnedTrasformGestureBase_t3366405746::get_offset_of_screenTransformThreshold_50(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3157 = { sizeof (TransformType_t724632304)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3157[3] = 
{
	TransformType_t724632304::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3158 = { sizeof (TransformGestureBase_t91965108), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3158[22] = 
{
	0,
	0,
	0,
	TransformGestureBase_t91965108::get_offset_of_transformStartedInvoker_35(),
	TransformGestureBase_t91965108::get_offset_of_transformedInvoker_36(),
	TransformGestureBase_t91965108::get_offset_of_transformCompletedInvoker_37(),
	TransformGestureBase_t91965108::get_offset_of_minScreenPointsPixelDistance_38(),
	TransformGestureBase_t91965108::get_offset_of_minScreenPointsPixelDistanceSquared_39(),
	TransformGestureBase_t91965108::get_offset_of_screenTransformPixelThreshold_40(),
	TransformGestureBase_t91965108::get_offset_of_screenTransformPixelThresholdSquared_41(),
	TransformGestureBase_t91965108::get_offset_of_deltaPosition_42(),
	TransformGestureBase_t91965108::get_offset_of_deltaRotation_43(),
	TransformGestureBase_t91965108::get_offset_of_deltaScale_44(),
	TransformGestureBase_t91965108::get_offset_of_screenPixelTranslationBuffer_45(),
	TransformGestureBase_t91965108::get_offset_of_screenPixelRotationBuffer_46(),
	TransformGestureBase_t91965108::get_offset_of_angleBuffer_47(),
	TransformGestureBase_t91965108::get_offset_of_screenPixelScalingBuffer_48(),
	TransformGestureBase_t91965108::get_offset_of_scaleBuffer_49(),
	TransformGestureBase_t91965108::get_offset_of_isTransforming_50(),
	TransformGestureBase_t91965108::get_offset_of_type_51(),
	TransformGestureBase_t91965108::get_offset_of_minScreenPointsDistance_52(),
	TransformGestureBase_t91965108::get_offset_of_screenTransformThreshold_53(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3159 = { sizeof (TransformType_t94296928)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3159[4] = 
{
	TransformType_t94296928::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3160 = { sizeof (ClusteredPinnedTransformGesture_t2370140594), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3161 = { sizeof (ClusteredScreenTransformGesture_t1866155674), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3161[1] = 
{
	ClusteredScreenTransformGesture_t1866155674::get_offset_of_clusters_54(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3162 = { sizeof (ClusteredTransformGesture_t1600768702), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3162[1] = 
{
	ClusteredTransformGesture_t1600768702::get_offset_of_clusters_58(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3163 = { sizeof (FlickGesture_t402498036), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3163[12] = 
{
	0,
	FlickGesture_t402498036::get_offset_of_flickedInvoker_33(),
	FlickGesture_t402498036::get_offset_of_U3CScreenFlickVectorU3Ek__BackingField_34(),
	FlickGesture_t402498036::get_offset_of_U3CScreenFlickTimeU3Ek__BackingField_35(),
	FlickGesture_t402498036::get_offset_of_flickTime_36(),
	FlickGesture_t402498036::get_offset_of_minDistance_37(),
	FlickGesture_t402498036::get_offset_of_movementThreshold_38(),
	FlickGesture_t402498036::get_offset_of_direction_39(),
	FlickGesture_t402498036::get_offset_of_moving_40(),
	FlickGesture_t402498036::get_offset_of_movementBuffer_41(),
	FlickGesture_t402498036::get_offset_of_isActive_42(),
	FlickGesture_t402498036::get_offset_of_deltaSequence_43(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3164 = { sizeof (GestureDirection_t1726119045)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3164[4] = 
{
	GestureDirection_t1726119045::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3165 = { sizeof (Gesture_t2352305985), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3165[30] = 
{
	0,
	0,
	Gesture_t2352305985::get_offset_of_stateChangedInvoker_4(),
	Gesture_t2352305985::get_offset_of_cancelledInvoker_5(),
	Gesture_t2352305985::get_offset_of_U3CPreviousStateU3Ek__BackingField_6(),
	Gesture_t2352305985::get_offset_of_U3CDelegateU3Ek__BackingField_7(),
	Gesture_t2352305985::get_offset_of_U3CtouchManagerU3Ek__BackingField_8(),
	Gesture_t2352305985::get_offset_of_U3CtouchesNumStateU3Ek__BackingField_9(),
	Gesture_t2352305985::get_offset_of_activeTouches_10(),
	Gesture_t2352305985::get_offset_of_cachedTransform_11(),
	Gesture_t2352305985::get_offset_of_advancedProps_12(),
	Gesture_t2352305985::get_offset_of_minTouches_13(),
	Gesture_t2352305985::get_offset_of_maxTouches_14(),
	Gesture_t2352305985::get_offset_of_combineTouches_15(),
	Gesture_t2352305985::get_offset_of_combineTouchesInterval_16(),
	Gesture_t2352305985::get_offset_of_useSendMessage_17(),
	Gesture_t2352305985::get_offset_of_sendStateChangeMessages_18(),
	Gesture_t2352305985::get_offset_of_sendMessageTarget_19(),
	Gesture_t2352305985::get_offset_of_requireGestureToFail_20(),
	Gesture_t2352305985::get_offset_of_friendlyGestures_21(),
	Gesture_t2352305985::get_offset_of_numTouches_22(),
	Gesture_t2352305985::get_offset_of_layer_23(),
	Gesture_t2352305985::get_offset_of_readonlyActiveTouches_24(),
	Gesture_t2352305985::get_offset_of_touchSequence_25(),
	Gesture_t2352305985::get_offset_of_gestureManagerInstance_26(),
	Gesture_t2352305985::get_offset_of_delayedStateChange_27(),
	Gesture_t2352305985::get_offset_of_requiredGestureFailed_28(),
	Gesture_t2352305985::get_offset_of_state_29(),
	Gesture_t2352305985::get_offset_of_cachedScreenPosition_30(),
	Gesture_t2352305985::get_offset_of_cachedPreviousScreenPosition_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3166 = { sizeof (GestureState_t2128095272)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3166[8] = 
{
	GestureState_t2128095272::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3167 = { sizeof (TouchesNumState_t594133898)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3167[7] = 
{
	TouchesNumState_t594133898::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3168 = { sizeof (GestureStateChangeEventArgs_t3499981191), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3168[2] = 
{
	GestureStateChangeEventArgs_t3499981191::get_offset_of_U3CPreviousStateU3Ek__BackingField_1(),
	GestureStateChangeEventArgs_t3499981191::get_offset_of_U3CStateU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3169 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3170 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3171 = { sizeof (LongPressGesture_t656184630), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3171[6] = 
{
	0,
	LongPressGesture_t656184630::get_offset_of_longPressedInvoker_33(),
	LongPressGesture_t656184630::get_offset_of_timeToPress_34(),
	LongPressGesture_t656184630::get_offset_of_distanceLimit_35(),
	LongPressGesture_t656184630::get_offset_of_distanceLimitInPixelsSquared_36(),
	LongPressGesture_t656184630::get_offset_of_totalMovement_37(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3172 = { sizeof (U3CwaitU3Ec__Iterator0_t3884913881), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3172[5] = 
{
	U3CwaitU3Ec__Iterator0_t3884913881::get_offset_of_U3CtargetTimeU3E__0_0(),
	U3CwaitU3Ec__Iterator0_t3884913881::get_offset_of_U24this_1(),
	U3CwaitU3Ec__Iterator0_t3884913881::get_offset_of_U24current_2(),
	U3CwaitU3Ec__Iterator0_t3884913881::get_offset_of_U24disposing_3(),
	U3CwaitU3Ec__Iterator0_t3884913881::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3173 = { sizeof (MetaGesture_t1110051842), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3173[8] = 
{
	0,
	0,
	0,
	0,
	MetaGesture_t1110051842::get_offset_of_touchBeganInvoker_36(),
	MetaGesture_t1110051842::get_offset_of_touchMovedInvoker_37(),
	MetaGesture_t1110051842::get_offset_of_touchEndedInvoker_38(),
	MetaGesture_t1110051842::get_offset_of_touchCancelledInvoker_39(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3174 = { sizeof (MetaGestureEventArgs_t256591615), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3174[1] = 
{
	MetaGestureEventArgs_t256591615::get_offset_of_U3CTouchU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3175 = { sizeof (PinnedTransformGesture_t2478931093), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3175[4] = 
{
	PinnedTransformGesture_t2478931093::get_offset_of_projection_51(),
	PinnedTransformGesture_t2478931093::get_offset_of_projectionPlaneNormal_52(),
	PinnedTransformGesture_t2478931093::get_offset_of_projectionLayer_53(),
	PinnedTransformGesture_t2478931093::get_offset_of_transformPlane_54(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3176 = { sizeof (ProjectionType_t510080709)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3176[4] = 
{
	ProjectionType_t510080709::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3177 = { sizeof (PressGesture_t582183752), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3177[3] = 
{
	0,
	PressGesture_t582183752::get_offset_of_pressedInvoker_33(),
	PressGesture_t582183752::get_offset_of_ignoreChildren_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3178 = { sizeof (ReleaseGesture_t248506278), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3178[3] = 
{
	0,
	ReleaseGesture_t248506278::get_offset_of_releasedInvoker_33(),
	ReleaseGesture_t248506278::get_offset_of_ignoreChildren_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3179 = { sizeof (ScreenTransformGesture_t3088562929), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3180 = { sizeof (TapGesture_t556401502), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3180[10] = 
{
	0,
	TapGesture_t556401502::get_offset_of_tappedInvoker_33(),
	TapGesture_t556401502::get_offset_of_numberOfTapsRequired_34(),
	TapGesture_t556401502::get_offset_of_timeLimit_35(),
	TapGesture_t556401502::get_offset_of_distanceLimit_36(),
	TapGesture_t556401502::get_offset_of_distanceLimitInPixelsSquared_37(),
	TapGesture_t556401502::get_offset_of_isActive_38(),
	TapGesture_t556401502::get_offset_of_tapsDone_39(),
	TapGesture_t556401502::get_offset_of_startPosition_40(),
	TapGesture_t556401502::get_offset_of_totalMovement_41(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3181 = { sizeof (U3CwaitU3Ec__Iterator0_t3077308673), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3181[5] = 
{
	U3CwaitU3Ec__Iterator0_t3077308673::get_offset_of_U3CtargetTimeU3E__0_0(),
	U3CwaitU3Ec__Iterator0_t3077308673::get_offset_of_U24this_1(),
	U3CwaitU3Ec__Iterator0_t3077308673::get_offset_of_U24current_2(),
	U3CwaitU3Ec__Iterator0_t3077308673::get_offset_of_U24disposing_3(),
	U3CwaitU3Ec__Iterator0_t3077308673::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3182 = { sizeof (TransformGesture_t780972309), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3182[4] = 
{
	TransformGesture_t780972309::get_offset_of_projection_54(),
	TransformGesture_t780972309::get_offset_of_projectionPlaneNormal_55(),
	TransformGesture_t780972309::get_offset_of_projectionLayer_56(),
	TransformGesture_t780972309::get_offset_of_transformPlane_57(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3183 = { sizeof (ProjectionType_t462923329)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3183[4] = 
{
	ProjectionType_t462923329::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3184 = { sizeof (UIGesture_t3400500675), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3184[1] = 
{
	UIGesture_t3400500675::get_offset_of_pointerData_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3185 = { sizeof (TouchData_t3880599975)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3185[2] = 
{
	TouchData_t3880599975::get_offset_of_OnTarget_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TouchData_t3880599975::get_offset_of_Data_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3186 = { sizeof (HitTest_t768639505), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3187 = { sizeof (ObjectHitResult_t3057876522)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3187[5] = 
{
	ObjectHitResult_t3057876522::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3188 = { sizeof (TouchHit_t4186847494)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3188[5] = 
{
	TouchHit_t4186847494::get_offset_of_type_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TouchHit_t4186847494::get_offset_of_transform_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TouchHit_t4186847494::get_offset_of_raycastHit_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TouchHit_t4186847494::get_offset_of_raycastHit2D_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TouchHit_t4186847494::get_offset_of_raycastResult_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3189 = { sizeof (TouchHitType_t1472696400)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3189[4] = 
{
	TouchHitType_t1472696400::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3190 = { sizeof (Untouchable_t2967803006), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3190[1] = 
{
	Untouchable_t2967803006::get_offset_of_DiscardTouch_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3191 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3192 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3193 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3194 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3195 = { sizeof (MouseHandler_t3116661769), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3195[8] = 
{
	MouseHandler_t3116661769::get_offset_of_beginTouch_0(),
	MouseHandler_t3116661769::get_offset_of_moveTouch_1(),
	MouseHandler_t3116661769::get_offset_of_endTouch_2(),
	MouseHandler_t3116661769::get_offset_of_cancelTouch_3(),
	MouseHandler_t3116661769::get_offset_of_tags_4(),
	MouseHandler_t3116661769::get_offset_of_mousePointId_5(),
	MouseHandler_t3116661769::get_offset_of_fakeMousePointId_6(),
	MouseHandler_t3116661769::get_offset_of_mousePointPos_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3196 = { sizeof (TouchHandler_t2521645213), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3196[7] = 
{
	TouchHandler_t2521645213::get_offset_of_beginTouch_0(),
	TouchHandler_t2521645213::get_offset_of_moveTouch_1(),
	TouchHandler_t2521645213::get_offset_of_endTouch_2(),
	TouchHandler_t2521645213::get_offset_of_cancelTouch_3(),
	TouchHandler_t2521645213::get_offset_of_tags_4(),
	TouchHandler_t2521645213::get_offset_of_systemToInternalId_5(),
	TouchHandler_t2521645213::get_offset_of_touchesNum_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3197 = { sizeof (TouchState_t2732082299)+ sizeof (RuntimeObject), sizeof(TouchState_t2732082299 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3197[2] = 
{
	TouchState_t2732082299::get_offset_of_Id_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TouchState_t2732082299::get_offset_of_Phase_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3198 = { sizeof (InputSource_t3078263767), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3198[3] = 
{
	InputSource_t3078263767::get_offset_of_U3CCoordinatesRemapperU3Ek__BackingField_2(),
	InputSource_t3078263767::get_offset_of_advancedProps_3(),
	InputSource_t3078263767::get_offset_of_manager_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3199 = { sizeof (MobileInput_t1619040116), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3199[3] = 
{
	MobileInput_t1619040116::get_offset_of_DisableOnNonTouchPlatforms_5(),
	MobileInput_t1619040116::get_offset_of_Tags_6(),
	MobileInput_t1619040116::get_offset_of_touchHandler_7(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
