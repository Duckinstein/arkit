﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.Xml.XPath.XPathNavigator
struct XPathNavigator_t3981235968;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Collections.ArrayList
struct ArrayList_t4252133567;
// System.Xml.XPath.XPathNodeIterator
struct XPathNodeIterator_t3192332357;
// System.String
struct String_t;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.Xml.XPath.Expression
struct Expression_t1283317256;
// System.IntPtr[]
struct IntPtrU5BU5D_t169632028;
// System.Collections.IDictionary
struct IDictionary_t596158605;
// System.Xml.XmlNamespaceManager/NsDecl[]
struct NsDeclU5BU5D_t228542166;
// System.Xml.XmlNameTable
struct XmlNameTable_t1345805268;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;
// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t1944712516;
// System.Xml.IXmlNamespaceResolver
struct IXmlNamespaceResolver_t3928241465;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.Xml.NameTable/Entry[]
struct EntryU5BU5D_t180042139;
// System.Collections.IEnumerable
struct IEnumerable_t2911409499;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Xml.XPath.XPathSorters
struct XPathSorters_t4019574815;
// System.Xml.XPath.BaseIterator
struct BaseIterator_t2454437973;
// System.Xml.XPath.NodeSet
struct NodeSet_t2895962396;
// System.Xml.XPath.NodeTest
struct NodeTest_t2072055152;
// System.Collections.SortedList
struct SortedList_t3004938869;
// System.Collections.IList
struct IList_t3321498491;
// System.Xml.XPath.AxisSpecifier
struct AxisSpecifier_t201930955;
// System.Collections.IComparer
struct IComparer_t3952557350;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef EXPRESSION_T1283317256_H
#define EXPRESSION_T1283317256_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.Expression
struct  Expression_t1283317256  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRESSION_T1283317256_H
#ifndef XPATHNODEITERATOR_T3192332357_H
#define XPATHNODEITERATOR_T3192332357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathNodeIterator
struct  XPathNodeIterator_t3192332357  : public RuntimeObject
{
public:
	// System.Int32 System.Xml.XPath.XPathNodeIterator::_count
	int32_t ____count_0;

public:
	inline static int32_t get_offset_of__count_0() { return static_cast<int32_t>(offsetof(XPathNodeIterator_t3192332357, ____count_0)); }
	inline int32_t get__count_0() const { return ____count_0; }
	inline int32_t* get_address_of__count_0() { return &____count_0; }
	inline void set__count_0(int32_t value)
	{
		____count_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHNODEITERATOR_T3192332357_H
#ifndef XPATHSORTELEMENT_T1040291071_H
#define XPATHSORTELEMENT_T1040291071_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathSortElement
struct  XPathSortElement_t1040291071  : public RuntimeObject
{
public:
	// System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathSortElement::Navigator
	XPathNavigator_t3981235968 * ___Navigator_0;
	// System.Object[] System.Xml.XPath.XPathSortElement::Values
	ObjectU5BU5D_t3614634134* ___Values_1;

public:
	inline static int32_t get_offset_of_Navigator_0() { return static_cast<int32_t>(offsetof(XPathSortElement_t1040291071, ___Navigator_0)); }
	inline XPathNavigator_t3981235968 * get_Navigator_0() const { return ___Navigator_0; }
	inline XPathNavigator_t3981235968 ** get_address_of_Navigator_0() { return &___Navigator_0; }
	inline void set_Navigator_0(XPathNavigator_t3981235968 * value)
	{
		___Navigator_0 = value;
		Il2CppCodeGenWriteBarrier((&___Navigator_0), value);
	}

	inline static int32_t get_offset_of_Values_1() { return static_cast<int32_t>(offsetof(XPathSortElement_t1040291071, ___Values_1)); }
	inline ObjectU5BU5D_t3614634134* get_Values_1() const { return ___Values_1; }
	inline ObjectU5BU5D_t3614634134** get_address_of_Values_1() { return &___Values_1; }
	inline void set_Values_1(ObjectU5BU5D_t3614634134* value)
	{
		___Values_1 = value;
		Il2CppCodeGenWriteBarrier((&___Values_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHSORTELEMENT_T1040291071_H
#ifndef XPATHSORTERS_T4019574815_H
#define XPATHSORTERS_T4019574815_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathSorters
struct  XPathSorters_t4019574815  : public RuntimeObject
{
public:
	// System.Collections.ArrayList System.Xml.XPath.XPathSorters::_rgSorters
	ArrayList_t4252133567 * ____rgSorters_0;

public:
	inline static int32_t get_offset_of__rgSorters_0() { return static_cast<int32_t>(offsetof(XPathSorters_t4019574815, ____rgSorters_0)); }
	inline ArrayList_t4252133567 * get__rgSorters_0() const { return ____rgSorters_0; }
	inline ArrayList_t4252133567 ** get_address_of__rgSorters_0() { return &____rgSorters_0; }
	inline void set__rgSorters_0(ArrayList_t4252133567 * value)
	{
		____rgSorters_0 = value;
		Il2CppCodeGenWriteBarrier((&____rgSorters_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHSORTERS_T4019574815_H
#ifndef U3CGETENUMERATORU3EC__ITERATOR2_T959253998_H
#define U3CGETENUMERATORU3EC__ITERATOR2_T959253998_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathNodeIterator/<GetEnumerator>c__Iterator2
struct  U3CGetEnumeratorU3Ec__Iterator2_t959253998  : public RuntimeObject
{
public:
	// System.Int32 System.Xml.XPath.XPathNodeIterator/<GetEnumerator>c__Iterator2::$PC
	int32_t ___U24PC_0;
	// System.Object System.Xml.XPath.XPathNodeIterator/<GetEnumerator>c__Iterator2::$current
	RuntimeObject * ___U24current_1;
	// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.XPathNodeIterator/<GetEnumerator>c__Iterator2::<>f__this
	XPathNodeIterator_t3192332357 * ___U3CU3Ef__this_2;

public:
	inline static int32_t get_offset_of_U24PC_0() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator2_t959253998, ___U24PC_0)); }
	inline int32_t get_U24PC_0() const { return ___U24PC_0; }
	inline int32_t* get_address_of_U24PC_0() { return &___U24PC_0; }
	inline void set_U24PC_0(int32_t value)
	{
		___U24PC_0 = value;
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator2_t959253998, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_2() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator2_t959253998, ___U3CU3Ef__this_2)); }
	inline XPathNodeIterator_t3192332357 * get_U3CU3Ef__this_2() const { return ___U3CU3Ef__this_2; }
	inline XPathNodeIterator_t3192332357 ** get_address_of_U3CU3Ef__this_2() { return &___U3CU3Ef__this_2; }
	inline void set_U3CU3Ef__this_2(XPathNodeIterator_t3192332357 * value)
	{
		___U3CU3Ef__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETENUMERATORU3EC__ITERATOR2_T959253998_H
#ifndef XPATHITERATORCOMPARER_T2522471076_H
#define XPATHITERATORCOMPARER_T2522471076_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathIteratorComparer
struct  XPathIteratorComparer_t2522471076  : public RuntimeObject
{
public:

public:
};

struct XPathIteratorComparer_t2522471076_StaticFields
{
public:
	// System.Xml.XPath.XPathIteratorComparer System.Xml.XPath.XPathIteratorComparer::Instance
	XPathIteratorComparer_t2522471076 * ___Instance_0;

public:
	inline static int32_t get_offset_of_Instance_0() { return static_cast<int32_t>(offsetof(XPathIteratorComparer_t2522471076_StaticFields, ___Instance_0)); }
	inline XPathIteratorComparer_t2522471076 * get_Instance_0() const { return ___Instance_0; }
	inline XPathIteratorComparer_t2522471076 ** get_address_of_Instance_0() { return &___Instance_0; }
	inline void set_Instance_0(XPathIteratorComparer_t2522471076 * value)
	{
		___Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHITERATORCOMPARER_T2522471076_H
#ifndef TOKENIZER_T2998805977_H
#define TOKENIZER_T2998805977_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.XPath.Tokenizer
struct  Tokenizer_t2998805977  : public RuntimeObject
{
public:
	// System.String Mono.Xml.XPath.Tokenizer::m_rgchInput
	String_t* ___m_rgchInput_0;
	// System.Int32 Mono.Xml.XPath.Tokenizer::m_ich
	int32_t ___m_ich_1;
	// System.Int32 Mono.Xml.XPath.Tokenizer::m_cch
	int32_t ___m_cch_2;
	// System.Int32 Mono.Xml.XPath.Tokenizer::m_iToken
	int32_t ___m_iToken_3;
	// System.Int32 Mono.Xml.XPath.Tokenizer::m_iTokenPrev
	int32_t ___m_iTokenPrev_4;
	// System.Object Mono.Xml.XPath.Tokenizer::m_objToken
	RuntimeObject * ___m_objToken_5;
	// System.Boolean Mono.Xml.XPath.Tokenizer::m_fPrevWasOperator
	bool ___m_fPrevWasOperator_6;
	// System.Boolean Mono.Xml.XPath.Tokenizer::m_fThisIsOperator
	bool ___m_fThisIsOperator_7;

public:
	inline static int32_t get_offset_of_m_rgchInput_0() { return static_cast<int32_t>(offsetof(Tokenizer_t2998805977, ___m_rgchInput_0)); }
	inline String_t* get_m_rgchInput_0() const { return ___m_rgchInput_0; }
	inline String_t** get_address_of_m_rgchInput_0() { return &___m_rgchInput_0; }
	inline void set_m_rgchInput_0(String_t* value)
	{
		___m_rgchInput_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_rgchInput_0), value);
	}

	inline static int32_t get_offset_of_m_ich_1() { return static_cast<int32_t>(offsetof(Tokenizer_t2998805977, ___m_ich_1)); }
	inline int32_t get_m_ich_1() const { return ___m_ich_1; }
	inline int32_t* get_address_of_m_ich_1() { return &___m_ich_1; }
	inline void set_m_ich_1(int32_t value)
	{
		___m_ich_1 = value;
	}

	inline static int32_t get_offset_of_m_cch_2() { return static_cast<int32_t>(offsetof(Tokenizer_t2998805977, ___m_cch_2)); }
	inline int32_t get_m_cch_2() const { return ___m_cch_2; }
	inline int32_t* get_address_of_m_cch_2() { return &___m_cch_2; }
	inline void set_m_cch_2(int32_t value)
	{
		___m_cch_2 = value;
	}

	inline static int32_t get_offset_of_m_iToken_3() { return static_cast<int32_t>(offsetof(Tokenizer_t2998805977, ___m_iToken_3)); }
	inline int32_t get_m_iToken_3() const { return ___m_iToken_3; }
	inline int32_t* get_address_of_m_iToken_3() { return &___m_iToken_3; }
	inline void set_m_iToken_3(int32_t value)
	{
		___m_iToken_3 = value;
	}

	inline static int32_t get_offset_of_m_iTokenPrev_4() { return static_cast<int32_t>(offsetof(Tokenizer_t2998805977, ___m_iTokenPrev_4)); }
	inline int32_t get_m_iTokenPrev_4() const { return ___m_iTokenPrev_4; }
	inline int32_t* get_address_of_m_iTokenPrev_4() { return &___m_iTokenPrev_4; }
	inline void set_m_iTokenPrev_4(int32_t value)
	{
		___m_iTokenPrev_4 = value;
	}

	inline static int32_t get_offset_of_m_objToken_5() { return static_cast<int32_t>(offsetof(Tokenizer_t2998805977, ___m_objToken_5)); }
	inline RuntimeObject * get_m_objToken_5() const { return ___m_objToken_5; }
	inline RuntimeObject ** get_address_of_m_objToken_5() { return &___m_objToken_5; }
	inline void set_m_objToken_5(RuntimeObject * value)
	{
		___m_objToken_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_objToken_5), value);
	}

	inline static int32_t get_offset_of_m_fPrevWasOperator_6() { return static_cast<int32_t>(offsetof(Tokenizer_t2998805977, ___m_fPrevWasOperator_6)); }
	inline bool get_m_fPrevWasOperator_6() const { return ___m_fPrevWasOperator_6; }
	inline bool* get_address_of_m_fPrevWasOperator_6() { return &___m_fPrevWasOperator_6; }
	inline void set_m_fPrevWasOperator_6(bool value)
	{
		___m_fPrevWasOperator_6 = value;
	}

	inline static int32_t get_offset_of_m_fThisIsOperator_7() { return static_cast<int32_t>(offsetof(Tokenizer_t2998805977, ___m_fThisIsOperator_7)); }
	inline bool get_m_fThisIsOperator_7() const { return ___m_fThisIsOperator_7; }
	inline bool* get_address_of_m_fThisIsOperator_7() { return &___m_fThisIsOperator_7; }
	inline void set_m_fThisIsOperator_7(bool value)
	{
		___m_fThisIsOperator_7 = value;
	}
};

struct Tokenizer_t2998805977_StaticFields
{
public:
	// System.Collections.Hashtable Mono.Xml.XPath.Tokenizer::s_mapTokens
	Hashtable_t909839986 * ___s_mapTokens_8;
	// System.Object[] Mono.Xml.XPath.Tokenizer::s_rgTokenMap
	ObjectU5BU5D_t3614634134* ___s_rgTokenMap_9;

public:
	inline static int32_t get_offset_of_s_mapTokens_8() { return static_cast<int32_t>(offsetof(Tokenizer_t2998805977_StaticFields, ___s_mapTokens_8)); }
	inline Hashtable_t909839986 * get_s_mapTokens_8() const { return ___s_mapTokens_8; }
	inline Hashtable_t909839986 ** get_address_of_s_mapTokens_8() { return &___s_mapTokens_8; }
	inline void set_s_mapTokens_8(Hashtable_t909839986 * value)
	{
		___s_mapTokens_8 = value;
		Il2CppCodeGenWriteBarrier((&___s_mapTokens_8), value);
	}

	inline static int32_t get_offset_of_s_rgTokenMap_9() { return static_cast<int32_t>(offsetof(Tokenizer_t2998805977_StaticFields, ___s_rgTokenMap_9)); }
	inline ObjectU5BU5D_t3614634134* get_s_rgTokenMap_9() const { return ___s_rgTokenMap_9; }
	inline ObjectU5BU5D_t3614634134** get_address_of_s_rgTokenMap_9() { return &___s_rgTokenMap_9; }
	inline void set_s_rgTokenMap_9(ObjectU5BU5D_t3614634134* value)
	{
		___s_rgTokenMap_9 = value;
		Il2CppCodeGenWriteBarrier((&___s_rgTokenMap_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOKENIZER_T2998805977_H
#ifndef FUNCTIONARGUMENTS_T2900945452_H
#define FUNCTIONARGUMENTS_T2900945452_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.FunctionArguments
struct  FunctionArguments_t2900945452  : public RuntimeObject
{
public:
	// System.Xml.XPath.Expression System.Xml.XPath.FunctionArguments::_arg
	Expression_t1283317256 * ____arg_0;
	// System.Xml.XPath.FunctionArguments System.Xml.XPath.FunctionArguments::_tail
	FunctionArguments_t2900945452 * ____tail_1;

public:
	inline static int32_t get_offset_of__arg_0() { return static_cast<int32_t>(offsetof(FunctionArguments_t2900945452, ____arg_0)); }
	inline Expression_t1283317256 * get__arg_0() const { return ____arg_0; }
	inline Expression_t1283317256 ** get_address_of__arg_0() { return &____arg_0; }
	inline void set__arg_0(Expression_t1283317256 * value)
	{
		____arg_0 = value;
		Il2CppCodeGenWriteBarrier((&____arg_0), value);
	}

	inline static int32_t get_offset_of__tail_1() { return static_cast<int32_t>(offsetof(FunctionArguments_t2900945452, ____tail_1)); }
	inline FunctionArguments_t2900945452 * get__tail_1() const { return ____tail_1; }
	inline FunctionArguments_t2900945452 ** get_address_of__tail_1() { return &____tail_1; }
	inline void set__tail_1(FunctionArguments_t2900945452 * value)
	{
		____tail_1 = value;
		Il2CppCodeGenWriteBarrier((&____tail_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNCTIONARGUMENTS_T2900945452_H
#ifndef XPATHITEM_T3130801258_H
#define XPATHITEM_T3130801258_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathItem
struct  XPathItem_t3130801258  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHITEM_T3130801258_H
#ifndef XPATHEXPRESSION_T452251917_H
#define XPATHEXPRESSION_T452251917_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathExpression
struct  XPathExpression_t452251917  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHEXPRESSION_T452251917_H
#ifndef XPATHNAVIGATORCOMPARER_T2670709129_H
#define XPATHNAVIGATORCOMPARER_T2670709129_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathNavigatorComparer
struct  XPathNavigatorComparer_t2670709129  : public RuntimeObject
{
public:

public:
};

struct XPathNavigatorComparer_t2670709129_StaticFields
{
public:
	// System.Xml.XPath.XPathNavigatorComparer System.Xml.XPath.XPathNavigatorComparer::Instance
	XPathNavigatorComparer_t2670709129 * ___Instance_0;

public:
	inline static int32_t get_offset_of_Instance_0() { return static_cast<int32_t>(offsetof(XPathNavigatorComparer_t2670709129_StaticFields, ___Instance_0)); }
	inline XPathNavigatorComparer_t2670709129 * get_Instance_0() const { return ___Instance_0; }
	inline XPathNavigatorComparer_t2670709129 ** get_address_of_Instance_0() { return &___Instance_0; }
	inline void set_Instance_0(XPathNavigatorComparer_t2670709129 * value)
	{
		___Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHNAVIGATORCOMPARER_T2670709129_H
#ifndef EXCEPTION_T1927440687_H
#define EXCEPTION_T1927440687_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t1927440687  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t169632028* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t1927440687 * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t169632028* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t169632028** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t169632028* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___inner_exception_1)); }
	inline Exception_t1927440687 * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t1927440687 ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t1927440687 * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T1927440687_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef ATTRIBUTE_T542643598_H
#define ATTRIBUTE_T542643598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t542643598  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T542643598_H
#ifndef XMLNAMESPACEMANAGER_T486731501_H
#define XMLNAMESPACEMANAGER_T486731501_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNamespaceManager
struct  XmlNamespaceManager_t486731501  : public RuntimeObject
{
public:
	// System.Xml.XmlNamespaceManager/NsDecl[] System.Xml.XmlNamespaceManager::decls
	NsDeclU5BU5D_t228542166* ___decls_0;
	// System.Int32 System.Xml.XmlNamespaceManager::declPos
	int32_t ___declPos_1;
	// System.String System.Xml.XmlNamespaceManager::defaultNamespace
	String_t* ___defaultNamespace_2;
	// System.Xml.XmlNameTable System.Xml.XmlNamespaceManager::nameTable
	XmlNameTable_t1345805268 * ___nameTable_3;
	// System.Boolean System.Xml.XmlNamespaceManager::internalAtomizedNames
	bool ___internalAtomizedNames_4;

public:
	inline static int32_t get_offset_of_decls_0() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t486731501, ___decls_0)); }
	inline NsDeclU5BU5D_t228542166* get_decls_0() const { return ___decls_0; }
	inline NsDeclU5BU5D_t228542166** get_address_of_decls_0() { return &___decls_0; }
	inline void set_decls_0(NsDeclU5BU5D_t228542166* value)
	{
		___decls_0 = value;
		Il2CppCodeGenWriteBarrier((&___decls_0), value);
	}

	inline static int32_t get_offset_of_declPos_1() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t486731501, ___declPos_1)); }
	inline int32_t get_declPos_1() const { return ___declPos_1; }
	inline int32_t* get_address_of_declPos_1() { return &___declPos_1; }
	inline void set_declPos_1(int32_t value)
	{
		___declPos_1 = value;
	}

	inline static int32_t get_offset_of_defaultNamespace_2() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t486731501, ___defaultNamespace_2)); }
	inline String_t* get_defaultNamespace_2() const { return ___defaultNamespace_2; }
	inline String_t** get_address_of_defaultNamespace_2() { return &___defaultNamespace_2; }
	inline void set_defaultNamespace_2(String_t* value)
	{
		___defaultNamespace_2 = value;
		Il2CppCodeGenWriteBarrier((&___defaultNamespace_2), value);
	}

	inline static int32_t get_offset_of_nameTable_3() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t486731501, ___nameTable_3)); }
	inline XmlNameTable_t1345805268 * get_nameTable_3() const { return ___nameTable_3; }
	inline XmlNameTable_t1345805268 ** get_address_of_nameTable_3() { return &___nameTable_3; }
	inline void set_nameTable_3(XmlNameTable_t1345805268 * value)
	{
		___nameTable_3 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_3), value);
	}

	inline static int32_t get_offset_of_internalAtomizedNames_4() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t486731501, ___internalAtomizedNames_4)); }
	inline bool get_internalAtomizedNames_4() const { return ___internalAtomizedNames_4; }
	inline bool* get_address_of_internalAtomizedNames_4() { return &___internalAtomizedNames_4; }
	inline void set_internalAtomizedNames_4(bool value)
	{
		___internalAtomizedNames_4 = value;
	}
};

struct XmlNamespaceManager_t486731501_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.XmlNamespaceManager::<>f__switch$map28
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map28_5;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map28_5() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t486731501_StaticFields, ___U3CU3Ef__switchU24map28_5)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map28_5() const { return ___U3CU3Ef__switchU24map28_5; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map28_5() { return &___U3CU3Ef__switchU24map28_5; }
	inline void set_U3CU3Ef__switchU24map28_5(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map28_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map28_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNAMESPACEMANAGER_T486731501_H
#ifndef XMLNAMETABLE_T1345805268_H
#define XMLNAMETABLE_T1345805268_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNameTable
struct  XmlNameTable_t1345805268  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNAMETABLE_T1345805268_H
#ifndef EXPRNEG_T1881334439_H
#define EXPRNEG_T1881334439_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.ExprNEG
struct  ExprNEG_t1881334439  : public Expression_t1283317256
{
public:
	// System.Xml.XPath.Expression System.Xml.XPath.ExprNEG::_expr
	Expression_t1283317256 * ____expr_0;

public:
	inline static int32_t get_offset_of__expr_0() { return static_cast<int32_t>(offsetof(ExprNEG_t1881334439, ____expr_0)); }
	inline Expression_t1283317256 * get__expr_0() const { return ____expr_0; }
	inline Expression_t1283317256 ** get_address_of__expr_0() { return &____expr_0; }
	inline void set__expr_0(Expression_t1283317256 * value)
	{
		____expr_0 = value;
		Il2CppCodeGenWriteBarrier((&____expr_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRNEG_T1881334439_H
#ifndef EXPRVARIABLE_T3362236065_H
#define EXPRVARIABLE_T3362236065_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.ExprVariable
struct  ExprVariable_t3362236065  : public Expression_t1283317256
{
public:
	// System.Xml.XmlQualifiedName System.Xml.XPath.ExprVariable::_name
	XmlQualifiedName_t1944712516 * ____name_0;
	// System.Boolean System.Xml.XPath.ExprVariable::resolvedName
	bool ___resolvedName_1;

public:
	inline static int32_t get_offset_of__name_0() { return static_cast<int32_t>(offsetof(ExprVariable_t3362236065, ____name_0)); }
	inline XmlQualifiedName_t1944712516 * get__name_0() const { return ____name_0; }
	inline XmlQualifiedName_t1944712516 ** get_address_of__name_0() { return &____name_0; }
	inline void set__name_0(XmlQualifiedName_t1944712516 * value)
	{
		____name_0 = value;
		Il2CppCodeGenWriteBarrier((&____name_0), value);
	}

	inline static int32_t get_offset_of_resolvedName_1() { return static_cast<int32_t>(offsetof(ExprVariable_t3362236065, ___resolvedName_1)); }
	inline bool get_resolvedName_1() const { return ___resolvedName_1; }
	inline bool* get_address_of_resolvedName_1() { return &___resolvedName_1; }
	inline void set_resolvedName_1(bool value)
	{
		___resolvedName_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRVARIABLE_T3362236065_H
#ifndef EXPRFUNCTIONCALL_T1376373777_H
#define EXPRFUNCTIONCALL_T1376373777_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.ExprFunctionCall
struct  ExprFunctionCall_t1376373777  : public Expression_t1283317256
{
public:
	// System.Xml.XmlQualifiedName System.Xml.XPath.ExprFunctionCall::_name
	XmlQualifiedName_t1944712516 * ____name_0;
	// System.Boolean System.Xml.XPath.ExprFunctionCall::resolvedName
	bool ___resolvedName_1;
	// System.Collections.ArrayList System.Xml.XPath.ExprFunctionCall::_args
	ArrayList_t4252133567 * ____args_2;

public:
	inline static int32_t get_offset_of__name_0() { return static_cast<int32_t>(offsetof(ExprFunctionCall_t1376373777, ____name_0)); }
	inline XmlQualifiedName_t1944712516 * get__name_0() const { return ____name_0; }
	inline XmlQualifiedName_t1944712516 ** get_address_of__name_0() { return &____name_0; }
	inline void set__name_0(XmlQualifiedName_t1944712516 * value)
	{
		____name_0 = value;
		Il2CppCodeGenWriteBarrier((&____name_0), value);
	}

	inline static int32_t get_offset_of_resolvedName_1() { return static_cast<int32_t>(offsetof(ExprFunctionCall_t1376373777, ___resolvedName_1)); }
	inline bool get_resolvedName_1() const { return ___resolvedName_1; }
	inline bool* get_address_of_resolvedName_1() { return &___resolvedName_1; }
	inline void set_resolvedName_1(bool value)
	{
		___resolvedName_1 = value;
	}

	inline static int32_t get_offset_of__args_2() { return static_cast<int32_t>(offsetof(ExprFunctionCall_t1376373777, ____args_2)); }
	inline ArrayList_t4252133567 * get__args_2() const { return ____args_2; }
	inline ArrayList_t4252133567 ** get_address_of__args_2() { return &____args_2; }
	inline void set__args_2(ArrayList_t4252133567 * value)
	{
		____args_2 = value;
		Il2CppCodeGenWriteBarrier((&____args_2), value);
	}
};

struct ExprFunctionCall_t1376373777_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.XPath.ExprFunctionCall::<>f__switch$map41
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map41_3;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map41_3() { return static_cast<int32_t>(offsetof(ExprFunctionCall_t1376373777_StaticFields, ___U3CU3Ef__switchU24map41_3)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map41_3() const { return ___U3CU3Ef__switchU24map41_3; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map41_3() { return &___U3CU3Ef__switchU24map41_3; }
	inline void set_U3CU3Ef__switchU24map41_3(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map41_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map41_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRFUNCTIONCALL_T1376373777_H
#ifndef EXPRNUMBER_T2687440122_H
#define EXPRNUMBER_T2687440122_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.ExprNumber
struct  ExprNumber_t2687440122  : public Expression_t1283317256
{
public:
	// System.Double System.Xml.XPath.ExprNumber::_value
	double ____value_0;

public:
	inline static int32_t get_offset_of__value_0() { return static_cast<int32_t>(offsetof(ExprNumber_t2687440122, ____value_0)); }
	inline double get__value_0() const { return ____value_0; }
	inline double* get_address_of__value_0() { return &____value_0; }
	inline void set__value_0(double value)
	{
		____value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRNUMBER_T2687440122_H
#ifndef EXPRLITERAL_T1691835634_H
#define EXPRLITERAL_T1691835634_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.ExprLiteral
struct  ExprLiteral_t1691835634  : public Expression_t1283317256
{
public:
	// System.String System.Xml.XPath.ExprLiteral::_value
	String_t* ____value_0;

public:
	inline static int32_t get_offset_of__value_0() { return static_cast<int32_t>(offsetof(ExprLiteral_t1691835634, ____value_0)); }
	inline String_t* get__value_0() const { return ____value_0; }
	inline String_t** get_address_of__value_0() { return &____value_0; }
	inline void set__value_0(String_t* value)
	{
		____value_0 = value;
		Il2CppCodeGenWriteBarrier((&____value_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRLITERAL_T1691835634_H
#ifndef NODESET_T2895962396_H
#define NODESET_T2895962396_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.NodeSet
struct  NodeSet_t2895962396  : public Expression_t1283317256
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NODESET_T2895962396_H
#ifndef EXPRPARENS_T545584810_H
#define EXPRPARENS_T545584810_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.ExprParens
struct  ExprParens_t545584810  : public Expression_t1283317256
{
public:
	// System.Xml.XPath.Expression System.Xml.XPath.ExprParens::_expr
	Expression_t1283317256 * ____expr_0;

public:
	inline static int32_t get_offset_of__expr_0() { return static_cast<int32_t>(offsetof(ExprParens_t545584810, ____expr_0)); }
	inline Expression_t1283317256 * get__expr_0() const { return ____expr_0; }
	inline Expression_t1283317256 ** get_address_of__expr_0() { return &____expr_0; }
	inline void set__expr_0(Expression_t1283317256 * value)
	{
		____expr_0 = value;
		Il2CppCodeGenWriteBarrier((&____expr_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRPARENS_T545584810_H
#ifndef BASEITERATOR_T2454437973_H
#define BASEITERATOR_T2454437973_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.BaseIterator
struct  BaseIterator_t2454437973  : public XPathNodeIterator_t3192332357
{
public:
	// System.Xml.IXmlNamespaceResolver System.Xml.XPath.BaseIterator::_nsm
	RuntimeObject* ____nsm_1;
	// System.Int32 System.Xml.XPath.BaseIterator::position
	int32_t ___position_2;

public:
	inline static int32_t get_offset_of__nsm_1() { return static_cast<int32_t>(offsetof(BaseIterator_t2454437973, ____nsm_1)); }
	inline RuntimeObject* get__nsm_1() const { return ____nsm_1; }
	inline RuntimeObject** get_address_of__nsm_1() { return &____nsm_1; }
	inline void set__nsm_1(RuntimeObject* value)
	{
		____nsm_1 = value;
		Il2CppCodeGenWriteBarrier((&____nsm_1), value);
	}

	inline static int32_t get_offset_of_position_2() { return static_cast<int32_t>(offsetof(BaseIterator_t2454437973, ___position_2)); }
	inline int32_t get_position_2() const { return ___position_2; }
	inline int32_t* get_address_of_position_2() { return &___position_2; }
	inline void set_position_2(int32_t value)
	{
		___position_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEITERATOR_T2454437973_H
#ifndef XPATHFUNCTION_T759167395_H
#define XPATHFUNCTION_T759167395_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathFunction
struct  XPathFunction_t759167395  : public Expression_t1283317256
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHFUNCTION_T759167395_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t1328083999* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t1328083999* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t1328083999** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t1328083999* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef MONOFIXATTRIBUTE_T4096033756_H
#define MONOFIXATTRIBUTE_T4096033756_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.MonoFIXAttribute
struct  MonoFIXAttribute_t4096033756  : public Attribute_t542643598
{
public:
	// System.String System.Xml.MonoFIXAttribute::comment
	String_t* ___comment_0;

public:
	inline static int32_t get_offset_of_comment_0() { return static_cast<int32_t>(offsetof(MonoFIXAttribute_t4096033756, ___comment_0)); }
	inline String_t* get_comment_0() const { return ___comment_0; }
	inline String_t** get_address_of_comment_0() { return &___comment_0; }
	inline void set_comment_0(String_t* value)
	{
		___comment_0 = value;
		Il2CppCodeGenWriteBarrier((&___comment_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOFIXATTRIBUTE_T4096033756_H
#ifndef NAMETABLE_T594386929_H
#define NAMETABLE_T594386929_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.NameTable
struct  NameTable_t594386929  : public XmlNameTable_t1345805268
{
public:
	// System.Int32 System.Xml.NameTable::count
	int32_t ___count_0;
	// System.Xml.NameTable/Entry[] System.Xml.NameTable::buckets
	EntryU5BU5D_t180042139* ___buckets_1;
	// System.Int32 System.Xml.NameTable::size
	int32_t ___size_2;

public:
	inline static int32_t get_offset_of_count_0() { return static_cast<int32_t>(offsetof(NameTable_t594386929, ___count_0)); }
	inline int32_t get_count_0() const { return ___count_0; }
	inline int32_t* get_address_of_count_0() { return &___count_0; }
	inline void set_count_0(int32_t value)
	{
		___count_0 = value;
	}

	inline static int32_t get_offset_of_buckets_1() { return static_cast<int32_t>(offsetof(NameTable_t594386929, ___buckets_1)); }
	inline EntryU5BU5D_t180042139* get_buckets_1() const { return ___buckets_1; }
	inline EntryU5BU5D_t180042139** get_address_of_buckets_1() { return &___buckets_1; }
	inline void set_buckets_1(EntryU5BU5D_t180042139* value)
	{
		___buckets_1 = value;
		Il2CppCodeGenWriteBarrier((&___buckets_1), value);
	}

	inline static int32_t get_offset_of_size_2() { return static_cast<int32_t>(offsetof(NameTable_t594386929, ___size_2)); }
	inline int32_t get_size_2() const { return ___size_2; }
	inline int32_t* get_address_of_size_2() { return &___size_2; }
	inline void set_size_2(int32_t value)
	{
		___size_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMETABLE_T594386929_H
#ifndef SYSTEMEXCEPTION_T3877406272_H
#define SYSTEMEXCEPTION_T3877406272_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t3877406272  : public Exception_t1927440687
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T3877406272_H
#ifndef XSLTCONTEXT_T2013960098_H
#define XSLTCONTEXT_T2013960098_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Xsl.XsltContext
struct  XsltContext_t2013960098  : public XmlNamespaceManager_t486731501
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSLTCONTEXT_T2013960098_H
#ifndef ENUMERABLEITERATOR_T1602910416_H
#define ENUMERABLEITERATOR_T1602910416_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathNavigator/EnumerableIterator
struct  EnumerableIterator_t1602910416  : public XPathNodeIterator_t3192332357
{
public:
	// System.Collections.IEnumerable System.Xml.XPath.XPathNavigator/EnumerableIterator::source
	RuntimeObject* ___source_1;
	// System.Collections.IEnumerator System.Xml.XPath.XPathNavigator/EnumerableIterator::e
	RuntimeObject* ___e_2;
	// System.Int32 System.Xml.XPath.XPathNavigator/EnumerableIterator::pos
	int32_t ___pos_3;

public:
	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(EnumerableIterator_t1602910416, ___source_1)); }
	inline RuntimeObject* get_source_1() const { return ___source_1; }
	inline RuntimeObject** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(RuntimeObject* value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier((&___source_1), value);
	}

	inline static int32_t get_offset_of_e_2() { return static_cast<int32_t>(offsetof(EnumerableIterator_t1602910416, ___e_2)); }
	inline RuntimeObject* get_e_2() const { return ___e_2; }
	inline RuntimeObject** get_address_of_e_2() { return &___e_2; }
	inline void set_e_2(RuntimeObject* value)
	{
		___e_2 = value;
		Il2CppCodeGenWriteBarrier((&___e_2), value);
	}

	inline static int32_t get_offset_of_pos_3() { return static_cast<int32_t>(offsetof(EnumerableIterator_t1602910416, ___pos_3)); }
	inline int32_t get_pos_3() const { return ___pos_3; }
	inline int32_t* get_address_of_pos_3() { return &___pos_3; }
	inline void set_pos_3(int32_t value)
	{
		___pos_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERABLEITERATOR_T1602910416_H
#ifndef XPATHNAVIGATOR_T3981235968_H
#define XPATHNAVIGATOR_T3981235968_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathNavigator
struct  XPathNavigator_t3981235968  : public XPathItem_t3130801258
{
public:

public:
};

struct XPathNavigator_t3981235968_StaticFields
{
public:
	// System.Char[] System.Xml.XPath.XPathNavigator::escape_text_chars
	CharU5BU5D_t1328083999* ___escape_text_chars_0;
	// System.Char[] System.Xml.XPath.XPathNavigator::escape_attr_chars
	CharU5BU5D_t1328083999* ___escape_attr_chars_1;

public:
	inline static int32_t get_offset_of_escape_text_chars_0() { return static_cast<int32_t>(offsetof(XPathNavigator_t3981235968_StaticFields, ___escape_text_chars_0)); }
	inline CharU5BU5D_t1328083999* get_escape_text_chars_0() const { return ___escape_text_chars_0; }
	inline CharU5BU5D_t1328083999** get_address_of_escape_text_chars_0() { return &___escape_text_chars_0; }
	inline void set_escape_text_chars_0(CharU5BU5D_t1328083999* value)
	{
		___escape_text_chars_0 = value;
		Il2CppCodeGenWriteBarrier((&___escape_text_chars_0), value);
	}

	inline static int32_t get_offset_of_escape_attr_chars_1() { return static_cast<int32_t>(offsetof(XPathNavigator_t3981235968_StaticFields, ___escape_attr_chars_1)); }
	inline CharU5BU5D_t1328083999* get_escape_attr_chars_1() const { return ___escape_attr_chars_1; }
	inline CharU5BU5D_t1328083999** get_address_of_escape_attr_chars_1() { return &___escape_attr_chars_1; }
	inline void set_escape_attr_chars_1(CharU5BU5D_t1328083999* value)
	{
		___escape_attr_chars_1 = value;
		Il2CppCodeGenWriteBarrier((&___escape_attr_chars_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHNAVIGATOR_T3981235968_H
#ifndef COMPILEDEXPRESSION_T3686330919_H
#define COMPILEDEXPRESSION_T3686330919_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.CompiledExpression
struct  CompiledExpression_t3686330919  : public XPathExpression_t452251917
{
public:
	// System.Xml.IXmlNamespaceResolver System.Xml.XPath.CompiledExpression::_nsm
	RuntimeObject* ____nsm_0;
	// System.Xml.XPath.Expression System.Xml.XPath.CompiledExpression::_expr
	Expression_t1283317256 * ____expr_1;
	// System.Xml.XPath.XPathSorters System.Xml.XPath.CompiledExpression::_sorters
	XPathSorters_t4019574815 * ____sorters_2;
	// System.String System.Xml.XPath.CompiledExpression::rawExpression
	String_t* ___rawExpression_3;

public:
	inline static int32_t get_offset_of__nsm_0() { return static_cast<int32_t>(offsetof(CompiledExpression_t3686330919, ____nsm_0)); }
	inline RuntimeObject* get__nsm_0() const { return ____nsm_0; }
	inline RuntimeObject** get_address_of__nsm_0() { return &____nsm_0; }
	inline void set__nsm_0(RuntimeObject* value)
	{
		____nsm_0 = value;
		Il2CppCodeGenWriteBarrier((&____nsm_0), value);
	}

	inline static int32_t get_offset_of__expr_1() { return static_cast<int32_t>(offsetof(CompiledExpression_t3686330919, ____expr_1)); }
	inline Expression_t1283317256 * get__expr_1() const { return ____expr_1; }
	inline Expression_t1283317256 ** get_address_of__expr_1() { return &____expr_1; }
	inline void set__expr_1(Expression_t1283317256 * value)
	{
		____expr_1 = value;
		Il2CppCodeGenWriteBarrier((&____expr_1), value);
	}

	inline static int32_t get_offset_of__sorters_2() { return static_cast<int32_t>(offsetof(CompiledExpression_t3686330919, ____sorters_2)); }
	inline XPathSorters_t4019574815 * get__sorters_2() const { return ____sorters_2; }
	inline XPathSorters_t4019574815 ** get_address_of__sorters_2() { return &____sorters_2; }
	inline void set__sorters_2(XPathSorters_t4019574815 * value)
	{
		____sorters_2 = value;
		Il2CppCodeGenWriteBarrier((&____sorters_2), value);
	}

	inline static int32_t get_offset_of_rawExpression_3() { return static_cast<int32_t>(offsetof(CompiledExpression_t3686330919, ___rawExpression_3)); }
	inline String_t* get_rawExpression_3() const { return ___rawExpression_3; }
	inline String_t** get_address_of_rawExpression_3() { return &___rawExpression_3; }
	inline void set_rawExpression_3(String_t* value)
	{
		___rawExpression_3 = value;
		Il2CppCodeGenWriteBarrier((&___rawExpression_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPILEDEXPRESSION_T3686330919_H
#ifndef EXPRBINARY_T2134514854_H
#define EXPRBINARY_T2134514854_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.ExprBinary
struct  ExprBinary_t2134514854  : public Expression_t1283317256
{
public:
	// System.Xml.XPath.Expression System.Xml.XPath.ExprBinary::_left
	Expression_t1283317256 * ____left_0;
	// System.Xml.XPath.Expression System.Xml.XPath.ExprBinary::_right
	Expression_t1283317256 * ____right_1;

public:
	inline static int32_t get_offset_of__left_0() { return static_cast<int32_t>(offsetof(ExprBinary_t2134514854, ____left_0)); }
	inline Expression_t1283317256 * get__left_0() const { return ____left_0; }
	inline Expression_t1283317256 ** get_address_of__left_0() { return &____left_0; }
	inline void set__left_0(Expression_t1283317256 * value)
	{
		____left_0 = value;
		Il2CppCodeGenWriteBarrier((&____left_0), value);
	}

	inline static int32_t get_offset_of__right_1() { return static_cast<int32_t>(offsetof(ExprBinary_t2134514854, ____right_1)); }
	inline Expression_t1283317256 * get__right_1() const { return ____right_1; }
	inline Expression_t1283317256 ** get_address_of__right_1() { return &____right_1; }
	inline void set__right_1(Expression_t1283317256 * value)
	{
		____right_1 = value;
		Il2CppCodeGenWriteBarrier((&____right_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRBINARY_T2134514854_H
#ifndef XPATHRESULTTYPE_T1521569578_H
#define XPATHRESULTTYPE_T1521569578_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathResultType
struct  XPathResultType_t1521569578 
{
public:
	// System.Int32 System.Xml.XPath.XPathResultType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XPathResultType_t1521569578, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHRESULTTYPE_T1521569578_H
#ifndef XPATHNODETYPE_T817388867_H
#define XPATHNODETYPE_T817388867_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathNodeType
struct  XPathNodeType_t817388867 
{
public:
	// System.Int32 System.Xml.XPath.XPathNodeType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XPathNodeType_t817388867, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHNODETYPE_T817388867_H
#ifndef CHILDITERATOR_T1144511930_H
#define CHILDITERATOR_T1144511930_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.ChildIterator
struct  ChildIterator_t1144511930  : public BaseIterator_t2454437973
{
public:
	// System.Xml.XPath.XPathNavigator System.Xml.XPath.ChildIterator::_nav
	XPathNavigator_t3981235968 * ____nav_3;

public:
	inline static int32_t get_offset_of__nav_3() { return static_cast<int32_t>(offsetof(ChildIterator_t1144511930, ____nav_3)); }
	inline XPathNavigator_t3981235968 * get__nav_3() const { return ____nav_3; }
	inline XPathNavigator_t3981235968 ** get_address_of__nav_3() { return &____nav_3; }
	inline void set__nav_3(XPathNavigator_t3981235968 * value)
	{
		____nav_3 = value;
		Il2CppCodeGenWriteBarrier((&____nav_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHILDITERATOR_T1144511930_H
#ifndef PARENSITERATOR_T1637647049_H
#define PARENSITERATOR_T1637647049_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.ParensIterator
struct  ParensIterator_t1637647049  : public BaseIterator_t2454437973
{
public:
	// System.Xml.XPath.BaseIterator System.Xml.XPath.ParensIterator::_iter
	BaseIterator_t2454437973 * ____iter_3;

public:
	inline static int32_t get_offset_of__iter_3() { return static_cast<int32_t>(offsetof(ParensIterator_t1637647049, ____iter_3)); }
	inline BaseIterator_t2454437973 * get__iter_3() const { return ____iter_3; }
	inline BaseIterator_t2454437973 ** get_address_of__iter_3() { return &____iter_3; }
	inline void set__iter_3(BaseIterator_t2454437973 * value)
	{
		____iter_3 = value;
		Il2CppCodeGenWriteBarrier((&____iter_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARENSITERATOR_T1637647049_H
#ifndef WRAPPERITERATOR_T2718786873_H
#define WRAPPERITERATOR_T2718786873_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.WrapperIterator
struct  WrapperIterator_t2718786873  : public BaseIterator_t2454437973
{
public:
	// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.WrapperIterator::iter
	XPathNodeIterator_t3192332357 * ___iter_3;

public:
	inline static int32_t get_offset_of_iter_3() { return static_cast<int32_t>(offsetof(WrapperIterator_t2718786873, ___iter_3)); }
	inline XPathNodeIterator_t3192332357 * get_iter_3() const { return ___iter_3; }
	inline XPathNodeIterator_t3192332357 ** get_address_of_iter_3() { return &___iter_3; }
	inline void set_iter_3(XPathNodeIterator_t3192332357 * value)
	{
		___iter_3 = value;
		Il2CppCodeGenWriteBarrier((&___iter_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRAPPERITERATOR_T2718786873_H
#ifndef UNIONITERATOR_T284038993_H
#define UNIONITERATOR_T284038993_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.UnionIterator
struct  UnionIterator_t284038993  : public BaseIterator_t2454437973
{
public:
	// System.Xml.XPath.BaseIterator System.Xml.XPath.UnionIterator::_left
	BaseIterator_t2454437973 * ____left_3;
	// System.Xml.XPath.BaseIterator System.Xml.XPath.UnionIterator::_right
	BaseIterator_t2454437973 * ____right_4;
	// System.Boolean System.Xml.XPath.UnionIterator::keepLeft
	bool ___keepLeft_5;
	// System.Boolean System.Xml.XPath.UnionIterator::keepRight
	bool ___keepRight_6;
	// System.Xml.XPath.XPathNavigator System.Xml.XPath.UnionIterator::_current
	XPathNavigator_t3981235968 * ____current_7;

public:
	inline static int32_t get_offset_of__left_3() { return static_cast<int32_t>(offsetof(UnionIterator_t284038993, ____left_3)); }
	inline BaseIterator_t2454437973 * get__left_3() const { return ____left_3; }
	inline BaseIterator_t2454437973 ** get_address_of__left_3() { return &____left_3; }
	inline void set__left_3(BaseIterator_t2454437973 * value)
	{
		____left_3 = value;
		Il2CppCodeGenWriteBarrier((&____left_3), value);
	}

	inline static int32_t get_offset_of__right_4() { return static_cast<int32_t>(offsetof(UnionIterator_t284038993, ____right_4)); }
	inline BaseIterator_t2454437973 * get__right_4() const { return ____right_4; }
	inline BaseIterator_t2454437973 ** get_address_of__right_4() { return &____right_4; }
	inline void set__right_4(BaseIterator_t2454437973 * value)
	{
		____right_4 = value;
		Il2CppCodeGenWriteBarrier((&____right_4), value);
	}

	inline static int32_t get_offset_of_keepLeft_5() { return static_cast<int32_t>(offsetof(UnionIterator_t284038993, ___keepLeft_5)); }
	inline bool get_keepLeft_5() const { return ___keepLeft_5; }
	inline bool* get_address_of_keepLeft_5() { return &___keepLeft_5; }
	inline void set_keepLeft_5(bool value)
	{
		___keepLeft_5 = value;
	}

	inline static int32_t get_offset_of_keepRight_6() { return static_cast<int32_t>(offsetof(UnionIterator_t284038993, ___keepRight_6)); }
	inline bool get_keepRight_6() const { return ___keepRight_6; }
	inline bool* get_address_of_keepRight_6() { return &___keepRight_6; }
	inline void set_keepRight_6(bool value)
	{
		___keepRight_6 = value;
	}

	inline static int32_t get_offset_of__current_7() { return static_cast<int32_t>(offsetof(UnionIterator_t284038993, ____current_7)); }
	inline XPathNavigator_t3981235968 * get__current_7() const { return ____current_7; }
	inline XPathNavigator_t3981235968 ** get_address_of__current_7() { return &____current_7; }
	inline void set__current_7(XPathNavigator_t3981235968 * value)
	{
		____current_7 = value;
		Il2CppCodeGenWriteBarrier((&____current_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIONITERATOR_T284038993_H
#ifndef XMLDATATYPE_T315095065_H
#define XMLDATATYPE_T315095065_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XmlDataType
struct  XmlDataType_t315095065 
{
public:
	// System.Int32 System.Xml.XPath.XmlDataType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XmlDataType_t315095065, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLDATATYPE_T315095065_H
#ifndef SIMPLESLASHITERATOR_T400959233_H
#define SIMPLESLASHITERATOR_T400959233_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.SimpleSlashIterator
struct  SimpleSlashIterator_t400959233  : public BaseIterator_t2454437973
{
public:
	// System.Xml.XPath.NodeSet System.Xml.XPath.SimpleSlashIterator::_expr
	NodeSet_t2895962396 * ____expr_3;
	// System.Xml.XPath.BaseIterator System.Xml.XPath.SimpleSlashIterator::_left
	BaseIterator_t2454437973 * ____left_4;
	// System.Xml.XPath.BaseIterator System.Xml.XPath.SimpleSlashIterator::_right
	BaseIterator_t2454437973 * ____right_5;
	// System.Xml.XPath.XPathNavigator System.Xml.XPath.SimpleSlashIterator::_current
	XPathNavigator_t3981235968 * ____current_6;

public:
	inline static int32_t get_offset_of__expr_3() { return static_cast<int32_t>(offsetof(SimpleSlashIterator_t400959233, ____expr_3)); }
	inline NodeSet_t2895962396 * get__expr_3() const { return ____expr_3; }
	inline NodeSet_t2895962396 ** get_address_of__expr_3() { return &____expr_3; }
	inline void set__expr_3(NodeSet_t2895962396 * value)
	{
		____expr_3 = value;
		Il2CppCodeGenWriteBarrier((&____expr_3), value);
	}

	inline static int32_t get_offset_of__left_4() { return static_cast<int32_t>(offsetof(SimpleSlashIterator_t400959233, ____left_4)); }
	inline BaseIterator_t2454437973 * get__left_4() const { return ____left_4; }
	inline BaseIterator_t2454437973 ** get_address_of__left_4() { return &____left_4; }
	inline void set__left_4(BaseIterator_t2454437973 * value)
	{
		____left_4 = value;
		Il2CppCodeGenWriteBarrier((&____left_4), value);
	}

	inline static int32_t get_offset_of__right_5() { return static_cast<int32_t>(offsetof(SimpleSlashIterator_t400959233, ____right_5)); }
	inline BaseIterator_t2454437973 * get__right_5() const { return ____right_5; }
	inline BaseIterator_t2454437973 ** get_address_of__right_5() { return &____right_5; }
	inline void set__right_5(BaseIterator_t2454437973 * value)
	{
		____right_5 = value;
		Il2CppCodeGenWriteBarrier((&____right_5), value);
	}

	inline static int32_t get_offset_of__current_6() { return static_cast<int32_t>(offsetof(SimpleSlashIterator_t400959233, ____current_6)); }
	inline XPathNavigator_t3981235968 * get__current_6() const { return ____current_6; }
	inline XPathNavigator_t3981235968 ** get_address_of__current_6() { return &____current_6; }
	inline void set__current_6(XPathNavigator_t3981235968 * value)
	{
		____current_6 = value;
		Il2CppCodeGenWriteBarrier((&____current_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLESLASHITERATOR_T400959233_H
#ifndef AXISITERATOR_T3253336785_H
#define AXISITERATOR_T3253336785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.AxisIterator
struct  AxisIterator_t3253336785  : public BaseIterator_t2454437973
{
public:
	// System.Xml.XPath.BaseIterator System.Xml.XPath.AxisIterator::_iter
	BaseIterator_t2454437973 * ____iter_3;
	// System.Xml.XPath.NodeTest System.Xml.XPath.AxisIterator::_test
	NodeTest_t2072055152 * ____test_4;

public:
	inline static int32_t get_offset_of__iter_3() { return static_cast<int32_t>(offsetof(AxisIterator_t3253336785, ____iter_3)); }
	inline BaseIterator_t2454437973 * get__iter_3() const { return ____iter_3; }
	inline BaseIterator_t2454437973 ** get_address_of__iter_3() { return &____iter_3; }
	inline void set__iter_3(BaseIterator_t2454437973 * value)
	{
		____iter_3 = value;
		Il2CppCodeGenWriteBarrier((&____iter_3), value);
	}

	inline static int32_t get_offset_of__test_4() { return static_cast<int32_t>(offsetof(AxisIterator_t3253336785, ____test_4)); }
	inline NodeTest_t2072055152 * get__test_4() const { return ____test_4; }
	inline NodeTest_t2072055152 ** get_address_of__test_4() { return &____test_4; }
	inline void set__test_4(NodeTest_t2072055152 * value)
	{
		____test_4 = value;
		Il2CppCodeGenWriteBarrier((&____test_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISITERATOR_T3253336785_H
#ifndef SLASHITERATOR_T3247429967_H
#define SLASHITERATOR_T3247429967_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.SlashIterator
struct  SlashIterator_t3247429967  : public BaseIterator_t2454437973
{
public:
	// System.Xml.XPath.BaseIterator System.Xml.XPath.SlashIterator::_iterLeft
	BaseIterator_t2454437973 * ____iterLeft_3;
	// System.Xml.XPath.BaseIterator System.Xml.XPath.SlashIterator::_iterRight
	BaseIterator_t2454437973 * ____iterRight_4;
	// System.Xml.XPath.NodeSet System.Xml.XPath.SlashIterator::_expr
	NodeSet_t2895962396 * ____expr_5;
	// System.Collections.SortedList System.Xml.XPath.SlashIterator::_iterList
	SortedList_t3004938869 * ____iterList_6;
	// System.Boolean System.Xml.XPath.SlashIterator::_finished
	bool ____finished_7;
	// System.Xml.XPath.BaseIterator System.Xml.XPath.SlashIterator::_nextIterRight
	BaseIterator_t2454437973 * ____nextIterRight_8;

public:
	inline static int32_t get_offset_of__iterLeft_3() { return static_cast<int32_t>(offsetof(SlashIterator_t3247429967, ____iterLeft_3)); }
	inline BaseIterator_t2454437973 * get__iterLeft_3() const { return ____iterLeft_3; }
	inline BaseIterator_t2454437973 ** get_address_of__iterLeft_3() { return &____iterLeft_3; }
	inline void set__iterLeft_3(BaseIterator_t2454437973 * value)
	{
		____iterLeft_3 = value;
		Il2CppCodeGenWriteBarrier((&____iterLeft_3), value);
	}

	inline static int32_t get_offset_of__iterRight_4() { return static_cast<int32_t>(offsetof(SlashIterator_t3247429967, ____iterRight_4)); }
	inline BaseIterator_t2454437973 * get__iterRight_4() const { return ____iterRight_4; }
	inline BaseIterator_t2454437973 ** get_address_of__iterRight_4() { return &____iterRight_4; }
	inline void set__iterRight_4(BaseIterator_t2454437973 * value)
	{
		____iterRight_4 = value;
		Il2CppCodeGenWriteBarrier((&____iterRight_4), value);
	}

	inline static int32_t get_offset_of__expr_5() { return static_cast<int32_t>(offsetof(SlashIterator_t3247429967, ____expr_5)); }
	inline NodeSet_t2895962396 * get__expr_5() const { return ____expr_5; }
	inline NodeSet_t2895962396 ** get_address_of__expr_5() { return &____expr_5; }
	inline void set__expr_5(NodeSet_t2895962396 * value)
	{
		____expr_5 = value;
		Il2CppCodeGenWriteBarrier((&____expr_5), value);
	}

	inline static int32_t get_offset_of__iterList_6() { return static_cast<int32_t>(offsetof(SlashIterator_t3247429967, ____iterList_6)); }
	inline SortedList_t3004938869 * get__iterList_6() const { return ____iterList_6; }
	inline SortedList_t3004938869 ** get_address_of__iterList_6() { return &____iterList_6; }
	inline void set__iterList_6(SortedList_t3004938869 * value)
	{
		____iterList_6 = value;
		Il2CppCodeGenWriteBarrier((&____iterList_6), value);
	}

	inline static int32_t get_offset_of__finished_7() { return static_cast<int32_t>(offsetof(SlashIterator_t3247429967, ____finished_7)); }
	inline bool get__finished_7() const { return ____finished_7; }
	inline bool* get_address_of__finished_7() { return &____finished_7; }
	inline void set__finished_7(bool value)
	{
		____finished_7 = value;
	}

	inline static int32_t get_offset_of__nextIterRight_8() { return static_cast<int32_t>(offsetof(SlashIterator_t3247429967, ____nextIterRight_8)); }
	inline BaseIterator_t2454437973 * get__nextIterRight_8() const { return ____nextIterRight_8; }
	inline BaseIterator_t2454437973 ** get_address_of__nextIterRight_8() { return &____nextIterRight_8; }
	inline void set__nextIterRight_8(BaseIterator_t2454437973 * value)
	{
		____nextIterRight_8 = value;
		Il2CppCodeGenWriteBarrier((&____nextIterRight_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SLASHITERATOR_T3247429967_H
#ifndef SORTEDITERATOR_T3199707209_H
#define SORTEDITERATOR_T3199707209_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.SortedIterator
struct  SortedIterator_t3199707209  : public BaseIterator_t2454437973
{
public:
	// System.Collections.ArrayList System.Xml.XPath.SortedIterator::list
	ArrayList_t4252133567 * ___list_3;

public:
	inline static int32_t get_offset_of_list_3() { return static_cast<int32_t>(offsetof(SortedIterator_t3199707209, ___list_3)); }
	inline ArrayList_t4252133567 * get_list_3() const { return ___list_3; }
	inline ArrayList_t4252133567 ** get_address_of_list_3() { return &___list_3; }
	inline void set_list_3(ArrayList_t4252133567 * value)
	{
		___list_3 = value;
		Il2CppCodeGenWriteBarrier((&___list_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SORTEDITERATOR_T3199707209_H
#ifndef EXPRBOOLEAN_T2584142721_H
#define EXPRBOOLEAN_T2584142721_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.ExprBoolean
struct  ExprBoolean_t2584142721  : public ExprBinary_t2134514854
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRBOOLEAN_T2584142721_H
#ifndef LISTITERATOR_T3804705054_H
#define LISTITERATOR_T3804705054_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.ListIterator
struct  ListIterator_t3804705054  : public BaseIterator_t2454437973
{
public:
	// System.Collections.IList System.Xml.XPath.ListIterator::_list
	RuntimeObject* ____list_3;

public:
	inline static int32_t get_offset_of__list_3() { return static_cast<int32_t>(offsetof(ListIterator_t3804705054, ____list_3)); }
	inline RuntimeObject* get__list_3() const { return ____list_3; }
	inline RuntimeObject** get_address_of__list_3() { return &____list_3; }
	inline void set__list_3(RuntimeObject* value)
	{
		____list_3 = value;
		Il2CppCodeGenWriteBarrier((&____list_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTITERATOR_T3804705054_H
#ifndef XPATHEXCEPTION_T1503722168_H
#define XPATHEXCEPTION_T1503722168_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathException
struct  XPathException_t1503722168  : public SystemException_t3877406272
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHEXCEPTION_T1503722168_H
#ifndef XPATHNAMESPACESCOPE_T3601604274_H
#define XPATHNAMESPACESCOPE_T3601604274_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathNamespaceScope
struct  XPathNamespaceScope_t3601604274 
{
public:
	// System.Int32 System.Xml.XPath.XPathNamespaceScope::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XPathNamespaceScope_t3601604274, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHNAMESPACESCOPE_T3601604274_H
#ifndef XPATHBOOLEANFUNCTION_T2511646183_H
#define XPATHBOOLEANFUNCTION_T2511646183_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathBooleanFunction
struct  XPathBooleanFunction_t2511646183  : public XPathFunction_t759167395
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHBOOLEANFUNCTION_T2511646183_H
#ifndef NODETEST_T2072055152_H
#define NODETEST_T2072055152_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.NodeTest
struct  NodeTest_t2072055152  : public NodeSet_t2895962396
{
public:
	// System.Xml.XPath.AxisSpecifier System.Xml.XPath.NodeTest::_axis
	AxisSpecifier_t201930955 * ____axis_0;

public:
	inline static int32_t get_offset_of__axis_0() { return static_cast<int32_t>(offsetof(NodeTest_t2072055152, ____axis_0)); }
	inline AxisSpecifier_t201930955 * get__axis_0() const { return ____axis_0; }
	inline AxisSpecifier_t201930955 ** get_address_of__axis_0() { return &____axis_0; }
	inline void set__axis_0(AxisSpecifier_t201930955 * value)
	{
		____axis_0 = value;
		Il2CppCodeGenWriteBarrier((&____axis_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NODETEST_T2072055152_H
#ifndef XPATHFUNCTIONLANG_T718178243_H
#define XPATHFUNCTIONLANG_T718178243_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathFunctionLang
struct  XPathFunctionLang_t718178243  : public XPathFunction_t759167395
{
public:
	// System.Xml.XPath.Expression System.Xml.XPath.XPathFunctionLang::arg0
	Expression_t1283317256 * ___arg0_0;

public:
	inline static int32_t get_offset_of_arg0_0() { return static_cast<int32_t>(offsetof(XPathFunctionLang_t718178243, ___arg0_0)); }
	inline Expression_t1283317256 * get_arg0_0() const { return ___arg0_0; }
	inline Expression_t1283317256 ** get_address_of_arg0_0() { return &___arg0_0; }
	inline void set_arg0_0(Expression_t1283317256 * value)
	{
		___arg0_0 = value;
		Il2CppCodeGenWriteBarrier((&___arg0_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHFUNCTIONLANG_T718178243_H
#ifndef EXPRROOT_T2449183281_H
#define EXPRROOT_T2449183281_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.ExprRoot
struct  ExprRoot_t2449183281  : public NodeSet_t2895962396
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRROOT_T2449183281_H
#ifndef EXPRUNION_T1011953538_H
#define EXPRUNION_T1011953538_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.ExprUNION
struct  ExprUNION_t1011953538  : public NodeSet_t2895962396
{
public:
	// System.Xml.XPath.Expression System.Xml.XPath.ExprUNION::left
	Expression_t1283317256 * ___left_0;
	// System.Xml.XPath.Expression System.Xml.XPath.ExprUNION::right
	Expression_t1283317256 * ___right_1;

public:
	inline static int32_t get_offset_of_left_0() { return static_cast<int32_t>(offsetof(ExprUNION_t1011953538, ___left_0)); }
	inline Expression_t1283317256 * get_left_0() const { return ___left_0; }
	inline Expression_t1283317256 ** get_address_of_left_0() { return &___left_0; }
	inline void set_left_0(Expression_t1283317256 * value)
	{
		___left_0 = value;
		Il2CppCodeGenWriteBarrier((&___left_0), value);
	}

	inline static int32_t get_offset_of_right_1() { return static_cast<int32_t>(offsetof(ExprUNION_t1011953538, ___right_1)); }
	inline Expression_t1283317256 * get_right_1() const { return ___right_1; }
	inline Expression_t1283317256 ** get_address_of_right_1() { return &___right_1; }
	inline void set_right_1(Expression_t1283317256 * value)
	{
		___right_1 = value;
		Il2CppCodeGenWriteBarrier((&___right_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRUNION_T1011953538_H
#ifndef AXES_T2027048019_H
#define AXES_T2027048019_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.Axes
struct  Axes_t2027048019 
{
public:
	// System.Int32 System.Xml.XPath.Axes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Axes_t2027048019, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXES_T2027048019_H
#ifndef EXPRSLASH2_T1892232446_H
#define EXPRSLASH2_T1892232446_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.ExprSLASH2
struct  ExprSLASH2_t1892232446  : public NodeSet_t2895962396
{
public:
	// System.Xml.XPath.Expression System.Xml.XPath.ExprSLASH2::left
	Expression_t1283317256 * ___left_0;
	// System.Xml.XPath.NodeSet System.Xml.XPath.ExprSLASH2::right
	NodeSet_t2895962396 * ___right_1;

public:
	inline static int32_t get_offset_of_left_0() { return static_cast<int32_t>(offsetof(ExprSLASH2_t1892232446, ___left_0)); }
	inline Expression_t1283317256 * get_left_0() const { return ___left_0; }
	inline Expression_t1283317256 ** get_address_of_left_0() { return &___left_0; }
	inline void set_left_0(Expression_t1283317256 * value)
	{
		___left_0 = value;
		Il2CppCodeGenWriteBarrier((&___left_0), value);
	}

	inline static int32_t get_offset_of_right_1() { return static_cast<int32_t>(offsetof(ExprSLASH2_t1892232446, ___right_1)); }
	inline NodeSet_t2895962396 * get_right_1() const { return ___right_1; }
	inline NodeSet_t2895962396 ** get_address_of_right_1() { return &___right_1; }
	inline void set_right_1(NodeSet_t2895962396 * value)
	{
		___right_1 = value;
		Il2CppCodeGenWriteBarrier((&___right_1), value);
	}
};

struct ExprSLASH2_t1892232446_StaticFields
{
public:
	// System.Xml.XPath.NodeTest System.Xml.XPath.ExprSLASH2::DescendantOrSelfStar
	NodeTest_t2072055152 * ___DescendantOrSelfStar_2;

public:
	inline static int32_t get_offset_of_DescendantOrSelfStar_2() { return static_cast<int32_t>(offsetof(ExprSLASH2_t1892232446_StaticFields, ___DescendantOrSelfStar_2)); }
	inline NodeTest_t2072055152 * get_DescendantOrSelfStar_2() const { return ___DescendantOrSelfStar_2; }
	inline NodeTest_t2072055152 ** get_address_of_DescendantOrSelfStar_2() { return &___DescendantOrSelfStar_2; }
	inline void set_DescendantOrSelfStar_2(NodeTest_t2072055152 * value)
	{
		___DescendantOrSelfStar_2 = value;
		Il2CppCodeGenWriteBarrier((&___DescendantOrSelfStar_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRSLASH2_T1892232446_H
#ifndef EXPRSLASH_T1432925936_H
#define EXPRSLASH_T1432925936_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.ExprSLASH
struct  ExprSLASH_t1432925936  : public NodeSet_t2895962396
{
public:
	// System.Xml.XPath.Expression System.Xml.XPath.ExprSLASH::left
	Expression_t1283317256 * ___left_0;
	// System.Xml.XPath.NodeSet System.Xml.XPath.ExprSLASH::right
	NodeSet_t2895962396 * ___right_1;

public:
	inline static int32_t get_offset_of_left_0() { return static_cast<int32_t>(offsetof(ExprSLASH_t1432925936, ___left_0)); }
	inline Expression_t1283317256 * get_left_0() const { return ___left_0; }
	inline Expression_t1283317256 ** get_address_of_left_0() { return &___left_0; }
	inline void set_left_0(Expression_t1283317256 * value)
	{
		___left_0 = value;
		Il2CppCodeGenWriteBarrier((&___left_0), value);
	}

	inline static int32_t get_offset_of_right_1() { return static_cast<int32_t>(offsetof(ExprSLASH_t1432925936, ___right_1)); }
	inline NodeSet_t2895962396 * get_right_1() const { return ___right_1; }
	inline NodeSet_t2895962396 ** get_address_of_right_1() { return &___right_1; }
	inline void set_right_1(NodeSet_t2895962396 * value)
	{
		___right_1 = value;
		Il2CppCodeGenWriteBarrier((&___right_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRSLASH_T1432925936_H
#ifndef EXPRNUMERIC_T503612810_H
#define EXPRNUMERIC_T503612810_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.ExprNumeric
struct  ExprNumeric_t503612810  : public ExprBinary_t2134514854
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRNUMERIC_T503612810_H
#ifndef XPATHNUMERICFUNCTION_T2367269690_H
#define XPATHNUMERICFUNCTION_T2367269690_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathNumericFunction
struct  XPathNumericFunction_t2367269690  : public XPathFunction_t759167395
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHNUMERICFUNCTION_T2367269690_H
#ifndef SIMPLEITERATOR_T833624542_H
#define SIMPLEITERATOR_T833624542_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.SimpleIterator
struct  SimpleIterator_t833624542  : public BaseIterator_t2454437973
{
public:
	// System.Xml.XPath.XPathNavigator System.Xml.XPath.SimpleIterator::_nav
	XPathNavigator_t3981235968 * ____nav_3;
	// System.Xml.XPath.XPathNavigator System.Xml.XPath.SimpleIterator::_current
	XPathNavigator_t3981235968 * ____current_4;
	// System.Boolean System.Xml.XPath.SimpleIterator::skipfirst
	bool ___skipfirst_5;

public:
	inline static int32_t get_offset_of__nav_3() { return static_cast<int32_t>(offsetof(SimpleIterator_t833624542, ____nav_3)); }
	inline XPathNavigator_t3981235968 * get__nav_3() const { return ____nav_3; }
	inline XPathNavigator_t3981235968 ** get_address_of__nav_3() { return &____nav_3; }
	inline void set__nav_3(XPathNavigator_t3981235968 * value)
	{
		____nav_3 = value;
		Il2CppCodeGenWriteBarrier((&____nav_3), value);
	}

	inline static int32_t get_offset_of__current_4() { return static_cast<int32_t>(offsetof(SimpleIterator_t833624542, ____current_4)); }
	inline XPathNavigator_t3981235968 * get__current_4() const { return ____current_4; }
	inline XPathNavigator_t3981235968 ** get_address_of__current_4() { return &____current_4; }
	inline void set__current_4(XPathNavigator_t3981235968 * value)
	{
		____current_4 = value;
		Il2CppCodeGenWriteBarrier((&____current_4), value);
	}

	inline static int32_t get_offset_of_skipfirst_5() { return static_cast<int32_t>(offsetof(SimpleIterator_t833624542, ___skipfirst_5)); }
	inline bool get_skipfirst_5() const { return ___skipfirst_5; }
	inline bool* get_address_of_skipfirst_5() { return &___skipfirst_5; }
	inline void set_skipfirst_5(bool value)
	{
		___skipfirst_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLEITERATOR_T833624542_H
#ifndef EXPRFILTER_T1897694423_H
#define EXPRFILTER_T1897694423_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.ExprFilter
struct  ExprFilter_t1897694423  : public NodeSet_t2895962396
{
public:
	// System.Xml.XPath.Expression System.Xml.XPath.ExprFilter::expr
	Expression_t1283317256 * ___expr_0;
	// System.Xml.XPath.Expression System.Xml.XPath.ExprFilter::pred
	Expression_t1283317256 * ___pred_1;

public:
	inline static int32_t get_offset_of_expr_0() { return static_cast<int32_t>(offsetof(ExprFilter_t1897694423, ___expr_0)); }
	inline Expression_t1283317256 * get_expr_0() const { return ___expr_0; }
	inline Expression_t1283317256 ** get_address_of_expr_0() { return &___expr_0; }
	inline void set_expr_0(Expression_t1283317256 * value)
	{
		___expr_0 = value;
		Il2CppCodeGenWriteBarrier((&___expr_0), value);
	}

	inline static int32_t get_offset_of_pred_1() { return static_cast<int32_t>(offsetof(ExprFilter_t1897694423, ___pred_1)); }
	inline Expression_t1283317256 * get_pred_1() const { return ___pred_1; }
	inline Expression_t1283317256 ** get_address_of_pred_1() { return &___pred_1; }
	inline void set_pred_1(Expression_t1283317256 * value)
	{
		___pred_1 = value;
		Il2CppCodeGenWriteBarrier((&___pred_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRFILTER_T1897694423_H
#ifndef EQUALITYEXPR_T2363998129_H
#define EQUALITYEXPR_T2363998129_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.EqualityExpr
struct  EqualityExpr_t2363998129  : public ExprBoolean_t2584142721
{
public:
	// System.Boolean System.Xml.XPath.EqualityExpr::trueVal
	bool ___trueVal_2;

public:
	inline static int32_t get_offset_of_trueVal_2() { return static_cast<int32_t>(offsetof(EqualityExpr_t2363998129, ___trueVal_2)); }
	inline bool get_trueVal_2() const { return ___trueVal_2; }
	inline bool* get_address_of_trueVal_2() { return &___trueVal_2; }
	inline void set_trueVal_2(bool value)
	{
		___trueVal_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYEXPR_T2363998129_H
#ifndef XPATHFUNCTIONBOOLEAN_T2454599187_H
#define XPATHFUNCTIONBOOLEAN_T2454599187_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathFunctionBoolean
struct  XPathFunctionBoolean_t2454599187  : public XPathBooleanFunction_t2511646183
{
public:
	// System.Xml.XPath.Expression System.Xml.XPath.XPathFunctionBoolean::arg0
	Expression_t1283317256 * ___arg0_0;

public:
	inline static int32_t get_offset_of_arg0_0() { return static_cast<int32_t>(offsetof(XPathFunctionBoolean_t2454599187, ___arg0_0)); }
	inline Expression_t1283317256 * get_arg0_0() const { return ___arg0_0; }
	inline Expression_t1283317256 ** get_address_of_arg0_0() { return &___arg0_0; }
	inline void set_arg0_0(Expression_t1283317256 * value)
	{
		___arg0_0 = value;
		Il2CppCodeGenWriteBarrier((&___arg0_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHFUNCTIONBOOLEAN_T2454599187_H
#ifndef XPATHFUNCTIONTRUE_T1672815575_H
#define XPATHFUNCTIONTRUE_T1672815575_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathFunctionTrue
struct  XPathFunctionTrue_t1672815575  : public XPathBooleanFunction_t2511646183
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHFUNCTIONTRUE_T1672815575_H
#ifndef XPATHFUNCTIONNOT_T1631674504_H
#define XPATHFUNCTIONNOT_T1631674504_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathFunctionNot
struct  XPathFunctionNot_t1631674504  : public XPathBooleanFunction_t2511646183
{
public:
	// System.Xml.XPath.Expression System.Xml.XPath.XPathFunctionNot::arg0
	Expression_t1283317256 * ___arg0_0;

public:
	inline static int32_t get_offset_of_arg0_0() { return static_cast<int32_t>(offsetof(XPathFunctionNot_t1631674504, ___arg0_0)); }
	inline Expression_t1283317256 * get_arg0_0() const { return ___arg0_0; }
	inline Expression_t1283317256 ** get_address_of_arg0_0() { return &___arg0_0; }
	inline void set_arg0_0(Expression_t1283317256 * value)
	{
		___arg0_0 = value;
		Il2CppCodeGenWriteBarrier((&___arg0_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHFUNCTIONNOT_T1631674504_H
#ifndef RELATIONALEXPR_T3066272106_H
#define RELATIONALEXPR_T3066272106_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.RelationalExpr
struct  RelationalExpr_t3066272106  : public ExprBoolean_t2584142721
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RELATIONALEXPR_T3066272106_H
#ifndef EXPRAND_T3057430278_H
#define EXPRAND_T3057430278_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.ExprAND
struct  ExprAND_t3057430278  : public ExprBoolean_t2584142721
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRAND_T3057430278_H
#ifndef XPATHFUNCTIONFLOOR_T2377969453_H
#define XPATHFUNCTIONFLOOR_T2377969453_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathFunctionFloor
struct  XPathFunctionFloor_t2377969453  : public XPathNumericFunction_t2367269690
{
public:
	// System.Xml.XPath.Expression System.Xml.XPath.XPathFunctionFloor::arg0
	Expression_t1283317256 * ___arg0_0;

public:
	inline static int32_t get_offset_of_arg0_0() { return static_cast<int32_t>(offsetof(XPathFunctionFloor_t2377969453, ___arg0_0)); }
	inline Expression_t1283317256 * get_arg0_0() const { return ___arg0_0; }
	inline Expression_t1283317256 ** get_address_of_arg0_0() { return &___arg0_0; }
	inline void set_arg0_0(Expression_t1283317256 * value)
	{
		___arg0_0 = value;
		Il2CppCodeGenWriteBarrier((&___arg0_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHFUNCTIONFLOOR_T2377969453_H
#ifndef XPATHFUNCTIONSUM_T2438242634_H
#define XPATHFUNCTIONSUM_T2438242634_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathFunctionSum
struct  XPathFunctionSum_t2438242634  : public XPathNumericFunction_t2367269690
{
public:
	// System.Xml.XPath.Expression System.Xml.XPath.XPathFunctionSum::arg0
	Expression_t1283317256 * ___arg0_0;

public:
	inline static int32_t get_offset_of_arg0_0() { return static_cast<int32_t>(offsetof(XPathFunctionSum_t2438242634, ___arg0_0)); }
	inline Expression_t1283317256 * get_arg0_0() const { return ___arg0_0; }
	inline Expression_t1283317256 ** get_address_of_arg0_0() { return &___arg0_0; }
	inline void set_arg0_0(Expression_t1283317256 * value)
	{
		___arg0_0 = value;
		Il2CppCodeGenWriteBarrier((&___arg0_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHFUNCTIONSUM_T2438242634_H
#ifndef XPATHFUNCTIONCEIL_T4011711386_H
#define XPATHFUNCTIONCEIL_T4011711386_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathFunctionCeil
struct  XPathFunctionCeil_t4011711386  : public XPathNumericFunction_t2367269690
{
public:
	// System.Xml.XPath.Expression System.Xml.XPath.XPathFunctionCeil::arg0
	Expression_t1283317256 * ___arg0_0;

public:
	inline static int32_t get_offset_of_arg0_0() { return static_cast<int32_t>(offsetof(XPathFunctionCeil_t4011711386, ___arg0_0)); }
	inline Expression_t1283317256 * get_arg0_0() const { return ___arg0_0; }
	inline Expression_t1283317256 ** get_address_of_arg0_0() { return &___arg0_0; }
	inline void set_arg0_0(Expression_t1283317256 * value)
	{
		___arg0_0 = value;
		Il2CppCodeGenWriteBarrier((&___arg0_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHFUNCTIONCEIL_T4011711386_H
#ifndef XPATHFUNCTIONROUND_T711107915_H
#define XPATHFUNCTIONROUND_T711107915_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathFunctionRound
struct  XPathFunctionRound_t711107915  : public XPathNumericFunction_t2367269690
{
public:
	// System.Xml.XPath.Expression System.Xml.XPath.XPathFunctionRound::arg0
	Expression_t1283317256 * ___arg0_0;

public:
	inline static int32_t get_offset_of_arg0_0() { return static_cast<int32_t>(offsetof(XPathFunctionRound_t711107915, ___arg0_0)); }
	inline Expression_t1283317256 * get_arg0_0() const { return ___arg0_0; }
	inline Expression_t1283317256 ** get_address_of_arg0_0() { return &___arg0_0; }
	inline void set_arg0_0(Expression_t1283317256 * value)
	{
		___arg0_0 = value;
		Il2CppCodeGenWriteBarrier((&___arg0_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHFUNCTIONROUND_T711107915_H
#ifndef XPATHFUNCTIONNUMBER_T1210619362_H
#define XPATHFUNCTIONNUMBER_T1210619362_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathFunctionNumber
struct  XPathFunctionNumber_t1210619362  : public XPathNumericFunction_t2367269690
{
public:
	// System.Xml.XPath.Expression System.Xml.XPath.XPathFunctionNumber::arg0
	Expression_t1283317256 * ___arg0_0;

public:
	inline static int32_t get_offset_of_arg0_0() { return static_cast<int32_t>(offsetof(XPathFunctionNumber_t1210619362, ___arg0_0)); }
	inline Expression_t1283317256 * get_arg0_0() const { return ___arg0_0; }
	inline Expression_t1283317256 ** get_address_of_arg0_0() { return &___arg0_0; }
	inline void set_arg0_0(Expression_t1283317256 * value)
	{
		___arg0_0 = value;
		Il2CppCodeGenWriteBarrier((&___arg0_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHFUNCTIONNUMBER_T1210619362_H
#ifndef U3CENUMERATECHILDRENU3EC__ITERATOR0_T1235609798_H
#define U3CENUMERATECHILDRENU3EC__ITERATOR0_T1235609798_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathNavigator/<EnumerateChildren>c__Iterator0
struct  U3CEnumerateChildrenU3Ec__Iterator0_t1235609798  : public RuntimeObject
{
public:
	// System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator/<EnumerateChildren>c__Iterator0::n
	XPathNavigator_t3981235968 * ___n_0;
	// System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator/<EnumerateChildren>c__Iterator0::<nav>__0
	XPathNavigator_t3981235968 * ___U3CnavU3E__0_1;
	// System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator/<EnumerateChildren>c__Iterator0::<nav2>__1
	XPathNavigator_t3981235968 * ___U3Cnav2U3E__1_2;
	// System.Xml.XPath.XPathNodeType System.Xml.XPath.XPathNavigator/<EnumerateChildren>c__Iterator0::type
	int32_t ___type_3;
	// System.Int32 System.Xml.XPath.XPathNavigator/<EnumerateChildren>c__Iterator0::$PC
	int32_t ___U24PC_4;
	// System.Object System.Xml.XPath.XPathNavigator/<EnumerateChildren>c__Iterator0::$current
	RuntimeObject * ___U24current_5;
	// System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator/<EnumerateChildren>c__Iterator0::<$>n
	XPathNavigator_t3981235968 * ___U3CU24U3En_6;
	// System.Xml.XPath.XPathNodeType System.Xml.XPath.XPathNavigator/<EnumerateChildren>c__Iterator0::<$>type
	int32_t ___U3CU24U3Etype_7;

public:
	inline static int32_t get_offset_of_n_0() { return static_cast<int32_t>(offsetof(U3CEnumerateChildrenU3Ec__Iterator0_t1235609798, ___n_0)); }
	inline XPathNavigator_t3981235968 * get_n_0() const { return ___n_0; }
	inline XPathNavigator_t3981235968 ** get_address_of_n_0() { return &___n_0; }
	inline void set_n_0(XPathNavigator_t3981235968 * value)
	{
		___n_0 = value;
		Il2CppCodeGenWriteBarrier((&___n_0), value);
	}

	inline static int32_t get_offset_of_U3CnavU3E__0_1() { return static_cast<int32_t>(offsetof(U3CEnumerateChildrenU3Ec__Iterator0_t1235609798, ___U3CnavU3E__0_1)); }
	inline XPathNavigator_t3981235968 * get_U3CnavU3E__0_1() const { return ___U3CnavU3E__0_1; }
	inline XPathNavigator_t3981235968 ** get_address_of_U3CnavU3E__0_1() { return &___U3CnavU3E__0_1; }
	inline void set_U3CnavU3E__0_1(XPathNavigator_t3981235968 * value)
	{
		___U3CnavU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnavU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3Cnav2U3E__1_2() { return static_cast<int32_t>(offsetof(U3CEnumerateChildrenU3Ec__Iterator0_t1235609798, ___U3Cnav2U3E__1_2)); }
	inline XPathNavigator_t3981235968 * get_U3Cnav2U3E__1_2() const { return ___U3Cnav2U3E__1_2; }
	inline XPathNavigator_t3981235968 ** get_address_of_U3Cnav2U3E__1_2() { return &___U3Cnav2U3E__1_2; }
	inline void set_U3Cnav2U3E__1_2(XPathNavigator_t3981235968 * value)
	{
		___U3Cnav2U3E__1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cnav2U3E__1_2), value);
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(U3CEnumerateChildrenU3Ec__Iterator0_t1235609798, ___type_3)); }
	inline int32_t get_type_3() const { return ___type_3; }
	inline int32_t* get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(int32_t value)
	{
		___type_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CEnumerateChildrenU3Ec__Iterator0_t1235609798, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CEnumerateChildrenU3Ec__Iterator0_t1235609798, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U3CU24U3En_6() { return static_cast<int32_t>(offsetof(U3CEnumerateChildrenU3Ec__Iterator0_t1235609798, ___U3CU24U3En_6)); }
	inline XPathNavigator_t3981235968 * get_U3CU24U3En_6() const { return ___U3CU24U3En_6; }
	inline XPathNavigator_t3981235968 ** get_address_of_U3CU24U3En_6() { return &___U3CU24U3En_6; }
	inline void set_U3CU24U3En_6(XPathNavigator_t3981235968 * value)
	{
		___U3CU24U3En_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU24U3En_6), value);
	}

	inline static int32_t get_offset_of_U3CU24U3Etype_7() { return static_cast<int32_t>(offsetof(U3CEnumerateChildrenU3Ec__Iterator0_t1235609798, ___U3CU24U3Etype_7)); }
	inline int32_t get_U3CU24U3Etype_7() const { return ___U3CU24U3Etype_7; }
	inline int32_t* get_address_of_U3CU24U3Etype_7() { return &___U3CU24U3Etype_7; }
	inline void set_U3CU24U3Etype_7(int32_t value)
	{
		___U3CU24U3Etype_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CENUMERATECHILDRENU3EC__ITERATOR0_T1235609798_H
#ifndef EXPROR_T3873281492_H
#define EXPROR_T3873281492_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.ExprOR
struct  ExprOR_t3873281492  : public ExprBoolean_t2584142721
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPROR_T3873281492_H
#ifndef XPATHSORTER_T3491953490_H
#define XPATHSORTER_T3491953490_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathSorter
struct  XPathSorter_t3491953490  : public RuntimeObject
{
public:
	// System.Xml.XPath.Expression System.Xml.XPath.XPathSorter::_expr
	Expression_t1283317256 * ____expr_0;
	// System.Collections.IComparer System.Xml.XPath.XPathSorter::_cmp
	RuntimeObject* ____cmp_1;
	// System.Xml.XPath.XmlDataType System.Xml.XPath.XPathSorter::_type
	int32_t ____type_2;

public:
	inline static int32_t get_offset_of__expr_0() { return static_cast<int32_t>(offsetof(XPathSorter_t3491953490, ____expr_0)); }
	inline Expression_t1283317256 * get__expr_0() const { return ____expr_0; }
	inline Expression_t1283317256 ** get_address_of__expr_0() { return &____expr_0; }
	inline void set__expr_0(Expression_t1283317256 * value)
	{
		____expr_0 = value;
		Il2CppCodeGenWriteBarrier((&____expr_0), value);
	}

	inline static int32_t get_offset_of__cmp_1() { return static_cast<int32_t>(offsetof(XPathSorter_t3491953490, ____cmp_1)); }
	inline RuntimeObject* get__cmp_1() const { return ____cmp_1; }
	inline RuntimeObject** get_address_of__cmp_1() { return &____cmp_1; }
	inline void set__cmp_1(RuntimeObject* value)
	{
		____cmp_1 = value;
		Il2CppCodeGenWriteBarrier((&____cmp_1), value);
	}

	inline static int32_t get_offset_of__type_2() { return static_cast<int32_t>(offsetof(XPathSorter_t3491953490, ____type_2)); }
	inline int32_t get__type_2() const { return ____type_2; }
	inline int32_t* get_address_of__type_2() { return &____type_2; }
	inline void set__type_2(int32_t value)
	{
		____type_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHSORTER_T3491953490_H
#ifndef XPATHFUNCTIONFALSE_T2929064940_H
#define XPATHFUNCTIONFALSE_T2929064940_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathFunctionFalse
struct  XPathFunctionFalse_t2929064940  : public XPathBooleanFunction_t2511646183
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHFUNCTIONFALSE_T2929064940_H
#ifndef EXPRPLUS_T627624341_H
#define EXPRPLUS_T627624341_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.ExprPLUS
struct  ExprPLUS_t627624341  : public ExprNumeric_t503612810
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRPLUS_T627624341_H
#ifndef EXPRMINUS_T3273980157_H
#define EXPRMINUS_T3273980157_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.ExprMINUS
struct  ExprMINUS_t3273980157  : public ExprNumeric_t503612810
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRMINUS_T3273980157_H
#ifndef SELFITERATOR_T1886393192_H
#define SELFITERATOR_T1886393192_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.SelfIterator
struct  SelfIterator_t1886393192  : public SimpleIterator_t833624542
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELFITERATOR_T1886393192_H
#ifndef FOLLOWINGSIBLINGITERATOR_T681640159_H
#define FOLLOWINGSIBLINGITERATOR_T681640159_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.FollowingSiblingIterator
struct  FollowingSiblingIterator_t681640159  : public SimpleIterator_t833624542
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FOLLOWINGSIBLINGITERATOR_T681640159_H
#ifndef PARENTITERATOR_T1638759120_H
#define PARENTITERATOR_T1638759120_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.ParentIterator
struct  ParentIterator_t1638759120  : public SimpleIterator_t833624542
{
public:
	// System.Boolean System.Xml.XPath.ParentIterator::canMove
	bool ___canMove_6;

public:
	inline static int32_t get_offset_of_canMove_6() { return static_cast<int32_t>(offsetof(ParentIterator_t1638759120, ___canMove_6)); }
	inline bool get_canMove_6() const { return ___canMove_6; }
	inline bool* get_address_of_canMove_6() { return &___canMove_6; }
	inline void set_canMove_6(bool value)
	{
		___canMove_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARENTITERATOR_T1638759120_H
#ifndef NODENAMETEST_T972157009_H
#define NODENAMETEST_T972157009_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.NodeNameTest
struct  NodeNameTest_t972157009  : public NodeTest_t2072055152
{
public:
	// System.Xml.XmlQualifiedName System.Xml.XPath.NodeNameTest::_name
	XmlQualifiedName_t1944712516 * ____name_1;
	// System.Boolean System.Xml.XPath.NodeNameTest::resolvedName
	bool ___resolvedName_2;

public:
	inline static int32_t get_offset_of__name_1() { return static_cast<int32_t>(offsetof(NodeNameTest_t972157009, ____name_1)); }
	inline XmlQualifiedName_t1944712516 * get__name_1() const { return ____name_1; }
	inline XmlQualifiedName_t1944712516 ** get_address_of__name_1() { return &____name_1; }
	inline void set__name_1(XmlQualifiedName_t1944712516 * value)
	{
		____name_1 = value;
		Il2CppCodeGenWriteBarrier((&____name_1), value);
	}

	inline static int32_t get_offset_of_resolvedName_2() { return static_cast<int32_t>(offsetof(NodeNameTest_t972157009, ___resolvedName_2)); }
	inline bool get_resolvedName_2() const { return ___resolvedName_2; }
	inline bool* get_address_of_resolvedName_2() { return &___resolvedName_2; }
	inline void set_resolvedName_2(bool value)
	{
		___resolvedName_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NODENAMETEST_T972157009_H
#ifndef EXPRDIV_T1827373722_H
#define EXPRDIV_T1827373722_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.ExprDIV
struct  ExprDIV_t1827373722  : public ExprNumeric_t503612810
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRDIV_T1827373722_H
#ifndef EXPRMOD_T3265849921_H
#define EXPRMOD_T3265849921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.ExprMOD
struct  ExprMOD_t3265849921  : public ExprNumeric_t503612810
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRMOD_T3265849921_H
#ifndef EXPRMULT_T1293289955_H
#define EXPRMULT_T1293289955_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.ExprMULT
struct  ExprMULT_t1293289955  : public ExprNumeric_t503612810
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRMULT_T1293289955_H
#ifndef NODETYPETEST_T383960256_H
#define NODETYPETEST_T383960256_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.NodeTypeTest
struct  NodeTypeTest_t383960256  : public NodeTest_t2072055152
{
public:
	// System.Xml.XPath.XPathNodeType System.Xml.XPath.NodeTypeTest::type
	int32_t ___type_1;
	// System.String System.Xml.XPath.NodeTypeTest::_param
	String_t* ____param_2;

public:
	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(NodeTypeTest_t383960256, ___type_1)); }
	inline int32_t get_type_1() const { return ___type_1; }
	inline int32_t* get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(int32_t value)
	{
		___type_1 = value;
	}

	inline static int32_t get_offset_of__param_2() { return static_cast<int32_t>(offsetof(NodeTypeTest_t383960256, ____param_2)); }
	inline String_t* get__param_2() const { return ____param_2; }
	inline String_t** get_address_of__param_2() { return &____param_2; }
	inline void set__param_2(String_t* value)
	{
		____param_2 = value;
		Il2CppCodeGenWriteBarrier((&____param_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NODETYPETEST_T383960256_H
#ifndef AXISSPECIFIER_T201930955_H
#define AXISSPECIFIER_T201930955_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.AxisSpecifier
struct  AxisSpecifier_t201930955  : public RuntimeObject
{
public:
	// System.Xml.XPath.Axes System.Xml.XPath.AxisSpecifier::_axis
	int32_t ____axis_0;

public:
	inline static int32_t get_offset_of__axis_0() { return static_cast<int32_t>(offsetof(AxisSpecifier_t201930955, ____axis_0)); }
	inline int32_t get__axis_0() const { return ____axis_0; }
	inline int32_t* get_address_of__axis_0() { return &____axis_0; }
	inline void set__axis_0(int32_t value)
	{
		____axis_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISSPECIFIER_T201930955_H
#ifndef PRECEDINGSIBLINGITERATOR_T3076405737_H
#define PRECEDINGSIBLINGITERATOR_T3076405737_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.PrecedingSiblingIterator
struct  PrecedingSiblingIterator_t3076405737  : public SimpleIterator_t833624542
{
public:
	// System.Boolean System.Xml.XPath.PrecedingSiblingIterator::finished
	bool ___finished_6;
	// System.Boolean System.Xml.XPath.PrecedingSiblingIterator::started
	bool ___started_7;
	// System.Xml.XPath.XPathNavigator System.Xml.XPath.PrecedingSiblingIterator::startPosition
	XPathNavigator_t3981235968 * ___startPosition_8;

public:
	inline static int32_t get_offset_of_finished_6() { return static_cast<int32_t>(offsetof(PrecedingSiblingIterator_t3076405737, ___finished_6)); }
	inline bool get_finished_6() const { return ___finished_6; }
	inline bool* get_address_of_finished_6() { return &___finished_6; }
	inline void set_finished_6(bool value)
	{
		___finished_6 = value;
	}

	inline static int32_t get_offset_of_started_7() { return static_cast<int32_t>(offsetof(PrecedingSiblingIterator_t3076405737, ___started_7)); }
	inline bool get_started_7() const { return ___started_7; }
	inline bool* get_address_of_started_7() { return &___started_7; }
	inline void set_started_7(bool value)
	{
		___started_7 = value;
	}

	inline static int32_t get_offset_of_startPosition_8() { return static_cast<int32_t>(offsetof(PrecedingSiblingIterator_t3076405737, ___startPosition_8)); }
	inline XPathNavigator_t3981235968 * get_startPosition_8() const { return ___startPosition_8; }
	inline XPathNavigator_t3981235968 ** get_address_of_startPosition_8() { return &___startPosition_8; }
	inline void set_startPosition_8(XPathNavigator_t3981235968 * value)
	{
		___startPosition_8 = value;
		Il2CppCodeGenWriteBarrier((&___startPosition_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRECEDINGSIBLINGITERATOR_T3076405737_H
#ifndef FOLLOWINGITERATOR_T2758624297_H
#define FOLLOWINGITERATOR_T2758624297_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.FollowingIterator
struct  FollowingIterator_t2758624297  : public SimpleIterator_t833624542
{
public:
	// System.Boolean System.Xml.XPath.FollowingIterator::_finished
	bool ____finished_6;

public:
	inline static int32_t get_offset_of__finished_6() { return static_cast<int32_t>(offsetof(FollowingIterator_t2758624297, ____finished_6)); }
	inline bool get__finished_6() const { return ____finished_6; }
	inline bool* get_address_of__finished_6() { return &____finished_6; }
	inline void set__finished_6(bool value)
	{
		____finished_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FOLLOWINGITERATOR_T2758624297_H
#ifndef DESCENDANTORSELFITERATOR_T3468500080_H
#define DESCENDANTORSELFITERATOR_T3468500080_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.DescendantOrSelfIterator
struct  DescendantOrSelfIterator_t3468500080  : public SimpleIterator_t833624542
{
public:
	// System.Int32 System.Xml.XPath.DescendantOrSelfIterator::_depth
	int32_t ____depth_6;
	// System.Boolean System.Xml.XPath.DescendantOrSelfIterator::_finished
	bool ____finished_7;

public:
	inline static int32_t get_offset_of__depth_6() { return static_cast<int32_t>(offsetof(DescendantOrSelfIterator_t3468500080, ____depth_6)); }
	inline int32_t get__depth_6() const { return ____depth_6; }
	inline int32_t* get_address_of__depth_6() { return &____depth_6; }
	inline void set__depth_6(int32_t value)
	{
		____depth_6 = value;
	}

	inline static int32_t get_offset_of__finished_7() { return static_cast<int32_t>(offsetof(DescendantOrSelfIterator_t3468500080, ____finished_7)); }
	inline bool get__finished_7() const { return ____finished_7; }
	inline bool* get_address_of__finished_7() { return &____finished_7; }
	inline void set__finished_7(bool value)
	{
		____finished_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESCENDANTORSELFITERATOR_T3468500080_H
#ifndef PREDICATEITERATOR_T2401536495_H
#define PREDICATEITERATOR_T2401536495_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.PredicateIterator
struct  PredicateIterator_t2401536495  : public BaseIterator_t2454437973
{
public:
	// System.Xml.XPath.BaseIterator System.Xml.XPath.PredicateIterator::_iter
	BaseIterator_t2454437973 * ____iter_3;
	// System.Xml.XPath.Expression System.Xml.XPath.PredicateIterator::_pred
	Expression_t1283317256 * ____pred_4;
	// System.Xml.XPath.XPathResultType System.Xml.XPath.PredicateIterator::resType
	int32_t ___resType_5;
	// System.Boolean System.Xml.XPath.PredicateIterator::finished
	bool ___finished_6;

public:
	inline static int32_t get_offset_of__iter_3() { return static_cast<int32_t>(offsetof(PredicateIterator_t2401536495, ____iter_3)); }
	inline BaseIterator_t2454437973 * get__iter_3() const { return ____iter_3; }
	inline BaseIterator_t2454437973 ** get_address_of__iter_3() { return &____iter_3; }
	inline void set__iter_3(BaseIterator_t2454437973 * value)
	{
		____iter_3 = value;
		Il2CppCodeGenWriteBarrier((&____iter_3), value);
	}

	inline static int32_t get_offset_of__pred_4() { return static_cast<int32_t>(offsetof(PredicateIterator_t2401536495, ____pred_4)); }
	inline Expression_t1283317256 * get__pred_4() const { return ____pred_4; }
	inline Expression_t1283317256 ** get_address_of__pred_4() { return &____pred_4; }
	inline void set__pred_4(Expression_t1283317256 * value)
	{
		____pred_4 = value;
		Il2CppCodeGenWriteBarrier((&____pred_4), value);
	}

	inline static int32_t get_offset_of_resType_5() { return static_cast<int32_t>(offsetof(PredicateIterator_t2401536495, ___resType_5)); }
	inline int32_t get_resType_5() const { return ___resType_5; }
	inline int32_t* get_address_of_resType_5() { return &___resType_5; }
	inline void set_resType_5(int32_t value)
	{
		___resType_5 = value;
	}

	inline static int32_t get_offset_of_finished_6() { return static_cast<int32_t>(offsetof(PredicateIterator_t2401536495, ___finished_6)); }
	inline bool get_finished_6() const { return ___finished_6; }
	inline bool* get_address_of_finished_6() { return &___finished_6; }
	inline void set_finished_6(bool value)
	{
		___finished_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREDICATEITERATOR_T2401536495_H
#ifndef ATTRIBUTEITERATOR_T2403056388_H
#define ATTRIBUTEITERATOR_T2403056388_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.AttributeIterator
struct  AttributeIterator_t2403056388  : public SimpleIterator_t833624542
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTEITERATOR_T2403056388_H
#ifndef NAMESPACEITERATOR_T3683603305_H
#define NAMESPACEITERATOR_T3683603305_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.NamespaceIterator
struct  NamespaceIterator_t3683603305  : public SimpleIterator_t833624542
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMESPACEITERATOR_T3683603305_H
#ifndef PRECEDINGITERATOR_T3277790919_H
#define PRECEDINGITERATOR_T3277790919_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.PrecedingIterator
struct  PrecedingIterator_t3277790919  : public SimpleIterator_t833624542
{
public:
	// System.Boolean System.Xml.XPath.PrecedingIterator::finished
	bool ___finished_6;
	// System.Boolean System.Xml.XPath.PrecedingIterator::started
	bool ___started_7;
	// System.Xml.XPath.XPathNavigator System.Xml.XPath.PrecedingIterator::startPosition
	XPathNavigator_t3981235968 * ___startPosition_8;

public:
	inline static int32_t get_offset_of_finished_6() { return static_cast<int32_t>(offsetof(PrecedingIterator_t3277790919, ___finished_6)); }
	inline bool get_finished_6() const { return ___finished_6; }
	inline bool* get_address_of_finished_6() { return &___finished_6; }
	inline void set_finished_6(bool value)
	{
		___finished_6 = value;
	}

	inline static int32_t get_offset_of_started_7() { return static_cast<int32_t>(offsetof(PrecedingIterator_t3277790919, ___started_7)); }
	inline bool get_started_7() const { return ___started_7; }
	inline bool* get_address_of_started_7() { return &___started_7; }
	inline void set_started_7(bool value)
	{
		___started_7 = value;
	}

	inline static int32_t get_offset_of_startPosition_8() { return static_cast<int32_t>(offsetof(PrecedingIterator_t3277790919, ___startPosition_8)); }
	inline XPathNavigator_t3981235968 * get_startPosition_8() const { return ___startPosition_8; }
	inline XPathNavigator_t3981235968 ** get_address_of_startPosition_8() { return &___startPosition_8; }
	inline void set_startPosition_8(XPathNavigator_t3981235968 * value)
	{
		___startPosition_8 = value;
		Il2CppCodeGenWriteBarrier((&___startPosition_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRECEDINGITERATOR_T3277790919_H
#ifndef ANCESTORITERATOR_T4023303433_H
#define ANCESTORITERATOR_T4023303433_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.AncestorIterator
struct  AncestorIterator_t4023303433  : public SimpleIterator_t833624542
{
public:
	// System.Int32 System.Xml.XPath.AncestorIterator::currentPosition
	int32_t ___currentPosition_6;
	// System.Collections.ArrayList System.Xml.XPath.AncestorIterator::navigators
	ArrayList_t4252133567 * ___navigators_7;
	// System.Xml.XPath.XPathNavigator System.Xml.XPath.AncestorIterator::startPosition
	XPathNavigator_t3981235968 * ___startPosition_8;

public:
	inline static int32_t get_offset_of_currentPosition_6() { return static_cast<int32_t>(offsetof(AncestorIterator_t4023303433, ___currentPosition_6)); }
	inline int32_t get_currentPosition_6() const { return ___currentPosition_6; }
	inline int32_t* get_address_of_currentPosition_6() { return &___currentPosition_6; }
	inline void set_currentPosition_6(int32_t value)
	{
		___currentPosition_6 = value;
	}

	inline static int32_t get_offset_of_navigators_7() { return static_cast<int32_t>(offsetof(AncestorIterator_t4023303433, ___navigators_7)); }
	inline ArrayList_t4252133567 * get_navigators_7() const { return ___navigators_7; }
	inline ArrayList_t4252133567 ** get_address_of_navigators_7() { return &___navigators_7; }
	inline void set_navigators_7(ArrayList_t4252133567 * value)
	{
		___navigators_7 = value;
		Il2CppCodeGenWriteBarrier((&___navigators_7), value);
	}

	inline static int32_t get_offset_of_startPosition_8() { return static_cast<int32_t>(offsetof(AncestorIterator_t4023303433, ___startPosition_8)); }
	inline XPathNavigator_t3981235968 * get_startPosition_8() const { return ___startPosition_8; }
	inline XPathNavigator_t3981235968 ** get_address_of_startPosition_8() { return &___startPosition_8; }
	inline void set_startPosition_8(XPathNavigator_t3981235968 * value)
	{
		___startPosition_8 = value;
		Il2CppCodeGenWriteBarrier((&___startPosition_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANCESTORITERATOR_T4023303433_H
#ifndef DESCENDANTITERATOR_T490680463_H
#define DESCENDANTITERATOR_T490680463_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.DescendantIterator
struct  DescendantIterator_t490680463  : public SimpleIterator_t833624542
{
public:
	// System.Int32 System.Xml.XPath.DescendantIterator::_depth
	int32_t ____depth_6;
	// System.Boolean System.Xml.XPath.DescendantIterator::_finished
	bool ____finished_7;

public:
	inline static int32_t get_offset_of__depth_6() { return static_cast<int32_t>(offsetof(DescendantIterator_t490680463, ____depth_6)); }
	inline int32_t get__depth_6() const { return ____depth_6; }
	inline int32_t* get_address_of__depth_6() { return &____depth_6; }
	inline void set__depth_6(int32_t value)
	{
		____depth_6 = value;
	}

	inline static int32_t get_offset_of__finished_7() { return static_cast<int32_t>(offsetof(DescendantIterator_t490680463, ____finished_7)); }
	inline bool get__finished_7() const { return ____finished_7; }
	inline bool* get_address_of__finished_7() { return &____finished_7; }
	inline void set__finished_7(bool value)
	{
		____finished_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESCENDANTITERATOR_T490680463_H
#ifndef ANCESTORORSELFITERATOR_T3792308554_H
#define ANCESTORORSELFITERATOR_T3792308554_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.AncestorOrSelfIterator
struct  AncestorOrSelfIterator_t3792308554  : public SimpleIterator_t833624542
{
public:
	// System.Int32 System.Xml.XPath.AncestorOrSelfIterator::currentPosition
	int32_t ___currentPosition_6;
	// System.Collections.ArrayList System.Xml.XPath.AncestorOrSelfIterator::navigators
	ArrayList_t4252133567 * ___navigators_7;
	// System.Xml.XPath.XPathNavigator System.Xml.XPath.AncestorOrSelfIterator::startPosition
	XPathNavigator_t3981235968 * ___startPosition_8;

public:
	inline static int32_t get_offset_of_currentPosition_6() { return static_cast<int32_t>(offsetof(AncestorOrSelfIterator_t3792308554, ___currentPosition_6)); }
	inline int32_t get_currentPosition_6() const { return ___currentPosition_6; }
	inline int32_t* get_address_of_currentPosition_6() { return &___currentPosition_6; }
	inline void set_currentPosition_6(int32_t value)
	{
		___currentPosition_6 = value;
	}

	inline static int32_t get_offset_of_navigators_7() { return static_cast<int32_t>(offsetof(AncestorOrSelfIterator_t3792308554, ___navigators_7)); }
	inline ArrayList_t4252133567 * get_navigators_7() const { return ___navigators_7; }
	inline ArrayList_t4252133567 ** get_address_of_navigators_7() { return &___navigators_7; }
	inline void set_navigators_7(ArrayList_t4252133567 * value)
	{
		___navigators_7 = value;
		Il2CppCodeGenWriteBarrier((&___navigators_7), value);
	}

	inline static int32_t get_offset_of_startPosition_8() { return static_cast<int32_t>(offsetof(AncestorOrSelfIterator_t3792308554, ___startPosition_8)); }
	inline XPathNavigator_t3981235968 * get_startPosition_8() const { return ___startPosition_8; }
	inline XPathNavigator_t3981235968 ** get_address_of_startPosition_8() { return &___startPosition_8; }
	inline void set_startPosition_8(XPathNavigator_t3981235968 * value)
	{
		___startPosition_8 = value;
		Il2CppCodeGenWriteBarrier((&___startPosition_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANCESTORORSELFITERATOR_T3792308554_H
#ifndef EXPRLE_T2307197528_H
#define EXPRLE_T2307197528_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.ExprLE
struct  ExprLE_t2307197528  : public RelationalExpr_t3066272106
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRLE_T2307197528_H
#ifndef EXPRNE_T1144398114_H
#define EXPRNE_T1144398114_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.ExprNE
struct  ExprNE_t1144398114  : public EqualityExpr_t2363998129
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRNE_T1144398114_H
#ifndef EXPREQ_T1097343967_H
#define EXPREQ_T1097343967_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.ExprEQ
struct  ExprEQ_t1097343967  : public EqualityExpr_t2363998129
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPREQ_T1097343967_H
#ifndef EXPRGE_T4229511829_H
#define EXPRGE_T4229511829_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.ExprGE
struct  ExprGE_t4229511829  : public RelationalExpr_t3066272106
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRGE_T4229511829_H
#ifndef EXPRLT_T2307197545_H
#define EXPRLT_T2307197545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.ExprLT
struct  ExprLT_t2307197545  : public RelationalExpr_t3066272106
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRLT_T2307197545_H
#ifndef NULLITERATOR_T2539636145_H
#define NULLITERATOR_T2539636145_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.NullIterator
struct  NullIterator_t2539636145  : public SelfIterator_t1886393192
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLITERATOR_T2539636145_H
#ifndef EXPRGT_T4229511846_H
#define EXPRGT_T4229511846_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.ExprGT
struct  ExprGT_t4229511846  : public RelationalExpr_t3066272106
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRGT_T4229511846_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1400 = { sizeof (XPathFunctionBoolean_t2454599187), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1400[1] = 
{
	XPathFunctionBoolean_t2454599187::get_offset_of_arg0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1401 = { sizeof (XPathFunctionNot_t1631674504), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1401[1] = 
{
	XPathFunctionNot_t1631674504::get_offset_of_arg0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1402 = { sizeof (XPathFunctionTrue_t1672815575), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1403 = { sizeof (XPathFunctionFalse_t2929064940), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1404 = { sizeof (XPathFunctionLang_t718178243), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1404[1] = 
{
	XPathFunctionLang_t718178243::get_offset_of_arg0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1405 = { sizeof (XPathNumericFunction_t2367269690), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1406 = { sizeof (XPathFunctionNumber_t1210619362), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1406[1] = 
{
	XPathFunctionNumber_t1210619362::get_offset_of_arg0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1407 = { sizeof (XPathFunctionSum_t2438242634), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1407[1] = 
{
	XPathFunctionSum_t2438242634::get_offset_of_arg0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1408 = { sizeof (XPathFunctionFloor_t2377969453), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1408[1] = 
{
	XPathFunctionFloor_t2377969453::get_offset_of_arg0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1409 = { sizeof (XPathFunctionCeil_t4011711386), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1409[1] = 
{
	XPathFunctionCeil_t4011711386::get_offset_of_arg0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1410 = { sizeof (XPathFunctionRound_t711107915), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1410[1] = 
{
	XPathFunctionRound_t711107915::get_offset_of_arg0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1411 = { sizeof (CompiledExpression_t3686330919), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1411[4] = 
{
	CompiledExpression_t3686330919::get_offset_of__nsm_0(),
	CompiledExpression_t3686330919::get_offset_of__expr_1(),
	CompiledExpression_t3686330919::get_offset_of__sorters_2(),
	CompiledExpression_t3686330919::get_offset_of_rawExpression_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1412 = { sizeof (XPathSortElement_t1040291071), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1412[2] = 
{
	XPathSortElement_t1040291071::get_offset_of_Navigator_0(),
	XPathSortElement_t1040291071::get_offset_of_Values_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1413 = { sizeof (XPathSorters_t4019574815), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1413[1] = 
{
	XPathSorters_t4019574815::get_offset_of__rgSorters_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1414 = { sizeof (XPathSorter_t3491953490), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1414[3] = 
{
	XPathSorter_t3491953490::get_offset_of__expr_0(),
	XPathSorter_t3491953490::get_offset_of__cmp_1(),
	XPathSorter_t3491953490::get_offset_of__type_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1415 = { sizeof (Expression_t1283317256), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1416 = { sizeof (ExprBinary_t2134514854), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1416[2] = 
{
	ExprBinary_t2134514854::get_offset_of__left_0(),
	ExprBinary_t2134514854::get_offset_of__right_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1417 = { sizeof (ExprBoolean_t2584142721), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1418 = { sizeof (ExprOR_t3873281492), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1419 = { sizeof (ExprAND_t3057430278), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1420 = { sizeof (EqualityExpr_t2363998129), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1420[1] = 
{
	EqualityExpr_t2363998129::get_offset_of_trueVal_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1421 = { sizeof (ExprEQ_t1097343967), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1422 = { sizeof (ExprNE_t1144398114), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1423 = { sizeof (RelationalExpr_t3066272106), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1424 = { sizeof (ExprGT_t4229511846), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1425 = { sizeof (ExprGE_t4229511829), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1426 = { sizeof (ExprLT_t2307197545), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1427 = { sizeof (ExprLE_t2307197528), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1428 = { sizeof (ExprNumeric_t503612810), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1429 = { sizeof (ExprPLUS_t627624341), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1430 = { sizeof (ExprMINUS_t3273980157), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1431 = { sizeof (ExprMULT_t1293289955), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1432 = { sizeof (ExprDIV_t1827373722), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1433 = { sizeof (ExprMOD_t3265849921), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1434 = { sizeof (ExprNEG_t1881334439), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1434[1] = 
{
	ExprNEG_t1881334439::get_offset_of__expr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1435 = { sizeof (NodeSet_t2895962396), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1436 = { sizeof (ExprUNION_t1011953538), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1436[2] = 
{
	ExprUNION_t1011953538::get_offset_of_left_0(),
	ExprUNION_t1011953538::get_offset_of_right_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1437 = { sizeof (ExprSLASH_t1432925936), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1437[2] = 
{
	ExprSLASH_t1432925936::get_offset_of_left_0(),
	ExprSLASH_t1432925936::get_offset_of_right_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1438 = { sizeof (ExprSLASH2_t1892232446), -1, sizeof(ExprSLASH2_t1892232446_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1438[3] = 
{
	ExprSLASH2_t1892232446::get_offset_of_left_0(),
	ExprSLASH2_t1892232446::get_offset_of_right_1(),
	ExprSLASH2_t1892232446_StaticFields::get_offset_of_DescendantOrSelfStar_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1439 = { sizeof (ExprRoot_t2449183281), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1440 = { sizeof (Axes_t2027048019)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1440[14] = 
{
	Axes_t2027048019::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1441 = { sizeof (AxisSpecifier_t201930955), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1441[1] = 
{
	AxisSpecifier_t201930955::get_offset_of__axis_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1442 = { sizeof (NodeTest_t2072055152), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1442[1] = 
{
	NodeTest_t2072055152::get_offset_of__axis_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1443 = { sizeof (NodeTypeTest_t383960256), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1443[2] = 
{
	NodeTypeTest_t383960256::get_offset_of_type_1(),
	NodeTypeTest_t383960256::get_offset_of__param_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1444 = { sizeof (NodeNameTest_t972157009), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1444[2] = 
{
	NodeNameTest_t972157009::get_offset_of__name_1(),
	NodeNameTest_t972157009::get_offset_of_resolvedName_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1445 = { sizeof (ExprFilter_t1897694423), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1445[2] = 
{
	ExprFilter_t1897694423::get_offset_of_expr_0(),
	ExprFilter_t1897694423::get_offset_of_pred_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1446 = { sizeof (ExprNumber_t2687440122), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1446[1] = 
{
	ExprNumber_t2687440122::get_offset_of__value_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1447 = { sizeof (ExprLiteral_t1691835634), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1447[1] = 
{
	ExprLiteral_t1691835634::get_offset_of__value_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1448 = { sizeof (ExprVariable_t3362236065), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1448[2] = 
{
	ExprVariable_t3362236065::get_offset_of__name_0(),
	ExprVariable_t3362236065::get_offset_of_resolvedName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1449 = { sizeof (ExprParens_t545584810), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1449[1] = 
{
	ExprParens_t545584810::get_offset_of__expr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1450 = { sizeof (FunctionArguments_t2900945452), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1450[2] = 
{
	FunctionArguments_t2900945452::get_offset_of__arg_0(),
	FunctionArguments_t2900945452::get_offset_of__tail_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1451 = { sizeof (ExprFunctionCall_t1376373777), -1, sizeof(ExprFunctionCall_t1376373777_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1451[4] = 
{
	ExprFunctionCall_t1376373777::get_offset_of__name_0(),
	ExprFunctionCall_t1376373777::get_offset_of_resolvedName_1(),
	ExprFunctionCall_t1376373777::get_offset_of__args_2(),
	ExprFunctionCall_t1376373777_StaticFields::get_offset_of_U3CU3Ef__switchU24map41_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1452 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1453 = { sizeof (BaseIterator_t2454437973), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1453[2] = 
{
	BaseIterator_t2454437973::get_offset_of__nsm_1(),
	BaseIterator_t2454437973::get_offset_of_position_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1454 = { sizeof (WrapperIterator_t2718786873), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1454[1] = 
{
	WrapperIterator_t2718786873::get_offset_of_iter_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1455 = { sizeof (SimpleIterator_t833624542), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1455[3] = 
{
	SimpleIterator_t833624542::get_offset_of__nav_3(),
	SimpleIterator_t833624542::get_offset_of__current_4(),
	SimpleIterator_t833624542::get_offset_of_skipfirst_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1456 = { sizeof (SelfIterator_t1886393192), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1457 = { sizeof (NullIterator_t2539636145), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1458 = { sizeof (ParensIterator_t1637647049), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1458[1] = 
{
	ParensIterator_t1637647049::get_offset_of__iter_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1459 = { sizeof (ParentIterator_t1638759120), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1459[1] = 
{
	ParentIterator_t1638759120::get_offset_of_canMove_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1460 = { sizeof (ChildIterator_t1144511930), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1460[1] = 
{
	ChildIterator_t1144511930::get_offset_of__nav_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1461 = { sizeof (FollowingSiblingIterator_t681640159), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1462 = { sizeof (PrecedingSiblingIterator_t3076405737), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1462[3] = 
{
	PrecedingSiblingIterator_t3076405737::get_offset_of_finished_6(),
	PrecedingSiblingIterator_t3076405737::get_offset_of_started_7(),
	PrecedingSiblingIterator_t3076405737::get_offset_of_startPosition_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1463 = { sizeof (AncestorIterator_t4023303433), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1463[3] = 
{
	AncestorIterator_t4023303433::get_offset_of_currentPosition_6(),
	AncestorIterator_t4023303433::get_offset_of_navigators_7(),
	AncestorIterator_t4023303433::get_offset_of_startPosition_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1464 = { sizeof (AncestorOrSelfIterator_t3792308554), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1464[3] = 
{
	AncestorOrSelfIterator_t3792308554::get_offset_of_currentPosition_6(),
	AncestorOrSelfIterator_t3792308554::get_offset_of_navigators_7(),
	AncestorOrSelfIterator_t3792308554::get_offset_of_startPosition_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1465 = { sizeof (DescendantIterator_t490680463), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1465[2] = 
{
	DescendantIterator_t490680463::get_offset_of__depth_6(),
	DescendantIterator_t490680463::get_offset_of__finished_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1466 = { sizeof (DescendantOrSelfIterator_t3468500080), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1466[2] = 
{
	DescendantOrSelfIterator_t3468500080::get_offset_of__depth_6(),
	DescendantOrSelfIterator_t3468500080::get_offset_of__finished_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1467 = { sizeof (FollowingIterator_t2758624297), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1467[1] = 
{
	FollowingIterator_t2758624297::get_offset_of__finished_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1468 = { sizeof (PrecedingIterator_t3277790919), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1468[3] = 
{
	PrecedingIterator_t3277790919::get_offset_of_finished_6(),
	PrecedingIterator_t3277790919::get_offset_of_started_7(),
	PrecedingIterator_t3277790919::get_offset_of_startPosition_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1469 = { sizeof (NamespaceIterator_t3683603305), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1470 = { sizeof (AttributeIterator_t2403056388), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1471 = { sizeof (AxisIterator_t3253336785), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1471[2] = 
{
	AxisIterator_t3253336785::get_offset_of__iter_3(),
	AxisIterator_t3253336785::get_offset_of__test_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1472 = { sizeof (SimpleSlashIterator_t400959233), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1472[4] = 
{
	SimpleSlashIterator_t400959233::get_offset_of__expr_3(),
	SimpleSlashIterator_t400959233::get_offset_of__left_4(),
	SimpleSlashIterator_t400959233::get_offset_of__right_5(),
	SimpleSlashIterator_t400959233::get_offset_of__current_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1473 = { sizeof (SortedIterator_t3199707209), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1473[1] = 
{
	SortedIterator_t3199707209::get_offset_of_list_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1474 = { sizeof (SlashIterator_t3247429967), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1474[6] = 
{
	SlashIterator_t3247429967::get_offset_of__iterLeft_3(),
	SlashIterator_t3247429967::get_offset_of__iterRight_4(),
	SlashIterator_t3247429967::get_offset_of__expr_5(),
	SlashIterator_t3247429967::get_offset_of__iterList_6(),
	SlashIterator_t3247429967::get_offset_of__finished_7(),
	SlashIterator_t3247429967::get_offset_of__nextIterRight_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1475 = { sizeof (PredicateIterator_t2401536495), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1475[4] = 
{
	PredicateIterator_t2401536495::get_offset_of__iter_3(),
	PredicateIterator_t2401536495::get_offset_of__pred_4(),
	PredicateIterator_t2401536495::get_offset_of_resType_5(),
	PredicateIterator_t2401536495::get_offset_of_finished_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1476 = { sizeof (ListIterator_t3804705054), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1476[1] = 
{
	ListIterator_t3804705054::get_offset_of__list_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1477 = { sizeof (UnionIterator_t284038993), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1477[5] = 
{
	UnionIterator_t284038993::get_offset_of__left_3(),
	UnionIterator_t284038993::get_offset_of__right_4(),
	UnionIterator_t284038993::get_offset_of_keepLeft_5(),
	UnionIterator_t284038993::get_offset_of_keepRight_6(),
	UnionIterator_t284038993::get_offset_of__current_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1478 = { sizeof (Tokenizer_t2998805977), -1, sizeof(Tokenizer_t2998805977_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1478[10] = 
{
	Tokenizer_t2998805977::get_offset_of_m_rgchInput_0(),
	Tokenizer_t2998805977::get_offset_of_m_ich_1(),
	Tokenizer_t2998805977::get_offset_of_m_cch_2(),
	Tokenizer_t2998805977::get_offset_of_m_iToken_3(),
	Tokenizer_t2998805977::get_offset_of_m_iTokenPrev_4(),
	Tokenizer_t2998805977::get_offset_of_m_objToken_5(),
	Tokenizer_t2998805977::get_offset_of_m_fPrevWasOperator_6(),
	Tokenizer_t2998805977::get_offset_of_m_fThisIsOperator_7(),
	Tokenizer_t2998805977_StaticFields::get_offset_of_s_mapTokens_8(),
	Tokenizer_t2998805977_StaticFields::get_offset_of_s_rgTokenMap_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1479 = { sizeof (XPathIteratorComparer_t2522471076), -1, sizeof(XPathIteratorComparer_t2522471076_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1479[1] = 
{
	XPathIteratorComparer_t2522471076_StaticFields::get_offset_of_Instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1480 = { sizeof (XPathNavigatorComparer_t2670709129), -1, sizeof(XPathNavigatorComparer_t2670709129_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1480[1] = 
{
	XPathNavigatorComparer_t2670709129_StaticFields::get_offset_of_Instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1481 = { sizeof (XPathException_t1503722168), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1482 = { sizeof (XPathExpression_t452251917), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1483 = { sizeof (XPathItem_t3130801258), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1484 = { sizeof (XPathNamespaceScope_t3601604274)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1484[4] = 
{
	XPathNamespaceScope_t3601604274::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1485 = { sizeof (XPathNavigator_t3981235968), -1, sizeof(XPathNavigator_t3981235968_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1485[2] = 
{
	XPathNavigator_t3981235968_StaticFields::get_offset_of_escape_text_chars_0(),
	XPathNavigator_t3981235968_StaticFields::get_offset_of_escape_attr_chars_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1486 = { sizeof (EnumerableIterator_t1602910416), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1486[3] = 
{
	EnumerableIterator_t1602910416::get_offset_of_source_1(),
	EnumerableIterator_t1602910416::get_offset_of_e_2(),
	EnumerableIterator_t1602910416::get_offset_of_pos_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1487 = { sizeof (U3CEnumerateChildrenU3Ec__Iterator0_t1235609798), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1487[8] = 
{
	U3CEnumerateChildrenU3Ec__Iterator0_t1235609798::get_offset_of_n_0(),
	U3CEnumerateChildrenU3Ec__Iterator0_t1235609798::get_offset_of_U3CnavU3E__0_1(),
	U3CEnumerateChildrenU3Ec__Iterator0_t1235609798::get_offset_of_U3Cnav2U3E__1_2(),
	U3CEnumerateChildrenU3Ec__Iterator0_t1235609798::get_offset_of_type_3(),
	U3CEnumerateChildrenU3Ec__Iterator0_t1235609798::get_offset_of_U24PC_4(),
	U3CEnumerateChildrenU3Ec__Iterator0_t1235609798::get_offset_of_U24current_5(),
	U3CEnumerateChildrenU3Ec__Iterator0_t1235609798::get_offset_of_U3CU24U3En_6(),
	U3CEnumerateChildrenU3Ec__Iterator0_t1235609798::get_offset_of_U3CU24U3Etype_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1488 = { sizeof (XPathNodeIterator_t3192332357), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1488[1] = 
{
	XPathNodeIterator_t3192332357::get_offset_of__count_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1489 = { sizeof (U3CGetEnumeratorU3Ec__Iterator2_t959253998), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1489[3] = 
{
	U3CGetEnumeratorU3Ec__Iterator2_t959253998::get_offset_of_U24PC_0(),
	U3CGetEnumeratorU3Ec__Iterator2_t959253998::get_offset_of_U24current_1(),
	U3CGetEnumeratorU3Ec__Iterator2_t959253998::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1490 = { sizeof (XPathNodeType_t817388867)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1490[11] = 
{
	XPathNodeType_t817388867::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1491 = { sizeof (XPathResultType_t1521569578)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1491[8] = 
{
	XPathResultType_t1521569578::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1492 = { sizeof (XmlDataType_t315095065)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1492[3] = 
{
	XmlDataType_t315095065::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1493 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1494 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1495 = { sizeof (XsltContext_t2013960098), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1496 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1497 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1498 = { sizeof (MonoFIXAttribute_t4096033756), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1498[1] = 
{
	MonoFIXAttribute_t4096033756::get_offset_of_comment_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1499 = { sizeof (NameTable_t594386929), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1499[3] = 
{
	NameTable_t594386929::get_offset_of_count_0(),
	NameTable_t594386929::get_offset_of_buckets_1(),
	NameTable_t594386929::get_offset_of_size_2(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
