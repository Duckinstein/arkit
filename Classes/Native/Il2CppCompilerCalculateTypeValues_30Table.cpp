﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// IBM.Watson.DeveloperCloud.Widgets.Widget
struct Widget_t3624771850;
// System.Type
struct Type_t;
// System.Collections.Generic.List`1<IBM.Watson.DeveloperCloud.Widgets.Widget/Output/Connection>
struct List_1_t1855455449;
// IBM.Watson.DeveloperCloud.Widgets.Widget/Output
struct Output_t220081548;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.String
struct String_t;
// IBM.Watson.DeveloperCloud.Widgets.Widget/Input
struct Input_t3157785889;
// System.Collections.Generic.List`1<IBM.Watson.DeveloperCloud.Widgets.Widget/Output>
struct List_1_t3884169976;
// IBM.Watson.DeveloperCloud.Widgets.Widget/OnReceiveData
struct OnReceiveData_t2274732329;
// System.Collections.Generic.Queue`1<UnityEngine.Object>
struct Queue_1_t841258952;
// FullSerializer.fsSerializer
struct fsSerializer_t4193731081;
// System.Collections.Generic.Dictionary`2<System.Type,System.Object>
struct Dictionary_2_t331839896;
// System.Single[]
struct SingleU5BU5D_t577127397;
// IBM.Watson.DeveloperCloud.Widgets.MicrophoneWidget
struct MicrophoneWidget_t2442369682;
// IBM.Watson.DeveloperCloud.Utilities.SerializedDelegate
struct SerializedDelegate_t808690627;
// IBM.Watson.DeveloperCloud.Widgets.LanguageTranslatorWidget
struct LanguageTranslatorWidget_t907487206;
// System.IntPtr[]
struct IntPtrU5BU5D_t169632028;
// System.Collections.IDictionary
struct IDictionary_t596158605;
// UnityEngine.AudioClip
struct AudioClip_t1932558630;
// FullSerializer.Internal.DirectConverters.AnimationCurve_DirectConverter
struct AnimationCurve_DirectConverter_t1633993451;
// FullSerializer.Internal.DirectConverters.Bounds_DirectConverter
struct Bounds_DirectConverter_t3918181867;
// FullSerializer.Internal.DirectConverters.Gradient_DirectConverter
struct Gradient_DirectConverter_t4192375398;
// FullSerializer.Internal.DirectConverters.Keyframe_DirectConverter
struct Keyframe_DirectConverter_t1121933266;
// FullSerializer.Internal.DirectConverters.LayerMask_DirectConverter
struct LayerMask_DirectConverter_t2775871809;
// FullSerializer.Internal.DirectConverters.Rect_DirectConverter
struct Rect_DirectConverter_t2065695396;
// System.Collections.Generic.List`1<System.Type>
struct List_1_t672924358;
// UnityEngine.Transform
struct Transform_t3275118058;
// IBM.Watson.DeveloperCloud.DataTypes.WebCamTextureData
struct WebCamTextureData_t1034677858;
// IBM.Watson.DeveloperCloud.Widgets.WebCamWidget
struct WebCamWidget_t58556181;
// System.Collections.Generic.Dictionary`2<System.Type,System.String>
struct Dictionary_2_t3966578130;
// System.Collections.Generic.List`1<FullSerializer.fsAotCompilationManager/AotCompilation>
struct List_1_t3685996670;
// UnityEngine.Collider
struct Collider_t3497673348;
// UnityEngine.Collider2D
struct Collider2D_t646061738;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UnityEngine.Collider[]
struct ColliderU5BU5D_t462843629;
// UnityEngine.Collider2D[]
struct Collider2DU5BU5D_t3535523695;
// UnityEngine.RectTransform[]
struct RectTransformU5BU5D_t3948421699;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Func`2<FullSerializer.fsDataType,System.String>
struct Func_2_t1180654441;
// IBM.Watson.DeveloperCloud.Utilities.TouchEventManager/<UnregisterDoubleTapEvent>c__AnonStoreyA
struct U3CUnregisterDoubleTapEventU3Ec__AnonStoreyA_t137061456;
// IBM.Watson.DeveloperCloud.Utilities.TouchEventManager/<UnregisterTapEvent>c__AnonStorey6
struct U3CUnregisterTapEventU3Ec__AnonStorey6_t3827711990;
// IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.ClassifyResult
struct ClassifyResult_t2166103447;
// FullSerializer.Internal.fsMetaProperty[]
struct fsMetaPropertyU5BU5D_t4057973332;
// UnityEngine.WebCamTexture
struct WebCamTexture_t1079476942;
// System.Void
struct Void_t1841601450;
// System.Char[]
struct CharU5BU5D_t1328083999;
// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechRecognitionEvent
struct SpeechRecognitionEvent_t1591882439;
// UnityEngine.EventSystems.BaseRaycaster
struct BaseRaycaster_t2336171397;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1572802995;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.Type[]
struct TypeU5BU5D_t1664964607;
// IBM.Watson.DeveloperCloud.Widgets.Widget/Data
struct Data_t3166909639;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// System.Collections.Generic.Dictionary`2<System.Int32,IBM.Watson.DeveloperCloud.Utilities.Runnable/Routine>
struct Dictionary_2_t980883800;
// UnityEngine.UI.Graphic
struct Graphic_t2426225576;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;
// UnityEngine.Camera
struct Camera_t189460977;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<IBM.Watson.DeveloperCloud.Utilities.TouchEventManager/TouchEventData>>
struct Dictionary_2_t449014016;
// TouchScript.Gestures.TapGesture
struct TapGesture_t556401502;
// TouchScript.Gestures.ScreenTransformGesture
struct ScreenTransformGesture_t3088562929;
// TouchScript.Gestures.PressGesture
struct PressGesture_t582183752;
// TouchScript.Gestures.ReleaseGesture
struct ReleaseGesture_t248506278;
// TouchScript.Gestures.LongPressGesture
struct LongPressGesture_t656184630;
// IBM.Watson.DeveloperCloud.Widgets.Widget/Input[]
struct InputU5BU5D_t1078881596;
// IBM.Watson.DeveloperCloud.Widgets.Widget/Output[]
struct OutputU5BU5D_t475762373;
// UnityEngine.Transform[]
struct TransformU5BU5D_t3764228911;
// VikingCrewTools.SpeechbubbleManager
struct SpeechbubbleManager_t3871578363;
// IBM.Watson.DeveloperCloud.Services.Conversation.v1.Conversation
struct Conversation_t105466997;
// IBM.Watson.DeveloperCloud.Services.Conversation.v1.Context
struct Context_t2592795451;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// UnityEngine.Video.VideoPlayer
struct VideoPlayer_t10059812;
// Boo.Lang.List`1<System.String>
struct List_1_t3696025851;
// Boo.Lang.List`1<System.Single>
struct List_1_t3743315550;
// Boo.Lang.List`1<System.Int32>
struct List_1_t3738683066;
// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText
struct SpeechToText_t2713896346;
// UnityEngine.UI.Text
struct Text_t356221433;
// IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.NaturalLanguageClassifier
struct NaturalLanguageClassifier_t2492034444;
// IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.Classifier
struct Classifier_t1414011573;
// System.Collections.Generic.List`1<IBM.Watson.DeveloperCloud.Widgets.NaturalLanguageClassifierWidget/ClassEventMapping>
struct List_1_t2963572661;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;
// UnityEngine.UI.InputField
struct InputField_t1631627530;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech
struct TextToSpeech_t3349357562;
// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;
// System.Collections.Generic.Queue`1<IBM.Watson.DeveloperCloud.Widgets.TextToSpeechWidget/Speech>
struct Queue_1_t1482798040;
// IBM.Watson.DeveloperCloud.Widgets.TextToSpeechWidget/Speech
struct Speech_t1663141205;
// UnityEngine.UI.RawImage
struct RawImage_t2749640213;
// UnityEngine.Material
struct Material_t193706927;
// System.Collections.Generic.List`1<IBM.Watson.DeveloperCloud.Widgets.TouchWidget/TapEventMapping>
struct List_1_t1677282647;
// System.Collections.Generic.List`1<IBM.Watson.DeveloperCloud.Widgets.TouchWidget/FullScreenDragEventMapping>
struct List_1_t3241548797;
// IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.LanguageTranslation
struct LanguageTranslation_t3099415401;
// UnityEngine.UI.Dropdown
struct Dropdown_t1985816271;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.String>>
struct Dictionary_2_t3313120627;
// System.Collections.Generic.List`1<IBM.Watson.DeveloperCloud.Widgets.EventWidget/Mapping>
struct List_1_t812061395;
// System.Collections.Generic.List`1<IBM.Watson.DeveloperCloud.Widgets.KeyboardWidget/Mapping>
struct List_1_t2493597736;
// System.Collections.Generic.List`1<UnityEngine.AudioClip>
struct List_1_t1301679762;
// System.Collections.Generic.List`1<IBM.Watson.DeveloperCloud.Widgets.ClassifierWidget/Mapping>
struct List_1_t1971267372;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef OUTPUT_T220081548_H
#define OUTPUT_T220081548_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Widgets.Widget/Output
struct  Output_t220081548  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Widgets.Widget IBM.Watson.DeveloperCloud.Widgets.Widget/Output::<Owner>k__BackingField
	Widget_t3624771850 * ___U3COwnerU3Ek__BackingField_0;
	// System.Type IBM.Watson.DeveloperCloud.Widgets.Widget/Output::<DataType>k__BackingField
	Type_t * ___U3CDataTypeU3Ek__BackingField_1;
	// System.Boolean IBM.Watson.DeveloperCloud.Widgets.Widget/Output::<AllowMany>k__BackingField
	bool ___U3CAllowManyU3Ek__BackingField_2;
	// System.Collections.Generic.List`1<IBM.Watson.DeveloperCloud.Widgets.Widget/Output/Connection> IBM.Watson.DeveloperCloud.Widgets.Widget/Output::m_Connections
	List_1_t1855455449 * ___m_Connections_3;

public:
	inline static int32_t get_offset_of_U3COwnerU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Output_t220081548, ___U3COwnerU3Ek__BackingField_0)); }
	inline Widget_t3624771850 * get_U3COwnerU3Ek__BackingField_0() const { return ___U3COwnerU3Ek__BackingField_0; }
	inline Widget_t3624771850 ** get_address_of_U3COwnerU3Ek__BackingField_0() { return &___U3COwnerU3Ek__BackingField_0; }
	inline void set_U3COwnerU3Ek__BackingField_0(Widget_t3624771850 * value)
	{
		___U3COwnerU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3COwnerU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CDataTypeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Output_t220081548, ___U3CDataTypeU3Ek__BackingField_1)); }
	inline Type_t * get_U3CDataTypeU3Ek__BackingField_1() const { return ___U3CDataTypeU3Ek__BackingField_1; }
	inline Type_t ** get_address_of_U3CDataTypeU3Ek__BackingField_1() { return &___U3CDataTypeU3Ek__BackingField_1; }
	inline void set_U3CDataTypeU3Ek__BackingField_1(Type_t * value)
	{
		___U3CDataTypeU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataTypeU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CAllowManyU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Output_t220081548, ___U3CAllowManyU3Ek__BackingField_2)); }
	inline bool get_U3CAllowManyU3Ek__BackingField_2() const { return ___U3CAllowManyU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CAllowManyU3Ek__BackingField_2() { return &___U3CAllowManyU3Ek__BackingField_2; }
	inline void set_U3CAllowManyU3Ek__BackingField_2(bool value)
	{
		___U3CAllowManyU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_m_Connections_3() { return static_cast<int32_t>(offsetof(Output_t220081548, ___m_Connections_3)); }
	inline List_1_t1855455449 * get_m_Connections_3() const { return ___m_Connections_3; }
	inline List_1_t1855455449 ** get_address_of_m_Connections_3() { return &___m_Connections_3; }
	inline void set_m_Connections_3(List_1_t1855455449 * value)
	{
		___m_Connections_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Connections_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OUTPUT_T220081548_H
#ifndef CONNECTION_T2486334317_H
#define CONNECTION_T2486334317_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Widgets.Widget/Output/Connection
struct  Connection_t2486334317  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Widgets.Widget/Output IBM.Watson.DeveloperCloud.Widgets.Widget/Output/Connection::m_Owner
	Output_t220081548 * ___m_Owner_0;
	// UnityEngine.GameObject IBM.Watson.DeveloperCloud.Widgets.Widget/Output/Connection::m_TargetObject
	GameObject_t1756533147 * ___m_TargetObject_1;
	// System.String IBM.Watson.DeveloperCloud.Widgets.Widget/Output/Connection::m_TargetConnection
	String_t* ___m_TargetConnection_2;
	// System.Boolean IBM.Watson.DeveloperCloud.Widgets.Widget/Output/Connection::m_TargetInputResolved
	bool ___m_TargetInputResolved_3;
	// IBM.Watson.DeveloperCloud.Widgets.Widget/Input IBM.Watson.DeveloperCloud.Widgets.Widget/Output/Connection::m_TargetInput
	Input_t3157785889 * ___m_TargetInput_4;

public:
	inline static int32_t get_offset_of_m_Owner_0() { return static_cast<int32_t>(offsetof(Connection_t2486334317, ___m_Owner_0)); }
	inline Output_t220081548 * get_m_Owner_0() const { return ___m_Owner_0; }
	inline Output_t220081548 ** get_address_of_m_Owner_0() { return &___m_Owner_0; }
	inline void set_m_Owner_0(Output_t220081548 * value)
	{
		___m_Owner_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Owner_0), value);
	}

	inline static int32_t get_offset_of_m_TargetObject_1() { return static_cast<int32_t>(offsetof(Connection_t2486334317, ___m_TargetObject_1)); }
	inline GameObject_t1756533147 * get_m_TargetObject_1() const { return ___m_TargetObject_1; }
	inline GameObject_t1756533147 ** get_address_of_m_TargetObject_1() { return &___m_TargetObject_1; }
	inline void set_m_TargetObject_1(GameObject_t1756533147 * value)
	{
		___m_TargetObject_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_TargetObject_1), value);
	}

	inline static int32_t get_offset_of_m_TargetConnection_2() { return static_cast<int32_t>(offsetof(Connection_t2486334317, ___m_TargetConnection_2)); }
	inline String_t* get_m_TargetConnection_2() const { return ___m_TargetConnection_2; }
	inline String_t** get_address_of_m_TargetConnection_2() { return &___m_TargetConnection_2; }
	inline void set_m_TargetConnection_2(String_t* value)
	{
		___m_TargetConnection_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TargetConnection_2), value);
	}

	inline static int32_t get_offset_of_m_TargetInputResolved_3() { return static_cast<int32_t>(offsetof(Connection_t2486334317, ___m_TargetInputResolved_3)); }
	inline bool get_m_TargetInputResolved_3() const { return ___m_TargetInputResolved_3; }
	inline bool* get_address_of_m_TargetInputResolved_3() { return &___m_TargetInputResolved_3; }
	inline void set_m_TargetInputResolved_3(bool value)
	{
		___m_TargetInputResolved_3 = value;
	}

	inline static int32_t get_offset_of_m_TargetInput_4() { return static_cast<int32_t>(offsetof(Connection_t2486334317, ___m_TargetInput_4)); }
	inline Input_t3157785889 * get_m_TargetInput_4() const { return ___m_TargetInput_4; }
	inline Input_t3157785889 ** get_address_of_m_TargetInput_4() { return &___m_TargetInput_4; }
	inline void set_m_TargetInput_4(Input_t3157785889 * value)
	{
		___m_TargetInput_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_TargetInput_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTION_T2486334317_H
#ifndef DATA_T3166909639_H
#define DATA_T3166909639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Widgets.Widget/Data
struct  Data_t3166909639  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATA_T3166909639_H
#ifndef INPUT_T3157785889_H
#define INPUT_T3157785889_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Widgets.Widget/Input
struct  Input_t3157785889  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<IBM.Watson.DeveloperCloud.Widgets.Widget/Output> IBM.Watson.DeveloperCloud.Widgets.Widget/Input::m_Connections
	List_1_t3884169976 * ___m_Connections_0;
	// IBM.Watson.DeveloperCloud.Widgets.Widget IBM.Watson.DeveloperCloud.Widgets.Widget/Input::<Owner>k__BackingField
	Widget_t3624771850 * ___U3COwnerU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Widgets.Widget/Input::<InputName>k__BackingField
	String_t* ___U3CInputNameU3Ek__BackingField_2;
	// System.Type IBM.Watson.DeveloperCloud.Widgets.Widget/Input::<DataType>k__BackingField
	Type_t * ___U3CDataTypeU3Ek__BackingField_3;
	// System.Boolean IBM.Watson.DeveloperCloud.Widgets.Widget/Input::<AllowMany>k__BackingField
	bool ___U3CAllowManyU3Ek__BackingField_4;
	// System.String IBM.Watson.DeveloperCloud.Widgets.Widget/Input::<ReceiverFunction>k__BackingField
	String_t* ___U3CReceiverFunctionU3Ek__BackingField_5;
	// IBM.Watson.DeveloperCloud.Widgets.Widget/OnReceiveData IBM.Watson.DeveloperCloud.Widgets.Widget/Input::<DataReceiver>k__BackingField
	OnReceiveData_t2274732329 * ___U3CDataReceiverU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_m_Connections_0() { return static_cast<int32_t>(offsetof(Input_t3157785889, ___m_Connections_0)); }
	inline List_1_t3884169976 * get_m_Connections_0() const { return ___m_Connections_0; }
	inline List_1_t3884169976 ** get_address_of_m_Connections_0() { return &___m_Connections_0; }
	inline void set_m_Connections_0(List_1_t3884169976 * value)
	{
		___m_Connections_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Connections_0), value);
	}

	inline static int32_t get_offset_of_U3COwnerU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Input_t3157785889, ___U3COwnerU3Ek__BackingField_1)); }
	inline Widget_t3624771850 * get_U3COwnerU3Ek__BackingField_1() const { return ___U3COwnerU3Ek__BackingField_1; }
	inline Widget_t3624771850 ** get_address_of_U3COwnerU3Ek__BackingField_1() { return &___U3COwnerU3Ek__BackingField_1; }
	inline void set_U3COwnerU3Ek__BackingField_1(Widget_t3624771850 * value)
	{
		___U3COwnerU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3COwnerU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CInputNameU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Input_t3157785889, ___U3CInputNameU3Ek__BackingField_2)); }
	inline String_t* get_U3CInputNameU3Ek__BackingField_2() const { return ___U3CInputNameU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CInputNameU3Ek__BackingField_2() { return &___U3CInputNameU3Ek__BackingField_2; }
	inline void set_U3CInputNameU3Ek__BackingField_2(String_t* value)
	{
		___U3CInputNameU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInputNameU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CDataTypeU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Input_t3157785889, ___U3CDataTypeU3Ek__BackingField_3)); }
	inline Type_t * get_U3CDataTypeU3Ek__BackingField_3() const { return ___U3CDataTypeU3Ek__BackingField_3; }
	inline Type_t ** get_address_of_U3CDataTypeU3Ek__BackingField_3() { return &___U3CDataTypeU3Ek__BackingField_3; }
	inline void set_U3CDataTypeU3Ek__BackingField_3(Type_t * value)
	{
		___U3CDataTypeU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataTypeU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CAllowManyU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Input_t3157785889, ___U3CAllowManyU3Ek__BackingField_4)); }
	inline bool get_U3CAllowManyU3Ek__BackingField_4() const { return ___U3CAllowManyU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CAllowManyU3Ek__BackingField_4() { return &___U3CAllowManyU3Ek__BackingField_4; }
	inline void set_U3CAllowManyU3Ek__BackingField_4(bool value)
	{
		___U3CAllowManyU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CReceiverFunctionU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Input_t3157785889, ___U3CReceiverFunctionU3Ek__BackingField_5)); }
	inline String_t* get_U3CReceiverFunctionU3Ek__BackingField_5() const { return ___U3CReceiverFunctionU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CReceiverFunctionU3Ek__BackingField_5() { return &___U3CReceiverFunctionU3Ek__BackingField_5; }
	inline void set_U3CReceiverFunctionU3Ek__BackingField_5(String_t* value)
	{
		___U3CReceiverFunctionU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CReceiverFunctionU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CDataReceiverU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Input_t3157785889, ___U3CDataReceiverU3Ek__BackingField_6)); }
	inline OnReceiveData_t2274732329 * get_U3CDataReceiverU3Ek__BackingField_6() const { return ___U3CDataReceiverU3Ek__BackingField_6; }
	inline OnReceiveData_t2274732329 ** get_address_of_U3CDataReceiverU3Ek__BackingField_6() { return &___U3CDataReceiverU3Ek__BackingField_6; }
	inline void set_U3CDataReceiverU3Ek__BackingField_6(OnReceiveData_t2274732329 * value)
	{
		___U3CDataReceiverU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataReceiverU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUT_T3157785889_H
#ifndef U3CPROCESSDESTROYQUEUEU3EC__ITERATOR0_T2014034615_H
#define U3CPROCESSDESTROYQUEUEU3EC__ITERATOR0_T2014034615_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Utilities.UnityObjectUtil/<ProcessDestroyQueue>c__Iterator0
struct  U3CProcessDestroyQueueU3Ec__Iterator0_t2014034615  : public RuntimeObject
{
public:
	// System.Object IBM.Watson.DeveloperCloud.Utilities.UnityObjectUtil/<ProcessDestroyQueue>c__Iterator0::$locvar0
	RuntimeObject * ___U24locvar0_0;
	// System.Object IBM.Watson.DeveloperCloud.Utilities.UnityObjectUtil/<ProcessDestroyQueue>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean IBM.Watson.DeveloperCloud.Utilities.UnityObjectUtil/<ProcessDestroyQueue>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 IBM.Watson.DeveloperCloud.Utilities.UnityObjectUtil/<ProcessDestroyQueue>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24locvar0_0() { return static_cast<int32_t>(offsetof(U3CProcessDestroyQueueU3Ec__Iterator0_t2014034615, ___U24locvar0_0)); }
	inline RuntimeObject * get_U24locvar0_0() const { return ___U24locvar0_0; }
	inline RuntimeObject ** get_address_of_U24locvar0_0() { return &___U24locvar0_0; }
	inline void set_U24locvar0_0(RuntimeObject * value)
	{
		___U24locvar0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CProcessDestroyQueueU3Ec__Iterator0_t2014034615, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CProcessDestroyQueueU3Ec__Iterator0_t2014034615, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CProcessDestroyQueueU3Ec__Iterator0_t2014034615, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPROCESSDESTROYQUEUEU3EC__ITERATOR0_T2014034615_H
#ifndef UNITYOBJECTUTIL_T4155662584_H
#define UNITYOBJECTUTIL_T4155662584_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Utilities.UnityObjectUtil
struct  UnityObjectUtil_t4155662584  : public RuntimeObject
{
public:

public:
};

struct UnityObjectUtil_t4155662584_StaticFields
{
public:
	// System.Collections.Generic.Queue`1<UnityEngine.Object> IBM.Watson.DeveloperCloud.Utilities.UnityObjectUtil::sm_DestroyQueue
	Queue_1_t841258952 * ___sm_DestroyQueue_0;
	// System.Int32 IBM.Watson.DeveloperCloud.Utilities.UnityObjectUtil::sm_DestroyQueueID
	int32_t ___sm_DestroyQueueID_1;

public:
	inline static int32_t get_offset_of_sm_DestroyQueue_0() { return static_cast<int32_t>(offsetof(UnityObjectUtil_t4155662584_StaticFields, ___sm_DestroyQueue_0)); }
	inline Queue_1_t841258952 * get_sm_DestroyQueue_0() const { return ___sm_DestroyQueue_0; }
	inline Queue_1_t841258952 ** get_address_of_sm_DestroyQueue_0() { return &___sm_DestroyQueue_0; }
	inline void set_sm_DestroyQueue_0(Queue_1_t841258952 * value)
	{
		___sm_DestroyQueue_0 = value;
		Il2CppCodeGenWriteBarrier((&___sm_DestroyQueue_0), value);
	}

	inline static int32_t get_offset_of_sm_DestroyQueueID_1() { return static_cast<int32_t>(offsetof(UnityObjectUtil_t4155662584_StaticFields, ___sm_DestroyQueueID_1)); }
	inline int32_t get_sm_DestroyQueueID_1() const { return ___sm_DestroyQueueID_1; }
	inline int32_t* get_address_of_sm_DestroyQueueID_1() { return &___sm_DestroyQueueID_1; }
	inline void set_sm_DestroyQueueID_1(int32_t value)
	{
		___sm_DestroyQueueID_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYOBJECTUTIL_T4155662584_H
#ifndef UTILITY_T666154278_H
#define UTILITY_T666154278_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Utilities.Utility
struct  Utility_t666154278  : public RuntimeObject
{
public:

public:
};

struct Utility_t666154278_StaticFields
{
public:
	// FullSerializer.fsSerializer IBM.Watson.DeveloperCloud.Utilities.Utility::sm_Serializer
	fsSerializer_t4193731081 * ___sm_Serializer_0;
	// System.String IBM.Watson.DeveloperCloud.Utilities.Utility::sm_MacAddress
	String_t* ___sm_MacAddress_1;

public:
	inline static int32_t get_offset_of_sm_Serializer_0() { return static_cast<int32_t>(offsetof(Utility_t666154278_StaticFields, ___sm_Serializer_0)); }
	inline fsSerializer_t4193731081 * get_sm_Serializer_0() const { return ___sm_Serializer_0; }
	inline fsSerializer_t4193731081 ** get_address_of_sm_Serializer_0() { return &___sm_Serializer_0; }
	inline void set_sm_Serializer_0(fsSerializer_t4193731081 * value)
	{
		___sm_Serializer_0 = value;
		Il2CppCodeGenWriteBarrier((&___sm_Serializer_0), value);
	}

	inline static int32_t get_offset_of_sm_MacAddress_1() { return static_cast<int32_t>(offsetof(Utility_t666154278_StaticFields, ___sm_MacAddress_1)); }
	inline String_t* get_sm_MacAddress_1() const { return ___sm_MacAddress_1; }
	inline String_t** get_address_of_sm_MacAddress_1() { return &___sm_MacAddress_1; }
	inline void set_sm_MacAddress_1(String_t* value)
	{
		___sm_MacAddress_1 = value;
		Il2CppCodeGenWriteBarrier((&___sm_MacAddress_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UTILITY_T666154278_H
#ifndef WAVEFILE_T3116037077_H
#define WAVEFILE_T3116037077_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Utilities.WaveFile
struct  WaveFile_t3116037077  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAVEFILE_T3116037077_H
#ifndef FSCONTEXT_T2896355488_H
#define FSCONTEXT_T2896355488_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsContext
struct  fsContext_t2896355488  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,System.Object> FullSerializer.fsContext::_contextObjects
	Dictionary_2_t331839896 * ____contextObjects_0;

public:
	inline static int32_t get_offset_of__contextObjects_0() { return static_cast<int32_t>(offsetof(fsContext_t2896355488, ____contextObjects_0)); }
	inline Dictionary_2_t331839896 * get__contextObjects_0() const { return ____contextObjects_0; }
	inline Dictionary_2_t331839896 ** get_address_of__contextObjects_0() { return &____contextObjects_0; }
	inline void set__contextObjects_0(Dictionary_2_t331839896 * value)
	{
		____contextObjects_0 = value;
		Il2CppCodeGenWriteBarrier((&____contextObjects_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSCONTEXT_T2896355488_H
#ifndef ATTRIBUTE_T542643598_H
#define ATTRIBUTE_T542643598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t542643598  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T542643598_H
#ifndef CLASSEVENTMAPPING_T3594451529_H
#define CLASSEVENTMAPPING_T3594451529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Widgets.NaturalLanguageClassifierWidget/ClassEventMapping
struct  ClassEventMapping_t3594451529  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Widgets.NaturalLanguageClassifierWidget/ClassEventMapping::m_Class
	String_t* ___m_Class_0;
	// System.String IBM.Watson.DeveloperCloud.Widgets.NaturalLanguageClassifierWidget/ClassEventMapping::m_Event
	String_t* ___m_Event_1;

public:
	inline static int32_t get_offset_of_m_Class_0() { return static_cast<int32_t>(offsetof(ClassEventMapping_t3594451529, ___m_Class_0)); }
	inline String_t* get_m_Class_0() const { return ___m_Class_0; }
	inline String_t** get_address_of_m_Class_0() { return &___m_Class_0; }
	inline void set_m_Class_0(String_t* value)
	{
		___m_Class_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Class_0), value);
	}

	inline static int32_t get_offset_of_m_Event_1() { return static_cast<int32_t>(offsetof(ClassEventMapping_t3594451529, ___m_Event_1)); }
	inline String_t* get_m_Event_1() const { return ___m_Event_1; }
	inline String_t** get_address_of_m_Event_1() { return &___m_Event_1; }
	inline void set_m_Event_1(String_t* value)
	{
		___m_Event_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Event_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLASSEVENTMAPPING_T3594451529_H
#ifndef U3CRECORDINGHANDLERU3EC__ITERATOR0_T3999079579_H
#define U3CRECORDINGHANDLERU3EC__ITERATOR0_T3999079579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Widgets.MicrophoneWidget/<RecordingHandler>c__Iterator0
struct  U3CRecordingHandlerU3Ec__Iterator0_t3999079579  : public RuntimeObject
{
public:
	// System.Boolean IBM.Watson.DeveloperCloud.Widgets.MicrophoneWidget/<RecordingHandler>c__Iterator0::<bFirstBlock>__0
	bool ___U3CbFirstBlockU3E__0_0;
	// System.Int32 IBM.Watson.DeveloperCloud.Widgets.MicrophoneWidget/<RecordingHandler>c__Iterator0::<midPoint>__0
	int32_t ___U3CmidPointU3E__0_1;
	// System.Boolean IBM.Watson.DeveloperCloud.Widgets.MicrophoneWidget/<RecordingHandler>c__Iterator0::<bOutputLevelData>__0
	bool ___U3CbOutputLevelDataU3E__0_2;
	// System.Boolean IBM.Watson.DeveloperCloud.Widgets.MicrophoneWidget/<RecordingHandler>c__Iterator0::<bOutputAudio>__0
	bool ___U3CbOutputAudioU3E__0_3;
	// System.Int32 IBM.Watson.DeveloperCloud.Widgets.MicrophoneWidget/<RecordingHandler>c__Iterator0::<lastReadPos>__0
	int32_t ___U3ClastReadPosU3E__0_4;
	// System.Single[] IBM.Watson.DeveloperCloud.Widgets.MicrophoneWidget/<RecordingHandler>c__Iterator0::<samples>__0
	SingleU5BU5D_t577127397* ___U3CsamplesU3E__0_5;
	// System.Int32 IBM.Watson.DeveloperCloud.Widgets.MicrophoneWidget/<RecordingHandler>c__Iterator0::<writePos>__1
	int32_t ___U3CwritePosU3E__1_6;
	// System.Int32 IBM.Watson.DeveloperCloud.Widgets.MicrophoneWidget/<RecordingHandler>c__Iterator0::<remaining>__2
	int32_t ___U3CremainingU3E__2_7;
	// System.Single IBM.Watson.DeveloperCloud.Widgets.MicrophoneWidget/<RecordingHandler>c__Iterator0::<timeRemaining>__2
	float ___U3CtimeRemainingU3E__2_8;
	// IBM.Watson.DeveloperCloud.Widgets.MicrophoneWidget IBM.Watson.DeveloperCloud.Widgets.MicrophoneWidget/<RecordingHandler>c__Iterator0::$this
	MicrophoneWidget_t2442369682 * ___U24this_9;
	// System.Object IBM.Watson.DeveloperCloud.Widgets.MicrophoneWidget/<RecordingHandler>c__Iterator0::$current
	RuntimeObject * ___U24current_10;
	// System.Boolean IBM.Watson.DeveloperCloud.Widgets.MicrophoneWidget/<RecordingHandler>c__Iterator0::$disposing
	bool ___U24disposing_11;
	// System.Int32 IBM.Watson.DeveloperCloud.Widgets.MicrophoneWidget/<RecordingHandler>c__Iterator0::$PC
	int32_t ___U24PC_12;

public:
	inline static int32_t get_offset_of_U3CbFirstBlockU3E__0_0() { return static_cast<int32_t>(offsetof(U3CRecordingHandlerU3Ec__Iterator0_t3999079579, ___U3CbFirstBlockU3E__0_0)); }
	inline bool get_U3CbFirstBlockU3E__0_0() const { return ___U3CbFirstBlockU3E__0_0; }
	inline bool* get_address_of_U3CbFirstBlockU3E__0_0() { return &___U3CbFirstBlockU3E__0_0; }
	inline void set_U3CbFirstBlockU3E__0_0(bool value)
	{
		___U3CbFirstBlockU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CmidPointU3E__0_1() { return static_cast<int32_t>(offsetof(U3CRecordingHandlerU3Ec__Iterator0_t3999079579, ___U3CmidPointU3E__0_1)); }
	inline int32_t get_U3CmidPointU3E__0_1() const { return ___U3CmidPointU3E__0_1; }
	inline int32_t* get_address_of_U3CmidPointU3E__0_1() { return &___U3CmidPointU3E__0_1; }
	inline void set_U3CmidPointU3E__0_1(int32_t value)
	{
		___U3CmidPointU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CbOutputLevelDataU3E__0_2() { return static_cast<int32_t>(offsetof(U3CRecordingHandlerU3Ec__Iterator0_t3999079579, ___U3CbOutputLevelDataU3E__0_2)); }
	inline bool get_U3CbOutputLevelDataU3E__0_2() const { return ___U3CbOutputLevelDataU3E__0_2; }
	inline bool* get_address_of_U3CbOutputLevelDataU3E__0_2() { return &___U3CbOutputLevelDataU3E__0_2; }
	inline void set_U3CbOutputLevelDataU3E__0_2(bool value)
	{
		___U3CbOutputLevelDataU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3CbOutputAudioU3E__0_3() { return static_cast<int32_t>(offsetof(U3CRecordingHandlerU3Ec__Iterator0_t3999079579, ___U3CbOutputAudioU3E__0_3)); }
	inline bool get_U3CbOutputAudioU3E__0_3() const { return ___U3CbOutputAudioU3E__0_3; }
	inline bool* get_address_of_U3CbOutputAudioU3E__0_3() { return &___U3CbOutputAudioU3E__0_3; }
	inline void set_U3CbOutputAudioU3E__0_3(bool value)
	{
		___U3CbOutputAudioU3E__0_3 = value;
	}

	inline static int32_t get_offset_of_U3ClastReadPosU3E__0_4() { return static_cast<int32_t>(offsetof(U3CRecordingHandlerU3Ec__Iterator0_t3999079579, ___U3ClastReadPosU3E__0_4)); }
	inline int32_t get_U3ClastReadPosU3E__0_4() const { return ___U3ClastReadPosU3E__0_4; }
	inline int32_t* get_address_of_U3ClastReadPosU3E__0_4() { return &___U3ClastReadPosU3E__0_4; }
	inline void set_U3ClastReadPosU3E__0_4(int32_t value)
	{
		___U3ClastReadPosU3E__0_4 = value;
	}

	inline static int32_t get_offset_of_U3CsamplesU3E__0_5() { return static_cast<int32_t>(offsetof(U3CRecordingHandlerU3Ec__Iterator0_t3999079579, ___U3CsamplesU3E__0_5)); }
	inline SingleU5BU5D_t577127397* get_U3CsamplesU3E__0_5() const { return ___U3CsamplesU3E__0_5; }
	inline SingleU5BU5D_t577127397** get_address_of_U3CsamplesU3E__0_5() { return &___U3CsamplesU3E__0_5; }
	inline void set_U3CsamplesU3E__0_5(SingleU5BU5D_t577127397* value)
	{
		___U3CsamplesU3E__0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsamplesU3E__0_5), value);
	}

	inline static int32_t get_offset_of_U3CwritePosU3E__1_6() { return static_cast<int32_t>(offsetof(U3CRecordingHandlerU3Ec__Iterator0_t3999079579, ___U3CwritePosU3E__1_6)); }
	inline int32_t get_U3CwritePosU3E__1_6() const { return ___U3CwritePosU3E__1_6; }
	inline int32_t* get_address_of_U3CwritePosU3E__1_6() { return &___U3CwritePosU3E__1_6; }
	inline void set_U3CwritePosU3E__1_6(int32_t value)
	{
		___U3CwritePosU3E__1_6 = value;
	}

	inline static int32_t get_offset_of_U3CremainingU3E__2_7() { return static_cast<int32_t>(offsetof(U3CRecordingHandlerU3Ec__Iterator0_t3999079579, ___U3CremainingU3E__2_7)); }
	inline int32_t get_U3CremainingU3E__2_7() const { return ___U3CremainingU3E__2_7; }
	inline int32_t* get_address_of_U3CremainingU3E__2_7() { return &___U3CremainingU3E__2_7; }
	inline void set_U3CremainingU3E__2_7(int32_t value)
	{
		___U3CremainingU3E__2_7 = value;
	}

	inline static int32_t get_offset_of_U3CtimeRemainingU3E__2_8() { return static_cast<int32_t>(offsetof(U3CRecordingHandlerU3Ec__Iterator0_t3999079579, ___U3CtimeRemainingU3E__2_8)); }
	inline float get_U3CtimeRemainingU3E__2_8() const { return ___U3CtimeRemainingU3E__2_8; }
	inline float* get_address_of_U3CtimeRemainingU3E__2_8() { return &___U3CtimeRemainingU3E__2_8; }
	inline void set_U3CtimeRemainingU3E__2_8(float value)
	{
		___U3CtimeRemainingU3E__2_8 = value;
	}

	inline static int32_t get_offset_of_U24this_9() { return static_cast<int32_t>(offsetof(U3CRecordingHandlerU3Ec__Iterator0_t3999079579, ___U24this_9)); }
	inline MicrophoneWidget_t2442369682 * get_U24this_9() const { return ___U24this_9; }
	inline MicrophoneWidget_t2442369682 ** get_address_of_U24this_9() { return &___U24this_9; }
	inline void set_U24this_9(MicrophoneWidget_t2442369682 * value)
	{
		___U24this_9 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_9), value);
	}

	inline static int32_t get_offset_of_U24current_10() { return static_cast<int32_t>(offsetof(U3CRecordingHandlerU3Ec__Iterator0_t3999079579, ___U24current_10)); }
	inline RuntimeObject * get_U24current_10() const { return ___U24current_10; }
	inline RuntimeObject ** get_address_of_U24current_10() { return &___U24current_10; }
	inline void set_U24current_10(RuntimeObject * value)
	{
		___U24current_10 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_10), value);
	}

	inline static int32_t get_offset_of_U24disposing_11() { return static_cast<int32_t>(offsetof(U3CRecordingHandlerU3Ec__Iterator0_t3999079579, ___U24disposing_11)); }
	inline bool get_U24disposing_11() const { return ___U24disposing_11; }
	inline bool* get_address_of_U24disposing_11() { return &___U24disposing_11; }
	inline void set_U24disposing_11(bool value)
	{
		___U24disposing_11 = value;
	}

	inline static int32_t get_offset_of_U24PC_12() { return static_cast<int32_t>(offsetof(U3CRecordingHandlerU3Ec__Iterator0_t3999079579, ___U24PC_12)); }
	inline int32_t get_U24PC_12() const { return ___U24PC_12; }
	inline int32_t* get_address_of_U24PC_12() { return &___U24PC_12; }
	inline void set_U24PC_12(int32_t value)
	{
		___U24PC_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRECORDINGHANDLERU3EC__ITERATOR0_T3999079579_H
#ifndef MAPPING_T1442940263_H
#define MAPPING_T1442940263_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Widgets.EventWidget/Mapping
struct  Mapping_t1442940263  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Widgets.EventWidget/Mapping::m_Event
	String_t* ___m_Event_0;
	// IBM.Watson.DeveloperCloud.Utilities.SerializedDelegate IBM.Watson.DeveloperCloud.Widgets.EventWidget/Mapping::m_Callback
	SerializedDelegate_t808690627 * ___m_Callback_1;

public:
	inline static int32_t get_offset_of_m_Event_0() { return static_cast<int32_t>(offsetof(Mapping_t1442940263, ___m_Event_0)); }
	inline String_t* get_m_Event_0() const { return ___m_Event_0; }
	inline String_t** get_address_of_m_Event_0() { return &___m_Event_0; }
	inline void set_m_Event_0(String_t* value)
	{
		___m_Event_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Event_0), value);
	}

	inline static int32_t get_offset_of_m_Callback_1() { return static_cast<int32_t>(offsetof(Mapping_t1442940263, ___m_Callback_1)); }
	inline SerializedDelegate_t808690627 * get_m_Callback_1() const { return ___m_Callback_1; }
	inline SerializedDelegate_t808690627 ** get_address_of_m_Callback_1() { return &___m_Callback_1; }
	inline void set_m_Callback_1(SerializedDelegate_t808690627 * value)
	{
		___m_Callback_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Callback_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAPPING_T1442940263_H
#ifndef TRANSLATEREQUEST_T2111065942_H
#define TRANSLATEREQUEST_T2111065942_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Widgets.LanguageTranslatorWidget/TranslateRequest
struct  TranslateRequest_t2111065942  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Widgets.LanguageTranslatorWidget IBM.Watson.DeveloperCloud.Widgets.LanguageTranslatorWidget/TranslateRequest::m_Widget
	LanguageTranslatorWidget_t907487206 * ___m_Widget_0;
	// System.String IBM.Watson.DeveloperCloud.Widgets.LanguageTranslatorWidget/TranslateRequest::m_Text
	String_t* ___m_Text_1;

public:
	inline static int32_t get_offset_of_m_Widget_0() { return static_cast<int32_t>(offsetof(TranslateRequest_t2111065942, ___m_Widget_0)); }
	inline LanguageTranslatorWidget_t907487206 * get_m_Widget_0() const { return ___m_Widget_0; }
	inline LanguageTranslatorWidget_t907487206 ** get_address_of_m_Widget_0() { return &___m_Widget_0; }
	inline void set_m_Widget_0(LanguageTranslatorWidget_t907487206 * value)
	{
		___m_Widget_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Widget_0), value);
	}

	inline static int32_t get_offset_of_m_Text_1() { return static_cast<int32_t>(offsetof(TranslateRequest_t2111065942, ___m_Text_1)); }
	inline String_t* get_m_Text_1() const { return ___m_Text_1; }
	inline String_t** get_address_of_m_Text_1() { return &___m_Text_1; }
	inline void set_m_Text_1(String_t* value)
	{
		___m_Text_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Text_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSLATEREQUEST_T2111065942_H
#ifndef EXCEPTION_T1927440687_H
#define EXCEPTION_T1927440687_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t1927440687  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t169632028* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t1927440687 * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t169632028* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t169632028** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t169632028* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___inner_exception_1)); }
	inline Exception_t1927440687 * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t1927440687 ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t1927440687 * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T1927440687_H
#ifndef MAPPING_T2602146240_H
#define MAPPING_T2602146240_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Widgets.ClassifierWidget/Mapping
struct  Mapping_t2602146240  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Widgets.ClassifierWidget/Mapping::m_Class
	String_t* ___m_Class_0;
	// IBM.Watson.DeveloperCloud.Utilities.SerializedDelegate IBM.Watson.DeveloperCloud.Widgets.ClassifierWidget/Mapping::m_Callback
	SerializedDelegate_t808690627 * ___m_Callback_1;
	// System.Boolean IBM.Watson.DeveloperCloud.Widgets.ClassifierWidget/Mapping::m_Exclusive
	bool ___m_Exclusive_2;

public:
	inline static int32_t get_offset_of_m_Class_0() { return static_cast<int32_t>(offsetof(Mapping_t2602146240, ___m_Class_0)); }
	inline String_t* get_m_Class_0() const { return ___m_Class_0; }
	inline String_t** get_address_of_m_Class_0() { return &___m_Class_0; }
	inline void set_m_Class_0(String_t* value)
	{
		___m_Class_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Class_0), value);
	}

	inline static int32_t get_offset_of_m_Callback_1() { return static_cast<int32_t>(offsetof(Mapping_t2602146240, ___m_Callback_1)); }
	inline SerializedDelegate_t808690627 * get_m_Callback_1() const { return ___m_Callback_1; }
	inline SerializedDelegate_t808690627 ** get_address_of_m_Callback_1() { return &___m_Callback_1; }
	inline void set_m_Callback_1(SerializedDelegate_t808690627 * value)
	{
		___m_Callback_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Callback_1), value);
	}

	inline static int32_t get_offset_of_m_Exclusive_2() { return static_cast<int32_t>(offsetof(Mapping_t2602146240, ___m_Exclusive_2)); }
	inline bool get_m_Exclusive_2() const { return ___m_Exclusive_2; }
	inline bool* get_address_of_m_Exclusive_2() { return &___m_Exclusive_2; }
	inline void set_m_Exclusive_2(bool value)
	{
		___m_Exclusive_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAPPING_T2602146240_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef SPEECH_T1663141205_H
#define SPEECH_T1663141205_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Widgets.TextToSpeechWidget/Speech
struct  Speech_t1663141205  : public RuntimeObject
{
public:
	// System.Boolean IBM.Watson.DeveloperCloud.Widgets.TextToSpeechWidget/Speech::<Ready>k__BackingField
	bool ___U3CReadyU3Ek__BackingField_0;
	// UnityEngine.AudioClip IBM.Watson.DeveloperCloud.Widgets.TextToSpeechWidget/Speech::<Clip>k__BackingField
	AudioClip_t1932558630 * ___U3CClipU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CReadyU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Speech_t1663141205, ___U3CReadyU3Ek__BackingField_0)); }
	inline bool get_U3CReadyU3Ek__BackingField_0() const { return ___U3CReadyU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CReadyU3Ek__BackingField_0() { return &___U3CReadyU3Ek__BackingField_0; }
	inline void set_U3CReadyU3Ek__BackingField_0(bool value)
	{
		___U3CReadyU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CClipU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Speech_t1663141205, ___U3CClipU3Ek__BackingField_1)); }
	inline AudioClip_t1932558630 * get_U3CClipU3Ek__BackingField_1() const { return ___U3CClipU3Ek__BackingField_1; }
	inline AudioClip_t1932558630 ** get_address_of_U3CClipU3Ek__BackingField_1() { return &___U3CClipU3Ek__BackingField_1; }
	inline void set_U3CClipU3Ek__BackingField_1(AudioClip_t1932558630 * value)
	{
		___U3CClipU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CClipU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPEECH_T1663141205_H
#ifndef FULLSCREENDRAGEVENTMAPPING_T3872427665_H
#define FULLSCREENDRAGEVENTMAPPING_T3872427665_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Widgets.TouchWidget/FullScreenDragEventMapping
struct  FullScreenDragEventMapping_t3872427665  : public RuntimeObject
{
public:
	// UnityEngine.GameObject IBM.Watson.DeveloperCloud.Widgets.TouchWidget/FullScreenDragEventMapping::m_DragLayerObject
	GameObject_t1756533147 * ___m_DragLayerObject_0;
	// System.Int32 IBM.Watson.DeveloperCloud.Widgets.TouchWidget/FullScreenDragEventMapping::m_NumberOfFinger
	int32_t ___m_NumberOfFinger_1;
	// System.Int32 IBM.Watson.DeveloperCloud.Widgets.TouchWidget/FullScreenDragEventMapping::m_SortingLayer
	int32_t ___m_SortingLayer_2;
	// System.Boolean IBM.Watson.DeveloperCloud.Widgets.TouchWidget/FullScreenDragEventMapping::m_IsDragInside
	bool ___m_IsDragInside_3;
	// System.String IBM.Watson.DeveloperCloud.Widgets.TouchWidget/FullScreenDragEventMapping::m_Callback
	String_t* ___m_Callback_4;

public:
	inline static int32_t get_offset_of_m_DragLayerObject_0() { return static_cast<int32_t>(offsetof(FullScreenDragEventMapping_t3872427665, ___m_DragLayerObject_0)); }
	inline GameObject_t1756533147 * get_m_DragLayerObject_0() const { return ___m_DragLayerObject_0; }
	inline GameObject_t1756533147 ** get_address_of_m_DragLayerObject_0() { return &___m_DragLayerObject_0; }
	inline void set_m_DragLayerObject_0(GameObject_t1756533147 * value)
	{
		___m_DragLayerObject_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_DragLayerObject_0), value);
	}

	inline static int32_t get_offset_of_m_NumberOfFinger_1() { return static_cast<int32_t>(offsetof(FullScreenDragEventMapping_t3872427665, ___m_NumberOfFinger_1)); }
	inline int32_t get_m_NumberOfFinger_1() const { return ___m_NumberOfFinger_1; }
	inline int32_t* get_address_of_m_NumberOfFinger_1() { return &___m_NumberOfFinger_1; }
	inline void set_m_NumberOfFinger_1(int32_t value)
	{
		___m_NumberOfFinger_1 = value;
	}

	inline static int32_t get_offset_of_m_SortingLayer_2() { return static_cast<int32_t>(offsetof(FullScreenDragEventMapping_t3872427665, ___m_SortingLayer_2)); }
	inline int32_t get_m_SortingLayer_2() const { return ___m_SortingLayer_2; }
	inline int32_t* get_address_of_m_SortingLayer_2() { return &___m_SortingLayer_2; }
	inline void set_m_SortingLayer_2(int32_t value)
	{
		___m_SortingLayer_2 = value;
	}

	inline static int32_t get_offset_of_m_IsDragInside_3() { return static_cast<int32_t>(offsetof(FullScreenDragEventMapping_t3872427665, ___m_IsDragInside_3)); }
	inline bool get_m_IsDragInside_3() const { return ___m_IsDragInside_3; }
	inline bool* get_address_of_m_IsDragInside_3() { return &___m_IsDragInside_3; }
	inline void set_m_IsDragInside_3(bool value)
	{
		___m_IsDragInside_3 = value;
	}

	inline static int32_t get_offset_of_m_Callback_4() { return static_cast<int32_t>(offsetof(FullScreenDragEventMapping_t3872427665, ___m_Callback_4)); }
	inline String_t* get_m_Callback_4() const { return ___m_Callback_4; }
	inline String_t** get_address_of_m_Callback_4() { return &___m_Callback_4; }
	inline void set_m_Callback_4(String_t* value)
	{
		___m_Callback_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Callback_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FULLSCREENDRAGEVENTMAPPING_T3872427665_H
#ifndef FSCONVERTERREGISTRAR_T866553964_H
#define FSCONVERTERREGISTRAR_T866553964_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsConverterRegistrar
struct  fsConverterRegistrar_t866553964  : public RuntimeObject
{
public:

public:
};

struct fsConverterRegistrar_t866553964_StaticFields
{
public:
	// FullSerializer.Internal.DirectConverters.AnimationCurve_DirectConverter FullSerializer.fsConverterRegistrar::Register_AnimationCurve_DirectConverter
	AnimationCurve_DirectConverter_t1633993451 * ___Register_AnimationCurve_DirectConverter_0;
	// FullSerializer.Internal.DirectConverters.Bounds_DirectConverter FullSerializer.fsConverterRegistrar::Register_Bounds_DirectConverter
	Bounds_DirectConverter_t3918181867 * ___Register_Bounds_DirectConverter_1;
	// FullSerializer.Internal.DirectConverters.Gradient_DirectConverter FullSerializer.fsConverterRegistrar::Register_Gradient_DirectConverter
	Gradient_DirectConverter_t4192375398 * ___Register_Gradient_DirectConverter_2;
	// FullSerializer.Internal.DirectConverters.Keyframe_DirectConverter FullSerializer.fsConverterRegistrar::Register_Keyframe_DirectConverter
	Keyframe_DirectConverter_t1121933266 * ___Register_Keyframe_DirectConverter_3;
	// FullSerializer.Internal.DirectConverters.LayerMask_DirectConverter FullSerializer.fsConverterRegistrar::Register_LayerMask_DirectConverter
	LayerMask_DirectConverter_t2775871809 * ___Register_LayerMask_DirectConverter_4;
	// FullSerializer.Internal.DirectConverters.Rect_DirectConverter FullSerializer.fsConverterRegistrar::Register_Rect_DirectConverter
	Rect_DirectConverter_t2065695396 * ___Register_Rect_DirectConverter_5;
	// System.Collections.Generic.List`1<System.Type> FullSerializer.fsConverterRegistrar::Converters
	List_1_t672924358 * ___Converters_6;

public:
	inline static int32_t get_offset_of_Register_AnimationCurve_DirectConverter_0() { return static_cast<int32_t>(offsetof(fsConverterRegistrar_t866553964_StaticFields, ___Register_AnimationCurve_DirectConverter_0)); }
	inline AnimationCurve_DirectConverter_t1633993451 * get_Register_AnimationCurve_DirectConverter_0() const { return ___Register_AnimationCurve_DirectConverter_0; }
	inline AnimationCurve_DirectConverter_t1633993451 ** get_address_of_Register_AnimationCurve_DirectConverter_0() { return &___Register_AnimationCurve_DirectConverter_0; }
	inline void set_Register_AnimationCurve_DirectConverter_0(AnimationCurve_DirectConverter_t1633993451 * value)
	{
		___Register_AnimationCurve_DirectConverter_0 = value;
		Il2CppCodeGenWriteBarrier((&___Register_AnimationCurve_DirectConverter_0), value);
	}

	inline static int32_t get_offset_of_Register_Bounds_DirectConverter_1() { return static_cast<int32_t>(offsetof(fsConverterRegistrar_t866553964_StaticFields, ___Register_Bounds_DirectConverter_1)); }
	inline Bounds_DirectConverter_t3918181867 * get_Register_Bounds_DirectConverter_1() const { return ___Register_Bounds_DirectConverter_1; }
	inline Bounds_DirectConverter_t3918181867 ** get_address_of_Register_Bounds_DirectConverter_1() { return &___Register_Bounds_DirectConverter_1; }
	inline void set_Register_Bounds_DirectConverter_1(Bounds_DirectConverter_t3918181867 * value)
	{
		___Register_Bounds_DirectConverter_1 = value;
		Il2CppCodeGenWriteBarrier((&___Register_Bounds_DirectConverter_1), value);
	}

	inline static int32_t get_offset_of_Register_Gradient_DirectConverter_2() { return static_cast<int32_t>(offsetof(fsConverterRegistrar_t866553964_StaticFields, ___Register_Gradient_DirectConverter_2)); }
	inline Gradient_DirectConverter_t4192375398 * get_Register_Gradient_DirectConverter_2() const { return ___Register_Gradient_DirectConverter_2; }
	inline Gradient_DirectConverter_t4192375398 ** get_address_of_Register_Gradient_DirectConverter_2() { return &___Register_Gradient_DirectConverter_2; }
	inline void set_Register_Gradient_DirectConverter_2(Gradient_DirectConverter_t4192375398 * value)
	{
		___Register_Gradient_DirectConverter_2 = value;
		Il2CppCodeGenWriteBarrier((&___Register_Gradient_DirectConverter_2), value);
	}

	inline static int32_t get_offset_of_Register_Keyframe_DirectConverter_3() { return static_cast<int32_t>(offsetof(fsConverterRegistrar_t866553964_StaticFields, ___Register_Keyframe_DirectConverter_3)); }
	inline Keyframe_DirectConverter_t1121933266 * get_Register_Keyframe_DirectConverter_3() const { return ___Register_Keyframe_DirectConverter_3; }
	inline Keyframe_DirectConverter_t1121933266 ** get_address_of_Register_Keyframe_DirectConverter_3() { return &___Register_Keyframe_DirectConverter_3; }
	inline void set_Register_Keyframe_DirectConverter_3(Keyframe_DirectConverter_t1121933266 * value)
	{
		___Register_Keyframe_DirectConverter_3 = value;
		Il2CppCodeGenWriteBarrier((&___Register_Keyframe_DirectConverter_3), value);
	}

	inline static int32_t get_offset_of_Register_LayerMask_DirectConverter_4() { return static_cast<int32_t>(offsetof(fsConverterRegistrar_t866553964_StaticFields, ___Register_LayerMask_DirectConverter_4)); }
	inline LayerMask_DirectConverter_t2775871809 * get_Register_LayerMask_DirectConverter_4() const { return ___Register_LayerMask_DirectConverter_4; }
	inline LayerMask_DirectConverter_t2775871809 ** get_address_of_Register_LayerMask_DirectConverter_4() { return &___Register_LayerMask_DirectConverter_4; }
	inline void set_Register_LayerMask_DirectConverter_4(LayerMask_DirectConverter_t2775871809 * value)
	{
		___Register_LayerMask_DirectConverter_4 = value;
		Il2CppCodeGenWriteBarrier((&___Register_LayerMask_DirectConverter_4), value);
	}

	inline static int32_t get_offset_of_Register_Rect_DirectConverter_5() { return static_cast<int32_t>(offsetof(fsConverterRegistrar_t866553964_StaticFields, ___Register_Rect_DirectConverter_5)); }
	inline Rect_DirectConverter_t2065695396 * get_Register_Rect_DirectConverter_5() const { return ___Register_Rect_DirectConverter_5; }
	inline Rect_DirectConverter_t2065695396 ** get_address_of_Register_Rect_DirectConverter_5() { return &___Register_Rect_DirectConverter_5; }
	inline void set_Register_Rect_DirectConverter_5(Rect_DirectConverter_t2065695396 * value)
	{
		___Register_Rect_DirectConverter_5 = value;
		Il2CppCodeGenWriteBarrier((&___Register_Rect_DirectConverter_5), value);
	}

	inline static int32_t get_offset_of_Converters_6() { return static_cast<int32_t>(offsetof(fsConverterRegistrar_t866553964_StaticFields, ___Converters_6)); }
	inline List_1_t672924358 * get_Converters_6() const { return ___Converters_6; }
	inline List_1_t672924358 ** get_address_of_Converters_6() { return &___Converters_6; }
	inline void set_Converters_6(List_1_t672924358 * value)
	{
		___Converters_6 = value;
		Il2CppCodeGenWriteBarrier((&___Converters_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSCONVERTERREGISTRAR_T866553964_H
#ifndef U3CONEFINGERTRANSFORMEDHANDLERU3EC__ANONSTOREY2_T3856358067_H
#define U3CONEFINGERTRANSFORMEDHANDLERU3EC__ANONSTOREY2_T3856358067_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Utilities.TouchEventManager/<OneFingerTransformedHandler>c__AnonStorey2
struct  U3COneFingerTransformedHandlerU3Ec__AnonStorey2_t3856358067  : public RuntimeObject
{
public:
	// UnityEngine.Transform IBM.Watson.DeveloperCloud.Utilities.TouchEventManager/<OneFingerTransformedHandler>c__AnonStorey2::hitTransform3D
	Transform_t3275118058 * ___hitTransform3D_0;
	// UnityEngine.Transform IBM.Watson.DeveloperCloud.Utilities.TouchEventManager/<OneFingerTransformedHandler>c__AnonStorey2::hitTransform2D
	Transform_t3275118058 * ___hitTransform2D_1;
	// UnityEngine.Transform IBM.Watson.DeveloperCloud.Utilities.TouchEventManager/<OneFingerTransformedHandler>c__AnonStorey2::hitTransform2DEventSystem
	Transform_t3275118058 * ___hitTransform2DEventSystem_2;

public:
	inline static int32_t get_offset_of_hitTransform3D_0() { return static_cast<int32_t>(offsetof(U3COneFingerTransformedHandlerU3Ec__AnonStorey2_t3856358067, ___hitTransform3D_0)); }
	inline Transform_t3275118058 * get_hitTransform3D_0() const { return ___hitTransform3D_0; }
	inline Transform_t3275118058 ** get_address_of_hitTransform3D_0() { return &___hitTransform3D_0; }
	inline void set_hitTransform3D_0(Transform_t3275118058 * value)
	{
		___hitTransform3D_0 = value;
		Il2CppCodeGenWriteBarrier((&___hitTransform3D_0), value);
	}

	inline static int32_t get_offset_of_hitTransform2D_1() { return static_cast<int32_t>(offsetof(U3COneFingerTransformedHandlerU3Ec__AnonStorey2_t3856358067, ___hitTransform2D_1)); }
	inline Transform_t3275118058 * get_hitTransform2D_1() const { return ___hitTransform2D_1; }
	inline Transform_t3275118058 ** get_address_of_hitTransform2D_1() { return &___hitTransform2D_1; }
	inline void set_hitTransform2D_1(Transform_t3275118058 * value)
	{
		___hitTransform2D_1 = value;
		Il2CppCodeGenWriteBarrier((&___hitTransform2D_1), value);
	}

	inline static int32_t get_offset_of_hitTransform2DEventSystem_2() { return static_cast<int32_t>(offsetof(U3COneFingerTransformedHandlerU3Ec__AnonStorey2_t3856358067, ___hitTransform2DEventSystem_2)); }
	inline Transform_t3275118058 * get_hitTransform2DEventSystem_2() const { return ___hitTransform2DEventSystem_2; }
	inline Transform_t3275118058 ** get_address_of_hitTransform2DEventSystem_2() { return &___hitTransform2DEventSystem_2; }
	inline void set_hitTransform2DEventSystem_2(Transform_t3275118058 * value)
	{
		___hitTransform2DEventSystem_2 = value;
		Il2CppCodeGenWriteBarrier((&___hitTransform2DEventSystem_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONEFINGERTRANSFORMEDHANDLERU3EC__ANONSTOREY2_T3856358067_H
#ifndef U3CTWOFINGERTRANSFORMEDHANDLERU3EC__ANONSTOREY4_T3436378969_H
#define U3CTWOFINGERTRANSFORMEDHANDLERU3EC__ANONSTOREY4_T3436378969_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Utilities.TouchEventManager/<TwoFingerTransformedHandler>c__AnonStorey4
struct  U3CTwoFingerTransformedHandlerU3Ec__AnonStorey4_t3436378969  : public RuntimeObject
{
public:
	// UnityEngine.Transform IBM.Watson.DeveloperCloud.Utilities.TouchEventManager/<TwoFingerTransformedHandler>c__AnonStorey4::hitTransform3D
	Transform_t3275118058 * ___hitTransform3D_0;
	// UnityEngine.Transform IBM.Watson.DeveloperCloud.Utilities.TouchEventManager/<TwoFingerTransformedHandler>c__AnonStorey4::hitTransform2D
	Transform_t3275118058 * ___hitTransform2D_1;
	// UnityEngine.Transform IBM.Watson.DeveloperCloud.Utilities.TouchEventManager/<TwoFingerTransformedHandler>c__AnonStorey4::hitTransform2DEventSystem
	Transform_t3275118058 * ___hitTransform2DEventSystem_2;

public:
	inline static int32_t get_offset_of_hitTransform3D_0() { return static_cast<int32_t>(offsetof(U3CTwoFingerTransformedHandlerU3Ec__AnonStorey4_t3436378969, ___hitTransform3D_0)); }
	inline Transform_t3275118058 * get_hitTransform3D_0() const { return ___hitTransform3D_0; }
	inline Transform_t3275118058 ** get_address_of_hitTransform3D_0() { return &___hitTransform3D_0; }
	inline void set_hitTransform3D_0(Transform_t3275118058 * value)
	{
		___hitTransform3D_0 = value;
		Il2CppCodeGenWriteBarrier((&___hitTransform3D_0), value);
	}

	inline static int32_t get_offset_of_hitTransform2D_1() { return static_cast<int32_t>(offsetof(U3CTwoFingerTransformedHandlerU3Ec__AnonStorey4_t3436378969, ___hitTransform2D_1)); }
	inline Transform_t3275118058 * get_hitTransform2D_1() const { return ___hitTransform2D_1; }
	inline Transform_t3275118058 ** get_address_of_hitTransform2D_1() { return &___hitTransform2D_1; }
	inline void set_hitTransform2D_1(Transform_t3275118058 * value)
	{
		___hitTransform2D_1 = value;
		Il2CppCodeGenWriteBarrier((&___hitTransform2D_1), value);
	}

	inline static int32_t get_offset_of_hitTransform2DEventSystem_2() { return static_cast<int32_t>(offsetof(U3CTwoFingerTransformedHandlerU3Ec__AnonStorey4_t3436378969, ___hitTransform2DEventSystem_2)); }
	inline Transform_t3275118058 * get_hitTransform2DEventSystem_2() const { return ___hitTransform2DEventSystem_2; }
	inline Transform_t3275118058 ** get_address_of_hitTransform2DEventSystem_2() { return &___hitTransform2DEventSystem_2; }
	inline void set_hitTransform2DEventSystem_2(Transform_t3275118058 * value)
	{
		___hitTransform2DEventSystem_2 = value;
		Il2CppCodeGenWriteBarrier((&___hitTransform2DEventSystem_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTWOFINGERTRANSFORMEDHANDLERU3EC__ANONSTOREY4_T3436378969_H
#ifndef U3CRECORDINGHANDLERU3EC__ITERATOR0_T1226459312_H
#define U3CRECORDINGHANDLERU3EC__ITERATOR0_T1226459312_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Widgets.WebCamWidget/<RecordingHandler>c__Iterator0
struct  U3CRecordingHandlerU3Ec__Iterator0_t1226459312  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.DataTypes.WebCamTextureData IBM.Watson.DeveloperCloud.Widgets.WebCamWidget/<RecordingHandler>c__Iterator0::<camData>__0
	WebCamTextureData_t1034677858 * ___U3CcamDataU3E__0_0;
	// IBM.Watson.DeveloperCloud.Widgets.WebCamWidget IBM.Watson.DeveloperCloud.Widgets.WebCamWidget/<RecordingHandler>c__Iterator0::$this
	WebCamWidget_t58556181 * ___U24this_1;
	// System.Object IBM.Watson.DeveloperCloud.Widgets.WebCamWidget/<RecordingHandler>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean IBM.Watson.DeveloperCloud.Widgets.WebCamWidget/<RecordingHandler>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 IBM.Watson.DeveloperCloud.Widgets.WebCamWidget/<RecordingHandler>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CcamDataU3E__0_0() { return static_cast<int32_t>(offsetof(U3CRecordingHandlerU3Ec__Iterator0_t1226459312, ___U3CcamDataU3E__0_0)); }
	inline WebCamTextureData_t1034677858 * get_U3CcamDataU3E__0_0() const { return ___U3CcamDataU3E__0_0; }
	inline WebCamTextureData_t1034677858 ** get_address_of_U3CcamDataU3E__0_0() { return &___U3CcamDataU3E__0_0; }
	inline void set_U3CcamDataU3E__0_0(WebCamTextureData_t1034677858 * value)
	{
		___U3CcamDataU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcamDataU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CRecordingHandlerU3Ec__Iterator0_t1226459312, ___U24this_1)); }
	inline WebCamWidget_t58556181 * get_U24this_1() const { return ___U24this_1; }
	inline WebCamWidget_t58556181 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(WebCamWidget_t58556181 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CRecordingHandlerU3Ec__Iterator0_t1226459312, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CRecordingHandlerU3Ec__Iterator0_t1226459312, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CRecordingHandlerU3Ec__Iterator0_t1226459312, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRECORDINGHANDLERU3EC__ITERATOR0_T1226459312_H
#ifndef U3CUNREGISTERDRAGEVENTU3EC__ANONSTOREY0_T3641881935_H
#define U3CUNREGISTERDRAGEVENTU3EC__ANONSTOREY0_T3641881935_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Utilities.TouchEventManager/<UnregisterDragEvent>c__AnonStorey0
struct  U3CUnregisterDragEventU3Ec__AnonStorey0_t3641881935  : public RuntimeObject
{
public:
	// UnityEngine.GameObject IBM.Watson.DeveloperCloud.Utilities.TouchEventManager/<UnregisterDragEvent>c__AnonStorey0::gameObjectToDrag
	GameObject_t1756533147 * ___gameObjectToDrag_0;
	// System.String IBM.Watson.DeveloperCloud.Utilities.TouchEventManager/<UnregisterDragEvent>c__AnonStorey0::callback
	String_t* ___callback_1;
	// System.Int32 IBM.Watson.DeveloperCloud.Utilities.TouchEventManager/<UnregisterDragEvent>c__AnonStorey0::SortingLayer
	int32_t ___SortingLayer_2;
	// System.Boolean IBM.Watson.DeveloperCloud.Utilities.TouchEventManager/<UnregisterDragEvent>c__AnonStorey0::isDragInside
	bool ___isDragInside_3;

public:
	inline static int32_t get_offset_of_gameObjectToDrag_0() { return static_cast<int32_t>(offsetof(U3CUnregisterDragEventU3Ec__AnonStorey0_t3641881935, ___gameObjectToDrag_0)); }
	inline GameObject_t1756533147 * get_gameObjectToDrag_0() const { return ___gameObjectToDrag_0; }
	inline GameObject_t1756533147 ** get_address_of_gameObjectToDrag_0() { return &___gameObjectToDrag_0; }
	inline void set_gameObjectToDrag_0(GameObject_t1756533147 * value)
	{
		___gameObjectToDrag_0 = value;
		Il2CppCodeGenWriteBarrier((&___gameObjectToDrag_0), value);
	}

	inline static int32_t get_offset_of_callback_1() { return static_cast<int32_t>(offsetof(U3CUnregisterDragEventU3Ec__AnonStorey0_t3641881935, ___callback_1)); }
	inline String_t* get_callback_1() const { return ___callback_1; }
	inline String_t** get_address_of_callback_1() { return &___callback_1; }
	inline void set_callback_1(String_t* value)
	{
		___callback_1 = value;
		Il2CppCodeGenWriteBarrier((&___callback_1), value);
	}

	inline static int32_t get_offset_of_SortingLayer_2() { return static_cast<int32_t>(offsetof(U3CUnregisterDragEventU3Ec__AnonStorey0_t3641881935, ___SortingLayer_2)); }
	inline int32_t get_SortingLayer_2() const { return ___SortingLayer_2; }
	inline int32_t* get_address_of_SortingLayer_2() { return &___SortingLayer_2; }
	inline void set_SortingLayer_2(int32_t value)
	{
		___SortingLayer_2 = value;
	}

	inline static int32_t get_offset_of_isDragInside_3() { return static_cast<int32_t>(offsetof(U3CUnregisterDragEventU3Ec__AnonStorey0_t3641881935, ___isDragInside_3)); }
	inline bool get_isDragInside_3() const { return ___isDragInside_3; }
	inline bool* get_address_of_isDragInside_3() { return &___isDragInside_3; }
	inline void set_isDragInside_3(bool value)
	{
		___isDragInside_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUNREGISTERDRAGEVENTU3EC__ANONSTOREY0_T3641881935_H
#ifndef SERIALIZEDDELEGATE_T808690627_H
#define SERIALIZEDDELEGATE_T808690627_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Utilities.SerializedDelegate
struct  SerializedDelegate_t808690627  : public RuntimeObject
{
public:
	// System.Type IBM.Watson.DeveloperCloud.Utilities.SerializedDelegate::<DelegateType>k__BackingField
	Type_t * ___U3CDelegateTypeU3Ek__BackingField_0;
	// UnityEngine.GameObject IBM.Watson.DeveloperCloud.Utilities.SerializedDelegate::m_Target
	GameObject_t1756533147 * ___m_Target_1;
	// System.String IBM.Watson.DeveloperCloud.Utilities.SerializedDelegate::m_Component
	String_t* ___m_Component_2;
	// System.String IBM.Watson.DeveloperCloud.Utilities.SerializedDelegate::m_Method
	String_t* ___m_Method_3;

public:
	inline static int32_t get_offset_of_U3CDelegateTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SerializedDelegate_t808690627, ___U3CDelegateTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CDelegateTypeU3Ek__BackingField_0() const { return ___U3CDelegateTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CDelegateTypeU3Ek__BackingField_0() { return &___U3CDelegateTypeU3Ek__BackingField_0; }
	inline void set_U3CDelegateTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CDelegateTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDelegateTypeU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_m_Target_1() { return static_cast<int32_t>(offsetof(SerializedDelegate_t808690627, ___m_Target_1)); }
	inline GameObject_t1756533147 * get_m_Target_1() const { return ___m_Target_1; }
	inline GameObject_t1756533147 ** get_address_of_m_Target_1() { return &___m_Target_1; }
	inline void set_m_Target_1(GameObject_t1756533147 * value)
	{
		___m_Target_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_1), value);
	}

	inline static int32_t get_offset_of_m_Component_2() { return static_cast<int32_t>(offsetof(SerializedDelegate_t808690627, ___m_Component_2)); }
	inline String_t* get_m_Component_2() const { return ___m_Component_2; }
	inline String_t** get_address_of_m_Component_2() { return &___m_Component_2; }
	inline void set_m_Component_2(String_t* value)
	{
		___m_Component_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Component_2), value);
	}

	inline static int32_t get_offset_of_m_Method_3() { return static_cast<int32_t>(offsetof(SerializedDelegate_t808690627, ___m_Method_3)); }
	inline String_t* get_m_Method_3() const { return ___m_Method_3; }
	inline String_t** get_address_of_m_Method_3() { return &___m_Method_3; }
	inline void set_m_Method_3(String_t* value)
	{
		___m_Method_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Method_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZEDDELEGATE_T808690627_H
#ifndef FSAOTCOMPILATIONMANAGER_T3563908143_H
#define FSAOTCOMPILATIONMANAGER_T3563908143_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsAotCompilationManager
struct  fsAotCompilationManager_t3563908143  : public RuntimeObject
{
public:

public:
};

struct fsAotCompilationManager_t3563908143_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,System.String> FullSerializer.fsAotCompilationManager::_computedAotCompilations
	Dictionary_2_t3966578130 * ____computedAotCompilations_0;
	// System.Collections.Generic.List`1<FullSerializer.fsAotCompilationManager/AotCompilation> FullSerializer.fsAotCompilationManager::_uncomputedAotCompilations
	List_1_t3685996670 * ____uncomputedAotCompilations_1;

public:
	inline static int32_t get_offset_of__computedAotCompilations_0() { return static_cast<int32_t>(offsetof(fsAotCompilationManager_t3563908143_StaticFields, ____computedAotCompilations_0)); }
	inline Dictionary_2_t3966578130 * get__computedAotCompilations_0() const { return ____computedAotCompilations_0; }
	inline Dictionary_2_t3966578130 ** get_address_of__computedAotCompilations_0() { return &____computedAotCompilations_0; }
	inline void set__computedAotCompilations_0(Dictionary_2_t3966578130 * value)
	{
		____computedAotCompilations_0 = value;
		Il2CppCodeGenWriteBarrier((&____computedAotCompilations_0), value);
	}

	inline static int32_t get_offset_of__uncomputedAotCompilations_1() { return static_cast<int32_t>(offsetof(fsAotCompilationManager_t3563908143_StaticFields, ____uncomputedAotCompilations_1)); }
	inline List_1_t3685996670 * get__uncomputedAotCompilations_1() const { return ____uncomputedAotCompilations_1; }
	inline List_1_t3685996670 ** get_address_of__uncomputedAotCompilations_1() { return &____uncomputedAotCompilations_1; }
	inline void set__uncomputedAotCompilations_1(List_1_t3685996670 * value)
	{
		____uncomputedAotCompilations_1 = value;
		Il2CppCodeGenWriteBarrier((&____uncomputedAotCompilations_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSAOTCOMPILATIONMANAGER_T3563908143_H
#ifndef TOUCHEVENTDATA_T2072067249_H
#define TOUCHEVENTDATA_T2072067249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Utilities.TouchEventManager/TouchEventData
struct  TouchEventData_t2072067249  : public RuntimeObject
{
public:
	// UnityEngine.Collider IBM.Watson.DeveloperCloud.Utilities.TouchEventManager/TouchEventData::m_Collider
	Collider_t3497673348 * ___m_Collider_0;
	// UnityEngine.Collider2D IBM.Watson.DeveloperCloud.Utilities.TouchEventManager/TouchEventData::m_Collider2D
	Collider2D_t646061738 * ___m_Collider2D_1;
	// UnityEngine.RectTransform IBM.Watson.DeveloperCloud.Utilities.TouchEventManager/TouchEventData::m_RectTransform
	RectTransform_t3349966182 * ___m_RectTransform_2;
	// UnityEngine.Collider[] IBM.Watson.DeveloperCloud.Utilities.TouchEventManager/TouchEventData::m_ColliderList
	ColliderU5BU5D_t462843629* ___m_ColliderList_3;
	// UnityEngine.Collider2D[] IBM.Watson.DeveloperCloud.Utilities.TouchEventManager/TouchEventData::m_Collider2DList
	Collider2DU5BU5D_t3535523695* ___m_Collider2DList_4;
	// UnityEngine.RectTransform[] IBM.Watson.DeveloperCloud.Utilities.TouchEventManager/TouchEventData::m_RectTransformList
	RectTransformU5BU5D_t3948421699* ___m_RectTransformList_5;
	// UnityEngine.GameObject IBM.Watson.DeveloperCloud.Utilities.TouchEventManager/TouchEventData::m_GameObject
	GameObject_t1756533147 * ___m_GameObject_6;
	// System.String IBM.Watson.DeveloperCloud.Utilities.TouchEventManager/TouchEventData::m_tapEventCallback
	String_t* ___m_tapEventCallback_7;
	// System.String IBM.Watson.DeveloperCloud.Utilities.TouchEventManager/TouchEventData::m_dragEventCallback
	String_t* ___m_dragEventCallback_8;
	// System.Boolean IBM.Watson.DeveloperCloud.Utilities.TouchEventManager/TouchEventData::m_isInside
	bool ___m_isInside_9;
	// System.Int32 IBM.Watson.DeveloperCloud.Utilities.TouchEventManager/TouchEventData::m_SortingLayer
	int32_t ___m_SortingLayer_10;

public:
	inline static int32_t get_offset_of_m_Collider_0() { return static_cast<int32_t>(offsetof(TouchEventData_t2072067249, ___m_Collider_0)); }
	inline Collider_t3497673348 * get_m_Collider_0() const { return ___m_Collider_0; }
	inline Collider_t3497673348 ** get_address_of_m_Collider_0() { return &___m_Collider_0; }
	inline void set_m_Collider_0(Collider_t3497673348 * value)
	{
		___m_Collider_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Collider_0), value);
	}

	inline static int32_t get_offset_of_m_Collider2D_1() { return static_cast<int32_t>(offsetof(TouchEventData_t2072067249, ___m_Collider2D_1)); }
	inline Collider2D_t646061738 * get_m_Collider2D_1() const { return ___m_Collider2D_1; }
	inline Collider2D_t646061738 ** get_address_of_m_Collider2D_1() { return &___m_Collider2D_1; }
	inline void set_m_Collider2D_1(Collider2D_t646061738 * value)
	{
		___m_Collider2D_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Collider2D_1), value);
	}

	inline static int32_t get_offset_of_m_RectTransform_2() { return static_cast<int32_t>(offsetof(TouchEventData_t2072067249, ___m_RectTransform_2)); }
	inline RectTransform_t3349966182 * get_m_RectTransform_2() const { return ___m_RectTransform_2; }
	inline RectTransform_t3349966182 ** get_address_of_m_RectTransform_2() { return &___m_RectTransform_2; }
	inline void set_m_RectTransform_2(RectTransform_t3349966182 * value)
	{
		___m_RectTransform_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_2), value);
	}

	inline static int32_t get_offset_of_m_ColliderList_3() { return static_cast<int32_t>(offsetof(TouchEventData_t2072067249, ___m_ColliderList_3)); }
	inline ColliderU5BU5D_t462843629* get_m_ColliderList_3() const { return ___m_ColliderList_3; }
	inline ColliderU5BU5D_t462843629** get_address_of_m_ColliderList_3() { return &___m_ColliderList_3; }
	inline void set_m_ColliderList_3(ColliderU5BU5D_t462843629* value)
	{
		___m_ColliderList_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColliderList_3), value);
	}

	inline static int32_t get_offset_of_m_Collider2DList_4() { return static_cast<int32_t>(offsetof(TouchEventData_t2072067249, ___m_Collider2DList_4)); }
	inline Collider2DU5BU5D_t3535523695* get_m_Collider2DList_4() const { return ___m_Collider2DList_4; }
	inline Collider2DU5BU5D_t3535523695** get_address_of_m_Collider2DList_4() { return &___m_Collider2DList_4; }
	inline void set_m_Collider2DList_4(Collider2DU5BU5D_t3535523695* value)
	{
		___m_Collider2DList_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Collider2DList_4), value);
	}

	inline static int32_t get_offset_of_m_RectTransformList_5() { return static_cast<int32_t>(offsetof(TouchEventData_t2072067249, ___m_RectTransformList_5)); }
	inline RectTransformU5BU5D_t3948421699* get_m_RectTransformList_5() const { return ___m_RectTransformList_5; }
	inline RectTransformU5BU5D_t3948421699** get_address_of_m_RectTransformList_5() { return &___m_RectTransformList_5; }
	inline void set_m_RectTransformList_5(RectTransformU5BU5D_t3948421699* value)
	{
		___m_RectTransformList_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransformList_5), value);
	}

	inline static int32_t get_offset_of_m_GameObject_6() { return static_cast<int32_t>(offsetof(TouchEventData_t2072067249, ___m_GameObject_6)); }
	inline GameObject_t1756533147 * get_m_GameObject_6() const { return ___m_GameObject_6; }
	inline GameObject_t1756533147 ** get_address_of_m_GameObject_6() { return &___m_GameObject_6; }
	inline void set_m_GameObject_6(GameObject_t1756533147 * value)
	{
		___m_GameObject_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_GameObject_6), value);
	}

	inline static int32_t get_offset_of_m_tapEventCallback_7() { return static_cast<int32_t>(offsetof(TouchEventData_t2072067249, ___m_tapEventCallback_7)); }
	inline String_t* get_m_tapEventCallback_7() const { return ___m_tapEventCallback_7; }
	inline String_t** get_address_of_m_tapEventCallback_7() { return &___m_tapEventCallback_7; }
	inline void set_m_tapEventCallback_7(String_t* value)
	{
		___m_tapEventCallback_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_tapEventCallback_7), value);
	}

	inline static int32_t get_offset_of_m_dragEventCallback_8() { return static_cast<int32_t>(offsetof(TouchEventData_t2072067249, ___m_dragEventCallback_8)); }
	inline String_t* get_m_dragEventCallback_8() const { return ___m_dragEventCallback_8; }
	inline String_t** get_address_of_m_dragEventCallback_8() { return &___m_dragEventCallback_8; }
	inline void set_m_dragEventCallback_8(String_t* value)
	{
		___m_dragEventCallback_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_dragEventCallback_8), value);
	}

	inline static int32_t get_offset_of_m_isInside_9() { return static_cast<int32_t>(offsetof(TouchEventData_t2072067249, ___m_isInside_9)); }
	inline bool get_m_isInside_9() const { return ___m_isInside_9; }
	inline bool* get_address_of_m_isInside_9() { return &___m_isInside_9; }
	inline void set_m_isInside_9(bool value)
	{
		___m_isInside_9 = value;
	}

	inline static int32_t get_offset_of_m_SortingLayer_10() { return static_cast<int32_t>(offsetof(TouchEventData_t2072067249, ___m_SortingLayer_10)); }
	inline int32_t get_m_SortingLayer_10() const { return ___m_SortingLayer_10; }
	inline int32_t* get_address_of_m_SortingLayer_10() { return &___m_SortingLayer_10; }
	inline void set_m_SortingLayer_10(int32_t value)
	{
		___m_SortingLayer_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHEVENTDATA_T2072067249_H
#ifndef ROUTINE_T1973058165_H
#define ROUTINE_T1973058165_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Utilities.Runnable/Routine
struct  Routine_t1973058165  : public RuntimeObject
{
public:
	// System.Int32 IBM.Watson.DeveloperCloud.Utilities.Runnable/Routine::<ID>k__BackingField
	int32_t ___U3CIDU3Ek__BackingField_0;
	// System.Boolean IBM.Watson.DeveloperCloud.Utilities.Runnable/Routine::<Stop>k__BackingField
	bool ___U3CStopU3Ek__BackingField_1;
	// System.Boolean IBM.Watson.DeveloperCloud.Utilities.Runnable/Routine::m_bMoveNext
	bool ___m_bMoveNext_2;
	// System.Collections.IEnumerator IBM.Watson.DeveloperCloud.Utilities.Runnable/Routine::m_Enumerator
	RuntimeObject* ___m_Enumerator_3;

public:
	inline static int32_t get_offset_of_U3CIDU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Routine_t1973058165, ___U3CIDU3Ek__BackingField_0)); }
	inline int32_t get_U3CIDU3Ek__BackingField_0() const { return ___U3CIDU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CIDU3Ek__BackingField_0() { return &___U3CIDU3Ek__BackingField_0; }
	inline void set_U3CIDU3Ek__BackingField_0(int32_t value)
	{
		___U3CIDU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CStopU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Routine_t1973058165, ___U3CStopU3Ek__BackingField_1)); }
	inline bool get_U3CStopU3Ek__BackingField_1() const { return ___U3CStopU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CStopU3Ek__BackingField_1() { return &___U3CStopU3Ek__BackingField_1; }
	inline void set_U3CStopU3Ek__BackingField_1(bool value)
	{
		___U3CStopU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_m_bMoveNext_2() { return static_cast<int32_t>(offsetof(Routine_t1973058165, ___m_bMoveNext_2)); }
	inline bool get_m_bMoveNext_2() const { return ___m_bMoveNext_2; }
	inline bool* get_address_of_m_bMoveNext_2() { return &___m_bMoveNext_2; }
	inline void set_m_bMoveNext_2(bool value)
	{
		___m_bMoveNext_2 = value;
	}

	inline static int32_t get_offset_of_m_Enumerator_3() { return static_cast<int32_t>(offsetof(Routine_t1973058165, ___m_Enumerator_3)); }
	inline RuntimeObject* get_m_Enumerator_3() const { return ___m_Enumerator_3; }
	inline RuntimeObject** get_address_of_m_Enumerator_3() { return &___m_Enumerator_3; }
	inline void set_m_Enumerator_3(RuntimeObject* value)
	{
		___m_Enumerator_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Enumerator_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROUTINE_T1973058165_H
#ifndef FSBASECONVERTER_T1241677426_H
#define FSBASECONVERTER_T1241677426_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsBaseConverter
struct  fsBaseConverter_t1241677426  : public RuntimeObject
{
public:
	// FullSerializer.fsSerializer FullSerializer.fsBaseConverter::Serializer
	fsSerializer_t4193731081 * ___Serializer_0;

public:
	inline static int32_t get_offset_of_Serializer_0() { return static_cast<int32_t>(offsetof(fsBaseConverter_t1241677426, ___Serializer_0)); }
	inline fsSerializer_t4193731081 * get_Serializer_0() const { return ___Serializer_0; }
	inline fsSerializer_t4193731081 ** get_address_of_Serializer_0() { return &___Serializer_0; }
	inline void set_Serializer_0(fsSerializer_t4193731081 * value)
	{
		___Serializer_0 = value;
		Il2CppCodeGenWriteBarrier((&___Serializer_0), value);
	}
};

struct fsBaseConverter_t1241677426_StaticFields
{
public:
	// System.Func`2<FullSerializer.fsDataType,System.String> FullSerializer.fsBaseConverter::<>f__am$cache0
	Func_2_t1180654441 * ___U3CU3Ef__amU24cache0_1;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_1() { return static_cast<int32_t>(offsetof(fsBaseConverter_t1241677426_StaticFields, ___U3CU3Ef__amU24cache0_1)); }
	inline Func_2_t1180654441 * get_U3CU3Ef__amU24cache0_1() const { return ___U3CU3Ef__amU24cache0_1; }
	inline Func_2_t1180654441 ** get_address_of_U3CU3Ef__amU24cache0_1() { return &___U3CU3Ef__amU24cache0_1; }
	inline void set_U3CU3Ef__amU24cache0_1(Func_2_t1180654441 * value)
	{
		___U3CU3Ef__amU24cache0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSBASECONVERTER_T1241677426_H
#ifndef U3CUNREGISTERDOUBLETAPEVENTU3EC__ANONSTOREY9_T4055595416_H
#define U3CUNREGISTERDOUBLETAPEVENTU3EC__ANONSTOREY9_T4055595416_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Utilities.TouchEventManager/<UnregisterDoubleTapEvent>c__AnonStorey9
struct  U3CUnregisterDoubleTapEventU3Ec__AnonStorey9_t4055595416  : public RuntimeObject
{
public:
	// UnityEngine.Collider IBM.Watson.DeveloperCloud.Utilities.TouchEventManager/<UnregisterDoubleTapEvent>c__AnonStorey9::itemCollider
	Collider_t3497673348 * ___itemCollider_0;
	// IBM.Watson.DeveloperCloud.Utilities.TouchEventManager/<UnregisterDoubleTapEvent>c__AnonStoreyA IBM.Watson.DeveloperCloud.Utilities.TouchEventManager/<UnregisterDoubleTapEvent>c__AnonStorey9::<>f__ref$10
	U3CUnregisterDoubleTapEventU3Ec__AnonStoreyA_t137061456 * ___U3CU3Ef__refU2410_1;

public:
	inline static int32_t get_offset_of_itemCollider_0() { return static_cast<int32_t>(offsetof(U3CUnregisterDoubleTapEventU3Ec__AnonStorey9_t4055595416, ___itemCollider_0)); }
	inline Collider_t3497673348 * get_itemCollider_0() const { return ___itemCollider_0; }
	inline Collider_t3497673348 ** get_address_of_itemCollider_0() { return &___itemCollider_0; }
	inline void set_itemCollider_0(Collider_t3497673348 * value)
	{
		___itemCollider_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemCollider_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU2410_1() { return static_cast<int32_t>(offsetof(U3CUnregisterDoubleTapEventU3Ec__AnonStorey9_t4055595416, ___U3CU3Ef__refU2410_1)); }
	inline U3CUnregisterDoubleTapEventU3Ec__AnonStoreyA_t137061456 * get_U3CU3Ef__refU2410_1() const { return ___U3CU3Ef__refU2410_1; }
	inline U3CUnregisterDoubleTapEventU3Ec__AnonStoreyA_t137061456 ** get_address_of_U3CU3Ef__refU2410_1() { return &___U3CU3Ef__refU2410_1; }
	inline void set_U3CU3Ef__refU2410_1(U3CUnregisterDoubleTapEventU3Ec__AnonStoreyA_t137061456 * value)
	{
		___U3CU3Ef__refU2410_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU2410_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUNREGISTERDOUBLETAPEVENTU3EC__ANONSTOREY9_T4055595416_H
#ifndef U3CUNREGISTERDOUBLETAPEVENTU3EC__ANONSTOREYA_T137061456_H
#define U3CUNREGISTERDOUBLETAPEVENTU3EC__ANONSTOREYA_T137061456_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Utilities.TouchEventManager/<UnregisterDoubleTapEvent>c__AnonStoreyA
struct  U3CUnregisterDoubleTapEventU3Ec__AnonStoreyA_t137061456  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Utilities.TouchEventManager/<UnregisterDoubleTapEvent>c__AnonStoreyA::callback
	String_t* ___callback_0;
	// System.Int32 IBM.Watson.DeveloperCloud.Utilities.TouchEventManager/<UnregisterDoubleTapEvent>c__AnonStoreyA::SortingLayer
	int32_t ___SortingLayer_1;
	// System.Boolean IBM.Watson.DeveloperCloud.Utilities.TouchEventManager/<UnregisterDoubleTapEvent>c__AnonStoreyA::isDoubleTapInside
	bool ___isDoubleTapInside_2;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CUnregisterDoubleTapEventU3Ec__AnonStoreyA_t137061456, ___callback_0)); }
	inline String_t* get_callback_0() const { return ___callback_0; }
	inline String_t** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(String_t* value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}

	inline static int32_t get_offset_of_SortingLayer_1() { return static_cast<int32_t>(offsetof(U3CUnregisterDoubleTapEventU3Ec__AnonStoreyA_t137061456, ___SortingLayer_1)); }
	inline int32_t get_SortingLayer_1() const { return ___SortingLayer_1; }
	inline int32_t* get_address_of_SortingLayer_1() { return &___SortingLayer_1; }
	inline void set_SortingLayer_1(int32_t value)
	{
		___SortingLayer_1 = value;
	}

	inline static int32_t get_offset_of_isDoubleTapInside_2() { return static_cast<int32_t>(offsetof(U3CUnregisterDoubleTapEventU3Ec__AnonStoreyA_t137061456, ___isDoubleTapInside_2)); }
	inline bool get_isDoubleTapInside_2() const { return ___isDoubleTapInside_2; }
	inline bool* get_address_of_isDoubleTapInside_2() { return &___isDoubleTapInside_2; }
	inline void set_isDoubleTapInside_2(bool value)
	{
		___isDoubleTapInside_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUNREGISTERDOUBLETAPEVENTU3EC__ANONSTOREYA_T137061456_H
#ifndef U3CUNREGISTERDOUBLETAPEVENTU3EC__ANONSTOREYC_T1299860870_H
#define U3CUNREGISTERDOUBLETAPEVENTU3EC__ANONSTOREYC_T1299860870_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Utilities.TouchEventManager/<UnregisterDoubleTapEvent>c__AnonStoreyC
struct  U3CUnregisterDoubleTapEventU3Ec__AnonStoreyC_t1299860870  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform IBM.Watson.DeveloperCloud.Utilities.TouchEventManager/<UnregisterDoubleTapEvent>c__AnonStoreyC::itemRectTransform
	RectTransform_t3349966182 * ___itemRectTransform_0;
	// IBM.Watson.DeveloperCloud.Utilities.TouchEventManager/<UnregisterDoubleTapEvent>c__AnonStoreyA IBM.Watson.DeveloperCloud.Utilities.TouchEventManager/<UnregisterDoubleTapEvent>c__AnonStoreyC::<>f__ref$10
	U3CUnregisterDoubleTapEventU3Ec__AnonStoreyA_t137061456 * ___U3CU3Ef__refU2410_1;

public:
	inline static int32_t get_offset_of_itemRectTransform_0() { return static_cast<int32_t>(offsetof(U3CUnregisterDoubleTapEventU3Ec__AnonStoreyC_t1299860870, ___itemRectTransform_0)); }
	inline RectTransform_t3349966182 * get_itemRectTransform_0() const { return ___itemRectTransform_0; }
	inline RectTransform_t3349966182 ** get_address_of_itemRectTransform_0() { return &___itemRectTransform_0; }
	inline void set_itemRectTransform_0(RectTransform_t3349966182 * value)
	{
		___itemRectTransform_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemRectTransform_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU2410_1() { return static_cast<int32_t>(offsetof(U3CUnregisterDoubleTapEventU3Ec__AnonStoreyC_t1299860870, ___U3CU3Ef__refU2410_1)); }
	inline U3CUnregisterDoubleTapEventU3Ec__AnonStoreyA_t137061456 * get_U3CU3Ef__refU2410_1() const { return ___U3CU3Ef__refU2410_1; }
	inline U3CUnregisterDoubleTapEventU3Ec__AnonStoreyA_t137061456 ** get_address_of_U3CU3Ef__refU2410_1() { return &___U3CU3Ef__refU2410_1; }
	inline void set_U3CU3Ef__refU2410_1(U3CUnregisterDoubleTapEventU3Ec__AnonStoreyA_t137061456 * value)
	{
		___U3CU3Ef__refU2410_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU2410_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUNREGISTERDOUBLETAPEVENTU3EC__ANONSTOREYC_T1299860870_H
#ifndef U3CUNREGISTERDOUBLETAPEVENTU3EC__ANONSTOREYB_T4028744225_H
#define U3CUNREGISTERDOUBLETAPEVENTU3EC__ANONSTOREYB_T4028744225_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Utilities.TouchEventManager/<UnregisterDoubleTapEvent>c__AnonStoreyB
struct  U3CUnregisterDoubleTapEventU3Ec__AnonStoreyB_t4028744225  : public RuntimeObject
{
public:
	// UnityEngine.Collider2D IBM.Watson.DeveloperCloud.Utilities.TouchEventManager/<UnregisterDoubleTapEvent>c__AnonStoreyB::itemCollider2D
	Collider2D_t646061738 * ___itemCollider2D_0;
	// IBM.Watson.DeveloperCloud.Utilities.TouchEventManager/<UnregisterDoubleTapEvent>c__AnonStoreyA IBM.Watson.DeveloperCloud.Utilities.TouchEventManager/<UnregisterDoubleTapEvent>c__AnonStoreyB::<>f__ref$10
	U3CUnregisterDoubleTapEventU3Ec__AnonStoreyA_t137061456 * ___U3CU3Ef__refU2410_1;

public:
	inline static int32_t get_offset_of_itemCollider2D_0() { return static_cast<int32_t>(offsetof(U3CUnregisterDoubleTapEventU3Ec__AnonStoreyB_t4028744225, ___itemCollider2D_0)); }
	inline Collider2D_t646061738 * get_itemCollider2D_0() const { return ___itemCollider2D_0; }
	inline Collider2D_t646061738 ** get_address_of_itemCollider2D_0() { return &___itemCollider2D_0; }
	inline void set_itemCollider2D_0(Collider2D_t646061738 * value)
	{
		___itemCollider2D_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemCollider2D_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU2410_1() { return static_cast<int32_t>(offsetof(U3CUnregisterDoubleTapEventU3Ec__AnonStoreyB_t4028744225, ___U3CU3Ef__refU2410_1)); }
	inline U3CUnregisterDoubleTapEventU3Ec__AnonStoreyA_t137061456 * get_U3CU3Ef__refU2410_1() const { return ___U3CU3Ef__refU2410_1; }
	inline U3CUnregisterDoubleTapEventU3Ec__AnonStoreyA_t137061456 ** get_address_of_U3CU3Ef__refU2410_1() { return &___U3CU3Ef__refU2410_1; }
	inline void set_U3CU3Ef__refU2410_1(U3CUnregisterDoubleTapEventU3Ec__AnonStoreyA_t137061456 * value)
	{
		___U3CU3Ef__refU2410_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU2410_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUNREGISTERDOUBLETAPEVENTU3EC__ANONSTOREYB_T4028744225_H
#ifndef U3CUNREGISTERTAPEVENTU3EC__ANONSTOREY8_T4278050684_H
#define U3CUNREGISTERTAPEVENTU3EC__ANONSTOREY8_T4278050684_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Utilities.TouchEventManager/<UnregisterTapEvent>c__AnonStorey8
struct  U3CUnregisterTapEventU3Ec__AnonStorey8_t4278050684  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform IBM.Watson.DeveloperCloud.Utilities.TouchEventManager/<UnregisterTapEvent>c__AnonStorey8::itemRectTransform
	RectTransform_t3349966182 * ___itemRectTransform_0;
	// IBM.Watson.DeveloperCloud.Utilities.TouchEventManager/<UnregisterTapEvent>c__AnonStorey6 IBM.Watson.DeveloperCloud.Utilities.TouchEventManager/<UnregisterTapEvent>c__AnonStorey8::<>f__ref$6
	U3CUnregisterTapEventU3Ec__AnonStorey6_t3827711990 * ___U3CU3Ef__refU246_1;

public:
	inline static int32_t get_offset_of_itemRectTransform_0() { return static_cast<int32_t>(offsetof(U3CUnregisterTapEventU3Ec__AnonStorey8_t4278050684, ___itemRectTransform_0)); }
	inline RectTransform_t3349966182 * get_itemRectTransform_0() const { return ___itemRectTransform_0; }
	inline RectTransform_t3349966182 ** get_address_of_itemRectTransform_0() { return &___itemRectTransform_0; }
	inline void set_itemRectTransform_0(RectTransform_t3349966182 * value)
	{
		___itemRectTransform_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemRectTransform_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU246_1() { return static_cast<int32_t>(offsetof(U3CUnregisterTapEventU3Ec__AnonStorey8_t4278050684, ___U3CU3Ef__refU246_1)); }
	inline U3CUnregisterTapEventU3Ec__AnonStorey6_t3827711990 * get_U3CU3Ef__refU246_1() const { return ___U3CU3Ef__refU246_1; }
	inline U3CUnregisterTapEventU3Ec__AnonStorey6_t3827711990 ** get_address_of_U3CU3Ef__refU246_1() { return &___U3CU3Ef__refU246_1; }
	inline void set_U3CU3Ef__refU246_1(U3CUnregisterTapEventU3Ec__AnonStorey6_t3827711990 * value)
	{
		___U3CU3Ef__refU246_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU246_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUNREGISTERTAPEVENTU3EC__ANONSTOREY8_T4278050684_H
#ifndef U3CUNREGISTERTAPEVENTU3EC__ANONSTOREY5_T1098828635_H
#define U3CUNREGISTERTAPEVENTU3EC__ANONSTOREY5_T1098828635_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Utilities.TouchEventManager/<UnregisterTapEvent>c__AnonStorey5
struct  U3CUnregisterTapEventU3Ec__AnonStorey5_t1098828635  : public RuntimeObject
{
public:
	// UnityEngine.Collider IBM.Watson.DeveloperCloud.Utilities.TouchEventManager/<UnregisterTapEvent>c__AnonStorey5::itemCollider
	Collider_t3497673348 * ___itemCollider_0;
	// IBM.Watson.DeveloperCloud.Utilities.TouchEventManager/<UnregisterTapEvent>c__AnonStorey6 IBM.Watson.DeveloperCloud.Utilities.TouchEventManager/<UnregisterTapEvent>c__AnonStorey5::<>f__ref$6
	U3CUnregisterTapEventU3Ec__AnonStorey6_t3827711990 * ___U3CU3Ef__refU246_1;

public:
	inline static int32_t get_offset_of_itemCollider_0() { return static_cast<int32_t>(offsetof(U3CUnregisterTapEventU3Ec__AnonStorey5_t1098828635, ___itemCollider_0)); }
	inline Collider_t3497673348 * get_itemCollider_0() const { return ___itemCollider_0; }
	inline Collider_t3497673348 ** get_address_of_itemCollider_0() { return &___itemCollider_0; }
	inline void set_itemCollider_0(Collider_t3497673348 * value)
	{
		___itemCollider_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemCollider_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU246_1() { return static_cast<int32_t>(offsetof(U3CUnregisterTapEventU3Ec__AnonStorey5_t1098828635, ___U3CU3Ef__refU246_1)); }
	inline U3CUnregisterTapEventU3Ec__AnonStorey6_t3827711990 * get_U3CU3Ef__refU246_1() const { return ___U3CU3Ef__refU246_1; }
	inline U3CUnregisterTapEventU3Ec__AnonStorey6_t3827711990 ** get_address_of_U3CU3Ef__refU246_1() { return &___U3CU3Ef__refU246_1; }
	inline void set_U3CU3Ef__refU246_1(U3CUnregisterTapEventU3Ec__AnonStorey6_t3827711990 * value)
	{
		___U3CU3Ef__refU246_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU246_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUNREGISTERTAPEVENTU3EC__ANONSTOREY5_T1098828635_H
#ifndef U3CUNREGISTERTAPEVENTU3EC__ANONSTOREY6_T3827711990_H
#define U3CUNREGISTERTAPEVENTU3EC__ANONSTOREY6_T3827711990_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Utilities.TouchEventManager/<UnregisterTapEvent>c__AnonStorey6
struct  U3CUnregisterTapEventU3Ec__AnonStorey6_t3827711990  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Utilities.TouchEventManager/<UnregisterTapEvent>c__AnonStorey6::callback
	String_t* ___callback_0;
	// System.Int32 IBM.Watson.DeveloperCloud.Utilities.TouchEventManager/<UnregisterTapEvent>c__AnonStorey6::SortingLayer
	int32_t ___SortingLayer_1;
	// System.Boolean IBM.Watson.DeveloperCloud.Utilities.TouchEventManager/<UnregisterTapEvent>c__AnonStorey6::isTapInside
	bool ___isTapInside_2;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CUnregisterTapEventU3Ec__AnonStorey6_t3827711990, ___callback_0)); }
	inline String_t* get_callback_0() const { return ___callback_0; }
	inline String_t** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(String_t* value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}

	inline static int32_t get_offset_of_SortingLayer_1() { return static_cast<int32_t>(offsetof(U3CUnregisterTapEventU3Ec__AnonStorey6_t3827711990, ___SortingLayer_1)); }
	inline int32_t get_SortingLayer_1() const { return ___SortingLayer_1; }
	inline int32_t* get_address_of_SortingLayer_1() { return &___SortingLayer_1; }
	inline void set_SortingLayer_1(int32_t value)
	{
		___SortingLayer_1 = value;
	}

	inline static int32_t get_offset_of_isTapInside_2() { return static_cast<int32_t>(offsetof(U3CUnregisterTapEventU3Ec__AnonStorey6_t3827711990, ___isTapInside_2)); }
	inline bool get_isTapInside_2() const { return ___isTapInside_2; }
	inline bool* get_address_of_isTapInside_2() { return &___isTapInside_2; }
	inline void set_isTapInside_2(bool value)
	{
		___isTapInside_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUNREGISTERTAPEVENTU3EC__ANONSTOREY6_T3827711990_H
#ifndef U3CUNREGISTERTAPEVENTU3EC__ANONSTOREY7_T2261628049_H
#define U3CUNREGISTERTAPEVENTU3EC__ANONSTOREY7_T2261628049_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Utilities.TouchEventManager/<UnregisterTapEvent>c__AnonStorey7
struct  U3CUnregisterTapEventU3Ec__AnonStorey7_t2261628049  : public RuntimeObject
{
public:
	// UnityEngine.Collider2D IBM.Watson.DeveloperCloud.Utilities.TouchEventManager/<UnregisterTapEvent>c__AnonStorey7::itemCollider2D
	Collider2D_t646061738 * ___itemCollider2D_0;
	// IBM.Watson.DeveloperCloud.Utilities.TouchEventManager/<UnregisterTapEvent>c__AnonStorey6 IBM.Watson.DeveloperCloud.Utilities.TouchEventManager/<UnregisterTapEvent>c__AnonStorey7::<>f__ref$6
	U3CUnregisterTapEventU3Ec__AnonStorey6_t3827711990 * ___U3CU3Ef__refU246_1;

public:
	inline static int32_t get_offset_of_itemCollider2D_0() { return static_cast<int32_t>(offsetof(U3CUnregisterTapEventU3Ec__AnonStorey7_t2261628049, ___itemCollider2D_0)); }
	inline Collider2D_t646061738 * get_itemCollider2D_0() const { return ___itemCollider2D_0; }
	inline Collider2D_t646061738 ** get_address_of_itemCollider2D_0() { return &___itemCollider2D_0; }
	inline void set_itemCollider2D_0(Collider2D_t646061738 * value)
	{
		___itemCollider2D_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemCollider2D_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU246_1() { return static_cast<int32_t>(offsetof(U3CUnregisterTapEventU3Ec__AnonStorey7_t2261628049, ___U3CU3Ef__refU246_1)); }
	inline U3CUnregisterTapEventU3Ec__AnonStorey6_t3827711990 * get_U3CU3Ef__refU246_1() const { return ___U3CU3Ef__refU246_1; }
	inline U3CUnregisterTapEventU3Ec__AnonStorey6_t3827711990 ** get_address_of_U3CU3Ef__refU246_1() { return &___U3CU3Ef__refU246_1; }
	inline void set_U3CU3Ef__refU246_1(U3CUnregisterTapEventU3Ec__AnonStorey6_t3827711990 * value)
	{
		___U3CU3Ef__refU246_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU246_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUNREGISTERTAPEVENTU3EC__ANONSTOREY7_T2261628049_H
#ifndef CLASSIFYRESULTDATA_T1440000831_H
#define CLASSIFYRESULTDATA_T1440000831_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.DataTypes.ClassifyResultData
struct  ClassifyResultData_t1440000831  : public Data_t3166909639
{
public:
	// IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.ClassifyResult IBM.Watson.DeveloperCloud.DataTypes.ClassifyResultData::<Result>k__BackingField
	ClassifyResult_t2166103447 * ___U3CResultU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CResultU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ClassifyResultData_t1440000831, ___U3CResultU3Ek__BackingField_0)); }
	inline ClassifyResult_t2166103447 * get_U3CResultU3Ek__BackingField_0() const { return ___U3CResultU3Ek__BackingField_0; }
	inline ClassifyResult_t2166103447 ** get_address_of_U3CResultU3Ek__BackingField_0() { return &___U3CResultU3Ek__BackingField_0; }
	inline void set_U3CResultU3Ek__BackingField_0(ClassifyResult_t2166103447 * value)
	{
		___U3CResultU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CResultU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLASSIFYRESULTDATA_T1440000831_H
#ifndef AOTCOMPILATION_T21908242_H
#define AOTCOMPILATION_T21908242_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsAotCompilationManager/AotCompilation
struct  AotCompilation_t21908242 
{
public:
	// System.Type FullSerializer.fsAotCompilationManager/AotCompilation::Type
	Type_t * ___Type_0;
	// FullSerializer.Internal.fsMetaProperty[] FullSerializer.fsAotCompilationManager/AotCompilation::Members
	fsMetaPropertyU5BU5D_t4057973332* ___Members_1;
	// System.Boolean FullSerializer.fsAotCompilationManager/AotCompilation::IsConstructorPublic
	bool ___IsConstructorPublic_2;

public:
	inline static int32_t get_offset_of_Type_0() { return static_cast<int32_t>(offsetof(AotCompilation_t21908242, ___Type_0)); }
	inline Type_t * get_Type_0() const { return ___Type_0; }
	inline Type_t ** get_address_of_Type_0() { return &___Type_0; }
	inline void set_Type_0(Type_t * value)
	{
		___Type_0 = value;
		Il2CppCodeGenWriteBarrier((&___Type_0), value);
	}

	inline static int32_t get_offset_of_Members_1() { return static_cast<int32_t>(offsetof(AotCompilation_t21908242, ___Members_1)); }
	inline fsMetaPropertyU5BU5D_t4057973332* get_Members_1() const { return ___Members_1; }
	inline fsMetaPropertyU5BU5D_t4057973332** get_address_of_Members_1() { return &___Members_1; }
	inline void set_Members_1(fsMetaPropertyU5BU5D_t4057973332* value)
	{
		___Members_1 = value;
		Il2CppCodeGenWriteBarrier((&___Members_1), value);
	}

	inline static int32_t get_offset_of_IsConstructorPublic_2() { return static_cast<int32_t>(offsetof(AotCompilation_t21908242, ___IsConstructorPublic_2)); }
	inline bool get_IsConstructorPublic_2() const { return ___IsConstructorPublic_2; }
	inline bool* get_address_of_IsConstructorPublic_2() { return &___IsConstructorPublic_2; }
	inline void set_IsConstructorPublic_2(bool value)
	{
		___IsConstructorPublic_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of FullSerializer.fsAotCompilationManager/AotCompilation
struct AotCompilation_t21908242_marshaled_pinvoke
{
	Type_t * ___Type_0;
	fsMetaPropertyU5BU5D_t4057973332* ___Members_1;
	int32_t ___IsConstructorPublic_2;
};
// Native definition for COM marshalling of FullSerializer.fsAotCompilationManager/AotCompilation
struct AotCompilation_t21908242_marshaled_com
{
	Type_t * ___Type_0;
	fsMetaPropertyU5BU5D_t4057973332* ___Members_1;
	int32_t ___IsConstructorPublic_2;
};
#endif // AOTCOMPILATION_T21908242_H
#ifndef COLOR_T2020392075_H
#define COLOR_T2020392075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2020392075 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2020392075_H
#ifndef FSCONVERTER_T466758137_H
#define FSCONVERTER_T466758137_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsConverter
struct  fsConverter_t466758137  : public fsBaseConverter_t1241677426
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSCONVERTER_T466758137_H
#ifndef LAYERMASK_T3188175821_H
#define LAYERMASK_T3188175821_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LayerMask
struct  LayerMask_t3188175821 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t3188175821, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_T3188175821_H
#ifndef FSFORWARDATTRIBUTE_T3349051188_H
#define FSFORWARDATTRIBUTE_T3349051188_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsForwardAttribute
struct  fsForwardAttribute_t3349051188  : public Attribute_t542643598
{
public:
	// System.String FullSerializer.fsForwardAttribute::MemberName
	String_t* ___MemberName_0;

public:
	inline static int32_t get_offset_of_MemberName_0() { return static_cast<int32_t>(offsetof(fsForwardAttribute_t3349051188, ___MemberName_0)); }
	inline String_t* get_MemberName_0() const { return ___MemberName_0; }
	inline String_t** get_address_of_MemberName_0() { return &___MemberName_0; }
	inline void set_MemberName_0(String_t* value)
	{
		___MemberName_0 = value;
		Il2CppCodeGenWriteBarrier((&___MemberName_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSFORWARDATTRIBUTE_T3349051188_H
#ifndef VOID_T1841601450_H
#define VOID_T1841601450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1841601450 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1841601450_H
#ifndef VECTOR3_T2243707580_H
#define VECTOR3_T2243707580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t2243707580 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t2243707580_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t2243707580  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t2243707580  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t2243707580  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t2243707580  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t2243707580  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t2243707580  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t2243707580  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t2243707580  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t2243707580  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t2243707580  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___zeroVector_4)); }
	inline Vector3_t2243707580  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t2243707580 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t2243707580  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___oneVector_5)); }
	inline Vector3_t2243707580  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t2243707580 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t2243707580  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___upVector_6)); }
	inline Vector3_t2243707580  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t2243707580 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t2243707580  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___downVector_7)); }
	inline Vector3_t2243707580  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t2243707580 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t2243707580  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___leftVector_8)); }
	inline Vector3_t2243707580  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t2243707580 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t2243707580  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___rightVector_9)); }
	inline Vector3_t2243707580  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t2243707580 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t2243707580  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___forwardVector_10)); }
	inline Vector3_t2243707580  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t2243707580 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t2243707580  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___backVector_11)); }
	inline Vector3_t2243707580  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t2243707580 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t2243707580  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t2243707580  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t2243707580 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t2243707580  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t2243707580  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t2243707580 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t2243707580  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T2243707580_H
#ifndef VECTOR2_T2243707579_H
#define VECTOR2_T2243707579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2243707579 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2243707579, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2243707579, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2243707579_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2243707579  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2243707579  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2243707579  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2243707579  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2243707579  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2243707579  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2243707579  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2243707579  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2243707579  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2243707579 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2243707579  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___oneVector_3)); }
	inline Vector2_t2243707579  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2243707579 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2243707579  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___upVector_4)); }
	inline Vector2_t2243707579  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2243707579 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2243707579  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___downVector_5)); }
	inline Vector2_t2243707579  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2243707579 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2243707579  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___leftVector_6)); }
	inline Vector2_t2243707579  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2243707579 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2243707579  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___rightVector_7)); }
	inline Vector2_t2243707579  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2243707579 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2243707579  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2243707579  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2243707579 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2243707579  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2243707579  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2243707579 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2243707579  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2243707579_H
#ifndef WAV_PCM_T2882996802_H
#define WAV_PCM_T2882996802_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Utilities.WaveFile/WAV_PCM
#pragma pack(push, tp, 1)
struct  WAV_PCM_t2882996802 
{
public:
	// System.UInt16 IBM.Watson.DeveloperCloud.Utilities.WaveFile/WAV_PCM::format_tag
	uint16_t ___format_tag_0;
	// System.UInt16 IBM.Watson.DeveloperCloud.Utilities.WaveFile/WAV_PCM::channels
	uint16_t ___channels_1;
	// System.UInt32 IBM.Watson.DeveloperCloud.Utilities.WaveFile/WAV_PCM::sample_rate
	uint32_t ___sample_rate_2;
	// System.UInt32 IBM.Watson.DeveloperCloud.Utilities.WaveFile/WAV_PCM::average_data_rate
	uint32_t ___average_data_rate_3;
	// System.UInt16 IBM.Watson.DeveloperCloud.Utilities.WaveFile/WAV_PCM::alignment
	uint16_t ___alignment_4;
	// System.UInt16 IBM.Watson.DeveloperCloud.Utilities.WaveFile/WAV_PCM::bits_per_sample
	uint16_t ___bits_per_sample_5;

public:
	inline static int32_t get_offset_of_format_tag_0() { return static_cast<int32_t>(offsetof(WAV_PCM_t2882996802, ___format_tag_0)); }
	inline uint16_t get_format_tag_0() const { return ___format_tag_0; }
	inline uint16_t* get_address_of_format_tag_0() { return &___format_tag_0; }
	inline void set_format_tag_0(uint16_t value)
	{
		___format_tag_0 = value;
	}

	inline static int32_t get_offset_of_channels_1() { return static_cast<int32_t>(offsetof(WAV_PCM_t2882996802, ___channels_1)); }
	inline uint16_t get_channels_1() const { return ___channels_1; }
	inline uint16_t* get_address_of_channels_1() { return &___channels_1; }
	inline void set_channels_1(uint16_t value)
	{
		___channels_1 = value;
	}

	inline static int32_t get_offset_of_sample_rate_2() { return static_cast<int32_t>(offsetof(WAV_PCM_t2882996802, ___sample_rate_2)); }
	inline uint32_t get_sample_rate_2() const { return ___sample_rate_2; }
	inline uint32_t* get_address_of_sample_rate_2() { return &___sample_rate_2; }
	inline void set_sample_rate_2(uint32_t value)
	{
		___sample_rate_2 = value;
	}

	inline static int32_t get_offset_of_average_data_rate_3() { return static_cast<int32_t>(offsetof(WAV_PCM_t2882996802, ___average_data_rate_3)); }
	inline uint32_t get_average_data_rate_3() const { return ___average_data_rate_3; }
	inline uint32_t* get_address_of_average_data_rate_3() { return &___average_data_rate_3; }
	inline void set_average_data_rate_3(uint32_t value)
	{
		___average_data_rate_3 = value;
	}

	inline static int32_t get_offset_of_alignment_4() { return static_cast<int32_t>(offsetof(WAV_PCM_t2882996802, ___alignment_4)); }
	inline uint16_t get_alignment_4() const { return ___alignment_4; }
	inline uint16_t* get_address_of_alignment_4() { return &___alignment_4; }
	inline void set_alignment_4(uint16_t value)
	{
		___alignment_4 = value;
	}

	inline static int32_t get_offset_of_bits_per_sample_5() { return static_cast<int32_t>(offsetof(WAV_PCM_t2882996802, ___bits_per_sample_5)); }
	inline uint16_t get_bits_per_sample_5() const { return ___bits_per_sample_5; }
	inline uint16_t* get_address_of_bits_per_sample_5() { return &___bits_per_sample_5; }
	inline void set_bits_per_sample_5(uint16_t value)
	{
		___bits_per_sample_5 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAV_PCM_T2882996802_H
#ifndef TEXTTOSPEECHDATA_T1402652982_H
#define TEXTTOSPEECHDATA_T1402652982_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.DataTypes.TextToSpeechData
struct  TextToSpeechData_t1402652982  : public Data_t3166909639
{
public:
	// System.String IBM.Watson.DeveloperCloud.DataTypes.TextToSpeechData::<Text>k__BackingField
	String_t* ___U3CTextU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CTextU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TextToSpeechData_t1402652982, ___U3CTextU3Ek__BackingField_0)); }
	inline String_t* get_U3CTextU3Ek__BackingField_0() const { return ___U3CTextU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CTextU3Ek__BackingField_0() { return &___U3CTextU3Ek__BackingField_0; }
	inline void set_U3CTextU3Ek__BackingField_0(String_t* value)
	{
		___U3CTextU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTextU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTTOSPEECHDATA_T1402652982_H
#ifndef WEBCAMTEXTUREDATA_T1034677858_H
#define WEBCAMTEXTUREDATA_T1034677858_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.DataTypes.WebCamTextureData
struct  WebCamTextureData_t1034677858  : public Data_t3166909639
{
public:
	// UnityEngine.WebCamTexture IBM.Watson.DeveloperCloud.DataTypes.WebCamTextureData::<CamTexture>k__BackingField
	WebCamTexture_t1079476942 * ___U3CCamTextureU3Ek__BackingField_0;
	// System.Int32 IBM.Watson.DeveloperCloud.DataTypes.WebCamTextureData::<RequestedWidth>k__BackingField
	int32_t ___U3CRequestedWidthU3Ek__BackingField_1;
	// System.Int32 IBM.Watson.DeveloperCloud.DataTypes.WebCamTextureData::<RequestedHeight>k__BackingField
	int32_t ___U3CRequestedHeightU3Ek__BackingField_2;
	// System.Int32 IBM.Watson.DeveloperCloud.DataTypes.WebCamTextureData::<RequestedFPS>k__BackingField
	int32_t ___U3CRequestedFPSU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CCamTextureU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(WebCamTextureData_t1034677858, ___U3CCamTextureU3Ek__BackingField_0)); }
	inline WebCamTexture_t1079476942 * get_U3CCamTextureU3Ek__BackingField_0() const { return ___U3CCamTextureU3Ek__BackingField_0; }
	inline WebCamTexture_t1079476942 ** get_address_of_U3CCamTextureU3Ek__BackingField_0() { return &___U3CCamTextureU3Ek__BackingField_0; }
	inline void set_U3CCamTextureU3Ek__BackingField_0(WebCamTexture_t1079476942 * value)
	{
		___U3CCamTextureU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCamTextureU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CRequestedWidthU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(WebCamTextureData_t1034677858, ___U3CRequestedWidthU3Ek__BackingField_1)); }
	inline int32_t get_U3CRequestedWidthU3Ek__BackingField_1() const { return ___U3CRequestedWidthU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CRequestedWidthU3Ek__BackingField_1() { return &___U3CRequestedWidthU3Ek__BackingField_1; }
	inline void set_U3CRequestedWidthU3Ek__BackingField_1(int32_t value)
	{
		___U3CRequestedWidthU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CRequestedHeightU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(WebCamTextureData_t1034677858, ___U3CRequestedHeightU3Ek__BackingField_2)); }
	inline int32_t get_U3CRequestedHeightU3Ek__BackingField_2() const { return ___U3CRequestedHeightU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CRequestedHeightU3Ek__BackingField_2() { return &___U3CRequestedHeightU3Ek__BackingField_2; }
	inline void set_U3CRequestedHeightU3Ek__BackingField_2(int32_t value)
	{
		___U3CRequestedHeightU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CRequestedFPSU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(WebCamTextureData_t1034677858, ___U3CRequestedFPSU3Ek__BackingField_3)); }
	inline int32_t get_U3CRequestedFPSU3Ek__BackingField_3() const { return ___U3CRequestedFPSU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CRequestedFPSU3Ek__BackingField_3() { return &___U3CRequestedFPSU3Ek__BackingField_3; }
	inline void set_U3CRequestedFPSU3Ek__BackingField_3(int32_t value)
	{
		___U3CRequestedFPSU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBCAMTEXTUREDATA_T1034677858_H
#ifndef AUDIODATA_T3894398524_H
#define AUDIODATA_T3894398524_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.DataTypes.AudioData
struct  AudioData_t3894398524  : public Data_t3166909639
{
public:
	// UnityEngine.AudioClip IBM.Watson.DeveloperCloud.DataTypes.AudioData::<Clip>k__BackingField
	AudioClip_t1932558630 * ___U3CClipU3Ek__BackingField_0;
	// System.Single IBM.Watson.DeveloperCloud.DataTypes.AudioData::<MaxLevel>k__BackingField
	float ___U3CMaxLevelU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CClipU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AudioData_t3894398524, ___U3CClipU3Ek__BackingField_0)); }
	inline AudioClip_t1932558630 * get_U3CClipU3Ek__BackingField_0() const { return ___U3CClipU3Ek__BackingField_0; }
	inline AudioClip_t1932558630 ** get_address_of_U3CClipU3Ek__BackingField_0() { return &___U3CClipU3Ek__BackingField_0; }
	inline void set_U3CClipU3Ek__BackingField_0(AudioClip_t1932558630 * value)
	{
		___U3CClipU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CClipU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CMaxLevelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AudioData_t3894398524, ___U3CMaxLevelU3Ek__BackingField_1)); }
	inline float get_U3CMaxLevelU3Ek__BackingField_1() const { return ___U3CMaxLevelU3Ek__BackingField_1; }
	inline float* get_address_of_U3CMaxLevelU3Ek__BackingField_1() { return &___U3CMaxLevelU3Ek__BackingField_1; }
	inline void set_U3CMaxLevelU3Ek__BackingField_1(float value)
	{
		___U3CMaxLevelU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIODATA_T3894398524_H
#ifndef LANGUAGEDATA_T1599146524_H
#define LANGUAGEDATA_T1599146524_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.DataTypes.LanguageData
struct  LanguageData_t1599146524  : public Data_t3166909639
{
public:
	// System.String IBM.Watson.DeveloperCloud.DataTypes.LanguageData::<Language>k__BackingField
	String_t* ___U3CLanguageU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CLanguageU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(LanguageData_t1599146524, ___U3CLanguageU3Ek__BackingField_0)); }
	inline String_t* get_U3CLanguageU3Ek__BackingField_0() const { return ___U3CLanguageU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CLanguageU3Ek__BackingField_0() { return &___U3CLanguageU3Ek__BackingField_0; }
	inline void set_U3CLanguageU3Ek__BackingField_0(String_t* value)
	{
		___U3CLanguageU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLanguageU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LANGUAGEDATA_T1599146524_H
#ifndef TIMESPAN_T3430258949_H
#define TIMESPAN_T3430258949_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t3430258949 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_8;

public:
	inline static int32_t get_offset_of__ticks_8() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949, ____ticks_8)); }
	inline int64_t get__ticks_8() const { return ____ticks_8; }
	inline int64_t* get_address_of__ticks_8() { return &____ticks_8; }
	inline void set__ticks_8(int64_t value)
	{
		____ticks_8 = value;
	}
};

struct TimeSpan_t3430258949_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t3430258949  ___MaxValue_5;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t3430258949  ___MinValue_6;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t3430258949  ___Zero_7;

public:
	inline static int32_t get_offset_of_MaxValue_5() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949_StaticFields, ___MaxValue_5)); }
	inline TimeSpan_t3430258949  get_MaxValue_5() const { return ___MaxValue_5; }
	inline TimeSpan_t3430258949 * get_address_of_MaxValue_5() { return &___MaxValue_5; }
	inline void set_MaxValue_5(TimeSpan_t3430258949  value)
	{
		___MaxValue_5 = value;
	}

	inline static int32_t get_offset_of_MinValue_6() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949_StaticFields, ___MinValue_6)); }
	inline TimeSpan_t3430258949  get_MinValue_6() const { return ___MinValue_6; }
	inline TimeSpan_t3430258949 * get_address_of_MinValue_6() { return &___MinValue_6; }
	inline void set_MinValue_6(TimeSpan_t3430258949  value)
	{
		___MinValue_6 = value;
	}

	inline static int32_t get_offset_of_Zero_7() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949_StaticFields, ___Zero_7)); }
	inline TimeSpan_t3430258949  get_Zero_7() const { return ___Zero_7; }
	inline TimeSpan_t3430258949 * get_address_of_Zero_7() { return &___Zero_7; }
	inline void set_Zero_7(TimeSpan_t3430258949  value)
	{
		___Zero_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T3430258949_H
#ifndef FSDIRECTCONVERTER_T763460818_H
#define FSDIRECTCONVERTER_T763460818_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsDirectConverter
struct  fsDirectConverter_t763460818  : public fsBaseConverter_t1241677426
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSDIRECTCONVERTER_T763460818_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	IntPtr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline IntPtr_t get_Zero_1() const { return ___Zero_1; }
	inline IntPtr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(IntPtr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t1328083999* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t1328083999* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t1328083999** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t1328083999* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef IFF_CHUNK_T4256546638_H
#define IFF_CHUNK_T4256546638_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Utilities.WaveFile/IFF_CHUNK
#pragma pack(push, tp, 1)
struct  IFF_CHUNK_t4256546638 
{
public:
	// System.UInt32 IBM.Watson.DeveloperCloud.Utilities.WaveFile/IFF_CHUNK::id
	uint32_t ___id_0;
	// System.UInt32 IBM.Watson.DeveloperCloud.Utilities.WaveFile/IFF_CHUNK::length
	uint32_t ___length_1;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(IFF_CHUNK_t4256546638, ___id_0)); }
	inline uint32_t get_id_0() const { return ___id_0; }
	inline uint32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(uint32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_length_1() { return static_cast<int32_t>(offsetof(IFF_CHUNK_t4256546638, ___length_1)); }
	inline uint32_t get_length_1() const { return ___length_1; }
	inline uint32_t* get_address_of_length_1() { return &___length_1; }
	inline void set_length_1(uint32_t value)
	{
		___length_1 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IFF_CHUNK_T4256546638_H
#ifndef IFF_FORM_CHUNK_T624775701_H
#define IFF_FORM_CHUNK_T624775701_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Utilities.WaveFile/IFF_FORM_CHUNK
#pragma pack(push, tp, 1)
struct  IFF_FORM_CHUNK_t624775701 
{
public:
	// System.UInt32 IBM.Watson.DeveloperCloud.Utilities.WaveFile/IFF_FORM_CHUNK::form_id
	uint32_t ___form_id_0;
	// System.UInt32 IBM.Watson.DeveloperCloud.Utilities.WaveFile/IFF_FORM_CHUNK::form_length
	uint32_t ___form_length_1;
	// System.UInt32 IBM.Watson.DeveloperCloud.Utilities.WaveFile/IFF_FORM_CHUNK::id
	uint32_t ___id_2;

public:
	inline static int32_t get_offset_of_form_id_0() { return static_cast<int32_t>(offsetof(IFF_FORM_CHUNK_t624775701, ___form_id_0)); }
	inline uint32_t get_form_id_0() const { return ___form_id_0; }
	inline uint32_t* get_address_of_form_id_0() { return &___form_id_0; }
	inline void set_form_id_0(uint32_t value)
	{
		___form_id_0 = value;
	}

	inline static int32_t get_offset_of_form_length_1() { return static_cast<int32_t>(offsetof(IFF_FORM_CHUNK_t624775701, ___form_length_1)); }
	inline uint32_t get_form_length_1() const { return ___form_length_1; }
	inline uint32_t* get_address_of_form_length_1() { return &___form_length_1; }
	inline void set_form_length_1(uint32_t value)
	{
		___form_length_1 = value;
	}

	inline static int32_t get_offset_of_id_2() { return static_cast<int32_t>(offsetof(IFF_FORM_CHUNK_t624775701, ___id_2)); }
	inline uint32_t get_id_2() const { return ___id_2; }
	inline uint32_t* get_address_of_id_2() { return &___id_2; }
	inline void set_id_2(uint32_t value)
	{
		___id_2 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IFF_FORM_CHUNK_T624775701_H
#ifndef WATSONEXCEPTION_T1186470237_H
#define WATSONEXCEPTION_T1186470237_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Utilities.WatsonException
struct  WatsonException_t1186470237  : public Exception_t1927440687
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WATSONEXCEPTION_T1186470237_H
#ifndef SPEAKINGSTATEDATA_T3706378241_H
#define SPEAKINGSTATEDATA_T3706378241_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.DataTypes.SpeakingStateData
struct  SpeakingStateData_t3706378241  : public Data_t3166909639
{
public:
	// System.Boolean IBM.Watson.DeveloperCloud.DataTypes.SpeakingStateData::<Boolean>k__BackingField
	bool ___U3CBooleanU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CBooleanU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SpeakingStateData_t3706378241, ___U3CBooleanU3Ek__BackingField_0)); }
	inline bool get_U3CBooleanU3Ek__BackingField_0() const { return ___U3CBooleanU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CBooleanU3Ek__BackingField_0() { return &___U3CBooleanU3Ek__BackingField_0; }
	inline void set_U3CBooleanU3Ek__BackingField_0(bool value)
	{
		___U3CBooleanU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPEAKINGSTATEDATA_T3706378241_H
#ifndef DISABLEMICDATA_T3343074493_H
#define DISABLEMICDATA_T3343074493_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.DataTypes.DisableMicData
struct  DisableMicData_t3343074493  : public Data_t3166909639
{
public:
	// System.Boolean IBM.Watson.DeveloperCloud.DataTypes.DisableMicData::<Boolean>k__BackingField
	bool ___U3CBooleanU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CBooleanU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DisableMicData_t3343074493, ___U3CBooleanU3Ek__BackingField_0)); }
	inline bool get_U3CBooleanU3Ek__BackingField_0() const { return ___U3CBooleanU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CBooleanU3Ek__BackingField_0() { return &___U3CBooleanU3Ek__BackingField_0; }
	inline void set_U3CBooleanU3Ek__BackingField_0(bool value)
	{
		___U3CBooleanU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISABLEMICDATA_T3343074493_H
#ifndef DISABLEWEBCAMDATA_T2549114729_H
#define DISABLEWEBCAMDATA_T2549114729_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.DataTypes.DisableWebCamData
struct  DisableWebCamData_t2549114729  : public Data_t3166909639
{
public:
	// System.Boolean IBM.Watson.DeveloperCloud.DataTypes.DisableWebCamData::<Boolean>k__BackingField
	bool ___U3CBooleanU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CBooleanU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DisableWebCamData_t2549114729, ___U3CBooleanU3Ek__BackingField_0)); }
	inline bool get_U3CBooleanU3Ek__BackingField_0() const { return ___U3CBooleanU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CBooleanU3Ek__BackingField_0() { return &___U3CBooleanU3Ek__BackingField_0; }
	inline void set_U3CBooleanU3Ek__BackingField_0(bool value)
	{
		___U3CBooleanU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISABLEWEBCAMDATA_T2549114729_H
#ifndef LEVELDATA_T2457055884_H
#define LEVELDATA_T2457055884_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.DataTypes.LevelData
struct  LevelData_t2457055884  : public Data_t3166909639
{
public:
	// System.Single IBM.Watson.DeveloperCloud.DataTypes.LevelData::<Float>k__BackingField
	float ___U3CFloatU3Ek__BackingField_0;
	// System.Single IBM.Watson.DeveloperCloud.DataTypes.LevelData::<Modifier>k__BackingField
	float ___U3CModifierU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CFloatU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(LevelData_t2457055884, ___U3CFloatU3Ek__BackingField_0)); }
	inline float get_U3CFloatU3Ek__BackingField_0() const { return ___U3CFloatU3Ek__BackingField_0; }
	inline float* get_address_of_U3CFloatU3Ek__BackingField_0() { return &___U3CFloatU3Ek__BackingField_0; }
	inline void set_U3CFloatU3Ek__BackingField_0(float value)
	{
		___U3CFloatU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CModifierU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(LevelData_t2457055884, ___U3CModifierU3Ek__BackingField_1)); }
	inline float get_U3CModifierU3Ek__BackingField_1() const { return ___U3CModifierU3Ek__BackingField_1; }
	inline float* get_address_of_U3CModifierU3Ek__BackingField_1() { return &___U3CModifierU3Ek__BackingField_1; }
	inline void set_U3CModifierU3Ek__BackingField_1(float value)
	{
		___U3CModifierU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELDATA_T2457055884_H
#ifndef DISABLECLOSECAPTIONDATA_T3291053220_H
#define DISABLECLOSECAPTIONDATA_T3291053220_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.DataTypes.DisableCloseCaptionData
struct  DisableCloseCaptionData_t3291053220  : public Data_t3166909639
{
public:
	// System.Boolean IBM.Watson.DeveloperCloud.DataTypes.DisableCloseCaptionData::<Boolean>k__BackingField
	bool ___U3CBooleanU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CBooleanU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DisableCloseCaptionData_t3291053220, ___U3CBooleanU3Ek__BackingField_0)); }
	inline bool get_U3CBooleanU3Ek__BackingField_0() const { return ___U3CBooleanU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CBooleanU3Ek__BackingField_0() { return &___U3CBooleanU3Ek__BackingField_0; }
	inline void set_U3CBooleanU3Ek__BackingField_0(bool value)
	{
		___U3CBooleanU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISABLECLOSECAPTIONDATA_T3291053220_H
#ifndef BOOLEANDATA_T512765254_H
#define BOOLEANDATA_T512765254_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.DataTypes.BooleanData
struct  BooleanData_t512765254  : public Data_t3166909639
{
public:
	// System.Boolean IBM.Watson.DeveloperCloud.DataTypes.BooleanData::<Boolean>k__BackingField
	bool ___U3CBooleanU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CBooleanU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(BooleanData_t512765254, ___U3CBooleanU3Ek__BackingField_0)); }
	inline bool get_U3CBooleanU3Ek__BackingField_0() const { return ___U3CBooleanU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CBooleanU3Ek__BackingField_0() { return &___U3CBooleanU3Ek__BackingField_0; }
	inline void set_U3CBooleanU3Ek__BackingField_0(bool value)
	{
		___U3CBooleanU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEANDATA_T512765254_H
#ifndef SPEECHTOTEXTDATA_T1322365462_H
#define SPEECHTOTEXTDATA_T1322365462_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.DataTypes.SpeechToTextData
struct  SpeechToTextData_t1322365462  : public Data_t3166909639
{
public:
	// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechRecognitionEvent IBM.Watson.DeveloperCloud.DataTypes.SpeechToTextData::<Results>k__BackingField
	SpeechRecognitionEvent_t1591882439 * ___U3CResultsU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.DataTypes.SpeechToTextData::_Text
	String_t* ____Text_1;

public:
	inline static int32_t get_offset_of_U3CResultsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SpeechToTextData_t1322365462, ___U3CResultsU3Ek__BackingField_0)); }
	inline SpeechRecognitionEvent_t1591882439 * get_U3CResultsU3Ek__BackingField_0() const { return ___U3CResultsU3Ek__BackingField_0; }
	inline SpeechRecognitionEvent_t1591882439 ** get_address_of_U3CResultsU3Ek__BackingField_0() { return &___U3CResultsU3Ek__BackingField_0; }
	inline void set_U3CResultsU3Ek__BackingField_0(SpeechRecognitionEvent_t1591882439 * value)
	{
		___U3CResultsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CResultsU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of__Text_1() { return static_cast<int32_t>(offsetof(SpeechToTextData_t1322365462, ____Text_1)); }
	inline String_t* get__Text_1() const { return ____Text_1; }
	inline String_t** get_address_of__Text_1() { return &____Text_1; }
	inline void set__Text_1(String_t* value)
	{
		____Text_1 = value;
		Il2CppCodeGenWriteBarrier((&____Text_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPEECHTOTEXTDATA_T1322365462_H
#ifndef FSDIRECTCONVERTER_1_T837437498_H
#define FSDIRECTCONVERTER_1_T837437498_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsDirectConverter`1<UnityEngine.Gradient>
struct  fsDirectConverter_1_t837437498  : public fsDirectConverter_t763460818
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSDIRECTCONVERTER_1_T837437498_H
#ifndef FSDIRECTCONVERTER_1_T2981293126_H
#define FSDIRECTCONVERTER_1_T2981293126_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsDirectConverter`1<UnityEngine.Keyframe>
struct  fsDirectConverter_1_t2981293126  : public fsDirectConverter_t763460818
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSDIRECTCONVERTER_1_T2981293126_H
#ifndef FSTYPECONVERTER_T3442245107_H
#define FSTYPECONVERTER_T3442245107_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.Internal.fsTypeConverter
struct  fsTypeConverter_t3442245107  : public fsConverter_t466758137
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSTYPECONVERTER_T3442245107_H
#ifndef FSREFLECTEDCONVERTER_T171754893_H
#define FSREFLECTEDCONVERTER_T171754893_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.Internal.fsReflectedConverter
struct  fsReflectedConverter_t171754893  : public fsConverter_t466758137
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSREFLECTEDCONVERTER_T171754893_H
#ifndef FSWEAKREFERENCECONVERTER_T940327902_H
#define FSWEAKREFERENCECONVERTER_T940327902_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.Internal.fsWeakReferenceConverter
struct  fsWeakReferenceConverter_t940327902  : public fsConverter_t466758137
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSWEAKREFERENCECONVERTER_T940327902_H
#ifndef DATETIMEKIND_T2186819611_H
#define DATETIMEKIND_T2186819611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeKind
struct  DateTimeKind_t2186819611 
{
public:
	// System.Int32 System.DateTimeKind::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeKind_t2186819611, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEKIND_T2186819611_H
#ifndef FSDIRECTCONVERTER_1_T425030311_H
#define FSDIRECTCONVERTER_1_T425030311_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsDirectConverter`1<UnityEngine.LayerMask>
struct  fsDirectConverter_1_t425030311  : public fsDirectConverter_t763460818
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSDIRECTCONVERTER_1_T425030311_H
#ifndef KEYMODIFIERS_T231190421_H
#define KEYMODIFIERS_T231190421_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Utilities.KeyModifiers
struct  KeyModifiers_t231190421 
{
public:
	// System.Int32 IBM.Watson.DeveloperCloud.Utilities.KeyModifiers::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(KeyModifiers_t231190421, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYMODIFIERS_T231190421_H
#ifndef FSDIRECTCONVERTER_1_T918610116_H
#define FSDIRECTCONVERTER_1_T918610116_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsDirectConverter`1<UnityEngine.Rect>
struct  fsDirectConverter_1_t918610116  : public fsDirectConverter_t763460818
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSDIRECTCONVERTER_1_T918610116_H
#ifndef KEYCODE_T2283395152_H
#define KEYCODE_T2283395152_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.KeyCode
struct  KeyCode_t2283395152 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(KeyCode_t2283395152, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCODE_T2283395152_H
#ifndef VOICETYPE_T981025524_H
#define VOICETYPE_T981025524_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.VoiceType
struct  VoiceType_t981025524 
{
public:
	// System.Int32 IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.VoiceType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(VoiceType_t981025524, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOICETYPE_T981025524_H
#ifndef RAYCASTRESULT_T21186376_H
#define RAYCASTRESULT_T21186376_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.RaycastResult
struct  RaycastResult_t21186376 
{
public:
	// UnityEngine.GameObject UnityEngine.EventSystems.RaycastResult::m_GameObject
	GameObject_t1756533147 * ___m_GameObject_0;
	// UnityEngine.EventSystems.BaseRaycaster UnityEngine.EventSystems.RaycastResult::module
	BaseRaycaster_t2336171397 * ___module_1;
	// System.Single UnityEngine.EventSystems.RaycastResult::distance
	float ___distance_2;
	// System.Single UnityEngine.EventSystems.RaycastResult::index
	float ___index_3;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::depth
	int32_t ___depth_4;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::sortingLayer
	int32_t ___sortingLayer_5;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::sortingOrder
	int32_t ___sortingOrder_6;
	// UnityEngine.Vector3 UnityEngine.EventSystems.RaycastResult::worldPosition
	Vector3_t2243707580  ___worldPosition_7;
	// UnityEngine.Vector3 UnityEngine.EventSystems.RaycastResult::worldNormal
	Vector3_t2243707580  ___worldNormal_8;
	// UnityEngine.Vector2 UnityEngine.EventSystems.RaycastResult::screenPosition
	Vector2_t2243707579  ___screenPosition_9;

public:
	inline static int32_t get_offset_of_m_GameObject_0() { return static_cast<int32_t>(offsetof(RaycastResult_t21186376, ___m_GameObject_0)); }
	inline GameObject_t1756533147 * get_m_GameObject_0() const { return ___m_GameObject_0; }
	inline GameObject_t1756533147 ** get_address_of_m_GameObject_0() { return &___m_GameObject_0; }
	inline void set_m_GameObject_0(GameObject_t1756533147 * value)
	{
		___m_GameObject_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_GameObject_0), value);
	}

	inline static int32_t get_offset_of_module_1() { return static_cast<int32_t>(offsetof(RaycastResult_t21186376, ___module_1)); }
	inline BaseRaycaster_t2336171397 * get_module_1() const { return ___module_1; }
	inline BaseRaycaster_t2336171397 ** get_address_of_module_1() { return &___module_1; }
	inline void set_module_1(BaseRaycaster_t2336171397 * value)
	{
		___module_1 = value;
		Il2CppCodeGenWriteBarrier((&___module_1), value);
	}

	inline static int32_t get_offset_of_distance_2() { return static_cast<int32_t>(offsetof(RaycastResult_t21186376, ___distance_2)); }
	inline float get_distance_2() const { return ___distance_2; }
	inline float* get_address_of_distance_2() { return &___distance_2; }
	inline void set_distance_2(float value)
	{
		___distance_2 = value;
	}

	inline static int32_t get_offset_of_index_3() { return static_cast<int32_t>(offsetof(RaycastResult_t21186376, ___index_3)); }
	inline float get_index_3() const { return ___index_3; }
	inline float* get_address_of_index_3() { return &___index_3; }
	inline void set_index_3(float value)
	{
		___index_3 = value;
	}

	inline static int32_t get_offset_of_depth_4() { return static_cast<int32_t>(offsetof(RaycastResult_t21186376, ___depth_4)); }
	inline int32_t get_depth_4() const { return ___depth_4; }
	inline int32_t* get_address_of_depth_4() { return &___depth_4; }
	inline void set_depth_4(int32_t value)
	{
		___depth_4 = value;
	}

	inline static int32_t get_offset_of_sortingLayer_5() { return static_cast<int32_t>(offsetof(RaycastResult_t21186376, ___sortingLayer_5)); }
	inline int32_t get_sortingLayer_5() const { return ___sortingLayer_5; }
	inline int32_t* get_address_of_sortingLayer_5() { return &___sortingLayer_5; }
	inline void set_sortingLayer_5(int32_t value)
	{
		___sortingLayer_5 = value;
	}

	inline static int32_t get_offset_of_sortingOrder_6() { return static_cast<int32_t>(offsetof(RaycastResult_t21186376, ___sortingOrder_6)); }
	inline int32_t get_sortingOrder_6() const { return ___sortingOrder_6; }
	inline int32_t* get_address_of_sortingOrder_6() { return &___sortingOrder_6; }
	inline void set_sortingOrder_6(int32_t value)
	{
		___sortingOrder_6 = value;
	}

	inline static int32_t get_offset_of_worldPosition_7() { return static_cast<int32_t>(offsetof(RaycastResult_t21186376, ___worldPosition_7)); }
	inline Vector3_t2243707580  get_worldPosition_7() const { return ___worldPosition_7; }
	inline Vector3_t2243707580 * get_address_of_worldPosition_7() { return &___worldPosition_7; }
	inline void set_worldPosition_7(Vector3_t2243707580  value)
	{
		___worldPosition_7 = value;
	}

	inline static int32_t get_offset_of_worldNormal_8() { return static_cast<int32_t>(offsetof(RaycastResult_t21186376, ___worldNormal_8)); }
	inline Vector3_t2243707580  get_worldNormal_8() const { return ___worldNormal_8; }
	inline Vector3_t2243707580 * get_address_of_worldNormal_8() { return &___worldNormal_8; }
	inline void set_worldNormal_8(Vector3_t2243707580  value)
	{
		___worldNormal_8 = value;
	}

	inline static int32_t get_offset_of_screenPosition_9() { return static_cast<int32_t>(offsetof(RaycastResult_t21186376, ___screenPosition_9)); }
	inline Vector2_t2243707579  get_screenPosition_9() const { return ___screenPosition_9; }
	inline Vector2_t2243707579 * get_address_of_screenPosition_9() { return &___screenPosition_9; }
	inline void set_screenPosition_9(Vector2_t2243707579  value)
	{
		___screenPosition_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.EventSystems.RaycastResult
struct RaycastResult_t21186376_marshaled_pinvoke
{
	GameObject_t1756533147 * ___m_GameObject_0;
	BaseRaycaster_t2336171397 * ___module_1;
	float ___distance_2;
	float ___index_3;
	int32_t ___depth_4;
	int32_t ___sortingLayer_5;
	int32_t ___sortingOrder_6;
	Vector3_t2243707580  ___worldPosition_7;
	Vector3_t2243707580  ___worldNormal_8;
	Vector2_t2243707579  ___screenPosition_9;
};
// Native definition for COM marshalling of UnityEngine.EventSystems.RaycastResult
struct RaycastResult_t21186376_marshaled_com
{
	GameObject_t1756533147 * ___m_GameObject_0;
	BaseRaycaster_t2336171397 * ___module_1;
	float ___distance_2;
	float ___index_3;
	int32_t ___depth_4;
	int32_t ___sortingLayer_5;
	int32_t ___sortingOrder_6;
	Vector3_t2243707580  ___worldPosition_7;
	Vector3_t2243707580  ___worldNormal_8;
	Vector2_t2243707579  ___screenPosition_9;
};
#endif // RAYCASTRESULT_T21186376_H
#ifndef FSDIRECTCONVERTER_1_T543395641_H
#define FSDIRECTCONVERTER_1_T543395641_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsDirectConverter`1<UnityEngine.AnimationCurve>
struct  fsDirectConverter_1_t543395641  : public fsDirectConverter_t763460818
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSDIRECTCONVERTER_1_T543395641_H
#ifndef FSDIRECTCONVERTER_1_T270218193_H
#define FSDIRECTCONVERTER_1_T270218193_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsDirectConverter`1<UnityEngine.Bounds>
struct  fsDirectConverter_1_t270218193  : public fsDirectConverter_t763460818
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSDIRECTCONVERTER_1_T270218193_H
#ifndef FSMEMBERSERIALIZATION_T691367231_H
#define FSMEMBERSERIALIZATION_T691367231_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsMemberSerialization
struct  fsMemberSerialization_t691367231 
{
public:
	// System.Int32 FullSerializer.fsMemberSerialization::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(fsMemberSerialization_t691367231, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSMEMBERSERIALIZATION_T691367231_H
#ifndef DELEGATE_T3022476291_H
#define DELEGATE_T3022476291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t3022476291  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	IntPtr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	IntPtr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	IntPtr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	IntPtr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1572802995 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___invoke_impl_1)); }
	inline IntPtr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline IntPtr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(IntPtr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_3)); }
	inline IntPtr_t get_method_3() const { return ___method_3; }
	inline IntPtr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(IntPtr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___delegate_trampoline_4)); }
	inline IntPtr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline IntPtr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(IntPtr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_code_5)); }
	inline IntPtr_t get_method_code_5() const { return ___method_code_5; }
	inline IntPtr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(IntPtr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___data_8)); }
	inline DelegateData_t1572802995 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1572802995 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1572802995 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T3022476291_H
#ifndef FSPRIMITIVECONVERTER_T3016566550_H
#define FSPRIMITIVECONVERTER_T3016566550_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.Internal.fsPrimitiveConverter
struct  fsPrimitiveConverter_t3016566550  : public fsConverter_t466758137
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSPRIMITIVECONVERTER_T3016566550_H
#ifndef FSDICTIONARYCONVERTER_T4168040623_H
#define FSDICTIONARYCONVERTER_T4168040623_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.Internal.fsDictionaryConverter
struct  fsDictionaryConverter_t4168040623  : public fsConverter_t466758137
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSDICTIONARYCONVERTER_T4168040623_H
#ifndef FSENUMCONVERTER_T1608130800_H
#define FSENUMCONVERTER_T1608130800_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.Internal.fsEnumConverter
struct  fsEnumConverter_t1608130800  : public fsConverter_t466758137
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSENUMCONVERTER_T1608130800_H
#ifndef FSFORWARDCONVERTER_T3272928430_H
#define FSFORWARDCONVERTER_T3272928430_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.Internal.fsForwardConverter
struct  fsForwardConverter_t3272928430  : public fsConverter_t466758137
{
public:
	// System.String FullSerializer.Internal.fsForwardConverter::_memberName
	String_t* ____memberName_2;

public:
	inline static int32_t get_offset_of__memberName_2() { return static_cast<int32_t>(offsetof(fsForwardConverter_t3272928430, ____memberName_2)); }
	inline String_t* get__memberName_2() const { return ____memberName_2; }
	inline String_t** get_address_of__memberName_2() { return &____memberName_2; }
	inline void set__memberName_2(String_t* value)
	{
		____memberName_2 = value;
		Il2CppCodeGenWriteBarrier((&____memberName_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSFORWARDCONVERTER_T3272928430_H
#ifndef FSDATECONVERTER_T3677588831_H
#define FSDATECONVERTER_T3677588831_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.Internal.fsDateConverter
struct  fsDateConverter_t3677588831  : public fsConverter_t466758137
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSDATECONVERTER_T3677588831_H
#ifndef TAPEVENTMAPPING_T2308161515_H
#define TAPEVENTMAPPING_T2308161515_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Widgets.TouchWidget/TapEventMapping
struct  TapEventMapping_t2308161515  : public RuntimeObject
{
public:
	// UnityEngine.GameObject IBM.Watson.DeveloperCloud.Widgets.TouchWidget/TapEventMapping::m_TapObject
	GameObject_t1756533147 * ___m_TapObject_0;
	// System.Boolean IBM.Watson.DeveloperCloud.Widgets.TouchWidget/TapEventMapping::m_TapOnObject
	bool ___m_TapOnObject_1;
	// System.Int32 IBM.Watson.DeveloperCloud.Widgets.TouchWidget/TapEventMapping::m_SortingLayer
	int32_t ___m_SortingLayer_2;
	// UnityEngine.LayerMask IBM.Watson.DeveloperCloud.Widgets.TouchWidget/TapEventMapping::m_LayerMask
	LayerMask_t3188175821  ___m_LayerMask_3;
	// System.String IBM.Watson.DeveloperCloud.Widgets.TouchWidget/TapEventMapping::m_Callback
	String_t* ___m_Callback_4;

public:
	inline static int32_t get_offset_of_m_TapObject_0() { return static_cast<int32_t>(offsetof(TapEventMapping_t2308161515, ___m_TapObject_0)); }
	inline GameObject_t1756533147 * get_m_TapObject_0() const { return ___m_TapObject_0; }
	inline GameObject_t1756533147 ** get_address_of_m_TapObject_0() { return &___m_TapObject_0; }
	inline void set_m_TapObject_0(GameObject_t1756533147 * value)
	{
		___m_TapObject_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_TapObject_0), value);
	}

	inline static int32_t get_offset_of_m_TapOnObject_1() { return static_cast<int32_t>(offsetof(TapEventMapping_t2308161515, ___m_TapOnObject_1)); }
	inline bool get_m_TapOnObject_1() const { return ___m_TapOnObject_1; }
	inline bool* get_address_of_m_TapOnObject_1() { return &___m_TapOnObject_1; }
	inline void set_m_TapOnObject_1(bool value)
	{
		___m_TapOnObject_1 = value;
	}

	inline static int32_t get_offset_of_m_SortingLayer_2() { return static_cast<int32_t>(offsetof(TapEventMapping_t2308161515, ___m_SortingLayer_2)); }
	inline int32_t get_m_SortingLayer_2() const { return ___m_SortingLayer_2; }
	inline int32_t* get_address_of_m_SortingLayer_2() { return &___m_SortingLayer_2; }
	inline void set_m_SortingLayer_2(int32_t value)
	{
		___m_SortingLayer_2 = value;
	}

	inline static int32_t get_offset_of_m_LayerMask_3() { return static_cast<int32_t>(offsetof(TapEventMapping_t2308161515, ___m_LayerMask_3)); }
	inline LayerMask_t3188175821  get_m_LayerMask_3() const { return ___m_LayerMask_3; }
	inline LayerMask_t3188175821 * get_address_of_m_LayerMask_3() { return &___m_LayerMask_3; }
	inline void set_m_LayerMask_3(LayerMask_t3188175821  value)
	{
		___m_LayerMask_3 = value;
	}

	inline static int32_t get_offset_of_m_Callback_4() { return static_cast<int32_t>(offsetof(TapEventMapping_t2308161515, ___m_Callback_4)); }
	inline String_t* get_m_Callback_4() const { return ___m_Callback_4; }
	inline String_t** get_address_of_m_Callback_4() { return &___m_Callback_4; }
	inline void set_m_Callback_4(String_t* value)
	{
		___m_Callback_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Callback_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAPEVENTMAPPING_T2308161515_H
#ifndef OBJECT_T1021602117_H
#define OBJECT_T1021602117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1021602117  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	IntPtr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1021602117, ___m_CachedPtr_0)); }
	inline IntPtr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline IntPtr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(IntPtr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1021602117_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1021602117_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1021602117_H
#ifndef FSARRAYCONVERTER_T1376358826_H
#define FSARRAYCONVERTER_T1376358826_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.Internal.fsArrayConverter
struct  fsArrayConverter_t1376358826  : public fsConverter_t466758137
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSARRAYCONVERTER_T1376358826_H
#ifndef FSIENUMERABLECONVERTER_T2313469578_H
#define FSIENUMERABLECONVERTER_T2313469578_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.Internal.fsIEnumerableConverter
struct  fsIEnumerableConverter_t2313469578  : public fsConverter_t466758137
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSIENUMERABLECONVERTER_T2313469578_H
#ifndef FSGUIDCONVERTER_T2907267772_H
#define FSGUIDCONVERTER_T2907267772_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.Internal.fsGuidConverter
struct  fsGuidConverter_t2907267772  : public fsConverter_t466758137
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSGUIDCONVERTER_T2907267772_H
#ifndef FSNULLABLECONVERTER_T3298327032_H
#define FSNULLABLECONVERTER_T3298327032_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.Internal.fsNullableConverter
struct  fsNullableConverter_t3298327032  : public fsConverter_t466758137
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSNULLABLECONVERTER_T3298327032_H
#ifndef FSKEYVALUEPAIRCONVERTER_T2945333237_H
#define FSKEYVALUEPAIRCONVERTER_T2945333237_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.Internal.fsKeyValuePairConverter
struct  fsKeyValuePairConverter_t2945333237  : public fsConverter_t466758137
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSKEYVALUEPAIRCONVERTER_T2945333237_H
#ifndef MULTICASTDELEGATE_T3201952435_H
#define MULTICASTDELEGATE_T3201952435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t3201952435  : public Delegate_t3022476291
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t3201952435 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t3201952435 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___prev_9)); }
	inline MulticastDelegate_t3201952435 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t3201952435 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t3201952435 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___kpm_next_10)); }
	inline MulticastDelegate_t3201952435 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t3201952435 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t3201952435 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T3201952435_H
#ifndef COMPONENT_T3819376471_H
#define COMPONENT_T3819376471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t3819376471  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T3819376471_H
#ifndef U3CTWOFINGERTRANSFORMEDHANDLERU3EC__ANONSTOREY3_T3436378966_H
#define U3CTWOFINGERTRANSFORMEDHANDLERU3EC__ANONSTOREY3_T3436378966_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Utilities.TouchEventManager/<TwoFingerTransformedHandler>c__AnonStorey3
struct  U3CTwoFingerTransformedHandlerU3Ec__AnonStorey3_t3436378966  : public RuntimeObject
{
public:
	// UnityEngine.EventSystems.RaycastResult IBM.Watson.DeveloperCloud.Utilities.TouchEventManager/<TwoFingerTransformedHandler>c__AnonStorey3::itemRaycastResult
	RaycastResult_t21186376  ___itemRaycastResult_0;

public:
	inline static int32_t get_offset_of_itemRaycastResult_0() { return static_cast<int32_t>(offsetof(U3CTwoFingerTransformedHandlerU3Ec__AnonStorey3_t3436378966, ___itemRaycastResult_0)); }
	inline RaycastResult_t21186376  get_itemRaycastResult_0() const { return ___itemRaycastResult_0; }
	inline RaycastResult_t21186376 * get_address_of_itemRaycastResult_0() { return &___itemRaycastResult_0; }
	inline void set_itemRaycastResult_0(RaycastResult_t21186376  value)
	{
		___itemRaycastResult_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTWOFINGERTRANSFORMEDHANDLERU3EC__ANONSTOREY3_T3436378966_H
#ifndef U3CONEFINGERTRANSFORMEDHANDLERU3EC__ANONSTOREY1_T3856358066_H
#define U3CONEFINGERTRANSFORMEDHANDLERU3EC__ANONSTOREY1_T3856358066_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Utilities.TouchEventManager/<OneFingerTransformedHandler>c__AnonStorey1
struct  U3COneFingerTransformedHandlerU3Ec__AnonStorey1_t3856358066  : public RuntimeObject
{
public:
	// UnityEngine.EventSystems.RaycastResult IBM.Watson.DeveloperCloud.Utilities.TouchEventManager/<OneFingerTransformedHandler>c__AnonStorey1::itemRaycastResult
	RaycastResult_t21186376  ___itemRaycastResult_0;

public:
	inline static int32_t get_offset_of_itemRaycastResult_0() { return static_cast<int32_t>(offsetof(U3COneFingerTransformedHandlerU3Ec__AnonStorey1_t3856358066, ___itemRaycastResult_0)); }
	inline RaycastResult_t21186376  get_itemRaycastResult_0() const { return ___itemRaycastResult_0; }
	inline RaycastResult_t21186376 * get_address_of_itemRaycastResult_0() { return &___itemRaycastResult_0; }
	inline void set_itemRaycastResult_0(RaycastResult_t21186376  value)
	{
		___itemRaycastResult_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONEFINGERTRANSFORMEDHANDLERU3EC__ANONSTOREY1_T3856358066_H
#ifndef VOICEDATA_T4097945788_H
#define VOICEDATA_T4097945788_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.DataTypes.VoiceData
struct  VoiceData_t4097945788  : public Data_t3166909639
{
public:
	// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.VoiceType IBM.Watson.DeveloperCloud.DataTypes.VoiceData::<Voice>k__BackingField
	int32_t ___U3CVoiceU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CVoiceU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(VoiceData_t4097945788, ___U3CVoiceU3Ek__BackingField_0)); }
	inline int32_t get_U3CVoiceU3Ek__BackingField_0() const { return ___U3CVoiceU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CVoiceU3Ek__BackingField_0() { return &___U3CVoiceU3Ek__BackingField_0; }
	inline void set_U3CVoiceU3Ek__BackingField_0(int32_t value)
	{
		___U3CVoiceU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOICEDATA_T4097945788_H
#ifndef DATETIME_T693205669_H
#define DATETIME_T693205669_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t693205669 
{
public:
	// System.TimeSpan System.DateTime::ticks
	TimeSpan_t3430258949  ___ticks_10;
	// System.DateTimeKind System.DateTime::kind
	int32_t ___kind_11;

public:
	inline static int32_t get_offset_of_ticks_10() { return static_cast<int32_t>(offsetof(DateTime_t693205669, ___ticks_10)); }
	inline TimeSpan_t3430258949  get_ticks_10() const { return ___ticks_10; }
	inline TimeSpan_t3430258949 * get_address_of_ticks_10() { return &___ticks_10; }
	inline void set_ticks_10(TimeSpan_t3430258949  value)
	{
		___ticks_10 = value;
	}

	inline static int32_t get_offset_of_kind_11() { return static_cast<int32_t>(offsetof(DateTime_t693205669, ___kind_11)); }
	inline int32_t get_kind_11() const { return ___kind_11; }
	inline int32_t* get_address_of_kind_11() { return &___kind_11; }
	inline void set_kind_11(int32_t value)
	{
		___kind_11 = value;
	}
};

struct DateTime_t693205669_StaticFields
{
public:
	// System.DateTime System.DateTime::MaxValue
	DateTime_t693205669  ___MaxValue_12;
	// System.DateTime System.DateTime::MinValue
	DateTime_t693205669  ___MinValue_13;
	// System.String[] System.DateTime::ParseTimeFormats
	StringU5BU5D_t1642385972* ___ParseTimeFormats_14;
	// System.String[] System.DateTime::ParseYearDayMonthFormats
	StringU5BU5D_t1642385972* ___ParseYearDayMonthFormats_15;
	// System.String[] System.DateTime::ParseYearMonthDayFormats
	StringU5BU5D_t1642385972* ___ParseYearMonthDayFormats_16;
	// System.String[] System.DateTime::ParseDayMonthYearFormats
	StringU5BU5D_t1642385972* ___ParseDayMonthYearFormats_17;
	// System.String[] System.DateTime::ParseMonthDayYearFormats
	StringU5BU5D_t1642385972* ___ParseMonthDayYearFormats_18;
	// System.String[] System.DateTime::MonthDayShortFormats
	StringU5BU5D_t1642385972* ___MonthDayShortFormats_19;
	// System.String[] System.DateTime::DayMonthShortFormats
	StringU5BU5D_t1642385972* ___DayMonthShortFormats_20;
	// System.Int32[] System.DateTime::daysmonth
	Int32U5BU5D_t3030399641* ___daysmonth_21;
	// System.Int32[] System.DateTime::daysmonthleap
	Int32U5BU5D_t3030399641* ___daysmonthleap_22;
	// System.Object System.DateTime::to_local_time_span_object
	RuntimeObject * ___to_local_time_span_object_23;
	// System.Int64 System.DateTime::last_now
	int64_t ___last_now_24;

public:
	inline static int32_t get_offset_of_MaxValue_12() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___MaxValue_12)); }
	inline DateTime_t693205669  get_MaxValue_12() const { return ___MaxValue_12; }
	inline DateTime_t693205669 * get_address_of_MaxValue_12() { return &___MaxValue_12; }
	inline void set_MaxValue_12(DateTime_t693205669  value)
	{
		___MaxValue_12 = value;
	}

	inline static int32_t get_offset_of_MinValue_13() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___MinValue_13)); }
	inline DateTime_t693205669  get_MinValue_13() const { return ___MinValue_13; }
	inline DateTime_t693205669 * get_address_of_MinValue_13() { return &___MinValue_13; }
	inline void set_MinValue_13(DateTime_t693205669  value)
	{
		___MinValue_13 = value;
	}

	inline static int32_t get_offset_of_ParseTimeFormats_14() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseTimeFormats_14)); }
	inline StringU5BU5D_t1642385972* get_ParseTimeFormats_14() const { return ___ParseTimeFormats_14; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseTimeFormats_14() { return &___ParseTimeFormats_14; }
	inline void set_ParseTimeFormats_14(StringU5BU5D_t1642385972* value)
	{
		___ParseTimeFormats_14 = value;
		Il2CppCodeGenWriteBarrier((&___ParseTimeFormats_14), value);
	}

	inline static int32_t get_offset_of_ParseYearDayMonthFormats_15() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseYearDayMonthFormats_15)); }
	inline StringU5BU5D_t1642385972* get_ParseYearDayMonthFormats_15() const { return ___ParseYearDayMonthFormats_15; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseYearDayMonthFormats_15() { return &___ParseYearDayMonthFormats_15; }
	inline void set_ParseYearDayMonthFormats_15(StringU5BU5D_t1642385972* value)
	{
		___ParseYearDayMonthFormats_15 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearDayMonthFormats_15), value);
	}

	inline static int32_t get_offset_of_ParseYearMonthDayFormats_16() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseYearMonthDayFormats_16)); }
	inline StringU5BU5D_t1642385972* get_ParseYearMonthDayFormats_16() const { return ___ParseYearMonthDayFormats_16; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseYearMonthDayFormats_16() { return &___ParseYearMonthDayFormats_16; }
	inline void set_ParseYearMonthDayFormats_16(StringU5BU5D_t1642385972* value)
	{
		___ParseYearMonthDayFormats_16 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearMonthDayFormats_16), value);
	}

	inline static int32_t get_offset_of_ParseDayMonthYearFormats_17() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseDayMonthYearFormats_17)); }
	inline StringU5BU5D_t1642385972* get_ParseDayMonthYearFormats_17() const { return ___ParseDayMonthYearFormats_17; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseDayMonthYearFormats_17() { return &___ParseDayMonthYearFormats_17; }
	inline void set_ParseDayMonthYearFormats_17(StringU5BU5D_t1642385972* value)
	{
		___ParseDayMonthYearFormats_17 = value;
		Il2CppCodeGenWriteBarrier((&___ParseDayMonthYearFormats_17), value);
	}

	inline static int32_t get_offset_of_ParseMonthDayYearFormats_18() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseMonthDayYearFormats_18)); }
	inline StringU5BU5D_t1642385972* get_ParseMonthDayYearFormats_18() const { return ___ParseMonthDayYearFormats_18; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseMonthDayYearFormats_18() { return &___ParseMonthDayYearFormats_18; }
	inline void set_ParseMonthDayYearFormats_18(StringU5BU5D_t1642385972* value)
	{
		___ParseMonthDayYearFormats_18 = value;
		Il2CppCodeGenWriteBarrier((&___ParseMonthDayYearFormats_18), value);
	}

	inline static int32_t get_offset_of_MonthDayShortFormats_19() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___MonthDayShortFormats_19)); }
	inline StringU5BU5D_t1642385972* get_MonthDayShortFormats_19() const { return ___MonthDayShortFormats_19; }
	inline StringU5BU5D_t1642385972** get_address_of_MonthDayShortFormats_19() { return &___MonthDayShortFormats_19; }
	inline void set_MonthDayShortFormats_19(StringU5BU5D_t1642385972* value)
	{
		___MonthDayShortFormats_19 = value;
		Il2CppCodeGenWriteBarrier((&___MonthDayShortFormats_19), value);
	}

	inline static int32_t get_offset_of_DayMonthShortFormats_20() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___DayMonthShortFormats_20)); }
	inline StringU5BU5D_t1642385972* get_DayMonthShortFormats_20() const { return ___DayMonthShortFormats_20; }
	inline StringU5BU5D_t1642385972** get_address_of_DayMonthShortFormats_20() { return &___DayMonthShortFormats_20; }
	inline void set_DayMonthShortFormats_20(StringU5BU5D_t1642385972* value)
	{
		___DayMonthShortFormats_20 = value;
		Il2CppCodeGenWriteBarrier((&___DayMonthShortFormats_20), value);
	}

	inline static int32_t get_offset_of_daysmonth_21() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___daysmonth_21)); }
	inline Int32U5BU5D_t3030399641* get_daysmonth_21() const { return ___daysmonth_21; }
	inline Int32U5BU5D_t3030399641** get_address_of_daysmonth_21() { return &___daysmonth_21; }
	inline void set_daysmonth_21(Int32U5BU5D_t3030399641* value)
	{
		___daysmonth_21 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonth_21), value);
	}

	inline static int32_t get_offset_of_daysmonthleap_22() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___daysmonthleap_22)); }
	inline Int32U5BU5D_t3030399641* get_daysmonthleap_22() const { return ___daysmonthleap_22; }
	inline Int32U5BU5D_t3030399641** get_address_of_daysmonthleap_22() { return &___daysmonthleap_22; }
	inline void set_daysmonthleap_22(Int32U5BU5D_t3030399641* value)
	{
		___daysmonthleap_22 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonthleap_22), value);
	}

	inline static int32_t get_offset_of_to_local_time_span_object_23() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___to_local_time_span_object_23)); }
	inline RuntimeObject * get_to_local_time_span_object_23() const { return ___to_local_time_span_object_23; }
	inline RuntimeObject ** get_address_of_to_local_time_span_object_23() { return &___to_local_time_span_object_23; }
	inline void set_to_local_time_span_object_23(RuntimeObject * value)
	{
		___to_local_time_span_object_23 = value;
		Il2CppCodeGenWriteBarrier((&___to_local_time_span_object_23), value);
	}

	inline static int32_t get_offset_of_last_now_24() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___last_now_24)); }
	inline int64_t get_last_now_24() const { return ___last_now_24; }
	inline int64_t* get_address_of_last_now_24() { return &___last_now_24; }
	inline void set_last_now_24(int64_t value)
	{
		___last_now_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T693205669_H
#ifndef MAPPING_T3124476604_H
#define MAPPING_T3124476604_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Widgets.KeyboardWidget/Mapping
struct  Mapping_t3124476604  : public RuntimeObject
{
public:
	// UnityEngine.KeyCode IBM.Watson.DeveloperCloud.Widgets.KeyboardWidget/Mapping::m_Key
	int32_t ___m_Key_0;
	// IBM.Watson.DeveloperCloud.Utilities.KeyModifiers IBM.Watson.DeveloperCloud.Widgets.KeyboardWidget/Mapping::m_Modifiers
	int32_t ___m_Modifiers_1;
	// System.String IBM.Watson.DeveloperCloud.Widgets.KeyboardWidget/Mapping::m_Event
	String_t* ___m_Event_2;

public:
	inline static int32_t get_offset_of_m_Key_0() { return static_cast<int32_t>(offsetof(Mapping_t3124476604, ___m_Key_0)); }
	inline int32_t get_m_Key_0() const { return ___m_Key_0; }
	inline int32_t* get_address_of_m_Key_0() { return &___m_Key_0; }
	inline void set_m_Key_0(int32_t value)
	{
		___m_Key_0 = value;
	}

	inline static int32_t get_offset_of_m_Modifiers_1() { return static_cast<int32_t>(offsetof(Mapping_t3124476604, ___m_Modifiers_1)); }
	inline int32_t get_m_Modifiers_1() const { return ___m_Modifiers_1; }
	inline int32_t* get_address_of_m_Modifiers_1() { return &___m_Modifiers_1; }
	inline void set_m_Modifiers_1(int32_t value)
	{
		___m_Modifiers_1 = value;
	}

	inline static int32_t get_offset_of_m_Event_2() { return static_cast<int32_t>(offsetof(Mapping_t3124476604, ___m_Event_2)); }
	inline String_t* get_m_Event_2() const { return ___m_Event_2; }
	inline String_t** get_address_of_m_Event_2() { return &___m_Event_2; }
	inline void set_m_Event_2(String_t* value)
	{
		___m_Event_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Event_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAPPING_T3124476604_H
#ifndef GRADIENT_DIRECTCONVERTER_T4192375398_H
#define GRADIENT_DIRECTCONVERTER_T4192375398_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.Internal.DirectConverters.Gradient_DirectConverter
struct  Gradient_DirectConverter_t4192375398  : public fsDirectConverter_1_t837437498
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRADIENT_DIRECTCONVERTER_T4192375398_H
#ifndef KEYFRAME_DIRECTCONVERTER_T1121933266_H
#define KEYFRAME_DIRECTCONVERTER_T1121933266_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.Internal.DirectConverters.Keyframe_DirectConverter
struct  Keyframe_DirectConverter_t1121933266  : public fsDirectConverter_1_t2981293126
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYFRAME_DIRECTCONVERTER_T1121933266_H
#ifndef ANIMATIONCURVE_DIRECTCONVERTER_T1633993451_H
#define ANIMATIONCURVE_DIRECTCONVERTER_T1633993451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.Internal.DirectConverters.AnimationCurve_DirectConverter
struct  AnimationCurve_DirectConverter_t1633993451  : public fsDirectConverter_1_t543395641
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONCURVE_DIRECTCONVERTER_T1633993451_H
#ifndef RECT_DIRECTCONVERTER_T2065695396_H
#define RECT_DIRECTCONVERTER_T2065695396_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.Internal.DirectConverters.Rect_DirectConverter
struct  Rect_DirectConverter_t2065695396  : public fsDirectConverter_1_t918610116
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_DIRECTCONVERTER_T2065695396_H
#ifndef LAYERMASK_DIRECTCONVERTER_T2775871809_H
#define LAYERMASK_DIRECTCONVERTER_T2775871809_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.Internal.DirectConverters.LayerMask_DirectConverter
struct  LayerMask_DirectConverter_t2775871809  : public fsDirectConverter_1_t425030311
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_DIRECTCONVERTER_T2775871809_H
#ifndef FSCONFIG_T3026457307_H
#define FSCONFIG_T3026457307_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.fsConfig
struct  fsConfig_t3026457307  : public RuntimeObject
{
public:

public:
};

struct fsConfig_t3026457307_StaticFields
{
public:
	// System.Type[] FullSerializer.fsConfig::SerializeAttributes
	TypeU5BU5D_t1664964607* ___SerializeAttributes_0;
	// System.Type[] FullSerializer.fsConfig::IgnoreSerializeAttributes
	TypeU5BU5D_t1664964607* ___IgnoreSerializeAttributes_1;
	// FullSerializer.fsMemberSerialization FullSerializer.fsConfig::_defaultMemberSerialization
	int32_t ____defaultMemberSerialization_2;
	// System.Boolean FullSerializer.fsConfig::SerializeNonAutoProperties
	bool ___SerializeNonAutoProperties_3;
	// System.Boolean FullSerializer.fsConfig::SerializeNonPublicSetProperties
	bool ___SerializeNonPublicSetProperties_4;
	// System.Boolean FullSerializer.fsConfig::IsCaseSensitive
	bool ___IsCaseSensitive_5;
	// System.String FullSerializer.fsConfig::CustomDateTimeFormatString
	String_t* ___CustomDateTimeFormatString_6;
	// System.Boolean FullSerializer.fsConfig::Serialize64BitIntegerAsString
	bool ___Serialize64BitIntegerAsString_7;
	// System.Boolean FullSerializer.fsConfig::SerializeEnumsAsInteger
	bool ___SerializeEnumsAsInteger_8;

public:
	inline static int32_t get_offset_of_SerializeAttributes_0() { return static_cast<int32_t>(offsetof(fsConfig_t3026457307_StaticFields, ___SerializeAttributes_0)); }
	inline TypeU5BU5D_t1664964607* get_SerializeAttributes_0() const { return ___SerializeAttributes_0; }
	inline TypeU5BU5D_t1664964607** get_address_of_SerializeAttributes_0() { return &___SerializeAttributes_0; }
	inline void set_SerializeAttributes_0(TypeU5BU5D_t1664964607* value)
	{
		___SerializeAttributes_0 = value;
		Il2CppCodeGenWriteBarrier((&___SerializeAttributes_0), value);
	}

	inline static int32_t get_offset_of_IgnoreSerializeAttributes_1() { return static_cast<int32_t>(offsetof(fsConfig_t3026457307_StaticFields, ___IgnoreSerializeAttributes_1)); }
	inline TypeU5BU5D_t1664964607* get_IgnoreSerializeAttributes_1() const { return ___IgnoreSerializeAttributes_1; }
	inline TypeU5BU5D_t1664964607** get_address_of_IgnoreSerializeAttributes_1() { return &___IgnoreSerializeAttributes_1; }
	inline void set_IgnoreSerializeAttributes_1(TypeU5BU5D_t1664964607* value)
	{
		___IgnoreSerializeAttributes_1 = value;
		Il2CppCodeGenWriteBarrier((&___IgnoreSerializeAttributes_1), value);
	}

	inline static int32_t get_offset_of__defaultMemberSerialization_2() { return static_cast<int32_t>(offsetof(fsConfig_t3026457307_StaticFields, ____defaultMemberSerialization_2)); }
	inline int32_t get__defaultMemberSerialization_2() const { return ____defaultMemberSerialization_2; }
	inline int32_t* get_address_of__defaultMemberSerialization_2() { return &____defaultMemberSerialization_2; }
	inline void set__defaultMemberSerialization_2(int32_t value)
	{
		____defaultMemberSerialization_2 = value;
	}

	inline static int32_t get_offset_of_SerializeNonAutoProperties_3() { return static_cast<int32_t>(offsetof(fsConfig_t3026457307_StaticFields, ___SerializeNonAutoProperties_3)); }
	inline bool get_SerializeNonAutoProperties_3() const { return ___SerializeNonAutoProperties_3; }
	inline bool* get_address_of_SerializeNonAutoProperties_3() { return &___SerializeNonAutoProperties_3; }
	inline void set_SerializeNonAutoProperties_3(bool value)
	{
		___SerializeNonAutoProperties_3 = value;
	}

	inline static int32_t get_offset_of_SerializeNonPublicSetProperties_4() { return static_cast<int32_t>(offsetof(fsConfig_t3026457307_StaticFields, ___SerializeNonPublicSetProperties_4)); }
	inline bool get_SerializeNonPublicSetProperties_4() const { return ___SerializeNonPublicSetProperties_4; }
	inline bool* get_address_of_SerializeNonPublicSetProperties_4() { return &___SerializeNonPublicSetProperties_4; }
	inline void set_SerializeNonPublicSetProperties_4(bool value)
	{
		___SerializeNonPublicSetProperties_4 = value;
	}

	inline static int32_t get_offset_of_IsCaseSensitive_5() { return static_cast<int32_t>(offsetof(fsConfig_t3026457307_StaticFields, ___IsCaseSensitive_5)); }
	inline bool get_IsCaseSensitive_5() const { return ___IsCaseSensitive_5; }
	inline bool* get_address_of_IsCaseSensitive_5() { return &___IsCaseSensitive_5; }
	inline void set_IsCaseSensitive_5(bool value)
	{
		___IsCaseSensitive_5 = value;
	}

	inline static int32_t get_offset_of_CustomDateTimeFormatString_6() { return static_cast<int32_t>(offsetof(fsConfig_t3026457307_StaticFields, ___CustomDateTimeFormatString_6)); }
	inline String_t* get_CustomDateTimeFormatString_6() const { return ___CustomDateTimeFormatString_6; }
	inline String_t** get_address_of_CustomDateTimeFormatString_6() { return &___CustomDateTimeFormatString_6; }
	inline void set_CustomDateTimeFormatString_6(String_t* value)
	{
		___CustomDateTimeFormatString_6 = value;
		Il2CppCodeGenWriteBarrier((&___CustomDateTimeFormatString_6), value);
	}

	inline static int32_t get_offset_of_Serialize64BitIntegerAsString_7() { return static_cast<int32_t>(offsetof(fsConfig_t3026457307_StaticFields, ___Serialize64BitIntegerAsString_7)); }
	inline bool get_Serialize64BitIntegerAsString_7() const { return ___Serialize64BitIntegerAsString_7; }
	inline bool* get_address_of_Serialize64BitIntegerAsString_7() { return &___Serialize64BitIntegerAsString_7; }
	inline void set_Serialize64BitIntegerAsString_7(bool value)
	{
		___Serialize64BitIntegerAsString_7 = value;
	}

	inline static int32_t get_offset_of_SerializeEnumsAsInteger_8() { return static_cast<int32_t>(offsetof(fsConfig_t3026457307_StaticFields, ___SerializeEnumsAsInteger_8)); }
	inline bool get_SerializeEnumsAsInteger_8() const { return ___SerializeEnumsAsInteger_8; }
	inline bool* get_address_of_SerializeEnumsAsInteger_8() { return &___SerializeEnumsAsInteger_8; }
	inline void set_SerializeEnumsAsInteger_8(bool value)
	{
		___SerializeEnumsAsInteger_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSCONFIG_T3026457307_H
#ifndef BOUNDS_DIRECTCONVERTER_T3918181867_H
#define BOUNDS_DIRECTCONVERTER_T3918181867_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullSerializer.Internal.DirectConverters.Bounds_DirectConverter
struct  Bounds_DirectConverter_t3918181867  : public fsDirectConverter_1_t270218193
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDS_DIRECTCONVERTER_T3918181867_H
#ifndef ONRECEIVEDATA_T2274732329_H
#define ONRECEIVEDATA_T2274732329_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Widgets.Widget/OnReceiveData
struct  OnReceiveData_t2274732329  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONRECEIVEDATA_T2274732329_H
#ifndef BEHAVIOUR_T955675639_H
#define BEHAVIOUR_T955675639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t955675639  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T955675639_H
#ifndef ONCLASSIFIERRESULT_T2664561427_H
#define ONCLASSIFIERRESULT_T2664561427_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Widgets.ClassifierWidget/OnClassifierResult
struct  OnClassifierResult_t2664561427  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONCLASSIFIERRESULT_T2664561427_H
#ifndef MONOBEHAVIOUR_T1158329972_H
#define MONOBEHAVIOUR_T1158329972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t1158329972  : public Behaviour_t955675639
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T1158329972_H
#ifndef RUNNABLE_T1422227699_H
#define RUNNABLE_T1422227699_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Utilities.Runnable
struct  Runnable_t1422227699  : public MonoBehaviour_t1158329972
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,IBM.Watson.DeveloperCloud.Utilities.Runnable/Routine> IBM.Watson.DeveloperCloud.Utilities.Runnable::m_Routines
	Dictionary_2_t980883800 * ___m_Routines_2;
	// System.Int32 IBM.Watson.DeveloperCloud.Utilities.Runnable::m_NextRoutineId
	int32_t ___m_NextRoutineId_3;

public:
	inline static int32_t get_offset_of_m_Routines_2() { return static_cast<int32_t>(offsetof(Runnable_t1422227699, ___m_Routines_2)); }
	inline Dictionary_2_t980883800 * get_m_Routines_2() const { return ___m_Routines_2; }
	inline Dictionary_2_t980883800 ** get_address_of_m_Routines_2() { return &___m_Routines_2; }
	inline void set_m_Routines_2(Dictionary_2_t980883800 * value)
	{
		___m_Routines_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Routines_2), value);
	}

	inline static int32_t get_offset_of_m_NextRoutineId_3() { return static_cast<int32_t>(offsetof(Runnable_t1422227699, ___m_NextRoutineId_3)); }
	inline int32_t get_m_NextRoutineId_3() const { return ___m_NextRoutineId_3; }
	inline int32_t* get_address_of_m_NextRoutineId_3() { return &___m_NextRoutineId_3; }
	inline void set_m_NextRoutineId_3(int32_t value)
	{
		___m_NextRoutineId_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNNABLE_T1422227699_H
#ifndef TIMEDDESTROY_T2617744245_H
#define TIMEDDESTROY_T2617744245_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Utilities.TimedDestroy
struct  TimedDestroy_t2617744245  : public MonoBehaviour_t1158329972
{
public:
	// System.Single IBM.Watson.DeveloperCloud.Utilities.TimedDestroy::m_DestroyTime
	float ___m_DestroyTime_2;
	// System.Single IBM.Watson.DeveloperCloud.Utilities.TimedDestroy::m_ElapsedTime
	float ___m_ElapsedTime_3;
	// System.Boolean IBM.Watson.DeveloperCloud.Utilities.TimedDestroy::m_TimeReachedToDestroy
	bool ___m_TimeReachedToDestroy_4;
	// System.Boolean IBM.Watson.DeveloperCloud.Utilities.TimedDestroy::m_AlphaFade
	bool ___m_AlphaFade_5;
	// System.Boolean IBM.Watson.DeveloperCloud.Utilities.TimedDestroy::m_AlphaFadeOnAwake
	bool ___m_AlphaFadeOnAwake_6;
	// System.Single IBM.Watson.DeveloperCloud.Utilities.TimedDestroy::m_FadeTime
	float ___m_FadeTime_7;
	// System.Single IBM.Watson.DeveloperCloud.Utilities.TimedDestroy::m_FadeTimeOnAwake
	float ___m_FadeTimeOnAwake_8;
	// UnityEngine.UI.Graphic IBM.Watson.DeveloperCloud.Utilities.TimedDestroy::m_AlphaTarget
	Graphic_t2426225576 * ___m_AlphaTarget_9;
	// System.Boolean IBM.Watson.DeveloperCloud.Utilities.TimedDestroy::m_Fading
	bool ___m_Fading_10;
	// System.Single IBM.Watson.DeveloperCloud.Utilities.TimedDestroy::m_FadeStart
	float ___m_FadeStart_11;
	// UnityEngine.Color IBM.Watson.DeveloperCloud.Utilities.TimedDestroy::m_InitialColor
	Color_t2020392075  ___m_InitialColor_12;
	// System.Single IBM.Watson.DeveloperCloud.Utilities.TimedDestroy::m_FadeAwakeRatio
	float ___m_FadeAwakeRatio_13;

public:
	inline static int32_t get_offset_of_m_DestroyTime_2() { return static_cast<int32_t>(offsetof(TimedDestroy_t2617744245, ___m_DestroyTime_2)); }
	inline float get_m_DestroyTime_2() const { return ___m_DestroyTime_2; }
	inline float* get_address_of_m_DestroyTime_2() { return &___m_DestroyTime_2; }
	inline void set_m_DestroyTime_2(float value)
	{
		___m_DestroyTime_2 = value;
	}

	inline static int32_t get_offset_of_m_ElapsedTime_3() { return static_cast<int32_t>(offsetof(TimedDestroy_t2617744245, ___m_ElapsedTime_3)); }
	inline float get_m_ElapsedTime_3() const { return ___m_ElapsedTime_3; }
	inline float* get_address_of_m_ElapsedTime_3() { return &___m_ElapsedTime_3; }
	inline void set_m_ElapsedTime_3(float value)
	{
		___m_ElapsedTime_3 = value;
	}

	inline static int32_t get_offset_of_m_TimeReachedToDestroy_4() { return static_cast<int32_t>(offsetof(TimedDestroy_t2617744245, ___m_TimeReachedToDestroy_4)); }
	inline bool get_m_TimeReachedToDestroy_4() const { return ___m_TimeReachedToDestroy_4; }
	inline bool* get_address_of_m_TimeReachedToDestroy_4() { return &___m_TimeReachedToDestroy_4; }
	inline void set_m_TimeReachedToDestroy_4(bool value)
	{
		___m_TimeReachedToDestroy_4 = value;
	}

	inline static int32_t get_offset_of_m_AlphaFade_5() { return static_cast<int32_t>(offsetof(TimedDestroy_t2617744245, ___m_AlphaFade_5)); }
	inline bool get_m_AlphaFade_5() const { return ___m_AlphaFade_5; }
	inline bool* get_address_of_m_AlphaFade_5() { return &___m_AlphaFade_5; }
	inline void set_m_AlphaFade_5(bool value)
	{
		___m_AlphaFade_5 = value;
	}

	inline static int32_t get_offset_of_m_AlphaFadeOnAwake_6() { return static_cast<int32_t>(offsetof(TimedDestroy_t2617744245, ___m_AlphaFadeOnAwake_6)); }
	inline bool get_m_AlphaFadeOnAwake_6() const { return ___m_AlphaFadeOnAwake_6; }
	inline bool* get_address_of_m_AlphaFadeOnAwake_6() { return &___m_AlphaFadeOnAwake_6; }
	inline void set_m_AlphaFadeOnAwake_6(bool value)
	{
		___m_AlphaFadeOnAwake_6 = value;
	}

	inline static int32_t get_offset_of_m_FadeTime_7() { return static_cast<int32_t>(offsetof(TimedDestroy_t2617744245, ___m_FadeTime_7)); }
	inline float get_m_FadeTime_7() const { return ___m_FadeTime_7; }
	inline float* get_address_of_m_FadeTime_7() { return &___m_FadeTime_7; }
	inline void set_m_FadeTime_7(float value)
	{
		___m_FadeTime_7 = value;
	}

	inline static int32_t get_offset_of_m_FadeTimeOnAwake_8() { return static_cast<int32_t>(offsetof(TimedDestroy_t2617744245, ___m_FadeTimeOnAwake_8)); }
	inline float get_m_FadeTimeOnAwake_8() const { return ___m_FadeTimeOnAwake_8; }
	inline float* get_address_of_m_FadeTimeOnAwake_8() { return &___m_FadeTimeOnAwake_8; }
	inline void set_m_FadeTimeOnAwake_8(float value)
	{
		___m_FadeTimeOnAwake_8 = value;
	}

	inline static int32_t get_offset_of_m_AlphaTarget_9() { return static_cast<int32_t>(offsetof(TimedDestroy_t2617744245, ___m_AlphaTarget_9)); }
	inline Graphic_t2426225576 * get_m_AlphaTarget_9() const { return ___m_AlphaTarget_9; }
	inline Graphic_t2426225576 ** get_address_of_m_AlphaTarget_9() { return &___m_AlphaTarget_9; }
	inline void set_m_AlphaTarget_9(Graphic_t2426225576 * value)
	{
		___m_AlphaTarget_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_AlphaTarget_9), value);
	}

	inline static int32_t get_offset_of_m_Fading_10() { return static_cast<int32_t>(offsetof(TimedDestroy_t2617744245, ___m_Fading_10)); }
	inline bool get_m_Fading_10() const { return ___m_Fading_10; }
	inline bool* get_address_of_m_Fading_10() { return &___m_Fading_10; }
	inline void set_m_Fading_10(bool value)
	{
		___m_Fading_10 = value;
	}

	inline static int32_t get_offset_of_m_FadeStart_11() { return static_cast<int32_t>(offsetof(TimedDestroy_t2617744245, ___m_FadeStart_11)); }
	inline float get_m_FadeStart_11() const { return ___m_FadeStart_11; }
	inline float* get_address_of_m_FadeStart_11() { return &___m_FadeStart_11; }
	inline void set_m_FadeStart_11(float value)
	{
		___m_FadeStart_11 = value;
	}

	inline static int32_t get_offset_of_m_InitialColor_12() { return static_cast<int32_t>(offsetof(TimedDestroy_t2617744245, ___m_InitialColor_12)); }
	inline Color_t2020392075  get_m_InitialColor_12() const { return ___m_InitialColor_12; }
	inline Color_t2020392075 * get_address_of_m_InitialColor_12() { return &___m_InitialColor_12; }
	inline void set_m_InitialColor_12(Color_t2020392075  value)
	{
		___m_InitialColor_12 = value;
	}

	inline static int32_t get_offset_of_m_FadeAwakeRatio_13() { return static_cast<int32_t>(offsetof(TimedDestroy_t2617744245, ___m_FadeAwakeRatio_13)); }
	inline float get_m_FadeAwakeRatio_13() const { return ___m_FadeAwakeRatio_13; }
	inline float* get_address_of_m_FadeAwakeRatio_13() { return &___m_FadeAwakeRatio_13; }
	inline void set_m_FadeAwakeRatio_13(float value)
	{
		___m_FadeAwakeRatio_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMEDDESTROY_T2617744245_H
#ifndef NESTEDPREFABS_T4027735438_H
#define NESTEDPREFABS_T4027735438_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Utilities.NestedPrefabs
struct  NestedPrefabs_t4027735438  : public MonoBehaviour_t1158329972
{
public:
	// System.Collections.Generic.List`1<UnityEngine.GameObject> IBM.Watson.DeveloperCloud.Utilities.NestedPrefabs::m_Prefabs
	List_1_t1125654279 * ___m_Prefabs_2;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> IBM.Watson.DeveloperCloud.Utilities.NestedPrefabs::m_GameObjectCreated
	List_1_t1125654279 * ___m_GameObjectCreated_3;
	// System.Boolean IBM.Watson.DeveloperCloud.Utilities.NestedPrefabs::m_SetParent
	bool ___m_SetParent_4;

public:
	inline static int32_t get_offset_of_m_Prefabs_2() { return static_cast<int32_t>(offsetof(NestedPrefabs_t4027735438, ___m_Prefabs_2)); }
	inline List_1_t1125654279 * get_m_Prefabs_2() const { return ___m_Prefabs_2; }
	inline List_1_t1125654279 ** get_address_of_m_Prefabs_2() { return &___m_Prefabs_2; }
	inline void set_m_Prefabs_2(List_1_t1125654279 * value)
	{
		___m_Prefabs_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Prefabs_2), value);
	}

	inline static int32_t get_offset_of_m_GameObjectCreated_3() { return static_cast<int32_t>(offsetof(NestedPrefabs_t4027735438, ___m_GameObjectCreated_3)); }
	inline List_1_t1125654279 * get_m_GameObjectCreated_3() const { return ___m_GameObjectCreated_3; }
	inline List_1_t1125654279 ** get_address_of_m_GameObjectCreated_3() { return &___m_GameObjectCreated_3; }
	inline void set_m_GameObjectCreated_3(List_1_t1125654279 * value)
	{
		___m_GameObjectCreated_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_GameObjectCreated_3), value);
	}

	inline static int32_t get_offset_of_m_SetParent_4() { return static_cast<int32_t>(offsetof(NestedPrefabs_t4027735438, ___m_SetParent_4)); }
	inline bool get_m_SetParent_4() const { return ___m_SetParent_4; }
	inline bool* get_address_of_m_SetParent_4() { return &___m_SetParent_4; }
	inline void set_m_SetParent_4(bool value)
	{
		___m_SetParent_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NESTEDPREFABS_T4027735438_H
#ifndef TOUCHEVENTMANAGER_T1889607896_H
#define TOUCHEVENTMANAGER_T1889607896_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Utilities.TouchEventManager
struct  TouchEventManager_t1889607896  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Camera IBM.Watson.DeveloperCloud.Utilities.TouchEventManager::m_mainCamera
	Camera_t189460977 * ___m_mainCamera_2;
	// System.Boolean IBM.Watson.DeveloperCloud.Utilities.TouchEventManager::m_Active
	bool ___m_Active_3;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<IBM.Watson.DeveloperCloud.Utilities.TouchEventManager/TouchEventData>> IBM.Watson.DeveloperCloud.Utilities.TouchEventManager::m_TapEvents
	Dictionary_2_t449014016 * ___m_TapEvents_4;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<IBM.Watson.DeveloperCloud.Utilities.TouchEventManager/TouchEventData>> IBM.Watson.DeveloperCloud.Utilities.TouchEventManager::m_DoubleTapEvents
	Dictionary_2_t449014016 * ___m_DoubleTapEvents_5;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<IBM.Watson.DeveloperCloud.Utilities.TouchEventManager/TouchEventData>> IBM.Watson.DeveloperCloud.Utilities.TouchEventManager::m_DragEvents
	Dictionary_2_t449014016 * ___m_DragEvents_6;
	// TouchScript.Gestures.TapGesture IBM.Watson.DeveloperCloud.Utilities.TouchEventManager::m_TapGesture
	TapGesture_t556401502 * ___m_TapGesture_7;
	// TouchScript.Gestures.TapGesture IBM.Watson.DeveloperCloud.Utilities.TouchEventManager::m_DoubleTapGesture
	TapGesture_t556401502 * ___m_DoubleTapGesture_8;
	// TouchScript.Gestures.TapGesture IBM.Watson.DeveloperCloud.Utilities.TouchEventManager::m_ThreeTapGesture
	TapGesture_t556401502 * ___m_ThreeTapGesture_9;
	// TouchScript.Gestures.ScreenTransformGesture IBM.Watson.DeveloperCloud.Utilities.TouchEventManager::m_OneFingerMoveGesture
	ScreenTransformGesture_t3088562929 * ___m_OneFingerMoveGesture_10;
	// TouchScript.Gestures.ScreenTransformGesture IBM.Watson.DeveloperCloud.Utilities.TouchEventManager::m_TwoFingerMoveGesture
	ScreenTransformGesture_t3088562929 * ___m_TwoFingerMoveGesture_11;
	// TouchScript.Gestures.PressGesture IBM.Watson.DeveloperCloud.Utilities.TouchEventManager::m_PressGesture
	PressGesture_t582183752 * ___m_PressGesture_12;
	// TouchScript.Gestures.ReleaseGesture IBM.Watson.DeveloperCloud.Utilities.TouchEventManager::m_ReleaseGesture
	ReleaseGesture_t248506278 * ___m_ReleaseGesture_13;
	// TouchScript.Gestures.LongPressGesture IBM.Watson.DeveloperCloud.Utilities.TouchEventManager::m_LongPressGesture
	LongPressGesture_t656184630 * ___m_LongPressGesture_14;

public:
	inline static int32_t get_offset_of_m_mainCamera_2() { return static_cast<int32_t>(offsetof(TouchEventManager_t1889607896, ___m_mainCamera_2)); }
	inline Camera_t189460977 * get_m_mainCamera_2() const { return ___m_mainCamera_2; }
	inline Camera_t189460977 ** get_address_of_m_mainCamera_2() { return &___m_mainCamera_2; }
	inline void set_m_mainCamera_2(Camera_t189460977 * value)
	{
		___m_mainCamera_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_mainCamera_2), value);
	}

	inline static int32_t get_offset_of_m_Active_3() { return static_cast<int32_t>(offsetof(TouchEventManager_t1889607896, ___m_Active_3)); }
	inline bool get_m_Active_3() const { return ___m_Active_3; }
	inline bool* get_address_of_m_Active_3() { return &___m_Active_3; }
	inline void set_m_Active_3(bool value)
	{
		___m_Active_3 = value;
	}

	inline static int32_t get_offset_of_m_TapEvents_4() { return static_cast<int32_t>(offsetof(TouchEventManager_t1889607896, ___m_TapEvents_4)); }
	inline Dictionary_2_t449014016 * get_m_TapEvents_4() const { return ___m_TapEvents_4; }
	inline Dictionary_2_t449014016 ** get_address_of_m_TapEvents_4() { return &___m_TapEvents_4; }
	inline void set_m_TapEvents_4(Dictionary_2_t449014016 * value)
	{
		___m_TapEvents_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_TapEvents_4), value);
	}

	inline static int32_t get_offset_of_m_DoubleTapEvents_5() { return static_cast<int32_t>(offsetof(TouchEventManager_t1889607896, ___m_DoubleTapEvents_5)); }
	inline Dictionary_2_t449014016 * get_m_DoubleTapEvents_5() const { return ___m_DoubleTapEvents_5; }
	inline Dictionary_2_t449014016 ** get_address_of_m_DoubleTapEvents_5() { return &___m_DoubleTapEvents_5; }
	inline void set_m_DoubleTapEvents_5(Dictionary_2_t449014016 * value)
	{
		___m_DoubleTapEvents_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_DoubleTapEvents_5), value);
	}

	inline static int32_t get_offset_of_m_DragEvents_6() { return static_cast<int32_t>(offsetof(TouchEventManager_t1889607896, ___m_DragEvents_6)); }
	inline Dictionary_2_t449014016 * get_m_DragEvents_6() const { return ___m_DragEvents_6; }
	inline Dictionary_2_t449014016 ** get_address_of_m_DragEvents_6() { return &___m_DragEvents_6; }
	inline void set_m_DragEvents_6(Dictionary_2_t449014016 * value)
	{
		___m_DragEvents_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_DragEvents_6), value);
	}

	inline static int32_t get_offset_of_m_TapGesture_7() { return static_cast<int32_t>(offsetof(TouchEventManager_t1889607896, ___m_TapGesture_7)); }
	inline TapGesture_t556401502 * get_m_TapGesture_7() const { return ___m_TapGesture_7; }
	inline TapGesture_t556401502 ** get_address_of_m_TapGesture_7() { return &___m_TapGesture_7; }
	inline void set_m_TapGesture_7(TapGesture_t556401502 * value)
	{
		___m_TapGesture_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_TapGesture_7), value);
	}

	inline static int32_t get_offset_of_m_DoubleTapGesture_8() { return static_cast<int32_t>(offsetof(TouchEventManager_t1889607896, ___m_DoubleTapGesture_8)); }
	inline TapGesture_t556401502 * get_m_DoubleTapGesture_8() const { return ___m_DoubleTapGesture_8; }
	inline TapGesture_t556401502 ** get_address_of_m_DoubleTapGesture_8() { return &___m_DoubleTapGesture_8; }
	inline void set_m_DoubleTapGesture_8(TapGesture_t556401502 * value)
	{
		___m_DoubleTapGesture_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_DoubleTapGesture_8), value);
	}

	inline static int32_t get_offset_of_m_ThreeTapGesture_9() { return static_cast<int32_t>(offsetof(TouchEventManager_t1889607896, ___m_ThreeTapGesture_9)); }
	inline TapGesture_t556401502 * get_m_ThreeTapGesture_9() const { return ___m_ThreeTapGesture_9; }
	inline TapGesture_t556401502 ** get_address_of_m_ThreeTapGesture_9() { return &___m_ThreeTapGesture_9; }
	inline void set_m_ThreeTapGesture_9(TapGesture_t556401502 * value)
	{
		___m_ThreeTapGesture_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_ThreeTapGesture_9), value);
	}

	inline static int32_t get_offset_of_m_OneFingerMoveGesture_10() { return static_cast<int32_t>(offsetof(TouchEventManager_t1889607896, ___m_OneFingerMoveGesture_10)); }
	inline ScreenTransformGesture_t3088562929 * get_m_OneFingerMoveGesture_10() const { return ___m_OneFingerMoveGesture_10; }
	inline ScreenTransformGesture_t3088562929 ** get_address_of_m_OneFingerMoveGesture_10() { return &___m_OneFingerMoveGesture_10; }
	inline void set_m_OneFingerMoveGesture_10(ScreenTransformGesture_t3088562929 * value)
	{
		___m_OneFingerMoveGesture_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_OneFingerMoveGesture_10), value);
	}

	inline static int32_t get_offset_of_m_TwoFingerMoveGesture_11() { return static_cast<int32_t>(offsetof(TouchEventManager_t1889607896, ___m_TwoFingerMoveGesture_11)); }
	inline ScreenTransformGesture_t3088562929 * get_m_TwoFingerMoveGesture_11() const { return ___m_TwoFingerMoveGesture_11; }
	inline ScreenTransformGesture_t3088562929 ** get_address_of_m_TwoFingerMoveGesture_11() { return &___m_TwoFingerMoveGesture_11; }
	inline void set_m_TwoFingerMoveGesture_11(ScreenTransformGesture_t3088562929 * value)
	{
		___m_TwoFingerMoveGesture_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_TwoFingerMoveGesture_11), value);
	}

	inline static int32_t get_offset_of_m_PressGesture_12() { return static_cast<int32_t>(offsetof(TouchEventManager_t1889607896, ___m_PressGesture_12)); }
	inline PressGesture_t582183752 * get_m_PressGesture_12() const { return ___m_PressGesture_12; }
	inline PressGesture_t582183752 ** get_address_of_m_PressGesture_12() { return &___m_PressGesture_12; }
	inline void set_m_PressGesture_12(PressGesture_t582183752 * value)
	{
		___m_PressGesture_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_PressGesture_12), value);
	}

	inline static int32_t get_offset_of_m_ReleaseGesture_13() { return static_cast<int32_t>(offsetof(TouchEventManager_t1889607896, ___m_ReleaseGesture_13)); }
	inline ReleaseGesture_t248506278 * get_m_ReleaseGesture_13() const { return ___m_ReleaseGesture_13; }
	inline ReleaseGesture_t248506278 ** get_address_of_m_ReleaseGesture_13() { return &___m_ReleaseGesture_13; }
	inline void set_m_ReleaseGesture_13(ReleaseGesture_t248506278 * value)
	{
		___m_ReleaseGesture_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_ReleaseGesture_13), value);
	}

	inline static int32_t get_offset_of_m_LongPressGesture_14() { return static_cast<int32_t>(offsetof(TouchEventManager_t1889607896, ___m_LongPressGesture_14)); }
	inline LongPressGesture_t656184630 * get_m_LongPressGesture_14() const { return ___m_LongPressGesture_14; }
	inline LongPressGesture_t656184630 ** get_address_of_m_LongPressGesture_14() { return &___m_LongPressGesture_14; }
	inline void set_m_LongPressGesture_14(LongPressGesture_t656184630 * value)
	{
		___m_LongPressGesture_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_LongPressGesture_14), value);
	}
};

struct TouchEventManager_t1889607896_StaticFields
{
public:
	// IBM.Watson.DeveloperCloud.Utilities.TouchEventManager IBM.Watson.DeveloperCloud.Utilities.TouchEventManager::sm_Instance
	TouchEventManager_t1889607896 * ___sm_Instance_15;

public:
	inline static int32_t get_offset_of_sm_Instance_15() { return static_cast<int32_t>(offsetof(TouchEventManager_t1889607896_StaticFields, ___sm_Instance_15)); }
	inline TouchEventManager_t1889607896 * get_sm_Instance_15() const { return ___sm_Instance_15; }
	inline TouchEventManager_t1889607896 ** get_address_of_sm_Instance_15() { return &___sm_Instance_15; }
	inline void set_sm_Instance_15(TouchEventManager_t1889607896 * value)
	{
		___sm_Instance_15 = value;
		Il2CppCodeGenWriteBarrier((&___sm_Instance_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHEVENTMANAGER_T1889607896_H
#ifndef WIDGET_T3624771850_H
#define WIDGET_T3624771850_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Widgets.Widget
struct  Widget_t3624771850  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean IBM.Watson.DeveloperCloud.Widgets.Widget::m_AutoConnect
	bool ___m_AutoConnect_2;
	// System.Boolean IBM.Watson.DeveloperCloud.Widgets.Widget::m_Initialized
	bool ___m_Initialized_3;
	// IBM.Watson.DeveloperCloud.Widgets.Widget/Input[] IBM.Watson.DeveloperCloud.Widgets.Widget::m_Inputs
	InputU5BU5D_t1078881596* ___m_Inputs_4;
	// IBM.Watson.DeveloperCloud.Widgets.Widget/Output[] IBM.Watson.DeveloperCloud.Widgets.Widget::m_Outputs
	OutputU5BU5D_t475762373* ___m_Outputs_5;

public:
	inline static int32_t get_offset_of_m_AutoConnect_2() { return static_cast<int32_t>(offsetof(Widget_t3624771850, ___m_AutoConnect_2)); }
	inline bool get_m_AutoConnect_2() const { return ___m_AutoConnect_2; }
	inline bool* get_address_of_m_AutoConnect_2() { return &___m_AutoConnect_2; }
	inline void set_m_AutoConnect_2(bool value)
	{
		___m_AutoConnect_2 = value;
	}

	inline static int32_t get_offset_of_m_Initialized_3() { return static_cast<int32_t>(offsetof(Widget_t3624771850, ___m_Initialized_3)); }
	inline bool get_m_Initialized_3() const { return ___m_Initialized_3; }
	inline bool* get_address_of_m_Initialized_3() { return &___m_Initialized_3; }
	inline void set_m_Initialized_3(bool value)
	{
		___m_Initialized_3 = value;
	}

	inline static int32_t get_offset_of_m_Inputs_4() { return static_cast<int32_t>(offsetof(Widget_t3624771850, ___m_Inputs_4)); }
	inline InputU5BU5D_t1078881596* get_m_Inputs_4() const { return ___m_Inputs_4; }
	inline InputU5BU5D_t1078881596** get_address_of_m_Inputs_4() { return &___m_Inputs_4; }
	inline void set_m_Inputs_4(InputU5BU5D_t1078881596* value)
	{
		___m_Inputs_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Inputs_4), value);
	}

	inline static int32_t get_offset_of_m_Outputs_5() { return static_cast<int32_t>(offsetof(Widget_t3624771850, ___m_Outputs_5)); }
	inline OutputU5BU5D_t475762373* get_m_Outputs_5() const { return ___m_Outputs_5; }
	inline OutputU5BU5D_t475762373** get_address_of_m_Outputs_5() { return &___m_Outputs_5; }
	inline void set_m_Outputs_5(OutputU5BU5D_t475762373* value)
	{
		___m_Outputs_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Outputs_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WIDGET_T3624771850_H
#ifndef SPEECHTOTEXTWIDGET_T957037126_H
#define SPEECHTOTEXTWIDGET_T957037126_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Widgets.SpeechToTextWidget
struct  SpeechToTextWidget_t957037126  : public Widget_t3624771850
{
public:
	// UnityEngine.Transform[] IBM.Watson.DeveloperCloud.Widgets.SpeechToTextWidget::Characters
	TransformU5BU5D_t3764228911* ___Characters_6;
	// System.String[] IBM.Watson.DeveloperCloud.Widgets.SpeechToTextWidget::CharacterNames
	StringU5BU5D_t1642385972* ___CharacterNames_7;
	// System.Single IBM.Watson.DeveloperCloud.Widgets.SpeechToTextWidget::distanceInFront
	float ___distanceInFront_8;
	// VikingCrewTools.SpeechbubbleManager IBM.Watson.DeveloperCloud.Widgets.SpeechToTextWidget::SpeechBubbles
	SpeechbubbleManager_t3871578363 * ___SpeechBubbles_9;
	// UnityEngine.GameObject IBM.Watson.DeveloperCloud.Widgets.SpeechToTextWidget::MainCam
	GameObject_t1756533147 * ___MainCam_10;
	// IBM.Watson.DeveloperCloud.Services.Conversation.v1.Conversation IBM.Watson.DeveloperCloud.Widgets.SpeechToTextWidget::m_Conversation
	Conversation_t105466997 * ___m_Conversation_11;
	// System.String IBM.Watson.DeveloperCloud.Widgets.SpeechToTextWidget::m_WorkspaceID
	String_t* ___m_WorkspaceID_12;
	// System.Boolean IBM.Watson.DeveloperCloud.Widgets.SpeechToTextWidget::m_UseAlternateIntents
	bool ___m_UseAlternateIntents_13;
	// System.String[] IBM.Watson.DeveloperCloud.Widgets.SpeechToTextWidget::questionArray
	StringU5BU5D_t1642385972* ___questionArray_14;
	// IBM.Watson.DeveloperCloud.Services.Conversation.v1.Context IBM.Watson.DeveloperCloud.Widgets.SpeechToTextWidget::myContext
	Context_t2592795451 * ___myContext_15;
	// System.String[] IBM.Watson.DeveloperCloud.Widgets.SpeechToTextWidget::PosterNames
	StringU5BU5D_t1642385972* ___PosterNames_16;
	// UnityEngine.GameObject[] IBM.Watson.DeveloperCloud.Widgets.SpeechToTextWidget::Posters
	GameObjectU5BU5D_t3057952154* ___Posters_17;
	// System.Single[] IBM.Watson.DeveloperCloud.Widgets.SpeechToTextWidget::PosterTimer
	SingleU5BU5D_t577127397* ___PosterTimer_18;
	// UnityEngine.Video.VideoPlayer IBM.Watson.DeveloperCloud.Widgets.SpeechToTextWidget::moviePlayer
	VideoPlayer_t10059812 * ___moviePlayer_19;
	// System.String IBM.Watson.DeveloperCloud.Widgets.SpeechToTextWidget::MovieCue
	String_t* ___MovieCue_20;
	// Boo.Lang.List`1<System.String> IBM.Watson.DeveloperCloud.Widgets.SpeechToTextWidget::NewMessages
	List_1_t3696025851 * ___NewMessages_21;
	// Boo.Lang.List`1<System.Single> IBM.Watson.DeveloperCloud.Widgets.SpeechToTextWidget::MessageDelay
	List_1_t3743315550 * ___MessageDelay_22;
	// Boo.Lang.List`1<System.Int32> IBM.Watson.DeveloperCloud.Widgets.SpeechToTextWidget::MessageChar
	List_1_t3738683066 * ___MessageChar_23;
	// UnityEngine.GameObject IBM.Watson.DeveloperCloud.Widgets.SpeechToTextWidget::tts
	GameObject_t1756533147 * ___tts_24;
	// IBM.Watson.DeveloperCloud.Widgets.Widget/Input IBM.Watson.DeveloperCloud.Widgets.SpeechToTextWidget::m_AudioInput
	Input_t3157785889 * ___m_AudioInput_25;
	// IBM.Watson.DeveloperCloud.Widgets.Widget/Input IBM.Watson.DeveloperCloud.Widgets.SpeechToTextWidget::m_LanguageInput
	Input_t3157785889 * ___m_LanguageInput_26;
	// IBM.Watson.DeveloperCloud.Widgets.Widget/Output IBM.Watson.DeveloperCloud.Widgets.SpeechToTextWidget::m_ResultOutput
	Output_t220081548 * ___m_ResultOutput_27;
	// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText IBM.Watson.DeveloperCloud.Widgets.SpeechToTextWidget::m_SpeechToText
	SpeechToText_t2713896346 * ___m_SpeechToText_28;
	// UnityEngine.UI.Text IBM.Watson.DeveloperCloud.Widgets.SpeechToTextWidget::m_StatusText
	Text_t356221433 * ___m_StatusText_29;
	// System.Boolean IBM.Watson.DeveloperCloud.Widgets.SpeechToTextWidget::m_DetectSilence
	bool ___m_DetectSilence_30;
	// System.Single IBM.Watson.DeveloperCloud.Widgets.SpeechToTextWidget::m_SilenceThreshold
	float ___m_SilenceThreshold_31;
	// System.Boolean IBM.Watson.DeveloperCloud.Widgets.SpeechToTextWidget::m_WordConfidence
	bool ___m_WordConfidence_32;
	// System.Boolean IBM.Watson.DeveloperCloud.Widgets.SpeechToTextWidget::m_TimeStamps
	bool ___m_TimeStamps_33;
	// System.Int32 IBM.Watson.DeveloperCloud.Widgets.SpeechToTextWidget::m_MaxAlternatives
	int32_t ___m_MaxAlternatives_34;
	// System.Boolean IBM.Watson.DeveloperCloud.Widgets.SpeechToTextWidget::m_EnableContinous
	bool ___m_EnableContinous_35;
	// System.Boolean IBM.Watson.DeveloperCloud.Widgets.SpeechToTextWidget::m_EnableInterimResults
	bool ___m_EnableInterimResults_36;
	// UnityEngine.UI.Text IBM.Watson.DeveloperCloud.Widgets.SpeechToTextWidget::m_Transcript
	Text_t356221433 * ___m_Transcript_37;
	// System.String IBM.Watson.DeveloperCloud.Widgets.SpeechToTextWidget::m_Language
	String_t* ___m_Language_38;
	// System.Single IBM.Watson.DeveloperCloud.Widgets.SpeechToTextWidget::MessageTimer
	float ___MessageTimer_39;

public:
	inline static int32_t get_offset_of_Characters_6() { return static_cast<int32_t>(offsetof(SpeechToTextWidget_t957037126, ___Characters_6)); }
	inline TransformU5BU5D_t3764228911* get_Characters_6() const { return ___Characters_6; }
	inline TransformU5BU5D_t3764228911** get_address_of_Characters_6() { return &___Characters_6; }
	inline void set_Characters_6(TransformU5BU5D_t3764228911* value)
	{
		___Characters_6 = value;
		Il2CppCodeGenWriteBarrier((&___Characters_6), value);
	}

	inline static int32_t get_offset_of_CharacterNames_7() { return static_cast<int32_t>(offsetof(SpeechToTextWidget_t957037126, ___CharacterNames_7)); }
	inline StringU5BU5D_t1642385972* get_CharacterNames_7() const { return ___CharacterNames_7; }
	inline StringU5BU5D_t1642385972** get_address_of_CharacterNames_7() { return &___CharacterNames_7; }
	inline void set_CharacterNames_7(StringU5BU5D_t1642385972* value)
	{
		___CharacterNames_7 = value;
		Il2CppCodeGenWriteBarrier((&___CharacterNames_7), value);
	}

	inline static int32_t get_offset_of_distanceInFront_8() { return static_cast<int32_t>(offsetof(SpeechToTextWidget_t957037126, ___distanceInFront_8)); }
	inline float get_distanceInFront_8() const { return ___distanceInFront_8; }
	inline float* get_address_of_distanceInFront_8() { return &___distanceInFront_8; }
	inline void set_distanceInFront_8(float value)
	{
		___distanceInFront_8 = value;
	}

	inline static int32_t get_offset_of_SpeechBubbles_9() { return static_cast<int32_t>(offsetof(SpeechToTextWidget_t957037126, ___SpeechBubbles_9)); }
	inline SpeechbubbleManager_t3871578363 * get_SpeechBubbles_9() const { return ___SpeechBubbles_9; }
	inline SpeechbubbleManager_t3871578363 ** get_address_of_SpeechBubbles_9() { return &___SpeechBubbles_9; }
	inline void set_SpeechBubbles_9(SpeechbubbleManager_t3871578363 * value)
	{
		___SpeechBubbles_9 = value;
		Il2CppCodeGenWriteBarrier((&___SpeechBubbles_9), value);
	}

	inline static int32_t get_offset_of_MainCam_10() { return static_cast<int32_t>(offsetof(SpeechToTextWidget_t957037126, ___MainCam_10)); }
	inline GameObject_t1756533147 * get_MainCam_10() const { return ___MainCam_10; }
	inline GameObject_t1756533147 ** get_address_of_MainCam_10() { return &___MainCam_10; }
	inline void set_MainCam_10(GameObject_t1756533147 * value)
	{
		___MainCam_10 = value;
		Il2CppCodeGenWriteBarrier((&___MainCam_10), value);
	}

	inline static int32_t get_offset_of_m_Conversation_11() { return static_cast<int32_t>(offsetof(SpeechToTextWidget_t957037126, ___m_Conversation_11)); }
	inline Conversation_t105466997 * get_m_Conversation_11() const { return ___m_Conversation_11; }
	inline Conversation_t105466997 ** get_address_of_m_Conversation_11() { return &___m_Conversation_11; }
	inline void set_m_Conversation_11(Conversation_t105466997 * value)
	{
		___m_Conversation_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Conversation_11), value);
	}

	inline static int32_t get_offset_of_m_WorkspaceID_12() { return static_cast<int32_t>(offsetof(SpeechToTextWidget_t957037126, ___m_WorkspaceID_12)); }
	inline String_t* get_m_WorkspaceID_12() const { return ___m_WorkspaceID_12; }
	inline String_t** get_address_of_m_WorkspaceID_12() { return &___m_WorkspaceID_12; }
	inline void set_m_WorkspaceID_12(String_t* value)
	{
		___m_WorkspaceID_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_WorkspaceID_12), value);
	}

	inline static int32_t get_offset_of_m_UseAlternateIntents_13() { return static_cast<int32_t>(offsetof(SpeechToTextWidget_t957037126, ___m_UseAlternateIntents_13)); }
	inline bool get_m_UseAlternateIntents_13() const { return ___m_UseAlternateIntents_13; }
	inline bool* get_address_of_m_UseAlternateIntents_13() { return &___m_UseAlternateIntents_13; }
	inline void set_m_UseAlternateIntents_13(bool value)
	{
		___m_UseAlternateIntents_13 = value;
	}

	inline static int32_t get_offset_of_questionArray_14() { return static_cast<int32_t>(offsetof(SpeechToTextWidget_t957037126, ___questionArray_14)); }
	inline StringU5BU5D_t1642385972* get_questionArray_14() const { return ___questionArray_14; }
	inline StringU5BU5D_t1642385972** get_address_of_questionArray_14() { return &___questionArray_14; }
	inline void set_questionArray_14(StringU5BU5D_t1642385972* value)
	{
		___questionArray_14 = value;
		Il2CppCodeGenWriteBarrier((&___questionArray_14), value);
	}

	inline static int32_t get_offset_of_myContext_15() { return static_cast<int32_t>(offsetof(SpeechToTextWidget_t957037126, ___myContext_15)); }
	inline Context_t2592795451 * get_myContext_15() const { return ___myContext_15; }
	inline Context_t2592795451 ** get_address_of_myContext_15() { return &___myContext_15; }
	inline void set_myContext_15(Context_t2592795451 * value)
	{
		___myContext_15 = value;
		Il2CppCodeGenWriteBarrier((&___myContext_15), value);
	}

	inline static int32_t get_offset_of_PosterNames_16() { return static_cast<int32_t>(offsetof(SpeechToTextWidget_t957037126, ___PosterNames_16)); }
	inline StringU5BU5D_t1642385972* get_PosterNames_16() const { return ___PosterNames_16; }
	inline StringU5BU5D_t1642385972** get_address_of_PosterNames_16() { return &___PosterNames_16; }
	inline void set_PosterNames_16(StringU5BU5D_t1642385972* value)
	{
		___PosterNames_16 = value;
		Il2CppCodeGenWriteBarrier((&___PosterNames_16), value);
	}

	inline static int32_t get_offset_of_Posters_17() { return static_cast<int32_t>(offsetof(SpeechToTextWidget_t957037126, ___Posters_17)); }
	inline GameObjectU5BU5D_t3057952154* get_Posters_17() const { return ___Posters_17; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_Posters_17() { return &___Posters_17; }
	inline void set_Posters_17(GameObjectU5BU5D_t3057952154* value)
	{
		___Posters_17 = value;
		Il2CppCodeGenWriteBarrier((&___Posters_17), value);
	}

	inline static int32_t get_offset_of_PosterTimer_18() { return static_cast<int32_t>(offsetof(SpeechToTextWidget_t957037126, ___PosterTimer_18)); }
	inline SingleU5BU5D_t577127397* get_PosterTimer_18() const { return ___PosterTimer_18; }
	inline SingleU5BU5D_t577127397** get_address_of_PosterTimer_18() { return &___PosterTimer_18; }
	inline void set_PosterTimer_18(SingleU5BU5D_t577127397* value)
	{
		___PosterTimer_18 = value;
		Il2CppCodeGenWriteBarrier((&___PosterTimer_18), value);
	}

	inline static int32_t get_offset_of_moviePlayer_19() { return static_cast<int32_t>(offsetof(SpeechToTextWidget_t957037126, ___moviePlayer_19)); }
	inline VideoPlayer_t10059812 * get_moviePlayer_19() const { return ___moviePlayer_19; }
	inline VideoPlayer_t10059812 ** get_address_of_moviePlayer_19() { return &___moviePlayer_19; }
	inline void set_moviePlayer_19(VideoPlayer_t10059812 * value)
	{
		___moviePlayer_19 = value;
		Il2CppCodeGenWriteBarrier((&___moviePlayer_19), value);
	}

	inline static int32_t get_offset_of_MovieCue_20() { return static_cast<int32_t>(offsetof(SpeechToTextWidget_t957037126, ___MovieCue_20)); }
	inline String_t* get_MovieCue_20() const { return ___MovieCue_20; }
	inline String_t** get_address_of_MovieCue_20() { return &___MovieCue_20; }
	inline void set_MovieCue_20(String_t* value)
	{
		___MovieCue_20 = value;
		Il2CppCodeGenWriteBarrier((&___MovieCue_20), value);
	}

	inline static int32_t get_offset_of_NewMessages_21() { return static_cast<int32_t>(offsetof(SpeechToTextWidget_t957037126, ___NewMessages_21)); }
	inline List_1_t3696025851 * get_NewMessages_21() const { return ___NewMessages_21; }
	inline List_1_t3696025851 ** get_address_of_NewMessages_21() { return &___NewMessages_21; }
	inline void set_NewMessages_21(List_1_t3696025851 * value)
	{
		___NewMessages_21 = value;
		Il2CppCodeGenWriteBarrier((&___NewMessages_21), value);
	}

	inline static int32_t get_offset_of_MessageDelay_22() { return static_cast<int32_t>(offsetof(SpeechToTextWidget_t957037126, ___MessageDelay_22)); }
	inline List_1_t3743315550 * get_MessageDelay_22() const { return ___MessageDelay_22; }
	inline List_1_t3743315550 ** get_address_of_MessageDelay_22() { return &___MessageDelay_22; }
	inline void set_MessageDelay_22(List_1_t3743315550 * value)
	{
		___MessageDelay_22 = value;
		Il2CppCodeGenWriteBarrier((&___MessageDelay_22), value);
	}

	inline static int32_t get_offset_of_MessageChar_23() { return static_cast<int32_t>(offsetof(SpeechToTextWidget_t957037126, ___MessageChar_23)); }
	inline List_1_t3738683066 * get_MessageChar_23() const { return ___MessageChar_23; }
	inline List_1_t3738683066 ** get_address_of_MessageChar_23() { return &___MessageChar_23; }
	inline void set_MessageChar_23(List_1_t3738683066 * value)
	{
		___MessageChar_23 = value;
		Il2CppCodeGenWriteBarrier((&___MessageChar_23), value);
	}

	inline static int32_t get_offset_of_tts_24() { return static_cast<int32_t>(offsetof(SpeechToTextWidget_t957037126, ___tts_24)); }
	inline GameObject_t1756533147 * get_tts_24() const { return ___tts_24; }
	inline GameObject_t1756533147 ** get_address_of_tts_24() { return &___tts_24; }
	inline void set_tts_24(GameObject_t1756533147 * value)
	{
		___tts_24 = value;
		Il2CppCodeGenWriteBarrier((&___tts_24), value);
	}

	inline static int32_t get_offset_of_m_AudioInput_25() { return static_cast<int32_t>(offsetof(SpeechToTextWidget_t957037126, ___m_AudioInput_25)); }
	inline Input_t3157785889 * get_m_AudioInput_25() const { return ___m_AudioInput_25; }
	inline Input_t3157785889 ** get_address_of_m_AudioInput_25() { return &___m_AudioInput_25; }
	inline void set_m_AudioInput_25(Input_t3157785889 * value)
	{
		___m_AudioInput_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_AudioInput_25), value);
	}

	inline static int32_t get_offset_of_m_LanguageInput_26() { return static_cast<int32_t>(offsetof(SpeechToTextWidget_t957037126, ___m_LanguageInput_26)); }
	inline Input_t3157785889 * get_m_LanguageInput_26() const { return ___m_LanguageInput_26; }
	inline Input_t3157785889 ** get_address_of_m_LanguageInput_26() { return &___m_LanguageInput_26; }
	inline void set_m_LanguageInput_26(Input_t3157785889 * value)
	{
		___m_LanguageInput_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_LanguageInput_26), value);
	}

	inline static int32_t get_offset_of_m_ResultOutput_27() { return static_cast<int32_t>(offsetof(SpeechToTextWidget_t957037126, ___m_ResultOutput_27)); }
	inline Output_t220081548 * get_m_ResultOutput_27() const { return ___m_ResultOutput_27; }
	inline Output_t220081548 ** get_address_of_m_ResultOutput_27() { return &___m_ResultOutput_27; }
	inline void set_m_ResultOutput_27(Output_t220081548 * value)
	{
		___m_ResultOutput_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_ResultOutput_27), value);
	}

	inline static int32_t get_offset_of_m_SpeechToText_28() { return static_cast<int32_t>(offsetof(SpeechToTextWidget_t957037126, ___m_SpeechToText_28)); }
	inline SpeechToText_t2713896346 * get_m_SpeechToText_28() const { return ___m_SpeechToText_28; }
	inline SpeechToText_t2713896346 ** get_address_of_m_SpeechToText_28() { return &___m_SpeechToText_28; }
	inline void set_m_SpeechToText_28(SpeechToText_t2713896346 * value)
	{
		___m_SpeechToText_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_SpeechToText_28), value);
	}

	inline static int32_t get_offset_of_m_StatusText_29() { return static_cast<int32_t>(offsetof(SpeechToTextWidget_t957037126, ___m_StatusText_29)); }
	inline Text_t356221433 * get_m_StatusText_29() const { return ___m_StatusText_29; }
	inline Text_t356221433 ** get_address_of_m_StatusText_29() { return &___m_StatusText_29; }
	inline void set_m_StatusText_29(Text_t356221433 * value)
	{
		___m_StatusText_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_StatusText_29), value);
	}

	inline static int32_t get_offset_of_m_DetectSilence_30() { return static_cast<int32_t>(offsetof(SpeechToTextWidget_t957037126, ___m_DetectSilence_30)); }
	inline bool get_m_DetectSilence_30() const { return ___m_DetectSilence_30; }
	inline bool* get_address_of_m_DetectSilence_30() { return &___m_DetectSilence_30; }
	inline void set_m_DetectSilence_30(bool value)
	{
		___m_DetectSilence_30 = value;
	}

	inline static int32_t get_offset_of_m_SilenceThreshold_31() { return static_cast<int32_t>(offsetof(SpeechToTextWidget_t957037126, ___m_SilenceThreshold_31)); }
	inline float get_m_SilenceThreshold_31() const { return ___m_SilenceThreshold_31; }
	inline float* get_address_of_m_SilenceThreshold_31() { return &___m_SilenceThreshold_31; }
	inline void set_m_SilenceThreshold_31(float value)
	{
		___m_SilenceThreshold_31 = value;
	}

	inline static int32_t get_offset_of_m_WordConfidence_32() { return static_cast<int32_t>(offsetof(SpeechToTextWidget_t957037126, ___m_WordConfidence_32)); }
	inline bool get_m_WordConfidence_32() const { return ___m_WordConfidence_32; }
	inline bool* get_address_of_m_WordConfidence_32() { return &___m_WordConfidence_32; }
	inline void set_m_WordConfidence_32(bool value)
	{
		___m_WordConfidence_32 = value;
	}

	inline static int32_t get_offset_of_m_TimeStamps_33() { return static_cast<int32_t>(offsetof(SpeechToTextWidget_t957037126, ___m_TimeStamps_33)); }
	inline bool get_m_TimeStamps_33() const { return ___m_TimeStamps_33; }
	inline bool* get_address_of_m_TimeStamps_33() { return &___m_TimeStamps_33; }
	inline void set_m_TimeStamps_33(bool value)
	{
		___m_TimeStamps_33 = value;
	}

	inline static int32_t get_offset_of_m_MaxAlternatives_34() { return static_cast<int32_t>(offsetof(SpeechToTextWidget_t957037126, ___m_MaxAlternatives_34)); }
	inline int32_t get_m_MaxAlternatives_34() const { return ___m_MaxAlternatives_34; }
	inline int32_t* get_address_of_m_MaxAlternatives_34() { return &___m_MaxAlternatives_34; }
	inline void set_m_MaxAlternatives_34(int32_t value)
	{
		___m_MaxAlternatives_34 = value;
	}

	inline static int32_t get_offset_of_m_EnableContinous_35() { return static_cast<int32_t>(offsetof(SpeechToTextWidget_t957037126, ___m_EnableContinous_35)); }
	inline bool get_m_EnableContinous_35() const { return ___m_EnableContinous_35; }
	inline bool* get_address_of_m_EnableContinous_35() { return &___m_EnableContinous_35; }
	inline void set_m_EnableContinous_35(bool value)
	{
		___m_EnableContinous_35 = value;
	}

	inline static int32_t get_offset_of_m_EnableInterimResults_36() { return static_cast<int32_t>(offsetof(SpeechToTextWidget_t957037126, ___m_EnableInterimResults_36)); }
	inline bool get_m_EnableInterimResults_36() const { return ___m_EnableInterimResults_36; }
	inline bool* get_address_of_m_EnableInterimResults_36() { return &___m_EnableInterimResults_36; }
	inline void set_m_EnableInterimResults_36(bool value)
	{
		___m_EnableInterimResults_36 = value;
	}

	inline static int32_t get_offset_of_m_Transcript_37() { return static_cast<int32_t>(offsetof(SpeechToTextWidget_t957037126, ___m_Transcript_37)); }
	inline Text_t356221433 * get_m_Transcript_37() const { return ___m_Transcript_37; }
	inline Text_t356221433 ** get_address_of_m_Transcript_37() { return &___m_Transcript_37; }
	inline void set_m_Transcript_37(Text_t356221433 * value)
	{
		___m_Transcript_37 = value;
		Il2CppCodeGenWriteBarrier((&___m_Transcript_37), value);
	}

	inline static int32_t get_offset_of_m_Language_38() { return static_cast<int32_t>(offsetof(SpeechToTextWidget_t957037126, ___m_Language_38)); }
	inline String_t* get_m_Language_38() const { return ___m_Language_38; }
	inline String_t** get_address_of_m_Language_38() { return &___m_Language_38; }
	inline void set_m_Language_38(String_t* value)
	{
		___m_Language_38 = value;
		Il2CppCodeGenWriteBarrier((&___m_Language_38), value);
	}

	inline static int32_t get_offset_of_MessageTimer_39() { return static_cast<int32_t>(offsetof(SpeechToTextWidget_t957037126, ___MessageTimer_39)); }
	inline float get_MessageTimer_39() const { return ___MessageTimer_39; }
	inline float* get_address_of_MessageTimer_39() { return &___MessageTimer_39; }
	inline void set_MessageTimer_39(float value)
	{
		___MessageTimer_39 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPEECHTOTEXTWIDGET_T957037126_H
#ifndef NATURALLANGUAGECLASSIFIERWIDGET_T2550209684_H
#define NATURALLANGUAGECLASSIFIERWIDGET_T2550209684_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Widgets.NaturalLanguageClassifierWidget
struct  NaturalLanguageClassifierWidget_t2550209684  : public Widget_t3624771850
{
public:
	// IBM.Watson.DeveloperCloud.Widgets.Widget/Input IBM.Watson.DeveloperCloud.Widgets.NaturalLanguageClassifierWidget::m_RecognizeInput
	Input_t3157785889 * ___m_RecognizeInput_6;
	// IBM.Watson.DeveloperCloud.Widgets.Widget/Output IBM.Watson.DeveloperCloud.Widgets.NaturalLanguageClassifierWidget::m_ClassifyOutput
	Output_t220081548 * ___m_ClassifyOutput_7;
	// IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.NaturalLanguageClassifier IBM.Watson.DeveloperCloud.Widgets.NaturalLanguageClassifierWidget::m_NaturalLanguageClassifier
	NaturalLanguageClassifier_t2492034444 * ___m_NaturalLanguageClassifier_8;
	// IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.Classifier IBM.Watson.DeveloperCloud.Widgets.NaturalLanguageClassifierWidget::m_Selected
	Classifier_t1414011573 * ___m_Selected_9;
	// System.String IBM.Watson.DeveloperCloud.Widgets.NaturalLanguageClassifierWidget::m_ClassifierName
	String_t* ___m_ClassifierName_10;
	// System.String IBM.Watson.DeveloperCloud.Widgets.NaturalLanguageClassifierWidget::m_ClassifierId
	String_t* ___m_ClassifierId_11;
	// System.Single IBM.Watson.DeveloperCloud.Widgets.NaturalLanguageClassifierWidget::m_MinWordConfidence
	float ___m_MinWordConfidence_12;
	// System.Single IBM.Watson.DeveloperCloud.Widgets.NaturalLanguageClassifierWidget::m_MinWordConfidenceDelta
	float ___m_MinWordConfidenceDelta_13;
	// System.Single IBM.Watson.DeveloperCloud.Widgets.NaturalLanguageClassifierWidget::m_IgnoreWordConfidence
	float ___m_IgnoreWordConfidence_14;
	// System.Single IBM.Watson.DeveloperCloud.Widgets.NaturalLanguageClassifierWidget::m_IgnoreWordConfidenceDelta
	float ___m_IgnoreWordConfidenceDelta_15;
	// System.Single IBM.Watson.DeveloperCloud.Widgets.NaturalLanguageClassifierWidget::m_MinClassEventConfidence
	float ___m_MinClassEventConfidence_16;
	// System.Single IBM.Watson.DeveloperCloud.Widgets.NaturalLanguageClassifierWidget::m_MinClassEventConfidenceDelta
	float ___m_MinClassEventConfidenceDelta_17;
	// System.String IBM.Watson.DeveloperCloud.Widgets.NaturalLanguageClassifierWidget::m_Language
	String_t* ___m_Language_18;
	// System.Collections.Generic.List`1<IBM.Watson.DeveloperCloud.Widgets.NaturalLanguageClassifierWidget/ClassEventMapping> IBM.Watson.DeveloperCloud.Widgets.NaturalLanguageClassifierWidget::m_ClassEventList
	List_1_t2963572661 * ___m_ClassEventList_19;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> IBM.Watson.DeveloperCloud.Widgets.NaturalLanguageClassifierWidget::m_ClassEventMap
	Dictionary_2_t3943999495 * ___m_ClassEventMap_20;
	// UnityEngine.UI.Text IBM.Watson.DeveloperCloud.Widgets.NaturalLanguageClassifierWidget::m_TopClassText
	Text_t356221433 * ___m_TopClassText_21;

public:
	inline static int32_t get_offset_of_m_RecognizeInput_6() { return static_cast<int32_t>(offsetof(NaturalLanguageClassifierWidget_t2550209684, ___m_RecognizeInput_6)); }
	inline Input_t3157785889 * get_m_RecognizeInput_6() const { return ___m_RecognizeInput_6; }
	inline Input_t3157785889 ** get_address_of_m_RecognizeInput_6() { return &___m_RecognizeInput_6; }
	inline void set_m_RecognizeInput_6(Input_t3157785889 * value)
	{
		___m_RecognizeInput_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_RecognizeInput_6), value);
	}

	inline static int32_t get_offset_of_m_ClassifyOutput_7() { return static_cast<int32_t>(offsetof(NaturalLanguageClassifierWidget_t2550209684, ___m_ClassifyOutput_7)); }
	inline Output_t220081548 * get_m_ClassifyOutput_7() const { return ___m_ClassifyOutput_7; }
	inline Output_t220081548 ** get_address_of_m_ClassifyOutput_7() { return &___m_ClassifyOutput_7; }
	inline void set_m_ClassifyOutput_7(Output_t220081548 * value)
	{
		___m_ClassifyOutput_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_ClassifyOutput_7), value);
	}

	inline static int32_t get_offset_of_m_NaturalLanguageClassifier_8() { return static_cast<int32_t>(offsetof(NaturalLanguageClassifierWidget_t2550209684, ___m_NaturalLanguageClassifier_8)); }
	inline NaturalLanguageClassifier_t2492034444 * get_m_NaturalLanguageClassifier_8() const { return ___m_NaturalLanguageClassifier_8; }
	inline NaturalLanguageClassifier_t2492034444 ** get_address_of_m_NaturalLanguageClassifier_8() { return &___m_NaturalLanguageClassifier_8; }
	inline void set_m_NaturalLanguageClassifier_8(NaturalLanguageClassifier_t2492034444 * value)
	{
		___m_NaturalLanguageClassifier_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_NaturalLanguageClassifier_8), value);
	}

	inline static int32_t get_offset_of_m_Selected_9() { return static_cast<int32_t>(offsetof(NaturalLanguageClassifierWidget_t2550209684, ___m_Selected_9)); }
	inline Classifier_t1414011573 * get_m_Selected_9() const { return ___m_Selected_9; }
	inline Classifier_t1414011573 ** get_address_of_m_Selected_9() { return &___m_Selected_9; }
	inline void set_m_Selected_9(Classifier_t1414011573 * value)
	{
		___m_Selected_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Selected_9), value);
	}

	inline static int32_t get_offset_of_m_ClassifierName_10() { return static_cast<int32_t>(offsetof(NaturalLanguageClassifierWidget_t2550209684, ___m_ClassifierName_10)); }
	inline String_t* get_m_ClassifierName_10() const { return ___m_ClassifierName_10; }
	inline String_t** get_address_of_m_ClassifierName_10() { return &___m_ClassifierName_10; }
	inline void set_m_ClassifierName_10(String_t* value)
	{
		___m_ClassifierName_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_ClassifierName_10), value);
	}

	inline static int32_t get_offset_of_m_ClassifierId_11() { return static_cast<int32_t>(offsetof(NaturalLanguageClassifierWidget_t2550209684, ___m_ClassifierId_11)); }
	inline String_t* get_m_ClassifierId_11() const { return ___m_ClassifierId_11; }
	inline String_t** get_address_of_m_ClassifierId_11() { return &___m_ClassifierId_11; }
	inline void set_m_ClassifierId_11(String_t* value)
	{
		___m_ClassifierId_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_ClassifierId_11), value);
	}

	inline static int32_t get_offset_of_m_MinWordConfidence_12() { return static_cast<int32_t>(offsetof(NaturalLanguageClassifierWidget_t2550209684, ___m_MinWordConfidence_12)); }
	inline float get_m_MinWordConfidence_12() const { return ___m_MinWordConfidence_12; }
	inline float* get_address_of_m_MinWordConfidence_12() { return &___m_MinWordConfidence_12; }
	inline void set_m_MinWordConfidence_12(float value)
	{
		___m_MinWordConfidence_12 = value;
	}

	inline static int32_t get_offset_of_m_MinWordConfidenceDelta_13() { return static_cast<int32_t>(offsetof(NaturalLanguageClassifierWidget_t2550209684, ___m_MinWordConfidenceDelta_13)); }
	inline float get_m_MinWordConfidenceDelta_13() const { return ___m_MinWordConfidenceDelta_13; }
	inline float* get_address_of_m_MinWordConfidenceDelta_13() { return &___m_MinWordConfidenceDelta_13; }
	inline void set_m_MinWordConfidenceDelta_13(float value)
	{
		___m_MinWordConfidenceDelta_13 = value;
	}

	inline static int32_t get_offset_of_m_IgnoreWordConfidence_14() { return static_cast<int32_t>(offsetof(NaturalLanguageClassifierWidget_t2550209684, ___m_IgnoreWordConfidence_14)); }
	inline float get_m_IgnoreWordConfidence_14() const { return ___m_IgnoreWordConfidence_14; }
	inline float* get_address_of_m_IgnoreWordConfidence_14() { return &___m_IgnoreWordConfidence_14; }
	inline void set_m_IgnoreWordConfidence_14(float value)
	{
		___m_IgnoreWordConfidence_14 = value;
	}

	inline static int32_t get_offset_of_m_IgnoreWordConfidenceDelta_15() { return static_cast<int32_t>(offsetof(NaturalLanguageClassifierWidget_t2550209684, ___m_IgnoreWordConfidenceDelta_15)); }
	inline float get_m_IgnoreWordConfidenceDelta_15() const { return ___m_IgnoreWordConfidenceDelta_15; }
	inline float* get_address_of_m_IgnoreWordConfidenceDelta_15() { return &___m_IgnoreWordConfidenceDelta_15; }
	inline void set_m_IgnoreWordConfidenceDelta_15(float value)
	{
		___m_IgnoreWordConfidenceDelta_15 = value;
	}

	inline static int32_t get_offset_of_m_MinClassEventConfidence_16() { return static_cast<int32_t>(offsetof(NaturalLanguageClassifierWidget_t2550209684, ___m_MinClassEventConfidence_16)); }
	inline float get_m_MinClassEventConfidence_16() const { return ___m_MinClassEventConfidence_16; }
	inline float* get_address_of_m_MinClassEventConfidence_16() { return &___m_MinClassEventConfidence_16; }
	inline void set_m_MinClassEventConfidence_16(float value)
	{
		___m_MinClassEventConfidence_16 = value;
	}

	inline static int32_t get_offset_of_m_MinClassEventConfidenceDelta_17() { return static_cast<int32_t>(offsetof(NaturalLanguageClassifierWidget_t2550209684, ___m_MinClassEventConfidenceDelta_17)); }
	inline float get_m_MinClassEventConfidenceDelta_17() const { return ___m_MinClassEventConfidenceDelta_17; }
	inline float* get_address_of_m_MinClassEventConfidenceDelta_17() { return &___m_MinClassEventConfidenceDelta_17; }
	inline void set_m_MinClassEventConfidenceDelta_17(float value)
	{
		___m_MinClassEventConfidenceDelta_17 = value;
	}

	inline static int32_t get_offset_of_m_Language_18() { return static_cast<int32_t>(offsetof(NaturalLanguageClassifierWidget_t2550209684, ___m_Language_18)); }
	inline String_t* get_m_Language_18() const { return ___m_Language_18; }
	inline String_t** get_address_of_m_Language_18() { return &___m_Language_18; }
	inline void set_m_Language_18(String_t* value)
	{
		___m_Language_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_Language_18), value);
	}

	inline static int32_t get_offset_of_m_ClassEventList_19() { return static_cast<int32_t>(offsetof(NaturalLanguageClassifierWidget_t2550209684, ___m_ClassEventList_19)); }
	inline List_1_t2963572661 * get_m_ClassEventList_19() const { return ___m_ClassEventList_19; }
	inline List_1_t2963572661 ** get_address_of_m_ClassEventList_19() { return &___m_ClassEventList_19; }
	inline void set_m_ClassEventList_19(List_1_t2963572661 * value)
	{
		___m_ClassEventList_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_ClassEventList_19), value);
	}

	inline static int32_t get_offset_of_m_ClassEventMap_20() { return static_cast<int32_t>(offsetof(NaturalLanguageClassifierWidget_t2550209684, ___m_ClassEventMap_20)); }
	inline Dictionary_2_t3943999495 * get_m_ClassEventMap_20() const { return ___m_ClassEventMap_20; }
	inline Dictionary_2_t3943999495 ** get_address_of_m_ClassEventMap_20() { return &___m_ClassEventMap_20; }
	inline void set_m_ClassEventMap_20(Dictionary_2_t3943999495 * value)
	{
		___m_ClassEventMap_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_ClassEventMap_20), value);
	}

	inline static int32_t get_offset_of_m_TopClassText_21() { return static_cast<int32_t>(offsetof(NaturalLanguageClassifierWidget_t2550209684, ___m_TopClassText_21)); }
	inline Text_t356221433 * get_m_TopClassText_21() const { return ___m_TopClassText_21; }
	inline Text_t356221433 ** get_address_of_m_TopClassText_21() { return &___m_TopClassText_21; }
	inline void set_m_TopClassText_21(Text_t356221433 * value)
	{
		___m_TopClassText_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_TopClassText_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATURALLANGUAGECLASSIFIERWIDGET_T2550209684_H
#ifndef SPEECHDISPLAYWIDGET_T1126703566_H
#define SPEECHDISPLAYWIDGET_T1126703566_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Widgets.SpeechDisplayWidget
struct  SpeechDisplayWidget_t1126703566  : public Widget_t3624771850
{
public:
	// IBM.Watson.DeveloperCloud.Widgets.Widget/Input IBM.Watson.DeveloperCloud.Widgets.SpeechDisplayWidget::m_SpeechInput
	Input_t3157785889 * ___m_SpeechInput_6;
	// System.Boolean IBM.Watson.DeveloperCloud.Widgets.SpeechDisplayWidget::m_ContinuousText
	bool ___m_ContinuousText_7;
	// UnityEngine.UI.Text IBM.Watson.DeveloperCloud.Widgets.SpeechDisplayWidget::m_Output
	Text_t356221433 * ___m_Output_8;
	// UnityEngine.UI.InputField IBM.Watson.DeveloperCloud.Widgets.SpeechDisplayWidget::m_OutputAsInputField
	InputField_t1631627530 * ___m_OutputAsInputField_9;
	// UnityEngine.UI.Text IBM.Watson.DeveloperCloud.Widgets.SpeechDisplayWidget::m_OutputStatus
	Text_t356221433 * ___m_OutputStatus_10;
	// System.Single IBM.Watson.DeveloperCloud.Widgets.SpeechDisplayWidget::m_MinConfidenceToShow
	float ___m_MinConfidenceToShow_11;
	// System.String IBM.Watson.DeveloperCloud.Widgets.SpeechDisplayWidget::m_PreviousOutputTextWithStatus
	String_t* ___m_PreviousOutputTextWithStatus_12;
	// System.String IBM.Watson.DeveloperCloud.Widgets.SpeechDisplayWidget::m_PreviousOutputText
	String_t* ___m_PreviousOutputText_13;
	// System.Single IBM.Watson.DeveloperCloud.Widgets.SpeechDisplayWidget::m_ThresholdTimeFromLastInput
	float ___m_ThresholdTimeFromLastInput_14;
	// System.Single IBM.Watson.DeveloperCloud.Widgets.SpeechDisplayWidget::m_TimeAtLastInterim
	float ___m_TimeAtLastInterim_15;

public:
	inline static int32_t get_offset_of_m_SpeechInput_6() { return static_cast<int32_t>(offsetof(SpeechDisplayWidget_t1126703566, ___m_SpeechInput_6)); }
	inline Input_t3157785889 * get_m_SpeechInput_6() const { return ___m_SpeechInput_6; }
	inline Input_t3157785889 ** get_address_of_m_SpeechInput_6() { return &___m_SpeechInput_6; }
	inline void set_m_SpeechInput_6(Input_t3157785889 * value)
	{
		___m_SpeechInput_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_SpeechInput_6), value);
	}

	inline static int32_t get_offset_of_m_ContinuousText_7() { return static_cast<int32_t>(offsetof(SpeechDisplayWidget_t1126703566, ___m_ContinuousText_7)); }
	inline bool get_m_ContinuousText_7() const { return ___m_ContinuousText_7; }
	inline bool* get_address_of_m_ContinuousText_7() { return &___m_ContinuousText_7; }
	inline void set_m_ContinuousText_7(bool value)
	{
		___m_ContinuousText_7 = value;
	}

	inline static int32_t get_offset_of_m_Output_8() { return static_cast<int32_t>(offsetof(SpeechDisplayWidget_t1126703566, ___m_Output_8)); }
	inline Text_t356221433 * get_m_Output_8() const { return ___m_Output_8; }
	inline Text_t356221433 ** get_address_of_m_Output_8() { return &___m_Output_8; }
	inline void set_m_Output_8(Text_t356221433 * value)
	{
		___m_Output_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_Output_8), value);
	}

	inline static int32_t get_offset_of_m_OutputAsInputField_9() { return static_cast<int32_t>(offsetof(SpeechDisplayWidget_t1126703566, ___m_OutputAsInputField_9)); }
	inline InputField_t1631627530 * get_m_OutputAsInputField_9() const { return ___m_OutputAsInputField_9; }
	inline InputField_t1631627530 ** get_address_of_m_OutputAsInputField_9() { return &___m_OutputAsInputField_9; }
	inline void set_m_OutputAsInputField_9(InputField_t1631627530 * value)
	{
		___m_OutputAsInputField_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_OutputAsInputField_9), value);
	}

	inline static int32_t get_offset_of_m_OutputStatus_10() { return static_cast<int32_t>(offsetof(SpeechDisplayWidget_t1126703566, ___m_OutputStatus_10)); }
	inline Text_t356221433 * get_m_OutputStatus_10() const { return ___m_OutputStatus_10; }
	inline Text_t356221433 ** get_address_of_m_OutputStatus_10() { return &___m_OutputStatus_10; }
	inline void set_m_OutputStatus_10(Text_t356221433 * value)
	{
		___m_OutputStatus_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_OutputStatus_10), value);
	}

	inline static int32_t get_offset_of_m_MinConfidenceToShow_11() { return static_cast<int32_t>(offsetof(SpeechDisplayWidget_t1126703566, ___m_MinConfidenceToShow_11)); }
	inline float get_m_MinConfidenceToShow_11() const { return ___m_MinConfidenceToShow_11; }
	inline float* get_address_of_m_MinConfidenceToShow_11() { return &___m_MinConfidenceToShow_11; }
	inline void set_m_MinConfidenceToShow_11(float value)
	{
		___m_MinConfidenceToShow_11 = value;
	}

	inline static int32_t get_offset_of_m_PreviousOutputTextWithStatus_12() { return static_cast<int32_t>(offsetof(SpeechDisplayWidget_t1126703566, ___m_PreviousOutputTextWithStatus_12)); }
	inline String_t* get_m_PreviousOutputTextWithStatus_12() const { return ___m_PreviousOutputTextWithStatus_12; }
	inline String_t** get_address_of_m_PreviousOutputTextWithStatus_12() { return &___m_PreviousOutputTextWithStatus_12; }
	inline void set_m_PreviousOutputTextWithStatus_12(String_t* value)
	{
		___m_PreviousOutputTextWithStatus_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_PreviousOutputTextWithStatus_12), value);
	}

	inline static int32_t get_offset_of_m_PreviousOutputText_13() { return static_cast<int32_t>(offsetof(SpeechDisplayWidget_t1126703566, ___m_PreviousOutputText_13)); }
	inline String_t* get_m_PreviousOutputText_13() const { return ___m_PreviousOutputText_13; }
	inline String_t** get_address_of_m_PreviousOutputText_13() { return &___m_PreviousOutputText_13; }
	inline void set_m_PreviousOutputText_13(String_t* value)
	{
		___m_PreviousOutputText_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_PreviousOutputText_13), value);
	}

	inline static int32_t get_offset_of_m_ThresholdTimeFromLastInput_14() { return static_cast<int32_t>(offsetof(SpeechDisplayWidget_t1126703566, ___m_ThresholdTimeFromLastInput_14)); }
	inline float get_m_ThresholdTimeFromLastInput_14() const { return ___m_ThresholdTimeFromLastInput_14; }
	inline float* get_address_of_m_ThresholdTimeFromLastInput_14() { return &___m_ThresholdTimeFromLastInput_14; }
	inline void set_m_ThresholdTimeFromLastInput_14(float value)
	{
		___m_ThresholdTimeFromLastInput_14 = value;
	}

	inline static int32_t get_offset_of_m_TimeAtLastInterim_15() { return static_cast<int32_t>(offsetof(SpeechDisplayWidget_t1126703566, ___m_TimeAtLastInterim_15)); }
	inline float get_m_TimeAtLastInterim_15() const { return ___m_TimeAtLastInterim_15; }
	inline float* get_address_of_m_TimeAtLastInterim_15() { return &___m_TimeAtLastInterim_15; }
	inline void set_m_TimeAtLastInterim_15(float value)
	{
		___m_TimeAtLastInterim_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPEECHDISPLAYWIDGET_T1126703566_H
#ifndef TEXTTOSPEECHWIDGET_T2096692646_H
#define TEXTTOSPEECHWIDGET_T2096692646_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Widgets.TextToSpeechWidget
struct  TextToSpeechWidget_t2096692646  : public Widget_t3624771850
{
public:
	// IBM.Watson.DeveloperCloud.Widgets.Widget/Input IBM.Watson.DeveloperCloud.Widgets.TextToSpeechWidget::m_TextInput
	Input_t3157785889 * ___m_TextInput_6;
	// IBM.Watson.DeveloperCloud.Widgets.Widget/Input IBM.Watson.DeveloperCloud.Widgets.TextToSpeechWidget::m_VoiceInput
	Input_t3157785889 * ___m_VoiceInput_7;
	// IBM.Watson.DeveloperCloud.Widgets.Widget/Output IBM.Watson.DeveloperCloud.Widgets.TextToSpeechWidget::m_Speaking
	Output_t220081548 * ___m_Speaking_8;
	// IBM.Watson.DeveloperCloud.Widgets.Widget/Output IBM.Watson.DeveloperCloud.Widgets.TextToSpeechWidget::m_DisableMic
	Output_t220081548 * ___m_DisableMic_9;
	// IBM.Watson.DeveloperCloud.Widgets.Widget/Output IBM.Watson.DeveloperCloud.Widgets.TextToSpeechWidget::m_LevelOut
	Output_t220081548 * ___m_LevelOut_10;
	// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech IBM.Watson.DeveloperCloud.Widgets.TextToSpeechWidget::m_TextToSpeech
	TextToSpeech_t3349357562 * ___m_TextToSpeech_11;
	// System.Single IBM.Watson.DeveloperCloud.Widgets.TextToSpeechWidget::m_LevelOutInterval
	float ___m_LevelOutInterval_12;
	// System.Single IBM.Watson.DeveloperCloud.Widgets.TextToSpeechWidget::m_LevelOutputModifier
	float ___m_LevelOutputModifier_13;
	// UnityEngine.UI.Button IBM.Watson.DeveloperCloud.Widgets.TextToSpeechWidget::m_TextToSpeechButton
	Button_t2872111280 * ___m_TextToSpeechButton_14;
	// UnityEngine.UI.InputField IBM.Watson.DeveloperCloud.Widgets.TextToSpeechWidget::m_Input
	InputField_t1631627530 * ___m_Input_15;
	// UnityEngine.UI.Text IBM.Watson.DeveloperCloud.Widgets.TextToSpeechWidget::m_StatusText
	Text_t356221433 * ___m_StatusText_16;
	// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.VoiceType IBM.Watson.DeveloperCloud.Widgets.TextToSpeechWidget::m_Voice
	int32_t ___m_Voice_17;
	// System.Boolean IBM.Watson.DeveloperCloud.Widgets.TextToSpeechWidget::m_UsePost
	bool ___m_UsePost_18;
	// UnityEngine.AudioSource IBM.Watson.DeveloperCloud.Widgets.TextToSpeechWidget::m_Source
	AudioSource_t1135106623 * ___m_Source_19;
	// System.Int32 IBM.Watson.DeveloperCloud.Widgets.TextToSpeechWidget::m_LastPlayPos
	int32_t ___m_LastPlayPos_20;
	// System.Collections.Generic.Queue`1<IBM.Watson.DeveloperCloud.Widgets.TextToSpeechWidget/Speech> IBM.Watson.DeveloperCloud.Widgets.TextToSpeechWidget::m_SpeechQueue
	Queue_1_t1482798040 * ___m_SpeechQueue_21;
	// IBM.Watson.DeveloperCloud.Widgets.TextToSpeechWidget/Speech IBM.Watson.DeveloperCloud.Widgets.TextToSpeechWidget::m_ActiveSpeech
	Speech_t1663141205 * ___m_ActiveSpeech_22;

public:
	inline static int32_t get_offset_of_m_TextInput_6() { return static_cast<int32_t>(offsetof(TextToSpeechWidget_t2096692646, ___m_TextInput_6)); }
	inline Input_t3157785889 * get_m_TextInput_6() const { return ___m_TextInput_6; }
	inline Input_t3157785889 ** get_address_of_m_TextInput_6() { return &___m_TextInput_6; }
	inline void set_m_TextInput_6(Input_t3157785889 * value)
	{
		___m_TextInput_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextInput_6), value);
	}

	inline static int32_t get_offset_of_m_VoiceInput_7() { return static_cast<int32_t>(offsetof(TextToSpeechWidget_t2096692646, ___m_VoiceInput_7)); }
	inline Input_t3157785889 * get_m_VoiceInput_7() const { return ___m_VoiceInput_7; }
	inline Input_t3157785889 ** get_address_of_m_VoiceInput_7() { return &___m_VoiceInput_7; }
	inline void set_m_VoiceInput_7(Input_t3157785889 * value)
	{
		___m_VoiceInput_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_VoiceInput_7), value);
	}

	inline static int32_t get_offset_of_m_Speaking_8() { return static_cast<int32_t>(offsetof(TextToSpeechWidget_t2096692646, ___m_Speaking_8)); }
	inline Output_t220081548 * get_m_Speaking_8() const { return ___m_Speaking_8; }
	inline Output_t220081548 ** get_address_of_m_Speaking_8() { return &___m_Speaking_8; }
	inline void set_m_Speaking_8(Output_t220081548 * value)
	{
		___m_Speaking_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_Speaking_8), value);
	}

	inline static int32_t get_offset_of_m_DisableMic_9() { return static_cast<int32_t>(offsetof(TextToSpeechWidget_t2096692646, ___m_DisableMic_9)); }
	inline Output_t220081548 * get_m_DisableMic_9() const { return ___m_DisableMic_9; }
	inline Output_t220081548 ** get_address_of_m_DisableMic_9() { return &___m_DisableMic_9; }
	inline void set_m_DisableMic_9(Output_t220081548 * value)
	{
		___m_DisableMic_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_DisableMic_9), value);
	}

	inline static int32_t get_offset_of_m_LevelOut_10() { return static_cast<int32_t>(offsetof(TextToSpeechWidget_t2096692646, ___m_LevelOut_10)); }
	inline Output_t220081548 * get_m_LevelOut_10() const { return ___m_LevelOut_10; }
	inline Output_t220081548 ** get_address_of_m_LevelOut_10() { return &___m_LevelOut_10; }
	inline void set_m_LevelOut_10(Output_t220081548 * value)
	{
		___m_LevelOut_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_LevelOut_10), value);
	}

	inline static int32_t get_offset_of_m_TextToSpeech_11() { return static_cast<int32_t>(offsetof(TextToSpeechWidget_t2096692646, ___m_TextToSpeech_11)); }
	inline TextToSpeech_t3349357562 * get_m_TextToSpeech_11() const { return ___m_TextToSpeech_11; }
	inline TextToSpeech_t3349357562 ** get_address_of_m_TextToSpeech_11() { return &___m_TextToSpeech_11; }
	inline void set_m_TextToSpeech_11(TextToSpeech_t3349357562 * value)
	{
		___m_TextToSpeech_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextToSpeech_11), value);
	}

	inline static int32_t get_offset_of_m_LevelOutInterval_12() { return static_cast<int32_t>(offsetof(TextToSpeechWidget_t2096692646, ___m_LevelOutInterval_12)); }
	inline float get_m_LevelOutInterval_12() const { return ___m_LevelOutInterval_12; }
	inline float* get_address_of_m_LevelOutInterval_12() { return &___m_LevelOutInterval_12; }
	inline void set_m_LevelOutInterval_12(float value)
	{
		___m_LevelOutInterval_12 = value;
	}

	inline static int32_t get_offset_of_m_LevelOutputModifier_13() { return static_cast<int32_t>(offsetof(TextToSpeechWidget_t2096692646, ___m_LevelOutputModifier_13)); }
	inline float get_m_LevelOutputModifier_13() const { return ___m_LevelOutputModifier_13; }
	inline float* get_address_of_m_LevelOutputModifier_13() { return &___m_LevelOutputModifier_13; }
	inline void set_m_LevelOutputModifier_13(float value)
	{
		___m_LevelOutputModifier_13 = value;
	}

	inline static int32_t get_offset_of_m_TextToSpeechButton_14() { return static_cast<int32_t>(offsetof(TextToSpeechWidget_t2096692646, ___m_TextToSpeechButton_14)); }
	inline Button_t2872111280 * get_m_TextToSpeechButton_14() const { return ___m_TextToSpeechButton_14; }
	inline Button_t2872111280 ** get_address_of_m_TextToSpeechButton_14() { return &___m_TextToSpeechButton_14; }
	inline void set_m_TextToSpeechButton_14(Button_t2872111280 * value)
	{
		___m_TextToSpeechButton_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextToSpeechButton_14), value);
	}

	inline static int32_t get_offset_of_m_Input_15() { return static_cast<int32_t>(offsetof(TextToSpeechWidget_t2096692646, ___m_Input_15)); }
	inline InputField_t1631627530 * get_m_Input_15() const { return ___m_Input_15; }
	inline InputField_t1631627530 ** get_address_of_m_Input_15() { return &___m_Input_15; }
	inline void set_m_Input_15(InputField_t1631627530 * value)
	{
		___m_Input_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_Input_15), value);
	}

	inline static int32_t get_offset_of_m_StatusText_16() { return static_cast<int32_t>(offsetof(TextToSpeechWidget_t2096692646, ___m_StatusText_16)); }
	inline Text_t356221433 * get_m_StatusText_16() const { return ___m_StatusText_16; }
	inline Text_t356221433 ** get_address_of_m_StatusText_16() { return &___m_StatusText_16; }
	inline void set_m_StatusText_16(Text_t356221433 * value)
	{
		___m_StatusText_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_StatusText_16), value);
	}

	inline static int32_t get_offset_of_m_Voice_17() { return static_cast<int32_t>(offsetof(TextToSpeechWidget_t2096692646, ___m_Voice_17)); }
	inline int32_t get_m_Voice_17() const { return ___m_Voice_17; }
	inline int32_t* get_address_of_m_Voice_17() { return &___m_Voice_17; }
	inline void set_m_Voice_17(int32_t value)
	{
		___m_Voice_17 = value;
	}

	inline static int32_t get_offset_of_m_UsePost_18() { return static_cast<int32_t>(offsetof(TextToSpeechWidget_t2096692646, ___m_UsePost_18)); }
	inline bool get_m_UsePost_18() const { return ___m_UsePost_18; }
	inline bool* get_address_of_m_UsePost_18() { return &___m_UsePost_18; }
	inline void set_m_UsePost_18(bool value)
	{
		___m_UsePost_18 = value;
	}

	inline static int32_t get_offset_of_m_Source_19() { return static_cast<int32_t>(offsetof(TextToSpeechWidget_t2096692646, ___m_Source_19)); }
	inline AudioSource_t1135106623 * get_m_Source_19() const { return ___m_Source_19; }
	inline AudioSource_t1135106623 ** get_address_of_m_Source_19() { return &___m_Source_19; }
	inline void set_m_Source_19(AudioSource_t1135106623 * value)
	{
		___m_Source_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_Source_19), value);
	}

	inline static int32_t get_offset_of_m_LastPlayPos_20() { return static_cast<int32_t>(offsetof(TextToSpeechWidget_t2096692646, ___m_LastPlayPos_20)); }
	inline int32_t get_m_LastPlayPos_20() const { return ___m_LastPlayPos_20; }
	inline int32_t* get_address_of_m_LastPlayPos_20() { return &___m_LastPlayPos_20; }
	inline void set_m_LastPlayPos_20(int32_t value)
	{
		___m_LastPlayPos_20 = value;
	}

	inline static int32_t get_offset_of_m_SpeechQueue_21() { return static_cast<int32_t>(offsetof(TextToSpeechWidget_t2096692646, ___m_SpeechQueue_21)); }
	inline Queue_1_t1482798040 * get_m_SpeechQueue_21() const { return ___m_SpeechQueue_21; }
	inline Queue_1_t1482798040 ** get_address_of_m_SpeechQueue_21() { return &___m_SpeechQueue_21; }
	inline void set_m_SpeechQueue_21(Queue_1_t1482798040 * value)
	{
		___m_SpeechQueue_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_SpeechQueue_21), value);
	}

	inline static int32_t get_offset_of_m_ActiveSpeech_22() { return static_cast<int32_t>(offsetof(TextToSpeechWidget_t2096692646, ___m_ActiveSpeech_22)); }
	inline Speech_t1663141205 * get_m_ActiveSpeech_22() const { return ___m_ActiveSpeech_22; }
	inline Speech_t1663141205 ** get_address_of_m_ActiveSpeech_22() { return &___m_ActiveSpeech_22; }
	inline void set_m_ActiveSpeech_22(Speech_t1663141205 * value)
	{
		___m_ActiveSpeech_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_ActiveSpeech_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTTOSPEECHWIDGET_T2096692646_H
#ifndef WEBCAMWIDGET_T58556181_H
#define WEBCAMWIDGET_T58556181_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Widgets.WebCamWidget
struct  WebCamWidget_t58556181  : public Widget_t3624771850
{
public:
	// IBM.Watson.DeveloperCloud.Widgets.Widget/Input IBM.Watson.DeveloperCloud.Widgets.WebCamWidget::m_DisableInput
	Input_t3157785889 * ___m_DisableInput_6;
	// IBM.Watson.DeveloperCloud.Widgets.Widget/Output IBM.Watson.DeveloperCloud.Widgets.WebCamWidget::m_WebCamTextureOutput
	Output_t220081548 * ___m_WebCamTextureOutput_7;
	// IBM.Watson.DeveloperCloud.Widgets.Widget/Output IBM.Watson.DeveloperCloud.Widgets.WebCamWidget::m_ActivateOutput
	Output_t220081548 * ___m_ActivateOutput_8;
	// System.Boolean IBM.Watson.DeveloperCloud.Widgets.WebCamWidget::m_Active
	bool ___m_Active_9;
	// System.Boolean IBM.Watson.DeveloperCloud.Widgets.WebCamWidget::m_Disabled
	bool ___m_Disabled_10;
	// System.Boolean IBM.Watson.DeveloperCloud.Widgets.WebCamWidget::m_Failure
	bool ___m_Failure_11;
	// System.DateTime IBM.Watson.DeveloperCloud.Widgets.WebCamWidget::m_LastFailure
	DateTime_t693205669  ___m_LastFailure_12;
	// System.Boolean IBM.Watson.DeveloperCloud.Widgets.WebCamWidget::m_ActivateOnStart
	bool ___m_ActivateOnStart_13;
	// System.Int32 IBM.Watson.DeveloperCloud.Widgets.WebCamWidget::m_RequestedWidth
	int32_t ___m_RequestedWidth_14;
	// System.Int32 IBM.Watson.DeveloperCloud.Widgets.WebCamWidget::m_RequestedHeight
	int32_t ___m_RequestedHeight_15;
	// System.Int32 IBM.Watson.DeveloperCloud.Widgets.WebCamWidget::m_RequestedFPS
	int32_t ___m_RequestedFPS_16;
	// UnityEngine.UI.Text IBM.Watson.DeveloperCloud.Widgets.WebCamWidget::m_StatusText
	Text_t356221433 * ___m_StatusText_17;
	// System.Int32 IBM.Watson.DeveloperCloud.Widgets.WebCamWidget::m_RecordingRoutine
	int32_t ___m_RecordingRoutine_18;
	// UnityEngine.WebCamTexture IBM.Watson.DeveloperCloud.Widgets.WebCamWidget::m_WebCamTexture
	WebCamTexture_t1079476942 * ___m_WebCamTexture_19;
	// System.Int32 IBM.Watson.DeveloperCloud.Widgets.WebCamWidget::m_WebCamIndex
	int32_t ___m_WebCamIndex_20;

public:
	inline static int32_t get_offset_of_m_DisableInput_6() { return static_cast<int32_t>(offsetof(WebCamWidget_t58556181, ___m_DisableInput_6)); }
	inline Input_t3157785889 * get_m_DisableInput_6() const { return ___m_DisableInput_6; }
	inline Input_t3157785889 ** get_address_of_m_DisableInput_6() { return &___m_DisableInput_6; }
	inline void set_m_DisableInput_6(Input_t3157785889 * value)
	{
		___m_DisableInput_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_DisableInput_6), value);
	}

	inline static int32_t get_offset_of_m_WebCamTextureOutput_7() { return static_cast<int32_t>(offsetof(WebCamWidget_t58556181, ___m_WebCamTextureOutput_7)); }
	inline Output_t220081548 * get_m_WebCamTextureOutput_7() const { return ___m_WebCamTextureOutput_7; }
	inline Output_t220081548 ** get_address_of_m_WebCamTextureOutput_7() { return &___m_WebCamTextureOutput_7; }
	inline void set_m_WebCamTextureOutput_7(Output_t220081548 * value)
	{
		___m_WebCamTextureOutput_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_WebCamTextureOutput_7), value);
	}

	inline static int32_t get_offset_of_m_ActivateOutput_8() { return static_cast<int32_t>(offsetof(WebCamWidget_t58556181, ___m_ActivateOutput_8)); }
	inline Output_t220081548 * get_m_ActivateOutput_8() const { return ___m_ActivateOutput_8; }
	inline Output_t220081548 ** get_address_of_m_ActivateOutput_8() { return &___m_ActivateOutput_8; }
	inline void set_m_ActivateOutput_8(Output_t220081548 * value)
	{
		___m_ActivateOutput_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_ActivateOutput_8), value);
	}

	inline static int32_t get_offset_of_m_Active_9() { return static_cast<int32_t>(offsetof(WebCamWidget_t58556181, ___m_Active_9)); }
	inline bool get_m_Active_9() const { return ___m_Active_9; }
	inline bool* get_address_of_m_Active_9() { return &___m_Active_9; }
	inline void set_m_Active_9(bool value)
	{
		___m_Active_9 = value;
	}

	inline static int32_t get_offset_of_m_Disabled_10() { return static_cast<int32_t>(offsetof(WebCamWidget_t58556181, ___m_Disabled_10)); }
	inline bool get_m_Disabled_10() const { return ___m_Disabled_10; }
	inline bool* get_address_of_m_Disabled_10() { return &___m_Disabled_10; }
	inline void set_m_Disabled_10(bool value)
	{
		___m_Disabled_10 = value;
	}

	inline static int32_t get_offset_of_m_Failure_11() { return static_cast<int32_t>(offsetof(WebCamWidget_t58556181, ___m_Failure_11)); }
	inline bool get_m_Failure_11() const { return ___m_Failure_11; }
	inline bool* get_address_of_m_Failure_11() { return &___m_Failure_11; }
	inline void set_m_Failure_11(bool value)
	{
		___m_Failure_11 = value;
	}

	inline static int32_t get_offset_of_m_LastFailure_12() { return static_cast<int32_t>(offsetof(WebCamWidget_t58556181, ___m_LastFailure_12)); }
	inline DateTime_t693205669  get_m_LastFailure_12() const { return ___m_LastFailure_12; }
	inline DateTime_t693205669 * get_address_of_m_LastFailure_12() { return &___m_LastFailure_12; }
	inline void set_m_LastFailure_12(DateTime_t693205669  value)
	{
		___m_LastFailure_12 = value;
	}

	inline static int32_t get_offset_of_m_ActivateOnStart_13() { return static_cast<int32_t>(offsetof(WebCamWidget_t58556181, ___m_ActivateOnStart_13)); }
	inline bool get_m_ActivateOnStart_13() const { return ___m_ActivateOnStart_13; }
	inline bool* get_address_of_m_ActivateOnStart_13() { return &___m_ActivateOnStart_13; }
	inline void set_m_ActivateOnStart_13(bool value)
	{
		___m_ActivateOnStart_13 = value;
	}

	inline static int32_t get_offset_of_m_RequestedWidth_14() { return static_cast<int32_t>(offsetof(WebCamWidget_t58556181, ___m_RequestedWidth_14)); }
	inline int32_t get_m_RequestedWidth_14() const { return ___m_RequestedWidth_14; }
	inline int32_t* get_address_of_m_RequestedWidth_14() { return &___m_RequestedWidth_14; }
	inline void set_m_RequestedWidth_14(int32_t value)
	{
		___m_RequestedWidth_14 = value;
	}

	inline static int32_t get_offset_of_m_RequestedHeight_15() { return static_cast<int32_t>(offsetof(WebCamWidget_t58556181, ___m_RequestedHeight_15)); }
	inline int32_t get_m_RequestedHeight_15() const { return ___m_RequestedHeight_15; }
	inline int32_t* get_address_of_m_RequestedHeight_15() { return &___m_RequestedHeight_15; }
	inline void set_m_RequestedHeight_15(int32_t value)
	{
		___m_RequestedHeight_15 = value;
	}

	inline static int32_t get_offset_of_m_RequestedFPS_16() { return static_cast<int32_t>(offsetof(WebCamWidget_t58556181, ___m_RequestedFPS_16)); }
	inline int32_t get_m_RequestedFPS_16() const { return ___m_RequestedFPS_16; }
	inline int32_t* get_address_of_m_RequestedFPS_16() { return &___m_RequestedFPS_16; }
	inline void set_m_RequestedFPS_16(int32_t value)
	{
		___m_RequestedFPS_16 = value;
	}

	inline static int32_t get_offset_of_m_StatusText_17() { return static_cast<int32_t>(offsetof(WebCamWidget_t58556181, ___m_StatusText_17)); }
	inline Text_t356221433 * get_m_StatusText_17() const { return ___m_StatusText_17; }
	inline Text_t356221433 ** get_address_of_m_StatusText_17() { return &___m_StatusText_17; }
	inline void set_m_StatusText_17(Text_t356221433 * value)
	{
		___m_StatusText_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_StatusText_17), value);
	}

	inline static int32_t get_offset_of_m_RecordingRoutine_18() { return static_cast<int32_t>(offsetof(WebCamWidget_t58556181, ___m_RecordingRoutine_18)); }
	inline int32_t get_m_RecordingRoutine_18() const { return ___m_RecordingRoutine_18; }
	inline int32_t* get_address_of_m_RecordingRoutine_18() { return &___m_RecordingRoutine_18; }
	inline void set_m_RecordingRoutine_18(int32_t value)
	{
		___m_RecordingRoutine_18 = value;
	}

	inline static int32_t get_offset_of_m_WebCamTexture_19() { return static_cast<int32_t>(offsetof(WebCamWidget_t58556181, ___m_WebCamTexture_19)); }
	inline WebCamTexture_t1079476942 * get_m_WebCamTexture_19() const { return ___m_WebCamTexture_19; }
	inline WebCamTexture_t1079476942 ** get_address_of_m_WebCamTexture_19() { return &___m_WebCamTexture_19; }
	inline void set_m_WebCamTexture_19(WebCamTexture_t1079476942 * value)
	{
		___m_WebCamTexture_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_WebCamTexture_19), value);
	}

	inline static int32_t get_offset_of_m_WebCamIndex_20() { return static_cast<int32_t>(offsetof(WebCamWidget_t58556181, ___m_WebCamIndex_20)); }
	inline int32_t get_m_WebCamIndex_20() const { return ___m_WebCamIndex_20; }
	inline int32_t* get_address_of_m_WebCamIndex_20() { return &___m_WebCamIndex_20; }
	inline void set_m_WebCamIndex_20(int32_t value)
	{
		___m_WebCamIndex_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBCAMWIDGET_T58556181_H
#ifndef WEBCAMDISPLAYWIDGET_T3233697851_H
#define WEBCAMDISPLAYWIDGET_T3233697851_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Widgets.WebCamDisplayWidget
struct  WebCamDisplayWidget_t3233697851  : public Widget_t3624771850
{
public:
	// IBM.Watson.DeveloperCloud.Widgets.Widget/Input IBM.Watson.DeveloperCloud.Widgets.WebCamDisplayWidget::m_WebCamTextureInput
	Input_t3157785889 * ___m_WebCamTextureInput_6;
	// UnityEngine.UI.RawImage IBM.Watson.DeveloperCloud.Widgets.WebCamDisplayWidget::m_RawImage
	RawImage_t2749640213 * ___m_RawImage_7;
	// UnityEngine.Material IBM.Watson.DeveloperCloud.Widgets.WebCamDisplayWidget::m_Material
	Material_t193706927 * ___m_Material_8;
	// UnityEngine.WebCamTexture IBM.Watson.DeveloperCloud.Widgets.WebCamDisplayWidget::m_WebCamTexture
	WebCamTexture_t1079476942 * ___m_WebCamTexture_9;
	// System.Int32 IBM.Watson.DeveloperCloud.Widgets.WebCamDisplayWidget::m_RequestedWidth
	int32_t ___m_RequestedWidth_10;
	// System.Int32 IBM.Watson.DeveloperCloud.Widgets.WebCamDisplayWidget::m_RequestedHeight
	int32_t ___m_RequestedHeight_11;
	// System.Int32 IBM.Watson.DeveloperCloud.Widgets.WebCamDisplayWidget::m_RequestedFPS
	int32_t ___m_RequestedFPS_12;

public:
	inline static int32_t get_offset_of_m_WebCamTextureInput_6() { return static_cast<int32_t>(offsetof(WebCamDisplayWidget_t3233697851, ___m_WebCamTextureInput_6)); }
	inline Input_t3157785889 * get_m_WebCamTextureInput_6() const { return ___m_WebCamTextureInput_6; }
	inline Input_t3157785889 ** get_address_of_m_WebCamTextureInput_6() { return &___m_WebCamTextureInput_6; }
	inline void set_m_WebCamTextureInput_6(Input_t3157785889 * value)
	{
		___m_WebCamTextureInput_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_WebCamTextureInput_6), value);
	}

	inline static int32_t get_offset_of_m_RawImage_7() { return static_cast<int32_t>(offsetof(WebCamDisplayWidget_t3233697851, ___m_RawImage_7)); }
	inline RawImage_t2749640213 * get_m_RawImage_7() const { return ___m_RawImage_7; }
	inline RawImage_t2749640213 ** get_address_of_m_RawImage_7() { return &___m_RawImage_7; }
	inline void set_m_RawImage_7(RawImage_t2749640213 * value)
	{
		___m_RawImage_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_RawImage_7), value);
	}

	inline static int32_t get_offset_of_m_Material_8() { return static_cast<int32_t>(offsetof(WebCamDisplayWidget_t3233697851, ___m_Material_8)); }
	inline Material_t193706927 * get_m_Material_8() const { return ___m_Material_8; }
	inline Material_t193706927 ** get_address_of_m_Material_8() { return &___m_Material_8; }
	inline void set_m_Material_8(Material_t193706927 * value)
	{
		___m_Material_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_8), value);
	}

	inline static int32_t get_offset_of_m_WebCamTexture_9() { return static_cast<int32_t>(offsetof(WebCamDisplayWidget_t3233697851, ___m_WebCamTexture_9)); }
	inline WebCamTexture_t1079476942 * get_m_WebCamTexture_9() const { return ___m_WebCamTexture_9; }
	inline WebCamTexture_t1079476942 ** get_address_of_m_WebCamTexture_9() { return &___m_WebCamTexture_9; }
	inline void set_m_WebCamTexture_9(WebCamTexture_t1079476942 * value)
	{
		___m_WebCamTexture_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_WebCamTexture_9), value);
	}

	inline static int32_t get_offset_of_m_RequestedWidth_10() { return static_cast<int32_t>(offsetof(WebCamDisplayWidget_t3233697851, ___m_RequestedWidth_10)); }
	inline int32_t get_m_RequestedWidth_10() const { return ___m_RequestedWidth_10; }
	inline int32_t* get_address_of_m_RequestedWidth_10() { return &___m_RequestedWidth_10; }
	inline void set_m_RequestedWidth_10(int32_t value)
	{
		___m_RequestedWidth_10 = value;
	}

	inline static int32_t get_offset_of_m_RequestedHeight_11() { return static_cast<int32_t>(offsetof(WebCamDisplayWidget_t3233697851, ___m_RequestedHeight_11)); }
	inline int32_t get_m_RequestedHeight_11() const { return ___m_RequestedHeight_11; }
	inline int32_t* get_address_of_m_RequestedHeight_11() { return &___m_RequestedHeight_11; }
	inline void set_m_RequestedHeight_11(int32_t value)
	{
		___m_RequestedHeight_11 = value;
	}

	inline static int32_t get_offset_of_m_RequestedFPS_12() { return static_cast<int32_t>(offsetof(WebCamDisplayWidget_t3233697851, ___m_RequestedFPS_12)); }
	inline int32_t get_m_RequestedFPS_12() const { return ___m_RequestedFPS_12; }
	inline int32_t* get_address_of_m_RequestedFPS_12() { return &___m_RequestedFPS_12; }
	inline void set_m_RequestedFPS_12(int32_t value)
	{
		___m_RequestedFPS_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBCAMDISPLAYWIDGET_T3233697851_H
#ifndef TOUCHWIDGET_T3160682287_H
#define TOUCHWIDGET_T3160682287_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Widgets.TouchWidget
struct  TouchWidget_t3160682287  : public Widget_t3624771850
{
public:
	// System.Collections.Generic.List`1<IBM.Watson.DeveloperCloud.Widgets.TouchWidget/TapEventMapping> IBM.Watson.DeveloperCloud.Widgets.TouchWidget::m_TapMappings
	List_1_t1677282647 * ___m_TapMappings_6;
	// System.Collections.Generic.List`1<IBM.Watson.DeveloperCloud.Widgets.TouchWidget/FullScreenDragEventMapping> IBM.Watson.DeveloperCloud.Widgets.TouchWidget::m_FullScreenDragMappings
	List_1_t3241548797 * ___m_FullScreenDragMappings_7;

public:
	inline static int32_t get_offset_of_m_TapMappings_6() { return static_cast<int32_t>(offsetof(TouchWidget_t3160682287, ___m_TapMappings_6)); }
	inline List_1_t1677282647 * get_m_TapMappings_6() const { return ___m_TapMappings_6; }
	inline List_1_t1677282647 ** get_address_of_m_TapMappings_6() { return &___m_TapMappings_6; }
	inline void set_m_TapMappings_6(List_1_t1677282647 * value)
	{
		___m_TapMappings_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_TapMappings_6), value);
	}

	inline static int32_t get_offset_of_m_FullScreenDragMappings_7() { return static_cast<int32_t>(offsetof(TouchWidget_t3160682287, ___m_FullScreenDragMappings_7)); }
	inline List_1_t3241548797 * get_m_FullScreenDragMappings_7() const { return ___m_FullScreenDragMappings_7; }
	inline List_1_t3241548797 ** get_address_of_m_FullScreenDragMappings_7() { return &___m_FullScreenDragMappings_7; }
	inline void set_m_FullScreenDragMappings_7(List_1_t3241548797 * value)
	{
		___m_FullScreenDragMappings_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_FullScreenDragMappings_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHWIDGET_T3160682287_H
#ifndef LANGUAGETRANSLATORWIDGET_T907487206_H
#define LANGUAGETRANSLATORWIDGET_T907487206_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Widgets.LanguageTranslatorWidget
struct  LanguageTranslatorWidget_t907487206  : public Widget_t3624771850
{
public:
	// IBM.Watson.DeveloperCloud.Widgets.Widget/Input IBM.Watson.DeveloperCloud.Widgets.LanguageTranslatorWidget::m_SpeechInput
	Input_t3157785889 * ___m_SpeechInput_6;
	// IBM.Watson.DeveloperCloud.Widgets.Widget/Output IBM.Watson.DeveloperCloud.Widgets.LanguageTranslatorWidget::m_RecognizeLanguageOutput
	Output_t220081548 * ___m_RecognizeLanguageOutput_7;
	// IBM.Watson.DeveloperCloud.Widgets.Widget/Output IBM.Watson.DeveloperCloud.Widgets.LanguageTranslatorWidget::m_SpeechOutput
	Output_t220081548 * ___m_SpeechOutput_8;
	// IBM.Watson.DeveloperCloud.Widgets.Widget/Output IBM.Watson.DeveloperCloud.Widgets.LanguageTranslatorWidget::m_VoiceOutput
	Output_t220081548 * ___m_VoiceOutput_9;
	// IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.LanguageTranslation IBM.Watson.DeveloperCloud.Widgets.LanguageTranslatorWidget::m_Translate
	LanguageTranslation_t3099415401 * ___m_Translate_10;
	// System.String IBM.Watson.DeveloperCloud.Widgets.LanguageTranslatorWidget::m_SourceLanguage
	String_t* ___m_SourceLanguage_11;
	// System.String IBM.Watson.DeveloperCloud.Widgets.LanguageTranslatorWidget::m_TargetLanguage
	String_t* ___m_TargetLanguage_12;
	// UnityEngine.UI.InputField IBM.Watson.DeveloperCloud.Widgets.LanguageTranslatorWidget::m_Input
	InputField_t1631627530 * ___m_Input_13;
	// UnityEngine.UI.Text IBM.Watson.DeveloperCloud.Widgets.LanguageTranslatorWidget::m_Output
	Text_t356221433 * ___m_Output_14;
	// UnityEngine.UI.Dropdown IBM.Watson.DeveloperCloud.Widgets.LanguageTranslatorWidget::m_DropDownSourceLanguage
	Dropdown_t1985816271 * ___m_DropDownSourceLanguage_15;
	// UnityEngine.UI.Dropdown IBM.Watson.DeveloperCloud.Widgets.LanguageTranslatorWidget::m_DropDownTargetLanguage
	Dropdown_t1985816271 * ___m_DropDownTargetLanguage_16;
	// System.String IBM.Watson.DeveloperCloud.Widgets.LanguageTranslatorWidget::m_DefaultDomainToUse
	String_t* ___m_DefaultDomainToUse_17;
	// System.String IBM.Watson.DeveloperCloud.Widgets.LanguageTranslatorWidget::m_DetectLanguageName
	String_t* ___m_DetectLanguageName_18;
	// System.String IBM.Watson.DeveloperCloud.Widgets.LanguageTranslatorWidget::m_DetectLanguageID
	String_t* ___m_DetectLanguageID_19;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> IBM.Watson.DeveloperCloud.Widgets.LanguageTranslatorWidget::m_LanguageIDToName
	Dictionary_2_t3943999495 * ___m_LanguageIDToName_20;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> IBM.Watson.DeveloperCloud.Widgets.LanguageTranslatorWidget::m_LanguageNameToID
	Dictionary_2_t3943999495 * ___m_LanguageNameToID_21;
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.String>> IBM.Watson.DeveloperCloud.Widgets.LanguageTranslatorWidget::m_LanguageToTranslate
	Dictionary_2_t3313120627 * ___m_LanguageToTranslate_22;
	// System.String[] IBM.Watson.DeveloperCloud.Widgets.LanguageTranslatorWidget::m_Languages
	StringU5BU5D_t1642385972* ___m_Languages_23;
	// System.String IBM.Watson.DeveloperCloud.Widgets.LanguageTranslatorWidget::m_TranslateText
	String_t* ___m_TranslateText_24;

public:
	inline static int32_t get_offset_of_m_SpeechInput_6() { return static_cast<int32_t>(offsetof(LanguageTranslatorWidget_t907487206, ___m_SpeechInput_6)); }
	inline Input_t3157785889 * get_m_SpeechInput_6() const { return ___m_SpeechInput_6; }
	inline Input_t3157785889 ** get_address_of_m_SpeechInput_6() { return &___m_SpeechInput_6; }
	inline void set_m_SpeechInput_6(Input_t3157785889 * value)
	{
		___m_SpeechInput_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_SpeechInput_6), value);
	}

	inline static int32_t get_offset_of_m_RecognizeLanguageOutput_7() { return static_cast<int32_t>(offsetof(LanguageTranslatorWidget_t907487206, ___m_RecognizeLanguageOutput_7)); }
	inline Output_t220081548 * get_m_RecognizeLanguageOutput_7() const { return ___m_RecognizeLanguageOutput_7; }
	inline Output_t220081548 ** get_address_of_m_RecognizeLanguageOutput_7() { return &___m_RecognizeLanguageOutput_7; }
	inline void set_m_RecognizeLanguageOutput_7(Output_t220081548 * value)
	{
		___m_RecognizeLanguageOutput_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_RecognizeLanguageOutput_7), value);
	}

	inline static int32_t get_offset_of_m_SpeechOutput_8() { return static_cast<int32_t>(offsetof(LanguageTranslatorWidget_t907487206, ___m_SpeechOutput_8)); }
	inline Output_t220081548 * get_m_SpeechOutput_8() const { return ___m_SpeechOutput_8; }
	inline Output_t220081548 ** get_address_of_m_SpeechOutput_8() { return &___m_SpeechOutput_8; }
	inline void set_m_SpeechOutput_8(Output_t220081548 * value)
	{
		___m_SpeechOutput_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_SpeechOutput_8), value);
	}

	inline static int32_t get_offset_of_m_VoiceOutput_9() { return static_cast<int32_t>(offsetof(LanguageTranslatorWidget_t907487206, ___m_VoiceOutput_9)); }
	inline Output_t220081548 * get_m_VoiceOutput_9() const { return ___m_VoiceOutput_9; }
	inline Output_t220081548 ** get_address_of_m_VoiceOutput_9() { return &___m_VoiceOutput_9; }
	inline void set_m_VoiceOutput_9(Output_t220081548 * value)
	{
		___m_VoiceOutput_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_VoiceOutput_9), value);
	}

	inline static int32_t get_offset_of_m_Translate_10() { return static_cast<int32_t>(offsetof(LanguageTranslatorWidget_t907487206, ___m_Translate_10)); }
	inline LanguageTranslation_t3099415401 * get_m_Translate_10() const { return ___m_Translate_10; }
	inline LanguageTranslation_t3099415401 ** get_address_of_m_Translate_10() { return &___m_Translate_10; }
	inline void set_m_Translate_10(LanguageTranslation_t3099415401 * value)
	{
		___m_Translate_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_Translate_10), value);
	}

	inline static int32_t get_offset_of_m_SourceLanguage_11() { return static_cast<int32_t>(offsetof(LanguageTranslatorWidget_t907487206, ___m_SourceLanguage_11)); }
	inline String_t* get_m_SourceLanguage_11() const { return ___m_SourceLanguage_11; }
	inline String_t** get_address_of_m_SourceLanguage_11() { return &___m_SourceLanguage_11; }
	inline void set_m_SourceLanguage_11(String_t* value)
	{
		___m_SourceLanguage_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_SourceLanguage_11), value);
	}

	inline static int32_t get_offset_of_m_TargetLanguage_12() { return static_cast<int32_t>(offsetof(LanguageTranslatorWidget_t907487206, ___m_TargetLanguage_12)); }
	inline String_t* get_m_TargetLanguage_12() const { return ___m_TargetLanguage_12; }
	inline String_t** get_address_of_m_TargetLanguage_12() { return &___m_TargetLanguage_12; }
	inline void set_m_TargetLanguage_12(String_t* value)
	{
		___m_TargetLanguage_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_TargetLanguage_12), value);
	}

	inline static int32_t get_offset_of_m_Input_13() { return static_cast<int32_t>(offsetof(LanguageTranslatorWidget_t907487206, ___m_Input_13)); }
	inline InputField_t1631627530 * get_m_Input_13() const { return ___m_Input_13; }
	inline InputField_t1631627530 ** get_address_of_m_Input_13() { return &___m_Input_13; }
	inline void set_m_Input_13(InputField_t1631627530 * value)
	{
		___m_Input_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_Input_13), value);
	}

	inline static int32_t get_offset_of_m_Output_14() { return static_cast<int32_t>(offsetof(LanguageTranslatorWidget_t907487206, ___m_Output_14)); }
	inline Text_t356221433 * get_m_Output_14() const { return ___m_Output_14; }
	inline Text_t356221433 ** get_address_of_m_Output_14() { return &___m_Output_14; }
	inline void set_m_Output_14(Text_t356221433 * value)
	{
		___m_Output_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_Output_14), value);
	}

	inline static int32_t get_offset_of_m_DropDownSourceLanguage_15() { return static_cast<int32_t>(offsetof(LanguageTranslatorWidget_t907487206, ___m_DropDownSourceLanguage_15)); }
	inline Dropdown_t1985816271 * get_m_DropDownSourceLanguage_15() const { return ___m_DropDownSourceLanguage_15; }
	inline Dropdown_t1985816271 ** get_address_of_m_DropDownSourceLanguage_15() { return &___m_DropDownSourceLanguage_15; }
	inline void set_m_DropDownSourceLanguage_15(Dropdown_t1985816271 * value)
	{
		___m_DropDownSourceLanguage_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_DropDownSourceLanguage_15), value);
	}

	inline static int32_t get_offset_of_m_DropDownTargetLanguage_16() { return static_cast<int32_t>(offsetof(LanguageTranslatorWidget_t907487206, ___m_DropDownTargetLanguage_16)); }
	inline Dropdown_t1985816271 * get_m_DropDownTargetLanguage_16() const { return ___m_DropDownTargetLanguage_16; }
	inline Dropdown_t1985816271 ** get_address_of_m_DropDownTargetLanguage_16() { return &___m_DropDownTargetLanguage_16; }
	inline void set_m_DropDownTargetLanguage_16(Dropdown_t1985816271 * value)
	{
		___m_DropDownTargetLanguage_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_DropDownTargetLanguage_16), value);
	}

	inline static int32_t get_offset_of_m_DefaultDomainToUse_17() { return static_cast<int32_t>(offsetof(LanguageTranslatorWidget_t907487206, ___m_DefaultDomainToUse_17)); }
	inline String_t* get_m_DefaultDomainToUse_17() const { return ___m_DefaultDomainToUse_17; }
	inline String_t** get_address_of_m_DefaultDomainToUse_17() { return &___m_DefaultDomainToUse_17; }
	inline void set_m_DefaultDomainToUse_17(String_t* value)
	{
		___m_DefaultDomainToUse_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_DefaultDomainToUse_17), value);
	}

	inline static int32_t get_offset_of_m_DetectLanguageName_18() { return static_cast<int32_t>(offsetof(LanguageTranslatorWidget_t907487206, ___m_DetectLanguageName_18)); }
	inline String_t* get_m_DetectLanguageName_18() const { return ___m_DetectLanguageName_18; }
	inline String_t** get_address_of_m_DetectLanguageName_18() { return &___m_DetectLanguageName_18; }
	inline void set_m_DetectLanguageName_18(String_t* value)
	{
		___m_DetectLanguageName_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_DetectLanguageName_18), value);
	}

	inline static int32_t get_offset_of_m_DetectLanguageID_19() { return static_cast<int32_t>(offsetof(LanguageTranslatorWidget_t907487206, ___m_DetectLanguageID_19)); }
	inline String_t* get_m_DetectLanguageID_19() const { return ___m_DetectLanguageID_19; }
	inline String_t** get_address_of_m_DetectLanguageID_19() { return &___m_DetectLanguageID_19; }
	inline void set_m_DetectLanguageID_19(String_t* value)
	{
		___m_DetectLanguageID_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_DetectLanguageID_19), value);
	}

	inline static int32_t get_offset_of_m_LanguageIDToName_20() { return static_cast<int32_t>(offsetof(LanguageTranslatorWidget_t907487206, ___m_LanguageIDToName_20)); }
	inline Dictionary_2_t3943999495 * get_m_LanguageIDToName_20() const { return ___m_LanguageIDToName_20; }
	inline Dictionary_2_t3943999495 ** get_address_of_m_LanguageIDToName_20() { return &___m_LanguageIDToName_20; }
	inline void set_m_LanguageIDToName_20(Dictionary_2_t3943999495 * value)
	{
		___m_LanguageIDToName_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_LanguageIDToName_20), value);
	}

	inline static int32_t get_offset_of_m_LanguageNameToID_21() { return static_cast<int32_t>(offsetof(LanguageTranslatorWidget_t907487206, ___m_LanguageNameToID_21)); }
	inline Dictionary_2_t3943999495 * get_m_LanguageNameToID_21() const { return ___m_LanguageNameToID_21; }
	inline Dictionary_2_t3943999495 ** get_address_of_m_LanguageNameToID_21() { return &___m_LanguageNameToID_21; }
	inline void set_m_LanguageNameToID_21(Dictionary_2_t3943999495 * value)
	{
		___m_LanguageNameToID_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_LanguageNameToID_21), value);
	}

	inline static int32_t get_offset_of_m_LanguageToTranslate_22() { return static_cast<int32_t>(offsetof(LanguageTranslatorWidget_t907487206, ___m_LanguageToTranslate_22)); }
	inline Dictionary_2_t3313120627 * get_m_LanguageToTranslate_22() const { return ___m_LanguageToTranslate_22; }
	inline Dictionary_2_t3313120627 ** get_address_of_m_LanguageToTranslate_22() { return &___m_LanguageToTranslate_22; }
	inline void set_m_LanguageToTranslate_22(Dictionary_2_t3313120627 * value)
	{
		___m_LanguageToTranslate_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_LanguageToTranslate_22), value);
	}

	inline static int32_t get_offset_of_m_Languages_23() { return static_cast<int32_t>(offsetof(LanguageTranslatorWidget_t907487206, ___m_Languages_23)); }
	inline StringU5BU5D_t1642385972* get_m_Languages_23() const { return ___m_Languages_23; }
	inline StringU5BU5D_t1642385972** get_address_of_m_Languages_23() { return &___m_Languages_23; }
	inline void set_m_Languages_23(StringU5BU5D_t1642385972* value)
	{
		___m_Languages_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_Languages_23), value);
	}

	inline static int32_t get_offset_of_m_TranslateText_24() { return static_cast<int32_t>(offsetof(LanguageTranslatorWidget_t907487206, ___m_TranslateText_24)); }
	inline String_t* get_m_TranslateText_24() const { return ___m_TranslateText_24; }
	inline String_t** get_address_of_m_TranslateText_24() { return &___m_TranslateText_24; }
	inline void set_m_TranslateText_24(String_t* value)
	{
		___m_TranslateText_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_TranslateText_24), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LANGUAGETRANSLATORWIDGET_T907487206_H
#ifndef EVENTWIDGET_T528106534_H
#define EVENTWIDGET_T528106534_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Widgets.EventWidget
struct  EventWidget_t528106534  : public Widget_t3624771850
{
public:
	// System.Collections.Generic.List`1<IBM.Watson.DeveloperCloud.Widgets.EventWidget/Mapping> IBM.Watson.DeveloperCloud.Widgets.EventWidget::m_Mappings
	List_1_t812061395 * ___m_Mappings_6;

public:
	inline static int32_t get_offset_of_m_Mappings_6() { return static_cast<int32_t>(offsetof(EventWidget_t528106534, ___m_Mappings_6)); }
	inline List_1_t812061395 * get_m_Mappings_6() const { return ___m_Mappings_6; }
	inline List_1_t812061395 ** get_address_of_m_Mappings_6() { return &___m_Mappings_6; }
	inline void set_m_Mappings_6(List_1_t812061395 * value)
	{
		___m_Mappings_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Mappings_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTWIDGET_T528106534_H
#ifndef KEYBOARDWIDGET_T3474172959_H
#define KEYBOARDWIDGET_T3474172959_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Widgets.KeyboardWidget
struct  KeyboardWidget_t3474172959  : public Widget_t3624771850
{
public:
	// System.Collections.Generic.List`1<IBM.Watson.DeveloperCloud.Widgets.KeyboardWidget/Mapping> IBM.Watson.DeveloperCloud.Widgets.KeyboardWidget::m_Mappings
	List_1_t2493597736 * ___m_Mappings_6;

public:
	inline static int32_t get_offset_of_m_Mappings_6() { return static_cast<int32_t>(offsetof(KeyboardWidget_t3474172959, ___m_Mappings_6)); }
	inline List_1_t2493597736 * get_m_Mappings_6() const { return ___m_Mappings_6; }
	inline List_1_t2493597736 ** get_address_of_m_Mappings_6() { return &___m_Mappings_6; }
	inline void set_m_Mappings_6(List_1_t2493597736 * value)
	{
		___m_Mappings_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Mappings_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYBOARDWIDGET_T3474172959_H
#ifndef MICROPHONEWIDGET_T2442369682_H
#define MICROPHONEWIDGET_T2442369682_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Widgets.MicrophoneWidget
struct  MicrophoneWidget_t2442369682  : public Widget_t3624771850
{
public:
	// IBM.Watson.DeveloperCloud.Widgets.Widget/Input IBM.Watson.DeveloperCloud.Widgets.MicrophoneWidget::m_DisableInput
	Input_t3157785889 * ___m_DisableInput_6;
	// IBM.Watson.DeveloperCloud.Widgets.Widget/Output IBM.Watson.DeveloperCloud.Widgets.MicrophoneWidget::m_AudioOutput
	Output_t220081548 * ___m_AudioOutput_7;
	// IBM.Watson.DeveloperCloud.Widgets.Widget/Output IBM.Watson.DeveloperCloud.Widgets.MicrophoneWidget::m_LevelOutput
	Output_t220081548 * ___m_LevelOutput_8;
	// IBM.Watson.DeveloperCloud.Widgets.Widget/Output IBM.Watson.DeveloperCloud.Widgets.MicrophoneWidget::m_ActivateOutput
	Output_t220081548 * ___m_ActivateOutput_9;
	// System.Boolean IBM.Watson.DeveloperCloud.Widgets.MicrophoneWidget::m_Active
	bool ___m_Active_10;
	// System.Boolean IBM.Watson.DeveloperCloud.Widgets.MicrophoneWidget::m_Disabled
	bool ___m_Disabled_11;
	// System.Boolean IBM.Watson.DeveloperCloud.Widgets.MicrophoneWidget::m_Failure
	bool ___m_Failure_12;
	// System.DateTime IBM.Watson.DeveloperCloud.Widgets.MicrophoneWidget::m_LastFailure
	DateTime_t693205669  ___m_LastFailure_13;
	// System.Boolean IBM.Watson.DeveloperCloud.Widgets.MicrophoneWidget::m_ActivateOnStart
	bool ___m_ActivateOnStart_14;
	// System.Int32 IBM.Watson.DeveloperCloud.Widgets.MicrophoneWidget::m_RecordingBufferSize
	int32_t ___m_RecordingBufferSize_15;
	// System.Int32 IBM.Watson.DeveloperCloud.Widgets.MicrophoneWidget::m_RecordingHZ
	int32_t ___m_RecordingHZ_16;
	// System.String IBM.Watson.DeveloperCloud.Widgets.MicrophoneWidget::m_MicrophoneID
	String_t* ___m_MicrophoneID_17;
	// System.Single IBM.Watson.DeveloperCloud.Widgets.MicrophoneWidget::m_LevelOutputInterval
	float ___m_LevelOutputInterval_18;
	// System.Single IBM.Watson.DeveloperCloud.Widgets.MicrophoneWidget::m_LevelOutputModifier
	float ___m_LevelOutputModifier_19;
	// System.Boolean IBM.Watson.DeveloperCloud.Widgets.MicrophoneWidget::m_PlaybackRecording
	bool ___m_PlaybackRecording_20;
	// UnityEngine.UI.Text IBM.Watson.DeveloperCloud.Widgets.MicrophoneWidget::m_StatusText
	Text_t356221433 * ___m_StatusText_21;
	// System.Int32 IBM.Watson.DeveloperCloud.Widgets.MicrophoneWidget::m_RecordingRoutine
	int32_t ___m_RecordingRoutine_22;
	// UnityEngine.AudioClip IBM.Watson.DeveloperCloud.Widgets.MicrophoneWidget::m_Recording
	AudioClip_t1932558630 * ___m_Recording_23;
	// System.Collections.Generic.List`1<UnityEngine.AudioClip> IBM.Watson.DeveloperCloud.Widgets.MicrophoneWidget::m_Playback
	List_1_t1301679762 * ___m_Playback_24;

public:
	inline static int32_t get_offset_of_m_DisableInput_6() { return static_cast<int32_t>(offsetof(MicrophoneWidget_t2442369682, ___m_DisableInput_6)); }
	inline Input_t3157785889 * get_m_DisableInput_6() const { return ___m_DisableInput_6; }
	inline Input_t3157785889 ** get_address_of_m_DisableInput_6() { return &___m_DisableInput_6; }
	inline void set_m_DisableInput_6(Input_t3157785889 * value)
	{
		___m_DisableInput_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_DisableInput_6), value);
	}

	inline static int32_t get_offset_of_m_AudioOutput_7() { return static_cast<int32_t>(offsetof(MicrophoneWidget_t2442369682, ___m_AudioOutput_7)); }
	inline Output_t220081548 * get_m_AudioOutput_7() const { return ___m_AudioOutput_7; }
	inline Output_t220081548 ** get_address_of_m_AudioOutput_7() { return &___m_AudioOutput_7; }
	inline void set_m_AudioOutput_7(Output_t220081548 * value)
	{
		___m_AudioOutput_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_AudioOutput_7), value);
	}

	inline static int32_t get_offset_of_m_LevelOutput_8() { return static_cast<int32_t>(offsetof(MicrophoneWidget_t2442369682, ___m_LevelOutput_8)); }
	inline Output_t220081548 * get_m_LevelOutput_8() const { return ___m_LevelOutput_8; }
	inline Output_t220081548 ** get_address_of_m_LevelOutput_8() { return &___m_LevelOutput_8; }
	inline void set_m_LevelOutput_8(Output_t220081548 * value)
	{
		___m_LevelOutput_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_LevelOutput_8), value);
	}

	inline static int32_t get_offset_of_m_ActivateOutput_9() { return static_cast<int32_t>(offsetof(MicrophoneWidget_t2442369682, ___m_ActivateOutput_9)); }
	inline Output_t220081548 * get_m_ActivateOutput_9() const { return ___m_ActivateOutput_9; }
	inline Output_t220081548 ** get_address_of_m_ActivateOutput_9() { return &___m_ActivateOutput_9; }
	inline void set_m_ActivateOutput_9(Output_t220081548 * value)
	{
		___m_ActivateOutput_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_ActivateOutput_9), value);
	}

	inline static int32_t get_offset_of_m_Active_10() { return static_cast<int32_t>(offsetof(MicrophoneWidget_t2442369682, ___m_Active_10)); }
	inline bool get_m_Active_10() const { return ___m_Active_10; }
	inline bool* get_address_of_m_Active_10() { return &___m_Active_10; }
	inline void set_m_Active_10(bool value)
	{
		___m_Active_10 = value;
	}

	inline static int32_t get_offset_of_m_Disabled_11() { return static_cast<int32_t>(offsetof(MicrophoneWidget_t2442369682, ___m_Disabled_11)); }
	inline bool get_m_Disabled_11() const { return ___m_Disabled_11; }
	inline bool* get_address_of_m_Disabled_11() { return &___m_Disabled_11; }
	inline void set_m_Disabled_11(bool value)
	{
		___m_Disabled_11 = value;
	}

	inline static int32_t get_offset_of_m_Failure_12() { return static_cast<int32_t>(offsetof(MicrophoneWidget_t2442369682, ___m_Failure_12)); }
	inline bool get_m_Failure_12() const { return ___m_Failure_12; }
	inline bool* get_address_of_m_Failure_12() { return &___m_Failure_12; }
	inline void set_m_Failure_12(bool value)
	{
		___m_Failure_12 = value;
	}

	inline static int32_t get_offset_of_m_LastFailure_13() { return static_cast<int32_t>(offsetof(MicrophoneWidget_t2442369682, ___m_LastFailure_13)); }
	inline DateTime_t693205669  get_m_LastFailure_13() const { return ___m_LastFailure_13; }
	inline DateTime_t693205669 * get_address_of_m_LastFailure_13() { return &___m_LastFailure_13; }
	inline void set_m_LastFailure_13(DateTime_t693205669  value)
	{
		___m_LastFailure_13 = value;
	}

	inline static int32_t get_offset_of_m_ActivateOnStart_14() { return static_cast<int32_t>(offsetof(MicrophoneWidget_t2442369682, ___m_ActivateOnStart_14)); }
	inline bool get_m_ActivateOnStart_14() const { return ___m_ActivateOnStart_14; }
	inline bool* get_address_of_m_ActivateOnStart_14() { return &___m_ActivateOnStart_14; }
	inline void set_m_ActivateOnStart_14(bool value)
	{
		___m_ActivateOnStart_14 = value;
	}

	inline static int32_t get_offset_of_m_RecordingBufferSize_15() { return static_cast<int32_t>(offsetof(MicrophoneWidget_t2442369682, ___m_RecordingBufferSize_15)); }
	inline int32_t get_m_RecordingBufferSize_15() const { return ___m_RecordingBufferSize_15; }
	inline int32_t* get_address_of_m_RecordingBufferSize_15() { return &___m_RecordingBufferSize_15; }
	inline void set_m_RecordingBufferSize_15(int32_t value)
	{
		___m_RecordingBufferSize_15 = value;
	}

	inline static int32_t get_offset_of_m_RecordingHZ_16() { return static_cast<int32_t>(offsetof(MicrophoneWidget_t2442369682, ___m_RecordingHZ_16)); }
	inline int32_t get_m_RecordingHZ_16() const { return ___m_RecordingHZ_16; }
	inline int32_t* get_address_of_m_RecordingHZ_16() { return &___m_RecordingHZ_16; }
	inline void set_m_RecordingHZ_16(int32_t value)
	{
		___m_RecordingHZ_16 = value;
	}

	inline static int32_t get_offset_of_m_MicrophoneID_17() { return static_cast<int32_t>(offsetof(MicrophoneWidget_t2442369682, ___m_MicrophoneID_17)); }
	inline String_t* get_m_MicrophoneID_17() const { return ___m_MicrophoneID_17; }
	inline String_t** get_address_of_m_MicrophoneID_17() { return &___m_MicrophoneID_17; }
	inline void set_m_MicrophoneID_17(String_t* value)
	{
		___m_MicrophoneID_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_MicrophoneID_17), value);
	}

	inline static int32_t get_offset_of_m_LevelOutputInterval_18() { return static_cast<int32_t>(offsetof(MicrophoneWidget_t2442369682, ___m_LevelOutputInterval_18)); }
	inline float get_m_LevelOutputInterval_18() const { return ___m_LevelOutputInterval_18; }
	inline float* get_address_of_m_LevelOutputInterval_18() { return &___m_LevelOutputInterval_18; }
	inline void set_m_LevelOutputInterval_18(float value)
	{
		___m_LevelOutputInterval_18 = value;
	}

	inline static int32_t get_offset_of_m_LevelOutputModifier_19() { return static_cast<int32_t>(offsetof(MicrophoneWidget_t2442369682, ___m_LevelOutputModifier_19)); }
	inline float get_m_LevelOutputModifier_19() const { return ___m_LevelOutputModifier_19; }
	inline float* get_address_of_m_LevelOutputModifier_19() { return &___m_LevelOutputModifier_19; }
	inline void set_m_LevelOutputModifier_19(float value)
	{
		___m_LevelOutputModifier_19 = value;
	}

	inline static int32_t get_offset_of_m_PlaybackRecording_20() { return static_cast<int32_t>(offsetof(MicrophoneWidget_t2442369682, ___m_PlaybackRecording_20)); }
	inline bool get_m_PlaybackRecording_20() const { return ___m_PlaybackRecording_20; }
	inline bool* get_address_of_m_PlaybackRecording_20() { return &___m_PlaybackRecording_20; }
	inline void set_m_PlaybackRecording_20(bool value)
	{
		___m_PlaybackRecording_20 = value;
	}

	inline static int32_t get_offset_of_m_StatusText_21() { return static_cast<int32_t>(offsetof(MicrophoneWidget_t2442369682, ___m_StatusText_21)); }
	inline Text_t356221433 * get_m_StatusText_21() const { return ___m_StatusText_21; }
	inline Text_t356221433 ** get_address_of_m_StatusText_21() { return &___m_StatusText_21; }
	inline void set_m_StatusText_21(Text_t356221433 * value)
	{
		___m_StatusText_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_StatusText_21), value);
	}

	inline static int32_t get_offset_of_m_RecordingRoutine_22() { return static_cast<int32_t>(offsetof(MicrophoneWidget_t2442369682, ___m_RecordingRoutine_22)); }
	inline int32_t get_m_RecordingRoutine_22() const { return ___m_RecordingRoutine_22; }
	inline int32_t* get_address_of_m_RecordingRoutine_22() { return &___m_RecordingRoutine_22; }
	inline void set_m_RecordingRoutine_22(int32_t value)
	{
		___m_RecordingRoutine_22 = value;
	}

	inline static int32_t get_offset_of_m_Recording_23() { return static_cast<int32_t>(offsetof(MicrophoneWidget_t2442369682, ___m_Recording_23)); }
	inline AudioClip_t1932558630 * get_m_Recording_23() const { return ___m_Recording_23; }
	inline AudioClip_t1932558630 ** get_address_of_m_Recording_23() { return &___m_Recording_23; }
	inline void set_m_Recording_23(AudioClip_t1932558630 * value)
	{
		___m_Recording_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_Recording_23), value);
	}

	inline static int32_t get_offset_of_m_Playback_24() { return static_cast<int32_t>(offsetof(MicrophoneWidget_t2442369682, ___m_Playback_24)); }
	inline List_1_t1301679762 * get_m_Playback_24() const { return ___m_Playback_24; }
	inline List_1_t1301679762 ** get_address_of_m_Playback_24() { return &___m_Playback_24; }
	inline void set_m_Playback_24(List_1_t1301679762 * value)
	{
		___m_Playback_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_Playback_24), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MICROPHONEWIDGET_T2442369682_H
#ifndef ACTIVATEWIDGET_T3114088099_H
#define ACTIVATEWIDGET_T3114088099_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Widgets.ActivateWidget
struct  ActivateWidget_t3114088099  : public Widget_t3624771850
{
public:
	// IBM.Watson.DeveloperCloud.Widgets.Widget/Output IBM.Watson.DeveloperCloud.Widgets.ActivateWidget::m_ActivateOutput
	Output_t220081548 * ___m_ActivateOutput_6;
	// System.Boolean IBM.Watson.DeveloperCloud.Widgets.ActivateWidget::m_SendValue
	bool ___m_SendValue_7;

public:
	inline static int32_t get_offset_of_m_ActivateOutput_6() { return static_cast<int32_t>(offsetof(ActivateWidget_t3114088099, ___m_ActivateOutput_6)); }
	inline Output_t220081548 * get_m_ActivateOutput_6() const { return ___m_ActivateOutput_6; }
	inline Output_t220081548 ** get_address_of_m_ActivateOutput_6() { return &___m_ActivateOutput_6; }
	inline void set_m_ActivateOutput_6(Output_t220081548 * value)
	{
		___m_ActivateOutput_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_ActivateOutput_6), value);
	}

	inline static int32_t get_offset_of_m_SendValue_7() { return static_cast<int32_t>(offsetof(ActivateWidget_t3114088099, ___m_SendValue_7)); }
	inline bool get_m_SendValue_7() const { return ___m_SendValue_7; }
	inline bool* get_address_of_m_SendValue_7() { return &___m_SendValue_7; }
	inline void set_m_SendValue_7(bool value)
	{
		___m_SendValue_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIVATEWIDGET_T3114088099_H
#ifndef CLASSDISPLAYWIDGET_T1127558564_H
#define CLASSDISPLAYWIDGET_T1127558564_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Widgets.ClassDisplayWidget
struct  ClassDisplayWidget_t1127558564  : public Widget_t3624771850
{
public:
	// IBM.Watson.DeveloperCloud.Widgets.Widget/Input IBM.Watson.DeveloperCloud.Widgets.ClassDisplayWidget::m_ClassInput
	Input_t3157785889 * ___m_ClassInput_6;
	// UnityEngine.UI.Text IBM.Watson.DeveloperCloud.Widgets.ClassDisplayWidget::m_ClassDisplay
	Text_t356221433 * ___m_ClassDisplay_7;

public:
	inline static int32_t get_offset_of_m_ClassInput_6() { return static_cast<int32_t>(offsetof(ClassDisplayWidget_t1127558564, ___m_ClassInput_6)); }
	inline Input_t3157785889 * get_m_ClassInput_6() const { return ___m_ClassInput_6; }
	inline Input_t3157785889 ** get_address_of_m_ClassInput_6() { return &___m_ClassInput_6; }
	inline void set_m_ClassInput_6(Input_t3157785889 * value)
	{
		___m_ClassInput_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_ClassInput_6), value);
	}

	inline static int32_t get_offset_of_m_ClassDisplay_7() { return static_cast<int32_t>(offsetof(ClassDisplayWidget_t1127558564, ___m_ClassDisplay_7)); }
	inline Text_t356221433 * get_m_ClassDisplay_7() const { return ___m_ClassDisplay_7; }
	inline Text_t356221433 ** get_address_of_m_ClassDisplay_7() { return &___m_ClassDisplay_7; }
	inline void set_m_ClassDisplay_7(Text_t356221433 * value)
	{
		___m_ClassDisplay_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_ClassDisplay_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLASSDISPLAYWIDGET_T1127558564_H
#ifndef CLASSIFIERWIDGET_T1446757817_H
#define CLASSIFIERWIDGET_T1446757817_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Widgets.ClassifierWidget
struct  ClassifierWidget_t1446757817  : public Widget_t3624771850
{
public:
	// IBM.Watson.DeveloperCloud.Widgets.Widget/Input IBM.Watson.DeveloperCloud.Widgets.ClassifierWidget::m_ClassifyInput
	Input_t3157785889 * ___m_ClassifyInput_6;
	// IBM.Watson.DeveloperCloud.Widgets.Widget/Output IBM.Watson.DeveloperCloud.Widgets.ClassifierWidget::m_ClassifyOutput
	Output_t220081548 * ___m_ClassifyOutput_7;
	// System.Collections.Generic.List`1<IBM.Watson.DeveloperCloud.Widgets.ClassifierWidget/Mapping> IBM.Watson.DeveloperCloud.Widgets.ClassifierWidget::m_Mappings
	List_1_t1971267372 * ___m_Mappings_8;

public:
	inline static int32_t get_offset_of_m_ClassifyInput_6() { return static_cast<int32_t>(offsetof(ClassifierWidget_t1446757817, ___m_ClassifyInput_6)); }
	inline Input_t3157785889 * get_m_ClassifyInput_6() const { return ___m_ClassifyInput_6; }
	inline Input_t3157785889 ** get_address_of_m_ClassifyInput_6() { return &___m_ClassifyInput_6; }
	inline void set_m_ClassifyInput_6(Input_t3157785889 * value)
	{
		___m_ClassifyInput_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_ClassifyInput_6), value);
	}

	inline static int32_t get_offset_of_m_ClassifyOutput_7() { return static_cast<int32_t>(offsetof(ClassifierWidget_t1446757817, ___m_ClassifyOutput_7)); }
	inline Output_t220081548 * get_m_ClassifyOutput_7() const { return ___m_ClassifyOutput_7; }
	inline Output_t220081548 ** get_address_of_m_ClassifyOutput_7() { return &___m_ClassifyOutput_7; }
	inline void set_m_ClassifyOutput_7(Output_t220081548 * value)
	{
		___m_ClassifyOutput_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_ClassifyOutput_7), value);
	}

	inline static int32_t get_offset_of_m_Mappings_8() { return static_cast<int32_t>(offsetof(ClassifierWidget_t1446757817, ___m_Mappings_8)); }
	inline List_1_t1971267372 * get_m_Mappings_8() const { return ___m_Mappings_8; }
	inline List_1_t1971267372 ** get_address_of_m_Mappings_8() { return &___m_Mappings_8; }
	inline void set_m_Mappings_8(List_1_t1971267372 * value)
	{
		___m_Mappings_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_Mappings_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLASSIFIERWIDGET_T1446757817_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3000 = { sizeof (NestedPrefabs_t4027735438), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3000[3] = 
{
	NestedPrefabs_t4027735438::get_offset_of_m_Prefabs_2(),
	NestedPrefabs_t4027735438::get_offset_of_m_GameObjectCreated_3(),
	NestedPrefabs_t4027735438::get_offset_of_m_SetParent_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3001 = { sizeof (Runnable_t1422227699), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3001[2] = 
{
	Runnable_t1422227699::get_offset_of_m_Routines_2(),
	Runnable_t1422227699::get_offset_of_m_NextRoutineId_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3002 = { sizeof (Routine_t1973058165), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3002[4] = 
{
	Routine_t1973058165::get_offset_of_U3CIDU3Ek__BackingField_0(),
	Routine_t1973058165::get_offset_of_U3CStopU3Ek__BackingField_1(),
	Routine_t1973058165::get_offset_of_m_bMoveNext_2(),
	Routine_t1973058165::get_offset_of_m_Enumerator_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3003 = { sizeof (SerializedDelegate_t808690627), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3003[4] = 
{
	SerializedDelegate_t808690627::get_offset_of_U3CDelegateTypeU3Ek__BackingField_0(),
	SerializedDelegate_t808690627::get_offset_of_m_Target_1(),
	SerializedDelegate_t808690627::get_offset_of_m_Component_2(),
	SerializedDelegate_t808690627::get_offset_of_m_Method_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3004 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3004[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3005 = { sizeof (TimedDestroy_t2617744245), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3005[12] = 
{
	TimedDestroy_t2617744245::get_offset_of_m_DestroyTime_2(),
	TimedDestroy_t2617744245::get_offset_of_m_ElapsedTime_3(),
	TimedDestroy_t2617744245::get_offset_of_m_TimeReachedToDestroy_4(),
	TimedDestroy_t2617744245::get_offset_of_m_AlphaFade_5(),
	TimedDestroy_t2617744245::get_offset_of_m_AlphaFadeOnAwake_6(),
	TimedDestroy_t2617744245::get_offset_of_m_FadeTime_7(),
	TimedDestroy_t2617744245::get_offset_of_m_FadeTimeOnAwake_8(),
	TimedDestroy_t2617744245::get_offset_of_m_AlphaTarget_9(),
	TimedDestroy_t2617744245::get_offset_of_m_Fading_10(),
	TimedDestroy_t2617744245::get_offset_of_m_FadeStart_11(),
	TimedDestroy_t2617744245::get_offset_of_m_InitialColor_12(),
	TimedDestroy_t2617744245::get_offset_of_m_FadeAwakeRatio_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3006 = { sizeof (TouchEventManager_t1889607896), -1, sizeof(TouchEventManager_t1889607896_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3006[14] = 
{
	TouchEventManager_t1889607896::get_offset_of_m_mainCamera_2(),
	TouchEventManager_t1889607896::get_offset_of_m_Active_3(),
	TouchEventManager_t1889607896::get_offset_of_m_TapEvents_4(),
	TouchEventManager_t1889607896::get_offset_of_m_DoubleTapEvents_5(),
	TouchEventManager_t1889607896::get_offset_of_m_DragEvents_6(),
	TouchEventManager_t1889607896::get_offset_of_m_TapGesture_7(),
	TouchEventManager_t1889607896::get_offset_of_m_DoubleTapGesture_8(),
	TouchEventManager_t1889607896::get_offset_of_m_ThreeTapGesture_9(),
	TouchEventManager_t1889607896::get_offset_of_m_OneFingerMoveGesture_10(),
	TouchEventManager_t1889607896::get_offset_of_m_TwoFingerMoveGesture_11(),
	TouchEventManager_t1889607896::get_offset_of_m_PressGesture_12(),
	TouchEventManager_t1889607896::get_offset_of_m_ReleaseGesture_13(),
	TouchEventManager_t1889607896::get_offset_of_m_LongPressGesture_14(),
	TouchEventManager_t1889607896_StaticFields::get_offset_of_sm_Instance_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3007 = { sizeof (TouchEventData_t2072067249), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3007[11] = 
{
	TouchEventData_t2072067249::get_offset_of_m_Collider_0(),
	TouchEventData_t2072067249::get_offset_of_m_Collider2D_1(),
	TouchEventData_t2072067249::get_offset_of_m_RectTransform_2(),
	TouchEventData_t2072067249::get_offset_of_m_ColliderList_3(),
	TouchEventData_t2072067249::get_offset_of_m_Collider2DList_4(),
	TouchEventData_t2072067249::get_offset_of_m_RectTransformList_5(),
	TouchEventData_t2072067249::get_offset_of_m_GameObject_6(),
	TouchEventData_t2072067249::get_offset_of_m_tapEventCallback_7(),
	TouchEventData_t2072067249::get_offset_of_m_dragEventCallback_8(),
	TouchEventData_t2072067249::get_offset_of_m_isInside_9(),
	TouchEventData_t2072067249::get_offset_of_m_SortingLayer_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3008 = { sizeof (U3CUnregisterDragEventU3Ec__AnonStorey0_t3641881935), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3008[4] = 
{
	U3CUnregisterDragEventU3Ec__AnonStorey0_t3641881935::get_offset_of_gameObjectToDrag_0(),
	U3CUnregisterDragEventU3Ec__AnonStorey0_t3641881935::get_offset_of_callback_1(),
	U3CUnregisterDragEventU3Ec__AnonStorey0_t3641881935::get_offset_of_SortingLayer_2(),
	U3CUnregisterDragEventU3Ec__AnonStorey0_t3641881935::get_offset_of_isDragInside_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3009 = { sizeof (U3COneFingerTransformedHandlerU3Ec__AnonStorey2_t3856358067), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3009[3] = 
{
	U3COneFingerTransformedHandlerU3Ec__AnonStorey2_t3856358067::get_offset_of_hitTransform3D_0(),
	U3COneFingerTransformedHandlerU3Ec__AnonStorey2_t3856358067::get_offset_of_hitTransform2D_1(),
	U3COneFingerTransformedHandlerU3Ec__AnonStorey2_t3856358067::get_offset_of_hitTransform2DEventSystem_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3010 = { sizeof (U3COneFingerTransformedHandlerU3Ec__AnonStorey1_t3856358066), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3010[1] = 
{
	U3COneFingerTransformedHandlerU3Ec__AnonStorey1_t3856358066::get_offset_of_itemRaycastResult_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3011 = { sizeof (U3CTwoFingerTransformedHandlerU3Ec__AnonStorey4_t3436378969), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3011[3] = 
{
	U3CTwoFingerTransformedHandlerU3Ec__AnonStorey4_t3436378969::get_offset_of_hitTransform3D_0(),
	U3CTwoFingerTransformedHandlerU3Ec__AnonStorey4_t3436378969::get_offset_of_hitTransform2D_1(),
	U3CTwoFingerTransformedHandlerU3Ec__AnonStorey4_t3436378969::get_offset_of_hitTransform2DEventSystem_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3012 = { sizeof (U3CTwoFingerTransformedHandlerU3Ec__AnonStorey3_t3436378966), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3012[1] = 
{
	U3CTwoFingerTransformedHandlerU3Ec__AnonStorey3_t3436378966::get_offset_of_itemRaycastResult_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3013 = { sizeof (U3CUnregisterTapEventU3Ec__AnonStorey6_t3827711990), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3013[3] = 
{
	U3CUnregisterTapEventU3Ec__AnonStorey6_t3827711990::get_offset_of_callback_0(),
	U3CUnregisterTapEventU3Ec__AnonStorey6_t3827711990::get_offset_of_SortingLayer_1(),
	U3CUnregisterTapEventU3Ec__AnonStorey6_t3827711990::get_offset_of_isTapInside_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3014 = { sizeof (U3CUnregisterTapEventU3Ec__AnonStorey5_t1098828635), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3014[2] = 
{
	U3CUnregisterTapEventU3Ec__AnonStorey5_t1098828635::get_offset_of_itemCollider_0(),
	U3CUnregisterTapEventU3Ec__AnonStorey5_t1098828635::get_offset_of_U3CU3Ef__refU246_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3015 = { sizeof (U3CUnregisterTapEventU3Ec__AnonStorey7_t2261628049), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3015[2] = 
{
	U3CUnregisterTapEventU3Ec__AnonStorey7_t2261628049::get_offset_of_itemCollider2D_0(),
	U3CUnregisterTapEventU3Ec__AnonStorey7_t2261628049::get_offset_of_U3CU3Ef__refU246_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3016 = { sizeof (U3CUnregisterTapEventU3Ec__AnonStorey8_t4278050684), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3016[2] = 
{
	U3CUnregisterTapEventU3Ec__AnonStorey8_t4278050684::get_offset_of_itemRectTransform_0(),
	U3CUnregisterTapEventU3Ec__AnonStorey8_t4278050684::get_offset_of_U3CU3Ef__refU246_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3017 = { sizeof (U3CUnregisterDoubleTapEventU3Ec__AnonStoreyA_t137061456), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3017[3] = 
{
	U3CUnregisterDoubleTapEventU3Ec__AnonStoreyA_t137061456::get_offset_of_callback_0(),
	U3CUnregisterDoubleTapEventU3Ec__AnonStoreyA_t137061456::get_offset_of_SortingLayer_1(),
	U3CUnregisterDoubleTapEventU3Ec__AnonStoreyA_t137061456::get_offset_of_isDoubleTapInside_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3018 = { sizeof (U3CUnregisterDoubleTapEventU3Ec__AnonStorey9_t4055595416), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3018[2] = 
{
	U3CUnregisterDoubleTapEventU3Ec__AnonStorey9_t4055595416::get_offset_of_itemCollider_0(),
	U3CUnregisterDoubleTapEventU3Ec__AnonStorey9_t4055595416::get_offset_of_U3CU3Ef__refU2410_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3019 = { sizeof (U3CUnregisterDoubleTapEventU3Ec__AnonStoreyB_t4028744225), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3019[2] = 
{
	U3CUnregisterDoubleTapEventU3Ec__AnonStoreyB_t4028744225::get_offset_of_itemCollider2D_0(),
	U3CUnregisterDoubleTapEventU3Ec__AnonStoreyB_t4028744225::get_offset_of_U3CU3Ef__refU2410_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3020 = { sizeof (U3CUnregisterDoubleTapEventU3Ec__AnonStoreyC_t1299860870), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3020[2] = 
{
	U3CUnregisterDoubleTapEventU3Ec__AnonStoreyC_t1299860870::get_offset_of_itemRectTransform_0(),
	U3CUnregisterDoubleTapEventU3Ec__AnonStoreyC_t1299860870::get_offset_of_U3CU3Ef__refU2410_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3021 = { sizeof (UnityObjectUtil_t4155662584), -1, sizeof(UnityObjectUtil_t4155662584_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3021[2] = 
{
	UnityObjectUtil_t4155662584_StaticFields::get_offset_of_sm_DestroyQueue_0(),
	UnityObjectUtil_t4155662584_StaticFields::get_offset_of_sm_DestroyQueueID_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3022 = { sizeof (U3CProcessDestroyQueueU3Ec__Iterator0_t2014034615), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3022[4] = 
{
	U3CProcessDestroyQueueU3Ec__Iterator0_t2014034615::get_offset_of_U24locvar0_0(),
	U3CProcessDestroyQueueU3Ec__Iterator0_t2014034615::get_offset_of_U24current_1(),
	U3CProcessDestroyQueueU3Ec__Iterator0_t2014034615::get_offset_of_U24disposing_2(),
	U3CProcessDestroyQueueU3Ec__Iterator0_t2014034615::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3023 = { sizeof (Utility_t666154278), -1, sizeof(Utility_t666154278_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3023[2] = 
{
	Utility_t666154278_StaticFields::get_offset_of_sm_Serializer_0(),
	Utility_t666154278_StaticFields::get_offset_of_sm_MacAddress_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3024 = { sizeof (WatsonException_t1186470237), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3025 = { sizeof (WaveFile_t3116037077), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3026 = { sizeof (IFF_FORM_CHUNK_t624775701)+ sizeof (RuntimeObject), sizeof(IFF_FORM_CHUNK_t624775701 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3026[3] = 
{
	IFF_FORM_CHUNK_t624775701::get_offset_of_form_id_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	IFF_FORM_CHUNK_t624775701::get_offset_of_form_length_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	IFF_FORM_CHUNK_t624775701::get_offset_of_id_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3027 = { sizeof (IFF_CHUNK_t4256546638)+ sizeof (RuntimeObject), sizeof(IFF_CHUNK_t4256546638 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3027[2] = 
{
	IFF_CHUNK_t4256546638::get_offset_of_id_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	IFF_CHUNK_t4256546638::get_offset_of_length_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3028 = { sizeof (WAV_PCM_t2882996802)+ sizeof (RuntimeObject), sizeof(WAV_PCM_t2882996802 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3028[6] = 
{
	WAV_PCM_t2882996802::get_offset_of_format_tag_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WAV_PCM_t2882996802::get_offset_of_channels_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WAV_PCM_t2882996802::get_offset_of_sample_rate_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WAV_PCM_t2882996802::get_offset_of_average_data_rate_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WAV_PCM_t2882996802::get_offset_of_alignment_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WAV_PCM_t2882996802::get_offset_of_bits_per_sample_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3029 = { sizeof (ActivateWidget_t3114088099), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3029[2] = 
{
	ActivateWidget_t3114088099::get_offset_of_m_ActivateOutput_6(),
	ActivateWidget_t3114088099::get_offset_of_m_SendValue_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3030 = { sizeof (ClassDisplayWidget_t1127558564), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3030[2] = 
{
	ClassDisplayWidget_t1127558564::get_offset_of_m_ClassInput_6(),
	ClassDisplayWidget_t1127558564::get_offset_of_m_ClassDisplay_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3031 = { sizeof (ClassifierWidget_t1446757817), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3031[3] = 
{
	ClassifierWidget_t1446757817::get_offset_of_m_ClassifyInput_6(),
	ClassifierWidget_t1446757817::get_offset_of_m_ClassifyOutput_7(),
	ClassifierWidget_t1446757817::get_offset_of_m_Mappings_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3032 = { sizeof (OnClassifierResult_t2664561427), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3033 = { sizeof (Mapping_t2602146240), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3033[3] = 
{
	Mapping_t2602146240::get_offset_of_m_Class_0(),
	Mapping_t2602146240::get_offset_of_m_Callback_1(),
	Mapping_t2602146240::get_offset_of_m_Exclusive_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3034 = { sizeof (TextToSpeechData_t1402652982), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3034[1] = 
{
	TextToSpeechData_t1402652982::get_offset_of_U3CTextU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3035 = { sizeof (LanguageData_t1599146524), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3035[1] = 
{
	LanguageData_t1599146524::get_offset_of_U3CLanguageU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3036 = { sizeof (VoiceData_t4097945788), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3036[1] = 
{
	VoiceData_t4097945788::get_offset_of_U3CVoiceU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3037 = { sizeof (AudioData_t3894398524), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3037[2] = 
{
	AudioData_t3894398524::get_offset_of_U3CClipU3Ek__BackingField_0(),
	AudioData_t3894398524::get_offset_of_U3CMaxLevelU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3038 = { sizeof (WebCamTextureData_t1034677858), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3038[4] = 
{
	WebCamTextureData_t1034677858::get_offset_of_U3CCamTextureU3Ek__BackingField_0(),
	WebCamTextureData_t1034677858::get_offset_of_U3CRequestedWidthU3Ek__BackingField_1(),
	WebCamTextureData_t1034677858::get_offset_of_U3CRequestedHeightU3Ek__BackingField_2(),
	WebCamTextureData_t1034677858::get_offset_of_U3CRequestedFPSU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3039 = { sizeof (BooleanData_t512765254), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3039[1] = 
{
	BooleanData_t512765254::get_offset_of_U3CBooleanU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3040 = { sizeof (SpeakingStateData_t3706378241), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3040[1] = 
{
	SpeakingStateData_t3706378241::get_offset_of_U3CBooleanU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3041 = { sizeof (DisableMicData_t3343074493), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3041[1] = 
{
	DisableMicData_t3343074493::get_offset_of_U3CBooleanU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3042 = { sizeof (DisableWebCamData_t2549114729), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3042[1] = 
{
	DisableWebCamData_t2549114729::get_offset_of_U3CBooleanU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3043 = { sizeof (DisableCloseCaptionData_t3291053220), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3043[1] = 
{
	DisableCloseCaptionData_t3291053220::get_offset_of_U3CBooleanU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3044 = { sizeof (LevelData_t2457055884), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3044[2] = 
{
	LevelData_t2457055884::get_offset_of_U3CFloatU3Ek__BackingField_0(),
	LevelData_t2457055884::get_offset_of_U3CModifierU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3045 = { sizeof (SpeechToTextData_t1322365462), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3045[2] = 
{
	SpeechToTextData_t1322365462::get_offset_of_U3CResultsU3Ek__BackingField_0(),
	SpeechToTextData_t1322365462::get_offset_of__Text_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3046 = { sizeof (ClassifyResultData_t1440000831), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3046[1] = 
{
	ClassifyResultData_t1440000831::get_offset_of_U3CResultU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3047 = { sizeof (EventWidget_t528106534), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3047[1] = 
{
	EventWidget_t528106534::get_offset_of_m_Mappings_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3048 = { sizeof (Mapping_t1442940263), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3048[2] = 
{
	Mapping_t1442940263::get_offset_of_m_Event_0(),
	Mapping_t1442940263::get_offset_of_m_Callback_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3049 = { sizeof (KeyboardWidget_t3474172959), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3049[1] = 
{
	KeyboardWidget_t3474172959::get_offset_of_m_Mappings_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3050 = { sizeof (Mapping_t3124476604), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3050[3] = 
{
	Mapping_t3124476604::get_offset_of_m_Key_0(),
	Mapping_t3124476604::get_offset_of_m_Modifiers_1(),
	Mapping_t3124476604::get_offset_of_m_Event_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3051 = { sizeof (LanguageTranslatorWidget_t907487206), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3051[19] = 
{
	LanguageTranslatorWidget_t907487206::get_offset_of_m_SpeechInput_6(),
	LanguageTranslatorWidget_t907487206::get_offset_of_m_RecognizeLanguageOutput_7(),
	LanguageTranslatorWidget_t907487206::get_offset_of_m_SpeechOutput_8(),
	LanguageTranslatorWidget_t907487206::get_offset_of_m_VoiceOutput_9(),
	LanguageTranslatorWidget_t907487206::get_offset_of_m_Translate_10(),
	LanguageTranslatorWidget_t907487206::get_offset_of_m_SourceLanguage_11(),
	LanguageTranslatorWidget_t907487206::get_offset_of_m_TargetLanguage_12(),
	LanguageTranslatorWidget_t907487206::get_offset_of_m_Input_13(),
	LanguageTranslatorWidget_t907487206::get_offset_of_m_Output_14(),
	LanguageTranslatorWidget_t907487206::get_offset_of_m_DropDownSourceLanguage_15(),
	LanguageTranslatorWidget_t907487206::get_offset_of_m_DropDownTargetLanguage_16(),
	LanguageTranslatorWidget_t907487206::get_offset_of_m_DefaultDomainToUse_17(),
	LanguageTranslatorWidget_t907487206::get_offset_of_m_DetectLanguageName_18(),
	LanguageTranslatorWidget_t907487206::get_offset_of_m_DetectLanguageID_19(),
	LanguageTranslatorWidget_t907487206::get_offset_of_m_LanguageIDToName_20(),
	LanguageTranslatorWidget_t907487206::get_offset_of_m_LanguageNameToID_21(),
	LanguageTranslatorWidget_t907487206::get_offset_of_m_LanguageToTranslate_22(),
	LanguageTranslatorWidget_t907487206::get_offset_of_m_Languages_23(),
	LanguageTranslatorWidget_t907487206::get_offset_of_m_TranslateText_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3052 = { sizeof (TranslateRequest_t2111065942), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3052[2] = 
{
	TranslateRequest_t2111065942::get_offset_of_m_Widget_0(),
	TranslateRequest_t2111065942::get_offset_of_m_Text_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3053 = { sizeof (MicrophoneWidget_t2442369682), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3053[20] = 
{
	MicrophoneWidget_t2442369682::get_offset_of_m_DisableInput_6(),
	MicrophoneWidget_t2442369682::get_offset_of_m_AudioOutput_7(),
	MicrophoneWidget_t2442369682::get_offset_of_m_LevelOutput_8(),
	MicrophoneWidget_t2442369682::get_offset_of_m_ActivateOutput_9(),
	MicrophoneWidget_t2442369682::get_offset_of_m_Active_10(),
	MicrophoneWidget_t2442369682::get_offset_of_m_Disabled_11(),
	MicrophoneWidget_t2442369682::get_offset_of_m_Failure_12(),
	MicrophoneWidget_t2442369682::get_offset_of_m_LastFailure_13(),
	MicrophoneWidget_t2442369682::get_offset_of_m_ActivateOnStart_14(),
	MicrophoneWidget_t2442369682::get_offset_of_m_RecordingBufferSize_15(),
	MicrophoneWidget_t2442369682::get_offset_of_m_RecordingHZ_16(),
	MicrophoneWidget_t2442369682::get_offset_of_m_MicrophoneID_17(),
	MicrophoneWidget_t2442369682::get_offset_of_m_LevelOutputInterval_18(),
	MicrophoneWidget_t2442369682::get_offset_of_m_LevelOutputModifier_19(),
	MicrophoneWidget_t2442369682::get_offset_of_m_PlaybackRecording_20(),
	MicrophoneWidget_t2442369682::get_offset_of_m_StatusText_21(),
	MicrophoneWidget_t2442369682::get_offset_of_m_RecordingRoutine_22(),
	MicrophoneWidget_t2442369682::get_offset_of_m_Recording_23(),
	MicrophoneWidget_t2442369682::get_offset_of_m_Playback_24(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3054 = { sizeof (U3CRecordingHandlerU3Ec__Iterator0_t3999079579), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3054[13] = 
{
	U3CRecordingHandlerU3Ec__Iterator0_t3999079579::get_offset_of_U3CbFirstBlockU3E__0_0(),
	U3CRecordingHandlerU3Ec__Iterator0_t3999079579::get_offset_of_U3CmidPointU3E__0_1(),
	U3CRecordingHandlerU3Ec__Iterator0_t3999079579::get_offset_of_U3CbOutputLevelDataU3E__0_2(),
	U3CRecordingHandlerU3Ec__Iterator0_t3999079579::get_offset_of_U3CbOutputAudioU3E__0_3(),
	U3CRecordingHandlerU3Ec__Iterator0_t3999079579::get_offset_of_U3ClastReadPosU3E__0_4(),
	U3CRecordingHandlerU3Ec__Iterator0_t3999079579::get_offset_of_U3CsamplesU3E__0_5(),
	U3CRecordingHandlerU3Ec__Iterator0_t3999079579::get_offset_of_U3CwritePosU3E__1_6(),
	U3CRecordingHandlerU3Ec__Iterator0_t3999079579::get_offset_of_U3CremainingU3E__2_7(),
	U3CRecordingHandlerU3Ec__Iterator0_t3999079579::get_offset_of_U3CtimeRemainingU3E__2_8(),
	U3CRecordingHandlerU3Ec__Iterator0_t3999079579::get_offset_of_U24this_9(),
	U3CRecordingHandlerU3Ec__Iterator0_t3999079579::get_offset_of_U24current_10(),
	U3CRecordingHandlerU3Ec__Iterator0_t3999079579::get_offset_of_U24disposing_11(),
	U3CRecordingHandlerU3Ec__Iterator0_t3999079579::get_offset_of_U24PC_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3055 = { sizeof (NaturalLanguageClassifierWidget_t2550209684), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3055[16] = 
{
	NaturalLanguageClassifierWidget_t2550209684::get_offset_of_m_RecognizeInput_6(),
	NaturalLanguageClassifierWidget_t2550209684::get_offset_of_m_ClassifyOutput_7(),
	NaturalLanguageClassifierWidget_t2550209684::get_offset_of_m_NaturalLanguageClassifier_8(),
	NaturalLanguageClassifierWidget_t2550209684::get_offset_of_m_Selected_9(),
	NaturalLanguageClassifierWidget_t2550209684::get_offset_of_m_ClassifierName_10(),
	NaturalLanguageClassifierWidget_t2550209684::get_offset_of_m_ClassifierId_11(),
	NaturalLanguageClassifierWidget_t2550209684::get_offset_of_m_MinWordConfidence_12(),
	NaturalLanguageClassifierWidget_t2550209684::get_offset_of_m_MinWordConfidenceDelta_13(),
	NaturalLanguageClassifierWidget_t2550209684::get_offset_of_m_IgnoreWordConfidence_14(),
	NaturalLanguageClassifierWidget_t2550209684::get_offset_of_m_IgnoreWordConfidenceDelta_15(),
	NaturalLanguageClassifierWidget_t2550209684::get_offset_of_m_MinClassEventConfidence_16(),
	NaturalLanguageClassifierWidget_t2550209684::get_offset_of_m_MinClassEventConfidenceDelta_17(),
	NaturalLanguageClassifierWidget_t2550209684::get_offset_of_m_Language_18(),
	NaturalLanguageClassifierWidget_t2550209684::get_offset_of_m_ClassEventList_19(),
	NaturalLanguageClassifierWidget_t2550209684::get_offset_of_m_ClassEventMap_20(),
	NaturalLanguageClassifierWidget_t2550209684::get_offset_of_m_TopClassText_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3056 = { sizeof (ClassEventMapping_t3594451529), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3056[2] = 
{
	ClassEventMapping_t3594451529::get_offset_of_m_Class_0(),
	ClassEventMapping_t3594451529::get_offset_of_m_Event_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3057 = { sizeof (SpeechDisplayWidget_t1126703566), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3057[10] = 
{
	SpeechDisplayWidget_t1126703566::get_offset_of_m_SpeechInput_6(),
	SpeechDisplayWidget_t1126703566::get_offset_of_m_ContinuousText_7(),
	SpeechDisplayWidget_t1126703566::get_offset_of_m_Output_8(),
	SpeechDisplayWidget_t1126703566::get_offset_of_m_OutputAsInputField_9(),
	SpeechDisplayWidget_t1126703566::get_offset_of_m_OutputStatus_10(),
	SpeechDisplayWidget_t1126703566::get_offset_of_m_MinConfidenceToShow_11(),
	SpeechDisplayWidget_t1126703566::get_offset_of_m_PreviousOutputTextWithStatus_12(),
	SpeechDisplayWidget_t1126703566::get_offset_of_m_PreviousOutputText_13(),
	SpeechDisplayWidget_t1126703566::get_offset_of_m_ThresholdTimeFromLastInput_14(),
	SpeechDisplayWidget_t1126703566::get_offset_of_m_TimeAtLastInterim_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3058 = { sizeof (SpeechToTextWidget_t957037126), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3058[34] = 
{
	SpeechToTextWidget_t957037126::get_offset_of_Characters_6(),
	SpeechToTextWidget_t957037126::get_offset_of_CharacterNames_7(),
	SpeechToTextWidget_t957037126::get_offset_of_distanceInFront_8(),
	SpeechToTextWidget_t957037126::get_offset_of_SpeechBubbles_9(),
	SpeechToTextWidget_t957037126::get_offset_of_MainCam_10(),
	SpeechToTextWidget_t957037126::get_offset_of_m_Conversation_11(),
	SpeechToTextWidget_t957037126::get_offset_of_m_WorkspaceID_12(),
	SpeechToTextWidget_t957037126::get_offset_of_m_UseAlternateIntents_13(),
	SpeechToTextWidget_t957037126::get_offset_of_questionArray_14(),
	SpeechToTextWidget_t957037126::get_offset_of_myContext_15(),
	SpeechToTextWidget_t957037126::get_offset_of_PosterNames_16(),
	SpeechToTextWidget_t957037126::get_offset_of_Posters_17(),
	SpeechToTextWidget_t957037126::get_offset_of_PosterTimer_18(),
	SpeechToTextWidget_t957037126::get_offset_of_moviePlayer_19(),
	SpeechToTextWidget_t957037126::get_offset_of_MovieCue_20(),
	SpeechToTextWidget_t957037126::get_offset_of_NewMessages_21(),
	SpeechToTextWidget_t957037126::get_offset_of_MessageDelay_22(),
	SpeechToTextWidget_t957037126::get_offset_of_MessageChar_23(),
	SpeechToTextWidget_t957037126::get_offset_of_tts_24(),
	SpeechToTextWidget_t957037126::get_offset_of_m_AudioInput_25(),
	SpeechToTextWidget_t957037126::get_offset_of_m_LanguageInput_26(),
	SpeechToTextWidget_t957037126::get_offset_of_m_ResultOutput_27(),
	SpeechToTextWidget_t957037126::get_offset_of_m_SpeechToText_28(),
	SpeechToTextWidget_t957037126::get_offset_of_m_StatusText_29(),
	SpeechToTextWidget_t957037126::get_offset_of_m_DetectSilence_30(),
	SpeechToTextWidget_t957037126::get_offset_of_m_SilenceThreshold_31(),
	SpeechToTextWidget_t957037126::get_offset_of_m_WordConfidence_32(),
	SpeechToTextWidget_t957037126::get_offset_of_m_TimeStamps_33(),
	SpeechToTextWidget_t957037126::get_offset_of_m_MaxAlternatives_34(),
	SpeechToTextWidget_t957037126::get_offset_of_m_EnableContinous_35(),
	SpeechToTextWidget_t957037126::get_offset_of_m_EnableInterimResults_36(),
	SpeechToTextWidget_t957037126::get_offset_of_m_Transcript_37(),
	SpeechToTextWidget_t957037126::get_offset_of_m_Language_38(),
	SpeechToTextWidget_t957037126::get_offset_of_MessageTimer_39(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3059 = { sizeof (TextToSpeechWidget_t2096692646), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3059[17] = 
{
	TextToSpeechWidget_t2096692646::get_offset_of_m_TextInput_6(),
	TextToSpeechWidget_t2096692646::get_offset_of_m_VoiceInput_7(),
	TextToSpeechWidget_t2096692646::get_offset_of_m_Speaking_8(),
	TextToSpeechWidget_t2096692646::get_offset_of_m_DisableMic_9(),
	TextToSpeechWidget_t2096692646::get_offset_of_m_LevelOut_10(),
	TextToSpeechWidget_t2096692646::get_offset_of_m_TextToSpeech_11(),
	TextToSpeechWidget_t2096692646::get_offset_of_m_LevelOutInterval_12(),
	TextToSpeechWidget_t2096692646::get_offset_of_m_LevelOutputModifier_13(),
	TextToSpeechWidget_t2096692646::get_offset_of_m_TextToSpeechButton_14(),
	TextToSpeechWidget_t2096692646::get_offset_of_m_Input_15(),
	TextToSpeechWidget_t2096692646::get_offset_of_m_StatusText_16(),
	TextToSpeechWidget_t2096692646::get_offset_of_m_Voice_17(),
	TextToSpeechWidget_t2096692646::get_offset_of_m_UsePost_18(),
	TextToSpeechWidget_t2096692646::get_offset_of_m_Source_19(),
	TextToSpeechWidget_t2096692646::get_offset_of_m_LastPlayPos_20(),
	TextToSpeechWidget_t2096692646::get_offset_of_m_SpeechQueue_21(),
	TextToSpeechWidget_t2096692646::get_offset_of_m_ActiveSpeech_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3060 = { sizeof (Speech_t1663141205), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3060[2] = 
{
	Speech_t1663141205::get_offset_of_U3CReadyU3Ek__BackingField_0(),
	Speech_t1663141205::get_offset_of_U3CClipU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3061 = { sizeof (TouchWidget_t3160682287), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3061[2] = 
{
	TouchWidget_t3160682287::get_offset_of_m_TapMappings_6(),
	TouchWidget_t3160682287::get_offset_of_m_FullScreenDragMappings_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3062 = { sizeof (TapEventMapping_t2308161515), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3062[5] = 
{
	TapEventMapping_t2308161515::get_offset_of_m_TapObject_0(),
	TapEventMapping_t2308161515::get_offset_of_m_TapOnObject_1(),
	TapEventMapping_t2308161515::get_offset_of_m_SortingLayer_2(),
	TapEventMapping_t2308161515::get_offset_of_m_LayerMask_3(),
	TapEventMapping_t2308161515::get_offset_of_m_Callback_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3063 = { sizeof (FullScreenDragEventMapping_t3872427665), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3063[5] = 
{
	FullScreenDragEventMapping_t3872427665::get_offset_of_m_DragLayerObject_0(),
	FullScreenDragEventMapping_t3872427665::get_offset_of_m_NumberOfFinger_1(),
	FullScreenDragEventMapping_t3872427665::get_offset_of_m_SortingLayer_2(),
	FullScreenDragEventMapping_t3872427665::get_offset_of_m_IsDragInside_3(),
	FullScreenDragEventMapping_t3872427665::get_offset_of_m_Callback_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3064 = { sizeof (WebCamDisplayWidget_t3233697851), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3064[7] = 
{
	WebCamDisplayWidget_t3233697851::get_offset_of_m_WebCamTextureInput_6(),
	WebCamDisplayWidget_t3233697851::get_offset_of_m_RawImage_7(),
	WebCamDisplayWidget_t3233697851::get_offset_of_m_Material_8(),
	WebCamDisplayWidget_t3233697851::get_offset_of_m_WebCamTexture_9(),
	WebCamDisplayWidget_t3233697851::get_offset_of_m_RequestedWidth_10(),
	WebCamDisplayWidget_t3233697851::get_offset_of_m_RequestedHeight_11(),
	WebCamDisplayWidget_t3233697851::get_offset_of_m_RequestedFPS_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3065 = { sizeof (WebCamWidget_t58556181), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3065[15] = 
{
	WebCamWidget_t58556181::get_offset_of_m_DisableInput_6(),
	WebCamWidget_t58556181::get_offset_of_m_WebCamTextureOutput_7(),
	WebCamWidget_t58556181::get_offset_of_m_ActivateOutput_8(),
	WebCamWidget_t58556181::get_offset_of_m_Active_9(),
	WebCamWidget_t58556181::get_offset_of_m_Disabled_10(),
	WebCamWidget_t58556181::get_offset_of_m_Failure_11(),
	WebCamWidget_t58556181::get_offset_of_m_LastFailure_12(),
	WebCamWidget_t58556181::get_offset_of_m_ActivateOnStart_13(),
	WebCamWidget_t58556181::get_offset_of_m_RequestedWidth_14(),
	WebCamWidget_t58556181::get_offset_of_m_RequestedHeight_15(),
	WebCamWidget_t58556181::get_offset_of_m_RequestedFPS_16(),
	WebCamWidget_t58556181::get_offset_of_m_StatusText_17(),
	WebCamWidget_t58556181::get_offset_of_m_RecordingRoutine_18(),
	WebCamWidget_t58556181::get_offset_of_m_WebCamTexture_19(),
	WebCamWidget_t58556181::get_offset_of_m_WebCamIndex_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3066 = { sizeof (U3CRecordingHandlerU3Ec__Iterator0_t1226459312), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3066[5] = 
{
	U3CRecordingHandlerU3Ec__Iterator0_t1226459312::get_offset_of_U3CcamDataU3E__0_0(),
	U3CRecordingHandlerU3Ec__Iterator0_t1226459312::get_offset_of_U24this_1(),
	U3CRecordingHandlerU3Ec__Iterator0_t1226459312::get_offset_of_U24current_2(),
	U3CRecordingHandlerU3Ec__Iterator0_t1226459312::get_offset_of_U24disposing_3(),
	U3CRecordingHandlerU3Ec__Iterator0_t1226459312::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3067 = { sizeof (Widget_t3624771850), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3067[4] = 
{
	Widget_t3624771850::get_offset_of_m_AutoConnect_2(),
	Widget_t3624771850::get_offset_of_m_Initialized_3(),
	Widget_t3624771850::get_offset_of_m_Inputs_4(),
	Widget_t3624771850::get_offset_of_m_Outputs_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3068 = { sizeof (Data_t3166909639), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3069 = { sizeof (OnReceiveData_t2274732329), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3070 = { sizeof (Input_t3157785889), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3070[7] = 
{
	Input_t3157785889::get_offset_of_m_Connections_0(),
	Input_t3157785889::get_offset_of_U3COwnerU3Ek__BackingField_1(),
	Input_t3157785889::get_offset_of_U3CInputNameU3Ek__BackingField_2(),
	Input_t3157785889::get_offset_of_U3CDataTypeU3Ek__BackingField_3(),
	Input_t3157785889::get_offset_of_U3CAllowManyU3Ek__BackingField_4(),
	Input_t3157785889::get_offset_of_U3CReceiverFunctionU3Ek__BackingField_5(),
	Input_t3157785889::get_offset_of_U3CDataReceiverU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3071 = { sizeof (Output_t220081548), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3071[4] = 
{
	Output_t220081548::get_offset_of_U3COwnerU3Ek__BackingField_0(),
	Output_t220081548::get_offset_of_U3CDataTypeU3Ek__BackingField_1(),
	Output_t220081548::get_offset_of_U3CAllowManyU3Ek__BackingField_2(),
	Output_t220081548::get_offset_of_m_Connections_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3072 = { sizeof (Connection_t2486334317), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3072[5] = 
{
	Connection_t2486334317::get_offset_of_m_Owner_0(),
	Connection_t2486334317::get_offset_of_m_TargetObject_1(),
	Connection_t2486334317::get_offset_of_m_TargetConnection_2(),
	Connection_t2486334317::get_offset_of_m_TargetInputResolved_3(),
	Connection_t2486334317::get_offset_of_m_TargetInput_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3073 = { sizeof (fsArrayConverter_t1376358826), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3074 = { sizeof (fsDateConverter_t3677588831), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3074[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3075 = { sizeof (fsDictionaryConverter_t4168040623), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3076 = { sizeof (fsEnumConverter_t1608130800), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3077 = { sizeof (fsForwardAttribute_t3349051188), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3077[1] = 
{
	fsForwardAttribute_t3349051188::get_offset_of_MemberName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3078 = { sizeof (fsForwardConverter_t3272928430), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3078[1] = 
{
	fsForwardConverter_t3272928430::get_offset_of__memberName_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3079 = { sizeof (fsGuidConverter_t2907267772), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3080 = { sizeof (fsIEnumerableConverter_t2313469578), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3081 = { sizeof (fsKeyValuePairConverter_t2945333237), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3082 = { sizeof (fsNullableConverter_t3298327032), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3083 = { sizeof (fsPrimitiveConverter_t3016566550), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3084 = { sizeof (fsReflectedConverter_t171754893), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3085 = { sizeof (fsTypeConverter_t3442245107), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3086 = { sizeof (fsWeakReferenceConverter_t940327902), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3087 = { sizeof (fsConverterRegistrar_t866553964), -1, sizeof(fsConverterRegistrar_t866553964_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3087[7] = 
{
	fsConverterRegistrar_t866553964_StaticFields::get_offset_of_Register_AnimationCurve_DirectConverter_0(),
	fsConverterRegistrar_t866553964_StaticFields::get_offset_of_Register_Bounds_DirectConverter_1(),
	fsConverterRegistrar_t866553964_StaticFields::get_offset_of_Register_Gradient_DirectConverter_2(),
	fsConverterRegistrar_t866553964_StaticFields::get_offset_of_Register_Keyframe_DirectConverter_3(),
	fsConverterRegistrar_t866553964_StaticFields::get_offset_of_Register_LayerMask_DirectConverter_4(),
	fsConverterRegistrar_t866553964_StaticFields::get_offset_of_Register_Rect_DirectConverter_5(),
	fsConverterRegistrar_t866553964_StaticFields::get_offset_of_Converters_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3088 = { sizeof (AnimationCurve_DirectConverter_t1633993451), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3089 = { sizeof (Bounds_DirectConverter_t3918181867), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3090 = { sizeof (Gradient_DirectConverter_t4192375398), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3091 = { sizeof (Keyframe_DirectConverter_t1121933266), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3092 = { sizeof (LayerMask_DirectConverter_t2775871809), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3093 = { sizeof (Rect_DirectConverter_t2065695396), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3094 = { sizeof (fsAotCompilationManager_t3563908143), -1, sizeof(fsAotCompilationManager_t3563908143_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3094[2] = 
{
	fsAotCompilationManager_t3563908143_StaticFields::get_offset_of__computedAotCompilations_0(),
	fsAotCompilationManager_t3563908143_StaticFields::get_offset_of__uncomputedAotCompilations_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3095 = { sizeof (AotCompilation_t21908242)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3095[3] = 
{
	AotCompilation_t21908242::get_offset_of_Type_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AotCompilation_t21908242::get_offset_of_Members_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AotCompilation_t21908242::get_offset_of_IsConstructorPublic_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3096 = { sizeof (fsBaseConverter_t1241677426), -1, sizeof(fsBaseConverter_t1241677426_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3096[2] = 
{
	fsBaseConverter_t1241677426::get_offset_of_Serializer_0(),
	fsBaseConverter_t1241677426_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3097 = { sizeof (fsConfig_t3026457307), -1, sizeof(fsConfig_t3026457307_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3097[9] = 
{
	fsConfig_t3026457307_StaticFields::get_offset_of_SerializeAttributes_0(),
	fsConfig_t3026457307_StaticFields::get_offset_of_IgnoreSerializeAttributes_1(),
	fsConfig_t3026457307_StaticFields::get_offset_of__defaultMemberSerialization_2(),
	fsConfig_t3026457307_StaticFields::get_offset_of_SerializeNonAutoProperties_3(),
	fsConfig_t3026457307_StaticFields::get_offset_of_SerializeNonPublicSetProperties_4(),
	fsConfig_t3026457307_StaticFields::get_offset_of_IsCaseSensitive_5(),
	fsConfig_t3026457307_StaticFields::get_offset_of_CustomDateTimeFormatString_6(),
	fsConfig_t3026457307_StaticFields::get_offset_of_Serialize64BitIntegerAsString_7(),
	fsConfig_t3026457307_StaticFields::get_offset_of_SerializeEnumsAsInteger_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3098 = { sizeof (fsContext_t2896355488), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3098[1] = 
{
	fsContext_t2896355488::get_offset_of__contextObjects_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3099 = { sizeof (fsConverter_t466758137), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
