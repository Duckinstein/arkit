﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.String
struct String_t;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.TargetedSentiment[]
struct TargetedSentimentU5BU5D_t1237468536;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.DocSentiment
struct DocSentiment_t2354958393;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Taxonomy[]
struct TaxonomyU5BU5D_t3261933456;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.ImageKeyword[]
struct ImageKeywordU5BU5D_t3897519977;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.PublicationDate
struct PublicationDate_t608222142;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Authors
struct Authors_t34055864;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Feed[]
struct FeedU5BU5D_t332386033;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Keyword[]
struct KeywordU5BU5D_t3893757366;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Concept[]
struct ConceptU5BU5D_t3502654871;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Entity[]
struct EntityU5BU5D_t3969836080;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Relation[]
struct RelationU5BU5D_t1525062961;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Date[]
struct DateU5BU5D_t3621454771;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.DocEmotions[]
struct DocEmotionsU5BU5D_t2333025007;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Microformat[]
struct MicroformatU5BU5D_t3715084158;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.PositionOnMap
struct PositionOnMap_t66846110;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.KnowledgeGraph
struct KnowledgeGraph_t3341597436;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.DocEmotions
struct DocEmotions_t3627397418;
// System.String[]
struct StringU5BU5D_t1642385972;
// IBM.Watson.DeveloperCloud.Debug.DebugConsole
struct DebugConsole_t4095885638;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Collections.Generic.Dictionary`2<System.String,IBM.Watson.DeveloperCloud.Connection.RESTConnector/Form>
struct Dictionary_2_t2694055125;
// IBM.Watson.DeveloperCloud.Connection.RESTConnector/ResponseEvent
struct ResponseEvent_t1616568356;
// IBM.Watson.DeveloperCloud.Connection.RESTConnector/ProgressEvent
struct ProgressEvent_t4185145044;
// IBM.Watson.DeveloperCloud.Debug.DebugConsole/GetDebugInfo
struct GetDebugInfo_t2665501740;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Text
struct Text_t356221433;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Entity
struct Entity_t2757698109;
// System.Collections.Generic.List`1<IBM.Watson.DeveloperCloud.Logging.ILogReactor>
struct List_1_t1547628219;
// UnityEngine.Application/LogCallback
struct LogCallback_t1867914413;
// FullSerializer.fsSerializer
struct fsSerializer_t4193731081;
// IBM.Watson.DeveloperCloud.Connection.WSConnector
struct WSConnector_t2552241039;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Subject
struct Subject_t927036590;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Action
struct Action_t3935607096;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.ObjectData
struct ObjectData_t1677090869;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Location
struct Location_t4202627189;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Temporal
struct Temporal_t3690293474;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Keyword
struct Keyword_t3384522415;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI
struct AlchemyAPI_t2955839919;
// IBM.Watson.DeveloperCloud.Services.ServiceStatus
struct ServiceStatus_t1443707987;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Verb
struct Verb_t2846458383;
// System.Void
struct Void_t1841601450;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/OnGetMicroformats
struct OnGetMicroformats_t327929786;
// System.Char[]
struct CharU5BU5D_t1328083999;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/OnGetRelations
struct OnGetRelations_t1280830431;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/OnGetPublicationDate
struct OnGetPublicationDate_t443403836;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/OnGetTextSentiment
struct OnGetTextSentiment_t3575883834;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/OnGetDates
struct OnGetDates_t91791237;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/OnGetEmotions
struct OnGetEmotions_t1524549668;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/OnGetKeywords
struct OnGetKeywords_t1978903802;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/OnDetectFeeds
struct OnDetectFeeds_t4021436334;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/OnGetEntities
struct OnGetEntities_t1909489667;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/OnGetLanguages
struct OnGetLanguages_t3197075847;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/OnGetAuthors
struct OnGetAuthors_t505057758;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/OnGetNews
struct OnGetNews_t1755812277;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/OnGetCombinedData
struct OnGetCombinedData_t4285430159;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/OnGetTitle
struct OnGetTitle_t1839253948;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/OnGetText
struct OnGetText_t3268193843;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/OnGetTargetedSentiment
struct OnGetTargetedSentiment_t2739505381;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/OnGetRankedTaxonomy
struct OnGetRankedTaxonomy_t935340330;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/OnGetRankedConcepts
struct OnGetRankedConcepts_t3530605856;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1572802995;
// IBM.Watson.DeveloperCloud.Connection.WSConnector/ConnectorEvent
struct ConnectorEvent_t1029302242;
// IBM.Watson.DeveloperCloud.Connection.WSConnector/MessageEvent
struct MessageEvent_t2276855538;
// IBM.Watson.DeveloperCloud.Utilities.Credentials
struct Credentials_t1494051450;
// System.Threading.Thread
struct Thread_t241561612;
// System.Threading.AutoResetEvent
struct AutoResetEvent_t15112628;
// System.Collections.Generic.Queue`1<IBM.Watson.DeveloperCloud.Connection.WSConnector/Message>
struct Queue_1_t2334678625;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Disambiguated
struct Disambiguated_t227057183;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Quotation[]
struct QuotationU5BU5D_t3047261617;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.CombinedCallData
struct CombinedCallData_t1377778673;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.NewsResponse
struct NewsResponse_t3536772326;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Title
struct Title_t2058683094;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.TaxonomyData
struct TaxonomyData_t3741516269;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.TextData
struct TextData_t3443651365;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.EmotionData
struct EmotionData_t625023405;
// IBM.Watson.DeveloperCloud.Connection.WSConnector/Message
struct Message_t2515021790;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.LanguageData
struct LanguageData_t2624020128;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.KeywordData
struct KeywordData_t1578948319;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.FeedData
struct FeedData_t845804320;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.MicroformatData
struct MicroformatData_t655366775;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.SentimentData
struct SentimentData_t471353809;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.RelationsData
struct RelationsData_t3907134773;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.PubDateData
struct PubDateData_t3405155195;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AuthorsData
struct AuthorsData_t3643075816;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.DateData
struct DateData_t3593288038;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.ConceptsData
struct ConceptsData_t441878239;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.EntityData
struct EntityData_t436806285;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.TargetedSentimentData
struct TargetedSentimentData_t3798577861;
// System.Collections.Generic.List`1<IBM.Watson.DeveloperCloud.Debug.DebugConsole/DebugInfo>
struct List_1_t4044068874;
// UnityEngine.UI.LayoutGroup
struct LayoutGroup_t3962498969;
// UnityEngine.UI.InputField
struct InputField_t1631627530;
// IBM.Watson.DeveloperCloud.Widgets.MicrophoneWidget
struct MicrophoneWidget_t2442369682;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef TARGETEDSENTIMENTDATA_T3798577861_H
#define TARGETEDSENTIMENTDATA_T3798577861_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.TargetedSentimentData
struct  TargetedSentimentData_t3798577861  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.TargetedSentimentData::<status>k__BackingField
	String_t* ___U3CstatusU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.TargetedSentimentData::<url>k__BackingField
	String_t* ___U3CurlU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.TargetedSentimentData::<text>k__BackingField
	String_t* ___U3CtextU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.TargetedSentimentData::<language>k__BackingField
	String_t* ___U3ClanguageU3Ek__BackingField_3;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.TargetedSentiment[] IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.TargetedSentimentData::<results>k__BackingField
	TargetedSentimentU5BU5D_t1237468536* ___U3CresultsU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CstatusU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TargetedSentimentData_t3798577861, ___U3CstatusU3Ek__BackingField_0)); }
	inline String_t* get_U3CstatusU3Ek__BackingField_0() const { return ___U3CstatusU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CstatusU3Ek__BackingField_0() { return &___U3CstatusU3Ek__BackingField_0; }
	inline void set_U3CstatusU3Ek__BackingField_0(String_t* value)
	{
		___U3CstatusU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstatusU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CurlU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TargetedSentimentData_t3798577861, ___U3CurlU3Ek__BackingField_1)); }
	inline String_t* get_U3CurlU3Ek__BackingField_1() const { return ___U3CurlU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CurlU3Ek__BackingField_1() { return &___U3CurlU3Ek__BackingField_1; }
	inline void set_U3CurlU3Ek__BackingField_1(String_t* value)
	{
		___U3CurlU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CurlU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CtextU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(TargetedSentimentData_t3798577861, ___U3CtextU3Ek__BackingField_2)); }
	inline String_t* get_U3CtextU3Ek__BackingField_2() const { return ___U3CtextU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CtextU3Ek__BackingField_2() { return &___U3CtextU3Ek__BackingField_2; }
	inline void set_U3CtextU3Ek__BackingField_2(String_t* value)
	{
		___U3CtextU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3ClanguageU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(TargetedSentimentData_t3798577861, ___U3ClanguageU3Ek__BackingField_3)); }
	inline String_t* get_U3ClanguageU3Ek__BackingField_3() const { return ___U3ClanguageU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3ClanguageU3Ek__BackingField_3() { return &___U3ClanguageU3Ek__BackingField_3; }
	inline void set_U3ClanguageU3Ek__BackingField_3(String_t* value)
	{
		___U3ClanguageU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClanguageU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CresultsU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(TargetedSentimentData_t3798577861, ___U3CresultsU3Ek__BackingField_4)); }
	inline TargetedSentimentU5BU5D_t1237468536* get_U3CresultsU3Ek__BackingField_4() const { return ___U3CresultsU3Ek__BackingField_4; }
	inline TargetedSentimentU5BU5D_t1237468536** get_address_of_U3CresultsU3Ek__BackingField_4() { return &___U3CresultsU3Ek__BackingField_4; }
	inline void set_U3CresultsU3Ek__BackingField_4(TargetedSentimentU5BU5D_t1237468536* value)
	{
		___U3CresultsU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CresultsU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TARGETEDSENTIMENTDATA_T3798577861_H
#ifndef TARGETEDSENTIMENT_T494351509_H
#define TARGETEDSENTIMENT_T494351509_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.TargetedSentiment
struct  TargetedSentiment_t494351509  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.DocSentiment IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.TargetedSentiment::<sentiment>k__BackingField
	DocSentiment_t2354958393 * ___U3CsentimentU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.TargetedSentiment::<text>k__BackingField
	String_t* ___U3CtextU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CsentimentU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TargetedSentiment_t494351509, ___U3CsentimentU3Ek__BackingField_0)); }
	inline DocSentiment_t2354958393 * get_U3CsentimentU3Ek__BackingField_0() const { return ___U3CsentimentU3Ek__BackingField_0; }
	inline DocSentiment_t2354958393 ** get_address_of_U3CsentimentU3Ek__BackingField_0() { return &___U3CsentimentU3Ek__BackingField_0; }
	inline void set_U3CsentimentU3Ek__BackingField_0(DocSentiment_t2354958393 * value)
	{
		___U3CsentimentU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsentimentU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CtextU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TargetedSentiment_t494351509, ___U3CtextU3Ek__BackingField_1)); }
	inline String_t* get_U3CtextU3Ek__BackingField_1() const { return ___U3CtextU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CtextU3Ek__BackingField_1() { return &___U3CtextU3Ek__BackingField_1; }
	inline void set_U3CtextU3Ek__BackingField_1(String_t* value)
	{
		___U3CtextU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TARGETEDSENTIMENT_T494351509_H
#ifndef TAXONOMYDATA_T3741516269_H
#define TAXONOMYDATA_T3741516269_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.TaxonomyData
struct  TaxonomyData_t3741516269  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.TaxonomyData::<status>k__BackingField
	String_t* ___U3CstatusU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.TaxonomyData::<url>k__BackingField
	String_t* ___U3CurlU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.TaxonomyData::<language>k__BackingField
	String_t* ___U3ClanguageU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.TaxonomyData::<text>k__BackingField
	String_t* ___U3CtextU3Ek__BackingField_3;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Taxonomy[] IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.TaxonomyData::<taxonomy>k__BackingField
	TaxonomyU5BU5D_t3261933456* ___U3CtaxonomyU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CstatusU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TaxonomyData_t3741516269, ___U3CstatusU3Ek__BackingField_0)); }
	inline String_t* get_U3CstatusU3Ek__BackingField_0() const { return ___U3CstatusU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CstatusU3Ek__BackingField_0() { return &___U3CstatusU3Ek__BackingField_0; }
	inline void set_U3CstatusU3Ek__BackingField_0(String_t* value)
	{
		___U3CstatusU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstatusU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CurlU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TaxonomyData_t3741516269, ___U3CurlU3Ek__BackingField_1)); }
	inline String_t* get_U3CurlU3Ek__BackingField_1() const { return ___U3CurlU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CurlU3Ek__BackingField_1() { return &___U3CurlU3Ek__BackingField_1; }
	inline void set_U3CurlU3Ek__BackingField_1(String_t* value)
	{
		___U3CurlU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CurlU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3ClanguageU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(TaxonomyData_t3741516269, ___U3ClanguageU3Ek__BackingField_2)); }
	inline String_t* get_U3ClanguageU3Ek__BackingField_2() const { return ___U3ClanguageU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3ClanguageU3Ek__BackingField_2() { return &___U3ClanguageU3Ek__BackingField_2; }
	inline void set_U3ClanguageU3Ek__BackingField_2(String_t* value)
	{
		___U3ClanguageU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClanguageU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CtextU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(TaxonomyData_t3741516269, ___U3CtextU3Ek__BackingField_3)); }
	inline String_t* get_U3CtextU3Ek__BackingField_3() const { return ___U3CtextU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CtextU3Ek__BackingField_3() { return &___U3CtextU3Ek__BackingField_3; }
	inline void set_U3CtextU3Ek__BackingField_3(String_t* value)
	{
		___U3CtextU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CtaxonomyU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(TaxonomyData_t3741516269, ___U3CtaxonomyU3Ek__BackingField_4)); }
	inline TaxonomyU5BU5D_t3261933456* get_U3CtaxonomyU3Ek__BackingField_4() const { return ___U3CtaxonomyU3Ek__BackingField_4; }
	inline TaxonomyU5BU5D_t3261933456** get_address_of_U3CtaxonomyU3Ek__BackingField_4() { return &___U3CtaxonomyU3Ek__BackingField_4; }
	inline void set_U3CtaxonomyU3Ek__BackingField_4(TaxonomyU5BU5D_t3261933456* value)
	{
		___U3CtaxonomyU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtaxonomyU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAXONOMYDATA_T3741516269_H
#ifndef SENTIMENTDATA_T471353809_H
#define SENTIMENTDATA_T471353809_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.SentimentData
struct  SentimentData_t471353809  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.SentimentData::<status>k__BackingField
	String_t* ___U3CstatusU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.SentimentData::<url>k__BackingField
	String_t* ___U3CurlU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.SentimentData::<language>k__BackingField
	String_t* ___U3ClanguageU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.SentimentData::<text>k__BackingField
	String_t* ___U3CtextU3Ek__BackingField_3;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.DocSentiment IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.SentimentData::<docSentiment>k__BackingField
	DocSentiment_t2354958393 * ___U3CdocSentimentU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CstatusU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SentimentData_t471353809, ___U3CstatusU3Ek__BackingField_0)); }
	inline String_t* get_U3CstatusU3Ek__BackingField_0() const { return ___U3CstatusU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CstatusU3Ek__BackingField_0() { return &___U3CstatusU3Ek__BackingField_0; }
	inline void set_U3CstatusU3Ek__BackingField_0(String_t* value)
	{
		___U3CstatusU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstatusU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CurlU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(SentimentData_t471353809, ___U3CurlU3Ek__BackingField_1)); }
	inline String_t* get_U3CurlU3Ek__BackingField_1() const { return ___U3CurlU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CurlU3Ek__BackingField_1() { return &___U3CurlU3Ek__BackingField_1; }
	inline void set_U3CurlU3Ek__BackingField_1(String_t* value)
	{
		___U3CurlU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CurlU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3ClanguageU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SentimentData_t471353809, ___U3ClanguageU3Ek__BackingField_2)); }
	inline String_t* get_U3ClanguageU3Ek__BackingField_2() const { return ___U3ClanguageU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3ClanguageU3Ek__BackingField_2() { return &___U3ClanguageU3Ek__BackingField_2; }
	inline void set_U3ClanguageU3Ek__BackingField_2(String_t* value)
	{
		___U3ClanguageU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClanguageU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CtextU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SentimentData_t471353809, ___U3CtextU3Ek__BackingField_3)); }
	inline String_t* get_U3CtextU3Ek__BackingField_3() const { return ___U3CtextU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CtextU3Ek__BackingField_3() { return &___U3CtextU3Ek__BackingField_3; }
	inline void set_U3CtextU3Ek__BackingField_3(String_t* value)
	{
		___U3CtextU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CdocSentimentU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(SentimentData_t471353809, ___U3CdocSentimentU3Ek__BackingField_4)); }
	inline DocSentiment_t2354958393 * get_U3CdocSentimentU3Ek__BackingField_4() const { return ___U3CdocSentimentU3Ek__BackingField_4; }
	inline DocSentiment_t2354958393 ** get_address_of_U3CdocSentimentU3Ek__BackingField_4() { return &___U3CdocSentimentU3Ek__BackingField_4; }
	inline void set_U3CdocSentimentU3Ek__BackingField_4(DocSentiment_t2354958393 * value)
	{
		___U3CdocSentimentU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdocSentimentU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SENTIMENTDATA_T471353809_H
#ifndef COMBINEDCALLDATA_T1377778673_H
#define COMBINEDCALLDATA_T1377778673_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.CombinedCallData
struct  CombinedCallData_t1377778673  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.CombinedCallData::<status>k__BackingField
	String_t* ___U3CstatusU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.CombinedCallData::<url>k__BackingField
	String_t* ___U3CurlU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.CombinedCallData::<language>k__BackingField
	String_t* ___U3ClanguageU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.CombinedCallData::<title>k__BackingField
	String_t* ___U3CtitleU3Ek__BackingField_3;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.CombinedCallData::<text>k__BackingField
	String_t* ___U3CtextU3Ek__BackingField_4;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.CombinedCallData::<image>k__BackingField
	String_t* ___U3CimageU3Ek__BackingField_5;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.ImageKeyword[] IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.CombinedCallData::<imageKeywords>k__BackingField
	ImageKeywordU5BU5D_t3897519977* ___U3CimageKeywordsU3Ek__BackingField_6;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.PublicationDate IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.CombinedCallData::<publicationDate>k__BackingField
	PublicationDate_t608222142 * ___U3CpublicationDateU3Ek__BackingField_7;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Authors IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.CombinedCallData::<authors>k__BackingField
	Authors_t34055864 * ___U3CauthorsU3Ek__BackingField_8;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.DocSentiment IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.CombinedCallData::<docSentiment>k__BackingField
	DocSentiment_t2354958393 * ___U3CdocSentimentU3Ek__BackingField_9;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Feed[] IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.CombinedCallData::<feeds>k__BackingField
	FeedU5BU5D_t332386033* ___U3CfeedsU3Ek__BackingField_10;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Keyword[] IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.CombinedCallData::<keywords>k__BackingField
	KeywordU5BU5D_t3893757366* ___U3CkeywordsU3Ek__BackingField_11;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Concept[] IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.CombinedCallData::<concepts>k__BackingField
	ConceptU5BU5D_t3502654871* ___U3CconceptsU3Ek__BackingField_12;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Entity[] IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.CombinedCallData::<entities>k__BackingField
	EntityU5BU5D_t3969836080* ___U3CentitiesU3Ek__BackingField_13;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Relation[] IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.CombinedCallData::<relations>k__BackingField
	RelationU5BU5D_t1525062961* ___U3CrelationsU3Ek__BackingField_14;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Taxonomy[] IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.CombinedCallData::<taxonomy>k__BackingField
	TaxonomyU5BU5D_t3261933456* ___U3CtaxonomyU3Ek__BackingField_15;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Date[] IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.CombinedCallData::<dates>k__BackingField
	DateU5BU5D_t3621454771* ___U3CdatesU3Ek__BackingField_16;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.DocEmotions[] IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.CombinedCallData::<docEmotions>k__BackingField
	DocEmotionsU5BU5D_t2333025007* ___U3CdocEmotionsU3Ek__BackingField_17;
	// System.Collections.Generic.List`1<System.String> IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.CombinedCallData::_EntityCombined
	List_1_t1398341365 * ____EntityCombined_18;

public:
	inline static int32_t get_offset_of_U3CstatusU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CombinedCallData_t1377778673, ___U3CstatusU3Ek__BackingField_0)); }
	inline String_t* get_U3CstatusU3Ek__BackingField_0() const { return ___U3CstatusU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CstatusU3Ek__BackingField_0() { return &___U3CstatusU3Ek__BackingField_0; }
	inline void set_U3CstatusU3Ek__BackingField_0(String_t* value)
	{
		___U3CstatusU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstatusU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CurlU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CombinedCallData_t1377778673, ___U3CurlU3Ek__BackingField_1)); }
	inline String_t* get_U3CurlU3Ek__BackingField_1() const { return ___U3CurlU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CurlU3Ek__BackingField_1() { return &___U3CurlU3Ek__BackingField_1; }
	inline void set_U3CurlU3Ek__BackingField_1(String_t* value)
	{
		___U3CurlU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CurlU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3ClanguageU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CombinedCallData_t1377778673, ___U3ClanguageU3Ek__BackingField_2)); }
	inline String_t* get_U3ClanguageU3Ek__BackingField_2() const { return ___U3ClanguageU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3ClanguageU3Ek__BackingField_2() { return &___U3ClanguageU3Ek__BackingField_2; }
	inline void set_U3ClanguageU3Ek__BackingField_2(String_t* value)
	{
		___U3ClanguageU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClanguageU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CtitleU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(CombinedCallData_t1377778673, ___U3CtitleU3Ek__BackingField_3)); }
	inline String_t* get_U3CtitleU3Ek__BackingField_3() const { return ___U3CtitleU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CtitleU3Ek__BackingField_3() { return &___U3CtitleU3Ek__BackingField_3; }
	inline void set_U3CtitleU3Ek__BackingField_3(String_t* value)
	{
		___U3CtitleU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtitleU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CtextU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(CombinedCallData_t1377778673, ___U3CtextU3Ek__BackingField_4)); }
	inline String_t* get_U3CtextU3Ek__BackingField_4() const { return ___U3CtextU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CtextU3Ek__BackingField_4() { return &___U3CtextU3Ek__BackingField_4; }
	inline void set_U3CtextU3Ek__BackingField_4(String_t* value)
	{
		___U3CtextU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CimageU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(CombinedCallData_t1377778673, ___U3CimageU3Ek__BackingField_5)); }
	inline String_t* get_U3CimageU3Ek__BackingField_5() const { return ___U3CimageU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CimageU3Ek__BackingField_5() { return &___U3CimageU3Ek__BackingField_5; }
	inline void set_U3CimageU3Ek__BackingField_5(String_t* value)
	{
		___U3CimageU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CimageU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CimageKeywordsU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(CombinedCallData_t1377778673, ___U3CimageKeywordsU3Ek__BackingField_6)); }
	inline ImageKeywordU5BU5D_t3897519977* get_U3CimageKeywordsU3Ek__BackingField_6() const { return ___U3CimageKeywordsU3Ek__BackingField_6; }
	inline ImageKeywordU5BU5D_t3897519977** get_address_of_U3CimageKeywordsU3Ek__BackingField_6() { return &___U3CimageKeywordsU3Ek__BackingField_6; }
	inline void set_U3CimageKeywordsU3Ek__BackingField_6(ImageKeywordU5BU5D_t3897519977* value)
	{
		___U3CimageKeywordsU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CimageKeywordsU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CpublicationDateU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(CombinedCallData_t1377778673, ___U3CpublicationDateU3Ek__BackingField_7)); }
	inline PublicationDate_t608222142 * get_U3CpublicationDateU3Ek__BackingField_7() const { return ___U3CpublicationDateU3Ek__BackingField_7; }
	inline PublicationDate_t608222142 ** get_address_of_U3CpublicationDateU3Ek__BackingField_7() { return &___U3CpublicationDateU3Ek__BackingField_7; }
	inline void set_U3CpublicationDateU3Ek__BackingField_7(PublicationDate_t608222142 * value)
	{
		___U3CpublicationDateU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpublicationDateU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CauthorsU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(CombinedCallData_t1377778673, ___U3CauthorsU3Ek__BackingField_8)); }
	inline Authors_t34055864 * get_U3CauthorsU3Ek__BackingField_8() const { return ___U3CauthorsU3Ek__BackingField_8; }
	inline Authors_t34055864 ** get_address_of_U3CauthorsU3Ek__BackingField_8() { return &___U3CauthorsU3Ek__BackingField_8; }
	inline void set_U3CauthorsU3Ek__BackingField_8(Authors_t34055864 * value)
	{
		___U3CauthorsU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CauthorsU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CdocSentimentU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(CombinedCallData_t1377778673, ___U3CdocSentimentU3Ek__BackingField_9)); }
	inline DocSentiment_t2354958393 * get_U3CdocSentimentU3Ek__BackingField_9() const { return ___U3CdocSentimentU3Ek__BackingField_9; }
	inline DocSentiment_t2354958393 ** get_address_of_U3CdocSentimentU3Ek__BackingField_9() { return &___U3CdocSentimentU3Ek__BackingField_9; }
	inline void set_U3CdocSentimentU3Ek__BackingField_9(DocSentiment_t2354958393 * value)
	{
		___U3CdocSentimentU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdocSentimentU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CfeedsU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(CombinedCallData_t1377778673, ___U3CfeedsU3Ek__BackingField_10)); }
	inline FeedU5BU5D_t332386033* get_U3CfeedsU3Ek__BackingField_10() const { return ___U3CfeedsU3Ek__BackingField_10; }
	inline FeedU5BU5D_t332386033** get_address_of_U3CfeedsU3Ek__BackingField_10() { return &___U3CfeedsU3Ek__BackingField_10; }
	inline void set_U3CfeedsU3Ek__BackingField_10(FeedU5BU5D_t332386033* value)
	{
		___U3CfeedsU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CfeedsU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_U3CkeywordsU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(CombinedCallData_t1377778673, ___U3CkeywordsU3Ek__BackingField_11)); }
	inline KeywordU5BU5D_t3893757366* get_U3CkeywordsU3Ek__BackingField_11() const { return ___U3CkeywordsU3Ek__BackingField_11; }
	inline KeywordU5BU5D_t3893757366** get_address_of_U3CkeywordsU3Ek__BackingField_11() { return &___U3CkeywordsU3Ek__BackingField_11; }
	inline void set_U3CkeywordsU3Ek__BackingField_11(KeywordU5BU5D_t3893757366* value)
	{
		___U3CkeywordsU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CkeywordsU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CconceptsU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(CombinedCallData_t1377778673, ___U3CconceptsU3Ek__BackingField_12)); }
	inline ConceptU5BU5D_t3502654871* get_U3CconceptsU3Ek__BackingField_12() const { return ___U3CconceptsU3Ek__BackingField_12; }
	inline ConceptU5BU5D_t3502654871** get_address_of_U3CconceptsU3Ek__BackingField_12() { return &___U3CconceptsU3Ek__BackingField_12; }
	inline void set_U3CconceptsU3Ek__BackingField_12(ConceptU5BU5D_t3502654871* value)
	{
		___U3CconceptsU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CconceptsU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CentitiesU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(CombinedCallData_t1377778673, ___U3CentitiesU3Ek__BackingField_13)); }
	inline EntityU5BU5D_t3969836080* get_U3CentitiesU3Ek__BackingField_13() const { return ___U3CentitiesU3Ek__BackingField_13; }
	inline EntityU5BU5D_t3969836080** get_address_of_U3CentitiesU3Ek__BackingField_13() { return &___U3CentitiesU3Ek__BackingField_13; }
	inline void set_U3CentitiesU3Ek__BackingField_13(EntityU5BU5D_t3969836080* value)
	{
		___U3CentitiesU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CentitiesU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CrelationsU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(CombinedCallData_t1377778673, ___U3CrelationsU3Ek__BackingField_14)); }
	inline RelationU5BU5D_t1525062961* get_U3CrelationsU3Ek__BackingField_14() const { return ___U3CrelationsU3Ek__BackingField_14; }
	inline RelationU5BU5D_t1525062961** get_address_of_U3CrelationsU3Ek__BackingField_14() { return &___U3CrelationsU3Ek__BackingField_14; }
	inline void set_U3CrelationsU3Ek__BackingField_14(RelationU5BU5D_t1525062961* value)
	{
		___U3CrelationsU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrelationsU3Ek__BackingField_14), value);
	}

	inline static int32_t get_offset_of_U3CtaxonomyU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(CombinedCallData_t1377778673, ___U3CtaxonomyU3Ek__BackingField_15)); }
	inline TaxonomyU5BU5D_t3261933456* get_U3CtaxonomyU3Ek__BackingField_15() const { return ___U3CtaxonomyU3Ek__BackingField_15; }
	inline TaxonomyU5BU5D_t3261933456** get_address_of_U3CtaxonomyU3Ek__BackingField_15() { return &___U3CtaxonomyU3Ek__BackingField_15; }
	inline void set_U3CtaxonomyU3Ek__BackingField_15(TaxonomyU5BU5D_t3261933456* value)
	{
		___U3CtaxonomyU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtaxonomyU3Ek__BackingField_15), value);
	}

	inline static int32_t get_offset_of_U3CdatesU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(CombinedCallData_t1377778673, ___U3CdatesU3Ek__BackingField_16)); }
	inline DateU5BU5D_t3621454771* get_U3CdatesU3Ek__BackingField_16() const { return ___U3CdatesU3Ek__BackingField_16; }
	inline DateU5BU5D_t3621454771** get_address_of_U3CdatesU3Ek__BackingField_16() { return &___U3CdatesU3Ek__BackingField_16; }
	inline void set_U3CdatesU3Ek__BackingField_16(DateU5BU5D_t3621454771* value)
	{
		___U3CdatesU3Ek__BackingField_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdatesU3Ek__BackingField_16), value);
	}

	inline static int32_t get_offset_of_U3CdocEmotionsU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(CombinedCallData_t1377778673, ___U3CdocEmotionsU3Ek__BackingField_17)); }
	inline DocEmotionsU5BU5D_t2333025007* get_U3CdocEmotionsU3Ek__BackingField_17() const { return ___U3CdocEmotionsU3Ek__BackingField_17; }
	inline DocEmotionsU5BU5D_t2333025007** get_address_of_U3CdocEmotionsU3Ek__BackingField_17() { return &___U3CdocEmotionsU3Ek__BackingField_17; }
	inline void set_U3CdocEmotionsU3Ek__BackingField_17(DocEmotionsU5BU5D_t2333025007* value)
	{
		___U3CdocEmotionsU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdocEmotionsU3Ek__BackingField_17), value);
	}

	inline static int32_t get_offset_of__EntityCombined_18() { return static_cast<int32_t>(offsetof(CombinedCallData_t1377778673, ____EntityCombined_18)); }
	inline List_1_t1398341365 * get__EntityCombined_18() const { return ____EntityCombined_18; }
	inline List_1_t1398341365 ** get_address_of__EntityCombined_18() { return &____EntityCombined_18; }
	inline void set__EntityCombined_18(List_1_t1398341365 * value)
	{
		____EntityCombined_18 = value;
		Il2CppCodeGenWriteBarrier((&____EntityCombined_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMBINEDCALLDATA_T1377778673_H
#ifndef PUBDATEDATA_T3405155195_H
#define PUBDATEDATA_T3405155195_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.PubDateData
struct  PubDateData_t3405155195  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.PubDateData::<status>k__BackingField
	String_t* ___U3CstatusU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.PubDateData::<url>k__BackingField
	String_t* ___U3CurlU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.PubDateData::<language>k__BackingField
	String_t* ___U3ClanguageU3Ek__BackingField_2;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.PublicationDate IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.PubDateData::<publicationDate>k__BackingField
	PublicationDate_t608222142 * ___U3CpublicationDateU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CstatusU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(PubDateData_t3405155195, ___U3CstatusU3Ek__BackingField_0)); }
	inline String_t* get_U3CstatusU3Ek__BackingField_0() const { return ___U3CstatusU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CstatusU3Ek__BackingField_0() { return &___U3CstatusU3Ek__BackingField_0; }
	inline void set_U3CstatusU3Ek__BackingField_0(String_t* value)
	{
		___U3CstatusU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstatusU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CurlU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PubDateData_t3405155195, ___U3CurlU3Ek__BackingField_1)); }
	inline String_t* get_U3CurlU3Ek__BackingField_1() const { return ___U3CurlU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CurlU3Ek__BackingField_1() { return &___U3CurlU3Ek__BackingField_1; }
	inline void set_U3CurlU3Ek__BackingField_1(String_t* value)
	{
		___U3CurlU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CurlU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3ClanguageU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PubDateData_t3405155195, ___U3ClanguageU3Ek__BackingField_2)); }
	inline String_t* get_U3ClanguageU3Ek__BackingField_2() const { return ___U3ClanguageU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3ClanguageU3Ek__BackingField_2() { return &___U3ClanguageU3Ek__BackingField_2; }
	inline void set_U3ClanguageU3Ek__BackingField_2(String_t* value)
	{
		___U3ClanguageU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClanguageU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CpublicationDateU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(PubDateData_t3405155195, ___U3CpublicationDateU3Ek__BackingField_3)); }
	inline PublicationDate_t608222142 * get_U3CpublicationDateU3Ek__BackingField_3() const { return ___U3CpublicationDateU3Ek__BackingField_3; }
	inline PublicationDate_t608222142 ** get_address_of_U3CpublicationDateU3Ek__BackingField_3() { return &___U3CpublicationDateU3Ek__BackingField_3; }
	inline void set_U3CpublicationDateU3Ek__BackingField_3(PublicationDate_t608222142 * value)
	{
		___U3CpublicationDateU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpublicationDateU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUBDATEDATA_T3405155195_H
#ifndef RELATIONSDATA_T3907134773_H
#define RELATIONSDATA_T3907134773_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.RelationsData
struct  RelationsData_t3907134773  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.RelationsData::<status>k__BackingField
	String_t* ___U3CstatusU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.RelationsData::<url>k__BackingField
	String_t* ___U3CurlU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.RelationsData::<language>k__BackingField
	String_t* ___U3ClanguageU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.RelationsData::<text>k__BackingField
	String_t* ___U3CtextU3Ek__BackingField_3;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Relation[] IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.RelationsData::<relations>k__BackingField
	RelationU5BU5D_t1525062961* ___U3CrelationsU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CstatusU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RelationsData_t3907134773, ___U3CstatusU3Ek__BackingField_0)); }
	inline String_t* get_U3CstatusU3Ek__BackingField_0() const { return ___U3CstatusU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CstatusU3Ek__BackingField_0() { return &___U3CstatusU3Ek__BackingField_0; }
	inline void set_U3CstatusU3Ek__BackingField_0(String_t* value)
	{
		___U3CstatusU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstatusU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CurlU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RelationsData_t3907134773, ___U3CurlU3Ek__BackingField_1)); }
	inline String_t* get_U3CurlU3Ek__BackingField_1() const { return ___U3CurlU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CurlU3Ek__BackingField_1() { return &___U3CurlU3Ek__BackingField_1; }
	inline void set_U3CurlU3Ek__BackingField_1(String_t* value)
	{
		___U3CurlU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CurlU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3ClanguageU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(RelationsData_t3907134773, ___U3ClanguageU3Ek__BackingField_2)); }
	inline String_t* get_U3ClanguageU3Ek__BackingField_2() const { return ___U3ClanguageU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3ClanguageU3Ek__BackingField_2() { return &___U3ClanguageU3Ek__BackingField_2; }
	inline void set_U3ClanguageU3Ek__BackingField_2(String_t* value)
	{
		___U3ClanguageU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClanguageU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CtextU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(RelationsData_t3907134773, ___U3CtextU3Ek__BackingField_3)); }
	inline String_t* get_U3CtextU3Ek__BackingField_3() const { return ___U3CtextU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CtextU3Ek__BackingField_3() { return &___U3CtextU3Ek__BackingField_3; }
	inline void set_U3CtextU3Ek__BackingField_3(String_t* value)
	{
		___U3CtextU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CrelationsU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(RelationsData_t3907134773, ___U3CrelationsU3Ek__BackingField_4)); }
	inline RelationU5BU5D_t1525062961* get_U3CrelationsU3Ek__BackingField_4() const { return ___U3CrelationsU3Ek__BackingField_4; }
	inline RelationU5BU5D_t1525062961** get_address_of_U3CrelationsU3Ek__BackingField_4() { return &___U3CrelationsU3Ek__BackingField_4; }
	inline void set_U3CrelationsU3Ek__BackingField_4(RelationU5BU5D_t1525062961* value)
	{
		___U3CrelationsU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrelationsU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RELATIONSDATA_T3907134773_H
#ifndef TEXTDATA_T3443651365_H
#define TEXTDATA_T3443651365_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.TextData
struct  TextData_t3443651365  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.TextData::<status>k__BackingField
	String_t* ___U3CstatusU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.TextData::<url>k__BackingField
	String_t* ___U3CurlU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.TextData::<text>k__BackingField
	String_t* ___U3CtextU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.TextData::<language>k__BackingField
	String_t* ___U3ClanguageU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CstatusU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TextData_t3443651365, ___U3CstatusU3Ek__BackingField_0)); }
	inline String_t* get_U3CstatusU3Ek__BackingField_0() const { return ___U3CstatusU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CstatusU3Ek__BackingField_0() { return &___U3CstatusU3Ek__BackingField_0; }
	inline void set_U3CstatusU3Ek__BackingField_0(String_t* value)
	{
		___U3CstatusU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstatusU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CurlU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TextData_t3443651365, ___U3CurlU3Ek__BackingField_1)); }
	inline String_t* get_U3CurlU3Ek__BackingField_1() const { return ___U3CurlU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CurlU3Ek__BackingField_1() { return &___U3CurlU3Ek__BackingField_1; }
	inline void set_U3CurlU3Ek__BackingField_1(String_t* value)
	{
		___U3CurlU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CurlU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CtextU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(TextData_t3443651365, ___U3CtextU3Ek__BackingField_2)); }
	inline String_t* get_U3CtextU3Ek__BackingField_2() const { return ___U3CtextU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CtextU3Ek__BackingField_2() { return &___U3CtextU3Ek__BackingField_2; }
	inline void set_U3CtextU3Ek__BackingField_2(String_t* value)
	{
		___U3CtextU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3ClanguageU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(TextData_t3443651365, ___U3ClanguageU3Ek__BackingField_3)); }
	inline String_t* get_U3ClanguageU3Ek__BackingField_3() const { return ___U3ClanguageU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3ClanguageU3Ek__BackingField_3() { return &___U3ClanguageU3Ek__BackingField_3; }
	inline void set_U3ClanguageU3Ek__BackingField_3(String_t* value)
	{
		___U3ClanguageU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClanguageU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTDATA_T3443651365_H
#ifndef QUOTATION_T2638841872_H
#define QUOTATION_T2638841872_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Quotation
struct  Quotation_t2638841872  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Quotation::<quotation>k__BackingField
	String_t* ___U3CquotationU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CquotationU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Quotation_t2638841872, ___U3CquotationU3Ek__BackingField_0)); }
	inline String_t* get_U3CquotationU3Ek__BackingField_0() const { return ___U3CquotationU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CquotationU3Ek__BackingField_0() { return &___U3CquotationU3Ek__BackingField_0; }
	inline void set_U3CquotationU3Ek__BackingField_0(String_t* value)
	{
		___U3CquotationU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CquotationU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUOTATION_T2638841872_H
#ifndef DOCSENTIMENT_T2354958393_H
#define DOCSENTIMENT_T2354958393_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.DocSentiment
struct  DocSentiment_t2354958393  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.DocSentiment::<type>k__BackingField
	String_t* ___U3CtypeU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.DocSentiment::<score>k__BackingField
	String_t* ___U3CscoreU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.DocSentiment::<mixed>k__BackingField
	String_t* ___U3CmixedU3Ek__BackingField_2;
	// System.Double IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.DocSentiment::m_Score
	double ___m_Score_3;

public:
	inline static int32_t get_offset_of_U3CtypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DocSentiment_t2354958393, ___U3CtypeU3Ek__BackingField_0)); }
	inline String_t* get_U3CtypeU3Ek__BackingField_0() const { return ___U3CtypeU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CtypeU3Ek__BackingField_0() { return &___U3CtypeU3Ek__BackingField_0; }
	inline void set_U3CtypeU3Ek__BackingField_0(String_t* value)
	{
		___U3CtypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtypeU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CscoreU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(DocSentiment_t2354958393, ___U3CscoreU3Ek__BackingField_1)); }
	inline String_t* get_U3CscoreU3Ek__BackingField_1() const { return ___U3CscoreU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CscoreU3Ek__BackingField_1() { return &___U3CscoreU3Ek__BackingField_1; }
	inline void set_U3CscoreU3Ek__BackingField_1(String_t* value)
	{
		___U3CscoreU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CscoreU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CmixedU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(DocSentiment_t2354958393, ___U3CmixedU3Ek__BackingField_2)); }
	inline String_t* get_U3CmixedU3Ek__BackingField_2() const { return ___U3CmixedU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CmixedU3Ek__BackingField_2() { return &___U3CmixedU3Ek__BackingField_2; }
	inline void set_U3CmixedU3Ek__BackingField_2(String_t* value)
	{
		___U3CmixedU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmixedU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_m_Score_3() { return static_cast<int32_t>(offsetof(DocSentiment_t2354958393, ___m_Score_3)); }
	inline double get_m_Score_3() const { return ___m_Score_3; }
	inline double* get_address_of_m_Score_3() { return &___m_Score_3; }
	inline void set_m_Score_3(double value)
	{
		___m_Score_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOCSENTIMENT_T2354958393_H
#ifndef PUBLICATIONDATE_T608222142_H
#define PUBLICATIONDATE_T608222142_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.PublicationDate
struct  PublicationDate_t608222142  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.PublicationDate::<date>k__BackingField
	String_t* ___U3CdateU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.PublicationDate::<confident>k__BackingField
	String_t* ___U3CconfidentU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CdateU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(PublicationDate_t608222142, ___U3CdateU3Ek__BackingField_0)); }
	inline String_t* get_U3CdateU3Ek__BackingField_0() const { return ___U3CdateU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CdateU3Ek__BackingField_0() { return &___U3CdateU3Ek__BackingField_0; }
	inline void set_U3CdateU3Ek__BackingField_0(String_t* value)
	{
		___U3CdateU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdateU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CconfidentU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PublicationDate_t608222142, ___U3CconfidentU3Ek__BackingField_1)); }
	inline String_t* get_U3CconfidentU3Ek__BackingField_1() const { return ___U3CconfidentU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CconfidentU3Ek__BackingField_1() { return &___U3CconfidentU3Ek__BackingField_1; }
	inline void set_U3CconfidentU3Ek__BackingField_1(String_t* value)
	{
		___U3CconfidentU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CconfidentU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUBLICATIONDATE_T608222142_H
#ifndef KEYWORDDATA_T1578948319_H
#define KEYWORDDATA_T1578948319_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.KeywordData
struct  KeywordData_t1578948319  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.KeywordData::<status>k__BackingField
	String_t* ___U3CstatusU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.KeywordData::<url>k__BackingField
	String_t* ___U3CurlU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.KeywordData::<language>k__BackingField
	String_t* ___U3ClanguageU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.KeywordData::<text>k__BackingField
	String_t* ___U3CtextU3Ek__BackingField_3;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Keyword[] IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.KeywordData::<keywords>k__BackingField
	KeywordU5BU5D_t3893757366* ___U3CkeywordsU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CstatusU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(KeywordData_t1578948319, ___U3CstatusU3Ek__BackingField_0)); }
	inline String_t* get_U3CstatusU3Ek__BackingField_0() const { return ___U3CstatusU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CstatusU3Ek__BackingField_0() { return &___U3CstatusU3Ek__BackingField_0; }
	inline void set_U3CstatusU3Ek__BackingField_0(String_t* value)
	{
		___U3CstatusU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstatusU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CurlU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(KeywordData_t1578948319, ___U3CurlU3Ek__BackingField_1)); }
	inline String_t* get_U3CurlU3Ek__BackingField_1() const { return ___U3CurlU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CurlU3Ek__BackingField_1() { return &___U3CurlU3Ek__BackingField_1; }
	inline void set_U3CurlU3Ek__BackingField_1(String_t* value)
	{
		___U3CurlU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CurlU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3ClanguageU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(KeywordData_t1578948319, ___U3ClanguageU3Ek__BackingField_2)); }
	inline String_t* get_U3ClanguageU3Ek__BackingField_2() const { return ___U3ClanguageU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3ClanguageU3Ek__BackingField_2() { return &___U3ClanguageU3Ek__BackingField_2; }
	inline void set_U3ClanguageU3Ek__BackingField_2(String_t* value)
	{
		___U3ClanguageU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClanguageU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CtextU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(KeywordData_t1578948319, ___U3CtextU3Ek__BackingField_3)); }
	inline String_t* get_U3CtextU3Ek__BackingField_3() const { return ___U3CtextU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CtextU3Ek__BackingField_3() { return &___U3CtextU3Ek__BackingField_3; }
	inline void set_U3CtextU3Ek__BackingField_3(String_t* value)
	{
		___U3CtextU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CkeywordsU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(KeywordData_t1578948319, ___U3CkeywordsU3Ek__BackingField_4)); }
	inline KeywordU5BU5D_t3893757366* get_U3CkeywordsU3Ek__BackingField_4() const { return ___U3CkeywordsU3Ek__BackingField_4; }
	inline KeywordU5BU5D_t3893757366** get_address_of_U3CkeywordsU3Ek__BackingField_4() { return &___U3CkeywordsU3Ek__BackingField_4; }
	inline void set_U3CkeywordsU3Ek__BackingField_4(KeywordU5BU5D_t3893757366* value)
	{
		___U3CkeywordsU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CkeywordsU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYWORDDATA_T1578948319_H
#ifndef TITLE_T2058683094_H
#define TITLE_T2058683094_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Title
struct  Title_t2058683094  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Title::<status>k__BackingField
	String_t* ___U3CstatusU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Title::<url>k__BackingField
	String_t* ___U3CurlU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Title::<title>k__BackingField
	String_t* ___U3CtitleU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CstatusU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Title_t2058683094, ___U3CstatusU3Ek__BackingField_0)); }
	inline String_t* get_U3CstatusU3Ek__BackingField_0() const { return ___U3CstatusU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CstatusU3Ek__BackingField_0() { return &___U3CstatusU3Ek__BackingField_0; }
	inline void set_U3CstatusU3Ek__BackingField_0(String_t* value)
	{
		___U3CstatusU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstatusU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CurlU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Title_t2058683094, ___U3CurlU3Ek__BackingField_1)); }
	inline String_t* get_U3CurlU3Ek__BackingField_1() const { return ___U3CurlU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CurlU3Ek__BackingField_1() { return &___U3CurlU3Ek__BackingField_1; }
	inline void set_U3CurlU3Ek__BackingField_1(String_t* value)
	{
		___U3CurlU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CurlU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CtitleU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Title_t2058683094, ___U3CtitleU3Ek__BackingField_2)); }
	inline String_t* get_U3CtitleU3Ek__BackingField_2() const { return ___U3CtitleU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CtitleU3Ek__BackingField_2() { return &___U3CtitleU3Ek__BackingField_2; }
	inline void set_U3CtitleU3Ek__BackingField_2(String_t* value)
	{
		___U3CtitleU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtitleU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TITLE_T2058683094_H
#ifndef KNOWLEDGEGRAPH_T3341597436_H
#define KNOWLEDGEGRAPH_T3341597436_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.KnowledgeGraph
struct  KnowledgeGraph_t3341597436  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.KnowledgeGraph::<typeHierarchy>k__BackingField
	String_t* ___U3CtypeHierarchyU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CtypeHierarchyU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(KnowledgeGraph_t3341597436, ___U3CtypeHierarchyU3Ek__BackingField_0)); }
	inline String_t* get_U3CtypeHierarchyU3Ek__BackingField_0() const { return ___U3CtypeHierarchyU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CtypeHierarchyU3Ek__BackingField_0() { return &___U3CtypeHierarchyU3Ek__BackingField_0; }
	inline void set_U3CtypeHierarchyU3Ek__BackingField_0(String_t* value)
	{
		___U3CtypeHierarchyU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtypeHierarchyU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KNOWLEDGEGRAPH_T3341597436_H
#ifndef DISAMBIGUATED_T227057183_H
#define DISAMBIGUATED_T227057183_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Disambiguated
struct  Disambiguated_t227057183  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Disambiguated::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Disambiguated::<subType>k__BackingField
	String_t* ___U3CsubTypeU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Disambiguated::<website>k__BackingField
	String_t* ___U3CwebsiteU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Disambiguated::<geo>k__BackingField
	String_t* ___U3CgeoU3Ek__BackingField_3;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Disambiguated::<dbpedia>k__BackingField
	String_t* ___U3CdbpediaU3Ek__BackingField_4;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Disambiguated::<yago>k__BackingField
	String_t* ___U3CyagoU3Ek__BackingField_5;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Disambiguated::<opencyc>k__BackingField
	String_t* ___U3CopencycU3Ek__BackingField_6;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Disambiguated::<umbel>k__BackingField
	String_t* ___U3CumbelU3Ek__BackingField_7;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Disambiguated::<freebase>k__BackingField
	String_t* ___U3CfreebaseU3Ek__BackingField_8;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Disambiguated::<ciaFactbook>k__BackingField
	String_t* ___U3CciaFactbookU3Ek__BackingField_9;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Disambiguated::<census>k__BackingField
	String_t* ___U3CcensusU3Ek__BackingField_10;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Disambiguated::<geonames>k__BackingField
	String_t* ___U3CgeonamesU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Disambiguated::<musicBrainz>k__BackingField
	String_t* ___U3CmusicBrainzU3Ek__BackingField_12;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Disambiguated::<crunchbase>k__BackingField
	String_t* ___U3CcrunchbaseU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Disambiguated_t227057183, ___U3CnameU3Ek__BackingField_0)); }
	inline String_t* get_U3CnameU3Ek__BackingField_0() const { return ___U3CnameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_0() { return &___U3CnameU3Ek__BackingField_0; }
	inline void set_U3CnameU3Ek__BackingField_0(String_t* value)
	{
		___U3CnameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CsubTypeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Disambiguated_t227057183, ___U3CsubTypeU3Ek__BackingField_1)); }
	inline String_t* get_U3CsubTypeU3Ek__BackingField_1() const { return ___U3CsubTypeU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CsubTypeU3Ek__BackingField_1() { return &___U3CsubTypeU3Ek__BackingField_1; }
	inline void set_U3CsubTypeU3Ek__BackingField_1(String_t* value)
	{
		___U3CsubTypeU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsubTypeU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CwebsiteU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Disambiguated_t227057183, ___U3CwebsiteU3Ek__BackingField_2)); }
	inline String_t* get_U3CwebsiteU3Ek__BackingField_2() const { return ___U3CwebsiteU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CwebsiteU3Ek__BackingField_2() { return &___U3CwebsiteU3Ek__BackingField_2; }
	inline void set_U3CwebsiteU3Ek__BackingField_2(String_t* value)
	{
		___U3CwebsiteU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwebsiteU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CgeoU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Disambiguated_t227057183, ___U3CgeoU3Ek__BackingField_3)); }
	inline String_t* get_U3CgeoU3Ek__BackingField_3() const { return ___U3CgeoU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CgeoU3Ek__BackingField_3() { return &___U3CgeoU3Ek__BackingField_3; }
	inline void set_U3CgeoU3Ek__BackingField_3(String_t* value)
	{
		___U3CgeoU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CgeoU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CdbpediaU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Disambiguated_t227057183, ___U3CdbpediaU3Ek__BackingField_4)); }
	inline String_t* get_U3CdbpediaU3Ek__BackingField_4() const { return ___U3CdbpediaU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CdbpediaU3Ek__BackingField_4() { return &___U3CdbpediaU3Ek__BackingField_4; }
	inline void set_U3CdbpediaU3Ek__BackingField_4(String_t* value)
	{
		___U3CdbpediaU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdbpediaU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CyagoU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Disambiguated_t227057183, ___U3CyagoU3Ek__BackingField_5)); }
	inline String_t* get_U3CyagoU3Ek__BackingField_5() const { return ___U3CyagoU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CyagoU3Ek__BackingField_5() { return &___U3CyagoU3Ek__BackingField_5; }
	inline void set_U3CyagoU3Ek__BackingField_5(String_t* value)
	{
		___U3CyagoU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CyagoU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CopencycU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Disambiguated_t227057183, ___U3CopencycU3Ek__BackingField_6)); }
	inline String_t* get_U3CopencycU3Ek__BackingField_6() const { return ___U3CopencycU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CopencycU3Ek__BackingField_6() { return &___U3CopencycU3Ek__BackingField_6; }
	inline void set_U3CopencycU3Ek__BackingField_6(String_t* value)
	{
		___U3CopencycU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CopencycU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CumbelU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(Disambiguated_t227057183, ___U3CumbelU3Ek__BackingField_7)); }
	inline String_t* get_U3CumbelU3Ek__BackingField_7() const { return ___U3CumbelU3Ek__BackingField_7; }
	inline String_t** get_address_of_U3CumbelU3Ek__BackingField_7() { return &___U3CumbelU3Ek__BackingField_7; }
	inline void set_U3CumbelU3Ek__BackingField_7(String_t* value)
	{
		___U3CumbelU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CumbelU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CfreebaseU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(Disambiguated_t227057183, ___U3CfreebaseU3Ek__BackingField_8)); }
	inline String_t* get_U3CfreebaseU3Ek__BackingField_8() const { return ___U3CfreebaseU3Ek__BackingField_8; }
	inline String_t** get_address_of_U3CfreebaseU3Ek__BackingField_8() { return &___U3CfreebaseU3Ek__BackingField_8; }
	inline void set_U3CfreebaseU3Ek__BackingField_8(String_t* value)
	{
		___U3CfreebaseU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CfreebaseU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CciaFactbookU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(Disambiguated_t227057183, ___U3CciaFactbookU3Ek__BackingField_9)); }
	inline String_t* get_U3CciaFactbookU3Ek__BackingField_9() const { return ___U3CciaFactbookU3Ek__BackingField_9; }
	inline String_t** get_address_of_U3CciaFactbookU3Ek__BackingField_9() { return &___U3CciaFactbookU3Ek__BackingField_9; }
	inline void set_U3CciaFactbookU3Ek__BackingField_9(String_t* value)
	{
		___U3CciaFactbookU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CciaFactbookU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CcensusU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(Disambiguated_t227057183, ___U3CcensusU3Ek__BackingField_10)); }
	inline String_t* get_U3CcensusU3Ek__BackingField_10() const { return ___U3CcensusU3Ek__BackingField_10; }
	inline String_t** get_address_of_U3CcensusU3Ek__BackingField_10() { return &___U3CcensusU3Ek__BackingField_10; }
	inline void set_U3CcensusU3Ek__BackingField_10(String_t* value)
	{
		___U3CcensusU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcensusU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_U3CgeonamesU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(Disambiguated_t227057183, ___U3CgeonamesU3Ek__BackingField_11)); }
	inline String_t* get_U3CgeonamesU3Ek__BackingField_11() const { return ___U3CgeonamesU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CgeonamesU3Ek__BackingField_11() { return &___U3CgeonamesU3Ek__BackingField_11; }
	inline void set_U3CgeonamesU3Ek__BackingField_11(String_t* value)
	{
		___U3CgeonamesU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CgeonamesU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CmusicBrainzU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(Disambiguated_t227057183, ___U3CmusicBrainzU3Ek__BackingField_12)); }
	inline String_t* get_U3CmusicBrainzU3Ek__BackingField_12() const { return ___U3CmusicBrainzU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CmusicBrainzU3Ek__BackingField_12() { return &___U3CmusicBrainzU3Ek__BackingField_12; }
	inline void set_U3CmusicBrainzU3Ek__BackingField_12(String_t* value)
	{
		___U3CmusicBrainzU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmusicBrainzU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CcrunchbaseU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(Disambiguated_t227057183, ___U3CcrunchbaseU3Ek__BackingField_13)); }
	inline String_t* get_U3CcrunchbaseU3Ek__BackingField_13() const { return ___U3CcrunchbaseU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CcrunchbaseU3Ek__BackingField_13() { return &___U3CcrunchbaseU3Ek__BackingField_13; }
	inline void set_U3CcrunchbaseU3Ek__BackingField_13(String_t* value)
	{
		___U3CcrunchbaseU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcrunchbaseU3Ek__BackingField_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISAMBIGUATED_T227057183_H
#ifndef IMAGEKEYWORD_T1009764024_H
#define IMAGEKEYWORD_T1009764024_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.ImageKeyword
struct  ImageKeyword_t1009764024  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.ImageKeyword::<text>k__BackingField
	String_t* ___U3CtextU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.ImageKeyword::<score>k__BackingField
	String_t* ___U3CscoreU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CtextU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ImageKeyword_t1009764024, ___U3CtextU3Ek__BackingField_0)); }
	inline String_t* get_U3CtextU3Ek__BackingField_0() const { return ___U3CtextU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CtextU3Ek__BackingField_0() { return &___U3CtextU3Ek__BackingField_0; }
	inline void set_U3CtextU3Ek__BackingField_0(String_t* value)
	{
		___U3CtextU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CscoreU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ImageKeyword_t1009764024, ___U3CscoreU3Ek__BackingField_1)); }
	inline String_t* get_U3CscoreU3Ek__BackingField_1() const { return ___U3CscoreU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CscoreU3Ek__BackingField_1() { return &___U3CscoreU3Ek__BackingField_1; }
	inline void set_U3CscoreU3Ek__BackingField_1(String_t* value)
	{
		___U3CscoreU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CscoreU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGEKEYWORD_T1009764024_H
#ifndef MICROFORMATDATA_T655366775_H
#define MICROFORMATDATA_T655366775_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.MicroformatData
struct  MicroformatData_t655366775  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.MicroformatData::<status>k__BackingField
	String_t* ___U3CstatusU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.MicroformatData::<url>k__BackingField
	String_t* ___U3CurlU3Ek__BackingField_1;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Microformat[] IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.MicroformatData::<microformats>k__BackingField
	MicroformatU5BU5D_t3715084158* ___U3CmicroformatsU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CstatusU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MicroformatData_t655366775, ___U3CstatusU3Ek__BackingField_0)); }
	inline String_t* get_U3CstatusU3Ek__BackingField_0() const { return ___U3CstatusU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CstatusU3Ek__BackingField_0() { return &___U3CstatusU3Ek__BackingField_0; }
	inline void set_U3CstatusU3Ek__BackingField_0(String_t* value)
	{
		___U3CstatusU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstatusU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CurlU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(MicroformatData_t655366775, ___U3CurlU3Ek__BackingField_1)); }
	inline String_t* get_U3CurlU3Ek__BackingField_1() const { return ___U3CurlU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CurlU3Ek__BackingField_1() { return &___U3CurlU3Ek__BackingField_1; }
	inline void set_U3CurlU3Ek__BackingField_1(String_t* value)
	{
		___U3CurlU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CurlU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CmicroformatsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(MicroformatData_t655366775, ___U3CmicroformatsU3Ek__BackingField_2)); }
	inline MicroformatU5BU5D_t3715084158* get_U3CmicroformatsU3Ek__BackingField_2() const { return ___U3CmicroformatsU3Ek__BackingField_2; }
	inline MicroformatU5BU5D_t3715084158** get_address_of_U3CmicroformatsU3Ek__BackingField_2() { return &___U3CmicroformatsU3Ek__BackingField_2; }
	inline void set_U3CmicroformatsU3Ek__BackingField_2(MicroformatU5BU5D_t3715084158* value)
	{
		___U3CmicroformatsU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmicroformatsU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MICROFORMATDATA_T655366775_H
#ifndef ENTITYDATA_T436806285_H
#define ENTITYDATA_T436806285_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.EntityData
struct  EntityData_t436806285  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.EntityData::<status>k__BackingField
	String_t* ___U3CstatusU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.EntityData::<url>k__BackingField
	String_t* ___U3CurlU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.EntityData::<language>k__BackingField
	String_t* ___U3ClanguageU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.EntityData::<text>k__BackingField
	String_t* ___U3CtextU3Ek__BackingField_3;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Entity[] IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.EntityData::<entities>k__BackingField
	EntityU5BU5D_t3969836080* ___U3CentitiesU3Ek__BackingField_4;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.PositionOnMap IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.EntityData::m_GeoLocation
	PositionOnMap_t66846110 * ___m_GeoLocation_5;

public:
	inline static int32_t get_offset_of_U3CstatusU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(EntityData_t436806285, ___U3CstatusU3Ek__BackingField_0)); }
	inline String_t* get_U3CstatusU3Ek__BackingField_0() const { return ___U3CstatusU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CstatusU3Ek__BackingField_0() { return &___U3CstatusU3Ek__BackingField_0; }
	inline void set_U3CstatusU3Ek__BackingField_0(String_t* value)
	{
		___U3CstatusU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstatusU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CurlU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(EntityData_t436806285, ___U3CurlU3Ek__BackingField_1)); }
	inline String_t* get_U3CurlU3Ek__BackingField_1() const { return ___U3CurlU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CurlU3Ek__BackingField_1() { return &___U3CurlU3Ek__BackingField_1; }
	inline void set_U3CurlU3Ek__BackingField_1(String_t* value)
	{
		___U3CurlU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CurlU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3ClanguageU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(EntityData_t436806285, ___U3ClanguageU3Ek__BackingField_2)); }
	inline String_t* get_U3ClanguageU3Ek__BackingField_2() const { return ___U3ClanguageU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3ClanguageU3Ek__BackingField_2() { return &___U3ClanguageU3Ek__BackingField_2; }
	inline void set_U3ClanguageU3Ek__BackingField_2(String_t* value)
	{
		___U3ClanguageU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClanguageU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CtextU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(EntityData_t436806285, ___U3CtextU3Ek__BackingField_3)); }
	inline String_t* get_U3CtextU3Ek__BackingField_3() const { return ___U3CtextU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CtextU3Ek__BackingField_3() { return &___U3CtextU3Ek__BackingField_3; }
	inline void set_U3CtextU3Ek__BackingField_3(String_t* value)
	{
		___U3CtextU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CentitiesU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(EntityData_t436806285, ___U3CentitiesU3Ek__BackingField_4)); }
	inline EntityU5BU5D_t3969836080* get_U3CentitiesU3Ek__BackingField_4() const { return ___U3CentitiesU3Ek__BackingField_4; }
	inline EntityU5BU5D_t3969836080** get_address_of_U3CentitiesU3Ek__BackingField_4() { return &___U3CentitiesU3Ek__BackingField_4; }
	inline void set_U3CentitiesU3Ek__BackingField_4(EntityU5BU5D_t3969836080* value)
	{
		___U3CentitiesU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CentitiesU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_m_GeoLocation_5() { return static_cast<int32_t>(offsetof(EntityData_t436806285, ___m_GeoLocation_5)); }
	inline PositionOnMap_t66846110 * get_m_GeoLocation_5() const { return ___m_GeoLocation_5; }
	inline PositionOnMap_t66846110 ** get_address_of_m_GeoLocation_5() { return &___m_GeoLocation_5; }
	inline void set_m_GeoLocation_5(PositionOnMap_t66846110 * value)
	{
		___m_GeoLocation_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_GeoLocation_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTITYDATA_T436806285_H
#ifndef DOCEMOTIONS_T3627397418_H
#define DOCEMOTIONS_T3627397418_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.DocEmotions
struct  DocEmotions_t3627397418  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.DocEmotions::<anger>k__BackingField
	String_t* ___U3CangerU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.DocEmotions::<disgust>k__BackingField
	String_t* ___U3CdisgustU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.DocEmotions::<fear>k__BackingField
	String_t* ___U3CfearU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.DocEmotions::<joy>k__BackingField
	String_t* ___U3CjoyU3Ek__BackingField_3;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.DocEmotions::<sadness>k__BackingField
	String_t* ___U3CsadnessU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CangerU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DocEmotions_t3627397418, ___U3CangerU3Ek__BackingField_0)); }
	inline String_t* get_U3CangerU3Ek__BackingField_0() const { return ___U3CangerU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CangerU3Ek__BackingField_0() { return &___U3CangerU3Ek__BackingField_0; }
	inline void set_U3CangerU3Ek__BackingField_0(String_t* value)
	{
		___U3CangerU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CangerU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CdisgustU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(DocEmotions_t3627397418, ___U3CdisgustU3Ek__BackingField_1)); }
	inline String_t* get_U3CdisgustU3Ek__BackingField_1() const { return ___U3CdisgustU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CdisgustU3Ek__BackingField_1() { return &___U3CdisgustU3Ek__BackingField_1; }
	inline void set_U3CdisgustU3Ek__BackingField_1(String_t* value)
	{
		___U3CdisgustU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdisgustU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CfearU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(DocEmotions_t3627397418, ___U3CfearU3Ek__BackingField_2)); }
	inline String_t* get_U3CfearU3Ek__BackingField_2() const { return ___U3CfearU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CfearU3Ek__BackingField_2() { return &___U3CfearU3Ek__BackingField_2; }
	inline void set_U3CfearU3Ek__BackingField_2(String_t* value)
	{
		___U3CfearU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CfearU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CjoyU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(DocEmotions_t3627397418, ___U3CjoyU3Ek__BackingField_3)); }
	inline String_t* get_U3CjoyU3Ek__BackingField_3() const { return ___U3CjoyU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CjoyU3Ek__BackingField_3() { return &___U3CjoyU3Ek__BackingField_3; }
	inline void set_U3CjoyU3Ek__BackingField_3(String_t* value)
	{
		___U3CjoyU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CjoyU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CsadnessU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(DocEmotions_t3627397418, ___U3CsadnessU3Ek__BackingField_4)); }
	inline String_t* get_U3CsadnessU3Ek__BackingField_4() const { return ___U3CsadnessU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CsadnessU3Ek__BackingField_4() { return &___U3CsadnessU3Ek__BackingField_4; }
	inline void set_U3CsadnessU3Ek__BackingField_4(String_t* value)
	{
		___U3CsadnessU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsadnessU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOCEMOTIONS_T3627397418_H
#ifndef POSITIONONMAP_T66846110_H
#define POSITIONONMAP_T66846110_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.PositionOnMap
struct  PositionOnMap_t66846110  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.PositionOnMap::PositionName
	String_t* ___PositionName_0;
	// System.Double IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.PositionOnMap::Latitude
	double ___Latitude_1;
	// System.Double IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.PositionOnMap::Longitude
	double ___Longitude_2;

public:
	inline static int32_t get_offset_of_PositionName_0() { return static_cast<int32_t>(offsetof(PositionOnMap_t66846110, ___PositionName_0)); }
	inline String_t* get_PositionName_0() const { return ___PositionName_0; }
	inline String_t** get_address_of_PositionName_0() { return &___PositionName_0; }
	inline void set_PositionName_0(String_t* value)
	{
		___PositionName_0 = value;
		Il2CppCodeGenWriteBarrier((&___PositionName_0), value);
	}

	inline static int32_t get_offset_of_Latitude_1() { return static_cast<int32_t>(offsetof(PositionOnMap_t66846110, ___Latitude_1)); }
	inline double get_Latitude_1() const { return ___Latitude_1; }
	inline double* get_address_of_Latitude_1() { return &___Latitude_1; }
	inline void set_Latitude_1(double value)
	{
		___Latitude_1 = value;
	}

	inline static int32_t get_offset_of_Longitude_2() { return static_cast<int32_t>(offsetof(PositionOnMap_t66846110, ___Longitude_2)); }
	inline double get_Longitude_2() const { return ___Longitude_2; }
	inline double* get_address_of_Longitude_2() { return &___Longitude_2; }
	inline void set_Longitude_2(double value)
	{
		___Longitude_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSITIONONMAP_T66846110_H
#ifndef FEEDDATA_T845804320_H
#define FEEDDATA_T845804320_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.FeedData
struct  FeedData_t845804320  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.FeedData::<status>k__BackingField
	String_t* ___U3CstatusU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.FeedData::<url>k__BackingField
	String_t* ___U3CurlU3Ek__BackingField_1;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Feed[] IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.FeedData::<feeds>k__BackingField
	FeedU5BU5D_t332386033* ___U3CfeedsU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CstatusU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(FeedData_t845804320, ___U3CstatusU3Ek__BackingField_0)); }
	inline String_t* get_U3CstatusU3Ek__BackingField_0() const { return ___U3CstatusU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CstatusU3Ek__BackingField_0() { return &___U3CstatusU3Ek__BackingField_0; }
	inline void set_U3CstatusU3Ek__BackingField_0(String_t* value)
	{
		___U3CstatusU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstatusU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CurlU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(FeedData_t845804320, ___U3CurlU3Ek__BackingField_1)); }
	inline String_t* get_U3CurlU3Ek__BackingField_1() const { return ___U3CurlU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CurlU3Ek__BackingField_1() { return &___U3CurlU3Ek__BackingField_1; }
	inline void set_U3CurlU3Ek__BackingField_1(String_t* value)
	{
		___U3CurlU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CurlU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CfeedsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(FeedData_t845804320, ___U3CfeedsU3Ek__BackingField_2)); }
	inline FeedU5BU5D_t332386033* get_U3CfeedsU3Ek__BackingField_2() const { return ___U3CfeedsU3Ek__BackingField_2; }
	inline FeedU5BU5D_t332386033** get_address_of_U3CfeedsU3Ek__BackingField_2() { return &___U3CfeedsU3Ek__BackingField_2; }
	inline void set_U3CfeedsU3Ek__BackingField_2(FeedU5BU5D_t332386033* value)
	{
		___U3CfeedsU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CfeedsU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FEEDDATA_T845804320_H
#ifndef KEYWORD_T3384522415_H
#define KEYWORD_T3384522415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Keyword
struct  Keyword_t3384522415  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Keyword::<text>k__BackingField
	String_t* ___U3CtextU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Keyword::<relevance>k__BackingField
	String_t* ___U3CrelevanceU3Ek__BackingField_1;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.KnowledgeGraph IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Keyword::<knowledgeGraph>k__BackingField
	KnowledgeGraph_t3341597436 * ___U3CknowledgeGraphU3Ek__BackingField_2;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.DocSentiment IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Keyword::<sentiment>k__BackingField
	DocSentiment_t2354958393 * ___U3CsentimentU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CtextU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Keyword_t3384522415, ___U3CtextU3Ek__BackingField_0)); }
	inline String_t* get_U3CtextU3Ek__BackingField_0() const { return ___U3CtextU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CtextU3Ek__BackingField_0() { return &___U3CtextU3Ek__BackingField_0; }
	inline void set_U3CtextU3Ek__BackingField_0(String_t* value)
	{
		___U3CtextU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CrelevanceU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Keyword_t3384522415, ___U3CrelevanceU3Ek__BackingField_1)); }
	inline String_t* get_U3CrelevanceU3Ek__BackingField_1() const { return ___U3CrelevanceU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CrelevanceU3Ek__BackingField_1() { return &___U3CrelevanceU3Ek__BackingField_1; }
	inline void set_U3CrelevanceU3Ek__BackingField_1(String_t* value)
	{
		___U3CrelevanceU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrelevanceU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CknowledgeGraphU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Keyword_t3384522415, ___U3CknowledgeGraphU3Ek__BackingField_2)); }
	inline KnowledgeGraph_t3341597436 * get_U3CknowledgeGraphU3Ek__BackingField_2() const { return ___U3CknowledgeGraphU3Ek__BackingField_2; }
	inline KnowledgeGraph_t3341597436 ** get_address_of_U3CknowledgeGraphU3Ek__BackingField_2() { return &___U3CknowledgeGraphU3Ek__BackingField_2; }
	inline void set_U3CknowledgeGraphU3Ek__BackingField_2(KnowledgeGraph_t3341597436 * value)
	{
		___U3CknowledgeGraphU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CknowledgeGraphU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CsentimentU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Keyword_t3384522415, ___U3CsentimentU3Ek__BackingField_3)); }
	inline DocSentiment_t2354958393 * get_U3CsentimentU3Ek__BackingField_3() const { return ___U3CsentimentU3Ek__BackingField_3; }
	inline DocSentiment_t2354958393 ** get_address_of_U3CsentimentU3Ek__BackingField_3() { return &___U3CsentimentU3Ek__BackingField_3; }
	inline void set_U3CsentimentU3Ek__BackingField_3(DocSentiment_t2354958393 * value)
	{
		___U3CsentimentU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsentimentU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYWORD_T3384522415_H
#ifndef LANGUAGEDATA_T2624020128_H
#define LANGUAGEDATA_T2624020128_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.LanguageData
struct  LanguageData_t2624020128  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.LanguageData::<status>k__BackingField
	String_t* ___U3CstatusU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.LanguageData::<url>k__BackingField
	String_t* ___U3CurlU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.LanguageData::<language>k__BackingField
	String_t* ___U3ClanguageU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.LanguageData::<iso_639_1>k__BackingField
	String_t* ___U3Ciso_639_1U3Ek__BackingField_3;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.LanguageData::<iso_639_2>k__BackingField
	String_t* ___U3Ciso_639_2U3Ek__BackingField_4;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.LanguageData::<iso_639_3>k__BackingField
	String_t* ___U3Ciso_639_3U3Ek__BackingField_5;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.LanguageData::<ethnologue>k__BackingField
	String_t* ___U3CethnologueU3Ek__BackingField_6;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.LanguageData::<native_speakers>k__BackingField
	String_t* ___U3Cnative_speakersU3Ek__BackingField_7;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.LanguageData::<wikipedia>k__BackingField
	String_t* ___U3CwikipediaU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of_U3CstatusU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(LanguageData_t2624020128, ___U3CstatusU3Ek__BackingField_0)); }
	inline String_t* get_U3CstatusU3Ek__BackingField_0() const { return ___U3CstatusU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CstatusU3Ek__BackingField_0() { return &___U3CstatusU3Ek__BackingField_0; }
	inline void set_U3CstatusU3Ek__BackingField_0(String_t* value)
	{
		___U3CstatusU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstatusU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CurlU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(LanguageData_t2624020128, ___U3CurlU3Ek__BackingField_1)); }
	inline String_t* get_U3CurlU3Ek__BackingField_1() const { return ___U3CurlU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CurlU3Ek__BackingField_1() { return &___U3CurlU3Ek__BackingField_1; }
	inline void set_U3CurlU3Ek__BackingField_1(String_t* value)
	{
		___U3CurlU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CurlU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3ClanguageU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(LanguageData_t2624020128, ___U3ClanguageU3Ek__BackingField_2)); }
	inline String_t* get_U3ClanguageU3Ek__BackingField_2() const { return ___U3ClanguageU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3ClanguageU3Ek__BackingField_2() { return &___U3ClanguageU3Ek__BackingField_2; }
	inline void set_U3ClanguageU3Ek__BackingField_2(String_t* value)
	{
		___U3ClanguageU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClanguageU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3Ciso_639_1U3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(LanguageData_t2624020128, ___U3Ciso_639_1U3Ek__BackingField_3)); }
	inline String_t* get_U3Ciso_639_1U3Ek__BackingField_3() const { return ___U3Ciso_639_1U3Ek__BackingField_3; }
	inline String_t** get_address_of_U3Ciso_639_1U3Ek__BackingField_3() { return &___U3Ciso_639_1U3Ek__BackingField_3; }
	inline void set_U3Ciso_639_1U3Ek__BackingField_3(String_t* value)
	{
		___U3Ciso_639_1U3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3Ciso_639_1U3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3Ciso_639_2U3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(LanguageData_t2624020128, ___U3Ciso_639_2U3Ek__BackingField_4)); }
	inline String_t* get_U3Ciso_639_2U3Ek__BackingField_4() const { return ___U3Ciso_639_2U3Ek__BackingField_4; }
	inline String_t** get_address_of_U3Ciso_639_2U3Ek__BackingField_4() { return &___U3Ciso_639_2U3Ek__BackingField_4; }
	inline void set_U3Ciso_639_2U3Ek__BackingField_4(String_t* value)
	{
		___U3Ciso_639_2U3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3Ciso_639_2U3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3Ciso_639_3U3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(LanguageData_t2624020128, ___U3Ciso_639_3U3Ek__BackingField_5)); }
	inline String_t* get_U3Ciso_639_3U3Ek__BackingField_5() const { return ___U3Ciso_639_3U3Ek__BackingField_5; }
	inline String_t** get_address_of_U3Ciso_639_3U3Ek__BackingField_5() { return &___U3Ciso_639_3U3Ek__BackingField_5; }
	inline void set_U3Ciso_639_3U3Ek__BackingField_5(String_t* value)
	{
		___U3Ciso_639_3U3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3Ciso_639_3U3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CethnologueU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(LanguageData_t2624020128, ___U3CethnologueU3Ek__BackingField_6)); }
	inline String_t* get_U3CethnologueU3Ek__BackingField_6() const { return ___U3CethnologueU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CethnologueU3Ek__BackingField_6() { return &___U3CethnologueU3Ek__BackingField_6; }
	inline void set_U3CethnologueU3Ek__BackingField_6(String_t* value)
	{
		___U3CethnologueU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CethnologueU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3Cnative_speakersU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(LanguageData_t2624020128, ___U3Cnative_speakersU3Ek__BackingField_7)); }
	inline String_t* get_U3Cnative_speakersU3Ek__BackingField_7() const { return ___U3Cnative_speakersU3Ek__BackingField_7; }
	inline String_t** get_address_of_U3Cnative_speakersU3Ek__BackingField_7() { return &___U3Cnative_speakersU3Ek__BackingField_7; }
	inline void set_U3Cnative_speakersU3Ek__BackingField_7(String_t* value)
	{
		___U3Cnative_speakersU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cnative_speakersU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CwikipediaU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(LanguageData_t2624020128, ___U3CwikipediaU3Ek__BackingField_8)); }
	inline String_t* get_U3CwikipediaU3Ek__BackingField_8() const { return ___U3CwikipediaU3Ek__BackingField_8; }
	inline String_t** get_address_of_U3CwikipediaU3Ek__BackingField_8() { return &___U3CwikipediaU3Ek__BackingField_8; }
	inline void set_U3CwikipediaU3Ek__BackingField_8(String_t* value)
	{
		___U3CwikipediaU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwikipediaU3Ek__BackingField_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LANGUAGEDATA_T2624020128_H
#ifndef EMOTIONDATA_T625023405_H
#define EMOTIONDATA_T625023405_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.EmotionData
struct  EmotionData_t625023405  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.EmotionData::<status>k__BackingField
	String_t* ___U3CstatusU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.EmotionData::<url>k__BackingField
	String_t* ___U3CurlU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.EmotionData::<language>k__BackingField
	String_t* ___U3ClanguageU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.EmotionData::<text>k__BackingField
	String_t* ___U3CtextU3Ek__BackingField_3;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.DocEmotions IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.EmotionData::<docEmotions>k__BackingField
	DocEmotions_t3627397418 * ___U3CdocEmotionsU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CstatusU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(EmotionData_t625023405, ___U3CstatusU3Ek__BackingField_0)); }
	inline String_t* get_U3CstatusU3Ek__BackingField_0() const { return ___U3CstatusU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CstatusU3Ek__BackingField_0() { return &___U3CstatusU3Ek__BackingField_0; }
	inline void set_U3CstatusU3Ek__BackingField_0(String_t* value)
	{
		___U3CstatusU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstatusU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CurlU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(EmotionData_t625023405, ___U3CurlU3Ek__BackingField_1)); }
	inline String_t* get_U3CurlU3Ek__BackingField_1() const { return ___U3CurlU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CurlU3Ek__BackingField_1() { return &___U3CurlU3Ek__BackingField_1; }
	inline void set_U3CurlU3Ek__BackingField_1(String_t* value)
	{
		___U3CurlU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CurlU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3ClanguageU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(EmotionData_t625023405, ___U3ClanguageU3Ek__BackingField_2)); }
	inline String_t* get_U3ClanguageU3Ek__BackingField_2() const { return ___U3ClanguageU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3ClanguageU3Ek__BackingField_2() { return &___U3ClanguageU3Ek__BackingField_2; }
	inline void set_U3ClanguageU3Ek__BackingField_2(String_t* value)
	{
		___U3ClanguageU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClanguageU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CtextU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(EmotionData_t625023405, ___U3CtextU3Ek__BackingField_3)); }
	inline String_t* get_U3CtextU3Ek__BackingField_3() const { return ___U3CtextU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CtextU3Ek__BackingField_3() { return &___U3CtextU3Ek__BackingField_3; }
	inline void set_U3CtextU3Ek__BackingField_3(String_t* value)
	{
		___U3CtextU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CdocEmotionsU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(EmotionData_t625023405, ___U3CdocEmotionsU3Ek__BackingField_4)); }
	inline DocEmotions_t3627397418 * get_U3CdocEmotionsU3Ek__BackingField_4() const { return ___U3CdocEmotionsU3Ek__BackingField_4; }
	inline DocEmotions_t3627397418 ** get_address_of_U3CdocEmotionsU3Ek__BackingField_4() { return &___U3CdocEmotionsU3Ek__BackingField_4; }
	inline void set_U3CdocEmotionsU3Ek__BackingField_4(DocEmotions_t3627397418 * value)
	{
		___U3CdocEmotionsU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdocEmotionsU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMOTIONDATA_T625023405_H
#ifndef AUTHORS_T34055864_H
#define AUTHORS_T34055864_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Authors
struct  Authors_t34055864  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Authors::<confident>k__BackingField
	String_t* ___U3CconfidentU3Ek__BackingField_0;
	// System.String[] IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Authors::<names>k__BackingField
	StringU5BU5D_t1642385972* ___U3CnamesU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CconfidentU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Authors_t34055864, ___U3CconfidentU3Ek__BackingField_0)); }
	inline String_t* get_U3CconfidentU3Ek__BackingField_0() const { return ___U3CconfidentU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CconfidentU3Ek__BackingField_0() { return &___U3CconfidentU3Ek__BackingField_0; }
	inline void set_U3CconfidentU3Ek__BackingField_0(String_t* value)
	{
		___U3CconfidentU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CconfidentU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CnamesU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Authors_t34055864, ___U3CnamesU3Ek__BackingField_1)); }
	inline StringU5BU5D_t1642385972* get_U3CnamesU3Ek__BackingField_1() const { return ___U3CnamesU3Ek__BackingField_1; }
	inline StringU5BU5D_t1642385972** get_address_of_U3CnamesU3Ek__BackingField_1() { return &___U3CnamesU3Ek__BackingField_1; }
	inline void set_U3CnamesU3Ek__BackingField_1(StringU5BU5D_t1642385972* value)
	{
		___U3CnamesU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnamesU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTHORS_T34055864_H
#ifndef AUTHORSDATA_T3643075816_H
#define AUTHORSDATA_T3643075816_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AuthorsData
struct  AuthorsData_t3643075816  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AuthorsData::<status>k__BackingField
	String_t* ___U3CstatusU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AuthorsData::<url>k__BackingField
	String_t* ___U3CurlU3Ek__BackingField_1;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Authors IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AuthorsData::<authors>k__BackingField
	Authors_t34055864 * ___U3CauthorsU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CstatusU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AuthorsData_t3643075816, ___U3CstatusU3Ek__BackingField_0)); }
	inline String_t* get_U3CstatusU3Ek__BackingField_0() const { return ___U3CstatusU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CstatusU3Ek__BackingField_0() { return &___U3CstatusU3Ek__BackingField_0; }
	inline void set_U3CstatusU3Ek__BackingField_0(String_t* value)
	{
		___U3CstatusU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstatusU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CurlU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AuthorsData_t3643075816, ___U3CurlU3Ek__BackingField_1)); }
	inline String_t* get_U3CurlU3Ek__BackingField_1() const { return ___U3CurlU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CurlU3Ek__BackingField_1() { return &___U3CurlU3Ek__BackingField_1; }
	inline void set_U3CurlU3Ek__BackingField_1(String_t* value)
	{
		___U3CurlU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CurlU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CauthorsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AuthorsData_t3643075816, ___U3CauthorsU3Ek__BackingField_2)); }
	inline Authors_t34055864 * get_U3CauthorsU3Ek__BackingField_2() const { return ___U3CauthorsU3Ek__BackingField_2; }
	inline Authors_t34055864 ** get_address_of_U3CauthorsU3Ek__BackingField_2() { return &___U3CauthorsU3Ek__BackingField_2; }
	inline void set_U3CauthorsU3Ek__BackingField_2(Authors_t34055864 * value)
	{
		___U3CauthorsU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CauthorsU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTHORSDATA_T3643075816_H
#ifndef FEED_T2248050640_H
#define FEED_T2248050640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Feed
struct  Feed_t2248050640  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Feed::<feed>k__BackingField
	String_t* ___U3CfeedU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CfeedU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Feed_t2248050640, ___U3CfeedU3Ek__BackingField_0)); }
	inline String_t* get_U3CfeedU3Ek__BackingField_0() const { return ___U3CfeedU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CfeedU3Ek__BackingField_0() { return &___U3CfeedU3Ek__BackingField_0; }
	inline void set_U3CfeedU3Ek__BackingField_0(String_t* value)
	{
		___U3CfeedU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CfeedU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FEED_T2248050640_H
#ifndef CONCEPTSDATA_T441878239_H
#define CONCEPTSDATA_T441878239_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.ConceptsData
struct  ConceptsData_t441878239  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.ConceptsData::<status>k__BackingField
	String_t* ___U3CstatusU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.ConceptsData::<url>k__BackingField
	String_t* ___U3CurlU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.ConceptsData::<language>k__BackingField
	String_t* ___U3ClanguageU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.ConceptsData::<text>k__BackingField
	String_t* ___U3CtextU3Ek__BackingField_3;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Concept[] IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.ConceptsData::<concepts>k__BackingField
	ConceptU5BU5D_t3502654871* ___U3CconceptsU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CstatusU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ConceptsData_t441878239, ___U3CstatusU3Ek__BackingField_0)); }
	inline String_t* get_U3CstatusU3Ek__BackingField_0() const { return ___U3CstatusU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CstatusU3Ek__BackingField_0() { return &___U3CstatusU3Ek__BackingField_0; }
	inline void set_U3CstatusU3Ek__BackingField_0(String_t* value)
	{
		___U3CstatusU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstatusU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CurlU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ConceptsData_t441878239, ___U3CurlU3Ek__BackingField_1)); }
	inline String_t* get_U3CurlU3Ek__BackingField_1() const { return ___U3CurlU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CurlU3Ek__BackingField_1() { return &___U3CurlU3Ek__BackingField_1; }
	inline void set_U3CurlU3Ek__BackingField_1(String_t* value)
	{
		___U3CurlU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CurlU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3ClanguageU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ConceptsData_t441878239, ___U3ClanguageU3Ek__BackingField_2)); }
	inline String_t* get_U3ClanguageU3Ek__BackingField_2() const { return ___U3ClanguageU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3ClanguageU3Ek__BackingField_2() { return &___U3ClanguageU3Ek__BackingField_2; }
	inline void set_U3ClanguageU3Ek__BackingField_2(String_t* value)
	{
		___U3ClanguageU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClanguageU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CtextU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ConceptsData_t441878239, ___U3CtextU3Ek__BackingField_3)); }
	inline String_t* get_U3CtextU3Ek__BackingField_3() const { return ___U3CtextU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CtextU3Ek__BackingField_3() { return &___U3CtextU3Ek__BackingField_3; }
	inline void set_U3CtextU3Ek__BackingField_3(String_t* value)
	{
		___U3CtextU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CconceptsU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(ConceptsData_t441878239, ___U3CconceptsU3Ek__BackingField_4)); }
	inline ConceptU5BU5D_t3502654871* get_U3CconceptsU3Ek__BackingField_4() const { return ___U3CconceptsU3Ek__BackingField_4; }
	inline ConceptU5BU5D_t3502654871** get_address_of_U3CconceptsU3Ek__BackingField_4() { return &___U3CconceptsU3Ek__BackingField_4; }
	inline void set_U3CconceptsU3Ek__BackingField_4(ConceptU5BU5D_t3502654871* value)
	{
		___U3CconceptsU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CconceptsU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONCEPTSDATA_T441878239_H
#ifndef MICROFORMAT_T171559559_H
#define MICROFORMAT_T171559559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Microformat
struct  Microformat_t171559559  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Microformat::<field>k__BackingField
	String_t* ___U3CfieldU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Microformat::<data>k__BackingField
	String_t* ___U3CdataU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CfieldU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Microformat_t171559559, ___U3CfieldU3Ek__BackingField_0)); }
	inline String_t* get_U3CfieldU3Ek__BackingField_0() const { return ___U3CfieldU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CfieldU3Ek__BackingField_0() { return &___U3CfieldU3Ek__BackingField_0; }
	inline void set_U3CfieldU3Ek__BackingField_0(String_t* value)
	{
		___U3CfieldU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CfieldU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CdataU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Microformat_t171559559, ___U3CdataU3Ek__BackingField_1)); }
	inline String_t* get_U3CdataU3Ek__BackingField_1() const { return ___U3CdataU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CdataU3Ek__BackingField_1() { return &___U3CdataU3Ek__BackingField_1; }
	inline void set_U3CdataU3Ek__BackingField_1(String_t* value)
	{
		___U3CdataU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdataU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MICROFORMAT_T171559559_H
#ifndef DATEDATA_T3593288038_H
#define DATEDATA_T3593288038_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.DateData
struct  DateData_t3593288038  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.DateData::<status>k__BackingField
	String_t* ___U3CstatusU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.DateData::<language>k__BackingField
	String_t* ___U3ClanguageU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.DateData::<url>k__BackingField
	String_t* ___U3CurlU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.DateData::<text>k__BackingField
	String_t* ___U3CtextU3Ek__BackingField_3;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Date[] IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.DateData::<dates>k__BackingField
	DateU5BU5D_t3621454771* ___U3CdatesU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CstatusU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DateData_t3593288038, ___U3CstatusU3Ek__BackingField_0)); }
	inline String_t* get_U3CstatusU3Ek__BackingField_0() const { return ___U3CstatusU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CstatusU3Ek__BackingField_0() { return &___U3CstatusU3Ek__BackingField_0; }
	inline void set_U3CstatusU3Ek__BackingField_0(String_t* value)
	{
		___U3CstatusU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstatusU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3ClanguageU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(DateData_t3593288038, ___U3ClanguageU3Ek__BackingField_1)); }
	inline String_t* get_U3ClanguageU3Ek__BackingField_1() const { return ___U3ClanguageU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3ClanguageU3Ek__BackingField_1() { return &___U3ClanguageU3Ek__BackingField_1; }
	inline void set_U3ClanguageU3Ek__BackingField_1(String_t* value)
	{
		___U3ClanguageU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClanguageU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CurlU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(DateData_t3593288038, ___U3CurlU3Ek__BackingField_2)); }
	inline String_t* get_U3CurlU3Ek__BackingField_2() const { return ___U3CurlU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CurlU3Ek__BackingField_2() { return &___U3CurlU3Ek__BackingField_2; }
	inline void set_U3CurlU3Ek__BackingField_2(String_t* value)
	{
		___U3CurlU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CurlU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CtextU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(DateData_t3593288038, ___U3CtextU3Ek__BackingField_3)); }
	inline String_t* get_U3CtextU3Ek__BackingField_3() const { return ___U3CtextU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CtextU3Ek__BackingField_3() { return &___U3CtextU3Ek__BackingField_3; }
	inline void set_U3CtextU3Ek__BackingField_3(String_t* value)
	{
		___U3CtextU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CdatesU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(DateData_t3593288038, ___U3CdatesU3Ek__BackingField_4)); }
	inline DateU5BU5D_t3621454771* get_U3CdatesU3Ek__BackingField_4() const { return ___U3CdatesU3Ek__BackingField_4; }
	inline DateU5BU5D_t3621454771** get_address_of_U3CdatesU3Ek__BackingField_4() { return &___U3CdatesU3Ek__BackingField_4; }
	inline void set_U3CdatesU3Ek__BackingField_4(DateU5BU5D_t3621454771* value)
	{
		___U3CdatesU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdatesU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATEDATA_T3593288038_H
#ifndef CONCEPT_T2322716066_H
#define CONCEPT_T2322716066_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Concept
struct  Concept_t2322716066  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Concept::<text>k__BackingField
	String_t* ___U3CtextU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Concept::<relevance>k__BackingField
	String_t* ___U3CrelevanceU3Ek__BackingField_1;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.KnowledgeGraph IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Concept::<knowledgeGraph>k__BackingField
	KnowledgeGraph_t3341597436 * ___U3CknowledgeGraphU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Concept::<website>k__BackingField
	String_t* ___U3CwebsiteU3Ek__BackingField_3;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Concept::<geo>k__BackingField
	String_t* ___U3CgeoU3Ek__BackingField_4;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Concept::<dbpedia>k__BackingField
	String_t* ___U3CdbpediaU3Ek__BackingField_5;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Concept::<freebase>k__BackingField
	String_t* ___U3CfreebaseU3Ek__BackingField_6;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Concept::<yago>k__BackingField
	String_t* ___U3CyagoU3Ek__BackingField_7;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Concept::<opencyc>k__BackingField
	String_t* ___U3CopencycU3Ek__BackingField_8;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Concept::<ciaFactbook>k__BackingField
	String_t* ___U3CciaFactbookU3Ek__BackingField_9;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Concept::<census>k__BackingField
	String_t* ___U3CcensusU3Ek__BackingField_10;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Concept::<geonames>k__BackingField
	String_t* ___U3CgeonamesU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Concept::<musicBrainz>k__BackingField
	String_t* ___U3CmusicBrainzU3Ek__BackingField_12;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Concept::<crunchbase>k__BackingField
	String_t* ___U3CcrunchbaseU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_U3CtextU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Concept_t2322716066, ___U3CtextU3Ek__BackingField_0)); }
	inline String_t* get_U3CtextU3Ek__BackingField_0() const { return ___U3CtextU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CtextU3Ek__BackingField_0() { return &___U3CtextU3Ek__BackingField_0; }
	inline void set_U3CtextU3Ek__BackingField_0(String_t* value)
	{
		___U3CtextU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CrelevanceU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Concept_t2322716066, ___U3CrelevanceU3Ek__BackingField_1)); }
	inline String_t* get_U3CrelevanceU3Ek__BackingField_1() const { return ___U3CrelevanceU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CrelevanceU3Ek__BackingField_1() { return &___U3CrelevanceU3Ek__BackingField_1; }
	inline void set_U3CrelevanceU3Ek__BackingField_1(String_t* value)
	{
		___U3CrelevanceU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrelevanceU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CknowledgeGraphU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Concept_t2322716066, ___U3CknowledgeGraphU3Ek__BackingField_2)); }
	inline KnowledgeGraph_t3341597436 * get_U3CknowledgeGraphU3Ek__BackingField_2() const { return ___U3CknowledgeGraphU3Ek__BackingField_2; }
	inline KnowledgeGraph_t3341597436 ** get_address_of_U3CknowledgeGraphU3Ek__BackingField_2() { return &___U3CknowledgeGraphU3Ek__BackingField_2; }
	inline void set_U3CknowledgeGraphU3Ek__BackingField_2(KnowledgeGraph_t3341597436 * value)
	{
		___U3CknowledgeGraphU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CknowledgeGraphU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CwebsiteU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Concept_t2322716066, ___U3CwebsiteU3Ek__BackingField_3)); }
	inline String_t* get_U3CwebsiteU3Ek__BackingField_3() const { return ___U3CwebsiteU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CwebsiteU3Ek__BackingField_3() { return &___U3CwebsiteU3Ek__BackingField_3; }
	inline void set_U3CwebsiteU3Ek__BackingField_3(String_t* value)
	{
		___U3CwebsiteU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwebsiteU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CgeoU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Concept_t2322716066, ___U3CgeoU3Ek__BackingField_4)); }
	inline String_t* get_U3CgeoU3Ek__BackingField_4() const { return ___U3CgeoU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CgeoU3Ek__BackingField_4() { return &___U3CgeoU3Ek__BackingField_4; }
	inline void set_U3CgeoU3Ek__BackingField_4(String_t* value)
	{
		___U3CgeoU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CgeoU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CdbpediaU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Concept_t2322716066, ___U3CdbpediaU3Ek__BackingField_5)); }
	inline String_t* get_U3CdbpediaU3Ek__BackingField_5() const { return ___U3CdbpediaU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CdbpediaU3Ek__BackingField_5() { return &___U3CdbpediaU3Ek__BackingField_5; }
	inline void set_U3CdbpediaU3Ek__BackingField_5(String_t* value)
	{
		___U3CdbpediaU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdbpediaU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CfreebaseU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Concept_t2322716066, ___U3CfreebaseU3Ek__BackingField_6)); }
	inline String_t* get_U3CfreebaseU3Ek__BackingField_6() const { return ___U3CfreebaseU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CfreebaseU3Ek__BackingField_6() { return &___U3CfreebaseU3Ek__BackingField_6; }
	inline void set_U3CfreebaseU3Ek__BackingField_6(String_t* value)
	{
		___U3CfreebaseU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CfreebaseU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CyagoU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(Concept_t2322716066, ___U3CyagoU3Ek__BackingField_7)); }
	inline String_t* get_U3CyagoU3Ek__BackingField_7() const { return ___U3CyagoU3Ek__BackingField_7; }
	inline String_t** get_address_of_U3CyagoU3Ek__BackingField_7() { return &___U3CyagoU3Ek__BackingField_7; }
	inline void set_U3CyagoU3Ek__BackingField_7(String_t* value)
	{
		___U3CyagoU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CyagoU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CopencycU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(Concept_t2322716066, ___U3CopencycU3Ek__BackingField_8)); }
	inline String_t* get_U3CopencycU3Ek__BackingField_8() const { return ___U3CopencycU3Ek__BackingField_8; }
	inline String_t** get_address_of_U3CopencycU3Ek__BackingField_8() { return &___U3CopencycU3Ek__BackingField_8; }
	inline void set_U3CopencycU3Ek__BackingField_8(String_t* value)
	{
		___U3CopencycU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CopencycU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CciaFactbookU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(Concept_t2322716066, ___U3CciaFactbookU3Ek__BackingField_9)); }
	inline String_t* get_U3CciaFactbookU3Ek__BackingField_9() const { return ___U3CciaFactbookU3Ek__BackingField_9; }
	inline String_t** get_address_of_U3CciaFactbookU3Ek__BackingField_9() { return &___U3CciaFactbookU3Ek__BackingField_9; }
	inline void set_U3CciaFactbookU3Ek__BackingField_9(String_t* value)
	{
		___U3CciaFactbookU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CciaFactbookU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CcensusU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(Concept_t2322716066, ___U3CcensusU3Ek__BackingField_10)); }
	inline String_t* get_U3CcensusU3Ek__BackingField_10() const { return ___U3CcensusU3Ek__BackingField_10; }
	inline String_t** get_address_of_U3CcensusU3Ek__BackingField_10() { return &___U3CcensusU3Ek__BackingField_10; }
	inline void set_U3CcensusU3Ek__BackingField_10(String_t* value)
	{
		___U3CcensusU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcensusU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_U3CgeonamesU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(Concept_t2322716066, ___U3CgeonamesU3Ek__BackingField_11)); }
	inline String_t* get_U3CgeonamesU3Ek__BackingField_11() const { return ___U3CgeonamesU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CgeonamesU3Ek__BackingField_11() { return &___U3CgeonamesU3Ek__BackingField_11; }
	inline void set_U3CgeonamesU3Ek__BackingField_11(String_t* value)
	{
		___U3CgeonamesU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CgeonamesU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CmusicBrainzU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(Concept_t2322716066, ___U3CmusicBrainzU3Ek__BackingField_12)); }
	inline String_t* get_U3CmusicBrainzU3Ek__BackingField_12() const { return ___U3CmusicBrainzU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CmusicBrainzU3Ek__BackingField_12() { return &___U3CmusicBrainzU3Ek__BackingField_12; }
	inline void set_U3CmusicBrainzU3Ek__BackingField_12(String_t* value)
	{
		___U3CmusicBrainzU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmusicBrainzU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CcrunchbaseU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(Concept_t2322716066, ___U3CcrunchbaseU3Ek__BackingField_13)); }
	inline String_t* get_U3CcrunchbaseU3Ek__BackingField_13() const { return ___U3CcrunchbaseU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CcrunchbaseU3Ek__BackingField_13() { return &___U3CcrunchbaseU3Ek__BackingField_13; }
	inline void set_U3CcrunchbaseU3Ek__BackingField_13(String_t* value)
	{
		___U3CcrunchbaseU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcrunchbaseU3Ek__BackingField_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONCEPT_T2322716066_H
#ifndef U3CACTIVATEMICAFTERTIMEU3EC__ITERATOR0_T647232991_H
#define U3CACTIVATEMICAFTERTIMEU3EC__ITERATOR0_T647232991_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Debug.DebugConsole/<ActivateMicAfterTime>c__Iterator0
struct  U3CActivateMicAfterTimeU3Ec__Iterator0_t647232991  : public RuntimeObject
{
public:
	// System.Single IBM.Watson.DeveloperCloud.Debug.DebugConsole/<ActivateMicAfterTime>c__Iterator0::time
	float ___time_0;
	// IBM.Watson.DeveloperCloud.Debug.DebugConsole IBM.Watson.DeveloperCloud.Debug.DebugConsole/<ActivateMicAfterTime>c__Iterator0::$this
	DebugConsole_t4095885638 * ___U24this_1;
	// System.Object IBM.Watson.DeveloperCloud.Debug.DebugConsole/<ActivateMicAfterTime>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean IBM.Watson.DeveloperCloud.Debug.DebugConsole/<ActivateMicAfterTime>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 IBM.Watson.DeveloperCloud.Debug.DebugConsole/<ActivateMicAfterTime>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_time_0() { return static_cast<int32_t>(offsetof(U3CActivateMicAfterTimeU3Ec__Iterator0_t647232991, ___time_0)); }
	inline float get_time_0() const { return ___time_0; }
	inline float* get_address_of_time_0() { return &___time_0; }
	inline void set_time_0(float value)
	{
		___time_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CActivateMicAfterTimeU3Ec__Iterator0_t647232991, ___U24this_1)); }
	inline DebugConsole_t4095885638 * get_U24this_1() const { return ___U24this_1; }
	inline DebugConsole_t4095885638 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(DebugConsole_t4095885638 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CActivateMicAfterTimeU3Ec__Iterator0_t647232991, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CActivateMicAfterTimeU3Ec__Iterator0_t647232991, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CActivateMicAfterTimeU3Ec__Iterator0_t647232991, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CACTIVATEMICAFTERTIMEU3EC__ITERATOR0_T647232991_H
#ifndef REQUEST_T466816980_H
#define REQUEST_T466816980_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request
struct  Request_t466816980  : public RuntimeObject
{
public:
	// System.Single IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Timeout>k__BackingField
	float ___U3CTimeoutU3Ek__BackingField_0;
	// System.Boolean IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Cancel>k__BackingField
	bool ___U3CCancelU3Ek__BackingField_1;
	// System.Boolean IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Delete>k__BackingField
	bool ___U3CDeleteU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Function>k__BackingField
	String_t* ___U3CFunctionU3Ek__BackingField_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Parameters>k__BackingField
	Dictionary_2_t309261261 * ___U3CParametersU3Ek__BackingField_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Headers>k__BackingField
	Dictionary_2_t3943999495 * ___U3CHeadersU3Ek__BackingField_5;
	// System.Byte[] IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Send>k__BackingField
	ByteU5BU5D_t3397334013* ___U3CSendU3Ek__BackingField_6;
	// System.Collections.Generic.Dictionary`2<System.String,IBM.Watson.DeveloperCloud.Connection.RESTConnector/Form> IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Forms>k__BackingField
	Dictionary_2_t2694055125 * ___U3CFormsU3Ek__BackingField_7;
	// IBM.Watson.DeveloperCloud.Connection.RESTConnector/ResponseEvent IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<OnResponse>k__BackingField
	ResponseEvent_t1616568356 * ___U3COnResponseU3Ek__BackingField_8;
	// IBM.Watson.DeveloperCloud.Connection.RESTConnector/ProgressEvent IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<OnDownloadProgress>k__BackingField
	ProgressEvent_t4185145044 * ___U3COnDownloadProgressU3Ek__BackingField_9;
	// IBM.Watson.DeveloperCloud.Connection.RESTConnector/ProgressEvent IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<OnUploadProgress>k__BackingField
	ProgressEvent_t4185145044 * ___U3COnUploadProgressU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of_U3CTimeoutU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CTimeoutU3Ek__BackingField_0)); }
	inline float get_U3CTimeoutU3Ek__BackingField_0() const { return ___U3CTimeoutU3Ek__BackingField_0; }
	inline float* get_address_of_U3CTimeoutU3Ek__BackingField_0() { return &___U3CTimeoutU3Ek__BackingField_0; }
	inline void set_U3CTimeoutU3Ek__BackingField_0(float value)
	{
		___U3CTimeoutU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CCancelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CCancelU3Ek__BackingField_1)); }
	inline bool get_U3CCancelU3Ek__BackingField_1() const { return ___U3CCancelU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CCancelU3Ek__BackingField_1() { return &___U3CCancelU3Ek__BackingField_1; }
	inline void set_U3CCancelU3Ek__BackingField_1(bool value)
	{
		___U3CCancelU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CDeleteU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CDeleteU3Ek__BackingField_2)); }
	inline bool get_U3CDeleteU3Ek__BackingField_2() const { return ___U3CDeleteU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CDeleteU3Ek__BackingField_2() { return &___U3CDeleteU3Ek__BackingField_2; }
	inline void set_U3CDeleteU3Ek__BackingField_2(bool value)
	{
		___U3CDeleteU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CFunctionU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CFunctionU3Ek__BackingField_3)); }
	inline String_t* get_U3CFunctionU3Ek__BackingField_3() const { return ___U3CFunctionU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CFunctionU3Ek__BackingField_3() { return &___U3CFunctionU3Ek__BackingField_3; }
	inline void set_U3CFunctionU3Ek__BackingField_3(String_t* value)
	{
		___U3CFunctionU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFunctionU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CParametersU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CParametersU3Ek__BackingField_4)); }
	inline Dictionary_2_t309261261 * get_U3CParametersU3Ek__BackingField_4() const { return ___U3CParametersU3Ek__BackingField_4; }
	inline Dictionary_2_t309261261 ** get_address_of_U3CParametersU3Ek__BackingField_4() { return &___U3CParametersU3Ek__BackingField_4; }
	inline void set_U3CParametersU3Ek__BackingField_4(Dictionary_2_t309261261 * value)
	{
		___U3CParametersU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CParametersU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CHeadersU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CHeadersU3Ek__BackingField_5)); }
	inline Dictionary_2_t3943999495 * get_U3CHeadersU3Ek__BackingField_5() const { return ___U3CHeadersU3Ek__BackingField_5; }
	inline Dictionary_2_t3943999495 ** get_address_of_U3CHeadersU3Ek__BackingField_5() { return &___U3CHeadersU3Ek__BackingField_5; }
	inline void set_U3CHeadersU3Ek__BackingField_5(Dictionary_2_t3943999495 * value)
	{
		___U3CHeadersU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CHeadersU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CSendU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CSendU3Ek__BackingField_6)); }
	inline ByteU5BU5D_t3397334013* get_U3CSendU3Ek__BackingField_6() const { return ___U3CSendU3Ek__BackingField_6; }
	inline ByteU5BU5D_t3397334013** get_address_of_U3CSendU3Ek__BackingField_6() { return &___U3CSendU3Ek__BackingField_6; }
	inline void set_U3CSendU3Ek__BackingField_6(ByteU5BU5D_t3397334013* value)
	{
		___U3CSendU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSendU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CFormsU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CFormsU3Ek__BackingField_7)); }
	inline Dictionary_2_t2694055125 * get_U3CFormsU3Ek__BackingField_7() const { return ___U3CFormsU3Ek__BackingField_7; }
	inline Dictionary_2_t2694055125 ** get_address_of_U3CFormsU3Ek__BackingField_7() { return &___U3CFormsU3Ek__BackingField_7; }
	inline void set_U3CFormsU3Ek__BackingField_7(Dictionary_2_t2694055125 * value)
	{
		___U3CFormsU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFormsU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3COnResponseU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3COnResponseU3Ek__BackingField_8)); }
	inline ResponseEvent_t1616568356 * get_U3COnResponseU3Ek__BackingField_8() const { return ___U3COnResponseU3Ek__BackingField_8; }
	inline ResponseEvent_t1616568356 ** get_address_of_U3COnResponseU3Ek__BackingField_8() { return &___U3COnResponseU3Ek__BackingField_8; }
	inline void set_U3COnResponseU3Ek__BackingField_8(ResponseEvent_t1616568356 * value)
	{
		___U3COnResponseU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3COnResponseU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3COnDownloadProgressU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3COnDownloadProgressU3Ek__BackingField_9)); }
	inline ProgressEvent_t4185145044 * get_U3COnDownloadProgressU3Ek__BackingField_9() const { return ___U3COnDownloadProgressU3Ek__BackingField_9; }
	inline ProgressEvent_t4185145044 ** get_address_of_U3COnDownloadProgressU3Ek__BackingField_9() { return &___U3COnDownloadProgressU3Ek__BackingField_9; }
	inline void set_U3COnDownloadProgressU3Ek__BackingField_9(ProgressEvent_t4185145044 * value)
	{
		___U3COnDownloadProgressU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3COnDownloadProgressU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3COnUploadProgressU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3COnUploadProgressU3Ek__BackingField_10)); }
	inline ProgressEvent_t4185145044 * get_U3COnUploadProgressU3Ek__BackingField_10() const { return ___U3COnUploadProgressU3Ek__BackingField_10; }
	inline ProgressEvent_t4185145044 ** get_address_of_U3COnUploadProgressU3Ek__BackingField_10() { return &___U3COnUploadProgressU3Ek__BackingField_10; }
	inline void set_U3COnUploadProgressU3Ek__BackingField_10(ProgressEvent_t4185145044 * value)
	{
		___U3COnUploadProgressU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3COnUploadProgressU3Ek__BackingField_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUEST_T466816980_H
#ifndef DEBUGINFO_T379980446_H
#define DEBUGINFO_T379980446_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Debug.DebugConsole/DebugInfo
struct  DebugInfo_t379980446  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Debug.DebugConsole/DebugInfo::m_Label
	String_t* ___m_Label_0;
	// IBM.Watson.DeveloperCloud.Debug.DebugConsole/GetDebugInfo IBM.Watson.DeveloperCloud.Debug.DebugConsole/DebugInfo::m_Callback
	GetDebugInfo_t2665501740 * ___m_Callback_1;
	// UnityEngine.GameObject IBM.Watson.DeveloperCloud.Debug.DebugConsole/DebugInfo::m_InfoObject
	GameObject_t1756533147 * ___m_InfoObject_2;
	// UnityEngine.UI.Text IBM.Watson.DeveloperCloud.Debug.DebugConsole/DebugInfo::m_TextOutput
	Text_t356221433 * ___m_TextOutput_3;

public:
	inline static int32_t get_offset_of_m_Label_0() { return static_cast<int32_t>(offsetof(DebugInfo_t379980446, ___m_Label_0)); }
	inline String_t* get_m_Label_0() const { return ___m_Label_0; }
	inline String_t** get_address_of_m_Label_0() { return &___m_Label_0; }
	inline void set_m_Label_0(String_t* value)
	{
		___m_Label_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Label_0), value);
	}

	inline static int32_t get_offset_of_m_Callback_1() { return static_cast<int32_t>(offsetof(DebugInfo_t379980446, ___m_Callback_1)); }
	inline GetDebugInfo_t2665501740 * get_m_Callback_1() const { return ___m_Callback_1; }
	inline GetDebugInfo_t2665501740 ** get_address_of_m_Callback_1() { return &___m_Callback_1; }
	inline void set_m_Callback_1(GetDebugInfo_t2665501740 * value)
	{
		___m_Callback_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Callback_1), value);
	}

	inline static int32_t get_offset_of_m_InfoObject_2() { return static_cast<int32_t>(offsetof(DebugInfo_t379980446, ___m_InfoObject_2)); }
	inline GameObject_t1756533147 * get_m_InfoObject_2() const { return ___m_InfoObject_2; }
	inline GameObject_t1756533147 ** get_address_of_m_InfoObject_2() { return &___m_InfoObject_2; }
	inline void set_m_InfoObject_2(GameObject_t1756533147 * value)
	{
		___m_InfoObject_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_InfoObject_2), value);
	}

	inline static int32_t get_offset_of_m_TextOutput_3() { return static_cast<int32_t>(offsetof(DebugInfo_t379980446, ___m_TextOutput_3)); }
	inline Text_t356221433 * get_m_TextOutput_3() const { return ___m_TextOutput_3; }
	inline Text_t356221433 ** get_address_of_m_TextOutput_3() { return &___m_TextOutput_3; }
	inline void set_m_TextOutput_3(Text_t356221433 * value)
	{
		___m_TextOutput_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextOutput_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGINFO_T379980446_H
#ifndef LOG_T4252317216_H
#define LOG_T4252317216_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Logging.Log
struct  Log_t4252317216  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOG_T4252317216_H
#ifndef OBJECTDATA_T1677090869_H
#define OBJECTDATA_T1677090869_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.ObjectData
struct  ObjectData_t1677090869  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.ObjectData::<text>k__BackingField
	String_t* ___U3CtextU3Ek__BackingField_0;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.DocSentiment IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.ObjectData::<sentiment>k__BackingField
	DocSentiment_t2354958393 * ___U3CsentimentU3Ek__BackingField_1;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.DocSentiment IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.ObjectData::<sentimentFromSubject>k__BackingField
	DocSentiment_t2354958393 * ___U3CsentimentFromSubjectU3Ek__BackingField_2;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Entity IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.ObjectData::<entity>k__BackingField
	Entity_t2757698109 * ___U3CentityU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CtextU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ObjectData_t1677090869, ___U3CtextU3Ek__BackingField_0)); }
	inline String_t* get_U3CtextU3Ek__BackingField_0() const { return ___U3CtextU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CtextU3Ek__BackingField_0() { return &___U3CtextU3Ek__BackingField_0; }
	inline void set_U3CtextU3Ek__BackingField_0(String_t* value)
	{
		___U3CtextU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CsentimentU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ObjectData_t1677090869, ___U3CsentimentU3Ek__BackingField_1)); }
	inline DocSentiment_t2354958393 * get_U3CsentimentU3Ek__BackingField_1() const { return ___U3CsentimentU3Ek__BackingField_1; }
	inline DocSentiment_t2354958393 ** get_address_of_U3CsentimentU3Ek__BackingField_1() { return &___U3CsentimentU3Ek__BackingField_1; }
	inline void set_U3CsentimentU3Ek__BackingField_1(DocSentiment_t2354958393 * value)
	{
		___U3CsentimentU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsentimentU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CsentimentFromSubjectU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ObjectData_t1677090869, ___U3CsentimentFromSubjectU3Ek__BackingField_2)); }
	inline DocSentiment_t2354958393 * get_U3CsentimentFromSubjectU3Ek__BackingField_2() const { return ___U3CsentimentFromSubjectU3Ek__BackingField_2; }
	inline DocSentiment_t2354958393 ** get_address_of_U3CsentimentFromSubjectU3Ek__BackingField_2() { return &___U3CsentimentFromSubjectU3Ek__BackingField_2; }
	inline void set_U3CsentimentFromSubjectU3Ek__BackingField_2(DocSentiment_t2354958393 * value)
	{
		___U3CsentimentFromSubjectU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsentimentFromSubjectU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CentityU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ObjectData_t1677090869, ___U3CentityU3Ek__BackingField_3)); }
	inline Entity_t2757698109 * get_U3CentityU3Ek__BackingField_3() const { return ___U3CentityU3Ek__BackingField_3; }
	inline Entity_t2757698109 ** get_address_of_U3CentityU3Ek__BackingField_3() { return &___U3CentityU3Ek__BackingField_3; }
	inline void set_U3CentityU3Ek__BackingField_3(Entity_t2757698109 * value)
	{
		___U3CentityU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CentityU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTDATA_T1677090869_H
#ifndef LOGSYSTEM_T3371271695_H
#define LOGSYSTEM_T3371271695_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Logging.LogSystem
struct  LogSystem_t3371271695  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<IBM.Watson.DeveloperCloud.Logging.ILogReactor> IBM.Watson.DeveloperCloud.Logging.LogSystem::m_Reactors
	List_1_t1547628219 * ___m_Reactors_1;

public:
	inline static int32_t get_offset_of_m_Reactors_1() { return static_cast<int32_t>(offsetof(LogSystem_t3371271695, ___m_Reactors_1)); }
	inline List_1_t1547628219 * get_m_Reactors_1() const { return ___m_Reactors_1; }
	inline List_1_t1547628219 ** get_address_of_m_Reactors_1() { return &___m_Reactors_1; }
	inline void set_m_Reactors_1(List_1_t1547628219 * value)
	{
		___m_Reactors_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Reactors_1), value);
	}
};

struct LogSystem_t3371271695_StaticFields
{
public:
	// System.Boolean IBM.Watson.DeveloperCloud.Logging.LogSystem::sm_bInstalledDefaultReactors
	bool ___sm_bInstalledDefaultReactors_0;
	// UnityEngine.Application/LogCallback IBM.Watson.DeveloperCloud.Logging.LogSystem::<>f__mg$cache0
	LogCallback_t1867914413 * ___U3CU3Ef__mgU24cache0_2;

public:
	inline static int32_t get_offset_of_sm_bInstalledDefaultReactors_0() { return static_cast<int32_t>(offsetof(LogSystem_t3371271695_StaticFields, ___sm_bInstalledDefaultReactors_0)); }
	inline bool get_sm_bInstalledDefaultReactors_0() const { return ___sm_bInstalledDefaultReactors_0; }
	inline bool* get_address_of_sm_bInstalledDefaultReactors_0() { return &___sm_bInstalledDefaultReactors_0; }
	inline void set_sm_bInstalledDefaultReactors_0(bool value)
	{
		___sm_bInstalledDefaultReactors_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_2() { return static_cast<int32_t>(offsetof(LogSystem_t3371271695_StaticFields, ___U3CU3Ef__mgU24cache0_2)); }
	inline LogCallback_t1867914413 * get_U3CU3Ef__mgU24cache0_2() const { return ___U3CU3Ef__mgU24cache0_2; }
	inline LogCallback_t1867914413 ** get_address_of_U3CU3Ef__mgU24cache0_2() { return &___U3CU3Ef__mgU24cache0_2; }
	inline void set_U3CU3Ef__mgU24cache0_2(LogCallback_t1867914413 * value)
	{
		___U3CU3Ef__mgU24cache0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGSYSTEM_T3371271695_H
#ifndef ALCHEMYAPI_T2955839919_H
#define ALCHEMYAPI_T2955839919_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI
struct  AlchemyAPI_t2955839919  : public RuntimeObject
{
public:

public:
};

struct AlchemyAPI_t2955839919_StaticFields
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI::mp_ApiKey
	String_t* ___mp_ApiKey_1;
	// FullSerializer.fsSerializer IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI::sm_Serializer
	fsSerializer_t4193731081 * ___sm_Serializer_2;

public:
	inline static int32_t get_offset_of_mp_ApiKey_1() { return static_cast<int32_t>(offsetof(AlchemyAPI_t2955839919_StaticFields, ___mp_ApiKey_1)); }
	inline String_t* get_mp_ApiKey_1() const { return ___mp_ApiKey_1; }
	inline String_t** get_address_of_mp_ApiKey_1() { return &___mp_ApiKey_1; }
	inline void set_mp_ApiKey_1(String_t* value)
	{
		___mp_ApiKey_1 = value;
		Il2CppCodeGenWriteBarrier((&___mp_ApiKey_1), value);
	}

	inline static int32_t get_offset_of_sm_Serializer_2() { return static_cast<int32_t>(offsetof(AlchemyAPI_t2955839919_StaticFields, ___sm_Serializer_2)); }
	inline fsSerializer_t4193731081 * get_sm_Serializer_2() const { return ___sm_Serializer_2; }
	inline fsSerializer_t4193731081 ** get_address_of_sm_Serializer_2() { return &___sm_Serializer_2; }
	inline void set_sm_Serializer_2(fsSerializer_t4193731081 * value)
	{
		___sm_Serializer_2 = value;
		Il2CppCodeGenWriteBarrier((&___sm_Serializer_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALCHEMYAPI_T2955839919_H
#ifndef TAXONOMY_T1527601757_H
#define TAXONOMY_T1527601757_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Taxonomy
struct  Taxonomy_t1527601757  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Taxonomy::<label>k__BackingField
	String_t* ___U3ClabelU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Taxonomy::<score>k__BackingField
	String_t* ___U3CscoreU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Taxonomy::<confident>k__BackingField
	String_t* ___U3CconfidentU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3ClabelU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Taxonomy_t1527601757, ___U3ClabelU3Ek__BackingField_0)); }
	inline String_t* get_U3ClabelU3Ek__BackingField_0() const { return ___U3ClabelU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3ClabelU3Ek__BackingField_0() { return &___U3ClabelU3Ek__BackingField_0; }
	inline void set_U3ClabelU3Ek__BackingField_0(String_t* value)
	{
		___U3ClabelU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClabelU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CscoreU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Taxonomy_t1527601757, ___U3CscoreU3Ek__BackingField_1)); }
	inline String_t* get_U3CscoreU3Ek__BackingField_1() const { return ___U3CscoreU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CscoreU3Ek__BackingField_1() { return &___U3CscoreU3Ek__BackingField_1; }
	inline void set_U3CscoreU3Ek__BackingField_1(String_t* value)
	{
		___U3CscoreU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CscoreU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CconfidentU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Taxonomy_t1527601757, ___U3CconfidentU3Ek__BackingField_2)); }
	inline String_t* get_U3CconfidentU3Ek__BackingField_2() const { return ___U3CconfidentU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CconfidentU3Ek__BackingField_2() { return &___U3CconfidentU3Ek__BackingField_2; }
	inline void set_U3CconfidentU3Ek__BackingField_2(String_t* value)
	{
		___U3CconfidentU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CconfidentU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAXONOMY_T1527601757_H
#ifndef VERB_T2846458383_H
#define VERB_T2846458383_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Verb
struct  Verb_t2846458383  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Verb::<text>k__BackingField
	String_t* ___U3CtextU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Verb::<tense>k__BackingField
	String_t* ___U3CtenseU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Verb::<negated>k__BackingField
	String_t* ___U3CnegatedU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CtextU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Verb_t2846458383, ___U3CtextU3Ek__BackingField_0)); }
	inline String_t* get_U3CtextU3Ek__BackingField_0() const { return ___U3CtextU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CtextU3Ek__BackingField_0() { return &___U3CtextU3Ek__BackingField_0; }
	inline void set_U3CtextU3Ek__BackingField_0(String_t* value)
	{
		___U3CtextU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CtenseU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Verb_t2846458383, ___U3CtenseU3Ek__BackingField_1)); }
	inline String_t* get_U3CtenseU3Ek__BackingField_1() const { return ___U3CtenseU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CtenseU3Ek__BackingField_1() { return &___U3CtenseU3Ek__BackingField_1; }
	inline void set_U3CtenseU3Ek__BackingField_1(String_t* value)
	{
		___U3CtenseU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtenseU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CnegatedU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Verb_t2846458383, ___U3CnegatedU3Ek__BackingField_2)); }
	inline String_t* get_U3CnegatedU3Ek__BackingField_2() const { return ___U3CnegatedU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CnegatedU3Ek__BackingField_2() { return &___U3CnegatedU3Ek__BackingField_2; }
	inline void set_U3CnegatedU3Ek__BackingField_2(String_t* value)
	{
		___U3CnegatedU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnegatedU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERB_T2846458383_H
#ifndef LOCATION_T4202627189_H
#define LOCATION_T4202627189_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Location
struct  Location_t4202627189  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Location::<text>k__BackingField
	String_t* ___U3CtextU3Ek__BackingField_0;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Entity[] IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Location::<entities>k__BackingField
	EntityU5BU5D_t3969836080* ___U3CentitiesU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CtextU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Location_t4202627189, ___U3CtextU3Ek__BackingField_0)); }
	inline String_t* get_U3CtextU3Ek__BackingField_0() const { return ___U3CtextU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CtextU3Ek__BackingField_0() { return &___U3CtextU3Ek__BackingField_0; }
	inline void set_U3CtextU3Ek__BackingField_0(String_t* value)
	{
		___U3CtextU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CentitiesU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Location_t4202627189, ___U3CentitiesU3Ek__BackingField_1)); }
	inline EntityU5BU5D_t3969836080* get_U3CentitiesU3Ek__BackingField_1() const { return ___U3CentitiesU3Ek__BackingField_1; }
	inline EntityU5BU5D_t3969836080** get_address_of_U3CentitiesU3Ek__BackingField_1() { return &___U3CentitiesU3Ek__BackingField_1; }
	inline void set_U3CentitiesU3Ek__BackingField_1(EntityU5BU5D_t3969836080* value)
	{
		___U3CentitiesU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CentitiesU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCATION_T4202627189_H
#ifndef U3CPROCESSRECEIVEQUEUEU3EC__ITERATOR0_T1751931715_H
#define U3CPROCESSRECEIVEQUEUEU3EC__ITERATOR0_T1751931715_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Connection.WSConnector/<ProcessReceiveQueue>c__Iterator0
struct  U3CProcessReceiveQueueU3Ec__Iterator0_t1751931715  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Connection.WSConnector IBM.Watson.DeveloperCloud.Connection.WSConnector/<ProcessReceiveQueue>c__Iterator0::$this
	WSConnector_t2552241039 * ___U24this_0;
	// System.Object IBM.Watson.DeveloperCloud.Connection.WSConnector/<ProcessReceiveQueue>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean IBM.Watson.DeveloperCloud.Connection.WSConnector/<ProcessReceiveQueue>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 IBM.Watson.DeveloperCloud.Connection.WSConnector/<ProcessReceiveQueue>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CProcessReceiveQueueU3Ec__Iterator0_t1751931715, ___U24this_0)); }
	inline WSConnector_t2552241039 * get_U24this_0() const { return ___U24this_0; }
	inline WSConnector_t2552241039 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(WSConnector_t2552241039 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CProcessReceiveQueueU3Ec__Iterator0_t1751931715, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CProcessReceiveQueueU3Ec__Iterator0_t1751931715, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CProcessReceiveQueueU3Ec__Iterator0_t1751931715, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPROCESSRECEIVEQUEUEU3EC__ITERATOR0_T1751931715_H
#ifndef RELATION_T2336466576_H
#define RELATION_T2336466576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Relation
struct  Relation_t2336466576  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Relation::<sentence>k__BackingField
	String_t* ___U3CsentenceU3Ek__BackingField_0;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Subject IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Relation::<subject>k__BackingField
	Subject_t927036590 * ___U3CsubjectU3Ek__BackingField_1;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Action IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Relation::<action>k__BackingField
	Action_t3935607096 * ___U3CactionU3Ek__BackingField_2;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.ObjectData IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Relation::<object>k__BackingField
	ObjectData_t1677090869 * ___U3CobjectU3Ek__BackingField_3;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Location IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Relation::<location>k__BackingField
	Location_t4202627189 * ___U3ClocationU3Ek__BackingField_4;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Temporal IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Relation::<temporal>k__BackingField
	Temporal_t3690293474 * ___U3CtemporalU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CsentenceU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Relation_t2336466576, ___U3CsentenceU3Ek__BackingField_0)); }
	inline String_t* get_U3CsentenceU3Ek__BackingField_0() const { return ___U3CsentenceU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CsentenceU3Ek__BackingField_0() { return &___U3CsentenceU3Ek__BackingField_0; }
	inline void set_U3CsentenceU3Ek__BackingField_0(String_t* value)
	{
		___U3CsentenceU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsentenceU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CsubjectU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Relation_t2336466576, ___U3CsubjectU3Ek__BackingField_1)); }
	inline Subject_t927036590 * get_U3CsubjectU3Ek__BackingField_1() const { return ___U3CsubjectU3Ek__BackingField_1; }
	inline Subject_t927036590 ** get_address_of_U3CsubjectU3Ek__BackingField_1() { return &___U3CsubjectU3Ek__BackingField_1; }
	inline void set_U3CsubjectU3Ek__BackingField_1(Subject_t927036590 * value)
	{
		___U3CsubjectU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsubjectU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CactionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Relation_t2336466576, ___U3CactionU3Ek__BackingField_2)); }
	inline Action_t3935607096 * get_U3CactionU3Ek__BackingField_2() const { return ___U3CactionU3Ek__BackingField_2; }
	inline Action_t3935607096 ** get_address_of_U3CactionU3Ek__BackingField_2() { return &___U3CactionU3Ek__BackingField_2; }
	inline void set_U3CactionU3Ek__BackingField_2(Action_t3935607096 * value)
	{
		___U3CactionU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CactionU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CobjectU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Relation_t2336466576, ___U3CobjectU3Ek__BackingField_3)); }
	inline ObjectData_t1677090869 * get_U3CobjectU3Ek__BackingField_3() const { return ___U3CobjectU3Ek__BackingField_3; }
	inline ObjectData_t1677090869 ** get_address_of_U3CobjectU3Ek__BackingField_3() { return &___U3CobjectU3Ek__BackingField_3; }
	inline void set_U3CobjectU3Ek__BackingField_3(ObjectData_t1677090869 * value)
	{
		___U3CobjectU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CobjectU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3ClocationU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Relation_t2336466576, ___U3ClocationU3Ek__BackingField_4)); }
	inline Location_t4202627189 * get_U3ClocationU3Ek__BackingField_4() const { return ___U3ClocationU3Ek__BackingField_4; }
	inline Location_t4202627189 ** get_address_of_U3ClocationU3Ek__BackingField_4() { return &___U3ClocationU3Ek__BackingField_4; }
	inline void set_U3ClocationU3Ek__BackingField_4(Location_t4202627189 * value)
	{
		___U3ClocationU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClocationU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CtemporalU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Relation_t2336466576, ___U3CtemporalU3Ek__BackingField_5)); }
	inline Temporal_t3690293474 * get_U3CtemporalU3Ek__BackingField_5() const { return ___U3CtemporalU3Ek__BackingField_5; }
	inline Temporal_t3690293474 ** get_address_of_U3CtemporalU3Ek__BackingField_5() { return &___U3CtemporalU3Ek__BackingField_5; }
	inline void set_U3CtemporalU3Ek__BackingField_5(Temporal_t3690293474 * value)
	{
		___U3CtemporalU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtemporalU3Ek__BackingField_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RELATION_T2336466576_H
#ifndef SUBJECT_T927036590_H
#define SUBJECT_T927036590_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Subject
struct  Subject_t927036590  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Subject::<text>k__BackingField
	String_t* ___U3CtextU3Ek__BackingField_0;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.DocSentiment IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Subject::<sentiment>k__BackingField
	DocSentiment_t2354958393 * ___U3CsentimentU3Ek__BackingField_1;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Entity IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Subject::<entities>k__BackingField
	Entity_t2757698109 * ___U3CentitiesU3Ek__BackingField_2;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Keyword IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Subject::<keywords>k__BackingField
	Keyword_t3384522415 * ___U3CkeywordsU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CtextU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Subject_t927036590, ___U3CtextU3Ek__BackingField_0)); }
	inline String_t* get_U3CtextU3Ek__BackingField_0() const { return ___U3CtextU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CtextU3Ek__BackingField_0() { return &___U3CtextU3Ek__BackingField_0; }
	inline void set_U3CtextU3Ek__BackingField_0(String_t* value)
	{
		___U3CtextU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CsentimentU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Subject_t927036590, ___U3CsentimentU3Ek__BackingField_1)); }
	inline DocSentiment_t2354958393 * get_U3CsentimentU3Ek__BackingField_1() const { return ___U3CsentimentU3Ek__BackingField_1; }
	inline DocSentiment_t2354958393 ** get_address_of_U3CsentimentU3Ek__BackingField_1() { return &___U3CsentimentU3Ek__BackingField_1; }
	inline void set_U3CsentimentU3Ek__BackingField_1(DocSentiment_t2354958393 * value)
	{
		___U3CsentimentU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsentimentU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CentitiesU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Subject_t927036590, ___U3CentitiesU3Ek__BackingField_2)); }
	inline Entity_t2757698109 * get_U3CentitiesU3Ek__BackingField_2() const { return ___U3CentitiesU3Ek__BackingField_2; }
	inline Entity_t2757698109 ** get_address_of_U3CentitiesU3Ek__BackingField_2() { return &___U3CentitiesU3Ek__BackingField_2; }
	inline void set_U3CentitiesU3Ek__BackingField_2(Entity_t2757698109 * value)
	{
		___U3CentitiesU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CentitiesU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CkeywordsU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Subject_t927036590, ___U3CkeywordsU3Ek__BackingField_3)); }
	inline Keyword_t3384522415 * get_U3CkeywordsU3Ek__BackingField_3() const { return ___U3CkeywordsU3Ek__BackingField_3; }
	inline Keyword_t3384522415 ** get_address_of_U3CkeywordsU3Ek__BackingField_3() { return &___U3CkeywordsU3Ek__BackingField_3; }
	inline void set_U3CkeywordsU3Ek__BackingField_3(Keyword_t3384522415 * value)
	{
		___U3CkeywordsU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CkeywordsU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBJECT_T927036590_H
#ifndef CHECKSERVICESTATUS_T3778204258_H
#define CHECKSERVICESTATUS_T3778204258_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/CheckServiceStatus
struct  CheckServiceStatus_t3778204258  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/CheckServiceStatus::m_Service
	AlchemyAPI_t2955839919 * ___m_Service_0;
	// IBM.Watson.DeveloperCloud.Services.ServiceStatus IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/CheckServiceStatus::m_Callback
	ServiceStatus_t1443707987 * ___m_Callback_1;

public:
	inline static int32_t get_offset_of_m_Service_0() { return static_cast<int32_t>(offsetof(CheckServiceStatus_t3778204258, ___m_Service_0)); }
	inline AlchemyAPI_t2955839919 * get_m_Service_0() const { return ___m_Service_0; }
	inline AlchemyAPI_t2955839919 ** get_address_of_m_Service_0() { return &___m_Service_0; }
	inline void set_m_Service_0(AlchemyAPI_t2955839919 * value)
	{
		___m_Service_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Service_0), value);
	}

	inline static int32_t get_offset_of_m_Callback_1() { return static_cast<int32_t>(offsetof(CheckServiceStatus_t3778204258, ___m_Callback_1)); }
	inline ServiceStatus_t1443707987 * get_m_Callback_1() const { return ___m_Callback_1; }
	inline ServiceStatus_t1443707987 ** get_address_of_m_Callback_1() { return &___m_Callback_1; }
	inline void set_m_Callback_1(ServiceStatus_t1443707987 * value)
	{
		___m_Callback_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Callback_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHECKSERVICESTATUS_T3778204258_H
#ifndef MESSAGE_T2515021790_H
#define MESSAGE_T2515021790_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Connection.WSConnector/Message
struct  Message_t2515021790  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGE_T2515021790_H
#ifndef ACTION_T3935607096_H
#define ACTION_T3935607096_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Action
struct  Action_t3935607096  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Action::<text>k__BackingField
	String_t* ___U3CtextU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Action::<lemmatized>k__BackingField
	String_t* ___U3ClemmatizedU3Ek__BackingField_1;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Verb IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Action::<verb>k__BackingField
	Verb_t2846458383 * ___U3CverbU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CtextU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Action_t3935607096, ___U3CtextU3Ek__BackingField_0)); }
	inline String_t* get_U3CtextU3Ek__BackingField_0() const { return ___U3CtextU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CtextU3Ek__BackingField_0() { return &___U3CtextU3Ek__BackingField_0; }
	inline void set_U3CtextU3Ek__BackingField_0(String_t* value)
	{
		___U3CtextU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3ClemmatizedU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Action_t3935607096, ___U3ClemmatizedU3Ek__BackingField_1)); }
	inline String_t* get_U3ClemmatizedU3Ek__BackingField_1() const { return ___U3ClemmatizedU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3ClemmatizedU3Ek__BackingField_1() { return &___U3ClemmatizedU3Ek__BackingField_1; }
	inline void set_U3ClemmatizedU3Ek__BackingField_1(String_t* value)
	{
		___U3ClemmatizedU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClemmatizedU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CverbU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Action_t3935607096, ___U3CverbU3Ek__BackingField_2)); }
	inline Verb_t2846458383 * get_U3CverbU3Ek__BackingField_2() const { return ___U3CverbU3Ek__BackingField_2; }
	inline Verb_t2846458383 ** get_address_of_U3CverbU3Ek__BackingField_2() { return &___U3CverbU3Ek__BackingField_2; }
	inline void set_U3CverbU3Ek__BackingField_2(Verb_t2846458383 * value)
	{
		___U3CverbU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CverbU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_T3935607096_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	IntPtr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline IntPtr_t get_Zero_1() const { return ___Zero_1; }
	inline IntPtr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(IntPtr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef GETMICROFORMATSREQUEST_T3175661922_H
#define GETMICROFORMATSREQUEST_T3175661922_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/GetMicroformatsRequest
struct  GetMicroformatsRequest_t3175661922  : public Request_t466816980
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/GetMicroformatsRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_11;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/OnGetMicroformats IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/GetMicroformatsRequest::<Callback>k__BackingField
	OnGetMicroformats_t327929786 * ___U3CCallbackU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetMicroformatsRequest_t3175661922, ___U3CDataU3Ek__BackingField_11)); }
	inline String_t* get_U3CDataU3Ek__BackingField_11() const { return ___U3CDataU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_11() { return &___U3CDataU3Ek__BackingField_11; }
	inline void set_U3CDataU3Ek__BackingField_11(String_t* value)
	{
		___U3CDataU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GetMicroformatsRequest_t3175661922, ___U3CCallbackU3Ek__BackingField_12)); }
	inline OnGetMicroformats_t327929786 * get_U3CCallbackU3Ek__BackingField_12() const { return ___U3CCallbackU3Ek__BackingField_12; }
	inline OnGetMicroformats_t327929786 ** get_address_of_U3CCallbackU3Ek__BackingField_12() { return &___U3CCallbackU3Ek__BackingField_12; }
	inline void set_U3CCallbackU3Ek__BackingField_12(OnGetMicroformats_t327929786 * value)
	{
		___U3CCallbackU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETMICROFORMATSREQUEST_T3175661922_H
#ifndef BINARYMESSAGE_T3924129269_H
#define BINARYMESSAGE_T3924129269_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Connection.WSConnector/BinaryMessage
struct  BinaryMessage_t3924129269  : public Message_t2515021790
{
public:
	// System.Byte[] IBM.Watson.DeveloperCloud.Connection.WSConnector/BinaryMessage::<Data>k__BackingField
	ByteU5BU5D_t3397334013* ___U3CDataU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(BinaryMessage_t3924129269, ___U3CDataU3Ek__BackingField_0)); }
	inline ByteU5BU5D_t3397334013* get_U3CDataU3Ek__BackingField_0() const { return ___U3CDataU3Ek__BackingField_0; }
	inline ByteU5BU5D_t3397334013** get_address_of_U3CDataU3Ek__BackingField_0() { return &___U3CDataU3Ek__BackingField_0; }
	inline void set_U3CDataU3Ek__BackingField_0(ByteU5BU5D_t3397334013* value)
	{
		___U3CDataU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINARYMESSAGE_T3924129269_H
#ifndef TEXTMESSAGE_T143068955_H
#define TEXTMESSAGE_T143068955_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Connection.WSConnector/TextMessage
struct  TextMessage_t143068955  : public Message_t2515021790
{
public:
	// System.String IBM.Watson.DeveloperCloud.Connection.WSConnector/TextMessage::<Text>k__BackingField
	String_t* ___U3CTextU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CTextU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TextMessage_t143068955, ___U3CTextU3Ek__BackingField_0)); }
	inline String_t* get_U3CTextU3Ek__BackingField_0() const { return ___U3CTextU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CTextU3Ek__BackingField_0() { return &___U3CTextU3Ek__BackingField_0; }
	inline void set_U3CTextU3Ek__BackingField_0(String_t* value)
	{
		___U3CTextU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTextU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTMESSAGE_T143068955_H
#ifndef TIMESPAN_T3430258949_H
#define TIMESPAN_T3430258949_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t3430258949 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_8;

public:
	inline static int32_t get_offset_of__ticks_8() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949, ____ticks_8)); }
	inline int64_t get__ticks_8() const { return ____ticks_8; }
	inline int64_t* get_address_of__ticks_8() { return &____ticks_8; }
	inline void set__ticks_8(int64_t value)
	{
		____ticks_8 = value;
	}
};

struct TimeSpan_t3430258949_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t3430258949  ___MaxValue_5;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t3430258949  ___MinValue_6;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t3430258949  ___Zero_7;

public:
	inline static int32_t get_offset_of_MaxValue_5() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949_StaticFields, ___MaxValue_5)); }
	inline TimeSpan_t3430258949  get_MaxValue_5() const { return ___MaxValue_5; }
	inline TimeSpan_t3430258949 * get_address_of_MaxValue_5() { return &___MaxValue_5; }
	inline void set_MaxValue_5(TimeSpan_t3430258949  value)
	{
		___MaxValue_5 = value;
	}

	inline static int32_t get_offset_of_MinValue_6() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949_StaticFields, ___MinValue_6)); }
	inline TimeSpan_t3430258949  get_MinValue_6() const { return ___MinValue_6; }
	inline TimeSpan_t3430258949 * get_address_of_MinValue_6() { return &___MinValue_6; }
	inline void set_MinValue_6(TimeSpan_t3430258949  value)
	{
		___MinValue_6 = value;
	}

	inline static int32_t get_offset_of_Zero_7() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949_StaticFields, ___Zero_7)); }
	inline TimeSpan_t3430258949  get_Zero_7() const { return ___Zero_7; }
	inline TimeSpan_t3430258949 * get_address_of_Zero_7() { return &___Zero_7; }
	inline void set_Zero_7(TimeSpan_t3430258949  value)
	{
		___Zero_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T3430258949_H
#ifndef VOID_T1841601450_H
#define VOID_T1841601450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1841601450 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1841601450_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t1328083999* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t1328083999* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t1328083999** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t1328083999* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef GETRELATIONSREQUEST_T1673598355_H
#define GETRELATIONSREQUEST_T1673598355_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/GetRelationsRequest
struct  GetRelationsRequest_t1673598355  : public Request_t466816980
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/GetRelationsRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_11;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/OnGetRelations IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/GetRelationsRequest::<Callback>k__BackingField
	OnGetRelations_t1280830431 * ___U3CCallbackU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetRelationsRequest_t1673598355, ___U3CDataU3Ek__BackingField_11)); }
	inline String_t* get_U3CDataU3Ek__BackingField_11() const { return ___U3CDataU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_11() { return &___U3CDataU3Ek__BackingField_11; }
	inline void set_U3CDataU3Ek__BackingField_11(String_t* value)
	{
		___U3CDataU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GetRelationsRequest_t1673598355, ___U3CCallbackU3Ek__BackingField_12)); }
	inline OnGetRelations_t1280830431 * get_U3CCallbackU3Ek__BackingField_12() const { return ___U3CCallbackU3Ek__BackingField_12; }
	inline OnGetRelations_t1280830431 ** get_address_of_U3CCallbackU3Ek__BackingField_12() { return &___U3CCallbackU3Ek__BackingField_12; }
	inline void set_U3CCallbackU3Ek__BackingField_12(OnGetRelations_t1280830431 * value)
	{
		___U3CCallbackU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRELATIONSREQUEST_T1673598355_H
#ifndef GETPUBLICATIONDATEREQUEST_T3398587598_H
#define GETPUBLICATIONDATEREQUEST_T3398587598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/GetPublicationDateRequest
struct  GetPublicationDateRequest_t3398587598  : public Request_t466816980
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/GetPublicationDateRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_11;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/OnGetPublicationDate IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/GetPublicationDateRequest::<Callback>k__BackingField
	OnGetPublicationDate_t443403836 * ___U3CCallbackU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetPublicationDateRequest_t3398587598, ___U3CDataU3Ek__BackingField_11)); }
	inline String_t* get_U3CDataU3Ek__BackingField_11() const { return ___U3CDataU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_11() { return &___U3CDataU3Ek__BackingField_11; }
	inline void set_U3CDataU3Ek__BackingField_11(String_t* value)
	{
		___U3CDataU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GetPublicationDateRequest_t3398587598, ___U3CCallbackU3Ek__BackingField_12)); }
	inline OnGetPublicationDate_t443403836 * get_U3CCallbackU3Ek__BackingField_12() const { return ___U3CCallbackU3Ek__BackingField_12; }
	inline OnGetPublicationDate_t443403836 ** get_address_of_U3CCallbackU3Ek__BackingField_12() { return &___U3CCallbackU3Ek__BackingField_12; }
	inline void set_U3CCallbackU3Ek__BackingField_12(OnGetPublicationDate_t443403836 * value)
	{
		___U3CCallbackU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETPUBLICATIONDATEREQUEST_T3398587598_H
#ifndef GETTEXTSENTIMENTREQUEST_T1002716236_H
#define GETTEXTSENTIMENTREQUEST_T1002716236_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/GetTextSentimentRequest
struct  GetTextSentimentRequest_t1002716236  : public Request_t466816980
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/GetTextSentimentRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_11;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/OnGetTextSentiment IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/GetTextSentimentRequest::<Callback>k__BackingField
	OnGetTextSentiment_t3575883834 * ___U3CCallbackU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetTextSentimentRequest_t1002716236, ___U3CDataU3Ek__BackingField_11)); }
	inline String_t* get_U3CDataU3Ek__BackingField_11() const { return ___U3CDataU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_11() { return &___U3CDataU3Ek__BackingField_11; }
	inline void set_U3CDataU3Ek__BackingField_11(String_t* value)
	{
		___U3CDataU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GetTextSentimentRequest_t1002716236, ___U3CCallbackU3Ek__BackingField_12)); }
	inline OnGetTextSentiment_t3575883834 * get_U3CCallbackU3Ek__BackingField_12() const { return ___U3CCallbackU3Ek__BackingField_12; }
	inline OnGetTextSentiment_t3575883834 ** get_address_of_U3CCallbackU3Ek__BackingField_12() { return &___U3CCallbackU3Ek__BackingField_12; }
	inline void set_U3CCallbackU3Ek__BackingField_12(OnGetTextSentiment_t3575883834 * value)
	{
		___U3CCallbackU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETTEXTSENTIMENTREQUEST_T1002716236_H
#ifndef GETDATESREQUEST_T2698785969_H
#define GETDATESREQUEST_T2698785969_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/GetDatesRequest
struct  GetDatesRequest_t2698785969  : public Request_t466816980
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/GetDatesRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_11;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/OnGetDates IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/GetDatesRequest::<Callback>k__BackingField
	OnGetDates_t91791237 * ___U3CCallbackU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetDatesRequest_t2698785969, ___U3CDataU3Ek__BackingField_11)); }
	inline String_t* get_U3CDataU3Ek__BackingField_11() const { return ___U3CDataU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_11() { return &___U3CDataU3Ek__BackingField_11; }
	inline void set_U3CDataU3Ek__BackingField_11(String_t* value)
	{
		___U3CDataU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GetDatesRequest_t2698785969, ___U3CCallbackU3Ek__BackingField_12)); }
	inline OnGetDates_t91791237 * get_U3CCallbackU3Ek__BackingField_12() const { return ___U3CCallbackU3Ek__BackingField_12; }
	inline OnGetDates_t91791237 ** get_address_of_U3CCallbackU3Ek__BackingField_12() { return &___U3CCallbackU3Ek__BackingField_12; }
	inline void set_U3CCallbackU3Ek__BackingField_12(OnGetDates_t91791237 * value)
	{
		___U3CCallbackU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETDATESREQUEST_T2698785969_H
#ifndef GETEMOTIONSREQUEST_T3200624748_H
#define GETEMOTIONSREQUEST_T3200624748_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/GetEmotionsRequest
struct  GetEmotionsRequest_t3200624748  : public Request_t466816980
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/GetEmotionsRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_11;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/OnGetEmotions IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/GetEmotionsRequest::<Callback>k__BackingField
	OnGetEmotions_t1524549668 * ___U3CCallbackU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetEmotionsRequest_t3200624748, ___U3CDataU3Ek__BackingField_11)); }
	inline String_t* get_U3CDataU3Ek__BackingField_11() const { return ___U3CDataU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_11() { return &___U3CDataU3Ek__BackingField_11; }
	inline void set_U3CDataU3Ek__BackingField_11(String_t* value)
	{
		___U3CDataU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GetEmotionsRequest_t3200624748, ___U3CCallbackU3Ek__BackingField_12)); }
	inline OnGetEmotions_t1524549668 * get_U3CCallbackU3Ek__BackingField_12() const { return ___U3CCallbackU3Ek__BackingField_12; }
	inline OnGetEmotions_t1524549668 ** get_address_of_U3CCallbackU3Ek__BackingField_12() { return &___U3CCallbackU3Ek__BackingField_12; }
	inline void set_U3CCallbackU3Ek__BackingField_12(OnGetEmotions_t1524549668 * value)
	{
		___U3CCallbackU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETEMOTIONSREQUEST_T3200624748_H
#ifndef GETKEYWORDSREQUEST_T1200965810_H
#define GETKEYWORDSREQUEST_T1200965810_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/GetKeywordsRequest
struct  GetKeywordsRequest_t1200965810  : public Request_t466816980
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/GetKeywordsRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_11;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/OnGetKeywords IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/GetKeywordsRequest::<Callback>k__BackingField
	OnGetKeywords_t1978903802 * ___U3CCallbackU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetKeywordsRequest_t1200965810, ___U3CDataU3Ek__BackingField_11)); }
	inline String_t* get_U3CDataU3Ek__BackingField_11() const { return ___U3CDataU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_11() { return &___U3CDataU3Ek__BackingField_11; }
	inline void set_U3CDataU3Ek__BackingField_11(String_t* value)
	{
		___U3CDataU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GetKeywordsRequest_t1200965810, ___U3CCallbackU3Ek__BackingField_12)); }
	inline OnGetKeywords_t1978903802 * get_U3CCallbackU3Ek__BackingField_12() const { return ___U3CCallbackU3Ek__BackingField_12; }
	inline OnGetKeywords_t1978903802 ** get_address_of_U3CCallbackU3Ek__BackingField_12() { return &___U3CCallbackU3Ek__BackingField_12; }
	inline void set_U3CCallbackU3Ek__BackingField_12(OnGetKeywords_t1978903802 * value)
	{
		___U3CCallbackU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETKEYWORDSREQUEST_T1200965810_H
#ifndef DETECTFEEDSREQUEST_T576411670_H
#define DETECTFEEDSREQUEST_T576411670_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/DetectFeedsRequest
struct  DetectFeedsRequest_t576411670  : public Request_t466816980
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/DetectFeedsRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_11;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/OnDetectFeeds IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/DetectFeedsRequest::<Callback>k__BackingField
	OnDetectFeeds_t4021436334 * ___U3CCallbackU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(DetectFeedsRequest_t576411670, ___U3CDataU3Ek__BackingField_11)); }
	inline String_t* get_U3CDataU3Ek__BackingField_11() const { return ___U3CDataU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_11() { return &___U3CDataU3Ek__BackingField_11; }
	inline void set_U3CDataU3Ek__BackingField_11(String_t* value)
	{
		___U3CDataU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(DetectFeedsRequest_t576411670, ___U3CCallbackU3Ek__BackingField_12)); }
	inline OnDetectFeeds_t4021436334 * get_U3CCallbackU3Ek__BackingField_12() const { return ___U3CCallbackU3Ek__BackingField_12; }
	inline OnDetectFeeds_t4021436334 ** get_address_of_U3CCallbackU3Ek__BackingField_12() { return &___U3CCallbackU3Ek__BackingField_12; }
	inline void set_U3CCallbackU3Ek__BackingField_12(OnDetectFeeds_t4021436334 * value)
	{
		___U3CCallbackU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DETECTFEEDSREQUEST_T576411670_H
#ifndef GETENTITIESREQUEST_T2587138723_H
#define GETENTITIESREQUEST_T2587138723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/GetEntitiesRequest
struct  GetEntitiesRequest_t2587138723  : public Request_t466816980
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/GetEntitiesRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_11;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/OnGetEntities IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/GetEntitiesRequest::<Callback>k__BackingField
	OnGetEntities_t1909489667 * ___U3CCallbackU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetEntitiesRequest_t2587138723, ___U3CDataU3Ek__BackingField_11)); }
	inline String_t* get_U3CDataU3Ek__BackingField_11() const { return ___U3CDataU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_11() { return &___U3CDataU3Ek__BackingField_11; }
	inline void set_U3CDataU3Ek__BackingField_11(String_t* value)
	{
		___U3CDataU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GetEntitiesRequest_t2587138723, ___U3CCallbackU3Ek__BackingField_12)); }
	inline OnGetEntities_t1909489667 * get_U3CCallbackU3Ek__BackingField_12() const { return ___U3CCallbackU3Ek__BackingField_12; }
	inline OnGetEntities_t1909489667 ** get_address_of_U3CCallbackU3Ek__BackingField_12() { return &___U3CCallbackU3Ek__BackingField_12; }
	inline void set_U3CCallbackU3Ek__BackingField_12(OnGetEntities_t1909489667 * value)
	{
		___U3CCallbackU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETENTITIESREQUEST_T2587138723_H
#ifndef GETLANGUAGESREQUEST_T4142277483_H
#define GETLANGUAGESREQUEST_T4142277483_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/GetLanguagesRequest
struct  GetLanguagesRequest_t4142277483  : public Request_t466816980
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/GetLanguagesRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_11;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/OnGetLanguages IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/GetLanguagesRequest::<Callback>k__BackingField
	OnGetLanguages_t3197075847 * ___U3CCallbackU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetLanguagesRequest_t4142277483, ___U3CDataU3Ek__BackingField_11)); }
	inline String_t* get_U3CDataU3Ek__BackingField_11() const { return ___U3CDataU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_11() { return &___U3CDataU3Ek__BackingField_11; }
	inline void set_U3CDataU3Ek__BackingField_11(String_t* value)
	{
		___U3CDataU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GetLanguagesRequest_t4142277483, ___U3CCallbackU3Ek__BackingField_12)); }
	inline OnGetLanguages_t3197075847 * get_U3CCallbackU3Ek__BackingField_12() const { return ___U3CCallbackU3Ek__BackingField_12; }
	inline OnGetLanguages_t3197075847 ** get_address_of_U3CCallbackU3Ek__BackingField_12() { return &___U3CCallbackU3Ek__BackingField_12; }
	inline void set_U3CCallbackU3Ek__BackingField_12(OnGetLanguages_t3197075847 * value)
	{
		___U3CCallbackU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETLANGUAGESREQUEST_T4142277483_H
#ifndef GETAUTHORSREQUEST_T1850603144_H
#define GETAUTHORSREQUEST_T1850603144_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/GetAuthorsRequest
struct  GetAuthorsRequest_t1850603144  : public Request_t466816980
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/GetAuthorsRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_11;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/OnGetAuthors IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/GetAuthorsRequest::<Callback>k__BackingField
	OnGetAuthors_t505057758 * ___U3CCallbackU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetAuthorsRequest_t1850603144, ___U3CDataU3Ek__BackingField_11)); }
	inline String_t* get_U3CDataU3Ek__BackingField_11() const { return ___U3CDataU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_11() { return &___U3CDataU3Ek__BackingField_11; }
	inline void set_U3CDataU3Ek__BackingField_11(String_t* value)
	{
		___U3CDataU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GetAuthorsRequest_t1850603144, ___U3CCallbackU3Ek__BackingField_12)); }
	inline OnGetAuthors_t505057758 * get_U3CCallbackU3Ek__BackingField_12() const { return ___U3CCallbackU3Ek__BackingField_12; }
	inline OnGetAuthors_t505057758 ** get_address_of_U3CCallbackU3Ek__BackingField_12() { return &___U3CCallbackU3Ek__BackingField_12; }
	inline void set_U3CCallbackU3Ek__BackingField_12(OnGetAuthors_t505057758 * value)
	{
		___U3CCallbackU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETAUTHORSREQUEST_T1850603144_H
#ifndef GETNEWSREQUEST_T4208688917_H
#define GETNEWSREQUEST_T4208688917_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/GetNewsRequest
struct  GetNewsRequest_t4208688917  : public Request_t466816980
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/GetNewsRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_11;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/OnGetNews IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/GetNewsRequest::<Callback>k__BackingField
	OnGetNews_t1755812277 * ___U3CCallbackU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetNewsRequest_t4208688917, ___U3CDataU3Ek__BackingField_11)); }
	inline String_t* get_U3CDataU3Ek__BackingField_11() const { return ___U3CDataU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_11() { return &___U3CDataU3Ek__BackingField_11; }
	inline void set_U3CDataU3Ek__BackingField_11(String_t* value)
	{
		___U3CDataU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GetNewsRequest_t4208688917, ___U3CCallbackU3Ek__BackingField_12)); }
	inline OnGetNews_t1755812277 * get_U3CCallbackU3Ek__BackingField_12() const { return ___U3CCallbackU3Ek__BackingField_12; }
	inline OnGetNews_t1755812277 ** get_address_of_U3CCallbackU3Ek__BackingField_12() { return &___U3CCallbackU3Ek__BackingField_12; }
	inline void set_U3CCallbackU3Ek__BackingField_12(OnGetNews_t1755812277 * value)
	{
		___U3CCallbackU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETNEWSREQUEST_T4208688917_H
#ifndef COMBINEDCALLREQUEST_T1669398659_H
#define COMBINEDCALLREQUEST_T1669398659_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/CombinedCallRequest
struct  CombinedCallRequest_t1669398659  : public Request_t466816980
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/CombinedCallRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_11;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/OnGetCombinedData IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/CombinedCallRequest::<Callback>k__BackingField
	OnGetCombinedData_t4285430159 * ___U3CCallbackU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(CombinedCallRequest_t1669398659, ___U3CDataU3Ek__BackingField_11)); }
	inline String_t* get_U3CDataU3Ek__BackingField_11() const { return ___U3CDataU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_11() { return &___U3CDataU3Ek__BackingField_11; }
	inline void set_U3CDataU3Ek__BackingField_11(String_t* value)
	{
		___U3CDataU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(CombinedCallRequest_t1669398659, ___U3CCallbackU3Ek__BackingField_12)); }
	inline OnGetCombinedData_t4285430159 * get_U3CCallbackU3Ek__BackingField_12() const { return ___U3CCallbackU3Ek__BackingField_12; }
	inline OnGetCombinedData_t4285430159 ** get_address_of_U3CCallbackU3Ek__BackingField_12() { return &___U3CCallbackU3Ek__BackingField_12; }
	inline void set_U3CCallbackU3Ek__BackingField_12(OnGetCombinedData_t4285430159 * value)
	{
		___U3CCallbackU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMBINEDCALLREQUEST_T1669398659_H
#ifndef GETTITLEREQUEST_T180924238_H
#define GETTITLEREQUEST_T180924238_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/GetTitleRequest
struct  GetTitleRequest_t180924238  : public Request_t466816980
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/GetTitleRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_11;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/OnGetTitle IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/GetTitleRequest::<Callback>k__BackingField
	OnGetTitle_t1839253948 * ___U3CCallbackU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetTitleRequest_t180924238, ___U3CDataU3Ek__BackingField_11)); }
	inline String_t* get_U3CDataU3Ek__BackingField_11() const { return ___U3CDataU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_11() { return &___U3CDataU3Ek__BackingField_11; }
	inline void set_U3CDataU3Ek__BackingField_11(String_t* value)
	{
		___U3CDataU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GetTitleRequest_t180924238, ___U3CCallbackU3Ek__BackingField_12)); }
	inline OnGetTitle_t1839253948 * get_U3CCallbackU3Ek__BackingField_12() const { return ___U3CCallbackU3Ek__BackingField_12; }
	inline OnGetTitle_t1839253948 ** get_address_of_U3CCallbackU3Ek__BackingField_12() { return &___U3CCallbackU3Ek__BackingField_12; }
	inline void set_U3CCallbackU3Ek__BackingField_12(OnGetTitle_t1839253948 * value)
	{
		___U3CCallbackU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETTITLEREQUEST_T180924238_H
#ifndef GETTEXTREQUEST_T1378117363_H
#define GETTEXTREQUEST_T1378117363_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/GetTextRequest
struct  GetTextRequest_t1378117363  : public Request_t466816980
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/GetTextRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_11;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/OnGetText IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/GetTextRequest::<Callback>k__BackingField
	OnGetText_t3268193843 * ___U3CCallbackU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetTextRequest_t1378117363, ___U3CDataU3Ek__BackingField_11)); }
	inline String_t* get_U3CDataU3Ek__BackingField_11() const { return ___U3CDataU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_11() { return &___U3CDataU3Ek__BackingField_11; }
	inline void set_U3CDataU3Ek__BackingField_11(String_t* value)
	{
		___U3CDataU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GetTextRequest_t1378117363, ___U3CCallbackU3Ek__BackingField_12)); }
	inline OnGetText_t3268193843 * get_U3CCallbackU3Ek__BackingField_12() const { return ___U3CCallbackU3Ek__BackingField_12; }
	inline OnGetText_t3268193843 ** get_address_of_U3CCallbackU3Ek__BackingField_12() { return &___U3CCallbackU3Ek__BackingField_12; }
	inline void set_U3CCallbackU3Ek__BackingField_12(OnGetText_t3268193843 * value)
	{
		___U3CCallbackU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETTEXTREQUEST_T1378117363_H
#ifndef GETTARGETEDSENTIMENTREQUEST_T3429846097_H
#define GETTARGETEDSENTIMENTREQUEST_T3429846097_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/GetTargetedSentimentRequest
struct  GetTargetedSentimentRequest_t3429846097  : public Request_t466816980
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/GetTargetedSentimentRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_11;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/OnGetTargetedSentiment IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/GetTargetedSentimentRequest::<Callback>k__BackingField
	OnGetTargetedSentiment_t2739505381 * ___U3CCallbackU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetTargetedSentimentRequest_t3429846097, ___U3CDataU3Ek__BackingField_11)); }
	inline String_t* get_U3CDataU3Ek__BackingField_11() const { return ___U3CDataU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_11() { return &___U3CDataU3Ek__BackingField_11; }
	inline void set_U3CDataU3Ek__BackingField_11(String_t* value)
	{
		___U3CDataU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GetTargetedSentimentRequest_t3429846097, ___U3CCallbackU3Ek__BackingField_12)); }
	inline OnGetTargetedSentiment_t2739505381 * get_U3CCallbackU3Ek__BackingField_12() const { return ___U3CCallbackU3Ek__BackingField_12; }
	inline OnGetTargetedSentiment_t2739505381 ** get_address_of_U3CCallbackU3Ek__BackingField_12() { return &___U3CCallbackU3Ek__BackingField_12; }
	inline void set_U3CCallbackU3Ek__BackingField_12(OnGetTargetedSentiment_t2739505381 * value)
	{
		___U3CCallbackU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETTARGETEDSENTIMENTREQUEST_T3429846097_H
#ifndef GETRANKEDTAXOMOMYREQUEST_T2472232113_H
#define GETRANKEDTAXOMOMYREQUEST_T2472232113_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/GetRankedTaxomomyRequest
struct  GetRankedTaxomomyRequest_t2472232113  : public Request_t466816980
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/GetRankedTaxomomyRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_11;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/OnGetRankedTaxonomy IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/GetRankedTaxomomyRequest::<Callback>k__BackingField
	OnGetRankedTaxonomy_t935340330 * ___U3CCallbackU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetRankedTaxomomyRequest_t2472232113, ___U3CDataU3Ek__BackingField_11)); }
	inline String_t* get_U3CDataU3Ek__BackingField_11() const { return ___U3CDataU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_11() { return &___U3CDataU3Ek__BackingField_11; }
	inline void set_U3CDataU3Ek__BackingField_11(String_t* value)
	{
		___U3CDataU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GetRankedTaxomomyRequest_t2472232113, ___U3CCallbackU3Ek__BackingField_12)); }
	inline OnGetRankedTaxonomy_t935340330 * get_U3CCallbackU3Ek__BackingField_12() const { return ___U3CCallbackU3Ek__BackingField_12; }
	inline OnGetRankedTaxonomy_t935340330 ** get_address_of_U3CCallbackU3Ek__BackingField_12() { return &___U3CCallbackU3Ek__BackingField_12; }
	inline void set_U3CCallbackU3Ek__BackingField_12(OnGetRankedTaxonomy_t935340330 * value)
	{
		___U3CCallbackU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRANKEDTAXOMOMYREQUEST_T2472232113_H
#ifndef GETRANKEDCONCEPTSREQUEST_T3286662032_H
#define GETRANKEDCONCEPTSREQUEST_T3286662032_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/GetRankedConceptsRequest
struct  GetRankedConceptsRequest_t3286662032  : public Request_t466816980
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/GetRankedConceptsRequest::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_11;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/OnGetRankedConcepts IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/GetRankedConceptsRequest::<Callback>k__BackingField
	OnGetRankedConcepts_t3530605856 * ___U3CCallbackU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetRankedConceptsRequest_t3286662032, ___U3CDataU3Ek__BackingField_11)); }
	inline String_t* get_U3CDataU3Ek__BackingField_11() const { return ___U3CDataU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_11() { return &___U3CDataU3Ek__BackingField_11; }
	inline void set_U3CDataU3Ek__BackingField_11(String_t* value)
	{
		___U3CDataU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GetRankedConceptsRequest_t3286662032, ___U3CCallbackU3Ek__BackingField_12)); }
	inline OnGetRankedConcepts_t3530605856 * get_U3CCallbackU3Ek__BackingField_12() const { return ___U3CCallbackU3Ek__BackingField_12; }
	inline OnGetRankedConcepts_t3530605856 ** get_address_of_U3CCallbackU3Ek__BackingField_12() { return &___U3CCallbackU3Ek__BackingField_12; }
	inline void set_U3CCallbackU3Ek__BackingField_12(OnGetRankedConcepts_t3530605856 * value)
	{
		___U3CCallbackU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRANKEDCONCEPTSREQUEST_T3286662032_H
#ifndef DELEGATE_T3022476291_H
#define DELEGATE_T3022476291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t3022476291  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	IntPtr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	IntPtr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	IntPtr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	IntPtr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1572802995 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___invoke_impl_1)); }
	inline IntPtr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline IntPtr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(IntPtr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_3)); }
	inline IntPtr_t get_method_3() const { return ___method_3; }
	inline IntPtr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(IntPtr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___delegate_trampoline_4)); }
	inline IntPtr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline IntPtr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(IntPtr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_code_5)); }
	inline IntPtr_t get_method_code_5() const { return ___method_code_5; }
	inline IntPtr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(IntPtr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___data_8)); }
	inline DelegateData_t1572802995 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1572802995 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1572802995 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T3022476291_H
#ifndef DATETIMEKIND_T2186819611_H
#define DATETIMEKIND_T2186819611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeKind
struct  DateTimeKind_t2186819611 
{
public:
	// System.Int32 System.DateTimeKind::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeKind_t2186819611, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEKIND_T2186819611_H
#ifndef CONNECTIONSTATE_T3540963536_H
#define CONNECTIONSTATE_T3540963536_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Connection.WSConnector/ConnectionState
struct  ConnectionState_t3540963536 
{
public:
	// System.Int32 IBM.Watson.DeveloperCloud.Connection.WSConnector/ConnectionState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ConnectionState_t3540963536, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTIONSTATE_T3540963536_H
#ifndef OBJECT_T1021602117_H
#define OBJECT_T1021602117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1021602117  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	IntPtr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1021602117, ___m_CachedPtr_0)); }
	inline IntPtr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline IntPtr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(IntPtr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1021602117_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1021602117_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1021602117_H
#ifndef LOGLEVEL_T3918900206_H
#define LOGLEVEL_T3918900206_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Logging.LogLevel
struct  LogLevel_t3918900206 
{
public:
	// System.Int32 IBM.Watson.DeveloperCloud.Logging.LogLevel::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LogLevel_t3918900206, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGLEVEL_T3918900206_H
#ifndef ENTITYSUBTYPE_T2472813267_H
#define ENTITYSUBTYPE_T2472813267_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.EntitySubType
struct  EntitySubType_t2472813267 
{
public:
	// System.Int32 IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.EntitySubType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EntitySubType_t2472813267, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTITYSUBTYPE_T2472813267_H
#ifndef ENTITYPRIMARYTYPE_T4275181819_H
#define ENTITYPRIMARYTYPE_T4275181819_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.EntityPrimaryType
struct  EntityPrimaryType_t4275181819 
{
public:
	// System.Int32 IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.EntityPrimaryType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EntityPrimaryType_t4275181819, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTITYPRIMARYTYPE_T4275181819_H
#ifndef MULTICASTDELEGATE_T3201952435_H
#define MULTICASTDELEGATE_T3201952435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t3201952435  : public Delegate_t3022476291
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t3201952435 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t3201952435 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___prev_9)); }
	inline MulticastDelegate_t3201952435 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t3201952435 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t3201952435 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___kpm_next_10)); }
	inline MulticastDelegate_t3201952435 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t3201952435 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t3201952435 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T3201952435_H
#ifndef WSCONNECTOR_T2552241039_H
#define WSCONNECTOR_T2552241039_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Connection.WSConnector
struct  WSConnector_t2552241039  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Connection.WSConnector/ConnectorEvent IBM.Watson.DeveloperCloud.Connection.WSConnector::<OnClose>k__BackingField
	ConnectorEvent_t1029302242 * ___U3COnCloseU3Ek__BackingField_0;
	// IBM.Watson.DeveloperCloud.Connection.WSConnector/MessageEvent IBM.Watson.DeveloperCloud.Connection.WSConnector::<OnMessage>k__BackingField
	MessageEvent_t2276855538 * ___U3COnMessageU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Connection.WSConnector::<URL>k__BackingField
	String_t* ___U3CURLU3Ek__BackingField_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> IBM.Watson.DeveloperCloud.Connection.WSConnector::<Headers>k__BackingField
	Dictionary_2_t3943999495 * ___U3CHeadersU3Ek__BackingField_3;
	// IBM.Watson.DeveloperCloud.Utilities.Credentials IBM.Watson.DeveloperCloud.Connection.WSConnector::<Authentication>k__BackingField
	Credentials_t1494051450 * ___U3CAuthenticationU3Ek__BackingField_4;
	// IBM.Watson.DeveloperCloud.Connection.WSConnector/ConnectionState IBM.Watson.DeveloperCloud.Connection.WSConnector::m_ConnectionState
	int32_t ___m_ConnectionState_5;
	// System.Threading.Thread IBM.Watson.DeveloperCloud.Connection.WSConnector::m_SendThread
	Thread_t241561612 * ___m_SendThread_6;
	// System.Threading.AutoResetEvent IBM.Watson.DeveloperCloud.Connection.WSConnector::m_SendEvent
	AutoResetEvent_t15112628 * ___m_SendEvent_7;
	// System.Collections.Generic.Queue`1<IBM.Watson.DeveloperCloud.Connection.WSConnector/Message> IBM.Watson.DeveloperCloud.Connection.WSConnector::m_SendQueue
	Queue_1_t2334678625 * ___m_SendQueue_8;
	// System.Threading.AutoResetEvent IBM.Watson.DeveloperCloud.Connection.WSConnector::m_ReceiveEvent
	AutoResetEvent_t15112628 * ___m_ReceiveEvent_9;
	// System.Collections.Generic.Queue`1<IBM.Watson.DeveloperCloud.Connection.WSConnector/Message> IBM.Watson.DeveloperCloud.Connection.WSConnector::m_ReceiveQueue
	Queue_1_t2334678625 * ___m_ReceiveQueue_10;
	// System.Int32 IBM.Watson.DeveloperCloud.Connection.WSConnector::m_ReceiverRoutine
	int32_t ___m_ReceiverRoutine_11;

public:
	inline static int32_t get_offset_of_U3COnCloseU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(WSConnector_t2552241039, ___U3COnCloseU3Ek__BackingField_0)); }
	inline ConnectorEvent_t1029302242 * get_U3COnCloseU3Ek__BackingField_0() const { return ___U3COnCloseU3Ek__BackingField_0; }
	inline ConnectorEvent_t1029302242 ** get_address_of_U3COnCloseU3Ek__BackingField_0() { return &___U3COnCloseU3Ek__BackingField_0; }
	inline void set_U3COnCloseU3Ek__BackingField_0(ConnectorEvent_t1029302242 * value)
	{
		___U3COnCloseU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3COnCloseU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3COnMessageU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(WSConnector_t2552241039, ___U3COnMessageU3Ek__BackingField_1)); }
	inline MessageEvent_t2276855538 * get_U3COnMessageU3Ek__BackingField_1() const { return ___U3COnMessageU3Ek__BackingField_1; }
	inline MessageEvent_t2276855538 ** get_address_of_U3COnMessageU3Ek__BackingField_1() { return &___U3COnMessageU3Ek__BackingField_1; }
	inline void set_U3COnMessageU3Ek__BackingField_1(MessageEvent_t2276855538 * value)
	{
		___U3COnMessageU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3COnMessageU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CURLU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(WSConnector_t2552241039, ___U3CURLU3Ek__BackingField_2)); }
	inline String_t* get_U3CURLU3Ek__BackingField_2() const { return ___U3CURLU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CURLU3Ek__BackingField_2() { return &___U3CURLU3Ek__BackingField_2; }
	inline void set_U3CURLU3Ek__BackingField_2(String_t* value)
	{
		___U3CURLU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CURLU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CHeadersU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(WSConnector_t2552241039, ___U3CHeadersU3Ek__BackingField_3)); }
	inline Dictionary_2_t3943999495 * get_U3CHeadersU3Ek__BackingField_3() const { return ___U3CHeadersU3Ek__BackingField_3; }
	inline Dictionary_2_t3943999495 ** get_address_of_U3CHeadersU3Ek__BackingField_3() { return &___U3CHeadersU3Ek__BackingField_3; }
	inline void set_U3CHeadersU3Ek__BackingField_3(Dictionary_2_t3943999495 * value)
	{
		___U3CHeadersU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CHeadersU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CAuthenticationU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(WSConnector_t2552241039, ___U3CAuthenticationU3Ek__BackingField_4)); }
	inline Credentials_t1494051450 * get_U3CAuthenticationU3Ek__BackingField_4() const { return ___U3CAuthenticationU3Ek__BackingField_4; }
	inline Credentials_t1494051450 ** get_address_of_U3CAuthenticationU3Ek__BackingField_4() { return &___U3CAuthenticationU3Ek__BackingField_4; }
	inline void set_U3CAuthenticationU3Ek__BackingField_4(Credentials_t1494051450 * value)
	{
		___U3CAuthenticationU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAuthenticationU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_m_ConnectionState_5() { return static_cast<int32_t>(offsetof(WSConnector_t2552241039, ___m_ConnectionState_5)); }
	inline int32_t get_m_ConnectionState_5() const { return ___m_ConnectionState_5; }
	inline int32_t* get_address_of_m_ConnectionState_5() { return &___m_ConnectionState_5; }
	inline void set_m_ConnectionState_5(int32_t value)
	{
		___m_ConnectionState_5 = value;
	}

	inline static int32_t get_offset_of_m_SendThread_6() { return static_cast<int32_t>(offsetof(WSConnector_t2552241039, ___m_SendThread_6)); }
	inline Thread_t241561612 * get_m_SendThread_6() const { return ___m_SendThread_6; }
	inline Thread_t241561612 ** get_address_of_m_SendThread_6() { return &___m_SendThread_6; }
	inline void set_m_SendThread_6(Thread_t241561612 * value)
	{
		___m_SendThread_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_SendThread_6), value);
	}

	inline static int32_t get_offset_of_m_SendEvent_7() { return static_cast<int32_t>(offsetof(WSConnector_t2552241039, ___m_SendEvent_7)); }
	inline AutoResetEvent_t15112628 * get_m_SendEvent_7() const { return ___m_SendEvent_7; }
	inline AutoResetEvent_t15112628 ** get_address_of_m_SendEvent_7() { return &___m_SendEvent_7; }
	inline void set_m_SendEvent_7(AutoResetEvent_t15112628 * value)
	{
		___m_SendEvent_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_SendEvent_7), value);
	}

	inline static int32_t get_offset_of_m_SendQueue_8() { return static_cast<int32_t>(offsetof(WSConnector_t2552241039, ___m_SendQueue_8)); }
	inline Queue_1_t2334678625 * get_m_SendQueue_8() const { return ___m_SendQueue_8; }
	inline Queue_1_t2334678625 ** get_address_of_m_SendQueue_8() { return &___m_SendQueue_8; }
	inline void set_m_SendQueue_8(Queue_1_t2334678625 * value)
	{
		___m_SendQueue_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_SendQueue_8), value);
	}

	inline static int32_t get_offset_of_m_ReceiveEvent_9() { return static_cast<int32_t>(offsetof(WSConnector_t2552241039, ___m_ReceiveEvent_9)); }
	inline AutoResetEvent_t15112628 * get_m_ReceiveEvent_9() const { return ___m_ReceiveEvent_9; }
	inline AutoResetEvent_t15112628 ** get_address_of_m_ReceiveEvent_9() { return &___m_ReceiveEvent_9; }
	inline void set_m_ReceiveEvent_9(AutoResetEvent_t15112628 * value)
	{
		___m_ReceiveEvent_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_ReceiveEvent_9), value);
	}

	inline static int32_t get_offset_of_m_ReceiveQueue_10() { return static_cast<int32_t>(offsetof(WSConnector_t2552241039, ___m_ReceiveQueue_10)); }
	inline Queue_1_t2334678625 * get_m_ReceiveQueue_10() const { return ___m_ReceiveQueue_10; }
	inline Queue_1_t2334678625 ** get_address_of_m_ReceiveQueue_10() { return &___m_ReceiveQueue_10; }
	inline void set_m_ReceiveQueue_10(Queue_1_t2334678625 * value)
	{
		___m_ReceiveQueue_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_ReceiveQueue_10), value);
	}

	inline static int32_t get_offset_of_m_ReceiverRoutine_11() { return static_cast<int32_t>(offsetof(WSConnector_t2552241039, ___m_ReceiverRoutine_11)); }
	inline int32_t get_m_ReceiverRoutine_11() const { return ___m_ReceiverRoutine_11; }
	inline int32_t* get_address_of_m_ReceiverRoutine_11() { return &___m_ReceiverRoutine_11; }
	inline void set_m_ReceiverRoutine_11(int32_t value)
	{
		___m_ReceiverRoutine_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WSCONNECTOR_T2552241039_H
#ifndef FILEREACTOR_T4232122414_H
#define FILEREACTOR_T4232122414_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Logging.FileReactor
struct  FileReactor_t4232122414  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Logging.FileReactor::<LogFile>k__BackingField
	String_t* ___U3CLogFileU3Ek__BackingField_0;
	// IBM.Watson.DeveloperCloud.Logging.LogLevel IBM.Watson.DeveloperCloud.Logging.FileReactor::<Level>k__BackingField
	int32_t ___U3CLevelU3Ek__BackingField_1;
	// System.Int32 IBM.Watson.DeveloperCloud.Logging.FileReactor::<LogHistory>k__BackingField
	int32_t ___U3CLogHistoryU3Ek__BackingField_2;
	// System.Int32 IBM.Watson.DeveloperCloud.Logging.FileReactor::<MaxLogSize>k__BackingField
	int32_t ___U3CMaxLogSizeU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CLogFileU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(FileReactor_t4232122414, ___U3CLogFileU3Ek__BackingField_0)); }
	inline String_t* get_U3CLogFileU3Ek__BackingField_0() const { return ___U3CLogFileU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CLogFileU3Ek__BackingField_0() { return &___U3CLogFileU3Ek__BackingField_0; }
	inline void set_U3CLogFileU3Ek__BackingField_0(String_t* value)
	{
		___U3CLogFileU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLogFileU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CLevelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(FileReactor_t4232122414, ___U3CLevelU3Ek__BackingField_1)); }
	inline int32_t get_U3CLevelU3Ek__BackingField_1() const { return ___U3CLevelU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CLevelU3Ek__BackingField_1() { return &___U3CLevelU3Ek__BackingField_1; }
	inline void set_U3CLevelU3Ek__BackingField_1(int32_t value)
	{
		___U3CLevelU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CLogHistoryU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(FileReactor_t4232122414, ___U3CLogHistoryU3Ek__BackingField_2)); }
	inline int32_t get_U3CLogHistoryU3Ek__BackingField_2() const { return ___U3CLogHistoryU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CLogHistoryU3Ek__BackingField_2() { return &___U3CLogHistoryU3Ek__BackingField_2; }
	inline void set_U3CLogHistoryU3Ek__BackingField_2(int32_t value)
	{
		___U3CLogHistoryU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CMaxLogSizeU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(FileReactor_t4232122414, ___U3CMaxLogSizeU3Ek__BackingField_3)); }
	inline int32_t get_U3CMaxLogSizeU3Ek__BackingField_3() const { return ___U3CMaxLogSizeU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CMaxLogSizeU3Ek__BackingField_3() { return &___U3CMaxLogSizeU3Ek__BackingField_3; }
	inline void set_U3CMaxLogSizeU3Ek__BackingField_3(int32_t value)
	{
		___U3CMaxLogSizeU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILEREACTOR_T4232122414_H
#ifndef COMPONENT_T3819376471_H
#define COMPONENT_T3819376471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t3819376471  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T3819376471_H
#ifndef DEBUGREACTOR_T558814947_H
#define DEBUGREACTOR_T558814947_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Debug.DebugReactor
struct  DebugReactor_t558814947  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Logging.LogLevel IBM.Watson.DeveloperCloud.Debug.DebugReactor::<Level>k__BackingField
	int32_t ___U3CLevelU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CLevelU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DebugReactor_t558814947, ___U3CLevelU3Ek__BackingField_0)); }
	inline int32_t get_U3CLevelU3Ek__BackingField_0() const { return ___U3CLevelU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CLevelU3Ek__BackingField_0() { return &___U3CLevelU3Ek__BackingField_0; }
	inline void set_U3CLevelU3Ek__BackingField_0(int32_t value)
	{
		___U3CLevelU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGREACTOR_T558814947_H
#ifndef DATETIME_T693205669_H
#define DATETIME_T693205669_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t693205669 
{
public:
	// System.TimeSpan System.DateTime::ticks
	TimeSpan_t3430258949  ___ticks_10;
	// System.DateTimeKind System.DateTime::kind
	int32_t ___kind_11;

public:
	inline static int32_t get_offset_of_ticks_10() { return static_cast<int32_t>(offsetof(DateTime_t693205669, ___ticks_10)); }
	inline TimeSpan_t3430258949  get_ticks_10() const { return ___ticks_10; }
	inline TimeSpan_t3430258949 * get_address_of_ticks_10() { return &___ticks_10; }
	inline void set_ticks_10(TimeSpan_t3430258949  value)
	{
		___ticks_10 = value;
	}

	inline static int32_t get_offset_of_kind_11() { return static_cast<int32_t>(offsetof(DateTime_t693205669, ___kind_11)); }
	inline int32_t get_kind_11() const { return ___kind_11; }
	inline int32_t* get_address_of_kind_11() { return &___kind_11; }
	inline void set_kind_11(int32_t value)
	{
		___kind_11 = value;
	}
};

struct DateTime_t693205669_StaticFields
{
public:
	// System.DateTime System.DateTime::MaxValue
	DateTime_t693205669  ___MaxValue_12;
	// System.DateTime System.DateTime::MinValue
	DateTime_t693205669  ___MinValue_13;
	// System.String[] System.DateTime::ParseTimeFormats
	StringU5BU5D_t1642385972* ___ParseTimeFormats_14;
	// System.String[] System.DateTime::ParseYearDayMonthFormats
	StringU5BU5D_t1642385972* ___ParseYearDayMonthFormats_15;
	// System.String[] System.DateTime::ParseYearMonthDayFormats
	StringU5BU5D_t1642385972* ___ParseYearMonthDayFormats_16;
	// System.String[] System.DateTime::ParseDayMonthYearFormats
	StringU5BU5D_t1642385972* ___ParseDayMonthYearFormats_17;
	// System.String[] System.DateTime::ParseMonthDayYearFormats
	StringU5BU5D_t1642385972* ___ParseMonthDayYearFormats_18;
	// System.String[] System.DateTime::MonthDayShortFormats
	StringU5BU5D_t1642385972* ___MonthDayShortFormats_19;
	// System.String[] System.DateTime::DayMonthShortFormats
	StringU5BU5D_t1642385972* ___DayMonthShortFormats_20;
	// System.Int32[] System.DateTime::daysmonth
	Int32U5BU5D_t3030399641* ___daysmonth_21;
	// System.Int32[] System.DateTime::daysmonthleap
	Int32U5BU5D_t3030399641* ___daysmonthleap_22;
	// System.Object System.DateTime::to_local_time_span_object
	RuntimeObject * ___to_local_time_span_object_23;
	// System.Int64 System.DateTime::last_now
	int64_t ___last_now_24;

public:
	inline static int32_t get_offset_of_MaxValue_12() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___MaxValue_12)); }
	inline DateTime_t693205669  get_MaxValue_12() const { return ___MaxValue_12; }
	inline DateTime_t693205669 * get_address_of_MaxValue_12() { return &___MaxValue_12; }
	inline void set_MaxValue_12(DateTime_t693205669  value)
	{
		___MaxValue_12 = value;
	}

	inline static int32_t get_offset_of_MinValue_13() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___MinValue_13)); }
	inline DateTime_t693205669  get_MinValue_13() const { return ___MinValue_13; }
	inline DateTime_t693205669 * get_address_of_MinValue_13() { return &___MinValue_13; }
	inline void set_MinValue_13(DateTime_t693205669  value)
	{
		___MinValue_13 = value;
	}

	inline static int32_t get_offset_of_ParseTimeFormats_14() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseTimeFormats_14)); }
	inline StringU5BU5D_t1642385972* get_ParseTimeFormats_14() const { return ___ParseTimeFormats_14; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseTimeFormats_14() { return &___ParseTimeFormats_14; }
	inline void set_ParseTimeFormats_14(StringU5BU5D_t1642385972* value)
	{
		___ParseTimeFormats_14 = value;
		Il2CppCodeGenWriteBarrier((&___ParseTimeFormats_14), value);
	}

	inline static int32_t get_offset_of_ParseYearDayMonthFormats_15() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseYearDayMonthFormats_15)); }
	inline StringU5BU5D_t1642385972* get_ParseYearDayMonthFormats_15() const { return ___ParseYearDayMonthFormats_15; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseYearDayMonthFormats_15() { return &___ParseYearDayMonthFormats_15; }
	inline void set_ParseYearDayMonthFormats_15(StringU5BU5D_t1642385972* value)
	{
		___ParseYearDayMonthFormats_15 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearDayMonthFormats_15), value);
	}

	inline static int32_t get_offset_of_ParseYearMonthDayFormats_16() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseYearMonthDayFormats_16)); }
	inline StringU5BU5D_t1642385972* get_ParseYearMonthDayFormats_16() const { return ___ParseYearMonthDayFormats_16; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseYearMonthDayFormats_16() { return &___ParseYearMonthDayFormats_16; }
	inline void set_ParseYearMonthDayFormats_16(StringU5BU5D_t1642385972* value)
	{
		___ParseYearMonthDayFormats_16 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearMonthDayFormats_16), value);
	}

	inline static int32_t get_offset_of_ParseDayMonthYearFormats_17() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseDayMonthYearFormats_17)); }
	inline StringU5BU5D_t1642385972* get_ParseDayMonthYearFormats_17() const { return ___ParseDayMonthYearFormats_17; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseDayMonthYearFormats_17() { return &___ParseDayMonthYearFormats_17; }
	inline void set_ParseDayMonthYearFormats_17(StringU5BU5D_t1642385972* value)
	{
		___ParseDayMonthYearFormats_17 = value;
		Il2CppCodeGenWriteBarrier((&___ParseDayMonthYearFormats_17), value);
	}

	inline static int32_t get_offset_of_ParseMonthDayYearFormats_18() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseMonthDayYearFormats_18)); }
	inline StringU5BU5D_t1642385972* get_ParseMonthDayYearFormats_18() const { return ___ParseMonthDayYearFormats_18; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseMonthDayYearFormats_18() { return &___ParseMonthDayYearFormats_18; }
	inline void set_ParseMonthDayYearFormats_18(StringU5BU5D_t1642385972* value)
	{
		___ParseMonthDayYearFormats_18 = value;
		Il2CppCodeGenWriteBarrier((&___ParseMonthDayYearFormats_18), value);
	}

	inline static int32_t get_offset_of_MonthDayShortFormats_19() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___MonthDayShortFormats_19)); }
	inline StringU5BU5D_t1642385972* get_MonthDayShortFormats_19() const { return ___MonthDayShortFormats_19; }
	inline StringU5BU5D_t1642385972** get_address_of_MonthDayShortFormats_19() { return &___MonthDayShortFormats_19; }
	inline void set_MonthDayShortFormats_19(StringU5BU5D_t1642385972* value)
	{
		___MonthDayShortFormats_19 = value;
		Il2CppCodeGenWriteBarrier((&___MonthDayShortFormats_19), value);
	}

	inline static int32_t get_offset_of_DayMonthShortFormats_20() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___DayMonthShortFormats_20)); }
	inline StringU5BU5D_t1642385972* get_DayMonthShortFormats_20() const { return ___DayMonthShortFormats_20; }
	inline StringU5BU5D_t1642385972** get_address_of_DayMonthShortFormats_20() { return &___DayMonthShortFormats_20; }
	inline void set_DayMonthShortFormats_20(StringU5BU5D_t1642385972* value)
	{
		___DayMonthShortFormats_20 = value;
		Il2CppCodeGenWriteBarrier((&___DayMonthShortFormats_20), value);
	}

	inline static int32_t get_offset_of_daysmonth_21() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___daysmonth_21)); }
	inline Int32U5BU5D_t3030399641* get_daysmonth_21() const { return ___daysmonth_21; }
	inline Int32U5BU5D_t3030399641** get_address_of_daysmonth_21() { return &___daysmonth_21; }
	inline void set_daysmonth_21(Int32U5BU5D_t3030399641* value)
	{
		___daysmonth_21 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonth_21), value);
	}

	inline static int32_t get_offset_of_daysmonthleap_22() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___daysmonthleap_22)); }
	inline Int32U5BU5D_t3030399641* get_daysmonthleap_22() const { return ___daysmonthleap_22; }
	inline Int32U5BU5D_t3030399641** get_address_of_daysmonthleap_22() { return &___daysmonthleap_22; }
	inline void set_daysmonthleap_22(Int32U5BU5D_t3030399641* value)
	{
		___daysmonthleap_22 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonthleap_22), value);
	}

	inline static int32_t get_offset_of_to_local_time_span_object_23() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___to_local_time_span_object_23)); }
	inline RuntimeObject * get_to_local_time_span_object_23() const { return ___to_local_time_span_object_23; }
	inline RuntimeObject ** get_address_of_to_local_time_span_object_23() { return &___to_local_time_span_object_23; }
	inline void set_to_local_time_span_object_23(RuntimeObject * value)
	{
		___to_local_time_span_object_23 = value;
		Il2CppCodeGenWriteBarrier((&___to_local_time_span_object_23), value);
	}

	inline static int32_t get_offset_of_last_now_24() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___last_now_24)); }
	inline int64_t get_last_now_24() const { return ___last_now_24; }
	inline int64_t* get_address_of_last_now_24() { return &___last_now_24; }
	inline void set_last_now_24(int64_t value)
	{
		___last_now_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T693205669_H
#ifndef ENTITY_T2757698109_H
#define ENTITY_T2757698109_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Entity
struct  Entity_t2757698109  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Entity::<type>k__BackingField
	String_t* ___U3CtypeU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Entity::<relevance>k__BackingField
	String_t* ___U3CrelevanceU3Ek__BackingField_1;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.KnowledgeGraph IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Entity::<knowledgeGraph>k__BackingField
	KnowledgeGraph_t3341597436 * ___U3CknowledgeGraphU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Entity::<count>k__BackingField
	String_t* ___U3CcountU3Ek__BackingField_3;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Entity::<text>k__BackingField
	String_t* ___U3CtextU3Ek__BackingField_4;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Disambiguated IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Entity::<disambiguated>k__BackingField
	Disambiguated_t227057183 * ___U3CdisambiguatedU3Ek__BackingField_5;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Quotation[] IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Entity::<quotations>k__BackingField
	QuotationU5BU5D_t3047261617* ___U3CquotationsU3Ek__BackingField_6;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.DocSentiment IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Entity::<sentiment>k__BackingField
	DocSentiment_t2354958393 * ___U3CsentimentU3Ek__BackingField_7;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.EntityPrimaryType IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Entity::_EntityType
	int32_t ____EntityType_8;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.PositionOnMap IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Entity::_GeoLocation
	PositionOnMap_t66846110 * ____GeoLocation_9;

public:
	inline static int32_t get_offset_of_U3CtypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Entity_t2757698109, ___U3CtypeU3Ek__BackingField_0)); }
	inline String_t* get_U3CtypeU3Ek__BackingField_0() const { return ___U3CtypeU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CtypeU3Ek__BackingField_0() { return &___U3CtypeU3Ek__BackingField_0; }
	inline void set_U3CtypeU3Ek__BackingField_0(String_t* value)
	{
		___U3CtypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtypeU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CrelevanceU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Entity_t2757698109, ___U3CrelevanceU3Ek__BackingField_1)); }
	inline String_t* get_U3CrelevanceU3Ek__BackingField_1() const { return ___U3CrelevanceU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CrelevanceU3Ek__BackingField_1() { return &___U3CrelevanceU3Ek__BackingField_1; }
	inline void set_U3CrelevanceU3Ek__BackingField_1(String_t* value)
	{
		___U3CrelevanceU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrelevanceU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CknowledgeGraphU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Entity_t2757698109, ___U3CknowledgeGraphU3Ek__BackingField_2)); }
	inline KnowledgeGraph_t3341597436 * get_U3CknowledgeGraphU3Ek__BackingField_2() const { return ___U3CknowledgeGraphU3Ek__BackingField_2; }
	inline KnowledgeGraph_t3341597436 ** get_address_of_U3CknowledgeGraphU3Ek__BackingField_2() { return &___U3CknowledgeGraphU3Ek__BackingField_2; }
	inline void set_U3CknowledgeGraphU3Ek__BackingField_2(KnowledgeGraph_t3341597436 * value)
	{
		___U3CknowledgeGraphU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CknowledgeGraphU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CcountU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Entity_t2757698109, ___U3CcountU3Ek__BackingField_3)); }
	inline String_t* get_U3CcountU3Ek__BackingField_3() const { return ___U3CcountU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CcountU3Ek__BackingField_3() { return &___U3CcountU3Ek__BackingField_3; }
	inline void set_U3CcountU3Ek__BackingField_3(String_t* value)
	{
		___U3CcountU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcountU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CtextU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Entity_t2757698109, ___U3CtextU3Ek__BackingField_4)); }
	inline String_t* get_U3CtextU3Ek__BackingField_4() const { return ___U3CtextU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CtextU3Ek__BackingField_4() { return &___U3CtextU3Ek__BackingField_4; }
	inline void set_U3CtextU3Ek__BackingField_4(String_t* value)
	{
		___U3CtextU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CdisambiguatedU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Entity_t2757698109, ___U3CdisambiguatedU3Ek__BackingField_5)); }
	inline Disambiguated_t227057183 * get_U3CdisambiguatedU3Ek__BackingField_5() const { return ___U3CdisambiguatedU3Ek__BackingField_5; }
	inline Disambiguated_t227057183 ** get_address_of_U3CdisambiguatedU3Ek__BackingField_5() { return &___U3CdisambiguatedU3Ek__BackingField_5; }
	inline void set_U3CdisambiguatedU3Ek__BackingField_5(Disambiguated_t227057183 * value)
	{
		___U3CdisambiguatedU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdisambiguatedU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CquotationsU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Entity_t2757698109, ___U3CquotationsU3Ek__BackingField_6)); }
	inline QuotationU5BU5D_t3047261617* get_U3CquotationsU3Ek__BackingField_6() const { return ___U3CquotationsU3Ek__BackingField_6; }
	inline QuotationU5BU5D_t3047261617** get_address_of_U3CquotationsU3Ek__BackingField_6() { return &___U3CquotationsU3Ek__BackingField_6; }
	inline void set_U3CquotationsU3Ek__BackingField_6(QuotationU5BU5D_t3047261617* value)
	{
		___U3CquotationsU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CquotationsU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CsentimentU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(Entity_t2757698109, ___U3CsentimentU3Ek__BackingField_7)); }
	inline DocSentiment_t2354958393 * get_U3CsentimentU3Ek__BackingField_7() const { return ___U3CsentimentU3Ek__BackingField_7; }
	inline DocSentiment_t2354958393 ** get_address_of_U3CsentimentU3Ek__BackingField_7() { return &___U3CsentimentU3Ek__BackingField_7; }
	inline void set_U3CsentimentU3Ek__BackingField_7(DocSentiment_t2354958393 * value)
	{
		___U3CsentimentU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsentimentU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of__EntityType_8() { return static_cast<int32_t>(offsetof(Entity_t2757698109, ____EntityType_8)); }
	inline int32_t get__EntityType_8() const { return ____EntityType_8; }
	inline int32_t* get_address_of__EntityType_8() { return &____EntityType_8; }
	inline void set__EntityType_8(int32_t value)
	{
		____EntityType_8 = value;
	}

	inline static int32_t get_offset_of__GeoLocation_9() { return static_cast<int32_t>(offsetof(Entity_t2757698109, ____GeoLocation_9)); }
	inline PositionOnMap_t66846110 * get__GeoLocation_9() const { return ____GeoLocation_9; }
	inline PositionOnMap_t66846110 ** get_address_of__GeoLocation_9() { return &____GeoLocation_9; }
	inline void set__GeoLocation_9(PositionOnMap_t66846110 * value)
	{
		____GeoLocation_9 = value;
		Il2CppCodeGenWriteBarrier((&____GeoLocation_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTITY_T2757698109_H
#ifndef ONGETCOMBINEDDATA_T4285430159_H
#define ONGETCOMBINEDDATA_T4285430159_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/OnGetCombinedData
struct  OnGetCombinedData_t4285430159  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONGETCOMBINEDDATA_T4285430159_H
#ifndef ONGETNEWS_T1755812277_H
#define ONGETNEWS_T1755812277_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/OnGetNews
struct  OnGetNews_t1755812277  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONGETNEWS_T1755812277_H
#ifndef ONGETTITLE_T1839253948_H
#define ONGETTITLE_T1839253948_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/OnGetTitle
struct  OnGetTitle_t1839253948  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONGETTITLE_T1839253948_H
#ifndef ONGETRANKEDTAXONOMY_T935340330_H
#define ONGETRANKEDTAXONOMY_T935340330_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/OnGetRankedTaxonomy
struct  OnGetRankedTaxonomy_t935340330  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONGETRANKEDTAXONOMY_T935340330_H
#ifndef ONGETTEXT_T3268193843_H
#define ONGETTEXT_T3268193843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/OnGetText
struct  OnGetText_t3268193843  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONGETTEXT_T3268193843_H
#ifndef GETDEBUGINFO_T2665501740_H
#define GETDEBUGINFO_T2665501740_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Debug.DebugConsole/GetDebugInfo
struct  GetDebugInfo_t2665501740  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETDEBUGINFO_T2665501740_H
#ifndef CONNECTOREVENT_T1029302242_H
#define CONNECTOREVENT_T1029302242_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Connection.WSConnector/ConnectorEvent
struct  ConnectorEvent_t1029302242  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTOREVENT_T1029302242_H
#ifndef ONGETEMOTIONS_T1524549668_H
#define ONGETEMOTIONS_T1524549668_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/OnGetEmotions
struct  OnGetEmotions_t1524549668  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONGETEMOTIONS_T1524549668_H
#ifndef MESSAGEEVENT_T2276855538_H
#define MESSAGEEVENT_T2276855538_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Connection.WSConnector/MessageEvent
struct  MessageEvent_t2276855538  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGEEVENT_T2276855538_H
#ifndef BEHAVIOUR_T955675639_H
#define BEHAVIOUR_T955675639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t955675639  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T955675639_H
#ifndef DATE_T2577429686_H
#define DATE_T2577429686_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Date
struct  Date_t2577429686  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Date::<date>k__BackingField
	String_t* ___U3CdateU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Date::<text>k__BackingField
	String_t* ___U3CtextU3Ek__BackingField_1;
	// System.DateTime IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Date::m_dateValue
	DateTime_t693205669  ___m_dateValue_2;

public:
	inline static int32_t get_offset_of_U3CdateU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Date_t2577429686, ___U3CdateU3Ek__BackingField_0)); }
	inline String_t* get_U3CdateU3Ek__BackingField_0() const { return ___U3CdateU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CdateU3Ek__BackingField_0() { return &___U3CdateU3Ek__BackingField_0; }
	inline void set_U3CdateU3Ek__BackingField_0(String_t* value)
	{
		___U3CdateU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdateU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CtextU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Date_t2577429686, ___U3CtextU3Ek__BackingField_1)); }
	inline String_t* get_U3CtextU3Ek__BackingField_1() const { return ___U3CtextU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CtextU3Ek__BackingField_1() { return &___U3CtextU3Ek__BackingField_1; }
	inline void set_U3CtextU3Ek__BackingField_1(String_t* value)
	{
		___U3CtextU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_m_dateValue_2() { return static_cast<int32_t>(offsetof(Date_t2577429686, ___m_dateValue_2)); }
	inline DateTime_t693205669  get_m_dateValue_2() const { return ___m_dateValue_2; }
	inline DateTime_t693205669 * get_address_of_m_dateValue_2() { return &___m_dateValue_2; }
	inline void set_m_dateValue_2(DateTime_t693205669  value)
	{
		___m_dateValue_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATE_T2577429686_H
#ifndef LOGRECORD_T1360968087_H
#define LOGRECORD_T1360968087_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Logging.LogRecord
struct  LogRecord_t1360968087  : public RuntimeObject
{
public:
	// System.DateTime IBM.Watson.DeveloperCloud.Logging.LogRecord::m_TimeStamp
	DateTime_t693205669  ___m_TimeStamp_0;
	// IBM.Watson.DeveloperCloud.Logging.LogLevel IBM.Watson.DeveloperCloud.Logging.LogRecord::m_Level
	int32_t ___m_Level_1;
	// System.String IBM.Watson.DeveloperCloud.Logging.LogRecord::m_SubSystem
	String_t* ___m_SubSystem_2;
	// System.String IBM.Watson.DeveloperCloud.Logging.LogRecord::m_Message
	String_t* ___m_Message_3;

public:
	inline static int32_t get_offset_of_m_TimeStamp_0() { return static_cast<int32_t>(offsetof(LogRecord_t1360968087, ___m_TimeStamp_0)); }
	inline DateTime_t693205669  get_m_TimeStamp_0() const { return ___m_TimeStamp_0; }
	inline DateTime_t693205669 * get_address_of_m_TimeStamp_0() { return &___m_TimeStamp_0; }
	inline void set_m_TimeStamp_0(DateTime_t693205669  value)
	{
		___m_TimeStamp_0 = value;
	}

	inline static int32_t get_offset_of_m_Level_1() { return static_cast<int32_t>(offsetof(LogRecord_t1360968087, ___m_Level_1)); }
	inline int32_t get_m_Level_1() const { return ___m_Level_1; }
	inline int32_t* get_address_of_m_Level_1() { return &___m_Level_1; }
	inline void set_m_Level_1(int32_t value)
	{
		___m_Level_1 = value;
	}

	inline static int32_t get_offset_of_m_SubSystem_2() { return static_cast<int32_t>(offsetof(LogRecord_t1360968087, ___m_SubSystem_2)); }
	inline String_t* get_m_SubSystem_2() const { return ___m_SubSystem_2; }
	inline String_t** get_address_of_m_SubSystem_2() { return &___m_SubSystem_2; }
	inline void set_m_SubSystem_2(String_t* value)
	{
		___m_SubSystem_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_SubSystem_2), value);
	}

	inline static int32_t get_offset_of_m_Message_3() { return static_cast<int32_t>(offsetof(LogRecord_t1360968087, ___m_Message_3)); }
	inline String_t* get_m_Message_3() const { return ___m_Message_3; }
	inline String_t** get_address_of_m_Message_3() { return &___m_Message_3; }
	inline void set_m_Message_3(String_t* value)
	{
		___m_Message_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Message_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGRECORD_T1360968087_H
#ifndef ONGETLANGUAGES_T3197075847_H
#define ONGETLANGUAGES_T3197075847_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/OnGetLanguages
struct  OnGetLanguages_t3197075847  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONGETLANGUAGES_T3197075847_H
#ifndef ONGETKEYWORDS_T1978903802_H
#define ONGETKEYWORDS_T1978903802_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/OnGetKeywords
struct  OnGetKeywords_t1978903802  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONGETKEYWORDS_T1978903802_H
#ifndef ONDETECTFEEDS_T4021436334_H
#define ONDETECTFEEDS_T4021436334_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/OnDetectFeeds
struct  OnDetectFeeds_t4021436334  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDETECTFEEDS_T4021436334_H
#ifndef ONGETMICROFORMATS_T327929786_H
#define ONGETMICROFORMATS_T327929786_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/OnGetMicroformats
struct  OnGetMicroformats_t327929786  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONGETMICROFORMATS_T327929786_H
#ifndef ONGETTEXTSENTIMENT_T3575883834_H
#define ONGETTEXTSENTIMENT_T3575883834_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/OnGetTextSentiment
struct  OnGetTextSentiment_t3575883834  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONGETTEXTSENTIMENT_T3575883834_H
#ifndef ONGETRELATIONS_T1280830431_H
#define ONGETRELATIONS_T1280830431_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/OnGetRelations
struct  OnGetRelations_t1280830431  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONGETRELATIONS_T1280830431_H
#ifndef ONGETPUBLICATIONDATE_T443403836_H
#define ONGETPUBLICATIONDATE_T443403836_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/OnGetPublicationDate
struct  OnGetPublicationDate_t443403836  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONGETPUBLICATIONDATE_T443403836_H
#ifndef ONGETAUTHORS_T505057758_H
#define ONGETAUTHORS_T505057758_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/OnGetAuthors
struct  OnGetAuthors_t505057758  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONGETAUTHORS_T505057758_H
#ifndef ONGETDATES_T91791237_H
#define ONGETDATES_T91791237_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/OnGetDates
struct  OnGetDates_t91791237  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONGETDATES_T91791237_H
#ifndef ONGETRANKEDCONCEPTS_T3530605856_H
#define ONGETRANKEDCONCEPTS_T3530605856_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/OnGetRankedConcepts
struct  OnGetRankedConcepts_t3530605856  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONGETRANKEDCONCEPTS_T3530605856_H
#ifndef ONGETENTITIES_T1909489667_H
#define ONGETENTITIES_T1909489667_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/OnGetEntities
struct  OnGetEntities_t1909489667  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONGETENTITIES_T1909489667_H
#ifndef ONGETTARGETEDSENTIMENT_T2739505381_H
#define ONGETTARGETEDSENTIMENT_T2739505381_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI/OnGetTargetedSentiment
struct  OnGetTargetedSentiment_t2739505381  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONGETTARGETEDSENTIMENT_T2739505381_H
#ifndef MONOBEHAVIOUR_T1158329972_H
#define MONOBEHAVIOUR_T1158329972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t1158329972  : public Behaviour_t955675639
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T1158329972_H
#ifndef DEBUGCONSOLE_T4095885638_H
#define DEBUGCONSOLE_T4095885638_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Debug.DebugConsole
struct  DebugConsole_t4095885638  : public MonoBehaviour_t1158329972
{
public:
	// System.Collections.Generic.List`1<IBM.Watson.DeveloperCloud.Debug.DebugConsole/DebugInfo> IBM.Watson.DeveloperCloud.Debug.DebugConsole::m_DebugInfos
	List_1_t4044068874 * ___m_DebugInfos_2;
	// System.Boolean IBM.Watson.DeveloperCloud.Debug.DebugConsole::m_ActiveOutput
	bool ___m_ActiveOutput_3;
	// System.Boolean IBM.Watson.DeveloperCloud.Debug.DebugConsole::m_ActiveInput
	bool ___m_ActiveInput_4;
	// UnityEngine.GameObject IBM.Watson.DeveloperCloud.Debug.DebugConsole::m_RootOutput
	GameObject_t1756533147 * ___m_RootOutput_5;
	// UnityEngine.GameObject IBM.Watson.DeveloperCloud.Debug.DebugConsole::m_RootInput
	GameObject_t1756533147 * ___m_RootInput_6;
	// UnityEngine.UI.LayoutGroup IBM.Watson.DeveloperCloud.Debug.DebugConsole::m_MessageLayout
	LayoutGroup_t3962498969 * ___m_MessageLayout_7;
	// UnityEngine.UI.Text IBM.Watson.DeveloperCloud.Debug.DebugConsole::m_MessagePrefab
	Text_t356221433 * ___m_MessagePrefab_8;
	// UnityEngine.UI.InputField IBM.Watson.DeveloperCloud.Debug.DebugConsole::m_CommandInput
	InputField_t1631627530 * ___m_CommandInput_9;
	// UnityEngine.UI.LayoutGroup IBM.Watson.DeveloperCloud.Debug.DebugConsole::m_DebugInfoLayout
	LayoutGroup_t3962498969 * ___m_DebugInfoLayout_10;
	// UnityEngine.UI.Text IBM.Watson.DeveloperCloud.Debug.DebugConsole::m_DebugInfoPrefab
	Text_t356221433 * ___m_DebugInfoPrefab_11;
	// IBM.Watson.DeveloperCloud.Widgets.MicrophoneWidget IBM.Watson.DeveloperCloud.Debug.DebugConsole::m_MicWidget
	MicrophoneWidget_t2442369682 * ___m_MicWidget_12;

public:
	inline static int32_t get_offset_of_m_DebugInfos_2() { return static_cast<int32_t>(offsetof(DebugConsole_t4095885638, ___m_DebugInfos_2)); }
	inline List_1_t4044068874 * get_m_DebugInfos_2() const { return ___m_DebugInfos_2; }
	inline List_1_t4044068874 ** get_address_of_m_DebugInfos_2() { return &___m_DebugInfos_2; }
	inline void set_m_DebugInfos_2(List_1_t4044068874 * value)
	{
		___m_DebugInfos_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_DebugInfos_2), value);
	}

	inline static int32_t get_offset_of_m_ActiveOutput_3() { return static_cast<int32_t>(offsetof(DebugConsole_t4095885638, ___m_ActiveOutput_3)); }
	inline bool get_m_ActiveOutput_3() const { return ___m_ActiveOutput_3; }
	inline bool* get_address_of_m_ActiveOutput_3() { return &___m_ActiveOutput_3; }
	inline void set_m_ActiveOutput_3(bool value)
	{
		___m_ActiveOutput_3 = value;
	}

	inline static int32_t get_offset_of_m_ActiveInput_4() { return static_cast<int32_t>(offsetof(DebugConsole_t4095885638, ___m_ActiveInput_4)); }
	inline bool get_m_ActiveInput_4() const { return ___m_ActiveInput_4; }
	inline bool* get_address_of_m_ActiveInput_4() { return &___m_ActiveInput_4; }
	inline void set_m_ActiveInput_4(bool value)
	{
		___m_ActiveInput_4 = value;
	}

	inline static int32_t get_offset_of_m_RootOutput_5() { return static_cast<int32_t>(offsetof(DebugConsole_t4095885638, ___m_RootOutput_5)); }
	inline GameObject_t1756533147 * get_m_RootOutput_5() const { return ___m_RootOutput_5; }
	inline GameObject_t1756533147 ** get_address_of_m_RootOutput_5() { return &___m_RootOutput_5; }
	inline void set_m_RootOutput_5(GameObject_t1756533147 * value)
	{
		___m_RootOutput_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_RootOutput_5), value);
	}

	inline static int32_t get_offset_of_m_RootInput_6() { return static_cast<int32_t>(offsetof(DebugConsole_t4095885638, ___m_RootInput_6)); }
	inline GameObject_t1756533147 * get_m_RootInput_6() const { return ___m_RootInput_6; }
	inline GameObject_t1756533147 ** get_address_of_m_RootInput_6() { return &___m_RootInput_6; }
	inline void set_m_RootInput_6(GameObject_t1756533147 * value)
	{
		___m_RootInput_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_RootInput_6), value);
	}

	inline static int32_t get_offset_of_m_MessageLayout_7() { return static_cast<int32_t>(offsetof(DebugConsole_t4095885638, ___m_MessageLayout_7)); }
	inline LayoutGroup_t3962498969 * get_m_MessageLayout_7() const { return ___m_MessageLayout_7; }
	inline LayoutGroup_t3962498969 ** get_address_of_m_MessageLayout_7() { return &___m_MessageLayout_7; }
	inline void set_m_MessageLayout_7(LayoutGroup_t3962498969 * value)
	{
		___m_MessageLayout_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_MessageLayout_7), value);
	}

	inline static int32_t get_offset_of_m_MessagePrefab_8() { return static_cast<int32_t>(offsetof(DebugConsole_t4095885638, ___m_MessagePrefab_8)); }
	inline Text_t356221433 * get_m_MessagePrefab_8() const { return ___m_MessagePrefab_8; }
	inline Text_t356221433 ** get_address_of_m_MessagePrefab_8() { return &___m_MessagePrefab_8; }
	inline void set_m_MessagePrefab_8(Text_t356221433 * value)
	{
		___m_MessagePrefab_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_MessagePrefab_8), value);
	}

	inline static int32_t get_offset_of_m_CommandInput_9() { return static_cast<int32_t>(offsetof(DebugConsole_t4095885638, ___m_CommandInput_9)); }
	inline InputField_t1631627530 * get_m_CommandInput_9() const { return ___m_CommandInput_9; }
	inline InputField_t1631627530 ** get_address_of_m_CommandInput_9() { return &___m_CommandInput_9; }
	inline void set_m_CommandInput_9(InputField_t1631627530 * value)
	{
		___m_CommandInput_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_CommandInput_9), value);
	}

	inline static int32_t get_offset_of_m_DebugInfoLayout_10() { return static_cast<int32_t>(offsetof(DebugConsole_t4095885638, ___m_DebugInfoLayout_10)); }
	inline LayoutGroup_t3962498969 * get_m_DebugInfoLayout_10() const { return ___m_DebugInfoLayout_10; }
	inline LayoutGroup_t3962498969 ** get_address_of_m_DebugInfoLayout_10() { return &___m_DebugInfoLayout_10; }
	inline void set_m_DebugInfoLayout_10(LayoutGroup_t3962498969 * value)
	{
		___m_DebugInfoLayout_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_DebugInfoLayout_10), value);
	}

	inline static int32_t get_offset_of_m_DebugInfoPrefab_11() { return static_cast<int32_t>(offsetof(DebugConsole_t4095885638, ___m_DebugInfoPrefab_11)); }
	inline Text_t356221433 * get_m_DebugInfoPrefab_11() const { return ___m_DebugInfoPrefab_11; }
	inline Text_t356221433 ** get_address_of_m_DebugInfoPrefab_11() { return &___m_DebugInfoPrefab_11; }
	inline void set_m_DebugInfoPrefab_11(Text_t356221433 * value)
	{
		___m_DebugInfoPrefab_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_DebugInfoPrefab_11), value);
	}

	inline static int32_t get_offset_of_m_MicWidget_12() { return static_cast<int32_t>(offsetof(DebugConsole_t4095885638, ___m_MicWidget_12)); }
	inline MicrophoneWidget_t2442369682 * get_m_MicWidget_12() const { return ___m_MicWidget_12; }
	inline MicrophoneWidget_t2442369682 ** get_address_of_m_MicWidget_12() { return &___m_MicWidget_12; }
	inline void set_m_MicWidget_12(MicrophoneWidget_t2442369682 * value)
	{
		___m_MicWidget_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_MicWidget_12), value);
	}
};

struct DebugConsole_t4095885638_StaticFields
{
public:
	// IBM.Watson.DeveloperCloud.Debug.DebugConsole IBM.Watson.DeveloperCloud.Debug.DebugConsole::<Instance>k__BackingField
	DebugConsole_t4095885638 * ___U3CInstanceU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(DebugConsole_t4095885638_StaticFields, ___U3CInstanceU3Ek__BackingField_13)); }
	inline DebugConsole_t4095885638 * get_U3CInstanceU3Ek__BackingField_13() const { return ___U3CInstanceU3Ek__BackingField_13; }
	inline DebugConsole_t4095885638 ** get_address_of_U3CInstanceU3Ek__BackingField_13() { return &___U3CInstanceU3Ek__BackingField_13; }
	inline void set_U3CInstanceU3Ek__BackingField_13(DebugConsole_t4095885638 * value)
	{
		___U3CInstanceU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGCONSOLE_T4095885638_H
#ifndef QUALITYMANAGER_T3466824526_H
#define QUALITYMANAGER_T3466824526_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Debug.QualityManager
struct  QualityManager_t3466824526  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUALITYMANAGER_T3466824526_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2300 = { sizeof (WSConnector_t2552241039), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2300[12] = 
{
	WSConnector_t2552241039::get_offset_of_U3COnCloseU3Ek__BackingField_0(),
	WSConnector_t2552241039::get_offset_of_U3COnMessageU3Ek__BackingField_1(),
	WSConnector_t2552241039::get_offset_of_U3CURLU3Ek__BackingField_2(),
	WSConnector_t2552241039::get_offset_of_U3CHeadersU3Ek__BackingField_3(),
	WSConnector_t2552241039::get_offset_of_U3CAuthenticationU3Ek__BackingField_4(),
	WSConnector_t2552241039::get_offset_of_m_ConnectionState_5(),
	WSConnector_t2552241039::get_offset_of_m_SendThread_6(),
	WSConnector_t2552241039::get_offset_of_m_SendEvent_7(),
	WSConnector_t2552241039::get_offset_of_m_SendQueue_8(),
	WSConnector_t2552241039::get_offset_of_m_ReceiveEvent_9(),
	WSConnector_t2552241039::get_offset_of_m_ReceiveQueue_10(),
	WSConnector_t2552241039::get_offset_of_m_ReceiverRoutine_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2301 = { sizeof (ConnectorEvent_t1029302242), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2302 = { sizeof (MessageEvent_t2276855538), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2303 = { sizeof (ConnectionState_t3540963536)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2303[5] = 
{
	ConnectionState_t3540963536::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2304 = { sizeof (Message_t2515021790), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2305 = { sizeof (BinaryMessage_t3924129269), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2305[1] = 
{
	BinaryMessage_t3924129269::get_offset_of_U3CDataU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2306 = { sizeof (TextMessage_t143068955), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2306[1] = 
{
	TextMessage_t143068955::get_offset_of_U3CTextU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2307 = { sizeof (U3CProcessReceiveQueueU3Ec__Iterator0_t1751931715), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2307[4] = 
{
	U3CProcessReceiveQueueU3Ec__Iterator0_t1751931715::get_offset_of_U24this_0(),
	U3CProcessReceiveQueueU3Ec__Iterator0_t1751931715::get_offset_of_U24current_1(),
	U3CProcessReceiveQueueU3Ec__Iterator0_t1751931715::get_offset_of_U24disposing_2(),
	U3CProcessReceiveQueueU3Ec__Iterator0_t1751931715::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2308 = { sizeof (DebugConsole_t4095885638), -1, sizeof(DebugConsole_t4095885638_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2308[12] = 
{
	DebugConsole_t4095885638::get_offset_of_m_DebugInfos_2(),
	DebugConsole_t4095885638::get_offset_of_m_ActiveOutput_3(),
	DebugConsole_t4095885638::get_offset_of_m_ActiveInput_4(),
	DebugConsole_t4095885638::get_offset_of_m_RootOutput_5(),
	DebugConsole_t4095885638::get_offset_of_m_RootInput_6(),
	DebugConsole_t4095885638::get_offset_of_m_MessageLayout_7(),
	DebugConsole_t4095885638::get_offset_of_m_MessagePrefab_8(),
	DebugConsole_t4095885638::get_offset_of_m_CommandInput_9(),
	DebugConsole_t4095885638::get_offset_of_m_DebugInfoLayout_10(),
	DebugConsole_t4095885638::get_offset_of_m_DebugInfoPrefab_11(),
	DebugConsole_t4095885638::get_offset_of_m_MicWidget_12(),
	DebugConsole_t4095885638_StaticFields::get_offset_of_U3CInstanceU3Ek__BackingField_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2309 = { sizeof (GetDebugInfo_t2665501740), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2310 = { sizeof (DebugInfo_t379980446), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2310[4] = 
{
	DebugInfo_t379980446::get_offset_of_m_Label_0(),
	DebugInfo_t379980446::get_offset_of_m_Callback_1(),
	DebugInfo_t379980446::get_offset_of_m_InfoObject_2(),
	DebugInfo_t379980446::get_offset_of_m_TextOutput_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2311 = { sizeof (U3CActivateMicAfterTimeU3Ec__Iterator0_t647232991), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2311[5] = 
{
	U3CActivateMicAfterTimeU3Ec__Iterator0_t647232991::get_offset_of_time_0(),
	U3CActivateMicAfterTimeU3Ec__Iterator0_t647232991::get_offset_of_U24this_1(),
	U3CActivateMicAfterTimeU3Ec__Iterator0_t647232991::get_offset_of_U24current_2(),
	U3CActivateMicAfterTimeU3Ec__Iterator0_t647232991::get_offset_of_U24disposing_3(),
	U3CActivateMicAfterTimeU3Ec__Iterator0_t647232991::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2312 = { sizeof (DebugReactor_t558814947), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2312[1] = 
{
	DebugReactor_t558814947::get_offset_of_U3CLevelU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2313 = { sizeof (QualityManager_t3466824526), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2314 = { sizeof (FileReactor_t4232122414), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2314[4] = 
{
	FileReactor_t4232122414::get_offset_of_U3CLogFileU3Ek__BackingField_0(),
	FileReactor_t4232122414::get_offset_of_U3CLevelU3Ek__BackingField_1(),
	FileReactor_t4232122414::get_offset_of_U3CLogHistoryU3Ek__BackingField_2(),
	FileReactor_t4232122414::get_offset_of_U3CMaxLogSizeU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2315 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2316 = { sizeof (LogLevel_t3918900206)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2316[7] = 
{
	LogLevel_t3918900206::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2317 = { sizeof (LogRecord_t1360968087), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2317[4] = 
{
	LogRecord_t1360968087::get_offset_of_m_TimeStamp_0(),
	LogRecord_t1360968087::get_offset_of_m_Level_1(),
	LogRecord_t1360968087::get_offset_of_m_SubSystem_2(),
	LogRecord_t1360968087::get_offset_of_m_Message_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2318 = { sizeof (LogSystem_t3371271695), -1, sizeof(LogSystem_t3371271695_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2318[3] = 
{
	LogSystem_t3371271695_StaticFields::get_offset_of_sm_bInstalledDefaultReactors_0(),
	LogSystem_t3371271695::get_offset_of_m_Reactors_1(),
	LogSystem_t3371271695_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2319 = { sizeof (Log_t4252317216), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2320 = { sizeof (AlchemyAPI_t2955839919), -1, sizeof(AlchemyAPI_t2955839919_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2320[51] = 
{
	0,
	AlchemyAPI_t2955839919_StaticFields::get_offset_of_mp_ApiKey_1(),
	AlchemyAPI_t2955839919_StaticFields::get_offset_of_sm_Serializer_2(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2321 = { sizeof (OnGetAuthors_t505057758), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2322 = { sizeof (GetAuthorsRequest_t1850603144), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2322[2] = 
{
	GetAuthorsRequest_t1850603144::get_offset_of_U3CDataU3Ek__BackingField_11(),
	GetAuthorsRequest_t1850603144::get_offset_of_U3CCallbackU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2323 = { sizeof (OnGetRankedConcepts_t3530605856), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2324 = { sizeof (GetRankedConceptsRequest_t3286662032), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2324[2] = 
{
	GetRankedConceptsRequest_t3286662032::get_offset_of_U3CDataU3Ek__BackingField_11(),
	GetRankedConceptsRequest_t3286662032::get_offset_of_U3CCallbackU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2325 = { sizeof (OnGetDates_t91791237), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2326 = { sizeof (GetDatesRequest_t2698785969), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2326[2] = 
{
	GetDatesRequest_t2698785969::get_offset_of_U3CDataU3Ek__BackingField_11(),
	GetDatesRequest_t2698785969::get_offset_of_U3CCallbackU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2327 = { sizeof (OnGetEmotions_t1524549668), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2328 = { sizeof (GetEmotionsRequest_t3200624748), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2328[2] = 
{
	GetEmotionsRequest_t3200624748::get_offset_of_U3CDataU3Ek__BackingField_11(),
	GetEmotionsRequest_t3200624748::get_offset_of_U3CCallbackU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2329 = { sizeof (OnGetEntities_t1909489667), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2330 = { sizeof (GetEntitiesRequest_t2587138723), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2330[2] = 
{
	GetEntitiesRequest_t2587138723::get_offset_of_U3CDataU3Ek__BackingField_11(),
	GetEntitiesRequest_t2587138723::get_offset_of_U3CCallbackU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2331 = { sizeof (OnDetectFeeds_t4021436334), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2332 = { sizeof (DetectFeedsRequest_t576411670), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2332[2] = 
{
	DetectFeedsRequest_t576411670::get_offset_of_U3CDataU3Ek__BackingField_11(),
	DetectFeedsRequest_t576411670::get_offset_of_U3CCallbackU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2333 = { sizeof (OnGetKeywords_t1978903802), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2334 = { sizeof (GetKeywordsRequest_t1200965810), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2334[2] = 
{
	GetKeywordsRequest_t1200965810::get_offset_of_U3CDataU3Ek__BackingField_11(),
	GetKeywordsRequest_t1200965810::get_offset_of_U3CCallbackU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2335 = { sizeof (OnGetLanguages_t3197075847), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2336 = { sizeof (GetLanguagesRequest_t4142277483), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2336[2] = 
{
	GetLanguagesRequest_t4142277483::get_offset_of_U3CDataU3Ek__BackingField_11(),
	GetLanguagesRequest_t4142277483::get_offset_of_U3CCallbackU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2337 = { sizeof (OnGetMicroformats_t327929786), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2338 = { sizeof (GetMicroformatsRequest_t3175661922), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2338[2] = 
{
	GetMicroformatsRequest_t3175661922::get_offset_of_U3CDataU3Ek__BackingField_11(),
	GetMicroformatsRequest_t3175661922::get_offset_of_U3CCallbackU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2339 = { sizeof (OnGetPublicationDate_t443403836), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2340 = { sizeof (GetPublicationDateRequest_t3398587598), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2340[2] = 
{
	GetPublicationDateRequest_t3398587598::get_offset_of_U3CDataU3Ek__BackingField_11(),
	GetPublicationDateRequest_t3398587598::get_offset_of_U3CCallbackU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2341 = { sizeof (OnGetRelations_t1280830431), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2342 = { sizeof (GetRelationsRequest_t1673598355), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2342[2] = 
{
	GetRelationsRequest_t1673598355::get_offset_of_U3CDataU3Ek__BackingField_11(),
	GetRelationsRequest_t1673598355::get_offset_of_U3CCallbackU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2343 = { sizeof (OnGetTextSentiment_t3575883834), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2344 = { sizeof (GetTextSentimentRequest_t1002716236), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2344[2] = 
{
	GetTextSentimentRequest_t1002716236::get_offset_of_U3CDataU3Ek__BackingField_11(),
	GetTextSentimentRequest_t1002716236::get_offset_of_U3CCallbackU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2345 = { sizeof (OnGetTargetedSentiment_t2739505381), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2346 = { sizeof (GetTargetedSentimentRequest_t3429846097), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2346[2] = 
{
	GetTargetedSentimentRequest_t3429846097::get_offset_of_U3CDataU3Ek__BackingField_11(),
	GetTargetedSentimentRequest_t3429846097::get_offset_of_U3CCallbackU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2347 = { sizeof (OnGetRankedTaxonomy_t935340330), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2348 = { sizeof (GetRankedTaxomomyRequest_t2472232113), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2348[2] = 
{
	GetRankedTaxomomyRequest_t2472232113::get_offset_of_U3CDataU3Ek__BackingField_11(),
	GetRankedTaxomomyRequest_t2472232113::get_offset_of_U3CCallbackU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2349 = { sizeof (OnGetText_t3268193843), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2350 = { sizeof (GetTextRequest_t1378117363), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2350[2] = 
{
	GetTextRequest_t1378117363::get_offset_of_U3CDataU3Ek__BackingField_11(),
	GetTextRequest_t1378117363::get_offset_of_U3CCallbackU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2351 = { sizeof (OnGetTitle_t1839253948), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2352 = { sizeof (GetTitleRequest_t180924238), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2352[2] = 
{
	GetTitleRequest_t180924238::get_offset_of_U3CDataU3Ek__BackingField_11(),
	GetTitleRequest_t180924238::get_offset_of_U3CCallbackU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2353 = { sizeof (OnGetCombinedData_t4285430159), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2354 = { sizeof (CombinedCallRequest_t1669398659), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2354[2] = 
{
	CombinedCallRequest_t1669398659::get_offset_of_U3CDataU3Ek__BackingField_11(),
	CombinedCallRequest_t1669398659::get_offset_of_U3CCallbackU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2355 = { sizeof (OnGetNews_t1755812277), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2356 = { sizeof (GetNewsRequest_t4208688917), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2356[2] = 
{
	GetNewsRequest_t4208688917::get_offset_of_U3CDataU3Ek__BackingField_11(),
	GetNewsRequest_t4208688917::get_offset_of_U3CCallbackU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2357 = { sizeof (CheckServiceStatus_t3778204258), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2357[2] = 
{
	CheckServiceStatus_t3778204258::get_offset_of_m_Service_0(),
	CheckServiceStatus_t3778204258::get_offset_of_m_Callback_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2358 = { sizeof (CombinedCallData_t1377778673), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2358[19] = 
{
	CombinedCallData_t1377778673::get_offset_of_U3CstatusU3Ek__BackingField_0(),
	CombinedCallData_t1377778673::get_offset_of_U3CurlU3Ek__BackingField_1(),
	CombinedCallData_t1377778673::get_offset_of_U3ClanguageU3Ek__BackingField_2(),
	CombinedCallData_t1377778673::get_offset_of_U3CtitleU3Ek__BackingField_3(),
	CombinedCallData_t1377778673::get_offset_of_U3CtextU3Ek__BackingField_4(),
	CombinedCallData_t1377778673::get_offset_of_U3CimageU3Ek__BackingField_5(),
	CombinedCallData_t1377778673::get_offset_of_U3CimageKeywordsU3Ek__BackingField_6(),
	CombinedCallData_t1377778673::get_offset_of_U3CpublicationDateU3Ek__BackingField_7(),
	CombinedCallData_t1377778673::get_offset_of_U3CauthorsU3Ek__BackingField_8(),
	CombinedCallData_t1377778673::get_offset_of_U3CdocSentimentU3Ek__BackingField_9(),
	CombinedCallData_t1377778673::get_offset_of_U3CfeedsU3Ek__BackingField_10(),
	CombinedCallData_t1377778673::get_offset_of_U3CkeywordsU3Ek__BackingField_11(),
	CombinedCallData_t1377778673::get_offset_of_U3CconceptsU3Ek__BackingField_12(),
	CombinedCallData_t1377778673::get_offset_of_U3CentitiesU3Ek__BackingField_13(),
	CombinedCallData_t1377778673::get_offset_of_U3CrelationsU3Ek__BackingField_14(),
	CombinedCallData_t1377778673::get_offset_of_U3CtaxonomyU3Ek__BackingField_15(),
	CombinedCallData_t1377778673::get_offset_of_U3CdatesU3Ek__BackingField_16(),
	CombinedCallData_t1377778673::get_offset_of_U3CdocEmotionsU3Ek__BackingField_17(),
	CombinedCallData_t1377778673::get_offset_of__EntityCombined_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2359 = { sizeof (ImageKeyword_t1009764024), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2359[2] = 
{
	ImageKeyword_t1009764024::get_offset_of_U3CtextU3Ek__BackingField_0(),
	ImageKeyword_t1009764024::get_offset_of_U3CscoreU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2360 = { sizeof (AuthorsData_t3643075816), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2360[3] = 
{
	AuthorsData_t3643075816::get_offset_of_U3CstatusU3Ek__BackingField_0(),
	AuthorsData_t3643075816::get_offset_of_U3CurlU3Ek__BackingField_1(),
	AuthorsData_t3643075816::get_offset_of_U3CauthorsU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2361 = { sizeof (Authors_t34055864), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2361[2] = 
{
	Authors_t34055864::get_offset_of_U3CconfidentU3Ek__BackingField_0(),
	Authors_t34055864::get_offset_of_U3CnamesU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2362 = { sizeof (ConceptsData_t441878239), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2362[5] = 
{
	ConceptsData_t441878239::get_offset_of_U3CstatusU3Ek__BackingField_0(),
	ConceptsData_t441878239::get_offset_of_U3CurlU3Ek__BackingField_1(),
	ConceptsData_t441878239::get_offset_of_U3ClanguageU3Ek__BackingField_2(),
	ConceptsData_t441878239::get_offset_of_U3CtextU3Ek__BackingField_3(),
	ConceptsData_t441878239::get_offset_of_U3CconceptsU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2363 = { sizeof (Concept_t2322716066), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2363[14] = 
{
	Concept_t2322716066::get_offset_of_U3CtextU3Ek__BackingField_0(),
	Concept_t2322716066::get_offset_of_U3CrelevanceU3Ek__BackingField_1(),
	Concept_t2322716066::get_offset_of_U3CknowledgeGraphU3Ek__BackingField_2(),
	Concept_t2322716066::get_offset_of_U3CwebsiteU3Ek__BackingField_3(),
	Concept_t2322716066::get_offset_of_U3CgeoU3Ek__BackingField_4(),
	Concept_t2322716066::get_offset_of_U3CdbpediaU3Ek__BackingField_5(),
	Concept_t2322716066::get_offset_of_U3CfreebaseU3Ek__BackingField_6(),
	Concept_t2322716066::get_offset_of_U3CyagoU3Ek__BackingField_7(),
	Concept_t2322716066::get_offset_of_U3CopencycU3Ek__BackingField_8(),
	Concept_t2322716066::get_offset_of_U3CciaFactbookU3Ek__BackingField_9(),
	Concept_t2322716066::get_offset_of_U3CcensusU3Ek__BackingField_10(),
	Concept_t2322716066::get_offset_of_U3CgeonamesU3Ek__BackingField_11(),
	Concept_t2322716066::get_offset_of_U3CmusicBrainzU3Ek__BackingField_12(),
	Concept_t2322716066::get_offset_of_U3CcrunchbaseU3Ek__BackingField_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2364 = { sizeof (DateData_t3593288038), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2364[5] = 
{
	DateData_t3593288038::get_offset_of_U3CstatusU3Ek__BackingField_0(),
	DateData_t3593288038::get_offset_of_U3ClanguageU3Ek__BackingField_1(),
	DateData_t3593288038::get_offset_of_U3CurlU3Ek__BackingField_2(),
	DateData_t3593288038::get_offset_of_U3CtextU3Ek__BackingField_3(),
	DateData_t3593288038::get_offset_of_U3CdatesU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2365 = { sizeof (Date_t2577429686), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2365[3] = 
{
	Date_t2577429686::get_offset_of_U3CdateU3Ek__BackingField_0(),
	Date_t2577429686::get_offset_of_U3CtextU3Ek__BackingField_1(),
	Date_t2577429686::get_offset_of_m_dateValue_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2366 = { sizeof (EmotionData_t625023405), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2366[5] = 
{
	EmotionData_t625023405::get_offset_of_U3CstatusU3Ek__BackingField_0(),
	EmotionData_t625023405::get_offset_of_U3CurlU3Ek__BackingField_1(),
	EmotionData_t625023405::get_offset_of_U3ClanguageU3Ek__BackingField_2(),
	EmotionData_t625023405::get_offset_of_U3CtextU3Ek__BackingField_3(),
	EmotionData_t625023405::get_offset_of_U3CdocEmotionsU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2367 = { sizeof (DocEmotions_t3627397418), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2367[5] = 
{
	DocEmotions_t3627397418::get_offset_of_U3CangerU3Ek__BackingField_0(),
	DocEmotions_t3627397418::get_offset_of_U3CdisgustU3Ek__BackingField_1(),
	DocEmotions_t3627397418::get_offset_of_U3CfearU3Ek__BackingField_2(),
	DocEmotions_t3627397418::get_offset_of_U3CjoyU3Ek__BackingField_3(),
	DocEmotions_t3627397418::get_offset_of_U3CsadnessU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2368 = { sizeof (EntityData_t436806285), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2368[6] = 
{
	EntityData_t436806285::get_offset_of_U3CstatusU3Ek__BackingField_0(),
	EntityData_t436806285::get_offset_of_U3CurlU3Ek__BackingField_1(),
	EntityData_t436806285::get_offset_of_U3ClanguageU3Ek__BackingField_2(),
	EntityData_t436806285::get_offset_of_U3CtextU3Ek__BackingField_3(),
	EntityData_t436806285::get_offset_of_U3CentitiesU3Ek__BackingField_4(),
	EntityData_t436806285::get_offset_of_m_GeoLocation_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2369 = { sizeof (Entity_t2757698109), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2369[10] = 
{
	Entity_t2757698109::get_offset_of_U3CtypeU3Ek__BackingField_0(),
	Entity_t2757698109::get_offset_of_U3CrelevanceU3Ek__BackingField_1(),
	Entity_t2757698109::get_offset_of_U3CknowledgeGraphU3Ek__BackingField_2(),
	Entity_t2757698109::get_offset_of_U3CcountU3Ek__BackingField_3(),
	Entity_t2757698109::get_offset_of_U3CtextU3Ek__BackingField_4(),
	Entity_t2757698109::get_offset_of_U3CdisambiguatedU3Ek__BackingField_5(),
	Entity_t2757698109::get_offset_of_U3CquotationsU3Ek__BackingField_6(),
	Entity_t2757698109::get_offset_of_U3CsentimentU3Ek__BackingField_7(),
	Entity_t2757698109::get_offset_of__EntityType_8(),
	Entity_t2757698109::get_offset_of__GeoLocation_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2370 = { sizeof (PositionOnMap_t66846110), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2370[3] = 
{
	PositionOnMap_t66846110::get_offset_of_PositionName_0(),
	PositionOnMap_t66846110::get_offset_of_Latitude_1(),
	PositionOnMap_t66846110::get_offset_of_Longitude_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2371 = { sizeof (EntityPrimaryType_t4275181819)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2371[45] = 
{
	EntityPrimaryType_t4275181819::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2372 = { sizeof (EntitySubType_t2472813267)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2372[438] = 
{
	EntitySubType_t2472813267::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2373 = { sizeof (FeedData_t845804320), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2373[3] = 
{
	FeedData_t845804320::get_offset_of_U3CstatusU3Ek__BackingField_0(),
	FeedData_t845804320::get_offset_of_U3CurlU3Ek__BackingField_1(),
	FeedData_t845804320::get_offset_of_U3CfeedsU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2374 = { sizeof (Feed_t2248050640), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2374[1] = 
{
	Feed_t2248050640::get_offset_of_U3CfeedU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2375 = { sizeof (KeywordData_t1578948319), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2375[5] = 
{
	KeywordData_t1578948319::get_offset_of_U3CstatusU3Ek__BackingField_0(),
	KeywordData_t1578948319::get_offset_of_U3CurlU3Ek__BackingField_1(),
	KeywordData_t1578948319::get_offset_of_U3ClanguageU3Ek__BackingField_2(),
	KeywordData_t1578948319::get_offset_of_U3CtextU3Ek__BackingField_3(),
	KeywordData_t1578948319::get_offset_of_U3CkeywordsU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2376 = { sizeof (Keyword_t3384522415), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2376[4] = 
{
	Keyword_t3384522415::get_offset_of_U3CtextU3Ek__BackingField_0(),
	Keyword_t3384522415::get_offset_of_U3CrelevanceU3Ek__BackingField_1(),
	Keyword_t3384522415::get_offset_of_U3CknowledgeGraphU3Ek__BackingField_2(),
	Keyword_t3384522415::get_offset_of_U3CsentimentU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2377 = { sizeof (LanguageData_t2624020128), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2377[9] = 
{
	LanguageData_t2624020128::get_offset_of_U3CstatusU3Ek__BackingField_0(),
	LanguageData_t2624020128::get_offset_of_U3CurlU3Ek__BackingField_1(),
	LanguageData_t2624020128::get_offset_of_U3ClanguageU3Ek__BackingField_2(),
	LanguageData_t2624020128::get_offset_of_U3Ciso_639_1U3Ek__BackingField_3(),
	LanguageData_t2624020128::get_offset_of_U3Ciso_639_2U3Ek__BackingField_4(),
	LanguageData_t2624020128::get_offset_of_U3Ciso_639_3U3Ek__BackingField_5(),
	LanguageData_t2624020128::get_offset_of_U3CethnologueU3Ek__BackingField_6(),
	LanguageData_t2624020128::get_offset_of_U3Cnative_speakersU3Ek__BackingField_7(),
	LanguageData_t2624020128::get_offset_of_U3CwikipediaU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2378 = { sizeof (MicroformatData_t655366775), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2378[3] = 
{
	MicroformatData_t655366775::get_offset_of_U3CstatusU3Ek__BackingField_0(),
	MicroformatData_t655366775::get_offset_of_U3CurlU3Ek__BackingField_1(),
	MicroformatData_t655366775::get_offset_of_U3CmicroformatsU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2379 = { sizeof (Microformat_t171559559), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2379[2] = 
{
	Microformat_t171559559::get_offset_of_U3CfieldU3Ek__BackingField_0(),
	Microformat_t171559559::get_offset_of_U3CdataU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2380 = { sizeof (PubDateData_t3405155195), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2380[4] = 
{
	PubDateData_t3405155195::get_offset_of_U3CstatusU3Ek__BackingField_0(),
	PubDateData_t3405155195::get_offset_of_U3CurlU3Ek__BackingField_1(),
	PubDateData_t3405155195::get_offset_of_U3ClanguageU3Ek__BackingField_2(),
	PubDateData_t3405155195::get_offset_of_U3CpublicationDateU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2381 = { sizeof (RelationsData_t3907134773), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2381[5] = 
{
	RelationsData_t3907134773::get_offset_of_U3CstatusU3Ek__BackingField_0(),
	RelationsData_t3907134773::get_offset_of_U3CurlU3Ek__BackingField_1(),
	RelationsData_t3907134773::get_offset_of_U3ClanguageU3Ek__BackingField_2(),
	RelationsData_t3907134773::get_offset_of_U3CtextU3Ek__BackingField_3(),
	RelationsData_t3907134773::get_offset_of_U3CrelationsU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2382 = { sizeof (SentimentData_t471353809), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2382[5] = 
{
	SentimentData_t471353809::get_offset_of_U3CstatusU3Ek__BackingField_0(),
	SentimentData_t471353809::get_offset_of_U3CurlU3Ek__BackingField_1(),
	SentimentData_t471353809::get_offset_of_U3ClanguageU3Ek__BackingField_2(),
	SentimentData_t471353809::get_offset_of_U3CtextU3Ek__BackingField_3(),
	SentimentData_t471353809::get_offset_of_U3CdocSentimentU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2383 = { sizeof (TargetedSentimentData_t3798577861), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2383[5] = 
{
	TargetedSentimentData_t3798577861::get_offset_of_U3CstatusU3Ek__BackingField_0(),
	TargetedSentimentData_t3798577861::get_offset_of_U3CurlU3Ek__BackingField_1(),
	TargetedSentimentData_t3798577861::get_offset_of_U3CtextU3Ek__BackingField_2(),
	TargetedSentimentData_t3798577861::get_offset_of_U3ClanguageU3Ek__BackingField_3(),
	TargetedSentimentData_t3798577861::get_offset_of_U3CresultsU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2384 = { sizeof (TargetedSentiment_t494351509), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2384[2] = 
{
	TargetedSentiment_t494351509::get_offset_of_U3CsentimentU3Ek__BackingField_0(),
	TargetedSentiment_t494351509::get_offset_of_U3CtextU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2385 = { sizeof (TaxonomyData_t3741516269), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2385[5] = 
{
	TaxonomyData_t3741516269::get_offset_of_U3CstatusU3Ek__BackingField_0(),
	TaxonomyData_t3741516269::get_offset_of_U3CurlU3Ek__BackingField_1(),
	TaxonomyData_t3741516269::get_offset_of_U3ClanguageU3Ek__BackingField_2(),
	TaxonomyData_t3741516269::get_offset_of_U3CtextU3Ek__BackingField_3(),
	TaxonomyData_t3741516269::get_offset_of_U3CtaxonomyU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2386 = { sizeof (TextData_t3443651365), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2386[4] = 
{
	TextData_t3443651365::get_offset_of_U3CstatusU3Ek__BackingField_0(),
	TextData_t3443651365::get_offset_of_U3CurlU3Ek__BackingField_1(),
	TextData_t3443651365::get_offset_of_U3CtextU3Ek__BackingField_2(),
	TextData_t3443651365::get_offset_of_U3ClanguageU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2387 = { sizeof (Title_t2058683094), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2387[3] = 
{
	Title_t2058683094::get_offset_of_U3CstatusU3Ek__BackingField_0(),
	Title_t2058683094::get_offset_of_U3CurlU3Ek__BackingField_1(),
	Title_t2058683094::get_offset_of_U3CtitleU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2388 = { sizeof (KnowledgeGraph_t3341597436), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2388[1] = 
{
	KnowledgeGraph_t3341597436::get_offset_of_U3CtypeHierarchyU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2389 = { sizeof (Disambiguated_t227057183), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2389[14] = 
{
	Disambiguated_t227057183::get_offset_of_U3CnameU3Ek__BackingField_0(),
	Disambiguated_t227057183::get_offset_of_U3CsubTypeU3Ek__BackingField_1(),
	Disambiguated_t227057183::get_offset_of_U3CwebsiteU3Ek__BackingField_2(),
	Disambiguated_t227057183::get_offset_of_U3CgeoU3Ek__BackingField_3(),
	Disambiguated_t227057183::get_offset_of_U3CdbpediaU3Ek__BackingField_4(),
	Disambiguated_t227057183::get_offset_of_U3CyagoU3Ek__BackingField_5(),
	Disambiguated_t227057183::get_offset_of_U3CopencycU3Ek__BackingField_6(),
	Disambiguated_t227057183::get_offset_of_U3CumbelU3Ek__BackingField_7(),
	Disambiguated_t227057183::get_offset_of_U3CfreebaseU3Ek__BackingField_8(),
	Disambiguated_t227057183::get_offset_of_U3CciaFactbookU3Ek__BackingField_9(),
	Disambiguated_t227057183::get_offset_of_U3CcensusU3Ek__BackingField_10(),
	Disambiguated_t227057183::get_offset_of_U3CgeonamesU3Ek__BackingField_11(),
	Disambiguated_t227057183::get_offset_of_U3CmusicBrainzU3Ek__BackingField_12(),
	Disambiguated_t227057183::get_offset_of_U3CcrunchbaseU3Ek__BackingField_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2390 = { sizeof (Quotation_t2638841872), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2390[1] = 
{
	Quotation_t2638841872::get_offset_of_U3CquotationU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2391 = { sizeof (DocSentiment_t2354958393), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2391[4] = 
{
	DocSentiment_t2354958393::get_offset_of_U3CtypeU3Ek__BackingField_0(),
	DocSentiment_t2354958393::get_offset_of_U3CscoreU3Ek__BackingField_1(),
	DocSentiment_t2354958393::get_offset_of_U3CmixedU3Ek__BackingField_2(),
	DocSentiment_t2354958393::get_offset_of_m_Score_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2392 = { sizeof (PublicationDate_t608222142), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2392[2] = 
{
	PublicationDate_t608222142::get_offset_of_U3CdateU3Ek__BackingField_0(),
	PublicationDate_t608222142::get_offset_of_U3CconfidentU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2393 = { sizeof (Relation_t2336466576), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2393[6] = 
{
	Relation_t2336466576::get_offset_of_U3CsentenceU3Ek__BackingField_0(),
	Relation_t2336466576::get_offset_of_U3CsubjectU3Ek__BackingField_1(),
	Relation_t2336466576::get_offset_of_U3CactionU3Ek__BackingField_2(),
	Relation_t2336466576::get_offset_of_U3CobjectU3Ek__BackingField_3(),
	Relation_t2336466576::get_offset_of_U3ClocationU3Ek__BackingField_4(),
	Relation_t2336466576::get_offset_of_U3CtemporalU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2394 = { sizeof (Subject_t927036590), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2394[4] = 
{
	Subject_t927036590::get_offset_of_U3CtextU3Ek__BackingField_0(),
	Subject_t927036590::get_offset_of_U3CsentimentU3Ek__BackingField_1(),
	Subject_t927036590::get_offset_of_U3CentitiesU3Ek__BackingField_2(),
	Subject_t927036590::get_offset_of_U3CkeywordsU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2395 = { sizeof (Action_t3935607096), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2395[3] = 
{
	Action_t3935607096::get_offset_of_U3CtextU3Ek__BackingField_0(),
	Action_t3935607096::get_offset_of_U3ClemmatizedU3Ek__BackingField_1(),
	Action_t3935607096::get_offset_of_U3CverbU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2396 = { sizeof (ObjectData_t1677090869), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2396[4] = 
{
	ObjectData_t1677090869::get_offset_of_U3CtextU3Ek__BackingField_0(),
	ObjectData_t1677090869::get_offset_of_U3CsentimentU3Ek__BackingField_1(),
	ObjectData_t1677090869::get_offset_of_U3CsentimentFromSubjectU3Ek__BackingField_2(),
	ObjectData_t1677090869::get_offset_of_U3CentityU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2397 = { sizeof (Verb_t2846458383), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2397[3] = 
{
	Verb_t2846458383::get_offset_of_U3CtextU3Ek__BackingField_0(),
	Verb_t2846458383::get_offset_of_U3CtenseU3Ek__BackingField_1(),
	Verb_t2846458383::get_offset_of_U3CnegatedU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2398 = { sizeof (Taxonomy_t1527601757), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2398[3] = 
{
	Taxonomy_t1527601757::get_offset_of_U3ClabelU3Ek__BackingField_0(),
	Taxonomy_t1527601757::get_offset_of_U3CscoreU3Ek__BackingField_1(),
	Taxonomy_t1527601757::get_offset_of_U3CconfidentU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2399 = { sizeof (Location_t4202627189), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2399[2] = 
{
	Location_t4202627189::get_offset_of_U3CtextU3Ek__BackingField_0(),
	Location_t4202627189::get_offset_of_U3CentitiesU3Ek__BackingField_1(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
