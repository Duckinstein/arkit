﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// OSCsharp.Data.OscBundle
struct OscBundle_t1126010605;
// OSCsharp.Data.OscTimeTag
struct OscTimeTag_t625345318;
// OSCsharp.Data.OscPacket
struct OscPacket_t504761797;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.ArgumentException
struct ArgumentException_t3259014390;
// System.Exception
struct Exception_t1927440687;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;
// OSCsharp.Data.OscMessage
struct OscMessage_t2764280154;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.Type
struct Type_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;
// System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
struct Dictionary_2_t1663937576;
// System.Collections.Generic.IList`1<System.Object>
struct IList_1_t3230389896;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>
struct ReadOnlyCollection_1_t2875234987;
// OSCsharp.Net.OscBundleReceivedEventArgs
struct OscBundleReceivedEventArgs_t3673224441;
// System.EventArgs
struct EventArgs_t3289624707;
// OSCsharp.Net.OscMessageReceivedEventArgs
struct OscMessageReceivedEventArgs_t1263041860;
// OSCsharp.Net.OscPacketReceivedEventArgs
struct OscPacketReceivedEventArgs_t3282045269;
// OSCsharp.Net.UDPReceiver
struct UDPReceiver_t3846109956;
// System.EventHandler`1<OSCsharp.Utils.ExceptionEventArgs>
struct EventHandler_1_t3778988004;
// System.Delegate
struct Delegate_t3022476291;
// System.EventHandler`1<OSCsharp.Net.OscMessageReceivedEventArgs>
struct EventHandler_1_t4149316328;
// System.Net.IPAddress
struct IPAddress_t1399971723;
// System.Net.IPEndPoint
struct IPEndPoint_t2615413766;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// System.Net.Sockets.UdpClient
struct UdpClient_t1278197702;
// System.Net.Sockets.Socket
struct Socket_t3821512045;
// System.Net.EndPoint
struct EndPoint_t4156119363;
// OSCsharp.Net.UDPReceiver/UdpState
struct UdpState_t1122067727;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.EventHandler`1<OSCsharp.Net.OscPacketReceivedEventArgs>
struct EventHandler_1_t1873352441;
// System.EventHandler`1<System.Object>
struct EventHandler_1_t1280756467;
// System.EventHandler`1<OSCsharp.Net.OscBundleReceivedEventArgs>
struct EventHandler_1_t2264531613;
// OSCsharp.Utils.ExceptionEventArgs
struct ExceptionEventArgs_t892713536;
// System.IntPtr[]
struct IntPtrU5BU5D_t169632028;
// System.Collections.IDictionary
struct IDictionary_t596158605;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.Collections.Generic.Link[]
struct LinkU5BU5D_t62501539;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_t1241853011;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.Collections.Generic.Dictionary`2/Transform`1<System.String,System.Int32,System.Collections.DictionaryEntry>
struct Transform_1_t2154239285;
// System.Void
struct Void_t1841601450;
// System.Byte
struct Byte_t3683104436;
// System.Double
struct Double_t4078015681;
// System.UInt16
struct UInt16_t986882611;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1572802995;
// System.Type[]
struct TypeU5BU5D_t1664964607;
// System.Reflection.MemberFilter
struct MemberFilter_t3405857066;
// System.Collections.Queue
struct Queue_t1288490777;
// System.Threading.Thread
struct Thread_t241561612;
// System.UInt16[]
struct UInt16U5BU5D_t2527266722;

extern RuntimeClass* OscPacket_t504761797_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1664629291;
extern const uint32_t OscBundle__ctor_m625764563_MetadataUsageId;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern RuntimeClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern RuntimeClass* OscBundle_t1126010605_il2cpp_TypeInfo_var;
extern const RuntimeMethod* OscPacket_ValueFromByteArray_TisString_t_m3665303177_RuntimeMethod_var;
extern const RuntimeMethod* OscPacket_ValueFromByteArray_TisOscTimeTag_t625345318_m2765599720_RuntimeMethod_var;
extern const RuntimeMethod* OscPacket_ValueFromByteArray_TisInt32_t2071877448_m417255044_RuntimeMethod_var;
extern const uint32_t OscBundle_FromByteArray_m2667454531_MetadataUsageId;
extern RuntimeClass* OscTimeTag_t625345318_il2cpp_TypeInfo_var;
extern RuntimeClass* Exception_t1927440687_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1_Add_m4157722533_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_m2375293942_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral3364703947;
extern const uint32_t OscBundle_Append_m347818774_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral372029315;
extern Il2CppCodeGenString* _stringLiteral3377275602;
extern const uint32_t OscMessage__ctor_m2211520241_MetadataUsageId;
extern RuntimeClass* OscMessage_t2764280154_il2cpp_TypeInfo_var;
extern RuntimeClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern RuntimeClass* Int64_t909078037_il2cpp_TypeInfo_var;
extern RuntimeClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern RuntimeClass* Double_t4078015681_il2cpp_TypeInfo_var;
extern RuntimeClass* Char_t3454481338_il2cpp_TypeInfo_var;
extern RuntimeClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern const RuntimeMethod* OscPacket_ValueFromByteArray_TisInt64_t909078037_m2124432849_RuntimeMethod_var;
extern const RuntimeMethod* OscPacket_ValueFromByteArray_TisSingle_t2076509932_m308377574_RuntimeMethod_var;
extern const RuntimeMethod* OscPacket_ValueFromByteArray_TisDouble_t4078015681_m3730957917_RuntimeMethod_var;
extern const RuntimeMethod* OscPacket_ValueFromByteArray_TisByteU5BU5D_t3397334013_m2572900720_RuntimeMethod_var;
extern const RuntimeMethod* OscPacket_ValueFromByteArray_TisChar_t3454481338_m257312846_RuntimeMethod_var;
extern const uint32_t OscMessage_FromByteArray_m4248709050_MetadataUsageId;
extern RuntimeClass* U3CPrivateImplementationDetailsU3EU7B2BD1A3B7U2DFF70U2D4DFDU2D9ABDU2D9AF73AFC1A70U7D_t2052325035_il2cpp_TypeInfo_var;
extern RuntimeClass* Dictionary_2_t3986656710_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Dictionary_2__ctor_m2118310873_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_Add_m1209957957_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_TryGetValue_m2977303364_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral2791486114;
extern Il2CppCodeGenString* _stringLiteral2031971229;
extern Il2CppCodeGenString* _stringLiteral3134835610;
extern Il2CppCodeGenString* _stringLiteral1952587961;
extern Il2CppCodeGenString* _stringLiteral3311046853;
extern Il2CppCodeGenString* _stringLiteral3975212764;
extern Il2CppCodeGenString* _stringLiteral3135498864;
extern Il2CppCodeGenString* _stringLiteral681582338;
extern Il2CppCodeGenString* _stringLiteral3120862801;
extern Il2CppCodeGenString* _stringLiteral191542790;
extern Il2CppCodeGenString* _stringLiteral1729348223;
extern const uint32_t OscMessage_Append_m2811322561_MetadataUsageId;
extern const uint32_t OscPacket_get_LittleEndianByteOrder_m1882071268_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral371857150;
extern const uint32_t OscPacket_set_Address_m1646762965_MetadataUsageId;
extern const RuntimeMethod* List_1_AsReadOnly_m1618299177_RuntimeMethod_var;
extern const uint32_t OscPacket_get_Data_m65815223_MetadataUsageId;
extern const uint32_t OscPacket__cctor_m2285063083_MetadataUsageId;
extern RuntimeClass* List_1_t2058570427_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1__ctor_m310736118_RuntimeMethod_var;
extern const uint32_t OscPacket__ctor_m3636146750_MetadataUsageId;
extern const uint32_t OscPacket_FromByteArray_m476730285_MetadataUsageId;
extern RuntimeClass* BitConverter_t3195628829_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Utility_CopySubArray_TisByte_t3683104436_m936098224_RuntimeMethod_var;
extern const uint32_t OscTimeTag__ctor_m183269774_MetadataUsageId;
extern RuntimeClass* DateTime_t693205669_il2cpp_TypeInfo_var;
extern const uint32_t OscTimeTag_op_Equality_m3410370307_MetadataUsageId;
extern const uint32_t OscTimeTag_op_LessThan_m1978965833_MetadataUsageId;
extern RuntimeClass* TimeSpan_t3430258949_il2cpp_TypeInfo_var;
extern const uint32_t OscTimeTag_IsValidTime_m2988141586_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2402648513;
extern const uint32_t OscTimeTag_Set_m4065929879_MetadataUsageId;
extern const uint32_t OscTimeTag_Equals_m3221546906_MetadataUsageId;
extern const uint32_t OscTimeTag__cctor_m3227110810_MetadataUsageId;
extern RuntimeClass* EventArgs_t3289624707_il2cpp_TypeInfo_var;
extern const uint32_t OscBundleReceivedEventArgs__ctor_m2002404183_MetadataUsageId;
extern const uint32_t OscMessageReceivedEventArgs__ctor_m359379643_MetadataUsageId;
extern const uint32_t OscPacketReceivedEventArgs__ctor_m3190781335_MetadataUsageId;
extern RuntimeClass* EventHandler_1_t3778988004_il2cpp_TypeInfo_var;
extern const uint32_t UDPReceiver_add_ErrorOccured_m1212653731_MetadataUsageId;
extern const uint32_t UDPReceiver_remove_ErrorOccured_m1224019382_MetadataUsageId;
extern RuntimeClass* EventHandler_1_t4149316328_il2cpp_TypeInfo_var;
extern const uint32_t UDPReceiver_add_MessageReceived_m3470532992_MetadataUsageId;
extern const uint32_t UDPReceiver_remove_MessageReceived_m3894025529_MetadataUsageId;
extern RuntimeClass* IPAddress_t1399971723_il2cpp_TypeInfo_var;
extern const uint32_t UDPReceiver__ctor_m3124846498_MetadataUsageId;
extern RuntimeClass* AsyncCallback_t163412349_il2cpp_TypeInfo_var;
extern const RuntimeMethod* UDPReceiver_endReceive_m4073855157_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral1477022020;
extern const uint32_t UDPReceiver__ctor_m896685042_MetadataUsageId;
extern RuntimeClass* IPEndPoint_t2615413766_il2cpp_TypeInfo_var;
extern RuntimeClass* UdpClient_t1278197702_il2cpp_TypeInfo_var;
extern RuntimeClass* Socket_t3821512045_il2cpp_TypeInfo_var;
extern RuntimeClass* UdpState_t1122067727_il2cpp_TypeInfo_var;
extern const uint32_t UDPReceiver_Start_m1033630334_MetadataUsageId;
extern RuntimeClass* IAsyncResult_t1999651008_il2cpp_TypeInfo_var;
extern RuntimeClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern const uint32_t UDPReceiver_endReceive_m4073855157_MetadataUsageId;
extern const uint32_t UDPReceiver_parseData_m902928170_MetadataUsageId;
extern RuntimeClass* OscPacketReceivedEventArgs_t3282045269_il2cpp_TypeInfo_var;
extern const RuntimeMethod* EventHandler_1_Invoke_m929141680_RuntimeMethod_var;
extern const uint32_t UDPReceiver_onPacketReceived_m3250243160_MetadataUsageId;
extern RuntimeClass* OscBundleReceivedEventArgs_t3673224441_il2cpp_TypeInfo_var;
extern RuntimeClass* ICollection_1_t3641524600_il2cpp_TypeInfo_var;
extern RuntimeClass* IList_1_t3230389896_il2cpp_TypeInfo_var;
extern const RuntimeMethod* EventHandler_1_Invoke_m3195521520_RuntimeMethod_var;
extern const uint32_t UDPReceiver_onBundleReceived_m562189912_MetadataUsageId;
extern RuntimeClass* OscMessageReceivedEventArgs_t1263041860_il2cpp_TypeInfo_var;
extern const RuntimeMethod* EventHandler_1_Invoke_m2373711195_RuntimeMethod_var;
extern const uint32_t UDPReceiver_onMessageReceived_m24838384_MetadataUsageId;
extern RuntimeClass* ExceptionEventArgs_t892713536_il2cpp_TypeInfo_var;
extern const RuntimeMethod* EventHandler_1_Invoke_m3855877691_RuntimeMethod_var;
extern const uint32_t UDPReceiver_onError_m295686389_MetadataUsageId;
extern const uint32_t ExceptionEventArgs__ctor_m1196714556_MetadataUsageId;
extern RuntimeClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern const uint32_t Utility_SwapEndian_m3369642783_MetadataUsageId;

struct ByteU5BU5D_t3397334013;
struct CharU5BU5D_t1328083999;


#ifndef U3CMODULEU3E_T3783534218_H
#define U3CMODULEU3E_T3783534218_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t3783534218 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T3783534218_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef EXCEPTION_T1927440687_H
#define EXCEPTION_T1927440687_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t1927440687  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t169632028* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t1927440687 * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t169632028* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t169632028** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t169632028* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___inner_exception_1)); }
	inline Exception_t1927440687 * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t1927440687 ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t1927440687 * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T1927440687_H
#ifndef LIST_1_T2058570427_H
#define LIST_1_T2058570427_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.Object>
struct  List_1_t2058570427  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_t3614634134* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t2058570427, ____items_1)); }
	inline ObjectU5BU5D_t3614634134* get__items_1() const { return ____items_1; }
	inline ObjectU5BU5D_t3614634134** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ObjectU5BU5D_t3614634134* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t2058570427, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t2058570427, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t2058570427_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	ObjectU5BU5D_t3614634134* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t2058570427_StaticFields, ___EmptyArray_4)); }
	inline ObjectU5BU5D_t3614634134* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline ObjectU5BU5D_t3614634134** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(ObjectU5BU5D_t3614634134* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T2058570427_H
#ifndef ENDPOINT_T4156119363_H
#define ENDPOINT_T4156119363_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.EndPoint
struct  EndPoint_t4156119363  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENDPOINT_T4156119363_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef EVENTARGS_T3289624707_H
#define EVENTARGS_T3289624707_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventArgs
struct  EventArgs_t3289624707  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t3289624707_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t3289624707 * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t3289624707_StaticFields, ___Empty_0)); }
	inline EventArgs_t3289624707 * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t3289624707 ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t3289624707 * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTARGS_T3289624707_H
#ifndef READONLYCOLLECTION_1_T2875234987_H
#define READONLYCOLLECTION_1_T2875234987_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>
struct  ReadOnlyCollection_1_t2875234987  : public RuntimeObject
{
public:
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1::list
	RuntimeObject* ___list_0;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(ReadOnlyCollection_1_t2875234987, ___list_0)); }
	inline RuntimeObject* get_list_0() const { return ___list_0; }
	inline RuntimeObject** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(RuntimeObject* value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READONLYCOLLECTION_1_T2875234987_H
#ifndef BITCONVERTER_T3195628829_H
#define BITCONVERTER_T3195628829_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.BitConverter
struct  BitConverter_t3195628829  : public RuntimeObject
{
public:

public:
};

struct BitConverter_t3195628829_StaticFields
{
public:
	// System.Boolean System.BitConverter::SwappedWordsInDouble
	bool ___SwappedWordsInDouble_0;
	// System.Boolean System.BitConverter::IsLittleEndian
	bool ___IsLittleEndian_1;

public:
	inline static int32_t get_offset_of_SwappedWordsInDouble_0() { return static_cast<int32_t>(offsetof(BitConverter_t3195628829_StaticFields, ___SwappedWordsInDouble_0)); }
	inline bool get_SwappedWordsInDouble_0() const { return ___SwappedWordsInDouble_0; }
	inline bool* get_address_of_SwappedWordsInDouble_0() { return &___SwappedWordsInDouble_0; }
	inline void set_SwappedWordsInDouble_0(bool value)
	{
		___SwappedWordsInDouble_0 = value;
	}

	inline static int32_t get_offset_of_IsLittleEndian_1() { return static_cast<int32_t>(offsetof(BitConverter_t3195628829_StaticFields, ___IsLittleEndian_1)); }
	inline bool get_IsLittleEndian_1() const { return ___IsLittleEndian_1; }
	inline bool* get_address_of_IsLittleEndian_1() { return &___IsLittleEndian_1; }
	inline void set_IsLittleEndian_1(bool value)
	{
		___IsLittleEndian_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BITCONVERTER_T3195628829_H
#ifndef MEMBERINFO_T_H
#define MEMBERINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFO_T_H
#ifndef DICTIONARY_2_T3986656710_H
#define DICTIONARY_2_T3986656710_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct  Dictionary_2_t3986656710  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t3030399641* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t62501539* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	StringU5BU5D_t1642385972* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	Int32U5BU5D_t3030399641* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t228987430 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t3986656710, ___table_4)); }
	inline Int32U5BU5D_t3030399641* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t3030399641** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t3030399641* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t3986656710, ___linkSlots_5)); }
	inline LinkU5BU5D_t62501539* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t62501539** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t62501539* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t3986656710, ___keySlots_6)); }
	inline StringU5BU5D_t1642385972* get_keySlots_6() const { return ___keySlots_6; }
	inline StringU5BU5D_t1642385972** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(StringU5BU5D_t1642385972* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t3986656710, ___valueSlots_7)); }
	inline Int32U5BU5D_t3030399641* get_valueSlots_7() const { return ___valueSlots_7; }
	inline Int32U5BU5D_t3030399641** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(Int32U5BU5D_t3030399641* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t3986656710, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t3986656710, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t3986656710, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t3986656710, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t3986656710, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t3986656710, ___serialization_info_13)); }
	inline SerializationInfo_t228987430 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t228987430 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t228987430 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t3986656710, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t3986656710_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t2154239285 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t3986656710_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t2154239285 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t2154239285 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t2154239285 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T3986656710_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3EU7B2BD1A3B7U2DFF70U2D4DFDU2D9ABDU2D9AF73AFC1A70U7D_T2052325035_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3EU7B2BD1A3B7U2DFF70U2D4DFDU2D9ABDU2D9AF73AFC1A70U7D_T2052325035_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>{2BD1A3B7-FF70-4DFD-9ABD-9AF73AFC1A70}
struct  U3CPrivateImplementationDetailsU3EU7B2BD1A3B7U2DFF70U2D4DFDU2D9ABDU2D9AF73AFC1A70U7D_t2052325035  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3EU7B2BD1A3B7U2DFF70U2D4DFDU2D9ABDU2D9AF73AFC1A70U7D_t2052325035_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> <PrivateImplementationDetails>{2BD1A3B7-FF70-4DFD-9ABD-9AF73AFC1A70}::$$method0x6000029-1
	Dictionary_2_t3986656710 * ___U24U24method0x6000029U2D1_0;

public:
	inline static int32_t get_offset_of_U24U24method0x6000029U2D1_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7B2BD1A3B7U2DFF70U2D4DFDU2D9ABDU2D9AF73AFC1A70U7D_t2052325035_StaticFields, ___U24U24method0x6000029U2D1_0)); }
	inline Dictionary_2_t3986656710 * get_U24U24method0x6000029U2D1_0() const { return ___U24U24method0x6000029U2D1_0; }
	inline Dictionary_2_t3986656710 ** get_address_of_U24U24method0x6000029U2D1_0() { return &___U24U24method0x6000029U2D1_0; }
	inline void set_U24U24method0x6000029U2D1_0(Dictionary_2_t3986656710 * value)
	{
		___U24U24method0x6000029U2D1_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24U24method0x6000029U2D1_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3EU7B2BD1A3B7U2DFF70U2D4DFDU2D9ABDU2D9AF73AFC1A70U7D_T2052325035_H
#ifndef UTILITY_T2927649758_H
#define UTILITY_T2927649758_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OSCsharp.Utils.Utility
struct  Utility_t2927649758  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UTILITY_T2927649758_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t1328083999* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t1328083999* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t1328083999** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t1328083999* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef OSCPACKET_T504761797_H
#define OSCPACKET_T504761797_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OSCsharp.Data.OscPacket
struct  OscPacket_t504761797  : public RuntimeObject
{
public:
	// System.String OSCsharp.Data.OscPacket::address
	String_t* ___address_1;
	// System.Collections.Generic.List`1<System.Object> OSCsharp.Data.OscPacket::data
	List_1_t2058570427 * ___data_2;

public:
	inline static int32_t get_offset_of_address_1() { return static_cast<int32_t>(offsetof(OscPacket_t504761797, ___address_1)); }
	inline String_t* get_address_1() const { return ___address_1; }
	inline String_t** get_address_of_address_1() { return &___address_1; }
	inline void set_address_1(String_t* value)
	{
		___address_1 = value;
		Il2CppCodeGenWriteBarrier((&___address_1), value);
	}

	inline static int32_t get_offset_of_data_2() { return static_cast<int32_t>(offsetof(OscPacket_t504761797, ___data_2)); }
	inline List_1_t2058570427 * get_data_2() const { return ___data_2; }
	inline List_1_t2058570427 ** get_address_of_data_2() { return &___data_2; }
	inline void set_data_2(List_1_t2058570427 * value)
	{
		___data_2 = value;
		Il2CppCodeGenWriteBarrier((&___data_2), value);
	}
};

struct OscPacket_t504761797_StaticFields
{
public:
	// System.Boolean OSCsharp.Data.OscPacket::littleEndianByteOrder
	bool ___littleEndianByteOrder_0;

public:
	inline static int32_t get_offset_of_littleEndianByteOrder_0() { return static_cast<int32_t>(offsetof(OscPacket_t504761797_StaticFields, ___littleEndianByteOrder_0)); }
	inline bool get_littleEndianByteOrder_0() const { return ___littleEndianByteOrder_0; }
	inline bool* get_address_of_littleEndianByteOrder_0() { return &___littleEndianByteOrder_0; }
	inline void set_littleEndianByteOrder_0(bool value)
	{
		___littleEndianByteOrder_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OSCPACKET_T504761797_H
#ifndef UDPSTATE_T1122067727_H
#define UDPSTATE_T1122067727_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OSCsharp.Net.UDPReceiver/UdpState
struct  UdpState_t1122067727  : public RuntimeObject
{
public:
	// System.Net.Sockets.UdpClient OSCsharp.Net.UDPReceiver/UdpState::<Client>k__BackingField
	UdpClient_t1278197702 * ___U3CClientU3Ek__BackingField_0;
	// System.Net.IPEndPoint OSCsharp.Net.UDPReceiver/UdpState::<IPEndPoint>k__BackingField
	IPEndPoint_t2615413766 * ___U3CIPEndPointU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CClientU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(UdpState_t1122067727, ___U3CClientU3Ek__BackingField_0)); }
	inline UdpClient_t1278197702 * get_U3CClientU3Ek__BackingField_0() const { return ___U3CClientU3Ek__BackingField_0; }
	inline UdpClient_t1278197702 ** get_address_of_U3CClientU3Ek__BackingField_0() { return &___U3CClientU3Ek__BackingField_0; }
	inline void set_U3CClientU3Ek__BackingField_0(UdpClient_t1278197702 * value)
	{
		___U3CClientU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CClientU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CIPEndPointU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(UdpState_t1122067727, ___U3CIPEndPointU3Ek__BackingField_1)); }
	inline IPEndPoint_t2615413766 * get_U3CIPEndPointU3Ek__BackingField_1() const { return ___U3CIPEndPointU3Ek__BackingField_1; }
	inline IPEndPoint_t2615413766 ** get_address_of_U3CIPEndPointU3Ek__BackingField_1() { return &___U3CIPEndPointU3Ek__BackingField_1; }
	inline void set_U3CIPEndPointU3Ek__BackingField_1(IPEndPoint_t2615413766 * value)
	{
		___U3CIPEndPointU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CIPEndPointU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UDPSTATE_T1122067727_H
#ifndef OSCMESSAGERECEIVEDEVENTARGS_T1263041860_H
#define OSCMESSAGERECEIVEDEVENTARGS_T1263041860_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OSCsharp.Net.OscMessageReceivedEventArgs
struct  OscMessageReceivedEventArgs_t1263041860  : public EventArgs_t3289624707
{
public:
	// OSCsharp.Data.OscMessage OSCsharp.Net.OscMessageReceivedEventArgs::<Message>k__BackingField
	OscMessage_t2764280154 * ___U3CMessageU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CMessageU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(OscMessageReceivedEventArgs_t1263041860, ___U3CMessageU3Ek__BackingField_1)); }
	inline OscMessage_t2764280154 * get_U3CMessageU3Ek__BackingField_1() const { return ___U3CMessageU3Ek__BackingField_1; }
	inline OscMessage_t2764280154 ** get_address_of_U3CMessageU3Ek__BackingField_1() { return &___U3CMessageU3Ek__BackingField_1; }
	inline void set_U3CMessageU3Ek__BackingField_1(OscMessage_t2764280154 * value)
	{
		___U3CMessageU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMessageU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OSCMESSAGERECEIVEDEVENTARGS_T1263041860_H
#ifndef SYSTEMEXCEPTION_T3877406272_H
#define SYSTEMEXCEPTION_T3877406272_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t3877406272  : public Exception_t1927440687
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T3877406272_H
#ifndef TIMESPAN_T3430258949_H
#define TIMESPAN_T3430258949_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t3430258949 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_8;

public:
	inline static int32_t get_offset_of__ticks_8() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949, ____ticks_8)); }
	inline int64_t get__ticks_8() const { return ____ticks_8; }
	inline int64_t* get_address_of__ticks_8() { return &____ticks_8; }
	inline void set__ticks_8(int64_t value)
	{
		____ticks_8 = value;
	}
};

struct TimeSpan_t3430258949_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t3430258949  ___MaxValue_5;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t3430258949  ___MinValue_6;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t3430258949  ___Zero_7;

public:
	inline static int32_t get_offset_of_MaxValue_5() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949_StaticFields, ___MaxValue_5)); }
	inline TimeSpan_t3430258949  get_MaxValue_5() const { return ___MaxValue_5; }
	inline TimeSpan_t3430258949 * get_address_of_MaxValue_5() { return &___MaxValue_5; }
	inline void set_MaxValue_5(TimeSpan_t3430258949  value)
	{
		___MaxValue_5 = value;
	}

	inline static int32_t get_offset_of_MinValue_6() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949_StaticFields, ___MinValue_6)); }
	inline TimeSpan_t3430258949  get_MinValue_6() const { return ___MinValue_6; }
	inline TimeSpan_t3430258949 * get_address_of_MinValue_6() { return &___MinValue_6; }
	inline void set_MinValue_6(TimeSpan_t3430258949  value)
	{
		___MinValue_6 = value;
	}

	inline static int32_t get_offset_of_Zero_7() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949_StaticFields, ___Zero_7)); }
	inline TimeSpan_t3430258949  get_Zero_7() const { return ___Zero_7; }
	inline TimeSpan_t3430258949 * get_address_of_Zero_7() { return &___Zero_7; }
	inline void set_Zero_7(TimeSpan_t3430258949  value)
	{
		___Zero_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T3430258949_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t1328083999* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t1328083999* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t1328083999** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t1328083999* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef IPENDPOINT_T2615413766_H
#define IPENDPOINT_T2615413766_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.IPEndPoint
struct  IPEndPoint_t2615413766  : public EndPoint_t4156119363
{
public:
	// System.Net.IPAddress System.Net.IPEndPoint::address
	IPAddress_t1399971723 * ___address_2;
	// System.Int32 System.Net.IPEndPoint::port
	int32_t ___port_3;

public:
	inline static int32_t get_offset_of_address_2() { return static_cast<int32_t>(offsetof(IPEndPoint_t2615413766, ___address_2)); }
	inline IPAddress_t1399971723 * get_address_2() const { return ___address_2; }
	inline IPAddress_t1399971723 ** get_address_of_address_2() { return &___address_2; }
	inline void set_address_2(IPAddress_t1399971723 * value)
	{
		___address_2 = value;
		Il2CppCodeGenWriteBarrier((&___address_2), value);
	}

	inline static int32_t get_offset_of_port_3() { return static_cast<int32_t>(offsetof(IPEndPoint_t2615413766, ___port_3)); }
	inline int32_t get_port_3() const { return ___port_3; }
	inline int32_t* get_address_of_port_3() { return &___port_3; }
	inline void set_port_3(int32_t value)
	{
		___port_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IPENDPOINT_T2615413766_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	IntPtr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline IntPtr_t get_Zero_1() const { return ___Zero_1; }
	inline IntPtr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(IntPtr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef OSCPACKETRECEIVEDEVENTARGS_T3282045269_H
#define OSCPACKETRECEIVEDEVENTARGS_T3282045269_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OSCsharp.Net.OscPacketReceivedEventArgs
struct  OscPacketReceivedEventArgs_t3282045269  : public EventArgs_t3289624707
{
public:
	// OSCsharp.Data.OscPacket OSCsharp.Net.OscPacketReceivedEventArgs::<Packet>k__BackingField
	OscPacket_t504761797 * ___U3CPacketU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CPacketU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(OscPacketReceivedEventArgs_t3282045269, ___U3CPacketU3Ek__BackingField_1)); }
	inline OscPacket_t504761797 * get_U3CPacketU3Ek__BackingField_1() const { return ___U3CPacketU3Ek__BackingField_1; }
	inline OscPacket_t504761797 ** get_address_of_U3CPacketU3Ek__BackingField_1() { return &___U3CPacketU3Ek__BackingField_1; }
	inline void set_U3CPacketU3Ek__BackingField_1(OscPacket_t504761797 * value)
	{
		___U3CPacketU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPacketU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OSCPACKETRECEIVEDEVENTARGS_T3282045269_H
#ifndef OSCBUNDLERECEIVEDEVENTARGS_T3673224441_H
#define OSCBUNDLERECEIVEDEVENTARGS_T3673224441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OSCsharp.Net.OscBundleReceivedEventArgs
struct  OscBundleReceivedEventArgs_t3673224441  : public EventArgs_t3289624707
{
public:
	// OSCsharp.Data.OscBundle OSCsharp.Net.OscBundleReceivedEventArgs::<Bundle>k__BackingField
	OscBundle_t1126010605 * ___U3CBundleU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CBundleU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(OscBundleReceivedEventArgs_t3673224441, ___U3CBundleU3Ek__BackingField_1)); }
	inline OscBundle_t1126010605 * get_U3CBundleU3Ek__BackingField_1() const { return ___U3CBundleU3Ek__BackingField_1; }
	inline OscBundle_t1126010605 ** get_address_of_U3CBundleU3Ek__BackingField_1() { return &___U3CBundleU3Ek__BackingField_1; }
	inline void set_U3CBundleU3Ek__BackingField_1(OscBundle_t1126010605 * value)
	{
		___U3CBundleU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBundleU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OSCBUNDLERECEIVEDEVENTARGS_T3673224441_H
#ifndef EXCEPTIONEVENTARGS_T892713536_H
#define EXCEPTIONEVENTARGS_T892713536_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OSCsharp.Utils.ExceptionEventArgs
struct  ExceptionEventArgs_t892713536  : public EventArgs_t3289624707
{
public:
	// System.Exception OSCsharp.Utils.ExceptionEventArgs::<Exception>k__BackingField
	Exception_t1927440687 * ___U3CExceptionU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CExceptionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ExceptionEventArgs_t892713536, ___U3CExceptionU3Ek__BackingField_1)); }
	inline Exception_t1927440687 * get_U3CExceptionU3Ek__BackingField_1() const { return ___U3CExceptionU3Ek__BackingField_1; }
	inline Exception_t1927440687 ** get_address_of_U3CExceptionU3Ek__BackingField_1() { return &___U3CExceptionU3Ek__BackingField_1; }
	inline void set_U3CExceptionU3Ek__BackingField_1(Exception_t1927440687 * value)
	{
		___U3CExceptionU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CExceptionU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTIONEVENTARGS_T892713536_H
#ifndef BYTE_T3683104436_H
#define BYTE_T3683104436_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Byte
struct  Byte_t3683104436 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Byte_t3683104436, ___m_value_2)); }
	inline uint8_t get_m_value_2() const { return ___m_value_2; }
	inline uint8_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(uint8_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTE_T3683104436_H
#ifndef VOID_T1841601450_H
#define VOID_T1841601450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1841601450 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1841601450_H
#ifndef BOOLEAN_T3825574718_H
#define BOOLEAN_T3825574718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t3825574718 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t3825574718, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t3825574718_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t3825574718_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t3825574718_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T3825574718_H
#ifndef OSCMESSAGE_T2764280154_H
#define OSCMESSAGE_T2764280154_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OSCsharp.Data.OscMessage
struct  OscMessage_t2764280154  : public OscPacket_t504761797
{
public:
	// System.String OSCsharp.Data.OscMessage::typeTag
	String_t* ___typeTag_19;

public:
	inline static int32_t get_offset_of_typeTag_19() { return static_cast<int32_t>(offsetof(OscMessage_t2764280154, ___typeTag_19)); }
	inline String_t* get_typeTag_19() const { return ___typeTag_19; }
	inline String_t** get_address_of_typeTag_19() { return &___typeTag_19; }
	inline void set_typeTag_19(String_t* value)
	{
		___typeTag_19 = value;
		Il2CppCodeGenWriteBarrier((&___typeTag_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OSCMESSAGE_T2764280154_H
#ifndef CHAR_T3454481338_H
#define CHAR_T3454481338_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Char
struct  Char_t3454481338 
{
public:
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Char_t3454481338, ___m_value_2)); }
	inline Il2CppChar get_m_value_2() const { return ___m_value_2; }
	inline Il2CppChar* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(Il2CppChar value)
	{
		___m_value_2 = value;
	}
};

struct Char_t3454481338_StaticFields
{
public:
	// System.Byte* System.Char::category_data
	uint8_t* ___category_data_3;
	// System.Byte* System.Char::numeric_data
	uint8_t* ___numeric_data_4;
	// System.Double* System.Char::numeric_data_values
	double* ___numeric_data_values_5;
	// System.UInt16* System.Char::to_lower_data_low
	uint16_t* ___to_lower_data_low_6;
	// System.UInt16* System.Char::to_lower_data_high
	uint16_t* ___to_lower_data_high_7;
	// System.UInt16* System.Char::to_upper_data_low
	uint16_t* ___to_upper_data_low_8;
	// System.UInt16* System.Char::to_upper_data_high
	uint16_t* ___to_upper_data_high_9;

public:
	inline static int32_t get_offset_of_category_data_3() { return static_cast<int32_t>(offsetof(Char_t3454481338_StaticFields, ___category_data_3)); }
	inline uint8_t* get_category_data_3() const { return ___category_data_3; }
	inline uint8_t** get_address_of_category_data_3() { return &___category_data_3; }
	inline void set_category_data_3(uint8_t* value)
	{
		___category_data_3 = value;
	}

	inline static int32_t get_offset_of_numeric_data_4() { return static_cast<int32_t>(offsetof(Char_t3454481338_StaticFields, ___numeric_data_4)); }
	inline uint8_t* get_numeric_data_4() const { return ___numeric_data_4; }
	inline uint8_t** get_address_of_numeric_data_4() { return &___numeric_data_4; }
	inline void set_numeric_data_4(uint8_t* value)
	{
		___numeric_data_4 = value;
	}

	inline static int32_t get_offset_of_numeric_data_values_5() { return static_cast<int32_t>(offsetof(Char_t3454481338_StaticFields, ___numeric_data_values_5)); }
	inline double* get_numeric_data_values_5() const { return ___numeric_data_values_5; }
	inline double** get_address_of_numeric_data_values_5() { return &___numeric_data_values_5; }
	inline void set_numeric_data_values_5(double* value)
	{
		___numeric_data_values_5 = value;
	}

	inline static int32_t get_offset_of_to_lower_data_low_6() { return static_cast<int32_t>(offsetof(Char_t3454481338_StaticFields, ___to_lower_data_low_6)); }
	inline uint16_t* get_to_lower_data_low_6() const { return ___to_lower_data_low_6; }
	inline uint16_t** get_address_of_to_lower_data_low_6() { return &___to_lower_data_low_6; }
	inline void set_to_lower_data_low_6(uint16_t* value)
	{
		___to_lower_data_low_6 = value;
	}

	inline static int32_t get_offset_of_to_lower_data_high_7() { return static_cast<int32_t>(offsetof(Char_t3454481338_StaticFields, ___to_lower_data_high_7)); }
	inline uint16_t* get_to_lower_data_high_7() const { return ___to_lower_data_high_7; }
	inline uint16_t** get_address_of_to_lower_data_high_7() { return &___to_lower_data_high_7; }
	inline void set_to_lower_data_high_7(uint16_t* value)
	{
		___to_lower_data_high_7 = value;
	}

	inline static int32_t get_offset_of_to_upper_data_low_8() { return static_cast<int32_t>(offsetof(Char_t3454481338_StaticFields, ___to_upper_data_low_8)); }
	inline uint16_t* get_to_upper_data_low_8() const { return ___to_upper_data_low_8; }
	inline uint16_t** get_address_of_to_upper_data_low_8() { return &___to_upper_data_low_8; }
	inline void set_to_upper_data_low_8(uint16_t* value)
	{
		___to_upper_data_low_8 = value;
	}

	inline static int32_t get_offset_of_to_upper_data_high_9() { return static_cast<int32_t>(offsetof(Char_t3454481338_StaticFields, ___to_upper_data_high_9)); }
	inline uint16_t* get_to_upper_data_high_9() const { return ___to_upper_data_high_9; }
	inline uint16_t** get_address_of_to_upper_data_high_9() { return &___to_upper_data_high_9; }
	inline void set_to_upper_data_high_9(uint16_t* value)
	{
		___to_upper_data_high_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHAR_T3454481338_H
#ifndef INT32_T2071877448_H
#define INT32_T2071877448_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2071877448 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2071877448, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2071877448_H
#ifndef INT64_T909078037_H
#define INT64_T909078037_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int64
struct  Int64_t909078037 
{
public:
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int64_t909078037, ___m_value_2)); }
	inline int64_t get_m_value_2() const { return ___m_value_2; }
	inline int64_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int64_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT64_T909078037_H
#ifndef DOUBLE_T4078015681_H
#define DOUBLE_T4078015681_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Double
struct  Double_t4078015681 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_13;

public:
	inline static int32_t get_offset_of_m_value_13() { return static_cast<int32_t>(offsetof(Double_t4078015681, ___m_value_13)); }
	inline double get_m_value_13() const { return ___m_value_13; }
	inline double* get_address_of_m_value_13() { return &___m_value_13; }
	inline void set_m_value_13(double value)
	{
		___m_value_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLE_T4078015681_H
#ifndef OSCBUNDLE_T1126010605_H
#define OSCBUNDLE_T1126010605_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OSCsharp.Data.OscBundle
struct  OscBundle_t1126010605  : public OscPacket_t504761797
{
public:
	// OSCsharp.Data.OscTimeTag OSCsharp.Data.OscBundle::timeStamp
	OscTimeTag_t625345318 * ___timeStamp_4;

public:
	inline static int32_t get_offset_of_timeStamp_4() { return static_cast<int32_t>(offsetof(OscBundle_t1126010605, ___timeStamp_4)); }
	inline OscTimeTag_t625345318 * get_timeStamp_4() const { return ___timeStamp_4; }
	inline OscTimeTag_t625345318 ** get_address_of_timeStamp_4() { return &___timeStamp_4; }
	inline void set_timeStamp_4(OscTimeTag_t625345318 * value)
	{
		___timeStamp_4 = value;
		Il2CppCodeGenWriteBarrier((&___timeStamp_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OSCBUNDLE_T1126010605_H
#ifndef UINT32_T2149682021_H
#define UINT32_T2149682021_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt32
struct  UInt32_t2149682021 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(UInt32_t2149682021, ___m_value_2)); }
	inline uint32_t get_m_value_2() const { return ___m_value_2; }
	inline uint32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(uint32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32_T2149682021_H
#ifndef SINGLE_T2076509932_H
#define SINGLE_T2076509932_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t2076509932 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t2076509932, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T2076509932_H
#ifndef PROTOCOLTYPE_T2178963134_H
#define PROTOCOLTYPE_T2178963134_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Sockets.ProtocolType
struct  ProtocolType_t2178963134 
{
public:
	// System.Int32 System.Net.Sockets.ProtocolType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ProtocolType_t2178963134, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROTOCOLTYPE_T2178963134_H
#ifndef SOCKETOPTIONNAME_T1089121285_H
#define SOCKETOPTIONNAME_T1089121285_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Sockets.SocketOptionName
struct  SocketOptionName_t1089121285 
{
public:
	// System.Int32 System.Net.Sockets.SocketOptionName::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SocketOptionName_t1089121285, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOCKETOPTIONNAME_T1089121285_H
#ifndef SOCKETOPTIONLEVEL_T1505247880_H
#define SOCKETOPTIONLEVEL_T1505247880_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Sockets.SocketOptionLevel
struct  SocketOptionLevel_t1505247880 
{
public:
	// System.Int32 System.Net.Sockets.SocketOptionLevel::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SocketOptionLevel_t1505247880, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOCKETOPTIONLEVEL_T1505247880_H
#ifndef RUNTIMETYPEHANDLE_T2330101084_H
#define RUNTIMETYPEHANDLE_T2330101084_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t2330101084 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	IntPtr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t2330101084, ___value_0)); }
	inline IntPtr_t get_value_0() const { return ___value_0; }
	inline IntPtr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(IntPtr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMETYPEHANDLE_T2330101084_H
#ifndef BINDINGFLAGS_T1082350898_H
#define BINDINGFLAGS_T1082350898_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_t1082350898 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BindingFlags_t1082350898, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_T1082350898_H
#ifndef INVALIDOPERATIONEXCEPTION_T721527559_H
#define INVALIDOPERATIONEXCEPTION_T721527559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.InvalidOperationException
struct  InvalidOperationException_t721527559  : public SystemException_t3877406272
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVALIDOPERATIONEXCEPTION_T721527559_H
#ifndef ARGUMENTEXCEPTION_T3259014390_H
#define ARGUMENTEXCEPTION_T3259014390_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArgumentException
struct  ArgumentException_t3259014390  : public SystemException_t3877406272
{
public:
	// System.String System.ArgumentException::param_name
	String_t* ___param_name_12;

public:
	inline static int32_t get_offset_of_param_name_12() { return static_cast<int32_t>(offsetof(ArgumentException_t3259014390, ___param_name_12)); }
	inline String_t* get_param_name_12() const { return ___param_name_12; }
	inline String_t** get_address_of_param_name_12() { return &___param_name_12; }
	inline void set_param_name_12(String_t* value)
	{
		___param_name_12 = value;
		Il2CppCodeGenWriteBarrier((&___param_name_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTEXCEPTION_T3259014390_H
#ifndef DATETIMEKIND_T2186819611_H
#define DATETIMEKIND_T2186819611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeKind
struct  DateTimeKind_t2186819611 
{
public:
	// System.Int32 System.DateTimeKind::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeKind_t2186819611, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEKIND_T2186819611_H
#ifndef DELEGATE_T3022476291_H
#define DELEGATE_T3022476291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t3022476291  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	IntPtr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	IntPtr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	IntPtr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	IntPtr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1572802995 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___invoke_impl_1)); }
	inline IntPtr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline IntPtr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(IntPtr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_3)); }
	inline IntPtr_t get_method_3() const { return ___method_3; }
	inline IntPtr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(IntPtr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___delegate_trampoline_4)); }
	inline IntPtr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline IntPtr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(IntPtr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_code_5)); }
	inline IntPtr_t get_method_code_5() const { return ___method_code_5; }
	inline IntPtr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(IntPtr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___data_8)); }
	inline DelegateData_t1572802995 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1572802995 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1572802995 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T3022476291_H
#ifndef TRANSMISSIONTYPE_T529366678_H
#define TRANSMISSIONTYPE_T529366678_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OSCsharp.Net.TransmissionType
struct  TransmissionType_t529366678 
{
public:
	// System.Int32 OSCsharp.Net.TransmissionType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TransmissionType_t529366678, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSMISSIONTYPE_T529366678_H
#ifndef SOCKETTYPE_T1143498533_H
#define SOCKETTYPE_T1143498533_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Sockets.SocketType
struct  SocketType_t1143498533 
{
public:
	// System.Int32 System.Net.Sockets.SocketType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SocketType_t1143498533, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOCKETTYPE_T1143498533_H
#ifndef ADDRESSFAMILY_T303362630_H
#define ADDRESSFAMILY_T303362630_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Sockets.AddressFamily
struct  AddressFamily_t303362630 
{
public:
	// System.Int32 System.Net.Sockets.AddressFamily::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AddressFamily_t303362630, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDRESSFAMILY_T303362630_H
#ifndef DATETIME_T693205669_H
#define DATETIME_T693205669_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t693205669 
{
public:
	// System.TimeSpan System.DateTime::ticks
	TimeSpan_t3430258949  ___ticks_10;
	// System.DateTimeKind System.DateTime::kind
	int32_t ___kind_11;

public:
	inline static int32_t get_offset_of_ticks_10() { return static_cast<int32_t>(offsetof(DateTime_t693205669, ___ticks_10)); }
	inline TimeSpan_t3430258949  get_ticks_10() const { return ___ticks_10; }
	inline TimeSpan_t3430258949 * get_address_of_ticks_10() { return &___ticks_10; }
	inline void set_ticks_10(TimeSpan_t3430258949  value)
	{
		___ticks_10 = value;
	}

	inline static int32_t get_offset_of_kind_11() { return static_cast<int32_t>(offsetof(DateTime_t693205669, ___kind_11)); }
	inline int32_t get_kind_11() const { return ___kind_11; }
	inline int32_t* get_address_of_kind_11() { return &___kind_11; }
	inline void set_kind_11(int32_t value)
	{
		___kind_11 = value;
	}
};

struct DateTime_t693205669_StaticFields
{
public:
	// System.DateTime System.DateTime::MaxValue
	DateTime_t693205669  ___MaxValue_12;
	// System.DateTime System.DateTime::MinValue
	DateTime_t693205669  ___MinValue_13;
	// System.String[] System.DateTime::ParseTimeFormats
	StringU5BU5D_t1642385972* ___ParseTimeFormats_14;
	// System.String[] System.DateTime::ParseYearDayMonthFormats
	StringU5BU5D_t1642385972* ___ParseYearDayMonthFormats_15;
	// System.String[] System.DateTime::ParseYearMonthDayFormats
	StringU5BU5D_t1642385972* ___ParseYearMonthDayFormats_16;
	// System.String[] System.DateTime::ParseDayMonthYearFormats
	StringU5BU5D_t1642385972* ___ParseDayMonthYearFormats_17;
	// System.String[] System.DateTime::ParseMonthDayYearFormats
	StringU5BU5D_t1642385972* ___ParseMonthDayYearFormats_18;
	// System.String[] System.DateTime::MonthDayShortFormats
	StringU5BU5D_t1642385972* ___MonthDayShortFormats_19;
	// System.String[] System.DateTime::DayMonthShortFormats
	StringU5BU5D_t1642385972* ___DayMonthShortFormats_20;
	// System.Int32[] System.DateTime::daysmonth
	Int32U5BU5D_t3030399641* ___daysmonth_21;
	// System.Int32[] System.DateTime::daysmonthleap
	Int32U5BU5D_t3030399641* ___daysmonthleap_22;
	// System.Object System.DateTime::to_local_time_span_object
	RuntimeObject * ___to_local_time_span_object_23;
	// System.Int64 System.DateTime::last_now
	int64_t ___last_now_24;

public:
	inline static int32_t get_offset_of_MaxValue_12() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___MaxValue_12)); }
	inline DateTime_t693205669  get_MaxValue_12() const { return ___MaxValue_12; }
	inline DateTime_t693205669 * get_address_of_MaxValue_12() { return &___MaxValue_12; }
	inline void set_MaxValue_12(DateTime_t693205669  value)
	{
		___MaxValue_12 = value;
	}

	inline static int32_t get_offset_of_MinValue_13() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___MinValue_13)); }
	inline DateTime_t693205669  get_MinValue_13() const { return ___MinValue_13; }
	inline DateTime_t693205669 * get_address_of_MinValue_13() { return &___MinValue_13; }
	inline void set_MinValue_13(DateTime_t693205669  value)
	{
		___MinValue_13 = value;
	}

	inline static int32_t get_offset_of_ParseTimeFormats_14() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseTimeFormats_14)); }
	inline StringU5BU5D_t1642385972* get_ParseTimeFormats_14() const { return ___ParseTimeFormats_14; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseTimeFormats_14() { return &___ParseTimeFormats_14; }
	inline void set_ParseTimeFormats_14(StringU5BU5D_t1642385972* value)
	{
		___ParseTimeFormats_14 = value;
		Il2CppCodeGenWriteBarrier((&___ParseTimeFormats_14), value);
	}

	inline static int32_t get_offset_of_ParseYearDayMonthFormats_15() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseYearDayMonthFormats_15)); }
	inline StringU5BU5D_t1642385972* get_ParseYearDayMonthFormats_15() const { return ___ParseYearDayMonthFormats_15; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseYearDayMonthFormats_15() { return &___ParseYearDayMonthFormats_15; }
	inline void set_ParseYearDayMonthFormats_15(StringU5BU5D_t1642385972* value)
	{
		___ParseYearDayMonthFormats_15 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearDayMonthFormats_15), value);
	}

	inline static int32_t get_offset_of_ParseYearMonthDayFormats_16() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseYearMonthDayFormats_16)); }
	inline StringU5BU5D_t1642385972* get_ParseYearMonthDayFormats_16() const { return ___ParseYearMonthDayFormats_16; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseYearMonthDayFormats_16() { return &___ParseYearMonthDayFormats_16; }
	inline void set_ParseYearMonthDayFormats_16(StringU5BU5D_t1642385972* value)
	{
		___ParseYearMonthDayFormats_16 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearMonthDayFormats_16), value);
	}

	inline static int32_t get_offset_of_ParseDayMonthYearFormats_17() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseDayMonthYearFormats_17)); }
	inline StringU5BU5D_t1642385972* get_ParseDayMonthYearFormats_17() const { return ___ParseDayMonthYearFormats_17; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseDayMonthYearFormats_17() { return &___ParseDayMonthYearFormats_17; }
	inline void set_ParseDayMonthYearFormats_17(StringU5BU5D_t1642385972* value)
	{
		___ParseDayMonthYearFormats_17 = value;
		Il2CppCodeGenWriteBarrier((&___ParseDayMonthYearFormats_17), value);
	}

	inline static int32_t get_offset_of_ParseMonthDayYearFormats_18() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseMonthDayYearFormats_18)); }
	inline StringU5BU5D_t1642385972* get_ParseMonthDayYearFormats_18() const { return ___ParseMonthDayYearFormats_18; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseMonthDayYearFormats_18() { return &___ParseMonthDayYearFormats_18; }
	inline void set_ParseMonthDayYearFormats_18(StringU5BU5D_t1642385972* value)
	{
		___ParseMonthDayYearFormats_18 = value;
		Il2CppCodeGenWriteBarrier((&___ParseMonthDayYearFormats_18), value);
	}

	inline static int32_t get_offset_of_MonthDayShortFormats_19() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___MonthDayShortFormats_19)); }
	inline StringU5BU5D_t1642385972* get_MonthDayShortFormats_19() const { return ___MonthDayShortFormats_19; }
	inline StringU5BU5D_t1642385972** get_address_of_MonthDayShortFormats_19() { return &___MonthDayShortFormats_19; }
	inline void set_MonthDayShortFormats_19(StringU5BU5D_t1642385972* value)
	{
		___MonthDayShortFormats_19 = value;
		Il2CppCodeGenWriteBarrier((&___MonthDayShortFormats_19), value);
	}

	inline static int32_t get_offset_of_DayMonthShortFormats_20() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___DayMonthShortFormats_20)); }
	inline StringU5BU5D_t1642385972* get_DayMonthShortFormats_20() const { return ___DayMonthShortFormats_20; }
	inline StringU5BU5D_t1642385972** get_address_of_DayMonthShortFormats_20() { return &___DayMonthShortFormats_20; }
	inline void set_DayMonthShortFormats_20(StringU5BU5D_t1642385972* value)
	{
		___DayMonthShortFormats_20 = value;
		Il2CppCodeGenWriteBarrier((&___DayMonthShortFormats_20), value);
	}

	inline static int32_t get_offset_of_daysmonth_21() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___daysmonth_21)); }
	inline Int32U5BU5D_t3030399641* get_daysmonth_21() const { return ___daysmonth_21; }
	inline Int32U5BU5D_t3030399641** get_address_of_daysmonth_21() { return &___daysmonth_21; }
	inline void set_daysmonth_21(Int32U5BU5D_t3030399641* value)
	{
		___daysmonth_21 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonth_21), value);
	}

	inline static int32_t get_offset_of_daysmonthleap_22() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___daysmonthleap_22)); }
	inline Int32U5BU5D_t3030399641* get_daysmonthleap_22() const { return ___daysmonthleap_22; }
	inline Int32U5BU5D_t3030399641** get_address_of_daysmonthleap_22() { return &___daysmonthleap_22; }
	inline void set_daysmonthleap_22(Int32U5BU5D_t3030399641* value)
	{
		___daysmonthleap_22 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonthleap_22), value);
	}

	inline static int32_t get_offset_of_to_local_time_span_object_23() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___to_local_time_span_object_23)); }
	inline RuntimeObject * get_to_local_time_span_object_23() const { return ___to_local_time_span_object_23; }
	inline RuntimeObject ** get_address_of_to_local_time_span_object_23() { return &___to_local_time_span_object_23; }
	inline void set_to_local_time_span_object_23(RuntimeObject * value)
	{
		___to_local_time_span_object_23 = value;
		Il2CppCodeGenWriteBarrier((&___to_local_time_span_object_23), value);
	}

	inline static int32_t get_offset_of_last_now_24() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___last_now_24)); }
	inline int64_t get_last_now_24() const { return ___last_now_24; }
	inline int64_t* get_address_of_last_now_24() { return &___last_now_24; }
	inline void set_last_now_24(int64_t value)
	{
		___last_now_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T693205669_H
#ifndef UDPRECEIVER_T3846109956_H
#define UDPRECEIVER_T3846109956_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OSCsharp.Net.UDPReceiver
struct  UDPReceiver_t3846109956  : public RuntimeObject
{
public:
	// System.EventHandler`1<OSCsharp.Net.OscPacketReceivedEventArgs> OSCsharp.Net.UDPReceiver::PacketReceived
	EventHandler_1_t1873352441 * ___PacketReceived_0;
	// System.EventHandler`1<OSCsharp.Net.OscBundleReceivedEventArgs> OSCsharp.Net.UDPReceiver::BundleReceived
	EventHandler_1_t2264531613 * ___BundleReceived_1;
	// System.EventHandler`1<OSCsharp.Utils.ExceptionEventArgs> OSCsharp.Net.UDPReceiver::ErrorOccured
	EventHandler_1_t3778988004 * ___ErrorOccured_2;
	// System.EventHandler`1<OSCsharp.Net.OscMessageReceivedEventArgs> OSCsharp.Net.UDPReceiver::messageReceivedInvoker
	EventHandler_1_t4149316328 * ___messageReceivedInvoker_3;
	// System.Net.Sockets.UdpClient OSCsharp.Net.UDPReceiver::udpClient
	UdpClient_t1278197702 * ___udpClient_4;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) OSCsharp.Net.UDPReceiver::acceptingConnections
	bool ___acceptingConnections_5;
	// System.AsyncCallback OSCsharp.Net.UDPReceiver::callback
	AsyncCallback_t163412349 * ___callback_6;
	// System.Net.IPAddress OSCsharp.Net.UDPReceiver::<IPAddress>k__BackingField
	IPAddress_t1399971723 * ___U3CIPAddressU3Ek__BackingField_7;
	// System.Int32 OSCsharp.Net.UDPReceiver::<Port>k__BackingField
	int32_t ___U3CPortU3Ek__BackingField_8;
	// System.Net.IPAddress OSCsharp.Net.UDPReceiver::<MulticastAddress>k__BackingField
	IPAddress_t1399971723 * ___U3CMulticastAddressU3Ek__BackingField_9;
	// System.Net.IPEndPoint OSCsharp.Net.UDPReceiver::<IPEndPoint>k__BackingField
	IPEndPoint_t2615413766 * ___U3CIPEndPointU3Ek__BackingField_10;
	// OSCsharp.Net.TransmissionType OSCsharp.Net.UDPReceiver::<TransmissionType>k__BackingField
	int32_t ___U3CTransmissionTypeU3Ek__BackingField_11;
	// System.Boolean OSCsharp.Net.UDPReceiver::<ConsumeParsingExceptions>k__BackingField
	bool ___U3CConsumeParsingExceptionsU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_PacketReceived_0() { return static_cast<int32_t>(offsetof(UDPReceiver_t3846109956, ___PacketReceived_0)); }
	inline EventHandler_1_t1873352441 * get_PacketReceived_0() const { return ___PacketReceived_0; }
	inline EventHandler_1_t1873352441 ** get_address_of_PacketReceived_0() { return &___PacketReceived_0; }
	inline void set_PacketReceived_0(EventHandler_1_t1873352441 * value)
	{
		___PacketReceived_0 = value;
		Il2CppCodeGenWriteBarrier((&___PacketReceived_0), value);
	}

	inline static int32_t get_offset_of_BundleReceived_1() { return static_cast<int32_t>(offsetof(UDPReceiver_t3846109956, ___BundleReceived_1)); }
	inline EventHandler_1_t2264531613 * get_BundleReceived_1() const { return ___BundleReceived_1; }
	inline EventHandler_1_t2264531613 ** get_address_of_BundleReceived_1() { return &___BundleReceived_1; }
	inline void set_BundleReceived_1(EventHandler_1_t2264531613 * value)
	{
		___BundleReceived_1 = value;
		Il2CppCodeGenWriteBarrier((&___BundleReceived_1), value);
	}

	inline static int32_t get_offset_of_ErrorOccured_2() { return static_cast<int32_t>(offsetof(UDPReceiver_t3846109956, ___ErrorOccured_2)); }
	inline EventHandler_1_t3778988004 * get_ErrorOccured_2() const { return ___ErrorOccured_2; }
	inline EventHandler_1_t3778988004 ** get_address_of_ErrorOccured_2() { return &___ErrorOccured_2; }
	inline void set_ErrorOccured_2(EventHandler_1_t3778988004 * value)
	{
		___ErrorOccured_2 = value;
		Il2CppCodeGenWriteBarrier((&___ErrorOccured_2), value);
	}

	inline static int32_t get_offset_of_messageReceivedInvoker_3() { return static_cast<int32_t>(offsetof(UDPReceiver_t3846109956, ___messageReceivedInvoker_3)); }
	inline EventHandler_1_t4149316328 * get_messageReceivedInvoker_3() const { return ___messageReceivedInvoker_3; }
	inline EventHandler_1_t4149316328 ** get_address_of_messageReceivedInvoker_3() { return &___messageReceivedInvoker_3; }
	inline void set_messageReceivedInvoker_3(EventHandler_1_t4149316328 * value)
	{
		___messageReceivedInvoker_3 = value;
		Il2CppCodeGenWriteBarrier((&___messageReceivedInvoker_3), value);
	}

	inline static int32_t get_offset_of_udpClient_4() { return static_cast<int32_t>(offsetof(UDPReceiver_t3846109956, ___udpClient_4)); }
	inline UdpClient_t1278197702 * get_udpClient_4() const { return ___udpClient_4; }
	inline UdpClient_t1278197702 ** get_address_of_udpClient_4() { return &___udpClient_4; }
	inline void set_udpClient_4(UdpClient_t1278197702 * value)
	{
		___udpClient_4 = value;
		Il2CppCodeGenWriteBarrier((&___udpClient_4), value);
	}

	inline static int32_t get_offset_of_acceptingConnections_5() { return static_cast<int32_t>(offsetof(UDPReceiver_t3846109956, ___acceptingConnections_5)); }
	inline bool get_acceptingConnections_5() const { return ___acceptingConnections_5; }
	inline bool* get_address_of_acceptingConnections_5() { return &___acceptingConnections_5; }
	inline void set_acceptingConnections_5(bool value)
	{
		___acceptingConnections_5 = value;
	}

	inline static int32_t get_offset_of_callback_6() { return static_cast<int32_t>(offsetof(UDPReceiver_t3846109956, ___callback_6)); }
	inline AsyncCallback_t163412349 * get_callback_6() const { return ___callback_6; }
	inline AsyncCallback_t163412349 ** get_address_of_callback_6() { return &___callback_6; }
	inline void set_callback_6(AsyncCallback_t163412349 * value)
	{
		___callback_6 = value;
		Il2CppCodeGenWriteBarrier((&___callback_6), value);
	}

	inline static int32_t get_offset_of_U3CIPAddressU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(UDPReceiver_t3846109956, ___U3CIPAddressU3Ek__BackingField_7)); }
	inline IPAddress_t1399971723 * get_U3CIPAddressU3Ek__BackingField_7() const { return ___U3CIPAddressU3Ek__BackingField_7; }
	inline IPAddress_t1399971723 ** get_address_of_U3CIPAddressU3Ek__BackingField_7() { return &___U3CIPAddressU3Ek__BackingField_7; }
	inline void set_U3CIPAddressU3Ek__BackingField_7(IPAddress_t1399971723 * value)
	{
		___U3CIPAddressU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CIPAddressU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CPortU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(UDPReceiver_t3846109956, ___U3CPortU3Ek__BackingField_8)); }
	inline int32_t get_U3CPortU3Ek__BackingField_8() const { return ___U3CPortU3Ek__BackingField_8; }
	inline int32_t* get_address_of_U3CPortU3Ek__BackingField_8() { return &___U3CPortU3Ek__BackingField_8; }
	inline void set_U3CPortU3Ek__BackingField_8(int32_t value)
	{
		___U3CPortU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CMulticastAddressU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(UDPReceiver_t3846109956, ___U3CMulticastAddressU3Ek__BackingField_9)); }
	inline IPAddress_t1399971723 * get_U3CMulticastAddressU3Ek__BackingField_9() const { return ___U3CMulticastAddressU3Ek__BackingField_9; }
	inline IPAddress_t1399971723 ** get_address_of_U3CMulticastAddressU3Ek__BackingField_9() { return &___U3CMulticastAddressU3Ek__BackingField_9; }
	inline void set_U3CMulticastAddressU3Ek__BackingField_9(IPAddress_t1399971723 * value)
	{
		___U3CMulticastAddressU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMulticastAddressU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CIPEndPointU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(UDPReceiver_t3846109956, ___U3CIPEndPointU3Ek__BackingField_10)); }
	inline IPEndPoint_t2615413766 * get_U3CIPEndPointU3Ek__BackingField_10() const { return ___U3CIPEndPointU3Ek__BackingField_10; }
	inline IPEndPoint_t2615413766 ** get_address_of_U3CIPEndPointU3Ek__BackingField_10() { return &___U3CIPEndPointU3Ek__BackingField_10; }
	inline void set_U3CIPEndPointU3Ek__BackingField_10(IPEndPoint_t2615413766 * value)
	{
		___U3CIPEndPointU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CIPEndPointU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_U3CTransmissionTypeU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(UDPReceiver_t3846109956, ___U3CTransmissionTypeU3Ek__BackingField_11)); }
	inline int32_t get_U3CTransmissionTypeU3Ek__BackingField_11() const { return ___U3CTransmissionTypeU3Ek__BackingField_11; }
	inline int32_t* get_address_of_U3CTransmissionTypeU3Ek__BackingField_11() { return &___U3CTransmissionTypeU3Ek__BackingField_11; }
	inline void set_U3CTransmissionTypeU3Ek__BackingField_11(int32_t value)
	{
		___U3CTransmissionTypeU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CConsumeParsingExceptionsU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(UDPReceiver_t3846109956, ___U3CConsumeParsingExceptionsU3Ek__BackingField_12)); }
	inline bool get_U3CConsumeParsingExceptionsU3Ek__BackingField_12() const { return ___U3CConsumeParsingExceptionsU3Ek__BackingField_12; }
	inline bool* get_address_of_U3CConsumeParsingExceptionsU3Ek__BackingField_12() { return &___U3CConsumeParsingExceptionsU3Ek__BackingField_12; }
	inline void set_U3CConsumeParsingExceptionsU3Ek__BackingField_12(bool value)
	{
		___U3CConsumeParsingExceptionsU3Ek__BackingField_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UDPRECEIVER_T3846109956_H
#ifndef MULTICASTDELEGATE_T3201952435_H
#define MULTICASTDELEGATE_T3201952435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t3201952435  : public Delegate_t3022476291
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t3201952435 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t3201952435 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___prev_9)); }
	inline MulticastDelegate_t3201952435 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t3201952435 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t3201952435 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___kpm_next_10)); }
	inline MulticastDelegate_t3201952435 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t3201952435 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t3201952435 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T3201952435_H
#ifndef TYPE_T_H
#define TYPE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t2330101084  ____impl_1;

public:
	inline static int32_t get_offset_of__impl_1() { return static_cast<int32_t>(offsetof(Type_t, ____impl_1)); }
	inline RuntimeTypeHandle_t2330101084  get__impl_1() const { return ____impl_1; }
	inline RuntimeTypeHandle_t2330101084 * get_address_of__impl_1() { return &____impl_1; }
	inline void set__impl_1(RuntimeTypeHandle_t2330101084  value)
	{
		____impl_1 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_2;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t1664964607* ___EmptyTypes_3;
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t3405857066 * ___FilterAttribute_4;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t3405857066 * ___FilterName_5;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t3405857066 * ___FilterNameIgnoreCase_6;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_7;

public:
	inline static int32_t get_offset_of_Delimiter_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_2)); }
	inline Il2CppChar get_Delimiter_2() const { return ___Delimiter_2; }
	inline Il2CppChar* get_address_of_Delimiter_2() { return &___Delimiter_2; }
	inline void set_Delimiter_2(Il2CppChar value)
	{
		___Delimiter_2 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_3)); }
	inline TypeU5BU5D_t1664964607* get_EmptyTypes_3() const { return ___EmptyTypes_3; }
	inline TypeU5BU5D_t1664964607** get_address_of_EmptyTypes_3() { return &___EmptyTypes_3; }
	inline void set_EmptyTypes_3(TypeU5BU5D_t1664964607* value)
	{
		___EmptyTypes_3 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_3), value);
	}

	inline static int32_t get_offset_of_FilterAttribute_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_4)); }
	inline MemberFilter_t3405857066 * get_FilterAttribute_4() const { return ___FilterAttribute_4; }
	inline MemberFilter_t3405857066 ** get_address_of_FilterAttribute_4() { return &___FilterAttribute_4; }
	inline void set_FilterAttribute_4(MemberFilter_t3405857066 * value)
	{
		___FilterAttribute_4 = value;
		Il2CppCodeGenWriteBarrier((&___FilterAttribute_4), value);
	}

	inline static int32_t get_offset_of_FilterName_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_5)); }
	inline MemberFilter_t3405857066 * get_FilterName_5() const { return ___FilterName_5; }
	inline MemberFilter_t3405857066 ** get_address_of_FilterName_5() { return &___FilterName_5; }
	inline void set_FilterName_5(MemberFilter_t3405857066 * value)
	{
		___FilterName_5 = value;
		Il2CppCodeGenWriteBarrier((&___FilterName_5), value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_6)); }
	inline MemberFilter_t3405857066 * get_FilterNameIgnoreCase_6() const { return ___FilterNameIgnoreCase_6; }
	inline MemberFilter_t3405857066 ** get_address_of_FilterNameIgnoreCase_6() { return &___FilterNameIgnoreCase_6; }
	inline void set_FilterNameIgnoreCase_6(MemberFilter_t3405857066 * value)
	{
		___FilterNameIgnoreCase_6 = value;
		Il2CppCodeGenWriteBarrier((&___FilterNameIgnoreCase_6), value);
	}

	inline static int32_t get_offset_of_Missing_7() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_7)); }
	inline RuntimeObject * get_Missing_7() const { return ___Missing_7; }
	inline RuntimeObject ** get_address_of_Missing_7() { return &___Missing_7; }
	inline void set_Missing_7(RuntimeObject * value)
	{
		___Missing_7 = value;
		Il2CppCodeGenWriteBarrier((&___Missing_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T_H
#ifndef SOCKET_T3821512045_H
#define SOCKET_T3821512045_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Sockets.Socket
struct  Socket_t3821512045  : public RuntimeObject
{
public:
	// System.Collections.Queue System.Net.Sockets.Socket::readQ
	Queue_t1288490777 * ___readQ_0;
	// System.Collections.Queue System.Net.Sockets.Socket::writeQ
	Queue_t1288490777 * ___writeQ_1;
	// System.Boolean System.Net.Sockets.Socket::islistening
	bool ___islistening_2;
	// System.Boolean System.Net.Sockets.Socket::useoverlappedIO
	bool ___useoverlappedIO_3;
	// System.Int32 System.Net.Sockets.Socket::MinListenPort
	int32_t ___MinListenPort_4;
	// System.Int32 System.Net.Sockets.Socket::MaxListenPort
	int32_t ___MaxListenPort_5;
	// System.Int32 System.Net.Sockets.Socket::linger_timeout
	int32_t ___linger_timeout_8;
	// System.IntPtr System.Net.Sockets.Socket::socket
	IntPtr_t ___socket_9;
	// System.Net.Sockets.AddressFamily System.Net.Sockets.Socket::address_family
	int32_t ___address_family_10;
	// System.Net.Sockets.SocketType System.Net.Sockets.Socket::socket_type
	int32_t ___socket_type_11;
	// System.Net.Sockets.ProtocolType System.Net.Sockets.Socket::protocol_type
	int32_t ___protocol_type_12;
	// System.Boolean System.Net.Sockets.Socket::blocking
	bool ___blocking_13;
	// System.Threading.Thread System.Net.Sockets.Socket::blocking_thread
	Thread_t241561612 * ___blocking_thread_14;
	// System.Boolean System.Net.Sockets.Socket::isbound
	bool ___isbound_15;
	// System.Int32 System.Net.Sockets.Socket::max_bind_count
	int32_t ___max_bind_count_17;
	// System.Boolean System.Net.Sockets.Socket::connected
	bool ___connected_18;
	// System.Boolean System.Net.Sockets.Socket::closed
	bool ___closed_19;
	// System.Boolean System.Net.Sockets.Socket::disposed
	bool ___disposed_20;
	// System.Net.EndPoint System.Net.Sockets.Socket::seed_endpoint
	EndPoint_t4156119363 * ___seed_endpoint_21;

public:
	inline static int32_t get_offset_of_readQ_0() { return static_cast<int32_t>(offsetof(Socket_t3821512045, ___readQ_0)); }
	inline Queue_t1288490777 * get_readQ_0() const { return ___readQ_0; }
	inline Queue_t1288490777 ** get_address_of_readQ_0() { return &___readQ_0; }
	inline void set_readQ_0(Queue_t1288490777 * value)
	{
		___readQ_0 = value;
		Il2CppCodeGenWriteBarrier((&___readQ_0), value);
	}

	inline static int32_t get_offset_of_writeQ_1() { return static_cast<int32_t>(offsetof(Socket_t3821512045, ___writeQ_1)); }
	inline Queue_t1288490777 * get_writeQ_1() const { return ___writeQ_1; }
	inline Queue_t1288490777 ** get_address_of_writeQ_1() { return &___writeQ_1; }
	inline void set_writeQ_1(Queue_t1288490777 * value)
	{
		___writeQ_1 = value;
		Il2CppCodeGenWriteBarrier((&___writeQ_1), value);
	}

	inline static int32_t get_offset_of_islistening_2() { return static_cast<int32_t>(offsetof(Socket_t3821512045, ___islistening_2)); }
	inline bool get_islistening_2() const { return ___islistening_2; }
	inline bool* get_address_of_islistening_2() { return &___islistening_2; }
	inline void set_islistening_2(bool value)
	{
		___islistening_2 = value;
	}

	inline static int32_t get_offset_of_useoverlappedIO_3() { return static_cast<int32_t>(offsetof(Socket_t3821512045, ___useoverlappedIO_3)); }
	inline bool get_useoverlappedIO_3() const { return ___useoverlappedIO_3; }
	inline bool* get_address_of_useoverlappedIO_3() { return &___useoverlappedIO_3; }
	inline void set_useoverlappedIO_3(bool value)
	{
		___useoverlappedIO_3 = value;
	}

	inline static int32_t get_offset_of_MinListenPort_4() { return static_cast<int32_t>(offsetof(Socket_t3821512045, ___MinListenPort_4)); }
	inline int32_t get_MinListenPort_4() const { return ___MinListenPort_4; }
	inline int32_t* get_address_of_MinListenPort_4() { return &___MinListenPort_4; }
	inline void set_MinListenPort_4(int32_t value)
	{
		___MinListenPort_4 = value;
	}

	inline static int32_t get_offset_of_MaxListenPort_5() { return static_cast<int32_t>(offsetof(Socket_t3821512045, ___MaxListenPort_5)); }
	inline int32_t get_MaxListenPort_5() const { return ___MaxListenPort_5; }
	inline int32_t* get_address_of_MaxListenPort_5() { return &___MaxListenPort_5; }
	inline void set_MaxListenPort_5(int32_t value)
	{
		___MaxListenPort_5 = value;
	}

	inline static int32_t get_offset_of_linger_timeout_8() { return static_cast<int32_t>(offsetof(Socket_t3821512045, ___linger_timeout_8)); }
	inline int32_t get_linger_timeout_8() const { return ___linger_timeout_8; }
	inline int32_t* get_address_of_linger_timeout_8() { return &___linger_timeout_8; }
	inline void set_linger_timeout_8(int32_t value)
	{
		___linger_timeout_8 = value;
	}

	inline static int32_t get_offset_of_socket_9() { return static_cast<int32_t>(offsetof(Socket_t3821512045, ___socket_9)); }
	inline IntPtr_t get_socket_9() const { return ___socket_9; }
	inline IntPtr_t* get_address_of_socket_9() { return &___socket_9; }
	inline void set_socket_9(IntPtr_t value)
	{
		___socket_9 = value;
	}

	inline static int32_t get_offset_of_address_family_10() { return static_cast<int32_t>(offsetof(Socket_t3821512045, ___address_family_10)); }
	inline int32_t get_address_family_10() const { return ___address_family_10; }
	inline int32_t* get_address_of_address_family_10() { return &___address_family_10; }
	inline void set_address_family_10(int32_t value)
	{
		___address_family_10 = value;
	}

	inline static int32_t get_offset_of_socket_type_11() { return static_cast<int32_t>(offsetof(Socket_t3821512045, ___socket_type_11)); }
	inline int32_t get_socket_type_11() const { return ___socket_type_11; }
	inline int32_t* get_address_of_socket_type_11() { return &___socket_type_11; }
	inline void set_socket_type_11(int32_t value)
	{
		___socket_type_11 = value;
	}

	inline static int32_t get_offset_of_protocol_type_12() { return static_cast<int32_t>(offsetof(Socket_t3821512045, ___protocol_type_12)); }
	inline int32_t get_protocol_type_12() const { return ___protocol_type_12; }
	inline int32_t* get_address_of_protocol_type_12() { return &___protocol_type_12; }
	inline void set_protocol_type_12(int32_t value)
	{
		___protocol_type_12 = value;
	}

	inline static int32_t get_offset_of_blocking_13() { return static_cast<int32_t>(offsetof(Socket_t3821512045, ___blocking_13)); }
	inline bool get_blocking_13() const { return ___blocking_13; }
	inline bool* get_address_of_blocking_13() { return &___blocking_13; }
	inline void set_blocking_13(bool value)
	{
		___blocking_13 = value;
	}

	inline static int32_t get_offset_of_blocking_thread_14() { return static_cast<int32_t>(offsetof(Socket_t3821512045, ___blocking_thread_14)); }
	inline Thread_t241561612 * get_blocking_thread_14() const { return ___blocking_thread_14; }
	inline Thread_t241561612 ** get_address_of_blocking_thread_14() { return &___blocking_thread_14; }
	inline void set_blocking_thread_14(Thread_t241561612 * value)
	{
		___blocking_thread_14 = value;
		Il2CppCodeGenWriteBarrier((&___blocking_thread_14), value);
	}

	inline static int32_t get_offset_of_isbound_15() { return static_cast<int32_t>(offsetof(Socket_t3821512045, ___isbound_15)); }
	inline bool get_isbound_15() const { return ___isbound_15; }
	inline bool* get_address_of_isbound_15() { return &___isbound_15; }
	inline void set_isbound_15(bool value)
	{
		___isbound_15 = value;
	}

	inline static int32_t get_offset_of_max_bind_count_17() { return static_cast<int32_t>(offsetof(Socket_t3821512045, ___max_bind_count_17)); }
	inline int32_t get_max_bind_count_17() const { return ___max_bind_count_17; }
	inline int32_t* get_address_of_max_bind_count_17() { return &___max_bind_count_17; }
	inline void set_max_bind_count_17(int32_t value)
	{
		___max_bind_count_17 = value;
	}

	inline static int32_t get_offset_of_connected_18() { return static_cast<int32_t>(offsetof(Socket_t3821512045, ___connected_18)); }
	inline bool get_connected_18() const { return ___connected_18; }
	inline bool* get_address_of_connected_18() { return &___connected_18; }
	inline void set_connected_18(bool value)
	{
		___connected_18 = value;
	}

	inline static int32_t get_offset_of_closed_19() { return static_cast<int32_t>(offsetof(Socket_t3821512045, ___closed_19)); }
	inline bool get_closed_19() const { return ___closed_19; }
	inline bool* get_address_of_closed_19() { return &___closed_19; }
	inline void set_closed_19(bool value)
	{
		___closed_19 = value;
	}

	inline static int32_t get_offset_of_disposed_20() { return static_cast<int32_t>(offsetof(Socket_t3821512045, ___disposed_20)); }
	inline bool get_disposed_20() const { return ___disposed_20; }
	inline bool* get_address_of_disposed_20() { return &___disposed_20; }
	inline void set_disposed_20(bool value)
	{
		___disposed_20 = value;
	}

	inline static int32_t get_offset_of_seed_endpoint_21() { return static_cast<int32_t>(offsetof(Socket_t3821512045, ___seed_endpoint_21)); }
	inline EndPoint_t4156119363 * get_seed_endpoint_21() const { return ___seed_endpoint_21; }
	inline EndPoint_t4156119363 ** get_address_of_seed_endpoint_21() { return &___seed_endpoint_21; }
	inline void set_seed_endpoint_21(EndPoint_t4156119363 * value)
	{
		___seed_endpoint_21 = value;
		Il2CppCodeGenWriteBarrier((&___seed_endpoint_21), value);
	}
};

struct Socket_t3821512045_StaticFields
{
public:
	// System.Int32 System.Net.Sockets.Socket::ipv4Supported
	int32_t ___ipv4Supported_6;
	// System.Int32 System.Net.Sockets.Socket::ipv6Supported
	int32_t ___ipv6Supported_7;
	// System.Int32 System.Net.Sockets.Socket::current_bind_count
	int32_t ___current_bind_count_16;
	// System.Reflection.MethodInfo System.Net.Sockets.Socket::check_socket_policy
	MethodInfo_t * ___check_socket_policy_22;

public:
	inline static int32_t get_offset_of_ipv4Supported_6() { return static_cast<int32_t>(offsetof(Socket_t3821512045_StaticFields, ___ipv4Supported_6)); }
	inline int32_t get_ipv4Supported_6() const { return ___ipv4Supported_6; }
	inline int32_t* get_address_of_ipv4Supported_6() { return &___ipv4Supported_6; }
	inline void set_ipv4Supported_6(int32_t value)
	{
		___ipv4Supported_6 = value;
	}

	inline static int32_t get_offset_of_ipv6Supported_7() { return static_cast<int32_t>(offsetof(Socket_t3821512045_StaticFields, ___ipv6Supported_7)); }
	inline int32_t get_ipv6Supported_7() const { return ___ipv6Supported_7; }
	inline int32_t* get_address_of_ipv6Supported_7() { return &___ipv6Supported_7; }
	inline void set_ipv6Supported_7(int32_t value)
	{
		___ipv6Supported_7 = value;
	}

	inline static int32_t get_offset_of_current_bind_count_16() { return static_cast<int32_t>(offsetof(Socket_t3821512045_StaticFields, ___current_bind_count_16)); }
	inline int32_t get_current_bind_count_16() const { return ___current_bind_count_16; }
	inline int32_t* get_address_of_current_bind_count_16() { return &___current_bind_count_16; }
	inline void set_current_bind_count_16(int32_t value)
	{
		___current_bind_count_16 = value;
	}

	inline static int32_t get_offset_of_check_socket_policy_22() { return static_cast<int32_t>(offsetof(Socket_t3821512045_StaticFields, ___check_socket_policy_22)); }
	inline MethodInfo_t * get_check_socket_policy_22() const { return ___check_socket_policy_22; }
	inline MethodInfo_t ** get_address_of_check_socket_policy_22() { return &___check_socket_policy_22; }
	inline void set_check_socket_policy_22(MethodInfo_t * value)
	{
		___check_socket_policy_22 = value;
		Il2CppCodeGenWriteBarrier((&___check_socket_policy_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOCKET_T3821512045_H
#ifndef IPADDRESS_T1399971723_H
#define IPADDRESS_T1399971723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.IPAddress
struct  IPAddress_t1399971723  : public RuntimeObject
{
public:
	// System.Int64 System.Net.IPAddress::m_Address
	int64_t ___m_Address_0;
	// System.Net.Sockets.AddressFamily System.Net.IPAddress::m_Family
	int32_t ___m_Family_1;
	// System.UInt16[] System.Net.IPAddress::m_Numbers
	UInt16U5BU5D_t2527266722* ___m_Numbers_2;
	// System.Int64 System.Net.IPAddress::m_ScopeId
	int64_t ___m_ScopeId_3;
	// System.Int32 System.Net.IPAddress::m_HashCode
	int32_t ___m_HashCode_11;

public:
	inline static int32_t get_offset_of_m_Address_0() { return static_cast<int32_t>(offsetof(IPAddress_t1399971723, ___m_Address_0)); }
	inline int64_t get_m_Address_0() const { return ___m_Address_0; }
	inline int64_t* get_address_of_m_Address_0() { return &___m_Address_0; }
	inline void set_m_Address_0(int64_t value)
	{
		___m_Address_0 = value;
	}

	inline static int32_t get_offset_of_m_Family_1() { return static_cast<int32_t>(offsetof(IPAddress_t1399971723, ___m_Family_1)); }
	inline int32_t get_m_Family_1() const { return ___m_Family_1; }
	inline int32_t* get_address_of_m_Family_1() { return &___m_Family_1; }
	inline void set_m_Family_1(int32_t value)
	{
		___m_Family_1 = value;
	}

	inline static int32_t get_offset_of_m_Numbers_2() { return static_cast<int32_t>(offsetof(IPAddress_t1399971723, ___m_Numbers_2)); }
	inline UInt16U5BU5D_t2527266722* get_m_Numbers_2() const { return ___m_Numbers_2; }
	inline UInt16U5BU5D_t2527266722** get_address_of_m_Numbers_2() { return &___m_Numbers_2; }
	inline void set_m_Numbers_2(UInt16U5BU5D_t2527266722* value)
	{
		___m_Numbers_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Numbers_2), value);
	}

	inline static int32_t get_offset_of_m_ScopeId_3() { return static_cast<int32_t>(offsetof(IPAddress_t1399971723, ___m_ScopeId_3)); }
	inline int64_t get_m_ScopeId_3() const { return ___m_ScopeId_3; }
	inline int64_t* get_address_of_m_ScopeId_3() { return &___m_ScopeId_3; }
	inline void set_m_ScopeId_3(int64_t value)
	{
		___m_ScopeId_3 = value;
	}

	inline static int32_t get_offset_of_m_HashCode_11() { return static_cast<int32_t>(offsetof(IPAddress_t1399971723, ___m_HashCode_11)); }
	inline int32_t get_m_HashCode_11() const { return ___m_HashCode_11; }
	inline int32_t* get_address_of_m_HashCode_11() { return &___m_HashCode_11; }
	inline void set_m_HashCode_11(int32_t value)
	{
		___m_HashCode_11 = value;
	}
};

struct IPAddress_t1399971723_StaticFields
{
public:
	// System.Net.IPAddress System.Net.IPAddress::Any
	IPAddress_t1399971723 * ___Any_4;
	// System.Net.IPAddress System.Net.IPAddress::Broadcast
	IPAddress_t1399971723 * ___Broadcast_5;
	// System.Net.IPAddress System.Net.IPAddress::Loopback
	IPAddress_t1399971723 * ___Loopback_6;
	// System.Net.IPAddress System.Net.IPAddress::None
	IPAddress_t1399971723 * ___None_7;
	// System.Net.IPAddress System.Net.IPAddress::IPv6Any
	IPAddress_t1399971723 * ___IPv6Any_8;
	// System.Net.IPAddress System.Net.IPAddress::IPv6Loopback
	IPAddress_t1399971723 * ___IPv6Loopback_9;
	// System.Net.IPAddress System.Net.IPAddress::IPv6None
	IPAddress_t1399971723 * ___IPv6None_10;

public:
	inline static int32_t get_offset_of_Any_4() { return static_cast<int32_t>(offsetof(IPAddress_t1399971723_StaticFields, ___Any_4)); }
	inline IPAddress_t1399971723 * get_Any_4() const { return ___Any_4; }
	inline IPAddress_t1399971723 ** get_address_of_Any_4() { return &___Any_4; }
	inline void set_Any_4(IPAddress_t1399971723 * value)
	{
		___Any_4 = value;
		Il2CppCodeGenWriteBarrier((&___Any_4), value);
	}

	inline static int32_t get_offset_of_Broadcast_5() { return static_cast<int32_t>(offsetof(IPAddress_t1399971723_StaticFields, ___Broadcast_5)); }
	inline IPAddress_t1399971723 * get_Broadcast_5() const { return ___Broadcast_5; }
	inline IPAddress_t1399971723 ** get_address_of_Broadcast_5() { return &___Broadcast_5; }
	inline void set_Broadcast_5(IPAddress_t1399971723 * value)
	{
		___Broadcast_5 = value;
		Il2CppCodeGenWriteBarrier((&___Broadcast_5), value);
	}

	inline static int32_t get_offset_of_Loopback_6() { return static_cast<int32_t>(offsetof(IPAddress_t1399971723_StaticFields, ___Loopback_6)); }
	inline IPAddress_t1399971723 * get_Loopback_6() const { return ___Loopback_6; }
	inline IPAddress_t1399971723 ** get_address_of_Loopback_6() { return &___Loopback_6; }
	inline void set_Loopback_6(IPAddress_t1399971723 * value)
	{
		___Loopback_6 = value;
		Il2CppCodeGenWriteBarrier((&___Loopback_6), value);
	}

	inline static int32_t get_offset_of_None_7() { return static_cast<int32_t>(offsetof(IPAddress_t1399971723_StaticFields, ___None_7)); }
	inline IPAddress_t1399971723 * get_None_7() const { return ___None_7; }
	inline IPAddress_t1399971723 ** get_address_of_None_7() { return &___None_7; }
	inline void set_None_7(IPAddress_t1399971723 * value)
	{
		___None_7 = value;
		Il2CppCodeGenWriteBarrier((&___None_7), value);
	}

	inline static int32_t get_offset_of_IPv6Any_8() { return static_cast<int32_t>(offsetof(IPAddress_t1399971723_StaticFields, ___IPv6Any_8)); }
	inline IPAddress_t1399971723 * get_IPv6Any_8() const { return ___IPv6Any_8; }
	inline IPAddress_t1399971723 ** get_address_of_IPv6Any_8() { return &___IPv6Any_8; }
	inline void set_IPv6Any_8(IPAddress_t1399971723 * value)
	{
		___IPv6Any_8 = value;
		Il2CppCodeGenWriteBarrier((&___IPv6Any_8), value);
	}

	inline static int32_t get_offset_of_IPv6Loopback_9() { return static_cast<int32_t>(offsetof(IPAddress_t1399971723_StaticFields, ___IPv6Loopback_9)); }
	inline IPAddress_t1399971723 * get_IPv6Loopback_9() const { return ___IPv6Loopback_9; }
	inline IPAddress_t1399971723 ** get_address_of_IPv6Loopback_9() { return &___IPv6Loopback_9; }
	inline void set_IPv6Loopback_9(IPAddress_t1399971723 * value)
	{
		___IPv6Loopback_9 = value;
		Il2CppCodeGenWriteBarrier((&___IPv6Loopback_9), value);
	}

	inline static int32_t get_offset_of_IPv6None_10() { return static_cast<int32_t>(offsetof(IPAddress_t1399971723_StaticFields, ___IPv6None_10)); }
	inline IPAddress_t1399971723 * get_IPv6None_10() const { return ___IPv6None_10; }
	inline IPAddress_t1399971723 ** get_address_of_IPv6None_10() { return &___IPv6None_10; }
	inline void set_IPv6None_10(IPAddress_t1399971723 * value)
	{
		___IPv6None_10 = value;
		Il2CppCodeGenWriteBarrier((&___IPv6None_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IPADDRESS_T1399971723_H
#ifndef UDPCLIENT_T1278197702_H
#define UDPCLIENT_T1278197702_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Sockets.UdpClient
struct  UdpClient_t1278197702  : public RuntimeObject
{
public:
	// System.Boolean System.Net.Sockets.UdpClient::disposed
	bool ___disposed_0;
	// System.Boolean System.Net.Sockets.UdpClient::active
	bool ___active_1;
	// System.Net.Sockets.Socket System.Net.Sockets.UdpClient::socket
	Socket_t3821512045 * ___socket_2;
	// System.Net.Sockets.AddressFamily System.Net.Sockets.UdpClient::family
	int32_t ___family_3;
	// System.Byte[] System.Net.Sockets.UdpClient::recvbuffer
	ByteU5BU5D_t3397334013* ___recvbuffer_4;

public:
	inline static int32_t get_offset_of_disposed_0() { return static_cast<int32_t>(offsetof(UdpClient_t1278197702, ___disposed_0)); }
	inline bool get_disposed_0() const { return ___disposed_0; }
	inline bool* get_address_of_disposed_0() { return &___disposed_0; }
	inline void set_disposed_0(bool value)
	{
		___disposed_0 = value;
	}

	inline static int32_t get_offset_of_active_1() { return static_cast<int32_t>(offsetof(UdpClient_t1278197702, ___active_1)); }
	inline bool get_active_1() const { return ___active_1; }
	inline bool* get_address_of_active_1() { return &___active_1; }
	inline void set_active_1(bool value)
	{
		___active_1 = value;
	}

	inline static int32_t get_offset_of_socket_2() { return static_cast<int32_t>(offsetof(UdpClient_t1278197702, ___socket_2)); }
	inline Socket_t3821512045 * get_socket_2() const { return ___socket_2; }
	inline Socket_t3821512045 ** get_address_of_socket_2() { return &___socket_2; }
	inline void set_socket_2(Socket_t3821512045 * value)
	{
		___socket_2 = value;
		Il2CppCodeGenWriteBarrier((&___socket_2), value);
	}

	inline static int32_t get_offset_of_family_3() { return static_cast<int32_t>(offsetof(UdpClient_t1278197702, ___family_3)); }
	inline int32_t get_family_3() const { return ___family_3; }
	inline int32_t* get_address_of_family_3() { return &___family_3; }
	inline void set_family_3(int32_t value)
	{
		___family_3 = value;
	}

	inline static int32_t get_offset_of_recvbuffer_4() { return static_cast<int32_t>(offsetof(UdpClient_t1278197702, ___recvbuffer_4)); }
	inline ByteU5BU5D_t3397334013* get_recvbuffer_4() const { return ___recvbuffer_4; }
	inline ByteU5BU5D_t3397334013** get_address_of_recvbuffer_4() { return &___recvbuffer_4; }
	inline void set_recvbuffer_4(ByteU5BU5D_t3397334013* value)
	{
		___recvbuffer_4 = value;
		Il2CppCodeGenWriteBarrier((&___recvbuffer_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UDPCLIENT_T1278197702_H
#ifndef OBJECTDISPOSEDEXCEPTION_T2695136451_H
#define OBJECTDISPOSEDEXCEPTION_T2695136451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ObjectDisposedException
struct  ObjectDisposedException_t2695136451  : public InvalidOperationException_t721527559
{
public:
	// System.String System.ObjectDisposedException::obj_name
	String_t* ___obj_name_12;
	// System.String System.ObjectDisposedException::msg
	String_t* ___msg_13;

public:
	inline static int32_t get_offset_of_obj_name_12() { return static_cast<int32_t>(offsetof(ObjectDisposedException_t2695136451, ___obj_name_12)); }
	inline String_t* get_obj_name_12() const { return ___obj_name_12; }
	inline String_t** get_address_of_obj_name_12() { return &___obj_name_12; }
	inline void set_obj_name_12(String_t* value)
	{
		___obj_name_12 = value;
		Il2CppCodeGenWriteBarrier((&___obj_name_12), value);
	}

	inline static int32_t get_offset_of_msg_13() { return static_cast<int32_t>(offsetof(ObjectDisposedException_t2695136451, ___msg_13)); }
	inline String_t* get_msg_13() const { return ___msg_13; }
	inline String_t** get_address_of_msg_13() { return &___msg_13; }
	inline void set_msg_13(String_t* value)
	{
		___msg_13 = value;
		Il2CppCodeGenWriteBarrier((&___msg_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTDISPOSEDEXCEPTION_T2695136451_H
#ifndef EVENTHANDLER_1_T2264531613_H
#define EVENTHANDLER_1_T2264531613_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventHandler`1<OSCsharp.Net.OscBundleReceivedEventArgs>
struct  EventHandler_1_t2264531613  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTHANDLER_1_T2264531613_H
#ifndef ASYNCCALLBACK_T163412349_H
#define ASYNCCALLBACK_T163412349_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.AsyncCallback
struct  AsyncCallback_t163412349  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCCALLBACK_T163412349_H
#ifndef EVENTHANDLER_1_T3778988004_H
#define EVENTHANDLER_1_T3778988004_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventHandler`1<OSCsharp.Utils.ExceptionEventArgs>
struct  EventHandler_1_t3778988004  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTHANDLER_1_T3778988004_H
#ifndef EVENTHANDLER_1_T1873352441_H
#define EVENTHANDLER_1_T1873352441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventHandler`1<OSCsharp.Net.OscPacketReceivedEventArgs>
struct  EventHandler_1_t1873352441  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTHANDLER_1_T1873352441_H
#ifndef EVENTHANDLER_1_T4149316328_H
#define EVENTHANDLER_1_T4149316328_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventHandler`1<OSCsharp.Net.OscMessageReceivedEventArgs>
struct  EventHandler_1_t4149316328  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTHANDLER_1_T4149316328_H
#ifndef OSCTIMETAG_T625345318_H
#define OSCTIMETAG_T625345318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OSCsharp.Data.OscTimeTag
struct  OscTimeTag_t625345318  : public RuntimeObject
{
public:
	// System.DateTime OSCsharp.Data.OscTimeTag::timeStamp
	DateTime_t693205669  ___timeStamp_2;

public:
	inline static int32_t get_offset_of_timeStamp_2() { return static_cast<int32_t>(offsetof(OscTimeTag_t625345318, ___timeStamp_2)); }
	inline DateTime_t693205669  get_timeStamp_2() const { return ___timeStamp_2; }
	inline DateTime_t693205669 * get_address_of_timeStamp_2() { return &___timeStamp_2; }
	inline void set_timeStamp_2(DateTime_t693205669  value)
	{
		___timeStamp_2 = value;
	}
};

struct OscTimeTag_t625345318_StaticFields
{
public:
	// System.DateTime OSCsharp.Data.OscTimeTag::Epoch
	DateTime_t693205669  ___Epoch_0;
	// OSCsharp.Data.OscTimeTag OSCsharp.Data.OscTimeTag::MinValue
	OscTimeTag_t625345318 * ___MinValue_1;

public:
	inline static int32_t get_offset_of_Epoch_0() { return static_cast<int32_t>(offsetof(OscTimeTag_t625345318_StaticFields, ___Epoch_0)); }
	inline DateTime_t693205669  get_Epoch_0() const { return ___Epoch_0; }
	inline DateTime_t693205669 * get_address_of_Epoch_0() { return &___Epoch_0; }
	inline void set_Epoch_0(DateTime_t693205669  value)
	{
		___Epoch_0 = value;
	}

	inline static int32_t get_offset_of_MinValue_1() { return static_cast<int32_t>(offsetof(OscTimeTag_t625345318_StaticFields, ___MinValue_1)); }
	inline OscTimeTag_t625345318 * get_MinValue_1() const { return ___MinValue_1; }
	inline OscTimeTag_t625345318 ** get_address_of_MinValue_1() { return &___MinValue_1; }
	inline void set_MinValue_1(OscTimeTag_t625345318 * value)
	{
		___MinValue_1 = value;
		Il2CppCodeGenWriteBarrier((&___MinValue_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OSCTIMETAG_T625345318_H
// System.Byte[]
struct ByteU5BU5D_t3397334013  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
// System.Char[]
struct CharU5BU5D_t1328083999  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Il2CppChar m_Items[1];

public:
	inline Il2CppChar GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppChar value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Il2CppChar GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppChar value)
	{
		m_Items[index] = value;
	}
};


// T OSCsharp.Data.OscPacket::ValueFromByteArray<System.Object>(System.Byte[],System.Int32&)
extern "C"  RuntimeObject * OscPacket_ValueFromByteArray_TisRuntimeObject_m2864590997_gshared (RuntimeObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___data0, int32_t* ___start1, const RuntimeMethod* method);
// T OSCsharp.Data.OscPacket::ValueFromByteArray<System.Int32>(System.Byte[],System.Int32&)
extern "C"  int32_t OscPacket_ValueFromByteArray_TisInt32_t2071877448_m417255044_gshared (RuntimeObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___data0, int32_t* ___start1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
extern "C"  void List_1_Add_m4157722533_gshared (List_1_t2058570427 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C"  int32_t List_1_get_Count_m2375293942_gshared (List_1_t2058570427 * __this, const RuntimeMethod* method);
// T OSCsharp.Data.OscPacket::ValueFromByteArray<System.Int64>(System.Byte[],System.Int32&)
extern "C"  int64_t OscPacket_ValueFromByteArray_TisInt64_t909078037_m2124432849_gshared (RuntimeObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___data0, int32_t* ___start1, const RuntimeMethod* method);
// T OSCsharp.Data.OscPacket::ValueFromByteArray<System.Single>(System.Byte[],System.Int32&)
extern "C"  float OscPacket_ValueFromByteArray_TisSingle_t2076509932_m308377574_gshared (RuntimeObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___data0, int32_t* ___start1, const RuntimeMethod* method);
// T OSCsharp.Data.OscPacket::ValueFromByteArray<System.Double>(System.Byte[],System.Int32&)
extern "C"  double OscPacket_ValueFromByteArray_TisDouble_t4078015681_m3730957917_gshared (RuntimeObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___data0, int32_t* ___start1, const RuntimeMethod* method);
// T OSCsharp.Data.OscPacket::ValueFromByteArray<System.Char>(System.Byte[],System.Int32&)
extern "C"  Il2CppChar OscPacket_ValueFromByteArray_TisChar_t3454481338_m257312846_gshared (RuntimeObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___data0, int32_t* ___start1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m3043033341_gshared (Dictionary_2_t1663937576 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::Add(!0,!1)
extern "C"  void Dictionary_2_Add_m790520409_gshared (Dictionary_2_t1663937576 * __this, RuntimeObject * p0, int32_t p1, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::TryGetValue(!0,!1&)
extern "C"  bool Dictionary_2_TryGetValue_m2330758874_gshared (Dictionary_2_t1663937576 * __this, RuntimeObject * p0, int32_t* p1, const RuntimeMethod* method);
// System.Collections.ObjectModel.ReadOnlyCollection`1<!0> System.Collections.Generic.List`1<System.Object>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t2875234987 * List_1_AsReadOnly_m1618299177_gshared (List_1_t2058570427 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C"  void List_1__ctor_m310736118_gshared (List_1_t2058570427 * __this, const RuntimeMethod* method);
// T[] OSCsharp.Utils.Utility::CopySubArray<System.Byte>(T[],System.Int32,System.Int32)
extern "C"  ByteU5BU5D_t3397334013* Utility_CopySubArray_TisByte_t3683104436_m936098224_gshared (RuntimeObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___source0, int32_t ___start1, int32_t ___length2, const RuntimeMethod* method);
// System.Void System.EventHandler`1<System.Object>::Invoke(System.Object,!0)
extern "C"  void EventHandler_1_Invoke_m2430999761_gshared (EventHandler_1_t1280756467 * __this, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method);

// System.Void OSCsharp.Data.OscPacket::.ctor(System.String)
extern "C"  void OscPacket__ctor_m3636146750 (OscPacket_t504761797 * __this, String_t* ___address0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// T OSCsharp.Data.OscPacket::ValueFromByteArray<System.String>(System.Byte[],System.Int32&)
#define OscPacket_ValueFromByteArray_TisString_t_m3665303177(__this /* static, unused */, ___data0, ___start1, method) ((  String_t* (*) (RuntimeObject * /* static, unused */, ByteU5BU5D_t3397334013*, int32_t*, const RuntimeMethod*))OscPacket_ValueFromByteArray_TisRuntimeObject_m2864590997_gshared)(__this /* static, unused */, ___data0, ___start1, method)
// System.Boolean System.String::op_Inequality(System.String,System.String)
extern "C"  bool String_op_Inequality_m304203149 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentException::.ctor()
extern "C"  void ArgumentException__ctor_m2105824819 (ArgumentException_t3259014390 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// T OSCsharp.Data.OscPacket::ValueFromByteArray<OSCsharp.Data.OscTimeTag>(System.Byte[],System.Int32&)
#define OscPacket_ValueFromByteArray_TisOscTimeTag_t625345318_m2765599720(__this /* static, unused */, ___data0, ___start1, method) ((  OscTimeTag_t625345318 * (*) (RuntimeObject * /* static, unused */, ByteU5BU5D_t3397334013*, int32_t*, const RuntimeMethod*))OscPacket_ValueFromByteArray_TisRuntimeObject_m2864590997_gshared)(__this /* static, unused */, ___data0, ___start1, method)
// System.Void OSCsharp.Data.OscBundle::.ctor(OSCsharp.Data.OscTimeTag)
extern "C"  void OscBundle__ctor_m625764563 (OscBundle_t1126010605 * __this, OscTimeTag_t625345318 * ___timeStamp0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// T OSCsharp.Data.OscPacket::ValueFromByteArray<System.Int32>(System.Byte[],System.Int32&)
#define OscPacket_ValueFromByteArray_TisInt32_t2071877448_m417255044(__this /* static, unused */, ___data0, ___start1, method) ((  int32_t (*) (RuntimeObject * /* static, unused */, ByteU5BU5D_t3397334013*, int32_t*, const RuntimeMethod*))OscPacket_ValueFromByteArray_TisInt32_t2071877448_m417255044_gshared)(__this /* static, unused */, ___data0, ___start1, method)
// OSCsharp.Data.OscPacket OSCsharp.Data.OscPacket::FromByteArray(System.Byte[],System.Int32&,System.Int32)
extern "C"  OscPacket_t504761797 * OscPacket_FromByteArray_m3135720259 (RuntimeObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___data0, int32_t* ___start1, int32_t ___end2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean OSCsharp.Data.OscTimeTag::op_LessThan(OSCsharp.Data.OscTimeTag,OSCsharp.Data.OscTimeTag)
extern "C"  bool OscTimeTag_op_LessThan_m1978965833 (RuntimeObject * __this /* static, unused */, OscTimeTag_t625345318 * ___lhs0, OscTimeTag_t625345318 * ___rhs1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Exception::.ctor(System.String)
extern "C"  void Exception__ctor_m485833136 (Exception_t1927440687 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
#define List_1_Add_m4157722533(__this, p0, method) ((  void (*) (List_1_t2058570427 *, RuntimeObject *, const RuntimeMethod*))List_1_Add_m4157722533_gshared)(__this, p0, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
#define List_1_get_Count_m2375293942(__this, method) ((  int32_t (*) (List_1_t2058570427 *, const RuntimeMethod*))List_1_get_Count_m2375293942_gshared)(__this, method)
// System.Boolean System.String::StartsWith(System.String)
extern "C"  bool String_StartsWith_m1841920685 (String_t* __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentException::.ctor(System.String)
extern "C"  void ArgumentException__ctor_m3739475201 (ArgumentException_t3259014390 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Char::ToString()
extern "C"  String_t* Char_ToString_m1976753030 (Il2CppChar* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Data.OscMessage::.ctor(System.String)
extern "C"  void OscMessage__ctor_m2211520241 (OscMessage_t2764280154 * __this, String_t* ___address0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Char[] System.String::ToCharArray()
extern "C"  CharU5BU5D_t1328083999* String_ToCharArray_m870309954 (String_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// T OSCsharp.Data.OscPacket::ValueFromByteArray<System.Int64>(System.Byte[],System.Int32&)
#define OscPacket_ValueFromByteArray_TisInt64_t909078037_m2124432849(__this /* static, unused */, ___data0, ___start1, method) ((  int64_t (*) (RuntimeObject * /* static, unused */, ByteU5BU5D_t3397334013*, int32_t*, const RuntimeMethod*))OscPacket_ValueFromByteArray_TisInt64_t909078037_m2124432849_gshared)(__this /* static, unused */, ___data0, ___start1, method)
// T OSCsharp.Data.OscPacket::ValueFromByteArray<System.Single>(System.Byte[],System.Int32&)
#define OscPacket_ValueFromByteArray_TisSingle_t2076509932_m308377574(__this /* static, unused */, ___data0, ___start1, method) ((  float (*) (RuntimeObject * /* static, unused */, ByteU5BU5D_t3397334013*, int32_t*, const RuntimeMethod*))OscPacket_ValueFromByteArray_TisSingle_t2076509932_m308377574_gshared)(__this /* static, unused */, ___data0, ___start1, method)
// T OSCsharp.Data.OscPacket::ValueFromByteArray<System.Double>(System.Byte[],System.Int32&)
#define OscPacket_ValueFromByteArray_TisDouble_t4078015681_m3730957917(__this /* static, unused */, ___data0, ___start1, method) ((  double (*) (RuntimeObject * /* static, unused */, ByteU5BU5D_t3397334013*, int32_t*, const RuntimeMethod*))OscPacket_ValueFromByteArray_TisDouble_t4078015681_m3730957917_gshared)(__this /* static, unused */, ___data0, ___start1, method)
// T OSCsharp.Data.OscPacket::ValueFromByteArray<System.Byte[]>(System.Byte[],System.Int32&)
#define OscPacket_ValueFromByteArray_TisByteU5BU5D_t3397334013_m2572900720(__this /* static, unused */, ___data0, ___start1, method) ((  ByteU5BU5D_t3397334013* (*) (RuntimeObject * /* static, unused */, ByteU5BU5D_t3397334013*, int32_t*, const RuntimeMethod*))OscPacket_ValueFromByteArray_TisRuntimeObject_m2864590997_gshared)(__this /* static, unused */, ___data0, ___start1, method)
// T OSCsharp.Data.OscPacket::ValueFromByteArray<System.Char>(System.Byte[],System.Int32&)
#define OscPacket_ValueFromByteArray_TisChar_t3454481338_m257312846(__this /* static, unused */, ___data0, ___start1, method) ((  Il2CppChar (*) (RuntimeObject * /* static, unused */, ByteU5BU5D_t3397334013*, int32_t*, const RuntimeMethod*))OscPacket_ValueFromByteArray_TisChar_t3454481338_m257312846_gshared)(__this /* static, unused */, ___data0, ___start1, method)
// System.Type System.Object::GetType()
extern "C"  Type_t * Object_GetType_m191970594 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::.ctor(System.Int32)
#define Dictionary_2__ctor_m2118310873(__this, p0, method) ((  void (*) (Dictionary_2_t3986656710 *, int32_t, const RuntimeMethod*))Dictionary_2__ctor_m3043033341_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1)
#define Dictionary_2_Add_m1209957957(__this, p0, p1, method) ((  void (*) (Dictionary_2_t3986656710 *, String_t*, int32_t, const RuntimeMethod*))Dictionary_2_Add_m790520409_gshared)(__this, p0, p1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Int32>::TryGetValue(!0,!1&)
#define Dictionary_2_TryGetValue_m2977303364(__this, p0, p1, method) ((  bool (*) (Dictionary_2_t3986656710 *, String_t*, int32_t*, const RuntimeMethod*))Dictionary_2_TryGetValue_m2330758874_gshared)(__this, p0, p1, method)
// System.Boolean System.Single::IsPositiveInfinity(System.Single)
extern "C"  bool Single_IsPositiveInfinity_m869158735 (RuntimeObject * __this /* static, unused */, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.Object,System.Object)
extern "C"  String_t* String_Concat_m56707527 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.ObjectModel.ReadOnlyCollection`1<!0> System.Collections.Generic.List`1<System.Object>::AsReadOnly()
#define List_1_AsReadOnly_m1618299177(__this, method) ((  ReadOnlyCollection_1_t2875234987 * (*) (List_1_t2058570427 *, const RuntimeMethod*))List_1_AsReadOnly_m1618299177_gshared)(__this, method)
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m2551263788 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Data.OscPacket::set_Address(System.String)
extern "C"  void OscPacket_set_Address_m1646762965 (OscPacket_t504761797 * __this, String_t* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
#define List_1__ctor_m310736118(__this, method) ((  void (*) (List_1_t2058570427 *, const RuntimeMethod*))List_1__ctor_m310736118_gshared)(__this, method)
// OSCsharp.Data.OscBundle OSCsharp.Data.OscBundle::FromByteArray(System.Byte[],System.Int32&,System.Int32)
extern "C"  OscBundle_t1126010605 * OscBundle_FromByteArray_m2667454531 (RuntimeObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___data0, int32_t* ___start1, int32_t ___end2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// OSCsharp.Data.OscMessage OSCsharp.Data.OscMessage::FromByteArray(System.Byte[],System.Int32&)
extern "C"  OscMessage_t2764280154 * OscMessage_FromByteArray_m4248709050 (RuntimeObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___data0, int32_t* ___start1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Data.OscTimeTag::Set(System.DateTime)
extern "C"  void OscTimeTag_Set_m4065929879 (OscTimeTag_t625345318 * __this, DateTime_t693205669  ___timeStamp0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// T[] OSCsharp.Utils.Utility::CopySubArray<System.Byte>(T[],System.Int32,System.Int32)
#define Utility_CopySubArray_TisByte_t3683104436_m936098224(__this /* static, unused */, ___source0, ___start1, ___length2, method) ((  ByteU5BU5D_t3397334013* (*) (RuntimeObject * /* static, unused */, ByteU5BU5D_t3397334013*, int32_t, int32_t, const RuntimeMethod*))Utility_CopySubArray_TisByte_t3683104436_m936098224_gshared)(__this /* static, unused */, ___source0, ___start1, ___length2, method)
// System.Boolean OSCsharp.Data.OscPacket::get_LittleEndianByteOrder()
extern "C"  bool OscPacket_get_LittleEndianByteOrder_m1882071268 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Byte[] OSCsharp.Utils.Utility::SwapEndian(System.Byte[])
extern "C"  ByteU5BU5D_t3397334013* Utility_SwapEndian_m3369642783 (RuntimeObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___data0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.UInt32 System.BitConverter::ToUInt32(System.Byte[],System.Int32)
extern "C"  uint32_t BitConverter_ToUInt32_m4153360341 (RuntimeObject * __this /* static, unused */, ByteU5BU5D_t3397334013* p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.DateTime::AddSeconds(System.Double)
extern "C"  DateTime_t693205669  DateTime_AddSeconds_m722082155 (DateTime_t693205669 * __this, double p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.DateTime::AddMilliseconds(System.Double)
extern "C"  DateTime_t693205669  DateTime_AddMilliseconds_m1813199744 (DateTime_t693205669 * __this, double p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Object::ReferenceEquals(System.Object,System.Object)
extern "C"  bool Object_ReferenceEquals_m3900584722 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.DateTime OSCsharp.Data.OscTimeTag::get_DateTime()
extern "C"  DateTime_t693205669  OscTimeTag_get_DateTime_m2172308342 (OscTimeTag_t625345318 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.DateTime::op_Equality(System.DateTime,System.DateTime)
extern "C"  bool DateTime_op_Equality_m1867073872 (RuntimeObject * __this /* static, unused */, DateTime_t693205669  p0, DateTime_t693205669  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.DateTime::op_LessThan(System.DateTime,System.DateTime)
extern "C"  bool DateTime_op_LessThan_m3944619870 (RuntimeObject * __this /* static, unused */, DateTime_t693205669  p0, DateTime_t693205669  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.TimeSpan System.TimeSpan::FromMilliseconds(System.Double)
extern "C"  TimeSpan_t3430258949  TimeSpan_FromMilliseconds_m664523225 (RuntimeObject * __this /* static, unused */, double p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.DateTime::op_Addition(System.DateTime,System.TimeSpan)
extern "C"  DateTime_t693205669  DateTime_op_Addition_m1268923147 (RuntimeObject * __this /* static, unused */, DateTime_t693205669  p0, TimeSpan_t3430258949  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.DateTime::op_GreaterThanOrEqual(System.DateTime,System.DateTime)
extern "C"  bool DateTime_op_GreaterThanOrEqual_m3818963848 (RuntimeObject * __this /* static, unused */, DateTime_t693205669  p0, DateTime_t693205669  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.DateTime::get_Ticks()
extern "C"  int64_t DateTime_get_Ticks_m310281298 (DateTime_t693205669 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.DateTimeKind System.DateTime::get_Kind()
extern "C"  int32_t DateTime_get_Kind_m1331920314 (DateTime_t693205669 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.DateTime::.ctor(System.Int64,System.DateTimeKind)
extern "C"  void DateTime__ctor_m1100326092 (DateTime_t693205669 * __this, int64_t p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean OSCsharp.Data.OscTimeTag::IsValidTime(System.DateTime)
extern "C"  bool OscTimeTag_IsValidTime_m2988141586 (RuntimeObject * __this /* static, unused */, DateTime_t693205669  ___timeStamp0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean OSCsharp.Data.OscTimeTag::op_Equality(OSCsharp.Data.OscTimeTag,OSCsharp.Data.OscTimeTag)
extern "C"  bool OscTimeTag_op_Equality_m3410370307 (RuntimeObject * __this /* static, unused */, OscTimeTag_t625345318 * ___lhs0, OscTimeTag_t625345318 * ___rhs1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.DateTime::Equals(System.DateTime)
extern "C"  bool DateTime_Equals_m1104060551 (DateTime_t693205669 * __this, DateTime_t693205669  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.DateTime::GetHashCode()
extern "C"  int32_t DateTime_GetHashCode_m974799321 (DateTime_t693205669 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.DateTime::ToString()
extern "C"  String_t* DateTime_ToString_m1117481977 (DateTime_t693205669 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.DateTime::.ctor(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void DateTime__ctor_m2857738939 (DateTime_t693205669 * __this, int32_t p0, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int32_t p6, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Data.OscTimeTag::.ctor(System.DateTime)
extern "C"  void OscTimeTag__ctor_m1655495837 (OscTimeTag_t625345318 * __this, DateTime_t693205669  ___timeStamp0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.EventArgs::.ctor()
extern "C"  void EventArgs__ctor_m3696060910 (EventArgs_t3289624707 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.OscBundleReceivedEventArgs::set_Bundle(OSCsharp.Data.OscBundle)
extern "C"  void OscBundleReceivedEventArgs_set_Bundle_m2468455950 (OscBundleReceivedEventArgs_t3673224441 * __this, OscBundle_t1126010605 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.OscMessageReceivedEventArgs::set_Message(OSCsharp.Data.OscMessage)
extern "C"  void OscMessageReceivedEventArgs_set_Message_m841798933 (OscMessageReceivedEventArgs_t1263041860 * __this, OscMessage_t2764280154 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.OscPacketReceivedEventArgs::set_Packet(OSCsharp.Data.OscPacket)
extern "C"  void OscPacketReceivedEventArgs_set_Packet_m2983320626 (OscPacketReceivedEventArgs_t3282045269 * __this, OscPacket_t504761797 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::Combine(System.Delegate,System.Delegate)
extern "C"  Delegate_t3022476291 * Delegate_Combine_m3791207084 (RuntimeObject * __this /* static, unused */, Delegate_t3022476291 * p0, Delegate_t3022476291 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::Remove(System.Delegate,System.Delegate)
extern "C"  Delegate_t3022476291 * Delegate_Remove_m2626518725 (RuntimeObject * __this /* static, unused */, Delegate_t3022476291 * p0, Delegate_t3022476291 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.UDPReceiver::.ctor(System.Net.IPAddress,System.Int32,System.Boolean)
extern "C"  void UDPReceiver__ctor_m3514168723 (UDPReceiver_t3846109956 * __this, IPAddress_t1399971723 * ___ipAddress0, int32_t ___port1, bool ___consumeParsingExceptions2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.UDPReceiver::.ctor(System.Net.IPAddress,System.Int32,OSCsharp.Net.TransmissionType,System.Net.IPAddress,System.Boolean)
extern "C"  void UDPReceiver__ctor_m896685042 (UDPReceiver_t3846109956 * __this, IPAddress_t1399971723 * ___ipAddress0, int32_t ___port1, int32_t ___transmissionType2, IPAddress_t1399971723 * ___multicastAddress3, bool ___consumeParsingExceptions4, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.UDPReceiver::set_IPAddress(System.Net.IPAddress)
extern "C"  void UDPReceiver_set_IPAddress_m3792884775 (UDPReceiver_t3846109956 * __this, IPAddress_t1399971723 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.UDPReceiver::set_Port(System.Int32)
extern "C"  void UDPReceiver_set_Port_m2491900965 (UDPReceiver_t3846109956 * __this, int32_t ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.UDPReceiver::set_TransmissionType(OSCsharp.Net.TransmissionType)
extern "C"  void UDPReceiver_set_TransmissionType_m1709185389 (UDPReceiver_t3846109956 * __this, int32_t ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// OSCsharp.Net.TransmissionType OSCsharp.Net.UDPReceiver::get_TransmissionType()
extern "C"  int32_t UDPReceiver_get_TransmissionType_m4100389850 (UDPReceiver_t3846109956 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.UDPReceiver::set_MulticastAddress(System.Net.IPAddress)
extern "C"  void UDPReceiver_set_MulticastAddress_m988773996 (UDPReceiver_t3846109956 * __this, IPAddress_t1399971723 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.UDPReceiver::set_ConsumeParsingExceptions(System.Boolean)
extern "C"  void UDPReceiver_set_ConsumeParsingExceptions_m2781480188 (UDPReceiver_t3846109956 * __this, bool ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.AsyncCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void AsyncCallback__ctor_m3071689932 (AsyncCallback_t163412349 * __this, RuntimeObject * p0, IntPtr_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Net.IPAddress OSCsharp.Net.UDPReceiver::get_IPAddress()
extern "C"  IPAddress_t1399971723 * UDPReceiver_get_IPAddress_m257810772 (UDPReceiver_t3846109956 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 OSCsharp.Net.UDPReceiver::get_Port()
extern "C"  int32_t UDPReceiver_get_Port_m3797322946 (UDPReceiver_t3846109956 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.IPEndPoint::.ctor(System.Net.IPAddress,System.Int32)
extern "C"  void IPEndPoint__ctor_m3477723888 (IPEndPoint_t2615413766 * __this, IPAddress_t1399971723 * p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.UDPReceiver::set_IPEndPoint(System.Net.IPEndPoint)
extern "C"  void UDPReceiver_set_IPEndPoint_m4018663211 (UDPReceiver_t3846109956 * __this, IPEndPoint_t2615413766 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Net.IPEndPoint OSCsharp.Net.UDPReceiver::get_IPEndPoint()
extern "C"  IPEndPoint_t2615413766 * UDPReceiver_get_IPEndPoint_m1093884654 (UDPReceiver_t3846109956 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.UdpClient::.ctor(System.Net.IPEndPoint)
extern "C"  void UdpClient__ctor_m881476380 (UdpClient_t1278197702 * __this, IPEndPoint_t2615413766 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.Socket::.ctor(System.Net.Sockets.AddressFamily,System.Net.Sockets.SocketType,System.Net.Sockets.ProtocolType)
extern "C"  void Socket__ctor_m2624772808 (Socket_t3821512045 * __this, int32_t p0, int32_t p1, int32_t p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.Socket::SetSocketOption(System.Net.Sockets.SocketOptionLevel,System.Net.Sockets.SocketOptionName,System.Int32)
extern "C"  void Socket_SetSocketOption_m2520328933 (Socket_t3821512045 * __this, int32_t p0, int32_t p1, int32_t p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.Socket::Bind(System.Net.EndPoint)
extern "C"  void Socket_Bind_m2654205209 (Socket_t3821512045 * __this, EndPoint_t4156119363 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.UdpClient::.ctor()
extern "C"  void UdpClient__ctor_m1976824576 (UdpClient_t1278197702 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.UdpClient::set_Client(System.Net.Sockets.Socket)
extern "C"  void UdpClient_set_Client_m3629597759 (UdpClient_t1278197702 * __this, Socket_t3821512045 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Net.IPAddress OSCsharp.Net.UDPReceiver::get_MulticastAddress()
extern "C"  IPAddress_t1399971723 * UDPReceiver_get_MulticastAddress_m3372256997 (UDPReceiver_t3846109956 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.UdpClient::JoinMulticastGroup(System.Net.IPAddress)
extern "C"  void UdpClient_JoinMulticastGroup_m3983752416 (UdpClient_t1278197702 * __this, IPAddress_t1399971723 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Exception::.ctor()
extern "C"  void Exception__ctor_m3886110570 (Exception_t1927440687 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.UDPReceiver/UdpState::.ctor(System.Net.Sockets.UdpClient,System.Net.IPEndPoint)
extern "C"  void UdpState__ctor_m3537245299 (UdpState_t1122067727 * __this, UdpClient_t1278197702 * ___client0, IPEndPoint_t2615413766 * ___ipEndPoint1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult System.Net.Sockets.UdpClient::BeginReceive(System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* UdpClient_BeginReceive_m3219495788 (UdpClient_t1278197702 * __this, AsyncCallback_t163412349 * p0, RuntimeObject * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.UdpClient::DropMulticastGroup(System.Net.IPAddress)
extern "C"  void UdpClient_DropMulticastGroup_m1415384411 (UdpClient_t1278197702 * __this, IPAddress_t1399971723 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Sockets.UdpClient::Close()
extern "C"  void UdpClient_Close_m4158802552 (UdpClient_t1278197702 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Net.Sockets.UdpClient OSCsharp.Net.UDPReceiver/UdpState::get_Client()
extern "C"  UdpClient_t1278197702 * UdpState_get_Client_m1371175984 (UdpState_t1122067727 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Net.IPEndPoint OSCsharp.Net.UDPReceiver/UdpState::get_IPEndPoint()
extern "C"  IPEndPoint_t2615413766 * UdpState_get_IPEndPoint_m1752194537 (UdpState_t1122067727 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Net.Sockets.UdpClient::EndReceive(System.IAsyncResult,System.Net.IPEndPoint&)
extern "C"  ByteU5BU5D_t3397334013* UdpClient_EndReceive_m1059013247 (UdpClient_t1278197702 * __this, RuntimeObject* p0, IPEndPoint_t2615413766 ** p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.UDPReceiver::parseData(System.Net.IPEndPoint,System.Byte[])
extern "C"  void UDPReceiver_parseData_m902928170 (UDPReceiver_t3846109956 * __this, IPEndPoint_t2615413766 * ___sourceEndPoint0, ByteU5BU5D_t3397334013* ___data1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// OSCsharp.Data.OscPacket OSCsharp.Data.OscPacket::FromByteArray(System.Byte[])
extern "C"  OscPacket_t504761797 * OscPacket_FromByteArray_m476730285 (RuntimeObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___data0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.UDPReceiver::onPacketReceived(OSCsharp.Data.OscPacket)
extern "C"  void UDPReceiver_onPacketReceived_m3250243160 (UDPReceiver_t3846109956 * __this, OscPacket_t504761797 * ___packet0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.UDPReceiver::onBundleReceived(OSCsharp.Data.OscBundle)
extern "C"  void UDPReceiver_onBundleReceived_m562189912 (UDPReceiver_t3846109956 * __this, OscBundle_t1126010605 * ___bundle0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.UDPReceiver::onMessageReceived(OSCsharp.Data.OscMessage)
extern "C"  void UDPReceiver_onMessageReceived_m24838384 (UDPReceiver_t3846109956 * __this, OscMessage_t2764280154 * ___message0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean OSCsharp.Net.UDPReceiver::get_ConsumeParsingExceptions()
extern "C"  bool UDPReceiver_get_ConsumeParsingExceptions_m2647385705 (UDPReceiver_t3846109956 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.UDPReceiver::onError(System.Exception)
extern "C"  void UDPReceiver_onError_m295686389 (UDPReceiver_t3846109956 * __this, Exception_t1927440687 * ___ex0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.OscPacketReceivedEventArgs::.ctor(OSCsharp.Data.OscPacket)
extern "C"  void OscPacketReceivedEventArgs__ctor_m3190781335 (OscPacketReceivedEventArgs_t3282045269 * __this, OscPacket_t504761797 * ___packet0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.EventHandler`1<OSCsharp.Net.OscPacketReceivedEventArgs>::Invoke(System.Object,!0)
#define EventHandler_1_Invoke_m929141680(__this, p0, p1, method) ((  void (*) (EventHandler_1_t1873352441 *, RuntimeObject *, OscPacketReceivedEventArgs_t3282045269 *, const RuntimeMethod*))EventHandler_1_Invoke_m2430999761_gshared)(__this, p0, p1, method)
// System.Void OSCsharp.Net.OscBundleReceivedEventArgs::.ctor(OSCsharp.Data.OscBundle)
extern "C"  void OscBundleReceivedEventArgs__ctor_m2002404183 (OscBundleReceivedEventArgs_t3673224441 * __this, OscBundle_t1126010605 * ___bundle0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.EventHandler`1<OSCsharp.Net.OscBundleReceivedEventArgs>::Invoke(System.Object,!0)
#define EventHandler_1_Invoke_m3195521520(__this, p0, p1, method) ((  void (*) (EventHandler_1_t2264531613 *, RuntimeObject *, OscBundleReceivedEventArgs_t3673224441 *, const RuntimeMethod*))EventHandler_1_Invoke_m2430999761_gshared)(__this, p0, p1, method)
// System.Collections.Generic.IList`1<System.Object> OSCsharp.Data.OscPacket::get_Data()
extern "C"  RuntimeObject* OscPacket_get_Data_m65815223 (OscPacket_t504761797 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.OscMessageReceivedEventArgs::.ctor(OSCsharp.Data.OscMessage)
extern "C"  void OscMessageReceivedEventArgs__ctor_m359379643 (OscMessageReceivedEventArgs_t1263041860 * __this, OscMessage_t2764280154 * ___message0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.EventHandler`1<OSCsharp.Net.OscMessageReceivedEventArgs>::Invoke(System.Object,!0)
#define EventHandler_1_Invoke_m2373711195(__this, p0, p1, method) ((  void (*) (EventHandler_1_t4149316328 *, RuntimeObject *, OscMessageReceivedEventArgs_t1263041860 *, const RuntimeMethod*))EventHandler_1_Invoke_m2430999761_gshared)(__this, p0, p1, method)
// System.Void OSCsharp.Utils.ExceptionEventArgs::.ctor(System.Exception)
extern "C"  void ExceptionEventArgs__ctor_m1196714556 (ExceptionEventArgs_t892713536 * __this, Exception_t1927440687 * ___ex0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.EventHandler`1<OSCsharp.Utils.ExceptionEventArgs>::Invoke(System.Object,!0)
#define EventHandler_1_Invoke_m3855877691(__this, p0, p1, method) ((  void (*) (EventHandler_1_t3778988004 *, RuntimeObject *, ExceptionEventArgs_t892713536 *, const RuntimeMethod*))EventHandler_1_Invoke_m2430999761_gshared)(__this, p0, p1, method)
// System.Void OSCsharp.Net.UDPReceiver/UdpState::set_Client(System.Net.Sockets.UdpClient)
extern "C"  void UdpState_set_Client_m3548438795 (UdpState_t1122067727 * __this, UdpClient_t1278197702 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.UDPReceiver/UdpState::set_IPEndPoint(System.Net.IPEndPoint)
extern "C"  void UdpState_set_IPEndPoint_m3072178698 (UdpState_t1122067727 * __this, IPEndPoint_t2615413766 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Utils.ExceptionEventArgs::set_Exception(System.Exception)
extern "C"  void ExceptionEventArgs_set_Exception_m3571541630 (ExceptionEventArgs_t892713536 * __this, Exception_t1927440687 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean OSCsharp.Data.OscBundle::get_IsBundle()
extern "C"  bool OscBundle_get_IsBundle_m1328128939 (OscBundle_t1126010605 * __this, const RuntimeMethod* method)
{
	{
		return (bool)1;
	}
}
// System.Void OSCsharp.Data.OscBundle::.ctor(OSCsharp.Data.OscTimeTag)
extern "C"  void OscBundle__ctor_m625764563 (OscBundle_t1126010605 * __this, OscTimeTag_t625345318 * ___timeStamp0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OscBundle__ctor_m625764563_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(OscPacket_t504761797_il2cpp_TypeInfo_var);
		OscPacket__ctor_m3636146750(__this, _stringLiteral1664629291, /*hidden argument*/NULL);
		OscTimeTag_t625345318 * L_0 = ___timeStamp0;
		__this->set_timeStamp_4(L_0);
		return;
	}
}
// OSCsharp.Data.OscBundle OSCsharp.Data.OscBundle::FromByteArray(System.Byte[],System.Int32&,System.Int32)
extern "C"  OscBundle_t1126010605 * OscBundle_FromByteArray_m2667454531 (RuntimeObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___data0, int32_t* ___start1, int32_t ___end2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OscBundle_FromByteArray_m2667454531_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	OscTimeTag_t625345318 * V_1 = NULL;
	OscBundle_t1126010605 * V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		ByteU5BU5D_t3397334013* L_0 = ___data0;
		int32_t* L_1 = ___start1;
		IL2CPP_RUNTIME_CLASS_INIT(OscPacket_t504761797_il2cpp_TypeInfo_var);
		String_t* L_2 = OscPacket_ValueFromByteArray_TisString_t_m3665303177(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/OscPacket_ValueFromByteArray_TisString_t_m3665303177_RuntimeMethod_var);
		V_0 = L_2;
		String_t* L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_4 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_3, _stringLiteral1664629291, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001b;
		}
	}
	{
		ArgumentException_t3259014390 * L_5 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_5, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_001b:
	{
		ByteU5BU5D_t3397334013* L_6 = ___data0;
		int32_t* L_7 = ___start1;
		IL2CPP_RUNTIME_CLASS_INIT(OscPacket_t504761797_il2cpp_TypeInfo_var);
		OscTimeTag_t625345318 * L_8 = OscPacket_ValueFromByteArray_TisOscTimeTag_t625345318_m2765599720(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/OscPacket_ValueFromByteArray_TisOscTimeTag_t625345318_m2765599720_RuntimeMethod_var);
		V_1 = L_8;
		OscTimeTag_t625345318 * L_9 = V_1;
		OscBundle_t1126010605 * L_10 = (OscBundle_t1126010605 *)il2cpp_codegen_object_new(OscBundle_t1126010605_il2cpp_TypeInfo_var);
		OscBundle__ctor_m625764563(L_10, L_9, /*hidden argument*/NULL);
		V_2 = L_10;
		goto IL_004a;
	}

IL_002c:
	{
		ByteU5BU5D_t3397334013* L_11 = ___data0;
		int32_t* L_12 = ___start1;
		IL2CPP_RUNTIME_CLASS_INIT(OscPacket_t504761797_il2cpp_TypeInfo_var);
		int32_t L_13 = OscPacket_ValueFromByteArray_TisInt32_t2071877448_m417255044(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/OscPacket_ValueFromByteArray_TisInt32_t2071877448_m417255044_RuntimeMethod_var);
		V_3 = L_13;
		int32_t* L_14 = ___start1;
		int32_t L_15 = V_3;
		V_4 = ((int32_t)((int32_t)(*((int32_t*)L_14))+(int32_t)L_15));
		OscBundle_t1126010605 * L_16 = V_2;
		ByteU5BU5D_t3397334013* L_17 = ___data0;
		int32_t* L_18 = ___start1;
		int32_t L_19 = V_4;
		OscPacket_t504761797 * L_20 = OscPacket_FromByteArray_m3135720259(NULL /*static, unused*/, L_17, L_18, L_19, /*hidden argument*/NULL);
		NullCheck(L_16);
		VirtFuncInvoker1< int32_t, RuntimeObject * >::Invoke(5 /* System.Int32 OSCsharp.Data.OscPacket::Append(System.Object) */, L_16, L_20);
	}

IL_004a:
	{
		int32_t* L_21 = ___start1;
		int32_t L_22 = ___end2;
		if ((((int32_t)(*((int32_t*)L_21))) < ((int32_t)L_22)))
		{
			goto IL_002c;
		}
	}
	{
		OscBundle_t1126010605 * L_23 = V_2;
		return L_23;
	}
}
// System.Int32 OSCsharp.Data.OscBundle::Append(System.Object)
extern "C"  int32_t OscBundle_Append_m347818774 (OscBundle_t1126010605 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OscBundle_Append_m347818774_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	OscBundle_t1126010605 * V_0 = NULL;
	{
		RuntimeObject * L_0 = ___value0;
		if (((OscPacket_t504761797 *)IsInstClass((RuntimeObject*)L_0, OscPacket_t504761797_il2cpp_TypeInfo_var)))
		{
			goto IL_000e;
		}
	}
	{
		ArgumentException_t3259014390 * L_1 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_000e:
	{
		RuntimeObject * L_2 = ___value0;
		V_0 = ((OscBundle_t1126010605 *)IsInstSealed((RuntimeObject*)L_2, OscBundle_t1126010605_il2cpp_TypeInfo_var));
		OscBundle_t1126010605 * L_3 = V_0;
		if (!L_3)
		{
			goto IL_0036;
		}
	}
	{
		OscBundle_t1126010605 * L_4 = V_0;
		NullCheck(L_4);
		OscTimeTag_t625345318 * L_5 = L_4->get_timeStamp_4();
		OscTimeTag_t625345318 * L_6 = __this->get_timeStamp_4();
		IL2CPP_RUNTIME_CLASS_INIT(OscTimeTag_t625345318_il2cpp_TypeInfo_var);
		bool L_7 = OscTimeTag_op_LessThan_m1978965833(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0036;
		}
	}
	{
		Exception_t1927440687 * L_8 = (Exception_t1927440687 *)il2cpp_codegen_object_new(Exception_t1927440687_il2cpp_TypeInfo_var);
		Exception__ctor_m485833136(L_8, _stringLiteral3364703947, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_0036:
	{
		List_1_t2058570427 * L_9 = ((OscPacket_t504761797 *)__this)->get_data_2();
		RuntimeObject * L_10 = ___value0;
		NullCheck(L_9);
		List_1_Add_m4157722533(L_9, L_10, /*hidden argument*/List_1_Add_m4157722533_RuntimeMethod_var);
		List_1_t2058570427 * L_11 = ((OscPacket_t504761797 *)__this)->get_data_2();
		NullCheck(L_11);
		int32_t L_12 = List_1_get_Count_m2375293942(L_11, /*hidden argument*/List_1_get_Count_m2375293942_RuntimeMethod_var);
		return ((int32_t)((int32_t)L_12-(int32_t)1));
	}
}
// System.Boolean OSCsharp.Data.OscMessage::get_IsBundle()
extern "C"  bool OscMessage_get_IsBundle_m4053277334 (OscMessage_t2764280154 * __this, const RuntimeMethod* method)
{
	{
		return (bool)0;
	}
}
// System.Void OSCsharp.Data.OscMessage::.ctor(System.String)
extern "C"  void OscMessage__ctor_m2211520241 (OscMessage_t2764280154 * __this, String_t* ___address0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OscMessage__ctor_m2211520241_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppChar V_0 = 0x0;
	{
		String_t* L_0 = ___address0;
		IL2CPP_RUNTIME_CLASS_INIT(OscPacket_t504761797_il2cpp_TypeInfo_var);
		OscPacket__ctor_m3636146750(__this, L_0, /*hidden argument*/NULL);
		String_t* L_1 = ___address0;
		NullCheck(L_1);
		bool L_2 = String_StartsWith_m1841920685(L_1, _stringLiteral372029315, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_001f;
		}
	}
	{
		ArgumentException_t3259014390 * L_3 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_3, _stringLiteral3377275602, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001f:
	{
		V_0 = ((int32_t)44);
		String_t* L_4 = Char_ToString_m1976753030((&V_0), /*hidden argument*/NULL);
		__this->set_typeTag_19(L_4);
		return;
	}
}
// OSCsharp.Data.OscMessage OSCsharp.Data.OscMessage::FromByteArray(System.Byte[],System.Int32&)
extern "C"  OscMessage_t2764280154 * OscMessage_FromByteArray_m4248709050 (RuntimeObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___data0, int32_t* ___start1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OscMessage_FromByteArray_m4248709050_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	OscMessage_t2764280154 * V_1 = NULL;
	CharU5BU5D_t1328083999* V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	Il2CppChar V_5 = 0x0;
	RuntimeObject * V_6 = NULL;
	Il2CppChar V_7 = 0x0;
	{
		ByteU5BU5D_t3397334013* L_0 = ___data0;
		int32_t* L_1 = ___start1;
		IL2CPP_RUNTIME_CLASS_INIT(OscPacket_t504761797_il2cpp_TypeInfo_var);
		String_t* L_2 = OscPacket_ValueFromByteArray_TisString_t_m3665303177(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/OscPacket_ValueFromByteArray_TisString_t_m3665303177_RuntimeMethod_var);
		V_0 = L_2;
		String_t* L_3 = V_0;
		OscMessage_t2764280154 * L_4 = (OscMessage_t2764280154 *)il2cpp_codegen_object_new(OscMessage_t2764280154_il2cpp_TypeInfo_var);
		OscMessage__ctor_m2211520241(L_4, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		ByteU5BU5D_t3397334013* L_5 = ___data0;
		int32_t* L_6 = ___start1;
		String_t* L_7 = OscPacket_ValueFromByteArray_TisString_t_m3665303177(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/OscPacket_ValueFromByteArray_TisString_t_m3665303177_RuntimeMethod_var);
		NullCheck(L_7);
		CharU5BU5D_t1328083999* L_8 = String_ToCharArray_m870309954(L_7, /*hidden argument*/NULL);
		V_2 = L_8;
		CharU5BU5D_t1328083999* L_9 = V_2;
		NullCheck(L_9);
		V_3 = (((int32_t)((int32_t)(((RuntimeArray *)L_9)->max_length))));
		V_4 = 0;
		goto IL_0167;
	}

IL_0028:
	{
		CharU5BU5D_t1328083999* L_10 = V_2;
		int32_t L_11 = V_4;
		NullCheck(L_10);
		int32_t L_12 = L_11;
		uint16_t L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		V_5 = L_13;
		Il2CppChar L_14 = V_5;
		V_7 = L_14;
		Il2CppChar L_15 = V_7;
		if ((((int32_t)L_15) > ((int32_t)((int32_t)73))))
		{
			goto IL_0058;
		}
	}
	{
		Il2CppChar L_16 = V_7;
		if ((((int32_t)L_16) == ((int32_t)((int32_t)44))))
		{
			goto IL_0161;
		}
	}
	{
		Il2CppChar L_17 = V_7;
		if ((((int32_t)L_17) == ((int32_t)((int32_t)70))))
		{
			goto IL_013d;
		}
	}
	{
		Il2CppChar L_18 = V_7;
		if ((((int32_t)L_18) == ((int32_t)((int32_t)73))))
		{
			goto IL_014c;
		}
	}
	{
		goto IL_0161;
	}

IL_0058:
	{
		Il2CppChar L_19 = V_7;
		if ((((int32_t)L_19) > ((int32_t)((int32_t)84))))
		{
			goto IL_007e;
		}
	}
	{
		Il2CppChar L_20 = V_7;
		if ((((int32_t)L_20) == ((int32_t)((int32_t)78))))
		{
			goto IL_0147;
		}
	}
	{
		Il2CppChar L_21 = V_7;
		switch (((int32_t)((int32_t)L_21-(int32_t)((int32_t)83))))
		{
			case 0:
			{
				goto IL_0102;
			}
			case 1:
			{
				goto IL_0133;
			}
		}
	}
	{
		goto IL_0161;
	}

IL_007e:
	{
		Il2CppChar L_22 = V_7;
		switch (((int32_t)((int32_t)L_22-(int32_t)((int32_t)98))))
		{
			case 0:
			{
				goto IL_010d;
			}
			case 1:
			{
				goto IL_0123;
			}
			case 2:
			{
				goto IL_00f2;
			}
			case 3:
			{
				goto IL_0161;
			}
			case 4:
			{
				goto IL_00e2;
			}
			case 5:
			{
				goto IL_0161;
			}
			case 6:
			{
				goto IL_00d2;
			}
			case 7:
			{
				goto IL_00bf;
			}
		}
	}
	{
		Il2CppChar L_23 = V_7;
		switch (((int32_t)((int32_t)L_23-(int32_t)((int32_t)115))))
		{
			case 0:
			{
				goto IL_0102;
			}
			case 1:
			{
				goto IL_0118;
			}
		}
	}
	{
		goto IL_0161;
	}

IL_00bf:
	{
		ByteU5BU5D_t3397334013* L_24 = ___data0;
		int32_t* L_25 = ___start1;
		IL2CPP_RUNTIME_CLASS_INIT(OscPacket_t504761797_il2cpp_TypeInfo_var);
		int32_t L_26 = OscPacket_ValueFromByteArray_TisInt32_t2071877448_m417255044(NULL /*static, unused*/, L_24, L_25, /*hidden argument*/OscPacket_ValueFromByteArray_TisInt32_t2071877448_m417255044_RuntimeMethod_var);
		int32_t L_27 = L_26;
		RuntimeObject * L_28 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_27);
		V_6 = L_28;
		goto IL_0158;
	}

IL_00d2:
	{
		ByteU5BU5D_t3397334013* L_29 = ___data0;
		int32_t* L_30 = ___start1;
		IL2CPP_RUNTIME_CLASS_INIT(OscPacket_t504761797_il2cpp_TypeInfo_var);
		int64_t L_31 = OscPacket_ValueFromByteArray_TisInt64_t909078037_m2124432849(NULL /*static, unused*/, L_29, L_30, /*hidden argument*/OscPacket_ValueFromByteArray_TisInt64_t909078037_m2124432849_RuntimeMethod_var);
		int64_t L_32 = L_31;
		RuntimeObject * L_33 = Box(Int64_t909078037_il2cpp_TypeInfo_var, &L_32);
		V_6 = L_33;
		goto IL_0158;
	}

IL_00e2:
	{
		ByteU5BU5D_t3397334013* L_34 = ___data0;
		int32_t* L_35 = ___start1;
		IL2CPP_RUNTIME_CLASS_INIT(OscPacket_t504761797_il2cpp_TypeInfo_var);
		float L_36 = OscPacket_ValueFromByteArray_TisSingle_t2076509932_m308377574(NULL /*static, unused*/, L_34, L_35, /*hidden argument*/OscPacket_ValueFromByteArray_TisSingle_t2076509932_m308377574_RuntimeMethod_var);
		float L_37 = L_36;
		RuntimeObject * L_38 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_37);
		V_6 = L_38;
		goto IL_0158;
	}

IL_00f2:
	{
		ByteU5BU5D_t3397334013* L_39 = ___data0;
		int32_t* L_40 = ___start1;
		IL2CPP_RUNTIME_CLASS_INIT(OscPacket_t504761797_il2cpp_TypeInfo_var);
		double L_41 = OscPacket_ValueFromByteArray_TisDouble_t4078015681_m3730957917(NULL /*static, unused*/, L_39, L_40, /*hidden argument*/OscPacket_ValueFromByteArray_TisDouble_t4078015681_m3730957917_RuntimeMethod_var);
		double L_42 = L_41;
		RuntimeObject * L_43 = Box(Double_t4078015681_il2cpp_TypeInfo_var, &L_42);
		V_6 = L_43;
		goto IL_0158;
	}

IL_0102:
	{
		ByteU5BU5D_t3397334013* L_44 = ___data0;
		int32_t* L_45 = ___start1;
		IL2CPP_RUNTIME_CLASS_INIT(OscPacket_t504761797_il2cpp_TypeInfo_var);
		String_t* L_46 = OscPacket_ValueFromByteArray_TisString_t_m3665303177(NULL /*static, unused*/, L_44, L_45, /*hidden argument*/OscPacket_ValueFromByteArray_TisString_t_m3665303177_RuntimeMethod_var);
		V_6 = L_46;
		goto IL_0158;
	}

IL_010d:
	{
		ByteU5BU5D_t3397334013* L_47 = ___data0;
		int32_t* L_48 = ___start1;
		IL2CPP_RUNTIME_CLASS_INIT(OscPacket_t504761797_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3397334013* L_49 = OscPacket_ValueFromByteArray_TisByteU5BU5D_t3397334013_m2572900720(NULL /*static, unused*/, L_47, L_48, /*hidden argument*/OscPacket_ValueFromByteArray_TisByteU5BU5D_t3397334013_m2572900720_RuntimeMethod_var);
		V_6 = (RuntimeObject *)L_49;
		goto IL_0158;
	}

IL_0118:
	{
		ByteU5BU5D_t3397334013* L_50 = ___data0;
		int32_t* L_51 = ___start1;
		IL2CPP_RUNTIME_CLASS_INIT(OscPacket_t504761797_il2cpp_TypeInfo_var);
		OscTimeTag_t625345318 * L_52 = OscPacket_ValueFromByteArray_TisOscTimeTag_t625345318_m2765599720(NULL /*static, unused*/, L_50, L_51, /*hidden argument*/OscPacket_ValueFromByteArray_TisOscTimeTag_t625345318_m2765599720_RuntimeMethod_var);
		V_6 = L_52;
		goto IL_0158;
	}

IL_0123:
	{
		ByteU5BU5D_t3397334013* L_53 = ___data0;
		int32_t* L_54 = ___start1;
		IL2CPP_RUNTIME_CLASS_INIT(OscPacket_t504761797_il2cpp_TypeInfo_var);
		Il2CppChar L_55 = OscPacket_ValueFromByteArray_TisChar_t3454481338_m257312846(NULL /*static, unused*/, L_53, L_54, /*hidden argument*/OscPacket_ValueFromByteArray_TisChar_t3454481338_m257312846_RuntimeMethod_var);
		Il2CppChar L_56 = L_55;
		RuntimeObject * L_57 = Box(Char_t3454481338_il2cpp_TypeInfo_var, &L_56);
		V_6 = L_57;
		goto IL_0158;
	}

IL_0133:
	{
		bool L_58 = ((bool)1);
		RuntimeObject * L_59 = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &L_58);
		V_6 = L_59;
		goto IL_0158;
	}

IL_013d:
	{
		bool L_60 = ((bool)0);
		RuntimeObject * L_61 = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &L_60);
		V_6 = L_61;
		goto IL_0158;
	}

IL_0147:
	{
		V_6 = NULL;
		goto IL_0158;
	}

IL_014c:
	{
		float L_62 = (std::numeric_limits<float>::infinity());
		RuntimeObject * L_63 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_62);
		V_6 = L_63;
	}

IL_0158:
	{
		OscMessage_t2764280154 * L_64 = V_1;
		RuntimeObject * L_65 = V_6;
		NullCheck(L_64);
		VirtFuncInvoker1< int32_t, RuntimeObject * >::Invoke(5 /* System.Int32 OSCsharp.Data.OscPacket::Append(System.Object) */, L_64, L_65);
	}

IL_0161:
	{
		int32_t L_66 = V_4;
		V_4 = ((int32_t)((int32_t)L_66+(int32_t)1));
	}

IL_0167:
	{
		int32_t L_67 = V_4;
		int32_t L_68 = V_3;
		if ((((int32_t)L_67) < ((int32_t)L_68)))
		{
			goto IL_0028;
		}
	}
	{
		OscMessage_t2764280154 * L_69 = V_1;
		return L_69;
	}
}
// System.Int32 OSCsharp.Data.OscMessage::Append(System.Object)
extern "C"  int32_t OscMessage_Append_m2811322561 (OscMessage_t2764280154 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OscMessage_Append_m2811322561_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppChar V_0 = 0x0;
	Type_t * V_1 = NULL;
	String_t* V_2 = NULL;
	int32_t V_3 = 0;
	int32_t G_B13_0 = 0;
	int32_t G_B23_0 = 0;
	{
		RuntimeObject * L_0 = ___value0;
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		V_0 = ((int32_t)78);
		goto IL_014d;
	}

IL_000b:
	{
		RuntimeObject * L_1 = ___value0;
		NullCheck(L_1);
		Type_t * L_2 = Object_GetType_m191970594(L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		Type_t * L_3 = V_1;
		NullCheck(L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_3);
		String_t* L_5 = L_4;
		V_2 = L_5;
		if (!L_5)
		{
			goto IL_0142;
		}
	}
	{
		Dictionary_2_t3986656710 * L_6 = ((U3CPrivateImplementationDetailsU3EU7B2BD1A3B7U2DFF70U2D4DFDU2D9ABDU2D9AF73AFC1A70U7D_t2052325035_StaticFields*)il2cpp_codegen_static_fields_for(U3CPrivateImplementationDetailsU3EU7B2BD1A3B7U2DFF70U2D4DFDU2D9ABDU2D9AF73AFC1A70U7D_t2052325035_il2cpp_TypeInfo_var))->get_U24U24method0x6000029U2D1_0();
		il2cpp_codegen_memory_barrier();
		if (L_6)
		{
			goto IL_00b2;
		}
	}
	{
		Dictionary_2_t3986656710 * L_7 = (Dictionary_2_t3986656710 *)il2cpp_codegen_object_new(Dictionary_2_t3986656710_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2118310873(L_7, ((int32_t)10), /*hidden argument*/Dictionary_2__ctor_m2118310873_RuntimeMethod_var);
		Dictionary_2_t3986656710 * L_8 = L_7;
		NullCheck(L_8);
		Dictionary_2_Add_m1209957957(L_8, _stringLiteral2791486114, 0, /*hidden argument*/Dictionary_2_Add_m1209957957_RuntimeMethod_var);
		Dictionary_2_t3986656710 * L_9 = L_8;
		NullCheck(L_9);
		Dictionary_2_Add_m1209957957(L_9, _stringLiteral2031971229, 1, /*hidden argument*/Dictionary_2_Add_m1209957957_RuntimeMethod_var);
		Dictionary_2_t3986656710 * L_10 = L_9;
		NullCheck(L_10);
		Dictionary_2_Add_m1209957957(L_10, _stringLiteral3134835610, 2, /*hidden argument*/Dictionary_2_Add_m1209957957_RuntimeMethod_var);
		Dictionary_2_t3986656710 * L_11 = L_10;
		NullCheck(L_11);
		Dictionary_2_Add_m1209957957(L_11, _stringLiteral1952587961, 3, /*hidden argument*/Dictionary_2_Add_m1209957957_RuntimeMethod_var);
		Dictionary_2_t3986656710 * L_12 = L_11;
		NullCheck(L_12);
		Dictionary_2_Add_m1209957957(L_12, _stringLiteral3311046853, 4, /*hidden argument*/Dictionary_2_Add_m1209957957_RuntimeMethod_var);
		Dictionary_2_t3986656710 * L_13 = L_12;
		NullCheck(L_13);
		Dictionary_2_Add_m1209957957(L_13, _stringLiteral3975212764, 5, /*hidden argument*/Dictionary_2_Add_m1209957957_RuntimeMethod_var);
		Dictionary_2_t3986656710 * L_14 = L_13;
		NullCheck(L_14);
		Dictionary_2_Add_m1209957957(L_14, _stringLiteral3135498864, 6, /*hidden argument*/Dictionary_2_Add_m1209957957_RuntimeMethod_var);
		Dictionary_2_t3986656710 * L_15 = L_14;
		NullCheck(L_15);
		Dictionary_2_Add_m1209957957(L_15, _stringLiteral681582338, 7, /*hidden argument*/Dictionary_2_Add_m1209957957_RuntimeMethod_var);
		Dictionary_2_t3986656710 * L_16 = L_15;
		NullCheck(L_16);
		Dictionary_2_Add_m1209957957(L_16, _stringLiteral3120862801, 8, /*hidden argument*/Dictionary_2_Add_m1209957957_RuntimeMethod_var);
		Dictionary_2_t3986656710 * L_17 = L_16;
		NullCheck(L_17);
		Dictionary_2_Add_m1209957957(L_17, _stringLiteral191542790, ((int32_t)9), /*hidden argument*/Dictionary_2_Add_m1209957957_RuntimeMethod_var);
		il2cpp_codegen_memory_barrier();
		((U3CPrivateImplementationDetailsU3EU7B2BD1A3B7U2DFF70U2D4DFDU2D9ABDU2D9AF73AFC1A70U7D_t2052325035_StaticFields*)il2cpp_codegen_static_fields_for(U3CPrivateImplementationDetailsU3EU7B2BD1A3B7U2DFF70U2D4DFDU2D9ABDU2D9AF73AFC1A70U7D_t2052325035_il2cpp_TypeInfo_var))->set_U24U24method0x6000029U2D1_0(L_17);
	}

IL_00b2:
	{
		Dictionary_2_t3986656710 * L_18 = ((U3CPrivateImplementationDetailsU3EU7B2BD1A3B7U2DFF70U2D4DFDU2D9ABDU2D9AF73AFC1A70U7D_t2052325035_StaticFields*)il2cpp_codegen_static_fields_for(U3CPrivateImplementationDetailsU3EU7B2BD1A3B7U2DFF70U2D4DFDU2D9ABDU2D9AF73AFC1A70U7D_t2052325035_il2cpp_TypeInfo_var))->get_U24U24method0x6000029U2D1_0();
		il2cpp_codegen_memory_barrier();
		String_t* L_19 = V_2;
		NullCheck(L_18);
		bool L_20 = Dictionary_2_TryGetValue_m2977303364(L_18, L_19, (&V_3), /*hidden argument*/Dictionary_2_TryGetValue_m2977303364_RuntimeMethod_var);
		if (!L_20)
		{
			goto IL_0142;
		}
	}
	{
		int32_t L_21 = V_3;
		switch (L_21)
		{
			case 0:
			{
				goto IL_00f3;
			}
			case 1:
			{
				goto IL_00f8;
			}
			case 2:
			{
				goto IL_00fd;
			}
			case 3:
			{
				goto IL_0113;
			}
			case 4:
			{
				goto IL_0118;
			}
			case 5:
			{
				goto IL_011d;
			}
			case 6:
			{
				goto IL_0122;
			}
			case 7:
			{
				goto IL_0127;
			}
			case 8:
			{
				goto IL_012c;
			}
			case 9:
			{
				goto IL_0131;
			}
		}
	}
	{
		goto IL_0142;
	}

IL_00f3:
	{
		V_0 = ((int32_t)105);
		goto IL_014d;
	}

IL_00f8:
	{
		V_0 = ((int32_t)104);
		goto IL_014d;
	}

IL_00fd:
	{
		RuntimeObject * L_22 = ___value0;
		bool L_23 = Single_IsPositiveInfinity_m869158735(NULL /*static, unused*/, ((*(float*)((float*)UnBox(L_22, Single_t2076509932_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		if (L_23)
		{
			goto IL_010e;
		}
	}
	{
		G_B13_0 = ((int32_t)102);
		goto IL_0110;
	}

IL_010e:
	{
		G_B13_0 = ((int32_t)73);
	}

IL_0110:
	{
		V_0 = G_B13_0;
		goto IL_014d;
	}

IL_0113:
	{
		V_0 = ((int32_t)100);
		goto IL_014d;
	}

IL_0118:
	{
		V_0 = ((int32_t)115);
		goto IL_014d;
	}

IL_011d:
	{
		V_0 = ((int32_t)98);
		goto IL_014d;
	}

IL_0122:
	{
		V_0 = ((int32_t)116);
		goto IL_014d;
	}

IL_0127:
	{
		V_0 = ((int32_t)99);
		goto IL_014d;
	}

IL_012c:
	{
		V_0 = ((int32_t)114);
		goto IL_014d;
	}

IL_0131:
	{
		RuntimeObject * L_24 = ___value0;
		if (((*(bool*)((bool*)UnBox(L_24, Boolean_t3825574718_il2cpp_TypeInfo_var)))))
		{
			goto IL_013d;
		}
	}
	{
		G_B23_0 = ((int32_t)70);
		goto IL_013f;
	}

IL_013d:
	{
		G_B23_0 = ((int32_t)84);
	}

IL_013f:
	{
		V_0 = G_B23_0;
		goto IL_014d;
	}

IL_0142:
	{
		Exception_t1927440687 * L_25 = (Exception_t1927440687 *)il2cpp_codegen_object_new(Exception_t1927440687_il2cpp_TypeInfo_var);
		Exception__ctor_m485833136(L_25, _stringLiteral1729348223, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_25);
	}

IL_014d:
	{
		String_t* L_26 = __this->get_typeTag_19();
		Il2CppChar L_27 = V_0;
		Il2CppChar L_28 = L_27;
		RuntimeObject * L_29 = Box(Char_t3454481338_il2cpp_TypeInfo_var, &L_28);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_30 = String_Concat_m56707527(NULL /*static, unused*/, L_26, L_29, /*hidden argument*/NULL);
		__this->set_typeTag_19(L_30);
		List_1_t2058570427 * L_31 = ((OscPacket_t504761797 *)__this)->get_data_2();
		RuntimeObject * L_32 = ___value0;
		NullCheck(L_31);
		List_1_Add_m4157722533(L_31, L_32, /*hidden argument*/List_1_Add_m4157722533_RuntimeMethod_var);
		List_1_t2058570427 * L_33 = ((OscPacket_t504761797 *)__this)->get_data_2();
		NullCheck(L_33);
		int32_t L_34 = List_1_get_Count_m2375293942(L_33, /*hidden argument*/List_1_get_Count_m2375293942_RuntimeMethod_var);
		return ((int32_t)((int32_t)L_34-(int32_t)1));
	}
}
// System.Boolean OSCsharp.Data.OscPacket::get_LittleEndianByteOrder()
extern "C"  bool OscPacket_get_LittleEndianByteOrder_m1882071268 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OscPacket_get_LittleEndianByteOrder_m1882071268_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(OscPacket_t504761797_il2cpp_TypeInfo_var);
		bool L_0 = ((OscPacket_t504761797_StaticFields*)il2cpp_codegen_static_fields_for(OscPacket_t504761797_il2cpp_TypeInfo_var))->get_littleEndianByteOrder_0();
		return L_0;
	}
}
// System.String OSCsharp.Data.OscPacket::get_Address()
extern "C"  String_t* OscPacket_get_Address_m1502094218 (OscPacket_t504761797 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_address_1();
		return L_0;
	}
}
// System.Void OSCsharp.Data.OscPacket::set_Address(System.String)
extern "C"  void OscPacket_set_Address_m1646762965 (OscPacket_t504761797 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OscPacket_set_Address_m1646762965_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___value0;
		if (L_0)
		{
			goto IL_000a;
		}
	}
	{
		___value0 = _stringLiteral371857150;
	}

IL_000a:
	{
		String_t* L_1 = ___value0;
		__this->set_address_1(L_1);
		return;
	}
}
// System.Collections.Generic.IList`1<System.Object> OSCsharp.Data.OscPacket::get_Data()
extern "C"  RuntimeObject* OscPacket_get_Data_m65815223 (OscPacket_t504761797 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OscPacket_get_Data_m65815223_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t2058570427 * L_0 = __this->get_data_2();
		NullCheck(L_0);
		ReadOnlyCollection_1_t2875234987 * L_1 = List_1_AsReadOnly_m1618299177(L_0, /*hidden argument*/List_1_AsReadOnly_m1618299177_RuntimeMethod_var);
		return L_1;
	}
}
// System.Void OSCsharp.Data.OscPacket::.cctor()
extern "C"  void OscPacket__cctor_m2285063083 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OscPacket__cctor_m2285063083_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((OscPacket_t504761797_StaticFields*)il2cpp_codegen_static_fields_for(OscPacket_t504761797_il2cpp_TypeInfo_var))->set_littleEndianByteOrder_0((bool)0);
		return;
	}
}
// System.Void OSCsharp.Data.OscPacket::.ctor(System.String)
extern "C"  void OscPacket__ctor_m3636146750 (OscPacket_t504761797 * __this, String_t* ___address0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OscPacket__ctor_m3636146750_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___address0;
		OscPacket_set_Address_m1646762965(__this, L_0, /*hidden argument*/NULL);
		List_1_t2058570427 * L_1 = (List_1_t2058570427 *)il2cpp_codegen_object_new(List_1_t2058570427_il2cpp_TypeInfo_var);
		List_1__ctor_m310736118(L_1, /*hidden argument*/List_1__ctor_m310736118_RuntimeMethod_var);
		__this->set_data_2(L_1);
		return;
	}
}
// OSCsharp.Data.OscPacket OSCsharp.Data.OscPacket::FromByteArray(System.Byte[])
extern "C"  OscPacket_t504761797 * OscPacket_FromByteArray_m476730285 (RuntimeObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___data0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OscPacket_FromByteArray_m476730285_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		ByteU5BU5D_t3397334013* L_0 = ___data0;
		if (L_0)
		{
			goto IL_0009;
		}
	}
	{
		ArgumentException_t3259014390 * L_1 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0009:
	{
		V_0 = 0;
		ByteU5BU5D_t3397334013* L_2 = ___data0;
		ByteU5BU5D_t3397334013* L_3 = ___data0;
		NullCheck(L_3);
		IL2CPP_RUNTIME_CLASS_INIT(OscPacket_t504761797_il2cpp_TypeInfo_var);
		OscPacket_t504761797 * L_4 = OscPacket_FromByteArray_m3135720259(NULL /*static, unused*/, L_2, (&V_0), (((int32_t)((int32_t)(((RuntimeArray *)L_3)->max_length)))), /*hidden argument*/NULL);
		return L_4;
	}
}
// OSCsharp.Data.OscPacket OSCsharp.Data.OscPacket::FromByteArray(System.Byte[],System.Int32&,System.Int32)
extern "C"  OscPacket_t504761797 * OscPacket_FromByteArray_m3135720259 (RuntimeObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___data0, int32_t* ___start1, int32_t ___end2, const RuntimeMethod* method)
{
	{
		ByteU5BU5D_t3397334013* L_0 = ___data0;
		int32_t* L_1 = ___start1;
		NullCheck(L_0);
		int32_t L_2 = (*((int32_t*)L_1));
		uint8_t L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		if ((!(((uint32_t)L_3) == ((uint32_t)((int32_t)35)))))
		{
			goto IL_0011;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_4 = ___data0;
		int32_t* L_5 = ___start1;
		int32_t L_6 = ___end2;
		OscBundle_t1126010605 * L_7 = OscBundle_FromByteArray_m2667454531(NULL /*static, unused*/, L_4, L_5, L_6, /*hidden argument*/NULL);
		return L_7;
	}

IL_0011:
	{
		ByteU5BU5D_t3397334013* L_8 = ___data0;
		int32_t* L_9 = ___start1;
		OscMessage_t2764280154 * L_10 = OscMessage_FromByteArray_m4248709050(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		return L_10;
	}
}
// System.DateTime OSCsharp.Data.OscTimeTag::get_DateTime()
extern "C"  DateTime_t693205669  OscTimeTag_get_DateTime_m2172308342 (OscTimeTag_t625345318 * __this, const RuntimeMethod* method)
{
	{
		DateTime_t693205669  L_0 = __this->get_timeStamp_2();
		return L_0;
	}
}
// System.Void OSCsharp.Data.OscTimeTag::.ctor(System.DateTime)
extern "C"  void OscTimeTag__ctor_m1655495837 (OscTimeTag_t625345318 * __this, DateTime_t693205669  ___timeStamp0, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		DateTime_t693205669  L_0 = ___timeStamp0;
		OscTimeTag_Set_m4065929879(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void OSCsharp.Data.OscTimeTag::.ctor(System.Byte[])
extern "C"  void OscTimeTag__ctor_m183269774 (OscTimeTag_t625345318 * __this, ByteU5BU5D_t3397334013* ___data0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OscTimeTag__ctor_m183269774_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_t3397334013* V_0 = NULL;
	ByteU5BU5D_t3397334013* V_1 = NULL;
	uint32_t V_2 = 0;
	uint32_t V_3 = 0;
	DateTime_t693205669  V_4;
	memset(&V_4, 0, sizeof(V_4));
	DateTime_t693205669  V_5;
	memset(&V_5, 0, sizeof(V_5));
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_0 = ___data0;
		ByteU5BU5D_t3397334013* L_1 = Utility_CopySubArray_TisByte_t3683104436_m936098224(NULL /*static, unused*/, L_0, 0, 4, /*hidden argument*/Utility_CopySubArray_TisByte_t3683104436_m936098224_RuntimeMethod_var);
		V_0 = L_1;
		ByteU5BU5D_t3397334013* L_2 = ___data0;
		ByteU5BU5D_t3397334013* L_3 = Utility_CopySubArray_TisByte_t3683104436_m936098224(NULL /*static, unused*/, L_2, 4, 4, /*hidden argument*/Utility_CopySubArray_TisByte_t3683104436_m936098224_RuntimeMethod_var);
		V_1 = L_3;
		IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t3195628829_il2cpp_TypeInfo_var);
		bool L_4 = ((BitConverter_t3195628829_StaticFields*)il2cpp_codegen_static_fields_for(BitConverter_t3195628829_il2cpp_TypeInfo_var))->get_IsLittleEndian_1();
		IL2CPP_RUNTIME_CLASS_INIT(OscPacket_t504761797_il2cpp_TypeInfo_var);
		bool L_5 = OscPacket_get_LittleEndianByteOrder_m1882071268(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_4) == ((int32_t)L_5)))
		{
			goto IL_0032;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_6 = V_0;
		ByteU5BU5D_t3397334013* L_7 = Utility_SwapEndian_m3369642783(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		ByteU5BU5D_t3397334013* L_8 = V_1;
		ByteU5BU5D_t3397334013* L_9 = Utility_SwapEndian_m3369642783(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		V_1 = L_9;
	}

IL_0032:
	{
		ByteU5BU5D_t3397334013* L_10 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t3195628829_il2cpp_TypeInfo_var);
		uint32_t L_11 = BitConverter_ToUInt32_m4153360341(NULL /*static, unused*/, L_10, 0, /*hidden argument*/NULL);
		V_2 = L_11;
		ByteU5BU5D_t3397334013* L_12 = V_1;
		uint32_t L_13 = BitConverter_ToUInt32_m4153360341(NULL /*static, unused*/, L_12, 0, /*hidden argument*/NULL);
		V_3 = L_13;
		IL2CPP_RUNTIME_CLASS_INIT(OscTimeTag_t625345318_il2cpp_TypeInfo_var);
		DateTime_t693205669  L_14 = ((OscTimeTag_t625345318_StaticFields*)il2cpp_codegen_static_fields_for(OscTimeTag_t625345318_il2cpp_TypeInfo_var))->get_Epoch_0();
		V_4 = L_14;
		uint32_t L_15 = V_2;
		DateTime_t693205669  L_16 = DateTime_AddSeconds_m722082155((&V_4), (((double)((double)(((double)((uint32_t)L_15)))))), /*hidden argument*/NULL);
		V_5 = L_16;
		uint32_t L_17 = V_3;
		DateTime_t693205669  L_18 = DateTime_AddMilliseconds_m1813199744((&V_5), (((double)((double)(((double)((uint32_t)L_17)))))), /*hidden argument*/NULL);
		__this->set_timeStamp_2(L_18);
		return;
	}
}
// System.Boolean OSCsharp.Data.OscTimeTag::op_Equality(OSCsharp.Data.OscTimeTag,OSCsharp.Data.OscTimeTag)
extern "C"  bool OscTimeTag_op_Equality_m3410370307 (RuntimeObject * __this /* static, unused */, OscTimeTag_t625345318 * ___lhs0, OscTimeTag_t625345318 * ___rhs1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OscTimeTag_op_Equality_m3410370307_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		OscTimeTag_t625345318 * L_0 = ___lhs0;
		OscTimeTag_t625345318 * L_1 = ___rhs1;
		bool L_2 = Object_ReferenceEquals_m3900584722(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_000b;
		}
	}
	{
		return (bool)1;
	}

IL_000b:
	{
		OscTimeTag_t625345318 * L_3 = ___lhs0;
		if (!L_3)
		{
			goto IL_0011;
		}
	}
	{
		OscTimeTag_t625345318 * L_4 = ___rhs1;
		if (L_4)
		{
			goto IL_0013;
		}
	}

IL_0011:
	{
		return (bool)0;
	}

IL_0013:
	{
		OscTimeTag_t625345318 * L_5 = ___lhs0;
		NullCheck(L_5);
		DateTime_t693205669  L_6 = OscTimeTag_get_DateTime_m2172308342(L_5, /*hidden argument*/NULL);
		OscTimeTag_t625345318 * L_7 = ___rhs1;
		NullCheck(L_7);
		DateTime_t693205669  L_8 = OscTimeTag_get_DateTime_m2172308342(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t693205669_il2cpp_TypeInfo_var);
		bool L_9 = DateTime_op_Equality_m1867073872(NULL /*static, unused*/, L_6, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// System.Boolean OSCsharp.Data.OscTimeTag::op_LessThan(OSCsharp.Data.OscTimeTag,OSCsharp.Data.OscTimeTag)
extern "C"  bool OscTimeTag_op_LessThan_m1978965833 (RuntimeObject * __this /* static, unused */, OscTimeTag_t625345318 * ___lhs0, OscTimeTag_t625345318 * ___rhs1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OscTimeTag_op_LessThan_m1978965833_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		OscTimeTag_t625345318 * L_0 = ___lhs0;
		NullCheck(L_0);
		DateTime_t693205669  L_1 = OscTimeTag_get_DateTime_m2172308342(L_0, /*hidden argument*/NULL);
		OscTimeTag_t625345318 * L_2 = ___rhs1;
		NullCheck(L_2);
		DateTime_t693205669  L_3 = OscTimeTag_get_DateTime_m2172308342(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t693205669_il2cpp_TypeInfo_var);
		bool L_4 = DateTime_op_LessThan_m3944619870(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean OSCsharp.Data.OscTimeTag::IsValidTime(System.DateTime)
extern "C"  bool OscTimeTag_IsValidTime_m2988141586 (RuntimeObject * __this /* static, unused */, DateTime_t693205669  ___timeStamp0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OscTimeTag_IsValidTime_m2988141586_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		DateTime_t693205669  L_0 = ___timeStamp0;
		IL2CPP_RUNTIME_CLASS_INIT(OscTimeTag_t625345318_il2cpp_TypeInfo_var);
		DateTime_t693205669  L_1 = ((OscTimeTag_t625345318_StaticFields*)il2cpp_codegen_static_fields_for(OscTimeTag_t625345318_il2cpp_TypeInfo_var))->get_Epoch_0();
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t3430258949_il2cpp_TypeInfo_var);
		TimeSpan_t3430258949  L_2 = TimeSpan_FromMilliseconds_m664523225(NULL /*static, unused*/, (1.0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t693205669_il2cpp_TypeInfo_var);
		DateTime_t693205669  L_3 = DateTime_op_Addition_m1268923147(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		bool L_4 = DateTime_op_GreaterThanOrEqual_m3818963848(NULL /*static, unused*/, L_0, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void OSCsharp.Data.OscTimeTag::Set(System.DateTime)
extern "C"  void OscTimeTag_Set_m4065929879 (OscTimeTag_t625345318 * __this, DateTime_t693205669  ___timeStamp0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OscTimeTag_Set_m4065929879_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int64_t L_0 = DateTime_get_Ticks_m310281298((&___timeStamp0), /*hidden argument*/NULL);
		int64_t L_1 = DateTime_get_Ticks_m310281298((&___timeStamp0), /*hidden argument*/NULL);
		int32_t L_2 = DateTime_get_Kind_m1331920314((&___timeStamp0), /*hidden argument*/NULL);
		DateTime__ctor_m1100326092((&___timeStamp0), ((int64_t)((int64_t)L_0-(int64_t)((int64_t)((int64_t)L_1%(int64_t)(((int64_t)((int64_t)((int32_t)10000)))))))), L_2, /*hidden argument*/NULL);
		DateTime_t693205669  L_3 = ___timeStamp0;
		IL2CPP_RUNTIME_CLASS_INIT(OscTimeTag_t625345318_il2cpp_TypeInfo_var);
		bool L_4 = OscTimeTag_IsValidTime_m2988141586(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0037;
		}
	}
	{
		ArgumentException_t3259014390 * L_5 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_5, _stringLiteral2402648513, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0037:
	{
		DateTime_t693205669  L_6 = ___timeStamp0;
		__this->set_timeStamp_2(L_6);
		return;
	}
}
// System.Boolean OSCsharp.Data.OscTimeTag::Equals(System.Object)
extern "C"  bool OscTimeTag_Equals_m3221546906 (OscTimeTag_t625345318 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OscTimeTag_Equals_m3221546906_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	OscTimeTag_t625345318 * V_0 = NULL;
	{
		RuntimeObject * L_0 = ___value0;
		if (L_0)
		{
			goto IL_0005;
		}
	}
	{
		return (bool)0;
	}

IL_0005:
	{
		RuntimeObject * L_1 = ___value0;
		V_0 = ((OscTimeTag_t625345318 *)IsInstClass((RuntimeObject*)L_1, OscTimeTag_t625345318_il2cpp_TypeInfo_var));
		OscTimeTag_t625345318 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(OscTimeTag_t625345318_il2cpp_TypeInfo_var);
		bool L_3 = OscTimeTag_op_Equality_m3410370307(NULL /*static, unused*/, L_2, (OscTimeTag_t625345318 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0017;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		DateTime_t693205669 * L_4 = __this->get_address_of_timeStamp_2();
		OscTimeTag_t625345318 * L_5 = V_0;
		NullCheck(L_5);
		DateTime_t693205669  L_6 = L_5->get_timeStamp_2();
		bool L_7 = DateTime_Equals_m1104060551(L_4, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Int32 OSCsharp.Data.OscTimeTag::GetHashCode()
extern "C"  int32_t OscTimeTag_GetHashCode_m1237726830 (OscTimeTag_t625345318 * __this, const RuntimeMethod* method)
{
	{
		DateTime_t693205669 * L_0 = __this->get_address_of_timeStamp_2();
		int32_t L_1 = DateTime_GetHashCode_m974799321(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String OSCsharp.Data.OscTimeTag::ToString()
extern "C"  String_t* OscTimeTag_ToString_m3425168866 (OscTimeTag_t625345318 * __this, const RuntimeMethod* method)
{
	{
		DateTime_t693205669 * L_0 = __this->get_address_of_timeStamp_2();
		String_t* L_1 = DateTime_ToString_m1117481977(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void OSCsharp.Data.OscTimeTag::.cctor()
extern "C"  void OscTimeTag__cctor_m3227110810 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OscTimeTag__cctor_m3227110810_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		DateTime_t693205669  L_0;
		memset(&L_0, 0, sizeof(L_0));
		DateTime__ctor_m2857738939((&L_0), ((int32_t)1900), 1, 1, 0, 0, 0, 0, /*hidden argument*/NULL);
		((OscTimeTag_t625345318_StaticFields*)il2cpp_codegen_static_fields_for(OscTimeTag_t625345318_il2cpp_TypeInfo_var))->set_Epoch_0(L_0);
		DateTime_t693205669  L_1 = ((OscTimeTag_t625345318_StaticFields*)il2cpp_codegen_static_fields_for(OscTimeTag_t625345318_il2cpp_TypeInfo_var))->get_Epoch_0();
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t3430258949_il2cpp_TypeInfo_var);
		TimeSpan_t3430258949  L_2 = TimeSpan_FromMilliseconds_m664523225(NULL /*static, unused*/, (1.0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t693205669_il2cpp_TypeInfo_var);
		DateTime_t693205669  L_3 = DateTime_op_Addition_m1268923147(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		OscTimeTag_t625345318 * L_4 = (OscTimeTag_t625345318 *)il2cpp_codegen_object_new(OscTimeTag_t625345318_il2cpp_TypeInfo_var);
		OscTimeTag__ctor_m1655495837(L_4, L_3, /*hidden argument*/NULL);
		((OscTimeTag_t625345318_StaticFields*)il2cpp_codegen_static_fields_for(OscTimeTag_t625345318_il2cpp_TypeInfo_var))->set_MinValue_1(L_4);
		return;
	}
}
// System.Void OSCsharp.Net.OscBundleReceivedEventArgs::set_Bundle(OSCsharp.Data.OscBundle)
extern "C"  void OscBundleReceivedEventArgs_set_Bundle_m2468455950 (OscBundleReceivedEventArgs_t3673224441 * __this, OscBundle_t1126010605 * ___value0, const RuntimeMethod* method)
{
	{
		OscBundle_t1126010605 * L_0 = ___value0;
		__this->set_U3CBundleU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void OSCsharp.Net.OscBundleReceivedEventArgs::.ctor(OSCsharp.Data.OscBundle)
extern "C"  void OscBundleReceivedEventArgs__ctor_m2002404183 (OscBundleReceivedEventArgs_t3673224441 * __this, OscBundle_t1126010605 * ___bundle0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OscBundleReceivedEventArgs__ctor_m2002404183_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t3289624707_il2cpp_TypeInfo_var);
		EventArgs__ctor_m3696060910(__this, /*hidden argument*/NULL);
		OscBundle_t1126010605 * L_0 = ___bundle0;
		OscBundleReceivedEventArgs_set_Bundle_m2468455950(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// OSCsharp.Data.OscMessage OSCsharp.Net.OscMessageReceivedEventArgs::get_Message()
extern "C"  OscMessage_t2764280154 * OscMessageReceivedEventArgs_get_Message_m3135203290 (OscMessageReceivedEventArgs_t1263041860 * __this, const RuntimeMethod* method)
{
	{
		OscMessage_t2764280154 * L_0 = __this->get_U3CMessageU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void OSCsharp.Net.OscMessageReceivedEventArgs::set_Message(OSCsharp.Data.OscMessage)
extern "C"  void OscMessageReceivedEventArgs_set_Message_m841798933 (OscMessageReceivedEventArgs_t1263041860 * __this, OscMessage_t2764280154 * ___value0, const RuntimeMethod* method)
{
	{
		OscMessage_t2764280154 * L_0 = ___value0;
		__this->set_U3CMessageU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void OSCsharp.Net.OscMessageReceivedEventArgs::.ctor(OSCsharp.Data.OscMessage)
extern "C"  void OscMessageReceivedEventArgs__ctor_m359379643 (OscMessageReceivedEventArgs_t1263041860 * __this, OscMessage_t2764280154 * ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OscMessageReceivedEventArgs__ctor_m359379643_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t3289624707_il2cpp_TypeInfo_var);
		EventArgs__ctor_m3696060910(__this, /*hidden argument*/NULL);
		OscMessage_t2764280154 * L_0 = ___message0;
		OscMessageReceivedEventArgs_set_Message_m841798933(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void OSCsharp.Net.OscPacketReceivedEventArgs::set_Packet(OSCsharp.Data.OscPacket)
extern "C"  void OscPacketReceivedEventArgs_set_Packet_m2983320626 (OscPacketReceivedEventArgs_t3282045269 * __this, OscPacket_t504761797 * ___value0, const RuntimeMethod* method)
{
	{
		OscPacket_t504761797 * L_0 = ___value0;
		__this->set_U3CPacketU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void OSCsharp.Net.OscPacketReceivedEventArgs::.ctor(OSCsharp.Data.OscPacket)
extern "C"  void OscPacketReceivedEventArgs__ctor_m3190781335 (OscPacketReceivedEventArgs_t3282045269 * __this, OscPacket_t504761797 * ___packet0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OscPacketReceivedEventArgs__ctor_m3190781335_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t3289624707_il2cpp_TypeInfo_var);
		EventArgs__ctor_m3696060910(__this, /*hidden argument*/NULL);
		OscPacket_t504761797 * L_0 = ___packet0;
		OscPacketReceivedEventArgs_set_Packet_m2983320626(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void OSCsharp.Net.UDPReceiver::add_ErrorOccured(System.EventHandler`1<OSCsharp.Utils.ExceptionEventArgs>)
extern "C"  void UDPReceiver_add_ErrorOccured_m1212653731 (UDPReceiver_t3846109956 * __this, EventHandler_1_t3778988004 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UDPReceiver_add_ErrorOccured_m1212653731_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t3778988004 * V_0 = NULL;
	EventHandler_1_t3778988004 * V_1 = NULL;
	EventHandler_1_t3778988004 * V_2 = NULL;
	{
		EventHandler_1_t3778988004 * L_0 = __this->get_ErrorOccured_2();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t3778988004 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t3778988004 * L_2 = V_1;
		EventHandler_1_t3778988004 * L_3 = ___value0;
		Delegate_t3022476291 * L_4 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((EventHandler_1_t3778988004 *)CastclassSealed((RuntimeObject*)L_4, EventHandler_1_t3778988004_il2cpp_TypeInfo_var));
		EventHandler_1_t3778988004 ** L_5 = __this->get_address_of_ErrorOccured_2();
		EventHandler_1_t3778988004 * L_6 = V_2;
		EventHandler_1_t3778988004 * L_7 = V_1;
		EventHandler_1_t3778988004 * L_8 = InterlockedCompareExchangeImpl<EventHandler_1_t3778988004 *>(L_5, L_6, L_7);
		V_0 = L_8;
		EventHandler_1_t3778988004 * L_9 = V_0;
		EventHandler_1_t3778988004 * L_10 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_t3778988004 *)L_9) == ((RuntimeObject*)(EventHandler_1_t3778988004 *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void OSCsharp.Net.UDPReceiver::remove_ErrorOccured(System.EventHandler`1<OSCsharp.Utils.ExceptionEventArgs>)
extern "C"  void UDPReceiver_remove_ErrorOccured_m1224019382 (UDPReceiver_t3846109956 * __this, EventHandler_1_t3778988004 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UDPReceiver_remove_ErrorOccured_m1224019382_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t3778988004 * V_0 = NULL;
	EventHandler_1_t3778988004 * V_1 = NULL;
	EventHandler_1_t3778988004 * V_2 = NULL;
	{
		EventHandler_1_t3778988004 * L_0 = __this->get_ErrorOccured_2();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t3778988004 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t3778988004 * L_2 = V_1;
		EventHandler_1_t3778988004 * L_3 = ___value0;
		Delegate_t3022476291 * L_4 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((EventHandler_1_t3778988004 *)CastclassSealed((RuntimeObject*)L_4, EventHandler_1_t3778988004_il2cpp_TypeInfo_var));
		EventHandler_1_t3778988004 ** L_5 = __this->get_address_of_ErrorOccured_2();
		EventHandler_1_t3778988004 * L_6 = V_2;
		EventHandler_1_t3778988004 * L_7 = V_1;
		EventHandler_1_t3778988004 * L_8 = InterlockedCompareExchangeImpl<EventHandler_1_t3778988004 *>(L_5, L_6, L_7);
		V_0 = L_8;
		EventHandler_1_t3778988004 * L_9 = V_0;
		EventHandler_1_t3778988004 * L_10 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_t3778988004 *)L_9) == ((RuntimeObject*)(EventHandler_1_t3778988004 *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void OSCsharp.Net.UDPReceiver::add_MessageReceived(System.EventHandler`1<OSCsharp.Net.OscMessageReceivedEventArgs>)
extern "C"  void UDPReceiver_add_MessageReceived_m3470532992 (UDPReceiver_t3846109956 * __this, EventHandler_1_t4149316328 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UDPReceiver_add_MessageReceived_m3470532992_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		EventHandler_1_t4149316328 * L_0 = __this->get_messageReceivedInvoker_3();
		EventHandler_1_t4149316328 * L_1 = ___value0;
		Delegate_t3022476291 * L_2 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_messageReceivedInvoker_3(((EventHandler_1_t4149316328 *)CastclassSealed((RuntimeObject*)L_2, EventHandler_1_t4149316328_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void OSCsharp.Net.UDPReceiver::remove_MessageReceived(System.EventHandler`1<OSCsharp.Net.OscMessageReceivedEventArgs>)
extern "C"  void UDPReceiver_remove_MessageReceived_m3894025529 (UDPReceiver_t3846109956 * __this, EventHandler_1_t4149316328 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UDPReceiver_remove_MessageReceived_m3894025529_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		EventHandler_1_t4149316328 * L_0 = __this->get_messageReceivedInvoker_3();
		EventHandler_1_t4149316328 * L_1 = ___value0;
		Delegate_t3022476291 * L_2 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_messageReceivedInvoker_3(((EventHandler_1_t4149316328 *)CastclassSealed((RuntimeObject*)L_2, EventHandler_1_t4149316328_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Net.IPAddress OSCsharp.Net.UDPReceiver::get_IPAddress()
extern "C"  IPAddress_t1399971723 * UDPReceiver_get_IPAddress_m257810772 (UDPReceiver_t3846109956 * __this, const RuntimeMethod* method)
{
	{
		IPAddress_t1399971723 * L_0 = __this->get_U3CIPAddressU3Ek__BackingField_7();
		return L_0;
	}
}
// System.Void OSCsharp.Net.UDPReceiver::set_IPAddress(System.Net.IPAddress)
extern "C"  void UDPReceiver_set_IPAddress_m3792884775 (UDPReceiver_t3846109956 * __this, IPAddress_t1399971723 * ___value0, const RuntimeMethod* method)
{
	{
		IPAddress_t1399971723 * L_0 = ___value0;
		__this->set_U3CIPAddressU3Ek__BackingField_7(L_0);
		return;
	}
}
// System.Int32 OSCsharp.Net.UDPReceiver::get_Port()
extern "C"  int32_t UDPReceiver_get_Port_m3797322946 (UDPReceiver_t3846109956 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_U3CPortU3Ek__BackingField_8();
		return L_0;
	}
}
// System.Void OSCsharp.Net.UDPReceiver::set_Port(System.Int32)
extern "C"  void UDPReceiver_set_Port_m2491900965 (UDPReceiver_t3846109956 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CPortU3Ek__BackingField_8(L_0);
		return;
	}
}
// System.Net.IPAddress OSCsharp.Net.UDPReceiver::get_MulticastAddress()
extern "C"  IPAddress_t1399971723 * UDPReceiver_get_MulticastAddress_m3372256997 (UDPReceiver_t3846109956 * __this, const RuntimeMethod* method)
{
	{
		IPAddress_t1399971723 * L_0 = __this->get_U3CMulticastAddressU3Ek__BackingField_9();
		return L_0;
	}
}
// System.Void OSCsharp.Net.UDPReceiver::set_MulticastAddress(System.Net.IPAddress)
extern "C"  void UDPReceiver_set_MulticastAddress_m988773996 (UDPReceiver_t3846109956 * __this, IPAddress_t1399971723 * ___value0, const RuntimeMethod* method)
{
	{
		IPAddress_t1399971723 * L_0 = ___value0;
		__this->set_U3CMulticastAddressU3Ek__BackingField_9(L_0);
		return;
	}
}
// System.Net.IPEndPoint OSCsharp.Net.UDPReceiver::get_IPEndPoint()
extern "C"  IPEndPoint_t2615413766 * UDPReceiver_get_IPEndPoint_m1093884654 (UDPReceiver_t3846109956 * __this, const RuntimeMethod* method)
{
	{
		IPEndPoint_t2615413766 * L_0 = __this->get_U3CIPEndPointU3Ek__BackingField_10();
		return L_0;
	}
}
// System.Void OSCsharp.Net.UDPReceiver::set_IPEndPoint(System.Net.IPEndPoint)
extern "C"  void UDPReceiver_set_IPEndPoint_m4018663211 (UDPReceiver_t3846109956 * __this, IPEndPoint_t2615413766 * ___value0, const RuntimeMethod* method)
{
	{
		IPEndPoint_t2615413766 * L_0 = ___value0;
		__this->set_U3CIPEndPointU3Ek__BackingField_10(L_0);
		return;
	}
}
// OSCsharp.Net.TransmissionType OSCsharp.Net.UDPReceiver::get_TransmissionType()
extern "C"  int32_t UDPReceiver_get_TransmissionType_m4100389850 (UDPReceiver_t3846109956 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_U3CTransmissionTypeU3Ek__BackingField_11();
		return L_0;
	}
}
// System.Void OSCsharp.Net.UDPReceiver::set_TransmissionType(OSCsharp.Net.TransmissionType)
extern "C"  void UDPReceiver_set_TransmissionType_m1709185389 (UDPReceiver_t3846109956 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CTransmissionTypeU3Ek__BackingField_11(L_0);
		return;
	}
}
// System.Boolean OSCsharp.Net.UDPReceiver::get_ConsumeParsingExceptions()
extern "C"  bool UDPReceiver_get_ConsumeParsingExceptions_m2647385705 (UDPReceiver_t3846109956 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_U3CConsumeParsingExceptionsU3Ek__BackingField_12();
		return L_0;
	}
}
// System.Void OSCsharp.Net.UDPReceiver::set_ConsumeParsingExceptions(System.Boolean)
extern "C"  void UDPReceiver_set_ConsumeParsingExceptions_m2781480188 (UDPReceiver_t3846109956 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CConsumeParsingExceptionsU3Ek__BackingField_12(L_0);
		return;
	}
}
// System.Boolean OSCsharp.Net.UDPReceiver::get_IsRunning()
extern "C"  bool UDPReceiver_get_IsRunning_m2474408206 (UDPReceiver_t3846109956 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_acceptingConnections_5();
		il2cpp_codegen_memory_barrier();
		return L_0;
	}
}
// System.Void OSCsharp.Net.UDPReceiver::.ctor(System.Int32,System.Boolean)
extern "C"  void UDPReceiver__ctor_m3124846498 (UDPReceiver_t3846109956 * __this, int32_t ___port0, bool ___consumeParsingExceptions1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UDPReceiver__ctor_m3124846498_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IPAddress_t1399971723_il2cpp_TypeInfo_var);
		IPAddress_t1399971723 * L_0 = ((IPAddress_t1399971723_StaticFields*)il2cpp_codegen_static_fields_for(IPAddress_t1399971723_il2cpp_TypeInfo_var))->get_Any_4();
		int32_t L_1 = ___port0;
		bool L_2 = ___consumeParsingExceptions1;
		UDPReceiver__ctor_m3514168723(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void OSCsharp.Net.UDPReceiver::.ctor(System.Net.IPAddress,System.Int32,System.Boolean)
extern "C"  void UDPReceiver__ctor_m3514168723 (UDPReceiver_t3846109956 * __this, IPAddress_t1399971723 * ___ipAddress0, int32_t ___port1, bool ___consumeParsingExceptions2, const RuntimeMethod* method)
{
	{
		IPAddress_t1399971723 * L_0 = ___ipAddress0;
		int32_t L_1 = ___port1;
		bool L_2 = ___consumeParsingExceptions2;
		UDPReceiver__ctor_m896685042(__this, L_0, L_1, 0, (IPAddress_t1399971723 *)NULL, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void OSCsharp.Net.UDPReceiver::.ctor(System.Net.IPAddress,System.Int32,OSCsharp.Net.TransmissionType,System.Net.IPAddress,System.Boolean)
extern "C"  void UDPReceiver__ctor_m896685042 (UDPReceiver_t3846109956 * __this, IPAddress_t1399971723 * ___ipAddress0, int32_t ___port1, int32_t ___transmissionType2, IPAddress_t1399971723 * ___multicastAddress3, bool ___consumeParsingExceptions4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UDPReceiver__ctor_m896685042_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		IPAddress_t1399971723 * L_0 = ___ipAddress0;
		UDPReceiver_set_IPAddress_m3792884775(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = ___port1;
		UDPReceiver_set_Port_m2491900965(__this, L_1, /*hidden argument*/NULL);
		int32_t L_2 = ___transmissionType2;
		UDPReceiver_set_TransmissionType_m1709185389(__this, L_2, /*hidden argument*/NULL);
		int32_t L_3 = UDPReceiver_get_TransmissionType_m4100389850(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)1))))
		{
			goto IL_003b;
		}
	}
	{
		IPAddress_t1399971723 * L_4 = ___multicastAddress3;
		if (L_4)
		{
			goto IL_0033;
		}
	}
	{
		ArgumentException_t3259014390 * L_5 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_5, _stringLiteral1477022020, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		IPAddress_t1399971723 * L_6 = ___multicastAddress3;
		UDPReceiver_set_MulticastAddress_m988773996(__this, L_6, /*hidden argument*/NULL);
	}

IL_003b:
	{
		bool L_7 = ___consumeParsingExceptions4;
		UDPReceiver_set_ConsumeParsingExceptions_m2781480188(__this, L_7, /*hidden argument*/NULL);
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)UDPReceiver_endReceive_m4073855157_RuntimeMethod_var);
		AsyncCallback_t163412349 * L_9 = (AsyncCallback_t163412349 *)il2cpp_codegen_object_new(AsyncCallback_t163412349_il2cpp_TypeInfo_var);
		AsyncCallback__ctor_m3071689932(L_9, __this, L_8, /*hidden argument*/NULL);
		__this->set_callback_6(L_9);
		return;
	}
}
// System.Void OSCsharp.Net.UDPReceiver::Start()
extern "C"  void UDPReceiver_Start_m1033630334 (UDPReceiver_t3846109956 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UDPReceiver_Start_m1033630334_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Socket_t3821512045 * V_0 = NULL;
	UdpState_t1122067727 * V_1 = NULL;
	int32_t V_2 = 0;
	{
		int32_t L_0 = UDPReceiver_get_TransmissionType_m4100389850(__this, /*hidden argument*/NULL);
		V_2 = L_0;
		int32_t L_1 = V_2;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0022;
			}
			case 1:
			{
				goto IL_004f;
			}
			case 2:
			{
				goto IL_00b2;
			}
			case 3:
			{
				goto IL_00b2;
			}
		}
	}
	{
		goto IL_00db;
	}

IL_0022:
	{
		IPAddress_t1399971723 * L_2 = UDPReceiver_get_IPAddress_m257810772(__this, /*hidden argument*/NULL);
		int32_t L_3 = UDPReceiver_get_Port_m3797322946(__this, /*hidden argument*/NULL);
		IPEndPoint_t2615413766 * L_4 = (IPEndPoint_t2615413766 *)il2cpp_codegen_object_new(IPEndPoint_t2615413766_il2cpp_TypeInfo_var);
		IPEndPoint__ctor_m3477723888(L_4, L_2, L_3, /*hidden argument*/NULL);
		UDPReceiver_set_IPEndPoint_m4018663211(__this, L_4, /*hidden argument*/NULL);
		IPEndPoint_t2615413766 * L_5 = UDPReceiver_get_IPEndPoint_m1093884654(__this, /*hidden argument*/NULL);
		UdpClient_t1278197702 * L_6 = (UdpClient_t1278197702 *)il2cpp_codegen_object_new(UdpClient_t1278197702_il2cpp_TypeInfo_var);
		UdpClient__ctor_m881476380(L_6, L_5, /*hidden argument*/NULL);
		__this->set_udpClient_4(L_6);
		goto IL_00e1;
	}

IL_004f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IPAddress_t1399971723_il2cpp_TypeInfo_var);
		IPAddress_t1399971723 * L_7 = ((IPAddress_t1399971723_StaticFields*)il2cpp_codegen_static_fields_for(IPAddress_t1399971723_il2cpp_TypeInfo_var))->get_Any_4();
		int32_t L_8 = UDPReceiver_get_Port_m3797322946(__this, /*hidden argument*/NULL);
		IPEndPoint_t2615413766 * L_9 = (IPEndPoint_t2615413766 *)il2cpp_codegen_object_new(IPEndPoint_t2615413766_il2cpp_TypeInfo_var);
		IPEndPoint__ctor_m3477723888(L_9, L_7, L_8, /*hidden argument*/NULL);
		UDPReceiver_set_IPEndPoint_m4018663211(__this, L_9, /*hidden argument*/NULL);
		Socket_t3821512045 * L_10 = (Socket_t3821512045 *)il2cpp_codegen_object_new(Socket_t3821512045_il2cpp_TypeInfo_var);
		Socket__ctor_m2624772808(L_10, 2, 2, ((int32_t)17), /*hidden argument*/NULL);
		V_0 = L_10;
		Socket_t3821512045 * L_11 = V_0;
		NullCheck(L_11);
		Socket_SetSocketOption_m2520328933(L_11, ((int32_t)65535), 4, 1, /*hidden argument*/NULL);
		Socket_t3821512045 * L_12 = V_0;
		IPEndPoint_t2615413766 * L_13 = UDPReceiver_get_IPEndPoint_m1093884654(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		Socket_Bind_m2654205209(L_12, L_13, /*hidden argument*/NULL);
		UdpClient_t1278197702 * L_14 = (UdpClient_t1278197702 *)il2cpp_codegen_object_new(UdpClient_t1278197702_il2cpp_TypeInfo_var);
		UdpClient__ctor_m1976824576(L_14, /*hidden argument*/NULL);
		__this->set_udpClient_4(L_14);
		UdpClient_t1278197702 * L_15 = __this->get_udpClient_4();
		Socket_t3821512045 * L_16 = V_0;
		NullCheck(L_15);
		UdpClient_set_Client_m3629597759(L_15, L_16, /*hidden argument*/NULL);
		UdpClient_t1278197702 * L_17 = __this->get_udpClient_4();
		IPAddress_t1399971723 * L_18 = UDPReceiver_get_MulticastAddress_m3372256997(__this, /*hidden argument*/NULL);
		NullCheck(L_17);
		UdpClient_JoinMulticastGroup_m3983752416(L_17, L_18, /*hidden argument*/NULL);
		goto IL_00e1;
	}

IL_00b2:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IPAddress_t1399971723_il2cpp_TypeInfo_var);
		IPAddress_t1399971723 * L_19 = ((IPAddress_t1399971723_StaticFields*)il2cpp_codegen_static_fields_for(IPAddress_t1399971723_il2cpp_TypeInfo_var))->get_Any_4();
		int32_t L_20 = UDPReceiver_get_Port_m3797322946(__this, /*hidden argument*/NULL);
		IPEndPoint_t2615413766 * L_21 = (IPEndPoint_t2615413766 *)il2cpp_codegen_object_new(IPEndPoint_t2615413766_il2cpp_TypeInfo_var);
		IPEndPoint__ctor_m3477723888(L_21, L_19, L_20, /*hidden argument*/NULL);
		UDPReceiver_set_IPEndPoint_m4018663211(__this, L_21, /*hidden argument*/NULL);
		IPEndPoint_t2615413766 * L_22 = UDPReceiver_get_IPEndPoint_m1093884654(__this, /*hidden argument*/NULL);
		UdpClient_t1278197702 * L_23 = (UdpClient_t1278197702 *)il2cpp_codegen_object_new(UdpClient_t1278197702_il2cpp_TypeInfo_var);
		UdpClient__ctor_m881476380(L_23, L_22, /*hidden argument*/NULL);
		__this->set_udpClient_4(L_23);
		goto IL_00e1;
	}

IL_00db:
	{
		Exception_t1927440687 * L_24 = (Exception_t1927440687 *)il2cpp_codegen_object_new(Exception_t1927440687_il2cpp_TypeInfo_var);
		Exception__ctor_m3886110570(L_24, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_24);
	}

IL_00e1:
	{
		UdpClient_t1278197702 * L_25 = __this->get_udpClient_4();
		IPEndPoint_t2615413766 * L_26 = UDPReceiver_get_IPEndPoint_m1093884654(__this, /*hidden argument*/NULL);
		UdpState_t1122067727 * L_27 = (UdpState_t1122067727 *)il2cpp_codegen_object_new(UdpState_t1122067727_il2cpp_TypeInfo_var);
		UdpState__ctor_m3537245299(L_27, L_25, L_26, /*hidden argument*/NULL);
		V_1 = L_27;
		il2cpp_codegen_memory_barrier();
		__this->set_acceptingConnections_5(1);
		UdpClient_t1278197702 * L_28 = __this->get_udpClient_4();
		AsyncCallback_t163412349 * L_29 = __this->get_callback_6();
		UdpState_t1122067727 * L_30 = V_1;
		NullCheck(L_28);
		UdpClient_BeginReceive_m3219495788(L_28, L_29, L_30, /*hidden argument*/NULL);
		return;
	}
}
// System.Void OSCsharp.Net.UDPReceiver::Stop()
extern "C"  void UDPReceiver_Stop_m3061802084 (UDPReceiver_t3846109956 * __this, const RuntimeMethod* method)
{
	{
		il2cpp_codegen_memory_barrier();
		__this->set_acceptingConnections_5(0);
		UdpClient_t1278197702 * L_0 = __this->get_udpClient_4();
		if (!L_0)
		{
			goto IL_0036;
		}
	}
	{
		int32_t L_1 = UDPReceiver_get_TransmissionType_m4100389850(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_002b;
		}
	}
	{
		UdpClient_t1278197702 * L_2 = __this->get_udpClient_4();
		IPAddress_t1399971723 * L_3 = UDPReceiver_get_MulticastAddress_m3372256997(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		UdpClient_DropMulticastGroup_m1415384411(L_2, L_3, /*hidden argument*/NULL);
	}

IL_002b:
	{
		UdpClient_t1278197702 * L_4 = __this->get_udpClient_4();
		NullCheck(L_4);
		UdpClient_Close_m4158802552(L_4, /*hidden argument*/NULL);
	}

IL_0036:
	{
		return;
	}
}
// System.Void OSCsharp.Net.UDPReceiver::endReceive(System.IAsyncResult)
extern "C"  void UDPReceiver_endReceive_m4073855157 (UDPReceiver_t3846109956 * __this, RuntimeObject* ___asyncResult0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UDPReceiver_endReceive_m4073855157_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UdpState_t1122067727 * V_0 = NULL;
	UdpClient_t1278197702 * V_1 = NULL;
	IPEndPoint_t2615413766 * V_2 = NULL;
	ByteU5BU5D_t3397334013* V_3 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			RuntimeObject* L_0 = ___asyncResult0;
			NullCheck(L_0);
			RuntimeObject * L_1 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(0 /* System.Object System.IAsyncResult::get_AsyncState() */, IAsyncResult_t1999651008_il2cpp_TypeInfo_var, L_0);
			V_0 = ((UdpState_t1122067727 *)CastclassClass((RuntimeObject*)L_1, UdpState_t1122067727_il2cpp_TypeInfo_var));
			UdpState_t1122067727 * L_2 = V_0;
			NullCheck(L_2);
			UdpClient_t1278197702 * L_3 = UdpState_get_Client_m1371175984(L_2, /*hidden argument*/NULL);
			V_1 = L_3;
			UdpState_t1122067727 * L_4 = V_0;
			NullCheck(L_4);
			IPEndPoint_t2615413766 * L_5 = UdpState_get_IPEndPoint_m1752194537(L_4, /*hidden argument*/NULL);
			V_2 = L_5;
			UdpClient_t1278197702 * L_6 = V_1;
			RuntimeObject* L_7 = ___asyncResult0;
			NullCheck(L_6);
			ByteU5BU5D_t3397334013* L_8 = UdpClient_EndReceive_m1059013247(L_6, L_7, (&V_2), /*hidden argument*/NULL);
			V_3 = L_8;
			ByteU5BU5D_t3397334013* L_9 = V_3;
			if (!L_9)
			{
				goto IL_0035;
			}
		}

IL_0027:
		{
			ByteU5BU5D_t3397334013* L_10 = V_3;
			NullCheck(L_10);
			if ((((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_10)->max_length))))) <= ((int32_t)0)))
			{
				goto IL_0035;
			}
		}

IL_002d:
		{
			IPEndPoint_t2615413766 * L_11 = V_2;
			ByteU5BU5D_t3397334013* L_12 = V_3;
			UDPReceiver_parseData_m902928170(__this, L_11, L_12, /*hidden argument*/NULL);
		}

IL_0035:
		{
			bool L_13 = __this->get_acceptingConnections_5();
			il2cpp_codegen_memory_barrier();
			if (!L_13)
			{
				goto IL_004d;
			}
		}

IL_003f:
		{
			UdpClient_t1278197702 * L_14 = V_1;
			AsyncCallback_t163412349 * L_15 = __this->get_callback_6();
			UdpState_t1122067727 * L_16 = V_0;
			NullCheck(L_14);
			UdpClient_BeginReceive_m3219495788(L_14, L_15, L_16, /*hidden argument*/NULL);
		}

IL_004d:
		{
			goto IL_0052;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_004f;
		throw e;
	}

CATCH_004f:
	{ // begin catch(System.ObjectDisposedException)
		goto IL_0052;
	} // end catch (depth: 1)

IL_0052:
	{
		return;
	}
}
// System.Void OSCsharp.Net.UDPReceiver::parseData(System.Net.IPEndPoint,System.Byte[])
extern "C"  void UDPReceiver_parseData_m902928170 (UDPReceiver_t3846109956 * __this, IPEndPoint_t2615413766 * ___sourceEndPoint0, ByteU5BU5D_t3397334013* ___data1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UDPReceiver_parseData_m902928170_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	OscPacket_t504761797 * V_0 = NULL;
	Exception_t1927440687 * V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			ByteU5BU5D_t3397334013* L_0 = ___data1;
			IL2CPP_RUNTIME_CLASS_INIT(OscPacket_t504761797_il2cpp_TypeInfo_var);
			OscPacket_t504761797 * L_1 = OscPacket_FromByteArray_m476730285(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
			V_0 = L_1;
			OscPacket_t504761797 * L_2 = V_0;
			UDPReceiver_onPacketReceived_m3250243160(__this, L_2, /*hidden argument*/NULL);
			OscPacket_t504761797 * L_3 = V_0;
			NullCheck(L_3);
			bool L_4 = VirtFuncInvoker0< bool >::Invoke(4 /* System.Boolean OSCsharp.Data.OscPacket::get_IsBundle() */, L_3);
			if (!L_4)
			{
				goto IL_0024;
			}
		}

IL_0016:
		{
			OscPacket_t504761797 * L_5 = V_0;
			UDPReceiver_onBundleReceived_m562189912(__this, ((OscBundle_t1126010605 *)IsInstSealed((RuntimeObject*)L_5, OscBundle_t1126010605_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
			goto IL_0030;
		}

IL_0024:
		{
			OscPacket_t504761797 * L_6 = V_0;
			UDPReceiver_onMessageReceived_m24838384(__this, ((OscMessage_t2764280154 *)IsInstClass((RuntimeObject*)L_6, OscMessage_t2764280154_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		}

IL_0030:
		{
			goto IL_0044;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1927440687_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0032;
		throw e;
	}

CATCH_0032:
	{ // begin catch(System.Exception)
		{
			V_1 = ((Exception_t1927440687 *)__exception_local);
			bool L_7 = UDPReceiver_get_ConsumeParsingExceptions_m2647385705(__this, /*hidden argument*/NULL);
			if (L_7)
			{
				goto IL_0042;
			}
		}

IL_003b:
		{
			Exception_t1927440687 * L_8 = V_1;
			UDPReceiver_onError_m295686389(__this, L_8, /*hidden argument*/NULL);
		}

IL_0042:
		{
			goto IL_0044;
		}
	} // end catch (depth: 1)

IL_0044:
	{
		return;
	}
}
// System.Void OSCsharp.Net.UDPReceiver::onPacketReceived(OSCsharp.Data.OscPacket)
extern "C"  void UDPReceiver_onPacketReceived_m3250243160 (UDPReceiver_t3846109956 * __this, OscPacket_t504761797 * ___packet0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UDPReceiver_onPacketReceived_m3250243160_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		EventHandler_1_t1873352441 * L_0 = __this->get_PacketReceived_0();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		EventHandler_1_t1873352441 * L_1 = __this->get_PacketReceived_0();
		OscPacket_t504761797 * L_2 = ___packet0;
		OscPacketReceivedEventArgs_t3282045269 * L_3 = (OscPacketReceivedEventArgs_t3282045269 *)il2cpp_codegen_object_new(OscPacketReceivedEventArgs_t3282045269_il2cpp_TypeInfo_var);
		OscPacketReceivedEventArgs__ctor_m3190781335(L_3, L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		EventHandler_1_Invoke_m929141680(L_1, __this, L_3, /*hidden argument*/EventHandler_1_Invoke_m929141680_RuntimeMethod_var);
	}

IL_001a:
	{
		return;
	}
}
// System.Void OSCsharp.Net.UDPReceiver::onBundleReceived(OSCsharp.Data.OscBundle)
extern "C"  void UDPReceiver_onBundleReceived_m562189912 (UDPReceiver_t3846109956 * __this, OscBundle_t1126010605 * ___bundle0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UDPReceiver_onBundleReceived_m562189912_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	RuntimeObject * V_2 = NULL;
	OscMessage_t2764280154 * V_3 = NULL;
	{
		EventHandler_1_t2264531613 * L_0 = __this->get_BundleReceived_1();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		EventHandler_1_t2264531613 * L_1 = __this->get_BundleReceived_1();
		OscBundle_t1126010605 * L_2 = ___bundle0;
		OscBundleReceivedEventArgs_t3673224441 * L_3 = (OscBundleReceivedEventArgs_t3673224441 *)il2cpp_codegen_object_new(OscBundleReceivedEventArgs_t3673224441_il2cpp_TypeInfo_var);
		OscBundleReceivedEventArgs__ctor_m2002404183(L_3, L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		EventHandler_1_Invoke_m3195521520(L_1, __this, L_3, /*hidden argument*/EventHandler_1_Invoke_m3195521520_RuntimeMethod_var);
	}

IL_001a:
	{
		OscBundle_t1126010605 * L_4 = ___bundle0;
		NullCheck(L_4);
		RuntimeObject* L_5 = OscPacket_get_Data_m65815223(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		int32_t L_6 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, ICollection_1_t3641524600_il2cpp_TypeInfo_var, L_5);
		V_0 = L_6;
		V_1 = 0;
		goto IL_0067;
	}

IL_002a:
	{
		OscBundle_t1126010605 * L_7 = ___bundle0;
		NullCheck(L_7);
		RuntimeObject* L_8 = OscPacket_get_Data_m65815223(L_7, /*hidden argument*/NULL);
		int32_t L_9 = V_1;
		NullCheck(L_8);
		RuntimeObject * L_10 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t3230389896_il2cpp_TypeInfo_var, L_8, L_9);
		V_2 = L_10;
		RuntimeObject * L_11 = V_2;
		if (!((OscBundle_t1126010605 *)IsInstSealed((RuntimeObject*)L_11, OscBundle_t1126010605_il2cpp_TypeInfo_var)))
		{
			goto IL_004d;
		}
	}
	{
		RuntimeObject * L_12 = V_2;
		UDPReceiver_onBundleReceived_m562189912(__this, ((OscBundle_t1126010605 *)CastclassSealed((RuntimeObject*)L_12, OscBundle_t1126010605_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		goto IL_0063;
	}

IL_004d:
	{
		RuntimeObject * L_13 = V_2;
		if (!((OscMessage_t2764280154 *)IsInstClass((RuntimeObject*)L_13, OscMessage_t2764280154_il2cpp_TypeInfo_var)))
		{
			goto IL_0063;
		}
	}
	{
		RuntimeObject * L_14 = V_2;
		V_3 = ((OscMessage_t2764280154 *)CastclassClass((RuntimeObject*)L_14, OscMessage_t2764280154_il2cpp_TypeInfo_var));
		OscMessage_t2764280154 * L_15 = V_3;
		UDPReceiver_onMessageReceived_m24838384(__this, L_15, /*hidden argument*/NULL);
	}

IL_0063:
	{
		int32_t L_16 = V_1;
		V_1 = ((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0067:
	{
		int32_t L_17 = V_1;
		int32_t L_18 = V_0;
		if ((((int32_t)L_17) < ((int32_t)L_18)))
		{
			goto IL_002a;
		}
	}
	{
		return;
	}
}
// System.Void OSCsharp.Net.UDPReceiver::onMessageReceived(OSCsharp.Data.OscMessage)
extern "C"  void UDPReceiver_onMessageReceived_m24838384 (UDPReceiver_t3846109956 * __this, OscMessage_t2764280154 * ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UDPReceiver_onMessageReceived_m24838384_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		EventHandler_1_t4149316328 * L_0 = __this->get_messageReceivedInvoker_3();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		EventHandler_1_t4149316328 * L_1 = __this->get_messageReceivedInvoker_3();
		OscMessage_t2764280154 * L_2 = ___message0;
		OscMessageReceivedEventArgs_t1263041860 * L_3 = (OscMessageReceivedEventArgs_t1263041860 *)il2cpp_codegen_object_new(OscMessageReceivedEventArgs_t1263041860_il2cpp_TypeInfo_var);
		OscMessageReceivedEventArgs__ctor_m359379643(L_3, L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		EventHandler_1_Invoke_m2373711195(L_1, __this, L_3, /*hidden argument*/EventHandler_1_Invoke_m2373711195_RuntimeMethod_var);
	}

IL_001a:
	{
		return;
	}
}
// System.Void OSCsharp.Net.UDPReceiver::onError(System.Exception)
extern "C"  void UDPReceiver_onError_m295686389 (UDPReceiver_t3846109956 * __this, Exception_t1927440687 * ___ex0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UDPReceiver_onError_m295686389_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		EventHandler_1_t3778988004 * L_0 = __this->get_ErrorOccured_2();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		EventHandler_1_t3778988004 * L_1 = __this->get_ErrorOccured_2();
		Exception_t1927440687 * L_2 = ___ex0;
		ExceptionEventArgs_t892713536 * L_3 = (ExceptionEventArgs_t892713536 *)il2cpp_codegen_object_new(ExceptionEventArgs_t892713536_il2cpp_TypeInfo_var);
		ExceptionEventArgs__ctor_m1196714556(L_3, L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		EventHandler_1_Invoke_m3855877691(L_1, __this, L_3, /*hidden argument*/EventHandler_1_Invoke_m3855877691_RuntimeMethod_var);
	}

IL_001a:
	{
		return;
	}
}
// System.Net.Sockets.UdpClient OSCsharp.Net.UDPReceiver/UdpState::get_Client()
extern "C"  UdpClient_t1278197702 * UdpState_get_Client_m1371175984 (UdpState_t1122067727 * __this, const RuntimeMethod* method)
{
	{
		UdpClient_t1278197702 * L_0 = __this->get_U3CClientU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void OSCsharp.Net.UDPReceiver/UdpState::set_Client(System.Net.Sockets.UdpClient)
extern "C"  void UdpState_set_Client_m3548438795 (UdpState_t1122067727 * __this, UdpClient_t1278197702 * ___value0, const RuntimeMethod* method)
{
	{
		UdpClient_t1278197702 * L_0 = ___value0;
		__this->set_U3CClientU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Net.IPEndPoint OSCsharp.Net.UDPReceiver/UdpState::get_IPEndPoint()
extern "C"  IPEndPoint_t2615413766 * UdpState_get_IPEndPoint_m1752194537 (UdpState_t1122067727 * __this, const RuntimeMethod* method)
{
	{
		IPEndPoint_t2615413766 * L_0 = __this->get_U3CIPEndPointU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void OSCsharp.Net.UDPReceiver/UdpState::set_IPEndPoint(System.Net.IPEndPoint)
extern "C"  void UdpState_set_IPEndPoint_m3072178698 (UdpState_t1122067727 * __this, IPEndPoint_t2615413766 * ___value0, const RuntimeMethod* method)
{
	{
		IPEndPoint_t2615413766 * L_0 = ___value0;
		__this->set_U3CIPEndPointU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void OSCsharp.Net.UDPReceiver/UdpState::.ctor(System.Net.Sockets.UdpClient,System.Net.IPEndPoint)
extern "C"  void UdpState__ctor_m3537245299 (UdpState_t1122067727 * __this, UdpClient_t1278197702 * ___client0, IPEndPoint_t2615413766 * ___ipEndPoint1, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		UdpClient_t1278197702 * L_0 = ___client0;
		UdpState_set_Client_m3548438795(__this, L_0, /*hidden argument*/NULL);
		IPEndPoint_t2615413766 * L_1 = ___ipEndPoint1;
		UdpState_set_IPEndPoint_m3072178698(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void OSCsharp.Utils.ExceptionEventArgs::set_Exception(System.Exception)
extern "C"  void ExceptionEventArgs_set_Exception_m3571541630 (ExceptionEventArgs_t892713536 * __this, Exception_t1927440687 * ___value0, const RuntimeMethod* method)
{
	{
		Exception_t1927440687 * L_0 = ___value0;
		__this->set_U3CExceptionU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void OSCsharp.Utils.ExceptionEventArgs::.ctor(System.Exception)
extern "C"  void ExceptionEventArgs__ctor_m1196714556 (ExceptionEventArgs_t892713536 * __this, Exception_t1927440687 * ___ex0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ExceptionEventArgs__ctor_m1196714556_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t3289624707_il2cpp_TypeInfo_var);
		EventArgs__ctor_m3696060910(__this, /*hidden argument*/NULL);
		Exception_t1927440687 * L_0 = ___ex0;
		ExceptionEventArgs_set_Exception_m3571541630(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Byte[] OSCsharp.Utils.Utility::SwapEndian(System.Byte[])
extern "C"  ByteU5BU5D_t3397334013* Utility_SwapEndian_m3369642783 (RuntimeObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___data0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Utility_SwapEndian_m3369642783_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_t3397334013* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		ByteU5BU5D_t3397334013* L_0 = ___data0;
		NullCheck(L_0);
		V_0 = ((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_0)->max_length))))));
		ByteU5BU5D_t3397334013* L_1 = ___data0;
		NullCheck(L_1);
		V_1 = ((int32_t)((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_1)->max_length))))-(int32_t)1));
		V_2 = 0;
		goto IL_0021;
	}

IL_0013:
	{
		ByteU5BU5D_t3397334013* L_2 = V_0;
		int32_t L_3 = V_2;
		ByteU5BU5D_t3397334013* L_4 = ___data0;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		uint8_t L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		NullCheck(L_2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_3), (uint8_t)L_7);
		int32_t L_8 = V_1;
		V_1 = ((int32_t)((int32_t)L_8-(int32_t)1));
		int32_t L_9 = V_2;
		V_2 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0021:
	{
		int32_t L_10 = V_1;
		if ((((int32_t)L_10) >= ((int32_t)0)))
		{
			goto IL_0013;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_11 = V_0;
		return L_11;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
