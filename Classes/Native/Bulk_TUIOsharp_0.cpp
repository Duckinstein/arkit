﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// TUIOsharp.DataProcessors.BlobProcessor
struct BlobProcessor_t3603341577;
// System.EventHandler`1<TUIOsharp.DataProcessors.TuioBlobEventArgs>
struct EventHandler_1_t2154285351;
// System.Delegate
struct Delegate_t3022476291;
// OSCsharp.Data.OscMessage
struct OscMessage_t2764280154;
// OSCsharp.Data.OscPacket
struct OscPacket_t504761797;
// System.String
struct String_t;
// System.Collections.Generic.IList`1<System.Object>
struct IList_1_t3230389896;
// System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioBlob>
struct Dictionary_2_t1054769049;
// TUIOsharp.Entities.TuioBlob
struct TuioBlob_t2046943414;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Object>
struct Dictionary_2_t1697274930;
// System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>
struct List_1_t1416064546;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1440998580;
// TUIOsharp.Entities.TuioEntity
struct TuioEntity_t295276718;
// TUIOsharp.DataProcessors.TuioBlobEventArgs
struct TuioBlobEventArgs_t3562978179;
// System.EventHandler`1<System.Object>
struct EventHandler_1_t1280756467;
// TUIOsharp.DataProcessors.CursorProcessor
struct CursorProcessor_t1785954004;
// System.EventHandler`1<TUIOsharp.DataProcessors.TuioCursorEventArgs>
struct EventHandler_1_t3249264480;
// System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioCursor>
struct Dictionary_2_t858177054;
// TUIOsharp.Entities.TuioCursor
struct TuioCursor_t1850351419;
// System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>
struct List_1_t1219472551;
// TUIOsharp.DataProcessors.TuioCursorEventArgs
struct TuioCursorEventArgs_t362990012;
// TUIOsharp.DataProcessors.ObjectProcessor
struct ObjectProcessor_t221569383;
// System.EventHandler`1<TUIOsharp.DataProcessors.TuioObjectEventArgs>
struct EventHandler_1_t402050513;
// System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioObject>
struct Dictionary_2_t244646649;
// TUIOsharp.Entities.TuioObject
struct TuioObject_t1236821014;
// System.Collections.Generic.List`1<TUIOsharp.Entities.TuioObject>
struct List_1_t605942146;
// TUIOsharp.DataProcessors.TuioObjectEventArgs
struct TuioObjectEventArgs_t1810743341;
// System.EventArgs
struct EventArgs_t3289624707;
// TUIOsharp.TuioServer
struct TuioServer_t595520884;
// System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>
struct List_1_t1583176531;
// OSCsharp.Net.UDPReceiver
struct UDPReceiver_t3846109956;
// System.EventHandler`1<OSCsharp.Net.OscMessageReceivedEventArgs>
struct EventHandler_1_t4149316328;
// System.EventHandler`1<OSCsharp.Utils.ExceptionEventArgs>
struct EventHandler_1_t3778988004;
// TUIOsharp.DataProcessors.IDataProcessor
struct IDataProcessor_t2214055399;
// OSCsharp.Utils.ExceptionEventArgs
struct ExceptionEventArgs_t892713536;
// OSCsharp.Net.OscMessageReceivedEventArgs
struct OscMessageReceivedEventArgs_t1263041860;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.Collections.Generic.Link[]
struct LinkU5BU5D_t62501539;
// TUIOsharp.Entities.TuioBlob[]
struct TuioBlobU5BU5D_t3918161843;
// System.Collections.Generic.IEqualityComparer`1<System.Int32>
struct IEqualityComparer_1_t1284510226;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,TUIOsharp.Entities.TuioBlob,System.Collections.DictionaryEntry>
struct Transform_1_t307591862;
// TUIOsharp.Entities.TuioObject[]
struct TuioObjectU5BU5D_t3851389651;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,TUIOsharp.Entities.TuioObject,System.Collections.DictionaryEntry>
struct Transform_1_t240819670;
// TUIOsharp.Entities.TuioCursor[]
struct TuioCursorU5BU5D_t885656954;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,TUIOsharp.Entities.TuioCursor,System.Collections.DictionaryEntry>
struct Transform_1_t1570054269;
// TUIOsharp.DataProcessors.IDataProcessor[]
struct IDataProcessorU5BU5D_t215962270;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.Void
struct Void_t1841601450;
// System.Exception
struct Exception_t1927440687;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1572802995;
// System.EventHandler`1<OSCsharp.Net.OscPacketReceivedEventArgs>
struct EventHandler_1_t1873352441;
// System.EventHandler`1<OSCsharp.Net.OscBundleReceivedEventArgs>
struct EventHandler_1_t2264531613;
// System.Net.Sockets.UdpClient
struct UdpClient_t1278197702;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// System.Net.IPAddress
struct IPAddress_t1399971723;
// System.Net.IPEndPoint
struct IPEndPoint_t2615413766;
// System.IAsyncResult
struct IAsyncResult_t1999651008;

extern RuntimeClass* EventHandler_1_t2154285351_il2cpp_TypeInfo_var;
extern const uint32_t BlobProcessor_add_BlobAdded_m1362271275_MetadataUsageId;
extern const uint32_t BlobProcessor_remove_BlobAdded_m3134592944_MetadataUsageId;
extern const uint32_t BlobProcessor_add_BlobUpdated_m1642149482_MetadataUsageId;
extern const uint32_t BlobProcessor_remove_BlobUpdated_m1870432989_MetadataUsageId;
extern const uint32_t BlobProcessor_add_BlobRemoved_m486758865_MetadataUsageId;
extern const uint32_t BlobProcessor_remove_BlobRemoved_m1953649818_MetadataUsageId;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern RuntimeClass* IList_1_t3230389896_il2cpp_TypeInfo_var;
extern RuntimeClass* ICollection_1_t3641524600_il2cpp_TypeInfo_var;
extern RuntimeClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern RuntimeClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern RuntimeClass* TuioBlob_t2046943414_il2cpp_TypeInfo_var;
extern RuntimeClass* TuioBlobEventArgs_t3562978179_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Dictionary_2_TryGetValue_m1913696641_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m2273408866_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m688682013_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_GetEnumerator_m2501398850_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m3060082652_RuntimeMethod_var;
extern const RuntimeMethod* KeyValuePair_2_get_Key_m758296990_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Contains_m3517981001_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Remove_m282822032_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m2555211001_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m797473218_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_m2267760198_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_m3035119279_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_ContainsKey_m3923242129_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_Add_m4879272_RuntimeMethod_var;
extern const RuntimeMethod* EventHandler_1_Invoke_m897217542_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_m3833405771_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_m3514532498_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_get_Item_m3637178650_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_Remove_m3017945611_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Clear_m3896574426_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Clear_m1175215109_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral760373117;
extern Il2CppCodeGenString* _stringLiteral3021628834;
extern Il2CppCodeGenString* _stringLiteral2273848675;
extern Il2CppCodeGenString* _stringLiteral1851287145;
extern const uint32_t BlobProcessor_ProcessMessage_m3196822674_MetadataUsageId;
extern RuntimeClass* Dictionary_2_t1054769049_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t1416064546_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t1440998580_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Dictionary_2__ctor_m92522272_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m1986803350_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m1598946593_RuntimeMethod_var;
extern const uint32_t BlobProcessor__ctor_m1736416811_MetadataUsageId;
extern RuntimeClass* EventHandler_1_t3249264480_il2cpp_TypeInfo_var;
extern const uint32_t CursorProcessor_add_CursorAdded_m3817569644_MetadataUsageId;
extern const uint32_t CursorProcessor_remove_CursorAdded_m11359957_MetadataUsageId;
extern const uint32_t CursorProcessor_add_CursorUpdated_m1967314603_MetadataUsageId;
extern const uint32_t CursorProcessor_remove_CursorUpdated_m107114368_MetadataUsageId;
extern const uint32_t CursorProcessor_add_CursorRemoved_m4072586006_MetadataUsageId;
extern const uint32_t CursorProcessor_remove_CursorRemoved_m2056441227_MetadataUsageId;
extern RuntimeClass* TuioCursor_t1850351419_il2cpp_TypeInfo_var;
extern RuntimeClass* TuioCursorEventArgs_t362990012_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Dictionary_2_TryGetValue_m1936579402_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m2438782433_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_GetEnumerator_m775930629_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m2497618057_RuntimeMethod_var;
extern const RuntimeMethod* KeyValuePair_2_get_Key_m624710547_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m3093636814_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m4213942773_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_m302198121_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_m2568686926_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_ContainsKey_m2357860370_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_Add_m1095222709_RuntimeMethod_var;
extern const RuntimeMethod* EventHandler_1_Invoke_m2962849243_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_get_Item_m866734463_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_Remove_m2712103718_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Clear_m2159038286_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral262980497;
extern const uint32_t CursorProcessor_ProcessMessage_m823963401_MetadataUsageId;
extern RuntimeClass* Dictionary_2_t858177054_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t1219472551_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Dictionary_2__ctor_m1652351677_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m3131880517_RuntimeMethod_var;
extern const uint32_t CursorProcessor__ctor_m3237265030_MetadataUsageId;
extern RuntimeClass* EventHandler_1_t402050513_il2cpp_TypeInfo_var;
extern const uint32_t ObjectProcessor_add_ObjectAdded_m4092744429_MetadataUsageId;
extern const uint32_t ObjectProcessor_remove_ObjectAdded_m3070580308_MetadataUsageId;
extern const uint32_t ObjectProcessor_add_ObjectUpdated_m2549856334_MetadataUsageId;
extern const uint32_t ObjectProcessor_remove_ObjectUpdated_m3226679647_MetadataUsageId;
extern const uint32_t ObjectProcessor_add_ObjectRemoved_m3544020203_MetadataUsageId;
extern const uint32_t ObjectProcessor_remove_ObjectRemoved_m2344088226_MetadataUsageId;
extern RuntimeClass* TuioObject_t1236821014_il2cpp_TypeInfo_var;
extern RuntimeClass* TuioObjectEventArgs_t1810743341_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Dictionary_2_TryGetValue_m1599847323_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m4210503130_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_GetEnumerator_m2329010490_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m874299548_RuntimeMethod_var;
extern const RuntimeMethod* KeyValuePair_2_get_Key_m303742594_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m3089698347_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m4106138594_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_m1698483734_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_m992642369_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_ContainsKey_m781358407_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_Add_m2557918960_RuntimeMethod_var;
extern const RuntimeMethod* EventHandler_1_Invoke_m1340678838_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_get_Item_m4097636154_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_Remove_m185872653_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Clear_m2478926039_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral310035036;
extern const uint32_t ObjectProcessor_ProcessMessage_m1510623726_MetadataUsageId;
extern RuntimeClass* Dictionary_2_t244646649_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t605942146_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Dictionary_2__ctor_m1364836728_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m4191486726_RuntimeMethod_var;
extern const uint32_t ObjectProcessor__ctor_m2046048849_MetadataUsageId;
extern RuntimeClass* EventArgs_t3289624707_il2cpp_TypeInfo_var;
extern const uint32_t TuioBlobEventArgs__ctor_m3423126035_MetadataUsageId;
extern const uint32_t TuioCursorEventArgs__ctor_m2169915347_MetadataUsageId;
extern const uint32_t TuioObjectEventArgs__ctor_m1688032627_MetadataUsageId;
extern RuntimeClass* List_1_t1583176531_il2cpp_TypeInfo_var;
extern RuntimeClass* UDPReceiver_t3846109956_il2cpp_TypeInfo_var;
extern RuntimeClass* EventHandler_1_t4149316328_il2cpp_TypeInfo_var;
extern RuntimeClass* EventHandler_1_t3778988004_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1__ctor_m4170624131_RuntimeMethod_var;
extern const RuntimeMethod* TuioServer_handlerOscMessageReceived_m1229429870_RuntimeMethod_var;
extern const RuntimeMethod* EventHandler_1__ctor_m384786560_RuntimeMethod_var;
extern const RuntimeMethod* TuioServer_handlerOscErrorOccured_m1202150247_RuntimeMethod_var;
extern const RuntimeMethod* EventHandler_1__ctor_m4043119420_RuntimeMethod_var;
extern const uint32_t TuioServer__ctor_m230865672_MetadataUsageId;
extern const RuntimeMethod* List_1_Contains_m181590265_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m2594654071_RuntimeMethod_var;
extern const uint32_t TuioServer_AddDataProcessor_m2160836129_MetadataUsageId;
extern const RuntimeMethod* List_1_Remove_m3595814454_RuntimeMethod_var;
extern const uint32_t TuioServer_RemoveDataProcessor_m1509103320_MetadataUsageId;
extern const RuntimeMethod* List_1_Clear_m3203701604_RuntimeMethod_var;
extern const uint32_t TuioServer_RemoveAllDataProcessors_m4138463077_MetadataUsageId;
extern RuntimeClass* IDataProcessor_t2214055399_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1_get_Count_m314033343_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_m2840009344_RuntimeMethod_var;
extern const uint32_t TuioServer_processMessage_m3007744804_MetadataUsageId;
extern const RuntimeMethod* EventHandler_1_Invoke_m3855877691_RuntimeMethod_var;
extern const uint32_t TuioServer_handlerOscErrorOccured_m1202150247_MetadataUsageId;



#ifndef U3CMODULEU3E_T3783534223_H
#define U3CMODULEU3E_T3783534223_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t3783534223 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T3783534223_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef DICTIONARY_2_T1054769049_H
#define DICTIONARY_2_T1054769049_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioBlob>
struct  Dictionary_2_t1054769049  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t3030399641* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t62501539* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	Int32U5BU5D_t3030399641* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	TuioBlobU5BU5D_t3918161843* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t228987430 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t1054769049, ___table_4)); }
	inline Int32U5BU5D_t3030399641* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t3030399641** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t3030399641* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t1054769049, ___linkSlots_5)); }
	inline LinkU5BU5D_t62501539* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t62501539** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t62501539* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t1054769049, ___keySlots_6)); }
	inline Int32U5BU5D_t3030399641* get_keySlots_6() const { return ___keySlots_6; }
	inline Int32U5BU5D_t3030399641** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(Int32U5BU5D_t3030399641* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t1054769049, ___valueSlots_7)); }
	inline TuioBlobU5BU5D_t3918161843* get_valueSlots_7() const { return ___valueSlots_7; }
	inline TuioBlobU5BU5D_t3918161843** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(TuioBlobU5BU5D_t3918161843* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t1054769049, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t1054769049, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t1054769049, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t1054769049, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t1054769049, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t1054769049, ___serialization_info_13)); }
	inline SerializationInfo_t228987430 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t228987430 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t228987430 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t1054769049, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t1054769049_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t307591862 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t1054769049_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t307591862 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t307591862 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t307591862 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T1054769049_H
#ifndef LIST_1_T1416064546_H
#define LIST_1_T1416064546_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>
struct  List_1_t1416064546  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	TuioBlobU5BU5D_t3918161843* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t1416064546, ____items_1)); }
	inline TuioBlobU5BU5D_t3918161843* get__items_1() const { return ____items_1; }
	inline TuioBlobU5BU5D_t3918161843** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(TuioBlobU5BU5D_t3918161843* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t1416064546, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t1416064546, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t1416064546_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	TuioBlobU5BU5D_t3918161843* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t1416064546_StaticFields, ___EmptyArray_4)); }
	inline TuioBlobU5BU5D_t3918161843* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline TuioBlobU5BU5D_t3918161843** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(TuioBlobU5BU5D_t3918161843* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T1416064546_H
#ifndef OSCPACKET_T504761797_H
#define OSCPACKET_T504761797_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OSCsharp.Data.OscPacket
struct  OscPacket_t504761797  : public RuntimeObject
{
public:
	// System.String OSCsharp.Data.OscPacket::address
	String_t* ___address_1;
	// System.Collections.Generic.List`1<System.Object> OSCsharp.Data.OscPacket::data
	List_1_t2058570427 * ___data_2;

public:
	inline static int32_t get_offset_of_address_1() { return static_cast<int32_t>(offsetof(OscPacket_t504761797, ___address_1)); }
	inline String_t* get_address_1() const { return ___address_1; }
	inline String_t** get_address_of_address_1() { return &___address_1; }
	inline void set_address_1(String_t* value)
	{
		___address_1 = value;
		Il2CppCodeGenWriteBarrier((&___address_1), value);
	}

	inline static int32_t get_offset_of_data_2() { return static_cast<int32_t>(offsetof(OscPacket_t504761797, ___data_2)); }
	inline List_1_t2058570427 * get_data_2() const { return ___data_2; }
	inline List_1_t2058570427 ** get_address_of_data_2() { return &___data_2; }
	inline void set_data_2(List_1_t2058570427 * value)
	{
		___data_2 = value;
		Il2CppCodeGenWriteBarrier((&___data_2), value);
	}
};

struct OscPacket_t504761797_StaticFields
{
public:
	// System.Boolean OSCsharp.Data.OscPacket::littleEndianByteOrder
	bool ___littleEndianByteOrder_0;

public:
	inline static int32_t get_offset_of_littleEndianByteOrder_0() { return static_cast<int32_t>(offsetof(OscPacket_t504761797_StaticFields, ___littleEndianByteOrder_0)); }
	inline bool get_littleEndianByteOrder_0() const { return ___littleEndianByteOrder_0; }
	inline bool* get_address_of_littleEndianByteOrder_0() { return &___littleEndianByteOrder_0; }
	inline void set_littleEndianByteOrder_0(bool value)
	{
		___littleEndianByteOrder_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OSCPACKET_T504761797_H
#ifndef DICTIONARY_2_T244646649_H
#define DICTIONARY_2_T244646649_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioObject>
struct  Dictionary_2_t244646649  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t3030399641* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t62501539* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	Int32U5BU5D_t3030399641* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	TuioObjectU5BU5D_t3851389651* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t228987430 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t244646649, ___table_4)); }
	inline Int32U5BU5D_t3030399641* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t3030399641** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t3030399641* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t244646649, ___linkSlots_5)); }
	inline LinkU5BU5D_t62501539* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t62501539** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t62501539* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t244646649, ___keySlots_6)); }
	inline Int32U5BU5D_t3030399641* get_keySlots_6() const { return ___keySlots_6; }
	inline Int32U5BU5D_t3030399641** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(Int32U5BU5D_t3030399641* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t244646649, ___valueSlots_7)); }
	inline TuioObjectU5BU5D_t3851389651* get_valueSlots_7() const { return ___valueSlots_7; }
	inline TuioObjectU5BU5D_t3851389651** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(TuioObjectU5BU5D_t3851389651* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t244646649, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t244646649, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t244646649, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t244646649, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t244646649, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t244646649, ___serialization_info_13)); }
	inline SerializationInfo_t228987430 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t228987430 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t228987430 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t244646649, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t244646649_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t240819670 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t244646649_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t240819670 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t240819670 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t240819670 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T244646649_H
#ifndef LIST_1_T605942146_H
#define LIST_1_T605942146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<TUIOsharp.Entities.TuioObject>
struct  List_1_t605942146  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	TuioObjectU5BU5D_t3851389651* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t605942146, ____items_1)); }
	inline TuioObjectU5BU5D_t3851389651* get__items_1() const { return ____items_1; }
	inline TuioObjectU5BU5D_t3851389651** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(TuioObjectU5BU5D_t3851389651* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t605942146, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t605942146, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t605942146_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	TuioObjectU5BU5D_t3851389651* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t605942146_StaticFields, ___EmptyArray_4)); }
	inline TuioObjectU5BU5D_t3851389651* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline TuioObjectU5BU5D_t3851389651** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(TuioObjectU5BU5D_t3851389651* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T605942146_H
#ifndef OBJECTPROCESSOR_T221569383_H
#define OBJECTPROCESSOR_T221569383_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TUIOsharp.DataProcessors.ObjectProcessor
struct  ObjectProcessor_t221569383  : public RuntimeObject
{
public:
	// System.EventHandler`1<TUIOsharp.DataProcessors.TuioObjectEventArgs> TUIOsharp.DataProcessors.ObjectProcessor::ObjectAdded
	EventHandler_1_t402050513 * ___ObjectAdded_0;
	// System.EventHandler`1<TUIOsharp.DataProcessors.TuioObjectEventArgs> TUIOsharp.DataProcessors.ObjectProcessor::ObjectUpdated
	EventHandler_1_t402050513 * ___ObjectUpdated_1;
	// System.EventHandler`1<TUIOsharp.DataProcessors.TuioObjectEventArgs> TUIOsharp.DataProcessors.ObjectProcessor::ObjectRemoved
	EventHandler_1_t402050513 * ___ObjectRemoved_2;
	// System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioObject> TUIOsharp.DataProcessors.ObjectProcessor::objects
	Dictionary_2_t244646649 * ___objects_3;
	// System.Collections.Generic.List`1<TUIOsharp.Entities.TuioObject> TUIOsharp.DataProcessors.ObjectProcessor::updatedObjects
	List_1_t605942146 * ___updatedObjects_4;
	// System.Collections.Generic.List`1<System.Int32> TUIOsharp.DataProcessors.ObjectProcessor::addedObjects
	List_1_t1440998580 * ___addedObjects_5;
	// System.Collections.Generic.List`1<System.Int32> TUIOsharp.DataProcessors.ObjectProcessor::removedObjects
	List_1_t1440998580 * ___removedObjects_6;
	// System.Int32 TUIOsharp.DataProcessors.ObjectProcessor::<FrameNumber>k__BackingField
	int32_t ___U3CFrameNumberU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_ObjectAdded_0() { return static_cast<int32_t>(offsetof(ObjectProcessor_t221569383, ___ObjectAdded_0)); }
	inline EventHandler_1_t402050513 * get_ObjectAdded_0() const { return ___ObjectAdded_0; }
	inline EventHandler_1_t402050513 ** get_address_of_ObjectAdded_0() { return &___ObjectAdded_0; }
	inline void set_ObjectAdded_0(EventHandler_1_t402050513 * value)
	{
		___ObjectAdded_0 = value;
		Il2CppCodeGenWriteBarrier((&___ObjectAdded_0), value);
	}

	inline static int32_t get_offset_of_ObjectUpdated_1() { return static_cast<int32_t>(offsetof(ObjectProcessor_t221569383, ___ObjectUpdated_1)); }
	inline EventHandler_1_t402050513 * get_ObjectUpdated_1() const { return ___ObjectUpdated_1; }
	inline EventHandler_1_t402050513 ** get_address_of_ObjectUpdated_1() { return &___ObjectUpdated_1; }
	inline void set_ObjectUpdated_1(EventHandler_1_t402050513 * value)
	{
		___ObjectUpdated_1 = value;
		Il2CppCodeGenWriteBarrier((&___ObjectUpdated_1), value);
	}

	inline static int32_t get_offset_of_ObjectRemoved_2() { return static_cast<int32_t>(offsetof(ObjectProcessor_t221569383, ___ObjectRemoved_2)); }
	inline EventHandler_1_t402050513 * get_ObjectRemoved_2() const { return ___ObjectRemoved_2; }
	inline EventHandler_1_t402050513 ** get_address_of_ObjectRemoved_2() { return &___ObjectRemoved_2; }
	inline void set_ObjectRemoved_2(EventHandler_1_t402050513 * value)
	{
		___ObjectRemoved_2 = value;
		Il2CppCodeGenWriteBarrier((&___ObjectRemoved_2), value);
	}

	inline static int32_t get_offset_of_objects_3() { return static_cast<int32_t>(offsetof(ObjectProcessor_t221569383, ___objects_3)); }
	inline Dictionary_2_t244646649 * get_objects_3() const { return ___objects_3; }
	inline Dictionary_2_t244646649 ** get_address_of_objects_3() { return &___objects_3; }
	inline void set_objects_3(Dictionary_2_t244646649 * value)
	{
		___objects_3 = value;
		Il2CppCodeGenWriteBarrier((&___objects_3), value);
	}

	inline static int32_t get_offset_of_updatedObjects_4() { return static_cast<int32_t>(offsetof(ObjectProcessor_t221569383, ___updatedObjects_4)); }
	inline List_1_t605942146 * get_updatedObjects_4() const { return ___updatedObjects_4; }
	inline List_1_t605942146 ** get_address_of_updatedObjects_4() { return &___updatedObjects_4; }
	inline void set_updatedObjects_4(List_1_t605942146 * value)
	{
		___updatedObjects_4 = value;
		Il2CppCodeGenWriteBarrier((&___updatedObjects_4), value);
	}

	inline static int32_t get_offset_of_addedObjects_5() { return static_cast<int32_t>(offsetof(ObjectProcessor_t221569383, ___addedObjects_5)); }
	inline List_1_t1440998580 * get_addedObjects_5() const { return ___addedObjects_5; }
	inline List_1_t1440998580 ** get_address_of_addedObjects_5() { return &___addedObjects_5; }
	inline void set_addedObjects_5(List_1_t1440998580 * value)
	{
		___addedObjects_5 = value;
		Il2CppCodeGenWriteBarrier((&___addedObjects_5), value);
	}

	inline static int32_t get_offset_of_removedObjects_6() { return static_cast<int32_t>(offsetof(ObjectProcessor_t221569383, ___removedObjects_6)); }
	inline List_1_t1440998580 * get_removedObjects_6() const { return ___removedObjects_6; }
	inline List_1_t1440998580 ** get_address_of_removedObjects_6() { return &___removedObjects_6; }
	inline void set_removedObjects_6(List_1_t1440998580 * value)
	{
		___removedObjects_6 = value;
		Il2CppCodeGenWriteBarrier((&___removedObjects_6), value);
	}

	inline static int32_t get_offset_of_U3CFrameNumberU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(ObjectProcessor_t221569383, ___U3CFrameNumberU3Ek__BackingField_7)); }
	inline int32_t get_U3CFrameNumberU3Ek__BackingField_7() const { return ___U3CFrameNumberU3Ek__BackingField_7; }
	inline int32_t* get_address_of_U3CFrameNumberU3Ek__BackingField_7() { return &___U3CFrameNumberU3Ek__BackingField_7; }
	inline void set_U3CFrameNumberU3Ek__BackingField_7(int32_t value)
	{
		___U3CFrameNumberU3Ek__BackingField_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTPROCESSOR_T221569383_H
#ifndef LIST_1_T1219472551_H
#define LIST_1_T1219472551_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>
struct  List_1_t1219472551  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	TuioCursorU5BU5D_t885656954* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t1219472551, ____items_1)); }
	inline TuioCursorU5BU5D_t885656954* get__items_1() const { return ____items_1; }
	inline TuioCursorU5BU5D_t885656954** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(TuioCursorU5BU5D_t885656954* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t1219472551, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t1219472551, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t1219472551_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	TuioCursorU5BU5D_t885656954* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t1219472551_StaticFields, ___EmptyArray_4)); }
	inline TuioCursorU5BU5D_t885656954* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline TuioCursorU5BU5D_t885656954** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(TuioCursorU5BU5D_t885656954* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T1219472551_H
#ifndef DICTIONARY_2_T858177054_H
#define DICTIONARY_2_T858177054_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioCursor>
struct  Dictionary_2_t858177054  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t3030399641* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t62501539* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	Int32U5BU5D_t3030399641* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	TuioCursorU5BU5D_t885656954* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t228987430 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t858177054, ___table_4)); }
	inline Int32U5BU5D_t3030399641* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t3030399641** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t3030399641* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t858177054, ___linkSlots_5)); }
	inline LinkU5BU5D_t62501539* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t62501539** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t62501539* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t858177054, ___keySlots_6)); }
	inline Int32U5BU5D_t3030399641* get_keySlots_6() const { return ___keySlots_6; }
	inline Int32U5BU5D_t3030399641** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(Int32U5BU5D_t3030399641* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t858177054, ___valueSlots_7)); }
	inline TuioCursorU5BU5D_t885656954* get_valueSlots_7() const { return ___valueSlots_7; }
	inline TuioCursorU5BU5D_t885656954** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(TuioCursorU5BU5D_t885656954* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t858177054, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t858177054, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t858177054, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t858177054, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t858177054, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t858177054, ___serialization_info_13)); }
	inline SerializationInfo_t228987430 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t228987430 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t228987430 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t858177054, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t858177054_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t1570054269 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t858177054_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t1570054269 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t1570054269 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t1570054269 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T858177054_H
#ifndef LIST_1_T1440998580_H
#define LIST_1_T1440998580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.Int32>
struct  List_1_t1440998580  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Int32U5BU5D_t3030399641* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t1440998580, ____items_1)); }
	inline Int32U5BU5D_t3030399641* get__items_1() const { return ____items_1; }
	inline Int32U5BU5D_t3030399641** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Int32U5BU5D_t3030399641* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t1440998580, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t1440998580, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t1440998580_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	Int32U5BU5D_t3030399641* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t1440998580_StaticFields, ___EmptyArray_4)); }
	inline Int32U5BU5D_t3030399641* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline Int32U5BU5D_t3030399641** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(Int32U5BU5D_t3030399641* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T1440998580_H
#ifndef TUIOENTITY_T295276718_H
#define TUIOENTITY_T295276718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TUIOsharp.Entities.TuioEntity
struct  TuioEntity_t295276718  : public RuntimeObject
{
public:
	// System.Int32 TUIOsharp.Entities.TuioEntity::<Id>k__BackingField
	int32_t ___U3CIdU3Ek__BackingField_0;
	// System.Single TUIOsharp.Entities.TuioEntity::<X>k__BackingField
	float ___U3CXU3Ek__BackingField_1;
	// System.Single TUIOsharp.Entities.TuioEntity::<Y>k__BackingField
	float ___U3CYU3Ek__BackingField_2;
	// System.Single TUIOsharp.Entities.TuioEntity::<VelocityX>k__BackingField
	float ___U3CVelocityXU3Ek__BackingField_3;
	// System.Single TUIOsharp.Entities.TuioEntity::<VelocityY>k__BackingField
	float ___U3CVelocityYU3Ek__BackingField_4;
	// System.Single TUIOsharp.Entities.TuioEntity::<Acceleration>k__BackingField
	float ___U3CAccelerationU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CIdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TuioEntity_t295276718, ___U3CIdU3Ek__BackingField_0)); }
	inline int32_t get_U3CIdU3Ek__BackingField_0() const { return ___U3CIdU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CIdU3Ek__BackingField_0() { return &___U3CIdU3Ek__BackingField_0; }
	inline void set_U3CIdU3Ek__BackingField_0(int32_t value)
	{
		___U3CIdU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CXU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TuioEntity_t295276718, ___U3CXU3Ek__BackingField_1)); }
	inline float get_U3CXU3Ek__BackingField_1() const { return ___U3CXU3Ek__BackingField_1; }
	inline float* get_address_of_U3CXU3Ek__BackingField_1() { return &___U3CXU3Ek__BackingField_1; }
	inline void set_U3CXU3Ek__BackingField_1(float value)
	{
		___U3CXU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CYU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(TuioEntity_t295276718, ___U3CYU3Ek__BackingField_2)); }
	inline float get_U3CYU3Ek__BackingField_2() const { return ___U3CYU3Ek__BackingField_2; }
	inline float* get_address_of_U3CYU3Ek__BackingField_2() { return &___U3CYU3Ek__BackingField_2; }
	inline void set_U3CYU3Ek__BackingField_2(float value)
	{
		___U3CYU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CVelocityXU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(TuioEntity_t295276718, ___U3CVelocityXU3Ek__BackingField_3)); }
	inline float get_U3CVelocityXU3Ek__BackingField_3() const { return ___U3CVelocityXU3Ek__BackingField_3; }
	inline float* get_address_of_U3CVelocityXU3Ek__BackingField_3() { return &___U3CVelocityXU3Ek__BackingField_3; }
	inline void set_U3CVelocityXU3Ek__BackingField_3(float value)
	{
		___U3CVelocityXU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CVelocityYU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(TuioEntity_t295276718, ___U3CVelocityYU3Ek__BackingField_4)); }
	inline float get_U3CVelocityYU3Ek__BackingField_4() const { return ___U3CVelocityYU3Ek__BackingField_4; }
	inline float* get_address_of_U3CVelocityYU3Ek__BackingField_4() { return &___U3CVelocityYU3Ek__BackingField_4; }
	inline void set_U3CVelocityYU3Ek__BackingField_4(float value)
	{
		___U3CVelocityYU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CAccelerationU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(TuioEntity_t295276718, ___U3CAccelerationU3Ek__BackingField_5)); }
	inline float get_U3CAccelerationU3Ek__BackingField_5() const { return ___U3CAccelerationU3Ek__BackingField_5; }
	inline float* get_address_of_U3CAccelerationU3Ek__BackingField_5() { return &___U3CAccelerationU3Ek__BackingField_5; }
	inline void set_U3CAccelerationU3Ek__BackingField_5(float value)
	{
		___U3CAccelerationU3Ek__BackingField_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUIOENTITY_T295276718_H
#ifndef CURSORPROCESSOR_T1785954004_H
#define CURSORPROCESSOR_T1785954004_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TUIOsharp.DataProcessors.CursorProcessor
struct  CursorProcessor_t1785954004  : public RuntimeObject
{
public:
	// System.EventHandler`1<TUIOsharp.DataProcessors.TuioCursorEventArgs> TUIOsharp.DataProcessors.CursorProcessor::CursorAdded
	EventHandler_1_t3249264480 * ___CursorAdded_0;
	// System.EventHandler`1<TUIOsharp.DataProcessors.TuioCursorEventArgs> TUIOsharp.DataProcessors.CursorProcessor::CursorUpdated
	EventHandler_1_t3249264480 * ___CursorUpdated_1;
	// System.EventHandler`1<TUIOsharp.DataProcessors.TuioCursorEventArgs> TUIOsharp.DataProcessors.CursorProcessor::CursorRemoved
	EventHandler_1_t3249264480 * ___CursorRemoved_2;
	// System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioCursor> TUIOsharp.DataProcessors.CursorProcessor::cursors
	Dictionary_2_t858177054 * ___cursors_3;
	// System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor> TUIOsharp.DataProcessors.CursorProcessor::updatedCursors
	List_1_t1219472551 * ___updatedCursors_4;
	// System.Collections.Generic.List`1<System.Int32> TUIOsharp.DataProcessors.CursorProcessor::addedCursors
	List_1_t1440998580 * ___addedCursors_5;
	// System.Collections.Generic.List`1<System.Int32> TUIOsharp.DataProcessors.CursorProcessor::removedCursors
	List_1_t1440998580 * ___removedCursors_6;
	// System.Int32 TUIOsharp.DataProcessors.CursorProcessor::<FrameNumber>k__BackingField
	int32_t ___U3CFrameNumberU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_CursorAdded_0() { return static_cast<int32_t>(offsetof(CursorProcessor_t1785954004, ___CursorAdded_0)); }
	inline EventHandler_1_t3249264480 * get_CursorAdded_0() const { return ___CursorAdded_0; }
	inline EventHandler_1_t3249264480 ** get_address_of_CursorAdded_0() { return &___CursorAdded_0; }
	inline void set_CursorAdded_0(EventHandler_1_t3249264480 * value)
	{
		___CursorAdded_0 = value;
		Il2CppCodeGenWriteBarrier((&___CursorAdded_0), value);
	}

	inline static int32_t get_offset_of_CursorUpdated_1() { return static_cast<int32_t>(offsetof(CursorProcessor_t1785954004, ___CursorUpdated_1)); }
	inline EventHandler_1_t3249264480 * get_CursorUpdated_1() const { return ___CursorUpdated_1; }
	inline EventHandler_1_t3249264480 ** get_address_of_CursorUpdated_1() { return &___CursorUpdated_1; }
	inline void set_CursorUpdated_1(EventHandler_1_t3249264480 * value)
	{
		___CursorUpdated_1 = value;
		Il2CppCodeGenWriteBarrier((&___CursorUpdated_1), value);
	}

	inline static int32_t get_offset_of_CursorRemoved_2() { return static_cast<int32_t>(offsetof(CursorProcessor_t1785954004, ___CursorRemoved_2)); }
	inline EventHandler_1_t3249264480 * get_CursorRemoved_2() const { return ___CursorRemoved_2; }
	inline EventHandler_1_t3249264480 ** get_address_of_CursorRemoved_2() { return &___CursorRemoved_2; }
	inline void set_CursorRemoved_2(EventHandler_1_t3249264480 * value)
	{
		___CursorRemoved_2 = value;
		Il2CppCodeGenWriteBarrier((&___CursorRemoved_2), value);
	}

	inline static int32_t get_offset_of_cursors_3() { return static_cast<int32_t>(offsetof(CursorProcessor_t1785954004, ___cursors_3)); }
	inline Dictionary_2_t858177054 * get_cursors_3() const { return ___cursors_3; }
	inline Dictionary_2_t858177054 ** get_address_of_cursors_3() { return &___cursors_3; }
	inline void set_cursors_3(Dictionary_2_t858177054 * value)
	{
		___cursors_3 = value;
		Il2CppCodeGenWriteBarrier((&___cursors_3), value);
	}

	inline static int32_t get_offset_of_updatedCursors_4() { return static_cast<int32_t>(offsetof(CursorProcessor_t1785954004, ___updatedCursors_4)); }
	inline List_1_t1219472551 * get_updatedCursors_4() const { return ___updatedCursors_4; }
	inline List_1_t1219472551 ** get_address_of_updatedCursors_4() { return &___updatedCursors_4; }
	inline void set_updatedCursors_4(List_1_t1219472551 * value)
	{
		___updatedCursors_4 = value;
		Il2CppCodeGenWriteBarrier((&___updatedCursors_4), value);
	}

	inline static int32_t get_offset_of_addedCursors_5() { return static_cast<int32_t>(offsetof(CursorProcessor_t1785954004, ___addedCursors_5)); }
	inline List_1_t1440998580 * get_addedCursors_5() const { return ___addedCursors_5; }
	inline List_1_t1440998580 ** get_address_of_addedCursors_5() { return &___addedCursors_5; }
	inline void set_addedCursors_5(List_1_t1440998580 * value)
	{
		___addedCursors_5 = value;
		Il2CppCodeGenWriteBarrier((&___addedCursors_5), value);
	}

	inline static int32_t get_offset_of_removedCursors_6() { return static_cast<int32_t>(offsetof(CursorProcessor_t1785954004, ___removedCursors_6)); }
	inline List_1_t1440998580 * get_removedCursors_6() const { return ___removedCursors_6; }
	inline List_1_t1440998580 ** get_address_of_removedCursors_6() { return &___removedCursors_6; }
	inline void set_removedCursors_6(List_1_t1440998580 * value)
	{
		___removedCursors_6 = value;
		Il2CppCodeGenWriteBarrier((&___removedCursors_6), value);
	}

	inline static int32_t get_offset_of_U3CFrameNumberU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(CursorProcessor_t1785954004, ___U3CFrameNumberU3Ek__BackingField_7)); }
	inline int32_t get_U3CFrameNumberU3Ek__BackingField_7() const { return ___U3CFrameNumberU3Ek__BackingField_7; }
	inline int32_t* get_address_of_U3CFrameNumberU3Ek__BackingField_7() { return &___U3CFrameNumberU3Ek__BackingField_7; }
	inline void set_U3CFrameNumberU3Ek__BackingField_7(int32_t value)
	{
		___U3CFrameNumberU3Ek__BackingField_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURSORPROCESSOR_T1785954004_H
#ifndef LIST_1_T1583176531_H
#define LIST_1_T1583176531_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>
struct  List_1_t1583176531  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	IDataProcessorU5BU5D_t215962270* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t1583176531, ____items_1)); }
	inline IDataProcessorU5BU5D_t215962270* get__items_1() const { return ____items_1; }
	inline IDataProcessorU5BU5D_t215962270** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(IDataProcessorU5BU5D_t215962270* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t1583176531, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t1583176531, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t1583176531_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	IDataProcessorU5BU5D_t215962270* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t1583176531_StaticFields, ___EmptyArray_4)); }
	inline IDataProcessorU5BU5D_t215962270* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline IDataProcessorU5BU5D_t215962270** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(IDataProcessorU5BU5D_t215962270* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T1583176531_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t1328083999* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t1328083999* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t1328083999** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t1328083999* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef BLOBPROCESSOR_T3603341577_H
#define BLOBPROCESSOR_T3603341577_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TUIOsharp.DataProcessors.BlobProcessor
struct  BlobProcessor_t3603341577  : public RuntimeObject
{
public:
	// System.EventHandler`1<TUIOsharp.DataProcessors.TuioBlobEventArgs> TUIOsharp.DataProcessors.BlobProcessor::BlobAdded
	EventHandler_1_t2154285351 * ___BlobAdded_0;
	// System.EventHandler`1<TUIOsharp.DataProcessors.TuioBlobEventArgs> TUIOsharp.DataProcessors.BlobProcessor::BlobUpdated
	EventHandler_1_t2154285351 * ___BlobUpdated_1;
	// System.EventHandler`1<TUIOsharp.DataProcessors.TuioBlobEventArgs> TUIOsharp.DataProcessors.BlobProcessor::BlobRemoved
	EventHandler_1_t2154285351 * ___BlobRemoved_2;
	// System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioBlob> TUIOsharp.DataProcessors.BlobProcessor::blobs
	Dictionary_2_t1054769049 * ___blobs_3;
	// System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob> TUIOsharp.DataProcessors.BlobProcessor::updatedBlobs
	List_1_t1416064546 * ___updatedBlobs_4;
	// System.Collections.Generic.List`1<System.Int32> TUIOsharp.DataProcessors.BlobProcessor::addedBlobs
	List_1_t1440998580 * ___addedBlobs_5;
	// System.Collections.Generic.List`1<System.Int32> TUIOsharp.DataProcessors.BlobProcessor::removedBlobs
	List_1_t1440998580 * ___removedBlobs_6;
	// System.Int32 TUIOsharp.DataProcessors.BlobProcessor::<FrameNumber>k__BackingField
	int32_t ___U3CFrameNumberU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_BlobAdded_0() { return static_cast<int32_t>(offsetof(BlobProcessor_t3603341577, ___BlobAdded_0)); }
	inline EventHandler_1_t2154285351 * get_BlobAdded_0() const { return ___BlobAdded_0; }
	inline EventHandler_1_t2154285351 ** get_address_of_BlobAdded_0() { return &___BlobAdded_0; }
	inline void set_BlobAdded_0(EventHandler_1_t2154285351 * value)
	{
		___BlobAdded_0 = value;
		Il2CppCodeGenWriteBarrier((&___BlobAdded_0), value);
	}

	inline static int32_t get_offset_of_BlobUpdated_1() { return static_cast<int32_t>(offsetof(BlobProcessor_t3603341577, ___BlobUpdated_1)); }
	inline EventHandler_1_t2154285351 * get_BlobUpdated_1() const { return ___BlobUpdated_1; }
	inline EventHandler_1_t2154285351 ** get_address_of_BlobUpdated_1() { return &___BlobUpdated_1; }
	inline void set_BlobUpdated_1(EventHandler_1_t2154285351 * value)
	{
		___BlobUpdated_1 = value;
		Il2CppCodeGenWriteBarrier((&___BlobUpdated_1), value);
	}

	inline static int32_t get_offset_of_BlobRemoved_2() { return static_cast<int32_t>(offsetof(BlobProcessor_t3603341577, ___BlobRemoved_2)); }
	inline EventHandler_1_t2154285351 * get_BlobRemoved_2() const { return ___BlobRemoved_2; }
	inline EventHandler_1_t2154285351 ** get_address_of_BlobRemoved_2() { return &___BlobRemoved_2; }
	inline void set_BlobRemoved_2(EventHandler_1_t2154285351 * value)
	{
		___BlobRemoved_2 = value;
		Il2CppCodeGenWriteBarrier((&___BlobRemoved_2), value);
	}

	inline static int32_t get_offset_of_blobs_3() { return static_cast<int32_t>(offsetof(BlobProcessor_t3603341577, ___blobs_3)); }
	inline Dictionary_2_t1054769049 * get_blobs_3() const { return ___blobs_3; }
	inline Dictionary_2_t1054769049 ** get_address_of_blobs_3() { return &___blobs_3; }
	inline void set_blobs_3(Dictionary_2_t1054769049 * value)
	{
		___blobs_3 = value;
		Il2CppCodeGenWriteBarrier((&___blobs_3), value);
	}

	inline static int32_t get_offset_of_updatedBlobs_4() { return static_cast<int32_t>(offsetof(BlobProcessor_t3603341577, ___updatedBlobs_4)); }
	inline List_1_t1416064546 * get_updatedBlobs_4() const { return ___updatedBlobs_4; }
	inline List_1_t1416064546 ** get_address_of_updatedBlobs_4() { return &___updatedBlobs_4; }
	inline void set_updatedBlobs_4(List_1_t1416064546 * value)
	{
		___updatedBlobs_4 = value;
		Il2CppCodeGenWriteBarrier((&___updatedBlobs_4), value);
	}

	inline static int32_t get_offset_of_addedBlobs_5() { return static_cast<int32_t>(offsetof(BlobProcessor_t3603341577, ___addedBlobs_5)); }
	inline List_1_t1440998580 * get_addedBlobs_5() const { return ___addedBlobs_5; }
	inline List_1_t1440998580 ** get_address_of_addedBlobs_5() { return &___addedBlobs_5; }
	inline void set_addedBlobs_5(List_1_t1440998580 * value)
	{
		___addedBlobs_5 = value;
		Il2CppCodeGenWriteBarrier((&___addedBlobs_5), value);
	}

	inline static int32_t get_offset_of_removedBlobs_6() { return static_cast<int32_t>(offsetof(BlobProcessor_t3603341577, ___removedBlobs_6)); }
	inline List_1_t1440998580 * get_removedBlobs_6() const { return ___removedBlobs_6; }
	inline List_1_t1440998580 ** get_address_of_removedBlobs_6() { return &___removedBlobs_6; }
	inline void set_removedBlobs_6(List_1_t1440998580 * value)
	{
		___removedBlobs_6 = value;
		Il2CppCodeGenWriteBarrier((&___removedBlobs_6), value);
	}

	inline static int32_t get_offset_of_U3CFrameNumberU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(BlobProcessor_t3603341577, ___U3CFrameNumberU3Ek__BackingField_7)); }
	inline int32_t get_U3CFrameNumberU3Ek__BackingField_7() const { return ___U3CFrameNumberU3Ek__BackingField_7; }
	inline int32_t* get_address_of_U3CFrameNumberU3Ek__BackingField_7() { return &___U3CFrameNumberU3Ek__BackingField_7; }
	inline void set_U3CFrameNumberU3Ek__BackingField_7(int32_t value)
	{
		___U3CFrameNumberU3Ek__BackingField_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOBPROCESSOR_T3603341577_H
#ifndef EVENTARGS_T3289624707_H
#define EVENTARGS_T3289624707_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventArgs
struct  EventArgs_t3289624707  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t3289624707_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t3289624707 * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t3289624707_StaticFields, ___Empty_0)); }
	inline EventArgs_t3289624707 * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t3289624707 ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t3289624707 * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTARGS_T3289624707_H
#ifndef TUIOSERVER_T595520884_H
#define TUIOSERVER_T595520884_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TUIOsharp.TuioServer
struct  TuioServer_t595520884  : public RuntimeObject
{
public:
	// System.EventHandler`1<OSCsharp.Utils.ExceptionEventArgs> TUIOsharp.TuioServer::ErrorOccured
	EventHandler_1_t3778988004 * ___ErrorOccured_0;
	// OSCsharp.Net.UDPReceiver TUIOsharp.TuioServer::udpReceiver
	UDPReceiver_t3846109956 * ___udpReceiver_1;
	// System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor> TUIOsharp.TuioServer::processors
	List_1_t1583176531 * ___processors_2;
	// System.Int32 TUIOsharp.TuioServer::<Port>k__BackingField
	int32_t ___U3CPortU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_ErrorOccured_0() { return static_cast<int32_t>(offsetof(TuioServer_t595520884, ___ErrorOccured_0)); }
	inline EventHandler_1_t3778988004 * get_ErrorOccured_0() const { return ___ErrorOccured_0; }
	inline EventHandler_1_t3778988004 ** get_address_of_ErrorOccured_0() { return &___ErrorOccured_0; }
	inline void set_ErrorOccured_0(EventHandler_1_t3778988004 * value)
	{
		___ErrorOccured_0 = value;
		Il2CppCodeGenWriteBarrier((&___ErrorOccured_0), value);
	}

	inline static int32_t get_offset_of_udpReceiver_1() { return static_cast<int32_t>(offsetof(TuioServer_t595520884, ___udpReceiver_1)); }
	inline UDPReceiver_t3846109956 * get_udpReceiver_1() const { return ___udpReceiver_1; }
	inline UDPReceiver_t3846109956 ** get_address_of_udpReceiver_1() { return &___udpReceiver_1; }
	inline void set_udpReceiver_1(UDPReceiver_t3846109956 * value)
	{
		___udpReceiver_1 = value;
		Il2CppCodeGenWriteBarrier((&___udpReceiver_1), value);
	}

	inline static int32_t get_offset_of_processors_2() { return static_cast<int32_t>(offsetof(TuioServer_t595520884, ___processors_2)); }
	inline List_1_t1583176531 * get_processors_2() const { return ___processors_2; }
	inline List_1_t1583176531 ** get_address_of_processors_2() { return &___processors_2; }
	inline void set_processors_2(List_1_t1583176531 * value)
	{
		___processors_2 = value;
		Il2CppCodeGenWriteBarrier((&___processors_2), value);
	}

	inline static int32_t get_offset_of_U3CPortU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(TuioServer_t595520884, ___U3CPortU3Ek__BackingField_3)); }
	inline int32_t get_U3CPortU3Ek__BackingField_3() const { return ___U3CPortU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CPortU3Ek__BackingField_3() { return &___U3CPortU3Ek__BackingField_3; }
	inline void set_U3CPortU3Ek__BackingField_3(int32_t value)
	{
		___U3CPortU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUIOSERVER_T595520884_H
#ifndef TUIOOBJECTEVENTARGS_T1810743341_H
#define TUIOOBJECTEVENTARGS_T1810743341_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TUIOsharp.DataProcessors.TuioObjectEventArgs
struct  TuioObjectEventArgs_t1810743341  : public EventArgs_t3289624707
{
public:
	// TUIOsharp.Entities.TuioObject TUIOsharp.DataProcessors.TuioObjectEventArgs::Object
	TuioObject_t1236821014 * ___Object_1;

public:
	inline static int32_t get_offset_of_Object_1() { return static_cast<int32_t>(offsetof(TuioObjectEventArgs_t1810743341, ___Object_1)); }
	inline TuioObject_t1236821014 * get_Object_1() const { return ___Object_1; }
	inline TuioObject_t1236821014 ** get_address_of_Object_1() { return &___Object_1; }
	inline void set_Object_1(TuioObject_t1236821014 * value)
	{
		___Object_1 = value;
		Il2CppCodeGenWriteBarrier((&___Object_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUIOOBJECTEVENTARGS_T1810743341_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t1328083999* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t1328083999* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t1328083999** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t1328083999* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef TUIOCURSOREVENTARGS_T362990012_H
#define TUIOCURSOREVENTARGS_T362990012_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TUIOsharp.DataProcessors.TuioCursorEventArgs
struct  TuioCursorEventArgs_t362990012  : public EventArgs_t3289624707
{
public:
	// TUIOsharp.Entities.TuioCursor TUIOsharp.DataProcessors.TuioCursorEventArgs::Cursor
	TuioCursor_t1850351419 * ___Cursor_1;

public:
	inline static int32_t get_offset_of_Cursor_1() { return static_cast<int32_t>(offsetof(TuioCursorEventArgs_t362990012, ___Cursor_1)); }
	inline TuioCursor_t1850351419 * get_Cursor_1() const { return ___Cursor_1; }
	inline TuioCursor_t1850351419 ** get_address_of_Cursor_1() { return &___Cursor_1; }
	inline void set_Cursor_1(TuioCursor_t1850351419 * value)
	{
		___Cursor_1 = value;
		Il2CppCodeGenWriteBarrier((&___Cursor_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUIOCURSOREVENTARGS_T362990012_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	IntPtr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline IntPtr_t get_Zero_1() const { return ___Zero_1; }
	inline IntPtr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(IntPtr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef TUIOOBJECT_T1236821014_H
#define TUIOOBJECT_T1236821014_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TUIOsharp.Entities.TuioObject
struct  TuioObject_t1236821014  : public TuioEntity_t295276718
{
public:
	// System.Int32 TUIOsharp.Entities.TuioObject::<ClassId>k__BackingField
	int32_t ___U3CClassIdU3Ek__BackingField_6;
	// System.Single TUIOsharp.Entities.TuioObject::<Angle>k__BackingField
	float ___U3CAngleU3Ek__BackingField_7;
	// System.Single TUIOsharp.Entities.TuioObject::<RotationVelocity>k__BackingField
	float ___U3CRotationVelocityU3Ek__BackingField_8;
	// System.Single TUIOsharp.Entities.TuioObject::<RotationAcceleration>k__BackingField
	float ___U3CRotationAccelerationU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of_U3CClassIdU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(TuioObject_t1236821014, ___U3CClassIdU3Ek__BackingField_6)); }
	inline int32_t get_U3CClassIdU3Ek__BackingField_6() const { return ___U3CClassIdU3Ek__BackingField_6; }
	inline int32_t* get_address_of_U3CClassIdU3Ek__BackingField_6() { return &___U3CClassIdU3Ek__BackingField_6; }
	inline void set_U3CClassIdU3Ek__BackingField_6(int32_t value)
	{
		___U3CClassIdU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CAngleU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(TuioObject_t1236821014, ___U3CAngleU3Ek__BackingField_7)); }
	inline float get_U3CAngleU3Ek__BackingField_7() const { return ___U3CAngleU3Ek__BackingField_7; }
	inline float* get_address_of_U3CAngleU3Ek__BackingField_7() { return &___U3CAngleU3Ek__BackingField_7; }
	inline void set_U3CAngleU3Ek__BackingField_7(float value)
	{
		___U3CAngleU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CRotationVelocityU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(TuioObject_t1236821014, ___U3CRotationVelocityU3Ek__BackingField_8)); }
	inline float get_U3CRotationVelocityU3Ek__BackingField_8() const { return ___U3CRotationVelocityU3Ek__BackingField_8; }
	inline float* get_address_of_U3CRotationVelocityU3Ek__BackingField_8() { return &___U3CRotationVelocityU3Ek__BackingField_8; }
	inline void set_U3CRotationVelocityU3Ek__BackingField_8(float value)
	{
		___U3CRotationVelocityU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CRotationAccelerationU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(TuioObject_t1236821014, ___U3CRotationAccelerationU3Ek__BackingField_9)); }
	inline float get_U3CRotationAccelerationU3Ek__BackingField_9() const { return ___U3CRotationAccelerationU3Ek__BackingField_9; }
	inline float* get_address_of_U3CRotationAccelerationU3Ek__BackingField_9() { return &___U3CRotationAccelerationU3Ek__BackingField_9; }
	inline void set_U3CRotationAccelerationU3Ek__BackingField_9(float value)
	{
		___U3CRotationAccelerationU3Ek__BackingField_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUIOOBJECT_T1236821014_H
#ifndef KEYVALUEPAIR_2_T2296959167_H
#define KEYVALUEPAIR_2_T2296959167_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Int32,TUIOsharp.Entities.TuioObject>
struct  KeyValuePair_2_t2296959167 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	int32_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	TuioObject_t1236821014 * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2296959167, ___key_0)); }
	inline int32_t get_key_0() const { return ___key_0; }
	inline int32_t* get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(int32_t value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2296959167, ___value_1)); }
	inline TuioObject_t1236821014 * get_value_1() const { return ___value_1; }
	inline TuioObject_t1236821014 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(TuioObject_t1236821014 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T2296959167_H
#ifndef EXCEPTIONEVENTARGS_T892713536_H
#define EXCEPTIONEVENTARGS_T892713536_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OSCsharp.Utils.ExceptionEventArgs
struct  ExceptionEventArgs_t892713536  : public EventArgs_t3289624707
{
public:
	// System.Exception OSCsharp.Utils.ExceptionEventArgs::<Exception>k__BackingField
	Exception_t1927440687 * ___U3CExceptionU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CExceptionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ExceptionEventArgs_t892713536, ___U3CExceptionU3Ek__BackingField_1)); }
	inline Exception_t1927440687 * get_U3CExceptionU3Ek__BackingField_1() const { return ___U3CExceptionU3Ek__BackingField_1; }
	inline Exception_t1927440687 ** get_address_of_U3CExceptionU3Ek__BackingField_1() { return &___U3CExceptionU3Ek__BackingField_1; }
	inline void set_U3CExceptionU3Ek__BackingField_1(Exception_t1927440687 * value)
	{
		___U3CExceptionU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CExceptionU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTIONEVENTARGS_T892713536_H
#ifndef OSCMESSAGERECEIVEDEVENTARGS_T1263041860_H
#define OSCMESSAGERECEIVEDEVENTARGS_T1263041860_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OSCsharp.Net.OscMessageReceivedEventArgs
struct  OscMessageReceivedEventArgs_t1263041860  : public EventArgs_t3289624707
{
public:
	// OSCsharp.Data.OscMessage OSCsharp.Net.OscMessageReceivedEventArgs::<Message>k__BackingField
	OscMessage_t2764280154 * ___U3CMessageU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CMessageU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(OscMessageReceivedEventArgs_t1263041860, ___U3CMessageU3Ek__BackingField_1)); }
	inline OscMessage_t2764280154 * get_U3CMessageU3Ek__BackingField_1() const { return ___U3CMessageU3Ek__BackingField_1; }
	inline OscMessage_t2764280154 ** get_address_of_U3CMessageU3Ek__BackingField_1() { return &___U3CMessageU3Ek__BackingField_1; }
	inline void set_U3CMessageU3Ek__BackingField_1(OscMessage_t2764280154 * value)
	{
		___U3CMessageU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMessageU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OSCMESSAGERECEIVEDEVENTARGS_T1263041860_H
#ifndef KEYVALUEPAIR_2_T3107081567_H
#define KEYVALUEPAIR_2_T3107081567_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Int32,TUIOsharp.Entities.TuioBlob>
struct  KeyValuePair_2_t3107081567 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	int32_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	TuioBlob_t2046943414 * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3107081567, ___key_0)); }
	inline int32_t get_key_0() const { return ___key_0; }
	inline int32_t* get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(int32_t value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3107081567, ___value_1)); }
	inline TuioBlob_t2046943414 * get_value_1() const { return ___value_1; }
	inline TuioBlob_t2046943414 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(TuioBlob_t2046943414 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T3107081567_H
#ifndef KEYVALUEPAIR_2_T3749587448_H
#define KEYVALUEPAIR_2_T3749587448_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>
struct  KeyValuePair_2_t3749587448 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	int32_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3749587448, ___key_0)); }
	inline int32_t get_key_0() const { return ___key_0; }
	inline int32_t* get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(int32_t value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3749587448, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T3749587448_H
#ifndef TUIOBLOBEVENTARGS_T3562978179_H
#define TUIOBLOBEVENTARGS_T3562978179_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TUIOsharp.DataProcessors.TuioBlobEventArgs
struct  TuioBlobEventArgs_t3562978179  : public EventArgs_t3289624707
{
public:
	// TUIOsharp.Entities.TuioBlob TUIOsharp.DataProcessors.TuioBlobEventArgs::Blob
	TuioBlob_t2046943414 * ___Blob_1;

public:
	inline static int32_t get_offset_of_Blob_1() { return static_cast<int32_t>(offsetof(TuioBlobEventArgs_t3562978179, ___Blob_1)); }
	inline TuioBlob_t2046943414 * get_Blob_1() const { return ___Blob_1; }
	inline TuioBlob_t2046943414 ** get_address_of_Blob_1() { return &___Blob_1; }
	inline void set_Blob_1(TuioBlob_t2046943414 * value)
	{
		___Blob_1 = value;
		Il2CppCodeGenWriteBarrier((&___Blob_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUIOBLOBEVENTARGS_T3562978179_H
#ifndef VOID_T1841601450_H
#define VOID_T1841601450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1841601450 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1841601450_H
#ifndef INT32_T2071877448_H
#define INT32_T2071877448_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2071877448 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2071877448, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2071877448_H
#ifndef OSCMESSAGE_T2764280154_H
#define OSCMESSAGE_T2764280154_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OSCsharp.Data.OscMessage
struct  OscMessage_t2764280154  : public OscPacket_t504761797
{
public:
	// System.String OSCsharp.Data.OscMessage::typeTag
	String_t* ___typeTag_19;

public:
	inline static int32_t get_offset_of_typeTag_19() { return static_cast<int32_t>(offsetof(OscMessage_t2764280154, ___typeTag_19)); }
	inline String_t* get_typeTag_19() const { return ___typeTag_19; }
	inline String_t** get_address_of_typeTag_19() { return &___typeTag_19; }
	inline void set_typeTag_19(String_t* value)
	{
		___typeTag_19 = value;
		Il2CppCodeGenWriteBarrier((&___typeTag_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OSCMESSAGE_T2764280154_H
#ifndef TUIOBLOB_T2046943414_H
#define TUIOBLOB_T2046943414_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TUIOsharp.Entities.TuioBlob
struct  TuioBlob_t2046943414  : public TuioEntity_t295276718
{
public:
	// System.Single TUIOsharp.Entities.TuioBlob::<Angle>k__BackingField
	float ___U3CAngleU3Ek__BackingField_6;
	// System.Single TUIOsharp.Entities.TuioBlob::<Width>k__BackingField
	float ___U3CWidthU3Ek__BackingField_7;
	// System.Single TUIOsharp.Entities.TuioBlob::<Height>k__BackingField
	float ___U3CHeightU3Ek__BackingField_8;
	// System.Single TUIOsharp.Entities.TuioBlob::<Area>k__BackingField
	float ___U3CAreaU3Ek__BackingField_9;
	// System.Single TUIOsharp.Entities.TuioBlob::<RotationVelocity>k__BackingField
	float ___U3CRotationVelocityU3Ek__BackingField_10;
	// System.Single TUIOsharp.Entities.TuioBlob::<RotationAcceleration>k__BackingField
	float ___U3CRotationAccelerationU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_U3CAngleU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(TuioBlob_t2046943414, ___U3CAngleU3Ek__BackingField_6)); }
	inline float get_U3CAngleU3Ek__BackingField_6() const { return ___U3CAngleU3Ek__BackingField_6; }
	inline float* get_address_of_U3CAngleU3Ek__BackingField_6() { return &___U3CAngleU3Ek__BackingField_6; }
	inline void set_U3CAngleU3Ek__BackingField_6(float value)
	{
		___U3CAngleU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CWidthU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(TuioBlob_t2046943414, ___U3CWidthU3Ek__BackingField_7)); }
	inline float get_U3CWidthU3Ek__BackingField_7() const { return ___U3CWidthU3Ek__BackingField_7; }
	inline float* get_address_of_U3CWidthU3Ek__BackingField_7() { return &___U3CWidthU3Ek__BackingField_7; }
	inline void set_U3CWidthU3Ek__BackingField_7(float value)
	{
		___U3CWidthU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CHeightU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(TuioBlob_t2046943414, ___U3CHeightU3Ek__BackingField_8)); }
	inline float get_U3CHeightU3Ek__BackingField_8() const { return ___U3CHeightU3Ek__BackingField_8; }
	inline float* get_address_of_U3CHeightU3Ek__BackingField_8() { return &___U3CHeightU3Ek__BackingField_8; }
	inline void set_U3CHeightU3Ek__BackingField_8(float value)
	{
		___U3CHeightU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CAreaU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(TuioBlob_t2046943414, ___U3CAreaU3Ek__BackingField_9)); }
	inline float get_U3CAreaU3Ek__BackingField_9() const { return ___U3CAreaU3Ek__BackingField_9; }
	inline float* get_address_of_U3CAreaU3Ek__BackingField_9() { return &___U3CAreaU3Ek__BackingField_9; }
	inline void set_U3CAreaU3Ek__BackingField_9(float value)
	{
		___U3CAreaU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CRotationVelocityU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(TuioBlob_t2046943414, ___U3CRotationVelocityU3Ek__BackingField_10)); }
	inline float get_U3CRotationVelocityU3Ek__BackingField_10() const { return ___U3CRotationVelocityU3Ek__BackingField_10; }
	inline float* get_address_of_U3CRotationVelocityU3Ek__BackingField_10() { return &___U3CRotationVelocityU3Ek__BackingField_10; }
	inline void set_U3CRotationVelocityU3Ek__BackingField_10(float value)
	{
		___U3CRotationVelocityU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CRotationAccelerationU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(TuioBlob_t2046943414, ___U3CRotationAccelerationU3Ek__BackingField_11)); }
	inline float get_U3CRotationAccelerationU3Ek__BackingField_11() const { return ___U3CRotationAccelerationU3Ek__BackingField_11; }
	inline float* get_address_of_U3CRotationAccelerationU3Ek__BackingField_11() { return &___U3CRotationAccelerationU3Ek__BackingField_11; }
	inline void set_U3CRotationAccelerationU3Ek__BackingField_11(float value)
	{
		___U3CRotationAccelerationU3Ek__BackingField_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUIOBLOB_T2046943414_H
#ifndef SINGLE_T2076509932_H
#define SINGLE_T2076509932_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t2076509932 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t2076509932, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T2076509932_H
#ifndef KEYVALUEPAIR_2_T2910489572_H
#define KEYVALUEPAIR_2_T2910489572_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Int32,TUIOsharp.Entities.TuioCursor>
struct  KeyValuePair_2_t2910489572 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	int32_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	TuioCursor_t1850351419 * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2910489572, ___key_0)); }
	inline int32_t get_key_0() const { return ___key_0; }
	inline int32_t* get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(int32_t value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2910489572, ___value_1)); }
	inline TuioCursor_t1850351419 * get_value_1() const { return ___value_1; }
	inline TuioCursor_t1850351419 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(TuioCursor_t1850351419 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T2910489572_H
#ifndef BOOLEAN_T3825574718_H
#define BOOLEAN_T3825574718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t3825574718 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t3825574718, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t3825574718_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t3825574718_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t3825574718_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T3825574718_H
#ifndef TUIOCURSOR_T1850351419_H
#define TUIOCURSOR_T1850351419_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TUIOsharp.Entities.TuioCursor
struct  TuioCursor_t1850351419  : public TuioEntity_t295276718
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUIOCURSOR_T1850351419_H
#ifndef TRANSMISSIONTYPE_T529366678_H
#define TRANSMISSIONTYPE_T529366678_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OSCsharp.Net.TransmissionType
struct  TransmissionType_t529366678 
{
public:
	// System.Int32 OSCsharp.Net.TransmissionType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TransmissionType_t529366678, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSMISSIONTYPE_T529366678_H
#ifndef ENUMERATOR_T3017299632_H
#define ENUMERATOR_T3017299632_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>
struct  Enumerator_t3017299632 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::dictionary
	Dictionary_2_t1697274930 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::stamp
	int32_t ___stamp_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::current
	KeyValuePair_2_t3749587448  ___current_3;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_t3017299632, ___dictionary_0)); }
	inline Dictionary_2_t1697274930 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t1697274930 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t1697274930 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t3017299632, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_stamp_2() { return static_cast<int32_t>(offsetof(Enumerator_t3017299632, ___stamp_2)); }
	inline int32_t get_stamp_2() const { return ___stamp_2; }
	inline int32_t* get_address_of_stamp_2() { return &___stamp_2; }
	inline void set_stamp_2(int32_t value)
	{
		___stamp_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t3017299632, ___current_3)); }
	inline KeyValuePair_2_t3749587448  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t3749587448 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t3749587448  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T3017299632_H
#ifndef DELEGATE_T3022476291_H
#define DELEGATE_T3022476291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t3022476291  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	IntPtr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	IntPtr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	IntPtr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	IntPtr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1572802995 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___invoke_impl_1)); }
	inline IntPtr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline IntPtr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(IntPtr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_3)); }
	inline IntPtr_t get_method_3() const { return ___method_3; }
	inline IntPtr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(IntPtr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___delegate_trampoline_4)); }
	inline IntPtr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline IntPtr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(IntPtr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_code_5)); }
	inline IntPtr_t get_method_code_5() const { return ___method_code_5; }
	inline IntPtr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(IntPtr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___data_8)); }
	inline DelegateData_t1572802995 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1572802995 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1572802995 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T3022476291_H
#ifndef ENUMERATOR_T2178201756_H
#define ENUMERATOR_T2178201756_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TUIOsharp.Entities.TuioCursor>
struct  Enumerator_t2178201756 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::dictionary
	Dictionary_2_t858177054 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::stamp
	int32_t ___stamp_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::current
	KeyValuePair_2_t2910489572  ___current_3;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_t2178201756, ___dictionary_0)); }
	inline Dictionary_2_t858177054 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t858177054 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t858177054 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t2178201756, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_stamp_2() { return static_cast<int32_t>(offsetof(Enumerator_t2178201756, ___stamp_2)); }
	inline int32_t get_stamp_2() const { return ___stamp_2; }
	inline int32_t* get_address_of_stamp_2() { return &___stamp_2; }
	inline void set_stamp_2(int32_t value)
	{
		___stamp_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t2178201756, ___current_3)); }
	inline KeyValuePair_2_t2910489572  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t2910489572 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t2910489572  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2178201756_H
#ifndef ENUMERATOR_T1564671351_H
#define ENUMERATOR_T1564671351_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TUIOsharp.Entities.TuioObject>
struct  Enumerator_t1564671351 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::dictionary
	Dictionary_2_t244646649 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::stamp
	int32_t ___stamp_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::current
	KeyValuePair_2_t2296959167  ___current_3;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_t1564671351, ___dictionary_0)); }
	inline Dictionary_2_t244646649 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t244646649 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t244646649 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t1564671351, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_stamp_2() { return static_cast<int32_t>(offsetof(Enumerator_t1564671351, ___stamp_2)); }
	inline int32_t get_stamp_2() const { return ___stamp_2; }
	inline int32_t* get_address_of_stamp_2() { return &___stamp_2; }
	inline void set_stamp_2(int32_t value)
	{
		___stamp_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t1564671351, ___current_3)); }
	inline KeyValuePair_2_t2296959167  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t2296959167 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t2296959167  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T1564671351_H
#ifndef ENUMERATOR_T2374793751_H
#define ENUMERATOR_T2374793751_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TUIOsharp.Entities.TuioBlob>
struct  Enumerator_t2374793751 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::dictionary
	Dictionary_2_t1054769049 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::stamp
	int32_t ___stamp_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::current
	KeyValuePair_2_t3107081567  ___current_3;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_t2374793751, ___dictionary_0)); }
	inline Dictionary_2_t1054769049 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t1054769049 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t1054769049 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t2374793751, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_stamp_2() { return static_cast<int32_t>(offsetof(Enumerator_t2374793751, ___stamp_2)); }
	inline int32_t get_stamp_2() const { return ___stamp_2; }
	inline int32_t* get_address_of_stamp_2() { return &___stamp_2; }
	inline void set_stamp_2(int32_t value)
	{
		___stamp_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t2374793751, ___current_3)); }
	inline KeyValuePair_2_t3107081567  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t3107081567 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t3107081567  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2374793751_H
#ifndef UDPRECEIVER_T3846109956_H
#define UDPRECEIVER_T3846109956_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OSCsharp.Net.UDPReceiver
struct  UDPReceiver_t3846109956  : public RuntimeObject
{
public:
	// System.EventHandler`1<OSCsharp.Net.OscPacketReceivedEventArgs> OSCsharp.Net.UDPReceiver::PacketReceived
	EventHandler_1_t1873352441 * ___PacketReceived_0;
	// System.EventHandler`1<OSCsharp.Net.OscBundleReceivedEventArgs> OSCsharp.Net.UDPReceiver::BundleReceived
	EventHandler_1_t2264531613 * ___BundleReceived_1;
	// System.EventHandler`1<OSCsharp.Utils.ExceptionEventArgs> OSCsharp.Net.UDPReceiver::ErrorOccured
	EventHandler_1_t3778988004 * ___ErrorOccured_2;
	// System.EventHandler`1<OSCsharp.Net.OscMessageReceivedEventArgs> OSCsharp.Net.UDPReceiver::messageReceivedInvoker
	EventHandler_1_t4149316328 * ___messageReceivedInvoker_3;
	// System.Net.Sockets.UdpClient OSCsharp.Net.UDPReceiver::udpClient
	UdpClient_t1278197702 * ___udpClient_4;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) OSCsharp.Net.UDPReceiver::acceptingConnections
	bool ___acceptingConnections_5;
	// System.AsyncCallback OSCsharp.Net.UDPReceiver::callback
	AsyncCallback_t163412349 * ___callback_6;
	// System.Net.IPAddress OSCsharp.Net.UDPReceiver::<IPAddress>k__BackingField
	IPAddress_t1399971723 * ___U3CIPAddressU3Ek__BackingField_7;
	// System.Int32 OSCsharp.Net.UDPReceiver::<Port>k__BackingField
	int32_t ___U3CPortU3Ek__BackingField_8;
	// System.Net.IPAddress OSCsharp.Net.UDPReceiver::<MulticastAddress>k__BackingField
	IPAddress_t1399971723 * ___U3CMulticastAddressU3Ek__BackingField_9;
	// System.Net.IPEndPoint OSCsharp.Net.UDPReceiver::<IPEndPoint>k__BackingField
	IPEndPoint_t2615413766 * ___U3CIPEndPointU3Ek__BackingField_10;
	// OSCsharp.Net.TransmissionType OSCsharp.Net.UDPReceiver::<TransmissionType>k__BackingField
	int32_t ___U3CTransmissionTypeU3Ek__BackingField_11;
	// System.Boolean OSCsharp.Net.UDPReceiver::<ConsumeParsingExceptions>k__BackingField
	bool ___U3CConsumeParsingExceptionsU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_PacketReceived_0() { return static_cast<int32_t>(offsetof(UDPReceiver_t3846109956, ___PacketReceived_0)); }
	inline EventHandler_1_t1873352441 * get_PacketReceived_0() const { return ___PacketReceived_0; }
	inline EventHandler_1_t1873352441 ** get_address_of_PacketReceived_0() { return &___PacketReceived_0; }
	inline void set_PacketReceived_0(EventHandler_1_t1873352441 * value)
	{
		___PacketReceived_0 = value;
		Il2CppCodeGenWriteBarrier((&___PacketReceived_0), value);
	}

	inline static int32_t get_offset_of_BundleReceived_1() { return static_cast<int32_t>(offsetof(UDPReceiver_t3846109956, ___BundleReceived_1)); }
	inline EventHandler_1_t2264531613 * get_BundleReceived_1() const { return ___BundleReceived_1; }
	inline EventHandler_1_t2264531613 ** get_address_of_BundleReceived_1() { return &___BundleReceived_1; }
	inline void set_BundleReceived_1(EventHandler_1_t2264531613 * value)
	{
		___BundleReceived_1 = value;
		Il2CppCodeGenWriteBarrier((&___BundleReceived_1), value);
	}

	inline static int32_t get_offset_of_ErrorOccured_2() { return static_cast<int32_t>(offsetof(UDPReceiver_t3846109956, ___ErrorOccured_2)); }
	inline EventHandler_1_t3778988004 * get_ErrorOccured_2() const { return ___ErrorOccured_2; }
	inline EventHandler_1_t3778988004 ** get_address_of_ErrorOccured_2() { return &___ErrorOccured_2; }
	inline void set_ErrorOccured_2(EventHandler_1_t3778988004 * value)
	{
		___ErrorOccured_2 = value;
		Il2CppCodeGenWriteBarrier((&___ErrorOccured_2), value);
	}

	inline static int32_t get_offset_of_messageReceivedInvoker_3() { return static_cast<int32_t>(offsetof(UDPReceiver_t3846109956, ___messageReceivedInvoker_3)); }
	inline EventHandler_1_t4149316328 * get_messageReceivedInvoker_3() const { return ___messageReceivedInvoker_3; }
	inline EventHandler_1_t4149316328 ** get_address_of_messageReceivedInvoker_3() { return &___messageReceivedInvoker_3; }
	inline void set_messageReceivedInvoker_3(EventHandler_1_t4149316328 * value)
	{
		___messageReceivedInvoker_3 = value;
		Il2CppCodeGenWriteBarrier((&___messageReceivedInvoker_3), value);
	}

	inline static int32_t get_offset_of_udpClient_4() { return static_cast<int32_t>(offsetof(UDPReceiver_t3846109956, ___udpClient_4)); }
	inline UdpClient_t1278197702 * get_udpClient_4() const { return ___udpClient_4; }
	inline UdpClient_t1278197702 ** get_address_of_udpClient_4() { return &___udpClient_4; }
	inline void set_udpClient_4(UdpClient_t1278197702 * value)
	{
		___udpClient_4 = value;
		Il2CppCodeGenWriteBarrier((&___udpClient_4), value);
	}

	inline static int32_t get_offset_of_acceptingConnections_5() { return static_cast<int32_t>(offsetof(UDPReceiver_t3846109956, ___acceptingConnections_5)); }
	inline bool get_acceptingConnections_5() const { return ___acceptingConnections_5; }
	inline bool* get_address_of_acceptingConnections_5() { return &___acceptingConnections_5; }
	inline void set_acceptingConnections_5(bool value)
	{
		___acceptingConnections_5 = value;
	}

	inline static int32_t get_offset_of_callback_6() { return static_cast<int32_t>(offsetof(UDPReceiver_t3846109956, ___callback_6)); }
	inline AsyncCallback_t163412349 * get_callback_6() const { return ___callback_6; }
	inline AsyncCallback_t163412349 ** get_address_of_callback_6() { return &___callback_6; }
	inline void set_callback_6(AsyncCallback_t163412349 * value)
	{
		___callback_6 = value;
		Il2CppCodeGenWriteBarrier((&___callback_6), value);
	}

	inline static int32_t get_offset_of_U3CIPAddressU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(UDPReceiver_t3846109956, ___U3CIPAddressU3Ek__BackingField_7)); }
	inline IPAddress_t1399971723 * get_U3CIPAddressU3Ek__BackingField_7() const { return ___U3CIPAddressU3Ek__BackingField_7; }
	inline IPAddress_t1399971723 ** get_address_of_U3CIPAddressU3Ek__BackingField_7() { return &___U3CIPAddressU3Ek__BackingField_7; }
	inline void set_U3CIPAddressU3Ek__BackingField_7(IPAddress_t1399971723 * value)
	{
		___U3CIPAddressU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CIPAddressU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CPortU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(UDPReceiver_t3846109956, ___U3CPortU3Ek__BackingField_8)); }
	inline int32_t get_U3CPortU3Ek__BackingField_8() const { return ___U3CPortU3Ek__BackingField_8; }
	inline int32_t* get_address_of_U3CPortU3Ek__BackingField_8() { return &___U3CPortU3Ek__BackingField_8; }
	inline void set_U3CPortU3Ek__BackingField_8(int32_t value)
	{
		___U3CPortU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CMulticastAddressU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(UDPReceiver_t3846109956, ___U3CMulticastAddressU3Ek__BackingField_9)); }
	inline IPAddress_t1399971723 * get_U3CMulticastAddressU3Ek__BackingField_9() const { return ___U3CMulticastAddressU3Ek__BackingField_9; }
	inline IPAddress_t1399971723 ** get_address_of_U3CMulticastAddressU3Ek__BackingField_9() { return &___U3CMulticastAddressU3Ek__BackingField_9; }
	inline void set_U3CMulticastAddressU3Ek__BackingField_9(IPAddress_t1399971723 * value)
	{
		___U3CMulticastAddressU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMulticastAddressU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CIPEndPointU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(UDPReceiver_t3846109956, ___U3CIPEndPointU3Ek__BackingField_10)); }
	inline IPEndPoint_t2615413766 * get_U3CIPEndPointU3Ek__BackingField_10() const { return ___U3CIPEndPointU3Ek__BackingField_10; }
	inline IPEndPoint_t2615413766 ** get_address_of_U3CIPEndPointU3Ek__BackingField_10() { return &___U3CIPEndPointU3Ek__BackingField_10; }
	inline void set_U3CIPEndPointU3Ek__BackingField_10(IPEndPoint_t2615413766 * value)
	{
		___U3CIPEndPointU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CIPEndPointU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_U3CTransmissionTypeU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(UDPReceiver_t3846109956, ___U3CTransmissionTypeU3Ek__BackingField_11)); }
	inline int32_t get_U3CTransmissionTypeU3Ek__BackingField_11() const { return ___U3CTransmissionTypeU3Ek__BackingField_11; }
	inline int32_t* get_address_of_U3CTransmissionTypeU3Ek__BackingField_11() { return &___U3CTransmissionTypeU3Ek__BackingField_11; }
	inline void set_U3CTransmissionTypeU3Ek__BackingField_11(int32_t value)
	{
		___U3CTransmissionTypeU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CConsumeParsingExceptionsU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(UDPReceiver_t3846109956, ___U3CConsumeParsingExceptionsU3Ek__BackingField_12)); }
	inline bool get_U3CConsumeParsingExceptionsU3Ek__BackingField_12() const { return ___U3CConsumeParsingExceptionsU3Ek__BackingField_12; }
	inline bool* get_address_of_U3CConsumeParsingExceptionsU3Ek__BackingField_12() { return &___U3CConsumeParsingExceptionsU3Ek__BackingField_12; }
	inline void set_U3CConsumeParsingExceptionsU3Ek__BackingField_12(bool value)
	{
		___U3CConsumeParsingExceptionsU3Ek__BackingField_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UDPRECEIVER_T3846109956_H
#ifndef MULTICASTDELEGATE_T3201952435_H
#define MULTICASTDELEGATE_T3201952435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t3201952435  : public Delegate_t3022476291
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t3201952435 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t3201952435 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___prev_9)); }
	inline MulticastDelegate_t3201952435 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t3201952435 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t3201952435 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___kpm_next_10)); }
	inline MulticastDelegate_t3201952435 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t3201952435 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t3201952435 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T3201952435_H
#ifndef EVENTHANDLER_1_T3249264480_H
#define EVENTHANDLER_1_T3249264480_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventHandler`1<TUIOsharp.DataProcessors.TuioCursorEventArgs>
struct  EventHandler_1_t3249264480  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTHANDLER_1_T3249264480_H
#ifndef EVENTHANDLER_1_T3778988004_H
#define EVENTHANDLER_1_T3778988004_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventHandler`1<OSCsharp.Utils.ExceptionEventArgs>
struct  EventHandler_1_t3778988004  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTHANDLER_1_T3778988004_H
#ifndef EVENTHANDLER_1_T4149316328_H
#define EVENTHANDLER_1_T4149316328_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventHandler`1<OSCsharp.Net.OscMessageReceivedEventArgs>
struct  EventHandler_1_t4149316328  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTHANDLER_1_T4149316328_H
#ifndef EVENTHANDLER_1_T402050513_H
#define EVENTHANDLER_1_T402050513_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventHandler`1<TUIOsharp.DataProcessors.TuioObjectEventArgs>
struct  EventHandler_1_t402050513  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTHANDLER_1_T402050513_H
#ifndef EVENTHANDLER_1_T2154285351_H
#define EVENTHANDLER_1_T2154285351_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventHandler`1<TUIOsharp.DataProcessors.TuioBlobEventArgs>
struct  EventHandler_1_t2154285351  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTHANDLER_1_T2154285351_H


// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::TryGetValue(!0,!1&)
extern "C"  bool Dictionary_2_TryGetValue_m2140744741_gshared (Dictionary_2_t1697274930 * __this, int32_t p0, RuntimeObject ** p1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
extern "C"  void List_1_Add_m4157722533_gshared (List_1_t2058570427 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::Add(!0)
extern "C"  void List_1_Add_m688682013_gshared (List_1_t1440998580 * __this, int32_t p0, const RuntimeMethod* method);
// System.Collections.Generic.Dictionary`2/Enumerator<!0,!1> System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::GetEnumerator()
extern "C"  Enumerator_t3017299632  Dictionary_2_GetEnumerator_m3404768274_gshared (Dictionary_2_t1697274930 * __this, const RuntimeMethod* method);
// System.Collections.Generic.KeyValuePair`2<!0,!1> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t3749587448  Enumerator_get_Current_m2754383612_gshared (Enumerator_t3017299632 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m1537018582_gshared (KeyValuePair_2_t3749587448 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<System.Int32>::Contains(!0)
extern "C"  bool List_1_Contains_m3517981001_gshared (List_1_t1440998580 * __this, int32_t p0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<System.Int32>::Remove(!0)
extern "C"  bool List_1_Remove_m282822032_gshared (List_1_t1440998580 * __this, int32_t p0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2770956757_gshared (Enumerator_t3017299632 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m2243145188_gshared (Enumerator_t3017299632 * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C"  int32_t List_1_get_Count_m2375293942_gshared (List_1_t2058570427 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern "C"  RuntimeObject * List_1_get_Item_m2062981835_gshared (List_1_t2058570427 * __this, int32_t p0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::ContainsKey(!0)
extern "C"  bool Dictionary_2_ContainsKey_m313884773_gshared (Dictionary_2_t1697274930 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::Add(!0,!1)
extern "C"  void Dictionary_2_Add_m1296007576_gshared (Dictionary_2_t1697274930 * __this, int32_t p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Void System.EventHandler`1<System.Object>::Invoke(System.Object,!0)
extern "C"  void EventHandler_1_Invoke_m2430999761_gshared (EventHandler_1_t1280756467 * __this, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Int32>::get_Count()
extern "C"  int32_t List_1_get_Count_m3833405771_gshared (List_1_t1440998580 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Int32>::get_Item(System.Int32)
extern "C"  int32_t List_1_get_Item_m3514532498_gshared (List_1_t1440998580 * __this, int32_t p0, const RuntimeMethod* method);
// !1 System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::get_Item(!0)
extern "C"  RuntimeObject * Dictionary_2_get_Item_m1573979506_gshared (Dictionary_2_t1697274930 * __this, int32_t p0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::Remove(!0)
extern "C"  bool Dictionary_2_Remove_m2771612799_gshared (Dictionary_2_t1697274930 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::Clear()
extern "C"  void List_1_Clear_m3896574426_gshared (List_1_t1440998580 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
extern "C"  void List_1_Clear_m4254626809_gshared (List_1_t2058570427 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m1868603968_gshared (Dictionary_2_t1697274930 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C"  void List_1__ctor_m310736118_gshared (List_1_t2058570427 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor()
extern "C"  void List_1__ctor_m1598946593_gshared (List_1_t1440998580 * __this, const RuntimeMethod* method);
// System.Void System.EventHandler`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void EventHandler_1__ctor_m805401670_gshared (EventHandler_1_t1280756467 * __this, RuntimeObject * p0, IntPtr_t p1, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<System.Object>::Contains(!0)
extern "C"  bool List_1_Contains_m1658838094_gshared (List_1_t2058570427 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<System.Object>::Remove(!0)
extern "C"  bool List_1_Remove_m3164383811_gshared (List_1_t2058570427 * __this, RuntimeObject * p0, const RuntimeMethod* method);

// System.Delegate System.Delegate::Combine(System.Delegate,System.Delegate)
extern "C"  Delegate_t3022476291 * Delegate_Combine_m3791207084 (RuntimeObject * __this /* static, unused */, Delegate_t3022476291 * p0, Delegate_t3022476291 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::Remove(System.Delegate,System.Delegate)
extern "C"  Delegate_t3022476291 * Delegate_Remove_m2626518725 (RuntimeObject * __this /* static, unused */, Delegate_t3022476291 * p0, Delegate_t3022476291 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String OSCsharp.Data.OscPacket::get_Address()
extern "C"  String_t* OscPacket_get_Address_m1502094218 (OscPacket_t504761797 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::op_Inequality(System.String,System.String)
extern "C"  bool String_op_Inequality_m304203149 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<System.Object> OSCsharp.Data.OscPacket::get_Data()
extern "C"  RuntimeObject* OscPacket_get_Data_m65815223 (OscPacket_t504761797 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::op_Equality(System.String,System.String)
extern "C"  bool String_op_Equality_m1790663636 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioBlob>::TryGetValue(!0,!1&)
#define Dictionary_2_TryGetValue_m1913696641(__this, p0, p1, method) ((  bool (*) (Dictionary_2_t1054769049 *, int32_t, TuioBlob_t2046943414 **, const RuntimeMethod*))Dictionary_2_TryGetValue_m2140744741_gshared)(__this, p0, p1, method)
// System.Void TUIOsharp.Entities.TuioBlob::.ctor(System.Int32)
extern "C"  void TuioBlob__ctor_m2840170451 (TuioBlob_t2046943414 * __this, int32_t ___id0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.Entities.TuioBlob::Update(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C"  void TuioBlob_Update_m2006179440 (TuioBlob_t2046943414 * __this, float ___x0, float ___y1, float ___angle2, float ___width3, float ___height4, float ___area5, float ___velocityX6, float ___velocityY7, float ___rotationVelocity8, float ___acceleration9, float ___rotationAcceleration10, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>::Add(!0)
#define List_1_Add_m2273408866(__this, p0, method) ((  void (*) (List_1_t1416064546 *, TuioBlob_t2046943414 *, const RuntimeMethod*))List_1_Add_m4157722533_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::Add(!0)
#define List_1_Add_m688682013(__this, p0, method) ((  void (*) (List_1_t1440998580 *, int32_t, const RuntimeMethod*))List_1_Add_m688682013_gshared)(__this, p0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<!0,!1> System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioBlob>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m2501398850(__this, method) ((  Enumerator_t2374793751  (*) (Dictionary_2_t1054769049 *, const RuntimeMethod*))Dictionary_2_GetEnumerator_m3404768274_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<!0,!1> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TUIOsharp.Entities.TuioBlob>::get_Current()
#define Enumerator_get_Current_m3060082652(__this, method) ((  KeyValuePair_2_t3107081567  (*) (Enumerator_t2374793751 *, const RuntimeMethod*))Enumerator_get_Current_m2754383612_gshared)(__this, method)
// !0 System.Collections.Generic.KeyValuePair`2<System.Int32,TUIOsharp.Entities.TuioBlob>::get_Key()
#define KeyValuePair_2_get_Key_m758296990(__this, method) ((  int32_t (*) (KeyValuePair_2_t3107081567 *, const RuntimeMethod*))KeyValuePair_2_get_Key_m1537018582_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Int32>::Contains(!0)
#define List_1_Contains_m3517981001(__this, p0, method) ((  bool (*) (List_1_t1440998580 *, int32_t, const RuntimeMethod*))List_1_Contains_m3517981001_gshared)(__this, p0, method)
// System.Boolean System.Collections.Generic.List`1<System.Int32>::Remove(!0)
#define List_1_Remove_m282822032(__this, p0, method) ((  bool (*) (List_1_t1440998580 *, int32_t, const RuntimeMethod*))List_1_Remove_m282822032_gshared)(__this, p0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TUIOsharp.Entities.TuioBlob>::MoveNext()
#define Enumerator_MoveNext_m2555211001(__this, method) ((  bool (*) (Enumerator_t2374793751 *, const RuntimeMethod*))Enumerator_MoveNext_m2770956757_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TUIOsharp.Entities.TuioBlob>::Dispose()
#define Enumerator_Dispose_m797473218(__this, method) ((  void (*) (Enumerator_t2374793751 *, const RuntimeMethod*))Enumerator_Dispose_m2243145188_gshared)(__this, method)
// System.Void TUIOsharp.DataProcessors.BlobProcessor::set_FrameNumber(System.Int32)
extern "C"  void BlobProcessor_set_FrameNumber_m1366367337 (BlobProcessor_t3603341577 * __this, int32_t ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>::get_Count()
#define List_1_get_Count_m2267760198(__this, method) ((  int32_t (*) (List_1_t1416064546 *, const RuntimeMethod*))List_1_get_Count_m2375293942_gshared)(__this, method)
// !0 System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>::get_Item(System.Int32)
#define List_1_get_Item_m3035119279(__this, p0, method) ((  TuioBlob_t2046943414 * (*) (List_1_t1416064546 *, int32_t, const RuntimeMethod*))List_1_get_Item_m2062981835_gshared)(__this, p0, method)
// System.Int32 TUIOsharp.Entities.TuioEntity::get_Id()
extern "C"  int32_t TuioEntity_get_Id_m4208226194 (TuioEntity_t295276718 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioBlob>::ContainsKey(!0)
#define Dictionary_2_ContainsKey_m3923242129(__this, p0, method) ((  bool (*) (Dictionary_2_t1054769049 *, int32_t, const RuntimeMethod*))Dictionary_2_ContainsKey_m313884773_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioBlob>::Add(!0,!1)
#define Dictionary_2_Add_m4879272(__this, p0, p1, method) ((  void (*) (Dictionary_2_t1054769049 *, int32_t, TuioBlob_t2046943414 *, const RuntimeMethod*))Dictionary_2_Add_m1296007576_gshared)(__this, p0, p1, method)
// System.Void TUIOsharp.DataProcessors.TuioBlobEventArgs::.ctor(TUIOsharp.Entities.TuioBlob)
extern "C"  void TuioBlobEventArgs__ctor_m3423126035 (TuioBlobEventArgs_t3562978179 * __this, TuioBlob_t2046943414 * ___blob0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.EventHandler`1<TUIOsharp.DataProcessors.TuioBlobEventArgs>::Invoke(System.Object,!0)
#define EventHandler_1_Invoke_m897217542(__this, p0, p1, method) ((  void (*) (EventHandler_1_t2154285351 *, RuntimeObject *, TuioBlobEventArgs_t3562978179 *, const RuntimeMethod*))EventHandler_1_Invoke_m2430999761_gshared)(__this, p0, p1, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::get_Count()
#define List_1_get_Count_m3833405771(__this, method) ((  int32_t (*) (List_1_t1440998580 *, const RuntimeMethod*))List_1_get_Count_m3833405771_gshared)(__this, method)
// !0 System.Collections.Generic.List`1<System.Int32>::get_Item(System.Int32)
#define List_1_get_Item_m3514532498(__this, p0, method) ((  int32_t (*) (List_1_t1440998580 *, int32_t, const RuntimeMethod*))List_1_get_Item_m3514532498_gshared)(__this, p0, method)
// !1 System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioBlob>::get_Item(!0)
#define Dictionary_2_get_Item_m3637178650(__this, p0, method) ((  TuioBlob_t2046943414 * (*) (Dictionary_2_t1054769049 *, int32_t, const RuntimeMethod*))Dictionary_2_get_Item_m1573979506_gshared)(__this, p0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioBlob>::Remove(!0)
#define Dictionary_2_Remove_m3017945611(__this, p0, method) ((  bool (*) (Dictionary_2_t1054769049 *, int32_t, const RuntimeMethod*))Dictionary_2_Remove_m2771612799_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::Clear()
#define List_1_Clear_m3896574426(__this, method) ((  void (*) (List_1_t1440998580 *, const RuntimeMethod*))List_1_Clear_m3896574426_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>::Clear()
#define List_1_Clear_m1175215109(__this, method) ((  void (*) (List_1_t1416064546 *, const RuntimeMethod*))List_1_Clear_m4254626809_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioBlob>::.ctor()
#define Dictionary_2__ctor_m92522272(__this, method) ((  void (*) (Dictionary_2_t1054769049 *, const RuntimeMethod*))Dictionary_2__ctor_m1868603968_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioBlob>::.ctor()
#define List_1__ctor_m1986803350(__this, method) ((  void (*) (List_1_t1416064546 *, const RuntimeMethod*))List_1__ctor_m310736118_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor()
#define List_1__ctor_m1598946593(__this, method) ((  void (*) (List_1_t1440998580 *, const RuntimeMethod*))List_1__ctor_m1598946593_gshared)(__this, method)
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m2551263788 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioCursor>::TryGetValue(!0,!1&)
#define Dictionary_2_TryGetValue_m1936579402(__this, p0, p1, method) ((  bool (*) (Dictionary_2_t858177054 *, int32_t, TuioCursor_t1850351419 **, const RuntimeMethod*))Dictionary_2_TryGetValue_m2140744741_gshared)(__this, p0, p1, method)
// System.Void TUIOsharp.Entities.TuioCursor::.ctor(System.Int32)
extern "C"  void TuioCursor__ctor_m2689165534 (TuioCursor_t1850351419 * __this, int32_t ___id0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.Entities.TuioCursor::Update(System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C"  void TuioCursor_Update_m650004219 (TuioCursor_t1850351419 * __this, float ___x0, float ___y1, float ___velocityX2, float ___velocityY3, float ___acceleration4, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>::Add(!0)
#define List_1_Add_m2438782433(__this, p0, method) ((  void (*) (List_1_t1219472551 *, TuioCursor_t1850351419 *, const RuntimeMethod*))List_1_Add_m4157722533_gshared)(__this, p0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<!0,!1> System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioCursor>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m775930629(__this, method) ((  Enumerator_t2178201756  (*) (Dictionary_2_t858177054 *, const RuntimeMethod*))Dictionary_2_GetEnumerator_m3404768274_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<!0,!1> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TUIOsharp.Entities.TuioCursor>::get_Current()
#define Enumerator_get_Current_m2497618057(__this, method) ((  KeyValuePair_2_t2910489572  (*) (Enumerator_t2178201756 *, const RuntimeMethod*))Enumerator_get_Current_m2754383612_gshared)(__this, method)
// !0 System.Collections.Generic.KeyValuePair`2<System.Int32,TUIOsharp.Entities.TuioCursor>::get_Key()
#define KeyValuePair_2_get_Key_m624710547(__this, method) ((  int32_t (*) (KeyValuePair_2_t2910489572 *, const RuntimeMethod*))KeyValuePair_2_get_Key_m1537018582_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TUIOsharp.Entities.TuioCursor>::MoveNext()
#define Enumerator_MoveNext_m3093636814(__this, method) ((  bool (*) (Enumerator_t2178201756 *, const RuntimeMethod*))Enumerator_MoveNext_m2770956757_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TUIOsharp.Entities.TuioCursor>::Dispose()
#define Enumerator_Dispose_m4213942773(__this, method) ((  void (*) (Enumerator_t2178201756 *, const RuntimeMethod*))Enumerator_Dispose_m2243145188_gshared)(__this, method)
// System.Void TUIOsharp.DataProcessors.CursorProcessor::set_FrameNumber(System.Int32)
extern "C"  void CursorProcessor_set_FrameNumber_m78169520 (CursorProcessor_t1785954004 * __this, int32_t ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>::get_Count()
#define List_1_get_Count_m302198121(__this, method) ((  int32_t (*) (List_1_t1219472551 *, const RuntimeMethod*))List_1_get_Count_m2375293942_gshared)(__this, method)
// !0 System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>::get_Item(System.Int32)
#define List_1_get_Item_m2568686926(__this, p0, method) ((  TuioCursor_t1850351419 * (*) (List_1_t1219472551 *, int32_t, const RuntimeMethod*))List_1_get_Item_m2062981835_gshared)(__this, p0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioCursor>::ContainsKey(!0)
#define Dictionary_2_ContainsKey_m2357860370(__this, p0, method) ((  bool (*) (Dictionary_2_t858177054 *, int32_t, const RuntimeMethod*))Dictionary_2_ContainsKey_m313884773_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioCursor>::Add(!0,!1)
#define Dictionary_2_Add_m1095222709(__this, p0, p1, method) ((  void (*) (Dictionary_2_t858177054 *, int32_t, TuioCursor_t1850351419 *, const RuntimeMethod*))Dictionary_2_Add_m1296007576_gshared)(__this, p0, p1, method)
// System.Void TUIOsharp.DataProcessors.TuioCursorEventArgs::.ctor(TUIOsharp.Entities.TuioCursor)
extern "C"  void TuioCursorEventArgs__ctor_m2169915347 (TuioCursorEventArgs_t362990012 * __this, TuioCursor_t1850351419 * ___cursor0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.EventHandler`1<TUIOsharp.DataProcessors.TuioCursorEventArgs>::Invoke(System.Object,!0)
#define EventHandler_1_Invoke_m2962849243(__this, p0, p1, method) ((  void (*) (EventHandler_1_t3249264480 *, RuntimeObject *, TuioCursorEventArgs_t362990012 *, const RuntimeMethod*))EventHandler_1_Invoke_m2430999761_gshared)(__this, p0, p1, method)
// !1 System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioCursor>::get_Item(!0)
#define Dictionary_2_get_Item_m866734463(__this, p0, method) ((  TuioCursor_t1850351419 * (*) (Dictionary_2_t858177054 *, int32_t, const RuntimeMethod*))Dictionary_2_get_Item_m1573979506_gshared)(__this, p0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioCursor>::Remove(!0)
#define Dictionary_2_Remove_m2712103718(__this, p0, method) ((  bool (*) (Dictionary_2_t858177054 *, int32_t, const RuntimeMethod*))Dictionary_2_Remove_m2771612799_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>::Clear()
#define List_1_Clear_m2159038286(__this, method) ((  void (*) (List_1_t1219472551 *, const RuntimeMethod*))List_1_Clear_m4254626809_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioCursor>::.ctor()
#define Dictionary_2__ctor_m1652351677(__this, method) ((  void (*) (Dictionary_2_t858177054 *, const RuntimeMethod*))Dictionary_2__ctor_m1868603968_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioCursor>::.ctor()
#define List_1__ctor_m3131880517(__this, method) ((  void (*) (List_1_t1219472551 *, const RuntimeMethod*))List_1__ctor_m310736118_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioObject>::TryGetValue(!0,!1&)
#define Dictionary_2_TryGetValue_m1599847323(__this, p0, p1, method) ((  bool (*) (Dictionary_2_t244646649 *, int32_t, TuioObject_t1236821014 **, const RuntimeMethod*))Dictionary_2_TryGetValue_m2140744741_gshared)(__this, p0, p1, method)
// System.Void TUIOsharp.Entities.TuioObject::.ctor(System.Int32,System.Int32)
extern "C"  void TuioObject__ctor_m2498796396 (TuioObject_t1236821014 * __this, int32_t ___id0, int32_t ___classId1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.Entities.TuioObject::Update(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C"  void TuioObject_Update_m829237429 (TuioObject_t1236821014 * __this, float ___x0, float ___y1, float ___angle2, float ___velocityX3, float ___velocityY4, float ___rotationVelocity5, float ___acceleration6, float ___rotationAcceleration7, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioObject>::Add(!0)
#define List_1_Add_m4210503130(__this, p0, method) ((  void (*) (List_1_t605942146 *, TuioObject_t1236821014 *, const RuntimeMethod*))List_1_Add_m4157722533_gshared)(__this, p0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<!0,!1> System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioObject>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m2329010490(__this, method) ((  Enumerator_t1564671351  (*) (Dictionary_2_t244646649 *, const RuntimeMethod*))Dictionary_2_GetEnumerator_m3404768274_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<!0,!1> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TUIOsharp.Entities.TuioObject>::get_Current()
#define Enumerator_get_Current_m874299548(__this, method) ((  KeyValuePair_2_t2296959167  (*) (Enumerator_t1564671351 *, const RuntimeMethod*))Enumerator_get_Current_m2754383612_gshared)(__this, method)
// !0 System.Collections.Generic.KeyValuePair`2<System.Int32,TUIOsharp.Entities.TuioObject>::get_Key()
#define KeyValuePair_2_get_Key_m303742594(__this, method) ((  int32_t (*) (KeyValuePair_2_t2296959167 *, const RuntimeMethod*))KeyValuePair_2_get_Key_m1537018582_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TUIOsharp.Entities.TuioObject>::MoveNext()
#define Enumerator_MoveNext_m3089698347(__this, method) ((  bool (*) (Enumerator_t1564671351 *, const RuntimeMethod*))Enumerator_MoveNext_m2770956757_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,TUIOsharp.Entities.TuioObject>::Dispose()
#define Enumerator_Dispose_m4106138594(__this, method) ((  void (*) (Enumerator_t1564671351 *, const RuntimeMethod*))Enumerator_Dispose_m2243145188_gshared)(__this, method)
// System.Void TUIOsharp.DataProcessors.ObjectProcessor::set_FrameNumber(System.Int32)
extern "C"  void ObjectProcessor_set_FrameNumber_m3936044951 (ObjectProcessor_t221569383 * __this, int32_t ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.List`1<TUIOsharp.Entities.TuioObject>::get_Count()
#define List_1_get_Count_m1698483734(__this, method) ((  int32_t (*) (List_1_t605942146 *, const RuntimeMethod*))List_1_get_Count_m2375293942_gshared)(__this, method)
// !0 System.Collections.Generic.List`1<TUIOsharp.Entities.TuioObject>::get_Item(System.Int32)
#define List_1_get_Item_m992642369(__this, p0, method) ((  TuioObject_t1236821014 * (*) (List_1_t605942146 *, int32_t, const RuntimeMethod*))List_1_get_Item_m2062981835_gshared)(__this, p0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioObject>::ContainsKey(!0)
#define Dictionary_2_ContainsKey_m781358407(__this, p0, method) ((  bool (*) (Dictionary_2_t244646649 *, int32_t, const RuntimeMethod*))Dictionary_2_ContainsKey_m313884773_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioObject>::Add(!0,!1)
#define Dictionary_2_Add_m2557918960(__this, p0, p1, method) ((  void (*) (Dictionary_2_t244646649 *, int32_t, TuioObject_t1236821014 *, const RuntimeMethod*))Dictionary_2_Add_m1296007576_gshared)(__this, p0, p1, method)
// System.Void TUIOsharp.DataProcessors.TuioObjectEventArgs::.ctor(TUIOsharp.Entities.TuioObject)
extern "C"  void TuioObjectEventArgs__ctor_m1688032627 (TuioObjectEventArgs_t1810743341 * __this, TuioObject_t1236821014 * ___obj0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.EventHandler`1<TUIOsharp.DataProcessors.TuioObjectEventArgs>::Invoke(System.Object,!0)
#define EventHandler_1_Invoke_m1340678838(__this, p0, p1, method) ((  void (*) (EventHandler_1_t402050513 *, RuntimeObject *, TuioObjectEventArgs_t1810743341 *, const RuntimeMethod*))EventHandler_1_Invoke_m2430999761_gshared)(__this, p0, p1, method)
// !1 System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioObject>::get_Item(!0)
#define Dictionary_2_get_Item_m4097636154(__this, p0, method) ((  TuioObject_t1236821014 * (*) (Dictionary_2_t244646649 *, int32_t, const RuntimeMethod*))Dictionary_2_get_Item_m1573979506_gshared)(__this, p0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioObject>::Remove(!0)
#define Dictionary_2_Remove_m185872653(__this, p0, method) ((  bool (*) (Dictionary_2_t244646649 *, int32_t, const RuntimeMethod*))Dictionary_2_Remove_m2771612799_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioObject>::Clear()
#define List_1_Clear_m2478926039(__this, method) ((  void (*) (List_1_t605942146 *, const RuntimeMethod*))List_1_Clear_m4254626809_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,TUIOsharp.Entities.TuioObject>::.ctor()
#define Dictionary_2__ctor_m1364836728(__this, method) ((  void (*) (Dictionary_2_t244646649 *, const RuntimeMethod*))Dictionary_2__ctor_m1868603968_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.Entities.TuioObject>::.ctor()
#define List_1__ctor_m4191486726(__this, method) ((  void (*) (List_1_t605942146 *, const RuntimeMethod*))List_1__ctor_m310736118_gshared)(__this, method)
// System.Void System.EventArgs::.ctor()
extern "C"  void EventArgs__ctor_m3696060910 (EventArgs_t3289624707 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.Entities.TuioBlob::.ctor(System.Int32,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C"  void TuioBlob__ctor_m1637502122 (TuioBlob_t2046943414 * __this, int32_t ___id0, float ___x1, float ___y2, float ___angle3, float ___width4, float ___height5, float ___area6, float ___velocityX7, float ___velocityY8, float ___rotationVelocity9, float ___acceleration10, float ___rotationAcceleration11, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.Entities.TuioEntity::.ctor(System.Int32,System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C"  void TuioEntity__ctor_m3219791970 (TuioEntity_t295276718 * __this, int32_t ___id0, float ___x1, float ___y2, float ___velocityX3, float ___velocityY4, float ___acceleration5, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.Entities.TuioBlob::set_Angle(System.Single)
extern "C"  void TuioBlob_set_Angle_m683906823 (TuioBlob_t2046943414 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.Entities.TuioBlob::set_Width(System.Single)
extern "C"  void TuioBlob_set_Width_m3192515620 (TuioBlob_t2046943414 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.Entities.TuioBlob::set_Height(System.Single)
extern "C"  void TuioBlob_set_Height_m2645716865 (TuioBlob_t2046943414 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.Entities.TuioBlob::set_Area(System.Single)
extern "C"  void TuioBlob_set_Area_m1501805605 (TuioBlob_t2046943414 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.Entities.TuioBlob::set_RotationVelocity(System.Single)
extern "C"  void TuioBlob_set_RotationVelocity_m2915155457 (TuioBlob_t2046943414 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.Entities.TuioBlob::set_RotationAcceleration(System.Single)
extern "C"  void TuioBlob_set_RotationAcceleration_m892624954 (TuioBlob_t2046943414 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.Entities.TuioEntity::set_X(System.Single)
extern "C"  void TuioEntity_set_X_m24639236 (TuioEntity_t295276718 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.Entities.TuioEntity::set_Y(System.Single)
extern "C"  void TuioEntity_set_Y_m3783946495 (TuioEntity_t295276718 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.Entities.TuioEntity::set_VelocityX(System.Single)
extern "C"  void TuioEntity_set_VelocityX_m731909633 (TuioEntity_t295276718 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.Entities.TuioEntity::set_VelocityY(System.Single)
extern "C"  void TuioEntity_set_VelocityY_m1367177094 (TuioEntity_t295276718 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.Entities.TuioEntity::set_Acceleration(System.Single)
extern "C"  void TuioEntity_set_Acceleration_m3065927520 (TuioEntity_t295276718 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.Entities.TuioCursor::.ctor(System.Int32,System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C"  void TuioCursor__ctor_m944054751 (TuioCursor_t1850351419 * __this, int32_t ___id0, float ___x1, float ___y2, float ___velocityX3, float ___velocityY4, float ___acceleration5, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.Entities.TuioEntity::set_Id(System.Int32)
extern "C"  void TuioEntity_set_Id_m4152509111 (TuioEntity_t295276718 * __this, int32_t ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.Entities.TuioObject::.ctor(System.Int32,System.Int32,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C"  void TuioObject__ctor_m3091979500 (TuioObject_t1236821014 * __this, int32_t ___id0, int32_t ___classId1, float ___x2, float ___y3, float ___angle4, float ___velocityX5, float ___velocityY6, float ___rotationVelocity7, float ___acceleration8, float ___rotationAcceleration9, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.Entities.TuioObject::set_ClassId(System.Int32)
extern "C"  void TuioObject_set_ClassId_m2907387411 (TuioObject_t1236821014 * __this, int32_t ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.Entities.TuioObject::set_Angle(System.Single)
extern "C"  void TuioObject_set_Angle_m3378571757 (TuioObject_t1236821014 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.Entities.TuioObject::set_RotationVelocity(System.Single)
extern "C"  void TuioObject_set_RotationVelocity_m2262321299 (TuioObject_t1236821014 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.Entities.TuioObject::set_RotationAcceleration(System.Single)
extern "C"  void TuioObject_set_RotationAcceleration_m2446722782 (TuioObject_t1236821014 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::.ctor()
#define List_1__ctor_m4170624131(__this, method) ((  void (*) (List_1_t1583176531 *, const RuntimeMethod*))List_1__ctor_m310736118_gshared)(__this, method)
// System.Void TUIOsharp.TuioServer::set_Port(System.Int32)
extern "C"  void TuioServer_set_Port_m2088027290 (TuioServer_t595520884 * __this, int32_t ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 TUIOsharp.TuioServer::get_Port()
extern "C"  int32_t TuioServer_get_Port_m3441621021 (TuioServer_t595520884 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.UDPReceiver::.ctor(System.Int32,System.Boolean)
extern "C"  void UDPReceiver__ctor_m3124846498 (UDPReceiver_t3846109956 * __this, int32_t p0, bool p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.EventHandler`1<OSCsharp.Net.OscMessageReceivedEventArgs>::.ctor(System.Object,System.IntPtr)
#define EventHandler_1__ctor_m384786560(__this, p0, p1, method) ((  void (*) (EventHandler_1_t4149316328 *, RuntimeObject *, IntPtr_t, const RuntimeMethod*))EventHandler_1__ctor_m805401670_gshared)(__this, p0, p1, method)
// System.Void OSCsharp.Net.UDPReceiver::add_MessageReceived(System.EventHandler`1<OSCsharp.Net.OscMessageReceivedEventArgs>)
extern "C"  void UDPReceiver_add_MessageReceived_m3470532992 (UDPReceiver_t3846109956 * __this, EventHandler_1_t4149316328 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.EventHandler`1<OSCsharp.Utils.ExceptionEventArgs>::.ctor(System.Object,System.IntPtr)
#define EventHandler_1__ctor_m4043119420(__this, p0, p1, method) ((  void (*) (EventHandler_1_t3778988004 *, RuntimeObject *, IntPtr_t, const RuntimeMethod*))EventHandler_1__ctor_m805401670_gshared)(__this, p0, p1, method)
// System.Void OSCsharp.Net.UDPReceiver::add_ErrorOccured(System.EventHandler`1<OSCsharp.Utils.ExceptionEventArgs>)
extern "C"  void UDPReceiver_add_ErrorOccured_m1212653731 (UDPReceiver_t3846109956 * __this, EventHandler_1_t3778988004 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean OSCsharp.Net.UDPReceiver::get_IsRunning()
extern "C"  bool UDPReceiver_get_IsRunning_m2474408206 (UDPReceiver_t3846109956 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.UDPReceiver::Start()
extern "C"  void UDPReceiver_Start_m1033630334 (UDPReceiver_t3846109956 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void OSCsharp.Net.UDPReceiver::Stop()
extern "C"  void UDPReceiver_Stop_m3061802084 (UDPReceiver_t3846109956 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::Contains(!0)
#define List_1_Contains_m181590265(__this, p0, method) ((  bool (*) (List_1_t1583176531 *, RuntimeObject*, const RuntimeMethod*))List_1_Contains_m1658838094_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::Add(!0)
#define List_1_Add_m2594654071(__this, p0, method) ((  void (*) (List_1_t1583176531 *, RuntimeObject*, const RuntimeMethod*))List_1_Add_m4157722533_gshared)(__this, p0, method)
// System.Boolean System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::Remove(!0)
#define List_1_Remove_m3595814454(__this, p0, method) ((  bool (*) (List_1_t1583176531 *, RuntimeObject*, const RuntimeMethod*))List_1_Remove_m3164383811_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::Clear()
#define List_1_Clear_m3203701604(__this, method) ((  void (*) (List_1_t1583176531 *, const RuntimeMethod*))List_1_Clear_m4254626809_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::get_Count()
#define List_1_get_Count_m314033343(__this, method) ((  int32_t (*) (List_1_t1583176531 *, const RuntimeMethod*))List_1_get_Count_m2375293942_gshared)(__this, method)
// !0 System.Collections.Generic.List`1<TUIOsharp.DataProcessors.IDataProcessor>::get_Item(System.Int32)
#define List_1_get_Item_m2840009344(__this, p0, method) ((  RuntimeObject* (*) (List_1_t1583176531 *, int32_t, const RuntimeMethod*))List_1_get_Item_m2062981835_gshared)(__this, p0, method)
// System.Void System.EventHandler`1<OSCsharp.Utils.ExceptionEventArgs>::Invoke(System.Object,!0)
#define EventHandler_1_Invoke_m3855877691(__this, p0, p1, method) ((  void (*) (EventHandler_1_t3778988004 *, RuntimeObject *, ExceptionEventArgs_t892713536 *, const RuntimeMethod*))EventHandler_1_Invoke_m2430999761_gshared)(__this, p0, p1, method)
// OSCsharp.Data.OscMessage OSCsharp.Net.OscMessageReceivedEventArgs::get_Message()
extern "C"  OscMessage_t2764280154 * OscMessageReceivedEventArgs_get_Message_m3135203290 (OscMessageReceivedEventArgs_t1263041860 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void TUIOsharp.TuioServer::processMessage(OSCsharp.Data.OscMessage)
extern "C"  void TuioServer_processMessage_m3007744804 (TuioServer_t595520884 * __this, OscMessage_t2764280154 * ___message0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TUIOsharp.DataProcessors.BlobProcessor::add_BlobAdded(System.EventHandler`1<TUIOsharp.DataProcessors.TuioBlobEventArgs>)
extern "C"  void BlobProcessor_add_BlobAdded_m1362271275 (BlobProcessor_t3603341577 * __this, EventHandler_1_t2154285351 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BlobProcessor_add_BlobAdded_m1362271275_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t2154285351 * V_0 = NULL;
	EventHandler_1_t2154285351 * V_1 = NULL;
	EventHandler_1_t2154285351 * V_2 = NULL;
	{
		EventHandler_1_t2154285351 * L_0 = __this->get_BlobAdded_0();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t2154285351 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t2154285351 * L_2 = V_1;
		EventHandler_1_t2154285351 * L_3 = ___value0;
		Delegate_t3022476291 * L_4 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((EventHandler_1_t2154285351 *)CastclassSealed((RuntimeObject*)L_4, EventHandler_1_t2154285351_il2cpp_TypeInfo_var));
		EventHandler_1_t2154285351 ** L_5 = __this->get_address_of_BlobAdded_0();
		EventHandler_1_t2154285351 * L_6 = V_2;
		EventHandler_1_t2154285351 * L_7 = V_1;
		EventHandler_1_t2154285351 * L_8 = InterlockedCompareExchangeImpl<EventHandler_1_t2154285351 *>(L_5, L_6, L_7);
		V_0 = L_8;
		EventHandler_1_t2154285351 * L_9 = V_0;
		EventHandler_1_t2154285351 * L_10 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_t2154285351 *)L_9) == ((RuntimeObject*)(EventHandler_1_t2154285351 *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void TUIOsharp.DataProcessors.BlobProcessor::remove_BlobAdded(System.EventHandler`1<TUIOsharp.DataProcessors.TuioBlobEventArgs>)
extern "C"  void BlobProcessor_remove_BlobAdded_m3134592944 (BlobProcessor_t3603341577 * __this, EventHandler_1_t2154285351 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BlobProcessor_remove_BlobAdded_m3134592944_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t2154285351 * V_0 = NULL;
	EventHandler_1_t2154285351 * V_1 = NULL;
	EventHandler_1_t2154285351 * V_2 = NULL;
	{
		EventHandler_1_t2154285351 * L_0 = __this->get_BlobAdded_0();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t2154285351 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t2154285351 * L_2 = V_1;
		EventHandler_1_t2154285351 * L_3 = ___value0;
		Delegate_t3022476291 * L_4 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((EventHandler_1_t2154285351 *)CastclassSealed((RuntimeObject*)L_4, EventHandler_1_t2154285351_il2cpp_TypeInfo_var));
		EventHandler_1_t2154285351 ** L_5 = __this->get_address_of_BlobAdded_0();
		EventHandler_1_t2154285351 * L_6 = V_2;
		EventHandler_1_t2154285351 * L_7 = V_1;
		EventHandler_1_t2154285351 * L_8 = InterlockedCompareExchangeImpl<EventHandler_1_t2154285351 *>(L_5, L_6, L_7);
		V_0 = L_8;
		EventHandler_1_t2154285351 * L_9 = V_0;
		EventHandler_1_t2154285351 * L_10 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_t2154285351 *)L_9) == ((RuntimeObject*)(EventHandler_1_t2154285351 *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void TUIOsharp.DataProcessors.BlobProcessor::add_BlobUpdated(System.EventHandler`1<TUIOsharp.DataProcessors.TuioBlobEventArgs>)
extern "C"  void BlobProcessor_add_BlobUpdated_m1642149482 (BlobProcessor_t3603341577 * __this, EventHandler_1_t2154285351 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BlobProcessor_add_BlobUpdated_m1642149482_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t2154285351 * V_0 = NULL;
	EventHandler_1_t2154285351 * V_1 = NULL;
	EventHandler_1_t2154285351 * V_2 = NULL;
	{
		EventHandler_1_t2154285351 * L_0 = __this->get_BlobUpdated_1();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t2154285351 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t2154285351 * L_2 = V_1;
		EventHandler_1_t2154285351 * L_3 = ___value0;
		Delegate_t3022476291 * L_4 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((EventHandler_1_t2154285351 *)CastclassSealed((RuntimeObject*)L_4, EventHandler_1_t2154285351_il2cpp_TypeInfo_var));
		EventHandler_1_t2154285351 ** L_5 = __this->get_address_of_BlobUpdated_1();
		EventHandler_1_t2154285351 * L_6 = V_2;
		EventHandler_1_t2154285351 * L_7 = V_1;
		EventHandler_1_t2154285351 * L_8 = InterlockedCompareExchangeImpl<EventHandler_1_t2154285351 *>(L_5, L_6, L_7);
		V_0 = L_8;
		EventHandler_1_t2154285351 * L_9 = V_0;
		EventHandler_1_t2154285351 * L_10 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_t2154285351 *)L_9) == ((RuntimeObject*)(EventHandler_1_t2154285351 *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void TUIOsharp.DataProcessors.BlobProcessor::remove_BlobUpdated(System.EventHandler`1<TUIOsharp.DataProcessors.TuioBlobEventArgs>)
extern "C"  void BlobProcessor_remove_BlobUpdated_m1870432989 (BlobProcessor_t3603341577 * __this, EventHandler_1_t2154285351 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BlobProcessor_remove_BlobUpdated_m1870432989_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t2154285351 * V_0 = NULL;
	EventHandler_1_t2154285351 * V_1 = NULL;
	EventHandler_1_t2154285351 * V_2 = NULL;
	{
		EventHandler_1_t2154285351 * L_0 = __this->get_BlobUpdated_1();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t2154285351 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t2154285351 * L_2 = V_1;
		EventHandler_1_t2154285351 * L_3 = ___value0;
		Delegate_t3022476291 * L_4 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((EventHandler_1_t2154285351 *)CastclassSealed((RuntimeObject*)L_4, EventHandler_1_t2154285351_il2cpp_TypeInfo_var));
		EventHandler_1_t2154285351 ** L_5 = __this->get_address_of_BlobUpdated_1();
		EventHandler_1_t2154285351 * L_6 = V_2;
		EventHandler_1_t2154285351 * L_7 = V_1;
		EventHandler_1_t2154285351 * L_8 = InterlockedCompareExchangeImpl<EventHandler_1_t2154285351 *>(L_5, L_6, L_7);
		V_0 = L_8;
		EventHandler_1_t2154285351 * L_9 = V_0;
		EventHandler_1_t2154285351 * L_10 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_t2154285351 *)L_9) == ((RuntimeObject*)(EventHandler_1_t2154285351 *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void TUIOsharp.DataProcessors.BlobProcessor::add_BlobRemoved(System.EventHandler`1<TUIOsharp.DataProcessors.TuioBlobEventArgs>)
extern "C"  void BlobProcessor_add_BlobRemoved_m486758865 (BlobProcessor_t3603341577 * __this, EventHandler_1_t2154285351 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BlobProcessor_add_BlobRemoved_m486758865_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t2154285351 * V_0 = NULL;
	EventHandler_1_t2154285351 * V_1 = NULL;
	EventHandler_1_t2154285351 * V_2 = NULL;
	{
		EventHandler_1_t2154285351 * L_0 = __this->get_BlobRemoved_2();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t2154285351 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t2154285351 * L_2 = V_1;
		EventHandler_1_t2154285351 * L_3 = ___value0;
		Delegate_t3022476291 * L_4 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((EventHandler_1_t2154285351 *)CastclassSealed((RuntimeObject*)L_4, EventHandler_1_t2154285351_il2cpp_TypeInfo_var));
		EventHandler_1_t2154285351 ** L_5 = __this->get_address_of_BlobRemoved_2();
		EventHandler_1_t2154285351 * L_6 = V_2;
		EventHandler_1_t2154285351 * L_7 = V_1;
		EventHandler_1_t2154285351 * L_8 = InterlockedCompareExchangeImpl<EventHandler_1_t2154285351 *>(L_5, L_6, L_7);
		V_0 = L_8;
		EventHandler_1_t2154285351 * L_9 = V_0;
		EventHandler_1_t2154285351 * L_10 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_t2154285351 *)L_9) == ((RuntimeObject*)(EventHandler_1_t2154285351 *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void TUIOsharp.DataProcessors.BlobProcessor::remove_BlobRemoved(System.EventHandler`1<TUIOsharp.DataProcessors.TuioBlobEventArgs>)
extern "C"  void BlobProcessor_remove_BlobRemoved_m1953649818 (BlobProcessor_t3603341577 * __this, EventHandler_1_t2154285351 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BlobProcessor_remove_BlobRemoved_m1953649818_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t2154285351 * V_0 = NULL;
	EventHandler_1_t2154285351 * V_1 = NULL;
	EventHandler_1_t2154285351 * V_2 = NULL;
	{
		EventHandler_1_t2154285351 * L_0 = __this->get_BlobRemoved_2();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t2154285351 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t2154285351 * L_2 = V_1;
		EventHandler_1_t2154285351 * L_3 = ___value0;
		Delegate_t3022476291 * L_4 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((EventHandler_1_t2154285351 *)CastclassSealed((RuntimeObject*)L_4, EventHandler_1_t2154285351_il2cpp_TypeInfo_var));
		EventHandler_1_t2154285351 ** L_5 = __this->get_address_of_BlobRemoved_2();
		EventHandler_1_t2154285351 * L_6 = V_2;
		EventHandler_1_t2154285351 * L_7 = V_1;
		EventHandler_1_t2154285351 * L_8 = InterlockedCompareExchangeImpl<EventHandler_1_t2154285351 *>(L_5, L_6, L_7);
		V_0 = L_8;
		EventHandler_1_t2154285351 * L_9 = V_0;
		EventHandler_1_t2154285351 * L_10 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_t2154285351 *)L_9) == ((RuntimeObject*)(EventHandler_1_t2154285351 *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void TUIOsharp.DataProcessors.BlobProcessor::set_FrameNumber(System.Int32)
extern "C"  void BlobProcessor_set_FrameNumber_m1366367337 (BlobProcessor_t3603341577 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CFrameNumberU3Ek__BackingField_7(L_0);
		return;
	}
}
// System.Void TUIOsharp.DataProcessors.BlobProcessor::ProcessMessage(OSCsharp.Data.OscMessage)
extern "C"  void BlobProcessor_ProcessMessage_m3196822674 (BlobProcessor_t3603341577 * __this, OscMessage_t2764280154 * ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BlobProcessor_ProcessMessage_m3196822674_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	float V_9 = 0.0f;
	float V_10 = 0.0f;
	float V_11 = 0.0f;
	float V_12 = 0.0f;
	TuioBlob_t2046943414 * V_13 = NULL;
	int32_t V_14 = 0;
	int32_t V_15 = 0;
	KeyValuePair_2_t3107081567  V_16;
	memset(&V_16, 0, sizeof(V_16));
	int32_t V_17 = 0;
	int32_t V_18 = 0;
	TuioBlob_t2046943414 * V_19 = NULL;
	int32_t V_20 = 0;
	int32_t V_21 = 0;
	String_t* V_22 = NULL;
	Enumerator_t2374793751  V_23;
	memset(&V_23, 0, sizeof(V_23));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		OscMessage_t2764280154 * L_0 = ___message0;
		NullCheck(L_0);
		String_t* L_1 = OscPacket_get_Address_m1502094218(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_1, _stringLiteral760373117, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0013;
		}
	}
	{
		return;
	}

IL_0013:
	{
		OscMessage_t2764280154 * L_3 = ___message0;
		NullCheck(L_3);
		RuntimeObject* L_4 = OscPacket_get_Data_m65815223(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		RuntimeObject * L_5 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t3230389896_il2cpp_TypeInfo_var, L_4, 0);
		NullCheck(L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_5);
		V_0 = L_6;
		String_t* L_7 = V_0;
		String_t* L_8 = L_7;
		V_22 = L_8;
		if (!L_8)
		{
			goto IL_0390;
		}
	}
	{
		String_t* L_9 = V_22;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_10 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_9, _stringLiteral3021628834, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_005f;
		}
	}
	{
		String_t* L_11 = V_22;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_12 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_11, _stringLiteral2273848675, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_0195;
		}
	}
	{
		String_t* L_13 = V_22;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_14 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_13, _stringLiteral1851287145, /*hidden argument*/NULL);
		if (L_14)
		{
			goto IL_023d;
		}
	}
	{
		return;
	}

IL_005f:
	{
		OscMessage_t2764280154 * L_15 = ___message0;
		NullCheck(L_15);
		RuntimeObject* L_16 = OscPacket_get_Data_m65815223(L_15, /*hidden argument*/NULL);
		NullCheck(L_16);
		int32_t L_17 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, ICollection_1_t3641524600_il2cpp_TypeInfo_var, L_16);
		if ((((int32_t)L_17) >= ((int32_t)((int32_t)13))))
		{
			goto IL_006f;
		}
	}
	{
		return;
	}

IL_006f:
	{
		OscMessage_t2764280154 * L_18 = ___message0;
		NullCheck(L_18);
		RuntimeObject* L_19 = OscPacket_get_Data_m65815223(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		RuntimeObject * L_20 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t3230389896_il2cpp_TypeInfo_var, L_19, 1);
		V_1 = ((*(int32_t*)((int32_t*)UnBox(L_20, Int32_t2071877448_il2cpp_TypeInfo_var))));
		OscMessage_t2764280154 * L_21 = ___message0;
		NullCheck(L_21);
		RuntimeObject* L_22 = OscPacket_get_Data_m65815223(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		RuntimeObject * L_23 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t3230389896_il2cpp_TypeInfo_var, L_22, 2);
		V_2 = ((*(float*)((float*)UnBox(L_23, Single_t2076509932_il2cpp_TypeInfo_var))));
		OscMessage_t2764280154 * L_24 = ___message0;
		NullCheck(L_24);
		RuntimeObject* L_25 = OscPacket_get_Data_m65815223(L_24, /*hidden argument*/NULL);
		NullCheck(L_25);
		RuntimeObject * L_26 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t3230389896_il2cpp_TypeInfo_var, L_25, 3);
		V_3 = ((*(float*)((float*)UnBox(L_26, Single_t2076509932_il2cpp_TypeInfo_var))));
		OscMessage_t2764280154 * L_27 = ___message0;
		NullCheck(L_27);
		RuntimeObject* L_28 = OscPacket_get_Data_m65815223(L_27, /*hidden argument*/NULL);
		NullCheck(L_28);
		RuntimeObject * L_29 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t3230389896_il2cpp_TypeInfo_var, L_28, 4);
		V_4 = ((*(float*)((float*)UnBox(L_29, Single_t2076509932_il2cpp_TypeInfo_var))));
		OscMessage_t2764280154 * L_30 = ___message0;
		NullCheck(L_30);
		RuntimeObject* L_31 = OscPacket_get_Data_m65815223(L_30, /*hidden argument*/NULL);
		NullCheck(L_31);
		RuntimeObject * L_32 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t3230389896_il2cpp_TypeInfo_var, L_31, 5);
		V_5 = ((*(float*)((float*)UnBox(L_32, Single_t2076509932_il2cpp_TypeInfo_var))));
		OscMessage_t2764280154 * L_33 = ___message0;
		NullCheck(L_33);
		RuntimeObject* L_34 = OscPacket_get_Data_m65815223(L_33, /*hidden argument*/NULL);
		NullCheck(L_34);
		RuntimeObject * L_35 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t3230389896_il2cpp_TypeInfo_var, L_34, 6);
		V_6 = ((*(float*)((float*)UnBox(L_35, Single_t2076509932_il2cpp_TypeInfo_var))));
		OscMessage_t2764280154 * L_36 = ___message0;
		NullCheck(L_36);
		RuntimeObject* L_37 = OscPacket_get_Data_m65815223(L_36, /*hidden argument*/NULL);
		NullCheck(L_37);
		RuntimeObject * L_38 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t3230389896_il2cpp_TypeInfo_var, L_37, 7);
		V_7 = ((*(float*)((float*)UnBox(L_38, Single_t2076509932_il2cpp_TypeInfo_var))));
		OscMessage_t2764280154 * L_39 = ___message0;
		NullCheck(L_39);
		RuntimeObject* L_40 = OscPacket_get_Data_m65815223(L_39, /*hidden argument*/NULL);
		NullCheck(L_40);
		RuntimeObject * L_41 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t3230389896_il2cpp_TypeInfo_var, L_40, 8);
		V_8 = ((*(float*)((float*)UnBox(L_41, Single_t2076509932_il2cpp_TypeInfo_var))));
		OscMessage_t2764280154 * L_42 = ___message0;
		NullCheck(L_42);
		RuntimeObject* L_43 = OscPacket_get_Data_m65815223(L_42, /*hidden argument*/NULL);
		NullCheck(L_43);
		RuntimeObject * L_44 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t3230389896_il2cpp_TypeInfo_var, L_43, ((int32_t)9));
		V_9 = ((*(float*)((float*)UnBox(L_44, Single_t2076509932_il2cpp_TypeInfo_var))));
		OscMessage_t2764280154 * L_45 = ___message0;
		NullCheck(L_45);
		RuntimeObject* L_46 = OscPacket_get_Data_m65815223(L_45, /*hidden argument*/NULL);
		NullCheck(L_46);
		RuntimeObject * L_47 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t3230389896_il2cpp_TypeInfo_var, L_46, ((int32_t)10));
		V_10 = ((*(float*)((float*)UnBox(L_47, Single_t2076509932_il2cpp_TypeInfo_var))));
		OscMessage_t2764280154 * L_48 = ___message0;
		NullCheck(L_48);
		RuntimeObject* L_49 = OscPacket_get_Data_m65815223(L_48, /*hidden argument*/NULL);
		NullCheck(L_49);
		RuntimeObject * L_50 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t3230389896_il2cpp_TypeInfo_var, L_49, ((int32_t)11));
		V_11 = ((*(float*)((float*)UnBox(L_50, Single_t2076509932_il2cpp_TypeInfo_var))));
		OscMessage_t2764280154 * L_51 = ___message0;
		NullCheck(L_51);
		RuntimeObject* L_52 = OscPacket_get_Data_m65815223(L_51, /*hidden argument*/NULL);
		NullCheck(L_52);
		RuntimeObject * L_53 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t3230389896_il2cpp_TypeInfo_var, L_52, ((int32_t)12));
		V_12 = ((*(float*)((float*)UnBox(L_53, Single_t2076509932_il2cpp_TypeInfo_var))));
		Dictionary_2_t1054769049 * L_54 = __this->get_blobs_3();
		int32_t L_55 = V_1;
		NullCheck(L_54);
		bool L_56 = Dictionary_2_TryGetValue_m1913696641(L_54, L_55, (&V_13), /*hidden argument*/Dictionary_2_TryGetValue_m1913696641_RuntimeMethod_var);
		if (L_56)
		{
			goto IL_016c;
		}
	}
	{
		int32_t L_57 = V_1;
		TuioBlob_t2046943414 * L_58 = (TuioBlob_t2046943414 *)il2cpp_codegen_object_new(TuioBlob_t2046943414_il2cpp_TypeInfo_var);
		TuioBlob__ctor_m2840170451(L_58, L_57, /*hidden argument*/NULL);
		V_13 = L_58;
	}

IL_016c:
	{
		TuioBlob_t2046943414 * L_59 = V_13;
		float L_60 = V_2;
		float L_61 = V_3;
		float L_62 = V_4;
		float L_63 = V_5;
		float L_64 = V_6;
		float L_65 = V_7;
		float L_66 = V_8;
		float L_67 = V_9;
		float L_68 = V_10;
		float L_69 = V_11;
		float L_70 = V_12;
		NullCheck(L_59);
		TuioBlob_Update_m2006179440(L_59, L_60, L_61, L_62, L_63, L_64, L_65, L_66, L_67, L_68, L_69, L_70, /*hidden argument*/NULL);
		List_1_t1416064546 * L_71 = __this->get_updatedBlobs_4();
		TuioBlob_t2046943414 * L_72 = V_13;
		NullCheck(L_71);
		List_1_Add_m2273408866(L_71, L_72, /*hidden argument*/List_1_Add_m2273408866_RuntimeMethod_var);
		return;
	}

IL_0195:
	{
		OscMessage_t2764280154 * L_73 = ___message0;
		NullCheck(L_73);
		RuntimeObject* L_74 = OscPacket_get_Data_m65815223(L_73, /*hidden argument*/NULL);
		NullCheck(L_74);
		int32_t L_75 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, ICollection_1_t3641524600_il2cpp_TypeInfo_var, L_74);
		V_14 = L_75;
		V_15 = 1;
		goto IL_01ca;
	}

IL_01a7:
	{
		List_1_t1440998580 * L_76 = __this->get_addedBlobs_5();
		OscMessage_t2764280154 * L_77 = ___message0;
		NullCheck(L_77);
		RuntimeObject* L_78 = OscPacket_get_Data_m65815223(L_77, /*hidden argument*/NULL);
		int32_t L_79 = V_15;
		NullCheck(L_78);
		RuntimeObject * L_80 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t3230389896_il2cpp_TypeInfo_var, L_78, L_79);
		NullCheck(L_76);
		List_1_Add_m688682013(L_76, ((*(int32_t*)((int32_t*)UnBox(L_80, Int32_t2071877448_il2cpp_TypeInfo_var)))), /*hidden argument*/List_1_Add_m688682013_RuntimeMethod_var);
		int32_t L_81 = V_15;
		V_15 = ((int32_t)((int32_t)L_81+(int32_t)1));
	}

IL_01ca:
	{
		int32_t L_82 = V_15;
		int32_t L_83 = V_14;
		if ((((int32_t)L_82) < ((int32_t)L_83)))
		{
			goto IL_01a7;
		}
	}
	{
		Dictionary_2_t1054769049 * L_84 = __this->get_blobs_3();
		NullCheck(L_84);
		Enumerator_t2374793751  L_85 = Dictionary_2_GetEnumerator_m2501398850(L_84, /*hidden argument*/Dictionary_2_GetEnumerator_m2501398850_RuntimeMethod_var);
		V_23 = L_85;
	}

IL_01dd:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0221;
		}

IL_01df:
		{
			KeyValuePair_2_t3107081567  L_86 = Enumerator_get_Current_m3060082652((&V_23), /*hidden argument*/Enumerator_get_Current_m3060082652_RuntimeMethod_var);
			V_16 = L_86;
			List_1_t1440998580 * L_87 = __this->get_addedBlobs_5();
			int32_t L_88 = KeyValuePair_2_get_Key_m758296990((&V_16), /*hidden argument*/KeyValuePair_2_get_Key_m758296990_RuntimeMethod_var);
			NullCheck(L_87);
			bool L_89 = List_1_Contains_m3517981001(L_87, L_88, /*hidden argument*/List_1_Contains_m3517981001_RuntimeMethod_var);
			if (L_89)
			{
				goto IL_020e;
			}
		}

IL_01fc:
		{
			List_1_t1440998580 * L_90 = __this->get_removedBlobs_6();
			int32_t L_91 = KeyValuePair_2_get_Key_m758296990((&V_16), /*hidden argument*/KeyValuePair_2_get_Key_m758296990_RuntimeMethod_var);
			NullCheck(L_90);
			List_1_Add_m688682013(L_90, L_91, /*hidden argument*/List_1_Add_m688682013_RuntimeMethod_var);
		}

IL_020e:
		{
			List_1_t1440998580 * L_92 = __this->get_addedBlobs_5();
			int32_t L_93 = KeyValuePair_2_get_Key_m758296990((&V_16), /*hidden argument*/KeyValuePair_2_get_Key_m758296990_RuntimeMethod_var);
			NullCheck(L_92);
			List_1_Remove_m282822032(L_92, L_93, /*hidden argument*/List_1_Remove_m282822032_RuntimeMethod_var);
		}

IL_0221:
		{
			bool L_94 = Enumerator_MoveNext_m2555211001((&V_23), /*hidden argument*/Enumerator_MoveNext_m2555211001_RuntimeMethod_var);
			if (L_94)
			{
				goto IL_01df;
			}
		}

IL_022a:
		{
			IL2CPP_LEAVE(0x390, FINALLY_022f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_022f;
	}

FINALLY_022f:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m797473218((&V_23), /*hidden argument*/Enumerator_Dispose_m797473218_RuntimeMethod_var);
		IL2CPP_END_FINALLY(559)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(559)
	{
		IL2CPP_JUMP_TBL(0x390, IL_0390)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_023d:
	{
		OscMessage_t2764280154 * L_95 = ___message0;
		NullCheck(L_95);
		RuntimeObject* L_96 = OscPacket_get_Data_m65815223(L_95, /*hidden argument*/NULL);
		NullCheck(L_96);
		int32_t L_97 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, ICollection_1_t3641524600_il2cpp_TypeInfo_var, L_96);
		if ((((int32_t)L_97) >= ((int32_t)2)))
		{
			goto IL_024c;
		}
	}
	{
		return;
	}

IL_024c:
	{
		OscMessage_t2764280154 * L_98 = ___message0;
		NullCheck(L_98);
		RuntimeObject* L_99 = OscPacket_get_Data_m65815223(L_98, /*hidden argument*/NULL);
		NullCheck(L_99);
		RuntimeObject * L_100 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t3230389896_il2cpp_TypeInfo_var, L_99, 1);
		BlobProcessor_set_FrameNumber_m1366367337(__this, ((*(int32_t*)((int32_t*)UnBox(L_100, Int32_t2071877448_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		List_1_t1416064546 * L_101 = __this->get_updatedBlobs_4();
		NullCheck(L_101);
		int32_t L_102 = List_1_get_Count_m2267760198(L_101, /*hidden argument*/List_1_get_Count_m2267760198_RuntimeMethod_var);
		V_17 = L_102;
		V_18 = 0;
		goto IL_0301;
	}

IL_0278:
	{
		List_1_t1416064546 * L_103 = __this->get_updatedBlobs_4();
		int32_t L_104 = V_18;
		NullCheck(L_103);
		TuioBlob_t2046943414 * L_105 = List_1_get_Item_m3035119279(L_103, L_104, /*hidden argument*/List_1_get_Item_m3035119279_RuntimeMethod_var);
		V_19 = L_105;
		List_1_t1440998580 * L_106 = __this->get_addedBlobs_5();
		TuioBlob_t2046943414 * L_107 = V_19;
		NullCheck(L_107);
		int32_t L_108 = TuioEntity_get_Id_m4208226194(L_107, /*hidden argument*/NULL);
		NullCheck(L_106);
		bool L_109 = List_1_Contains_m3517981001(L_106, L_108, /*hidden argument*/List_1_Contains_m3517981001_RuntimeMethod_var);
		if (!L_109)
		{
			goto IL_02e0;
		}
	}
	{
		Dictionary_2_t1054769049 * L_110 = __this->get_blobs_3();
		TuioBlob_t2046943414 * L_111 = V_19;
		NullCheck(L_111);
		int32_t L_112 = TuioEntity_get_Id_m4208226194(L_111, /*hidden argument*/NULL);
		NullCheck(L_110);
		bool L_113 = Dictionary_2_ContainsKey_m3923242129(L_110, L_112, /*hidden argument*/Dictionary_2_ContainsKey_m3923242129_RuntimeMethod_var);
		if (L_113)
		{
			goto IL_02e0;
		}
	}
	{
		Dictionary_2_t1054769049 * L_114 = __this->get_blobs_3();
		TuioBlob_t2046943414 * L_115 = V_19;
		NullCheck(L_115);
		int32_t L_116 = TuioEntity_get_Id_m4208226194(L_115, /*hidden argument*/NULL);
		TuioBlob_t2046943414 * L_117 = V_19;
		NullCheck(L_114);
		Dictionary_2_Add_m4879272(L_114, L_116, L_117, /*hidden argument*/Dictionary_2_Add_m4879272_RuntimeMethod_var);
		EventHandler_1_t2154285351 * L_118 = __this->get_BlobAdded_0();
		if (!L_118)
		{
			goto IL_02fb;
		}
	}
	{
		EventHandler_1_t2154285351 * L_119 = __this->get_BlobAdded_0();
		TuioBlob_t2046943414 * L_120 = V_19;
		TuioBlobEventArgs_t3562978179 * L_121 = (TuioBlobEventArgs_t3562978179 *)il2cpp_codegen_object_new(TuioBlobEventArgs_t3562978179_il2cpp_TypeInfo_var);
		TuioBlobEventArgs__ctor_m3423126035(L_121, L_120, /*hidden argument*/NULL);
		NullCheck(L_119);
		EventHandler_1_Invoke_m897217542(L_119, __this, L_121, /*hidden argument*/EventHandler_1_Invoke_m897217542_RuntimeMethod_var);
		goto IL_02fb;
	}

IL_02e0:
	{
		EventHandler_1_t2154285351 * L_122 = __this->get_BlobUpdated_1();
		if (!L_122)
		{
			goto IL_02fb;
		}
	}
	{
		EventHandler_1_t2154285351 * L_123 = __this->get_BlobUpdated_1();
		TuioBlob_t2046943414 * L_124 = V_19;
		TuioBlobEventArgs_t3562978179 * L_125 = (TuioBlobEventArgs_t3562978179 *)il2cpp_codegen_object_new(TuioBlobEventArgs_t3562978179_il2cpp_TypeInfo_var);
		TuioBlobEventArgs__ctor_m3423126035(L_125, L_124, /*hidden argument*/NULL);
		NullCheck(L_123);
		EventHandler_1_Invoke_m897217542(L_123, __this, L_125, /*hidden argument*/EventHandler_1_Invoke_m897217542_RuntimeMethod_var);
	}

IL_02fb:
	{
		int32_t L_126 = V_18;
		V_18 = ((int32_t)((int32_t)L_126+(int32_t)1));
	}

IL_0301:
	{
		int32_t L_127 = V_18;
		int32_t L_128 = V_17;
		if ((((int32_t)L_127) < ((int32_t)L_128)))
		{
			goto IL_0278;
		}
	}
	{
		List_1_t1440998580 * L_129 = __this->get_removedBlobs_6();
		NullCheck(L_129);
		int32_t L_130 = List_1_get_Count_m3833405771(L_129, /*hidden argument*/List_1_get_Count_m3833405771_RuntimeMethod_var);
		V_17 = L_130;
		V_20 = 0;
		goto IL_0369;
	}

IL_031c:
	{
		List_1_t1440998580 * L_131 = __this->get_removedBlobs_6();
		int32_t L_132 = V_20;
		NullCheck(L_131);
		int32_t L_133 = List_1_get_Item_m3514532498(L_131, L_132, /*hidden argument*/List_1_get_Item_m3514532498_RuntimeMethod_var);
		V_21 = L_133;
		Dictionary_2_t1054769049 * L_134 = __this->get_blobs_3();
		int32_t L_135 = V_21;
		NullCheck(L_134);
		TuioBlob_t2046943414 * L_136 = Dictionary_2_get_Item_m3637178650(L_134, L_135, /*hidden argument*/Dictionary_2_get_Item_m3637178650_RuntimeMethod_var);
		V_13 = L_136;
		Dictionary_2_t1054769049 * L_137 = __this->get_blobs_3();
		int32_t L_138 = V_21;
		NullCheck(L_137);
		Dictionary_2_Remove_m3017945611(L_137, L_138, /*hidden argument*/Dictionary_2_Remove_m3017945611_RuntimeMethod_var);
		EventHandler_1_t2154285351 * L_139 = __this->get_BlobRemoved_2();
		if (!L_139)
		{
			goto IL_0363;
		}
	}
	{
		EventHandler_1_t2154285351 * L_140 = __this->get_BlobRemoved_2();
		TuioBlob_t2046943414 * L_141 = V_13;
		TuioBlobEventArgs_t3562978179 * L_142 = (TuioBlobEventArgs_t3562978179 *)il2cpp_codegen_object_new(TuioBlobEventArgs_t3562978179_il2cpp_TypeInfo_var);
		TuioBlobEventArgs__ctor_m3423126035(L_142, L_141, /*hidden argument*/NULL);
		NullCheck(L_140);
		EventHandler_1_Invoke_m897217542(L_140, __this, L_142, /*hidden argument*/EventHandler_1_Invoke_m897217542_RuntimeMethod_var);
	}

IL_0363:
	{
		int32_t L_143 = V_20;
		V_20 = ((int32_t)((int32_t)L_143+(int32_t)1));
	}

IL_0369:
	{
		int32_t L_144 = V_20;
		int32_t L_145 = V_17;
		if ((((int32_t)L_144) < ((int32_t)L_145)))
		{
			goto IL_031c;
		}
	}
	{
		List_1_t1440998580 * L_146 = __this->get_addedBlobs_5();
		NullCheck(L_146);
		List_1_Clear_m3896574426(L_146, /*hidden argument*/List_1_Clear_m3896574426_RuntimeMethod_var);
		List_1_t1440998580 * L_147 = __this->get_removedBlobs_6();
		NullCheck(L_147);
		List_1_Clear_m3896574426(L_147, /*hidden argument*/List_1_Clear_m3896574426_RuntimeMethod_var);
		List_1_t1416064546 * L_148 = __this->get_updatedBlobs_4();
		NullCheck(L_148);
		List_1_Clear_m1175215109(L_148, /*hidden argument*/List_1_Clear_m1175215109_RuntimeMethod_var);
	}

IL_0390:
	{
		return;
	}
}
// System.Void TUIOsharp.DataProcessors.BlobProcessor::.ctor()
extern "C"  void BlobProcessor__ctor_m1736416811 (BlobProcessor_t3603341577 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BlobProcessor__ctor_m1736416811_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t1054769049 * L_0 = (Dictionary_2_t1054769049 *)il2cpp_codegen_object_new(Dictionary_2_t1054769049_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m92522272(L_0, /*hidden argument*/Dictionary_2__ctor_m92522272_RuntimeMethod_var);
		__this->set_blobs_3(L_0);
		List_1_t1416064546 * L_1 = (List_1_t1416064546 *)il2cpp_codegen_object_new(List_1_t1416064546_il2cpp_TypeInfo_var);
		List_1__ctor_m1986803350(L_1, /*hidden argument*/List_1__ctor_m1986803350_RuntimeMethod_var);
		__this->set_updatedBlobs_4(L_1);
		List_1_t1440998580 * L_2 = (List_1_t1440998580 *)il2cpp_codegen_object_new(List_1_t1440998580_il2cpp_TypeInfo_var);
		List_1__ctor_m1598946593(L_2, /*hidden argument*/List_1__ctor_m1598946593_RuntimeMethod_var);
		__this->set_addedBlobs_5(L_2);
		List_1_t1440998580 * L_3 = (List_1_t1440998580 *)il2cpp_codegen_object_new(List_1_t1440998580_il2cpp_TypeInfo_var);
		List_1__ctor_m1598946593(L_3, /*hidden argument*/List_1__ctor_m1598946593_RuntimeMethod_var);
		__this->set_removedBlobs_6(L_3);
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TUIOsharp.DataProcessors.CursorProcessor::add_CursorAdded(System.EventHandler`1<TUIOsharp.DataProcessors.TuioCursorEventArgs>)
extern "C"  void CursorProcessor_add_CursorAdded_m3817569644 (CursorProcessor_t1785954004 * __this, EventHandler_1_t3249264480 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CursorProcessor_add_CursorAdded_m3817569644_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t3249264480 * V_0 = NULL;
	EventHandler_1_t3249264480 * V_1 = NULL;
	EventHandler_1_t3249264480 * V_2 = NULL;
	{
		EventHandler_1_t3249264480 * L_0 = __this->get_CursorAdded_0();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t3249264480 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t3249264480 * L_2 = V_1;
		EventHandler_1_t3249264480 * L_3 = ___value0;
		Delegate_t3022476291 * L_4 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((EventHandler_1_t3249264480 *)CastclassSealed((RuntimeObject*)L_4, EventHandler_1_t3249264480_il2cpp_TypeInfo_var));
		EventHandler_1_t3249264480 ** L_5 = __this->get_address_of_CursorAdded_0();
		EventHandler_1_t3249264480 * L_6 = V_2;
		EventHandler_1_t3249264480 * L_7 = V_1;
		EventHandler_1_t3249264480 * L_8 = InterlockedCompareExchangeImpl<EventHandler_1_t3249264480 *>(L_5, L_6, L_7);
		V_0 = L_8;
		EventHandler_1_t3249264480 * L_9 = V_0;
		EventHandler_1_t3249264480 * L_10 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_t3249264480 *)L_9) == ((RuntimeObject*)(EventHandler_1_t3249264480 *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void TUIOsharp.DataProcessors.CursorProcessor::remove_CursorAdded(System.EventHandler`1<TUIOsharp.DataProcessors.TuioCursorEventArgs>)
extern "C"  void CursorProcessor_remove_CursorAdded_m11359957 (CursorProcessor_t1785954004 * __this, EventHandler_1_t3249264480 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CursorProcessor_remove_CursorAdded_m11359957_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t3249264480 * V_0 = NULL;
	EventHandler_1_t3249264480 * V_1 = NULL;
	EventHandler_1_t3249264480 * V_2 = NULL;
	{
		EventHandler_1_t3249264480 * L_0 = __this->get_CursorAdded_0();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t3249264480 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t3249264480 * L_2 = V_1;
		EventHandler_1_t3249264480 * L_3 = ___value0;
		Delegate_t3022476291 * L_4 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((EventHandler_1_t3249264480 *)CastclassSealed((RuntimeObject*)L_4, EventHandler_1_t3249264480_il2cpp_TypeInfo_var));
		EventHandler_1_t3249264480 ** L_5 = __this->get_address_of_CursorAdded_0();
		EventHandler_1_t3249264480 * L_6 = V_2;
		EventHandler_1_t3249264480 * L_7 = V_1;
		EventHandler_1_t3249264480 * L_8 = InterlockedCompareExchangeImpl<EventHandler_1_t3249264480 *>(L_5, L_6, L_7);
		V_0 = L_8;
		EventHandler_1_t3249264480 * L_9 = V_0;
		EventHandler_1_t3249264480 * L_10 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_t3249264480 *)L_9) == ((RuntimeObject*)(EventHandler_1_t3249264480 *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void TUIOsharp.DataProcessors.CursorProcessor::add_CursorUpdated(System.EventHandler`1<TUIOsharp.DataProcessors.TuioCursorEventArgs>)
extern "C"  void CursorProcessor_add_CursorUpdated_m1967314603 (CursorProcessor_t1785954004 * __this, EventHandler_1_t3249264480 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CursorProcessor_add_CursorUpdated_m1967314603_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t3249264480 * V_0 = NULL;
	EventHandler_1_t3249264480 * V_1 = NULL;
	EventHandler_1_t3249264480 * V_2 = NULL;
	{
		EventHandler_1_t3249264480 * L_0 = __this->get_CursorUpdated_1();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t3249264480 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t3249264480 * L_2 = V_1;
		EventHandler_1_t3249264480 * L_3 = ___value0;
		Delegate_t3022476291 * L_4 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((EventHandler_1_t3249264480 *)CastclassSealed((RuntimeObject*)L_4, EventHandler_1_t3249264480_il2cpp_TypeInfo_var));
		EventHandler_1_t3249264480 ** L_5 = __this->get_address_of_CursorUpdated_1();
		EventHandler_1_t3249264480 * L_6 = V_2;
		EventHandler_1_t3249264480 * L_7 = V_1;
		EventHandler_1_t3249264480 * L_8 = InterlockedCompareExchangeImpl<EventHandler_1_t3249264480 *>(L_5, L_6, L_7);
		V_0 = L_8;
		EventHandler_1_t3249264480 * L_9 = V_0;
		EventHandler_1_t3249264480 * L_10 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_t3249264480 *)L_9) == ((RuntimeObject*)(EventHandler_1_t3249264480 *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void TUIOsharp.DataProcessors.CursorProcessor::remove_CursorUpdated(System.EventHandler`1<TUIOsharp.DataProcessors.TuioCursorEventArgs>)
extern "C"  void CursorProcessor_remove_CursorUpdated_m107114368 (CursorProcessor_t1785954004 * __this, EventHandler_1_t3249264480 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CursorProcessor_remove_CursorUpdated_m107114368_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t3249264480 * V_0 = NULL;
	EventHandler_1_t3249264480 * V_1 = NULL;
	EventHandler_1_t3249264480 * V_2 = NULL;
	{
		EventHandler_1_t3249264480 * L_0 = __this->get_CursorUpdated_1();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t3249264480 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t3249264480 * L_2 = V_1;
		EventHandler_1_t3249264480 * L_3 = ___value0;
		Delegate_t3022476291 * L_4 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((EventHandler_1_t3249264480 *)CastclassSealed((RuntimeObject*)L_4, EventHandler_1_t3249264480_il2cpp_TypeInfo_var));
		EventHandler_1_t3249264480 ** L_5 = __this->get_address_of_CursorUpdated_1();
		EventHandler_1_t3249264480 * L_6 = V_2;
		EventHandler_1_t3249264480 * L_7 = V_1;
		EventHandler_1_t3249264480 * L_8 = InterlockedCompareExchangeImpl<EventHandler_1_t3249264480 *>(L_5, L_6, L_7);
		V_0 = L_8;
		EventHandler_1_t3249264480 * L_9 = V_0;
		EventHandler_1_t3249264480 * L_10 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_t3249264480 *)L_9) == ((RuntimeObject*)(EventHandler_1_t3249264480 *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void TUIOsharp.DataProcessors.CursorProcessor::add_CursorRemoved(System.EventHandler`1<TUIOsharp.DataProcessors.TuioCursorEventArgs>)
extern "C"  void CursorProcessor_add_CursorRemoved_m4072586006 (CursorProcessor_t1785954004 * __this, EventHandler_1_t3249264480 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CursorProcessor_add_CursorRemoved_m4072586006_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t3249264480 * V_0 = NULL;
	EventHandler_1_t3249264480 * V_1 = NULL;
	EventHandler_1_t3249264480 * V_2 = NULL;
	{
		EventHandler_1_t3249264480 * L_0 = __this->get_CursorRemoved_2();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t3249264480 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t3249264480 * L_2 = V_1;
		EventHandler_1_t3249264480 * L_3 = ___value0;
		Delegate_t3022476291 * L_4 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((EventHandler_1_t3249264480 *)CastclassSealed((RuntimeObject*)L_4, EventHandler_1_t3249264480_il2cpp_TypeInfo_var));
		EventHandler_1_t3249264480 ** L_5 = __this->get_address_of_CursorRemoved_2();
		EventHandler_1_t3249264480 * L_6 = V_2;
		EventHandler_1_t3249264480 * L_7 = V_1;
		EventHandler_1_t3249264480 * L_8 = InterlockedCompareExchangeImpl<EventHandler_1_t3249264480 *>(L_5, L_6, L_7);
		V_0 = L_8;
		EventHandler_1_t3249264480 * L_9 = V_0;
		EventHandler_1_t3249264480 * L_10 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_t3249264480 *)L_9) == ((RuntimeObject*)(EventHandler_1_t3249264480 *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void TUIOsharp.DataProcessors.CursorProcessor::remove_CursorRemoved(System.EventHandler`1<TUIOsharp.DataProcessors.TuioCursorEventArgs>)
extern "C"  void CursorProcessor_remove_CursorRemoved_m2056441227 (CursorProcessor_t1785954004 * __this, EventHandler_1_t3249264480 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CursorProcessor_remove_CursorRemoved_m2056441227_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t3249264480 * V_0 = NULL;
	EventHandler_1_t3249264480 * V_1 = NULL;
	EventHandler_1_t3249264480 * V_2 = NULL;
	{
		EventHandler_1_t3249264480 * L_0 = __this->get_CursorRemoved_2();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t3249264480 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t3249264480 * L_2 = V_1;
		EventHandler_1_t3249264480 * L_3 = ___value0;
		Delegate_t3022476291 * L_4 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((EventHandler_1_t3249264480 *)CastclassSealed((RuntimeObject*)L_4, EventHandler_1_t3249264480_il2cpp_TypeInfo_var));
		EventHandler_1_t3249264480 ** L_5 = __this->get_address_of_CursorRemoved_2();
		EventHandler_1_t3249264480 * L_6 = V_2;
		EventHandler_1_t3249264480 * L_7 = V_1;
		EventHandler_1_t3249264480 * L_8 = InterlockedCompareExchangeImpl<EventHandler_1_t3249264480 *>(L_5, L_6, L_7);
		V_0 = L_8;
		EventHandler_1_t3249264480 * L_9 = V_0;
		EventHandler_1_t3249264480 * L_10 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_t3249264480 *)L_9) == ((RuntimeObject*)(EventHandler_1_t3249264480 *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void TUIOsharp.DataProcessors.CursorProcessor::set_FrameNumber(System.Int32)
extern "C"  void CursorProcessor_set_FrameNumber_m78169520 (CursorProcessor_t1785954004 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CFrameNumberU3Ek__BackingField_7(L_0);
		return;
	}
}
// System.Void TUIOsharp.DataProcessors.CursorProcessor::ProcessMessage(OSCsharp.Data.OscMessage)
extern "C"  void CursorProcessor_ProcessMessage_m823963401 (CursorProcessor_t1785954004 * __this, OscMessage_t2764280154 * ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CursorProcessor_ProcessMessage_m823963401_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	TuioCursor_t1850351419 * V_7 = NULL;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	KeyValuePair_2_t2910489572  V_10;
	memset(&V_10, 0, sizeof(V_10));
	int32_t V_11 = 0;
	int32_t V_12 = 0;
	TuioCursor_t1850351419 * V_13 = NULL;
	int32_t V_14 = 0;
	int32_t V_15 = 0;
	String_t* V_16 = NULL;
	Enumerator_t2178201756  V_17;
	memset(&V_17, 0, sizeof(V_17));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		OscMessage_t2764280154 * L_0 = ___message0;
		NullCheck(L_0);
		String_t* L_1 = OscPacket_get_Address_m1502094218(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_1, _stringLiteral262980497, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0013;
		}
	}
	{
		return;
	}

IL_0013:
	{
		OscMessage_t2764280154 * L_3 = ___message0;
		NullCheck(L_3);
		RuntimeObject* L_4 = OscPacket_get_Data_m65815223(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		RuntimeObject * L_5 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t3230389896_il2cpp_TypeInfo_var, L_4, 0);
		NullCheck(L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_5);
		V_0 = L_6;
		String_t* L_7 = V_0;
		String_t* L_8 = L_7;
		V_16 = L_8;
		if (!L_8)
		{
			goto IL_030d;
		}
	}
	{
		String_t* L_9 = V_16;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_10 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_9, _stringLiteral3021628834, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_005f;
		}
	}
	{
		String_t* L_11 = V_16;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_12 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_11, _stringLiteral2273848675, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_0112;
		}
	}
	{
		String_t* L_13 = V_16;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_14 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_13, _stringLiteral1851287145, /*hidden argument*/NULL);
		if (L_14)
		{
			goto IL_01ba;
		}
	}
	{
		return;
	}

IL_005f:
	{
		OscMessage_t2764280154 * L_15 = ___message0;
		NullCheck(L_15);
		RuntimeObject* L_16 = OscPacket_get_Data_m65815223(L_15, /*hidden argument*/NULL);
		NullCheck(L_16);
		int32_t L_17 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, ICollection_1_t3641524600_il2cpp_TypeInfo_var, L_16);
		if ((((int32_t)L_17) >= ((int32_t)7)))
		{
			goto IL_006e;
		}
	}
	{
		return;
	}

IL_006e:
	{
		OscMessage_t2764280154 * L_18 = ___message0;
		NullCheck(L_18);
		RuntimeObject* L_19 = OscPacket_get_Data_m65815223(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		RuntimeObject * L_20 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t3230389896_il2cpp_TypeInfo_var, L_19, 1);
		V_1 = ((*(int32_t*)((int32_t*)UnBox(L_20, Int32_t2071877448_il2cpp_TypeInfo_var))));
		OscMessage_t2764280154 * L_21 = ___message0;
		NullCheck(L_21);
		RuntimeObject* L_22 = OscPacket_get_Data_m65815223(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		RuntimeObject * L_23 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t3230389896_il2cpp_TypeInfo_var, L_22, 2);
		V_2 = ((*(float*)((float*)UnBox(L_23, Single_t2076509932_il2cpp_TypeInfo_var))));
		OscMessage_t2764280154 * L_24 = ___message0;
		NullCheck(L_24);
		RuntimeObject* L_25 = OscPacket_get_Data_m65815223(L_24, /*hidden argument*/NULL);
		NullCheck(L_25);
		RuntimeObject * L_26 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t3230389896_il2cpp_TypeInfo_var, L_25, 3);
		V_3 = ((*(float*)((float*)UnBox(L_26, Single_t2076509932_il2cpp_TypeInfo_var))));
		OscMessage_t2764280154 * L_27 = ___message0;
		NullCheck(L_27);
		RuntimeObject* L_28 = OscPacket_get_Data_m65815223(L_27, /*hidden argument*/NULL);
		NullCheck(L_28);
		RuntimeObject * L_29 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t3230389896_il2cpp_TypeInfo_var, L_28, 4);
		V_4 = ((*(float*)((float*)UnBox(L_29, Single_t2076509932_il2cpp_TypeInfo_var))));
		OscMessage_t2764280154 * L_30 = ___message0;
		NullCheck(L_30);
		RuntimeObject* L_31 = OscPacket_get_Data_m65815223(L_30, /*hidden argument*/NULL);
		NullCheck(L_31);
		RuntimeObject * L_32 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t3230389896_il2cpp_TypeInfo_var, L_31, 5);
		V_5 = ((*(float*)((float*)UnBox(L_32, Single_t2076509932_il2cpp_TypeInfo_var))));
		OscMessage_t2764280154 * L_33 = ___message0;
		NullCheck(L_33);
		RuntimeObject* L_34 = OscPacket_get_Data_m65815223(L_33, /*hidden argument*/NULL);
		NullCheck(L_34);
		RuntimeObject * L_35 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t3230389896_il2cpp_TypeInfo_var, L_34, 6);
		V_6 = ((*(float*)((float*)UnBox(L_35, Single_t2076509932_il2cpp_TypeInfo_var))));
		Dictionary_2_t858177054 * L_36 = __this->get_cursors_3();
		int32_t L_37 = V_1;
		NullCheck(L_36);
		bool L_38 = Dictionary_2_TryGetValue_m1936579402(L_36, L_37, (&V_7), /*hidden argument*/Dictionary_2_TryGetValue_m1936579402_RuntimeMethod_var);
		if (L_38)
		{
			goto IL_00f5;
		}
	}
	{
		int32_t L_39 = V_1;
		TuioCursor_t1850351419 * L_40 = (TuioCursor_t1850351419 *)il2cpp_codegen_object_new(TuioCursor_t1850351419_il2cpp_TypeInfo_var);
		TuioCursor__ctor_m2689165534(L_40, L_39, /*hidden argument*/NULL);
		V_7 = L_40;
	}

IL_00f5:
	{
		TuioCursor_t1850351419 * L_41 = V_7;
		float L_42 = V_2;
		float L_43 = V_3;
		float L_44 = V_4;
		float L_45 = V_5;
		float L_46 = V_6;
		NullCheck(L_41);
		TuioCursor_Update_m650004219(L_41, L_42, L_43, L_44, L_45, L_46, /*hidden argument*/NULL);
		List_1_t1219472551 * L_47 = __this->get_updatedCursors_4();
		TuioCursor_t1850351419 * L_48 = V_7;
		NullCheck(L_47);
		List_1_Add_m2438782433(L_47, L_48, /*hidden argument*/List_1_Add_m2438782433_RuntimeMethod_var);
		return;
	}

IL_0112:
	{
		OscMessage_t2764280154 * L_49 = ___message0;
		NullCheck(L_49);
		RuntimeObject* L_50 = OscPacket_get_Data_m65815223(L_49, /*hidden argument*/NULL);
		NullCheck(L_50);
		int32_t L_51 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, ICollection_1_t3641524600_il2cpp_TypeInfo_var, L_50);
		V_8 = L_51;
		V_9 = 1;
		goto IL_0147;
	}

IL_0124:
	{
		List_1_t1440998580 * L_52 = __this->get_addedCursors_5();
		OscMessage_t2764280154 * L_53 = ___message0;
		NullCheck(L_53);
		RuntimeObject* L_54 = OscPacket_get_Data_m65815223(L_53, /*hidden argument*/NULL);
		int32_t L_55 = V_9;
		NullCheck(L_54);
		RuntimeObject * L_56 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t3230389896_il2cpp_TypeInfo_var, L_54, L_55);
		NullCheck(L_52);
		List_1_Add_m688682013(L_52, ((*(int32_t*)((int32_t*)UnBox(L_56, Int32_t2071877448_il2cpp_TypeInfo_var)))), /*hidden argument*/List_1_Add_m688682013_RuntimeMethod_var);
		int32_t L_57 = V_9;
		V_9 = ((int32_t)((int32_t)L_57+(int32_t)1));
	}

IL_0147:
	{
		int32_t L_58 = V_9;
		int32_t L_59 = V_8;
		if ((((int32_t)L_58) < ((int32_t)L_59)))
		{
			goto IL_0124;
		}
	}
	{
		Dictionary_2_t858177054 * L_60 = __this->get_cursors_3();
		NullCheck(L_60);
		Enumerator_t2178201756  L_61 = Dictionary_2_GetEnumerator_m775930629(L_60, /*hidden argument*/Dictionary_2_GetEnumerator_m775930629_RuntimeMethod_var);
		V_17 = L_61;
	}

IL_015a:
	try
	{ // begin try (depth: 1)
		{
			goto IL_019e;
		}

IL_015c:
		{
			KeyValuePair_2_t2910489572  L_62 = Enumerator_get_Current_m2497618057((&V_17), /*hidden argument*/Enumerator_get_Current_m2497618057_RuntimeMethod_var);
			V_10 = L_62;
			List_1_t1440998580 * L_63 = __this->get_addedCursors_5();
			int32_t L_64 = KeyValuePair_2_get_Key_m624710547((&V_10), /*hidden argument*/KeyValuePair_2_get_Key_m624710547_RuntimeMethod_var);
			NullCheck(L_63);
			bool L_65 = List_1_Contains_m3517981001(L_63, L_64, /*hidden argument*/List_1_Contains_m3517981001_RuntimeMethod_var);
			if (L_65)
			{
				goto IL_018b;
			}
		}

IL_0179:
		{
			List_1_t1440998580 * L_66 = __this->get_removedCursors_6();
			int32_t L_67 = KeyValuePair_2_get_Key_m624710547((&V_10), /*hidden argument*/KeyValuePair_2_get_Key_m624710547_RuntimeMethod_var);
			NullCheck(L_66);
			List_1_Add_m688682013(L_66, L_67, /*hidden argument*/List_1_Add_m688682013_RuntimeMethod_var);
		}

IL_018b:
		{
			List_1_t1440998580 * L_68 = __this->get_addedCursors_5();
			int32_t L_69 = KeyValuePair_2_get_Key_m624710547((&V_10), /*hidden argument*/KeyValuePair_2_get_Key_m624710547_RuntimeMethod_var);
			NullCheck(L_68);
			List_1_Remove_m282822032(L_68, L_69, /*hidden argument*/List_1_Remove_m282822032_RuntimeMethod_var);
		}

IL_019e:
		{
			bool L_70 = Enumerator_MoveNext_m3093636814((&V_17), /*hidden argument*/Enumerator_MoveNext_m3093636814_RuntimeMethod_var);
			if (L_70)
			{
				goto IL_015c;
			}
		}

IL_01a7:
		{
			IL2CPP_LEAVE(0x30D, FINALLY_01ac);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_01ac;
	}

FINALLY_01ac:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m4213942773((&V_17), /*hidden argument*/Enumerator_Dispose_m4213942773_RuntimeMethod_var);
		IL2CPP_END_FINALLY(428)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(428)
	{
		IL2CPP_JUMP_TBL(0x30D, IL_030d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_01ba:
	{
		OscMessage_t2764280154 * L_71 = ___message0;
		NullCheck(L_71);
		RuntimeObject* L_72 = OscPacket_get_Data_m65815223(L_71, /*hidden argument*/NULL);
		NullCheck(L_72);
		int32_t L_73 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, ICollection_1_t3641524600_il2cpp_TypeInfo_var, L_72);
		if ((((int32_t)L_73) >= ((int32_t)2)))
		{
			goto IL_01c9;
		}
	}
	{
		return;
	}

IL_01c9:
	{
		OscMessage_t2764280154 * L_74 = ___message0;
		NullCheck(L_74);
		RuntimeObject* L_75 = OscPacket_get_Data_m65815223(L_74, /*hidden argument*/NULL);
		NullCheck(L_75);
		RuntimeObject * L_76 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t3230389896_il2cpp_TypeInfo_var, L_75, 1);
		CursorProcessor_set_FrameNumber_m78169520(__this, ((*(int32_t*)((int32_t*)UnBox(L_76, Int32_t2071877448_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		List_1_t1219472551 * L_77 = __this->get_updatedCursors_4();
		NullCheck(L_77);
		int32_t L_78 = List_1_get_Count_m302198121(L_77, /*hidden argument*/List_1_get_Count_m302198121_RuntimeMethod_var);
		V_11 = L_78;
		V_12 = 0;
		goto IL_027e;
	}

IL_01f5:
	{
		List_1_t1219472551 * L_79 = __this->get_updatedCursors_4();
		int32_t L_80 = V_12;
		NullCheck(L_79);
		TuioCursor_t1850351419 * L_81 = List_1_get_Item_m2568686926(L_79, L_80, /*hidden argument*/List_1_get_Item_m2568686926_RuntimeMethod_var);
		V_13 = L_81;
		List_1_t1440998580 * L_82 = __this->get_addedCursors_5();
		TuioCursor_t1850351419 * L_83 = V_13;
		NullCheck(L_83);
		int32_t L_84 = TuioEntity_get_Id_m4208226194(L_83, /*hidden argument*/NULL);
		NullCheck(L_82);
		bool L_85 = List_1_Contains_m3517981001(L_82, L_84, /*hidden argument*/List_1_Contains_m3517981001_RuntimeMethod_var);
		if (!L_85)
		{
			goto IL_025d;
		}
	}
	{
		Dictionary_2_t858177054 * L_86 = __this->get_cursors_3();
		TuioCursor_t1850351419 * L_87 = V_13;
		NullCheck(L_87);
		int32_t L_88 = TuioEntity_get_Id_m4208226194(L_87, /*hidden argument*/NULL);
		NullCheck(L_86);
		bool L_89 = Dictionary_2_ContainsKey_m2357860370(L_86, L_88, /*hidden argument*/Dictionary_2_ContainsKey_m2357860370_RuntimeMethod_var);
		if (L_89)
		{
			goto IL_025d;
		}
	}
	{
		Dictionary_2_t858177054 * L_90 = __this->get_cursors_3();
		TuioCursor_t1850351419 * L_91 = V_13;
		NullCheck(L_91);
		int32_t L_92 = TuioEntity_get_Id_m4208226194(L_91, /*hidden argument*/NULL);
		TuioCursor_t1850351419 * L_93 = V_13;
		NullCheck(L_90);
		Dictionary_2_Add_m1095222709(L_90, L_92, L_93, /*hidden argument*/Dictionary_2_Add_m1095222709_RuntimeMethod_var);
		EventHandler_1_t3249264480 * L_94 = __this->get_CursorAdded_0();
		if (!L_94)
		{
			goto IL_0278;
		}
	}
	{
		EventHandler_1_t3249264480 * L_95 = __this->get_CursorAdded_0();
		TuioCursor_t1850351419 * L_96 = V_13;
		TuioCursorEventArgs_t362990012 * L_97 = (TuioCursorEventArgs_t362990012 *)il2cpp_codegen_object_new(TuioCursorEventArgs_t362990012_il2cpp_TypeInfo_var);
		TuioCursorEventArgs__ctor_m2169915347(L_97, L_96, /*hidden argument*/NULL);
		NullCheck(L_95);
		EventHandler_1_Invoke_m2962849243(L_95, __this, L_97, /*hidden argument*/EventHandler_1_Invoke_m2962849243_RuntimeMethod_var);
		goto IL_0278;
	}

IL_025d:
	{
		EventHandler_1_t3249264480 * L_98 = __this->get_CursorUpdated_1();
		if (!L_98)
		{
			goto IL_0278;
		}
	}
	{
		EventHandler_1_t3249264480 * L_99 = __this->get_CursorUpdated_1();
		TuioCursor_t1850351419 * L_100 = V_13;
		TuioCursorEventArgs_t362990012 * L_101 = (TuioCursorEventArgs_t362990012 *)il2cpp_codegen_object_new(TuioCursorEventArgs_t362990012_il2cpp_TypeInfo_var);
		TuioCursorEventArgs__ctor_m2169915347(L_101, L_100, /*hidden argument*/NULL);
		NullCheck(L_99);
		EventHandler_1_Invoke_m2962849243(L_99, __this, L_101, /*hidden argument*/EventHandler_1_Invoke_m2962849243_RuntimeMethod_var);
	}

IL_0278:
	{
		int32_t L_102 = V_12;
		V_12 = ((int32_t)((int32_t)L_102+(int32_t)1));
	}

IL_027e:
	{
		int32_t L_103 = V_12;
		int32_t L_104 = V_11;
		if ((((int32_t)L_103) < ((int32_t)L_104)))
		{
			goto IL_01f5;
		}
	}
	{
		List_1_t1440998580 * L_105 = __this->get_removedCursors_6();
		NullCheck(L_105);
		int32_t L_106 = List_1_get_Count_m3833405771(L_105, /*hidden argument*/List_1_get_Count_m3833405771_RuntimeMethod_var);
		V_11 = L_106;
		V_14 = 0;
		goto IL_02e6;
	}

IL_0299:
	{
		List_1_t1440998580 * L_107 = __this->get_removedCursors_6();
		int32_t L_108 = V_14;
		NullCheck(L_107);
		int32_t L_109 = List_1_get_Item_m3514532498(L_107, L_108, /*hidden argument*/List_1_get_Item_m3514532498_RuntimeMethod_var);
		V_15 = L_109;
		Dictionary_2_t858177054 * L_110 = __this->get_cursors_3();
		int32_t L_111 = V_15;
		NullCheck(L_110);
		TuioCursor_t1850351419 * L_112 = Dictionary_2_get_Item_m866734463(L_110, L_111, /*hidden argument*/Dictionary_2_get_Item_m866734463_RuntimeMethod_var);
		V_7 = L_112;
		Dictionary_2_t858177054 * L_113 = __this->get_cursors_3();
		int32_t L_114 = V_15;
		NullCheck(L_113);
		Dictionary_2_Remove_m2712103718(L_113, L_114, /*hidden argument*/Dictionary_2_Remove_m2712103718_RuntimeMethod_var);
		EventHandler_1_t3249264480 * L_115 = __this->get_CursorRemoved_2();
		if (!L_115)
		{
			goto IL_02e0;
		}
	}
	{
		EventHandler_1_t3249264480 * L_116 = __this->get_CursorRemoved_2();
		TuioCursor_t1850351419 * L_117 = V_7;
		TuioCursorEventArgs_t362990012 * L_118 = (TuioCursorEventArgs_t362990012 *)il2cpp_codegen_object_new(TuioCursorEventArgs_t362990012_il2cpp_TypeInfo_var);
		TuioCursorEventArgs__ctor_m2169915347(L_118, L_117, /*hidden argument*/NULL);
		NullCheck(L_116);
		EventHandler_1_Invoke_m2962849243(L_116, __this, L_118, /*hidden argument*/EventHandler_1_Invoke_m2962849243_RuntimeMethod_var);
	}

IL_02e0:
	{
		int32_t L_119 = V_14;
		V_14 = ((int32_t)((int32_t)L_119+(int32_t)1));
	}

IL_02e6:
	{
		int32_t L_120 = V_14;
		int32_t L_121 = V_11;
		if ((((int32_t)L_120) < ((int32_t)L_121)))
		{
			goto IL_0299;
		}
	}
	{
		List_1_t1440998580 * L_122 = __this->get_addedCursors_5();
		NullCheck(L_122);
		List_1_Clear_m3896574426(L_122, /*hidden argument*/List_1_Clear_m3896574426_RuntimeMethod_var);
		List_1_t1440998580 * L_123 = __this->get_removedCursors_6();
		NullCheck(L_123);
		List_1_Clear_m3896574426(L_123, /*hidden argument*/List_1_Clear_m3896574426_RuntimeMethod_var);
		List_1_t1219472551 * L_124 = __this->get_updatedCursors_4();
		NullCheck(L_124);
		List_1_Clear_m2159038286(L_124, /*hidden argument*/List_1_Clear_m2159038286_RuntimeMethod_var);
	}

IL_030d:
	{
		return;
	}
}
// System.Void TUIOsharp.DataProcessors.CursorProcessor::.ctor()
extern "C"  void CursorProcessor__ctor_m3237265030 (CursorProcessor_t1785954004 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CursorProcessor__ctor_m3237265030_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t858177054 * L_0 = (Dictionary_2_t858177054 *)il2cpp_codegen_object_new(Dictionary_2_t858177054_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1652351677(L_0, /*hidden argument*/Dictionary_2__ctor_m1652351677_RuntimeMethod_var);
		__this->set_cursors_3(L_0);
		List_1_t1219472551 * L_1 = (List_1_t1219472551 *)il2cpp_codegen_object_new(List_1_t1219472551_il2cpp_TypeInfo_var);
		List_1__ctor_m3131880517(L_1, /*hidden argument*/List_1__ctor_m3131880517_RuntimeMethod_var);
		__this->set_updatedCursors_4(L_1);
		List_1_t1440998580 * L_2 = (List_1_t1440998580 *)il2cpp_codegen_object_new(List_1_t1440998580_il2cpp_TypeInfo_var);
		List_1__ctor_m1598946593(L_2, /*hidden argument*/List_1__ctor_m1598946593_RuntimeMethod_var);
		__this->set_addedCursors_5(L_2);
		List_1_t1440998580 * L_3 = (List_1_t1440998580 *)il2cpp_codegen_object_new(List_1_t1440998580_il2cpp_TypeInfo_var);
		List_1__ctor_m1598946593(L_3, /*hidden argument*/List_1__ctor_m1598946593_RuntimeMethod_var);
		__this->set_removedCursors_6(L_3);
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TUIOsharp.DataProcessors.ObjectProcessor::add_ObjectAdded(System.EventHandler`1<TUIOsharp.DataProcessors.TuioObjectEventArgs>)
extern "C"  void ObjectProcessor_add_ObjectAdded_m4092744429 (ObjectProcessor_t221569383 * __this, EventHandler_1_t402050513 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ObjectProcessor_add_ObjectAdded_m4092744429_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t402050513 * V_0 = NULL;
	EventHandler_1_t402050513 * V_1 = NULL;
	EventHandler_1_t402050513 * V_2 = NULL;
	{
		EventHandler_1_t402050513 * L_0 = __this->get_ObjectAdded_0();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t402050513 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t402050513 * L_2 = V_1;
		EventHandler_1_t402050513 * L_3 = ___value0;
		Delegate_t3022476291 * L_4 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((EventHandler_1_t402050513 *)CastclassSealed((RuntimeObject*)L_4, EventHandler_1_t402050513_il2cpp_TypeInfo_var));
		EventHandler_1_t402050513 ** L_5 = __this->get_address_of_ObjectAdded_0();
		EventHandler_1_t402050513 * L_6 = V_2;
		EventHandler_1_t402050513 * L_7 = V_1;
		EventHandler_1_t402050513 * L_8 = InterlockedCompareExchangeImpl<EventHandler_1_t402050513 *>(L_5, L_6, L_7);
		V_0 = L_8;
		EventHandler_1_t402050513 * L_9 = V_0;
		EventHandler_1_t402050513 * L_10 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_t402050513 *)L_9) == ((RuntimeObject*)(EventHandler_1_t402050513 *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void TUIOsharp.DataProcessors.ObjectProcessor::remove_ObjectAdded(System.EventHandler`1<TUIOsharp.DataProcessors.TuioObjectEventArgs>)
extern "C"  void ObjectProcessor_remove_ObjectAdded_m3070580308 (ObjectProcessor_t221569383 * __this, EventHandler_1_t402050513 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ObjectProcessor_remove_ObjectAdded_m3070580308_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t402050513 * V_0 = NULL;
	EventHandler_1_t402050513 * V_1 = NULL;
	EventHandler_1_t402050513 * V_2 = NULL;
	{
		EventHandler_1_t402050513 * L_0 = __this->get_ObjectAdded_0();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t402050513 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t402050513 * L_2 = V_1;
		EventHandler_1_t402050513 * L_3 = ___value0;
		Delegate_t3022476291 * L_4 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((EventHandler_1_t402050513 *)CastclassSealed((RuntimeObject*)L_4, EventHandler_1_t402050513_il2cpp_TypeInfo_var));
		EventHandler_1_t402050513 ** L_5 = __this->get_address_of_ObjectAdded_0();
		EventHandler_1_t402050513 * L_6 = V_2;
		EventHandler_1_t402050513 * L_7 = V_1;
		EventHandler_1_t402050513 * L_8 = InterlockedCompareExchangeImpl<EventHandler_1_t402050513 *>(L_5, L_6, L_7);
		V_0 = L_8;
		EventHandler_1_t402050513 * L_9 = V_0;
		EventHandler_1_t402050513 * L_10 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_t402050513 *)L_9) == ((RuntimeObject*)(EventHandler_1_t402050513 *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void TUIOsharp.DataProcessors.ObjectProcessor::add_ObjectUpdated(System.EventHandler`1<TUIOsharp.DataProcessors.TuioObjectEventArgs>)
extern "C"  void ObjectProcessor_add_ObjectUpdated_m2549856334 (ObjectProcessor_t221569383 * __this, EventHandler_1_t402050513 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ObjectProcessor_add_ObjectUpdated_m2549856334_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t402050513 * V_0 = NULL;
	EventHandler_1_t402050513 * V_1 = NULL;
	EventHandler_1_t402050513 * V_2 = NULL;
	{
		EventHandler_1_t402050513 * L_0 = __this->get_ObjectUpdated_1();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t402050513 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t402050513 * L_2 = V_1;
		EventHandler_1_t402050513 * L_3 = ___value0;
		Delegate_t3022476291 * L_4 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((EventHandler_1_t402050513 *)CastclassSealed((RuntimeObject*)L_4, EventHandler_1_t402050513_il2cpp_TypeInfo_var));
		EventHandler_1_t402050513 ** L_5 = __this->get_address_of_ObjectUpdated_1();
		EventHandler_1_t402050513 * L_6 = V_2;
		EventHandler_1_t402050513 * L_7 = V_1;
		EventHandler_1_t402050513 * L_8 = InterlockedCompareExchangeImpl<EventHandler_1_t402050513 *>(L_5, L_6, L_7);
		V_0 = L_8;
		EventHandler_1_t402050513 * L_9 = V_0;
		EventHandler_1_t402050513 * L_10 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_t402050513 *)L_9) == ((RuntimeObject*)(EventHandler_1_t402050513 *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void TUIOsharp.DataProcessors.ObjectProcessor::remove_ObjectUpdated(System.EventHandler`1<TUIOsharp.DataProcessors.TuioObjectEventArgs>)
extern "C"  void ObjectProcessor_remove_ObjectUpdated_m3226679647 (ObjectProcessor_t221569383 * __this, EventHandler_1_t402050513 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ObjectProcessor_remove_ObjectUpdated_m3226679647_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t402050513 * V_0 = NULL;
	EventHandler_1_t402050513 * V_1 = NULL;
	EventHandler_1_t402050513 * V_2 = NULL;
	{
		EventHandler_1_t402050513 * L_0 = __this->get_ObjectUpdated_1();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t402050513 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t402050513 * L_2 = V_1;
		EventHandler_1_t402050513 * L_3 = ___value0;
		Delegate_t3022476291 * L_4 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((EventHandler_1_t402050513 *)CastclassSealed((RuntimeObject*)L_4, EventHandler_1_t402050513_il2cpp_TypeInfo_var));
		EventHandler_1_t402050513 ** L_5 = __this->get_address_of_ObjectUpdated_1();
		EventHandler_1_t402050513 * L_6 = V_2;
		EventHandler_1_t402050513 * L_7 = V_1;
		EventHandler_1_t402050513 * L_8 = InterlockedCompareExchangeImpl<EventHandler_1_t402050513 *>(L_5, L_6, L_7);
		V_0 = L_8;
		EventHandler_1_t402050513 * L_9 = V_0;
		EventHandler_1_t402050513 * L_10 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_t402050513 *)L_9) == ((RuntimeObject*)(EventHandler_1_t402050513 *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void TUIOsharp.DataProcessors.ObjectProcessor::add_ObjectRemoved(System.EventHandler`1<TUIOsharp.DataProcessors.TuioObjectEventArgs>)
extern "C"  void ObjectProcessor_add_ObjectRemoved_m3544020203 (ObjectProcessor_t221569383 * __this, EventHandler_1_t402050513 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ObjectProcessor_add_ObjectRemoved_m3544020203_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t402050513 * V_0 = NULL;
	EventHandler_1_t402050513 * V_1 = NULL;
	EventHandler_1_t402050513 * V_2 = NULL;
	{
		EventHandler_1_t402050513 * L_0 = __this->get_ObjectRemoved_2();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t402050513 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t402050513 * L_2 = V_1;
		EventHandler_1_t402050513 * L_3 = ___value0;
		Delegate_t3022476291 * L_4 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((EventHandler_1_t402050513 *)CastclassSealed((RuntimeObject*)L_4, EventHandler_1_t402050513_il2cpp_TypeInfo_var));
		EventHandler_1_t402050513 ** L_5 = __this->get_address_of_ObjectRemoved_2();
		EventHandler_1_t402050513 * L_6 = V_2;
		EventHandler_1_t402050513 * L_7 = V_1;
		EventHandler_1_t402050513 * L_8 = InterlockedCompareExchangeImpl<EventHandler_1_t402050513 *>(L_5, L_6, L_7);
		V_0 = L_8;
		EventHandler_1_t402050513 * L_9 = V_0;
		EventHandler_1_t402050513 * L_10 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_t402050513 *)L_9) == ((RuntimeObject*)(EventHandler_1_t402050513 *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void TUIOsharp.DataProcessors.ObjectProcessor::remove_ObjectRemoved(System.EventHandler`1<TUIOsharp.DataProcessors.TuioObjectEventArgs>)
extern "C"  void ObjectProcessor_remove_ObjectRemoved_m2344088226 (ObjectProcessor_t221569383 * __this, EventHandler_1_t402050513 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ObjectProcessor_remove_ObjectRemoved_m2344088226_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_1_t402050513 * V_0 = NULL;
	EventHandler_1_t402050513 * V_1 = NULL;
	EventHandler_1_t402050513 * V_2 = NULL;
	{
		EventHandler_1_t402050513 * L_0 = __this->get_ObjectRemoved_2();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_1_t402050513 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_1_t402050513 * L_2 = V_1;
		EventHandler_1_t402050513 * L_3 = ___value0;
		Delegate_t3022476291 * L_4 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((EventHandler_1_t402050513 *)CastclassSealed((RuntimeObject*)L_4, EventHandler_1_t402050513_il2cpp_TypeInfo_var));
		EventHandler_1_t402050513 ** L_5 = __this->get_address_of_ObjectRemoved_2();
		EventHandler_1_t402050513 * L_6 = V_2;
		EventHandler_1_t402050513 * L_7 = V_1;
		EventHandler_1_t402050513 * L_8 = InterlockedCompareExchangeImpl<EventHandler_1_t402050513 *>(L_5, L_6, L_7);
		V_0 = L_8;
		EventHandler_1_t402050513 * L_9 = V_0;
		EventHandler_1_t402050513 * L_10 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_1_t402050513 *)L_9) == ((RuntimeObject*)(EventHandler_1_t402050513 *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void TUIOsharp.DataProcessors.ObjectProcessor::set_FrameNumber(System.Int32)
extern "C"  void ObjectProcessor_set_FrameNumber_m3936044951 (ObjectProcessor_t221569383 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CFrameNumberU3Ek__BackingField_7(L_0);
		return;
	}
}
// System.Void TUIOsharp.DataProcessors.ObjectProcessor::ProcessMessage(OSCsharp.Data.OscMessage)
extern "C"  void ObjectProcessor_ProcessMessage_m1510623726 (ObjectProcessor_t221569383 * __this, OscMessage_t2764280154 * ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ObjectProcessor_ProcessMessage_m1510623726_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	float V_9 = 0.0f;
	float V_10 = 0.0f;
	TuioObject_t1236821014 * V_11 = NULL;
	int32_t V_12 = 0;
	int32_t V_13 = 0;
	KeyValuePair_2_t2296959167  V_14;
	memset(&V_14, 0, sizeof(V_14));
	int32_t V_15 = 0;
	int32_t V_16 = 0;
	TuioObject_t1236821014 * V_17 = NULL;
	int32_t V_18 = 0;
	int32_t V_19 = 0;
	String_t* V_20 = NULL;
	Enumerator_t1564671351  V_21;
	memset(&V_21, 0, sizeof(V_21));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		OscMessage_t2764280154 * L_0 = ___message0;
		NullCheck(L_0);
		String_t* L_1 = OscPacket_get_Address_m1502094218(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_1, _stringLiteral310035036, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0013;
		}
	}
	{
		return;
	}

IL_0013:
	{
		OscMessage_t2764280154 * L_3 = ___message0;
		NullCheck(L_3);
		RuntimeObject* L_4 = OscPacket_get_Data_m65815223(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		RuntimeObject * L_5 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t3230389896_il2cpp_TypeInfo_var, L_4, 0);
		NullCheck(L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_5);
		V_0 = L_6;
		String_t* L_7 = V_0;
		String_t* L_8 = L_7;
		V_20 = L_8;
		if (!L_8)
		{
			goto IL_0364;
		}
	}
	{
		String_t* L_9 = V_20;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_10 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_9, _stringLiteral3021628834, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_005f;
		}
	}
	{
		String_t* L_11 = V_20;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_12 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_11, _stringLiteral2273848675, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_0169;
		}
	}
	{
		String_t* L_13 = V_20;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_14 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_13, _stringLiteral1851287145, /*hidden argument*/NULL);
		if (L_14)
		{
			goto IL_0211;
		}
	}
	{
		return;
	}

IL_005f:
	{
		OscMessage_t2764280154 * L_15 = ___message0;
		NullCheck(L_15);
		RuntimeObject* L_16 = OscPacket_get_Data_m65815223(L_15, /*hidden argument*/NULL);
		NullCheck(L_16);
		int32_t L_17 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, ICollection_1_t3641524600_il2cpp_TypeInfo_var, L_16);
		if ((((int32_t)L_17) >= ((int32_t)((int32_t)11))))
		{
			goto IL_006f;
		}
	}
	{
		return;
	}

IL_006f:
	{
		OscMessage_t2764280154 * L_18 = ___message0;
		NullCheck(L_18);
		RuntimeObject* L_19 = OscPacket_get_Data_m65815223(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		RuntimeObject * L_20 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t3230389896_il2cpp_TypeInfo_var, L_19, 1);
		V_1 = ((*(int32_t*)((int32_t*)UnBox(L_20, Int32_t2071877448_il2cpp_TypeInfo_var))));
		OscMessage_t2764280154 * L_21 = ___message0;
		NullCheck(L_21);
		RuntimeObject* L_22 = OscPacket_get_Data_m65815223(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		RuntimeObject * L_23 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t3230389896_il2cpp_TypeInfo_var, L_22, 2);
		V_2 = ((*(int32_t*)((int32_t*)UnBox(L_23, Int32_t2071877448_il2cpp_TypeInfo_var))));
		OscMessage_t2764280154 * L_24 = ___message0;
		NullCheck(L_24);
		RuntimeObject* L_25 = OscPacket_get_Data_m65815223(L_24, /*hidden argument*/NULL);
		NullCheck(L_25);
		RuntimeObject * L_26 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t3230389896_il2cpp_TypeInfo_var, L_25, 3);
		V_3 = ((*(float*)((float*)UnBox(L_26, Single_t2076509932_il2cpp_TypeInfo_var))));
		OscMessage_t2764280154 * L_27 = ___message0;
		NullCheck(L_27);
		RuntimeObject* L_28 = OscPacket_get_Data_m65815223(L_27, /*hidden argument*/NULL);
		NullCheck(L_28);
		RuntimeObject * L_29 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t3230389896_il2cpp_TypeInfo_var, L_28, 4);
		V_4 = ((*(float*)((float*)UnBox(L_29, Single_t2076509932_il2cpp_TypeInfo_var))));
		OscMessage_t2764280154 * L_30 = ___message0;
		NullCheck(L_30);
		RuntimeObject* L_31 = OscPacket_get_Data_m65815223(L_30, /*hidden argument*/NULL);
		NullCheck(L_31);
		RuntimeObject * L_32 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t3230389896_il2cpp_TypeInfo_var, L_31, 5);
		V_5 = ((*(float*)((float*)UnBox(L_32, Single_t2076509932_il2cpp_TypeInfo_var))));
		OscMessage_t2764280154 * L_33 = ___message0;
		NullCheck(L_33);
		RuntimeObject* L_34 = OscPacket_get_Data_m65815223(L_33, /*hidden argument*/NULL);
		NullCheck(L_34);
		RuntimeObject * L_35 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t3230389896_il2cpp_TypeInfo_var, L_34, 6);
		V_6 = ((*(float*)((float*)UnBox(L_35, Single_t2076509932_il2cpp_TypeInfo_var))));
		OscMessage_t2764280154 * L_36 = ___message0;
		NullCheck(L_36);
		RuntimeObject* L_37 = OscPacket_get_Data_m65815223(L_36, /*hidden argument*/NULL);
		NullCheck(L_37);
		RuntimeObject * L_38 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t3230389896_il2cpp_TypeInfo_var, L_37, 7);
		V_7 = ((*(float*)((float*)UnBox(L_38, Single_t2076509932_il2cpp_TypeInfo_var))));
		OscMessage_t2764280154 * L_39 = ___message0;
		NullCheck(L_39);
		RuntimeObject* L_40 = OscPacket_get_Data_m65815223(L_39, /*hidden argument*/NULL);
		NullCheck(L_40);
		RuntimeObject * L_41 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t3230389896_il2cpp_TypeInfo_var, L_40, 8);
		V_8 = ((*(float*)((float*)UnBox(L_41, Single_t2076509932_il2cpp_TypeInfo_var))));
		OscMessage_t2764280154 * L_42 = ___message0;
		NullCheck(L_42);
		RuntimeObject* L_43 = OscPacket_get_Data_m65815223(L_42, /*hidden argument*/NULL);
		NullCheck(L_43);
		RuntimeObject * L_44 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t3230389896_il2cpp_TypeInfo_var, L_43, ((int32_t)9));
		V_9 = ((*(float*)((float*)UnBox(L_44, Single_t2076509932_il2cpp_TypeInfo_var))));
		OscMessage_t2764280154 * L_45 = ___message0;
		NullCheck(L_45);
		RuntimeObject* L_46 = OscPacket_get_Data_m65815223(L_45, /*hidden argument*/NULL);
		NullCheck(L_46);
		RuntimeObject * L_47 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t3230389896_il2cpp_TypeInfo_var, L_46, ((int32_t)10));
		V_10 = ((*(float*)((float*)UnBox(L_47, Single_t2076509932_il2cpp_TypeInfo_var))));
		Dictionary_2_t244646649 * L_48 = __this->get_objects_3();
		int32_t L_49 = V_1;
		NullCheck(L_48);
		bool L_50 = Dictionary_2_TryGetValue_m1599847323(L_48, L_49, (&V_11), /*hidden argument*/Dictionary_2_TryGetValue_m1599847323_RuntimeMethod_var);
		if (L_50)
		{
			goto IL_0145;
		}
	}
	{
		int32_t L_51 = V_1;
		int32_t L_52 = V_2;
		TuioObject_t1236821014 * L_53 = (TuioObject_t1236821014 *)il2cpp_codegen_object_new(TuioObject_t1236821014_il2cpp_TypeInfo_var);
		TuioObject__ctor_m2498796396(L_53, L_51, L_52, /*hidden argument*/NULL);
		V_11 = L_53;
	}

IL_0145:
	{
		TuioObject_t1236821014 * L_54 = V_11;
		float L_55 = V_3;
		float L_56 = V_4;
		float L_57 = V_5;
		float L_58 = V_6;
		float L_59 = V_7;
		float L_60 = V_8;
		float L_61 = V_9;
		float L_62 = V_10;
		NullCheck(L_54);
		TuioObject_Update_m829237429(L_54, L_55, L_56, L_57, L_58, L_59, L_60, L_61, L_62, /*hidden argument*/NULL);
		List_1_t605942146 * L_63 = __this->get_updatedObjects_4();
		TuioObject_t1236821014 * L_64 = V_11;
		NullCheck(L_63);
		List_1_Add_m4210503130(L_63, L_64, /*hidden argument*/List_1_Add_m4210503130_RuntimeMethod_var);
		return;
	}

IL_0169:
	{
		OscMessage_t2764280154 * L_65 = ___message0;
		NullCheck(L_65);
		RuntimeObject* L_66 = OscPacket_get_Data_m65815223(L_65, /*hidden argument*/NULL);
		NullCheck(L_66);
		int32_t L_67 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, ICollection_1_t3641524600_il2cpp_TypeInfo_var, L_66);
		V_12 = L_67;
		V_13 = 1;
		goto IL_019e;
	}

IL_017b:
	{
		List_1_t1440998580 * L_68 = __this->get_addedObjects_5();
		OscMessage_t2764280154 * L_69 = ___message0;
		NullCheck(L_69);
		RuntimeObject* L_70 = OscPacket_get_Data_m65815223(L_69, /*hidden argument*/NULL);
		int32_t L_71 = V_13;
		NullCheck(L_70);
		RuntimeObject * L_72 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t3230389896_il2cpp_TypeInfo_var, L_70, L_71);
		NullCheck(L_68);
		List_1_Add_m688682013(L_68, ((*(int32_t*)((int32_t*)UnBox(L_72, Int32_t2071877448_il2cpp_TypeInfo_var)))), /*hidden argument*/List_1_Add_m688682013_RuntimeMethod_var);
		int32_t L_73 = V_13;
		V_13 = ((int32_t)((int32_t)L_73+(int32_t)1));
	}

IL_019e:
	{
		int32_t L_74 = V_13;
		int32_t L_75 = V_12;
		if ((((int32_t)L_74) < ((int32_t)L_75)))
		{
			goto IL_017b;
		}
	}
	{
		Dictionary_2_t244646649 * L_76 = __this->get_objects_3();
		NullCheck(L_76);
		Enumerator_t1564671351  L_77 = Dictionary_2_GetEnumerator_m2329010490(L_76, /*hidden argument*/Dictionary_2_GetEnumerator_m2329010490_RuntimeMethod_var);
		V_21 = L_77;
	}

IL_01b1:
	try
	{ // begin try (depth: 1)
		{
			goto IL_01f5;
		}

IL_01b3:
		{
			KeyValuePair_2_t2296959167  L_78 = Enumerator_get_Current_m874299548((&V_21), /*hidden argument*/Enumerator_get_Current_m874299548_RuntimeMethod_var);
			V_14 = L_78;
			List_1_t1440998580 * L_79 = __this->get_addedObjects_5();
			int32_t L_80 = KeyValuePair_2_get_Key_m303742594((&V_14), /*hidden argument*/KeyValuePair_2_get_Key_m303742594_RuntimeMethod_var);
			NullCheck(L_79);
			bool L_81 = List_1_Contains_m3517981001(L_79, L_80, /*hidden argument*/List_1_Contains_m3517981001_RuntimeMethod_var);
			if (L_81)
			{
				goto IL_01e2;
			}
		}

IL_01d0:
		{
			List_1_t1440998580 * L_82 = __this->get_removedObjects_6();
			int32_t L_83 = KeyValuePair_2_get_Key_m303742594((&V_14), /*hidden argument*/KeyValuePair_2_get_Key_m303742594_RuntimeMethod_var);
			NullCheck(L_82);
			List_1_Add_m688682013(L_82, L_83, /*hidden argument*/List_1_Add_m688682013_RuntimeMethod_var);
		}

IL_01e2:
		{
			List_1_t1440998580 * L_84 = __this->get_addedObjects_5();
			int32_t L_85 = KeyValuePair_2_get_Key_m303742594((&V_14), /*hidden argument*/KeyValuePair_2_get_Key_m303742594_RuntimeMethod_var);
			NullCheck(L_84);
			List_1_Remove_m282822032(L_84, L_85, /*hidden argument*/List_1_Remove_m282822032_RuntimeMethod_var);
		}

IL_01f5:
		{
			bool L_86 = Enumerator_MoveNext_m3089698347((&V_21), /*hidden argument*/Enumerator_MoveNext_m3089698347_RuntimeMethod_var);
			if (L_86)
			{
				goto IL_01b3;
			}
		}

IL_01fe:
		{
			IL2CPP_LEAVE(0x364, FINALLY_0203);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0203;
	}

FINALLY_0203:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m4106138594((&V_21), /*hidden argument*/Enumerator_Dispose_m4106138594_RuntimeMethod_var);
		IL2CPP_END_FINALLY(515)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(515)
	{
		IL2CPP_JUMP_TBL(0x364, IL_0364)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0211:
	{
		OscMessage_t2764280154 * L_87 = ___message0;
		NullCheck(L_87);
		RuntimeObject* L_88 = OscPacket_get_Data_m65815223(L_87, /*hidden argument*/NULL);
		NullCheck(L_88);
		int32_t L_89 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, ICollection_1_t3641524600_il2cpp_TypeInfo_var, L_88);
		if ((((int32_t)L_89) >= ((int32_t)2)))
		{
			goto IL_0220;
		}
	}
	{
		return;
	}

IL_0220:
	{
		OscMessage_t2764280154 * L_90 = ___message0;
		NullCheck(L_90);
		RuntimeObject* L_91 = OscPacket_get_Data_m65815223(L_90, /*hidden argument*/NULL);
		NullCheck(L_91);
		RuntimeObject * L_92 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IList_1_t3230389896_il2cpp_TypeInfo_var, L_91, 1);
		ObjectProcessor_set_FrameNumber_m3936044951(__this, ((*(int32_t*)((int32_t*)UnBox(L_92, Int32_t2071877448_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		List_1_t605942146 * L_93 = __this->get_updatedObjects_4();
		NullCheck(L_93);
		int32_t L_94 = List_1_get_Count_m1698483734(L_93, /*hidden argument*/List_1_get_Count_m1698483734_RuntimeMethod_var);
		V_15 = L_94;
		V_16 = 0;
		goto IL_02d5;
	}

IL_024c:
	{
		List_1_t605942146 * L_95 = __this->get_updatedObjects_4();
		int32_t L_96 = V_16;
		NullCheck(L_95);
		TuioObject_t1236821014 * L_97 = List_1_get_Item_m992642369(L_95, L_96, /*hidden argument*/List_1_get_Item_m992642369_RuntimeMethod_var);
		V_17 = L_97;
		List_1_t1440998580 * L_98 = __this->get_addedObjects_5();
		TuioObject_t1236821014 * L_99 = V_17;
		NullCheck(L_99);
		int32_t L_100 = TuioEntity_get_Id_m4208226194(L_99, /*hidden argument*/NULL);
		NullCheck(L_98);
		bool L_101 = List_1_Contains_m3517981001(L_98, L_100, /*hidden argument*/List_1_Contains_m3517981001_RuntimeMethod_var);
		if (!L_101)
		{
			goto IL_02b4;
		}
	}
	{
		Dictionary_2_t244646649 * L_102 = __this->get_objects_3();
		TuioObject_t1236821014 * L_103 = V_17;
		NullCheck(L_103);
		int32_t L_104 = TuioEntity_get_Id_m4208226194(L_103, /*hidden argument*/NULL);
		NullCheck(L_102);
		bool L_105 = Dictionary_2_ContainsKey_m781358407(L_102, L_104, /*hidden argument*/Dictionary_2_ContainsKey_m781358407_RuntimeMethod_var);
		if (L_105)
		{
			goto IL_02b4;
		}
	}
	{
		Dictionary_2_t244646649 * L_106 = __this->get_objects_3();
		TuioObject_t1236821014 * L_107 = V_17;
		NullCheck(L_107);
		int32_t L_108 = TuioEntity_get_Id_m4208226194(L_107, /*hidden argument*/NULL);
		TuioObject_t1236821014 * L_109 = V_17;
		NullCheck(L_106);
		Dictionary_2_Add_m2557918960(L_106, L_108, L_109, /*hidden argument*/Dictionary_2_Add_m2557918960_RuntimeMethod_var);
		EventHandler_1_t402050513 * L_110 = __this->get_ObjectAdded_0();
		if (!L_110)
		{
			goto IL_02cf;
		}
	}
	{
		EventHandler_1_t402050513 * L_111 = __this->get_ObjectAdded_0();
		TuioObject_t1236821014 * L_112 = V_17;
		TuioObjectEventArgs_t1810743341 * L_113 = (TuioObjectEventArgs_t1810743341 *)il2cpp_codegen_object_new(TuioObjectEventArgs_t1810743341_il2cpp_TypeInfo_var);
		TuioObjectEventArgs__ctor_m1688032627(L_113, L_112, /*hidden argument*/NULL);
		NullCheck(L_111);
		EventHandler_1_Invoke_m1340678838(L_111, __this, L_113, /*hidden argument*/EventHandler_1_Invoke_m1340678838_RuntimeMethod_var);
		goto IL_02cf;
	}

IL_02b4:
	{
		EventHandler_1_t402050513 * L_114 = __this->get_ObjectUpdated_1();
		if (!L_114)
		{
			goto IL_02cf;
		}
	}
	{
		EventHandler_1_t402050513 * L_115 = __this->get_ObjectUpdated_1();
		TuioObject_t1236821014 * L_116 = V_17;
		TuioObjectEventArgs_t1810743341 * L_117 = (TuioObjectEventArgs_t1810743341 *)il2cpp_codegen_object_new(TuioObjectEventArgs_t1810743341_il2cpp_TypeInfo_var);
		TuioObjectEventArgs__ctor_m1688032627(L_117, L_116, /*hidden argument*/NULL);
		NullCheck(L_115);
		EventHandler_1_Invoke_m1340678838(L_115, __this, L_117, /*hidden argument*/EventHandler_1_Invoke_m1340678838_RuntimeMethod_var);
	}

IL_02cf:
	{
		int32_t L_118 = V_16;
		V_16 = ((int32_t)((int32_t)L_118+(int32_t)1));
	}

IL_02d5:
	{
		int32_t L_119 = V_16;
		int32_t L_120 = V_15;
		if ((((int32_t)L_119) < ((int32_t)L_120)))
		{
			goto IL_024c;
		}
	}
	{
		List_1_t1440998580 * L_121 = __this->get_removedObjects_6();
		NullCheck(L_121);
		int32_t L_122 = List_1_get_Count_m3833405771(L_121, /*hidden argument*/List_1_get_Count_m3833405771_RuntimeMethod_var);
		V_15 = L_122;
		V_18 = 0;
		goto IL_033d;
	}

IL_02f0:
	{
		List_1_t1440998580 * L_123 = __this->get_removedObjects_6();
		int32_t L_124 = V_18;
		NullCheck(L_123);
		int32_t L_125 = List_1_get_Item_m3514532498(L_123, L_124, /*hidden argument*/List_1_get_Item_m3514532498_RuntimeMethod_var);
		V_19 = L_125;
		Dictionary_2_t244646649 * L_126 = __this->get_objects_3();
		int32_t L_127 = V_19;
		NullCheck(L_126);
		TuioObject_t1236821014 * L_128 = Dictionary_2_get_Item_m4097636154(L_126, L_127, /*hidden argument*/Dictionary_2_get_Item_m4097636154_RuntimeMethod_var);
		V_11 = L_128;
		Dictionary_2_t244646649 * L_129 = __this->get_objects_3();
		int32_t L_130 = V_19;
		NullCheck(L_129);
		Dictionary_2_Remove_m185872653(L_129, L_130, /*hidden argument*/Dictionary_2_Remove_m185872653_RuntimeMethod_var);
		EventHandler_1_t402050513 * L_131 = __this->get_ObjectRemoved_2();
		if (!L_131)
		{
			goto IL_0337;
		}
	}
	{
		EventHandler_1_t402050513 * L_132 = __this->get_ObjectRemoved_2();
		TuioObject_t1236821014 * L_133 = V_11;
		TuioObjectEventArgs_t1810743341 * L_134 = (TuioObjectEventArgs_t1810743341 *)il2cpp_codegen_object_new(TuioObjectEventArgs_t1810743341_il2cpp_TypeInfo_var);
		TuioObjectEventArgs__ctor_m1688032627(L_134, L_133, /*hidden argument*/NULL);
		NullCheck(L_132);
		EventHandler_1_Invoke_m1340678838(L_132, __this, L_134, /*hidden argument*/EventHandler_1_Invoke_m1340678838_RuntimeMethod_var);
	}

IL_0337:
	{
		int32_t L_135 = V_18;
		V_18 = ((int32_t)((int32_t)L_135+(int32_t)1));
	}

IL_033d:
	{
		int32_t L_136 = V_18;
		int32_t L_137 = V_15;
		if ((((int32_t)L_136) < ((int32_t)L_137)))
		{
			goto IL_02f0;
		}
	}
	{
		List_1_t1440998580 * L_138 = __this->get_addedObjects_5();
		NullCheck(L_138);
		List_1_Clear_m3896574426(L_138, /*hidden argument*/List_1_Clear_m3896574426_RuntimeMethod_var);
		List_1_t1440998580 * L_139 = __this->get_removedObjects_6();
		NullCheck(L_139);
		List_1_Clear_m3896574426(L_139, /*hidden argument*/List_1_Clear_m3896574426_RuntimeMethod_var);
		List_1_t605942146 * L_140 = __this->get_updatedObjects_4();
		NullCheck(L_140);
		List_1_Clear_m2478926039(L_140, /*hidden argument*/List_1_Clear_m2478926039_RuntimeMethod_var);
	}

IL_0364:
	{
		return;
	}
}
// System.Void TUIOsharp.DataProcessors.ObjectProcessor::.ctor()
extern "C"  void ObjectProcessor__ctor_m2046048849 (ObjectProcessor_t221569383 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ObjectProcessor__ctor_m2046048849_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t244646649 * L_0 = (Dictionary_2_t244646649 *)il2cpp_codegen_object_new(Dictionary_2_t244646649_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1364836728(L_0, /*hidden argument*/Dictionary_2__ctor_m1364836728_RuntimeMethod_var);
		__this->set_objects_3(L_0);
		List_1_t605942146 * L_1 = (List_1_t605942146 *)il2cpp_codegen_object_new(List_1_t605942146_il2cpp_TypeInfo_var);
		List_1__ctor_m4191486726(L_1, /*hidden argument*/List_1__ctor_m4191486726_RuntimeMethod_var);
		__this->set_updatedObjects_4(L_1);
		List_1_t1440998580 * L_2 = (List_1_t1440998580 *)il2cpp_codegen_object_new(List_1_t1440998580_il2cpp_TypeInfo_var);
		List_1__ctor_m1598946593(L_2, /*hidden argument*/List_1__ctor_m1598946593_RuntimeMethod_var);
		__this->set_addedObjects_5(L_2);
		List_1_t1440998580 * L_3 = (List_1_t1440998580 *)il2cpp_codegen_object_new(List_1_t1440998580_il2cpp_TypeInfo_var);
		List_1__ctor_m1598946593(L_3, /*hidden argument*/List_1__ctor_m1598946593_RuntimeMethod_var);
		__this->set_removedObjects_6(L_3);
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TUIOsharp.DataProcessors.TuioBlobEventArgs::.ctor(TUIOsharp.Entities.TuioBlob)
extern "C"  void TuioBlobEventArgs__ctor_m3423126035 (TuioBlobEventArgs_t3562978179 * __this, TuioBlob_t2046943414 * ___blob0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TuioBlobEventArgs__ctor_m3423126035_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t3289624707_il2cpp_TypeInfo_var);
		EventArgs__ctor_m3696060910(__this, /*hidden argument*/NULL);
		TuioBlob_t2046943414 * L_0 = ___blob0;
		__this->set_Blob_1(L_0);
		return;
	}
}
// System.Void TUIOsharp.DataProcessors.TuioCursorEventArgs::.ctor(TUIOsharp.Entities.TuioCursor)
extern "C"  void TuioCursorEventArgs__ctor_m2169915347 (TuioCursorEventArgs_t362990012 * __this, TuioCursor_t1850351419 * ___cursor0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TuioCursorEventArgs__ctor_m2169915347_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t3289624707_il2cpp_TypeInfo_var);
		EventArgs__ctor_m3696060910(__this, /*hidden argument*/NULL);
		TuioCursor_t1850351419 * L_0 = ___cursor0;
		__this->set_Cursor_1(L_0);
		return;
	}
}
// System.Void TUIOsharp.DataProcessors.TuioObjectEventArgs::.ctor(TUIOsharp.Entities.TuioObject)
extern "C"  void TuioObjectEventArgs__ctor_m1688032627 (TuioObjectEventArgs_t1810743341 * __this, TuioObject_t1236821014 * ___obj0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TuioObjectEventArgs__ctor_m1688032627_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t3289624707_il2cpp_TypeInfo_var);
		EventArgs__ctor_m3696060910(__this, /*hidden argument*/NULL);
		TuioObject_t1236821014 * L_0 = ___obj0;
		__this->set_Object_1(L_0);
		return;
	}
}
// System.Single TUIOsharp.Entities.TuioBlob::get_Angle()
extern "C"  float TuioBlob_get_Angle_m4160794346 (TuioBlob_t2046943414 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_U3CAngleU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void TUIOsharp.Entities.TuioBlob::set_Angle(System.Single)
extern "C"  void TuioBlob_set_Angle_m683906823 (TuioBlob_t2046943414 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_U3CAngleU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.Single TUIOsharp.Entities.TuioBlob::get_Width()
extern "C"  float TuioBlob_get_Width_m1248651415 (TuioBlob_t2046943414 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_U3CWidthU3Ek__BackingField_7();
		return L_0;
	}
}
// System.Void TUIOsharp.Entities.TuioBlob::set_Width(System.Single)
extern "C"  void TuioBlob_set_Width_m3192515620 (TuioBlob_t2046943414 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_U3CWidthU3Ek__BackingField_7(L_0);
		return;
	}
}
// System.Single TUIOsharp.Entities.TuioBlob::get_Height()
extern "C"  float TuioBlob_get_Height_m1807649522 (TuioBlob_t2046943414 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_U3CHeightU3Ek__BackingField_8();
		return L_0;
	}
}
// System.Void TUIOsharp.Entities.TuioBlob::set_Height(System.Single)
extern "C"  void TuioBlob_set_Height_m2645716865 (TuioBlob_t2046943414 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_U3CHeightU3Ek__BackingField_8(L_0);
		return;
	}
}
// System.Single TUIOsharp.Entities.TuioBlob::get_Area()
extern "C"  float TuioBlob_get_Area_m3719711224 (TuioBlob_t2046943414 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_U3CAreaU3Ek__BackingField_9();
		return L_0;
	}
}
// System.Void TUIOsharp.Entities.TuioBlob::set_Area(System.Single)
extern "C"  void TuioBlob_set_Area_m1501805605 (TuioBlob_t2046943414 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_U3CAreaU3Ek__BackingField_9(L_0);
		return;
	}
}
// System.Single TUIOsharp.Entities.TuioBlob::get_RotationVelocity()
extern "C"  float TuioBlob_get_RotationVelocity_m1406953876 (TuioBlob_t2046943414 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_U3CRotationVelocityU3Ek__BackingField_10();
		return L_0;
	}
}
// System.Void TUIOsharp.Entities.TuioBlob::set_RotationVelocity(System.Single)
extern "C"  void TuioBlob_set_RotationVelocity_m2915155457 (TuioBlob_t2046943414 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_U3CRotationVelocityU3Ek__BackingField_10(L_0);
		return;
	}
}
// System.Single TUIOsharp.Entities.TuioBlob::get_RotationAcceleration()
extern "C"  float TuioBlob_get_RotationAcceleration_m1478657511 (TuioBlob_t2046943414 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_U3CRotationAccelerationU3Ek__BackingField_11();
		return L_0;
	}
}
// System.Void TUIOsharp.Entities.TuioBlob::set_RotationAcceleration(System.Single)
extern "C"  void TuioBlob_set_RotationAcceleration_m892624954 (TuioBlob_t2046943414 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_U3CRotationAccelerationU3Ek__BackingField_11(L_0);
		return;
	}
}
// System.Void TUIOsharp.Entities.TuioBlob::.ctor(System.Int32)
extern "C"  void TuioBlob__ctor_m2840170451 (TuioBlob_t2046943414 * __this, int32_t ___id0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___id0;
		TuioBlob__ctor_m1637502122(__this, L_0, (0.0f), (0.0f), (0.0f), (0.0f), (0.0f), (0.0f), (0.0f), (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void TUIOsharp.Entities.TuioBlob::.ctor(System.Int32,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C"  void TuioBlob__ctor_m1637502122 (TuioBlob_t2046943414 * __this, int32_t ___id0, float ___x1, float ___y2, float ___angle3, float ___width4, float ___height5, float ___area6, float ___velocityX7, float ___velocityY8, float ___rotationVelocity9, float ___acceleration10, float ___rotationAcceleration11, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___id0;
		float L_1 = ___x1;
		float L_2 = ___y2;
		float L_3 = ___velocityX7;
		float L_4 = ___velocityY8;
		float L_5 = ___acceleration10;
		TuioEntity__ctor_m3219791970(__this, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		float L_6 = ___angle3;
		TuioBlob_set_Angle_m683906823(__this, L_6, /*hidden argument*/NULL);
		float L_7 = ___width4;
		TuioBlob_set_Width_m3192515620(__this, L_7, /*hidden argument*/NULL);
		float L_8 = ___height5;
		TuioBlob_set_Height_m2645716865(__this, L_8, /*hidden argument*/NULL);
		float L_9 = ___area6;
		TuioBlob_set_Area_m1501805605(__this, L_9, /*hidden argument*/NULL);
		float L_10 = ___rotationVelocity9;
		TuioBlob_set_RotationVelocity_m2915155457(__this, L_10, /*hidden argument*/NULL);
		float L_11 = ___rotationAcceleration11;
		TuioBlob_set_RotationAcceleration_m892624954(__this, L_11, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TUIOsharp.Entities.TuioBlob::Update(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C"  void TuioBlob_Update_m2006179440 (TuioBlob_t2046943414 * __this, float ___x0, float ___y1, float ___angle2, float ___width3, float ___height4, float ___area5, float ___velocityX6, float ___velocityY7, float ___rotationVelocity8, float ___acceleration9, float ___rotationAcceleration10, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		TuioEntity_set_X_m24639236(__this, L_0, /*hidden argument*/NULL);
		float L_1 = ___y1;
		TuioEntity_set_Y_m3783946495(__this, L_1, /*hidden argument*/NULL);
		float L_2 = ___angle2;
		TuioBlob_set_Angle_m683906823(__this, L_2, /*hidden argument*/NULL);
		float L_3 = ___width3;
		TuioBlob_set_Width_m3192515620(__this, L_3, /*hidden argument*/NULL);
		float L_4 = ___height4;
		TuioBlob_set_Height_m2645716865(__this, L_4, /*hidden argument*/NULL);
		float L_5 = ___area5;
		TuioBlob_set_Area_m1501805605(__this, L_5, /*hidden argument*/NULL);
		float L_6 = ___velocityX6;
		TuioEntity_set_VelocityX_m731909633(__this, L_6, /*hidden argument*/NULL);
		float L_7 = ___velocityX6;
		TuioEntity_set_VelocityY_m1367177094(__this, L_7, /*hidden argument*/NULL);
		float L_8 = ___rotationVelocity8;
		TuioBlob_set_RotationVelocity_m2915155457(__this, L_8, /*hidden argument*/NULL);
		float L_9 = ___acceleration9;
		TuioEntity_set_Acceleration_m3065927520(__this, L_9, /*hidden argument*/NULL);
		float L_10 = ___rotationAcceleration10;
		TuioBlob_set_RotationAcceleration_m892624954(__this, L_10, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TUIOsharp.Entities.TuioCursor::.ctor(System.Int32)
extern "C"  void TuioCursor__ctor_m2689165534 (TuioCursor_t1850351419 * __this, int32_t ___id0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___id0;
		TuioCursor__ctor_m944054751(__this, L_0, (0.0f), (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void TUIOsharp.Entities.TuioCursor::.ctor(System.Int32,System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C"  void TuioCursor__ctor_m944054751 (TuioCursor_t1850351419 * __this, int32_t ___id0, float ___x1, float ___y2, float ___velocityX3, float ___velocityY4, float ___acceleration5, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___id0;
		float L_1 = ___x1;
		float L_2 = ___y2;
		float L_3 = ___velocityX3;
		float L_4 = ___velocityY4;
		float L_5 = ___acceleration5;
		TuioEntity__ctor_m3219791970(__this, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TUIOsharp.Entities.TuioCursor::Update(System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C"  void TuioCursor_Update_m650004219 (TuioCursor_t1850351419 * __this, float ___x0, float ___y1, float ___velocityX2, float ___velocityY3, float ___acceleration4, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		TuioEntity_set_X_m24639236(__this, L_0, /*hidden argument*/NULL);
		float L_1 = ___y1;
		TuioEntity_set_Y_m3783946495(__this, L_1, /*hidden argument*/NULL);
		float L_2 = ___velocityX2;
		TuioEntity_set_VelocityX_m731909633(__this, L_2, /*hidden argument*/NULL);
		float L_3 = ___velocityX2;
		TuioEntity_set_VelocityY_m1367177094(__this, L_3, /*hidden argument*/NULL);
		float L_4 = ___acceleration4;
		TuioEntity_set_Acceleration_m3065927520(__this, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 TUIOsharp.Entities.TuioEntity::get_Id()
extern "C"  int32_t TuioEntity_get_Id_m4208226194 (TuioEntity_t295276718 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_U3CIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void TUIOsharp.Entities.TuioEntity::set_Id(System.Int32)
extern "C"  void TuioEntity_set_Id_m4152509111 (TuioEntity_t295276718 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Single TUIOsharp.Entities.TuioEntity::get_X()
extern "C"  float TuioEntity_get_X_m1550875633 (TuioEntity_t295276718 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_U3CXU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void TUIOsharp.Entities.TuioEntity::set_X(System.Single)
extern "C"  void TuioEntity_set_X_m24639236 (TuioEntity_t295276718 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_U3CXU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Single TUIOsharp.Entities.TuioEntity::get_Y()
extern "C"  float TuioEntity_get_Y_m1692038134 (TuioEntity_t295276718 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_U3CYU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void TUIOsharp.Entities.TuioEntity::set_Y(System.Single)
extern "C"  void TuioEntity_set_Y_m3783946495 (TuioEntity_t295276718 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_U3CYU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void TUIOsharp.Entities.TuioEntity::set_VelocityX(System.Single)
extern "C"  void TuioEntity_set_VelocityX_m731909633 (TuioEntity_t295276718 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_U3CVelocityXU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void TUIOsharp.Entities.TuioEntity::set_VelocityY(System.Single)
extern "C"  void TuioEntity_set_VelocityY_m1367177094 (TuioEntity_t295276718 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_U3CVelocityYU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Void TUIOsharp.Entities.TuioEntity::set_Acceleration(System.Single)
extern "C"  void TuioEntity_set_Acceleration_m3065927520 (TuioEntity_t295276718 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_U3CAccelerationU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.Void TUIOsharp.Entities.TuioEntity::.ctor(System.Int32,System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C"  void TuioEntity__ctor_m3219791970 (TuioEntity_t295276718 * __this, int32_t ___id0, float ___x1, float ___y2, float ___velocityX3, float ___velocityY4, float ___acceleration5, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___id0;
		TuioEntity_set_Id_m4152509111(__this, L_0, /*hidden argument*/NULL);
		float L_1 = ___x1;
		TuioEntity_set_X_m24639236(__this, L_1, /*hidden argument*/NULL);
		float L_2 = ___y2;
		TuioEntity_set_Y_m3783946495(__this, L_2, /*hidden argument*/NULL);
		float L_3 = ___velocityX3;
		TuioEntity_set_VelocityX_m731909633(__this, L_3, /*hidden argument*/NULL);
		float L_4 = ___velocityY4;
		TuioEntity_set_VelocityY_m1367177094(__this, L_4, /*hidden argument*/NULL);
		float L_5 = ___acceleration5;
		TuioEntity_set_Acceleration_m3065927520(__this, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 TUIOsharp.Entities.TuioObject::get_ClassId()
extern "C"  int32_t TuioObject_get_ClassId_m1220990830 (TuioObject_t1236821014 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_U3CClassIdU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void TUIOsharp.Entities.TuioObject::set_ClassId(System.Int32)
extern "C"  void TuioObject_set_ClassId_m2907387411 (TuioObject_t1236821014 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CClassIdU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.Single TUIOsharp.Entities.TuioObject::get_Angle()
extern "C"  float TuioObject_get_Angle_m1282280138 (TuioObject_t1236821014 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_U3CAngleU3Ek__BackingField_7();
		return L_0;
	}
}
// System.Void TUIOsharp.Entities.TuioObject::set_Angle(System.Single)
extern "C"  void TuioObject_set_Angle_m3378571757 (TuioObject_t1236821014 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_U3CAngleU3Ek__BackingField_7(L_0);
		return;
	}
}
// System.Single TUIOsharp.Entities.TuioObject::get_RotationVelocity()
extern "C"  float TuioObject_get_RotationVelocity_m2864749088 (TuioObject_t1236821014 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_U3CRotationVelocityU3Ek__BackingField_8();
		return L_0;
	}
}
// System.Void TUIOsharp.Entities.TuioObject::set_RotationVelocity(System.Single)
extern "C"  void TuioObject_set_RotationVelocity_m2262321299 (TuioObject_t1236821014 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_U3CRotationVelocityU3Ek__BackingField_8(L_0);
		return;
	}
}
// System.Single TUIOsharp.Entities.TuioObject::get_RotationAcceleration()
extern "C"  float TuioObject_get_RotationAcceleration_m1852774865 (TuioObject_t1236821014 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_U3CRotationAccelerationU3Ek__BackingField_9();
		return L_0;
	}
}
// System.Void TUIOsharp.Entities.TuioObject::set_RotationAcceleration(System.Single)
extern "C"  void TuioObject_set_RotationAcceleration_m2446722782 (TuioObject_t1236821014 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_U3CRotationAccelerationU3Ek__BackingField_9(L_0);
		return;
	}
}
// System.Void TUIOsharp.Entities.TuioObject::.ctor(System.Int32,System.Int32)
extern "C"  void TuioObject__ctor_m2498796396 (TuioObject_t1236821014 * __this, int32_t ___id0, int32_t ___classId1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___id0;
		int32_t L_1 = ___classId1;
		TuioObject__ctor_m3091979500(__this, L_0, L_1, (0.0f), (0.0f), (0.0f), (0.0f), (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void TUIOsharp.Entities.TuioObject::.ctor(System.Int32,System.Int32,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C"  void TuioObject__ctor_m3091979500 (TuioObject_t1236821014 * __this, int32_t ___id0, int32_t ___classId1, float ___x2, float ___y3, float ___angle4, float ___velocityX5, float ___velocityY6, float ___rotationVelocity7, float ___acceleration8, float ___rotationAcceleration9, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___id0;
		float L_1 = ___x2;
		float L_2 = ___y3;
		float L_3 = ___velocityX5;
		float L_4 = ___velocityY6;
		float L_5 = ___acceleration8;
		TuioEntity__ctor_m3219791970(__this, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		int32_t L_6 = ___classId1;
		TuioObject_set_ClassId_m2907387411(__this, L_6, /*hidden argument*/NULL);
		float L_7 = ___angle4;
		TuioObject_set_Angle_m3378571757(__this, L_7, /*hidden argument*/NULL);
		float L_8 = ___rotationVelocity7;
		TuioObject_set_RotationVelocity_m2262321299(__this, L_8, /*hidden argument*/NULL);
		float L_9 = ___rotationAcceleration9;
		TuioObject_set_RotationAcceleration_m2446722782(__this, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TUIOsharp.Entities.TuioObject::Update(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C"  void TuioObject_Update_m829237429 (TuioObject_t1236821014 * __this, float ___x0, float ___y1, float ___angle2, float ___velocityX3, float ___velocityY4, float ___rotationVelocity5, float ___acceleration6, float ___rotationAcceleration7, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		TuioEntity_set_X_m24639236(__this, L_0, /*hidden argument*/NULL);
		float L_1 = ___y1;
		TuioEntity_set_Y_m3783946495(__this, L_1, /*hidden argument*/NULL);
		float L_2 = ___angle2;
		TuioObject_set_Angle_m3378571757(__this, L_2, /*hidden argument*/NULL);
		float L_3 = ___velocityX3;
		TuioEntity_set_VelocityX_m731909633(__this, L_3, /*hidden argument*/NULL);
		float L_4 = ___velocityX3;
		TuioEntity_set_VelocityY_m1367177094(__this, L_4, /*hidden argument*/NULL);
		float L_5 = ___rotationVelocity5;
		TuioObject_set_RotationVelocity_m2262321299(__this, L_5, /*hidden argument*/NULL);
		float L_6 = ___acceleration6;
		TuioEntity_set_Acceleration_m3065927520(__this, L_6, /*hidden argument*/NULL);
		float L_7 = ___rotationAcceleration7;
		TuioObject_set_RotationAcceleration_m2446722782(__this, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 TUIOsharp.TuioServer::get_Port()
extern "C"  int32_t TuioServer_get_Port_m3441621021 (TuioServer_t595520884 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_U3CPortU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void TUIOsharp.TuioServer::set_Port(System.Int32)
extern "C"  void TuioServer_set_Port_m2088027290 (TuioServer_t595520884 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CPortU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void TUIOsharp.TuioServer::.ctor(System.Int32)
extern "C"  void TuioServer__ctor_m230865672 (TuioServer_t595520884 * __this, int32_t ___port0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TuioServer__ctor_m230865672_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1583176531 * L_0 = (List_1_t1583176531 *)il2cpp_codegen_object_new(List_1_t1583176531_il2cpp_TypeInfo_var);
		List_1__ctor_m4170624131(L_0, /*hidden argument*/List_1__ctor_m4170624131_RuntimeMethod_var);
		__this->set_processors_2(L_0);
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		int32_t L_1 = ___port0;
		TuioServer_set_Port_m2088027290(__this, L_1, /*hidden argument*/NULL);
		int32_t L_2 = TuioServer_get_Port_m3441621021(__this, /*hidden argument*/NULL);
		UDPReceiver_t3846109956 * L_3 = (UDPReceiver_t3846109956 *)il2cpp_codegen_object_new(UDPReceiver_t3846109956_il2cpp_TypeInfo_var);
		UDPReceiver__ctor_m3124846498(L_3, L_2, (bool)0, /*hidden argument*/NULL);
		__this->set_udpReceiver_1(L_3);
		UDPReceiver_t3846109956 * L_4 = __this->get_udpReceiver_1();
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)TuioServer_handlerOscMessageReceived_m1229429870_RuntimeMethod_var);
		EventHandler_1_t4149316328 * L_6 = (EventHandler_1_t4149316328 *)il2cpp_codegen_object_new(EventHandler_1_t4149316328_il2cpp_TypeInfo_var);
		EventHandler_1__ctor_m384786560(L_6, __this, L_5, /*hidden argument*/EventHandler_1__ctor_m384786560_RuntimeMethod_var);
		NullCheck(L_4);
		UDPReceiver_add_MessageReceived_m3470532992(L_4, L_6, /*hidden argument*/NULL);
		UDPReceiver_t3846109956 * L_7 = __this->get_udpReceiver_1();
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)TuioServer_handlerOscErrorOccured_m1202150247_RuntimeMethod_var);
		EventHandler_1_t3778988004 * L_9 = (EventHandler_1_t3778988004 *)il2cpp_codegen_object_new(EventHandler_1_t3778988004_il2cpp_TypeInfo_var);
		EventHandler_1__ctor_m4043119420(L_9, __this, L_8, /*hidden argument*/EventHandler_1__ctor_m4043119420_RuntimeMethod_var);
		NullCheck(L_7);
		UDPReceiver_add_ErrorOccured_m1212653731(L_7, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TUIOsharp.TuioServer::Connect()
extern "C"  void TuioServer_Connect_m1407838501 (TuioServer_t595520884 * __this, const RuntimeMethod* method)
{
	{
		UDPReceiver_t3846109956 * L_0 = __this->get_udpReceiver_1();
		NullCheck(L_0);
		bool L_1 = UDPReceiver_get_IsRunning_m2474408206(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0018;
		}
	}
	{
		UDPReceiver_t3846109956 * L_2 = __this->get_udpReceiver_1();
		NullCheck(L_2);
		UDPReceiver_Start_m1033630334(L_2, /*hidden argument*/NULL);
	}

IL_0018:
	{
		return;
	}
}
// System.Void TUIOsharp.TuioServer::Disconnect()
extern "C"  void TuioServer_Disconnect_m2080463863 (TuioServer_t595520884 * __this, const RuntimeMethod* method)
{
	{
		UDPReceiver_t3846109956 * L_0 = __this->get_udpReceiver_1();
		NullCheck(L_0);
		bool L_1 = UDPReceiver_get_IsRunning_m2474408206(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		UDPReceiver_t3846109956 * L_2 = __this->get_udpReceiver_1();
		NullCheck(L_2);
		UDPReceiver_Stop_m3061802084(L_2, /*hidden argument*/NULL);
	}

IL_0018:
	{
		return;
	}
}
// System.Void TUIOsharp.TuioServer::AddDataProcessor(TUIOsharp.DataProcessors.IDataProcessor)
extern "C"  void TuioServer_AddDataProcessor_m2160836129 (TuioServer_t595520884 * __this, RuntimeObject* ___processor0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TuioServer_AddDataProcessor_m2160836129_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1583176531 * L_0 = __this->get_processors_2();
		RuntimeObject* L_1 = ___processor0;
		NullCheck(L_0);
		bool L_2 = List_1_Contains_m181590265(L_0, L_1, /*hidden argument*/List_1_Contains_m181590265_RuntimeMethod_var);
		if (!L_2)
		{
			goto IL_000f;
		}
	}
	{
		return;
	}

IL_000f:
	{
		List_1_t1583176531 * L_3 = __this->get_processors_2();
		RuntimeObject* L_4 = ___processor0;
		NullCheck(L_3);
		List_1_Add_m2594654071(L_3, L_4, /*hidden argument*/List_1_Add_m2594654071_RuntimeMethod_var);
		return;
	}
}
// System.Void TUIOsharp.TuioServer::RemoveDataProcessor(TUIOsharp.DataProcessors.IDataProcessor)
extern "C"  void TuioServer_RemoveDataProcessor_m1509103320 (TuioServer_t595520884 * __this, RuntimeObject* ___processor0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TuioServer_RemoveDataProcessor_m1509103320_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1583176531 * L_0 = __this->get_processors_2();
		RuntimeObject* L_1 = ___processor0;
		NullCheck(L_0);
		List_1_Remove_m3595814454(L_0, L_1, /*hidden argument*/List_1_Remove_m3595814454_RuntimeMethod_var);
		return;
	}
}
// System.Void TUIOsharp.TuioServer::RemoveAllDataProcessors()
extern "C"  void TuioServer_RemoveAllDataProcessors_m4138463077 (TuioServer_t595520884 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TuioServer_RemoveAllDataProcessors_m4138463077_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1583176531 * L_0 = __this->get_processors_2();
		NullCheck(L_0);
		List_1_Clear_m3203701604(L_0, /*hidden argument*/List_1_Clear_m3203701604_RuntimeMethod_var);
		return;
	}
}
// System.Void TUIOsharp.TuioServer::processMessage(OSCsharp.Data.OscMessage)
extern "C"  void TuioServer_processMessage_m3007744804 (TuioServer_t595520884 * __this, OscMessage_t2764280154 * ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TuioServer_processMessage_m3007744804_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		OscMessage_t2764280154 * L_0 = ___message0;
		NullCheck(L_0);
		RuntimeObject* L_1 = OscPacket_get_Data_m65815223(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_2 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, ICollection_1_t3641524600_il2cpp_TypeInfo_var, L_1);
		if (L_2)
		{
			goto IL_000e;
		}
	}
	{
		return;
	}

IL_000e:
	{
		List_1_t1583176531 * L_3 = __this->get_processors_2();
		NullCheck(L_3);
		int32_t L_4 = List_1_get_Count_m314033343(L_3, /*hidden argument*/List_1_get_Count_m314033343_RuntimeMethod_var);
		V_0 = L_4;
		V_1 = 0;
		goto IL_0034;
	}

IL_001e:
	{
		List_1_t1583176531 * L_5 = __this->get_processors_2();
		int32_t L_6 = V_1;
		NullCheck(L_5);
		RuntimeObject* L_7 = List_1_get_Item_m2840009344(L_5, L_6, /*hidden argument*/List_1_get_Item_m2840009344_RuntimeMethod_var);
		OscMessage_t2764280154 * L_8 = ___message0;
		NullCheck(L_7);
		InterfaceActionInvoker1< OscMessage_t2764280154 * >::Invoke(0 /* System.Void TUIOsharp.DataProcessors.IDataProcessor::ProcessMessage(OSCsharp.Data.OscMessage) */, IDataProcessor_t2214055399_il2cpp_TypeInfo_var, L_7, L_8);
		int32_t L_9 = V_1;
		V_1 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0034:
	{
		int32_t L_10 = V_1;
		int32_t L_11 = V_0;
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_001e;
		}
	}
	{
		return;
	}
}
// System.Void TUIOsharp.TuioServer::handlerOscErrorOccured(System.Object,OSCsharp.Utils.ExceptionEventArgs)
extern "C"  void TuioServer_handlerOscErrorOccured_m1202150247 (TuioServer_t595520884 * __this, RuntimeObject * ___sender0, ExceptionEventArgs_t892713536 * ___exceptionEventArgs1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TuioServer_handlerOscErrorOccured_m1202150247_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		EventHandler_1_t3778988004 * L_0 = __this->get_ErrorOccured_0();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		EventHandler_1_t3778988004 * L_1 = __this->get_ErrorOccured_0();
		ExceptionEventArgs_t892713536 * L_2 = ___exceptionEventArgs1;
		NullCheck(L_1);
		EventHandler_1_Invoke_m3855877691(L_1, __this, L_2, /*hidden argument*/EventHandler_1_Invoke_m3855877691_RuntimeMethod_var);
	}

IL_0015:
	{
		return;
	}
}
// System.Void TUIOsharp.TuioServer::handlerOscMessageReceived(System.Object,OSCsharp.Net.OscMessageReceivedEventArgs)
extern "C"  void TuioServer_handlerOscMessageReceived_m1229429870 (TuioServer_t595520884 * __this, RuntimeObject * ___sender0, OscMessageReceivedEventArgs_t1263041860 * ___oscMessageReceivedEventArgs1, const RuntimeMethod* method)
{
	{
		OscMessageReceivedEventArgs_t1263041860 * L_0 = ___oscMessageReceivedEventArgs1;
		NullCheck(L_0);
		OscMessage_t2764280154 * L_1 = OscMessageReceivedEventArgs_get_Message_m3135203290(L_0, /*hidden argument*/NULL);
		TuioServer_processMessage_m3007744804(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
