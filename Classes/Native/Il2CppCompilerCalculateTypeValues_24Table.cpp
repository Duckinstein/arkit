﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.String
struct String_t;
// IBM.Watson.DeveloperCloud.Utilities.DataCache
struct DataCache_t4250340070;
// FullSerializer.fsSerializer
struct fsSerializer_t4193731081;
// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Environment[]
struct EnvironmentU5BU5D_t1764741972;
// IBM.Watson.DeveloperCloud.Services.DeepQA.v1.DeepQA
struct DeepQA_t4122718210;
// IBM.Watson.DeveloperCloud.Services.ServiceStatus
struct ServiceStatus_t1443707987;
// IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Question
struct Question_t4096007710;
// IBM.Watson.DeveloperCloud.Services.DeepQA.v1.QuestionClass[]
struct QuestionClassU5BU5D_t1591382127;
// IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Response[]
struct ResponseU5BU5D_t1185773764;
// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Notice[]
struct NoticeU5BU5D_t1969629099;
// IBM.Watson.DeveloperCloud.Services.Discovery.v1.IndexCapacity
struct IndexCapacity_t726684902;
// IBM.Watson.DeveloperCloud.Services.Discovery.v1.DiskUsage
struct DiskUsage_t2133232754;
// IBM.Watson.DeveloperCloud.Services.Discovery.v1.MemoryUsage
struct MemoryUsage_t943632312;
// IBM.Watson.DeveloperCloud.Services.DeepQA.v1.SynSet[]
struct SynSetU5BU5D_t3100229505;
// IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Evidence[]
struct EvidenceU5BU5D_t1065303392;
// System.String[]
struct StringU5BU5D_t1642385972;
// IBM.Watson.DeveloperCloud.Services.DeepQA.v1.MetaDataMap
struct MetaDataMap_t2304952903;
// IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Synonym[]
struct SynonymU5BU5D_t1136100122;
// IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Slots
struct Slots_t3709295365;
// IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Word[]
struct WordU5BU5D_t1466090387;
// IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Value[]
struct ValueU5BU5D_t1766980932;
// IBM.Watson.DeveloperCloud.Services.DeepQA.v1.SynonymList[]
struct SynonymListU5BU5D_t2814318124;
// IBM.Watson.DeveloperCloud.Services.DeepQA.v1.ParseTree[]
struct ParseTreeU5BU5D_t639762008;
// IBM.Watson.DeveloperCloud.Services.DeepQA.v1.EvidenceRequest
struct EvidenceRequest_t2807863460;
// IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Answer[]
struct AnswerU5BU5D_t975328213;
// IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Answer/Cell[]
struct CellU5BU5D_t1882260018;
// IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Answer/Row[]
struct RowU5BU5D_t659188306;
// IBM.Watson.DeveloperCloud.Services.Discovery.v1.CollectionRef[]
struct CollectionRefU5BU5D_t2497189142;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Collections.Generic.Dictionary`2<System.String,IBM.Watson.DeveloperCloud.Connection.RESTConnector/Form>
struct Dictionary_2_t2694055125;
// IBM.Watson.DeveloperCloud.Connection.RESTConnector/ResponseEvent
struct ResponseEvent_t1616568356;
// IBM.Watson.DeveloperCloud.Connection.RESTConnector/ProgressEvent
struct ProgressEvent_t4185145044;
// IBM.Watson.DeveloperCloud.Services.Discovery.v1.DocumentCounts
struct DocumentCounts_t1301920119;
// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Conversions
struct Conversions_t3726444191;
// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Enrichment[]
struct EnrichmentU5BU5D_t3336389812;
// IBM.Watson.DeveloperCloud.Services.Discovery.v1.NormalizationOperation[]
struct NormalizationOperationU5BU5D_t1906606437;
// IBM.Watson.DeveloperCloud.Services.Discovery.v1.PdfSettings
struct PdfSettings_t1519277167;
// IBM.Watson.DeveloperCloud.Services.Discovery.v1.WordSettings
struct WordSettings_t2098347111;
// IBM.Watson.DeveloperCloud.Services.Discovery.v1.HtmlSettings
struct HtmlSettings_t99959826;
// IBM.Watson.DeveloperCloud.Services.Discovery.v1.PdfHeadingDetection
struct PdfHeadingDetection_t2648201765;
// IBM.Watson.DeveloperCloud.Services.Discovery.v1.ConfigurationRef[]
struct ConfigurationRefU5BU5D_t2410926920;
// IBM.Watson.DeveloperCloud.Services.Discovery.v1.FontSetting[]
struct FontSettingU5BU5D_t2456190136;
// IBM.Watson.DeveloperCloud.Services.Discovery.v1.WordStyle[]
struct WordStyleU5BU5D_t1077171056;
// IBM.Watson.DeveloperCloud.Services.Discovery.v1.WordHeadingDetection
struct WordHeadingDetection_t139434985;
// IBM.Watson.DeveloperCloud.Services.Discovery.v1.XPathPatterns
struct XPathPatterns_t1498040530;
// IBM.Watson.DeveloperCloud.Services.Discovery.v1.EnrichmentOptions
struct EnrichmentOptions_t3455576757;
// IBM.Watson.DeveloperCloud.Services.Conversation.v1.LogMessageResponse[]
struct LogMessageResponseU5BU5D_t1330322083;
// IBM.Watson.DeveloperCloud.Services.Conversation.v1.Conversation
struct Conversation_t105466997;
// IBM.Watson.DeveloperCloud.Services.Conversation.v1.InputData
struct InputData_t902315192;
// IBM.Watson.DeveloperCloud.Services.Conversation.v1.Context
struct Context_t2592795451;
// IBM.Watson.DeveloperCloud.Services.Conversation.v1.EntityResponse[]
struct EntityResponseU5BU5D_t480157455;
// IBM.Watson.DeveloperCloud.Services.Conversation.v1.Intent[]
struct IntentU5BU5D_t4227563387;
// IBM.Watson.DeveloperCloud.Services.Conversation.v1.OutputData
struct OutputData_t497470723;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// IBM.Watson.DeveloperCloud.Services.Conversation.v1.SystemResponse
struct SystemResponse_t541191420;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Docs[]
struct DocsU5BU5D_t3724963220;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Source
struct Source_t916780923;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Original
struct Original_t3571140033;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Enriched
struct Enriched_t407156716;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Decoded
struct Decoded_t1922409334;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Result
struct Result_t1515462031;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Keyword[]
struct KeywordU5BU5D_t3893757366;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Entity[]
struct EntityU5BU5D_t3969836080;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Concept[]
struct ConceptU5BU5D_t3502654871;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Relation[]
struct RelationU5BU5D_t1525062961;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.DocSentiment
struct DocSentiment_t2354958393;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Taxonomy[]
struct TaxonomyU5BU5D_t3261933456;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.URL
struct URL_t4128228189;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.ImageKeyword[]
struct ImageKeywordU5BU5D_t3897519977;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Feed[]
struct FeedU5BU5D_t332386033;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.PublicationDate
struct PublicationDate_t608222142;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Relation
struct Relation_t2336466576;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Taxonomy
struct Taxonomy_t1527601757;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.EnrichedTitle[]
struct EnrichedTitleU5BU5D_t3938334697;
// IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.Entity[]
struct EntityU5BU5D_t3416566442;
// IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.Example[]
struct ExampleU5BU5D_t854620307;
// IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.EntityExample
struct EntityExample_t3008277273;
// IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.DialogNode[]
struct DialogNodeU5BU5D_t270372031;
// IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.Output
struct Output_t301201853;
// IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.GoTo
struct GoTo_t52763683;
// IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.Workspace[]
struct WorkspaceU5BU5D_t2127228610;
// IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.MessageIntent[]
struct MessageIntentU5BU5D_t2945543646;
// IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.EntityExample[]
struct EntityExampleU5BU5D_t4277940324;
// IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.ConversationExperimental
struct ConversationExperimental_t215658501;
// IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.Intent[]
struct IntentU5BU5D_t168093507;
// IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.ConversationExperimental/OnMessage
struct OnMessage_t1296011603;
// System.Void
struct Void_t1841601450;
// IBM.Watson.DeveloperCloud.Services.DeepQA.v1.DeepQA/OnQuestion
struct OnQuestion_t3675891438;
// IBM.Watson.DeveloperCloud.Services.Conversation.v1.Conversation/OnMessage
struct OnMessage_t3665388051;
// IBM.Watson.DeveloperCloud.Services.Conversation.v1.MessageRequest
struct MessageRequest_t1934991468;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1572802995;
// IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.MessageResponse
struct MessageResponse_t2645798626;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.Workspaces
struct Workspaces_t1916725860;
// IBM.Watson.DeveloperCloud.Services.Conversation.v1.MessageResponse
struct MessageResponse_t470867978;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef DEEPQA_T4122718210_H
#define DEEPQA_T4122718210_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.DeepQA.v1.DeepQA
struct  DeepQA_t4122718210  : public RuntimeObject
{
public:
	// System.Boolean IBM.Watson.DeveloperCloud.Services.DeepQA.v1.DeepQA::<DisableCache>k__BackingField
	bool ___U3CDisableCacheU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.DeepQA.v1.DeepQA::m_ServiceId
	String_t* ___m_ServiceId_1;
	// IBM.Watson.DeveloperCloud.Utilities.DataCache IBM.Watson.DeveloperCloud.Services.DeepQA.v1.DeepQA::m_QuestionCache
	DataCache_t4250340070 * ___m_QuestionCache_2;

public:
	inline static int32_t get_offset_of_U3CDisableCacheU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DeepQA_t4122718210, ___U3CDisableCacheU3Ek__BackingField_0)); }
	inline bool get_U3CDisableCacheU3Ek__BackingField_0() const { return ___U3CDisableCacheU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CDisableCacheU3Ek__BackingField_0() { return &___U3CDisableCacheU3Ek__BackingField_0; }
	inline void set_U3CDisableCacheU3Ek__BackingField_0(bool value)
	{
		___U3CDisableCacheU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_m_ServiceId_1() { return static_cast<int32_t>(offsetof(DeepQA_t4122718210, ___m_ServiceId_1)); }
	inline String_t* get_m_ServiceId_1() const { return ___m_ServiceId_1; }
	inline String_t** get_address_of_m_ServiceId_1() { return &___m_ServiceId_1; }
	inline void set_m_ServiceId_1(String_t* value)
	{
		___m_ServiceId_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_ServiceId_1), value);
	}

	inline static int32_t get_offset_of_m_QuestionCache_2() { return static_cast<int32_t>(offsetof(DeepQA_t4122718210, ___m_QuestionCache_2)); }
	inline DataCache_t4250340070 * get_m_QuestionCache_2() const { return ___m_QuestionCache_2; }
	inline DataCache_t4250340070 ** get_address_of_m_QuestionCache_2() { return &___m_QuestionCache_2; }
	inline void set_m_QuestionCache_2(DataCache_t4250340070 * value)
	{
		___m_QuestionCache_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_QuestionCache_2), value);
	}
};

struct DeepQA_t4122718210_StaticFields
{
public:
	// FullSerializer.fsSerializer IBM.Watson.DeveloperCloud.Services.DeepQA.v1.DeepQA::sm_Serializer
	fsSerializer_t4193731081 * ___sm_Serializer_3;

public:
	inline static int32_t get_offset_of_sm_Serializer_3() { return static_cast<int32_t>(offsetof(DeepQA_t4122718210_StaticFields, ___sm_Serializer_3)); }
	inline fsSerializer_t4193731081 * get_sm_Serializer_3() const { return ___sm_Serializer_3; }
	inline fsSerializer_t4193731081 ** get_address_of_sm_Serializer_3() { return &___sm_Serializer_3; }
	inline void set_sm_Serializer_3(fsSerializer_t4193731081 * value)
	{
		___sm_Serializer_3 = value;
		Il2CppCodeGenWriteBarrier((&___sm_Serializer_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEEPQA_T4122718210_H
#ifndef GETENVIRONMENTSRESPONSE_T3378403827_H
#define GETENVIRONMENTSRESPONSE_T3378403827_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.GetEnvironmentsResponse
struct  GetEnvironmentsResponse_t3378403827  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Environment[] IBM.Watson.DeveloperCloud.Services.Discovery.v1.GetEnvironmentsResponse::<environments>k__BackingField
	EnvironmentU5BU5D_t1764741972* ___U3CenvironmentsU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CenvironmentsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(GetEnvironmentsResponse_t3378403827, ___U3CenvironmentsU3Ek__BackingField_0)); }
	inline EnvironmentU5BU5D_t1764741972* get_U3CenvironmentsU3Ek__BackingField_0() const { return ___U3CenvironmentsU3Ek__BackingField_0; }
	inline EnvironmentU5BU5D_t1764741972** get_address_of_U3CenvironmentsU3Ek__BackingField_0() { return &___U3CenvironmentsU3Ek__BackingField_0; }
	inline void set_U3CenvironmentsU3Ek__BackingField_0(EnvironmentU5BU5D_t1764741972* value)
	{
		___U3CenvironmentsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CenvironmentsU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETENVIRONMENTSRESPONSE_T3378403827_H
#ifndef CHECKSERVICESTATUS_T2304978370_H
#define CHECKSERVICESTATUS_T2304978370_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.DeepQA.v1.DeepQA/CheckServiceStatus
struct  CheckServiceStatus_t2304978370  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.DeepQA.v1.DeepQA IBM.Watson.DeveloperCloud.Services.DeepQA.v1.DeepQA/CheckServiceStatus::m_Service
	DeepQA_t4122718210 * ___m_Service_0;
	// IBM.Watson.DeveloperCloud.Services.ServiceStatus IBM.Watson.DeveloperCloud.Services.DeepQA.v1.DeepQA/CheckServiceStatus::m_Callback
	ServiceStatus_t1443707987 * ___m_Callback_1;

public:
	inline static int32_t get_offset_of_m_Service_0() { return static_cast<int32_t>(offsetof(CheckServiceStatus_t2304978370, ___m_Service_0)); }
	inline DeepQA_t4122718210 * get_m_Service_0() const { return ___m_Service_0; }
	inline DeepQA_t4122718210 ** get_address_of_m_Service_0() { return &___m_Service_0; }
	inline void set_m_Service_0(DeepQA_t4122718210 * value)
	{
		___m_Service_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Service_0), value);
	}

	inline static int32_t get_offset_of_m_Callback_1() { return static_cast<int32_t>(offsetof(CheckServiceStatus_t2304978370, ___m_Callback_1)); }
	inline ServiceStatus_t1443707987 * get_m_Callback_1() const { return ___m_Callback_1; }
	inline ServiceStatus_t1443707987 ** get_address_of_m_Callback_1() { return &___m_Callback_1; }
	inline void set_m_Callback_1(ServiceStatus_t1443707987 * value)
	{
		___m_Callback_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Callback_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHECKSERVICESTATUS_T2304978370_H
#ifndef QUESTIONCLASS_T734245290_H
#define QUESTIONCLASS_T734245290_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.DeepQA.v1.QuestionClass
struct  QuestionClass_t734245290  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.DeepQA.v1.QuestionClass::<out_of_domain>k__BackingField
	String_t* ___U3Cout_of_domainU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.DeepQA.v1.QuestionClass::<question>k__BackingField
	String_t* ___U3CquestionU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.DeepQA.v1.QuestionClass::<domain>k__BackingField
	String_t* ___U3CdomainU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3Cout_of_domainU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(QuestionClass_t734245290, ___U3Cout_of_domainU3Ek__BackingField_0)); }
	inline String_t* get_U3Cout_of_domainU3Ek__BackingField_0() const { return ___U3Cout_of_domainU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3Cout_of_domainU3Ek__BackingField_0() { return &___U3Cout_of_domainU3Ek__BackingField_0; }
	inline void set_U3Cout_of_domainU3Ek__BackingField_0(String_t* value)
	{
		___U3Cout_of_domainU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cout_of_domainU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CquestionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(QuestionClass_t734245290, ___U3CquestionU3Ek__BackingField_1)); }
	inline String_t* get_U3CquestionU3Ek__BackingField_1() const { return ___U3CquestionU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CquestionU3Ek__BackingField_1() { return &___U3CquestionU3Ek__BackingField_1; }
	inline void set_U3CquestionU3Ek__BackingField_1(String_t* value)
	{
		___U3CquestionU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CquestionU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CdomainU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(QuestionClass_t734245290, ___U3CdomainU3Ek__BackingField_2)); }
	inline String_t* get_U3CdomainU3Ek__BackingField_2() const { return ___U3CdomainU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CdomainU3Ek__BackingField_2() { return &___U3CdomainU3Ek__BackingField_2; }
	inline void set_U3CdomainU3Ek__BackingField_2(String_t* value)
	{
		___U3CdomainU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdomainU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUESTIONCLASS_T734245290_H
#ifndef RESPONSE_T4265902649_H
#define RESPONSE_T4265902649_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Response
struct  Response_t4265902649  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Question IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Response::<question>k__BackingField
	Question_t4096007710 * ___U3CquestionU3Ek__BackingField_0;
	// IBM.Watson.DeveloperCloud.Services.DeepQA.v1.QuestionClass[] IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Response::<questionClasses>k__BackingField
	QuestionClassU5BU5D_t1591382127* ___U3CquestionClassesU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CquestionU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Response_t4265902649, ___U3CquestionU3Ek__BackingField_0)); }
	inline Question_t4096007710 * get_U3CquestionU3Ek__BackingField_0() const { return ___U3CquestionU3Ek__BackingField_0; }
	inline Question_t4096007710 ** get_address_of_U3CquestionU3Ek__BackingField_0() { return &___U3CquestionU3Ek__BackingField_0; }
	inline void set_U3CquestionU3Ek__BackingField_0(Question_t4096007710 * value)
	{
		___U3CquestionU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CquestionU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CquestionClassesU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Response_t4265902649, ___U3CquestionClassesU3Ek__BackingField_1)); }
	inline QuestionClassU5BU5D_t1591382127* get_U3CquestionClassesU3Ek__BackingField_1() const { return ___U3CquestionClassesU3Ek__BackingField_1; }
	inline QuestionClassU5BU5D_t1591382127** get_address_of_U3CquestionClassesU3Ek__BackingField_1() { return &___U3CquestionClassesU3Ek__BackingField_1; }
	inline void set_U3CquestionClassesU3Ek__BackingField_1(QuestionClassU5BU5D_t1591382127* value)
	{
		___U3CquestionClassesU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CquestionClassesU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESPONSE_T4265902649_H
#ifndef RESPONSELIST_T276562843_H
#define RESPONSELIST_T276562843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.DeepQA.v1.ResponseList
struct  ResponseList_t276562843  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Response[] IBM.Watson.DeveloperCloud.Services.DeepQA.v1.ResponseList::<responses>k__BackingField
	ResponseU5BU5D_t1185773764* ___U3CresponsesU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CresponsesU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ResponseList_t276562843, ___U3CresponsesU3Ek__BackingField_0)); }
	inline ResponseU5BU5D_t1185773764* get_U3CresponsesU3Ek__BackingField_0() const { return ___U3CresponsesU3Ek__BackingField_0; }
	inline ResponseU5BU5D_t1185773764** get_address_of_U3CresponsesU3Ek__BackingField_0() { return &___U3CresponsesU3Ek__BackingField_0; }
	inline void set_U3CresponsesU3Ek__BackingField_0(ResponseU5BU5D_t1185773764* value)
	{
		___U3CresponsesU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CresponsesU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESPONSELIST_T276562843_H
#ifndef MEMORYUSAGE_T943632312_H
#define MEMORYUSAGE_T943632312_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.MemoryUsage
struct  MemoryUsage_t943632312  : public RuntimeObject
{
public:
	// System.UInt32 IBM.Watson.DeveloperCloud.Services.Discovery.v1.MemoryUsage::<used_bytes>k__BackingField
	uint32_t ___U3Cused_bytesU3Ek__BackingField_0;
	// System.UInt32 IBM.Watson.DeveloperCloud.Services.Discovery.v1.MemoryUsage::<total_bytes>k__BackingField
	uint32_t ___U3Ctotal_bytesU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.MemoryUsage::<used>k__BackingField
	String_t* ___U3CusedU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.MemoryUsage::<total>k__BackingField
	String_t* ___U3CtotalU3Ek__BackingField_3;
	// System.Double IBM.Watson.DeveloperCloud.Services.Discovery.v1.MemoryUsage::<percent_used>k__BackingField
	double ___U3Cpercent_usedU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3Cused_bytesU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MemoryUsage_t943632312, ___U3Cused_bytesU3Ek__BackingField_0)); }
	inline uint32_t get_U3Cused_bytesU3Ek__BackingField_0() const { return ___U3Cused_bytesU3Ek__BackingField_0; }
	inline uint32_t* get_address_of_U3Cused_bytesU3Ek__BackingField_0() { return &___U3Cused_bytesU3Ek__BackingField_0; }
	inline void set_U3Cused_bytesU3Ek__BackingField_0(uint32_t value)
	{
		___U3Cused_bytesU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3Ctotal_bytesU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(MemoryUsage_t943632312, ___U3Ctotal_bytesU3Ek__BackingField_1)); }
	inline uint32_t get_U3Ctotal_bytesU3Ek__BackingField_1() const { return ___U3Ctotal_bytesU3Ek__BackingField_1; }
	inline uint32_t* get_address_of_U3Ctotal_bytesU3Ek__BackingField_1() { return &___U3Ctotal_bytesU3Ek__BackingField_1; }
	inline void set_U3Ctotal_bytesU3Ek__BackingField_1(uint32_t value)
	{
		___U3Ctotal_bytesU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CusedU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(MemoryUsage_t943632312, ___U3CusedU3Ek__BackingField_2)); }
	inline String_t* get_U3CusedU3Ek__BackingField_2() const { return ___U3CusedU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CusedU3Ek__BackingField_2() { return &___U3CusedU3Ek__BackingField_2; }
	inline void set_U3CusedU3Ek__BackingField_2(String_t* value)
	{
		___U3CusedU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CusedU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CtotalU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(MemoryUsage_t943632312, ___U3CtotalU3Ek__BackingField_3)); }
	inline String_t* get_U3CtotalU3Ek__BackingField_3() const { return ___U3CtotalU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CtotalU3Ek__BackingField_3() { return &___U3CtotalU3Ek__BackingField_3; }
	inline void set_U3CtotalU3Ek__BackingField_3(String_t* value)
	{
		___U3CtotalU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtotalU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3Cpercent_usedU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(MemoryUsage_t943632312, ___U3Cpercent_usedU3Ek__BackingField_4)); }
	inline double get_U3Cpercent_usedU3Ek__BackingField_4() const { return ___U3Cpercent_usedU3Ek__BackingField_4; }
	inline double* get_address_of_U3Cpercent_usedU3Ek__BackingField_4() { return &___U3Cpercent_usedU3Ek__BackingField_4; }
	inline void set_U3Cpercent_usedU3Ek__BackingField_4(double value)
	{
		___U3Cpercent_usedU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMORYUSAGE_T943632312_H
#ifndef DELETEENVIRONMENTRESPONSE_T1123256339_H
#define DELETEENVIRONMENTRESPONSE_T1123256339_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.DeleteEnvironmentResponse
struct  DeleteEnvironmentResponse_t1123256339  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.DeleteEnvironmentResponse::<environment_id>k__BackingField
	String_t* ___U3Cenvironment_idU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.DeleteEnvironmentResponse::<status>k__BackingField
	String_t* ___U3CstatusU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3Cenvironment_idU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DeleteEnvironmentResponse_t1123256339, ___U3Cenvironment_idU3Ek__BackingField_0)); }
	inline String_t* get_U3Cenvironment_idU3Ek__BackingField_0() const { return ___U3Cenvironment_idU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3Cenvironment_idU3Ek__BackingField_0() { return &___U3Cenvironment_idU3Ek__BackingField_0; }
	inline void set_U3Cenvironment_idU3Ek__BackingField_0(String_t* value)
	{
		___U3Cenvironment_idU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cenvironment_idU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CstatusU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(DeleteEnvironmentResponse_t1123256339, ___U3CstatusU3Ek__BackingField_1)); }
	inline String_t* get_U3CstatusU3Ek__BackingField_1() const { return ___U3CstatusU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CstatusU3Ek__BackingField_1() { return &___U3CstatusU3Ek__BackingField_1; }
	inline void set_U3CstatusU3Ek__BackingField_1(String_t* value)
	{
		___U3CstatusU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstatusU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELETEENVIRONMENTRESPONSE_T1123256339_H
#ifndef TESTDOCUMENT_T1009335169_H
#define TESTDOCUMENT_T1009335169_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.TestDocument
struct  TestDocument_t1009335169  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.TestDocument::<configuration_id>k__BackingField
	String_t* ___U3Cconfiguration_idU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.TestDocument::<status>k__BackingField
	String_t* ___U3CstatusU3Ek__BackingField_1;
	// System.Double IBM.Watson.DeveloperCloud.Services.Discovery.v1.TestDocument::<enriched_field_units>k__BackingField
	double ___U3Cenriched_field_unitsU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.TestDocument::<original_media_type>k__BackingField
	String_t* ___U3Coriginal_media_typeU3Ek__BackingField_3;
	// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Notice[] IBM.Watson.DeveloperCloud.Services.Discovery.v1.TestDocument::<notices>k__BackingField
	NoticeU5BU5D_t1969629099* ___U3CnoticesU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3Cconfiguration_idU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TestDocument_t1009335169, ___U3Cconfiguration_idU3Ek__BackingField_0)); }
	inline String_t* get_U3Cconfiguration_idU3Ek__BackingField_0() const { return ___U3Cconfiguration_idU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3Cconfiguration_idU3Ek__BackingField_0() { return &___U3Cconfiguration_idU3Ek__BackingField_0; }
	inline void set_U3Cconfiguration_idU3Ek__BackingField_0(String_t* value)
	{
		___U3Cconfiguration_idU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cconfiguration_idU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CstatusU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TestDocument_t1009335169, ___U3CstatusU3Ek__BackingField_1)); }
	inline String_t* get_U3CstatusU3Ek__BackingField_1() const { return ___U3CstatusU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CstatusU3Ek__BackingField_1() { return &___U3CstatusU3Ek__BackingField_1; }
	inline void set_U3CstatusU3Ek__BackingField_1(String_t* value)
	{
		___U3CstatusU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstatusU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3Cenriched_field_unitsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(TestDocument_t1009335169, ___U3Cenriched_field_unitsU3Ek__BackingField_2)); }
	inline double get_U3Cenriched_field_unitsU3Ek__BackingField_2() const { return ___U3Cenriched_field_unitsU3Ek__BackingField_2; }
	inline double* get_address_of_U3Cenriched_field_unitsU3Ek__BackingField_2() { return &___U3Cenriched_field_unitsU3Ek__BackingField_2; }
	inline void set_U3Cenriched_field_unitsU3Ek__BackingField_2(double value)
	{
		___U3Cenriched_field_unitsU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3Coriginal_media_typeU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(TestDocument_t1009335169, ___U3Coriginal_media_typeU3Ek__BackingField_3)); }
	inline String_t* get_U3Coriginal_media_typeU3Ek__BackingField_3() const { return ___U3Coriginal_media_typeU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3Coriginal_media_typeU3Ek__BackingField_3() { return &___U3Coriginal_media_typeU3Ek__BackingField_3; }
	inline void set_U3Coriginal_media_typeU3Ek__BackingField_3(String_t* value)
	{
		___U3Coriginal_media_typeU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3Coriginal_media_typeU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CnoticesU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(TestDocument_t1009335169, ___U3CnoticesU3Ek__BackingField_4)); }
	inline NoticeU5BU5D_t1969629099* get_U3CnoticesU3Ek__BackingField_4() const { return ___U3CnoticesU3Ek__BackingField_4; }
	inline NoticeU5BU5D_t1969629099** get_address_of_U3CnoticesU3Ek__BackingField_4() { return &___U3CnoticesU3Ek__BackingField_4; }
	inline void set_U3CnoticesU3Ek__BackingField_4(NoticeU5BU5D_t1969629099* value)
	{
		___U3CnoticesU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnoticesU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TESTDOCUMENT_T1009335169_H
#ifndef ENVIRONMENT_T3491688169_H
#define ENVIRONMENT_T3491688169_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Environment
struct  Environment_t3491688169  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Environment::<environment_id>k__BackingField
	String_t* ___U3Cenvironment_idU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Environment::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Environment::<description>k__BackingField
	String_t* ___U3CdescriptionU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Environment::<created>k__BackingField
	String_t* ___U3CcreatedU3Ek__BackingField_3;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Environment::<updated>k__BackingField
	String_t* ___U3CupdatedU3Ek__BackingField_4;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Environment::<status>k__BackingField
	String_t* ___U3CstatusU3Ek__BackingField_5;
	// System.Boolean IBM.Watson.DeveloperCloud.Services.Discovery.v1.Environment::<read_only>k__BackingField
	bool ___U3Cread_onlyU3Ek__BackingField_6;
	// IBM.Watson.DeveloperCloud.Services.Discovery.v1.IndexCapacity IBM.Watson.DeveloperCloud.Services.Discovery.v1.Environment::<index_capacity>k__BackingField
	IndexCapacity_t726684902 * ___U3Cindex_capacityU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_U3Cenvironment_idU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Environment_t3491688169, ___U3Cenvironment_idU3Ek__BackingField_0)); }
	inline String_t* get_U3Cenvironment_idU3Ek__BackingField_0() const { return ___U3Cenvironment_idU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3Cenvironment_idU3Ek__BackingField_0() { return &___U3Cenvironment_idU3Ek__BackingField_0; }
	inline void set_U3Cenvironment_idU3Ek__BackingField_0(String_t* value)
	{
		___U3Cenvironment_idU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cenvironment_idU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Environment_t3491688169, ___U3CnameU3Ek__BackingField_1)); }
	inline String_t* get_U3CnameU3Ek__BackingField_1() const { return ___U3CnameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_1() { return &___U3CnameU3Ek__BackingField_1; }
	inline void set_U3CnameU3Ek__BackingField_1(String_t* value)
	{
		___U3CnameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CdescriptionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Environment_t3491688169, ___U3CdescriptionU3Ek__BackingField_2)); }
	inline String_t* get_U3CdescriptionU3Ek__BackingField_2() const { return ___U3CdescriptionU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CdescriptionU3Ek__BackingField_2() { return &___U3CdescriptionU3Ek__BackingField_2; }
	inline void set_U3CdescriptionU3Ek__BackingField_2(String_t* value)
	{
		___U3CdescriptionU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdescriptionU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CcreatedU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Environment_t3491688169, ___U3CcreatedU3Ek__BackingField_3)); }
	inline String_t* get_U3CcreatedU3Ek__BackingField_3() const { return ___U3CcreatedU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CcreatedU3Ek__BackingField_3() { return &___U3CcreatedU3Ek__BackingField_3; }
	inline void set_U3CcreatedU3Ek__BackingField_3(String_t* value)
	{
		___U3CcreatedU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcreatedU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CupdatedU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Environment_t3491688169, ___U3CupdatedU3Ek__BackingField_4)); }
	inline String_t* get_U3CupdatedU3Ek__BackingField_4() const { return ___U3CupdatedU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CupdatedU3Ek__BackingField_4() { return &___U3CupdatedU3Ek__BackingField_4; }
	inline void set_U3CupdatedU3Ek__BackingField_4(String_t* value)
	{
		___U3CupdatedU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CupdatedU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CstatusU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Environment_t3491688169, ___U3CstatusU3Ek__BackingField_5)); }
	inline String_t* get_U3CstatusU3Ek__BackingField_5() const { return ___U3CstatusU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CstatusU3Ek__BackingField_5() { return &___U3CstatusU3Ek__BackingField_5; }
	inline void set_U3CstatusU3Ek__BackingField_5(String_t* value)
	{
		___U3CstatusU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstatusU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3Cread_onlyU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Environment_t3491688169, ___U3Cread_onlyU3Ek__BackingField_6)); }
	inline bool get_U3Cread_onlyU3Ek__BackingField_6() const { return ___U3Cread_onlyU3Ek__BackingField_6; }
	inline bool* get_address_of_U3Cread_onlyU3Ek__BackingField_6() { return &___U3Cread_onlyU3Ek__BackingField_6; }
	inline void set_U3Cread_onlyU3Ek__BackingField_6(bool value)
	{
		___U3Cread_onlyU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3Cindex_capacityU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(Environment_t3491688169, ___U3Cindex_capacityU3Ek__BackingField_7)); }
	inline IndexCapacity_t726684902 * get_U3Cindex_capacityU3Ek__BackingField_7() const { return ___U3Cindex_capacityU3Ek__BackingField_7; }
	inline IndexCapacity_t726684902 ** get_address_of_U3Cindex_capacityU3Ek__BackingField_7() { return &___U3Cindex_capacityU3Ek__BackingField_7; }
	inline void set_U3Cindex_capacityU3Ek__BackingField_7(IndexCapacity_t726684902 * value)
	{
		___U3Cindex_capacityU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cindex_capacityU3Ek__BackingField_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENVIRONMENT_T3491688169_H
#ifndef INDEXCAPACITY_T726684902_H
#define INDEXCAPACITY_T726684902_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.IndexCapacity
struct  IndexCapacity_t726684902  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.Discovery.v1.DiskUsage IBM.Watson.DeveloperCloud.Services.Discovery.v1.IndexCapacity::<disk_usage>k__BackingField
	DiskUsage_t2133232754 * ___U3Cdisk_usageU3Ek__BackingField_0;
	// IBM.Watson.DeveloperCloud.Services.Discovery.v1.MemoryUsage IBM.Watson.DeveloperCloud.Services.Discovery.v1.IndexCapacity::<memory_usage>k__BackingField
	MemoryUsage_t943632312 * ___U3Cmemory_usageU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3Cdisk_usageU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(IndexCapacity_t726684902, ___U3Cdisk_usageU3Ek__BackingField_0)); }
	inline DiskUsage_t2133232754 * get_U3Cdisk_usageU3Ek__BackingField_0() const { return ___U3Cdisk_usageU3Ek__BackingField_0; }
	inline DiskUsage_t2133232754 ** get_address_of_U3Cdisk_usageU3Ek__BackingField_0() { return &___U3Cdisk_usageU3Ek__BackingField_0; }
	inline void set_U3Cdisk_usageU3Ek__BackingField_0(DiskUsage_t2133232754 * value)
	{
		___U3Cdisk_usageU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cdisk_usageU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3Cmemory_usageU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(IndexCapacity_t726684902, ___U3Cmemory_usageU3Ek__BackingField_1)); }
	inline MemoryUsage_t943632312 * get_U3Cmemory_usageU3Ek__BackingField_1() const { return ___U3Cmemory_usageU3Ek__BackingField_1; }
	inline MemoryUsage_t943632312 ** get_address_of_U3Cmemory_usageU3Ek__BackingField_1() { return &___U3Cmemory_usageU3Ek__BackingField_1; }
	inline void set_U3Cmemory_usageU3Ek__BackingField_1(MemoryUsage_t943632312 * value)
	{
		___U3Cmemory_usageU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cmemory_usageU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INDEXCAPACITY_T726684902_H
#ifndef DISKUSAGE_T2133232754_H
#define DISKUSAGE_T2133232754_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.DiskUsage
struct  DiskUsage_t2133232754  : public RuntimeObject
{
public:
	// System.UInt32 IBM.Watson.DeveloperCloud.Services.Discovery.v1.DiskUsage::<used_bytes>k__BackingField
	uint32_t ___U3Cused_bytesU3Ek__BackingField_0;
	// System.UInt32 IBM.Watson.DeveloperCloud.Services.Discovery.v1.DiskUsage::<total_bytes>k__BackingField
	uint32_t ___U3Ctotal_bytesU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.DiskUsage::<used>k__BackingField
	String_t* ___U3CusedU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.DiskUsage::<total>k__BackingField
	String_t* ___U3CtotalU3Ek__BackingField_3;
	// System.Double IBM.Watson.DeveloperCloud.Services.Discovery.v1.DiskUsage::<percent_used>k__BackingField
	double ___U3Cpercent_usedU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3Cused_bytesU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DiskUsage_t2133232754, ___U3Cused_bytesU3Ek__BackingField_0)); }
	inline uint32_t get_U3Cused_bytesU3Ek__BackingField_0() const { return ___U3Cused_bytesU3Ek__BackingField_0; }
	inline uint32_t* get_address_of_U3Cused_bytesU3Ek__BackingField_0() { return &___U3Cused_bytesU3Ek__BackingField_0; }
	inline void set_U3Cused_bytesU3Ek__BackingField_0(uint32_t value)
	{
		___U3Cused_bytesU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3Ctotal_bytesU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(DiskUsage_t2133232754, ___U3Ctotal_bytesU3Ek__BackingField_1)); }
	inline uint32_t get_U3Ctotal_bytesU3Ek__BackingField_1() const { return ___U3Ctotal_bytesU3Ek__BackingField_1; }
	inline uint32_t* get_address_of_U3Ctotal_bytesU3Ek__BackingField_1() { return &___U3Ctotal_bytesU3Ek__BackingField_1; }
	inline void set_U3Ctotal_bytesU3Ek__BackingField_1(uint32_t value)
	{
		___U3Ctotal_bytesU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CusedU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(DiskUsage_t2133232754, ___U3CusedU3Ek__BackingField_2)); }
	inline String_t* get_U3CusedU3Ek__BackingField_2() const { return ___U3CusedU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CusedU3Ek__BackingField_2() { return &___U3CusedU3Ek__BackingField_2; }
	inline void set_U3CusedU3Ek__BackingField_2(String_t* value)
	{
		___U3CusedU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CusedU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CtotalU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(DiskUsage_t2133232754, ___U3CtotalU3Ek__BackingField_3)); }
	inline String_t* get_U3CtotalU3Ek__BackingField_3() const { return ___U3CtotalU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CtotalU3Ek__BackingField_3() { return &___U3CtotalU3Ek__BackingField_3; }
	inline void set_U3CtotalU3Ek__BackingField_3(String_t* value)
	{
		___U3CtotalU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtotalU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3Cpercent_usedU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(DiskUsage_t2133232754, ___U3Cpercent_usedU3Ek__BackingField_4)); }
	inline double get_U3Cpercent_usedU3Ek__BackingField_4() const { return ___U3Cpercent_usedU3Ek__BackingField_4; }
	inline double* get_address_of_U3Cpercent_usedU3Ek__BackingField_4() { return &___U3Cpercent_usedU3Ek__BackingField_4; }
	inline void set_U3Cpercent_usedU3Ek__BackingField_4(double value)
	{
		___U3Cpercent_usedU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISKUSAGE_T2133232754_H
#ifndef SYNONYMLIST_T1745302257_H
#define SYNONYMLIST_T1745302257_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.DeepQA.v1.SynonymList
struct  SynonymList_t1745302257  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.DeepQA.v1.SynonymList::<partOfSpeech>k__BackingField
	String_t* ___U3CpartOfSpeechU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.DeepQA.v1.SynonymList::<value>k__BackingField
	String_t* ___U3CvalueU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.DeepQA.v1.SynonymList::<lemma>k__BackingField
	String_t* ___U3ClemmaU3Ek__BackingField_2;
	// IBM.Watson.DeveloperCloud.Services.DeepQA.v1.SynSet[] IBM.Watson.DeveloperCloud.Services.DeepQA.v1.SynonymList::<synSet>k__BackingField
	SynSetU5BU5D_t3100229505* ___U3CsynSetU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CpartOfSpeechU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SynonymList_t1745302257, ___U3CpartOfSpeechU3Ek__BackingField_0)); }
	inline String_t* get_U3CpartOfSpeechU3Ek__BackingField_0() const { return ___U3CpartOfSpeechU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CpartOfSpeechU3Ek__BackingField_0() { return &___U3CpartOfSpeechU3Ek__BackingField_0; }
	inline void set_U3CpartOfSpeechU3Ek__BackingField_0(String_t* value)
	{
		___U3CpartOfSpeechU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpartOfSpeechU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CvalueU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(SynonymList_t1745302257, ___U3CvalueU3Ek__BackingField_1)); }
	inline String_t* get_U3CvalueU3Ek__BackingField_1() const { return ___U3CvalueU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CvalueU3Ek__BackingField_1() { return &___U3CvalueU3Ek__BackingField_1; }
	inline void set_U3CvalueU3Ek__BackingField_1(String_t* value)
	{
		___U3CvalueU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CvalueU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3ClemmaU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SynonymList_t1745302257, ___U3ClemmaU3Ek__BackingField_2)); }
	inline String_t* get_U3ClemmaU3Ek__BackingField_2() const { return ___U3ClemmaU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3ClemmaU3Ek__BackingField_2() { return &___U3ClemmaU3Ek__BackingField_2; }
	inline void set_U3ClemmaU3Ek__BackingField_2(String_t* value)
	{
		___U3ClemmaU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClemmaU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CsynSetU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SynonymList_t1745302257, ___U3CsynSetU3Ek__BackingField_3)); }
	inline SynSetU5BU5D_t3100229505* get_U3CsynSetU3Ek__BackingField_3() const { return ___U3CsynSetU3Ek__BackingField_3; }
	inline SynSetU5BU5D_t3100229505** get_address_of_U3CsynSetU3Ek__BackingField_3() { return &___U3CsynSetU3Ek__BackingField_3; }
	inline void set_U3CsynSetU3Ek__BackingField_3(SynSetU5BU5D_t3100229505* value)
	{
		___U3CsynSetU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsynSetU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYNONYMLIST_T1745302257_H
#ifndef EVIDENCEREQUEST_T2807863460_H
#define EVIDENCEREQUEST_T2807863460_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.DeepQA.v1.EvidenceRequest
struct  EvidenceRequest_t2807863460  : public RuntimeObject
{
public:
	// System.Int64 IBM.Watson.DeveloperCloud.Services.DeepQA.v1.EvidenceRequest::<items>k__BackingField
	int64_t ___U3CitemsU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.DeepQA.v1.EvidenceRequest::<profile>k__BackingField
	String_t* ___U3CprofileU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CitemsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(EvidenceRequest_t2807863460, ___U3CitemsU3Ek__BackingField_0)); }
	inline int64_t get_U3CitemsU3Ek__BackingField_0() const { return ___U3CitemsU3Ek__BackingField_0; }
	inline int64_t* get_address_of_U3CitemsU3Ek__BackingField_0() { return &___U3CitemsU3Ek__BackingField_0; }
	inline void set_U3CitemsU3Ek__BackingField_0(int64_t value)
	{
		___U3CitemsU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CprofileU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(EvidenceRequest_t2807863460, ___U3CprofileU3Ek__BackingField_1)); }
	inline String_t* get_U3CprofileU3Ek__BackingField_1() const { return ___U3CprofileU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CprofileU3Ek__BackingField_1() { return &___U3CprofileU3Ek__BackingField_1; }
	inline void set_U3CprofileU3Ek__BackingField_1(String_t* value)
	{
		___U3CprofileU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CprofileU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVIDENCEREQUEST_T2807863460_H
#ifndef ANSWER_T3626344892_H
#define ANSWER_T3626344892_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Answer
struct  Answer_t3626344892  : public RuntimeObject
{
public:
	// System.Int64 IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Answer::<id>k__BackingField
	int64_t ___U3CidU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Answer::<text>k__BackingField
	String_t* ___U3CtextU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Answer::<pipeline>k__BackingField
	String_t* ___U3CpipelineU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Answer::<formattedText>k__BackingField
	String_t* ___U3CformattedTextU3Ek__BackingField_3;
	// System.Double IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Answer::<confidence>k__BackingField
	double ___U3CconfidenceU3Ek__BackingField_4;
	// IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Evidence[] IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Answer::<evidence>k__BackingField
	EvidenceU5BU5D_t1065303392* ___U3CevidenceU3Ek__BackingField_5;
	// System.String[] IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Answer::<entityTypes>k__BackingField
	StringU5BU5D_t1642385972* ___U3CentityTypesU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CidU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Answer_t3626344892, ___U3CidU3Ek__BackingField_0)); }
	inline int64_t get_U3CidU3Ek__BackingField_0() const { return ___U3CidU3Ek__BackingField_0; }
	inline int64_t* get_address_of_U3CidU3Ek__BackingField_0() { return &___U3CidU3Ek__BackingField_0; }
	inline void set_U3CidU3Ek__BackingField_0(int64_t value)
	{
		___U3CidU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CtextU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Answer_t3626344892, ___U3CtextU3Ek__BackingField_1)); }
	inline String_t* get_U3CtextU3Ek__BackingField_1() const { return ___U3CtextU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CtextU3Ek__BackingField_1() { return &___U3CtextU3Ek__BackingField_1; }
	inline void set_U3CtextU3Ek__BackingField_1(String_t* value)
	{
		___U3CtextU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CpipelineU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Answer_t3626344892, ___U3CpipelineU3Ek__BackingField_2)); }
	inline String_t* get_U3CpipelineU3Ek__BackingField_2() const { return ___U3CpipelineU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CpipelineU3Ek__BackingField_2() { return &___U3CpipelineU3Ek__BackingField_2; }
	inline void set_U3CpipelineU3Ek__BackingField_2(String_t* value)
	{
		___U3CpipelineU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpipelineU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CformattedTextU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Answer_t3626344892, ___U3CformattedTextU3Ek__BackingField_3)); }
	inline String_t* get_U3CformattedTextU3Ek__BackingField_3() const { return ___U3CformattedTextU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CformattedTextU3Ek__BackingField_3() { return &___U3CformattedTextU3Ek__BackingField_3; }
	inline void set_U3CformattedTextU3Ek__BackingField_3(String_t* value)
	{
		___U3CformattedTextU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CformattedTextU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CconfidenceU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Answer_t3626344892, ___U3CconfidenceU3Ek__BackingField_4)); }
	inline double get_U3CconfidenceU3Ek__BackingField_4() const { return ___U3CconfidenceU3Ek__BackingField_4; }
	inline double* get_address_of_U3CconfidenceU3Ek__BackingField_4() { return &___U3CconfidenceU3Ek__BackingField_4; }
	inline void set_U3CconfidenceU3Ek__BackingField_4(double value)
	{
		___U3CconfidenceU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CevidenceU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Answer_t3626344892, ___U3CevidenceU3Ek__BackingField_5)); }
	inline EvidenceU5BU5D_t1065303392* get_U3CevidenceU3Ek__BackingField_5() const { return ___U3CevidenceU3Ek__BackingField_5; }
	inline EvidenceU5BU5D_t1065303392** get_address_of_U3CevidenceU3Ek__BackingField_5() { return &___U3CevidenceU3Ek__BackingField_5; }
	inline void set_U3CevidenceU3Ek__BackingField_5(EvidenceU5BU5D_t1065303392* value)
	{
		___U3CevidenceU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CevidenceU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CentityTypesU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Answer_t3626344892, ___U3CentityTypesU3Ek__BackingField_6)); }
	inline StringU5BU5D_t1642385972* get_U3CentityTypesU3Ek__BackingField_6() const { return ___U3CentityTypesU3Ek__BackingField_6; }
	inline StringU5BU5D_t1642385972** get_address_of_U3CentityTypesU3Ek__BackingField_6() { return &___U3CentityTypesU3Ek__BackingField_6; }
	inline void set_U3CentityTypesU3Ek__BackingField_6(StringU5BU5D_t1642385972* value)
	{
		___U3CentityTypesU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CentityTypesU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANSWER_T3626344892_H
#ifndef EVIDENCE_T859225293_H
#define EVIDENCE_T859225293_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Evidence
struct  Evidence_t859225293  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Evidence::<value>k__BackingField
	String_t* ___U3CvalueU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Evidence::<text>k__BackingField
	String_t* ___U3CtextU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Evidence::<id>k__BackingField
	String_t* ___U3CidU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Evidence::<title>k__BackingField
	String_t* ___U3CtitleU3Ek__BackingField_3;
	// System.String IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Evidence::<document>k__BackingField
	String_t* ___U3CdocumentU3Ek__BackingField_4;
	// System.String IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Evidence::<copyright>k__BackingField
	String_t* ___U3CcopyrightU3Ek__BackingField_5;
	// System.String IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Evidence::<termsOfUse>k__BackingField
	String_t* ___U3CtermsOfUseU3Ek__BackingField_6;
	// IBM.Watson.DeveloperCloud.Services.DeepQA.v1.MetaDataMap IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Evidence::<metadataMap>k__BackingField
	MetaDataMap_t2304952903 * ___U3CmetadataMapU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_U3CvalueU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Evidence_t859225293, ___U3CvalueU3Ek__BackingField_0)); }
	inline String_t* get_U3CvalueU3Ek__BackingField_0() const { return ___U3CvalueU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CvalueU3Ek__BackingField_0() { return &___U3CvalueU3Ek__BackingField_0; }
	inline void set_U3CvalueU3Ek__BackingField_0(String_t* value)
	{
		___U3CvalueU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CvalueU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CtextU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Evidence_t859225293, ___U3CtextU3Ek__BackingField_1)); }
	inline String_t* get_U3CtextU3Ek__BackingField_1() const { return ___U3CtextU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CtextU3Ek__BackingField_1() { return &___U3CtextU3Ek__BackingField_1; }
	inline void set_U3CtextU3Ek__BackingField_1(String_t* value)
	{
		___U3CtextU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CidU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Evidence_t859225293, ___U3CidU3Ek__BackingField_2)); }
	inline String_t* get_U3CidU3Ek__BackingField_2() const { return ___U3CidU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CidU3Ek__BackingField_2() { return &___U3CidU3Ek__BackingField_2; }
	inline void set_U3CidU3Ek__BackingField_2(String_t* value)
	{
		___U3CidU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CidU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CtitleU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Evidence_t859225293, ___U3CtitleU3Ek__BackingField_3)); }
	inline String_t* get_U3CtitleU3Ek__BackingField_3() const { return ___U3CtitleU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CtitleU3Ek__BackingField_3() { return &___U3CtitleU3Ek__BackingField_3; }
	inline void set_U3CtitleU3Ek__BackingField_3(String_t* value)
	{
		___U3CtitleU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtitleU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CdocumentU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Evidence_t859225293, ___U3CdocumentU3Ek__BackingField_4)); }
	inline String_t* get_U3CdocumentU3Ek__BackingField_4() const { return ___U3CdocumentU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CdocumentU3Ek__BackingField_4() { return &___U3CdocumentU3Ek__BackingField_4; }
	inline void set_U3CdocumentU3Ek__BackingField_4(String_t* value)
	{
		___U3CdocumentU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdocumentU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CcopyrightU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Evidence_t859225293, ___U3CcopyrightU3Ek__BackingField_5)); }
	inline String_t* get_U3CcopyrightU3Ek__BackingField_5() const { return ___U3CcopyrightU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CcopyrightU3Ek__BackingField_5() { return &___U3CcopyrightU3Ek__BackingField_5; }
	inline void set_U3CcopyrightU3Ek__BackingField_5(String_t* value)
	{
		___U3CcopyrightU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcopyrightU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CtermsOfUseU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Evidence_t859225293, ___U3CtermsOfUseU3Ek__BackingField_6)); }
	inline String_t* get_U3CtermsOfUseU3Ek__BackingField_6() const { return ___U3CtermsOfUseU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CtermsOfUseU3Ek__BackingField_6() { return &___U3CtermsOfUseU3Ek__BackingField_6; }
	inline void set_U3CtermsOfUseU3Ek__BackingField_6(String_t* value)
	{
		___U3CtermsOfUseU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtermsOfUseU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CmetadataMapU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(Evidence_t859225293, ___U3CmetadataMapU3Ek__BackingField_7)); }
	inline MetaDataMap_t2304952903 * get_U3CmetadataMapU3Ek__BackingField_7() const { return ___U3CmetadataMapU3Ek__BackingField_7; }
	inline MetaDataMap_t2304952903 ** get_address_of_U3CmetadataMapU3Ek__BackingField_7() { return &___U3CmetadataMapU3Ek__BackingField_7; }
	inline void set_U3CmetadataMapU3Ek__BackingField_7(MetaDataMap_t2304952903 * value)
	{
		___U3CmetadataMapU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmetadataMapU3Ek__BackingField_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVIDENCE_T859225293_H
#ifndef SYNONYM_T1752818203_H
#define SYNONYM_T1752818203_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Synonym
struct  Synonym_t1752818203  : public RuntimeObject
{
public:
	// System.Boolean IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Synonym::<isChosen>k__BackingField
	bool ___U3CisChosenU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Synonym::<value>k__BackingField
	String_t* ___U3CvalueU3Ek__BackingField_1;
	// System.Double IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Synonym::<weight>k__BackingField
	double ___U3CweightU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CisChosenU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Synonym_t1752818203, ___U3CisChosenU3Ek__BackingField_0)); }
	inline bool get_U3CisChosenU3Ek__BackingField_0() const { return ___U3CisChosenU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CisChosenU3Ek__BackingField_0() { return &___U3CisChosenU3Ek__BackingField_0; }
	inline void set_U3CisChosenU3Ek__BackingField_0(bool value)
	{
		___U3CisChosenU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CvalueU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Synonym_t1752818203, ___U3CvalueU3Ek__BackingField_1)); }
	inline String_t* get_U3CvalueU3Ek__BackingField_1() const { return ___U3CvalueU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CvalueU3Ek__BackingField_1() { return &___U3CvalueU3Ek__BackingField_1; }
	inline void set_U3CvalueU3Ek__BackingField_1(String_t* value)
	{
		___U3CvalueU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CvalueU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CweightU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Synonym_t1752818203, ___U3CweightU3Ek__BackingField_2)); }
	inline double get_U3CweightU3Ek__BackingField_2() const { return ___U3CweightU3Ek__BackingField_2; }
	inline double* get_address_of_U3CweightU3Ek__BackingField_2() { return &___U3CweightU3Ek__BackingField_2; }
	inline void set_U3CweightU3Ek__BackingField_2(double value)
	{
		___U3CweightU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYNONYM_T1752818203_H
#ifndef SYNSET_T2414122624_H
#define SYNSET_T2414122624_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.DeepQA.v1.SynSet
struct  SynSet_t2414122624  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.DeepQA.v1.SynSet::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_0;
	// IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Synonym[] IBM.Watson.DeveloperCloud.Services.DeepQA.v1.SynSet::<synSet>k__BackingField
	SynonymU5BU5D_t1136100122* ___U3CsynSetU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SynSet_t2414122624, ___U3CnameU3Ek__BackingField_0)); }
	inline String_t* get_U3CnameU3Ek__BackingField_0() const { return ___U3CnameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_0() { return &___U3CnameU3Ek__BackingField_0; }
	inline void set_U3CnameU3Ek__BackingField_0(String_t* value)
	{
		___U3CnameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CsynSetU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(SynSet_t2414122624, ___U3CsynSetU3Ek__BackingField_1)); }
	inline SynonymU5BU5D_t1136100122* get_U3CsynSetU3Ek__BackingField_1() const { return ___U3CsynSetU3Ek__BackingField_1; }
	inline SynonymU5BU5D_t1136100122** get_address_of_U3CsynSetU3Ek__BackingField_1() { return &___U3CsynSetU3Ek__BackingField_1; }
	inline void set_U3CsynSetU3Ek__BackingField_1(SynonymU5BU5D_t1136100122* value)
	{
		___U3CsynSetU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsynSetU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYNSET_T2414122624_H
#ifndef SLOTS_T3709295365_H
#define SLOTS_T3709295365_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Slots
struct  Slots_t3709295365  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Slots::<pred>k__BackingField
	String_t* ___U3CpredU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Slots::<subj>k__BackingField
	String_t* ___U3CsubjU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Slots::<objprep>k__BackingField
	String_t* ___U3CobjprepU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Slots::<psubj>k__BackingField
	String_t* ___U3CpsubjU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CpredU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Slots_t3709295365, ___U3CpredU3Ek__BackingField_0)); }
	inline String_t* get_U3CpredU3Ek__BackingField_0() const { return ___U3CpredU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CpredU3Ek__BackingField_0() { return &___U3CpredU3Ek__BackingField_0; }
	inline void set_U3CpredU3Ek__BackingField_0(String_t* value)
	{
		___U3CpredU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpredU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CsubjU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Slots_t3709295365, ___U3CsubjU3Ek__BackingField_1)); }
	inline String_t* get_U3CsubjU3Ek__BackingField_1() const { return ___U3CsubjU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CsubjU3Ek__BackingField_1() { return &___U3CsubjU3Ek__BackingField_1; }
	inline void set_U3CsubjU3Ek__BackingField_1(String_t* value)
	{
		___U3CsubjU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsubjU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CobjprepU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Slots_t3709295365, ___U3CobjprepU3Ek__BackingField_2)); }
	inline String_t* get_U3CobjprepU3Ek__BackingField_2() const { return ___U3CobjprepU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CobjprepU3Ek__BackingField_2() { return &___U3CobjprepU3Ek__BackingField_2; }
	inline void set_U3CobjprepU3Ek__BackingField_2(String_t* value)
	{
		___U3CobjprepU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CobjprepU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CpsubjU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Slots_t3709295365, ___U3CpsubjU3Ek__BackingField_3)); }
	inline String_t* get_U3CpsubjU3Ek__BackingField_3() const { return ___U3CpsubjU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CpsubjU3Ek__BackingField_3() { return &___U3CpsubjU3Ek__BackingField_3; }
	inline void set_U3CpsubjU3Ek__BackingField_3(String_t* value)
	{
		___U3CpsubjU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpsubjU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SLOTS_T3709295365_H
#ifndef WORD_T3826420310_H
#define WORD_T3826420310_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Word
struct  Word_t3826420310  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Slots IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Word::<compSlotParseNodes>k__BackingField
	Slots_t3709295365 * ___U3CcompSlotParseNodesU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Word::<slotname>k__BackingField
	String_t* ___U3CslotnameU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Word::<wordtext>k__BackingField
	String_t* ___U3CwordtextU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Word::<slotnameoptions>k__BackingField
	String_t* ___U3CslotnameoptionsU3Ek__BackingField_3;
	// System.String IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Word::<wordsense>k__BackingField
	String_t* ___U3CwordsenseU3Ek__BackingField_4;
	// System.String IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Word::<numericsense>k__BackingField
	String_t* ___U3CnumericsenseU3Ek__BackingField_5;
	// System.String IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Word::<seqno>k__BackingField
	String_t* ___U3CseqnoU3Ek__BackingField_6;
	// System.String IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Word::<wordbegin>k__BackingField
	String_t* ___U3CwordbeginU3Ek__BackingField_7;
	// System.String IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Word::<framebegin>k__BackingField
	String_t* ___U3CframebeginU3Ek__BackingField_8;
	// System.String IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Word::<frameend>k__BackingField
	String_t* ___U3CframeendU3Ek__BackingField_9;
	// System.String IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Word::<wordend>k__BackingField
	String_t* ___U3CwordendU3Ek__BackingField_10;
	// System.String IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Word::<features>k__BackingField
	String_t* ___U3CfeaturesU3Ek__BackingField_11;
	// IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Word[] IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Word::<lmods>k__BackingField
	WordU5BU5D_t1466090387* ___U3ClmodsU3Ek__BackingField_12;
	// IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Word[] IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Word::<rmods>k__BackingField
	WordU5BU5D_t1466090387* ___U3CrmodsU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_U3CcompSlotParseNodesU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Word_t3826420310, ___U3CcompSlotParseNodesU3Ek__BackingField_0)); }
	inline Slots_t3709295365 * get_U3CcompSlotParseNodesU3Ek__BackingField_0() const { return ___U3CcompSlotParseNodesU3Ek__BackingField_0; }
	inline Slots_t3709295365 ** get_address_of_U3CcompSlotParseNodesU3Ek__BackingField_0() { return &___U3CcompSlotParseNodesU3Ek__BackingField_0; }
	inline void set_U3CcompSlotParseNodesU3Ek__BackingField_0(Slots_t3709295365 * value)
	{
		___U3CcompSlotParseNodesU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcompSlotParseNodesU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CslotnameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Word_t3826420310, ___U3CslotnameU3Ek__BackingField_1)); }
	inline String_t* get_U3CslotnameU3Ek__BackingField_1() const { return ___U3CslotnameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CslotnameU3Ek__BackingField_1() { return &___U3CslotnameU3Ek__BackingField_1; }
	inline void set_U3CslotnameU3Ek__BackingField_1(String_t* value)
	{
		___U3CslotnameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CslotnameU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CwordtextU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Word_t3826420310, ___U3CwordtextU3Ek__BackingField_2)); }
	inline String_t* get_U3CwordtextU3Ek__BackingField_2() const { return ___U3CwordtextU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CwordtextU3Ek__BackingField_2() { return &___U3CwordtextU3Ek__BackingField_2; }
	inline void set_U3CwordtextU3Ek__BackingField_2(String_t* value)
	{
		___U3CwordtextU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwordtextU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CslotnameoptionsU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Word_t3826420310, ___U3CslotnameoptionsU3Ek__BackingField_3)); }
	inline String_t* get_U3CslotnameoptionsU3Ek__BackingField_3() const { return ___U3CslotnameoptionsU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CslotnameoptionsU3Ek__BackingField_3() { return &___U3CslotnameoptionsU3Ek__BackingField_3; }
	inline void set_U3CslotnameoptionsU3Ek__BackingField_3(String_t* value)
	{
		___U3CslotnameoptionsU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CslotnameoptionsU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CwordsenseU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Word_t3826420310, ___U3CwordsenseU3Ek__BackingField_4)); }
	inline String_t* get_U3CwordsenseU3Ek__BackingField_4() const { return ___U3CwordsenseU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CwordsenseU3Ek__BackingField_4() { return &___U3CwordsenseU3Ek__BackingField_4; }
	inline void set_U3CwordsenseU3Ek__BackingField_4(String_t* value)
	{
		___U3CwordsenseU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwordsenseU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CnumericsenseU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Word_t3826420310, ___U3CnumericsenseU3Ek__BackingField_5)); }
	inline String_t* get_U3CnumericsenseU3Ek__BackingField_5() const { return ___U3CnumericsenseU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CnumericsenseU3Ek__BackingField_5() { return &___U3CnumericsenseU3Ek__BackingField_5; }
	inline void set_U3CnumericsenseU3Ek__BackingField_5(String_t* value)
	{
		___U3CnumericsenseU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnumericsenseU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CseqnoU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Word_t3826420310, ___U3CseqnoU3Ek__BackingField_6)); }
	inline String_t* get_U3CseqnoU3Ek__BackingField_6() const { return ___U3CseqnoU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CseqnoU3Ek__BackingField_6() { return &___U3CseqnoU3Ek__BackingField_6; }
	inline void set_U3CseqnoU3Ek__BackingField_6(String_t* value)
	{
		___U3CseqnoU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CseqnoU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CwordbeginU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(Word_t3826420310, ___U3CwordbeginU3Ek__BackingField_7)); }
	inline String_t* get_U3CwordbeginU3Ek__BackingField_7() const { return ___U3CwordbeginU3Ek__BackingField_7; }
	inline String_t** get_address_of_U3CwordbeginU3Ek__BackingField_7() { return &___U3CwordbeginU3Ek__BackingField_7; }
	inline void set_U3CwordbeginU3Ek__BackingField_7(String_t* value)
	{
		___U3CwordbeginU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwordbeginU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CframebeginU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(Word_t3826420310, ___U3CframebeginU3Ek__BackingField_8)); }
	inline String_t* get_U3CframebeginU3Ek__BackingField_8() const { return ___U3CframebeginU3Ek__BackingField_8; }
	inline String_t** get_address_of_U3CframebeginU3Ek__BackingField_8() { return &___U3CframebeginU3Ek__BackingField_8; }
	inline void set_U3CframebeginU3Ek__BackingField_8(String_t* value)
	{
		___U3CframebeginU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CframebeginU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CframeendU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(Word_t3826420310, ___U3CframeendU3Ek__BackingField_9)); }
	inline String_t* get_U3CframeendU3Ek__BackingField_9() const { return ___U3CframeendU3Ek__BackingField_9; }
	inline String_t** get_address_of_U3CframeendU3Ek__BackingField_9() { return &___U3CframeendU3Ek__BackingField_9; }
	inline void set_U3CframeendU3Ek__BackingField_9(String_t* value)
	{
		___U3CframeendU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CframeendU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CwordendU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(Word_t3826420310, ___U3CwordendU3Ek__BackingField_10)); }
	inline String_t* get_U3CwordendU3Ek__BackingField_10() const { return ___U3CwordendU3Ek__BackingField_10; }
	inline String_t** get_address_of_U3CwordendU3Ek__BackingField_10() { return &___U3CwordendU3Ek__BackingField_10; }
	inline void set_U3CwordendU3Ek__BackingField_10(String_t* value)
	{
		___U3CwordendU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwordendU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_U3CfeaturesU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(Word_t3826420310, ___U3CfeaturesU3Ek__BackingField_11)); }
	inline String_t* get_U3CfeaturesU3Ek__BackingField_11() const { return ___U3CfeaturesU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CfeaturesU3Ek__BackingField_11() { return &___U3CfeaturesU3Ek__BackingField_11; }
	inline void set_U3CfeaturesU3Ek__BackingField_11(String_t* value)
	{
		___U3CfeaturesU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CfeaturesU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3ClmodsU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(Word_t3826420310, ___U3ClmodsU3Ek__BackingField_12)); }
	inline WordU5BU5D_t1466090387* get_U3ClmodsU3Ek__BackingField_12() const { return ___U3ClmodsU3Ek__BackingField_12; }
	inline WordU5BU5D_t1466090387** get_address_of_U3ClmodsU3Ek__BackingField_12() { return &___U3ClmodsU3Ek__BackingField_12; }
	inline void set_U3ClmodsU3Ek__BackingField_12(WordU5BU5D_t1466090387* value)
	{
		___U3ClmodsU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClmodsU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CrmodsU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(Word_t3826420310, ___U3CrmodsU3Ek__BackingField_13)); }
	inline WordU5BU5D_t1466090387* get_U3CrmodsU3Ek__BackingField_13() const { return ___U3CrmodsU3Ek__BackingField_13; }
	inline WordU5BU5D_t1466090387** get_address_of_U3CrmodsU3Ek__BackingField_13() { return &___U3CrmodsU3Ek__BackingField_13; }
	inline void set_U3CrmodsU3Ek__BackingField_13(WordU5BU5D_t1466090387* value)
	{
		___U3CrmodsU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrmodsU3Ek__BackingField_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORD_T3826420310_H
#ifndef QUESTION_T4096007710_H
#define QUESTION_T4096007710_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Question
struct  Question_t4096007710  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Value[] IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Question::<qclasslist>k__BackingField
	ValueU5BU5D_t1766980932* ___U3CqclasslistU3Ek__BackingField_0;
	// IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Value[] IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Question::<focuslist>k__BackingField
	ValueU5BU5D_t1766980932* ___U3CfocuslistU3Ek__BackingField_1;
	// IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Value[] IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Question::<latlist>k__BackingField
	ValueU5BU5D_t1766980932* ___U3ClatlistU3Ek__BackingField_2;
	// IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Evidence[] IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Question::<evidencelist>k__BackingField
	EvidenceU5BU5D_t1065303392* ___U3CevidencelistU3Ek__BackingField_3;
	// IBM.Watson.DeveloperCloud.Services.DeepQA.v1.SynonymList[] IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Question::<synonymList>k__BackingField
	SynonymListU5BU5D_t2814318124* ___U3CsynonymListU3Ek__BackingField_4;
	// System.String[] IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Question::<disambiguatedEntities>k__BackingField
	StringU5BU5D_t1642385972* ___U3CdisambiguatedEntitiesU3Ek__BackingField_5;
	// IBM.Watson.DeveloperCloud.Services.DeepQA.v1.ParseTree[] IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Question::<xsgtopparses>k__BackingField
	ParseTreeU5BU5D_t639762008* ___U3CxsgtopparsesU3Ek__BackingField_6;
	// System.String IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Question::<casXml>k__BackingField
	String_t* ___U3CcasXmlU3Ek__BackingField_7;
	// System.String IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Question::<pipelineid>k__BackingField
	String_t* ___U3CpipelineidU3Ek__BackingField_8;
	// System.Boolean IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Question::<formattedAnswer>k__BackingField
	bool ___U3CformattedAnswerU3Ek__BackingField_9;
	// System.String IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Question::<selectedProcessingComponents>k__BackingField
	String_t* ___U3CselectedProcessingComponentsU3Ek__BackingField_10;
	// System.String IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Question::<category>k__BackingField
	String_t* ___U3CcategoryU3Ek__BackingField_11;
	// System.Int64 IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Question::<items>k__BackingField
	int64_t ___U3CitemsU3Ek__BackingField_12;
	// System.String IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Question::<status>k__BackingField
	String_t* ___U3CstatusU3Ek__BackingField_13;
	// System.String IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Question::<id>k__BackingField
	String_t* ___U3CidU3Ek__BackingField_14;
	// System.String IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Question::<questionText>k__BackingField
	String_t* ___U3CquestionTextU3Ek__BackingField_15;
	// IBM.Watson.DeveloperCloud.Services.DeepQA.v1.EvidenceRequest IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Question::<evidenceRequest>k__BackingField
	EvidenceRequest_t2807863460 * ___U3CevidenceRequestU3Ek__BackingField_16;
	// IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Answer[] IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Question::<answers>k__BackingField
	AnswerU5BU5D_t975328213* ___U3CanswersU3Ek__BackingField_17;
	// System.String[] IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Question::<errorNotifications>k__BackingField
	StringU5BU5D_t1642385972* ___U3CerrorNotificationsU3Ek__BackingField_18;
	// System.String IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Question::<passthru>k__BackingField
	String_t* ___U3CpassthruU3Ek__BackingField_19;
	// System.String IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Question::<questionId>k__BackingField
	String_t* ___U3CquestionIdU3Ek__BackingField_20;

public:
	inline static int32_t get_offset_of_U3CqclasslistU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Question_t4096007710, ___U3CqclasslistU3Ek__BackingField_0)); }
	inline ValueU5BU5D_t1766980932* get_U3CqclasslistU3Ek__BackingField_0() const { return ___U3CqclasslistU3Ek__BackingField_0; }
	inline ValueU5BU5D_t1766980932** get_address_of_U3CqclasslistU3Ek__BackingField_0() { return &___U3CqclasslistU3Ek__BackingField_0; }
	inline void set_U3CqclasslistU3Ek__BackingField_0(ValueU5BU5D_t1766980932* value)
	{
		___U3CqclasslistU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CqclasslistU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CfocuslistU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Question_t4096007710, ___U3CfocuslistU3Ek__BackingField_1)); }
	inline ValueU5BU5D_t1766980932* get_U3CfocuslistU3Ek__BackingField_1() const { return ___U3CfocuslistU3Ek__BackingField_1; }
	inline ValueU5BU5D_t1766980932** get_address_of_U3CfocuslistU3Ek__BackingField_1() { return &___U3CfocuslistU3Ek__BackingField_1; }
	inline void set_U3CfocuslistU3Ek__BackingField_1(ValueU5BU5D_t1766980932* value)
	{
		___U3CfocuslistU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CfocuslistU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3ClatlistU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Question_t4096007710, ___U3ClatlistU3Ek__BackingField_2)); }
	inline ValueU5BU5D_t1766980932* get_U3ClatlistU3Ek__BackingField_2() const { return ___U3ClatlistU3Ek__BackingField_2; }
	inline ValueU5BU5D_t1766980932** get_address_of_U3ClatlistU3Ek__BackingField_2() { return &___U3ClatlistU3Ek__BackingField_2; }
	inline void set_U3ClatlistU3Ek__BackingField_2(ValueU5BU5D_t1766980932* value)
	{
		___U3ClatlistU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClatlistU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CevidencelistU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Question_t4096007710, ___U3CevidencelistU3Ek__BackingField_3)); }
	inline EvidenceU5BU5D_t1065303392* get_U3CevidencelistU3Ek__BackingField_3() const { return ___U3CevidencelistU3Ek__BackingField_3; }
	inline EvidenceU5BU5D_t1065303392** get_address_of_U3CevidencelistU3Ek__BackingField_3() { return &___U3CevidencelistU3Ek__BackingField_3; }
	inline void set_U3CevidencelistU3Ek__BackingField_3(EvidenceU5BU5D_t1065303392* value)
	{
		___U3CevidencelistU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CevidencelistU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CsynonymListU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Question_t4096007710, ___U3CsynonymListU3Ek__BackingField_4)); }
	inline SynonymListU5BU5D_t2814318124* get_U3CsynonymListU3Ek__BackingField_4() const { return ___U3CsynonymListU3Ek__BackingField_4; }
	inline SynonymListU5BU5D_t2814318124** get_address_of_U3CsynonymListU3Ek__BackingField_4() { return &___U3CsynonymListU3Ek__BackingField_4; }
	inline void set_U3CsynonymListU3Ek__BackingField_4(SynonymListU5BU5D_t2814318124* value)
	{
		___U3CsynonymListU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsynonymListU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CdisambiguatedEntitiesU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Question_t4096007710, ___U3CdisambiguatedEntitiesU3Ek__BackingField_5)); }
	inline StringU5BU5D_t1642385972* get_U3CdisambiguatedEntitiesU3Ek__BackingField_5() const { return ___U3CdisambiguatedEntitiesU3Ek__BackingField_5; }
	inline StringU5BU5D_t1642385972** get_address_of_U3CdisambiguatedEntitiesU3Ek__BackingField_5() { return &___U3CdisambiguatedEntitiesU3Ek__BackingField_5; }
	inline void set_U3CdisambiguatedEntitiesU3Ek__BackingField_5(StringU5BU5D_t1642385972* value)
	{
		___U3CdisambiguatedEntitiesU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdisambiguatedEntitiesU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CxsgtopparsesU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Question_t4096007710, ___U3CxsgtopparsesU3Ek__BackingField_6)); }
	inline ParseTreeU5BU5D_t639762008* get_U3CxsgtopparsesU3Ek__BackingField_6() const { return ___U3CxsgtopparsesU3Ek__BackingField_6; }
	inline ParseTreeU5BU5D_t639762008** get_address_of_U3CxsgtopparsesU3Ek__BackingField_6() { return &___U3CxsgtopparsesU3Ek__BackingField_6; }
	inline void set_U3CxsgtopparsesU3Ek__BackingField_6(ParseTreeU5BU5D_t639762008* value)
	{
		___U3CxsgtopparsesU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CxsgtopparsesU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CcasXmlU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(Question_t4096007710, ___U3CcasXmlU3Ek__BackingField_7)); }
	inline String_t* get_U3CcasXmlU3Ek__BackingField_7() const { return ___U3CcasXmlU3Ek__BackingField_7; }
	inline String_t** get_address_of_U3CcasXmlU3Ek__BackingField_7() { return &___U3CcasXmlU3Ek__BackingField_7; }
	inline void set_U3CcasXmlU3Ek__BackingField_7(String_t* value)
	{
		___U3CcasXmlU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcasXmlU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CpipelineidU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(Question_t4096007710, ___U3CpipelineidU3Ek__BackingField_8)); }
	inline String_t* get_U3CpipelineidU3Ek__BackingField_8() const { return ___U3CpipelineidU3Ek__BackingField_8; }
	inline String_t** get_address_of_U3CpipelineidU3Ek__BackingField_8() { return &___U3CpipelineidU3Ek__BackingField_8; }
	inline void set_U3CpipelineidU3Ek__BackingField_8(String_t* value)
	{
		___U3CpipelineidU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpipelineidU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CformattedAnswerU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(Question_t4096007710, ___U3CformattedAnswerU3Ek__BackingField_9)); }
	inline bool get_U3CformattedAnswerU3Ek__BackingField_9() const { return ___U3CformattedAnswerU3Ek__BackingField_9; }
	inline bool* get_address_of_U3CformattedAnswerU3Ek__BackingField_9() { return &___U3CformattedAnswerU3Ek__BackingField_9; }
	inline void set_U3CformattedAnswerU3Ek__BackingField_9(bool value)
	{
		___U3CformattedAnswerU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CselectedProcessingComponentsU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(Question_t4096007710, ___U3CselectedProcessingComponentsU3Ek__BackingField_10)); }
	inline String_t* get_U3CselectedProcessingComponentsU3Ek__BackingField_10() const { return ___U3CselectedProcessingComponentsU3Ek__BackingField_10; }
	inline String_t** get_address_of_U3CselectedProcessingComponentsU3Ek__BackingField_10() { return &___U3CselectedProcessingComponentsU3Ek__BackingField_10; }
	inline void set_U3CselectedProcessingComponentsU3Ek__BackingField_10(String_t* value)
	{
		___U3CselectedProcessingComponentsU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CselectedProcessingComponentsU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_U3CcategoryU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(Question_t4096007710, ___U3CcategoryU3Ek__BackingField_11)); }
	inline String_t* get_U3CcategoryU3Ek__BackingField_11() const { return ___U3CcategoryU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CcategoryU3Ek__BackingField_11() { return &___U3CcategoryU3Ek__BackingField_11; }
	inline void set_U3CcategoryU3Ek__BackingField_11(String_t* value)
	{
		___U3CcategoryU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcategoryU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CitemsU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(Question_t4096007710, ___U3CitemsU3Ek__BackingField_12)); }
	inline int64_t get_U3CitemsU3Ek__BackingField_12() const { return ___U3CitemsU3Ek__BackingField_12; }
	inline int64_t* get_address_of_U3CitemsU3Ek__BackingField_12() { return &___U3CitemsU3Ek__BackingField_12; }
	inline void set_U3CitemsU3Ek__BackingField_12(int64_t value)
	{
		___U3CitemsU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CstatusU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(Question_t4096007710, ___U3CstatusU3Ek__BackingField_13)); }
	inline String_t* get_U3CstatusU3Ek__BackingField_13() const { return ___U3CstatusU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CstatusU3Ek__BackingField_13() { return &___U3CstatusU3Ek__BackingField_13; }
	inline void set_U3CstatusU3Ek__BackingField_13(String_t* value)
	{
		___U3CstatusU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstatusU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CidU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(Question_t4096007710, ___U3CidU3Ek__BackingField_14)); }
	inline String_t* get_U3CidU3Ek__BackingField_14() const { return ___U3CidU3Ek__BackingField_14; }
	inline String_t** get_address_of_U3CidU3Ek__BackingField_14() { return &___U3CidU3Ek__BackingField_14; }
	inline void set_U3CidU3Ek__BackingField_14(String_t* value)
	{
		___U3CidU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CidU3Ek__BackingField_14), value);
	}

	inline static int32_t get_offset_of_U3CquestionTextU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(Question_t4096007710, ___U3CquestionTextU3Ek__BackingField_15)); }
	inline String_t* get_U3CquestionTextU3Ek__BackingField_15() const { return ___U3CquestionTextU3Ek__BackingField_15; }
	inline String_t** get_address_of_U3CquestionTextU3Ek__BackingField_15() { return &___U3CquestionTextU3Ek__BackingField_15; }
	inline void set_U3CquestionTextU3Ek__BackingField_15(String_t* value)
	{
		___U3CquestionTextU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CquestionTextU3Ek__BackingField_15), value);
	}

	inline static int32_t get_offset_of_U3CevidenceRequestU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(Question_t4096007710, ___U3CevidenceRequestU3Ek__BackingField_16)); }
	inline EvidenceRequest_t2807863460 * get_U3CevidenceRequestU3Ek__BackingField_16() const { return ___U3CevidenceRequestU3Ek__BackingField_16; }
	inline EvidenceRequest_t2807863460 ** get_address_of_U3CevidenceRequestU3Ek__BackingField_16() { return &___U3CevidenceRequestU3Ek__BackingField_16; }
	inline void set_U3CevidenceRequestU3Ek__BackingField_16(EvidenceRequest_t2807863460 * value)
	{
		___U3CevidenceRequestU3Ek__BackingField_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CevidenceRequestU3Ek__BackingField_16), value);
	}

	inline static int32_t get_offset_of_U3CanswersU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(Question_t4096007710, ___U3CanswersU3Ek__BackingField_17)); }
	inline AnswerU5BU5D_t975328213* get_U3CanswersU3Ek__BackingField_17() const { return ___U3CanswersU3Ek__BackingField_17; }
	inline AnswerU5BU5D_t975328213** get_address_of_U3CanswersU3Ek__BackingField_17() { return &___U3CanswersU3Ek__BackingField_17; }
	inline void set_U3CanswersU3Ek__BackingField_17(AnswerU5BU5D_t975328213* value)
	{
		___U3CanswersU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CanswersU3Ek__BackingField_17), value);
	}

	inline static int32_t get_offset_of_U3CerrorNotificationsU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(Question_t4096007710, ___U3CerrorNotificationsU3Ek__BackingField_18)); }
	inline StringU5BU5D_t1642385972* get_U3CerrorNotificationsU3Ek__BackingField_18() const { return ___U3CerrorNotificationsU3Ek__BackingField_18; }
	inline StringU5BU5D_t1642385972** get_address_of_U3CerrorNotificationsU3Ek__BackingField_18() { return &___U3CerrorNotificationsU3Ek__BackingField_18; }
	inline void set_U3CerrorNotificationsU3Ek__BackingField_18(StringU5BU5D_t1642385972* value)
	{
		___U3CerrorNotificationsU3Ek__BackingField_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CerrorNotificationsU3Ek__BackingField_18), value);
	}

	inline static int32_t get_offset_of_U3CpassthruU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(Question_t4096007710, ___U3CpassthruU3Ek__BackingField_19)); }
	inline String_t* get_U3CpassthruU3Ek__BackingField_19() const { return ___U3CpassthruU3Ek__BackingField_19; }
	inline String_t** get_address_of_U3CpassthruU3Ek__BackingField_19() { return &___U3CpassthruU3Ek__BackingField_19; }
	inline void set_U3CpassthruU3Ek__BackingField_19(String_t* value)
	{
		___U3CpassthruU3Ek__BackingField_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpassthruU3Ek__BackingField_19), value);
	}

	inline static int32_t get_offset_of_U3CquestionIdU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(Question_t4096007710, ___U3CquestionIdU3Ek__BackingField_20)); }
	inline String_t* get_U3CquestionIdU3Ek__BackingField_20() const { return ___U3CquestionIdU3Ek__BackingField_20; }
	inline String_t** get_address_of_U3CquestionIdU3Ek__BackingField_20() { return &___U3CquestionIdU3Ek__BackingField_20; }
	inline void set_U3CquestionIdU3Ek__BackingField_20(String_t* value)
	{
		___U3CquestionIdU3Ek__BackingField_20 = value;
		Il2CppCodeGenWriteBarrier((&___U3CquestionIdU3Ek__BackingField_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUESTION_T4096007710_H
#ifndef CELL_T3725717219_H
#define CELL_T3725717219_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Answer/Cell
struct  Cell_t3725717219  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Answer/Cell::<Value>k__BackingField
	String_t* ___U3CValueU3Ek__BackingField_0;
	// System.Int32 IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Answer/Cell::<ColSpan>k__BackingField
	int32_t ___U3CColSpanU3Ek__BackingField_1;
	// System.Boolean IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Answer/Cell::<Highlighted>k__BackingField
	bool ___U3CHighlightedU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CValueU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Cell_t3725717219, ___U3CValueU3Ek__BackingField_0)); }
	inline String_t* get_U3CValueU3Ek__BackingField_0() const { return ___U3CValueU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CValueU3Ek__BackingField_0() { return &___U3CValueU3Ek__BackingField_0; }
	inline void set_U3CValueU3Ek__BackingField_0(String_t* value)
	{
		___U3CValueU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CValueU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CColSpanU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Cell_t3725717219, ___U3CColSpanU3Ek__BackingField_1)); }
	inline int32_t get_U3CColSpanU3Ek__BackingField_1() const { return ___U3CColSpanU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CColSpanU3Ek__BackingField_1() { return &___U3CColSpanU3Ek__BackingField_1; }
	inline void set_U3CColSpanU3Ek__BackingField_1(int32_t value)
	{
		___U3CColSpanU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CHighlightedU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Cell_t3725717219, ___U3CHighlightedU3Ek__BackingField_2)); }
	inline bool get_U3CHighlightedU3Ek__BackingField_2() const { return ___U3CHighlightedU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CHighlightedU3Ek__BackingField_2() { return &___U3CHighlightedU3Ek__BackingField_2; }
	inline void set_U3CHighlightedU3Ek__BackingField_2(bool value)
	{
		___U3CHighlightedU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CELL_T3725717219_H
#ifndef ROW_T3628585283_H
#define ROW_T3628585283_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Answer/Row
struct  Row_t3628585283  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Answer/Cell[] IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Answer/Row::<columns>k__BackingField
	CellU5BU5D_t1882260018* ___U3CcolumnsU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CcolumnsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Row_t3628585283, ___U3CcolumnsU3Ek__BackingField_0)); }
	inline CellU5BU5D_t1882260018* get_U3CcolumnsU3Ek__BackingField_0() const { return ___U3CcolumnsU3Ek__BackingField_0; }
	inline CellU5BU5D_t1882260018** get_address_of_U3CcolumnsU3Ek__BackingField_0() { return &___U3CcolumnsU3Ek__BackingField_0; }
	inline void set_U3CcolumnsU3Ek__BackingField_0(CellU5BU5D_t1882260018* value)
	{
		___U3CcolumnsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcolumnsU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROW_T3628585283_H
#ifndef TABLE_T205705827_H
#define TABLE_T205705827_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Answer/Table
struct  Table_t205705827  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Answer/Row[] IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Answer/Table::<rows>k__BackingField
	RowU5BU5D_t659188306* ___U3CrowsU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CrowsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Table_t205705827, ___U3CrowsU3Ek__BackingField_0)); }
	inline RowU5BU5D_t659188306* get_U3CrowsU3Ek__BackingField_0() const { return ___U3CrowsU3Ek__BackingField_0; }
	inline RowU5BU5D_t659188306** get_address_of_U3CrowsU3Ek__BackingField_0() { return &___U3CrowsU3Ek__BackingField_0; }
	inline void set_U3CrowsU3Ek__BackingField_0(RowU5BU5D_t659188306* value)
	{
		___U3CrowsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrowsU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TABLE_T205705827_H
#ifndef DOCUMENTSNAPSHOT_T3752605611_H
#define DOCUMENTSNAPSHOT_T3752605611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.DocumentSnapshot
struct  DocumentSnapshot_t3752605611  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.DocumentSnapshot::<step>k__BackingField
	String_t* ___U3CstepU3Ek__BackingField_0;
	// System.Object IBM.Watson.DeveloperCloud.Services.Discovery.v1.DocumentSnapshot::<snapshot>k__BackingField
	RuntimeObject * ___U3CsnapshotU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CstepU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DocumentSnapshot_t3752605611, ___U3CstepU3Ek__BackingField_0)); }
	inline String_t* get_U3CstepU3Ek__BackingField_0() const { return ___U3CstepU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CstepU3Ek__BackingField_0() { return &___U3CstepU3Ek__BackingField_0; }
	inline void set_U3CstepU3Ek__BackingField_0(String_t* value)
	{
		___U3CstepU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstepU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CsnapshotU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(DocumentSnapshot_t3752605611, ___U3CsnapshotU3Ek__BackingField_1)); }
	inline RuntimeObject * get_U3CsnapshotU3Ek__BackingField_1() const { return ___U3CsnapshotU3Ek__BackingField_1; }
	inline RuntimeObject ** get_address_of_U3CsnapshotU3Ek__BackingField_1() { return &___U3CsnapshotU3Ek__BackingField_1; }
	inline void set_U3CsnapshotU3Ek__BackingField_1(RuntimeObject * value)
	{
		___U3CsnapshotU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsnapshotU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOCUMENTSNAPSHOT_T3752605611_H
#ifndef WORDSTYLE_T1557885949_H
#define WORDSTYLE_T1557885949_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.WordStyle
struct  WordStyle_t1557885949  : public RuntimeObject
{
public:
	// System.Double IBM.Watson.DeveloperCloud.Services.Discovery.v1.WordStyle::<level>k__BackingField
	double ___U3ClevelU3Ek__BackingField_0;
	// System.String[] IBM.Watson.DeveloperCloud.Services.Discovery.v1.WordStyle::<names>k__BackingField
	StringU5BU5D_t1642385972* ___U3CnamesU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3ClevelU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(WordStyle_t1557885949, ___U3ClevelU3Ek__BackingField_0)); }
	inline double get_U3ClevelU3Ek__BackingField_0() const { return ___U3ClevelU3Ek__BackingField_0; }
	inline double* get_address_of_U3ClevelU3Ek__BackingField_0() { return &___U3ClevelU3Ek__BackingField_0; }
	inline void set_U3ClevelU3Ek__BackingField_0(double value)
	{
		___U3ClevelU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CnamesU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(WordStyle_t1557885949, ___U3CnamesU3Ek__BackingField_1)); }
	inline StringU5BU5D_t1642385972* get_U3CnamesU3Ek__BackingField_1() const { return ___U3CnamesU3Ek__BackingField_1; }
	inline StringU5BU5D_t1642385972** get_address_of_U3CnamesU3Ek__BackingField_1() { return &___U3CnamesU3Ek__BackingField_1; }
	inline void set_U3CnamesU3Ek__BackingField_1(StringU5BU5D_t1642385972* value)
	{
		___U3CnamesU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnamesU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORDSTYLE_T1557885949_H
#ifndef DELETECONFIGURATIONRESPONSE_T86652326_H
#define DELETECONFIGURATIONRESPONSE_T86652326_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.DeleteConfigurationResponse
struct  DeleteConfigurationResponse_t86652326  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.DeleteConfigurationResponse::<configuration_id>k__BackingField
	String_t* ___U3Cconfiguration_idU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.DeleteConfigurationResponse::<status>k__BackingField
	String_t* ___U3CstatusU3Ek__BackingField_1;
	// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Notice[] IBM.Watson.DeveloperCloud.Services.Discovery.v1.DeleteConfigurationResponse::<notices>k__BackingField
	NoticeU5BU5D_t1969629099* ___U3CnoticesU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3Cconfiguration_idU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DeleteConfigurationResponse_t86652326, ___U3Cconfiguration_idU3Ek__BackingField_0)); }
	inline String_t* get_U3Cconfiguration_idU3Ek__BackingField_0() const { return ___U3Cconfiguration_idU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3Cconfiguration_idU3Ek__BackingField_0() { return &___U3Cconfiguration_idU3Ek__BackingField_0; }
	inline void set_U3Cconfiguration_idU3Ek__BackingField_0(String_t* value)
	{
		___U3Cconfiguration_idU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cconfiguration_idU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CstatusU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(DeleteConfigurationResponse_t86652326, ___U3CstatusU3Ek__BackingField_1)); }
	inline String_t* get_U3CstatusU3Ek__BackingField_1() const { return ___U3CstatusU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CstatusU3Ek__BackingField_1() { return &___U3CstatusU3Ek__BackingField_1; }
	inline void set_U3CstatusU3Ek__BackingField_1(String_t* value)
	{
		___U3CstatusU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstatusU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CnoticesU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(DeleteConfigurationResponse_t86652326, ___U3CnoticesU3Ek__BackingField_2)); }
	inline NoticeU5BU5D_t1969629099* get_U3CnoticesU3Ek__BackingField_2() const { return ___U3CnoticesU3Ek__BackingField_2; }
	inline NoticeU5BU5D_t1969629099** get_address_of_U3CnoticesU3Ek__BackingField_2() { return &___U3CnoticesU3Ek__BackingField_2; }
	inline void set_U3CnoticesU3Ek__BackingField_2(NoticeU5BU5D_t1969629099* value)
	{
		___U3CnoticesU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnoticesU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELETECONFIGURATIONRESPONSE_T86652326_H
#ifndef GETCOLLECTIONSRESPONSE_T825581884_H
#define GETCOLLECTIONSRESPONSE_T825581884_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.GetCollectionsResponse
struct  GetCollectionsResponse_t825581884  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.Discovery.v1.CollectionRef[] IBM.Watson.DeveloperCloud.Services.Discovery.v1.GetCollectionsResponse::<collections>k__BackingField
	CollectionRefU5BU5D_t2497189142* ___U3CcollectionsU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CcollectionsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(GetCollectionsResponse_t825581884, ___U3CcollectionsU3Ek__BackingField_0)); }
	inline CollectionRefU5BU5D_t2497189142* get_U3CcollectionsU3Ek__BackingField_0() const { return ___U3CcollectionsU3Ek__BackingField_0; }
	inline CollectionRefU5BU5D_t2497189142** get_address_of_U3CcollectionsU3Ek__BackingField_0() { return &___U3CcollectionsU3Ek__BackingField_0; }
	inline void set_U3CcollectionsU3Ek__BackingField_0(CollectionRefU5BU5D_t2497189142* value)
	{
		___U3CcollectionsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcollectionsU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETCOLLECTIONSRESPONSE_T825581884_H
#ifndef XPATHPATTERNS_T1498040530_H
#define XPATHPATTERNS_T1498040530_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.XPathPatterns
struct  XPathPatterns_t1498040530  : public RuntimeObject
{
public:
	// System.String[] IBM.Watson.DeveloperCloud.Services.Discovery.v1.XPathPatterns::<xpaths>k__BackingField
	StringU5BU5D_t1642385972* ___U3CxpathsU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CxpathsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(XPathPatterns_t1498040530, ___U3CxpathsU3Ek__BackingField_0)); }
	inline StringU5BU5D_t1642385972* get_U3CxpathsU3Ek__BackingField_0() const { return ___U3CxpathsU3Ek__BackingField_0; }
	inline StringU5BU5D_t1642385972** get_address_of_U3CxpathsU3Ek__BackingField_0() { return &___U3CxpathsU3Ek__BackingField_0; }
	inline void set_U3CxpathsU3Ek__BackingField_0(StringU5BU5D_t1642385972* value)
	{
		___U3CxpathsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CxpathsU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHPATTERNS_T1498040530_H
#ifndef ENRICHMENTOPTIONS_T3455576757_H
#define ENRICHMENTOPTIONS_T3455576757_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.EnrichmentOptions
struct  EnrichmentOptions_t3455576757  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.EnrichmentOptions::<extract>k__BackingField
	String_t* ___U3CextractU3Ek__BackingField_0;
	// System.Boolean IBM.Watson.DeveloperCloud.Services.Discovery.v1.EnrichmentOptions::<sentiment>k__BackingField
	bool ___U3CsentimentU3Ek__BackingField_1;
	// System.Boolean IBM.Watson.DeveloperCloud.Services.Discovery.v1.EnrichmentOptions::<quotations>k__BackingField
	bool ___U3CquotationsU3Ek__BackingField_2;
	// System.Boolean IBM.Watson.DeveloperCloud.Services.Discovery.v1.EnrichmentOptions::<showSouceText>k__BackingField
	bool ___U3CshowSouceTextU3Ek__BackingField_3;
	// System.Boolean IBM.Watson.DeveloperCloud.Services.Discovery.v1.EnrichmentOptions::<hierarchicalTypedRelations>k__BackingField
	bool ___U3ChierarchicalTypedRelationsU3Ek__BackingField_4;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.EnrichmentOptions::<model>k__BackingField
	String_t* ___U3CmodelU3Ek__BackingField_5;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.EnrichmentOptions::<language>k__BackingField
	String_t* ___U3ClanguageU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CextractU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(EnrichmentOptions_t3455576757, ___U3CextractU3Ek__BackingField_0)); }
	inline String_t* get_U3CextractU3Ek__BackingField_0() const { return ___U3CextractU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CextractU3Ek__BackingField_0() { return &___U3CextractU3Ek__BackingField_0; }
	inline void set_U3CextractU3Ek__BackingField_0(String_t* value)
	{
		___U3CextractU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CextractU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CsentimentU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(EnrichmentOptions_t3455576757, ___U3CsentimentU3Ek__BackingField_1)); }
	inline bool get_U3CsentimentU3Ek__BackingField_1() const { return ___U3CsentimentU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CsentimentU3Ek__BackingField_1() { return &___U3CsentimentU3Ek__BackingField_1; }
	inline void set_U3CsentimentU3Ek__BackingField_1(bool value)
	{
		___U3CsentimentU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CquotationsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(EnrichmentOptions_t3455576757, ___U3CquotationsU3Ek__BackingField_2)); }
	inline bool get_U3CquotationsU3Ek__BackingField_2() const { return ___U3CquotationsU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CquotationsU3Ek__BackingField_2() { return &___U3CquotationsU3Ek__BackingField_2; }
	inline void set_U3CquotationsU3Ek__BackingField_2(bool value)
	{
		___U3CquotationsU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CshowSouceTextU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(EnrichmentOptions_t3455576757, ___U3CshowSouceTextU3Ek__BackingField_3)); }
	inline bool get_U3CshowSouceTextU3Ek__BackingField_3() const { return ___U3CshowSouceTextU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CshowSouceTextU3Ek__BackingField_3() { return &___U3CshowSouceTextU3Ek__BackingField_3; }
	inline void set_U3CshowSouceTextU3Ek__BackingField_3(bool value)
	{
		___U3CshowSouceTextU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3ChierarchicalTypedRelationsU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(EnrichmentOptions_t3455576757, ___U3ChierarchicalTypedRelationsU3Ek__BackingField_4)); }
	inline bool get_U3ChierarchicalTypedRelationsU3Ek__BackingField_4() const { return ___U3ChierarchicalTypedRelationsU3Ek__BackingField_4; }
	inline bool* get_address_of_U3ChierarchicalTypedRelationsU3Ek__BackingField_4() { return &___U3ChierarchicalTypedRelationsU3Ek__BackingField_4; }
	inline void set_U3ChierarchicalTypedRelationsU3Ek__BackingField_4(bool value)
	{
		___U3ChierarchicalTypedRelationsU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CmodelU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(EnrichmentOptions_t3455576757, ___U3CmodelU3Ek__BackingField_5)); }
	inline String_t* get_U3CmodelU3Ek__BackingField_5() const { return ___U3CmodelU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CmodelU3Ek__BackingField_5() { return &___U3CmodelU3Ek__BackingField_5; }
	inline void set_U3CmodelU3Ek__BackingField_5(String_t* value)
	{
		___U3CmodelU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmodelU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3ClanguageU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(EnrichmentOptions_t3455576757, ___U3ClanguageU3Ek__BackingField_6)); }
	inline String_t* get_U3ClanguageU3Ek__BackingField_6() const { return ___U3ClanguageU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3ClanguageU3Ek__BackingField_6() { return &___U3ClanguageU3Ek__BackingField_6; }
	inline void set_U3ClanguageU3Ek__BackingField_6(String_t* value)
	{
		___U3ClanguageU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClanguageU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENRICHMENTOPTIONS_T3455576757_H
#ifndef FONTSETTING_T3267780181_H
#define FONTSETTING_T3267780181_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.FontSetting
struct  FontSetting_t3267780181  : public RuntimeObject
{
public:
	// System.Double IBM.Watson.DeveloperCloud.Services.Discovery.v1.FontSetting::<level>k__BackingField
	double ___U3ClevelU3Ek__BackingField_0;
	// System.Single IBM.Watson.DeveloperCloud.Services.Discovery.v1.FontSetting::<min_size>k__BackingField
	float ___U3Cmin_sizeU3Ek__BackingField_1;
	// System.Single IBM.Watson.DeveloperCloud.Services.Discovery.v1.FontSetting::<max_size>k__BackingField
	float ___U3Cmax_sizeU3Ek__BackingField_2;
	// System.Boolean IBM.Watson.DeveloperCloud.Services.Discovery.v1.FontSetting::<bold>k__BackingField
	bool ___U3CboldU3Ek__BackingField_3;
	// System.Boolean IBM.Watson.DeveloperCloud.Services.Discovery.v1.FontSetting::<italic>k__BackingField
	bool ___U3CitalicU3Ek__BackingField_4;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.FontSetting::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3ClevelU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(FontSetting_t3267780181, ___U3ClevelU3Ek__BackingField_0)); }
	inline double get_U3ClevelU3Ek__BackingField_0() const { return ___U3ClevelU3Ek__BackingField_0; }
	inline double* get_address_of_U3ClevelU3Ek__BackingField_0() { return &___U3ClevelU3Ek__BackingField_0; }
	inline void set_U3ClevelU3Ek__BackingField_0(double value)
	{
		___U3ClevelU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3Cmin_sizeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(FontSetting_t3267780181, ___U3Cmin_sizeU3Ek__BackingField_1)); }
	inline float get_U3Cmin_sizeU3Ek__BackingField_1() const { return ___U3Cmin_sizeU3Ek__BackingField_1; }
	inline float* get_address_of_U3Cmin_sizeU3Ek__BackingField_1() { return &___U3Cmin_sizeU3Ek__BackingField_1; }
	inline void set_U3Cmin_sizeU3Ek__BackingField_1(float value)
	{
		___U3Cmin_sizeU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3Cmax_sizeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(FontSetting_t3267780181, ___U3Cmax_sizeU3Ek__BackingField_2)); }
	inline float get_U3Cmax_sizeU3Ek__BackingField_2() const { return ___U3Cmax_sizeU3Ek__BackingField_2; }
	inline float* get_address_of_U3Cmax_sizeU3Ek__BackingField_2() { return &___U3Cmax_sizeU3Ek__BackingField_2; }
	inline void set_U3Cmax_sizeU3Ek__BackingField_2(float value)
	{
		___U3Cmax_sizeU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CboldU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(FontSetting_t3267780181, ___U3CboldU3Ek__BackingField_3)); }
	inline bool get_U3CboldU3Ek__BackingField_3() const { return ___U3CboldU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CboldU3Ek__BackingField_3() { return &___U3CboldU3Ek__BackingField_3; }
	inline void set_U3CboldU3Ek__BackingField_3(bool value)
	{
		___U3CboldU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CitalicU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(FontSetting_t3267780181, ___U3CitalicU3Ek__BackingField_4)); }
	inline bool get_U3CitalicU3Ek__BackingField_4() const { return ___U3CitalicU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CitalicU3Ek__BackingField_4() { return &___U3CitalicU3Ek__BackingField_4; }
	inline void set_U3CitalicU3Ek__BackingField_4(bool value)
	{
		___U3CitalicU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(FontSetting_t3267780181, ___U3CnameU3Ek__BackingField_5)); }
	inline String_t* get_U3CnameU3Ek__BackingField_5() const { return ___U3CnameU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_5() { return &___U3CnameU3Ek__BackingField_5; }
	inline void set_U3CnameU3Ek__BackingField_5(String_t* value)
	{
		___U3CnameU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FONTSETTING_T3267780181_H
#ifndef DOCUMENTCOUNTS_T1301920119_H
#define DOCUMENTCOUNTS_T1301920119_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.DocumentCounts
struct  DocumentCounts_t1301920119  : public RuntimeObject
{
public:
	// System.Int32 IBM.Watson.DeveloperCloud.Services.Discovery.v1.DocumentCounts::<available>k__BackingField
	int32_t ___U3CavailableU3Ek__BackingField_0;
	// System.Int32 IBM.Watson.DeveloperCloud.Services.Discovery.v1.DocumentCounts::<processing>k__BackingField
	int32_t ___U3CprocessingU3Ek__BackingField_1;
	// System.Int32 IBM.Watson.DeveloperCloud.Services.Discovery.v1.DocumentCounts::<failed>k__BackingField
	int32_t ___U3CfailedU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CavailableU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DocumentCounts_t1301920119, ___U3CavailableU3Ek__BackingField_0)); }
	inline int32_t get_U3CavailableU3Ek__BackingField_0() const { return ___U3CavailableU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CavailableU3Ek__BackingField_0() { return &___U3CavailableU3Ek__BackingField_0; }
	inline void set_U3CavailableU3Ek__BackingField_0(int32_t value)
	{
		___U3CavailableU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CprocessingU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(DocumentCounts_t1301920119, ___U3CprocessingU3Ek__BackingField_1)); }
	inline int32_t get_U3CprocessingU3Ek__BackingField_1() const { return ___U3CprocessingU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CprocessingU3Ek__BackingField_1() { return &___U3CprocessingU3Ek__BackingField_1; }
	inline void set_U3CprocessingU3Ek__BackingField_1(int32_t value)
	{
		___U3CprocessingU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CfailedU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(DocumentCounts_t1301920119, ___U3CfailedU3Ek__BackingField_2)); }
	inline int32_t get_U3CfailedU3Ek__BackingField_2() const { return ___U3CfailedU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CfailedU3Ek__BackingField_2() { return &___U3CfailedU3Ek__BackingField_2; }
	inline void set_U3CfailedU3Ek__BackingField_2(int32_t value)
	{
		___U3CfailedU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOCUMENTCOUNTS_T1301920119_H
#ifndef REQUEST_T466816980_H
#define REQUEST_T466816980_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request
struct  Request_t466816980  : public RuntimeObject
{
public:
	// System.Single IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Timeout>k__BackingField
	float ___U3CTimeoutU3Ek__BackingField_0;
	// System.Boolean IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Cancel>k__BackingField
	bool ___U3CCancelU3Ek__BackingField_1;
	// System.Boolean IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Delete>k__BackingField
	bool ___U3CDeleteU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Function>k__BackingField
	String_t* ___U3CFunctionU3Ek__BackingField_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Parameters>k__BackingField
	Dictionary_2_t309261261 * ___U3CParametersU3Ek__BackingField_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Headers>k__BackingField
	Dictionary_2_t3943999495 * ___U3CHeadersU3Ek__BackingField_5;
	// System.Byte[] IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Send>k__BackingField
	ByteU5BU5D_t3397334013* ___U3CSendU3Ek__BackingField_6;
	// System.Collections.Generic.Dictionary`2<System.String,IBM.Watson.DeveloperCloud.Connection.RESTConnector/Form> IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Forms>k__BackingField
	Dictionary_2_t2694055125 * ___U3CFormsU3Ek__BackingField_7;
	// IBM.Watson.DeveloperCloud.Connection.RESTConnector/ResponseEvent IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<OnResponse>k__BackingField
	ResponseEvent_t1616568356 * ___U3COnResponseU3Ek__BackingField_8;
	// IBM.Watson.DeveloperCloud.Connection.RESTConnector/ProgressEvent IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<OnDownloadProgress>k__BackingField
	ProgressEvent_t4185145044 * ___U3COnDownloadProgressU3Ek__BackingField_9;
	// IBM.Watson.DeveloperCloud.Connection.RESTConnector/ProgressEvent IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<OnUploadProgress>k__BackingField
	ProgressEvent_t4185145044 * ___U3COnUploadProgressU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of_U3CTimeoutU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CTimeoutU3Ek__BackingField_0)); }
	inline float get_U3CTimeoutU3Ek__BackingField_0() const { return ___U3CTimeoutU3Ek__BackingField_0; }
	inline float* get_address_of_U3CTimeoutU3Ek__BackingField_0() { return &___U3CTimeoutU3Ek__BackingField_0; }
	inline void set_U3CTimeoutU3Ek__BackingField_0(float value)
	{
		___U3CTimeoutU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CCancelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CCancelU3Ek__BackingField_1)); }
	inline bool get_U3CCancelU3Ek__BackingField_1() const { return ___U3CCancelU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CCancelU3Ek__BackingField_1() { return &___U3CCancelU3Ek__BackingField_1; }
	inline void set_U3CCancelU3Ek__BackingField_1(bool value)
	{
		___U3CCancelU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CDeleteU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CDeleteU3Ek__BackingField_2)); }
	inline bool get_U3CDeleteU3Ek__BackingField_2() const { return ___U3CDeleteU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CDeleteU3Ek__BackingField_2() { return &___U3CDeleteU3Ek__BackingField_2; }
	inline void set_U3CDeleteU3Ek__BackingField_2(bool value)
	{
		___U3CDeleteU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CFunctionU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CFunctionU3Ek__BackingField_3)); }
	inline String_t* get_U3CFunctionU3Ek__BackingField_3() const { return ___U3CFunctionU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CFunctionU3Ek__BackingField_3() { return &___U3CFunctionU3Ek__BackingField_3; }
	inline void set_U3CFunctionU3Ek__BackingField_3(String_t* value)
	{
		___U3CFunctionU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFunctionU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CParametersU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CParametersU3Ek__BackingField_4)); }
	inline Dictionary_2_t309261261 * get_U3CParametersU3Ek__BackingField_4() const { return ___U3CParametersU3Ek__BackingField_4; }
	inline Dictionary_2_t309261261 ** get_address_of_U3CParametersU3Ek__BackingField_4() { return &___U3CParametersU3Ek__BackingField_4; }
	inline void set_U3CParametersU3Ek__BackingField_4(Dictionary_2_t309261261 * value)
	{
		___U3CParametersU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CParametersU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CHeadersU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CHeadersU3Ek__BackingField_5)); }
	inline Dictionary_2_t3943999495 * get_U3CHeadersU3Ek__BackingField_5() const { return ___U3CHeadersU3Ek__BackingField_5; }
	inline Dictionary_2_t3943999495 ** get_address_of_U3CHeadersU3Ek__BackingField_5() { return &___U3CHeadersU3Ek__BackingField_5; }
	inline void set_U3CHeadersU3Ek__BackingField_5(Dictionary_2_t3943999495 * value)
	{
		___U3CHeadersU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CHeadersU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CSendU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CSendU3Ek__BackingField_6)); }
	inline ByteU5BU5D_t3397334013* get_U3CSendU3Ek__BackingField_6() const { return ___U3CSendU3Ek__BackingField_6; }
	inline ByteU5BU5D_t3397334013** get_address_of_U3CSendU3Ek__BackingField_6() { return &___U3CSendU3Ek__BackingField_6; }
	inline void set_U3CSendU3Ek__BackingField_6(ByteU5BU5D_t3397334013* value)
	{
		___U3CSendU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSendU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CFormsU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CFormsU3Ek__BackingField_7)); }
	inline Dictionary_2_t2694055125 * get_U3CFormsU3Ek__BackingField_7() const { return ___U3CFormsU3Ek__BackingField_7; }
	inline Dictionary_2_t2694055125 ** get_address_of_U3CFormsU3Ek__BackingField_7() { return &___U3CFormsU3Ek__BackingField_7; }
	inline void set_U3CFormsU3Ek__BackingField_7(Dictionary_2_t2694055125 * value)
	{
		___U3CFormsU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFormsU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3COnResponseU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3COnResponseU3Ek__BackingField_8)); }
	inline ResponseEvent_t1616568356 * get_U3COnResponseU3Ek__BackingField_8() const { return ___U3COnResponseU3Ek__BackingField_8; }
	inline ResponseEvent_t1616568356 ** get_address_of_U3COnResponseU3Ek__BackingField_8() { return &___U3COnResponseU3Ek__BackingField_8; }
	inline void set_U3COnResponseU3Ek__BackingField_8(ResponseEvent_t1616568356 * value)
	{
		___U3COnResponseU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3COnResponseU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3COnDownloadProgressU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3COnDownloadProgressU3Ek__BackingField_9)); }
	inline ProgressEvent_t4185145044 * get_U3COnDownloadProgressU3Ek__BackingField_9() const { return ___U3COnDownloadProgressU3Ek__BackingField_9; }
	inline ProgressEvent_t4185145044 ** get_address_of_U3COnDownloadProgressU3Ek__BackingField_9() { return &___U3COnDownloadProgressU3Ek__BackingField_9; }
	inline void set_U3COnDownloadProgressU3Ek__BackingField_9(ProgressEvent_t4185145044 * value)
	{
		___U3COnDownloadProgressU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3COnDownloadProgressU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3COnUploadProgressU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3COnUploadProgressU3Ek__BackingField_10)); }
	inline ProgressEvent_t4185145044 * get_U3COnUploadProgressU3Ek__BackingField_10() const { return ___U3COnUploadProgressU3Ek__BackingField_10; }
	inline ProgressEvent_t4185145044 ** get_address_of_U3COnUploadProgressU3Ek__BackingField_10() { return &___U3COnUploadProgressU3Ek__BackingField_10; }
	inline void set_U3COnUploadProgressU3Ek__BackingField_10(ProgressEvent_t4185145044 * value)
	{
		___U3COnUploadProgressU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3COnUploadProgressU3Ek__BackingField_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUEST_T466816980_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef COLLECTIONREF_T3248558799_H
#define COLLECTIONREF_T3248558799_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.CollectionRef
struct  CollectionRef_t3248558799  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.CollectionRef::<collection_id>k__BackingField
	String_t* ___U3Ccollection_idU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.CollectionRef::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.CollectionRef::<description>k__BackingField
	String_t* ___U3CdescriptionU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.CollectionRef::<created>k__BackingField
	String_t* ___U3CcreatedU3Ek__BackingField_3;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.CollectionRef::<updated>k__BackingField
	String_t* ___U3CupdatedU3Ek__BackingField_4;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.CollectionRef::<status>k__BackingField
	String_t* ___U3CstatusU3Ek__BackingField_5;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.CollectionRef::<configuration_id>k__BackingField
	String_t* ___U3Cconfiguration_idU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3Ccollection_idU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CollectionRef_t3248558799, ___U3Ccollection_idU3Ek__BackingField_0)); }
	inline String_t* get_U3Ccollection_idU3Ek__BackingField_0() const { return ___U3Ccollection_idU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3Ccollection_idU3Ek__BackingField_0() { return &___U3Ccollection_idU3Ek__BackingField_0; }
	inline void set_U3Ccollection_idU3Ek__BackingField_0(String_t* value)
	{
		___U3Ccollection_idU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Ccollection_idU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CollectionRef_t3248558799, ___U3CnameU3Ek__BackingField_1)); }
	inline String_t* get_U3CnameU3Ek__BackingField_1() const { return ___U3CnameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_1() { return &___U3CnameU3Ek__BackingField_1; }
	inline void set_U3CnameU3Ek__BackingField_1(String_t* value)
	{
		___U3CnameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CdescriptionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CollectionRef_t3248558799, ___U3CdescriptionU3Ek__BackingField_2)); }
	inline String_t* get_U3CdescriptionU3Ek__BackingField_2() const { return ___U3CdescriptionU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CdescriptionU3Ek__BackingField_2() { return &___U3CdescriptionU3Ek__BackingField_2; }
	inline void set_U3CdescriptionU3Ek__BackingField_2(String_t* value)
	{
		___U3CdescriptionU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdescriptionU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CcreatedU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(CollectionRef_t3248558799, ___U3CcreatedU3Ek__BackingField_3)); }
	inline String_t* get_U3CcreatedU3Ek__BackingField_3() const { return ___U3CcreatedU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CcreatedU3Ek__BackingField_3() { return &___U3CcreatedU3Ek__BackingField_3; }
	inline void set_U3CcreatedU3Ek__BackingField_3(String_t* value)
	{
		___U3CcreatedU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcreatedU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CupdatedU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(CollectionRef_t3248558799, ___U3CupdatedU3Ek__BackingField_4)); }
	inline String_t* get_U3CupdatedU3Ek__BackingField_4() const { return ___U3CupdatedU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CupdatedU3Ek__BackingField_4() { return &___U3CupdatedU3Ek__BackingField_4; }
	inline void set_U3CupdatedU3Ek__BackingField_4(String_t* value)
	{
		___U3CupdatedU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CupdatedU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CstatusU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(CollectionRef_t3248558799, ___U3CstatusU3Ek__BackingField_5)); }
	inline String_t* get_U3CstatusU3Ek__BackingField_5() const { return ___U3CstatusU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CstatusU3Ek__BackingField_5() { return &___U3CstatusU3Ek__BackingField_5; }
	inline void set_U3CstatusU3Ek__BackingField_5(String_t* value)
	{
		___U3CstatusU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstatusU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3Cconfiguration_idU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(CollectionRef_t3248558799, ___U3Cconfiguration_idU3Ek__BackingField_6)); }
	inline String_t* get_U3Cconfiguration_idU3Ek__BackingField_6() const { return ___U3Cconfiguration_idU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3Cconfiguration_idU3Ek__BackingField_6() { return &___U3Cconfiguration_idU3Ek__BackingField_6; }
	inline void set_U3Cconfiguration_idU3Ek__BackingField_6(String_t* value)
	{
		___U3Cconfiguration_idU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cconfiguration_idU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTIONREF_T3248558799_H
#ifndef DELETECOLLECTIONRESPONSE_T263138046_H
#define DELETECOLLECTIONRESPONSE_T263138046_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.DeleteCollectionResponse
struct  DeleteCollectionResponse_t263138046  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.DeleteCollectionResponse::<collection_id>k__BackingField
	String_t* ___U3Ccollection_idU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.DeleteCollectionResponse::<status>k__BackingField
	String_t* ___U3CstatusU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3Ccollection_idU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DeleteCollectionResponse_t263138046, ___U3Ccollection_idU3Ek__BackingField_0)); }
	inline String_t* get_U3Ccollection_idU3Ek__BackingField_0() const { return ___U3Ccollection_idU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3Ccollection_idU3Ek__BackingField_0() { return &___U3Ccollection_idU3Ek__BackingField_0; }
	inline void set_U3Ccollection_idU3Ek__BackingField_0(String_t* value)
	{
		___U3Ccollection_idU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Ccollection_idU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CstatusU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(DeleteCollectionResponse_t263138046, ___U3CstatusU3Ek__BackingField_1)); }
	inline String_t* get_U3CstatusU3Ek__BackingField_1() const { return ___U3CstatusU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CstatusU3Ek__BackingField_1() { return &___U3CstatusU3Ek__BackingField_1; }
	inline void set_U3CstatusU3Ek__BackingField_1(String_t* value)
	{
		___U3CstatusU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstatusU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELETECOLLECTIONRESPONSE_T263138046_H
#ifndef COLLECTION_T3355621610_H
#define COLLECTION_T3355621610_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Collection
struct  Collection_t3355621610  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Collection::<collection_id>k__BackingField
	String_t* ___U3Ccollection_idU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Collection::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Collection::<description>k__BackingField
	String_t* ___U3CdescriptionU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Collection::<created>k__BackingField
	String_t* ___U3CcreatedU3Ek__BackingField_3;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Collection::<updated>k__BackingField
	String_t* ___U3CupdatedU3Ek__BackingField_4;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Collection::<status>k__BackingField
	String_t* ___U3CstatusU3Ek__BackingField_5;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Collection::<configuration_id>k__BackingField
	String_t* ___U3Cconfiguration_idU3Ek__BackingField_6;
	// IBM.Watson.DeveloperCloud.Services.Discovery.v1.DocumentCounts IBM.Watson.DeveloperCloud.Services.Discovery.v1.Collection::<document_counts>k__BackingField
	DocumentCounts_t1301920119 * ___U3Cdocument_countsU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_U3Ccollection_idU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Collection_t3355621610, ___U3Ccollection_idU3Ek__BackingField_0)); }
	inline String_t* get_U3Ccollection_idU3Ek__BackingField_0() const { return ___U3Ccollection_idU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3Ccollection_idU3Ek__BackingField_0() { return &___U3Ccollection_idU3Ek__BackingField_0; }
	inline void set_U3Ccollection_idU3Ek__BackingField_0(String_t* value)
	{
		___U3Ccollection_idU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Ccollection_idU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Collection_t3355621610, ___U3CnameU3Ek__BackingField_1)); }
	inline String_t* get_U3CnameU3Ek__BackingField_1() const { return ___U3CnameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_1() { return &___U3CnameU3Ek__BackingField_1; }
	inline void set_U3CnameU3Ek__BackingField_1(String_t* value)
	{
		___U3CnameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CdescriptionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Collection_t3355621610, ___U3CdescriptionU3Ek__BackingField_2)); }
	inline String_t* get_U3CdescriptionU3Ek__BackingField_2() const { return ___U3CdescriptionU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CdescriptionU3Ek__BackingField_2() { return &___U3CdescriptionU3Ek__BackingField_2; }
	inline void set_U3CdescriptionU3Ek__BackingField_2(String_t* value)
	{
		___U3CdescriptionU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdescriptionU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CcreatedU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Collection_t3355621610, ___U3CcreatedU3Ek__BackingField_3)); }
	inline String_t* get_U3CcreatedU3Ek__BackingField_3() const { return ___U3CcreatedU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CcreatedU3Ek__BackingField_3() { return &___U3CcreatedU3Ek__BackingField_3; }
	inline void set_U3CcreatedU3Ek__BackingField_3(String_t* value)
	{
		___U3CcreatedU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcreatedU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CupdatedU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Collection_t3355621610, ___U3CupdatedU3Ek__BackingField_4)); }
	inline String_t* get_U3CupdatedU3Ek__BackingField_4() const { return ___U3CupdatedU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CupdatedU3Ek__BackingField_4() { return &___U3CupdatedU3Ek__BackingField_4; }
	inline void set_U3CupdatedU3Ek__BackingField_4(String_t* value)
	{
		___U3CupdatedU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CupdatedU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CstatusU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Collection_t3355621610, ___U3CstatusU3Ek__BackingField_5)); }
	inline String_t* get_U3CstatusU3Ek__BackingField_5() const { return ___U3CstatusU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CstatusU3Ek__BackingField_5() { return &___U3CstatusU3Ek__BackingField_5; }
	inline void set_U3CstatusU3Ek__BackingField_5(String_t* value)
	{
		___U3CstatusU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstatusU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3Cconfiguration_idU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Collection_t3355621610, ___U3Cconfiguration_idU3Ek__BackingField_6)); }
	inline String_t* get_U3Cconfiguration_idU3Ek__BackingField_6() const { return ___U3Cconfiguration_idU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3Cconfiguration_idU3Ek__BackingField_6() { return &___U3Cconfiguration_idU3Ek__BackingField_6; }
	inline void set_U3Cconfiguration_idU3Ek__BackingField_6(String_t* value)
	{
		___U3Cconfiguration_idU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cconfiguration_idU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3Cdocument_countsU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(Collection_t3355621610, ___U3Cdocument_countsU3Ek__BackingField_7)); }
	inline DocumentCounts_t1301920119 * get_U3Cdocument_countsU3Ek__BackingField_7() const { return ___U3Cdocument_countsU3Ek__BackingField_7; }
	inline DocumentCounts_t1301920119 ** get_address_of_U3Cdocument_countsU3Ek__BackingField_7() { return &___U3Cdocument_countsU3Ek__BackingField_7; }
	inline void set_U3Cdocument_countsU3Ek__BackingField_7(DocumentCounts_t1301920119 * value)
	{
		___U3Cdocument_countsU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cdocument_countsU3Ek__BackingField_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTION_T3355621610_H
#ifndef CONFIGURATION_T2574692812_H
#define CONFIGURATION_T2574692812_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Configuration
struct  Configuration_t2574692812  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Configuration::<configuration_id>k__BackingField
	String_t* ___U3Cconfiguration_idU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Configuration::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Configuration::<created>k__BackingField
	String_t* ___U3CcreatedU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Configuration::<updated>k__BackingField
	String_t* ___U3CupdatedU3Ek__BackingField_3;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Configuration::<description>k__BackingField
	String_t* ___U3CdescriptionU3Ek__BackingField_4;
	// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Conversions IBM.Watson.DeveloperCloud.Services.Discovery.v1.Configuration::<conversions>k__BackingField
	Conversions_t3726444191 * ___U3CconversionsU3Ek__BackingField_5;
	// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Enrichment[] IBM.Watson.DeveloperCloud.Services.Discovery.v1.Configuration::<enrichments>k__BackingField
	EnrichmentU5BU5D_t3336389812* ___U3CenrichmentsU3Ek__BackingField_6;
	// IBM.Watson.DeveloperCloud.Services.Discovery.v1.NormalizationOperation[] IBM.Watson.DeveloperCloud.Services.Discovery.v1.Configuration::<normalizations>k__BackingField
	NormalizationOperationU5BU5D_t1906606437* ___U3CnormalizationsU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_U3Cconfiguration_idU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Configuration_t2574692812, ___U3Cconfiguration_idU3Ek__BackingField_0)); }
	inline String_t* get_U3Cconfiguration_idU3Ek__BackingField_0() const { return ___U3Cconfiguration_idU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3Cconfiguration_idU3Ek__BackingField_0() { return &___U3Cconfiguration_idU3Ek__BackingField_0; }
	inline void set_U3Cconfiguration_idU3Ek__BackingField_0(String_t* value)
	{
		___U3Cconfiguration_idU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cconfiguration_idU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Configuration_t2574692812, ___U3CnameU3Ek__BackingField_1)); }
	inline String_t* get_U3CnameU3Ek__BackingField_1() const { return ___U3CnameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_1() { return &___U3CnameU3Ek__BackingField_1; }
	inline void set_U3CnameU3Ek__BackingField_1(String_t* value)
	{
		___U3CnameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CcreatedU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Configuration_t2574692812, ___U3CcreatedU3Ek__BackingField_2)); }
	inline String_t* get_U3CcreatedU3Ek__BackingField_2() const { return ___U3CcreatedU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CcreatedU3Ek__BackingField_2() { return &___U3CcreatedU3Ek__BackingField_2; }
	inline void set_U3CcreatedU3Ek__BackingField_2(String_t* value)
	{
		___U3CcreatedU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcreatedU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CupdatedU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Configuration_t2574692812, ___U3CupdatedU3Ek__BackingField_3)); }
	inline String_t* get_U3CupdatedU3Ek__BackingField_3() const { return ___U3CupdatedU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CupdatedU3Ek__BackingField_3() { return &___U3CupdatedU3Ek__BackingField_3; }
	inline void set_U3CupdatedU3Ek__BackingField_3(String_t* value)
	{
		___U3CupdatedU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CupdatedU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CdescriptionU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Configuration_t2574692812, ___U3CdescriptionU3Ek__BackingField_4)); }
	inline String_t* get_U3CdescriptionU3Ek__BackingField_4() const { return ___U3CdescriptionU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CdescriptionU3Ek__BackingField_4() { return &___U3CdescriptionU3Ek__BackingField_4; }
	inline void set_U3CdescriptionU3Ek__BackingField_4(String_t* value)
	{
		___U3CdescriptionU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdescriptionU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CconversionsU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Configuration_t2574692812, ___U3CconversionsU3Ek__BackingField_5)); }
	inline Conversions_t3726444191 * get_U3CconversionsU3Ek__BackingField_5() const { return ___U3CconversionsU3Ek__BackingField_5; }
	inline Conversions_t3726444191 ** get_address_of_U3CconversionsU3Ek__BackingField_5() { return &___U3CconversionsU3Ek__BackingField_5; }
	inline void set_U3CconversionsU3Ek__BackingField_5(Conversions_t3726444191 * value)
	{
		___U3CconversionsU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CconversionsU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CenrichmentsU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Configuration_t2574692812, ___U3CenrichmentsU3Ek__BackingField_6)); }
	inline EnrichmentU5BU5D_t3336389812* get_U3CenrichmentsU3Ek__BackingField_6() const { return ___U3CenrichmentsU3Ek__BackingField_6; }
	inline EnrichmentU5BU5D_t3336389812** get_address_of_U3CenrichmentsU3Ek__BackingField_6() { return &___U3CenrichmentsU3Ek__BackingField_6; }
	inline void set_U3CenrichmentsU3Ek__BackingField_6(EnrichmentU5BU5D_t3336389812* value)
	{
		___U3CenrichmentsU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CenrichmentsU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CnormalizationsU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(Configuration_t2574692812, ___U3CnormalizationsU3Ek__BackingField_7)); }
	inline NormalizationOperationU5BU5D_t1906606437* get_U3CnormalizationsU3Ek__BackingField_7() const { return ___U3CnormalizationsU3Ek__BackingField_7; }
	inline NormalizationOperationU5BU5D_t1906606437** get_address_of_U3CnormalizationsU3Ek__BackingField_7() { return &___U3CnormalizationsU3Ek__BackingField_7; }
	inline void set_U3CnormalizationsU3Ek__BackingField_7(NormalizationOperationU5BU5D_t1906606437* value)
	{
		___U3CnormalizationsU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnormalizationsU3Ek__BackingField_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATION_T2574692812_H
#ifndef CONVERSIONS_T3726444191_H
#define CONVERSIONS_T3726444191_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Conversions
struct  Conversions_t3726444191  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.Discovery.v1.PdfSettings IBM.Watson.DeveloperCloud.Services.Discovery.v1.Conversions::<pdf>k__BackingField
	PdfSettings_t1519277167 * ___U3CpdfU3Ek__BackingField_0;
	// IBM.Watson.DeveloperCloud.Services.Discovery.v1.WordSettings IBM.Watson.DeveloperCloud.Services.Discovery.v1.Conversions::<word>k__BackingField
	WordSettings_t2098347111 * ___U3CwordU3Ek__BackingField_1;
	// IBM.Watson.DeveloperCloud.Services.Discovery.v1.HtmlSettings IBM.Watson.DeveloperCloud.Services.Discovery.v1.Conversions::<html>k__BackingField
	HtmlSettings_t99959826 * ___U3ChtmlU3Ek__BackingField_2;
	// IBM.Watson.DeveloperCloud.Services.Discovery.v1.NormalizationOperation[] IBM.Watson.DeveloperCloud.Services.Discovery.v1.Conversions::<json_normalizations>k__BackingField
	NormalizationOperationU5BU5D_t1906606437* ___U3Cjson_normalizationsU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CpdfU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Conversions_t3726444191, ___U3CpdfU3Ek__BackingField_0)); }
	inline PdfSettings_t1519277167 * get_U3CpdfU3Ek__BackingField_0() const { return ___U3CpdfU3Ek__BackingField_0; }
	inline PdfSettings_t1519277167 ** get_address_of_U3CpdfU3Ek__BackingField_0() { return &___U3CpdfU3Ek__BackingField_0; }
	inline void set_U3CpdfU3Ek__BackingField_0(PdfSettings_t1519277167 * value)
	{
		___U3CpdfU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpdfU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CwordU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Conversions_t3726444191, ___U3CwordU3Ek__BackingField_1)); }
	inline WordSettings_t2098347111 * get_U3CwordU3Ek__BackingField_1() const { return ___U3CwordU3Ek__BackingField_1; }
	inline WordSettings_t2098347111 ** get_address_of_U3CwordU3Ek__BackingField_1() { return &___U3CwordU3Ek__BackingField_1; }
	inline void set_U3CwordU3Ek__BackingField_1(WordSettings_t2098347111 * value)
	{
		___U3CwordU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwordU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3ChtmlU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Conversions_t3726444191, ___U3ChtmlU3Ek__BackingField_2)); }
	inline HtmlSettings_t99959826 * get_U3ChtmlU3Ek__BackingField_2() const { return ___U3ChtmlU3Ek__BackingField_2; }
	inline HtmlSettings_t99959826 ** get_address_of_U3ChtmlU3Ek__BackingField_2() { return &___U3ChtmlU3Ek__BackingField_2; }
	inline void set_U3ChtmlU3Ek__BackingField_2(HtmlSettings_t99959826 * value)
	{
		___U3ChtmlU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3ChtmlU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3Cjson_normalizationsU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Conversions_t3726444191, ___U3Cjson_normalizationsU3Ek__BackingField_3)); }
	inline NormalizationOperationU5BU5D_t1906606437* get_U3Cjson_normalizationsU3Ek__BackingField_3() const { return ___U3Cjson_normalizationsU3Ek__BackingField_3; }
	inline NormalizationOperationU5BU5D_t1906606437** get_address_of_U3Cjson_normalizationsU3Ek__BackingField_3() { return &___U3Cjson_normalizationsU3Ek__BackingField_3; }
	inline void set_U3Cjson_normalizationsU3Ek__BackingField_3(NormalizationOperationU5BU5D_t1906606437* value)
	{
		___U3Cjson_normalizationsU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cjson_normalizationsU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONVERSIONS_T3726444191_H
#ifndef PDFSETTINGS_T1519277167_H
#define PDFSETTINGS_T1519277167_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.PdfSettings
struct  PdfSettings_t1519277167  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.Discovery.v1.PdfHeadingDetection IBM.Watson.DeveloperCloud.Services.Discovery.v1.PdfSettings::<heading>k__BackingField
	PdfHeadingDetection_t2648201765 * ___U3CheadingU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CheadingU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(PdfSettings_t1519277167, ___U3CheadingU3Ek__BackingField_0)); }
	inline PdfHeadingDetection_t2648201765 * get_U3CheadingU3Ek__BackingField_0() const { return ___U3CheadingU3Ek__BackingField_0; }
	inline PdfHeadingDetection_t2648201765 ** get_address_of_U3CheadingU3Ek__BackingField_0() { return &___U3CheadingU3Ek__BackingField_0; }
	inline void set_U3CheadingU3Ek__BackingField_0(PdfHeadingDetection_t2648201765 * value)
	{
		___U3CheadingU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CheadingU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PDFSETTINGS_T1519277167_H
#ifndef NOTICE_T1592579614_H
#define NOTICE_T1592579614_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Notice
struct  Notice_t1592579614  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Notice::<notice_id>k__BackingField
	String_t* ___U3Cnotice_idU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Notice::<created>k__BackingField
	String_t* ___U3CcreatedU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Notice::<document_id>k__BackingField
	String_t* ___U3Cdocument_idU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Notice::<severity>k__BackingField
	String_t* ___U3CseverityU3Ek__BackingField_3;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Notice::<step>k__BackingField
	String_t* ___U3CstepU3Ek__BackingField_4;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Notice::<description>k__BackingField
	String_t* ___U3CdescriptionU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3Cnotice_idU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Notice_t1592579614, ___U3Cnotice_idU3Ek__BackingField_0)); }
	inline String_t* get_U3Cnotice_idU3Ek__BackingField_0() const { return ___U3Cnotice_idU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3Cnotice_idU3Ek__BackingField_0() { return &___U3Cnotice_idU3Ek__BackingField_0; }
	inline void set_U3Cnotice_idU3Ek__BackingField_0(String_t* value)
	{
		___U3Cnotice_idU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cnotice_idU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CcreatedU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Notice_t1592579614, ___U3CcreatedU3Ek__BackingField_1)); }
	inline String_t* get_U3CcreatedU3Ek__BackingField_1() const { return ___U3CcreatedU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CcreatedU3Ek__BackingField_1() { return &___U3CcreatedU3Ek__BackingField_1; }
	inline void set_U3CcreatedU3Ek__BackingField_1(String_t* value)
	{
		___U3CcreatedU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcreatedU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3Cdocument_idU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Notice_t1592579614, ___U3Cdocument_idU3Ek__BackingField_2)); }
	inline String_t* get_U3Cdocument_idU3Ek__BackingField_2() const { return ___U3Cdocument_idU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3Cdocument_idU3Ek__BackingField_2() { return &___U3Cdocument_idU3Ek__BackingField_2; }
	inline void set_U3Cdocument_idU3Ek__BackingField_2(String_t* value)
	{
		___U3Cdocument_idU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cdocument_idU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CseverityU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Notice_t1592579614, ___U3CseverityU3Ek__BackingField_3)); }
	inline String_t* get_U3CseverityU3Ek__BackingField_3() const { return ___U3CseverityU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CseverityU3Ek__BackingField_3() { return &___U3CseverityU3Ek__BackingField_3; }
	inline void set_U3CseverityU3Ek__BackingField_3(String_t* value)
	{
		___U3CseverityU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CseverityU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CstepU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Notice_t1592579614, ___U3CstepU3Ek__BackingField_4)); }
	inline String_t* get_U3CstepU3Ek__BackingField_4() const { return ___U3CstepU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CstepU3Ek__BackingField_4() { return &___U3CstepU3Ek__BackingField_4; }
	inline void set_U3CstepU3Ek__BackingField_4(String_t* value)
	{
		___U3CstepU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstepU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CdescriptionU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Notice_t1592579614, ___U3CdescriptionU3Ek__BackingField_5)); }
	inline String_t* get_U3CdescriptionU3Ek__BackingField_5() const { return ___U3CdescriptionU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CdescriptionU3Ek__BackingField_5() { return &___U3CdescriptionU3Ek__BackingField_5; }
	inline void set_U3CdescriptionU3Ek__BackingField_5(String_t* value)
	{
		___U3CdescriptionU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdescriptionU3Ek__BackingField_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTICE_T1592579614_H
#ifndef GETCONFIGURATIONSRESPONSE_T3984912378_H
#define GETCONFIGURATIONSRESPONSE_T3984912378_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.GetConfigurationsResponse
struct  GetConfigurationsResponse_t3984912378  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.Discovery.v1.ConfigurationRef[] IBM.Watson.DeveloperCloud.Services.Discovery.v1.GetConfigurationsResponse::<configurations>k__BackingField
	ConfigurationRefU5BU5D_t2410926920* ___U3CconfigurationsU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CconfigurationsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(GetConfigurationsResponse_t3984912378, ___U3CconfigurationsU3Ek__BackingField_0)); }
	inline ConfigurationRefU5BU5D_t2410926920* get_U3CconfigurationsU3Ek__BackingField_0() const { return ___U3CconfigurationsU3Ek__BackingField_0; }
	inline ConfigurationRefU5BU5D_t2410926920** get_address_of_U3CconfigurationsU3Ek__BackingField_0() { return &___U3CconfigurationsU3Ek__BackingField_0; }
	inline void set_U3CconfigurationsU3Ek__BackingField_0(ConfigurationRefU5BU5D_t2410926920* value)
	{
		___U3CconfigurationsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CconfigurationsU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETCONFIGURATIONSRESPONSE_T3984912378_H
#ifndef CONFIGURATIONREF_T2639036165_H
#define CONFIGURATIONREF_T2639036165_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.ConfigurationRef
struct  ConfigurationRef_t2639036165  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.ConfigurationRef::<configuration_id>k__BackingField
	String_t* ___U3Cconfiguration_idU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.ConfigurationRef::<created>k__BackingField
	String_t* ___U3CcreatedU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.ConfigurationRef::<updated>k__BackingField
	String_t* ___U3CupdatedU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.ConfigurationRef::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_3;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.ConfigurationRef::<description>k__BackingField
	String_t* ___U3CdescriptionU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3Cconfiguration_idU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ConfigurationRef_t2639036165, ___U3Cconfiguration_idU3Ek__BackingField_0)); }
	inline String_t* get_U3Cconfiguration_idU3Ek__BackingField_0() const { return ___U3Cconfiguration_idU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3Cconfiguration_idU3Ek__BackingField_0() { return &___U3Cconfiguration_idU3Ek__BackingField_0; }
	inline void set_U3Cconfiguration_idU3Ek__BackingField_0(String_t* value)
	{
		___U3Cconfiguration_idU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cconfiguration_idU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CcreatedU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ConfigurationRef_t2639036165, ___U3CcreatedU3Ek__BackingField_1)); }
	inline String_t* get_U3CcreatedU3Ek__BackingField_1() const { return ___U3CcreatedU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CcreatedU3Ek__BackingField_1() { return &___U3CcreatedU3Ek__BackingField_1; }
	inline void set_U3CcreatedU3Ek__BackingField_1(String_t* value)
	{
		___U3CcreatedU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcreatedU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CupdatedU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ConfigurationRef_t2639036165, ___U3CupdatedU3Ek__BackingField_2)); }
	inline String_t* get_U3CupdatedU3Ek__BackingField_2() const { return ___U3CupdatedU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CupdatedU3Ek__BackingField_2() { return &___U3CupdatedU3Ek__BackingField_2; }
	inline void set_U3CupdatedU3Ek__BackingField_2(String_t* value)
	{
		___U3CupdatedU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CupdatedU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ConfigurationRef_t2639036165, ___U3CnameU3Ek__BackingField_3)); }
	inline String_t* get_U3CnameU3Ek__BackingField_3() const { return ___U3CnameU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_3() { return &___U3CnameU3Ek__BackingField_3; }
	inline void set_U3CnameU3Ek__BackingField_3(String_t* value)
	{
		___U3CnameU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CdescriptionU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(ConfigurationRef_t2639036165, ___U3CdescriptionU3Ek__BackingField_4)); }
	inline String_t* get_U3CdescriptionU3Ek__BackingField_4() const { return ___U3CdescriptionU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CdescriptionU3Ek__BackingField_4() { return &___U3CdescriptionU3Ek__BackingField_4; }
	inline void set_U3CdescriptionU3Ek__BackingField_4(String_t* value)
	{
		___U3CdescriptionU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdescriptionU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONREF_T2639036165_H
#ifndef NORMALIZATIONOPERATION_T3873208172_H
#define NORMALIZATIONOPERATION_T3873208172_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.NormalizationOperation
struct  NormalizationOperation_t3873208172  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.NormalizationOperation::<operation>k__BackingField
	String_t* ___U3CoperationU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.NormalizationOperation::<source_field>k__BackingField
	String_t* ___U3Csource_fieldU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.NormalizationOperation::<destination_field>k__BackingField
	String_t* ___U3Cdestination_fieldU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CoperationU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(NormalizationOperation_t3873208172, ___U3CoperationU3Ek__BackingField_0)); }
	inline String_t* get_U3CoperationU3Ek__BackingField_0() const { return ___U3CoperationU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CoperationU3Ek__BackingField_0() { return &___U3CoperationU3Ek__BackingField_0; }
	inline void set_U3CoperationU3Ek__BackingField_0(String_t* value)
	{
		___U3CoperationU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CoperationU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3Csource_fieldU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(NormalizationOperation_t3873208172, ___U3Csource_fieldU3Ek__BackingField_1)); }
	inline String_t* get_U3Csource_fieldU3Ek__BackingField_1() const { return ___U3Csource_fieldU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3Csource_fieldU3Ek__BackingField_1() { return &___U3Csource_fieldU3Ek__BackingField_1; }
	inline void set_U3Csource_fieldU3Ek__BackingField_1(String_t* value)
	{
		___U3Csource_fieldU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3Csource_fieldU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3Cdestination_fieldU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(NormalizationOperation_t3873208172, ___U3Cdestination_fieldU3Ek__BackingField_2)); }
	inline String_t* get_U3Cdestination_fieldU3Ek__BackingField_2() const { return ___U3Cdestination_fieldU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3Cdestination_fieldU3Ek__BackingField_2() { return &___U3Cdestination_fieldU3Ek__BackingField_2; }
	inline void set_U3Cdestination_fieldU3Ek__BackingField_2(String_t* value)
	{
		___U3Cdestination_fieldU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cdestination_fieldU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NORMALIZATIONOPERATION_T3873208172_H
#ifndef PDFHEADINGDETECTION_T2648201765_H
#define PDFHEADINGDETECTION_T2648201765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.PdfHeadingDetection
struct  PdfHeadingDetection_t2648201765  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.Discovery.v1.FontSetting[] IBM.Watson.DeveloperCloud.Services.Discovery.v1.PdfHeadingDetection::<fonts>k__BackingField
	FontSettingU5BU5D_t2456190136* ___U3CfontsU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CfontsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(PdfHeadingDetection_t2648201765, ___U3CfontsU3Ek__BackingField_0)); }
	inline FontSettingU5BU5D_t2456190136* get_U3CfontsU3Ek__BackingField_0() const { return ___U3CfontsU3Ek__BackingField_0; }
	inline FontSettingU5BU5D_t2456190136** get_address_of_U3CfontsU3Ek__BackingField_0() { return &___U3CfontsU3Ek__BackingField_0; }
	inline void set_U3CfontsU3Ek__BackingField_0(FontSettingU5BU5D_t2456190136* value)
	{
		___U3CfontsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CfontsU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PDFHEADINGDETECTION_T2648201765_H
#ifndef WORDHEADINGDETECTION_T139434985_H
#define WORDHEADINGDETECTION_T139434985_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.WordHeadingDetection
struct  WordHeadingDetection_t139434985  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.Discovery.v1.FontSetting[] IBM.Watson.DeveloperCloud.Services.Discovery.v1.WordHeadingDetection::<fonts>k__BackingField
	FontSettingU5BU5D_t2456190136* ___U3CfontsU3Ek__BackingField_0;
	// IBM.Watson.DeveloperCloud.Services.Discovery.v1.WordStyle[] IBM.Watson.DeveloperCloud.Services.Discovery.v1.WordHeadingDetection::<styles>k__BackingField
	WordStyleU5BU5D_t1077171056* ___U3CstylesU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CfontsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(WordHeadingDetection_t139434985, ___U3CfontsU3Ek__BackingField_0)); }
	inline FontSettingU5BU5D_t2456190136* get_U3CfontsU3Ek__BackingField_0() const { return ___U3CfontsU3Ek__BackingField_0; }
	inline FontSettingU5BU5D_t2456190136** get_address_of_U3CfontsU3Ek__BackingField_0() { return &___U3CfontsU3Ek__BackingField_0; }
	inline void set_U3CfontsU3Ek__BackingField_0(FontSettingU5BU5D_t2456190136* value)
	{
		___U3CfontsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CfontsU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CstylesU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(WordHeadingDetection_t139434985, ___U3CstylesU3Ek__BackingField_1)); }
	inline WordStyleU5BU5D_t1077171056* get_U3CstylesU3Ek__BackingField_1() const { return ___U3CstylesU3Ek__BackingField_1; }
	inline WordStyleU5BU5D_t1077171056** get_address_of_U3CstylesU3Ek__BackingField_1() { return &___U3CstylesU3Ek__BackingField_1; }
	inline void set_U3CstylesU3Ek__BackingField_1(WordStyleU5BU5D_t1077171056* value)
	{
		___U3CstylesU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstylesU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORDHEADINGDETECTION_T139434985_H
#ifndef WORDSETTINGS_T2098347111_H
#define WORDSETTINGS_T2098347111_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.WordSettings
struct  WordSettings_t2098347111  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.Discovery.v1.WordHeadingDetection IBM.Watson.DeveloperCloud.Services.Discovery.v1.WordSettings::<heading>k__BackingField
	WordHeadingDetection_t139434985 * ___U3CheadingU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CheadingU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(WordSettings_t2098347111, ___U3CheadingU3Ek__BackingField_0)); }
	inline WordHeadingDetection_t139434985 * get_U3CheadingU3Ek__BackingField_0() const { return ___U3CheadingU3Ek__BackingField_0; }
	inline WordHeadingDetection_t139434985 ** get_address_of_U3CheadingU3Ek__BackingField_0() { return &___U3CheadingU3Ek__BackingField_0; }
	inline void set_U3CheadingU3Ek__BackingField_0(WordHeadingDetection_t139434985 * value)
	{
		___U3CheadingU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CheadingU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORDSETTINGS_T2098347111_H
#ifndef HTMLSETTINGS_T99959826_H
#define HTMLSETTINGS_T99959826_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.HtmlSettings
struct  HtmlSettings_t99959826  : public RuntimeObject
{
public:
	// System.String[] IBM.Watson.DeveloperCloud.Services.Discovery.v1.HtmlSettings::<exclude_tags_completely>k__BackingField
	StringU5BU5D_t1642385972* ___U3Cexclude_tags_completelyU3Ek__BackingField_0;
	// System.String[] IBM.Watson.DeveloperCloud.Services.Discovery.v1.HtmlSettings::<exclude_tags_keep_content>k__BackingField
	StringU5BU5D_t1642385972* ___U3Cexclude_tags_keep_contentU3Ek__BackingField_1;
	// IBM.Watson.DeveloperCloud.Services.Discovery.v1.XPathPatterns IBM.Watson.DeveloperCloud.Services.Discovery.v1.HtmlSettings::<keep_content>k__BackingField
	XPathPatterns_t1498040530 * ___U3Ckeep_contentU3Ek__BackingField_2;
	// IBM.Watson.DeveloperCloud.Services.Discovery.v1.XPathPatterns IBM.Watson.DeveloperCloud.Services.Discovery.v1.HtmlSettings::<exclude_content>k__BackingField
	XPathPatterns_t1498040530 * ___U3Cexclude_contentU3Ek__BackingField_3;
	// System.String[] IBM.Watson.DeveloperCloud.Services.Discovery.v1.HtmlSettings::<keep_tag_attributes>k__BackingField
	StringU5BU5D_t1642385972* ___U3Ckeep_tag_attributesU3Ek__BackingField_4;
	// System.String[] IBM.Watson.DeveloperCloud.Services.Discovery.v1.HtmlSettings::<exclude_tag_attributes>k__BackingField
	StringU5BU5D_t1642385972* ___U3Cexclude_tag_attributesU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3Cexclude_tags_completelyU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(HtmlSettings_t99959826, ___U3Cexclude_tags_completelyU3Ek__BackingField_0)); }
	inline StringU5BU5D_t1642385972* get_U3Cexclude_tags_completelyU3Ek__BackingField_0() const { return ___U3Cexclude_tags_completelyU3Ek__BackingField_0; }
	inline StringU5BU5D_t1642385972** get_address_of_U3Cexclude_tags_completelyU3Ek__BackingField_0() { return &___U3Cexclude_tags_completelyU3Ek__BackingField_0; }
	inline void set_U3Cexclude_tags_completelyU3Ek__BackingField_0(StringU5BU5D_t1642385972* value)
	{
		___U3Cexclude_tags_completelyU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cexclude_tags_completelyU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3Cexclude_tags_keep_contentU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(HtmlSettings_t99959826, ___U3Cexclude_tags_keep_contentU3Ek__BackingField_1)); }
	inline StringU5BU5D_t1642385972* get_U3Cexclude_tags_keep_contentU3Ek__BackingField_1() const { return ___U3Cexclude_tags_keep_contentU3Ek__BackingField_1; }
	inline StringU5BU5D_t1642385972** get_address_of_U3Cexclude_tags_keep_contentU3Ek__BackingField_1() { return &___U3Cexclude_tags_keep_contentU3Ek__BackingField_1; }
	inline void set_U3Cexclude_tags_keep_contentU3Ek__BackingField_1(StringU5BU5D_t1642385972* value)
	{
		___U3Cexclude_tags_keep_contentU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cexclude_tags_keep_contentU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3Ckeep_contentU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(HtmlSettings_t99959826, ___U3Ckeep_contentU3Ek__BackingField_2)); }
	inline XPathPatterns_t1498040530 * get_U3Ckeep_contentU3Ek__BackingField_2() const { return ___U3Ckeep_contentU3Ek__BackingField_2; }
	inline XPathPatterns_t1498040530 ** get_address_of_U3Ckeep_contentU3Ek__BackingField_2() { return &___U3Ckeep_contentU3Ek__BackingField_2; }
	inline void set_U3Ckeep_contentU3Ek__BackingField_2(XPathPatterns_t1498040530 * value)
	{
		___U3Ckeep_contentU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3Ckeep_contentU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3Cexclude_contentU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(HtmlSettings_t99959826, ___U3Cexclude_contentU3Ek__BackingField_3)); }
	inline XPathPatterns_t1498040530 * get_U3Cexclude_contentU3Ek__BackingField_3() const { return ___U3Cexclude_contentU3Ek__BackingField_3; }
	inline XPathPatterns_t1498040530 ** get_address_of_U3Cexclude_contentU3Ek__BackingField_3() { return &___U3Cexclude_contentU3Ek__BackingField_3; }
	inline void set_U3Cexclude_contentU3Ek__BackingField_3(XPathPatterns_t1498040530 * value)
	{
		___U3Cexclude_contentU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cexclude_contentU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3Ckeep_tag_attributesU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(HtmlSettings_t99959826, ___U3Ckeep_tag_attributesU3Ek__BackingField_4)); }
	inline StringU5BU5D_t1642385972* get_U3Ckeep_tag_attributesU3Ek__BackingField_4() const { return ___U3Ckeep_tag_attributesU3Ek__BackingField_4; }
	inline StringU5BU5D_t1642385972** get_address_of_U3Ckeep_tag_attributesU3Ek__BackingField_4() { return &___U3Ckeep_tag_attributesU3Ek__BackingField_4; }
	inline void set_U3Ckeep_tag_attributesU3Ek__BackingField_4(StringU5BU5D_t1642385972* value)
	{
		___U3Ckeep_tag_attributesU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3Ckeep_tag_attributesU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3Cexclude_tag_attributesU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(HtmlSettings_t99959826, ___U3Cexclude_tag_attributesU3Ek__BackingField_5)); }
	inline StringU5BU5D_t1642385972* get_U3Cexclude_tag_attributesU3Ek__BackingField_5() const { return ___U3Cexclude_tag_attributesU3Ek__BackingField_5; }
	inline StringU5BU5D_t1642385972** get_address_of_U3Cexclude_tag_attributesU3Ek__BackingField_5() { return &___U3Cexclude_tag_attributesU3Ek__BackingField_5; }
	inline void set_U3Cexclude_tag_attributesU3Ek__BackingField_5(StringU5BU5D_t1642385972* value)
	{
		___U3Cexclude_tag_attributesU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cexclude_tag_attributesU3Ek__BackingField_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTMLSETTINGS_T99959826_H
#ifndef ENRICHMENT_T533139721_H
#define ENRICHMENT_T533139721_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Enrichment
struct  Enrichment_t533139721  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Enrichment::<description>k__BackingField
	String_t* ___U3CdescriptionU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Enrichment::<destination_field>k__BackingField
	String_t* ___U3Cdestination_fieldU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Enrichment::<source_field>k__BackingField
	String_t* ___U3Csource_fieldU3Ek__BackingField_2;
	// System.Boolean IBM.Watson.DeveloperCloud.Services.Discovery.v1.Enrichment::<overwrite>k__BackingField
	bool ___U3CoverwriteU3Ek__BackingField_3;
	// System.String IBM.Watson.DeveloperCloud.Services.Discovery.v1.Enrichment::<enrichment>k__BackingField
	String_t* ___U3CenrichmentU3Ek__BackingField_4;
	// System.Boolean IBM.Watson.DeveloperCloud.Services.Discovery.v1.Enrichment::<ignore_downstream_errors>k__BackingField
	bool ___U3Cignore_downstream_errorsU3Ek__BackingField_5;
	// IBM.Watson.DeveloperCloud.Services.Discovery.v1.EnrichmentOptions IBM.Watson.DeveloperCloud.Services.Discovery.v1.Enrichment::<options>k__BackingField
	EnrichmentOptions_t3455576757 * ___U3CoptionsU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CdescriptionU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Enrichment_t533139721, ___U3CdescriptionU3Ek__BackingField_0)); }
	inline String_t* get_U3CdescriptionU3Ek__BackingField_0() const { return ___U3CdescriptionU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CdescriptionU3Ek__BackingField_0() { return &___U3CdescriptionU3Ek__BackingField_0; }
	inline void set_U3CdescriptionU3Ek__BackingField_0(String_t* value)
	{
		___U3CdescriptionU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdescriptionU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3Cdestination_fieldU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Enrichment_t533139721, ___U3Cdestination_fieldU3Ek__BackingField_1)); }
	inline String_t* get_U3Cdestination_fieldU3Ek__BackingField_1() const { return ___U3Cdestination_fieldU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3Cdestination_fieldU3Ek__BackingField_1() { return &___U3Cdestination_fieldU3Ek__BackingField_1; }
	inline void set_U3Cdestination_fieldU3Ek__BackingField_1(String_t* value)
	{
		___U3Cdestination_fieldU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cdestination_fieldU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3Csource_fieldU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Enrichment_t533139721, ___U3Csource_fieldU3Ek__BackingField_2)); }
	inline String_t* get_U3Csource_fieldU3Ek__BackingField_2() const { return ___U3Csource_fieldU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3Csource_fieldU3Ek__BackingField_2() { return &___U3Csource_fieldU3Ek__BackingField_2; }
	inline void set_U3Csource_fieldU3Ek__BackingField_2(String_t* value)
	{
		___U3Csource_fieldU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3Csource_fieldU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CoverwriteU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Enrichment_t533139721, ___U3CoverwriteU3Ek__BackingField_3)); }
	inline bool get_U3CoverwriteU3Ek__BackingField_3() const { return ___U3CoverwriteU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CoverwriteU3Ek__BackingField_3() { return &___U3CoverwriteU3Ek__BackingField_3; }
	inline void set_U3CoverwriteU3Ek__BackingField_3(bool value)
	{
		___U3CoverwriteU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CenrichmentU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Enrichment_t533139721, ___U3CenrichmentU3Ek__BackingField_4)); }
	inline String_t* get_U3CenrichmentU3Ek__BackingField_4() const { return ___U3CenrichmentU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CenrichmentU3Ek__BackingField_4() { return &___U3CenrichmentU3Ek__BackingField_4; }
	inline void set_U3CenrichmentU3Ek__BackingField_4(String_t* value)
	{
		___U3CenrichmentU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CenrichmentU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3Cignore_downstream_errorsU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Enrichment_t533139721, ___U3Cignore_downstream_errorsU3Ek__BackingField_5)); }
	inline bool get_U3Cignore_downstream_errorsU3Ek__BackingField_5() const { return ___U3Cignore_downstream_errorsU3Ek__BackingField_5; }
	inline bool* get_address_of_U3Cignore_downstream_errorsU3Ek__BackingField_5() { return &___U3Cignore_downstream_errorsU3Ek__BackingField_5; }
	inline void set_U3Cignore_downstream_errorsU3Ek__BackingField_5(bool value)
	{
		___U3Cignore_downstream_errorsU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CoptionsU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Enrichment_t533139721, ___U3CoptionsU3Ek__BackingField_6)); }
	inline EnrichmentOptions_t3455576757 * get_U3CoptionsU3Ek__BackingField_6() const { return ___U3CoptionsU3Ek__BackingField_6; }
	inline EnrichmentOptions_t3455576757 ** get_address_of_U3CoptionsU3Ek__BackingField_6() { return &___U3CoptionsU3Ek__BackingField_6; }
	inline void set_U3CoptionsU3Ek__BackingField_6(EnrichmentOptions_t3455576757 * value)
	{
		___U3CoptionsU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CoptionsU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENRICHMENT_T533139721_H
#ifndef INTENT_T1324785294_H
#define INTENT_T1324785294_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Conversation.v1.Intent
struct  Intent_t1324785294  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.Conversation.v1.Intent::<intent>k__BackingField
	String_t* ___U3CintentU3Ek__BackingField_0;
	// System.Single IBM.Watson.DeveloperCloud.Services.Conversation.v1.Intent::<confidence>k__BackingField
	float ___U3CconfidenceU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CintentU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Intent_t1324785294, ___U3CintentU3Ek__BackingField_0)); }
	inline String_t* get_U3CintentU3Ek__BackingField_0() const { return ___U3CintentU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CintentU3Ek__BackingField_0() { return &___U3CintentU3Ek__BackingField_0; }
	inline void set_U3CintentU3Ek__BackingField_0(String_t* value)
	{
		___U3CintentU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CintentU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CconfidenceU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Intent_t1324785294, ___U3CconfidenceU3Ek__BackingField_1)); }
	inline float get_U3CconfidenceU3Ek__BackingField_1() const { return ___U3CconfidenceU3Ek__BackingField_1; }
	inline float* get_address_of_U3CconfidenceU3Ek__BackingField_1() { return &___U3CconfidenceU3Ek__BackingField_1; }
	inline void set_U3CconfidenceU3Ek__BackingField_1(float value)
	{
		___U3CconfidenceU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTENT_T1324785294_H
#ifndef OUTPUTDATA_T497470723_H
#define OUTPUTDATA_T497470723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Conversation.v1.OutputData
struct  OutputData_t497470723  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.Conversation.v1.LogMessageResponse[] IBM.Watson.DeveloperCloud.Services.Conversation.v1.OutputData::<log_messages>k__BackingField
	LogMessageResponseU5BU5D_t1330322083* ___U3Clog_messagesU3Ek__BackingField_0;
	// System.String[] IBM.Watson.DeveloperCloud.Services.Conversation.v1.OutputData::<text>k__BackingField
	StringU5BU5D_t1642385972* ___U3CtextU3Ek__BackingField_1;
	// System.String[] IBM.Watson.DeveloperCloud.Services.Conversation.v1.OutputData::<nodes_visited>k__BackingField
	StringU5BU5D_t1642385972* ___U3Cnodes_visitedU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3Clog_messagesU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(OutputData_t497470723, ___U3Clog_messagesU3Ek__BackingField_0)); }
	inline LogMessageResponseU5BU5D_t1330322083* get_U3Clog_messagesU3Ek__BackingField_0() const { return ___U3Clog_messagesU3Ek__BackingField_0; }
	inline LogMessageResponseU5BU5D_t1330322083** get_address_of_U3Clog_messagesU3Ek__BackingField_0() { return &___U3Clog_messagesU3Ek__BackingField_0; }
	inline void set_U3Clog_messagesU3Ek__BackingField_0(LogMessageResponseU5BU5D_t1330322083* value)
	{
		___U3Clog_messagesU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Clog_messagesU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CtextU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(OutputData_t497470723, ___U3CtextU3Ek__BackingField_1)); }
	inline StringU5BU5D_t1642385972* get_U3CtextU3Ek__BackingField_1() const { return ___U3CtextU3Ek__BackingField_1; }
	inline StringU5BU5D_t1642385972** get_address_of_U3CtextU3Ek__BackingField_1() { return &___U3CtextU3Ek__BackingField_1; }
	inline void set_U3CtextU3Ek__BackingField_1(StringU5BU5D_t1642385972* value)
	{
		___U3CtextU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3Cnodes_visitedU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(OutputData_t497470723, ___U3Cnodes_visitedU3Ek__BackingField_2)); }
	inline StringU5BU5D_t1642385972* get_U3Cnodes_visitedU3Ek__BackingField_2() const { return ___U3Cnodes_visitedU3Ek__BackingField_2; }
	inline StringU5BU5D_t1642385972** get_address_of_U3Cnodes_visitedU3Ek__BackingField_2() { return &___U3Cnodes_visitedU3Ek__BackingField_2; }
	inline void set_U3Cnodes_visitedU3Ek__BackingField_2(StringU5BU5D_t1642385972* value)
	{
		___U3Cnodes_visitedU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cnodes_visitedU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OUTPUTDATA_T497470723_H
#ifndef LOGMESSAGERESPONSE_T3273544838_H
#define LOGMESSAGERESPONSE_T3273544838_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Conversation.v1.LogMessageResponse
struct  LogMessageResponse_t3273544838  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.Conversation.v1.LogMessageResponse::<level>k__BackingField
	String_t* ___U3ClevelU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.Conversation.v1.LogMessageResponse::<msg>k__BackingField
	String_t* ___U3CmsgU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3ClevelU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(LogMessageResponse_t3273544838, ___U3ClevelU3Ek__BackingField_0)); }
	inline String_t* get_U3ClevelU3Ek__BackingField_0() const { return ___U3ClevelU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3ClevelU3Ek__BackingField_0() { return &___U3ClevelU3Ek__BackingField_0; }
	inline void set_U3ClevelU3Ek__BackingField_0(String_t* value)
	{
		___U3ClevelU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClevelU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CmsgU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(LogMessageResponse_t3273544838, ___U3CmsgU3Ek__BackingField_1)); }
	inline String_t* get_U3CmsgU3Ek__BackingField_1() const { return ___U3CmsgU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CmsgU3Ek__BackingField_1() { return &___U3CmsgU3Ek__BackingField_1; }
	inline void set_U3CmsgU3Ek__BackingField_1(String_t* value)
	{
		___U3CmsgU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmsgU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGMESSAGERESPONSE_T3273544838_H
#ifndef CHECKSERVICESTATUS_T780401186_H
#define CHECKSERVICESTATUS_T780401186_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Conversation.v1.Conversation/CheckServiceStatus
struct  CheckServiceStatus_t780401186  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.Conversation.v1.Conversation IBM.Watson.DeveloperCloud.Services.Conversation.v1.Conversation/CheckServiceStatus::m_Service
	Conversation_t105466997 * ___m_Service_0;
	// IBM.Watson.DeveloperCloud.Services.ServiceStatus IBM.Watson.DeveloperCloud.Services.Conversation.v1.Conversation/CheckServiceStatus::m_Callback
	ServiceStatus_t1443707987 * ___m_Callback_1;
	// System.Int32 IBM.Watson.DeveloperCloud.Services.Conversation.v1.Conversation/CheckServiceStatus::m_ConversationCount
	int32_t ___m_ConversationCount_2;

public:
	inline static int32_t get_offset_of_m_Service_0() { return static_cast<int32_t>(offsetof(CheckServiceStatus_t780401186, ___m_Service_0)); }
	inline Conversation_t105466997 * get_m_Service_0() const { return ___m_Service_0; }
	inline Conversation_t105466997 ** get_address_of_m_Service_0() { return &___m_Service_0; }
	inline void set_m_Service_0(Conversation_t105466997 * value)
	{
		___m_Service_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Service_0), value);
	}

	inline static int32_t get_offset_of_m_Callback_1() { return static_cast<int32_t>(offsetof(CheckServiceStatus_t780401186, ___m_Callback_1)); }
	inline ServiceStatus_t1443707987 * get_m_Callback_1() const { return ___m_Callback_1; }
	inline ServiceStatus_t1443707987 ** get_address_of_m_Callback_1() { return &___m_Callback_1; }
	inline void set_m_Callback_1(ServiceStatus_t1443707987 * value)
	{
		___m_Callback_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Callback_1), value);
	}

	inline static int32_t get_offset_of_m_ConversationCount_2() { return static_cast<int32_t>(offsetof(CheckServiceStatus_t780401186, ___m_ConversationCount_2)); }
	inline int32_t get_m_ConversationCount_2() const { return ___m_ConversationCount_2; }
	inline int32_t* get_address_of_m_ConversationCount_2() { return &___m_ConversationCount_2; }
	inline void set_m_ConversationCount_2(int32_t value)
	{
		___m_ConversationCount_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHECKSERVICESTATUS_T780401186_H
#ifndef MESSAGERESPONSE_T470867978_H
#define MESSAGERESPONSE_T470867978_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Conversation.v1.MessageResponse
struct  MessageResponse_t470867978  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.Conversation.v1.InputData IBM.Watson.DeveloperCloud.Services.Conversation.v1.MessageResponse::<input>k__BackingField
	InputData_t902315192 * ___U3CinputU3Ek__BackingField_0;
	// IBM.Watson.DeveloperCloud.Services.Conversation.v1.Context IBM.Watson.DeveloperCloud.Services.Conversation.v1.MessageResponse::<context>k__BackingField
	Context_t2592795451 * ___U3CcontextU3Ek__BackingField_1;
	// IBM.Watson.DeveloperCloud.Services.Conversation.v1.EntityResponse[] IBM.Watson.DeveloperCloud.Services.Conversation.v1.MessageResponse::<entities>k__BackingField
	EntityResponseU5BU5D_t480157455* ___U3CentitiesU3Ek__BackingField_2;
	// IBM.Watson.DeveloperCloud.Services.Conversation.v1.Intent[] IBM.Watson.DeveloperCloud.Services.Conversation.v1.MessageResponse::<intents>k__BackingField
	IntentU5BU5D_t4227563387* ___U3CintentsU3Ek__BackingField_3;
	// IBM.Watson.DeveloperCloud.Services.Conversation.v1.OutputData IBM.Watson.DeveloperCloud.Services.Conversation.v1.MessageResponse::<output>k__BackingField
	OutputData_t497470723 * ___U3CoutputU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CinputU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MessageResponse_t470867978, ___U3CinputU3Ek__BackingField_0)); }
	inline InputData_t902315192 * get_U3CinputU3Ek__BackingField_0() const { return ___U3CinputU3Ek__BackingField_0; }
	inline InputData_t902315192 ** get_address_of_U3CinputU3Ek__BackingField_0() { return &___U3CinputU3Ek__BackingField_0; }
	inline void set_U3CinputU3Ek__BackingField_0(InputData_t902315192 * value)
	{
		___U3CinputU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CinputU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CcontextU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(MessageResponse_t470867978, ___U3CcontextU3Ek__BackingField_1)); }
	inline Context_t2592795451 * get_U3CcontextU3Ek__BackingField_1() const { return ___U3CcontextU3Ek__BackingField_1; }
	inline Context_t2592795451 ** get_address_of_U3CcontextU3Ek__BackingField_1() { return &___U3CcontextU3Ek__BackingField_1; }
	inline void set_U3CcontextU3Ek__BackingField_1(Context_t2592795451 * value)
	{
		___U3CcontextU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcontextU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CentitiesU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(MessageResponse_t470867978, ___U3CentitiesU3Ek__BackingField_2)); }
	inline EntityResponseU5BU5D_t480157455* get_U3CentitiesU3Ek__BackingField_2() const { return ___U3CentitiesU3Ek__BackingField_2; }
	inline EntityResponseU5BU5D_t480157455** get_address_of_U3CentitiesU3Ek__BackingField_2() { return &___U3CentitiesU3Ek__BackingField_2; }
	inline void set_U3CentitiesU3Ek__BackingField_2(EntityResponseU5BU5D_t480157455* value)
	{
		___U3CentitiesU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CentitiesU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CintentsU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(MessageResponse_t470867978, ___U3CintentsU3Ek__BackingField_3)); }
	inline IntentU5BU5D_t4227563387* get_U3CintentsU3Ek__BackingField_3() const { return ___U3CintentsU3Ek__BackingField_3; }
	inline IntentU5BU5D_t4227563387** get_address_of_U3CintentsU3Ek__BackingField_3() { return &___U3CintentsU3Ek__BackingField_3; }
	inline void set_U3CintentsU3Ek__BackingField_3(IntentU5BU5D_t4227563387* value)
	{
		___U3CintentsU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CintentsU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CoutputU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(MessageResponse_t470867978, ___U3CoutputU3Ek__BackingField_4)); }
	inline OutputData_t497470723 * get_U3CoutputU3Ek__BackingField_4() const { return ___U3CoutputU3Ek__BackingField_4; }
	inline OutputData_t497470723 ** get_address_of_U3CoutputU3Ek__BackingField_4() { return &___U3CoutputU3Ek__BackingField_4; }
	inline void set_U3CoutputU3Ek__BackingField_4(OutputData_t497470723 * value)
	{
		___U3CoutputU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CoutputU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGERESPONSE_T470867978_H
#ifndef ENTITYRESPONSE_T2137526154_H
#define ENTITYRESPONSE_T2137526154_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Conversation.v1.EntityResponse
struct  EntityResponse_t2137526154  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.Conversation.v1.EntityResponse::<entity>k__BackingField
	String_t* ___U3CentityU3Ek__BackingField_0;
	// System.Int32[] IBM.Watson.DeveloperCloud.Services.Conversation.v1.EntityResponse::<location>k__BackingField
	Int32U5BU5D_t3030399641* ___U3ClocationU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.Conversation.v1.EntityResponse::<value>k__BackingField
	String_t* ___U3CvalueU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CentityU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(EntityResponse_t2137526154, ___U3CentityU3Ek__BackingField_0)); }
	inline String_t* get_U3CentityU3Ek__BackingField_0() const { return ___U3CentityU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CentityU3Ek__BackingField_0() { return &___U3CentityU3Ek__BackingField_0; }
	inline void set_U3CentityU3Ek__BackingField_0(String_t* value)
	{
		___U3CentityU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CentityU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3ClocationU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(EntityResponse_t2137526154, ___U3ClocationU3Ek__BackingField_1)); }
	inline Int32U5BU5D_t3030399641* get_U3ClocationU3Ek__BackingField_1() const { return ___U3ClocationU3Ek__BackingField_1; }
	inline Int32U5BU5D_t3030399641** get_address_of_U3ClocationU3Ek__BackingField_1() { return &___U3ClocationU3Ek__BackingField_1; }
	inline void set_U3ClocationU3Ek__BackingField_1(Int32U5BU5D_t3030399641* value)
	{
		___U3ClocationU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClocationU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CvalueU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(EntityResponse_t2137526154, ___U3CvalueU3Ek__BackingField_2)); }
	inline String_t* get_U3CvalueU3Ek__BackingField_2() const { return ___U3CvalueU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CvalueU3Ek__BackingField_2() { return &___U3CvalueU3Ek__BackingField_2; }
	inline void set_U3CvalueU3Ek__BackingField_2(String_t* value)
	{
		___U3CvalueU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CvalueU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTITYRESPONSE_T2137526154_H
#ifndef SYSTEMRESPONSE_T541191420_H
#define SYSTEMRESPONSE_T541191420_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Conversation.v1.SystemResponse
struct  SystemResponse_t541191420  : public RuntimeObject
{
public:
	// System.String[] IBM.Watson.DeveloperCloud.Services.Conversation.v1.SystemResponse::<dialog_stack>k__BackingField
	StringU5BU5D_t1642385972* ___U3Cdialog_stackU3Ek__BackingField_0;
	// System.Int32 IBM.Watson.DeveloperCloud.Services.Conversation.v1.SystemResponse::<dialog_turn_counter>k__BackingField
	int32_t ___U3Cdialog_turn_counterU3Ek__BackingField_1;
	// System.Int32 IBM.Watson.DeveloperCloud.Services.Conversation.v1.SystemResponse::<dialog_request_counter>k__BackingField
	int32_t ___U3Cdialog_request_counterU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3Cdialog_stackU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SystemResponse_t541191420, ___U3Cdialog_stackU3Ek__BackingField_0)); }
	inline StringU5BU5D_t1642385972* get_U3Cdialog_stackU3Ek__BackingField_0() const { return ___U3Cdialog_stackU3Ek__BackingField_0; }
	inline StringU5BU5D_t1642385972** get_address_of_U3Cdialog_stackU3Ek__BackingField_0() { return &___U3Cdialog_stackU3Ek__BackingField_0; }
	inline void set_U3Cdialog_stackU3Ek__BackingField_0(StringU5BU5D_t1642385972* value)
	{
		___U3Cdialog_stackU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cdialog_stackU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3Cdialog_turn_counterU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(SystemResponse_t541191420, ___U3Cdialog_turn_counterU3Ek__BackingField_1)); }
	inline int32_t get_U3Cdialog_turn_counterU3Ek__BackingField_1() const { return ___U3Cdialog_turn_counterU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3Cdialog_turn_counterU3Ek__BackingField_1() { return &___U3Cdialog_turn_counterU3Ek__BackingField_1; }
	inline void set_U3Cdialog_turn_counterU3Ek__BackingField_1(int32_t value)
	{
		___U3Cdialog_turn_counterU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3Cdialog_request_counterU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SystemResponse_t541191420, ___U3Cdialog_request_counterU3Ek__BackingField_2)); }
	inline int32_t get_U3Cdialog_request_counterU3Ek__BackingField_2() const { return ___U3Cdialog_request_counterU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3Cdialog_request_counterU3Ek__BackingField_2() { return &___U3Cdialog_request_counterU3Ek__BackingField_2; }
	inline void set_U3Cdialog_request_counterU3Ek__BackingField_2(int32_t value)
	{
		___U3Cdialog_request_counterU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMRESPONSE_T541191420_H
#ifndef VERSION_T1829570756_H
#define VERSION_T1829570756_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Conversation.v1.Version
struct  Version_t1829570756  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERSION_T1829570756_H
#ifndef CONVERSATIONEXPERIMENTAL_T215658501_H
#define CONVERSATIONEXPERIMENTAL_T215658501_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.ConversationExperimental
struct  ConversationExperimental_t215658501  : public RuntimeObject
{
public:

public:
};

struct ConversationExperimental_t215658501_StaticFields
{
public:
	// FullSerializer.fsSerializer IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.ConversationExperimental::sm_Serializer
	fsSerializer_t4193731081 * ___sm_Serializer_1;

public:
	inline static int32_t get_offset_of_sm_Serializer_1() { return static_cast<int32_t>(offsetof(ConversationExperimental_t215658501_StaticFields, ___sm_Serializer_1)); }
	inline fsSerializer_t4193731081 * get_sm_Serializer_1() const { return ___sm_Serializer_1; }
	inline fsSerializer_t4193731081 ** get_address_of_sm_Serializer_1() { return &___sm_Serializer_1; }
	inline void set_sm_Serializer_1(fsSerializer_t4193731081 * value)
	{
		___sm_Serializer_1 = value;
		Il2CppCodeGenWriteBarrier((&___sm_Serializer_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONVERSATIONEXPERIMENTAL_T215658501_H
#ifndef MESSAGEREQUEST_T1934991468_H
#define MESSAGEREQUEST_T1934991468_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Conversation.v1.MessageRequest
struct  MessageRequest_t1934991468  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.Conversation.v1.InputData IBM.Watson.DeveloperCloud.Services.Conversation.v1.MessageRequest::<input>k__BackingField
	InputData_t902315192 * ___U3CinputU3Ek__BackingField_0;
	// System.Boolean IBM.Watson.DeveloperCloud.Services.Conversation.v1.MessageRequest::<alternate_intents>k__BackingField
	bool ___U3Calternate_intentsU3Ek__BackingField_1;
	// IBM.Watson.DeveloperCloud.Services.Conversation.v1.Context IBM.Watson.DeveloperCloud.Services.Conversation.v1.MessageRequest::<context>k__BackingField
	Context_t2592795451 * ___U3CcontextU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CinputU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MessageRequest_t1934991468, ___U3CinputU3Ek__BackingField_0)); }
	inline InputData_t902315192 * get_U3CinputU3Ek__BackingField_0() const { return ___U3CinputU3Ek__BackingField_0; }
	inline InputData_t902315192 ** get_address_of_U3CinputU3Ek__BackingField_0() { return &___U3CinputU3Ek__BackingField_0; }
	inline void set_U3CinputU3Ek__BackingField_0(InputData_t902315192 * value)
	{
		___U3CinputU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CinputU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3Calternate_intentsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(MessageRequest_t1934991468, ___U3Calternate_intentsU3Ek__BackingField_1)); }
	inline bool get_U3Calternate_intentsU3Ek__BackingField_1() const { return ___U3Calternate_intentsU3Ek__BackingField_1; }
	inline bool* get_address_of_U3Calternate_intentsU3Ek__BackingField_1() { return &___U3Calternate_intentsU3Ek__BackingField_1; }
	inline void set_U3Calternate_intentsU3Ek__BackingField_1(bool value)
	{
		___U3Calternate_intentsU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CcontextU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(MessageRequest_t1934991468, ___U3CcontextU3Ek__BackingField_2)); }
	inline Context_t2592795451 * get_U3CcontextU3Ek__BackingField_2() const { return ___U3CcontextU3Ek__BackingField_2; }
	inline Context_t2592795451 ** get_address_of_U3CcontextU3Ek__BackingField_2() { return &___U3CcontextU3Ek__BackingField_2; }
	inline void set_U3CcontextU3Ek__BackingField_2(Context_t2592795451 * value)
	{
		___U3CcontextU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcontextU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGEREQUEST_T1934991468_H
#ifndef INPUTDATA_T902315192_H
#define INPUTDATA_T902315192_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Conversation.v1.InputData
struct  InputData_t902315192  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.Conversation.v1.InputData::<text>k__BackingField
	String_t* ___U3CtextU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CtextU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(InputData_t902315192, ___U3CtextU3Ek__BackingField_0)); }
	inline String_t* get_U3CtextU3Ek__BackingField_0() const { return ___U3CtextU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CtextU3Ek__BackingField_0() { return &___U3CtextU3Ek__BackingField_0; }
	inline void set_U3CtextU3Ek__BackingField_0(String_t* value)
	{
		___U3CtextU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTDATA_T902315192_H
#ifndef CONTEXT_T2592795451_H
#define CONTEXT_T2592795451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Conversation.v1.Context
struct  Context_t2592795451  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.Conversation.v1.Context::<conversation_id>k__BackingField
	String_t* ___U3Cconversation_idU3Ek__BackingField_0;
	// IBM.Watson.DeveloperCloud.Services.Conversation.v1.SystemResponse IBM.Watson.DeveloperCloud.Services.Conversation.v1.Context::<system>k__BackingField
	SystemResponse_t541191420 * ___U3CsystemU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3Cconversation_idU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Context_t2592795451, ___U3Cconversation_idU3Ek__BackingField_0)); }
	inline String_t* get_U3Cconversation_idU3Ek__BackingField_0() const { return ___U3Cconversation_idU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3Cconversation_idU3Ek__BackingField_0() { return &___U3Cconversation_idU3Ek__BackingField_0; }
	inline void set_U3Cconversation_idU3Ek__BackingField_0(String_t* value)
	{
		___U3Cconversation_idU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cconversation_idU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CsystemU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Context_t2592795451, ___U3CsystemU3Ek__BackingField_1)); }
	inline SystemResponse_t541191420 * get_U3CsystemU3Ek__BackingField_1() const { return ___U3CsystemU3Ek__BackingField_1; }
	inline SystemResponse_t541191420 ** get_address_of_U3CsystemU3Ek__BackingField_1() { return &___U3CsystemU3Ek__BackingField_1; }
	inline void set_U3CsystemU3Ek__BackingField_1(SystemResponse_t541191420 * value)
	{
		___U3CsystemU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsystemU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTEXT_T2592795451_H
#ifndef RESULT_T1515462031_H
#define RESULT_T1515462031_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Result
struct  Result_t1515462031  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Result::<next>k__BackingField
	String_t* ___U3CnextU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Result::<status>k__BackingField
	String_t* ___U3CstatusU3Ek__BackingField_1;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Docs[] IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Result::<docs>k__BackingField
	DocsU5BU5D_t3724963220* ___U3CdocsU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CnextU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Result_t1515462031, ___U3CnextU3Ek__BackingField_0)); }
	inline String_t* get_U3CnextU3Ek__BackingField_0() const { return ___U3CnextU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CnextU3Ek__BackingField_0() { return &___U3CnextU3Ek__BackingField_0; }
	inline void set_U3CnextU3Ek__BackingField_0(String_t* value)
	{
		___U3CnextU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnextU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CstatusU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Result_t1515462031, ___U3CstatusU3Ek__BackingField_1)); }
	inline String_t* get_U3CstatusU3Ek__BackingField_1() const { return ___U3CstatusU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CstatusU3Ek__BackingField_1() { return &___U3CstatusU3Ek__BackingField_1; }
	inline void set_U3CstatusU3Ek__BackingField_1(String_t* value)
	{
		___U3CstatusU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstatusU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CdocsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Result_t1515462031, ___U3CdocsU3Ek__BackingField_2)); }
	inline DocsU5BU5D_t3724963220* get_U3CdocsU3Ek__BackingField_2() const { return ___U3CdocsU3Ek__BackingField_2; }
	inline DocsU5BU5D_t3724963220** get_address_of_U3CdocsU3Ek__BackingField_2() { return &___U3CdocsU3Ek__BackingField_2; }
	inline void set_U3CdocsU3Ek__BackingField_2(DocsU5BU5D_t3724963220* value)
	{
		___U3CdocsU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdocsU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESULT_T1515462031_H
#ifndef DOCS_T1723806121_H
#define DOCS_T1723806121_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Docs
struct  Docs_t1723806121  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Docs::<id>k__BackingField
	String_t* ___U3CidU3Ek__BackingField_0;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Source IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Docs::<source>k__BackingField
	Source_t916780923 * ___U3CsourceU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Docs::<timestamp>k__BackingField
	String_t* ___U3CtimestampU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CidU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Docs_t1723806121, ___U3CidU3Ek__BackingField_0)); }
	inline String_t* get_U3CidU3Ek__BackingField_0() const { return ___U3CidU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CidU3Ek__BackingField_0() { return &___U3CidU3Ek__BackingField_0; }
	inline void set_U3CidU3Ek__BackingField_0(String_t* value)
	{
		___U3CidU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CidU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CsourceU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Docs_t1723806121, ___U3CsourceU3Ek__BackingField_1)); }
	inline Source_t916780923 * get_U3CsourceU3Ek__BackingField_1() const { return ___U3CsourceU3Ek__BackingField_1; }
	inline Source_t916780923 ** get_address_of_U3CsourceU3Ek__BackingField_1() { return &___U3CsourceU3Ek__BackingField_1; }
	inline void set_U3CsourceU3Ek__BackingField_1(Source_t916780923 * value)
	{
		___U3CsourceU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsourceU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CtimestampU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Docs_t1723806121, ___U3CtimestampU3Ek__BackingField_2)); }
	inline String_t* get_U3CtimestampU3Ek__BackingField_2() const { return ___U3CtimestampU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CtimestampU3Ek__BackingField_2() { return &___U3CtimestampU3Ek__BackingField_2; }
	inline void set_U3CtimestampU3Ek__BackingField_2(String_t* value)
	{
		___U3CtimestampU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtimestampU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOCS_T1723806121_H
#ifndef SOURCE_T916780923_H
#define SOURCE_T916780923_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Source
struct  Source_t916780923  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Original IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Source::<original>k__BackingField
	Original_t3571140033 * ___U3CoriginalU3Ek__BackingField_0;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Enriched IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Source::<enriched>k__BackingField
	Enriched_t407156716 * ___U3CenrichedU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CoriginalU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Source_t916780923, ___U3CoriginalU3Ek__BackingField_0)); }
	inline Original_t3571140033 * get_U3CoriginalU3Ek__BackingField_0() const { return ___U3CoriginalU3Ek__BackingField_0; }
	inline Original_t3571140033 ** get_address_of_U3CoriginalU3Ek__BackingField_0() { return &___U3CoriginalU3Ek__BackingField_0; }
	inline void set_U3CoriginalU3Ek__BackingField_0(Original_t3571140033 * value)
	{
		___U3CoriginalU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CoriginalU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CenrichedU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Source_t916780923, ___U3CenrichedU3Ek__BackingField_1)); }
	inline Enriched_t407156716 * get_U3CenrichedU3Ek__BackingField_1() const { return ___U3CenrichedU3Ek__BackingField_1; }
	inline Enriched_t407156716 ** get_address_of_U3CenrichedU3Ek__BackingField_1() { return &___U3CenrichedU3Ek__BackingField_1; }
	inline void set_U3CenrichedU3Ek__BackingField_1(Enriched_t407156716 * value)
	{
		___U3CenrichedU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CenrichedU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOURCE_T916780923_H
#ifndef TEMPORAL_T3690293474_H
#define TEMPORAL_T3690293474_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Temporal
struct  Temporal_t3690293474  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Temporal::<text>k__BackingField
	String_t* ___U3CtextU3Ek__BackingField_0;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Decoded IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Temporal::<decoded>k__BackingField
	Decoded_t1922409334 * ___U3CdecodedU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CtextU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Temporal_t3690293474, ___U3CtextU3Ek__BackingField_0)); }
	inline String_t* get_U3CtextU3Ek__BackingField_0() const { return ___U3CtextU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CtextU3Ek__BackingField_0() { return &___U3CtextU3Ek__BackingField_0; }
	inline void set_U3CtextU3Ek__BackingField_0(String_t* value)
	{
		___U3CtextU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CdecodedU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Temporal_t3690293474, ___U3CdecodedU3Ek__BackingField_1)); }
	inline Decoded_t1922409334 * get_U3CdecodedU3Ek__BackingField_1() const { return ___U3CdecodedU3Ek__BackingField_1; }
	inline Decoded_t1922409334 ** get_address_of_U3CdecodedU3Ek__BackingField_1() { return &___U3CdecodedU3Ek__BackingField_1; }
	inline void set_U3CdecodedU3Ek__BackingField_1(Decoded_t1922409334 * value)
	{
		___U3CdecodedU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdecodedU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEMPORAL_T3690293474_H
#ifndef DECODED_T1922409334_H
#define DECODED_T1922409334_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Decoded
struct  Decoded_t1922409334  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Decoded::<type>k__BackingField
	String_t* ___U3CtypeU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Decoded::<value>k__BackingField
	String_t* ___U3CvalueU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Decoded::<start>k__BackingField
	String_t* ___U3CstartU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Decoded::<end>k__BackingField
	String_t* ___U3CendU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CtypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Decoded_t1922409334, ___U3CtypeU3Ek__BackingField_0)); }
	inline String_t* get_U3CtypeU3Ek__BackingField_0() const { return ___U3CtypeU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CtypeU3Ek__BackingField_0() { return &___U3CtypeU3Ek__BackingField_0; }
	inline void set_U3CtypeU3Ek__BackingField_0(String_t* value)
	{
		___U3CtypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtypeU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CvalueU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Decoded_t1922409334, ___U3CvalueU3Ek__BackingField_1)); }
	inline String_t* get_U3CvalueU3Ek__BackingField_1() const { return ___U3CvalueU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CvalueU3Ek__BackingField_1() { return &___U3CvalueU3Ek__BackingField_1; }
	inline void set_U3CvalueU3Ek__BackingField_1(String_t* value)
	{
		___U3CvalueU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CvalueU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CstartU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Decoded_t1922409334, ___U3CstartU3Ek__BackingField_2)); }
	inline String_t* get_U3CstartU3Ek__BackingField_2() const { return ___U3CstartU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CstartU3Ek__BackingField_2() { return &___U3CstartU3Ek__BackingField_2; }
	inline void set_U3CstartU3Ek__BackingField_2(String_t* value)
	{
		___U3CstartU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstartU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CendU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Decoded_t1922409334, ___U3CendU3Ek__BackingField_3)); }
	inline String_t* get_U3CendU3Ek__BackingField_3() const { return ___U3CendU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CendU3Ek__BackingField_3() { return &___U3CendU3Ek__BackingField_3; }
	inline void set_U3CendU3Ek__BackingField_3(String_t* value)
	{
		___U3CendU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CendU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECODED_T1922409334_H
#ifndef NEWSRESPONSE_T3536772326_H
#define NEWSRESPONSE_T3536772326_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.NewsResponse
struct  NewsResponse_t3536772326  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.NewsResponse::<status>k__BackingField
	String_t* ___U3CstatusU3Ek__BackingField_0;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Result IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.NewsResponse::<result>k__BackingField
	Result_t1515462031 * ___U3CresultU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CstatusU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(NewsResponse_t3536772326, ___U3CstatusU3Ek__BackingField_0)); }
	inline String_t* get_U3CstatusU3Ek__BackingField_0() const { return ___U3CstatusU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CstatusU3Ek__BackingField_0() { return &___U3CstatusU3Ek__BackingField_0; }
	inline void set_U3CstatusU3Ek__BackingField_0(String_t* value)
	{
		___U3CstatusU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstatusU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CresultU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(NewsResponse_t3536772326, ___U3CresultU3Ek__BackingField_1)); }
	inline Result_t1515462031 * get_U3CresultU3Ek__BackingField_1() const { return ___U3CresultU3Ek__BackingField_1; }
	inline Result_t1515462031 ** get_address_of_U3CresultU3Ek__BackingField_1() { return &___U3CresultU3Ek__BackingField_1; }
	inline void set_U3CresultU3Ek__BackingField_1(Result_t1515462031 * value)
	{
		___U3CresultU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CresultU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NEWSRESPONSE_T3536772326_H
#ifndef ENRICHEDTITLE_T2959558712_H
#define ENRICHEDTITLE_T2959558712_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.EnrichedTitle
struct  EnrichedTitle_t2959558712  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Keyword[] IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.EnrichedTitle::<keywords>k__BackingField
	KeywordU5BU5D_t3893757366* ___U3CkeywordsU3Ek__BackingField_0;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Entity[] IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.EnrichedTitle::<entities>k__BackingField
	EntityU5BU5D_t3969836080* ___U3CentitiesU3Ek__BackingField_1;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Concept[] IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.EnrichedTitle::<concepts>k__BackingField
	ConceptU5BU5D_t3502654871* ___U3CconceptsU3Ek__BackingField_2;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Relation[] IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.EnrichedTitle::<relations>k__BackingField
	RelationU5BU5D_t1525062961* ___U3CrelationsU3Ek__BackingField_3;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.DocSentiment IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.EnrichedTitle::<docSentiment>k__BackingField
	DocSentiment_t2354958393 * ___U3CdocSentimentU3Ek__BackingField_4;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Taxonomy[] IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.EnrichedTitle::<taxonomy>k__BackingField
	TaxonomyU5BU5D_t3261933456* ___U3CtaxonomyU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CkeywordsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(EnrichedTitle_t2959558712, ___U3CkeywordsU3Ek__BackingField_0)); }
	inline KeywordU5BU5D_t3893757366* get_U3CkeywordsU3Ek__BackingField_0() const { return ___U3CkeywordsU3Ek__BackingField_0; }
	inline KeywordU5BU5D_t3893757366** get_address_of_U3CkeywordsU3Ek__BackingField_0() { return &___U3CkeywordsU3Ek__BackingField_0; }
	inline void set_U3CkeywordsU3Ek__BackingField_0(KeywordU5BU5D_t3893757366* value)
	{
		___U3CkeywordsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CkeywordsU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CentitiesU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(EnrichedTitle_t2959558712, ___U3CentitiesU3Ek__BackingField_1)); }
	inline EntityU5BU5D_t3969836080* get_U3CentitiesU3Ek__BackingField_1() const { return ___U3CentitiesU3Ek__BackingField_1; }
	inline EntityU5BU5D_t3969836080** get_address_of_U3CentitiesU3Ek__BackingField_1() { return &___U3CentitiesU3Ek__BackingField_1; }
	inline void set_U3CentitiesU3Ek__BackingField_1(EntityU5BU5D_t3969836080* value)
	{
		___U3CentitiesU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CentitiesU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CconceptsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(EnrichedTitle_t2959558712, ___U3CconceptsU3Ek__BackingField_2)); }
	inline ConceptU5BU5D_t3502654871* get_U3CconceptsU3Ek__BackingField_2() const { return ___U3CconceptsU3Ek__BackingField_2; }
	inline ConceptU5BU5D_t3502654871** get_address_of_U3CconceptsU3Ek__BackingField_2() { return &___U3CconceptsU3Ek__BackingField_2; }
	inline void set_U3CconceptsU3Ek__BackingField_2(ConceptU5BU5D_t3502654871* value)
	{
		___U3CconceptsU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CconceptsU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CrelationsU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(EnrichedTitle_t2959558712, ___U3CrelationsU3Ek__BackingField_3)); }
	inline RelationU5BU5D_t1525062961* get_U3CrelationsU3Ek__BackingField_3() const { return ___U3CrelationsU3Ek__BackingField_3; }
	inline RelationU5BU5D_t1525062961** get_address_of_U3CrelationsU3Ek__BackingField_3() { return &___U3CrelationsU3Ek__BackingField_3; }
	inline void set_U3CrelationsU3Ek__BackingField_3(RelationU5BU5D_t1525062961* value)
	{
		___U3CrelationsU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrelationsU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CdocSentimentU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(EnrichedTitle_t2959558712, ___U3CdocSentimentU3Ek__BackingField_4)); }
	inline DocSentiment_t2354958393 * get_U3CdocSentimentU3Ek__BackingField_4() const { return ___U3CdocSentimentU3Ek__BackingField_4; }
	inline DocSentiment_t2354958393 ** get_address_of_U3CdocSentimentU3Ek__BackingField_4() { return &___U3CdocSentimentU3Ek__BackingField_4; }
	inline void set_U3CdocSentimentU3Ek__BackingField_4(DocSentiment_t2354958393 * value)
	{
		___U3CdocSentimentU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdocSentimentU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CtaxonomyU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(EnrichedTitle_t2959558712, ___U3CtaxonomyU3Ek__BackingField_5)); }
	inline TaxonomyU5BU5D_t3261933456* get_U3CtaxonomyU3Ek__BackingField_5() const { return ___U3CtaxonomyU3Ek__BackingField_5; }
	inline TaxonomyU5BU5D_t3261933456** get_address_of_U3CtaxonomyU3Ek__BackingField_5() { return &___U3CtaxonomyU3Ek__BackingField_5; }
	inline void set_U3CtaxonomyU3Ek__BackingField_5(TaxonomyU5BU5D_t3261933456* value)
	{
		___U3CtaxonomyU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtaxonomyU3Ek__BackingField_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENRICHEDTITLE_T2959558712_H
#ifndef FIELDS_T4182106063_H
#define FIELDS_T4182106063_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Fields
struct  Fields_t4182106063  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIELDS_T4182106063_H
#ifndef CONVERSATION_T105466997_H
#define CONVERSATION_T105466997_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Conversation.v1.Conversation
struct  Conversation_t105466997  : public RuntimeObject
{
public:

public:
};

struct Conversation_t105466997_StaticFields
{
public:
	// FullSerializer.fsSerializer IBM.Watson.DeveloperCloud.Services.Conversation.v1.Conversation::sm_Serializer
	fsSerializer_t4193731081 * ___sm_Serializer_2;

public:
	inline static int32_t get_offset_of_sm_Serializer_2() { return static_cast<int32_t>(offsetof(Conversation_t105466997_StaticFields, ___sm_Serializer_2)); }
	inline fsSerializer_t4193731081 * get_sm_Serializer_2() const { return ___sm_Serializer_2; }
	inline fsSerializer_t4193731081 ** get_address_of_sm_Serializer_2() { return &___sm_Serializer_2; }
	inline void set_sm_Serializer_2(fsSerializer_t4193731081 * value)
	{
		___sm_Serializer_2 = value;
		Il2CppCodeGenWriteBarrier((&___sm_Serializer_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONVERSATION_T105466997_H
#ifndef ORIGINAL_T3571140033_H
#define ORIGINAL_T3571140033_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Original
struct  Original_t3571140033  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Original::<url>k__BackingField
	String_t* ___U3CurlU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CurlU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Original_t3571140033, ___U3CurlU3Ek__BackingField_0)); }
	inline String_t* get_U3CurlU3Ek__BackingField_0() const { return ___U3CurlU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CurlU3Ek__BackingField_0() { return &___U3CurlU3Ek__BackingField_0; }
	inline void set_U3CurlU3Ek__BackingField_0(String_t* value)
	{
		___U3CurlU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CurlU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ORIGINAL_T3571140033_H
#ifndef ENRICHED_T407156716_H
#define ENRICHED_T407156716_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Enriched
struct  Enriched_t407156716  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.URL IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Enriched::<url>k__BackingField
	URL_t4128228189 * ___U3CurlU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CurlU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Enriched_t407156716, ___U3CurlU3Ek__BackingField_0)); }
	inline URL_t4128228189 * get_U3CurlU3Ek__BackingField_0() const { return ___U3CurlU3Ek__BackingField_0; }
	inline URL_t4128228189 ** get_address_of_U3CurlU3Ek__BackingField_0() { return &___U3CurlU3Ek__BackingField_0; }
	inline void set_U3CurlU3Ek__BackingField_0(URL_t4128228189 * value)
	{
		___U3CurlU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CurlU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENRICHED_T407156716_H
#ifndef URL_T4128228189_H
#define URL_T4128228189_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.URL
struct  URL_t4128228189  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.URL::<image>k__BackingField
	String_t* ___U3CimageU3Ek__BackingField_0;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.ImageKeyword[] IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.URL::<imageKeywords>k__BackingField
	ImageKeywordU5BU5D_t3897519977* ___U3CimageKeywordsU3Ek__BackingField_1;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Feed[] IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.URL::<feeds>k__BackingField
	FeedU5BU5D_t332386033* ___U3CfeedsU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.URL::<url>k__BackingField
	String_t* ___U3CurlU3Ek__BackingField_3;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.URL::<title>k__BackingField
	String_t* ___U3CtitleU3Ek__BackingField_4;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.URL::<cleanedTitle>k__BackingField
	String_t* ___U3CcleanedTitleU3Ek__BackingField_5;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.URL::<language>k__BackingField
	String_t* ___U3ClanguageU3Ek__BackingField_6;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.PublicationDate IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.URL::<publicationDate>k__BackingField
	PublicationDate_t608222142 * ___U3CpublicationDateU3Ek__BackingField_7;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.URL::<text>k__BackingField
	String_t* ___U3CtextU3Ek__BackingField_8;
	// System.String IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.URL::<author>k__BackingField
	String_t* ___U3CauthorU3Ek__BackingField_9;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Keyword[] IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.URL::<keywords>k__BackingField
	KeywordU5BU5D_t3893757366* ___U3CkeywordsU3Ek__BackingField_10;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Entity[] IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.URL::<entities>k__BackingField
	EntityU5BU5D_t3969836080* ___U3CentitiesU3Ek__BackingField_11;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Concept[] IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.URL::<concepts>k__BackingField
	ConceptU5BU5D_t3502654871* ___U3CconceptsU3Ek__BackingField_12;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Relation IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.URL::<relations>k__BackingField
	Relation_t2336466576 * ___U3CrelationsU3Ek__BackingField_13;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.DocSentiment IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.URL::<docSentiment>k__BackingField
	DocSentiment_t2354958393 * ___U3CdocSentimentU3Ek__BackingField_14;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.Taxonomy IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.URL::<taxonomy>k__BackingField
	Taxonomy_t1527601757 * ___U3CtaxonomyU3Ek__BackingField_15;
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.EnrichedTitle[] IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.URL::<enrichedTitle>k__BackingField
	EnrichedTitleU5BU5D_t3938334697* ___U3CenrichedTitleU3Ek__BackingField_16;

public:
	inline static int32_t get_offset_of_U3CimageU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(URL_t4128228189, ___U3CimageU3Ek__BackingField_0)); }
	inline String_t* get_U3CimageU3Ek__BackingField_0() const { return ___U3CimageU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CimageU3Ek__BackingField_0() { return &___U3CimageU3Ek__BackingField_0; }
	inline void set_U3CimageU3Ek__BackingField_0(String_t* value)
	{
		___U3CimageU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CimageU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CimageKeywordsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(URL_t4128228189, ___U3CimageKeywordsU3Ek__BackingField_1)); }
	inline ImageKeywordU5BU5D_t3897519977* get_U3CimageKeywordsU3Ek__BackingField_1() const { return ___U3CimageKeywordsU3Ek__BackingField_1; }
	inline ImageKeywordU5BU5D_t3897519977** get_address_of_U3CimageKeywordsU3Ek__BackingField_1() { return &___U3CimageKeywordsU3Ek__BackingField_1; }
	inline void set_U3CimageKeywordsU3Ek__BackingField_1(ImageKeywordU5BU5D_t3897519977* value)
	{
		___U3CimageKeywordsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CimageKeywordsU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CfeedsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(URL_t4128228189, ___U3CfeedsU3Ek__BackingField_2)); }
	inline FeedU5BU5D_t332386033* get_U3CfeedsU3Ek__BackingField_2() const { return ___U3CfeedsU3Ek__BackingField_2; }
	inline FeedU5BU5D_t332386033** get_address_of_U3CfeedsU3Ek__BackingField_2() { return &___U3CfeedsU3Ek__BackingField_2; }
	inline void set_U3CfeedsU3Ek__BackingField_2(FeedU5BU5D_t332386033* value)
	{
		___U3CfeedsU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CfeedsU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CurlU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(URL_t4128228189, ___U3CurlU3Ek__BackingField_3)); }
	inline String_t* get_U3CurlU3Ek__BackingField_3() const { return ___U3CurlU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CurlU3Ek__BackingField_3() { return &___U3CurlU3Ek__BackingField_3; }
	inline void set_U3CurlU3Ek__BackingField_3(String_t* value)
	{
		___U3CurlU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CurlU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CtitleU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(URL_t4128228189, ___U3CtitleU3Ek__BackingField_4)); }
	inline String_t* get_U3CtitleU3Ek__BackingField_4() const { return ___U3CtitleU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CtitleU3Ek__BackingField_4() { return &___U3CtitleU3Ek__BackingField_4; }
	inline void set_U3CtitleU3Ek__BackingField_4(String_t* value)
	{
		___U3CtitleU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtitleU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CcleanedTitleU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(URL_t4128228189, ___U3CcleanedTitleU3Ek__BackingField_5)); }
	inline String_t* get_U3CcleanedTitleU3Ek__BackingField_5() const { return ___U3CcleanedTitleU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CcleanedTitleU3Ek__BackingField_5() { return &___U3CcleanedTitleU3Ek__BackingField_5; }
	inline void set_U3CcleanedTitleU3Ek__BackingField_5(String_t* value)
	{
		___U3CcleanedTitleU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcleanedTitleU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3ClanguageU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(URL_t4128228189, ___U3ClanguageU3Ek__BackingField_6)); }
	inline String_t* get_U3ClanguageU3Ek__BackingField_6() const { return ___U3ClanguageU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3ClanguageU3Ek__BackingField_6() { return &___U3ClanguageU3Ek__BackingField_6; }
	inline void set_U3ClanguageU3Ek__BackingField_6(String_t* value)
	{
		___U3ClanguageU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClanguageU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CpublicationDateU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(URL_t4128228189, ___U3CpublicationDateU3Ek__BackingField_7)); }
	inline PublicationDate_t608222142 * get_U3CpublicationDateU3Ek__BackingField_7() const { return ___U3CpublicationDateU3Ek__BackingField_7; }
	inline PublicationDate_t608222142 ** get_address_of_U3CpublicationDateU3Ek__BackingField_7() { return &___U3CpublicationDateU3Ek__BackingField_7; }
	inline void set_U3CpublicationDateU3Ek__BackingField_7(PublicationDate_t608222142 * value)
	{
		___U3CpublicationDateU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpublicationDateU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CtextU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(URL_t4128228189, ___U3CtextU3Ek__BackingField_8)); }
	inline String_t* get_U3CtextU3Ek__BackingField_8() const { return ___U3CtextU3Ek__BackingField_8; }
	inline String_t** get_address_of_U3CtextU3Ek__BackingField_8() { return &___U3CtextU3Ek__BackingField_8; }
	inline void set_U3CtextU3Ek__BackingField_8(String_t* value)
	{
		___U3CtextU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CauthorU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(URL_t4128228189, ___U3CauthorU3Ek__BackingField_9)); }
	inline String_t* get_U3CauthorU3Ek__BackingField_9() const { return ___U3CauthorU3Ek__BackingField_9; }
	inline String_t** get_address_of_U3CauthorU3Ek__BackingField_9() { return &___U3CauthorU3Ek__BackingField_9; }
	inline void set_U3CauthorU3Ek__BackingField_9(String_t* value)
	{
		___U3CauthorU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CauthorU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CkeywordsU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(URL_t4128228189, ___U3CkeywordsU3Ek__BackingField_10)); }
	inline KeywordU5BU5D_t3893757366* get_U3CkeywordsU3Ek__BackingField_10() const { return ___U3CkeywordsU3Ek__BackingField_10; }
	inline KeywordU5BU5D_t3893757366** get_address_of_U3CkeywordsU3Ek__BackingField_10() { return &___U3CkeywordsU3Ek__BackingField_10; }
	inline void set_U3CkeywordsU3Ek__BackingField_10(KeywordU5BU5D_t3893757366* value)
	{
		___U3CkeywordsU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CkeywordsU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_U3CentitiesU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(URL_t4128228189, ___U3CentitiesU3Ek__BackingField_11)); }
	inline EntityU5BU5D_t3969836080* get_U3CentitiesU3Ek__BackingField_11() const { return ___U3CentitiesU3Ek__BackingField_11; }
	inline EntityU5BU5D_t3969836080** get_address_of_U3CentitiesU3Ek__BackingField_11() { return &___U3CentitiesU3Ek__BackingField_11; }
	inline void set_U3CentitiesU3Ek__BackingField_11(EntityU5BU5D_t3969836080* value)
	{
		___U3CentitiesU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CentitiesU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CconceptsU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(URL_t4128228189, ___U3CconceptsU3Ek__BackingField_12)); }
	inline ConceptU5BU5D_t3502654871* get_U3CconceptsU3Ek__BackingField_12() const { return ___U3CconceptsU3Ek__BackingField_12; }
	inline ConceptU5BU5D_t3502654871** get_address_of_U3CconceptsU3Ek__BackingField_12() { return &___U3CconceptsU3Ek__BackingField_12; }
	inline void set_U3CconceptsU3Ek__BackingField_12(ConceptU5BU5D_t3502654871* value)
	{
		___U3CconceptsU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CconceptsU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CrelationsU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(URL_t4128228189, ___U3CrelationsU3Ek__BackingField_13)); }
	inline Relation_t2336466576 * get_U3CrelationsU3Ek__BackingField_13() const { return ___U3CrelationsU3Ek__BackingField_13; }
	inline Relation_t2336466576 ** get_address_of_U3CrelationsU3Ek__BackingField_13() { return &___U3CrelationsU3Ek__BackingField_13; }
	inline void set_U3CrelationsU3Ek__BackingField_13(Relation_t2336466576 * value)
	{
		___U3CrelationsU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrelationsU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CdocSentimentU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(URL_t4128228189, ___U3CdocSentimentU3Ek__BackingField_14)); }
	inline DocSentiment_t2354958393 * get_U3CdocSentimentU3Ek__BackingField_14() const { return ___U3CdocSentimentU3Ek__BackingField_14; }
	inline DocSentiment_t2354958393 ** get_address_of_U3CdocSentimentU3Ek__BackingField_14() { return &___U3CdocSentimentU3Ek__BackingField_14; }
	inline void set_U3CdocSentimentU3Ek__BackingField_14(DocSentiment_t2354958393 * value)
	{
		___U3CdocSentimentU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdocSentimentU3Ek__BackingField_14), value);
	}

	inline static int32_t get_offset_of_U3CtaxonomyU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(URL_t4128228189, ___U3CtaxonomyU3Ek__BackingField_15)); }
	inline Taxonomy_t1527601757 * get_U3CtaxonomyU3Ek__BackingField_15() const { return ___U3CtaxonomyU3Ek__BackingField_15; }
	inline Taxonomy_t1527601757 ** get_address_of_U3CtaxonomyU3Ek__BackingField_15() { return &___U3CtaxonomyU3Ek__BackingField_15; }
	inline void set_U3CtaxonomyU3Ek__BackingField_15(Taxonomy_t1527601757 * value)
	{
		___U3CtaxonomyU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtaxonomyU3Ek__BackingField_15), value);
	}

	inline static int32_t get_offset_of_U3CenrichedTitleU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(URL_t4128228189, ___U3CenrichedTitleU3Ek__BackingField_16)); }
	inline EnrichedTitleU5BU5D_t3938334697* get_U3CenrichedTitleU3Ek__BackingField_16() const { return ___U3CenrichedTitleU3Ek__BackingField_16; }
	inline EnrichedTitleU5BU5D_t3938334697** get_address_of_U3CenrichedTitleU3Ek__BackingField_16() { return &___U3CenrichedTitleU3Ek__BackingField_16; }
	inline void set_U3CenrichedTitleU3Ek__BackingField_16(EnrichedTitleU5BU5D_t3938334697* value)
	{
		___U3CenrichedTitleU3Ek__BackingField_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CenrichedTitleU3Ek__BackingField_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URL_T4128228189_H
#ifndef ENTITIES_T3103205639_H
#define ENTITIES_T3103205639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.Entities
struct  Entities_t3103205639  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.Entity[] IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.Entities::<entities>k__BackingField
	EntityU5BU5D_t3416566442* ___U3CentitiesU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CentitiesU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Entities_t3103205639, ___U3CentitiesU3Ek__BackingField_0)); }
	inline EntityU5BU5D_t3416566442* get_U3CentitiesU3Ek__BackingField_0() const { return ___U3CentitiesU3Ek__BackingField_0; }
	inline EntityU5BU5D_t3416566442** get_address_of_U3CentitiesU3Ek__BackingField_0() { return &___U3CentitiesU3Ek__BackingField_0; }
	inline void set_U3CentitiesU3Ek__BackingField_0(EntityU5BU5D_t3416566442* value)
	{
		___U3CentitiesU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CentitiesU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTITIES_T3103205639_H
#ifndef ENTITY_T4255761867_H
#define ENTITY_T4255761867_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.Entity
struct  Entity_t4255761867  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.Entity::<entity>k__BackingField
	String_t* ___U3CentityU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.Entity::<description>k__BackingField
	String_t* ___U3CdescriptionU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.Entity::<created>k__BackingField
	String_t* ___U3CcreatedU3Ek__BackingField_2;
	// System.Boolean IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.Entity::<open_list>k__BackingField
	bool ___U3Copen_listU3Ek__BackingField_3;
	// System.String[] IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.Entity::<tags>k__BackingField
	StringU5BU5D_t1642385972* ___U3CtagsU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CentityU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Entity_t4255761867, ___U3CentityU3Ek__BackingField_0)); }
	inline String_t* get_U3CentityU3Ek__BackingField_0() const { return ___U3CentityU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CentityU3Ek__BackingField_0() { return &___U3CentityU3Ek__BackingField_0; }
	inline void set_U3CentityU3Ek__BackingField_0(String_t* value)
	{
		___U3CentityU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CentityU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CdescriptionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Entity_t4255761867, ___U3CdescriptionU3Ek__BackingField_1)); }
	inline String_t* get_U3CdescriptionU3Ek__BackingField_1() const { return ___U3CdescriptionU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CdescriptionU3Ek__BackingField_1() { return &___U3CdescriptionU3Ek__BackingField_1; }
	inline void set_U3CdescriptionU3Ek__BackingField_1(String_t* value)
	{
		___U3CdescriptionU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdescriptionU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CcreatedU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Entity_t4255761867, ___U3CcreatedU3Ek__BackingField_2)); }
	inline String_t* get_U3CcreatedU3Ek__BackingField_2() const { return ___U3CcreatedU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CcreatedU3Ek__BackingField_2() { return &___U3CcreatedU3Ek__BackingField_2; }
	inline void set_U3CcreatedU3Ek__BackingField_2(String_t* value)
	{
		___U3CcreatedU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcreatedU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3Copen_listU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Entity_t4255761867, ___U3Copen_listU3Ek__BackingField_3)); }
	inline bool get_U3Copen_listU3Ek__BackingField_3() const { return ___U3Copen_listU3Ek__BackingField_3; }
	inline bool* get_address_of_U3Copen_listU3Ek__BackingField_3() { return &___U3Copen_listU3Ek__BackingField_3; }
	inline void set_U3Copen_listU3Ek__BackingField_3(bool value)
	{
		___U3Copen_listU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CtagsU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Entity_t4255761867, ___U3CtagsU3Ek__BackingField_4)); }
	inline StringU5BU5D_t1642385972* get_U3CtagsU3Ek__BackingField_4() const { return ___U3CtagsU3Ek__BackingField_4; }
	inline StringU5BU5D_t1642385972** get_address_of_U3CtagsU3Ek__BackingField_4() { return &___U3CtagsU3Ek__BackingField_4; }
	inline void set_U3CtagsU3Ek__BackingField_4(StringU5BU5D_t1642385972* value)
	{
		___U3CtagsU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtagsU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTITY_T4255761867_H
#ifndef VALUES_T2448352850_H
#define VALUES_T2448352850_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.Values
struct  Values_t2448352850  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.Values::<value>k__BackingField
	String_t* ___U3CvalueU3Ek__BackingField_0;
	// System.String[] IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.Values::<synonyms>k__BackingField
	StringU5BU5D_t1642385972* ___U3CsynonymsU3Ek__BackingField_1;
	// System.Object IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.Values::<metadata>k__BackingField
	RuntimeObject * ___U3CmetadataU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CvalueU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Values_t2448352850, ___U3CvalueU3Ek__BackingField_0)); }
	inline String_t* get_U3CvalueU3Ek__BackingField_0() const { return ___U3CvalueU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CvalueU3Ek__BackingField_0() { return &___U3CvalueU3Ek__BackingField_0; }
	inline void set_U3CvalueU3Ek__BackingField_0(String_t* value)
	{
		___U3CvalueU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CvalueU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CsynonymsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Values_t2448352850, ___U3CsynonymsU3Ek__BackingField_1)); }
	inline StringU5BU5D_t1642385972* get_U3CsynonymsU3Ek__BackingField_1() const { return ___U3CsynonymsU3Ek__BackingField_1; }
	inline StringU5BU5D_t1642385972** get_address_of_U3CsynonymsU3Ek__BackingField_1() { return &___U3CsynonymsU3Ek__BackingField_1; }
	inline void set_U3CsynonymsU3Ek__BackingField_1(StringU5BU5D_t1642385972* value)
	{
		___U3CsynonymsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsynonymsU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CmetadataU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Values_t2448352850, ___U3CmetadataU3Ek__BackingField_2)); }
	inline RuntimeObject * get_U3CmetadataU3Ek__BackingField_2() const { return ___U3CmetadataU3Ek__BackingField_2; }
	inline RuntimeObject ** get_address_of_U3CmetadataU3Ek__BackingField_2() { return &___U3CmetadataU3Ek__BackingField_2; }
	inline void set_U3CmetadataU3Ek__BackingField_2(RuntimeObject * value)
	{
		___U3CmetadataU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmetadataU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALUES_T2448352850_H
#ifndef EXAMPLES_T3776239139_H
#define EXAMPLES_T3776239139_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.Examples
struct  Examples_t3776239139  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.Example[] IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.Examples::<examples>k__BackingField
	ExampleU5BU5D_t854620307* ___U3CexamplesU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CexamplesU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Examples_t3776239139, ___U3CexamplesU3Ek__BackingField_0)); }
	inline ExampleU5BU5D_t854620307* get_U3CexamplesU3Ek__BackingField_0() const { return ___U3CexamplesU3Ek__BackingField_0; }
	inline ExampleU5BU5D_t854620307** get_address_of_U3CexamplesU3Ek__BackingField_0() { return &___U3CexamplesU3Ek__BackingField_0; }
	inline void set_U3CexamplesU3Ek__BackingField_0(ExampleU5BU5D_t854620307* value)
	{
		___U3CexamplesU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CexamplesU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXAMPLES_T3776239139_H
#ifndef EXAMPLE_T1927362902_H
#define EXAMPLE_T1927362902_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.Example
struct  Example_t1927362902  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.Example::<text>k__BackingField
	String_t* ___U3CtextU3Ek__BackingField_0;
	// IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.EntityExample IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.Example::<entities>k__BackingField
	EntityExample_t3008277273 * ___U3CentitiesU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CtextU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Example_t1927362902, ___U3CtextU3Ek__BackingField_0)); }
	inline String_t* get_U3CtextU3Ek__BackingField_0() const { return ___U3CtextU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CtextU3Ek__BackingField_0() { return &___U3CtextU3Ek__BackingField_0; }
	inline void set_U3CtextU3Ek__BackingField_0(String_t* value)
	{
		___U3CtextU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CentitiesU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Example_t1927362902, ___U3CentitiesU3Ek__BackingField_1)); }
	inline EntityExample_t3008277273 * get_U3CentitiesU3Ek__BackingField_1() const { return ___U3CentitiesU3Ek__BackingField_1; }
	inline EntityExample_t3008277273 ** get_address_of_U3CentitiesU3Ek__BackingField_1() { return &___U3CentitiesU3Ek__BackingField_1; }
	inline void set_U3CentitiesU3Ek__BackingField_1(EntityExample_t3008277273 * value)
	{
		___U3CentitiesU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CentitiesU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXAMPLE_T1927362902_H
#ifndef VALUE_T2642671545_H
#define VALUE_T2642671545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Value
struct  Value_t2642671545  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.DeepQA.v1.Value::<value>k__BackingField
	String_t* ___U3CvalueU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CvalueU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Value_t2642671545, ___U3CvalueU3Ek__BackingField_0)); }
	inline String_t* get_U3CvalueU3Ek__BackingField_0() const { return ___U3CvalueU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CvalueU3Ek__BackingField_0() { return &___U3CvalueU3Ek__BackingField_0; }
	inline void set_U3CvalueU3Ek__BackingField_0(String_t* value)
	{
		___U3CvalueU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CvalueU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALUE_T2642671545_H
#ifndef OUTPUT_T301201853_H
#define OUTPUT_T301201853_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.Output
struct  Output_t301201853  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.Output::<text>k__BackingField
	String_t* ___U3CtextU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CtextU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Output_t301201853, ___U3CtextU3Ek__BackingField_0)); }
	inline String_t* get_U3CtextU3Ek__BackingField_0() const { return ___U3CtextU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CtextU3Ek__BackingField_0() { return &___U3CtextU3Ek__BackingField_0; }
	inline void set_U3CtextU3Ek__BackingField_0(String_t* value)
	{
		___U3CtextU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OUTPUT_T301201853_H
#ifndef GOTO_T52763683_H
#define GOTO_T52763683_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.GoTo
struct  GoTo_t52763683  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.GoTo::<dialog_node>k__BackingField
	String_t* ___U3Cdialog_nodeU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.GoTo::<selector>k__BackingField
	String_t* ___U3CselectorU3Ek__BackingField_1;
	// System.Boolean IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.GoTo::<m_return>k__BackingField
	bool ___U3Cm_returnU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3Cdialog_nodeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(GoTo_t52763683, ___U3Cdialog_nodeU3Ek__BackingField_0)); }
	inline String_t* get_U3Cdialog_nodeU3Ek__BackingField_0() const { return ___U3Cdialog_nodeU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3Cdialog_nodeU3Ek__BackingField_0() { return &___U3Cdialog_nodeU3Ek__BackingField_0; }
	inline void set_U3Cdialog_nodeU3Ek__BackingField_0(String_t* value)
	{
		___U3Cdialog_nodeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cdialog_nodeU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CselectorU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(GoTo_t52763683, ___U3CselectorU3Ek__BackingField_1)); }
	inline String_t* get_U3CselectorU3Ek__BackingField_1() const { return ___U3CselectorU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CselectorU3Ek__BackingField_1() { return &___U3CselectorU3Ek__BackingField_1; }
	inline void set_U3CselectorU3Ek__BackingField_1(String_t* value)
	{
		___U3CselectorU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CselectorU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3Cm_returnU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(GoTo_t52763683, ___U3Cm_returnU3Ek__BackingField_2)); }
	inline bool get_U3Cm_returnU3Ek__BackingField_2() const { return ___U3Cm_returnU3Ek__BackingField_2; }
	inline bool* get_address_of_U3Cm_returnU3Ek__BackingField_2() { return &___U3Cm_returnU3Ek__BackingField_2; }
	inline void set_U3Cm_returnU3Ek__BackingField_2(bool value)
	{
		___U3Cm_returnU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GOTO_T52763683_H
#ifndef VERSION_T2011363452_H
#define VERSION_T2011363452_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.Version
struct  Version_t2011363452  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERSION_T2011363452_H
#ifndef DIALOGNODES_T737584699_H
#define DIALOGNODES_T737584699_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.DialogNodes
struct  DialogNodes_t737584699  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.DialogNode[] IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.DialogNodes::<dialog_nodes>k__BackingField
	DialogNodeU5BU5D_t270372031* ___U3Cdialog_nodesU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3Cdialog_nodesU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DialogNodes_t737584699, ___U3Cdialog_nodesU3Ek__BackingField_0)); }
	inline DialogNodeU5BU5D_t270372031* get_U3Cdialog_nodesU3Ek__BackingField_0() const { return ___U3Cdialog_nodesU3Ek__BackingField_0; }
	inline DialogNodeU5BU5D_t270372031** get_address_of_U3Cdialog_nodesU3Ek__BackingField_0() { return &___U3Cdialog_nodesU3Ek__BackingField_0; }
	inline void set_U3Cdialog_nodesU3Ek__BackingField_0(DialogNodeU5BU5D_t270372031* value)
	{
		___U3Cdialog_nodesU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cdialog_nodesU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIALOGNODES_T737584699_H
#ifndef DIALOGNODE_T2015514522_H
#define DIALOGNODE_T2015514522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.DialogNode
struct  DialogNode_t2015514522  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.DialogNode::<dialog_node>k__BackingField
	String_t* ___U3Cdialog_nodeU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.DialogNode::<description>k__BackingField
	String_t* ___U3CdescriptionU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.DialogNode::<created>k__BackingField
	String_t* ___U3CcreatedU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.DialogNode::<conditions>k__BackingField
	String_t* ___U3CconditionsU3Ek__BackingField_3;
	// System.String IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.DialogNode::<parent>k__BackingField
	String_t* ___U3CparentU3Ek__BackingField_4;
	// System.String IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.DialogNode::<previous_sibling>k__BackingField
	String_t* ___U3Cprevious_siblingU3Ek__BackingField_5;
	// IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.Output IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.DialogNode::<output>k__BackingField
	Output_t301201853 * ___U3CoutputU3Ek__BackingField_6;
	// System.Object IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.DialogNode::<context>k__BackingField
	RuntimeObject * ___U3CcontextU3Ek__BackingField_7;
	// IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.GoTo IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.DialogNode::<go_to>k__BackingField
	GoTo_t52763683 * ___U3Cgo_toU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of_U3Cdialog_nodeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DialogNode_t2015514522, ___U3Cdialog_nodeU3Ek__BackingField_0)); }
	inline String_t* get_U3Cdialog_nodeU3Ek__BackingField_0() const { return ___U3Cdialog_nodeU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3Cdialog_nodeU3Ek__BackingField_0() { return &___U3Cdialog_nodeU3Ek__BackingField_0; }
	inline void set_U3Cdialog_nodeU3Ek__BackingField_0(String_t* value)
	{
		___U3Cdialog_nodeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cdialog_nodeU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CdescriptionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(DialogNode_t2015514522, ___U3CdescriptionU3Ek__BackingField_1)); }
	inline String_t* get_U3CdescriptionU3Ek__BackingField_1() const { return ___U3CdescriptionU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CdescriptionU3Ek__BackingField_1() { return &___U3CdescriptionU3Ek__BackingField_1; }
	inline void set_U3CdescriptionU3Ek__BackingField_1(String_t* value)
	{
		___U3CdescriptionU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdescriptionU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CcreatedU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(DialogNode_t2015514522, ___U3CcreatedU3Ek__BackingField_2)); }
	inline String_t* get_U3CcreatedU3Ek__BackingField_2() const { return ___U3CcreatedU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CcreatedU3Ek__BackingField_2() { return &___U3CcreatedU3Ek__BackingField_2; }
	inline void set_U3CcreatedU3Ek__BackingField_2(String_t* value)
	{
		___U3CcreatedU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcreatedU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CconditionsU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(DialogNode_t2015514522, ___U3CconditionsU3Ek__BackingField_3)); }
	inline String_t* get_U3CconditionsU3Ek__BackingField_3() const { return ___U3CconditionsU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CconditionsU3Ek__BackingField_3() { return &___U3CconditionsU3Ek__BackingField_3; }
	inline void set_U3CconditionsU3Ek__BackingField_3(String_t* value)
	{
		___U3CconditionsU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CconditionsU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CparentU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(DialogNode_t2015514522, ___U3CparentU3Ek__BackingField_4)); }
	inline String_t* get_U3CparentU3Ek__BackingField_4() const { return ___U3CparentU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CparentU3Ek__BackingField_4() { return &___U3CparentU3Ek__BackingField_4; }
	inline void set_U3CparentU3Ek__BackingField_4(String_t* value)
	{
		___U3CparentU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CparentU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3Cprevious_siblingU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(DialogNode_t2015514522, ___U3Cprevious_siblingU3Ek__BackingField_5)); }
	inline String_t* get_U3Cprevious_siblingU3Ek__BackingField_5() const { return ___U3Cprevious_siblingU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3Cprevious_siblingU3Ek__BackingField_5() { return &___U3Cprevious_siblingU3Ek__BackingField_5; }
	inline void set_U3Cprevious_siblingU3Ek__BackingField_5(String_t* value)
	{
		___U3Cprevious_siblingU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cprevious_siblingU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CoutputU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(DialogNode_t2015514522, ___U3CoutputU3Ek__BackingField_6)); }
	inline Output_t301201853 * get_U3CoutputU3Ek__BackingField_6() const { return ___U3CoutputU3Ek__BackingField_6; }
	inline Output_t301201853 ** get_address_of_U3CoutputU3Ek__BackingField_6() { return &___U3CoutputU3Ek__BackingField_6; }
	inline void set_U3CoutputU3Ek__BackingField_6(Output_t301201853 * value)
	{
		___U3CoutputU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CoutputU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CcontextU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(DialogNode_t2015514522, ___U3CcontextU3Ek__BackingField_7)); }
	inline RuntimeObject * get_U3CcontextU3Ek__BackingField_7() const { return ___U3CcontextU3Ek__BackingField_7; }
	inline RuntimeObject ** get_address_of_U3CcontextU3Ek__BackingField_7() { return &___U3CcontextU3Ek__BackingField_7; }
	inline void set_U3CcontextU3Ek__BackingField_7(RuntimeObject * value)
	{
		___U3CcontextU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcontextU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3Cgo_toU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(DialogNode_t2015514522, ___U3Cgo_toU3Ek__BackingField_8)); }
	inline GoTo_t52763683 * get_U3Cgo_toU3Ek__BackingField_8() const { return ___U3Cgo_toU3Ek__BackingField_8; }
	inline GoTo_t52763683 ** get_address_of_U3Cgo_toU3Ek__BackingField_8() { return &___U3Cgo_toU3Ek__BackingField_8; }
	inline void set_U3Cgo_toU3Ek__BackingField_8(GoTo_t52763683 * value)
	{
		___U3Cgo_toU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cgo_toU3Ek__BackingField_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIALOGNODE_T2015514522_H
#ifndef MESSAGEINTENT_T315351975_H
#define MESSAGEINTENT_T315351975_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.MessageIntent
struct  MessageIntent_t315351975  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.MessageIntent::<intent>k__BackingField
	String_t* ___U3CintentU3Ek__BackingField_0;
	// System.Double IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.MessageIntent::<confidence>k__BackingField
	double ___U3CconfidenceU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CintentU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MessageIntent_t315351975, ___U3CintentU3Ek__BackingField_0)); }
	inline String_t* get_U3CintentU3Ek__BackingField_0() const { return ___U3CintentU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CintentU3Ek__BackingField_0() { return &___U3CintentU3Ek__BackingField_0; }
	inline void set_U3CintentU3Ek__BackingField_0(String_t* value)
	{
		___U3CintentU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CintentU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CconfidenceU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(MessageIntent_t315351975, ___U3CconfidenceU3Ek__BackingField_1)); }
	inline double get_U3CconfidenceU3Ek__BackingField_1() const { return ___U3CconfidenceU3Ek__BackingField_1; }
	inline double* get_address_of_U3CconfidenceU3Ek__BackingField_1() { return &___U3CconfidenceU3Ek__BackingField_1; }
	inline void set_U3CconfidenceU3Ek__BackingField_1(double value)
	{
		___U3CconfidenceU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGEINTENT_T315351975_H
#ifndef WORKSPACES_T1916725860_H
#define WORKSPACES_T1916725860_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.Workspaces
struct  Workspaces_t1916725860  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.Workspace[] IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.Workspaces::<workspaces>k__BackingField
	WorkspaceU5BU5D_t2127228610* ___U3CworkspacesU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CworkspacesU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Workspaces_t1916725860, ___U3CworkspacesU3Ek__BackingField_0)); }
	inline WorkspaceU5BU5D_t2127228610* get_U3CworkspacesU3Ek__BackingField_0() const { return ___U3CworkspacesU3Ek__BackingField_0; }
	inline WorkspaceU5BU5D_t2127228610** get_address_of_U3CworkspacesU3Ek__BackingField_0() { return &___U3CworkspacesU3Ek__BackingField_0; }
	inline void set_U3CworkspacesU3Ek__BackingField_0(WorkspaceU5BU5D_t2127228610* value)
	{
		___U3CworkspacesU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CworkspacesU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORKSPACES_T1916725860_H
#ifndef WORKSPACE_T1018343827_H
#define WORKSPACE_T1018343827_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.Workspace
struct  Workspace_t1018343827  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.Workspace::<workspace_id>k__BackingField
	String_t* ___U3Cworkspace_idU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.Workspace::<created>k__BackingField
	String_t* ___U3CcreatedU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.Workspace::<description>k__BackingField
	String_t* ___U3CdescriptionU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.Workspace::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_3;
	// System.String IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.Workspace::<language>k__BackingField
	String_t* ___U3ClanguageU3Ek__BackingField_4;
	// System.String IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.Workspace::<status>k__BackingField
	String_t* ___U3CstatusU3Ek__BackingField_5;
	// System.Object IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.Workspace::<metadata>k__BackingField
	RuntimeObject * ___U3CmetadataU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3Cworkspace_idU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Workspace_t1018343827, ___U3Cworkspace_idU3Ek__BackingField_0)); }
	inline String_t* get_U3Cworkspace_idU3Ek__BackingField_0() const { return ___U3Cworkspace_idU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3Cworkspace_idU3Ek__BackingField_0() { return &___U3Cworkspace_idU3Ek__BackingField_0; }
	inline void set_U3Cworkspace_idU3Ek__BackingField_0(String_t* value)
	{
		___U3Cworkspace_idU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cworkspace_idU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CcreatedU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Workspace_t1018343827, ___U3CcreatedU3Ek__BackingField_1)); }
	inline String_t* get_U3CcreatedU3Ek__BackingField_1() const { return ___U3CcreatedU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CcreatedU3Ek__BackingField_1() { return &___U3CcreatedU3Ek__BackingField_1; }
	inline void set_U3CcreatedU3Ek__BackingField_1(String_t* value)
	{
		___U3CcreatedU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcreatedU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CdescriptionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Workspace_t1018343827, ___U3CdescriptionU3Ek__BackingField_2)); }
	inline String_t* get_U3CdescriptionU3Ek__BackingField_2() const { return ___U3CdescriptionU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CdescriptionU3Ek__BackingField_2() { return &___U3CdescriptionU3Ek__BackingField_2; }
	inline void set_U3CdescriptionU3Ek__BackingField_2(String_t* value)
	{
		___U3CdescriptionU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdescriptionU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Workspace_t1018343827, ___U3CnameU3Ek__BackingField_3)); }
	inline String_t* get_U3CnameU3Ek__BackingField_3() const { return ___U3CnameU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_3() { return &___U3CnameU3Ek__BackingField_3; }
	inline void set_U3CnameU3Ek__BackingField_3(String_t* value)
	{
		___U3CnameU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3ClanguageU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Workspace_t1018343827, ___U3ClanguageU3Ek__BackingField_4)); }
	inline String_t* get_U3ClanguageU3Ek__BackingField_4() const { return ___U3ClanguageU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3ClanguageU3Ek__BackingField_4() { return &___U3ClanguageU3Ek__BackingField_4; }
	inline void set_U3ClanguageU3Ek__BackingField_4(String_t* value)
	{
		___U3ClanguageU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClanguageU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CstatusU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Workspace_t1018343827, ___U3CstatusU3Ek__BackingField_5)); }
	inline String_t* get_U3CstatusU3Ek__BackingField_5() const { return ___U3CstatusU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CstatusU3Ek__BackingField_5() { return &___U3CstatusU3Ek__BackingField_5; }
	inline void set_U3CstatusU3Ek__BackingField_5(String_t* value)
	{
		___U3CstatusU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstatusU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CmetadataU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Workspace_t1018343827, ___U3CmetadataU3Ek__BackingField_6)); }
	inline RuntimeObject * get_U3CmetadataU3Ek__BackingField_6() const { return ___U3CmetadataU3Ek__BackingField_6; }
	inline RuntimeObject ** get_address_of_U3CmetadataU3Ek__BackingField_6() { return &___U3CmetadataU3Ek__BackingField_6; }
	inline void set_U3CmetadataU3Ek__BackingField_6(RuntimeObject * value)
	{
		___U3CmetadataU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmetadataU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORKSPACE_T1018343827_H
#ifndef MESSAGERESPONSE_T2645798626_H
#define MESSAGERESPONSE_T2645798626_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.MessageResponse
struct  MessageResponse_t2645798626  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.MessageIntent[] IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.MessageResponse::<intents>k__BackingField
	MessageIntentU5BU5D_t2945543646* ___U3CintentsU3Ek__BackingField_0;
	// IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.EntityExample[] IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.MessageResponse::<entities>k__BackingField
	EntityExampleU5BU5D_t4277940324* ___U3CentitiesU3Ek__BackingField_1;
	// IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.Output IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.MessageResponse::<output>k__BackingField
	Output_t301201853 * ___U3CoutputU3Ek__BackingField_2;
	// System.Object IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.MessageResponse::<context>k__BackingField
	RuntimeObject * ___U3CcontextU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CintentsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MessageResponse_t2645798626, ___U3CintentsU3Ek__BackingField_0)); }
	inline MessageIntentU5BU5D_t2945543646* get_U3CintentsU3Ek__BackingField_0() const { return ___U3CintentsU3Ek__BackingField_0; }
	inline MessageIntentU5BU5D_t2945543646** get_address_of_U3CintentsU3Ek__BackingField_0() { return &___U3CintentsU3Ek__BackingField_0; }
	inline void set_U3CintentsU3Ek__BackingField_0(MessageIntentU5BU5D_t2945543646* value)
	{
		___U3CintentsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CintentsU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CentitiesU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(MessageResponse_t2645798626, ___U3CentitiesU3Ek__BackingField_1)); }
	inline EntityExampleU5BU5D_t4277940324* get_U3CentitiesU3Ek__BackingField_1() const { return ___U3CentitiesU3Ek__BackingField_1; }
	inline EntityExampleU5BU5D_t4277940324** get_address_of_U3CentitiesU3Ek__BackingField_1() { return &___U3CentitiesU3Ek__BackingField_1; }
	inline void set_U3CentitiesU3Ek__BackingField_1(EntityExampleU5BU5D_t4277940324* value)
	{
		___U3CentitiesU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CentitiesU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CoutputU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(MessageResponse_t2645798626, ___U3CoutputU3Ek__BackingField_2)); }
	inline Output_t301201853 * get_U3CoutputU3Ek__BackingField_2() const { return ___U3CoutputU3Ek__BackingField_2; }
	inline Output_t301201853 ** get_address_of_U3CoutputU3Ek__BackingField_2() { return &___U3CoutputU3Ek__BackingField_2; }
	inline void set_U3CoutputU3Ek__BackingField_2(Output_t301201853 * value)
	{
		___U3CoutputU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CoutputU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CcontextU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(MessageResponse_t2645798626, ___U3CcontextU3Ek__BackingField_3)); }
	inline RuntimeObject * get_U3CcontextU3Ek__BackingField_3() const { return ___U3CcontextU3Ek__BackingField_3; }
	inline RuntimeObject ** get_address_of_U3CcontextU3Ek__BackingField_3() { return &___U3CcontextU3Ek__BackingField_3; }
	inline void set_U3CcontextU3Ek__BackingField_3(RuntimeObject * value)
	{
		___U3CcontextU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcontextU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGERESPONSE_T2645798626_H
#ifndef ENTITYEXAMPLE_T3008277273_H
#define ENTITYEXAMPLE_T3008277273_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.EntityExample
struct  EntityExample_t3008277273  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.EntityExample::<entity>k__BackingField
	String_t* ___U3CentityU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.EntityExample::<value>k__BackingField
	String_t* ___U3CvalueU3Ek__BackingField_1;
	// System.Int32[] IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.EntityExample::<location>k__BackingField
	Int32U5BU5D_t3030399641* ___U3ClocationU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CentityU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(EntityExample_t3008277273, ___U3CentityU3Ek__BackingField_0)); }
	inline String_t* get_U3CentityU3Ek__BackingField_0() const { return ___U3CentityU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CentityU3Ek__BackingField_0() { return &___U3CentityU3Ek__BackingField_0; }
	inline void set_U3CentityU3Ek__BackingField_0(String_t* value)
	{
		___U3CentityU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CentityU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CvalueU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(EntityExample_t3008277273, ___U3CvalueU3Ek__BackingField_1)); }
	inline String_t* get_U3CvalueU3Ek__BackingField_1() const { return ___U3CvalueU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CvalueU3Ek__BackingField_1() { return &___U3CvalueU3Ek__BackingField_1; }
	inline void set_U3CvalueU3Ek__BackingField_1(String_t* value)
	{
		___U3CvalueU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CvalueU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3ClocationU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(EntityExample_t3008277273, ___U3ClocationU3Ek__BackingField_2)); }
	inline Int32U5BU5D_t3030399641* get_U3ClocationU3Ek__BackingField_2() const { return ___U3ClocationU3Ek__BackingField_2; }
	inline Int32U5BU5D_t3030399641** get_address_of_U3ClocationU3Ek__BackingField_2() { return &___U3ClocationU3Ek__BackingField_2; }
	inline void set_U3ClocationU3Ek__BackingField_2(Int32U5BU5D_t3030399641* value)
	{
		___U3ClocationU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClocationU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTITYEXAMPLE_T3008277273_H
#ifndef METADATAMAP_T2304952903_H
#define METADATAMAP_T2304952903_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.DeepQA.v1.MetaDataMap
struct  MetaDataMap_t2304952903  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.DeepQA.v1.MetaDataMap::<originalFile>k__BackingField
	String_t* ___U3CoriginalFileU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.DeepQA.v1.MetaDataMap::<title>k__BackingField
	String_t* ___U3CtitleU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.DeepQA.v1.MetaDataMap::<corpusName>k__BackingField
	String_t* ___U3CcorpusNameU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Services.DeepQA.v1.MetaDataMap::<fileName>k__BackingField
	String_t* ___U3CfileNameU3Ek__BackingField_3;
	// System.String IBM.Watson.DeveloperCloud.Services.DeepQA.v1.MetaDataMap::<DOCNO>k__BackingField
	String_t* ___U3CDOCNOU3Ek__BackingField_4;
	// System.String IBM.Watson.DeveloperCloud.Services.DeepQA.v1.MetaDataMap::<CorpusPlusDocno>k__BackingField
	String_t* ___U3CCorpusPlusDocnoU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CoriginalFileU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MetaDataMap_t2304952903, ___U3CoriginalFileU3Ek__BackingField_0)); }
	inline String_t* get_U3CoriginalFileU3Ek__BackingField_0() const { return ___U3CoriginalFileU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CoriginalFileU3Ek__BackingField_0() { return &___U3CoriginalFileU3Ek__BackingField_0; }
	inline void set_U3CoriginalFileU3Ek__BackingField_0(String_t* value)
	{
		___U3CoriginalFileU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CoriginalFileU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CtitleU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(MetaDataMap_t2304952903, ___U3CtitleU3Ek__BackingField_1)); }
	inline String_t* get_U3CtitleU3Ek__BackingField_1() const { return ___U3CtitleU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CtitleU3Ek__BackingField_1() { return &___U3CtitleU3Ek__BackingField_1; }
	inline void set_U3CtitleU3Ek__BackingField_1(String_t* value)
	{
		___U3CtitleU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtitleU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CcorpusNameU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(MetaDataMap_t2304952903, ___U3CcorpusNameU3Ek__BackingField_2)); }
	inline String_t* get_U3CcorpusNameU3Ek__BackingField_2() const { return ___U3CcorpusNameU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CcorpusNameU3Ek__BackingField_2() { return &___U3CcorpusNameU3Ek__BackingField_2; }
	inline void set_U3CcorpusNameU3Ek__BackingField_2(String_t* value)
	{
		___U3CcorpusNameU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcorpusNameU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CfileNameU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(MetaDataMap_t2304952903, ___U3CfileNameU3Ek__BackingField_3)); }
	inline String_t* get_U3CfileNameU3Ek__BackingField_3() const { return ___U3CfileNameU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CfileNameU3Ek__BackingField_3() { return &___U3CfileNameU3Ek__BackingField_3; }
	inline void set_U3CfileNameU3Ek__BackingField_3(String_t* value)
	{
		___U3CfileNameU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CfileNameU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CDOCNOU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(MetaDataMap_t2304952903, ___U3CDOCNOU3Ek__BackingField_4)); }
	inline String_t* get_U3CDOCNOU3Ek__BackingField_4() const { return ___U3CDOCNOU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CDOCNOU3Ek__BackingField_4() { return &___U3CDOCNOU3Ek__BackingField_4; }
	inline void set_U3CDOCNOU3Ek__BackingField_4(String_t* value)
	{
		___U3CDOCNOU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDOCNOU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CCorpusPlusDocnoU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(MetaDataMap_t2304952903, ___U3CCorpusPlusDocnoU3Ek__BackingField_5)); }
	inline String_t* get_U3CCorpusPlusDocnoU3Ek__BackingField_5() const { return ___U3CCorpusPlusDocnoU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CCorpusPlusDocnoU3Ek__BackingField_5() { return &___U3CCorpusPlusDocnoU3Ek__BackingField_5; }
	inline void set_U3CCorpusPlusDocnoU3Ek__BackingField_5(String_t* value)
	{
		___U3CCorpusPlusDocnoU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCorpusPlusDocnoU3Ek__BackingField_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METADATAMAP_T2304952903_H
#ifndef CHECKSERVICESTATUS_T3855468642_H
#define CHECKSERVICESTATUS_T3855468642_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.ConversationExperimental/CheckServiceStatus
struct  CheckServiceStatus_t3855468642  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.ConversationExperimental IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.ConversationExperimental/CheckServiceStatus::m_Service
	ConversationExperimental_t215658501 * ___m_Service_0;
	// IBM.Watson.DeveloperCloud.Services.ServiceStatus IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.ConversationExperimental/CheckServiceStatus::m_Callback
	ServiceStatus_t1443707987 * ___m_Callback_1;
	// System.Int32 IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.ConversationExperimental/CheckServiceStatus::m_ConversationCount
	int32_t ___m_ConversationCount_2;

public:
	inline static int32_t get_offset_of_m_Service_0() { return static_cast<int32_t>(offsetof(CheckServiceStatus_t3855468642, ___m_Service_0)); }
	inline ConversationExperimental_t215658501 * get_m_Service_0() const { return ___m_Service_0; }
	inline ConversationExperimental_t215658501 ** get_address_of_m_Service_0() { return &___m_Service_0; }
	inline void set_m_Service_0(ConversationExperimental_t215658501 * value)
	{
		___m_Service_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Service_0), value);
	}

	inline static int32_t get_offset_of_m_Callback_1() { return static_cast<int32_t>(offsetof(CheckServiceStatus_t3855468642, ___m_Callback_1)); }
	inline ServiceStatus_t1443707987 * get_m_Callback_1() const { return ___m_Callback_1; }
	inline ServiceStatus_t1443707987 ** get_address_of_m_Callback_1() { return &___m_Callback_1; }
	inline void set_m_Callback_1(ServiceStatus_t1443707987 * value)
	{
		___m_Callback_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Callback_1), value);
	}

	inline static int32_t get_offset_of_m_ConversationCount_2() { return static_cast<int32_t>(offsetof(CheckServiceStatus_t3855468642, ___m_ConversationCount_2)); }
	inline int32_t get_m_ConversationCount_2() const { return ___m_ConversationCount_2; }
	inline int32_t* get_address_of_m_ConversationCount_2() { return &___m_ConversationCount_2; }
	inline void set_m_ConversationCount_2(int32_t value)
	{
		___m_ConversationCount_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHECKSERVICESTATUS_T3855468642_H
#ifndef INTENTS_T2964089509_H
#define INTENTS_T2964089509_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.Intents
struct  Intents_t2964089509  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.Intent[] IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.Intents::<intents>k__BackingField
	IntentU5BU5D_t168093507* ___U3CintentsU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CintentsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Intents_t2964089509, ___U3CintentsU3Ek__BackingField_0)); }
	inline IntentU5BU5D_t168093507* get_U3CintentsU3Ek__BackingField_0() const { return ___U3CintentsU3Ek__BackingField_0; }
	inline IntentU5BU5D_t168093507** get_address_of_U3CintentsU3Ek__BackingField_0() { return &___U3CintentsU3Ek__BackingField_0; }
	inline void set_U3CintentsU3Ek__BackingField_0(IntentU5BU5D_t168093507* value)
	{
		___U3CintentsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CintentsU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTENTS_T2964089509_H
#ifndef INTENT_T3642575974_H
#define INTENT_T3642575974_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.Intent
struct  Intent_t3642575974  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.Intent::<intent>k__BackingField
	String_t* ___U3CintentU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.Intent::<created>k__BackingField
	String_t* ___U3CcreatedU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.Intent::<description>k__BackingField
	String_t* ___U3CdescriptionU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CintentU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Intent_t3642575974, ___U3CintentU3Ek__BackingField_0)); }
	inline String_t* get_U3CintentU3Ek__BackingField_0() const { return ___U3CintentU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CintentU3Ek__BackingField_0() { return &___U3CintentU3Ek__BackingField_0; }
	inline void set_U3CintentU3Ek__BackingField_0(String_t* value)
	{
		___U3CintentU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CintentU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CcreatedU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Intent_t3642575974, ___U3CcreatedU3Ek__BackingField_1)); }
	inline String_t* get_U3CcreatedU3Ek__BackingField_1() const { return ___U3CcreatedU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CcreatedU3Ek__BackingField_1() { return &___U3CcreatedU3Ek__BackingField_1; }
	inline void set_U3CcreatedU3Ek__BackingField_1(String_t* value)
	{
		___U3CcreatedU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcreatedU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CdescriptionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Intent_t3642575974, ___U3CdescriptionU3Ek__BackingField_2)); }
	inline String_t* get_U3CdescriptionU3Ek__BackingField_2() const { return ___U3CdescriptionU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CdescriptionU3Ek__BackingField_2() { return &___U3CdescriptionU3Ek__BackingField_2; }
	inline void set_U3CdescriptionU3Ek__BackingField_2(String_t* value)
	{
		___U3CdescriptionU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdescriptionU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTENT_T3642575974_H
#ifndef MESSAGEREQ_T3247845610_H
#define MESSAGEREQ_T3247845610_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.ConversationExperimental/MessageReq
struct  MessageReq_t3247845610  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.ConversationExperimental/OnMessage IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.ConversationExperimental/MessageReq::<Callback>k__BackingField
	OnMessage_t1296011603 * ___U3CCallbackU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(MessageReq_t3247845610, ___U3CCallbackU3Ek__BackingField_11)); }
	inline OnMessage_t1296011603 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline OnMessage_t1296011603 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(OnMessage_t1296011603 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGEREQ_T3247845610_H
#ifndef VOID_T1841601450_H
#define VOID_T1841601450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1841601450 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1841601450_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	IntPtr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline IntPtr_t get_Zero_1() const { return ___Zero_1; }
	inline IntPtr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(IntPtr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef ASKQUESTIONREQ_T2244459416_H
#define ASKQUESTIONREQ_T2244459416_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.DeepQA.v1.DeepQA/AskQuestionReq
struct  AskQuestionReq_t2244459416  : public Request_t466816980
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.DeepQA.v1.DeepQA/AskQuestionReq::<QuestionID>k__BackingField
	String_t* ___U3CQuestionIDU3Ek__BackingField_11;
	// IBM.Watson.DeveloperCloud.Services.DeepQA.v1.DeepQA/OnQuestion IBM.Watson.DeveloperCloud.Services.DeepQA.v1.DeepQA/AskQuestionReq::<Callback>k__BackingField
	OnQuestion_t3675891438 * ___U3CCallbackU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_U3CQuestionIDU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(AskQuestionReq_t2244459416, ___U3CQuestionIDU3Ek__BackingField_11)); }
	inline String_t* get_U3CQuestionIDU3Ek__BackingField_11() const { return ___U3CQuestionIDU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CQuestionIDU3Ek__BackingField_11() { return &___U3CQuestionIDU3Ek__BackingField_11; }
	inline void set_U3CQuestionIDU3Ek__BackingField_11(String_t* value)
	{
		___U3CQuestionIDU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CQuestionIDU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(AskQuestionReq_t2244459416, ___U3CCallbackU3Ek__BackingField_12)); }
	inline OnQuestion_t3675891438 * get_U3CCallbackU3Ek__BackingField_12() const { return ___U3CCallbackU3Ek__BackingField_12; }
	inline OnQuestion_t3675891438 ** get_address_of_U3CCallbackU3Ek__BackingField_12() { return &___U3CCallbackU3Ek__BackingField_12; }
	inline void set_U3CCallbackU3Ek__BackingField_12(OnQuestion_t3675891438 * value)
	{
		___U3CCallbackU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASKQUESTIONREQ_T2244459416_H
#ifndef PARSETREE_T2821719349_H
#define PARSETREE_T2821719349_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.DeepQA.v1.ParseTree
struct  ParseTree_t2821719349  : public Word_t3826420310
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.DeepQA.v1.ParseTree::<parseScore>k__BackingField
	String_t* ___U3CparseScoreU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of_U3CparseScoreU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(ParseTree_t2821719349, ___U3CparseScoreU3Ek__BackingField_14)); }
	inline String_t* get_U3CparseScoreU3Ek__BackingField_14() const { return ___U3CparseScoreU3Ek__BackingField_14; }
	inline String_t** get_address_of_U3CparseScoreU3Ek__BackingField_14() { return &___U3CparseScoreU3Ek__BackingField_14; }
	inline void set_U3CparseScoreU3Ek__BackingField_14(String_t* value)
	{
		___U3CparseScoreU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CparseScoreU3Ek__BackingField_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARSETREE_T2821719349_H
#ifndef BOOLEAN_T3825574718_H
#define BOOLEAN_T3825574718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t3825574718 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t3825574718, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t3825574718_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t3825574718_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t3825574718_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T3825574718_H
#ifndef MESSAGEREQ_T1917649578_H
#define MESSAGEREQ_T1917649578_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Conversation.v1.Conversation/MessageReq
struct  MessageReq_t1917649578  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.Conversation.v1.Conversation/OnMessage IBM.Watson.DeveloperCloud.Services.Conversation.v1.Conversation/MessageReq::<Callback>k__BackingField
	OnMessage_t3665388051 * ___U3CCallbackU3Ek__BackingField_11;
	// IBM.Watson.DeveloperCloud.Services.Conversation.v1.MessageRequest IBM.Watson.DeveloperCloud.Services.Conversation.v1.Conversation/MessageReq::<MessageRequest>k__BackingField
	MessageRequest_t1934991468 * ___U3CMessageRequestU3Ek__BackingField_12;
	// System.String IBM.Watson.DeveloperCloud.Services.Conversation.v1.Conversation/MessageReq::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(MessageReq_t1917649578, ___U3CCallbackU3Ek__BackingField_11)); }
	inline OnMessage_t3665388051 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline OnMessage_t3665388051 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(OnMessage_t3665388051 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CMessageRequestU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(MessageReq_t1917649578, ___U3CMessageRequestU3Ek__BackingField_12)); }
	inline MessageRequest_t1934991468 * get_U3CMessageRequestU3Ek__BackingField_12() const { return ___U3CMessageRequestU3Ek__BackingField_12; }
	inline MessageRequest_t1934991468 ** get_address_of_U3CMessageRequestU3Ek__BackingField_12() { return &___U3CMessageRequestU3Ek__BackingField_12; }
	inline void set_U3CMessageRequestU3Ek__BackingField_12(MessageRequest_t1934991468 * value)
	{
		___U3CMessageRequestU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMessageRequestU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(MessageReq_t1917649578, ___U3CDataU3Ek__BackingField_13)); }
	inline String_t* get_U3CDataU3Ek__BackingField_13() const { return ___U3CDataU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_13() { return &___U3CDataU3Ek__BackingField_13; }
	inline void set_U3CDataU3Ek__BackingField_13(String_t* value)
	{
		___U3CDataU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGEREQ_T1917649578_H
#ifndef DELEGATE_T3022476291_H
#define DELEGATE_T3022476291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t3022476291  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	IntPtr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	IntPtr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	IntPtr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	IntPtr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1572802995 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___invoke_impl_1)); }
	inline IntPtr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline IntPtr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(IntPtr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_3)); }
	inline IntPtr_t get_method_3() const { return ___method_3; }
	inline IntPtr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(IntPtr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___delegate_trampoline_4)); }
	inline IntPtr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline IntPtr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(IntPtr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_code_5)); }
	inline IntPtr_t get_method_code_5() const { return ___method_code_5; }
	inline IntPtr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(IntPtr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___data_8)); }
	inline DelegateData_t1572802995 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1572802995 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1572802995 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T3022476291_H
#ifndef MULTICASTDELEGATE_T3201952435_H
#define MULTICASTDELEGATE_T3201952435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t3201952435  : public Delegate_t3022476291
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t3201952435 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t3201952435 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___prev_9)); }
	inline MulticastDelegate_t3201952435 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t3201952435 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t3201952435 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___kpm_next_10)); }
	inline MulticastDelegate_t3201952435 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t3201952435 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t3201952435 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T3201952435_H
#ifndef ONMESSAGE_T1296011603_H
#define ONMESSAGE_T1296011603_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.ConversationExperimental/OnMessage
struct  OnMessage_t1296011603  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONMESSAGE_T1296011603_H
#ifndef ONGETWORKSPACES_T3187210192_H
#define ONGETWORKSPACES_T3187210192_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.ConversationExperimental/OnGetWorkspaces
struct  OnGetWorkspaces_t3187210192  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONGETWORKSPACES_T3187210192_H
#ifndef ONMESSAGECALLBACK_T476834366_H
#define ONMESSAGECALLBACK_T476834366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.ConversationExperimental/OnMessageCallback
struct  OnMessageCallback_t476834366  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONMESSAGECALLBACK_T476834366_H
#ifndef ONMESSAGE_T3665388051_H
#define ONMESSAGE_T3665388051_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.Conversation.v1.Conversation/OnMessage
struct  OnMessage_t3665388051  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONMESSAGE_T3665388051_H
#ifndef ONQUESTION_T3675891438_H
#define ONQUESTION_T3675891438_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.DeepQA.v1.DeepQA/OnQuestion
struct  OnQuestion_t3675891438  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONQUESTION_T3675891438_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2400 = { sizeof (Temporal_t3690293474), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2400[2] = 
{
	Temporal_t3690293474::get_offset_of_U3CtextU3Ek__BackingField_0(),
	Temporal_t3690293474::get_offset_of_U3CdecodedU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2401 = { sizeof (Decoded_t1922409334), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2401[4] = 
{
	Decoded_t1922409334::get_offset_of_U3CtypeU3Ek__BackingField_0(),
	Decoded_t1922409334::get_offset_of_U3CvalueU3Ek__BackingField_1(),
	Decoded_t1922409334::get_offset_of_U3CstartU3Ek__BackingField_2(),
	Decoded_t1922409334::get_offset_of_U3CendU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2402 = { sizeof (NewsResponse_t3536772326), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2402[2] = 
{
	NewsResponse_t3536772326::get_offset_of_U3CstatusU3Ek__BackingField_0(),
	NewsResponse_t3536772326::get_offset_of_U3CresultU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2403 = { sizeof (Result_t1515462031), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2403[3] = 
{
	Result_t1515462031::get_offset_of_U3CnextU3Ek__BackingField_0(),
	Result_t1515462031::get_offset_of_U3CstatusU3Ek__BackingField_1(),
	Result_t1515462031::get_offset_of_U3CdocsU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2404 = { sizeof (Docs_t1723806121), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2404[3] = 
{
	Docs_t1723806121::get_offset_of_U3CidU3Ek__BackingField_0(),
	Docs_t1723806121::get_offset_of_U3CsourceU3Ek__BackingField_1(),
	Docs_t1723806121::get_offset_of_U3CtimestampU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2405 = { sizeof (Source_t916780923), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2405[2] = 
{
	Source_t916780923::get_offset_of_U3CoriginalU3Ek__BackingField_0(),
	Source_t916780923::get_offset_of_U3CenrichedU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2406 = { sizeof (Original_t3571140033), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2406[1] = 
{
	Original_t3571140033::get_offset_of_U3CurlU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2407 = { sizeof (Enriched_t407156716), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2407[1] = 
{
	Enriched_t407156716::get_offset_of_U3CurlU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2408 = { sizeof (URL_t4128228189), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2408[17] = 
{
	URL_t4128228189::get_offset_of_U3CimageU3Ek__BackingField_0(),
	URL_t4128228189::get_offset_of_U3CimageKeywordsU3Ek__BackingField_1(),
	URL_t4128228189::get_offset_of_U3CfeedsU3Ek__BackingField_2(),
	URL_t4128228189::get_offset_of_U3CurlU3Ek__BackingField_3(),
	URL_t4128228189::get_offset_of_U3CtitleU3Ek__BackingField_4(),
	URL_t4128228189::get_offset_of_U3CcleanedTitleU3Ek__BackingField_5(),
	URL_t4128228189::get_offset_of_U3ClanguageU3Ek__BackingField_6(),
	URL_t4128228189::get_offset_of_U3CpublicationDateU3Ek__BackingField_7(),
	URL_t4128228189::get_offset_of_U3CtextU3Ek__BackingField_8(),
	URL_t4128228189::get_offset_of_U3CauthorU3Ek__BackingField_9(),
	URL_t4128228189::get_offset_of_U3CkeywordsU3Ek__BackingField_10(),
	URL_t4128228189::get_offset_of_U3CentitiesU3Ek__BackingField_11(),
	URL_t4128228189::get_offset_of_U3CconceptsU3Ek__BackingField_12(),
	URL_t4128228189::get_offset_of_U3CrelationsU3Ek__BackingField_13(),
	URL_t4128228189::get_offset_of_U3CdocSentimentU3Ek__BackingField_14(),
	URL_t4128228189::get_offset_of_U3CtaxonomyU3Ek__BackingField_15(),
	URL_t4128228189::get_offset_of_U3CenrichedTitleU3Ek__BackingField_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2409 = { sizeof (EnrichedTitle_t2959558712), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2409[6] = 
{
	EnrichedTitle_t2959558712::get_offset_of_U3CkeywordsU3Ek__BackingField_0(),
	EnrichedTitle_t2959558712::get_offset_of_U3CentitiesU3Ek__BackingField_1(),
	EnrichedTitle_t2959558712::get_offset_of_U3CconceptsU3Ek__BackingField_2(),
	EnrichedTitle_t2959558712::get_offset_of_U3CrelationsU3Ek__BackingField_3(),
	EnrichedTitle_t2959558712::get_offset_of_U3CdocSentimentU3Ek__BackingField_4(),
	EnrichedTitle_t2959558712::get_offset_of_U3CtaxonomyU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2410 = { sizeof (Fields_t4182106063), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2410[307] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2411 = { sizeof (Conversation_t105466997), -1, sizeof(Conversation_t105466997_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2411[3] = 
{
	0,
	0,
	Conversation_t105466997_StaticFields::get_offset_of_sm_Serializer_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2412 = { sizeof (OnMessage_t3665388051), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2413 = { sizeof (MessageReq_t1917649578), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2413[3] = 
{
	MessageReq_t1917649578::get_offset_of_U3CCallbackU3Ek__BackingField_11(),
	MessageReq_t1917649578::get_offset_of_U3CMessageRequestU3Ek__BackingField_12(),
	MessageReq_t1917649578::get_offset_of_U3CDataU3Ek__BackingField_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2414 = { sizeof (CheckServiceStatus_t780401186), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2414[3] = 
{
	CheckServiceStatus_t780401186::get_offset_of_m_Service_0(),
	CheckServiceStatus_t780401186::get_offset_of_m_Callback_1(),
	CheckServiceStatus_t780401186::get_offset_of_m_ConversationCount_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2415 = { sizeof (MessageResponse_t470867978), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2415[5] = 
{
	MessageResponse_t470867978::get_offset_of_U3CinputU3Ek__BackingField_0(),
	MessageResponse_t470867978::get_offset_of_U3CcontextU3Ek__BackingField_1(),
	MessageResponse_t470867978::get_offset_of_U3CentitiesU3Ek__BackingField_2(),
	MessageResponse_t470867978::get_offset_of_U3CintentsU3Ek__BackingField_3(),
	MessageResponse_t470867978::get_offset_of_U3CoutputU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2416 = { sizeof (EntityResponse_t2137526154), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2416[3] = 
{
	EntityResponse_t2137526154::get_offset_of_U3CentityU3Ek__BackingField_0(),
	EntityResponse_t2137526154::get_offset_of_U3ClocationU3Ek__BackingField_1(),
	EntityResponse_t2137526154::get_offset_of_U3CvalueU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2417 = { sizeof (Intent_t1324785294), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2417[2] = 
{
	Intent_t1324785294::get_offset_of_U3CintentU3Ek__BackingField_0(),
	Intent_t1324785294::get_offset_of_U3CconfidenceU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2418 = { sizeof (OutputData_t497470723), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2418[3] = 
{
	OutputData_t497470723::get_offset_of_U3Clog_messagesU3Ek__BackingField_0(),
	OutputData_t497470723::get_offset_of_U3CtextU3Ek__BackingField_1(),
	OutputData_t497470723::get_offset_of_U3Cnodes_visitedU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2419 = { sizeof (LogMessageResponse_t3273544838), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2419[2] = 
{
	LogMessageResponse_t3273544838::get_offset_of_U3ClevelU3Ek__BackingField_0(),
	LogMessageResponse_t3273544838::get_offset_of_U3CmsgU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2420 = { sizeof (MessageRequest_t1934991468), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2420[3] = 
{
	MessageRequest_t1934991468::get_offset_of_U3CinputU3Ek__BackingField_0(),
	MessageRequest_t1934991468::get_offset_of_U3Calternate_intentsU3Ek__BackingField_1(),
	MessageRequest_t1934991468::get_offset_of_U3CcontextU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2421 = { sizeof (InputData_t902315192), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2421[1] = 
{
	InputData_t902315192::get_offset_of_U3CtextU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2422 = { sizeof (Context_t2592795451), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2422[2] = 
{
	Context_t2592795451::get_offset_of_U3Cconversation_idU3Ek__BackingField_0(),
	Context_t2592795451::get_offset_of_U3CsystemU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2423 = { sizeof (SystemResponse_t541191420), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2423[3] = 
{
	SystemResponse_t541191420::get_offset_of_U3Cdialog_stackU3Ek__BackingField_0(),
	SystemResponse_t541191420::get_offset_of_U3Cdialog_turn_counterU3Ek__BackingField_1(),
	SystemResponse_t541191420::get_offset_of_U3Cdialog_request_counterU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2424 = { sizeof (Version_t1829570756), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2424[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2425 = { sizeof (ConversationExperimental_t215658501), -1, sizeof(ConversationExperimental_t215658501_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2425[3] = 
{
	0,
	ConversationExperimental_t215658501_StaticFields::get_offset_of_sm_Serializer_1(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2426 = { sizeof (OnGetWorkspaces_t3187210192), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2427 = { sizeof (OnMessageCallback_t476834366), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2428 = { sizeof (OnMessage_t1296011603), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2429 = { sizeof (MessageReq_t3247845610), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2429[1] = 
{
	MessageReq_t3247845610::get_offset_of_U3CCallbackU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2430 = { sizeof (CheckServiceStatus_t3855468642), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2430[3] = 
{
	CheckServiceStatus_t3855468642::get_offset_of_m_Service_0(),
	CheckServiceStatus_t3855468642::get_offset_of_m_Callback_1(),
	CheckServiceStatus_t3855468642::get_offset_of_m_ConversationCount_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2431 = { sizeof (Workspaces_t1916725860), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2431[1] = 
{
	Workspaces_t1916725860::get_offset_of_U3CworkspacesU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2432 = { sizeof (Workspace_t1018343827), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2432[7] = 
{
	Workspace_t1018343827::get_offset_of_U3Cworkspace_idU3Ek__BackingField_0(),
	Workspace_t1018343827::get_offset_of_U3CcreatedU3Ek__BackingField_1(),
	Workspace_t1018343827::get_offset_of_U3CdescriptionU3Ek__BackingField_2(),
	Workspace_t1018343827::get_offset_of_U3CnameU3Ek__BackingField_3(),
	Workspace_t1018343827::get_offset_of_U3ClanguageU3Ek__BackingField_4(),
	Workspace_t1018343827::get_offset_of_U3CstatusU3Ek__BackingField_5(),
	Workspace_t1018343827::get_offset_of_U3CmetadataU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2433 = { sizeof (MessageResponse_t2645798626), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2433[4] = 
{
	MessageResponse_t2645798626::get_offset_of_U3CintentsU3Ek__BackingField_0(),
	MessageResponse_t2645798626::get_offset_of_U3CentitiesU3Ek__BackingField_1(),
	MessageResponse_t2645798626::get_offset_of_U3CoutputU3Ek__BackingField_2(),
	MessageResponse_t2645798626::get_offset_of_U3CcontextU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2434 = { sizeof (Intents_t2964089509), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2434[1] = 
{
	Intents_t2964089509::get_offset_of_U3CintentsU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2435 = { sizeof (Intent_t3642575974), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2435[3] = 
{
	Intent_t3642575974::get_offset_of_U3CintentU3Ek__BackingField_0(),
	Intent_t3642575974::get_offset_of_U3CcreatedU3Ek__BackingField_1(),
	Intent_t3642575974::get_offset_of_U3CdescriptionU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2436 = { sizeof (Examples_t3776239139), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2436[1] = 
{
	Examples_t3776239139::get_offset_of_U3CexamplesU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2437 = { sizeof (Example_t1927362902), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2437[2] = 
{
	Example_t1927362902::get_offset_of_U3CtextU3Ek__BackingField_0(),
	Example_t1927362902::get_offset_of_U3CentitiesU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2438 = { sizeof (EntityExample_t3008277273), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2438[3] = 
{
	EntityExample_t3008277273::get_offset_of_U3CentityU3Ek__BackingField_0(),
	EntityExample_t3008277273::get_offset_of_U3CvalueU3Ek__BackingField_1(),
	EntityExample_t3008277273::get_offset_of_U3ClocationU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2439 = { sizeof (Entities_t3103205639), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2439[1] = 
{
	Entities_t3103205639::get_offset_of_U3CentitiesU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2440 = { sizeof (Entity_t4255761867), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2440[5] = 
{
	Entity_t4255761867::get_offset_of_U3CentityU3Ek__BackingField_0(),
	Entity_t4255761867::get_offset_of_U3CdescriptionU3Ek__BackingField_1(),
	Entity_t4255761867::get_offset_of_U3CcreatedU3Ek__BackingField_2(),
	Entity_t4255761867::get_offset_of_U3Copen_listU3Ek__BackingField_3(),
	Entity_t4255761867::get_offset_of_U3CtagsU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2441 = { sizeof (Values_t2448352850), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2441[3] = 
{
	Values_t2448352850::get_offset_of_U3CvalueU3Ek__BackingField_0(),
	Values_t2448352850::get_offset_of_U3CsynonymsU3Ek__BackingField_1(),
	Values_t2448352850::get_offset_of_U3CmetadataU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2442 = { sizeof (DialogNodes_t737584699), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2442[1] = 
{
	DialogNodes_t737584699::get_offset_of_U3Cdialog_nodesU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2443 = { sizeof (DialogNode_t2015514522), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2443[9] = 
{
	DialogNode_t2015514522::get_offset_of_U3Cdialog_nodeU3Ek__BackingField_0(),
	DialogNode_t2015514522::get_offset_of_U3CdescriptionU3Ek__BackingField_1(),
	DialogNode_t2015514522::get_offset_of_U3CcreatedU3Ek__BackingField_2(),
	DialogNode_t2015514522::get_offset_of_U3CconditionsU3Ek__BackingField_3(),
	DialogNode_t2015514522::get_offset_of_U3CparentU3Ek__BackingField_4(),
	DialogNode_t2015514522::get_offset_of_U3Cprevious_siblingU3Ek__BackingField_5(),
	DialogNode_t2015514522::get_offset_of_U3CoutputU3Ek__BackingField_6(),
	DialogNode_t2015514522::get_offset_of_U3CcontextU3Ek__BackingField_7(),
	DialogNode_t2015514522::get_offset_of_U3Cgo_toU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2444 = { sizeof (MessageIntent_t315351975), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2444[2] = 
{
	MessageIntent_t315351975::get_offset_of_U3CintentU3Ek__BackingField_0(),
	MessageIntent_t315351975::get_offset_of_U3CconfidenceU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2445 = { sizeof (Output_t301201853), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2445[1] = 
{
	Output_t301201853::get_offset_of_U3CtextU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2446 = { sizeof (GoTo_t52763683), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2446[3] = 
{
	GoTo_t52763683::get_offset_of_U3Cdialog_nodeU3Ek__BackingField_0(),
	GoTo_t52763683::get_offset_of_U3CselectorU3Ek__BackingField_1(),
	GoTo_t52763683::get_offset_of_U3Cm_returnU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2447 = { sizeof (Version_t2011363452), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2447[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2448 = { sizeof (Value_t2642671545), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2448[1] = 
{
	Value_t2642671545::get_offset_of_U3CvalueU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2449 = { sizeof (MetaDataMap_t2304952903), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2449[6] = 
{
	MetaDataMap_t2304952903::get_offset_of_U3CoriginalFileU3Ek__BackingField_0(),
	MetaDataMap_t2304952903::get_offset_of_U3CtitleU3Ek__BackingField_1(),
	MetaDataMap_t2304952903::get_offset_of_U3CcorpusNameU3Ek__BackingField_2(),
	MetaDataMap_t2304952903::get_offset_of_U3CfileNameU3Ek__BackingField_3(),
	MetaDataMap_t2304952903::get_offset_of_U3CDOCNOU3Ek__BackingField_4(),
	MetaDataMap_t2304952903::get_offset_of_U3CCorpusPlusDocnoU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2450 = { sizeof (Evidence_t859225293), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2450[8] = 
{
	Evidence_t859225293::get_offset_of_U3CvalueU3Ek__BackingField_0(),
	Evidence_t859225293::get_offset_of_U3CtextU3Ek__BackingField_1(),
	Evidence_t859225293::get_offset_of_U3CidU3Ek__BackingField_2(),
	Evidence_t859225293::get_offset_of_U3CtitleU3Ek__BackingField_3(),
	Evidence_t859225293::get_offset_of_U3CdocumentU3Ek__BackingField_4(),
	Evidence_t859225293::get_offset_of_U3CcopyrightU3Ek__BackingField_5(),
	Evidence_t859225293::get_offset_of_U3CtermsOfUseU3Ek__BackingField_6(),
	Evidence_t859225293::get_offset_of_U3CmetadataMapU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2451 = { sizeof (Synonym_t1752818203), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2451[3] = 
{
	Synonym_t1752818203::get_offset_of_U3CisChosenU3Ek__BackingField_0(),
	Synonym_t1752818203::get_offset_of_U3CvalueU3Ek__BackingField_1(),
	Synonym_t1752818203::get_offset_of_U3CweightU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2452 = { sizeof (SynSet_t2414122624), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2452[2] = 
{
	SynSet_t2414122624::get_offset_of_U3CnameU3Ek__BackingField_0(),
	SynSet_t2414122624::get_offset_of_U3CsynSetU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2453 = { sizeof (SynonymList_t1745302257), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2453[4] = 
{
	SynonymList_t1745302257::get_offset_of_U3CpartOfSpeechU3Ek__BackingField_0(),
	SynonymList_t1745302257::get_offset_of_U3CvalueU3Ek__BackingField_1(),
	SynonymList_t1745302257::get_offset_of_U3ClemmaU3Ek__BackingField_2(),
	SynonymList_t1745302257::get_offset_of_U3CsynSetU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2454 = { sizeof (EvidenceRequest_t2807863460), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2454[2] = 
{
	EvidenceRequest_t2807863460::get_offset_of_U3CitemsU3Ek__BackingField_0(),
	EvidenceRequest_t2807863460::get_offset_of_U3CprofileU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2455 = { sizeof (Answer_t3626344892), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2455[7] = 
{
	Answer_t3626344892::get_offset_of_U3CidU3Ek__BackingField_0(),
	Answer_t3626344892::get_offset_of_U3CtextU3Ek__BackingField_1(),
	Answer_t3626344892::get_offset_of_U3CpipelineU3Ek__BackingField_2(),
	Answer_t3626344892::get_offset_of_U3CformattedTextU3Ek__BackingField_3(),
	Answer_t3626344892::get_offset_of_U3CconfidenceU3Ek__BackingField_4(),
	Answer_t3626344892::get_offset_of_U3CevidenceU3Ek__BackingField_5(),
	Answer_t3626344892::get_offset_of_U3CentityTypesU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2456 = { sizeof (Cell_t3725717219), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2456[3] = 
{
	Cell_t3725717219::get_offset_of_U3CValueU3Ek__BackingField_0(),
	Cell_t3725717219::get_offset_of_U3CColSpanU3Ek__BackingField_1(),
	Cell_t3725717219::get_offset_of_U3CHighlightedU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2457 = { sizeof (Row_t3628585283), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2457[1] = 
{
	Row_t3628585283::get_offset_of_U3CcolumnsU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2458 = { sizeof (Table_t205705827), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2458[1] = 
{
	Table_t205705827::get_offset_of_U3CrowsU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2459 = { sizeof (Slots_t3709295365), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2459[4] = 
{
	Slots_t3709295365::get_offset_of_U3CpredU3Ek__BackingField_0(),
	Slots_t3709295365::get_offset_of_U3CsubjU3Ek__BackingField_1(),
	Slots_t3709295365::get_offset_of_U3CobjprepU3Ek__BackingField_2(),
	Slots_t3709295365::get_offset_of_U3CpsubjU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2460 = { sizeof (Word_t3826420310), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2460[14] = 
{
	Word_t3826420310::get_offset_of_U3CcompSlotParseNodesU3Ek__BackingField_0(),
	Word_t3826420310::get_offset_of_U3CslotnameU3Ek__BackingField_1(),
	Word_t3826420310::get_offset_of_U3CwordtextU3Ek__BackingField_2(),
	Word_t3826420310::get_offset_of_U3CslotnameoptionsU3Ek__BackingField_3(),
	Word_t3826420310::get_offset_of_U3CwordsenseU3Ek__BackingField_4(),
	Word_t3826420310::get_offset_of_U3CnumericsenseU3Ek__BackingField_5(),
	Word_t3826420310::get_offset_of_U3CseqnoU3Ek__BackingField_6(),
	Word_t3826420310::get_offset_of_U3CwordbeginU3Ek__BackingField_7(),
	Word_t3826420310::get_offset_of_U3CframebeginU3Ek__BackingField_8(),
	Word_t3826420310::get_offset_of_U3CframeendU3Ek__BackingField_9(),
	Word_t3826420310::get_offset_of_U3CwordendU3Ek__BackingField_10(),
	Word_t3826420310::get_offset_of_U3CfeaturesU3Ek__BackingField_11(),
	Word_t3826420310::get_offset_of_U3ClmodsU3Ek__BackingField_12(),
	Word_t3826420310::get_offset_of_U3CrmodsU3Ek__BackingField_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2461 = { sizeof (ParseTree_t2821719349), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2461[1] = 
{
	ParseTree_t2821719349::get_offset_of_U3CparseScoreU3Ek__BackingField_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2462 = { sizeof (Question_t4096007710), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2462[21] = 
{
	Question_t4096007710::get_offset_of_U3CqclasslistU3Ek__BackingField_0(),
	Question_t4096007710::get_offset_of_U3CfocuslistU3Ek__BackingField_1(),
	Question_t4096007710::get_offset_of_U3ClatlistU3Ek__BackingField_2(),
	Question_t4096007710::get_offset_of_U3CevidencelistU3Ek__BackingField_3(),
	Question_t4096007710::get_offset_of_U3CsynonymListU3Ek__BackingField_4(),
	Question_t4096007710::get_offset_of_U3CdisambiguatedEntitiesU3Ek__BackingField_5(),
	Question_t4096007710::get_offset_of_U3CxsgtopparsesU3Ek__BackingField_6(),
	Question_t4096007710::get_offset_of_U3CcasXmlU3Ek__BackingField_7(),
	Question_t4096007710::get_offset_of_U3CpipelineidU3Ek__BackingField_8(),
	Question_t4096007710::get_offset_of_U3CformattedAnswerU3Ek__BackingField_9(),
	Question_t4096007710::get_offset_of_U3CselectedProcessingComponentsU3Ek__BackingField_10(),
	Question_t4096007710::get_offset_of_U3CcategoryU3Ek__BackingField_11(),
	Question_t4096007710::get_offset_of_U3CitemsU3Ek__BackingField_12(),
	Question_t4096007710::get_offset_of_U3CstatusU3Ek__BackingField_13(),
	Question_t4096007710::get_offset_of_U3CidU3Ek__BackingField_14(),
	Question_t4096007710::get_offset_of_U3CquestionTextU3Ek__BackingField_15(),
	Question_t4096007710::get_offset_of_U3CevidenceRequestU3Ek__BackingField_16(),
	Question_t4096007710::get_offset_of_U3CanswersU3Ek__BackingField_17(),
	Question_t4096007710::get_offset_of_U3CerrorNotificationsU3Ek__BackingField_18(),
	Question_t4096007710::get_offset_of_U3CpassthruU3Ek__BackingField_19(),
	Question_t4096007710::get_offset_of_U3CquestionIdU3Ek__BackingField_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2463 = { sizeof (QuestionClass_t734245290), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2463[3] = 
{
	QuestionClass_t734245290::get_offset_of_U3Cout_of_domainU3Ek__BackingField_0(),
	QuestionClass_t734245290::get_offset_of_U3CquestionU3Ek__BackingField_1(),
	QuestionClass_t734245290::get_offset_of_U3CdomainU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2464 = { sizeof (Response_t4265902649), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2464[2] = 
{
	Response_t4265902649::get_offset_of_U3CquestionU3Ek__BackingField_0(),
	Response_t4265902649::get_offset_of_U3CquestionClassesU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2465 = { sizeof (ResponseList_t276562843), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2465[1] = 
{
	ResponseList_t276562843::get_offset_of_U3CresponsesU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2466 = { sizeof (DeepQA_t4122718210), -1, sizeof(DeepQA_t4122718210_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2466[5] = 
{
	DeepQA_t4122718210::get_offset_of_U3CDisableCacheU3Ek__BackingField_0(),
	DeepQA_t4122718210::get_offset_of_m_ServiceId_1(),
	DeepQA_t4122718210::get_offset_of_m_QuestionCache_2(),
	DeepQA_t4122718210_StaticFields::get_offset_of_sm_Serializer_3(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2467 = { sizeof (OnQuestion_t3675891438), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2468 = { sizeof (AskQuestionReq_t2244459416), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2468[2] = 
{
	AskQuestionReq_t2244459416::get_offset_of_U3CQuestionIDU3Ek__BackingField_11(),
	AskQuestionReq_t2244459416::get_offset_of_U3CCallbackU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2469 = { sizeof (CheckServiceStatus_t2304978370), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2469[2] = 
{
	CheckServiceStatus_t2304978370::get_offset_of_m_Service_0(),
	CheckServiceStatus_t2304978370::get_offset_of_m_Callback_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2470 = { sizeof (GetEnvironmentsResponse_t3378403827), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2470[1] = 
{
	GetEnvironmentsResponse_t3378403827::get_offset_of_U3CenvironmentsU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2471 = { sizeof (Environment_t3491688169), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2471[8] = 
{
	Environment_t3491688169::get_offset_of_U3Cenvironment_idU3Ek__BackingField_0(),
	Environment_t3491688169::get_offset_of_U3CnameU3Ek__BackingField_1(),
	Environment_t3491688169::get_offset_of_U3CdescriptionU3Ek__BackingField_2(),
	Environment_t3491688169::get_offset_of_U3CcreatedU3Ek__BackingField_3(),
	Environment_t3491688169::get_offset_of_U3CupdatedU3Ek__BackingField_4(),
	Environment_t3491688169::get_offset_of_U3CstatusU3Ek__BackingField_5(),
	Environment_t3491688169::get_offset_of_U3Cread_onlyU3Ek__BackingField_6(),
	Environment_t3491688169::get_offset_of_U3Cindex_capacityU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2472 = { sizeof (IndexCapacity_t726684902), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2472[2] = 
{
	IndexCapacity_t726684902::get_offset_of_U3Cdisk_usageU3Ek__BackingField_0(),
	IndexCapacity_t726684902::get_offset_of_U3Cmemory_usageU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2473 = { sizeof (DiskUsage_t2133232754), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2473[5] = 
{
	DiskUsage_t2133232754::get_offset_of_U3Cused_bytesU3Ek__BackingField_0(),
	DiskUsage_t2133232754::get_offset_of_U3Ctotal_bytesU3Ek__BackingField_1(),
	DiskUsage_t2133232754::get_offset_of_U3CusedU3Ek__BackingField_2(),
	DiskUsage_t2133232754::get_offset_of_U3CtotalU3Ek__BackingField_3(),
	DiskUsage_t2133232754::get_offset_of_U3Cpercent_usedU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2474 = { sizeof (MemoryUsage_t943632312), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2474[5] = 
{
	MemoryUsage_t943632312::get_offset_of_U3Cused_bytesU3Ek__BackingField_0(),
	MemoryUsage_t943632312::get_offset_of_U3Ctotal_bytesU3Ek__BackingField_1(),
	MemoryUsage_t943632312::get_offset_of_U3CusedU3Ek__BackingField_2(),
	MemoryUsage_t943632312::get_offset_of_U3CtotalU3Ek__BackingField_3(),
	MemoryUsage_t943632312::get_offset_of_U3Cpercent_usedU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2475 = { sizeof (DeleteEnvironmentResponse_t1123256339), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2475[2] = 
{
	DeleteEnvironmentResponse_t1123256339::get_offset_of_U3Cenvironment_idU3Ek__BackingField_0(),
	DeleteEnvironmentResponse_t1123256339::get_offset_of_U3CstatusU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2476 = { sizeof (TestDocument_t1009335169), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2476[5] = 
{
	TestDocument_t1009335169::get_offset_of_U3Cconfiguration_idU3Ek__BackingField_0(),
	TestDocument_t1009335169::get_offset_of_U3CstatusU3Ek__BackingField_1(),
	TestDocument_t1009335169::get_offset_of_U3Cenriched_field_unitsU3Ek__BackingField_2(),
	TestDocument_t1009335169::get_offset_of_U3Coriginal_media_typeU3Ek__BackingField_3(),
	TestDocument_t1009335169::get_offset_of_U3CnoticesU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2477 = { sizeof (DocumentSnapshot_t3752605611), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2477[2] = 
{
	DocumentSnapshot_t3752605611::get_offset_of_U3CstepU3Ek__BackingField_0(),
	DocumentSnapshot_t3752605611::get_offset_of_U3CsnapshotU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2478 = { sizeof (Notice_t1592579614), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2478[6] = 
{
	Notice_t1592579614::get_offset_of_U3Cnotice_idU3Ek__BackingField_0(),
	Notice_t1592579614::get_offset_of_U3CcreatedU3Ek__BackingField_1(),
	Notice_t1592579614::get_offset_of_U3Cdocument_idU3Ek__BackingField_2(),
	Notice_t1592579614::get_offset_of_U3CseverityU3Ek__BackingField_3(),
	Notice_t1592579614::get_offset_of_U3CstepU3Ek__BackingField_4(),
	Notice_t1592579614::get_offset_of_U3CdescriptionU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2479 = { sizeof (GetConfigurationsResponse_t3984912378), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2479[1] = 
{
	GetConfigurationsResponse_t3984912378::get_offset_of_U3CconfigurationsU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2480 = { sizeof (ConfigurationRef_t2639036165), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2480[5] = 
{
	ConfigurationRef_t2639036165::get_offset_of_U3Cconfiguration_idU3Ek__BackingField_0(),
	ConfigurationRef_t2639036165::get_offset_of_U3CcreatedU3Ek__BackingField_1(),
	ConfigurationRef_t2639036165::get_offset_of_U3CupdatedU3Ek__BackingField_2(),
	ConfigurationRef_t2639036165::get_offset_of_U3CnameU3Ek__BackingField_3(),
	ConfigurationRef_t2639036165::get_offset_of_U3CdescriptionU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2481 = { sizeof (Configuration_t2574692812), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2481[8] = 
{
	Configuration_t2574692812::get_offset_of_U3Cconfiguration_idU3Ek__BackingField_0(),
	Configuration_t2574692812::get_offset_of_U3CnameU3Ek__BackingField_1(),
	Configuration_t2574692812::get_offset_of_U3CcreatedU3Ek__BackingField_2(),
	Configuration_t2574692812::get_offset_of_U3CupdatedU3Ek__BackingField_3(),
	Configuration_t2574692812::get_offset_of_U3CdescriptionU3Ek__BackingField_4(),
	Configuration_t2574692812::get_offset_of_U3CconversionsU3Ek__BackingField_5(),
	Configuration_t2574692812::get_offset_of_U3CenrichmentsU3Ek__BackingField_6(),
	Configuration_t2574692812::get_offset_of_U3CnormalizationsU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2482 = { sizeof (Conversions_t3726444191), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2482[4] = 
{
	Conversions_t3726444191::get_offset_of_U3CpdfU3Ek__BackingField_0(),
	Conversions_t3726444191::get_offset_of_U3CwordU3Ek__BackingField_1(),
	Conversions_t3726444191::get_offset_of_U3ChtmlU3Ek__BackingField_2(),
	Conversions_t3726444191::get_offset_of_U3Cjson_normalizationsU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2483 = { sizeof (PdfSettings_t1519277167), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2483[1] = 
{
	PdfSettings_t1519277167::get_offset_of_U3CheadingU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2484 = { sizeof (WordSettings_t2098347111), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2484[1] = 
{
	WordSettings_t2098347111::get_offset_of_U3CheadingU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2485 = { sizeof (HtmlSettings_t99959826), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2485[6] = 
{
	HtmlSettings_t99959826::get_offset_of_U3Cexclude_tags_completelyU3Ek__BackingField_0(),
	HtmlSettings_t99959826::get_offset_of_U3Cexclude_tags_keep_contentU3Ek__BackingField_1(),
	HtmlSettings_t99959826::get_offset_of_U3Ckeep_contentU3Ek__BackingField_2(),
	HtmlSettings_t99959826::get_offset_of_U3Cexclude_contentU3Ek__BackingField_3(),
	HtmlSettings_t99959826::get_offset_of_U3Ckeep_tag_attributesU3Ek__BackingField_4(),
	HtmlSettings_t99959826::get_offset_of_U3Cexclude_tag_attributesU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2486 = { sizeof (Enrichment_t533139721), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2486[7] = 
{
	Enrichment_t533139721::get_offset_of_U3CdescriptionU3Ek__BackingField_0(),
	Enrichment_t533139721::get_offset_of_U3Cdestination_fieldU3Ek__BackingField_1(),
	Enrichment_t533139721::get_offset_of_U3Csource_fieldU3Ek__BackingField_2(),
	Enrichment_t533139721::get_offset_of_U3CoverwriteU3Ek__BackingField_3(),
	Enrichment_t533139721::get_offset_of_U3CenrichmentU3Ek__BackingField_4(),
	Enrichment_t533139721::get_offset_of_U3Cignore_downstream_errorsU3Ek__BackingField_5(),
	Enrichment_t533139721::get_offset_of_U3CoptionsU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2487 = { sizeof (NormalizationOperation_t3873208172), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2487[3] = 
{
	NormalizationOperation_t3873208172::get_offset_of_U3CoperationU3Ek__BackingField_0(),
	NormalizationOperation_t3873208172::get_offset_of_U3Csource_fieldU3Ek__BackingField_1(),
	NormalizationOperation_t3873208172::get_offset_of_U3Cdestination_fieldU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2488 = { sizeof (PdfHeadingDetection_t2648201765), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2488[1] = 
{
	PdfHeadingDetection_t2648201765::get_offset_of_U3CfontsU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2489 = { sizeof (WordHeadingDetection_t139434985), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2489[2] = 
{
	WordHeadingDetection_t139434985::get_offset_of_U3CfontsU3Ek__BackingField_0(),
	WordHeadingDetection_t139434985::get_offset_of_U3CstylesU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2490 = { sizeof (XPathPatterns_t1498040530), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2490[1] = 
{
	XPathPatterns_t1498040530::get_offset_of_U3CxpathsU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2491 = { sizeof (EnrichmentOptions_t3455576757), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2491[7] = 
{
	EnrichmentOptions_t3455576757::get_offset_of_U3CextractU3Ek__BackingField_0(),
	EnrichmentOptions_t3455576757::get_offset_of_U3CsentimentU3Ek__BackingField_1(),
	EnrichmentOptions_t3455576757::get_offset_of_U3CquotationsU3Ek__BackingField_2(),
	EnrichmentOptions_t3455576757::get_offset_of_U3CshowSouceTextU3Ek__BackingField_3(),
	EnrichmentOptions_t3455576757::get_offset_of_U3ChierarchicalTypedRelationsU3Ek__BackingField_4(),
	EnrichmentOptions_t3455576757::get_offset_of_U3CmodelU3Ek__BackingField_5(),
	EnrichmentOptions_t3455576757::get_offset_of_U3ClanguageU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2492 = { sizeof (FontSetting_t3267780181), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2492[6] = 
{
	FontSetting_t3267780181::get_offset_of_U3ClevelU3Ek__BackingField_0(),
	FontSetting_t3267780181::get_offset_of_U3Cmin_sizeU3Ek__BackingField_1(),
	FontSetting_t3267780181::get_offset_of_U3Cmax_sizeU3Ek__BackingField_2(),
	FontSetting_t3267780181::get_offset_of_U3CboldU3Ek__BackingField_3(),
	FontSetting_t3267780181::get_offset_of_U3CitalicU3Ek__BackingField_4(),
	FontSetting_t3267780181::get_offset_of_U3CnameU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2493 = { sizeof (WordStyle_t1557885949), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2493[2] = 
{
	WordStyle_t1557885949::get_offset_of_U3ClevelU3Ek__BackingField_0(),
	WordStyle_t1557885949::get_offset_of_U3CnamesU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2494 = { sizeof (DeleteConfigurationResponse_t86652326), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2494[3] = 
{
	DeleteConfigurationResponse_t86652326::get_offset_of_U3Cconfiguration_idU3Ek__BackingField_0(),
	DeleteConfigurationResponse_t86652326::get_offset_of_U3CstatusU3Ek__BackingField_1(),
	DeleteConfigurationResponse_t86652326::get_offset_of_U3CnoticesU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2495 = { sizeof (GetCollectionsResponse_t825581884), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2495[1] = 
{
	GetCollectionsResponse_t825581884::get_offset_of_U3CcollectionsU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2496 = { sizeof (CollectionRef_t3248558799), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2496[7] = 
{
	CollectionRef_t3248558799::get_offset_of_U3Ccollection_idU3Ek__BackingField_0(),
	CollectionRef_t3248558799::get_offset_of_U3CnameU3Ek__BackingField_1(),
	CollectionRef_t3248558799::get_offset_of_U3CdescriptionU3Ek__BackingField_2(),
	CollectionRef_t3248558799::get_offset_of_U3CcreatedU3Ek__BackingField_3(),
	CollectionRef_t3248558799::get_offset_of_U3CupdatedU3Ek__BackingField_4(),
	CollectionRef_t3248558799::get_offset_of_U3CstatusU3Ek__BackingField_5(),
	CollectionRef_t3248558799::get_offset_of_U3Cconfiguration_idU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2497 = { sizeof (DeleteCollectionResponse_t263138046), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2497[2] = 
{
	DeleteCollectionResponse_t263138046::get_offset_of_U3Ccollection_idU3Ek__BackingField_0(),
	DeleteCollectionResponse_t263138046::get_offset_of_U3CstatusU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2498 = { sizeof (Collection_t3355621610), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2498[8] = 
{
	Collection_t3355621610::get_offset_of_U3Ccollection_idU3Ek__BackingField_0(),
	Collection_t3355621610::get_offset_of_U3CnameU3Ek__BackingField_1(),
	Collection_t3355621610::get_offset_of_U3CdescriptionU3Ek__BackingField_2(),
	Collection_t3355621610::get_offset_of_U3CcreatedU3Ek__BackingField_3(),
	Collection_t3355621610::get_offset_of_U3CupdatedU3Ek__BackingField_4(),
	Collection_t3355621610::get_offset_of_U3CstatusU3Ek__BackingField_5(),
	Collection_t3355621610::get_offset_of_U3Cconfiguration_idU3Ek__BackingField_6(),
	Collection_t3355621610::get_offset_of_U3Cdocument_countsU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2499 = { sizeof (DocumentCounts_t1301920119), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2499[3] = 
{
	DocumentCounts_t1301920119::get_offset_of_U3CavailableU3Ek__BackingField_0(),
	DocumentCounts_t1301920119::get_offset_of_U3CprocessingU3Ek__BackingField_1(),
	DocumentCounts_t1301920119::get_offset_of_U3CfailedU3Ek__BackingField_2(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
