﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// AR3DOFCameraManager
struct AR3DOFCameraManager_t2152865733;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t1158329972;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface
struct UnityARSessionNativeInterface_t1130867170;
// UnityEngine.Object
struct Object_t1021602117;
// UnityEngine.Camera
struct Camera_t189460977;
// UnityEngine.Component
struct Component_t3819376471;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.XR.iOS.UnityARVideo
struct UnityARVideo_t2351297253;
// UnityEngine.Transform
struct Transform_t3275118058;
// DontDestroyOnLoad
struct DontDestroyOnLoad_t3235789354;
// PointCloudParticleExample
struct PointCloudParticleExample_t986756623;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARFrameUpdate
struct ARFrameUpdate_t496507918;
// UnityEngine.ParticleSystem
struct ParticleSystem_t3394631041;
// UnityEngine.ParticleSystem/Particle[]
struct ParticleU5BU5D_t574222242;
// UnityARCameraManager
struct UnityARCameraManager_t2138856896;
// UnityARCameraNearFar
struct UnityARCameraNearFar_t519802600;
// UnityEngine.XR.iOS.ARPlaneAnchorGameObject
struct ARPlaneAnchorGameObject_t2305225887;
// UnityEngine.XR.iOS.UnityARAnchorManager
struct UnityARAnchorManager_t1086564192;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.XR.iOS.ARPlaneAnchorGameObject>
struct Dictionary_2_t4220005149;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t2281509423;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorAdded
struct ARAnchorAdded_t2646854145;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorUpdated
struct ARAnchorUpdated_t3886071158;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorRemoved
struct ARAnchorRemoved_t142665927;
// System.String
struct String_t;
// System.Collections.Generic.List`1<UnityEngine.XR.iOS.ARPlaneAnchorGameObject>
struct List_1_t1674347019;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,UnityEngine.XR.iOS.ARPlaneAnchorGameObject>
struct ValueCollection_t2923064992;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>
struct ValueCollection_t984569266;
// System.Collections.Generic.IEnumerable`1<UnityEngine.XR.iOS.ARPlaneAnchorGameObject>
struct IEnumerable_1_t2597352932;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t2981576340;
// UnityEngine.XR.iOS.UnityARGeneratePlane
struct UnityARGeneratePlane_t3368998101;
// UnityEngine.XR.iOS.UnityARHitTestExample
struct UnityARHitTestExample_t146867607;
// System.Collections.Generic.List`1<UnityEngine.XR.iOS.ARHitTestResult>
struct List_1_t2644634157;
// UnityEngine.XR.iOS.UnityARKitControl
struct UnityARKitControl_t1698990409;
// UnityEngine.XR.iOS.UnityARMatrixOps
struct UnityARMatrixOps_t4039665643;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFrameUpdate
struct internal_ARFrameUpdate_t3296518558;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorAdded
struct internal_ARAnchorAdded_t1622117597;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorUpdated
struct internal_ARAnchorUpdated_t3705772742;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorRemoved
struct internal_ARAnchorRemoved_t3189755211;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARSessionFailed
struct ARSessionFailed_t872580813;
// System.Delegate
struct Delegate_t3022476291;
// UnityEngine.XR.iOS.ARKitWorldTackingSessionConfiguration
struct ARKitWorldTackingSessionConfiguration_t1821734930;
// UnityEngine.XR.iOS.ARKitSessionConfiguration
struct ARKitSessionConfiguration_t318899795;
// UnityEngine.XR.iOS.UnityARHitTestResult
struct UnityARHitTestResult_t4129824344;
// System.Single[]
struct SingleU5BU5D_t577127397;
// UnityEngine.XR.iOS.ARPlaneAnchor
struct ARPlaneAnchor_t1439520888;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// UnityEngine.XR.iOS.UnityARUtility
struct UnityARUtility_t3608388148;
// UnityEngine.MeshFilter
struct MeshFilter_t3026937449;
// UnityEngine.Rendering.CommandBuffer
struct CommandBuffer_t1204166949;
// UnityEngine.Texture
struct Texture_t2243626319;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// UnityPointCloudExample
struct UnityPointCloudExample_t3196264220;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;
// UnityEngine.XR.iOS.ARHitTestResult[]
struct ARHitTestResultU5BU5D_t3889134364;
// UnityEngine.MeshCollider
struct MeshCollider_t2718867283;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.Collections.Generic.Link[]
struct LinkU5BU5D_t62501539;
// System.String[]
struct StringU5BU5D_t1642385972;
// UnityEngine.XR.iOS.ARPlaneAnchorGameObject[]
struct ARPlaneAnchorGameObjectU5BU5D_t4180009478;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_t1241853011;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.Collections.Generic.Dictionary`2/Transform`1<System.String,UnityEngine.XR.iOS.ARPlaneAnchorGameObject,System.Collections.DictionaryEntry>
struct Transform_1_t3303849122;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// System.Void
struct Void_t1841601450;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1572802995;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_t834278767;
// UnityEngine.XR.iOS.UnityARSessionRunOption[]
struct UnityARSessionRunOptionU5BU5D_t3114965901;
// UnityEngine.XR.iOS.UnityARAlignment[]
struct UnityARAlignmentU5BU5D_t218994990;
// UnityEngine.XR.iOS.UnityARPlaneDetection[]
struct UnityARPlaneDetectionU5BU5D_t191549612;

extern RuntimeClass* UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var;
extern RuntimeClass* ARKitSessionConfiguration_t318899795_il2cpp_TypeInfo_var;
extern RuntimeClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t AR3DOFCameraManager_Start_m3126774506_MetadataUsageId;
extern const RuntimeMethod* GameObject_GetComponent_TisUnityARVideo_t2351297253_m2230129522_RuntimeMethod_var;
extern const uint32_t AR3DOFCameraManager_SetCamera_m946604403_MetadataUsageId;
extern const RuntimeMethod* GameObject_AddComponent_TisUnityARVideo_t2351297253_m1329128087_RuntimeMethod_var;
extern const uint32_t AR3DOFCameraManager_SetupNewCamera_m439842942_MetadataUsageId;
extern const uint32_t AR3DOFCameraManager_Update_m2410279539_MetadataUsageId;
extern const uint32_t DontDestroyOnLoad_Start_m34112045_MetadataUsageId;
extern RuntimeClass* ARFrameUpdate_t496507918_il2cpp_TypeInfo_var;
extern const RuntimeMethod* PointCloudParticleExample_ARFrameUpdated_m2750070680_RuntimeMethod_var;
extern const RuntimeMethod* Object_Instantiate_TisParticleSystem_t3394631041_m4293040502_RuntimeMethod_var;
extern const uint32_t PointCloudParticleExample_Start_m1963896970_MetadataUsageId;
extern RuntimeClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern RuntimeClass* ParticleU5BU5D_t574222242_il2cpp_TypeInfo_var;
extern const uint32_t PointCloudParticleExample_Update_m2434219493_MetadataUsageId;
extern RuntimeClass* ARKitWorldTackingSessionConfiguration_t1821734930_il2cpp_TypeInfo_var;
extern const uint32_t UnityARCameraManager_Start_m1258756459_MetadataUsageId;
extern const uint32_t UnityARCameraManager_SetCamera_m4250956606_MetadataUsageId;
extern const uint32_t UnityARCameraManager_SetupNewCamera_m2358778511_MetadataUsageId;
extern const uint32_t UnityARCameraManager_Update_m4265169368_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisCamera_t189460977_m3276577584_RuntimeMethod_var;
extern const uint32_t UnityARCameraNearFar_Start_m3303427069_MetadataUsageId;
extern const uint32_t UnityARCameraNearFar_UpdateCameraClipPlanes_m887086422_MetadataUsageId;
extern RuntimeClass* Dictionary_2_t4220005149_il2cpp_TypeInfo_var;
extern RuntimeClass* ARAnchorAdded_t2646854145_il2cpp_TypeInfo_var;
extern RuntimeClass* ARAnchorUpdated_t3886071158_il2cpp_TypeInfo_var;
extern RuntimeClass* ARAnchorRemoved_t142665927_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Dictionary_2__ctor_m1722416527_RuntimeMethod_var;
extern const RuntimeMethod* UnityARAnchorManager_AddAnchor_m2816330223_RuntimeMethod_var;
extern const RuntimeMethod* UnityARAnchorManager_UpdateAnchor_m4142154447_RuntimeMethod_var;
extern const RuntimeMethod* UnityARAnchorManager_RemoveAnchor_m2109073956_RuntimeMethod_var;
extern const uint32_t UnityARAnchorManager__ctor_m1861900703_MetadataUsageId;
extern RuntimeClass* UnityARUtility_t3608388148_il2cpp_TypeInfo_var;
extern RuntimeClass* ARPlaneAnchorGameObject_t2305225887_il2cpp_TypeInfo_var;
extern const RuntimeMethod* GameObject_AddComponent_TisDontDestroyOnLoad_t3235789354_m4215585572_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_Add_m2748008583_RuntimeMethod_var;
extern const uint32_t UnityARAnchorManager_AddAnchor_m2816330223_MetadataUsageId;
extern const RuntimeMethod* Dictionary_2_ContainsKey_m9048208_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_get_Item_m4111408485_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_Remove_m2895225056_RuntimeMethod_var;
extern const uint32_t UnityARAnchorManager_RemoveAnchor_m2109073956_MetadataUsageId;
extern const RuntimeMethod* Dictionary_2_set_Item_m3140527944_RuntimeMethod_var;
extern const uint32_t UnityARAnchorManager_UpdateAnchor_m4142154447_MetadataUsageId;
extern const RuntimeMethod* List_1_GetEnumerator_m4260200565_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m1081001953_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m3579574253_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m113660709_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_Clear_m797095524_RuntimeMethod_var;
extern const uint32_t UnityARAnchorManager_Destroy_m1751028949_MetadataUsageId;
extern const RuntimeMethod* Dictionary_2_get_Values_m586969881_RuntimeMethod_var;
extern const RuntimeMethod* Enumerable_ToList_TisARPlaneAnchorGameObject_t2305225887_m3769884394_RuntimeMethod_var;
extern const uint32_t UnityARAnchorManager_GetCurrentPlaneAnchors_m642073964_MetadataUsageId;
extern RuntimeClass* UnityARAnchorManager_t1086564192_il2cpp_TypeInfo_var;
extern const uint32_t UnityARGeneratePlane_Start_m2009009798_MetadataUsageId;
extern const RuntimeMethod* List_1_get_Count_m1714425968_RuntimeMethod_var;
extern const uint32_t UnityARGeneratePlane_OnGUI_m3343046494_MetadataUsageId;
extern RuntimeClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern RuntimeClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1_get_Count_m1598304542_RuntimeMethod_var;
extern const RuntimeMethod* List_1_GetEnumerator_m2885046859_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m2775711191_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m3661301011_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m3109677227_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral1415053652;
extern Il2CppCodeGenString* _stringLiteral1537444060;
extern const uint32_t UnityARHitTestExample_HitTestWithResultType_m996939112_MetadataUsageId;
extern RuntimeClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector2_t2243707579_il2cpp_TypeInfo_var;
extern RuntimeClass* ARPoint_t3436811567_il2cpp_TypeInfo_var;
extern RuntimeClass* ARHitTestResultTypeU5BU5D_t1303085420_il2cpp_TypeInfo_var;
extern RuntimeField* U3CPrivateImplementationDetailsU3E_t1486305143____U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0_FieldInfo_var;
extern const uint32_t UnityARHitTestExample_Update_m1266118659_MetadataUsageId;
extern RuntimeClass* UnityARSessionRunOptionU5BU5D_t3114965901_il2cpp_TypeInfo_var;
extern RuntimeClass* UnityARAlignmentU5BU5D_t218994990_il2cpp_TypeInfo_var;
extern RuntimeClass* UnityARPlaneDetectionU5BU5D_t191549612_il2cpp_TypeInfo_var;
extern const uint32_t UnityARKitControl__ctor_m1699713484_MetadataUsageId;
extern RuntimeClass* GUI_t4082743951_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3833953726;
extern Il2CppCodeGenString* _stringLiteral2858758002;
extern Il2CppCodeGenString* _stringLiteral2341529191;
extern Il2CppCodeGenString* _stringLiteral1743625051;
extern Il2CppCodeGenString* _stringLiteral1174079095;
extern Il2CppCodeGenString* _stringLiteral2724865136;
extern Il2CppCodeGenString* _stringLiteral1414246128;
extern Il2CppCodeGenString* _stringLiteral1301536196;
extern Il2CppCodeGenString* _stringLiteral3675648761;
extern Il2CppCodeGenString* _stringLiteral3025722940;
extern Il2CppCodeGenString* _stringLiteral3575497429;
extern Il2CppCodeGenString* _stringLiteral4059134932;
extern Il2CppCodeGenString* _stringLiteral855845486;
extern Il2CppCodeGenString* _stringLiteral3168505747;
extern const uint32_t UnityARKitControl_OnGUI_m4075060656_MetadataUsageId;
extern RuntimeClass* Vector4_t2243707581_il2cpp_TypeInfo_var;
extern const uint32_t UnityARMatrixOps_GetPosition_m1153858439_MetadataUsageId;
extern RuntimeClass* Quaternion_t4030073918_il2cpp_TypeInfo_var;
extern const uint32_t UnityARMatrixOps_QuaternionFromMatrix_m1686577287_MetadataUsageId;
extern RuntimeClass* internal_ARFrameUpdate_t3296518558_il2cpp_TypeInfo_var;
extern RuntimeClass* internal_ARAnchorAdded_t1622117597_il2cpp_TypeInfo_var;
extern RuntimeClass* internal_ARAnchorUpdated_t3705772742_il2cpp_TypeInfo_var;
extern RuntimeClass* internal_ARAnchorRemoved_t3189755211_il2cpp_TypeInfo_var;
extern RuntimeClass* ARSessionFailed_t872580813_il2cpp_TypeInfo_var;
extern const RuntimeMethod* UnityARSessionNativeInterface__frame_update_m127370_RuntimeMethod_var;
extern const RuntimeMethod* UnityARSessionNativeInterface__anchor_added_m3584567327_RuntimeMethod_var;
extern const RuntimeMethod* UnityARSessionNativeInterface__anchor_updated_m1970308864_RuntimeMethod_var;
extern const RuntimeMethod* UnityARSessionNativeInterface__anchor_removed_m4240378233_RuntimeMethod_var;
extern const RuntimeMethod* UnityARSessionNativeInterface__ar_session_failed_m2515310284_RuntimeMethod_var;
extern const uint32_t UnityARSessionNativeInterface__ctor_m2294513111_MetadataUsageId;
extern const uint32_t UnityARSessionNativeInterface_add_ARFrameUpdatedEvent_m2850773202_MetadataUsageId;
extern const uint32_t UnityARSessionNativeInterface_remove_ARFrameUpdatedEvent_m415315455_MetadataUsageId;
extern const uint32_t UnityARSessionNativeInterface_add_ARAnchorAddedEvent_m658032036_MetadataUsageId;
extern const uint32_t UnityARSessionNativeInterface_remove_ARAnchorAddedEvent_m1622386471_MetadataUsageId;
extern const uint32_t UnityARSessionNativeInterface_add_ARAnchorUpdatedEvent_m1485763364_MetadataUsageId;
extern const uint32_t UnityARSessionNativeInterface_remove_ARAnchorUpdatedEvent_m2137207143_MetadataUsageId;
extern const uint32_t UnityARSessionNativeInterface_add_ARAnchorRemovedEvent_m2970646020_MetadataUsageId;
extern const uint32_t UnityARSessionNativeInterface_remove_ARAnchorRemovedEvent_m1176445575_MetadataUsageId;
extern const uint32_t UnityARSessionNativeInterface_add_ARSessionFailedEvent_m1972153860_MetadataUsageId;
extern const uint32_t UnityARSessionNativeInterface_remove_ARSessionFailedEvent_m2378491719_MetadataUsageId;
struct ARKitWorldTackingSessionConfiguration_t1821734930_marshaled_pinvoke;
struct ARKitWorldTackingSessionConfiguration_t1821734930;;
struct ARKitWorldTackingSessionConfiguration_t1821734930_marshaled_pinvoke;;
struct ARKitSessionConfiguration_t318899795_marshaled_pinvoke;
struct ARKitSessionConfiguration_t318899795;;
struct ARKitSessionConfiguration_t318899795_marshaled_pinvoke;;
struct UnityARHitTestResult_t4129824344_marshaled_pinvoke;
struct UnityARHitTestResult_t4129824344;;
struct UnityARHitTestResult_t4129824344_marshaled_pinvoke;;
extern const uint32_t UnityARSessionNativeInterface_GetARSessionNativeInterface_m3174488657_MetadataUsageId;
extern RuntimeClass* Matrix4x4_t2933234003_il2cpp_TypeInfo_var;
extern const uint32_t UnityARSessionNativeInterface_GetCameraPose_m3046824030_MetadataUsageId;
extern const uint32_t UnityARSessionNativeInterface_GetCameraProjection_m3168017698_MetadataUsageId;
extern const uint32_t UnityARSessionNativeInterface_SetCameraClipPlanes_m149558303_MetadataUsageId;
extern RuntimeClass* UnityARCamera_t4198559457_il2cpp_TypeInfo_var;
extern const uint32_t UnityARSessionNativeInterface__frame_update_m127370_MetadataUsageId;
extern RuntimeClass* IntPtr_t_il2cpp_TypeInfo_var;
extern RuntimeClass* SingleU5BU5D_t577127397_il2cpp_TypeInfo_var;
extern RuntimeClass* Marshal_t785896760_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector3U5BU5D_t1172311765_il2cpp_TypeInfo_var;
extern const uint32_t UnityARSessionNativeInterface_UpdatePointCloudData_m2452168060_MetadataUsageId;
extern RuntimeClass* ARPlaneAnchor_t1439520888_il2cpp_TypeInfo_var;
extern const uint32_t UnityARSessionNativeInterface_GetPlaneAnchorFromAnchorData_m3105060455_MetadataUsageId;
extern RuntimeClass* ARHitTestResult_t3275513025_il2cpp_TypeInfo_var;
extern const uint32_t UnityARSessionNativeInterface_GetHitTestResultFromResultData_m1356947160_MetadataUsageId;
extern const uint32_t UnityARSessionNativeInterface__anchor_added_m3584567327_MetadataUsageId;
extern const uint32_t UnityARSessionNativeInterface__anchor_updated_m1970308864_MetadataUsageId;
extern const uint32_t UnityARSessionNativeInterface__anchor_removed_m4240378233_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2733749110;
extern const uint32_t UnityARSessionNativeInterface__ar_session_failed_m2515310284_MetadataUsageId;
extern const uint32_t UnityARSessionNativeInterface_RunWithConfigAndOptions_m375276821_MetadataUsageId;
extern const uint32_t UnityARSessionNativeInterface_RunWithConfig_m3195925270_MetadataUsageId;
extern const uint32_t UnityARSessionNativeInterface_RunWithConfigAndOptions_m4179857830_MetadataUsageId;
extern const uint32_t UnityARSessionNativeInterface_RunWithConfig_m2478060541_MetadataUsageId;
extern const uint32_t UnityARSessionNativeInterface_Pause_m2220930613_MetadataUsageId;
extern RuntimeClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t2644634157_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1__ctor_m3856316316_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m1918634088_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral3977806437;
extern const uint32_t UnityARSessionNativeInterface_HitTest_m388588674_MetadataUsageId;
extern const uint32_t UnityARSessionNativeInterface_GetARVideoTextureHandles_m2905358883_MetadataUsageId;
extern const uint32_t UnityARSessionNativeInterface_GetARAmbientIntensity_m3261179343_MetadataUsageId;
extern const uint32_t UnityARSessionNativeInterface_GetARTrackingQuality_m2957489224_MetadataUsageId;
extern const uint32_t UnityARSessionNativeInterface_GetARYUVTexCoordScale_m1823875812_MetadataUsageId;
struct ARPlaneAnchor_t1439520888_marshaled_pinvoke;
struct ARPlaneAnchor_t1439520888;;
struct ARPlaneAnchor_t1439520888_marshaled_pinvoke;;
extern const uint32_t ARAnchorAdded_BeginInvoke_m1705105833_MetadataUsageId;
extern const uint32_t ARAnchorRemoved_BeginInvoke_m810761227_MetadataUsageId;
extern const uint32_t ARAnchorUpdated_BeginInvoke_m2718860606_MetadataUsageId;
extern const uint32_t ARFrameUpdate_BeginInvoke_m4062970305_MetadataUsageId;
extern RuntimeClass* UnityARAnchorData_t2901866349_il2cpp_TypeInfo_var;
extern const uint32_t internal_ARAnchorAdded_BeginInvoke_m105627114_MetadataUsageId;
extern const uint32_t internal_ARAnchorRemoved_BeginInvoke_m3654189460_MetadataUsageId;
extern const uint32_t internal_ARAnchorUpdated_BeginInvoke_m879269121_MetadataUsageId;
extern RuntimeClass* internal_UnityARCamera_t2580192745_il2cpp_TypeInfo_var;
extern const uint32_t internal_ARFrameUpdate_BeginInvoke_m2703452769_MetadataUsageId;
extern const uint32_t UnityARUtility_InitializePlanePrefab_m2887188869_MetadataUsageId;
extern RuntimeClass* GameObject_t1756533147_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Object_Instantiate_TisGameObject_t1756533147_m3664764861_RuntimeMethod_var;
extern const uint32_t UnityARUtility_CreatePlaneInScene_m836370693_MetadataUsageId;
extern const RuntimeMethod* GameObject_GetComponentInChildren_TisMeshFilter_t3026937449_m1346841033_RuntimeMethod_var;
extern const uint32_t UnityARUtility_UpdatePlaneWithAnchorTransform_m639257622_MetadataUsageId;
extern const uint32_t UnityARVideo_Start_m474328190_MetadataUsageId;
extern RuntimeClass* CommandBuffer_t1204166949_il2cpp_TypeInfo_var;
extern const uint32_t UnityARVideo_InitializeCommandBuffer_m2800566831_MetadataUsageId;
extern const uint32_t UnityARVideo_OnDestroy_m4088618451_MetadataUsageId;
extern RuntimeClass* Vector3_t2243707580_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1152424733;
extern Il2CppCodeGenString* _stringLiteral2391281062;
extern Il2CppCodeGenString* _stringLiteral2946740916;
extern Il2CppCodeGenString* _stringLiteral1387439463;
extern Il2CppCodeGenString* _stringLiteral2454363760;
extern const uint32_t UnityARVideo_OnPreRender_m3078128848_MetadataUsageId;
extern RuntimeClass* List_1_t1125654279_il2cpp_TypeInfo_var;
extern const RuntimeMethod* UnityPointCloudExample_ARFrameUpdated_m3631038593_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m704351054_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m3441471442_RuntimeMethod_var;
extern const uint32_t UnityPointCloudExample_Start_m3369543775_MetadataUsageId;
extern const RuntimeMethod* List_1_get_Item_m939767277_RuntimeMethod_var;
extern const uint32_t UnityPointCloudExample_Update_m1416789468_MetadataUsageId;
struct Vector3_t2243707580 ;

struct Vector3U5BU5D_t1172311765;
struct ParticleU5BU5D_t574222242;
struct ARHitTestResultTypeU5BU5D_t1303085420;
struct UnityARSessionRunOptionU5BU5D_t3114965901;
struct UnityARAlignmentU5BU5D_t218994990;
struct UnityARPlaneDetectionU5BU5D_t191549612;
struct SingleU5BU5D_t577127397;


#ifndef U3CMODULEU3E_T3783534225_H
#define U3CMODULEU3E_T3783534225_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t3783534225 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T3783534225_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef UNITYARMATRIXOPS_T4039665643_H
#define UNITYARMATRIXOPS_T4039665643_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARMatrixOps
struct  UnityARMatrixOps_t4039665643  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARMATRIXOPS_T4039665643_H
#ifndef LIST_1_T2644634157_H
#define LIST_1_T2644634157_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.XR.iOS.ARHitTestResult>
struct  List_1_t2644634157  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ARHitTestResultU5BU5D_t3889134364* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t2644634157, ____items_1)); }
	inline ARHitTestResultU5BU5D_t3889134364* get__items_1() const { return ____items_1; }
	inline ARHitTestResultU5BU5D_t3889134364** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ARHitTestResultU5BU5D_t3889134364* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t2644634157, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t2644634157, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t2644634157_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	ARHitTestResultU5BU5D_t3889134364* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t2644634157_StaticFields, ___EmptyArray_4)); }
	inline ARHitTestResultU5BU5D_t3889134364* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline ARHitTestResultU5BU5D_t3889134364** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(ARHitTestResultU5BU5D_t3889134364* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T2644634157_H
#ifndef UNITYARUTILITY_T3608388148_H
#define UNITYARUTILITY_T3608388148_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARUtility
struct  UnityARUtility_t3608388148  : public RuntimeObject
{
public:
	// UnityEngine.MeshCollider UnityEngine.XR.iOS.UnityARUtility::meshCollider
	MeshCollider_t2718867283 * ___meshCollider_0;
	// UnityEngine.MeshFilter UnityEngine.XR.iOS.UnityARUtility::meshFilter
	MeshFilter_t3026937449 * ___meshFilter_1;

public:
	inline static int32_t get_offset_of_meshCollider_0() { return static_cast<int32_t>(offsetof(UnityARUtility_t3608388148, ___meshCollider_0)); }
	inline MeshCollider_t2718867283 * get_meshCollider_0() const { return ___meshCollider_0; }
	inline MeshCollider_t2718867283 ** get_address_of_meshCollider_0() { return &___meshCollider_0; }
	inline void set_meshCollider_0(MeshCollider_t2718867283 * value)
	{
		___meshCollider_0 = value;
		Il2CppCodeGenWriteBarrier((&___meshCollider_0), value);
	}

	inline static int32_t get_offset_of_meshFilter_1() { return static_cast<int32_t>(offsetof(UnityARUtility_t3608388148, ___meshFilter_1)); }
	inline MeshFilter_t3026937449 * get_meshFilter_1() const { return ___meshFilter_1; }
	inline MeshFilter_t3026937449 ** get_address_of_meshFilter_1() { return &___meshFilter_1; }
	inline void set_meshFilter_1(MeshFilter_t3026937449 * value)
	{
		___meshFilter_1 = value;
		Il2CppCodeGenWriteBarrier((&___meshFilter_1), value);
	}
};

struct UnityARUtility_t3608388148_StaticFields
{
public:
	// UnityEngine.GameObject UnityEngine.XR.iOS.UnityARUtility::planePrefab
	GameObject_t1756533147 * ___planePrefab_2;

public:
	inline static int32_t get_offset_of_planePrefab_2() { return static_cast<int32_t>(offsetof(UnityARUtility_t3608388148_StaticFields, ___planePrefab_2)); }
	inline GameObject_t1756533147 * get_planePrefab_2() const { return ___planePrefab_2; }
	inline GameObject_t1756533147 ** get_address_of_planePrefab_2() { return &___planePrefab_2; }
	inline void set_planePrefab_2(GameObject_t1756533147 * value)
	{
		___planePrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___planePrefab_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARUTILITY_T3608388148_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t1328083999* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t1328083999* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t1328083999** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t1328083999* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef DICTIONARY_2_T4220005149_H
#define DICTIONARY_2_T4220005149_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.XR.iOS.ARPlaneAnchorGameObject>
struct  Dictionary_2_t4220005149  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t3030399641* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t62501539* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	StringU5BU5D_t1642385972* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	ARPlaneAnchorGameObjectU5BU5D_t4180009478* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t228987430 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t4220005149, ___table_4)); }
	inline Int32U5BU5D_t3030399641* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t3030399641** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t3030399641* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t4220005149, ___linkSlots_5)); }
	inline LinkU5BU5D_t62501539* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t62501539** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t62501539* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t4220005149, ___keySlots_6)); }
	inline StringU5BU5D_t1642385972* get_keySlots_6() const { return ___keySlots_6; }
	inline StringU5BU5D_t1642385972** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(StringU5BU5D_t1642385972* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t4220005149, ___valueSlots_7)); }
	inline ARPlaneAnchorGameObjectU5BU5D_t4180009478* get_valueSlots_7() const { return ___valueSlots_7; }
	inline ARPlaneAnchorGameObjectU5BU5D_t4180009478** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(ARPlaneAnchorGameObjectU5BU5D_t4180009478* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t4220005149, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t4220005149, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t4220005149, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t4220005149, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t4220005149, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t4220005149, ___serialization_info_13)); }
	inline SerializationInfo_t228987430 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t228987430 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t228987430 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t4220005149, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t4220005149_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t3303849122 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t4220005149_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t3303849122 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t3303849122 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t3303849122 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T4220005149_H
#ifndef UNITYARANCHORMANAGER_T1086564192_H
#define UNITYARANCHORMANAGER_T1086564192_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARAnchorManager
struct  UnityARAnchorManager_t1086564192  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.XR.iOS.ARPlaneAnchorGameObject> UnityEngine.XR.iOS.UnityARAnchorManager::planeAnchorMap
	Dictionary_2_t4220005149 * ___planeAnchorMap_0;

public:
	inline static int32_t get_offset_of_planeAnchorMap_0() { return static_cast<int32_t>(offsetof(UnityARAnchorManager_t1086564192, ___planeAnchorMap_0)); }
	inline Dictionary_2_t4220005149 * get_planeAnchorMap_0() const { return ___planeAnchorMap_0; }
	inline Dictionary_2_t4220005149 ** get_address_of_planeAnchorMap_0() { return &___planeAnchorMap_0; }
	inline void set_planeAnchorMap_0(Dictionary_2_t4220005149 * value)
	{
		___planeAnchorMap_0 = value;
		Il2CppCodeGenWriteBarrier((&___planeAnchorMap_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARANCHORMANAGER_T1086564192_H
#ifndef VALUECOLLECTION_T2923064992_H
#define VALUECOLLECTION_T2923064992_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,UnityEngine.XR.iOS.ARPlaneAnchorGameObject>
struct  ValueCollection_t2923064992  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection::dictionary
	Dictionary_2_t4220005149 * ___dictionary_0;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(ValueCollection_t2923064992, ___dictionary_0)); }
	inline Dictionary_2_t4220005149 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t4220005149 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t4220005149 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALUECOLLECTION_T2923064992_H
#ifndef LIST_1_T1674347019_H
#define LIST_1_T1674347019_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.XR.iOS.ARPlaneAnchorGameObject>
struct  List_1_t1674347019  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ARPlaneAnchorGameObjectU5BU5D_t4180009478* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t1674347019, ____items_1)); }
	inline ARPlaneAnchorGameObjectU5BU5D_t4180009478* get__items_1() const { return ____items_1; }
	inline ARPlaneAnchorGameObjectU5BU5D_t4180009478** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ARPlaneAnchorGameObjectU5BU5D_t4180009478* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t1674347019, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t1674347019, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t1674347019_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	ARPlaneAnchorGameObjectU5BU5D_t4180009478* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t1674347019_StaticFields, ___EmptyArray_4)); }
	inline ARPlaneAnchorGameObjectU5BU5D_t4180009478* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline ARPlaneAnchorGameObjectU5BU5D_t4180009478** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(ARPlaneAnchorGameObjectU5BU5D_t4180009478* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T1674347019_H
#ifndef LIST_1_T1125654279_H
#define LIST_1_T1125654279_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct  List_1_t1125654279  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	GameObjectU5BU5D_t3057952154* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t1125654279, ____items_1)); }
	inline GameObjectU5BU5D_t3057952154* get__items_1() const { return ____items_1; }
	inline GameObjectU5BU5D_t3057952154** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(GameObjectU5BU5D_t3057952154* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t1125654279, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t1125654279, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t1125654279_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	GameObjectU5BU5D_t3057952154* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t1125654279_StaticFields, ___EmptyArray_4)); }
	inline GameObjectU5BU5D_t3057952154* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(GameObjectU5BU5D_t3057952154* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T1125654279_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef RECT_T3681755626_H
#define RECT_T3681755626_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t3681755626 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t3681755626, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t3681755626, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t3681755626, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t3681755626, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T3681755626_H
#ifndef ARLIGHTESTIMATE_T3477821059_H
#define ARLIGHTESTIMATE_T3477821059_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARLightEstimate
struct  ARLightEstimate_t3477821059 
{
public:
	// System.Double UnityEngine.XR.iOS.ARLightEstimate::ambientIntensity
	double ___ambientIntensity_0;

public:
	inline static int32_t get_offset_of_ambientIntensity_0() { return static_cast<int32_t>(offsetof(ARLightEstimate_t3477821059, ___ambientIntensity_0)); }
	inline double get_ambientIntensity_0() const { return ___ambientIntensity_0; }
	inline double* get_address_of_ambientIntensity_0() { return &___ambientIntensity_0; }
	inline void set_ambientIntensity_0(double value)
	{
		___ambientIntensity_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARLIGHTESTIMATE_T3477821059_H
#ifndef ENUMERATOR_T1209076693_H
#define ENUMERATOR_T1209076693_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<UnityEngine.XR.iOS.ARPlaneAnchorGameObject>
struct  Enumerator_t1209076693 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t1674347019 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	ARPlaneAnchorGameObject_t2305225887 * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t1209076693, ___l_0)); }
	inline List_1_t1674347019 * get_l_0() const { return ___l_0; }
	inline List_1_t1674347019 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t1674347019 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t1209076693, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t1209076693, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t1209076693, ___current_3)); }
	inline ARPlaneAnchorGameObject_t2305225887 * get_current_3() const { return ___current_3; }
	inline ARPlaneAnchorGameObject_t2305225887 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(ARPlaneAnchorGameObject_t2305225887 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T1209076693_H
#ifndef VECTOR4_T2243707581_H
#define VECTOR4_T2243707581_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t2243707581 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t2243707581, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t2243707581, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t2243707581, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t2243707581, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t2243707581_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t2243707581  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t2243707581  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t2243707581  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t2243707581  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t2243707581_StaticFields, ___zeroVector_5)); }
	inline Vector4_t2243707581  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t2243707581 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t2243707581  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t2243707581_StaticFields, ___oneVector_6)); }
	inline Vector4_t2243707581  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t2243707581 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t2243707581  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t2243707581_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t2243707581  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t2243707581 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t2243707581  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t2243707581_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t2243707581  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t2243707581 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t2243707581  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T2243707581_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	IntPtr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline IntPtr_t get_Zero_1() const { return ___Zero_1; }
	inline IntPtr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(IntPtr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef U24ARRAYTYPEU3D24_T762068664_H
#define U24ARRAYTYPEU3D24_T762068664_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=24
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D24_t762068664 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D24_t762068664__padding[24];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D24_T762068664_H
#ifndef VECTOR2_T2243707579_H
#define VECTOR2_T2243707579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2243707579 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2243707579, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2243707579, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2243707579_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2243707579  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2243707579  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2243707579  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2243707579  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2243707579  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2243707579  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2243707579  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2243707579  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2243707579  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2243707579 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2243707579  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___oneVector_3)); }
	inline Vector2_t2243707579  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2243707579 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2243707579  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___upVector_4)); }
	inline Vector2_t2243707579  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2243707579 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2243707579  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___downVector_5)); }
	inline Vector2_t2243707579  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2243707579 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2243707579  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___leftVector_6)); }
	inline Vector2_t2243707579  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2243707579 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2243707579  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___rightVector_7)); }
	inline Vector2_t2243707579  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2243707579 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2243707579  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2243707579  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2243707579 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2243707579  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2243707579  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2243707579 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2243707579  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2243707579_H
#ifndef INT64_T909078037_H
#define INT64_T909078037_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int64
struct  Int64_t909078037 
{
public:
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int64_t909078037, ___m_value_2)); }
	inline int64_t get_m_value_2() const { return ___m_value_2; }
	inline int64_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int64_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT64_T909078037_H
#ifndef VOID_T1841601450_H
#define VOID_T1841601450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1841601450 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1841601450_H
#ifndef COLOR32_T874517518_H
#define COLOR32_T874517518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color32
struct ALIGN_TYPE(4) Color32_t874517518 
{
public:
	// System.Byte UnityEngine.Color32::r
	uint8_t ___r_0;
	// System.Byte UnityEngine.Color32::g
	uint8_t ___g_1;
	// System.Byte UnityEngine.Color32::b
	uint8_t ___b_2;
	// System.Byte UnityEngine.Color32::a
	uint8_t ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color32_t874517518, ___r_0)); }
	inline uint8_t get_r_0() const { return ___r_0; }
	inline uint8_t* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(uint8_t value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color32_t874517518, ___g_1)); }
	inline uint8_t get_g_1() const { return ___g_1; }
	inline uint8_t* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(uint8_t value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color32_t874517518, ___b_2)); }
	inline uint8_t get_b_2() const { return ___b_2; }
	inline uint8_t* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(uint8_t value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color32_t874517518, ___a_3)); }
	inline uint8_t get_a_3() const { return ___a_3; }
	inline uint8_t* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(uint8_t value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR32_T874517518_H
#ifndef DOUBLE_T4078015681_H
#define DOUBLE_T4078015681_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Double
struct  Double_t4078015681 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_13;

public:
	inline static int32_t get_offset_of_m_value_13() { return static_cast<int32_t>(offsetof(Double_t4078015681, ___m_value_13)); }
	inline double get_m_value_13() const { return ___m_value_13; }
	inline double* get_address_of_m_value_13() { return &___m_value_13; }
	inline void set_m_value_13(double value)
	{
		___m_value_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLE_T4078015681_H
#ifndef ENUMERATOR_T1593300101_H
#define ENUMERATOR_T1593300101_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<System.Object>
struct  Enumerator_t1593300101 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t2058570427 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	RuntimeObject * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t1593300101, ___l_0)); }
	inline List_1_t2058570427 * get_l_0() const { return ___l_0; }
	inline List_1_t2058570427 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t2058570427 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t1593300101, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t1593300101, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t1593300101, ___current_3)); }
	inline RuntimeObject * get_current_3() const { return ___current_3; }
	inline RuntimeObject ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T1593300101_H
#ifndef INT32_T2071877448_H
#define INT32_T2071877448_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2071877448 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2071877448, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2071877448_H
#ifndef ARSIZE_T3911821096_H
#define ARSIZE_T3911821096_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARSize
struct  ARSize_t3911821096 
{
public:
	// System.Double UnityEngine.XR.iOS.ARSize::width
	double ___width_0;
	// System.Double UnityEngine.XR.iOS.ARSize::height
	double ___height_1;

public:
	inline static int32_t get_offset_of_width_0() { return static_cast<int32_t>(offsetof(ARSize_t3911821096, ___width_0)); }
	inline double get_width_0() const { return ___width_0; }
	inline double* get_address_of_width_0() { return &___width_0; }
	inline void set_width_0(double value)
	{
		___width_0 = value;
	}

	inline static int32_t get_offset_of_height_1() { return static_cast<int32_t>(offsetof(ARSize_t3911821096, ___height_1)); }
	inline double get_height_1() const { return ___height_1; }
	inline double* get_address_of_height_1() { return &___height_1; }
	inline void set_height_1(double value)
	{
		___height_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARSIZE_T3911821096_H
#ifndef ARPOINT_T3436811567_H
#define ARPOINT_T3436811567_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARPoint
struct  ARPoint_t3436811567 
{
public:
	// System.Double UnityEngine.XR.iOS.ARPoint::x
	double ___x_0;
	// System.Double UnityEngine.XR.iOS.ARPoint::y
	double ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(ARPoint_t3436811567, ___x_0)); }
	inline double get_x_0() const { return ___x_0; }
	inline double* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(double value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(ARPoint_t3436811567, ___y_1)); }
	inline double get_y_1() const { return ___y_1; }
	inline double* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(double value)
	{
		___y_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARPOINT_T3436811567_H
#ifndef MATRIX4X4_T2933234003_H
#define MATRIX4X4_T2933234003_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t2933234003 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t2933234003_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t2933234003  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t2933234003  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t2933234003  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t2933234003 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t2933234003  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t2933234003  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t2933234003 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t2933234003  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T2933234003_H
#ifndef COLOR_T2020392075_H
#define COLOR_T2020392075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2020392075 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2020392075_H
#ifndef BOOLEAN_T3825574718_H
#define BOOLEAN_T3825574718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t3825574718 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t3825574718, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t3825574718_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t3825574718_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t3825574718_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T3825574718_H
#ifndef RESOLUTION_T3693662728_H
#define RESOLUTION_T3693662728_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Resolution
struct  Resolution_t3693662728 
{
public:
	// System.Int32 UnityEngine.Resolution::m_Width
	int32_t ___m_Width_0;
	// System.Int32 UnityEngine.Resolution::m_Height
	int32_t ___m_Height_1;
	// System.Int32 UnityEngine.Resolution::m_RefreshRate
	int32_t ___m_RefreshRate_2;

public:
	inline static int32_t get_offset_of_m_Width_0() { return static_cast<int32_t>(offsetof(Resolution_t3693662728, ___m_Width_0)); }
	inline int32_t get_m_Width_0() const { return ___m_Width_0; }
	inline int32_t* get_address_of_m_Width_0() { return &___m_Width_0; }
	inline void set_m_Width_0(int32_t value)
	{
		___m_Width_0 = value;
	}

	inline static int32_t get_offset_of_m_Height_1() { return static_cast<int32_t>(offsetof(Resolution_t3693662728, ___m_Height_1)); }
	inline int32_t get_m_Height_1() const { return ___m_Height_1; }
	inline int32_t* get_address_of_m_Height_1() { return &___m_Height_1; }
	inline void set_m_Height_1(int32_t value)
	{
		___m_Height_1 = value;
	}

	inline static int32_t get_offset_of_m_RefreshRate_2() { return static_cast<int32_t>(offsetof(Resolution_t3693662728, ___m_RefreshRate_2)); }
	inline int32_t get_m_RefreshRate_2() const { return ___m_RefreshRate_2; }
	inline int32_t* get_address_of_m_RefreshRate_2() { return &___m_RefreshRate_2; }
	inline void set_m_RefreshRate_2(int32_t value)
	{
		___m_RefreshRate_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESOLUTION_T3693662728_H
#ifndef SINGLE_T2076509932_H
#define SINGLE_T2076509932_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t2076509932 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t2076509932, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T2076509932_H
#ifndef UINT32_T2149682021_H
#define UINT32_T2149682021_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt32
struct  UInt32_t2149682021 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(UInt32_t2149682021, ___m_value_2)); }
	inline uint32_t get_m_value_2() const { return ___m_value_2; }
	inline uint32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(uint32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32_T2149682021_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t1328083999* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t1328083999* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t1328083999** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t1328083999* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef VECTOR3_T2243707580_H
#define VECTOR3_T2243707580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t2243707580 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t2243707580_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t2243707580  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t2243707580  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t2243707580  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t2243707580  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t2243707580  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t2243707580  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t2243707580  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t2243707580  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t2243707580  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t2243707580  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___zeroVector_4)); }
	inline Vector3_t2243707580  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t2243707580 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t2243707580  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___oneVector_5)); }
	inline Vector3_t2243707580  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t2243707580 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t2243707580  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___upVector_6)); }
	inline Vector3_t2243707580  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t2243707580 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t2243707580  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___downVector_7)); }
	inline Vector3_t2243707580  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t2243707580 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t2243707580  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___leftVector_8)); }
	inline Vector3_t2243707580  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t2243707580 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t2243707580  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___rightVector_9)); }
	inline Vector3_t2243707580  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t2243707580 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t2243707580  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___forwardVector_10)); }
	inline Vector3_t2243707580  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t2243707580 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t2243707580  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___backVector_11)); }
	inline Vector3_t2243707580  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t2243707580 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t2243707580  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t2243707580  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t2243707580 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t2243707580  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t2243707580  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t2243707580 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t2243707580  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T2243707580_H
#ifndef QUATERNION_T4030073918_H
#define QUATERNION_T4030073918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t4030073918 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t4030073918_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t4030073918  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t4030073918  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t4030073918 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t4030073918  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T4030073918_H
#ifndef TOUCHTYPE_T2732027771_H
#define TOUCHTYPE_T2732027771_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchType
struct  TouchType_t2732027771 
{
public:
	// System.Int32 UnityEngine.TouchType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchType_t2732027771, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHTYPE_T2732027771_H
#ifndef ARTRACKINGSTATEREASON_T4227173799_H
#define ARTRACKINGSTATEREASON_T4227173799_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARTrackingStateReason
struct  ARTrackingStateReason_t4227173799 
{
public:
	// System.Int32 UnityEngine.XR.iOS.ARTrackingStateReason::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ARTrackingStateReason_t4227173799, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARTRACKINGSTATEREASON_T4227173799_H
#ifndef ARTRACKINGSTATE_T2048880995_H
#define ARTRACKINGSTATE_T2048880995_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARTrackingState
struct  ARTrackingState_t2048880995 
{
public:
	// System.Int32 UnityEngine.XR.iOS.ARTrackingState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ARTrackingState_t2048880995, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARTRACKINGSTATE_T2048880995_H
#ifndef BUILTINRENDERTEXTURETYPE_T195473098_H
#define BUILTINRENDERTEXTURETYPE_T195473098_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.BuiltinRenderTextureType
struct  BuiltinRenderTextureType_t195473098 
{
public:
	// System.Int32 UnityEngine.Rendering.BuiltinRenderTextureType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BuiltinRenderTextureType_t195473098, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILTINRENDERTEXTURETYPE_T195473098_H
#ifndef TEXTUREWRAPMODE_T3683976566_H
#define TEXTUREWRAPMODE_T3683976566_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextureWrapMode
struct  TextureWrapMode_t3683976566 
{
public:
	// System.Int32 UnityEngine.TextureWrapMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextureWrapMode_t3683976566, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREWRAPMODE_T3683976566_H
#ifndef DELEGATE_T3022476291_H
#define DELEGATE_T3022476291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t3022476291  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	IntPtr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	IntPtr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	IntPtr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	IntPtr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1572802995 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___invoke_impl_1)); }
	inline IntPtr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline IntPtr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(IntPtr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_3)); }
	inline IntPtr_t get_method_3() const { return ___method_3; }
	inline IntPtr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(IntPtr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___delegate_trampoline_4)); }
	inline IntPtr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline IntPtr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(IntPtr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_code_5)); }
	inline IntPtr_t get_method_code_5() const { return ___method_code_5; }
	inline IntPtr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(IntPtr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___data_8)); }
	inline DelegateData_t1572802995 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1572802995 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1572802995 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T3022476291_H
#ifndef TEXTUREFORMAT_T1386130234_H
#define TEXTUREFORMAT_T1386130234_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextureFormat
struct  TextureFormat_t1386130234 
{
public:
	// System.Int32 UnityEngine.TextureFormat::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextureFormat_t1386130234, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREFORMAT_T1386130234_H
#ifndef COMMANDBUFFER_T1204166949_H
#define COMMANDBUFFER_T1204166949_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.CommandBuffer
struct  CommandBuffer_t1204166949  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Rendering.CommandBuffer::m_Ptr
	IntPtr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(CommandBuffer_t1204166949, ___m_Ptr_0)); }
	inline IntPtr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline IntPtr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(IntPtr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMMANDBUFFER_T1204166949_H
#ifndef CAMERAEVENT_T2007842675_H
#define CAMERAEVENT_T2007842675_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.CameraEvent
struct  CameraEvent_t2007842675 
{
public:
	// System.Int32 UnityEngine.Rendering.CameraEvent::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CameraEvent_t2007842675, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAEVENT_T2007842675_H
#ifndef RUNTIMEFIELDHANDLE_T2331729674_H
#define RUNTIMEFIELDHANDLE_T2331729674_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeFieldHandle
struct  RuntimeFieldHandle_t2331729674 
{
public:
	// System.IntPtr System.RuntimeFieldHandle::value
	IntPtr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeFieldHandle_t2331729674, ___value_0)); }
	inline IntPtr_t get_value_0() const { return ___value_0; }
	inline IntPtr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(IntPtr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEFIELDHANDLE_T2331729674_H
#ifndef TOUCHPHASE_T2458120420_H
#define TOUCHPHASE_T2458120420_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchPhase
struct  TouchPhase_t2458120420 
{
public:
	// System.Int32 UnityEngine.TouchPhase::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchPhase_t2458120420, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHPHASE_T2458120420_H
#ifndef UNITYARMATRIX4X4_T100931615_H
#define UNITYARMATRIX4X4_T100931615_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARMatrix4x4
struct  UnityARMatrix4x4_t100931615 
{
public:
	// UnityEngine.Vector4 UnityEngine.XR.iOS.UnityARMatrix4x4::column0
	Vector4_t2243707581  ___column0_0;
	// UnityEngine.Vector4 UnityEngine.XR.iOS.UnityARMatrix4x4::column1
	Vector4_t2243707581  ___column1_1;
	// UnityEngine.Vector4 UnityEngine.XR.iOS.UnityARMatrix4x4::column2
	Vector4_t2243707581  ___column2_2;
	// UnityEngine.Vector4 UnityEngine.XR.iOS.UnityARMatrix4x4::column3
	Vector4_t2243707581  ___column3_3;

public:
	inline static int32_t get_offset_of_column0_0() { return static_cast<int32_t>(offsetof(UnityARMatrix4x4_t100931615, ___column0_0)); }
	inline Vector4_t2243707581  get_column0_0() const { return ___column0_0; }
	inline Vector4_t2243707581 * get_address_of_column0_0() { return &___column0_0; }
	inline void set_column0_0(Vector4_t2243707581  value)
	{
		___column0_0 = value;
	}

	inline static int32_t get_offset_of_column1_1() { return static_cast<int32_t>(offsetof(UnityARMatrix4x4_t100931615, ___column1_1)); }
	inline Vector4_t2243707581  get_column1_1() const { return ___column1_1; }
	inline Vector4_t2243707581 * get_address_of_column1_1() { return &___column1_1; }
	inline void set_column1_1(Vector4_t2243707581  value)
	{
		___column1_1 = value;
	}

	inline static int32_t get_offset_of_column2_2() { return static_cast<int32_t>(offsetof(UnityARMatrix4x4_t100931615, ___column2_2)); }
	inline Vector4_t2243707581  get_column2_2() const { return ___column2_2; }
	inline Vector4_t2243707581 * get_address_of_column2_2() { return &___column2_2; }
	inline void set_column2_2(Vector4_t2243707581  value)
	{
		___column2_2 = value;
	}

	inline static int32_t get_offset_of_column3_3() { return static_cast<int32_t>(offsetof(UnityARMatrix4x4_t100931615, ___column3_3)); }
	inline Vector4_t2243707581  get_column3_3() const { return ___column3_3; }
	inline Vector4_t2243707581 * get_address_of_column3_3() { return &___column3_3; }
	inline void set_column3_3(Vector4_t2243707581  value)
	{
		___column3_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARMATRIX4X4_T100931615_H
#ifndef SCREENORIENTATION_T4019489636_H
#define SCREENORIENTATION_T4019489636_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScreenOrientation
struct  ScreenOrientation_t4019489636 
{
public:
	// System.Int32 UnityEngine.ScreenOrientation::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ScreenOrientation_t4019489636, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENORIENTATION_T4019489636_H
#ifndef UNITYARSESSIONRUNOPTION_T3123075684_H
#define UNITYARSESSIONRUNOPTION_T3123075684_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionRunOption
struct  UnityARSessionRunOption_t3123075684 
{
public:
	// System.Int32 UnityEngine.XR.iOS.UnityARSessionRunOption::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UnityARSessionRunOption_t3123075684, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARSESSIONRUNOPTION_T3123075684_H
#ifndef FILTERMODE_T10814199_H
#define FILTERMODE_T10814199_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.FilterMode
struct  FilterMode_t10814199 
{
public:
	// System.Int32 UnityEngine.FilterMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FilterMode_t10814199, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILTERMODE_T10814199_H
#ifndef ARTRACKINGQUALITY_T55960597_H
#define ARTRACKINGQUALITY_T55960597_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARTrackingQuality
struct  ARTrackingQuality_t55960597 
{
public:
	// System.Int64 UnityEngine.XR.iOS.ARTrackingQuality::value__
	int64_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ARTrackingQuality_t55960597, ___value___1)); }
	inline int64_t get_value___1() const { return ___value___1; }
	inline int64_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int64_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARTRACKINGQUALITY_T55960597_H
#ifndef UNITYARPLANEDETECTION_T612575857_H
#define UNITYARPLANEDETECTION_T612575857_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARPlaneDetection
struct  UnityARPlaneDetection_t612575857 
{
public:
	// System.Int32 UnityEngine.XR.iOS.UnityARPlaneDetection::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UnityARPlaneDetection_t612575857, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARPLANEDETECTION_T612575857_H
#ifndef ARHITTESTRESULTTYPE_T3616749745_H
#define ARHITTESTRESULTTYPE_T3616749745_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARHitTestResultType
struct  ARHitTestResultType_t3616749745 
{
public:
	// System.Int64 UnityEngine.XR.iOS.ARHitTestResultType::value__
	int64_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ARHitTestResultType_t3616749745, ___value___1)); }
	inline int64_t get_value___1() const { return ___value___1; }
	inline int64_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int64_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARHITTESTRESULTTYPE_T3616749745_H
#ifndef UNITYARALIGNMENT_T2379988631_H
#define UNITYARALIGNMENT_T2379988631_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARAlignment
struct  UnityARAlignment_t2379988631 
{
public:
	// System.Int32 UnityEngine.XR.iOS.UnityARAlignment::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UnityARAlignment_t2379988631, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARALIGNMENT_T2379988631_H
#ifndef PARTICLE_T250075699_H
#define PARTICLE_T250075699_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystem/Particle
struct  Particle_t250075699 
{
public:
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_Position
	Vector3_t2243707580  ___m_Position_0;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_Velocity
	Vector3_t2243707580  ___m_Velocity_1;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_AnimatedVelocity
	Vector3_t2243707580  ___m_AnimatedVelocity_2;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_InitialVelocity
	Vector3_t2243707580  ___m_InitialVelocity_3;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_AxisOfRotation
	Vector3_t2243707580  ___m_AxisOfRotation_4;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_Rotation
	Vector3_t2243707580  ___m_Rotation_5;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_AngularVelocity
	Vector3_t2243707580  ___m_AngularVelocity_6;
	// UnityEngine.Vector3 UnityEngine.ParticleSystem/Particle::m_StartSize
	Vector3_t2243707580  ___m_StartSize_7;
	// UnityEngine.Color32 UnityEngine.ParticleSystem/Particle::m_StartColor
	Color32_t874517518  ___m_StartColor_8;
	// System.UInt32 UnityEngine.ParticleSystem/Particle::m_RandomSeed
	uint32_t ___m_RandomSeed_9;
	// System.Single UnityEngine.ParticleSystem/Particle::m_Lifetime
	float ___m_Lifetime_10;
	// System.Single UnityEngine.ParticleSystem/Particle::m_StartLifetime
	float ___m_StartLifetime_11;
	// System.Single UnityEngine.ParticleSystem/Particle::m_EmitAccumulator0
	float ___m_EmitAccumulator0_12;
	// System.Single UnityEngine.ParticleSystem/Particle::m_EmitAccumulator1
	float ___m_EmitAccumulator1_13;

public:
	inline static int32_t get_offset_of_m_Position_0() { return static_cast<int32_t>(offsetof(Particle_t250075699, ___m_Position_0)); }
	inline Vector3_t2243707580  get_m_Position_0() const { return ___m_Position_0; }
	inline Vector3_t2243707580 * get_address_of_m_Position_0() { return &___m_Position_0; }
	inline void set_m_Position_0(Vector3_t2243707580  value)
	{
		___m_Position_0 = value;
	}

	inline static int32_t get_offset_of_m_Velocity_1() { return static_cast<int32_t>(offsetof(Particle_t250075699, ___m_Velocity_1)); }
	inline Vector3_t2243707580  get_m_Velocity_1() const { return ___m_Velocity_1; }
	inline Vector3_t2243707580 * get_address_of_m_Velocity_1() { return &___m_Velocity_1; }
	inline void set_m_Velocity_1(Vector3_t2243707580  value)
	{
		___m_Velocity_1 = value;
	}

	inline static int32_t get_offset_of_m_AnimatedVelocity_2() { return static_cast<int32_t>(offsetof(Particle_t250075699, ___m_AnimatedVelocity_2)); }
	inline Vector3_t2243707580  get_m_AnimatedVelocity_2() const { return ___m_AnimatedVelocity_2; }
	inline Vector3_t2243707580 * get_address_of_m_AnimatedVelocity_2() { return &___m_AnimatedVelocity_2; }
	inline void set_m_AnimatedVelocity_2(Vector3_t2243707580  value)
	{
		___m_AnimatedVelocity_2 = value;
	}

	inline static int32_t get_offset_of_m_InitialVelocity_3() { return static_cast<int32_t>(offsetof(Particle_t250075699, ___m_InitialVelocity_3)); }
	inline Vector3_t2243707580  get_m_InitialVelocity_3() const { return ___m_InitialVelocity_3; }
	inline Vector3_t2243707580 * get_address_of_m_InitialVelocity_3() { return &___m_InitialVelocity_3; }
	inline void set_m_InitialVelocity_3(Vector3_t2243707580  value)
	{
		___m_InitialVelocity_3 = value;
	}

	inline static int32_t get_offset_of_m_AxisOfRotation_4() { return static_cast<int32_t>(offsetof(Particle_t250075699, ___m_AxisOfRotation_4)); }
	inline Vector3_t2243707580  get_m_AxisOfRotation_4() const { return ___m_AxisOfRotation_4; }
	inline Vector3_t2243707580 * get_address_of_m_AxisOfRotation_4() { return &___m_AxisOfRotation_4; }
	inline void set_m_AxisOfRotation_4(Vector3_t2243707580  value)
	{
		___m_AxisOfRotation_4 = value;
	}

	inline static int32_t get_offset_of_m_Rotation_5() { return static_cast<int32_t>(offsetof(Particle_t250075699, ___m_Rotation_5)); }
	inline Vector3_t2243707580  get_m_Rotation_5() const { return ___m_Rotation_5; }
	inline Vector3_t2243707580 * get_address_of_m_Rotation_5() { return &___m_Rotation_5; }
	inline void set_m_Rotation_5(Vector3_t2243707580  value)
	{
		___m_Rotation_5 = value;
	}

	inline static int32_t get_offset_of_m_AngularVelocity_6() { return static_cast<int32_t>(offsetof(Particle_t250075699, ___m_AngularVelocity_6)); }
	inline Vector3_t2243707580  get_m_AngularVelocity_6() const { return ___m_AngularVelocity_6; }
	inline Vector3_t2243707580 * get_address_of_m_AngularVelocity_6() { return &___m_AngularVelocity_6; }
	inline void set_m_AngularVelocity_6(Vector3_t2243707580  value)
	{
		___m_AngularVelocity_6 = value;
	}

	inline static int32_t get_offset_of_m_StartSize_7() { return static_cast<int32_t>(offsetof(Particle_t250075699, ___m_StartSize_7)); }
	inline Vector3_t2243707580  get_m_StartSize_7() const { return ___m_StartSize_7; }
	inline Vector3_t2243707580 * get_address_of_m_StartSize_7() { return &___m_StartSize_7; }
	inline void set_m_StartSize_7(Vector3_t2243707580  value)
	{
		___m_StartSize_7 = value;
	}

	inline static int32_t get_offset_of_m_StartColor_8() { return static_cast<int32_t>(offsetof(Particle_t250075699, ___m_StartColor_8)); }
	inline Color32_t874517518  get_m_StartColor_8() const { return ___m_StartColor_8; }
	inline Color32_t874517518 * get_address_of_m_StartColor_8() { return &___m_StartColor_8; }
	inline void set_m_StartColor_8(Color32_t874517518  value)
	{
		___m_StartColor_8 = value;
	}

	inline static int32_t get_offset_of_m_RandomSeed_9() { return static_cast<int32_t>(offsetof(Particle_t250075699, ___m_RandomSeed_9)); }
	inline uint32_t get_m_RandomSeed_9() const { return ___m_RandomSeed_9; }
	inline uint32_t* get_address_of_m_RandomSeed_9() { return &___m_RandomSeed_9; }
	inline void set_m_RandomSeed_9(uint32_t value)
	{
		___m_RandomSeed_9 = value;
	}

	inline static int32_t get_offset_of_m_Lifetime_10() { return static_cast<int32_t>(offsetof(Particle_t250075699, ___m_Lifetime_10)); }
	inline float get_m_Lifetime_10() const { return ___m_Lifetime_10; }
	inline float* get_address_of_m_Lifetime_10() { return &___m_Lifetime_10; }
	inline void set_m_Lifetime_10(float value)
	{
		___m_Lifetime_10 = value;
	}

	inline static int32_t get_offset_of_m_StartLifetime_11() { return static_cast<int32_t>(offsetof(Particle_t250075699, ___m_StartLifetime_11)); }
	inline float get_m_StartLifetime_11() const { return ___m_StartLifetime_11; }
	inline float* get_address_of_m_StartLifetime_11() { return &___m_StartLifetime_11; }
	inline void set_m_StartLifetime_11(float value)
	{
		___m_StartLifetime_11 = value;
	}

	inline static int32_t get_offset_of_m_EmitAccumulator0_12() { return static_cast<int32_t>(offsetof(Particle_t250075699, ___m_EmitAccumulator0_12)); }
	inline float get_m_EmitAccumulator0_12() const { return ___m_EmitAccumulator0_12; }
	inline float* get_address_of_m_EmitAccumulator0_12() { return &___m_EmitAccumulator0_12; }
	inline void set_m_EmitAccumulator0_12(float value)
	{
		___m_EmitAccumulator0_12 = value;
	}

	inline static int32_t get_offset_of_m_EmitAccumulator1_13() { return static_cast<int32_t>(offsetof(Particle_t250075699, ___m_EmitAccumulator1_13)); }
	inline float get_m_EmitAccumulator1_13() const { return ___m_EmitAccumulator1_13; }
	inline float* get_address_of_m_EmitAccumulator1_13() { return &___m_EmitAccumulator1_13; }
	inline void set_m_EmitAccumulator1_13(float value)
	{
		___m_EmitAccumulator1_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLE_T250075699_H
#ifndef ARANCHOR_T1161832412_H
#define ARANCHOR_T1161832412_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARAnchor
struct  ARAnchor_t1161832412 
{
public:
	// System.String UnityEngine.XR.iOS.ARAnchor::identifier
	String_t* ___identifier_0;
	// UnityEngine.Matrix4x4 UnityEngine.XR.iOS.ARAnchor::transform
	Matrix4x4_t2933234003  ___transform_1;

public:
	inline static int32_t get_offset_of_identifier_0() { return static_cast<int32_t>(offsetof(ARAnchor_t1161832412, ___identifier_0)); }
	inline String_t* get_identifier_0() const { return ___identifier_0; }
	inline String_t** get_address_of_identifier_0() { return &___identifier_0; }
	inline void set_identifier_0(String_t* value)
	{
		___identifier_0 = value;
		Il2CppCodeGenWriteBarrier((&___identifier_0), value);
	}

	inline static int32_t get_offset_of_transform_1() { return static_cast<int32_t>(offsetof(ARAnchor_t1161832412, ___transform_1)); }
	inline Matrix4x4_t2933234003  get_transform_1() const { return ___transform_1; }
	inline Matrix4x4_t2933234003 * get_address_of_transform_1() { return &___transform_1; }
	inline void set_transform_1(Matrix4x4_t2933234003  value)
	{
		___transform_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.XR.iOS.ARAnchor
struct ARAnchor_t1161832412_marshaled_pinvoke
{
	char* ___identifier_0;
	Matrix4x4_t2933234003  ___transform_1;
};
// Native definition for COM marshalling of UnityEngine.XR.iOS.ARAnchor
struct ARAnchor_t1161832412_marshaled_com
{
	Il2CppChar* ___identifier_0;
	Matrix4x4_t2933234003  ___transform_1;
};
#endif // ARANCHOR_T1161832412_H
#ifndef ARERRORCODE_T2887756272_H
#define ARERRORCODE_T2887756272_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARErrorCode
struct  ARErrorCode_t2887756272 
{
public:
	// System.Int64 UnityEngine.XR.iOS.ARErrorCode::value__
	int64_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ARErrorCode_t2887756272, ___value___1)); }
	inline int64_t get_value___1() const { return ___value___1; }
	inline int64_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int64_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARERRORCODE_T2887756272_H
#ifndef OBJECT_T1021602117_H
#define OBJECT_T1021602117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1021602117  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	IntPtr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1021602117, ___m_CachedPtr_0)); }
	inline IntPtr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline IntPtr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(IntPtr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1021602117_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1021602117_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1021602117_H
#ifndef ARTEXTUREHANDLES_T3764914833_H
#define ARTEXTUREHANDLES_T3764914833_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARTextureHandles
struct  ARTextureHandles_t3764914833 
{
public:
	// System.IntPtr UnityEngine.XR.iOS.ARTextureHandles::textureY
	IntPtr_t ___textureY_0;
	// System.IntPtr UnityEngine.XR.iOS.ARTextureHandles::textureCbCr
	IntPtr_t ___textureCbCr_1;

public:
	inline static int32_t get_offset_of_textureY_0() { return static_cast<int32_t>(offsetof(ARTextureHandles_t3764914833, ___textureY_0)); }
	inline IntPtr_t get_textureY_0() const { return ___textureY_0; }
	inline IntPtr_t* get_address_of_textureY_0() { return &___textureY_0; }
	inline void set_textureY_0(IntPtr_t value)
	{
		___textureY_0 = value;
	}

	inline static int32_t get_offset_of_textureCbCr_1() { return static_cast<int32_t>(offsetof(ARTextureHandles_t3764914833, ___textureCbCr_1)); }
	inline IntPtr_t get_textureCbCr_1() const { return ___textureCbCr_1; }
	inline IntPtr_t* get_address_of_textureCbCr_1() { return &___textureCbCr_1; }
	inline void set_textureCbCr_1(IntPtr_t value)
	{
		___textureCbCr_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARTEXTUREHANDLES_T3764914833_H
#ifndef ARRECT_T3555590363_H
#define ARRECT_T3555590363_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARRect
struct  ARRect_t3555590363 
{
public:
	// UnityEngine.XR.iOS.ARPoint UnityEngine.XR.iOS.ARRect::origin
	ARPoint_t3436811567  ___origin_0;
	// UnityEngine.XR.iOS.ARSize UnityEngine.XR.iOS.ARRect::size
	ARSize_t3911821096  ___size_1;

public:
	inline static int32_t get_offset_of_origin_0() { return static_cast<int32_t>(offsetof(ARRect_t3555590363, ___origin_0)); }
	inline ARPoint_t3436811567  get_origin_0() const { return ___origin_0; }
	inline ARPoint_t3436811567 * get_address_of_origin_0() { return &___origin_0; }
	inline void set_origin_0(ARPoint_t3436811567  value)
	{
		___origin_0 = value;
	}

	inline static int32_t get_offset_of_size_1() { return static_cast<int32_t>(offsetof(ARRect_t3555590363, ___size_1)); }
	inline ARSize_t3911821096  get_size_1() const { return ___size_1; }
	inline ARSize_t3911821096 * get_address_of_size_1() { return &___size_1; }
	inline void set_size_1(ARSize_t3911821096  value)
	{
		___size_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRECT_T3555590363_H
#ifndef ARPLANEANCHORALIGNMENT_T2379298071_H
#define ARPLANEANCHORALIGNMENT_T2379298071_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARPlaneAnchorAlignment
struct  ARPlaneAnchorAlignment_t2379298071 
{
public:
	// System.Int64 UnityEngine.XR.iOS.ARPlaneAnchorAlignment::value__
	int64_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ARPlaneAnchorAlignment_t2379298071, ___value___1)); }
	inline int64_t get_value___1() const { return ___value___1; }
	inline int64_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int64_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARPLANEANCHORALIGNMENT_T2379298071_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1486305143_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1486305143_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t1486305143  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=24 <PrivateImplementationDetails>::$field-8E7629AD5AF686202B8CB7C014505C432FFE31E6
	U24ArrayTypeU3D24_t762068664  ___U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0;

public:
	inline static int32_t get_offset_of_U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields, ___U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0)); }
	inline U24ArrayTypeU3D24_t762068664  get_U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0() const { return ___U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0; }
	inline U24ArrayTypeU3D24_t762068664 * get_address_of_U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0() { return &___U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0; }
	inline void set_U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0(U24ArrayTypeU3D24_t762068664  value)
	{
		___U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1486305143_H
#ifndef UNITYARCAMERA_T4198559457_H
#define UNITYARCAMERA_T4198559457_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARCamera
struct  UnityARCamera_t4198559457 
{
public:
	// UnityEngine.XR.iOS.UnityARMatrix4x4 UnityEngine.XR.iOS.UnityARCamera::worldTransform
	UnityARMatrix4x4_t100931615  ___worldTransform_0;
	// UnityEngine.XR.iOS.UnityARMatrix4x4 UnityEngine.XR.iOS.UnityARCamera::projectionMatrix
	UnityARMatrix4x4_t100931615  ___projectionMatrix_1;
	// UnityEngine.XR.iOS.ARTrackingState UnityEngine.XR.iOS.UnityARCamera::trackingState
	int32_t ___trackingState_2;
	// UnityEngine.XR.iOS.ARTrackingStateReason UnityEngine.XR.iOS.UnityARCamera::trackingReason
	int32_t ___trackingReason_3;
	// UnityEngine.Vector3[] UnityEngine.XR.iOS.UnityARCamera::pointCloudData
	Vector3U5BU5D_t1172311765* ___pointCloudData_4;

public:
	inline static int32_t get_offset_of_worldTransform_0() { return static_cast<int32_t>(offsetof(UnityARCamera_t4198559457, ___worldTransform_0)); }
	inline UnityARMatrix4x4_t100931615  get_worldTransform_0() const { return ___worldTransform_0; }
	inline UnityARMatrix4x4_t100931615 * get_address_of_worldTransform_0() { return &___worldTransform_0; }
	inline void set_worldTransform_0(UnityARMatrix4x4_t100931615  value)
	{
		___worldTransform_0 = value;
	}

	inline static int32_t get_offset_of_projectionMatrix_1() { return static_cast<int32_t>(offsetof(UnityARCamera_t4198559457, ___projectionMatrix_1)); }
	inline UnityARMatrix4x4_t100931615  get_projectionMatrix_1() const { return ___projectionMatrix_1; }
	inline UnityARMatrix4x4_t100931615 * get_address_of_projectionMatrix_1() { return &___projectionMatrix_1; }
	inline void set_projectionMatrix_1(UnityARMatrix4x4_t100931615  value)
	{
		___projectionMatrix_1 = value;
	}

	inline static int32_t get_offset_of_trackingState_2() { return static_cast<int32_t>(offsetof(UnityARCamera_t4198559457, ___trackingState_2)); }
	inline int32_t get_trackingState_2() const { return ___trackingState_2; }
	inline int32_t* get_address_of_trackingState_2() { return &___trackingState_2; }
	inline void set_trackingState_2(int32_t value)
	{
		___trackingState_2 = value;
	}

	inline static int32_t get_offset_of_trackingReason_3() { return static_cast<int32_t>(offsetof(UnityARCamera_t4198559457, ___trackingReason_3)); }
	inline int32_t get_trackingReason_3() const { return ___trackingReason_3; }
	inline int32_t* get_address_of_trackingReason_3() { return &___trackingReason_3; }
	inline void set_trackingReason_3(int32_t value)
	{
		___trackingReason_3 = value;
	}

	inline static int32_t get_offset_of_pointCloudData_4() { return static_cast<int32_t>(offsetof(UnityARCamera_t4198559457, ___pointCloudData_4)); }
	inline Vector3U5BU5D_t1172311765* get_pointCloudData_4() const { return ___pointCloudData_4; }
	inline Vector3U5BU5D_t1172311765** get_address_of_pointCloudData_4() { return &___pointCloudData_4; }
	inline void set_pointCloudData_4(Vector3U5BU5D_t1172311765* value)
	{
		___pointCloudData_4 = value;
		Il2CppCodeGenWriteBarrier((&___pointCloudData_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.XR.iOS.UnityARCamera
struct UnityARCamera_t4198559457_marshaled_pinvoke
{
	UnityARMatrix4x4_t100931615  ___worldTransform_0;
	UnityARMatrix4x4_t100931615  ___projectionMatrix_1;
	int32_t ___trackingState_2;
	int32_t ___trackingReason_3;
	Vector3_t2243707580 * ___pointCloudData_4;
};
// Native definition for COM marshalling of UnityEngine.XR.iOS.UnityARCamera
struct UnityARCamera_t4198559457_marshaled_com
{
	UnityARMatrix4x4_t100931615  ___worldTransform_0;
	UnityARMatrix4x4_t100931615  ___projectionMatrix_1;
	int32_t ___trackingState_2;
	int32_t ___trackingReason_3;
	Vector3_t2243707580 * ___pointCloudData_4;
};
#endif // UNITYARCAMERA_T4198559457_H
#ifndef INTERNAL_UNITYARCAMERA_T2580192745_H
#define INTERNAL_UNITYARCAMERA_T2580192745_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.internal_UnityARCamera
struct  internal_UnityARCamera_t2580192745 
{
public:
	// UnityEngine.XR.iOS.UnityARMatrix4x4 UnityEngine.XR.iOS.internal_UnityARCamera::worldTransform
	UnityARMatrix4x4_t100931615  ___worldTransform_0;
	// UnityEngine.XR.iOS.UnityARMatrix4x4 UnityEngine.XR.iOS.internal_UnityARCamera::projectionMatrix
	UnityARMatrix4x4_t100931615  ___projectionMatrix_1;
	// UnityEngine.XR.iOS.ARTrackingState UnityEngine.XR.iOS.internal_UnityARCamera::trackingState
	int32_t ___trackingState_2;
	// UnityEngine.XR.iOS.ARTrackingStateReason UnityEngine.XR.iOS.internal_UnityARCamera::trackingReason
	int32_t ___trackingReason_3;
	// System.UInt32 UnityEngine.XR.iOS.internal_UnityARCamera::getPointCloudData
	uint32_t ___getPointCloudData_4;

public:
	inline static int32_t get_offset_of_worldTransform_0() { return static_cast<int32_t>(offsetof(internal_UnityARCamera_t2580192745, ___worldTransform_0)); }
	inline UnityARMatrix4x4_t100931615  get_worldTransform_0() const { return ___worldTransform_0; }
	inline UnityARMatrix4x4_t100931615 * get_address_of_worldTransform_0() { return &___worldTransform_0; }
	inline void set_worldTransform_0(UnityARMatrix4x4_t100931615  value)
	{
		___worldTransform_0 = value;
	}

	inline static int32_t get_offset_of_projectionMatrix_1() { return static_cast<int32_t>(offsetof(internal_UnityARCamera_t2580192745, ___projectionMatrix_1)); }
	inline UnityARMatrix4x4_t100931615  get_projectionMatrix_1() const { return ___projectionMatrix_1; }
	inline UnityARMatrix4x4_t100931615 * get_address_of_projectionMatrix_1() { return &___projectionMatrix_1; }
	inline void set_projectionMatrix_1(UnityARMatrix4x4_t100931615  value)
	{
		___projectionMatrix_1 = value;
	}

	inline static int32_t get_offset_of_trackingState_2() { return static_cast<int32_t>(offsetof(internal_UnityARCamera_t2580192745, ___trackingState_2)); }
	inline int32_t get_trackingState_2() const { return ___trackingState_2; }
	inline int32_t* get_address_of_trackingState_2() { return &___trackingState_2; }
	inline void set_trackingState_2(int32_t value)
	{
		___trackingState_2 = value;
	}

	inline static int32_t get_offset_of_trackingReason_3() { return static_cast<int32_t>(offsetof(internal_UnityARCamera_t2580192745, ___trackingReason_3)); }
	inline int32_t get_trackingReason_3() const { return ___trackingReason_3; }
	inline int32_t* get_address_of_trackingReason_3() { return &___trackingReason_3; }
	inline void set_trackingReason_3(int32_t value)
	{
		___trackingReason_3 = value;
	}

	inline static int32_t get_offset_of_getPointCloudData_4() { return static_cast<int32_t>(offsetof(internal_UnityARCamera_t2580192745, ___getPointCloudData_4)); }
	inline uint32_t get_getPointCloudData_4() const { return ___getPointCloudData_4; }
	inline uint32_t* get_address_of_getPointCloudData_4() { return &___getPointCloudData_4; }
	inline void set_getPointCloudData_4(uint32_t value)
	{
		___getPointCloudData_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNAL_UNITYARCAMERA_T2580192745_H
#ifndef UNITYARHITTESTRESULT_T4129824344_H
#define UNITYARHITTESTRESULT_T4129824344_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARHitTestResult
struct  UnityARHitTestResult_t4129824344 
{
public:
	// UnityEngine.XR.iOS.ARHitTestResultType UnityEngine.XR.iOS.UnityARHitTestResult::type
	int64_t ___type_0;
	// System.Double UnityEngine.XR.iOS.UnityARHitTestResult::distance
	double ___distance_1;
	// UnityEngine.Matrix4x4 UnityEngine.XR.iOS.UnityARHitTestResult::localTransform
	Matrix4x4_t2933234003  ___localTransform_2;
	// UnityEngine.Matrix4x4 UnityEngine.XR.iOS.UnityARHitTestResult::worldTransform
	Matrix4x4_t2933234003  ___worldTransform_3;
	// System.IntPtr UnityEngine.XR.iOS.UnityARHitTestResult::anchor
	IntPtr_t ___anchor_4;
	// System.Boolean UnityEngine.XR.iOS.UnityARHitTestResult::isValid
	bool ___isValid_5;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(UnityARHitTestResult_t4129824344, ___type_0)); }
	inline int64_t get_type_0() const { return ___type_0; }
	inline int64_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int64_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_distance_1() { return static_cast<int32_t>(offsetof(UnityARHitTestResult_t4129824344, ___distance_1)); }
	inline double get_distance_1() const { return ___distance_1; }
	inline double* get_address_of_distance_1() { return &___distance_1; }
	inline void set_distance_1(double value)
	{
		___distance_1 = value;
	}

	inline static int32_t get_offset_of_localTransform_2() { return static_cast<int32_t>(offsetof(UnityARHitTestResult_t4129824344, ___localTransform_2)); }
	inline Matrix4x4_t2933234003  get_localTransform_2() const { return ___localTransform_2; }
	inline Matrix4x4_t2933234003 * get_address_of_localTransform_2() { return &___localTransform_2; }
	inline void set_localTransform_2(Matrix4x4_t2933234003  value)
	{
		___localTransform_2 = value;
	}

	inline static int32_t get_offset_of_worldTransform_3() { return static_cast<int32_t>(offsetof(UnityARHitTestResult_t4129824344, ___worldTransform_3)); }
	inline Matrix4x4_t2933234003  get_worldTransform_3() const { return ___worldTransform_3; }
	inline Matrix4x4_t2933234003 * get_address_of_worldTransform_3() { return &___worldTransform_3; }
	inline void set_worldTransform_3(Matrix4x4_t2933234003  value)
	{
		___worldTransform_3 = value;
	}

	inline static int32_t get_offset_of_anchor_4() { return static_cast<int32_t>(offsetof(UnityARHitTestResult_t4129824344, ___anchor_4)); }
	inline IntPtr_t get_anchor_4() const { return ___anchor_4; }
	inline IntPtr_t* get_address_of_anchor_4() { return &___anchor_4; }
	inline void set_anchor_4(IntPtr_t value)
	{
		___anchor_4 = value;
	}

	inline static int32_t get_offset_of_isValid_5() { return static_cast<int32_t>(offsetof(UnityARHitTestResult_t4129824344, ___isValid_5)); }
	inline bool get_isValid_5() const { return ___isValid_5; }
	inline bool* get_address_of_isValid_5() { return &___isValid_5; }
	inline void set_isValid_5(bool value)
	{
		___isValid_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.XR.iOS.UnityARHitTestResult
struct UnityARHitTestResult_t4129824344_marshaled_pinvoke
{
	int64_t ___type_0;
	double ___distance_1;
	Matrix4x4_t2933234003  ___localTransform_2;
	Matrix4x4_t2933234003  ___worldTransform_3;
	intptr_t ___anchor_4;
	int32_t ___isValid_5;
};
// Native definition for COM marshalling of UnityEngine.XR.iOS.UnityARHitTestResult
struct UnityARHitTestResult_t4129824344_marshaled_com
{
	int64_t ___type_0;
	double ___distance_1;
	Matrix4x4_t2933234003  ___localTransform_2;
	Matrix4x4_t2933234003  ___worldTransform_3;
	intptr_t ___anchor_4;
	int32_t ___isValid_5;
};
#endif // UNITYARHITTESTRESULT_T4129824344_H
#ifndef COMPONENT_T3819376471_H
#define COMPONENT_T3819376471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t3819376471  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T3819376471_H
#ifndef TEXTURE_T2243626319_H
#define TEXTURE_T2243626319_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture
struct  Texture_t2243626319  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE_T2243626319_H
#ifndef GAMEOBJECT_T1756533147_H
#define GAMEOBJECT_T1756533147_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_t1756533147  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_T1756533147_H
#ifndef ARKITSESSIONCONFIGURATION_T318899795_H
#define ARKITSESSIONCONFIGURATION_T318899795_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARKitSessionConfiguration
struct  ARKitSessionConfiguration_t318899795 
{
public:
	// UnityEngine.XR.iOS.UnityARAlignment UnityEngine.XR.iOS.ARKitSessionConfiguration::alignment
	int32_t ___alignment_0;
	// System.Boolean UnityEngine.XR.iOS.ARKitSessionConfiguration::getPointCloudData
	bool ___getPointCloudData_1;
	// System.Boolean UnityEngine.XR.iOS.ARKitSessionConfiguration::enableLightEstimation
	bool ___enableLightEstimation_2;

public:
	inline static int32_t get_offset_of_alignment_0() { return static_cast<int32_t>(offsetof(ARKitSessionConfiguration_t318899795, ___alignment_0)); }
	inline int32_t get_alignment_0() const { return ___alignment_0; }
	inline int32_t* get_address_of_alignment_0() { return &___alignment_0; }
	inline void set_alignment_0(int32_t value)
	{
		___alignment_0 = value;
	}

	inline static int32_t get_offset_of_getPointCloudData_1() { return static_cast<int32_t>(offsetof(ARKitSessionConfiguration_t318899795, ___getPointCloudData_1)); }
	inline bool get_getPointCloudData_1() const { return ___getPointCloudData_1; }
	inline bool* get_address_of_getPointCloudData_1() { return &___getPointCloudData_1; }
	inline void set_getPointCloudData_1(bool value)
	{
		___getPointCloudData_1 = value;
	}

	inline static int32_t get_offset_of_enableLightEstimation_2() { return static_cast<int32_t>(offsetof(ARKitSessionConfiguration_t318899795, ___enableLightEstimation_2)); }
	inline bool get_enableLightEstimation_2() const { return ___enableLightEstimation_2; }
	inline bool* get_address_of_enableLightEstimation_2() { return &___enableLightEstimation_2; }
	inline void set_enableLightEstimation_2(bool value)
	{
		___enableLightEstimation_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.XR.iOS.ARKitSessionConfiguration
struct ARKitSessionConfiguration_t318899795_marshaled_pinvoke
{
	int32_t ___alignment_0;
	int32_t ___getPointCloudData_1;
	int32_t ___enableLightEstimation_2;
};
// Native definition for COM marshalling of UnityEngine.XR.iOS.ARKitSessionConfiguration
struct ARKitSessionConfiguration_t318899795_marshaled_com
{
	int32_t ___alignment_0;
	int32_t ___getPointCloudData_1;
	int32_t ___enableLightEstimation_2;
};
#endif // ARKITSESSIONCONFIGURATION_T318899795_H
#ifndef RENDERTARGETIDENTIFIER_T772440638_H
#define RENDERTARGETIDENTIFIER_T772440638_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.RenderTargetIdentifier
struct  RenderTargetIdentifier_t772440638 
{
public:
	// UnityEngine.Rendering.BuiltinRenderTextureType UnityEngine.Rendering.RenderTargetIdentifier::m_Type
	int32_t ___m_Type_0;
	// System.Int32 UnityEngine.Rendering.RenderTargetIdentifier::m_NameID
	int32_t ___m_NameID_1;
	// System.Int32 UnityEngine.Rendering.RenderTargetIdentifier::m_InstanceID
	int32_t ___m_InstanceID_2;

public:
	inline static int32_t get_offset_of_m_Type_0() { return static_cast<int32_t>(offsetof(RenderTargetIdentifier_t772440638, ___m_Type_0)); }
	inline int32_t get_m_Type_0() const { return ___m_Type_0; }
	inline int32_t* get_address_of_m_Type_0() { return &___m_Type_0; }
	inline void set_m_Type_0(int32_t value)
	{
		___m_Type_0 = value;
	}

	inline static int32_t get_offset_of_m_NameID_1() { return static_cast<int32_t>(offsetof(RenderTargetIdentifier_t772440638, ___m_NameID_1)); }
	inline int32_t get_m_NameID_1() const { return ___m_NameID_1; }
	inline int32_t* get_address_of_m_NameID_1() { return &___m_NameID_1; }
	inline void set_m_NameID_1(int32_t value)
	{
		___m_NameID_1 = value;
	}

	inline static int32_t get_offset_of_m_InstanceID_2() { return static_cast<int32_t>(offsetof(RenderTargetIdentifier_t772440638, ___m_InstanceID_2)); }
	inline int32_t get_m_InstanceID_2() const { return ___m_InstanceID_2; }
	inline int32_t* get_address_of_m_InstanceID_2() { return &___m_InstanceID_2; }
	inline void set_m_InstanceID_2(int32_t value)
	{
		___m_InstanceID_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERTARGETIDENTIFIER_T772440638_H
#ifndef MATERIAL_T193706927_H
#define MATERIAL_T193706927_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Material
struct  Material_t193706927  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIAL_T193706927_H
#ifndef MULTICASTDELEGATE_T3201952435_H
#define MULTICASTDELEGATE_T3201952435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t3201952435  : public Delegate_t3022476291
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t3201952435 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t3201952435 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___prev_9)); }
	inline MulticastDelegate_t3201952435 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t3201952435 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t3201952435 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___kpm_next_10)); }
	inline MulticastDelegate_t3201952435 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t3201952435 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t3201952435 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T3201952435_H
#ifndef ARKITWORLDTACKINGSESSIONCONFIGURATION_T1821734930_H
#define ARKITWORLDTACKINGSESSIONCONFIGURATION_T1821734930_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARKitWorldTackingSessionConfiguration
struct  ARKitWorldTackingSessionConfiguration_t1821734930 
{
public:
	// UnityEngine.XR.iOS.UnityARAlignment UnityEngine.XR.iOS.ARKitWorldTackingSessionConfiguration::alignment
	int32_t ___alignment_0;
	// UnityEngine.XR.iOS.UnityARPlaneDetection UnityEngine.XR.iOS.ARKitWorldTackingSessionConfiguration::planeDetection
	int32_t ___planeDetection_1;
	// System.Boolean UnityEngine.XR.iOS.ARKitWorldTackingSessionConfiguration::getPointCloudData
	bool ___getPointCloudData_2;
	// System.Boolean UnityEngine.XR.iOS.ARKitWorldTackingSessionConfiguration::enableLightEstimation
	bool ___enableLightEstimation_3;

public:
	inline static int32_t get_offset_of_alignment_0() { return static_cast<int32_t>(offsetof(ARKitWorldTackingSessionConfiguration_t1821734930, ___alignment_0)); }
	inline int32_t get_alignment_0() const { return ___alignment_0; }
	inline int32_t* get_address_of_alignment_0() { return &___alignment_0; }
	inline void set_alignment_0(int32_t value)
	{
		___alignment_0 = value;
	}

	inline static int32_t get_offset_of_planeDetection_1() { return static_cast<int32_t>(offsetof(ARKitWorldTackingSessionConfiguration_t1821734930, ___planeDetection_1)); }
	inline int32_t get_planeDetection_1() const { return ___planeDetection_1; }
	inline int32_t* get_address_of_planeDetection_1() { return &___planeDetection_1; }
	inline void set_planeDetection_1(int32_t value)
	{
		___planeDetection_1 = value;
	}

	inline static int32_t get_offset_of_getPointCloudData_2() { return static_cast<int32_t>(offsetof(ARKitWorldTackingSessionConfiguration_t1821734930, ___getPointCloudData_2)); }
	inline bool get_getPointCloudData_2() const { return ___getPointCloudData_2; }
	inline bool* get_address_of_getPointCloudData_2() { return &___getPointCloudData_2; }
	inline void set_getPointCloudData_2(bool value)
	{
		___getPointCloudData_2 = value;
	}

	inline static int32_t get_offset_of_enableLightEstimation_3() { return static_cast<int32_t>(offsetof(ARKitWorldTackingSessionConfiguration_t1821734930, ___enableLightEstimation_3)); }
	inline bool get_enableLightEstimation_3() const { return ___enableLightEstimation_3; }
	inline bool* get_address_of_enableLightEstimation_3() { return &___enableLightEstimation_3; }
	inline void set_enableLightEstimation_3(bool value)
	{
		___enableLightEstimation_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.XR.iOS.ARKitWorldTackingSessionConfiguration
struct ARKitWorldTackingSessionConfiguration_t1821734930_marshaled_pinvoke
{
	int32_t ___alignment_0;
	int32_t ___planeDetection_1;
	int32_t ___getPointCloudData_2;
	int32_t ___enableLightEstimation_3;
};
// Native definition for COM marshalling of UnityEngine.XR.iOS.ARKitWorldTackingSessionConfiguration
struct ARKitWorldTackingSessionConfiguration_t1821734930_marshaled_com
{
	int32_t ___alignment_0;
	int32_t ___planeDetection_1;
	int32_t ___getPointCloudData_2;
	int32_t ___enableLightEstimation_3;
};
#endif // ARKITWORLDTACKINGSESSIONCONFIGURATION_T1821734930_H
#ifndef ARPLANEANCHOR_T1439520888_H
#define ARPLANEANCHOR_T1439520888_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARPlaneAnchor
struct  ARPlaneAnchor_t1439520888 
{
public:
	// System.String UnityEngine.XR.iOS.ARPlaneAnchor::identifier
	String_t* ___identifier_0;
	// UnityEngine.Matrix4x4 UnityEngine.XR.iOS.ARPlaneAnchor::transform
	Matrix4x4_t2933234003  ___transform_1;
	// UnityEngine.XR.iOS.ARPlaneAnchorAlignment UnityEngine.XR.iOS.ARPlaneAnchor::alignment
	int64_t ___alignment_2;
	// UnityEngine.Vector3 UnityEngine.XR.iOS.ARPlaneAnchor::center
	Vector3_t2243707580  ___center_3;
	// UnityEngine.Vector3 UnityEngine.XR.iOS.ARPlaneAnchor::extent
	Vector3_t2243707580  ___extent_4;

public:
	inline static int32_t get_offset_of_identifier_0() { return static_cast<int32_t>(offsetof(ARPlaneAnchor_t1439520888, ___identifier_0)); }
	inline String_t* get_identifier_0() const { return ___identifier_0; }
	inline String_t** get_address_of_identifier_0() { return &___identifier_0; }
	inline void set_identifier_0(String_t* value)
	{
		___identifier_0 = value;
		Il2CppCodeGenWriteBarrier((&___identifier_0), value);
	}

	inline static int32_t get_offset_of_transform_1() { return static_cast<int32_t>(offsetof(ARPlaneAnchor_t1439520888, ___transform_1)); }
	inline Matrix4x4_t2933234003  get_transform_1() const { return ___transform_1; }
	inline Matrix4x4_t2933234003 * get_address_of_transform_1() { return &___transform_1; }
	inline void set_transform_1(Matrix4x4_t2933234003  value)
	{
		___transform_1 = value;
	}

	inline static int32_t get_offset_of_alignment_2() { return static_cast<int32_t>(offsetof(ARPlaneAnchor_t1439520888, ___alignment_2)); }
	inline int64_t get_alignment_2() const { return ___alignment_2; }
	inline int64_t* get_address_of_alignment_2() { return &___alignment_2; }
	inline void set_alignment_2(int64_t value)
	{
		___alignment_2 = value;
	}

	inline static int32_t get_offset_of_center_3() { return static_cast<int32_t>(offsetof(ARPlaneAnchor_t1439520888, ___center_3)); }
	inline Vector3_t2243707580  get_center_3() const { return ___center_3; }
	inline Vector3_t2243707580 * get_address_of_center_3() { return &___center_3; }
	inline void set_center_3(Vector3_t2243707580  value)
	{
		___center_3 = value;
	}

	inline static int32_t get_offset_of_extent_4() { return static_cast<int32_t>(offsetof(ARPlaneAnchor_t1439520888, ___extent_4)); }
	inline Vector3_t2243707580  get_extent_4() const { return ___extent_4; }
	inline Vector3_t2243707580 * get_address_of_extent_4() { return &___extent_4; }
	inline void set_extent_4(Vector3_t2243707580  value)
	{
		___extent_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.XR.iOS.ARPlaneAnchor
struct ARPlaneAnchor_t1439520888_marshaled_pinvoke
{
	char* ___identifier_0;
	Matrix4x4_t2933234003  ___transform_1;
	int64_t ___alignment_2;
	Vector3_t2243707580  ___center_3;
	Vector3_t2243707580  ___extent_4;
};
// Native definition for COM marshalling of UnityEngine.XR.iOS.ARPlaneAnchor
struct ARPlaneAnchor_t1439520888_marshaled_com
{
	Il2CppChar* ___identifier_0;
	Matrix4x4_t2933234003  ___transform_1;
	int64_t ___alignment_2;
	Vector3_t2243707580  ___center_3;
	Vector3_t2243707580  ___extent_4;
};
#endif // ARPLANEANCHOR_T1439520888_H
#ifndef ARCAMERA_T4158705974_H
#define ARCAMERA_T4158705974_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARCamera
struct  ARCamera_t4158705974 
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.XR.iOS.ARCamera::worldTransform
	Matrix4x4_t2933234003  ___worldTransform_0;
	// UnityEngine.Vector3 UnityEngine.XR.iOS.ARCamera::eulerAngles
	Vector3_t2243707580  ___eulerAngles_1;
	// UnityEngine.XR.iOS.ARTrackingQuality UnityEngine.XR.iOS.ARCamera::trackingQuality
	int64_t ___trackingQuality_2;
	// UnityEngine.Vector3 UnityEngine.XR.iOS.ARCamera::intrinsics_row1
	Vector3_t2243707580  ___intrinsics_row1_3;
	// UnityEngine.Vector3 UnityEngine.XR.iOS.ARCamera::intrinsics_row2
	Vector3_t2243707580  ___intrinsics_row2_4;
	// UnityEngine.Vector3 UnityEngine.XR.iOS.ARCamera::intrinsics_row3
	Vector3_t2243707580  ___intrinsics_row3_5;
	// UnityEngine.XR.iOS.ARSize UnityEngine.XR.iOS.ARCamera::imageResolution
	ARSize_t3911821096  ___imageResolution_6;

public:
	inline static int32_t get_offset_of_worldTransform_0() { return static_cast<int32_t>(offsetof(ARCamera_t4158705974, ___worldTransform_0)); }
	inline Matrix4x4_t2933234003  get_worldTransform_0() const { return ___worldTransform_0; }
	inline Matrix4x4_t2933234003 * get_address_of_worldTransform_0() { return &___worldTransform_0; }
	inline void set_worldTransform_0(Matrix4x4_t2933234003  value)
	{
		___worldTransform_0 = value;
	}

	inline static int32_t get_offset_of_eulerAngles_1() { return static_cast<int32_t>(offsetof(ARCamera_t4158705974, ___eulerAngles_1)); }
	inline Vector3_t2243707580  get_eulerAngles_1() const { return ___eulerAngles_1; }
	inline Vector3_t2243707580 * get_address_of_eulerAngles_1() { return &___eulerAngles_1; }
	inline void set_eulerAngles_1(Vector3_t2243707580  value)
	{
		___eulerAngles_1 = value;
	}

	inline static int32_t get_offset_of_trackingQuality_2() { return static_cast<int32_t>(offsetof(ARCamera_t4158705974, ___trackingQuality_2)); }
	inline int64_t get_trackingQuality_2() const { return ___trackingQuality_2; }
	inline int64_t* get_address_of_trackingQuality_2() { return &___trackingQuality_2; }
	inline void set_trackingQuality_2(int64_t value)
	{
		___trackingQuality_2 = value;
	}

	inline static int32_t get_offset_of_intrinsics_row1_3() { return static_cast<int32_t>(offsetof(ARCamera_t4158705974, ___intrinsics_row1_3)); }
	inline Vector3_t2243707580  get_intrinsics_row1_3() const { return ___intrinsics_row1_3; }
	inline Vector3_t2243707580 * get_address_of_intrinsics_row1_3() { return &___intrinsics_row1_3; }
	inline void set_intrinsics_row1_3(Vector3_t2243707580  value)
	{
		___intrinsics_row1_3 = value;
	}

	inline static int32_t get_offset_of_intrinsics_row2_4() { return static_cast<int32_t>(offsetof(ARCamera_t4158705974, ___intrinsics_row2_4)); }
	inline Vector3_t2243707580  get_intrinsics_row2_4() const { return ___intrinsics_row2_4; }
	inline Vector3_t2243707580 * get_address_of_intrinsics_row2_4() { return &___intrinsics_row2_4; }
	inline void set_intrinsics_row2_4(Vector3_t2243707580  value)
	{
		___intrinsics_row2_4 = value;
	}

	inline static int32_t get_offset_of_intrinsics_row3_5() { return static_cast<int32_t>(offsetof(ARCamera_t4158705974, ___intrinsics_row3_5)); }
	inline Vector3_t2243707580  get_intrinsics_row3_5() const { return ___intrinsics_row3_5; }
	inline Vector3_t2243707580 * get_address_of_intrinsics_row3_5() { return &___intrinsics_row3_5; }
	inline void set_intrinsics_row3_5(Vector3_t2243707580  value)
	{
		___intrinsics_row3_5 = value;
	}

	inline static int32_t get_offset_of_imageResolution_6() { return static_cast<int32_t>(offsetof(ARCamera_t4158705974, ___imageResolution_6)); }
	inline ARSize_t3911821096  get_imageResolution_6() const { return ___imageResolution_6; }
	inline ARSize_t3911821096 * get_address_of_imageResolution_6() { return &___imageResolution_6; }
	inline void set_imageResolution_6(ARSize_t3911821096  value)
	{
		___imageResolution_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARCAMERA_T4158705974_H
#ifndef ARHITTESTRESULT_T3275513025_H
#define ARHITTESTRESULT_T3275513025_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARHitTestResult
struct  ARHitTestResult_t3275513025 
{
public:
	// UnityEngine.XR.iOS.ARHitTestResultType UnityEngine.XR.iOS.ARHitTestResult::type
	int64_t ___type_0;
	// System.Double UnityEngine.XR.iOS.ARHitTestResult::distance
	double ___distance_1;
	// UnityEngine.Matrix4x4 UnityEngine.XR.iOS.ARHitTestResult::localTransform
	Matrix4x4_t2933234003  ___localTransform_2;
	// UnityEngine.Matrix4x4 UnityEngine.XR.iOS.ARHitTestResult::worldTransform
	Matrix4x4_t2933234003  ___worldTransform_3;
	// System.String UnityEngine.XR.iOS.ARHitTestResult::anchorIdentifier
	String_t* ___anchorIdentifier_4;
	// System.Boolean UnityEngine.XR.iOS.ARHitTestResult::isValid
	bool ___isValid_5;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(ARHitTestResult_t3275513025, ___type_0)); }
	inline int64_t get_type_0() const { return ___type_0; }
	inline int64_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int64_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_distance_1() { return static_cast<int32_t>(offsetof(ARHitTestResult_t3275513025, ___distance_1)); }
	inline double get_distance_1() const { return ___distance_1; }
	inline double* get_address_of_distance_1() { return &___distance_1; }
	inline void set_distance_1(double value)
	{
		___distance_1 = value;
	}

	inline static int32_t get_offset_of_localTransform_2() { return static_cast<int32_t>(offsetof(ARHitTestResult_t3275513025, ___localTransform_2)); }
	inline Matrix4x4_t2933234003  get_localTransform_2() const { return ___localTransform_2; }
	inline Matrix4x4_t2933234003 * get_address_of_localTransform_2() { return &___localTransform_2; }
	inline void set_localTransform_2(Matrix4x4_t2933234003  value)
	{
		___localTransform_2 = value;
	}

	inline static int32_t get_offset_of_worldTransform_3() { return static_cast<int32_t>(offsetof(ARHitTestResult_t3275513025, ___worldTransform_3)); }
	inline Matrix4x4_t2933234003  get_worldTransform_3() const { return ___worldTransform_3; }
	inline Matrix4x4_t2933234003 * get_address_of_worldTransform_3() { return &___worldTransform_3; }
	inline void set_worldTransform_3(Matrix4x4_t2933234003  value)
	{
		___worldTransform_3 = value;
	}

	inline static int32_t get_offset_of_anchorIdentifier_4() { return static_cast<int32_t>(offsetof(ARHitTestResult_t3275513025, ___anchorIdentifier_4)); }
	inline String_t* get_anchorIdentifier_4() const { return ___anchorIdentifier_4; }
	inline String_t** get_address_of_anchorIdentifier_4() { return &___anchorIdentifier_4; }
	inline void set_anchorIdentifier_4(String_t* value)
	{
		___anchorIdentifier_4 = value;
		Il2CppCodeGenWriteBarrier((&___anchorIdentifier_4), value);
	}

	inline static int32_t get_offset_of_isValid_5() { return static_cast<int32_t>(offsetof(ARHitTestResult_t3275513025, ___isValid_5)); }
	inline bool get_isValid_5() const { return ___isValid_5; }
	inline bool* get_address_of_isValid_5() { return &___isValid_5; }
	inline void set_isValid_5(bool value)
	{
		___isValid_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.XR.iOS.ARHitTestResult
struct ARHitTestResult_t3275513025_marshaled_pinvoke
{
	int64_t ___type_0;
	double ___distance_1;
	Matrix4x4_t2933234003  ___localTransform_2;
	Matrix4x4_t2933234003  ___worldTransform_3;
	char* ___anchorIdentifier_4;
	int32_t ___isValid_5;
};
// Native definition for COM marshalling of UnityEngine.XR.iOS.ARHitTestResult
struct ARHitTestResult_t3275513025_marshaled_com
{
	int64_t ___type_0;
	double ___distance_1;
	Matrix4x4_t2933234003  ___localTransform_2;
	Matrix4x4_t2933234003  ___worldTransform_3;
	Il2CppChar* ___anchorIdentifier_4;
	int32_t ___isValid_5;
};
#endif // ARHITTESTRESULT_T3275513025_H
#ifndef TOUCH_T407273883_H
#define TOUCH_T407273883_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Touch
struct  Touch_t407273883 
{
public:
	// System.Int32 UnityEngine.Touch::m_FingerId
	int32_t ___m_FingerId_0;
	// UnityEngine.Vector2 UnityEngine.Touch::m_Position
	Vector2_t2243707579  ___m_Position_1;
	// UnityEngine.Vector2 UnityEngine.Touch::m_RawPosition
	Vector2_t2243707579  ___m_RawPosition_2;
	// UnityEngine.Vector2 UnityEngine.Touch::m_PositionDelta
	Vector2_t2243707579  ___m_PositionDelta_3;
	// System.Single UnityEngine.Touch::m_TimeDelta
	float ___m_TimeDelta_4;
	// System.Int32 UnityEngine.Touch::m_TapCount
	int32_t ___m_TapCount_5;
	// UnityEngine.TouchPhase UnityEngine.Touch::m_Phase
	int32_t ___m_Phase_6;
	// UnityEngine.TouchType UnityEngine.Touch::m_Type
	int32_t ___m_Type_7;
	// System.Single UnityEngine.Touch::m_Pressure
	float ___m_Pressure_8;
	// System.Single UnityEngine.Touch::m_maximumPossiblePressure
	float ___m_maximumPossiblePressure_9;
	// System.Single UnityEngine.Touch::m_Radius
	float ___m_Radius_10;
	// System.Single UnityEngine.Touch::m_RadiusVariance
	float ___m_RadiusVariance_11;
	// System.Single UnityEngine.Touch::m_AltitudeAngle
	float ___m_AltitudeAngle_12;
	// System.Single UnityEngine.Touch::m_AzimuthAngle
	float ___m_AzimuthAngle_13;

public:
	inline static int32_t get_offset_of_m_FingerId_0() { return static_cast<int32_t>(offsetof(Touch_t407273883, ___m_FingerId_0)); }
	inline int32_t get_m_FingerId_0() const { return ___m_FingerId_0; }
	inline int32_t* get_address_of_m_FingerId_0() { return &___m_FingerId_0; }
	inline void set_m_FingerId_0(int32_t value)
	{
		___m_FingerId_0 = value;
	}

	inline static int32_t get_offset_of_m_Position_1() { return static_cast<int32_t>(offsetof(Touch_t407273883, ___m_Position_1)); }
	inline Vector2_t2243707579  get_m_Position_1() const { return ___m_Position_1; }
	inline Vector2_t2243707579 * get_address_of_m_Position_1() { return &___m_Position_1; }
	inline void set_m_Position_1(Vector2_t2243707579  value)
	{
		___m_Position_1 = value;
	}

	inline static int32_t get_offset_of_m_RawPosition_2() { return static_cast<int32_t>(offsetof(Touch_t407273883, ___m_RawPosition_2)); }
	inline Vector2_t2243707579  get_m_RawPosition_2() const { return ___m_RawPosition_2; }
	inline Vector2_t2243707579 * get_address_of_m_RawPosition_2() { return &___m_RawPosition_2; }
	inline void set_m_RawPosition_2(Vector2_t2243707579  value)
	{
		___m_RawPosition_2 = value;
	}

	inline static int32_t get_offset_of_m_PositionDelta_3() { return static_cast<int32_t>(offsetof(Touch_t407273883, ___m_PositionDelta_3)); }
	inline Vector2_t2243707579  get_m_PositionDelta_3() const { return ___m_PositionDelta_3; }
	inline Vector2_t2243707579 * get_address_of_m_PositionDelta_3() { return &___m_PositionDelta_3; }
	inline void set_m_PositionDelta_3(Vector2_t2243707579  value)
	{
		___m_PositionDelta_3 = value;
	}

	inline static int32_t get_offset_of_m_TimeDelta_4() { return static_cast<int32_t>(offsetof(Touch_t407273883, ___m_TimeDelta_4)); }
	inline float get_m_TimeDelta_4() const { return ___m_TimeDelta_4; }
	inline float* get_address_of_m_TimeDelta_4() { return &___m_TimeDelta_4; }
	inline void set_m_TimeDelta_4(float value)
	{
		___m_TimeDelta_4 = value;
	}

	inline static int32_t get_offset_of_m_TapCount_5() { return static_cast<int32_t>(offsetof(Touch_t407273883, ___m_TapCount_5)); }
	inline int32_t get_m_TapCount_5() const { return ___m_TapCount_5; }
	inline int32_t* get_address_of_m_TapCount_5() { return &___m_TapCount_5; }
	inline void set_m_TapCount_5(int32_t value)
	{
		___m_TapCount_5 = value;
	}

	inline static int32_t get_offset_of_m_Phase_6() { return static_cast<int32_t>(offsetof(Touch_t407273883, ___m_Phase_6)); }
	inline int32_t get_m_Phase_6() const { return ___m_Phase_6; }
	inline int32_t* get_address_of_m_Phase_6() { return &___m_Phase_6; }
	inline void set_m_Phase_6(int32_t value)
	{
		___m_Phase_6 = value;
	}

	inline static int32_t get_offset_of_m_Type_7() { return static_cast<int32_t>(offsetof(Touch_t407273883, ___m_Type_7)); }
	inline int32_t get_m_Type_7() const { return ___m_Type_7; }
	inline int32_t* get_address_of_m_Type_7() { return &___m_Type_7; }
	inline void set_m_Type_7(int32_t value)
	{
		___m_Type_7 = value;
	}

	inline static int32_t get_offset_of_m_Pressure_8() { return static_cast<int32_t>(offsetof(Touch_t407273883, ___m_Pressure_8)); }
	inline float get_m_Pressure_8() const { return ___m_Pressure_8; }
	inline float* get_address_of_m_Pressure_8() { return &___m_Pressure_8; }
	inline void set_m_Pressure_8(float value)
	{
		___m_Pressure_8 = value;
	}

	inline static int32_t get_offset_of_m_maximumPossiblePressure_9() { return static_cast<int32_t>(offsetof(Touch_t407273883, ___m_maximumPossiblePressure_9)); }
	inline float get_m_maximumPossiblePressure_9() const { return ___m_maximumPossiblePressure_9; }
	inline float* get_address_of_m_maximumPossiblePressure_9() { return &___m_maximumPossiblePressure_9; }
	inline void set_m_maximumPossiblePressure_9(float value)
	{
		___m_maximumPossiblePressure_9 = value;
	}

	inline static int32_t get_offset_of_m_Radius_10() { return static_cast<int32_t>(offsetof(Touch_t407273883, ___m_Radius_10)); }
	inline float get_m_Radius_10() const { return ___m_Radius_10; }
	inline float* get_address_of_m_Radius_10() { return &___m_Radius_10; }
	inline void set_m_Radius_10(float value)
	{
		___m_Radius_10 = value;
	}

	inline static int32_t get_offset_of_m_RadiusVariance_11() { return static_cast<int32_t>(offsetof(Touch_t407273883, ___m_RadiusVariance_11)); }
	inline float get_m_RadiusVariance_11() const { return ___m_RadiusVariance_11; }
	inline float* get_address_of_m_RadiusVariance_11() { return &___m_RadiusVariance_11; }
	inline void set_m_RadiusVariance_11(float value)
	{
		___m_RadiusVariance_11 = value;
	}

	inline static int32_t get_offset_of_m_AltitudeAngle_12() { return static_cast<int32_t>(offsetof(Touch_t407273883, ___m_AltitudeAngle_12)); }
	inline float get_m_AltitudeAngle_12() const { return ___m_AltitudeAngle_12; }
	inline float* get_address_of_m_AltitudeAngle_12() { return &___m_AltitudeAngle_12; }
	inline void set_m_AltitudeAngle_12(float value)
	{
		___m_AltitudeAngle_12 = value;
	}

	inline static int32_t get_offset_of_m_AzimuthAngle_13() { return static_cast<int32_t>(offsetof(Touch_t407273883, ___m_AzimuthAngle_13)); }
	inline float get_m_AzimuthAngle_13() const { return ___m_AzimuthAngle_13; }
	inline float* get_address_of_m_AzimuthAngle_13() { return &___m_AzimuthAngle_13; }
	inline void set_m_AzimuthAngle_13(float value)
	{
		___m_AzimuthAngle_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCH_T407273883_H
#ifndef UNITYARANCHORDATA_T2901866349_H
#define UNITYARANCHORDATA_T2901866349_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARAnchorData
struct  UnityARAnchorData_t2901866349 
{
public:
	// System.IntPtr UnityEngine.XR.iOS.UnityARAnchorData::ptrIdentifier
	IntPtr_t ___ptrIdentifier_0;
	// UnityEngine.XR.iOS.UnityARMatrix4x4 UnityEngine.XR.iOS.UnityARAnchorData::transform
	UnityARMatrix4x4_t100931615  ___transform_1;
	// UnityEngine.XR.iOS.ARPlaneAnchorAlignment UnityEngine.XR.iOS.UnityARAnchorData::alignment
	int64_t ___alignment_2;
	// UnityEngine.Vector4 UnityEngine.XR.iOS.UnityARAnchorData::center
	Vector4_t2243707581  ___center_3;
	// UnityEngine.Vector4 UnityEngine.XR.iOS.UnityARAnchorData::extent
	Vector4_t2243707581  ___extent_4;

public:
	inline static int32_t get_offset_of_ptrIdentifier_0() { return static_cast<int32_t>(offsetof(UnityARAnchorData_t2901866349, ___ptrIdentifier_0)); }
	inline IntPtr_t get_ptrIdentifier_0() const { return ___ptrIdentifier_0; }
	inline IntPtr_t* get_address_of_ptrIdentifier_0() { return &___ptrIdentifier_0; }
	inline void set_ptrIdentifier_0(IntPtr_t value)
	{
		___ptrIdentifier_0 = value;
	}

	inline static int32_t get_offset_of_transform_1() { return static_cast<int32_t>(offsetof(UnityARAnchorData_t2901866349, ___transform_1)); }
	inline UnityARMatrix4x4_t100931615  get_transform_1() const { return ___transform_1; }
	inline UnityARMatrix4x4_t100931615 * get_address_of_transform_1() { return &___transform_1; }
	inline void set_transform_1(UnityARMatrix4x4_t100931615  value)
	{
		___transform_1 = value;
	}

	inline static int32_t get_offset_of_alignment_2() { return static_cast<int32_t>(offsetof(UnityARAnchorData_t2901866349, ___alignment_2)); }
	inline int64_t get_alignment_2() const { return ___alignment_2; }
	inline int64_t* get_address_of_alignment_2() { return &___alignment_2; }
	inline void set_alignment_2(int64_t value)
	{
		___alignment_2 = value;
	}

	inline static int32_t get_offset_of_center_3() { return static_cast<int32_t>(offsetof(UnityARAnchorData_t2901866349, ___center_3)); }
	inline Vector4_t2243707581  get_center_3() const { return ___center_3; }
	inline Vector4_t2243707581 * get_address_of_center_3() { return &___center_3; }
	inline void set_center_3(Vector4_t2243707581  value)
	{
		___center_3 = value;
	}

	inline static int32_t get_offset_of_extent_4() { return static_cast<int32_t>(offsetof(UnityARAnchorData_t2901866349, ___extent_4)); }
	inline Vector4_t2243707581  get_extent_4() const { return ___extent_4; }
	inline Vector4_t2243707581 * get_address_of_extent_4() { return &___extent_4; }
	inline void set_extent_4(Vector4_t2243707581  value)
	{
		___extent_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARANCHORDATA_T2901866349_H
#ifndef MESHFILTER_T3026937449_H
#define MESHFILTER_T3026937449_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MeshFilter
struct  MeshFilter_t3026937449  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHFILTER_T3026937449_H
#ifndef ASYNCCALLBACK_T163412349_H
#define ASYNCCALLBACK_T163412349_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.AsyncCallback
struct  AsyncCallback_t163412349  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCCALLBACK_T163412349_H
#ifndef TRANSFORM_T3275118058_H
#define TRANSFORM_T3275118058_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_t3275118058  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_T3275118058_H
#ifndef ARFRAME_T1001293426_H
#define ARFRAME_T1001293426_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARFrame
struct  ARFrame_t1001293426 
{
public:
	// System.Double UnityEngine.XR.iOS.ARFrame::timestamp
	double ___timestamp_0;
	// System.IntPtr UnityEngine.XR.iOS.ARFrame::capturedImage
	IntPtr_t ___capturedImage_1;
	// UnityEngine.XR.iOS.ARCamera UnityEngine.XR.iOS.ARFrame::camera
	ARCamera_t4158705974  ___camera_2;
	// UnityEngine.XR.iOS.ARLightEstimate UnityEngine.XR.iOS.ARFrame::lightEstimate
	ARLightEstimate_t3477821059  ___lightEstimate_3;

public:
	inline static int32_t get_offset_of_timestamp_0() { return static_cast<int32_t>(offsetof(ARFrame_t1001293426, ___timestamp_0)); }
	inline double get_timestamp_0() const { return ___timestamp_0; }
	inline double* get_address_of_timestamp_0() { return &___timestamp_0; }
	inline void set_timestamp_0(double value)
	{
		___timestamp_0 = value;
	}

	inline static int32_t get_offset_of_capturedImage_1() { return static_cast<int32_t>(offsetof(ARFrame_t1001293426, ___capturedImage_1)); }
	inline IntPtr_t get_capturedImage_1() const { return ___capturedImage_1; }
	inline IntPtr_t* get_address_of_capturedImage_1() { return &___capturedImage_1; }
	inline void set_capturedImage_1(IntPtr_t value)
	{
		___capturedImage_1 = value;
	}

	inline static int32_t get_offset_of_camera_2() { return static_cast<int32_t>(offsetof(ARFrame_t1001293426, ___camera_2)); }
	inline ARCamera_t4158705974  get_camera_2() const { return ___camera_2; }
	inline ARCamera_t4158705974 * get_address_of_camera_2() { return &___camera_2; }
	inline void set_camera_2(ARCamera_t4158705974  value)
	{
		___camera_2 = value;
	}

	inline static int32_t get_offset_of_lightEstimate_3() { return static_cast<int32_t>(offsetof(ARFrame_t1001293426, ___lightEstimate_3)); }
	inline ARLightEstimate_t3477821059  get_lightEstimate_3() const { return ___lightEstimate_3; }
	inline ARLightEstimate_t3477821059 * get_address_of_lightEstimate_3() { return &___lightEstimate_3; }
	inline void set_lightEstimate_3(ARLightEstimate_t3477821059  value)
	{
		___lightEstimate_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARFRAME_T1001293426_H
#ifndef ARANCHORREMOVED_T142665927_H
#define ARANCHORREMOVED_T142665927_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorRemoved
struct  ARAnchorRemoved_t142665927  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARANCHORREMOVED_T142665927_H
#ifndef UNITYARSESSIONNATIVEINTERFACE_T1130867170_H
#define UNITYARSESSIONNATIVEINTERFACE_T1130867170_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface
struct  UnityARSessionNativeInterface_t1130867170  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.XR.iOS.UnityARSessionNativeInterface::m_NativeARSession
	IntPtr_t ___m_NativeARSession_5;

public:
	inline static int32_t get_offset_of_m_NativeARSession_5() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t1130867170, ___m_NativeARSession_5)); }
	inline IntPtr_t get_m_NativeARSession_5() const { return ___m_NativeARSession_5; }
	inline IntPtr_t* get_address_of_m_NativeARSession_5() { return &___m_NativeARSession_5; }
	inline void set_m_NativeARSession_5(IntPtr_t value)
	{
		___m_NativeARSession_5 = value;
	}
};

struct UnityARSessionNativeInterface_t1130867170_StaticFields
{
public:
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARFrameUpdate UnityEngine.XR.iOS.UnityARSessionNativeInterface::ARFrameUpdatedEvent
	ARFrameUpdate_t496507918 * ___ARFrameUpdatedEvent_0;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorAdded UnityEngine.XR.iOS.UnityARSessionNativeInterface::ARAnchorAddedEvent
	ARAnchorAdded_t2646854145 * ___ARAnchorAddedEvent_1;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorUpdated UnityEngine.XR.iOS.UnityARSessionNativeInterface::ARAnchorUpdatedEvent
	ARAnchorUpdated_t3886071158 * ___ARAnchorUpdatedEvent_2;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorRemoved UnityEngine.XR.iOS.UnityARSessionNativeInterface::ARAnchorRemovedEvent
	ARAnchorRemoved_t142665927 * ___ARAnchorRemovedEvent_3;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARSessionFailed UnityEngine.XR.iOS.UnityARSessionNativeInterface::ARSessionFailedEvent
	ARSessionFailed_t872580813 * ___ARSessionFailedEvent_4;
	// UnityEngine.XR.iOS.UnityARCamera UnityEngine.XR.iOS.UnityARSessionNativeInterface::s_Camera
	UnityARCamera_t4198559457  ___s_Camera_6;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface UnityEngine.XR.iOS.UnityARSessionNativeInterface::s_UnityARSessionNativeInterface
	UnityARSessionNativeInterface_t1130867170 * ___s_UnityARSessionNativeInterface_7;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFrameUpdate UnityEngine.XR.iOS.UnityARSessionNativeInterface::<>f__mg$cache0
	internal_ARFrameUpdate_t3296518558 * ___U3CU3Ef__mgU24cache0_8;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorAdded UnityEngine.XR.iOS.UnityARSessionNativeInterface::<>f__mg$cache1
	internal_ARAnchorAdded_t1622117597 * ___U3CU3Ef__mgU24cache1_9;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorUpdated UnityEngine.XR.iOS.UnityARSessionNativeInterface::<>f__mg$cache2
	internal_ARAnchorUpdated_t3705772742 * ___U3CU3Ef__mgU24cache2_10;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorRemoved UnityEngine.XR.iOS.UnityARSessionNativeInterface::<>f__mg$cache3
	internal_ARAnchorRemoved_t3189755211 * ___U3CU3Ef__mgU24cache3_11;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARSessionFailed UnityEngine.XR.iOS.UnityARSessionNativeInterface::<>f__mg$cache4
	ARSessionFailed_t872580813 * ___U3CU3Ef__mgU24cache4_12;

public:
	inline static int32_t get_offset_of_ARFrameUpdatedEvent_0() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t1130867170_StaticFields, ___ARFrameUpdatedEvent_0)); }
	inline ARFrameUpdate_t496507918 * get_ARFrameUpdatedEvent_0() const { return ___ARFrameUpdatedEvent_0; }
	inline ARFrameUpdate_t496507918 ** get_address_of_ARFrameUpdatedEvent_0() { return &___ARFrameUpdatedEvent_0; }
	inline void set_ARFrameUpdatedEvent_0(ARFrameUpdate_t496507918 * value)
	{
		___ARFrameUpdatedEvent_0 = value;
		Il2CppCodeGenWriteBarrier((&___ARFrameUpdatedEvent_0), value);
	}

	inline static int32_t get_offset_of_ARAnchorAddedEvent_1() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t1130867170_StaticFields, ___ARAnchorAddedEvent_1)); }
	inline ARAnchorAdded_t2646854145 * get_ARAnchorAddedEvent_1() const { return ___ARAnchorAddedEvent_1; }
	inline ARAnchorAdded_t2646854145 ** get_address_of_ARAnchorAddedEvent_1() { return &___ARAnchorAddedEvent_1; }
	inline void set_ARAnchorAddedEvent_1(ARAnchorAdded_t2646854145 * value)
	{
		___ARAnchorAddedEvent_1 = value;
		Il2CppCodeGenWriteBarrier((&___ARAnchorAddedEvent_1), value);
	}

	inline static int32_t get_offset_of_ARAnchorUpdatedEvent_2() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t1130867170_StaticFields, ___ARAnchorUpdatedEvent_2)); }
	inline ARAnchorUpdated_t3886071158 * get_ARAnchorUpdatedEvent_2() const { return ___ARAnchorUpdatedEvent_2; }
	inline ARAnchorUpdated_t3886071158 ** get_address_of_ARAnchorUpdatedEvent_2() { return &___ARAnchorUpdatedEvent_2; }
	inline void set_ARAnchorUpdatedEvent_2(ARAnchorUpdated_t3886071158 * value)
	{
		___ARAnchorUpdatedEvent_2 = value;
		Il2CppCodeGenWriteBarrier((&___ARAnchorUpdatedEvent_2), value);
	}

	inline static int32_t get_offset_of_ARAnchorRemovedEvent_3() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t1130867170_StaticFields, ___ARAnchorRemovedEvent_3)); }
	inline ARAnchorRemoved_t142665927 * get_ARAnchorRemovedEvent_3() const { return ___ARAnchorRemovedEvent_3; }
	inline ARAnchorRemoved_t142665927 ** get_address_of_ARAnchorRemovedEvent_3() { return &___ARAnchorRemovedEvent_3; }
	inline void set_ARAnchorRemovedEvent_3(ARAnchorRemoved_t142665927 * value)
	{
		___ARAnchorRemovedEvent_3 = value;
		Il2CppCodeGenWriteBarrier((&___ARAnchorRemovedEvent_3), value);
	}

	inline static int32_t get_offset_of_ARSessionFailedEvent_4() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t1130867170_StaticFields, ___ARSessionFailedEvent_4)); }
	inline ARSessionFailed_t872580813 * get_ARSessionFailedEvent_4() const { return ___ARSessionFailedEvent_4; }
	inline ARSessionFailed_t872580813 ** get_address_of_ARSessionFailedEvent_4() { return &___ARSessionFailedEvent_4; }
	inline void set_ARSessionFailedEvent_4(ARSessionFailed_t872580813 * value)
	{
		___ARSessionFailedEvent_4 = value;
		Il2CppCodeGenWriteBarrier((&___ARSessionFailedEvent_4), value);
	}

	inline static int32_t get_offset_of_s_Camera_6() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t1130867170_StaticFields, ___s_Camera_6)); }
	inline UnityARCamera_t4198559457  get_s_Camera_6() const { return ___s_Camera_6; }
	inline UnityARCamera_t4198559457 * get_address_of_s_Camera_6() { return &___s_Camera_6; }
	inline void set_s_Camera_6(UnityARCamera_t4198559457  value)
	{
		___s_Camera_6 = value;
	}

	inline static int32_t get_offset_of_s_UnityARSessionNativeInterface_7() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t1130867170_StaticFields, ___s_UnityARSessionNativeInterface_7)); }
	inline UnityARSessionNativeInterface_t1130867170 * get_s_UnityARSessionNativeInterface_7() const { return ___s_UnityARSessionNativeInterface_7; }
	inline UnityARSessionNativeInterface_t1130867170 ** get_address_of_s_UnityARSessionNativeInterface_7() { return &___s_UnityARSessionNativeInterface_7; }
	inline void set_s_UnityARSessionNativeInterface_7(UnityARSessionNativeInterface_t1130867170 * value)
	{
		___s_UnityARSessionNativeInterface_7 = value;
		Il2CppCodeGenWriteBarrier((&___s_UnityARSessionNativeInterface_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_8() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t1130867170_StaticFields, ___U3CU3Ef__mgU24cache0_8)); }
	inline internal_ARFrameUpdate_t3296518558 * get_U3CU3Ef__mgU24cache0_8() const { return ___U3CU3Ef__mgU24cache0_8; }
	inline internal_ARFrameUpdate_t3296518558 ** get_address_of_U3CU3Ef__mgU24cache0_8() { return &___U3CU3Ef__mgU24cache0_8; }
	inline void set_U3CU3Ef__mgU24cache0_8(internal_ARFrameUpdate_t3296518558 * value)
	{
		___U3CU3Ef__mgU24cache0_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_8), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_9() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t1130867170_StaticFields, ___U3CU3Ef__mgU24cache1_9)); }
	inline internal_ARAnchorAdded_t1622117597 * get_U3CU3Ef__mgU24cache1_9() const { return ___U3CU3Ef__mgU24cache1_9; }
	inline internal_ARAnchorAdded_t1622117597 ** get_address_of_U3CU3Ef__mgU24cache1_9() { return &___U3CU3Ef__mgU24cache1_9; }
	inline void set_U3CU3Ef__mgU24cache1_9(internal_ARAnchorAdded_t1622117597 * value)
	{
		___U3CU3Ef__mgU24cache1_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_9), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2_10() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t1130867170_StaticFields, ___U3CU3Ef__mgU24cache2_10)); }
	inline internal_ARAnchorUpdated_t3705772742 * get_U3CU3Ef__mgU24cache2_10() const { return ___U3CU3Ef__mgU24cache2_10; }
	inline internal_ARAnchorUpdated_t3705772742 ** get_address_of_U3CU3Ef__mgU24cache2_10() { return &___U3CU3Ef__mgU24cache2_10; }
	inline void set_U3CU3Ef__mgU24cache2_10(internal_ARAnchorUpdated_t3705772742 * value)
	{
		___U3CU3Ef__mgU24cache2_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache2_10), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache3_11() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t1130867170_StaticFields, ___U3CU3Ef__mgU24cache3_11)); }
	inline internal_ARAnchorRemoved_t3189755211 * get_U3CU3Ef__mgU24cache3_11() const { return ___U3CU3Ef__mgU24cache3_11; }
	inline internal_ARAnchorRemoved_t3189755211 ** get_address_of_U3CU3Ef__mgU24cache3_11() { return &___U3CU3Ef__mgU24cache3_11; }
	inline void set_U3CU3Ef__mgU24cache3_11(internal_ARAnchorRemoved_t3189755211 * value)
	{
		___U3CU3Ef__mgU24cache3_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache3_11), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache4_12() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t1130867170_StaticFields, ___U3CU3Ef__mgU24cache4_12)); }
	inline ARSessionFailed_t872580813 * get_U3CU3Ef__mgU24cache4_12() const { return ___U3CU3Ef__mgU24cache4_12; }
	inline ARSessionFailed_t872580813 ** get_address_of_U3CU3Ef__mgU24cache4_12() { return &___U3CU3Ef__mgU24cache4_12; }
	inline void set_U3CU3Ef__mgU24cache4_12(ARSessionFailed_t872580813 * value)
	{
		___U3CU3Ef__mgU24cache4_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache4_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARSESSIONNATIVEINTERFACE_T1130867170_H
#ifndef ARANCHORUPDATED_T3886071158_H
#define ARANCHORUPDATED_T3886071158_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorUpdated
struct  ARAnchorUpdated_t3886071158  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARANCHORUPDATED_T3886071158_H
#ifndef ARANCHORADDED_T2646854145_H
#define ARANCHORADDED_T2646854145_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorAdded
struct  ARAnchorAdded_t2646854145  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARANCHORADDED_T2646854145_H
#ifndef TEXTURE2D_T3542995729_H
#define TEXTURE2D_T3542995729_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture2D
struct  Texture2D_t3542995729  : public Texture_t2243626319
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE2D_T3542995729_H
#ifndef PARTICLESYSTEM_T3394631041_H
#define PARTICLESYSTEM_T3394631041_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystem
struct  ParticleSystem_t3394631041  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLESYSTEM_T3394631041_H
#ifndef INTERNAL_ARFRAMEUPDATE_T3296518558_H
#define INTERNAL_ARFRAMEUPDATE_T3296518558_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFrameUpdate
struct  internal_ARFrameUpdate_t3296518558  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNAL_ARFRAMEUPDATE_T3296518558_H
#ifndef ENUMERATOR_T2179363831_H
#define ENUMERATOR_T2179363831_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<UnityEngine.XR.iOS.ARHitTestResult>
struct  Enumerator_t2179363831 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t2644634157 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	ARHitTestResult_t3275513025  ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t2179363831, ___l_0)); }
	inline List_1_t2644634157 * get_l_0() const { return ___l_0; }
	inline List_1_t2644634157 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t2644634157 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t2179363831, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t2179363831, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t2179363831, ___current_3)); }
	inline ARHitTestResult_t3275513025  get_current_3() const { return ___current_3; }
	inline ARHitTestResult_t3275513025 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(ARHitTestResult_t3275513025  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2179363831_H
#ifndef ARFRAMEUPDATE_T496507918_H
#define ARFRAMEUPDATE_T496507918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARFrameUpdate
struct  ARFrameUpdate_t496507918  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARFRAMEUPDATE_T496507918_H
#ifndef ARPLANEANCHORGAMEOBJECT_T2305225887_H
#define ARPLANEANCHORGAMEOBJECT_T2305225887_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARPlaneAnchorGameObject
struct  ARPlaneAnchorGameObject_t2305225887  : public RuntimeObject
{
public:
	// UnityEngine.GameObject UnityEngine.XR.iOS.ARPlaneAnchorGameObject::gameObject
	GameObject_t1756533147 * ___gameObject_0;
	// UnityEngine.XR.iOS.ARPlaneAnchor UnityEngine.XR.iOS.ARPlaneAnchorGameObject::planeAnchor
	ARPlaneAnchor_t1439520888  ___planeAnchor_1;

public:
	inline static int32_t get_offset_of_gameObject_0() { return static_cast<int32_t>(offsetof(ARPlaneAnchorGameObject_t2305225887, ___gameObject_0)); }
	inline GameObject_t1756533147 * get_gameObject_0() const { return ___gameObject_0; }
	inline GameObject_t1756533147 ** get_address_of_gameObject_0() { return &___gameObject_0; }
	inline void set_gameObject_0(GameObject_t1756533147 * value)
	{
		___gameObject_0 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_0), value);
	}

	inline static int32_t get_offset_of_planeAnchor_1() { return static_cast<int32_t>(offsetof(ARPlaneAnchorGameObject_t2305225887, ___planeAnchor_1)); }
	inline ARPlaneAnchor_t1439520888  get_planeAnchor_1() const { return ___planeAnchor_1; }
	inline ARPlaneAnchor_t1439520888 * get_address_of_planeAnchor_1() { return &___planeAnchor_1; }
	inline void set_planeAnchor_1(ARPlaneAnchor_t1439520888  value)
	{
		___planeAnchor_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARPLANEANCHORGAMEOBJECT_T2305225887_H
#ifndef INTERNAL_ARANCHORADDED_T1622117597_H
#define INTERNAL_ARANCHORADDED_T1622117597_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorAdded
struct  internal_ARAnchorAdded_t1622117597  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNAL_ARANCHORADDED_T1622117597_H
#ifndef INTERNAL_ARANCHORREMOVED_T3189755211_H
#define INTERNAL_ARANCHORREMOVED_T3189755211_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorRemoved
struct  internal_ARAnchorRemoved_t3189755211  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNAL_ARANCHORREMOVED_T3189755211_H
#ifndef ARSESSIONFAILED_T872580813_H
#define ARSESSIONFAILED_T872580813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARSessionFailed
struct  ARSessionFailed_t872580813  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARSESSIONFAILED_T872580813_H
#ifndef INTERNAL_ARANCHORUPDATED_T3705772742_H
#define INTERNAL_ARANCHORUPDATED_T3705772742_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorUpdated
struct  internal_ARAnchorUpdated_t3705772742  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNAL_ARANCHORUPDATED_T3705772742_H
#ifndef BEHAVIOUR_T955675639_H
#define BEHAVIOUR_T955675639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t955675639  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T955675639_H
#ifndef MONOBEHAVIOUR_T1158329972_H
#define MONOBEHAVIOUR_T1158329972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t1158329972  : public Behaviour_t955675639
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T1158329972_H
#ifndef CAMERA_T189460977_H
#define CAMERA_T189460977_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Camera
struct  Camera_t189460977  : public Behaviour_t955675639
{
public:

public:
};

struct Camera_t189460977_StaticFields
{
public:
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_t834278767 * ___onPreCull_2;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_t834278767 * ___onPreRender_3;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_t834278767 * ___onPostRender_4;

public:
	inline static int32_t get_offset_of_onPreCull_2() { return static_cast<int32_t>(offsetof(Camera_t189460977_StaticFields, ___onPreCull_2)); }
	inline CameraCallback_t834278767 * get_onPreCull_2() const { return ___onPreCull_2; }
	inline CameraCallback_t834278767 ** get_address_of_onPreCull_2() { return &___onPreCull_2; }
	inline void set_onPreCull_2(CameraCallback_t834278767 * value)
	{
		___onPreCull_2 = value;
		Il2CppCodeGenWriteBarrier((&___onPreCull_2), value);
	}

	inline static int32_t get_offset_of_onPreRender_3() { return static_cast<int32_t>(offsetof(Camera_t189460977_StaticFields, ___onPreRender_3)); }
	inline CameraCallback_t834278767 * get_onPreRender_3() const { return ___onPreRender_3; }
	inline CameraCallback_t834278767 ** get_address_of_onPreRender_3() { return &___onPreRender_3; }
	inline void set_onPreRender_3(CameraCallback_t834278767 * value)
	{
		___onPreRender_3 = value;
		Il2CppCodeGenWriteBarrier((&___onPreRender_3), value);
	}

	inline static int32_t get_offset_of_onPostRender_4() { return static_cast<int32_t>(offsetof(Camera_t189460977_StaticFields, ___onPostRender_4)); }
	inline CameraCallback_t834278767 * get_onPostRender_4() const { return ___onPostRender_4; }
	inline CameraCallback_t834278767 ** get_address_of_onPostRender_4() { return &___onPostRender_4; }
	inline void set_onPostRender_4(CameraCallback_t834278767 * value)
	{
		___onPostRender_4 = value;
		Il2CppCodeGenWriteBarrier((&___onPostRender_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERA_T189460977_H
#ifndef AR3DOFCAMERAMANAGER_T2152865733_H
#define AR3DOFCAMERAMANAGER_T2152865733_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AR3DOFCameraManager
struct  AR3DOFCameraManager_t2152865733  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Camera AR3DOFCameraManager::m_camera
	Camera_t189460977 * ___m_camera_2;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface AR3DOFCameraManager::m_session
	UnityARSessionNativeInterface_t1130867170 * ___m_session_3;
	// UnityEngine.Material AR3DOFCameraManager::savedClearMaterial
	Material_t193706927 * ___savedClearMaterial_4;

public:
	inline static int32_t get_offset_of_m_camera_2() { return static_cast<int32_t>(offsetof(AR3DOFCameraManager_t2152865733, ___m_camera_2)); }
	inline Camera_t189460977 * get_m_camera_2() const { return ___m_camera_2; }
	inline Camera_t189460977 ** get_address_of_m_camera_2() { return &___m_camera_2; }
	inline void set_m_camera_2(Camera_t189460977 * value)
	{
		___m_camera_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_camera_2), value);
	}

	inline static int32_t get_offset_of_m_session_3() { return static_cast<int32_t>(offsetof(AR3DOFCameraManager_t2152865733, ___m_session_3)); }
	inline UnityARSessionNativeInterface_t1130867170 * get_m_session_3() const { return ___m_session_3; }
	inline UnityARSessionNativeInterface_t1130867170 ** get_address_of_m_session_3() { return &___m_session_3; }
	inline void set_m_session_3(UnityARSessionNativeInterface_t1130867170 * value)
	{
		___m_session_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_session_3), value);
	}

	inline static int32_t get_offset_of_savedClearMaterial_4() { return static_cast<int32_t>(offsetof(AR3DOFCameraManager_t2152865733, ___savedClearMaterial_4)); }
	inline Material_t193706927 * get_savedClearMaterial_4() const { return ___savedClearMaterial_4; }
	inline Material_t193706927 ** get_address_of_savedClearMaterial_4() { return &___savedClearMaterial_4; }
	inline void set_savedClearMaterial_4(Material_t193706927 * value)
	{
		___savedClearMaterial_4 = value;
		Il2CppCodeGenWriteBarrier((&___savedClearMaterial_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AR3DOFCAMERAMANAGER_T2152865733_H
#ifndef UNITYARCAMERAMANAGER_T2138856896_H
#define UNITYARCAMERAMANAGER_T2138856896_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityARCameraManager
struct  UnityARCameraManager_t2138856896  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Camera UnityARCameraManager::m_camera
	Camera_t189460977 * ___m_camera_2;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface UnityARCameraManager::m_session
	UnityARSessionNativeInterface_t1130867170 * ___m_session_3;
	// UnityEngine.Material UnityARCameraManager::savedClearMaterial
	Material_t193706927 * ___savedClearMaterial_4;

public:
	inline static int32_t get_offset_of_m_camera_2() { return static_cast<int32_t>(offsetof(UnityARCameraManager_t2138856896, ___m_camera_2)); }
	inline Camera_t189460977 * get_m_camera_2() const { return ___m_camera_2; }
	inline Camera_t189460977 ** get_address_of_m_camera_2() { return &___m_camera_2; }
	inline void set_m_camera_2(Camera_t189460977 * value)
	{
		___m_camera_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_camera_2), value);
	}

	inline static int32_t get_offset_of_m_session_3() { return static_cast<int32_t>(offsetof(UnityARCameraManager_t2138856896, ___m_session_3)); }
	inline UnityARSessionNativeInterface_t1130867170 * get_m_session_3() const { return ___m_session_3; }
	inline UnityARSessionNativeInterface_t1130867170 ** get_address_of_m_session_3() { return &___m_session_3; }
	inline void set_m_session_3(UnityARSessionNativeInterface_t1130867170 * value)
	{
		___m_session_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_session_3), value);
	}

	inline static int32_t get_offset_of_savedClearMaterial_4() { return static_cast<int32_t>(offsetof(UnityARCameraManager_t2138856896, ___savedClearMaterial_4)); }
	inline Material_t193706927 * get_savedClearMaterial_4() const { return ___savedClearMaterial_4; }
	inline Material_t193706927 ** get_address_of_savedClearMaterial_4() { return &___savedClearMaterial_4; }
	inline void set_savedClearMaterial_4(Material_t193706927 * value)
	{
		___savedClearMaterial_4 = value;
		Il2CppCodeGenWriteBarrier((&___savedClearMaterial_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARCAMERAMANAGER_T2138856896_H
#ifndef UNITYPOINTCLOUDEXAMPLE_T3196264220_H
#define UNITYPOINTCLOUDEXAMPLE_T3196264220_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityPointCloudExample
struct  UnityPointCloudExample_t3196264220  : public MonoBehaviour_t1158329972
{
public:
	// System.UInt32 UnityPointCloudExample::numPointsToShow
	uint32_t ___numPointsToShow_2;
	// UnityEngine.GameObject UnityPointCloudExample::PointCloudPrefab
	GameObject_t1756533147 * ___PointCloudPrefab_3;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> UnityPointCloudExample::pointCloudObjects
	List_1_t1125654279 * ___pointCloudObjects_4;
	// UnityEngine.Vector3[] UnityPointCloudExample::m_PointCloudData
	Vector3U5BU5D_t1172311765* ___m_PointCloudData_5;

public:
	inline static int32_t get_offset_of_numPointsToShow_2() { return static_cast<int32_t>(offsetof(UnityPointCloudExample_t3196264220, ___numPointsToShow_2)); }
	inline uint32_t get_numPointsToShow_2() const { return ___numPointsToShow_2; }
	inline uint32_t* get_address_of_numPointsToShow_2() { return &___numPointsToShow_2; }
	inline void set_numPointsToShow_2(uint32_t value)
	{
		___numPointsToShow_2 = value;
	}

	inline static int32_t get_offset_of_PointCloudPrefab_3() { return static_cast<int32_t>(offsetof(UnityPointCloudExample_t3196264220, ___PointCloudPrefab_3)); }
	inline GameObject_t1756533147 * get_PointCloudPrefab_3() const { return ___PointCloudPrefab_3; }
	inline GameObject_t1756533147 ** get_address_of_PointCloudPrefab_3() { return &___PointCloudPrefab_3; }
	inline void set_PointCloudPrefab_3(GameObject_t1756533147 * value)
	{
		___PointCloudPrefab_3 = value;
		Il2CppCodeGenWriteBarrier((&___PointCloudPrefab_3), value);
	}

	inline static int32_t get_offset_of_pointCloudObjects_4() { return static_cast<int32_t>(offsetof(UnityPointCloudExample_t3196264220, ___pointCloudObjects_4)); }
	inline List_1_t1125654279 * get_pointCloudObjects_4() const { return ___pointCloudObjects_4; }
	inline List_1_t1125654279 ** get_address_of_pointCloudObjects_4() { return &___pointCloudObjects_4; }
	inline void set_pointCloudObjects_4(List_1_t1125654279 * value)
	{
		___pointCloudObjects_4 = value;
		Il2CppCodeGenWriteBarrier((&___pointCloudObjects_4), value);
	}

	inline static int32_t get_offset_of_m_PointCloudData_5() { return static_cast<int32_t>(offsetof(UnityPointCloudExample_t3196264220, ___m_PointCloudData_5)); }
	inline Vector3U5BU5D_t1172311765* get_m_PointCloudData_5() const { return ___m_PointCloudData_5; }
	inline Vector3U5BU5D_t1172311765** get_address_of_m_PointCloudData_5() { return &___m_PointCloudData_5; }
	inline void set_m_PointCloudData_5(Vector3U5BU5D_t1172311765* value)
	{
		___m_PointCloudData_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_PointCloudData_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYPOINTCLOUDEXAMPLE_T3196264220_H
#ifndef UNITYARHITTESTEXAMPLE_T146867607_H
#define UNITYARHITTESTEXAMPLE_T146867607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARHitTestExample
struct  UnityARHitTestExample_t146867607  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform UnityEngine.XR.iOS.UnityARHitTestExample::m_HitTransform
	Transform_t3275118058 * ___m_HitTransform_2;

public:
	inline static int32_t get_offset_of_m_HitTransform_2() { return static_cast<int32_t>(offsetof(UnityARHitTestExample_t146867607, ___m_HitTransform_2)); }
	inline Transform_t3275118058 * get_m_HitTransform_2() const { return ___m_HitTransform_2; }
	inline Transform_t3275118058 ** get_address_of_m_HitTransform_2() { return &___m_HitTransform_2; }
	inline void set_m_HitTransform_2(Transform_t3275118058 * value)
	{
		___m_HitTransform_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_HitTransform_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARHITTESTEXAMPLE_T146867607_H
#ifndef UNITYARKITCONTROL_T1698990409_H
#define UNITYARKITCONTROL_T1698990409_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARKitControl
struct  UnityARKitControl_t1698990409  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.XR.iOS.UnityARSessionRunOption[] UnityEngine.XR.iOS.UnityARKitControl::runOptions
	UnityARSessionRunOptionU5BU5D_t3114965901* ___runOptions_2;
	// UnityEngine.XR.iOS.UnityARAlignment[] UnityEngine.XR.iOS.UnityARKitControl::alignmentOptions
	UnityARAlignmentU5BU5D_t218994990* ___alignmentOptions_3;
	// UnityEngine.XR.iOS.UnityARPlaneDetection[] UnityEngine.XR.iOS.UnityARKitControl::planeOptions
	UnityARPlaneDetectionU5BU5D_t191549612* ___planeOptions_4;
	// System.Int32 UnityEngine.XR.iOS.UnityARKitControl::currentOptionIndex
	int32_t ___currentOptionIndex_5;
	// System.Int32 UnityEngine.XR.iOS.UnityARKitControl::currentAlignmentIndex
	int32_t ___currentAlignmentIndex_6;
	// System.Int32 UnityEngine.XR.iOS.UnityARKitControl::currentPlaneIndex
	int32_t ___currentPlaneIndex_7;

public:
	inline static int32_t get_offset_of_runOptions_2() { return static_cast<int32_t>(offsetof(UnityARKitControl_t1698990409, ___runOptions_2)); }
	inline UnityARSessionRunOptionU5BU5D_t3114965901* get_runOptions_2() const { return ___runOptions_2; }
	inline UnityARSessionRunOptionU5BU5D_t3114965901** get_address_of_runOptions_2() { return &___runOptions_2; }
	inline void set_runOptions_2(UnityARSessionRunOptionU5BU5D_t3114965901* value)
	{
		___runOptions_2 = value;
		Il2CppCodeGenWriteBarrier((&___runOptions_2), value);
	}

	inline static int32_t get_offset_of_alignmentOptions_3() { return static_cast<int32_t>(offsetof(UnityARKitControl_t1698990409, ___alignmentOptions_3)); }
	inline UnityARAlignmentU5BU5D_t218994990* get_alignmentOptions_3() const { return ___alignmentOptions_3; }
	inline UnityARAlignmentU5BU5D_t218994990** get_address_of_alignmentOptions_3() { return &___alignmentOptions_3; }
	inline void set_alignmentOptions_3(UnityARAlignmentU5BU5D_t218994990* value)
	{
		___alignmentOptions_3 = value;
		Il2CppCodeGenWriteBarrier((&___alignmentOptions_3), value);
	}

	inline static int32_t get_offset_of_planeOptions_4() { return static_cast<int32_t>(offsetof(UnityARKitControl_t1698990409, ___planeOptions_4)); }
	inline UnityARPlaneDetectionU5BU5D_t191549612* get_planeOptions_4() const { return ___planeOptions_4; }
	inline UnityARPlaneDetectionU5BU5D_t191549612** get_address_of_planeOptions_4() { return &___planeOptions_4; }
	inline void set_planeOptions_4(UnityARPlaneDetectionU5BU5D_t191549612* value)
	{
		___planeOptions_4 = value;
		Il2CppCodeGenWriteBarrier((&___planeOptions_4), value);
	}

	inline static int32_t get_offset_of_currentOptionIndex_5() { return static_cast<int32_t>(offsetof(UnityARKitControl_t1698990409, ___currentOptionIndex_5)); }
	inline int32_t get_currentOptionIndex_5() const { return ___currentOptionIndex_5; }
	inline int32_t* get_address_of_currentOptionIndex_5() { return &___currentOptionIndex_5; }
	inline void set_currentOptionIndex_5(int32_t value)
	{
		___currentOptionIndex_5 = value;
	}

	inline static int32_t get_offset_of_currentAlignmentIndex_6() { return static_cast<int32_t>(offsetof(UnityARKitControl_t1698990409, ___currentAlignmentIndex_6)); }
	inline int32_t get_currentAlignmentIndex_6() const { return ___currentAlignmentIndex_6; }
	inline int32_t* get_address_of_currentAlignmentIndex_6() { return &___currentAlignmentIndex_6; }
	inline void set_currentAlignmentIndex_6(int32_t value)
	{
		___currentAlignmentIndex_6 = value;
	}

	inline static int32_t get_offset_of_currentPlaneIndex_7() { return static_cast<int32_t>(offsetof(UnityARKitControl_t1698990409, ___currentPlaneIndex_7)); }
	inline int32_t get_currentPlaneIndex_7() const { return ___currentPlaneIndex_7; }
	inline int32_t* get_address_of_currentPlaneIndex_7() { return &___currentPlaneIndex_7; }
	inline void set_currentPlaneIndex_7(int32_t value)
	{
		___currentPlaneIndex_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARKITCONTROL_T1698990409_H
#ifndef UNITYARCAMERANEARFAR_T519802600_H
#define UNITYARCAMERANEARFAR_T519802600_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityARCameraNearFar
struct  UnityARCameraNearFar_t519802600  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Camera UnityARCameraNearFar::attachedCamera
	Camera_t189460977 * ___attachedCamera_2;
	// System.Single UnityARCameraNearFar::currentNearZ
	float ___currentNearZ_3;
	// System.Single UnityARCameraNearFar::currentFarZ
	float ___currentFarZ_4;

public:
	inline static int32_t get_offset_of_attachedCamera_2() { return static_cast<int32_t>(offsetof(UnityARCameraNearFar_t519802600, ___attachedCamera_2)); }
	inline Camera_t189460977 * get_attachedCamera_2() const { return ___attachedCamera_2; }
	inline Camera_t189460977 ** get_address_of_attachedCamera_2() { return &___attachedCamera_2; }
	inline void set_attachedCamera_2(Camera_t189460977 * value)
	{
		___attachedCamera_2 = value;
		Il2CppCodeGenWriteBarrier((&___attachedCamera_2), value);
	}

	inline static int32_t get_offset_of_currentNearZ_3() { return static_cast<int32_t>(offsetof(UnityARCameraNearFar_t519802600, ___currentNearZ_3)); }
	inline float get_currentNearZ_3() const { return ___currentNearZ_3; }
	inline float* get_address_of_currentNearZ_3() { return &___currentNearZ_3; }
	inline void set_currentNearZ_3(float value)
	{
		___currentNearZ_3 = value;
	}

	inline static int32_t get_offset_of_currentFarZ_4() { return static_cast<int32_t>(offsetof(UnityARCameraNearFar_t519802600, ___currentFarZ_4)); }
	inline float get_currentFarZ_4() const { return ___currentFarZ_4; }
	inline float* get_address_of_currentFarZ_4() { return &___currentFarZ_4; }
	inline void set_currentFarZ_4(float value)
	{
		___currentFarZ_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARCAMERANEARFAR_T519802600_H
#ifndef POINTCLOUDPARTICLEEXAMPLE_T986756623_H
#define POINTCLOUDPARTICLEEXAMPLE_T986756623_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PointCloudParticleExample
struct  PointCloudParticleExample_t986756623  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.ParticleSystem PointCloudParticleExample::pointCloudParticlePrefab
	ParticleSystem_t3394631041 * ___pointCloudParticlePrefab_2;
	// System.Int32 PointCloudParticleExample::maxPointsToShow
	int32_t ___maxPointsToShow_3;
	// System.Single PointCloudParticleExample::particleSize
	float ___particleSize_4;
	// UnityEngine.Vector3[] PointCloudParticleExample::m_PointCloudData
	Vector3U5BU5D_t1172311765* ___m_PointCloudData_5;
	// System.Boolean PointCloudParticleExample::frameUpdated
	bool ___frameUpdated_6;
	// UnityEngine.ParticleSystem PointCloudParticleExample::currentPS
	ParticleSystem_t3394631041 * ___currentPS_7;
	// UnityEngine.ParticleSystem/Particle[] PointCloudParticleExample::particles
	ParticleU5BU5D_t574222242* ___particles_8;

public:
	inline static int32_t get_offset_of_pointCloudParticlePrefab_2() { return static_cast<int32_t>(offsetof(PointCloudParticleExample_t986756623, ___pointCloudParticlePrefab_2)); }
	inline ParticleSystem_t3394631041 * get_pointCloudParticlePrefab_2() const { return ___pointCloudParticlePrefab_2; }
	inline ParticleSystem_t3394631041 ** get_address_of_pointCloudParticlePrefab_2() { return &___pointCloudParticlePrefab_2; }
	inline void set_pointCloudParticlePrefab_2(ParticleSystem_t3394631041 * value)
	{
		___pointCloudParticlePrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___pointCloudParticlePrefab_2), value);
	}

	inline static int32_t get_offset_of_maxPointsToShow_3() { return static_cast<int32_t>(offsetof(PointCloudParticleExample_t986756623, ___maxPointsToShow_3)); }
	inline int32_t get_maxPointsToShow_3() const { return ___maxPointsToShow_3; }
	inline int32_t* get_address_of_maxPointsToShow_3() { return &___maxPointsToShow_3; }
	inline void set_maxPointsToShow_3(int32_t value)
	{
		___maxPointsToShow_3 = value;
	}

	inline static int32_t get_offset_of_particleSize_4() { return static_cast<int32_t>(offsetof(PointCloudParticleExample_t986756623, ___particleSize_4)); }
	inline float get_particleSize_4() const { return ___particleSize_4; }
	inline float* get_address_of_particleSize_4() { return &___particleSize_4; }
	inline void set_particleSize_4(float value)
	{
		___particleSize_4 = value;
	}

	inline static int32_t get_offset_of_m_PointCloudData_5() { return static_cast<int32_t>(offsetof(PointCloudParticleExample_t986756623, ___m_PointCloudData_5)); }
	inline Vector3U5BU5D_t1172311765* get_m_PointCloudData_5() const { return ___m_PointCloudData_5; }
	inline Vector3U5BU5D_t1172311765** get_address_of_m_PointCloudData_5() { return &___m_PointCloudData_5; }
	inline void set_m_PointCloudData_5(Vector3U5BU5D_t1172311765* value)
	{
		___m_PointCloudData_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_PointCloudData_5), value);
	}

	inline static int32_t get_offset_of_frameUpdated_6() { return static_cast<int32_t>(offsetof(PointCloudParticleExample_t986756623, ___frameUpdated_6)); }
	inline bool get_frameUpdated_6() const { return ___frameUpdated_6; }
	inline bool* get_address_of_frameUpdated_6() { return &___frameUpdated_6; }
	inline void set_frameUpdated_6(bool value)
	{
		___frameUpdated_6 = value;
	}

	inline static int32_t get_offset_of_currentPS_7() { return static_cast<int32_t>(offsetof(PointCloudParticleExample_t986756623, ___currentPS_7)); }
	inline ParticleSystem_t3394631041 * get_currentPS_7() const { return ___currentPS_7; }
	inline ParticleSystem_t3394631041 ** get_address_of_currentPS_7() { return &___currentPS_7; }
	inline void set_currentPS_7(ParticleSystem_t3394631041 * value)
	{
		___currentPS_7 = value;
		Il2CppCodeGenWriteBarrier((&___currentPS_7), value);
	}

	inline static int32_t get_offset_of_particles_8() { return static_cast<int32_t>(offsetof(PointCloudParticleExample_t986756623, ___particles_8)); }
	inline ParticleU5BU5D_t574222242* get_particles_8() const { return ___particles_8; }
	inline ParticleU5BU5D_t574222242** get_address_of_particles_8() { return &___particles_8; }
	inline void set_particles_8(ParticleU5BU5D_t574222242* value)
	{
		___particles_8 = value;
		Il2CppCodeGenWriteBarrier((&___particles_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTCLOUDPARTICLEEXAMPLE_T986756623_H
#ifndef UNITYARVIDEO_T2351297253_H
#define UNITYARVIDEO_T2351297253_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARVideo
struct  UnityARVideo_t2351297253  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Material UnityEngine.XR.iOS.UnityARVideo::m_ClearMaterial
	Material_t193706927 * ___m_ClearMaterial_2;
	// UnityEngine.Rendering.CommandBuffer UnityEngine.XR.iOS.UnityARVideo::m_VideoCommandBuffer
	CommandBuffer_t1204166949 * ___m_VideoCommandBuffer_3;
	// UnityEngine.Texture2D UnityEngine.XR.iOS.UnityARVideo::_videoTextureY
	Texture2D_t3542995729 * ____videoTextureY_4;
	// UnityEngine.Texture2D UnityEngine.XR.iOS.UnityARVideo::_videoTextureCbCr
	Texture2D_t3542995729 * ____videoTextureCbCr_5;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface UnityEngine.XR.iOS.UnityARVideo::m_Session
	UnityARSessionNativeInterface_t1130867170 * ___m_Session_6;
	// System.Boolean UnityEngine.XR.iOS.UnityARVideo::bCommandBufferInitialized
	bool ___bCommandBufferInitialized_7;

public:
	inline static int32_t get_offset_of_m_ClearMaterial_2() { return static_cast<int32_t>(offsetof(UnityARVideo_t2351297253, ___m_ClearMaterial_2)); }
	inline Material_t193706927 * get_m_ClearMaterial_2() const { return ___m_ClearMaterial_2; }
	inline Material_t193706927 ** get_address_of_m_ClearMaterial_2() { return &___m_ClearMaterial_2; }
	inline void set_m_ClearMaterial_2(Material_t193706927 * value)
	{
		___m_ClearMaterial_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ClearMaterial_2), value);
	}

	inline static int32_t get_offset_of_m_VideoCommandBuffer_3() { return static_cast<int32_t>(offsetof(UnityARVideo_t2351297253, ___m_VideoCommandBuffer_3)); }
	inline CommandBuffer_t1204166949 * get_m_VideoCommandBuffer_3() const { return ___m_VideoCommandBuffer_3; }
	inline CommandBuffer_t1204166949 ** get_address_of_m_VideoCommandBuffer_3() { return &___m_VideoCommandBuffer_3; }
	inline void set_m_VideoCommandBuffer_3(CommandBuffer_t1204166949 * value)
	{
		___m_VideoCommandBuffer_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_VideoCommandBuffer_3), value);
	}

	inline static int32_t get_offset_of__videoTextureY_4() { return static_cast<int32_t>(offsetof(UnityARVideo_t2351297253, ____videoTextureY_4)); }
	inline Texture2D_t3542995729 * get__videoTextureY_4() const { return ____videoTextureY_4; }
	inline Texture2D_t3542995729 ** get_address_of__videoTextureY_4() { return &____videoTextureY_4; }
	inline void set__videoTextureY_4(Texture2D_t3542995729 * value)
	{
		____videoTextureY_4 = value;
		Il2CppCodeGenWriteBarrier((&____videoTextureY_4), value);
	}

	inline static int32_t get_offset_of__videoTextureCbCr_5() { return static_cast<int32_t>(offsetof(UnityARVideo_t2351297253, ____videoTextureCbCr_5)); }
	inline Texture2D_t3542995729 * get__videoTextureCbCr_5() const { return ____videoTextureCbCr_5; }
	inline Texture2D_t3542995729 ** get_address_of__videoTextureCbCr_5() { return &____videoTextureCbCr_5; }
	inline void set__videoTextureCbCr_5(Texture2D_t3542995729 * value)
	{
		____videoTextureCbCr_5 = value;
		Il2CppCodeGenWriteBarrier((&____videoTextureCbCr_5), value);
	}

	inline static int32_t get_offset_of_m_Session_6() { return static_cast<int32_t>(offsetof(UnityARVideo_t2351297253, ___m_Session_6)); }
	inline UnityARSessionNativeInterface_t1130867170 * get_m_Session_6() const { return ___m_Session_6; }
	inline UnityARSessionNativeInterface_t1130867170 ** get_address_of_m_Session_6() { return &___m_Session_6; }
	inline void set_m_Session_6(UnityARSessionNativeInterface_t1130867170 * value)
	{
		___m_Session_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Session_6), value);
	}

	inline static int32_t get_offset_of_bCommandBufferInitialized_7() { return static_cast<int32_t>(offsetof(UnityARVideo_t2351297253, ___bCommandBufferInitialized_7)); }
	inline bool get_bCommandBufferInitialized_7() const { return ___bCommandBufferInitialized_7; }
	inline bool* get_address_of_bCommandBufferInitialized_7() { return &___bCommandBufferInitialized_7; }
	inline void set_bCommandBufferInitialized_7(bool value)
	{
		___bCommandBufferInitialized_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARVIDEO_T2351297253_H
#ifndef UNITYARGENERATEPLANE_T3368998101_H
#define UNITYARGENERATEPLANE_T3368998101_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARGeneratePlane
struct  UnityARGeneratePlane_t3368998101  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject UnityEngine.XR.iOS.UnityARGeneratePlane::planePrefab
	GameObject_t1756533147 * ___planePrefab_2;
	// UnityEngine.XR.iOS.UnityARAnchorManager UnityEngine.XR.iOS.UnityARGeneratePlane::unityARAnchorManager
	UnityARAnchorManager_t1086564192 * ___unityARAnchorManager_3;

public:
	inline static int32_t get_offset_of_planePrefab_2() { return static_cast<int32_t>(offsetof(UnityARGeneratePlane_t3368998101, ___planePrefab_2)); }
	inline GameObject_t1756533147 * get_planePrefab_2() const { return ___planePrefab_2; }
	inline GameObject_t1756533147 ** get_address_of_planePrefab_2() { return &___planePrefab_2; }
	inline void set_planePrefab_2(GameObject_t1756533147 * value)
	{
		___planePrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___planePrefab_2), value);
	}

	inline static int32_t get_offset_of_unityARAnchorManager_3() { return static_cast<int32_t>(offsetof(UnityARGeneratePlane_t3368998101, ___unityARAnchorManager_3)); }
	inline UnityARAnchorManager_t1086564192 * get_unityARAnchorManager_3() const { return ___unityARAnchorManager_3; }
	inline UnityARAnchorManager_t1086564192 ** get_address_of_unityARAnchorManager_3() { return &___unityARAnchorManager_3; }
	inline void set_unityARAnchorManager_3(UnityARAnchorManager_t1086564192 * value)
	{
		___unityARAnchorManager_3 = value;
		Il2CppCodeGenWriteBarrier((&___unityARAnchorManager_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARGENERATEPLANE_T3368998101_H
#ifndef DONTDESTROYONLOAD_T3235789354_H
#define DONTDESTROYONLOAD_T3235789354_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DontDestroyOnLoad
struct  DontDestroyOnLoad_t3235789354  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DONTDESTROYONLOAD_T3235789354_H
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector3_t2243707580  m_Items[1];

public:
	inline Vector3_t2243707580  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector3_t2243707580 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector3_t2243707580  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector3_t2243707580  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector3_t2243707580 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector3_t2243707580  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.ParticleSystem/Particle[]
struct ParticleU5BU5D_t574222242  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Particle_t250075699  m_Items[1];

public:
	inline Particle_t250075699  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Particle_t250075699 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Particle_t250075699  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Particle_t250075699  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Particle_t250075699 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Particle_t250075699  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.XR.iOS.ARHitTestResultType[]
struct ARHitTestResultTypeU5BU5D_t1303085420  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int64_t m_Items[1];

public:
	inline int64_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int64_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int64_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int64_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int64_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int64_t value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.XR.iOS.UnityARSessionRunOption[]
struct UnityARSessionRunOptionU5BU5D_t3114965901  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.XR.iOS.UnityARAlignment[]
struct UnityARAlignmentU5BU5D_t218994990  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.XR.iOS.UnityARPlaneDetection[]
struct UnityARPlaneDetectionU5BU5D_t191549612  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// System.Single[]
struct SingleU5BU5D_t577127397  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) float m_Items[1];

public:
	inline float GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline float* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, float value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline float GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline float* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, float value)
	{
		m_Items[index] = value;
	}
};

extern "C" void ARKitWorldTackingSessionConfiguration_t1821734930_marshal_pinvoke(const ARKitWorldTackingSessionConfiguration_t1821734930& unmarshaled, ARKitWorldTackingSessionConfiguration_t1821734930_marshaled_pinvoke& marshaled);
extern "C" void ARKitWorldTackingSessionConfiguration_t1821734930_marshal_pinvoke_back(const ARKitWorldTackingSessionConfiguration_t1821734930_marshaled_pinvoke& marshaled, ARKitWorldTackingSessionConfiguration_t1821734930& unmarshaled);
extern "C" void ARKitWorldTackingSessionConfiguration_t1821734930_marshal_pinvoke_cleanup(ARKitWorldTackingSessionConfiguration_t1821734930_marshaled_pinvoke& marshaled);
extern "C" void ARKitSessionConfiguration_t318899795_marshal_pinvoke(const ARKitSessionConfiguration_t318899795& unmarshaled, ARKitSessionConfiguration_t318899795_marshaled_pinvoke& marshaled);
extern "C" void ARKitSessionConfiguration_t318899795_marshal_pinvoke_back(const ARKitSessionConfiguration_t318899795_marshaled_pinvoke& marshaled, ARKitSessionConfiguration_t318899795& unmarshaled);
extern "C" void ARKitSessionConfiguration_t318899795_marshal_pinvoke_cleanup(ARKitSessionConfiguration_t318899795_marshaled_pinvoke& marshaled);
extern "C" void UnityARHitTestResult_t4129824344_marshal_pinvoke(const UnityARHitTestResult_t4129824344& unmarshaled, UnityARHitTestResult_t4129824344_marshaled_pinvoke& marshaled);
extern "C" void UnityARHitTestResult_t4129824344_marshal_pinvoke_back(const UnityARHitTestResult_t4129824344_marshaled_pinvoke& marshaled, UnityARHitTestResult_t4129824344& unmarshaled);
extern "C" void UnityARHitTestResult_t4129824344_marshal_pinvoke_cleanup(UnityARHitTestResult_t4129824344_marshaled_pinvoke& marshaled);
extern "C" void ARPlaneAnchor_t1439520888_marshal_pinvoke(const ARPlaneAnchor_t1439520888& unmarshaled, ARPlaneAnchor_t1439520888_marshaled_pinvoke& marshaled);
extern "C" void ARPlaneAnchor_t1439520888_marshal_pinvoke_back(const ARPlaneAnchor_t1439520888_marshaled_pinvoke& marshaled, ARPlaneAnchor_t1439520888& unmarshaled);
extern "C" void ARPlaneAnchor_t1439520888_marshal_pinvoke_cleanup(ARPlaneAnchor_t1439520888_marshaled_pinvoke& marshaled);

// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  RuntimeObject * GameObject_GetComponent_TisRuntimeObject_m2812611596_gshared (GameObject_t1756533147 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C"  RuntimeObject * GameObject_AddComponent_TisRuntimeObject_m3813873105_gshared (GameObject_t1756533147 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0)
extern "C"  RuntimeObject * Object_Instantiate_TisRuntimeObject_m447919519_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  RuntimeObject * Component_GetComponent_TisRuntimeObject_m4109961936_gshared (Component_t3819376471 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m584589095_gshared (Dictionary_2_t2281509423 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Add(!0,!1)
extern "C"  void Dictionary_2_Add_m4209421183_gshared (Dictionary_2_t2281509423 * __this, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::ContainsKey(!0)
extern "C"  bool Dictionary_2_ContainsKey_m3321918434_gshared (Dictionary_2_t2281509423 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// !1 System.Collections.Generic.Dictionary`2<System.Object,System.Object>::get_Item(!0)
extern "C"  RuntimeObject * Dictionary_2_get_Item_m4062719145_gshared (Dictionary_2_t2281509423 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Remove(!0)
extern "C"  bool Dictionary_2_Remove_m112127646_gshared (Dictionary_2_t2281509423 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::set_Item(!0,!1)
extern "C"  void Dictionary_2_set_Item_m1004257024_gshared (Dictionary_2_t2281509423 * __this, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
extern "C"  Enumerator_t1593300101  List_1_GetEnumerator_m2837081829_gshared (List_1_t2058570427 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
extern "C"  RuntimeObject * Enumerator_get_Current_m2577424081_gshared (Enumerator_t1593300101 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m44995089_gshared (Enumerator_t1593300101 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3736175406_gshared (Enumerator_t1593300101 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Clear()
extern "C"  void Dictionary_2_Clear_m2325793156_gshared (Dictionary_2_t2281509423 * __this, const RuntimeMethod* method);
// System.Collections.Generic.Dictionary`2/ValueCollection<!0,!1> System.Collections.Generic.Dictionary`2<System.Object,System.Object>::get_Values()
extern "C"  ValueCollection_t984569266 * Dictionary_2_get_Values_m2233445381_gshared (Dictionary_2_t2281509423 * __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  List_1_t2058570427 * Enumerable_ToList_TisRuntimeObject_m3492391627_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* p0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C"  int32_t List_1_get_Count_m2375293942_gshared (List_1_t2058570427 * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<UnityEngine.XR.iOS.ARHitTestResult>::get_Count()
extern "C"  int32_t List_1_get_Count_m1598304542_gshared (List_1_t2644634157 * __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<UnityEngine.XR.iOS.ARHitTestResult>::GetEnumerator()
extern "C"  Enumerator_t2179363831  List_1_GetEnumerator_m2885046859_gshared (List_1_t2644634157 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.XR.iOS.ARHitTestResult>::get_Current()
extern "C"  ARHitTestResult_t3275513025  Enumerator_get_Current_m2775711191_gshared (Enumerator_t2179363831 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.XR.iOS.ARHitTestResult>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3661301011_gshared (Enumerator_t2179363831 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.XR.iOS.ARHitTestResult>::Dispose()
extern "C"  void Enumerator_Dispose_m3109677227_gshared (Enumerator_t2179363831 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.XR.iOS.ARHitTestResult>::.ctor()
extern "C"  void List_1__ctor_m3856316316_gshared (List_1_t2644634157 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.XR.iOS.ARHitTestResult>::Add(!0)
extern "C"  void List_1_Add_m1918634088_gshared (List_1_t2644634157 * __this, ARHitTestResult_t3275513025  p0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponentInChildren<System.Object>()
extern "C"  RuntimeObject * GameObject_GetComponentInChildren_TisRuntimeObject_m327292296_gshared (GameObject_t1756533147 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C"  void List_1__ctor_m310736118_gshared (List_1_t2058570427 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
extern "C"  void List_1_Add_m4157722533_gshared (List_1_t2058570427 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern "C"  RuntimeObject * List_1_get_Item_m2062981835_gshared (List_1_t2058570427 * __this, int32_t p0, const RuntimeMethod* method);

// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m2464341955 (MonoBehaviour_t1158329972 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Application::set_targetFrameRate(System.Int32)
extern "C"  void Application_set_targetFrameRate_m2941880625 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface UnityEngine.XR.iOS.UnityARSessionNativeInterface::GetARSessionNativeInterface()
extern "C"  UnityARSessionNativeInterface_t1130867170 * UnityARSessionNativeInterface_GetARSessionNativeInterface_m3174488657 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::RunWithConfig(UnityEngine.XR.iOS.ARKitSessionConfiguration)
extern "C"  void UnityARSessionNativeInterface_RunWithConfig_m2478060541 (UnityARSessionNativeInterface_t1130867170 * __this, ARKitSessionConfiguration_t318899795  ___config0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m3764089466 (RuntimeObject * __this /* static, unused */, Object_t1021602117 * p0, Object_t1021602117 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera UnityEngine.Camera::get_main()
extern "C"  Camera_t189460977 * Camera_get_main_m475173995 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m2402264703 (RuntimeObject * __this /* static, unused */, Object_t1021602117 * p0, Object_t1021602117 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t1756533147 * Component_get_gameObject_m3105766835 (Component_t3819376471 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.XR.iOS.UnityARVideo>()
#define GameObject_GetComponent_TisUnityARVideo_t2351297253_m2230129522(__this, method) ((  UnityARVideo_t2351297253 * (*) (GameObject_t1756533147 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2812611596_gshared)(__this, method)
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern "C"  void Object_Destroy_m4145850038 (RuntimeObject * __this /* static, unused */, Object_t1021602117 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void AR3DOFCameraManager::SetupNewCamera(UnityEngine.Camera)
extern "C"  void AR3DOFCameraManager_SetupNewCamera_m439842942 (AR3DOFCameraManager_t2152865733 * __this, Camera_t189460977 * ___newCamera0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.XR.iOS.UnityARVideo>()
#define GameObject_AddComponent_TisUnityARVideo_t2351297253_m1329128087(__this, method) ((  UnityARVideo_t2351297253 * (*) (GameObject_t1756533147 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m3813873105_gshared)(__this, method)
// UnityEngine.Matrix4x4 UnityEngine.XR.iOS.UnityARSessionNativeInterface::GetCameraPose()
extern "C"  Matrix4x4_t2933234003  UnityARSessionNativeInterface_GetCameraPose_m3046824030 (UnityARSessionNativeInterface_t1130867170 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C"  Transform_t3275118058 * Component_get_transform_m2697483695 (Component_t3819376471 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.XR.iOS.UnityARMatrixOps::GetPosition(UnityEngine.Matrix4x4)
extern "C"  Vector3_t2243707580  UnityARMatrixOps_GetPosition_m1153858439 (RuntimeObject * __this /* static, unused */, Matrix4x4_t2933234003  ___matrix0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_localPosition(UnityEngine.Vector3)
extern "C"  void Transform_set_localPosition_m1026930133 (Transform_t3275118058 * __this, Vector3_t2243707580  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.XR.iOS.UnityARMatrixOps::GetRotation(UnityEngine.Matrix4x4)
extern "C"  Quaternion_t4030073918  UnityARMatrixOps_GetRotation_m1002641986 (RuntimeObject * __this /* static, unused */, Matrix4x4_t2933234003  ___matrix0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_localRotation(UnityEngine.Quaternion)
extern "C"  void Transform_set_localRotation_m2055111962 (Transform_t3275118058 * __this, Quaternion_t4030073918  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 UnityEngine.XR.iOS.UnityARSessionNativeInterface::GetCameraProjection()
extern "C"  Matrix4x4_t2933234003  UnityARSessionNativeInterface_GetCameraProjection_m3168017698 (UnityARSessionNativeInterface_t1130867170 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::set_projectionMatrix(UnityEngine.Matrix4x4)
extern "C"  void Camera_set_projectionMatrix_m2059836755 (Camera_t189460977 * __this, Matrix4x4_t2933234003  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)
extern "C"  void Object_DontDestroyOnLoad_m2330762974 (RuntimeObject * __this /* static, unused */, Object_t1021602117 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARFrameUpdate::.ctor(System.Object,System.IntPtr)
extern "C"  void ARFrameUpdate__ctor_m1399217559 (ARFrameUpdate_t496507918 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::add_ARFrameUpdatedEvent(UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARFrameUpdate)
extern "C"  void UnityARSessionNativeInterface_add_ARFrameUpdatedEvent_m2850773202 (RuntimeObject * __this /* static, unused */, ARFrameUpdate_t496507918 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Object::Instantiate<UnityEngine.ParticleSystem>(!!0)
#define Object_Instantiate_TisParticleSystem_t3394631041_m4293040502(__this /* static, unused */, p0, method) ((  ParticleSystem_t3394631041 * (*) (RuntimeObject * /* static, unused */, ParticleSystem_t3394631041 *, const RuntimeMethod*))Object_Instantiate_TisRuntimeObject_m447919519_gshared)(__this /* static, unused */, p0, method)
// System.Int32 UnityEngine.Mathf::Min(System.Int32,System.Int32)
extern "C"  int32_t Mathf_Min_m2906823867 (RuntimeObject * __this /* static, unused */, int32_t p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/Particle::set_position(UnityEngine.Vector3)
extern "C"  void Particle_set_position_m3680513126 (Particle_t250075699 * __this, Vector3_t2243707580  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Color__ctor_m3811852957 (Color_t2020392075 * __this, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color32 UnityEngine.Color32::op_Implicit(UnityEngine.Color)
extern "C"  Color32_t874517518  Color32_op_Implicit_m624191464 (RuntimeObject * __this /* static, unused */, Color_t2020392075  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/Particle::set_startColor(UnityEngine.Color32)
extern "C"  void Particle_set_startColor_m3936512348 (Particle_t250075699 * __this, Color32_t874517518  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/Particle::set_startSize(System.Single)
extern "C"  void Particle_set_startSize_m2457836830 (Particle_t250075699 * __this, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::SetParticles(UnityEngine.ParticleSystem/Particle[],System.Int32)
extern "C"  void ParticleSystem_SetParticles_m3035584975 (ParticleSystem_t3394631041 * __this, ParticleU5BU5D_t574222242* p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::RunWithConfig(UnityEngine.XR.iOS.ARKitWorldTackingSessionConfiguration)
extern "C"  void UnityARSessionNativeInterface_RunWithConfig_m3195925270 (UnityARSessionNativeInterface_t1130867170 * __this, ARKitWorldTackingSessionConfiguration_t1821734930  ___config0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityARCameraManager::SetupNewCamera(UnityEngine.Camera)
extern "C"  void UnityARCameraManager_SetupNewCamera_m2358778511 (UnityARCameraManager_t2138856896 * __this, Camera_t189460977 * ___newCamera0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Camera>()
#define Component_GetComponent_TisCamera_t189460977_m3276577584(__this, method) ((  Camera_t189460977 * (*) (Component_t3819376471 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m4109961936_gshared)(__this, method)
// System.Void UnityARCameraNearFar::UpdateCameraClipPlanes()
extern "C"  void UnityARCameraNearFar_UpdateCameraClipPlanes_m887086422 (UnityARCameraNearFar_t519802600 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Camera::get_nearClipPlane()
extern "C"  float Camera_get_nearClipPlane_m3536967407 (Camera_t189460977 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Camera::get_farClipPlane()
extern "C"  float Camera_get_farClipPlane_m3137713566 (Camera_t189460977 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::SetCameraClipPlanes(System.Single,System.Single)
extern "C"  void UnityARSessionNativeInterface_SetCameraClipPlanes_m149558303 (UnityARSessionNativeInterface_t1130867170 * __this, float ___nearZ0, float ___farZ1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.ARKitSessionConfiguration::.ctor(UnityEngine.XR.iOS.UnityARAlignment,System.Boolean,System.Boolean)
extern "C"  void ARKitSessionConfiguration__ctor_m3221520625 (ARKitSessionConfiguration_t318899795 * __this, int32_t ___alignment0, bool ___getPointCloudData1, bool ___enableLightEstimation2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.XR.iOS.ARKitSessionConfiguration::IsARKitSessionConfigurationSupported()
extern "C"  bool ARKitSessionConfiguration_IsARKitSessionConfigurationSupported_m3959947945 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.XR.iOS.ARKitSessionConfiguration::get_IsSupported()
extern "C"  bool ARKitSessionConfiguration_get_IsSupported_m919877731 (ARKitSessionConfiguration_t318899795 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.ARKitSessionConfiguration::set_IsSupported(System.Boolean)
extern "C"  void ARKitSessionConfiguration_set_IsSupported_m138323976 (ARKitSessionConfiguration_t318899795 * __this, bool ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.ARKitWorldTackingSessionConfiguration::.ctor(UnityEngine.XR.iOS.UnityARAlignment,UnityEngine.XR.iOS.UnityARPlaneDetection,System.Boolean,System.Boolean)
extern "C"  void ARKitWorldTackingSessionConfiguration__ctor_m4281543687 (ARKitWorldTackingSessionConfiguration_t1821734930 * __this, int32_t ___alignment0, int32_t ___planeDetection1, bool ___getPointCloudData2, bool ___enableLightEstimation3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.XR.iOS.ARKitWorldTackingSessionConfiguration::IsARKitWorldTrackingSessionConfigurationSupported()
extern "C"  bool ARKitWorldTackingSessionConfiguration_IsARKitWorldTrackingSessionConfigurationSupported_m34846127 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.XR.iOS.ARKitWorldTackingSessionConfiguration::get_IsSupported()
extern "C"  bool ARKitWorldTackingSessionConfiguration_get_IsSupported_m3789991538 (ARKitWorldTackingSessionConfiguration_t1821734930 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.ARKitWorldTackingSessionConfiguration::set_IsSupported(System.Boolean)
extern "C"  void ARKitWorldTackingSessionConfiguration_set_IsSupported_m46319519 (ARKitWorldTackingSessionConfiguration_t1821734930 * __this, bool ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m2551263788 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.XR.iOS.ARPlaneAnchorGameObject>::.ctor()
#define Dictionary_2__ctor_m1722416527(__this, method) ((  void (*) (Dictionary_2_t4220005149 *, const RuntimeMethod*))Dictionary_2__ctor_m584589095_gshared)(__this, method)
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorAdded::.ctor(System.Object,System.IntPtr)
extern "C"  void ARAnchorAdded__ctor_m3844186700 (ARAnchorAdded_t2646854145 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::add_ARAnchorAddedEvent(UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorAdded)
extern "C"  void UnityARSessionNativeInterface_add_ARAnchorAddedEvent_m658032036 (RuntimeObject * __this /* static, unused */, ARAnchorAdded_t2646854145 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorUpdated::.ctor(System.Object,System.IntPtr)
extern "C"  void ARAnchorUpdated__ctor_m1158457407 (ARAnchorUpdated_t3886071158 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::add_ARAnchorUpdatedEvent(UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorUpdated)
extern "C"  void UnityARSessionNativeInterface_add_ARAnchorUpdatedEvent_m1485763364 (RuntimeObject * __this /* static, unused */, ARAnchorUpdated_t3886071158 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorRemoved::.ctor(System.Object,System.IntPtr)
extern "C"  void ARAnchorRemoved__ctor_m535635486 (ARAnchorRemoved_t142665927 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::add_ARAnchorRemovedEvent(UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorRemoved)
extern "C"  void UnityARSessionNativeInterface_add_ARAnchorRemovedEvent_m2970646020 (RuntimeObject * __this /* static, unused */, ARAnchorRemoved_t142665927 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.XR.iOS.UnityARUtility::CreatePlaneInScene(UnityEngine.XR.iOS.ARPlaneAnchor)
extern "C"  GameObject_t1756533147 * UnityARUtility_CreatePlaneInScene_m836370693 (RuntimeObject * __this /* static, unused */, ARPlaneAnchor_t1439520888  ___arPlaneAnchor0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::AddComponent<DontDestroyOnLoad>()
#define GameObject_AddComponent_TisDontDestroyOnLoad_t3235789354_m4215585572(__this, method) ((  DontDestroyOnLoad_t3235789354 * (*) (GameObject_t1756533147 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m3813873105_gshared)(__this, method)
// System.Void UnityEngine.XR.iOS.ARPlaneAnchorGameObject::.ctor()
extern "C"  void ARPlaneAnchorGameObject__ctor_m3751462836 (ARPlaneAnchorGameObject_t2305225887 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.XR.iOS.ARPlaneAnchorGameObject>::Add(!0,!1)
#define Dictionary_2_Add_m2748008583(__this, p0, p1, method) ((  void (*) (Dictionary_2_t4220005149 *, String_t*, ARPlaneAnchorGameObject_t2305225887 *, const RuntimeMethod*))Dictionary_2_Add_m4209421183_gshared)(__this, p0, p1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityEngine.XR.iOS.ARPlaneAnchorGameObject>::ContainsKey(!0)
#define Dictionary_2_ContainsKey_m9048208(__this, p0, method) ((  bool (*) (Dictionary_2_t4220005149 *, String_t*, const RuntimeMethod*))Dictionary_2_ContainsKey_m3321918434_gshared)(__this, p0, method)
// !1 System.Collections.Generic.Dictionary`2<System.String,UnityEngine.XR.iOS.ARPlaneAnchorGameObject>::get_Item(!0)
#define Dictionary_2_get_Item_m4111408485(__this, p0, method) ((  ARPlaneAnchorGameObject_t2305225887 * (*) (Dictionary_2_t4220005149 *, String_t*, const RuntimeMethod*))Dictionary_2_get_Item_m4062719145_gshared)(__this, p0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityEngine.XR.iOS.ARPlaneAnchorGameObject>::Remove(!0)
#define Dictionary_2_Remove_m2895225056(__this, p0, method) ((  bool (*) (Dictionary_2_t4220005149 *, String_t*, const RuntimeMethod*))Dictionary_2_Remove_m112127646_gshared)(__this, p0, method)
// UnityEngine.GameObject UnityEngine.XR.iOS.UnityARUtility::UpdatePlaneWithAnchorTransform(UnityEngine.GameObject,UnityEngine.XR.iOS.ARPlaneAnchor)
extern "C"  GameObject_t1756533147 * UnityARUtility_UpdatePlaneWithAnchorTransform_m639257622 (RuntimeObject * __this /* static, unused */, GameObject_t1756533147 * ___plane0, ARPlaneAnchor_t1439520888  ___arPlaneAnchor1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.XR.iOS.ARPlaneAnchorGameObject>::set_Item(!0,!1)
#define Dictionary_2_set_Item_m3140527944(__this, p0, p1, method) ((  void (*) (Dictionary_2_t4220005149 *, String_t*, ARPlaneAnchorGameObject_t2305225887 *, const RuntimeMethod*))Dictionary_2_set_Item_m1004257024_gshared)(__this, p0, p1, method)
// System.Collections.Generic.List`1<UnityEngine.XR.iOS.ARPlaneAnchorGameObject> UnityEngine.XR.iOS.UnityARAnchorManager::GetCurrentPlaneAnchors()
extern "C"  List_1_t1674347019 * UnityARAnchorManager_GetCurrentPlaneAnchors_m642073964 (UnityARAnchorManager_t1086564192 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<UnityEngine.XR.iOS.ARPlaneAnchorGameObject>::GetEnumerator()
#define List_1_GetEnumerator_m4260200565(__this, method) ((  Enumerator_t1209076693  (*) (List_1_t1674347019 *, const RuntimeMethod*))List_1_GetEnumerator_m2837081829_gshared)(__this, method)
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.XR.iOS.ARPlaneAnchorGameObject>::get_Current()
#define Enumerator_get_Current_m1081001953(__this, method) ((  ARPlaneAnchorGameObject_t2305225887 * (*) (Enumerator_t1209076693 *, const RuntimeMethod*))Enumerator_get_Current_m2577424081_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.XR.iOS.ARPlaneAnchorGameObject>::MoveNext()
#define Enumerator_MoveNext_m3579574253(__this, method) ((  bool (*) (Enumerator_t1209076693 *, const RuntimeMethod*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.XR.iOS.ARPlaneAnchorGameObject>::Dispose()
#define Enumerator_Dispose_m113660709(__this, method) ((  void (*) (Enumerator_t1209076693 *, const RuntimeMethod*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.XR.iOS.ARPlaneAnchorGameObject>::Clear()
#define Dictionary_2_Clear_m797095524(__this, method) ((  void (*) (Dictionary_2_t4220005149 *, const RuntimeMethod*))Dictionary_2_Clear_m2325793156_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<!0,!1> System.Collections.Generic.Dictionary`2<System.String,UnityEngine.XR.iOS.ARPlaneAnchorGameObject>::get_Values()
#define Dictionary_2_get_Values_m586969881(__this, method) ((  ValueCollection_t2923064992 * (*) (Dictionary_2_t4220005149 *, const RuntimeMethod*))Dictionary_2_get_Values_m2233445381_gshared)(__this, method)
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<UnityEngine.XR.iOS.ARPlaneAnchorGameObject>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_ToList_TisARPlaneAnchorGameObject_t2305225887_m3769884394(__this /* static, unused */, p0, method) ((  List_1_t1674347019 * (*) (RuntimeObject * /* static, unused */, RuntimeObject*, const RuntimeMethod*))Enumerable_ToList_TisRuntimeObject_m3492391627_gshared)(__this /* static, unused */, p0, method)
// System.Void UnityEngine.XR.iOS.UnityARAnchorManager::.ctor()
extern "C"  void UnityARAnchorManager__ctor_m1861900703 (UnityARAnchorManager_t1086564192 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARUtility::InitializePlanePrefab(UnityEngine.GameObject)
extern "C"  void UnityARUtility_InitializePlanePrefab_m2887188869 (RuntimeObject * __this /* static, unused */, GameObject_t1756533147 * ___go0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARAnchorManager::Destroy()
extern "C"  void UnityARAnchorManager_Destroy_m1751028949 (UnityARAnchorManager_t1086564192 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.List`1<UnityEngine.XR.iOS.ARPlaneAnchorGameObject>::get_Count()
#define List_1_get_Count_m1714425968(__this, method) ((  int32_t (*) (List_1_t1674347019 *, const RuntimeMethod*))List_1_get_Count_m2375293942_gshared)(__this, method)
// System.Collections.Generic.List`1<UnityEngine.XR.iOS.ARHitTestResult> UnityEngine.XR.iOS.UnityARSessionNativeInterface::HitTest(UnityEngine.XR.iOS.ARPoint,UnityEngine.XR.iOS.ARHitTestResultType)
extern "C"  List_1_t2644634157 * UnityARSessionNativeInterface_HitTest_m388588674 (UnityARSessionNativeInterface_t1130867170 * __this, ARPoint_t3436811567  ___point0, int64_t ___types1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.List`1<UnityEngine.XR.iOS.ARHitTestResult>::get_Count()
#define List_1_get_Count_m1598304542(__this, method) ((  int32_t (*) (List_1_t2644634157 *, const RuntimeMethod*))List_1_get_Count_m1598304542_gshared)(__this, method)
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<UnityEngine.XR.iOS.ARHitTestResult>::GetEnumerator()
#define List_1_GetEnumerator_m2885046859(__this, method) ((  Enumerator_t2179363831  (*) (List_1_t2644634157 *, const RuntimeMethod*))List_1_GetEnumerator_m2885046859_gshared)(__this, method)
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.XR.iOS.ARHitTestResult>::get_Current()
#define Enumerator_get_Current_m2775711191(__this, method) ((  ARHitTestResult_t3275513025  (*) (Enumerator_t2179363831 *, const RuntimeMethod*))Enumerator_get_Current_m2775711191_gshared)(__this, method)
// System.Void UnityEngine.Debug::Log(System.Object)
extern "C"  void Debug_Log_m920475918 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
extern "C"  void Transform_set_position_m2469242620 (Transform_t3275118058 * __this, Vector3_t2243707580  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_rotation(UnityEngine.Quaternion)
extern "C"  void Transform_set_rotation_m3411284563 (Transform_t3275118058 * __this, Quaternion_t4030073918  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C"  Vector3_t2243707580  Transform_get_position_m1104419803 (Transform_t3275118058 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Format(System.String,System.Object,System.Object,System.Object)
extern "C"  String_t* String_Format_m4262916296 (RuntimeObject * __this /* static, unused */, String_t* p0, RuntimeObject * p1, RuntimeObject * p2, RuntimeObject * p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.XR.iOS.ARHitTestResult>::MoveNext()
#define Enumerator_MoveNext_m3661301011(__this, method) ((  bool (*) (Enumerator_t2179363831 *, const RuntimeMethod*))Enumerator_MoveNext_m3661301011_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.XR.iOS.ARHitTestResult>::Dispose()
#define Enumerator_Dispose_m3109677227(__this, method) ((  void (*) (Enumerator_t2179363831 *, const RuntimeMethod*))Enumerator_Dispose_m3109677227_gshared)(__this, method)
// System.Int32 UnityEngine.Input::get_touchCount()
extern "C"  int32_t Input_get_touchCount_m2050827666 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Touch UnityEngine.Input::GetTouch(System.Int32)
extern "C"  Touch_t407273883  Input_GetTouch_m1463942798 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.TouchPhase UnityEngine.Touch::get_phase()
extern "C"  int32_t Touch_get_phase_m196706494 (Touch_t407273883 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Touch::get_position()
extern "C"  Vector2_t2243707579  Touch_get_position_m2079703643 (Touch_t407273883 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector2)
extern "C"  Vector3_t2243707580  Vector2_op_Implicit_m176791411 (RuntimeObject * __this /* static, unused */, Vector2_t2243707579  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Camera::ScreenToViewportPoint(UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Camera_ScreenToViewportPoint_m2666228286 (Camera_t189460977 * __this, Vector3_t2243707580  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.CompilerServices.RuntimeHelpers::InitializeArray(System.Array,System.RuntimeFieldHandle)
extern "C"  void RuntimeHelpers_InitializeArray_m3920580167 (RuntimeObject * __this /* static, unused */, RuntimeArray * p0, RuntimeFieldHandle_t2331729674  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.XR.iOS.UnityARHitTestExample::HitTestWithResultType(UnityEngine.XR.iOS.ARPoint,UnityEngine.XR.iOS.ARHitTestResultType)
extern "C"  bool UnityARHitTestExample_HitTestWithResultType_m996939112 (UnityARHitTestExample_t146867607 * __this, ARPoint_t3436811567  ___point0, int64_t ___resultTypes1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Rect__ctor_m1220545469 (Rect_t3681755626 * __this, float p0, float p1, float p2, float p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUI::Button(UnityEngine.Rect,System.String)
extern "C"  bool GUI_Button_m3054448581 (RuntimeObject * __this /* static, unused */, Rect_t3681755626  p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::Pause()
extern "C"  void UnityARSessionNativeInterface_Pause_m2220930613 (UnityARSessionNativeInterface_t1130867170 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::RunWithConfigAndOptions(UnityEngine.XR.iOS.ARKitWorldTackingSessionConfiguration,UnityEngine.XR.iOS.UnityARSessionRunOption)
extern "C"  void UnityARSessionNativeInterface_RunWithConfigAndOptions_m375276821 (UnityARSessionNativeInterface_t1130867170 * __this, ARKitWorldTackingSessionConfiguration_t1821734930  ___config0, int32_t ___runOptions1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String)
extern "C"  String_t* String_Concat_m2596409543 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.Matrix4x4::GetColumn(System.Int32)
extern "C"  Vector4_t2243707581  Matrix4x4_GetColumn_m1367096730 (Matrix4x4_t2933234003 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector4::op_Implicit(UnityEngine.Vector4)
extern "C"  Vector3_t2243707580  Vector4_op_Implicit_m1902992875 (RuntimeObject * __this /* static, unused */, Vector4_t2243707581  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.XR.iOS.UnityARMatrixOps::QuaternionFromMatrix(UnityEngine.Matrix4x4)
extern "C"  Quaternion_t4030073918  UnityARMatrixOps_QuaternionFromMatrix_m1686577287 (RuntimeObject * __this /* static, unused */, Matrix4x4_t2933234003  ___m0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Matrix4x4::get_Item(System.Int32,System.Int32)
extern "C"  float Matrix4x4_get_Item_m312280350 (Matrix4x4_t2933234003 * __this, int32_t p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Max(System.Single,System.Single)
extern "C"  float Mathf_Max_m2564622569 (RuntimeObject * __this /* static, unused */, float p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Sign(System.Single)
extern "C"  float Mathf_Sign_m2039143327 (RuntimeObject * __this /* static, unused */, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::_frame_update(UnityEngine.XR.iOS.internal_UnityARCamera)
extern "C"  void UnityARSessionNativeInterface__frame_update_m127370 (RuntimeObject * __this /* static, unused */, internal_UnityARCamera_t2580192745  ___camera0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::_anchor_added(UnityEngine.XR.iOS.UnityARAnchorData)
extern "C"  void UnityARSessionNativeInterface__anchor_added_m3584567327 (RuntimeObject * __this /* static, unused */, UnityARAnchorData_t2901866349  ___anchor0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::_anchor_updated(UnityEngine.XR.iOS.UnityARAnchorData)
extern "C"  void UnityARSessionNativeInterface__anchor_updated_m1970308864 (RuntimeObject * __this /* static, unused */, UnityARAnchorData_t2901866349  ___anchor0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::_anchor_removed(UnityEngine.XR.iOS.UnityARAnchorData)
extern "C"  void UnityARSessionNativeInterface__anchor_removed_m4240378233 (RuntimeObject * __this /* static, unused */, UnityARAnchorData_t2901866349  ___anchor0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::_ar_session_failed(System.String)
extern "C"  void UnityARSessionNativeInterface__ar_session_failed_m2515310284 (RuntimeObject * __this /* static, unused */, String_t* ___error0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFrameUpdate::.ctor(System.Object,System.IntPtr)
extern "C"  void internal_ARFrameUpdate__ctor_m36448827 (internal_ARFrameUpdate_t3296518558 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorAdded::.ctor(System.Object,System.IntPtr)
extern "C"  void internal_ARAnchorAdded__ctor_m875644652 (internal_ARAnchorAdded_t1622117597 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorUpdated::.ctor(System.Object,System.IntPtr)
extern "C"  void internal_ARAnchorUpdated__ctor_m2904213987 (internal_ARAnchorUpdated_t3705772742 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorRemoved::.ctor(System.Object,System.IntPtr)
extern "C"  void internal_ARAnchorRemoved__ctor_m2856503254 (internal_ARAnchorRemoved_t3189755211 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARSessionFailed::.ctor(System.Object,System.IntPtr)
extern "C"  void ARSessionFailed__ctor_m3600321544 (ARSessionFailed_t872580813 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.IntPtr UnityEngine.XR.iOS.UnityARSessionNativeInterface::unity_CreateNativeARSession(UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFrameUpdate,UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorAdded,UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorUpdated,UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorRemoved,UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARSessionFailed)
extern "C"  IntPtr_t UnityARSessionNativeInterface_unity_CreateNativeARSession_m1019965107 (RuntimeObject * __this /* static, unused */, internal_ARFrameUpdate_t3296518558 * ___frameUpdate0, internal_ARAnchorAdded_t1622117597 * ___anchorAdded1, internal_ARAnchorUpdated_t3705772742 * ___anchorUpdated2, internal_ARAnchorRemoved_t3189755211 * ___anchorRemoved3, ARSessionFailed_t872580813 * ___sessionFailed4, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::Combine(System.Delegate,System.Delegate)
extern "C"  Delegate_t3022476291 * Delegate_Combine_m3791207084 (RuntimeObject * __this /* static, unused */, Delegate_t3022476291 * p0, Delegate_t3022476291 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::Remove(System.Delegate,System.Delegate)
extern "C"  Delegate_t3022476291 * Delegate_Remove_m2626518725 (RuntimeObject * __this /* static, unused */, Delegate_t3022476291 * p0, Delegate_t3022476291 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::.ctor()
extern "C"  void UnityARSessionNativeInterface__ctor_m2294513111 (UnityARSessionNativeInterface_t1130867170 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Matrix4x4::SetColumn(System.Int32,UnityEngine.Vector4)
extern "C"  void Matrix4x4_SetColumn_m3120649749 (Matrix4x4_t2933234003 * __this, int32_t p0, Vector4_t2243707581  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::SetCameraNearFar(System.Single,System.Single)
extern "C"  void UnityARSessionNativeInterface_SetCameraNearFar_m1247563277 (RuntimeObject * __this /* static, unused */, float ___nearZ0, float ___farZ1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::UpdatePointCloudData(UnityEngine.XR.iOS.UnityARCamera&)
extern "C"  void UnityARSessionNativeInterface_UpdatePointCloudData_m2452168060 (RuntimeObject * __this /* static, unused */, UnityARCamera_t4198559457 * ___camera0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARFrameUpdate::Invoke(UnityEngine.XR.iOS.UnityARCamera)
extern "C"  void ARFrameUpdate_Invoke_m3133737564 (ARFrameUpdate_t496507918 * __this, UnityARCamera_t4198559457  ___camera0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.XR.iOS.UnityARSessionNativeInterface::GetARPointCloud(System.IntPtr&,System.UInt32&)
extern "C"  bool UnityARSessionNativeInterface_GetARPointCloud_m2491076785 (RuntimeObject * __this /* static, unused */, IntPtr_t* ___verts0, uint32_t* ___vertLength1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::Copy(System.IntPtr,System.Single[],System.Int32,System.Int32)
extern "C"  void Marshal_Copy_m2353359830 (RuntimeObject * __this /* static, unused */, IntPtr_t p0, SingleU5BU5D_t577127397* p1, int32_t p2, int32_t p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.InteropServices.Marshal::PtrToStringAuto(System.IntPtr)
extern "C"  String_t* Marshal_PtrToStringAuto_m3496615756 (RuntimeObject * __this /* static, unused */, IntPtr_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Vector3__ctor_m2638739322 (Vector3_t2243707580 * __this, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IntPtr::op_Inequality(System.IntPtr,System.IntPtr)
extern "C"  bool IntPtr_op_Inequality_m3044532593 (RuntimeObject * __this /* static, unused */, IntPtr_t p0, IntPtr_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.XR.iOS.ARPlaneAnchor UnityEngine.XR.iOS.UnityARSessionNativeInterface::GetPlaneAnchorFromAnchorData(UnityEngine.XR.iOS.UnityARAnchorData)
extern "C"  ARPlaneAnchor_t1439520888  UnityARSessionNativeInterface_GetPlaneAnchorFromAnchorData_m3105060455 (RuntimeObject * __this /* static, unused */, UnityARAnchorData_t2901866349  ___anchor0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorAdded::Invoke(UnityEngine.XR.iOS.ARPlaneAnchor)
extern "C"  void ARAnchorAdded_Invoke_m3296517664 (ARAnchorAdded_t2646854145 * __this, ARPlaneAnchor_t1439520888  ___anchorData0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorUpdated::Invoke(UnityEngine.XR.iOS.ARPlaneAnchor)
extern "C"  void ARAnchorUpdated_Invoke_m1018775699 (ARAnchorUpdated_t3886071158 * __this, ARPlaneAnchor_t1439520888  ___anchorData0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorRemoved::Invoke(UnityEngine.XR.iOS.ARPlaneAnchor)
extern "C"  void ARAnchorRemoved_Invoke_m4120555414 (ARAnchorRemoved_t142665927 * __this, ARPlaneAnchor_t1439520888  ___anchorData0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARSessionFailed::Invoke(System.String)
extern "C"  void ARSessionFailed_Invoke_m3664045560 (ARSessionFailed_t872580813 * __this, String_t* ___error0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::StartWorldTrackingSessionWithOptions(System.IntPtr,UnityEngine.XR.iOS.ARKitWorldTackingSessionConfiguration,UnityEngine.XR.iOS.UnityARSessionRunOption)
extern "C"  void UnityARSessionNativeInterface_StartWorldTrackingSessionWithOptions_m3150342870 (RuntimeObject * __this /* static, unused */, IntPtr_t ___nativeSession0, ARKitWorldTackingSessionConfiguration_t1821734930  ___configuration1, int32_t ___runOptions2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::StartWorldTrackingSession(System.IntPtr,UnityEngine.XR.iOS.ARKitWorldTackingSessionConfiguration)
extern "C"  void UnityARSessionNativeInterface_StartWorldTrackingSession_m3140261726 (RuntimeObject * __this /* static, unused */, IntPtr_t ___nativeSession0, ARKitWorldTackingSessionConfiguration_t1821734930  ___configuration1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::StartSessionWithOptions(System.IntPtr,UnityEngine.XR.iOS.ARKitSessionConfiguration,UnityEngine.XR.iOS.UnityARSessionRunOption)
extern "C"  void UnityARSessionNativeInterface_StartSessionWithOptions_m1815473114 (RuntimeObject * __this /* static, unused */, IntPtr_t ___nativeSession0, ARKitSessionConfiguration_t318899795  ___configuration1, int32_t ___runOptions2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::StartSession(System.IntPtr,UnityEngine.XR.iOS.ARKitSessionConfiguration)
extern "C"  void UnityARSessionNativeInterface_StartSession_m227007524 (RuntimeObject * __this /* static, unused */, IntPtr_t ___nativeSession0, ARKitSessionConfiguration_t318899795  ___configuration1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::PauseSession(System.IntPtr)
extern "C"  void UnityARSessionNativeInterface_PauseSession_m2717865525 (RuntimeObject * __this /* static, unused */, IntPtr_t ___nativeSession0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.XR.iOS.UnityARSessionNativeInterface::HitTest(System.IntPtr,UnityEngine.XR.iOS.ARPoint,UnityEngine.XR.iOS.ARHitTestResultType)
extern "C"  int32_t UnityARSessionNativeInterface_HitTest_m1115034644 (RuntimeObject * __this /* static, unused */, IntPtr_t ___nativeSession0, ARPoint_t3436811567  ___point1, int64_t ___types2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Format(System.String,System.Object)
extern "C"  String_t* String_Format_m2024975688 (RuntimeObject * __this /* static, unused */, String_t* p0, RuntimeObject * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<UnityEngine.XR.iOS.ARHitTestResult>::.ctor()
#define List_1__ctor_m3856316316(__this, method) ((  void (*) (List_1_t2644634157 *, const RuntimeMethod*))List_1__ctor_m3856316316_gshared)(__this, method)
// UnityEngine.XR.iOS.UnityARHitTestResult UnityEngine.XR.iOS.UnityARSessionNativeInterface::GetLastHitTestResult(System.Int32)
extern "C"  UnityARHitTestResult_t4129824344  UnityARSessionNativeInterface_GetLastHitTestResult_m1489387413 (RuntimeObject * __this /* static, unused */, int32_t ___index0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.XR.iOS.ARHitTestResult UnityEngine.XR.iOS.UnityARSessionNativeInterface::GetHitTestResultFromResultData(UnityEngine.XR.iOS.UnityARHitTestResult)
extern "C"  ARHitTestResult_t3275513025  UnityARSessionNativeInterface_GetHitTestResultFromResultData_m1356947160 (RuntimeObject * __this /* static, unused */, UnityARHitTestResult_t4129824344  ___resultData0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<UnityEngine.XR.iOS.ARHitTestResult>::Add(!0)
#define List_1_Add_m1918634088(__this, p0, method) ((  void (*) (List_1_t2644634157 *, ARHitTestResult_t3275513025 , const RuntimeMethod*))List_1_Add_m1918634088_gshared)(__this, p0, method)
// UnityEngine.XR.iOS.ARTextureHandles UnityEngine.XR.iOS.UnityARSessionNativeInterface::GetVideoTextureHandles()
extern "C"  ARTextureHandles_t3764914833  UnityARSessionNativeInterface_GetVideoTextureHandles_m4073538006 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.XR.iOS.UnityARSessionNativeInterface::GetAmbientIntensity()
extern "C"  float UnityARSessionNativeInterface_GetAmbientIntensity_m3132982210 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.XR.iOS.UnityARSessionNativeInterface::GetTrackingQuality()
extern "C"  int32_t UnityARSessionNativeInterface_GetTrackingQuality_m3473680033 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.XR.iOS.UnityARSessionNativeInterface::GetYUVTexCoordScale()
extern "C"  float UnityARSessionNativeInterface_GetYUVTexCoordScale_m671637809 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorAdded::Invoke(UnityEngine.XR.iOS.UnityARAnchorData)
extern "C"  void internal_ARAnchorAdded_Invoke_m4273916311 (internal_ARAnchorAdded_t1622117597 * __this, UnityARAnchorData_t2901866349  ___anchorData0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorRemoved::Invoke(UnityEngine.XR.iOS.UnityARAnchorData)
extern "C"  void internal_ARAnchorRemoved_Invoke_m3299031849 (internal_ARAnchorRemoved_t3189755211 * __this, UnityARAnchorData_t2901866349  ___anchorData0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorUpdated::Invoke(UnityEngine.XR.iOS.UnityARAnchorData)
extern "C"  void internal_ARAnchorUpdated_Invoke_m609656910 (internal_ARAnchorUpdated_t3705772742 * __this, UnityARAnchorData_t2901866349  ___anchorData0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFrameUpdate::Invoke(UnityEngine.XR.iOS.internal_UnityARCamera)
extern "C"  void internal_ARFrameUpdate_Invoke_m3624040046 (internal_ARFrameUpdate_t3296518558 * __this, internal_UnityARCamera_t2580192745  ___camera0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0)
#define Object_Instantiate_TisGameObject_t1756533147_m3664764861(__this /* static, unused */, p0, method) ((  GameObject_t1756533147 * (*) (RuntimeObject * /* static, unused */, GameObject_t1756533147 *, const RuntimeMethod*))Object_Instantiate_TisRuntimeObject_m447919519_gshared)(__this /* static, unused */, p0, method)
// System.Void UnityEngine.GameObject::.ctor()
extern "C"  void GameObject__ctor_m498247354 (GameObject_t1756533147 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::set_name(System.String)
extern "C"  void Object_set_name_m4157836998 (Object_t1021602117 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
extern "C"  Transform_t3275118058 * GameObject_get_transform_m909382139 (GameObject_t1756533147 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponentInChildren<UnityEngine.MeshFilter>()
#define GameObject_GetComponentInChildren_TisMeshFilter_t3026937449_m1346841033(__this, method) ((  MeshFilter_t3026937449 * (*) (GameObject_t1756533147 *, const RuntimeMethod*))GameObject_GetComponentInChildren_TisRuntimeObject_m327292296_gshared)(__this, method)
// System.Void UnityEngine.Transform::set_localScale(UnityEngine.Vector3)
extern "C"  void Transform_set_localScale_m2325460848 (Transform_t3275118058 * __this, Vector3_t2243707580  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rendering.CommandBuffer::.ctor()
extern "C"  void CommandBuffer__ctor_m3893953450 (CommandBuffer_t1204166949 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rendering.RenderTargetIdentifier UnityEngine.Rendering.RenderTargetIdentifier::op_Implicit(UnityEngine.Rendering.BuiltinRenderTextureType)
extern "C"  RenderTargetIdentifier_t772440638  RenderTargetIdentifier_op_Implicit_m1621446097 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rendering.CommandBuffer::Blit(UnityEngine.Texture,UnityEngine.Rendering.RenderTargetIdentifier,UnityEngine.Material)
extern "C"  void CommandBuffer_Blit_m4282817560 (CommandBuffer_t1204166949 * __this, Texture_t2243626319 * p0, RenderTargetIdentifier_t772440638  p1, Material_t193706927 * p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::AddCommandBuffer(UnityEngine.Rendering.CameraEvent,UnityEngine.Rendering.CommandBuffer)
extern "C"  void Camera_AddCommandBuffer_m2569587168 (Camera_t189460977 * __this, int32_t p0, CommandBuffer_t1204166949 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::RemoveCommandBuffer(UnityEngine.Rendering.CameraEvent,UnityEngine.Rendering.CommandBuffer)
extern "C"  void Camera_RemoveCommandBuffer_m2103408695 (Camera_t189460977 * __this, int32_t p0, CommandBuffer_t1204166949 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.XR.iOS.ARTextureHandles UnityEngine.XR.iOS.UnityARSessionNativeInterface::GetARVideoTextureHandles()
extern "C"  ARTextureHandles_t3764914833  UnityARSessionNativeInterface_GetARVideoTextureHandles_m2905358883 (UnityARSessionNativeInterface_t1130867170 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IntPtr::op_Equality(System.IntPtr,System.IntPtr)
extern "C"  bool IntPtr_op_Equality_m1573482188 (RuntimeObject * __this /* static, unused */, IntPtr_t p0, IntPtr_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.XR.iOS.UnityARVideo::InitializeCommandBuffer()
extern "C"  void UnityARVideo_InitializeCommandBuffer_m2800566831 (UnityARVideo_t2351297253 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Resolution UnityEngine.Screen::get_currentResolution()
extern "C"  Resolution_t3693662728  Screen_get_currentResolution_m2361090437 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Resolution::get_width()
extern "C"  int32_t Resolution_get_width_m1438273472 (Resolution_t3693662728 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Resolution::get_height()
extern "C"  int32_t Resolution_get_height_m882683003 (Resolution_t3693662728 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D UnityEngine.Texture2D::CreateExternalTexture(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean,System.Boolean,System.IntPtr)
extern "C"  Texture2D_t3542995729 * Texture2D_CreateExternalTexture_m3402112250 (RuntimeObject * __this /* static, unused */, int32_t p0, int32_t p1, int32_t p2, bool p3, bool p4, IntPtr_t p5, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture::set_filterMode(UnityEngine.FilterMode)
extern "C"  void Texture_set_filterMode_m3838996656 (Texture_t2243626319 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture::set_wrapMode(UnityEngine.TextureWrapMode)
extern "C"  void Texture_set_wrapMode_m333956747 (Texture_t2243626319 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture2D::UpdateExternalTexture(System.IntPtr)
extern "C"  void Texture2D_UpdateExternalTexture_m1701322565 (Texture2D_t3542995729 * __this, IntPtr_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::SetTexture(System.String,UnityEngine.Texture)
extern "C"  void Material_SetTexture_m141095205 (Material_t193706927 * __this, String_t* p0, Texture_t2243626319 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ScreenOrientation UnityEngine.Screen::get_orientation()
extern "C"  int32_t Screen_get_orientation_m879255848 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
extern "C"  Vector3_t2243707580  Vector3_get_zero_m1527993324 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(System.Single,System.Single,System.Single)
extern "C"  Quaternion_t4030073918  Quaternion_Euler_m2887458175 (RuntimeObject * __this /* static, unused */, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_one()
extern "C"  Vector3_t2243707580  Vector3_get_one_m627547232 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::TRS(UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3)
extern "C"  Matrix4x4_t2933234003  Matrix4x4_TRS_m1913765359 (RuntimeObject * __this /* static, unused */, Vector3_t2243707580  p0, Quaternion_t4030073918  p1, Vector3_t2243707580  p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::SetMatrix(System.String,UnityEngine.Matrix4x4)
extern "C"  void Material_SetMatrix_m1387972957 (Material_t193706927 * __this, String_t* p0, Matrix4x4_t2933234003  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.XR.iOS.UnityARSessionNativeInterface::GetARYUVTexCoordScale()
extern "C"  float UnityARSessionNativeInterface_GetARYUVTexCoordScale_m1823875812 (UnityARSessionNativeInterface_t1130867170 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::SetFloat(System.String,System.Single)
extern "C"  void Material_SetFloat_m1926275467 (Material_t193706927 * __this, String_t* p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::SetInt(System.String,System.Int32)
extern "C"  void Material_SetInt_m522302436 (Material_t193706927 * __this, String_t* p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject>::.ctor()
#define List_1__ctor_m704351054(__this, method) ((  void (*) (List_1_t1125654279 *, const RuntimeMethod*))List_1__ctor_m310736118_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject>::Add(!0)
#define List_1_Add_m3441471442(__this, p0, method) ((  void (*) (List_1_t1125654279 *, GameObject_t1756533147 *, const RuntimeMethod*))List_1_Add_m4157722533_gshared)(__this, p0, method)
// UnityEngine.Vector4 UnityEngine.Vector4::op_Implicit(UnityEngine.Vector3)
extern "C"  Vector4_t2243707581  Vector4_op_Implicit_m1059320239 (RuntimeObject * __this /* static, unused */, Vector3_t2243707580  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !0 System.Collections.Generic.List`1<UnityEngine.GameObject>::get_Item(System.Int32)
#define List_1_get_Item_m939767277(__this, p0, method) ((  GameObject_t1756533147 * (*) (List_1_t1125654279 *, int32_t, const RuntimeMethod*))List_1_get_Item_m2062981835_gshared)(__this, p0, method)
// System.Int64 System.Math::Min(System.Int64,System.Int64)
extern "C"  int64_t Math_Min_m301707792 (RuntimeObject * __this /* static, unused */, int64_t p0, int64_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AR3DOFCameraManager::.ctor()
extern "C"  void AR3DOFCameraManager__ctor_m2150279366 (AR3DOFCameraManager_t2152865733 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AR3DOFCameraManager::Start()
extern "C"  void AR3DOFCameraManager_Start_m3126774506 (AR3DOFCameraManager_t2152865733 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AR3DOFCameraManager_Start_m3126774506_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ARKitSessionConfiguration_t318899795  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Application_set_targetFrameRate_m2941880625(NULL /*static, unused*/, ((int32_t)60), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		UnityARSessionNativeInterface_t1130867170 * L_0 = UnityARSessionNativeInterface_GetARSessionNativeInterface_m3174488657(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_m_session_3(L_0);
		Initobj (ARKitSessionConfiguration_t318899795_il2cpp_TypeInfo_var, (&V_0));
		(&V_0)->set_alignment_0(0);
		(&V_0)->set_getPointCloudData_1((bool)1);
		(&V_0)->set_enableLightEstimation_2((bool)1);
		UnityARSessionNativeInterface_t1130867170 * L_1 = __this->get_m_session_3();
		ARKitSessionConfiguration_t318899795  L_2 = V_0;
		NullCheck(L_1);
		UnityARSessionNativeInterface_RunWithConfig_m2478060541(L_1, L_2, /*hidden argument*/NULL);
		Camera_t189460977 * L_3 = __this->get_m_camera_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_3, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_005a;
		}
	}
	{
		Camera_t189460977 * L_5 = Camera_get_main_m475173995(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_m_camera_2(L_5);
	}

IL_005a:
	{
		return;
	}
}
// System.Void AR3DOFCameraManager::SetCamera(UnityEngine.Camera)
extern "C"  void AR3DOFCameraManager_SetCamera_m946604403 (AR3DOFCameraManager_t2152865733 * __this, Camera_t189460977 * ___newCamera0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AR3DOFCameraManager_SetCamera_m946604403_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UnityARVideo_t2351297253 * V_0 = NULL;
	{
		Camera_t189460977 * L_0 = __this->get_m_camera_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0040;
		}
	}
	{
		Camera_t189460977 * L_2 = __this->get_m_camera_2();
		NullCheck(L_2);
		GameObject_t1756533147 * L_3 = Component_get_gameObject_m3105766835(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		UnityARVideo_t2351297253 * L_4 = GameObject_GetComponent_TisUnityARVideo_t2351297253_m2230129522(L_3, /*hidden argument*/GameObject_GetComponent_TisUnityARVideo_t2351297253_m2230129522_RuntimeMethod_var);
		V_0 = L_4;
		UnityARVideo_t2351297253 * L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_5, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0040;
		}
	}
	{
		UnityARVideo_t2351297253 * L_7 = V_0;
		NullCheck(L_7);
		Material_t193706927 * L_8 = L_7->get_m_ClearMaterial_2();
		__this->set_savedClearMaterial_4(L_8);
		UnityARVideo_t2351297253 * L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
	}

IL_0040:
	{
		Camera_t189460977 * L_10 = ___newCamera0;
		AR3DOFCameraManager_SetupNewCamera_m439842942(__this, L_10, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AR3DOFCameraManager::SetupNewCamera(UnityEngine.Camera)
extern "C"  void AR3DOFCameraManager_SetupNewCamera_m439842942 (AR3DOFCameraManager_t2152865733 * __this, Camera_t189460977 * ___newCamera0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AR3DOFCameraManager_SetupNewCamera_m439842942_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UnityARVideo_t2351297253 * V_0 = NULL;
	{
		Camera_t189460977 * L_0 = ___newCamera0;
		__this->set_m_camera_2(L_0);
		Camera_t189460977 * L_1 = __this->get_m_camera_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0064;
		}
	}
	{
		Camera_t189460977 * L_3 = __this->get_m_camera_2();
		NullCheck(L_3);
		GameObject_t1756533147 * L_4 = Component_get_gameObject_m3105766835(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		UnityARVideo_t2351297253 * L_5 = GameObject_GetComponent_TisUnityARVideo_t2351297253_m2230129522(L_4, /*hidden argument*/GameObject_GetComponent_TisUnityARVideo_t2351297253_m2230129522_RuntimeMethod_var);
		V_0 = L_5;
		UnityARVideo_t2351297253 * L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_6, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0047;
		}
	}
	{
		UnityARVideo_t2351297253 * L_8 = V_0;
		NullCheck(L_8);
		Material_t193706927 * L_9 = L_8->get_m_ClearMaterial_2();
		__this->set_savedClearMaterial_4(L_9);
		UnityARVideo_t2351297253 * L_10 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
	}

IL_0047:
	{
		Camera_t189460977 * L_11 = __this->get_m_camera_2();
		NullCheck(L_11);
		GameObject_t1756533147 * L_12 = Component_get_gameObject_m3105766835(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		UnityARVideo_t2351297253 * L_13 = GameObject_AddComponent_TisUnityARVideo_t2351297253_m1329128087(L_12, /*hidden argument*/GameObject_AddComponent_TisUnityARVideo_t2351297253_m1329128087_RuntimeMethod_var);
		V_0 = L_13;
		UnityARVideo_t2351297253 * L_14 = V_0;
		Material_t193706927 * L_15 = __this->get_savedClearMaterial_4();
		NullCheck(L_14);
		L_14->set_m_ClearMaterial_2(L_15);
	}

IL_0064:
	{
		return;
	}
}
// System.Void AR3DOFCameraManager::Update()
extern "C"  void AR3DOFCameraManager_Update_m2410279539 (AR3DOFCameraManager_t2152865733 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AR3DOFCameraManager_Update_m2410279539_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Matrix4x4_t2933234003  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Camera_t189460977 * L_0 = __this->get_m_camera_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_005f;
		}
	}
	{
		UnityARSessionNativeInterface_t1130867170 * L_2 = __this->get_m_session_3();
		NullCheck(L_2);
		Matrix4x4_t2933234003  L_3 = UnityARSessionNativeInterface_GetCameraPose_m3046824030(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Camera_t189460977 * L_4 = __this->get_m_camera_2();
		NullCheck(L_4);
		Transform_t3275118058 * L_5 = Component_get_transform_m2697483695(L_4, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_6 = V_0;
		Vector3_t2243707580  L_7 = UnityARMatrixOps_GetPosition_m1153858439(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_set_localPosition_m1026930133(L_5, L_7, /*hidden argument*/NULL);
		Camera_t189460977 * L_8 = __this->get_m_camera_2();
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = Component_get_transform_m2697483695(L_8, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_10 = V_0;
		Quaternion_t4030073918  L_11 = UnityARMatrixOps_GetRotation_m1002641986(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		Transform_set_localRotation_m2055111962(L_9, L_11, /*hidden argument*/NULL);
		Camera_t189460977 * L_12 = __this->get_m_camera_2();
		UnityARSessionNativeInterface_t1130867170 * L_13 = __this->get_m_session_3();
		NullCheck(L_13);
		Matrix4x4_t2933234003  L_14 = UnityARSessionNativeInterface_GetCameraProjection_m3168017698(L_13, /*hidden argument*/NULL);
		NullCheck(L_12);
		Camera_set_projectionMatrix_m2059836755(L_12, L_14, /*hidden argument*/NULL);
	}

IL_005f:
	{
		return;
	}
}
// System.Void DontDestroyOnLoad::.ctor()
extern "C"  void DontDestroyOnLoad__ctor_m986497013 (DontDestroyOnLoad_t3235789354 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DontDestroyOnLoad::Start()
extern "C"  void DontDestroyOnLoad_Start_m34112045 (DontDestroyOnLoad_t3235789354 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DontDestroyOnLoad_Start_m34112045_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m2330762974(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DontDestroyOnLoad::Update()
extern "C"  void DontDestroyOnLoad_Update_m780726090 (DontDestroyOnLoad_t3235789354 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void PointCloudParticleExample::.ctor()
extern "C"  void PointCloudParticleExample__ctor_m3827552698 (PointCloudParticleExample_t986756623 * __this, const RuntimeMethod* method)
{
	{
		__this->set_particleSize_4((1.0f));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PointCloudParticleExample::Start()
extern "C"  void PointCloudParticleExample_Start_m1963896970 (PointCloudParticleExample_t986756623 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PointCloudParticleExample_Start_m1963896970_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)PointCloudParticleExample_ARFrameUpdated_m2750070680_RuntimeMethod_var);
		ARFrameUpdate_t496507918 * L_1 = (ARFrameUpdate_t496507918 *)il2cpp_codegen_object_new(ARFrameUpdate_t496507918_il2cpp_TypeInfo_var);
		ARFrameUpdate__ctor_m1399217559(L_1, __this, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		UnityARSessionNativeInterface_add_ARFrameUpdatedEvent_m2850773202(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		ParticleSystem_t3394631041 * L_2 = __this->get_pointCloudParticlePrefab_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		ParticleSystem_t3394631041 * L_3 = Object_Instantiate_TisParticleSystem_t3394631041_m4293040502(NULL /*static, unused*/, L_2, /*hidden argument*/Object_Instantiate_TisParticleSystem_t3394631041_m4293040502_RuntimeMethod_var);
		__this->set_currentPS_7(L_3);
		__this->set_frameUpdated_6((bool)0);
		return;
	}
}
// System.Void PointCloudParticleExample::ARFrameUpdated(UnityEngine.XR.iOS.UnityARCamera)
extern "C"  void PointCloudParticleExample_ARFrameUpdated_m2750070680 (PointCloudParticleExample_t986756623 * __this, UnityARCamera_t4198559457  ___camera0, const RuntimeMethod* method)
{
	{
		Vector3U5BU5D_t1172311765* L_0 = (&___camera0)->get_pointCloudData_4();
		__this->set_m_PointCloudData_5(L_0);
		__this->set_frameUpdated_6((bool)1);
		return;
	}
}
// System.Void PointCloudParticleExample::Update()
extern "C"  void PointCloudParticleExample_Update_m2434219493 (PointCloudParticleExample_t986756623 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PointCloudParticleExample_Update_m2434219493_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	ParticleU5BU5D_t574222242* V_1 = NULL;
	int32_t V_2 = 0;
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3U5BU5D_t1172311765* V_4 = NULL;
	int32_t V_5 = 0;
	ParticleU5BU5D_t574222242* V_6 = NULL;
	{
		bool L_0 = __this->get_frameUpdated_6();
		if (!L_0)
		{
			goto IL_00fa;
		}
	}
	{
		Vector3U5BU5D_t1172311765* L_1 = __this->get_m_PointCloudData_5();
		if (!L_1)
		{
			goto IL_00cb;
		}
	}
	{
		Vector3U5BU5D_t1172311765* L_2 = __this->get_m_PointCloudData_5();
		NullCheck(L_2);
		if ((((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_2)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_00cb;
		}
	}
	{
		Vector3U5BU5D_t1172311765* L_3 = __this->get_m_PointCloudData_5();
		NullCheck(L_3);
		int32_t L_4 = __this->get_maxPointsToShow_3();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_5 = Mathf_Min_m2906823867(NULL /*static, unused*/, (((int32_t)((int32_t)(((RuntimeArray *)L_3)->max_length)))), L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		int32_t L_6 = V_0;
		V_1 = ((ParticleU5BU5D_t574222242*)SZArrayNew(ParticleU5BU5D_t574222242_il2cpp_TypeInfo_var, (uint32_t)L_6));
		V_2 = 0;
		Vector3U5BU5D_t1172311765* L_7 = __this->get_m_PointCloudData_5();
		V_4 = L_7;
		V_5 = 0;
		goto IL_00ae;
	}

IL_0051:
	{
		Vector3U5BU5D_t1172311765* L_8 = V_4;
		int32_t L_9 = V_5;
		NullCheck(L_8);
		V_3 = (*(Vector3_t2243707580 *)((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_9))));
		ParticleU5BU5D_t574222242* L_10 = V_1;
		int32_t L_11 = V_2;
		NullCheck(L_10);
		Vector3_t2243707580  L_12 = V_3;
		Particle_set_position_m3680513126(((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_11))), L_12, /*hidden argument*/NULL);
		ParticleU5BU5D_t574222242* L_13 = V_1;
		int32_t L_14 = V_2;
		NullCheck(L_13);
		Color_t2020392075  L_15;
		memset(&L_15, 0, sizeof(L_15));
		Color__ctor_m3811852957((&L_15), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		Color32_t874517518  L_16 = Color32_op_Implicit_m624191464(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		Particle_set_startColor_m3936512348(((L_13)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_14))), L_16, /*hidden argument*/NULL);
		ParticleU5BU5D_t574222242* L_17 = V_1;
		int32_t L_18 = V_2;
		NullCheck(L_17);
		float L_19 = __this->get_particleSize_4();
		Particle_set_startSize_m2457836830(((L_17)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_18))), L_19, /*hidden argument*/NULL);
		int32_t L_20 = V_2;
		V_2 = ((int32_t)((int32_t)L_20+(int32_t)1));
		int32_t L_21 = V_5;
		V_5 = ((int32_t)((int32_t)L_21+(int32_t)1));
	}

IL_00ae:
	{
		int32_t L_22 = V_5;
		Vector3U5BU5D_t1172311765* L_23 = V_4;
		NullCheck(L_23);
		if ((((int32_t)L_22) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_23)->max_length)))))))
		{
			goto IL_0051;
		}
	}
	{
		ParticleSystem_t3394631041 * L_24 = __this->get_currentPS_7();
		ParticleU5BU5D_t574222242* L_25 = V_1;
		int32_t L_26 = V_0;
		NullCheck(L_24);
		ParticleSystem_SetParticles_m3035584975(L_24, L_25, L_26, /*hidden argument*/NULL);
		goto IL_00f3;
	}

IL_00cb:
	{
		V_6 = ((ParticleU5BU5D_t574222242*)SZArrayNew(ParticleU5BU5D_t574222242_il2cpp_TypeInfo_var, (uint32_t)1));
		ParticleU5BU5D_t574222242* L_27 = V_6;
		NullCheck(L_27);
		Particle_set_startSize_m2457836830(((L_27)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))), (0.0f), /*hidden argument*/NULL);
		ParticleSystem_t3394631041 * L_28 = __this->get_currentPS_7();
		ParticleU5BU5D_t574222242* L_29 = V_6;
		NullCheck(L_28);
		ParticleSystem_SetParticles_m3035584975(L_28, L_29, 1, /*hidden argument*/NULL);
	}

IL_00f3:
	{
		__this->set_frameUpdated_6((bool)0);
	}

IL_00fa:
	{
		return;
	}
}
// System.Void UnityARCameraManager::.ctor()
extern "C"  void UnityARCameraManager__ctor_m3319264183 (UnityARCameraManager_t2138856896 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityARCameraManager::Start()
extern "C"  void UnityARCameraManager_Start_m1258756459 (UnityARCameraManager_t2138856896 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARCameraManager_Start_m1258756459_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ARKitWorldTackingSessionConfiguration_t1821734930  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Application_set_targetFrameRate_m2941880625(NULL /*static, unused*/, ((int32_t)60), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		UnityARSessionNativeInterface_t1130867170 * L_0 = UnityARSessionNativeInterface_GetARSessionNativeInterface_m3174488657(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_m_session_3(L_0);
		Initobj (ARKitWorldTackingSessionConfiguration_t1821734930_il2cpp_TypeInfo_var, (&V_0));
		(&V_0)->set_planeDetection_1(1);
		(&V_0)->set_alignment_0(0);
		(&V_0)->set_getPointCloudData_2((bool)1);
		(&V_0)->set_enableLightEstimation_3((bool)1);
		UnityARSessionNativeInterface_t1130867170 * L_1 = __this->get_m_session_3();
		ARKitWorldTackingSessionConfiguration_t1821734930  L_2 = V_0;
		NullCheck(L_1);
		UnityARSessionNativeInterface_RunWithConfig_m3195925270(L_1, L_2, /*hidden argument*/NULL);
		Camera_t189460977 * L_3 = __this->get_m_camera_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_3, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0062;
		}
	}
	{
		Camera_t189460977 * L_5 = Camera_get_main_m475173995(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_m_camera_2(L_5);
	}

IL_0062:
	{
		return;
	}
}
// System.Void UnityARCameraManager::SetCamera(UnityEngine.Camera)
extern "C"  void UnityARCameraManager_SetCamera_m4250956606 (UnityARCameraManager_t2138856896 * __this, Camera_t189460977 * ___newCamera0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARCameraManager_SetCamera_m4250956606_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UnityARVideo_t2351297253 * V_0 = NULL;
	{
		Camera_t189460977 * L_0 = __this->get_m_camera_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0040;
		}
	}
	{
		Camera_t189460977 * L_2 = __this->get_m_camera_2();
		NullCheck(L_2);
		GameObject_t1756533147 * L_3 = Component_get_gameObject_m3105766835(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		UnityARVideo_t2351297253 * L_4 = GameObject_GetComponent_TisUnityARVideo_t2351297253_m2230129522(L_3, /*hidden argument*/GameObject_GetComponent_TisUnityARVideo_t2351297253_m2230129522_RuntimeMethod_var);
		V_0 = L_4;
		UnityARVideo_t2351297253 * L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_5, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0040;
		}
	}
	{
		UnityARVideo_t2351297253 * L_7 = V_0;
		NullCheck(L_7);
		Material_t193706927 * L_8 = L_7->get_m_ClearMaterial_2();
		__this->set_savedClearMaterial_4(L_8);
		UnityARVideo_t2351297253 * L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
	}

IL_0040:
	{
		Camera_t189460977 * L_10 = ___newCamera0;
		UnityARCameraManager_SetupNewCamera_m2358778511(__this, L_10, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityARCameraManager::SetupNewCamera(UnityEngine.Camera)
extern "C"  void UnityARCameraManager_SetupNewCamera_m2358778511 (UnityARCameraManager_t2138856896 * __this, Camera_t189460977 * ___newCamera0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARCameraManager_SetupNewCamera_m2358778511_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UnityARVideo_t2351297253 * V_0 = NULL;
	{
		Camera_t189460977 * L_0 = ___newCamera0;
		__this->set_m_camera_2(L_0);
		Camera_t189460977 * L_1 = __this->get_m_camera_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0064;
		}
	}
	{
		Camera_t189460977 * L_3 = __this->get_m_camera_2();
		NullCheck(L_3);
		GameObject_t1756533147 * L_4 = Component_get_gameObject_m3105766835(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		UnityARVideo_t2351297253 * L_5 = GameObject_GetComponent_TisUnityARVideo_t2351297253_m2230129522(L_4, /*hidden argument*/GameObject_GetComponent_TisUnityARVideo_t2351297253_m2230129522_RuntimeMethod_var);
		V_0 = L_5;
		UnityARVideo_t2351297253 * L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_6, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0047;
		}
	}
	{
		UnityARVideo_t2351297253 * L_8 = V_0;
		NullCheck(L_8);
		Material_t193706927 * L_9 = L_8->get_m_ClearMaterial_2();
		__this->set_savedClearMaterial_4(L_9);
		UnityARVideo_t2351297253 * L_10 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
	}

IL_0047:
	{
		Camera_t189460977 * L_11 = __this->get_m_camera_2();
		NullCheck(L_11);
		GameObject_t1756533147 * L_12 = Component_get_gameObject_m3105766835(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		UnityARVideo_t2351297253 * L_13 = GameObject_AddComponent_TisUnityARVideo_t2351297253_m1329128087(L_12, /*hidden argument*/GameObject_AddComponent_TisUnityARVideo_t2351297253_m1329128087_RuntimeMethod_var);
		V_0 = L_13;
		UnityARVideo_t2351297253 * L_14 = V_0;
		Material_t193706927 * L_15 = __this->get_savedClearMaterial_4();
		NullCheck(L_14);
		L_14->set_m_ClearMaterial_2(L_15);
	}

IL_0064:
	{
		return;
	}
}
// System.Void UnityARCameraManager::Update()
extern "C"  void UnityARCameraManager_Update_m4265169368 (UnityARCameraManager_t2138856896 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARCameraManager_Update_m4265169368_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Matrix4x4_t2933234003  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Camera_t189460977 * L_0 = __this->get_m_camera_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_005f;
		}
	}
	{
		UnityARSessionNativeInterface_t1130867170 * L_2 = __this->get_m_session_3();
		NullCheck(L_2);
		Matrix4x4_t2933234003  L_3 = UnityARSessionNativeInterface_GetCameraPose_m3046824030(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Camera_t189460977 * L_4 = __this->get_m_camera_2();
		NullCheck(L_4);
		Transform_t3275118058 * L_5 = Component_get_transform_m2697483695(L_4, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_6 = V_0;
		Vector3_t2243707580  L_7 = UnityARMatrixOps_GetPosition_m1153858439(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_set_localPosition_m1026930133(L_5, L_7, /*hidden argument*/NULL);
		Camera_t189460977 * L_8 = __this->get_m_camera_2();
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = Component_get_transform_m2697483695(L_8, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_10 = V_0;
		Quaternion_t4030073918  L_11 = UnityARMatrixOps_GetRotation_m1002641986(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		Transform_set_localRotation_m2055111962(L_9, L_11, /*hidden argument*/NULL);
		Camera_t189460977 * L_12 = __this->get_m_camera_2();
		UnityARSessionNativeInterface_t1130867170 * L_13 = __this->get_m_session_3();
		NullCheck(L_13);
		Matrix4x4_t2933234003  L_14 = UnityARSessionNativeInterface_GetCameraProjection_m3168017698(L_13, /*hidden argument*/NULL);
		NullCheck(L_12);
		Camera_set_projectionMatrix_m2059836755(L_12, L_14, /*hidden argument*/NULL);
	}

IL_005f:
	{
		return;
	}
}
// System.Void UnityARCameraNearFar::.ctor()
extern "C"  void UnityARCameraNearFar__ctor_m3044833845 (UnityARCameraNearFar_t519802600 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityARCameraNearFar::Start()
extern "C"  void UnityARCameraNearFar_Start_m3303427069 (UnityARCameraNearFar_t519802600 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARCameraNearFar_Start_m3303427069_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Camera_t189460977 * L_0 = Component_GetComponent_TisCamera_t189460977_m3276577584(__this, /*hidden argument*/Component_GetComponent_TisCamera_t189460977_m3276577584_RuntimeMethod_var);
		__this->set_attachedCamera_2(L_0);
		UnityARCameraNearFar_UpdateCameraClipPlanes_m887086422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityARCameraNearFar::UpdateCameraClipPlanes()
extern "C"  void UnityARCameraNearFar_UpdateCameraClipPlanes_m887086422 (UnityARCameraNearFar_t519802600 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARCameraNearFar_UpdateCameraClipPlanes_m887086422_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Camera_t189460977 * L_0 = __this->get_attachedCamera_2();
		NullCheck(L_0);
		float L_1 = Camera_get_nearClipPlane_m3536967407(L_0, /*hidden argument*/NULL);
		__this->set_currentNearZ_3(L_1);
		Camera_t189460977 * L_2 = __this->get_attachedCamera_2();
		NullCheck(L_2);
		float L_3 = Camera_get_farClipPlane_m3137713566(L_2, /*hidden argument*/NULL);
		__this->set_currentFarZ_4(L_3);
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		UnityARSessionNativeInterface_t1130867170 * L_4 = UnityARSessionNativeInterface_GetARSessionNativeInterface_m3174488657(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_5 = __this->get_currentNearZ_3();
		float L_6 = __this->get_currentFarZ_4();
		NullCheck(L_4);
		UnityARSessionNativeInterface_SetCameraClipPlanes_m149558303(L_4, L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityARCameraNearFar::Update()
extern "C"  void UnityARCameraNearFar_Update_m1832280956 (UnityARCameraNearFar_t519802600 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_currentNearZ_3();
		Camera_t189460977 * L_1 = __this->get_attachedCamera_2();
		NullCheck(L_1);
		float L_2 = Camera_get_nearClipPlane_m3536967407(L_1, /*hidden argument*/NULL);
		if ((!(((float)L_0) == ((float)L_2))))
		{
			goto IL_002c;
		}
	}
	{
		float L_3 = __this->get_currentFarZ_4();
		Camera_t189460977 * L_4 = __this->get_attachedCamera_2();
		NullCheck(L_4);
		float L_5 = Camera_get_farClipPlane_m3137713566(L_4, /*hidden argument*/NULL);
		if ((((float)L_3) == ((float)L_5)))
		{
			goto IL_0032;
		}
	}

IL_002c:
	{
		UnityARCameraNearFar_UpdateCameraClipPlanes_m887086422(__this, /*hidden argument*/NULL);
	}

IL_0032:
	{
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.XR.iOS.ARAnchor
extern "C" void ARAnchor_t1161832412_marshal_pinvoke(const ARAnchor_t1161832412& unmarshaled, ARAnchor_t1161832412_marshaled_pinvoke& marshaled)
{
	marshaled.___identifier_0 = il2cpp_codegen_marshal_string(unmarshaled.get_identifier_0());
	marshaled.___transform_1 = unmarshaled.get_transform_1();
}
extern "C" void ARAnchor_t1161832412_marshal_pinvoke_back(const ARAnchor_t1161832412_marshaled_pinvoke& marshaled, ARAnchor_t1161832412& unmarshaled)
{
	unmarshaled.set_identifier_0(il2cpp_codegen_marshal_string_result(marshaled.___identifier_0));
	Matrix4x4_t2933234003  unmarshaled_transform_temp_1;
	memset(&unmarshaled_transform_temp_1, 0, sizeof(unmarshaled_transform_temp_1));
	unmarshaled_transform_temp_1 = marshaled.___transform_1;
	unmarshaled.set_transform_1(unmarshaled_transform_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.XR.iOS.ARAnchor
extern "C" void ARAnchor_t1161832412_marshal_pinvoke_cleanup(ARAnchor_t1161832412_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___identifier_0);
	marshaled.___identifier_0 = NULL;
}
// Conversion methods for marshalling of: UnityEngine.XR.iOS.ARAnchor
extern "C" void ARAnchor_t1161832412_marshal_com(const ARAnchor_t1161832412& unmarshaled, ARAnchor_t1161832412_marshaled_com& marshaled)
{
	marshaled.___identifier_0 = il2cpp_codegen_marshal_bstring(unmarshaled.get_identifier_0());
	marshaled.___transform_1 = unmarshaled.get_transform_1();
}
extern "C" void ARAnchor_t1161832412_marshal_com_back(const ARAnchor_t1161832412_marshaled_com& marshaled, ARAnchor_t1161832412& unmarshaled)
{
	unmarshaled.set_identifier_0(il2cpp_codegen_marshal_bstring_result(marshaled.___identifier_0));
	Matrix4x4_t2933234003  unmarshaled_transform_temp_1;
	memset(&unmarshaled_transform_temp_1, 0, sizeof(unmarshaled_transform_temp_1));
	unmarshaled_transform_temp_1 = marshaled.___transform_1;
	unmarshaled.set_transform_1(unmarshaled_transform_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.XR.iOS.ARAnchor
extern "C" void ARAnchor_t1161832412_marshal_com_cleanup(ARAnchor_t1161832412_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___identifier_0);
	marshaled.___identifier_0 = NULL;
}
// Conversion methods for marshalling of: UnityEngine.XR.iOS.ARHitTestResult
extern "C" void ARHitTestResult_t3275513025_marshal_pinvoke(const ARHitTestResult_t3275513025& unmarshaled, ARHitTestResult_t3275513025_marshaled_pinvoke& marshaled)
{
	marshaled.___type_0 = unmarshaled.get_type_0();
	marshaled.___distance_1 = unmarshaled.get_distance_1();
	marshaled.___localTransform_2 = unmarshaled.get_localTransform_2();
	marshaled.___worldTransform_3 = unmarshaled.get_worldTransform_3();
	marshaled.___anchorIdentifier_4 = il2cpp_codegen_marshal_string(unmarshaled.get_anchorIdentifier_4());
	marshaled.___isValid_5 = static_cast<int32_t>(unmarshaled.get_isValid_5());
}
extern "C" void ARHitTestResult_t3275513025_marshal_pinvoke_back(const ARHitTestResult_t3275513025_marshaled_pinvoke& marshaled, ARHitTestResult_t3275513025& unmarshaled)
{
	int64_t unmarshaled_type_temp_0 = 0;
	unmarshaled_type_temp_0 = marshaled.___type_0;
	unmarshaled.set_type_0(unmarshaled_type_temp_0);
	double unmarshaled_distance_temp_1 = 0.0;
	unmarshaled_distance_temp_1 = marshaled.___distance_1;
	unmarshaled.set_distance_1(unmarshaled_distance_temp_1);
	Matrix4x4_t2933234003  unmarshaled_localTransform_temp_2;
	memset(&unmarshaled_localTransform_temp_2, 0, sizeof(unmarshaled_localTransform_temp_2));
	unmarshaled_localTransform_temp_2 = marshaled.___localTransform_2;
	unmarshaled.set_localTransform_2(unmarshaled_localTransform_temp_2);
	Matrix4x4_t2933234003  unmarshaled_worldTransform_temp_3;
	memset(&unmarshaled_worldTransform_temp_3, 0, sizeof(unmarshaled_worldTransform_temp_3));
	unmarshaled_worldTransform_temp_3 = marshaled.___worldTransform_3;
	unmarshaled.set_worldTransform_3(unmarshaled_worldTransform_temp_3);
	unmarshaled.set_anchorIdentifier_4(il2cpp_codegen_marshal_string_result(marshaled.___anchorIdentifier_4));
	bool unmarshaled_isValid_temp_5 = false;
	unmarshaled_isValid_temp_5 = static_cast<bool>(marshaled.___isValid_5);
	unmarshaled.set_isValid_5(unmarshaled_isValid_temp_5);
}
// Conversion method for clean up from marshalling of: UnityEngine.XR.iOS.ARHitTestResult
extern "C" void ARHitTestResult_t3275513025_marshal_pinvoke_cleanup(ARHitTestResult_t3275513025_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___anchorIdentifier_4);
	marshaled.___anchorIdentifier_4 = NULL;
}
// Conversion methods for marshalling of: UnityEngine.XR.iOS.ARHitTestResult
extern "C" void ARHitTestResult_t3275513025_marshal_com(const ARHitTestResult_t3275513025& unmarshaled, ARHitTestResult_t3275513025_marshaled_com& marshaled)
{
	marshaled.___type_0 = unmarshaled.get_type_0();
	marshaled.___distance_1 = unmarshaled.get_distance_1();
	marshaled.___localTransform_2 = unmarshaled.get_localTransform_2();
	marshaled.___worldTransform_3 = unmarshaled.get_worldTransform_3();
	marshaled.___anchorIdentifier_4 = il2cpp_codegen_marshal_bstring(unmarshaled.get_anchorIdentifier_4());
	marshaled.___isValid_5 = static_cast<int32_t>(unmarshaled.get_isValid_5());
}
extern "C" void ARHitTestResult_t3275513025_marshal_com_back(const ARHitTestResult_t3275513025_marshaled_com& marshaled, ARHitTestResult_t3275513025& unmarshaled)
{
	int64_t unmarshaled_type_temp_0 = 0;
	unmarshaled_type_temp_0 = marshaled.___type_0;
	unmarshaled.set_type_0(unmarshaled_type_temp_0);
	double unmarshaled_distance_temp_1 = 0.0;
	unmarshaled_distance_temp_1 = marshaled.___distance_1;
	unmarshaled.set_distance_1(unmarshaled_distance_temp_1);
	Matrix4x4_t2933234003  unmarshaled_localTransform_temp_2;
	memset(&unmarshaled_localTransform_temp_2, 0, sizeof(unmarshaled_localTransform_temp_2));
	unmarshaled_localTransform_temp_2 = marshaled.___localTransform_2;
	unmarshaled.set_localTransform_2(unmarshaled_localTransform_temp_2);
	Matrix4x4_t2933234003  unmarshaled_worldTransform_temp_3;
	memset(&unmarshaled_worldTransform_temp_3, 0, sizeof(unmarshaled_worldTransform_temp_3));
	unmarshaled_worldTransform_temp_3 = marshaled.___worldTransform_3;
	unmarshaled.set_worldTransform_3(unmarshaled_worldTransform_temp_3);
	unmarshaled.set_anchorIdentifier_4(il2cpp_codegen_marshal_bstring_result(marshaled.___anchorIdentifier_4));
	bool unmarshaled_isValid_temp_5 = false;
	unmarshaled_isValid_temp_5 = static_cast<bool>(marshaled.___isValid_5);
	unmarshaled.set_isValid_5(unmarshaled_isValid_temp_5);
}
// Conversion method for clean up from marshalling of: UnityEngine.XR.iOS.ARHitTestResult
extern "C" void ARHitTestResult_t3275513025_marshal_com_cleanup(ARHitTestResult_t3275513025_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___anchorIdentifier_4);
	marshaled.___anchorIdentifier_4 = NULL;
}
// Conversion methods for marshalling of: UnityEngine.XR.iOS.ARKitSessionConfiguration
extern "C" void ARKitSessionConfiguration_t318899795_marshal_pinvoke(const ARKitSessionConfiguration_t318899795& unmarshaled, ARKitSessionConfiguration_t318899795_marshaled_pinvoke& marshaled)
{
	marshaled.___alignment_0 = unmarshaled.get_alignment_0();
	marshaled.___getPointCloudData_1 = static_cast<int32_t>(unmarshaled.get_getPointCloudData_1());
	marshaled.___enableLightEstimation_2 = static_cast<int32_t>(unmarshaled.get_enableLightEstimation_2());
}
extern "C" void ARKitSessionConfiguration_t318899795_marshal_pinvoke_back(const ARKitSessionConfiguration_t318899795_marshaled_pinvoke& marshaled, ARKitSessionConfiguration_t318899795& unmarshaled)
{
	int32_t unmarshaled_alignment_temp_0 = 0;
	unmarshaled_alignment_temp_0 = marshaled.___alignment_0;
	unmarshaled.set_alignment_0(unmarshaled_alignment_temp_0);
	bool unmarshaled_getPointCloudData_temp_1 = false;
	unmarshaled_getPointCloudData_temp_1 = static_cast<bool>(marshaled.___getPointCloudData_1);
	unmarshaled.set_getPointCloudData_1(unmarshaled_getPointCloudData_temp_1);
	bool unmarshaled_enableLightEstimation_temp_2 = false;
	unmarshaled_enableLightEstimation_temp_2 = static_cast<bool>(marshaled.___enableLightEstimation_2);
	unmarshaled.set_enableLightEstimation_2(unmarshaled_enableLightEstimation_temp_2);
}
// Conversion method for clean up from marshalling of: UnityEngine.XR.iOS.ARKitSessionConfiguration
extern "C" void ARKitSessionConfiguration_t318899795_marshal_pinvoke_cleanup(ARKitSessionConfiguration_t318899795_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.XR.iOS.ARKitSessionConfiguration
extern "C" void ARKitSessionConfiguration_t318899795_marshal_com(const ARKitSessionConfiguration_t318899795& unmarshaled, ARKitSessionConfiguration_t318899795_marshaled_com& marshaled)
{
	marshaled.___alignment_0 = unmarshaled.get_alignment_0();
	marshaled.___getPointCloudData_1 = static_cast<int32_t>(unmarshaled.get_getPointCloudData_1());
	marshaled.___enableLightEstimation_2 = static_cast<int32_t>(unmarshaled.get_enableLightEstimation_2());
}
extern "C" void ARKitSessionConfiguration_t318899795_marshal_com_back(const ARKitSessionConfiguration_t318899795_marshaled_com& marshaled, ARKitSessionConfiguration_t318899795& unmarshaled)
{
	int32_t unmarshaled_alignment_temp_0 = 0;
	unmarshaled_alignment_temp_0 = marshaled.___alignment_0;
	unmarshaled.set_alignment_0(unmarshaled_alignment_temp_0);
	bool unmarshaled_getPointCloudData_temp_1 = false;
	unmarshaled_getPointCloudData_temp_1 = static_cast<bool>(marshaled.___getPointCloudData_1);
	unmarshaled.set_getPointCloudData_1(unmarshaled_getPointCloudData_temp_1);
	bool unmarshaled_enableLightEstimation_temp_2 = false;
	unmarshaled_enableLightEstimation_temp_2 = static_cast<bool>(marshaled.___enableLightEstimation_2);
	unmarshaled.set_enableLightEstimation_2(unmarshaled_enableLightEstimation_temp_2);
}
// Conversion method for clean up from marshalling of: UnityEngine.XR.iOS.ARKitSessionConfiguration
extern "C" void ARKitSessionConfiguration_t318899795_marshal_com_cleanup(ARKitSessionConfiguration_t318899795_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.XR.iOS.ARKitSessionConfiguration::.ctor(UnityEngine.XR.iOS.UnityARAlignment,System.Boolean,System.Boolean)
extern "C"  void ARKitSessionConfiguration__ctor_m3221520625 (ARKitSessionConfiguration_t318899795 * __this, int32_t ___alignment0, bool ___getPointCloudData1, bool ___enableLightEstimation2, const RuntimeMethod* method)
{
	{
		bool L_0 = ___getPointCloudData1;
		__this->set_getPointCloudData_1(L_0);
		int32_t L_1 = ___alignment0;
		__this->set_alignment_0(L_1);
		bool L_2 = ___enableLightEstimation2;
		__this->set_enableLightEstimation_2(L_2);
		return;
	}
}
extern "C"  void ARKitSessionConfiguration__ctor_m3221520625_AdjustorThunk (RuntimeObject * __this, int32_t ___alignment0, bool ___getPointCloudData1, bool ___enableLightEstimation2, const RuntimeMethod* method)
{
	ARKitSessionConfiguration_t318899795 * _thisAdjusted = reinterpret_cast<ARKitSessionConfiguration_t318899795 *>(__this + 1);
	ARKitSessionConfiguration__ctor_m3221520625(_thisAdjusted, ___alignment0, ___getPointCloudData1, ___enableLightEstimation2, method);
}
// System.Boolean UnityEngine.XR.iOS.ARKitSessionConfiguration::get_IsSupported()
extern "C"  bool ARKitSessionConfiguration_get_IsSupported_m919877731 (ARKitSessionConfiguration_t318899795 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = ARKitSessionConfiguration_IsARKitSessionConfigurationSupported_m3959947945(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
extern "C"  bool ARKitSessionConfiguration_get_IsSupported_m919877731_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	ARKitSessionConfiguration_t318899795 * _thisAdjusted = reinterpret_cast<ARKitSessionConfiguration_t318899795 *>(__this + 1);
	return ARKitSessionConfiguration_get_IsSupported_m919877731(_thisAdjusted, method);
}
// System.Void UnityEngine.XR.iOS.ARKitSessionConfiguration::set_IsSupported(System.Boolean)
extern "C"  void ARKitSessionConfiguration_set_IsSupported_m138323976 (ARKitSessionConfiguration_t318899795 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		return;
	}
}
extern "C"  void ARKitSessionConfiguration_set_IsSupported_m138323976_AdjustorThunk (RuntimeObject * __this, bool ___value0, const RuntimeMethod* method)
{
	ARKitSessionConfiguration_t318899795 * _thisAdjusted = reinterpret_cast<ARKitSessionConfiguration_t318899795 *>(__this + 1);
	ARKitSessionConfiguration_set_IsSupported_m138323976(_thisAdjusted, ___value0, method);
}
extern "C" int32_t DEFAULT_CALL IsARKitSessionConfigurationSupported();
// System.Boolean UnityEngine.XR.iOS.ARKitSessionConfiguration::IsARKitSessionConfigurationSupported()
extern "C"  bool ARKitSessionConfiguration_IsARKitSessionConfigurationSupported_m3959947945 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(IsARKitSessionConfigurationSupported)();

	return static_cast<bool>(returnValue);
}
// Conversion methods for marshalling of: UnityEngine.XR.iOS.ARKitWorldTackingSessionConfiguration
extern "C" void ARKitWorldTackingSessionConfiguration_t1821734930_marshal_pinvoke(const ARKitWorldTackingSessionConfiguration_t1821734930& unmarshaled, ARKitWorldTackingSessionConfiguration_t1821734930_marshaled_pinvoke& marshaled)
{
	marshaled.___alignment_0 = unmarshaled.get_alignment_0();
	marshaled.___planeDetection_1 = unmarshaled.get_planeDetection_1();
	marshaled.___getPointCloudData_2 = static_cast<int32_t>(unmarshaled.get_getPointCloudData_2());
	marshaled.___enableLightEstimation_3 = static_cast<int32_t>(unmarshaled.get_enableLightEstimation_3());
}
extern "C" void ARKitWorldTackingSessionConfiguration_t1821734930_marshal_pinvoke_back(const ARKitWorldTackingSessionConfiguration_t1821734930_marshaled_pinvoke& marshaled, ARKitWorldTackingSessionConfiguration_t1821734930& unmarshaled)
{
	int32_t unmarshaled_alignment_temp_0 = 0;
	unmarshaled_alignment_temp_0 = marshaled.___alignment_0;
	unmarshaled.set_alignment_0(unmarshaled_alignment_temp_0);
	int32_t unmarshaled_planeDetection_temp_1 = 0;
	unmarshaled_planeDetection_temp_1 = marshaled.___planeDetection_1;
	unmarshaled.set_planeDetection_1(unmarshaled_planeDetection_temp_1);
	bool unmarshaled_getPointCloudData_temp_2 = false;
	unmarshaled_getPointCloudData_temp_2 = static_cast<bool>(marshaled.___getPointCloudData_2);
	unmarshaled.set_getPointCloudData_2(unmarshaled_getPointCloudData_temp_2);
	bool unmarshaled_enableLightEstimation_temp_3 = false;
	unmarshaled_enableLightEstimation_temp_3 = static_cast<bool>(marshaled.___enableLightEstimation_3);
	unmarshaled.set_enableLightEstimation_3(unmarshaled_enableLightEstimation_temp_3);
}
// Conversion method for clean up from marshalling of: UnityEngine.XR.iOS.ARKitWorldTackingSessionConfiguration
extern "C" void ARKitWorldTackingSessionConfiguration_t1821734930_marshal_pinvoke_cleanup(ARKitWorldTackingSessionConfiguration_t1821734930_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.XR.iOS.ARKitWorldTackingSessionConfiguration
extern "C" void ARKitWorldTackingSessionConfiguration_t1821734930_marshal_com(const ARKitWorldTackingSessionConfiguration_t1821734930& unmarshaled, ARKitWorldTackingSessionConfiguration_t1821734930_marshaled_com& marshaled)
{
	marshaled.___alignment_0 = unmarshaled.get_alignment_0();
	marshaled.___planeDetection_1 = unmarshaled.get_planeDetection_1();
	marshaled.___getPointCloudData_2 = static_cast<int32_t>(unmarshaled.get_getPointCloudData_2());
	marshaled.___enableLightEstimation_3 = static_cast<int32_t>(unmarshaled.get_enableLightEstimation_3());
}
extern "C" void ARKitWorldTackingSessionConfiguration_t1821734930_marshal_com_back(const ARKitWorldTackingSessionConfiguration_t1821734930_marshaled_com& marshaled, ARKitWorldTackingSessionConfiguration_t1821734930& unmarshaled)
{
	int32_t unmarshaled_alignment_temp_0 = 0;
	unmarshaled_alignment_temp_0 = marshaled.___alignment_0;
	unmarshaled.set_alignment_0(unmarshaled_alignment_temp_0);
	int32_t unmarshaled_planeDetection_temp_1 = 0;
	unmarshaled_planeDetection_temp_1 = marshaled.___planeDetection_1;
	unmarshaled.set_planeDetection_1(unmarshaled_planeDetection_temp_1);
	bool unmarshaled_getPointCloudData_temp_2 = false;
	unmarshaled_getPointCloudData_temp_2 = static_cast<bool>(marshaled.___getPointCloudData_2);
	unmarshaled.set_getPointCloudData_2(unmarshaled_getPointCloudData_temp_2);
	bool unmarshaled_enableLightEstimation_temp_3 = false;
	unmarshaled_enableLightEstimation_temp_3 = static_cast<bool>(marshaled.___enableLightEstimation_3);
	unmarshaled.set_enableLightEstimation_3(unmarshaled_enableLightEstimation_temp_3);
}
// Conversion method for clean up from marshalling of: UnityEngine.XR.iOS.ARKitWorldTackingSessionConfiguration
extern "C" void ARKitWorldTackingSessionConfiguration_t1821734930_marshal_com_cleanup(ARKitWorldTackingSessionConfiguration_t1821734930_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.XR.iOS.ARKitWorldTackingSessionConfiguration::.ctor(UnityEngine.XR.iOS.UnityARAlignment,UnityEngine.XR.iOS.UnityARPlaneDetection,System.Boolean,System.Boolean)
extern "C"  void ARKitWorldTackingSessionConfiguration__ctor_m4281543687 (ARKitWorldTackingSessionConfiguration_t1821734930 * __this, int32_t ___alignment0, int32_t ___planeDetection1, bool ___getPointCloudData2, bool ___enableLightEstimation3, const RuntimeMethod* method)
{
	{
		bool L_0 = ___getPointCloudData2;
		__this->set_getPointCloudData_2(L_0);
		int32_t L_1 = ___alignment0;
		__this->set_alignment_0(L_1);
		int32_t L_2 = ___planeDetection1;
		__this->set_planeDetection_1(L_2);
		bool L_3 = ___enableLightEstimation3;
		__this->set_enableLightEstimation_3(L_3);
		return;
	}
}
extern "C"  void ARKitWorldTackingSessionConfiguration__ctor_m4281543687_AdjustorThunk (RuntimeObject * __this, int32_t ___alignment0, int32_t ___planeDetection1, bool ___getPointCloudData2, bool ___enableLightEstimation3, const RuntimeMethod* method)
{
	ARKitWorldTackingSessionConfiguration_t1821734930 * _thisAdjusted = reinterpret_cast<ARKitWorldTackingSessionConfiguration_t1821734930 *>(__this + 1);
	ARKitWorldTackingSessionConfiguration__ctor_m4281543687(_thisAdjusted, ___alignment0, ___planeDetection1, ___getPointCloudData2, ___enableLightEstimation3, method);
}
// System.Boolean UnityEngine.XR.iOS.ARKitWorldTackingSessionConfiguration::get_IsSupported()
extern "C"  bool ARKitWorldTackingSessionConfiguration_get_IsSupported_m3789991538 (ARKitWorldTackingSessionConfiguration_t1821734930 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = ARKitWorldTackingSessionConfiguration_IsARKitWorldTrackingSessionConfigurationSupported_m34846127(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
extern "C"  bool ARKitWorldTackingSessionConfiguration_get_IsSupported_m3789991538_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	ARKitWorldTackingSessionConfiguration_t1821734930 * _thisAdjusted = reinterpret_cast<ARKitWorldTackingSessionConfiguration_t1821734930 *>(__this + 1);
	return ARKitWorldTackingSessionConfiguration_get_IsSupported_m3789991538(_thisAdjusted, method);
}
// System.Void UnityEngine.XR.iOS.ARKitWorldTackingSessionConfiguration::set_IsSupported(System.Boolean)
extern "C"  void ARKitWorldTackingSessionConfiguration_set_IsSupported_m46319519 (ARKitWorldTackingSessionConfiguration_t1821734930 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		return;
	}
}
extern "C"  void ARKitWorldTackingSessionConfiguration_set_IsSupported_m46319519_AdjustorThunk (RuntimeObject * __this, bool ___value0, const RuntimeMethod* method)
{
	ARKitWorldTackingSessionConfiguration_t1821734930 * _thisAdjusted = reinterpret_cast<ARKitWorldTackingSessionConfiguration_t1821734930 *>(__this + 1);
	ARKitWorldTackingSessionConfiguration_set_IsSupported_m46319519(_thisAdjusted, ___value0, method);
}
extern "C" int32_t DEFAULT_CALL IsARKitWorldTrackingSessionConfigurationSupported();
// System.Boolean UnityEngine.XR.iOS.ARKitWorldTackingSessionConfiguration::IsARKitWorldTrackingSessionConfigurationSupported()
extern "C"  bool ARKitWorldTackingSessionConfiguration_IsARKitWorldTrackingSessionConfigurationSupported_m34846127 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(IsARKitWorldTrackingSessionConfigurationSupported)();

	return static_cast<bool>(returnValue);
}
// Conversion methods for marshalling of: UnityEngine.XR.iOS.ARPlaneAnchor
extern "C" void ARPlaneAnchor_t1439520888_marshal_pinvoke(const ARPlaneAnchor_t1439520888& unmarshaled, ARPlaneAnchor_t1439520888_marshaled_pinvoke& marshaled)
{
	marshaled.___identifier_0 = il2cpp_codegen_marshal_string(unmarshaled.get_identifier_0());
	marshaled.___transform_1 = unmarshaled.get_transform_1();
	marshaled.___alignment_2 = unmarshaled.get_alignment_2();
	marshaled.___center_3 = unmarshaled.get_center_3();
	marshaled.___extent_4 = unmarshaled.get_extent_4();
}
extern "C" void ARPlaneAnchor_t1439520888_marshal_pinvoke_back(const ARPlaneAnchor_t1439520888_marshaled_pinvoke& marshaled, ARPlaneAnchor_t1439520888& unmarshaled)
{
	unmarshaled.set_identifier_0(il2cpp_codegen_marshal_string_result(marshaled.___identifier_0));
	Matrix4x4_t2933234003  unmarshaled_transform_temp_1;
	memset(&unmarshaled_transform_temp_1, 0, sizeof(unmarshaled_transform_temp_1));
	unmarshaled_transform_temp_1 = marshaled.___transform_1;
	unmarshaled.set_transform_1(unmarshaled_transform_temp_1);
	int64_t unmarshaled_alignment_temp_2 = 0;
	unmarshaled_alignment_temp_2 = marshaled.___alignment_2;
	unmarshaled.set_alignment_2(unmarshaled_alignment_temp_2);
	Vector3_t2243707580  unmarshaled_center_temp_3;
	memset(&unmarshaled_center_temp_3, 0, sizeof(unmarshaled_center_temp_3));
	unmarshaled_center_temp_3 = marshaled.___center_3;
	unmarshaled.set_center_3(unmarshaled_center_temp_3);
	Vector3_t2243707580  unmarshaled_extent_temp_4;
	memset(&unmarshaled_extent_temp_4, 0, sizeof(unmarshaled_extent_temp_4));
	unmarshaled_extent_temp_4 = marshaled.___extent_4;
	unmarshaled.set_extent_4(unmarshaled_extent_temp_4);
}
// Conversion method for clean up from marshalling of: UnityEngine.XR.iOS.ARPlaneAnchor
extern "C" void ARPlaneAnchor_t1439520888_marshal_pinvoke_cleanup(ARPlaneAnchor_t1439520888_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___identifier_0);
	marshaled.___identifier_0 = NULL;
}
// Conversion methods for marshalling of: UnityEngine.XR.iOS.ARPlaneAnchor
extern "C" void ARPlaneAnchor_t1439520888_marshal_com(const ARPlaneAnchor_t1439520888& unmarshaled, ARPlaneAnchor_t1439520888_marshaled_com& marshaled)
{
	marshaled.___identifier_0 = il2cpp_codegen_marshal_bstring(unmarshaled.get_identifier_0());
	marshaled.___transform_1 = unmarshaled.get_transform_1();
	marshaled.___alignment_2 = unmarshaled.get_alignment_2();
	marshaled.___center_3 = unmarshaled.get_center_3();
	marshaled.___extent_4 = unmarshaled.get_extent_4();
}
extern "C" void ARPlaneAnchor_t1439520888_marshal_com_back(const ARPlaneAnchor_t1439520888_marshaled_com& marshaled, ARPlaneAnchor_t1439520888& unmarshaled)
{
	unmarshaled.set_identifier_0(il2cpp_codegen_marshal_bstring_result(marshaled.___identifier_0));
	Matrix4x4_t2933234003  unmarshaled_transform_temp_1;
	memset(&unmarshaled_transform_temp_1, 0, sizeof(unmarshaled_transform_temp_1));
	unmarshaled_transform_temp_1 = marshaled.___transform_1;
	unmarshaled.set_transform_1(unmarshaled_transform_temp_1);
	int64_t unmarshaled_alignment_temp_2 = 0;
	unmarshaled_alignment_temp_2 = marshaled.___alignment_2;
	unmarshaled.set_alignment_2(unmarshaled_alignment_temp_2);
	Vector3_t2243707580  unmarshaled_center_temp_3;
	memset(&unmarshaled_center_temp_3, 0, sizeof(unmarshaled_center_temp_3));
	unmarshaled_center_temp_3 = marshaled.___center_3;
	unmarshaled.set_center_3(unmarshaled_center_temp_3);
	Vector3_t2243707580  unmarshaled_extent_temp_4;
	memset(&unmarshaled_extent_temp_4, 0, sizeof(unmarshaled_extent_temp_4));
	unmarshaled_extent_temp_4 = marshaled.___extent_4;
	unmarshaled.set_extent_4(unmarshaled_extent_temp_4);
}
// Conversion method for clean up from marshalling of: UnityEngine.XR.iOS.ARPlaneAnchor
extern "C" void ARPlaneAnchor_t1439520888_marshal_com_cleanup(ARPlaneAnchor_t1439520888_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___identifier_0);
	marshaled.___identifier_0 = NULL;
}
// System.Void UnityEngine.XR.iOS.ARPlaneAnchorGameObject::.ctor()
extern "C"  void ARPlaneAnchorGameObject__ctor_m3751462836 (ARPlaneAnchorGameObject_t2305225887 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARAnchorManager::.ctor()
extern "C"  void UnityARAnchorManager__ctor_m1861900703 (UnityARAnchorManager_t1086564192 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARAnchorManager__ctor_m1861900703_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		Dictionary_2_t4220005149 * L_0 = (Dictionary_2_t4220005149 *)il2cpp_codegen_object_new(Dictionary_2_t4220005149_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1722416527(L_0, /*hidden argument*/Dictionary_2__ctor_m1722416527_RuntimeMethod_var);
		__this->set_planeAnchorMap_0(L_0);
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)UnityARAnchorManager_AddAnchor_m2816330223_RuntimeMethod_var);
		ARAnchorAdded_t2646854145 * L_2 = (ARAnchorAdded_t2646854145 *)il2cpp_codegen_object_new(ARAnchorAdded_t2646854145_il2cpp_TypeInfo_var);
		ARAnchorAdded__ctor_m3844186700(L_2, __this, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		UnityARSessionNativeInterface_add_ARAnchorAddedEvent_m658032036(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)UnityARAnchorManager_UpdateAnchor_m4142154447_RuntimeMethod_var);
		ARAnchorUpdated_t3886071158 * L_4 = (ARAnchorUpdated_t3886071158 *)il2cpp_codegen_object_new(ARAnchorUpdated_t3886071158_il2cpp_TypeInfo_var);
		ARAnchorUpdated__ctor_m1158457407(L_4, __this, L_3, /*hidden argument*/NULL);
		UnityARSessionNativeInterface_add_ARAnchorUpdatedEvent_m1485763364(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)UnityARAnchorManager_RemoveAnchor_m2109073956_RuntimeMethod_var);
		ARAnchorRemoved_t142665927 * L_6 = (ARAnchorRemoved_t142665927 *)il2cpp_codegen_object_new(ARAnchorRemoved_t142665927_il2cpp_TypeInfo_var);
		ARAnchorRemoved__ctor_m535635486(L_6, __this, L_5, /*hidden argument*/NULL);
		UnityARSessionNativeInterface_add_ARAnchorRemovedEvent_m2970646020(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARAnchorManager::AddAnchor(UnityEngine.XR.iOS.ARPlaneAnchor)
extern "C"  void UnityARAnchorManager_AddAnchor_m2816330223 (UnityARAnchorManager_t1086564192 * __this, ARPlaneAnchor_t1439520888  ___arPlaneAnchor0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARAnchorManager_AddAnchor_m2816330223_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	ARPlaneAnchorGameObject_t2305225887 * V_1 = NULL;
	{
		ARPlaneAnchor_t1439520888  L_0 = ___arPlaneAnchor0;
		IL2CPP_RUNTIME_CLASS_INIT(UnityARUtility_t3608388148_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_1 = UnityARUtility_CreatePlaneInScene_m836370693(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		GameObject_t1756533147 * L_2 = V_0;
		NullCheck(L_2);
		GameObject_AddComponent_TisDontDestroyOnLoad_t3235789354_m4215585572(L_2, /*hidden argument*/GameObject_AddComponent_TisDontDestroyOnLoad_t3235789354_m4215585572_RuntimeMethod_var);
		ARPlaneAnchorGameObject_t2305225887 * L_3 = (ARPlaneAnchorGameObject_t2305225887 *)il2cpp_codegen_object_new(ARPlaneAnchorGameObject_t2305225887_il2cpp_TypeInfo_var);
		ARPlaneAnchorGameObject__ctor_m3751462836(L_3, /*hidden argument*/NULL);
		V_1 = L_3;
		ARPlaneAnchorGameObject_t2305225887 * L_4 = V_1;
		ARPlaneAnchor_t1439520888  L_5 = ___arPlaneAnchor0;
		NullCheck(L_4);
		L_4->set_planeAnchor_1(L_5);
		ARPlaneAnchorGameObject_t2305225887 * L_6 = V_1;
		GameObject_t1756533147 * L_7 = V_0;
		NullCheck(L_6);
		L_6->set_gameObject_0(L_7);
		Dictionary_2_t4220005149 * L_8 = __this->get_planeAnchorMap_0();
		String_t* L_9 = (&___arPlaneAnchor0)->get_identifier_0();
		ARPlaneAnchorGameObject_t2305225887 * L_10 = V_1;
		NullCheck(L_8);
		Dictionary_2_Add_m2748008583(L_8, L_9, L_10, /*hidden argument*/Dictionary_2_Add_m2748008583_RuntimeMethod_var);
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARAnchorManager::RemoveAnchor(UnityEngine.XR.iOS.ARPlaneAnchor)
extern "C"  void UnityARAnchorManager_RemoveAnchor_m2109073956 (UnityARAnchorManager_t1086564192 * __this, ARPlaneAnchor_t1439520888  ___arPlaneAnchor0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARAnchorManager_RemoveAnchor_m2109073956_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ARPlaneAnchorGameObject_t2305225887 * V_0 = NULL;
	{
		Dictionary_2_t4220005149 * L_0 = __this->get_planeAnchorMap_0();
		String_t* L_1 = (&___arPlaneAnchor0)->get_identifier_0();
		NullCheck(L_0);
		bool L_2 = Dictionary_2_ContainsKey_m9048208(L_0, L_1, /*hidden argument*/Dictionary_2_ContainsKey_m9048208_RuntimeMethod_var);
		if (!L_2)
		{
			goto IL_0048;
		}
	}
	{
		Dictionary_2_t4220005149 * L_3 = __this->get_planeAnchorMap_0();
		String_t* L_4 = (&___arPlaneAnchor0)->get_identifier_0();
		NullCheck(L_3);
		ARPlaneAnchorGameObject_t2305225887 * L_5 = Dictionary_2_get_Item_m4111408485(L_3, L_4, /*hidden argument*/Dictionary_2_get_Item_m4111408485_RuntimeMethod_var);
		V_0 = L_5;
		ARPlaneAnchorGameObject_t2305225887 * L_6 = V_0;
		NullCheck(L_6);
		GameObject_t1756533147 * L_7 = L_6->get_gameObject_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		Dictionary_2_t4220005149 * L_8 = __this->get_planeAnchorMap_0();
		String_t* L_9 = (&___arPlaneAnchor0)->get_identifier_0();
		NullCheck(L_8);
		Dictionary_2_Remove_m2895225056(L_8, L_9, /*hidden argument*/Dictionary_2_Remove_m2895225056_RuntimeMethod_var);
	}

IL_0048:
	{
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARAnchorManager::UpdateAnchor(UnityEngine.XR.iOS.ARPlaneAnchor)
extern "C"  void UnityARAnchorManager_UpdateAnchor_m4142154447 (UnityARAnchorManager_t1086564192 * __this, ARPlaneAnchor_t1439520888  ___arPlaneAnchor0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARAnchorManager_UpdateAnchor_m4142154447_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ARPlaneAnchorGameObject_t2305225887 * V_0 = NULL;
	{
		Dictionary_2_t4220005149 * L_0 = __this->get_planeAnchorMap_0();
		String_t* L_1 = (&___arPlaneAnchor0)->get_identifier_0();
		NullCheck(L_0);
		bool L_2 = Dictionary_2_ContainsKey_m9048208(L_0, L_1, /*hidden argument*/Dictionary_2_ContainsKey_m9048208_RuntimeMethod_var);
		if (!L_2)
		{
			goto IL_0051;
		}
	}
	{
		Dictionary_2_t4220005149 * L_3 = __this->get_planeAnchorMap_0();
		String_t* L_4 = (&___arPlaneAnchor0)->get_identifier_0();
		NullCheck(L_3);
		ARPlaneAnchorGameObject_t2305225887 * L_5 = Dictionary_2_get_Item_m4111408485(L_3, L_4, /*hidden argument*/Dictionary_2_get_Item_m4111408485_RuntimeMethod_var);
		V_0 = L_5;
		ARPlaneAnchorGameObject_t2305225887 * L_6 = V_0;
		NullCheck(L_6);
		GameObject_t1756533147 * L_7 = L_6->get_gameObject_0();
		ARPlaneAnchor_t1439520888  L_8 = ___arPlaneAnchor0;
		IL2CPP_RUNTIME_CLASS_INIT(UnityARUtility_t3608388148_il2cpp_TypeInfo_var);
		UnityARUtility_UpdatePlaneWithAnchorTransform_m639257622(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		ARPlaneAnchorGameObject_t2305225887 * L_9 = V_0;
		ARPlaneAnchor_t1439520888  L_10 = ___arPlaneAnchor0;
		NullCheck(L_9);
		L_9->set_planeAnchor_1(L_10);
		Dictionary_2_t4220005149 * L_11 = __this->get_planeAnchorMap_0();
		String_t* L_12 = (&___arPlaneAnchor0)->get_identifier_0();
		ARPlaneAnchorGameObject_t2305225887 * L_13 = V_0;
		NullCheck(L_11);
		Dictionary_2_set_Item_m3140527944(L_11, L_12, L_13, /*hidden argument*/Dictionary_2_set_Item_m3140527944_RuntimeMethod_var);
	}

IL_0051:
	{
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARAnchorManager::Destroy()
extern "C"  void UnityARAnchorManager_Destroy_m1751028949 (UnityARAnchorManager_t1086564192 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARAnchorManager_Destroy_m1751028949_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ARPlaneAnchorGameObject_t2305225887 * V_0 = NULL;
	Enumerator_t1209076693  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t1674347019 * L_0 = UnityARAnchorManager_GetCurrentPlaneAnchors_m642073964(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Enumerator_t1209076693  L_1 = List_1_GetEnumerator_m4260200565(L_0, /*hidden argument*/List_1_GetEnumerator_m4260200565_RuntimeMethod_var);
		V_1 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0024;
		}

IL_0011:
		{
			ARPlaneAnchorGameObject_t2305225887 * L_2 = Enumerator_get_Current_m1081001953((&V_1), /*hidden argument*/Enumerator_get_Current_m1081001953_RuntimeMethod_var);
			V_0 = L_2;
			ARPlaneAnchorGameObject_t2305225887 * L_3 = V_0;
			NullCheck(L_3);
			GameObject_t1756533147 * L_4 = L_3->get_gameObject_0();
			IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
			Object_Destroy_m4145850038(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		}

IL_0024:
		{
			bool L_5 = Enumerator_MoveNext_m3579574253((&V_1), /*hidden argument*/Enumerator_MoveNext_m3579574253_RuntimeMethod_var);
			if (L_5)
			{
				goto IL_0011;
			}
		}

IL_0030:
		{
			IL2CPP_LEAVE(0x43, FINALLY_0035);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0035;
	}

FINALLY_0035:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m113660709((&V_1), /*hidden argument*/Enumerator_Dispose_m113660709_RuntimeMethod_var);
		IL2CPP_END_FINALLY(53)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(53)
	{
		IL2CPP_JUMP_TBL(0x43, IL_0043)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0043:
	{
		Dictionary_2_t4220005149 * L_6 = __this->get_planeAnchorMap_0();
		NullCheck(L_6);
		Dictionary_2_Clear_m797095524(L_6, /*hidden argument*/Dictionary_2_Clear_m797095524_RuntimeMethod_var);
		return;
	}
}
// System.Collections.Generic.List`1<UnityEngine.XR.iOS.ARPlaneAnchorGameObject> UnityEngine.XR.iOS.UnityARAnchorManager::GetCurrentPlaneAnchors()
extern "C"  List_1_t1674347019 * UnityARAnchorManager_GetCurrentPlaneAnchors_m642073964 (UnityARAnchorManager_t1086564192 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARAnchorManager_GetCurrentPlaneAnchors_m642073964_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t4220005149 * L_0 = __this->get_planeAnchorMap_0();
		NullCheck(L_0);
		ValueCollection_t2923064992 * L_1 = Dictionary_2_get_Values_m586969881(L_0, /*hidden argument*/Dictionary_2_get_Values_m586969881_RuntimeMethod_var);
		List_1_t1674347019 * L_2 = Enumerable_ToList_TisARPlaneAnchorGameObject_t2305225887_m3769884394(NULL /*static, unused*/, L_1, /*hidden argument*/Enumerable_ToList_TisARPlaneAnchorGameObject_t2305225887_m3769884394_RuntimeMethod_var);
		return L_2;
	}
}
// Conversion methods for marshalling of: UnityEngine.XR.iOS.UnityARCamera
extern "C" void UnityARCamera_t4198559457_marshal_pinvoke(const UnityARCamera_t4198559457& unmarshaled, UnityARCamera_t4198559457_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___pointCloudData_4Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'pointCloudData' of type 'UnityARCamera'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___pointCloudData_4Exception);
}
extern "C" void UnityARCamera_t4198559457_marshal_pinvoke_back(const UnityARCamera_t4198559457_marshaled_pinvoke& marshaled, UnityARCamera_t4198559457& unmarshaled)
{
	Il2CppCodeGenException* ___pointCloudData_4Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'pointCloudData' of type 'UnityARCamera'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___pointCloudData_4Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.XR.iOS.UnityARCamera
extern "C" void UnityARCamera_t4198559457_marshal_pinvoke_cleanup(UnityARCamera_t4198559457_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.XR.iOS.UnityARCamera
extern "C" void UnityARCamera_t4198559457_marshal_com(const UnityARCamera_t4198559457& unmarshaled, UnityARCamera_t4198559457_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___pointCloudData_4Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'pointCloudData' of type 'UnityARCamera'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___pointCloudData_4Exception);
}
extern "C" void UnityARCamera_t4198559457_marshal_com_back(const UnityARCamera_t4198559457_marshaled_com& marshaled, UnityARCamera_t4198559457& unmarshaled)
{
	Il2CppCodeGenException* ___pointCloudData_4Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'pointCloudData' of type 'UnityARCamera'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___pointCloudData_4Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.XR.iOS.UnityARCamera
extern "C" void UnityARCamera_t4198559457_marshal_com_cleanup(UnityARCamera_t4198559457_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.XR.iOS.UnityARGeneratePlane::.ctor()
extern "C"  void UnityARGeneratePlane__ctor_m691174942 (UnityARGeneratePlane_t3368998101 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARGeneratePlane::Start()
extern "C"  void UnityARGeneratePlane_Start_m2009009798 (UnityARGeneratePlane_t3368998101 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARGeneratePlane_Start_m2009009798_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityARAnchorManager_t1086564192 * L_0 = (UnityARAnchorManager_t1086564192 *)il2cpp_codegen_object_new(UnityARAnchorManager_t1086564192_il2cpp_TypeInfo_var);
		UnityARAnchorManager__ctor_m1861900703(L_0, /*hidden argument*/NULL);
		__this->set_unityARAnchorManager_3(L_0);
		GameObject_t1756533147 * L_1 = __this->get_planePrefab_2();
		IL2CPP_RUNTIME_CLASS_INIT(UnityARUtility_t3608388148_il2cpp_TypeInfo_var);
		UnityARUtility_InitializePlanePrefab_m2887188869(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARGeneratePlane::OnDestroy()
extern "C"  void UnityARGeneratePlane_OnDestroy_m3739140111 (UnityARGeneratePlane_t3368998101 * __this, const RuntimeMethod* method)
{
	{
		UnityARAnchorManager_t1086564192 * L_0 = __this->get_unityARAnchorManager_3();
		NullCheck(L_0);
		UnityARAnchorManager_Destroy_m1751028949(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARGeneratePlane::OnGUI()
extern "C"  void UnityARGeneratePlane_OnGUI_m3343046494 (UnityARGeneratePlane_t3368998101 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARGeneratePlane_OnGUI_m3343046494_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t1674347019 * V_0 = NULL;
	{
		UnityARAnchorManager_t1086564192 * L_0 = __this->get_unityARAnchorManager_3();
		NullCheck(L_0);
		List_1_t1674347019 * L_1 = UnityARAnchorManager_GetCurrentPlaneAnchors_m642073964(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		List_1_t1674347019 * L_2 = V_0;
		NullCheck(L_2);
		int32_t L_3 = List_1_get_Count_m1714425968(L_2, /*hidden argument*/List_1_get_Count_m1714425968_RuntimeMethod_var);
		if ((((int32_t)L_3) < ((int32_t)1)))
		{
			goto IL_0018;
		}
	}

IL_0018:
	{
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARHitTestExample::.ctor()
extern "C"  void UnityARHitTestExample__ctor_m737431788 (UnityARHitTestExample_t146867607 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.XR.iOS.UnityARHitTestExample::HitTestWithResultType(UnityEngine.XR.iOS.ARPoint,UnityEngine.XR.iOS.ARHitTestResultType)
extern "C"  bool UnityARHitTestExample_HitTestWithResultType_m996939112 (UnityARHitTestExample_t146867607 * __this, ARPoint_t3436811567  ___point0, int64_t ___resultTypes1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARHitTestExample_HitTestWithResultType_m996939112_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t2644634157 * V_0 = NULL;
	ARHitTestResult_t3275513025  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Enumerator_t2179363831  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t2243707580  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	bool V_6 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		UnityARSessionNativeInterface_t1130867170 * L_0 = UnityARSessionNativeInterface_GetARSessionNativeInterface_m3174488657(NULL /*static, unused*/, /*hidden argument*/NULL);
		ARPoint_t3436811567  L_1 = ___point0;
		int64_t L_2 = ___resultTypes1;
		NullCheck(L_0);
		List_1_t2644634157 * L_3 = UnityARSessionNativeInterface_HitTest_m388588674(L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		List_1_t2644634157 * L_4 = V_0;
		NullCheck(L_4);
		int32_t L_5 = List_1_get_Count_m1598304542(L_4, /*hidden argument*/List_1_get_Count_m1598304542_RuntimeMethod_var);
		if ((((int32_t)L_5) <= ((int32_t)0)))
		{
			goto IL_00e5;
		}
	}
	{
		List_1_t2644634157 * L_6 = V_0;
		NullCheck(L_6);
		Enumerator_t2179363831  L_7 = List_1_GetEnumerator_m2885046859(L_6, /*hidden argument*/List_1_GetEnumerator_m2885046859_RuntimeMethod_var);
		V_2 = L_7;
	}

IL_0020:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00c6;
		}

IL_0025:
		{
			ARHitTestResult_t3275513025  L_8 = Enumerator_get_Current_m2775711191((&V_2), /*hidden argument*/Enumerator_get_Current_m2775711191_RuntimeMethod_var);
			V_1 = L_8;
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
			Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral1415053652, /*hidden argument*/NULL);
			Transform_t3275118058 * L_9 = __this->get_m_HitTransform_2();
			Matrix4x4_t2933234003  L_10 = (&V_1)->get_worldTransform_3();
			Vector3_t2243707580  L_11 = UnityARMatrixOps_GetPosition_m1153858439(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
			NullCheck(L_9);
			Transform_set_position_m2469242620(L_9, L_11, /*hidden argument*/NULL);
			Transform_t3275118058 * L_12 = __this->get_m_HitTransform_2();
			Matrix4x4_t2933234003  L_13 = (&V_1)->get_worldTransform_3();
			Quaternion_t4030073918  L_14 = UnityARMatrixOps_GetRotation_m1002641986(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
			NullCheck(L_12);
			Transform_set_rotation_m3411284563(L_12, L_14, /*hidden argument*/NULL);
			Transform_t3275118058 * L_15 = __this->get_m_HitTransform_2();
			NullCheck(L_15);
			Vector3_t2243707580  L_16 = Transform_get_position_m1104419803(L_15, /*hidden argument*/NULL);
			V_3 = L_16;
			float L_17 = (&V_3)->get_x_1();
			float L_18 = L_17;
			RuntimeObject * L_19 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_18);
			Transform_t3275118058 * L_20 = __this->get_m_HitTransform_2();
			NullCheck(L_20);
			Vector3_t2243707580  L_21 = Transform_get_position_m1104419803(L_20, /*hidden argument*/NULL);
			V_4 = L_21;
			float L_22 = (&V_4)->get_y_2();
			float L_23 = L_22;
			RuntimeObject * L_24 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_23);
			Transform_t3275118058 * L_25 = __this->get_m_HitTransform_2();
			NullCheck(L_25);
			Vector3_t2243707580  L_26 = Transform_get_position_m1104419803(L_25, /*hidden argument*/NULL);
			V_5 = L_26;
			float L_27 = (&V_5)->get_z_3();
			float L_28 = L_27;
			RuntimeObject * L_29 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_28);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_30 = String_Format_m4262916296(NULL /*static, unused*/, _stringLiteral1537444060, L_19, L_24, L_29, /*hidden argument*/NULL);
			Debug_Log_m920475918(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
			V_6 = (bool)1;
			IL2CPP_LEAVE(0xE7, FINALLY_00d7);
		}

IL_00c6:
		{
			bool L_31 = Enumerator_MoveNext_m3661301011((&V_2), /*hidden argument*/Enumerator_MoveNext_m3661301011_RuntimeMethod_var);
			if (L_31)
			{
				goto IL_0025;
			}
		}

IL_00d2:
		{
			IL2CPP_LEAVE(0xE5, FINALLY_00d7);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00d7;
	}

FINALLY_00d7:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3109677227((&V_2), /*hidden argument*/Enumerator_Dispose_m3109677227_RuntimeMethod_var);
		IL2CPP_END_FINALLY(215)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(215)
	{
		IL2CPP_JUMP_TBL(0xE7, IL_00e7)
		IL2CPP_JUMP_TBL(0xE5, IL_00e5)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00e5:
	{
		return (bool)0;
	}

IL_00e7:
	{
		bool L_32 = V_6;
		return L_32;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARHitTestExample::Update()
extern "C"  void UnityARHitTestExample_Update_m1266118659 (UnityARHitTestExample_t146867607 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARHitTestExample_Update_m1266118659_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Touch_t407273883  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	ARPoint_t3436811567  V_2;
	memset(&V_2, 0, sizeof(V_2));
	ARPoint_t3436811567  V_3;
	memset(&V_3, 0, sizeof(V_3));
	ARHitTestResultTypeU5BU5D_t1303085420* V_4 = NULL;
	int64_t V_5 = 0;
	ARHitTestResultTypeU5BU5D_t1303085420* V_6 = NULL;
	int32_t V_7 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		int32_t L_0 = Input_get_touchCount_m2050827666(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_00b4;
		}
	}
	{
		Transform_t3275118058 * L_1 = __this->get_m_HitTransform_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_00b4;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		Touch_t407273883  L_3 = Input_GetTouch_m1463942798(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = Touch_get_phase_m196706494((&V_0), /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_00b4;
		}
	}
	{
		Camera_t189460977 * L_5 = Camera_get_main_m475173995(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t2243707579  L_6 = Touch_get_position_m2079703643((&V_0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2243707579_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_7 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		Vector3_t2243707580  L_8 = Camera_ScreenToViewportPoint_m2666228286(L_5, L_7, /*hidden argument*/NULL);
		V_1 = L_8;
		Initobj (ARPoint_t3436811567_il2cpp_TypeInfo_var, (&V_3));
		float L_9 = (&V_1)->get_x_1();
		(&V_3)->set_x_0((((double)((double)L_9))));
		float L_10 = (&V_1)->get_y_2();
		(&V_3)->set_y_1((((double)((double)L_10))));
		ARPoint_t3436811567  L_11 = V_3;
		V_2 = L_11;
		ARHitTestResultTypeU5BU5D_t1303085420* L_12 = ((ARHitTestResultTypeU5BU5D_t1303085420*)SZArrayNew(ARHitTestResultTypeU5BU5D_t1303085420_il2cpp_TypeInfo_var, (uint32_t)3));
		RuntimeHelpers_InitializeArray_m3920580167(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_12, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t1486305143____U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0_FieldInfo_var), /*hidden argument*/NULL);
		V_4 = L_12;
		ARHitTestResultTypeU5BU5D_t1303085420* L_13 = V_4;
		V_6 = L_13;
		V_7 = 0;
		goto IL_00a9;
	}

IL_008d:
	{
		ARHitTestResultTypeU5BU5D_t1303085420* L_14 = V_6;
		int32_t L_15 = V_7;
		NullCheck(L_14);
		int32_t L_16 = L_15;
		int64_t L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		V_5 = L_17;
		ARPoint_t3436811567  L_18 = V_2;
		int64_t L_19 = V_5;
		bool L_20 = UnityARHitTestExample_HitTestWithResultType_m996939112(__this, L_18, L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_00a3;
		}
	}
	{
		return;
	}

IL_00a3:
	{
		int32_t L_21 = V_7;
		V_7 = ((int32_t)((int32_t)L_21+(int32_t)1));
	}

IL_00a9:
	{
		int32_t L_22 = V_7;
		ARHitTestResultTypeU5BU5D_t1303085420* L_23 = V_6;
		NullCheck(L_23);
		if ((((int32_t)L_22) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_23)->max_length)))))))
		{
			goto IL_008d;
		}
	}

IL_00b4:
	{
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.XR.iOS.UnityARHitTestResult
extern "C" void UnityARHitTestResult_t4129824344_marshal_pinvoke(const UnityARHitTestResult_t4129824344& unmarshaled, UnityARHitTestResult_t4129824344_marshaled_pinvoke& marshaled)
{
	marshaled.___type_0 = unmarshaled.get_type_0();
	marshaled.___distance_1 = unmarshaled.get_distance_1();
	marshaled.___localTransform_2 = unmarshaled.get_localTransform_2();
	marshaled.___worldTransform_3 = unmarshaled.get_worldTransform_3();
	marshaled.___anchor_4 = reinterpret_cast<intptr_t>((unmarshaled.get_anchor_4()).get_m_value_0());
	marshaled.___isValid_5 = static_cast<int32_t>(unmarshaled.get_isValid_5());
}
extern "C" void UnityARHitTestResult_t4129824344_marshal_pinvoke_back(const UnityARHitTestResult_t4129824344_marshaled_pinvoke& marshaled, UnityARHitTestResult_t4129824344& unmarshaled)
{
	int64_t unmarshaled_type_temp_0 = 0;
	unmarshaled_type_temp_0 = marshaled.___type_0;
	unmarshaled.set_type_0(unmarshaled_type_temp_0);
	double unmarshaled_distance_temp_1 = 0.0;
	unmarshaled_distance_temp_1 = marshaled.___distance_1;
	unmarshaled.set_distance_1(unmarshaled_distance_temp_1);
	Matrix4x4_t2933234003  unmarshaled_localTransform_temp_2;
	memset(&unmarshaled_localTransform_temp_2, 0, sizeof(unmarshaled_localTransform_temp_2));
	unmarshaled_localTransform_temp_2 = marshaled.___localTransform_2;
	unmarshaled.set_localTransform_2(unmarshaled_localTransform_temp_2);
	Matrix4x4_t2933234003  unmarshaled_worldTransform_temp_3;
	memset(&unmarshaled_worldTransform_temp_3, 0, sizeof(unmarshaled_worldTransform_temp_3));
	unmarshaled_worldTransform_temp_3 = marshaled.___worldTransform_3;
	unmarshaled.set_worldTransform_3(unmarshaled_worldTransform_temp_3);
	IntPtr_t unmarshaled_anchor_temp_4;
	memset(&unmarshaled_anchor_temp_4, 0, sizeof(unmarshaled_anchor_temp_4));
	IntPtr_t unmarshaled_anchor_temp_4_temp;
	unmarshaled_anchor_temp_4_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)(marshaled.___anchor_4)));
	unmarshaled_anchor_temp_4 = unmarshaled_anchor_temp_4_temp;
	unmarshaled.set_anchor_4(unmarshaled_anchor_temp_4);
	bool unmarshaled_isValid_temp_5 = false;
	unmarshaled_isValid_temp_5 = static_cast<bool>(marshaled.___isValid_5);
	unmarshaled.set_isValid_5(unmarshaled_isValid_temp_5);
}
// Conversion method for clean up from marshalling of: UnityEngine.XR.iOS.UnityARHitTestResult
extern "C" void UnityARHitTestResult_t4129824344_marshal_pinvoke_cleanup(UnityARHitTestResult_t4129824344_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.XR.iOS.UnityARHitTestResult
extern "C" void UnityARHitTestResult_t4129824344_marshal_com(const UnityARHitTestResult_t4129824344& unmarshaled, UnityARHitTestResult_t4129824344_marshaled_com& marshaled)
{
	marshaled.___type_0 = unmarshaled.get_type_0();
	marshaled.___distance_1 = unmarshaled.get_distance_1();
	marshaled.___localTransform_2 = unmarshaled.get_localTransform_2();
	marshaled.___worldTransform_3 = unmarshaled.get_worldTransform_3();
	marshaled.___anchor_4 = reinterpret_cast<intptr_t>((unmarshaled.get_anchor_4()).get_m_value_0());
	marshaled.___isValid_5 = static_cast<int32_t>(unmarshaled.get_isValid_5());
}
extern "C" void UnityARHitTestResult_t4129824344_marshal_com_back(const UnityARHitTestResult_t4129824344_marshaled_com& marshaled, UnityARHitTestResult_t4129824344& unmarshaled)
{
	int64_t unmarshaled_type_temp_0 = 0;
	unmarshaled_type_temp_0 = marshaled.___type_0;
	unmarshaled.set_type_0(unmarshaled_type_temp_0);
	double unmarshaled_distance_temp_1 = 0.0;
	unmarshaled_distance_temp_1 = marshaled.___distance_1;
	unmarshaled.set_distance_1(unmarshaled_distance_temp_1);
	Matrix4x4_t2933234003  unmarshaled_localTransform_temp_2;
	memset(&unmarshaled_localTransform_temp_2, 0, sizeof(unmarshaled_localTransform_temp_2));
	unmarshaled_localTransform_temp_2 = marshaled.___localTransform_2;
	unmarshaled.set_localTransform_2(unmarshaled_localTransform_temp_2);
	Matrix4x4_t2933234003  unmarshaled_worldTransform_temp_3;
	memset(&unmarshaled_worldTransform_temp_3, 0, sizeof(unmarshaled_worldTransform_temp_3));
	unmarshaled_worldTransform_temp_3 = marshaled.___worldTransform_3;
	unmarshaled.set_worldTransform_3(unmarshaled_worldTransform_temp_3);
	IntPtr_t unmarshaled_anchor_temp_4;
	memset(&unmarshaled_anchor_temp_4, 0, sizeof(unmarshaled_anchor_temp_4));
	IntPtr_t unmarshaled_anchor_temp_4_temp;
	unmarshaled_anchor_temp_4_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)(marshaled.___anchor_4)));
	unmarshaled_anchor_temp_4 = unmarshaled_anchor_temp_4_temp;
	unmarshaled.set_anchor_4(unmarshaled_anchor_temp_4);
	bool unmarshaled_isValid_temp_5 = false;
	unmarshaled_isValid_temp_5 = static_cast<bool>(marshaled.___isValid_5);
	unmarshaled.set_isValid_5(unmarshaled_isValid_temp_5);
}
// Conversion method for clean up from marshalling of: UnityEngine.XR.iOS.UnityARHitTestResult
extern "C" void UnityARHitTestResult_t4129824344_marshal_com_cleanup(UnityARHitTestResult_t4129824344_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.XR.iOS.UnityARKitControl::.ctor()
extern "C"  void UnityARKitControl__ctor_m1699713484 (UnityARKitControl_t1698990409 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARKitControl__ctor_m1699713484_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_runOptions_2(((UnityARSessionRunOptionU5BU5D_t3114965901*)SZArrayNew(UnityARSessionRunOptionU5BU5D_t3114965901_il2cpp_TypeInfo_var, (uint32_t)4)));
		__this->set_alignmentOptions_3(((UnityARAlignmentU5BU5D_t218994990*)SZArrayNew(UnityARAlignmentU5BU5D_t218994990_il2cpp_TypeInfo_var, (uint32_t)3)));
		__this->set_planeOptions_4(((UnityARPlaneDetectionU5BU5D_t191549612*)SZArrayNew(UnityARPlaneDetectionU5BU5D_t191549612_il2cpp_TypeInfo_var, (uint32_t)4)));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARKitControl::Start()
extern "C"  void UnityARKitControl_Start_m620031836 (UnityARKitControl_t1698990409 * __this, const RuntimeMethod* method)
{
	{
		UnityARSessionRunOptionU5BU5D_t3114965901* L_0 = __this->get_runOptions_2();
		NullCheck(L_0);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (int32_t)3);
		UnityARSessionRunOptionU5BU5D_t3114965901* L_1 = __this->get_runOptions_2();
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (int32_t)1);
		UnityARSessionRunOptionU5BU5D_t3114965901* L_2 = __this->get_runOptions_2();
		NullCheck(L_2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(2), (int32_t)2);
		UnityARSessionRunOptionU5BU5D_t3114965901* L_3 = __this->get_runOptions_2();
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(3), (int32_t)0);
		UnityARAlignmentU5BU5D_t218994990* L_4 = __this->get_alignmentOptions_3();
		NullCheck(L_4);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (int32_t)2);
		UnityARAlignmentU5BU5D_t218994990* L_5 = __this->get_alignmentOptions_3();
		NullCheck(L_5);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(1), (int32_t)0);
		UnityARAlignmentU5BU5D_t218994990* L_6 = __this->get_alignmentOptions_3();
		NullCheck(L_6);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (int32_t)1);
		UnityARPlaneDetectionU5BU5D_t191549612* L_7 = __this->get_planeOptions_4();
		NullCheck(L_7);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (int32_t)1);
		UnityARPlaneDetectionU5BU5D_t191549612* L_8 = __this->get_planeOptions_4();
		NullCheck(L_8);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (int32_t)0);
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARKitControl::Update()
extern "C"  void UnityARKitControl_Update_m2500411141 (UnityARKitControl_t1698990409 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARKitControl::OnGUI()
extern "C"  void UnityARKitControl_OnGUI_m4075060656 (UnityARKitControl_t1698990409 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARKitControl_OnGUI_m4075060656_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ARKitWorldTackingSessionConfiguration_t1821734930  V_0;
	memset(&V_0, 0, sizeof(V_0));
	ARKitSessionConfiguration_t318899795  V_1;
	memset(&V_1, 0, sizeof(V_1));
	String_t* V_2 = NULL;
	String_t* V_3 = NULL;
	String_t* V_4 = NULL;
	String_t* G_B13_0 = NULL;
	String_t* G_B20_0 = NULL;
	String_t* G_B25_0 = NULL;
	{
		Rect_t3681755626  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Rect__ctor_m1220545469((&L_0), (100.0f), (100.0f), (200.0f), (50.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_1 = GUI_Button_m3054448581(NULL /*static, unused*/, L_0, _stringLiteral3833953726, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0032;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		UnityARSessionNativeInterface_t1130867170 * L_2 = UnityARSessionNativeInterface_GetARSessionNativeInterface_m3174488657(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		UnityARSessionNativeInterface_Pause_m2220930613(L_2, /*hidden argument*/NULL);
	}

IL_0032:
	{
		Rect_t3681755626  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Rect__ctor_m1220545469((&L_3), (300.0f), (100.0f), (200.0f), (50.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_4 = GUI_Button_m3054448581(NULL /*static, unused*/, L_3, _stringLiteral2858758002, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0095;
		}
	}
	{
		UnityARAlignmentU5BU5D_t218994990* L_5 = __this->get_alignmentOptions_3();
		int32_t L_6 = __this->get_currentAlignmentIndex_6();
		NullCheck(L_5);
		int32_t L_7 = L_6;
		int32_t L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		UnityARPlaneDetectionU5BU5D_t191549612* L_9 = __this->get_planeOptions_4();
		int32_t L_10 = __this->get_currentPlaneIndex_7();
		NullCheck(L_9);
		int32_t L_11 = L_10;
		int32_t L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		ARKitWorldTackingSessionConfiguration__ctor_m4281543687((&V_0), L_8, L_12, (bool)0, (bool)0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		UnityARSessionNativeInterface_t1130867170 * L_13 = UnityARSessionNativeInterface_GetARSessionNativeInterface_m3174488657(NULL /*static, unused*/, /*hidden argument*/NULL);
		ARKitWorldTackingSessionConfiguration_t1821734930  L_14 = V_0;
		UnityARSessionRunOptionU5BU5D_t3114965901* L_15 = __this->get_runOptions_2();
		int32_t L_16 = __this->get_currentOptionIndex_5();
		NullCheck(L_15);
		int32_t L_17 = L_16;
		int32_t L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		NullCheck(L_13);
		UnityARSessionNativeInterface_RunWithConfigAndOptions_m375276821(L_13, L_14, L_18, /*hidden argument*/NULL);
	}

IL_0095:
	{
		Rect_t3681755626  L_19;
		memset(&L_19, 0, sizeof(L_19));
		Rect__ctor_m1220545469((&L_19), (100.0f), (300.0f), (200.0f), (100.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_20 = GUI_Button_m3054448581(NULL /*static, unused*/, L_19, _stringLiteral2341529191, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_00de;
		}
	}
	{
		UnityARAlignmentU5BU5D_t218994990* L_21 = __this->get_alignmentOptions_3();
		int32_t L_22 = __this->get_currentAlignmentIndex_6();
		NullCheck(L_21);
		int32_t L_23 = L_22;
		int32_t L_24 = (L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_23));
		ARKitSessionConfiguration__ctor_m3221520625((&V_1), L_24, (bool)1, (bool)1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		UnityARSessionNativeInterface_t1130867170 * L_25 = UnityARSessionNativeInterface_GetARSessionNativeInterface_m3174488657(NULL /*static, unused*/, /*hidden argument*/NULL);
		ARKitSessionConfiguration_t318899795  L_26 = V_1;
		NullCheck(L_25);
		UnityARSessionNativeInterface_RunWithConfig_m2478060541(L_25, L_26, /*hidden argument*/NULL);
	}

IL_00de:
	{
		int32_t L_27 = __this->get_currentOptionIndex_5();
		if (L_27)
		{
			goto IL_00f3;
		}
	}
	{
		G_B13_0 = _stringLiteral1743625051;
		goto IL_0124;
	}

IL_00f3:
	{
		int32_t L_28 = __this->get_currentOptionIndex_5();
		if ((!(((uint32_t)L_28) == ((uint32_t)1))))
		{
			goto IL_0109;
		}
	}
	{
		G_B13_0 = _stringLiteral1174079095;
		goto IL_0124;
	}

IL_0109:
	{
		int32_t L_29 = __this->get_currentOptionIndex_5();
		if ((!(((uint32_t)L_29) == ((uint32_t)2))))
		{
			goto IL_011f;
		}
	}
	{
		G_B13_0 = _stringLiteral2724865136;
		goto IL_0124;
	}

IL_011f:
	{
		G_B13_0 = _stringLiteral1414246128;
	}

IL_0124:
	{
		V_2 = G_B13_0;
		Rect_t3681755626  L_30;
		memset(&L_30, 0, sizeof(L_30));
		Rect__ctor_m1220545469((&L_30), (100.0f), (200.0f), (150.0f), (50.0f), /*hidden argument*/NULL);
		String_t* L_31 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral1301536196, L_31, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_33 = GUI_Button_m3054448581(NULL /*static, unused*/, L_30, L_32, /*hidden argument*/NULL);
		if (!L_33)
		{
			goto IL_0163;
		}
	}
	{
		int32_t L_34 = __this->get_currentOptionIndex_5();
		__this->set_currentOptionIndex_5(((int32_t)((int32_t)((int32_t)((int32_t)L_34+(int32_t)1))%(int32_t)4)));
	}

IL_0163:
	{
		int32_t L_35 = __this->get_currentAlignmentIndex_6();
		if (L_35)
		{
			goto IL_0178;
		}
	}
	{
		G_B20_0 = _stringLiteral3675648761;
		goto IL_0193;
	}

IL_0178:
	{
		int32_t L_36 = __this->get_currentAlignmentIndex_6();
		if ((!(((uint32_t)L_36) == ((uint32_t)1))))
		{
			goto IL_018e;
		}
	}
	{
		G_B20_0 = _stringLiteral3025722940;
		goto IL_0193;
	}

IL_018e:
	{
		G_B20_0 = _stringLiteral3575497429;
	}

IL_0193:
	{
		V_3 = G_B20_0;
		Rect_t3681755626  L_37;
		memset(&L_37, 0, sizeof(L_37));
		Rect__ctor_m1220545469((&L_37), (300.0f), (200.0f), (150.0f), (50.0f), /*hidden argument*/NULL);
		String_t* L_38 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_39 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral4059134932, L_38, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_40 = GUI_Button_m3054448581(NULL /*static, unused*/, L_37, L_39, /*hidden argument*/NULL);
		if (!L_40)
		{
			goto IL_01d2;
		}
	}
	{
		int32_t L_41 = __this->get_currentAlignmentIndex_6();
		__this->set_currentAlignmentIndex_6(((int32_t)((int32_t)((int32_t)((int32_t)L_41+(int32_t)1))%(int32_t)3)));
	}

IL_01d2:
	{
		int32_t L_42 = __this->get_currentPlaneIndex_7();
		if (L_42)
		{
			goto IL_01e7;
		}
	}
	{
		G_B25_0 = _stringLiteral855845486;
		goto IL_01ec;
	}

IL_01e7:
	{
		G_B25_0 = _stringLiteral1414246128;
	}

IL_01ec:
	{
		V_4 = G_B25_0;
		Rect_t3681755626  L_43;
		memset(&L_43, 0, sizeof(L_43));
		Rect__ctor_m1220545469((&L_43), (500.0f), (200.0f), (150.0f), (50.0f), /*hidden argument*/NULL);
		String_t* L_44 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_45 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral3168505747, L_44, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_46 = GUI_Button_m3054448581(NULL /*static, unused*/, L_43, L_45, /*hidden argument*/NULL);
		if (!L_46)
		{
			goto IL_022d;
		}
	}
	{
		int32_t L_47 = __this->get_currentPlaneIndex_7();
		__this->set_currentPlaneIndex_7(((int32_t)((int32_t)((int32_t)((int32_t)L_47+(int32_t)1))%(int32_t)2)));
	}

IL_022d:
	{
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARMatrixOps::.ctor()
extern "C"  void UnityARMatrixOps__ctor_m4029735674 (UnityARMatrixOps_t4039665643 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.XR.iOS.UnityARMatrixOps::GetPosition(UnityEngine.Matrix4x4)
extern "C"  Vector3_t2243707580  UnityARMatrixOps_GetPosition_m1153858439 (RuntimeObject * __this /* static, unused */, Matrix4x4_t2933234003  ___matrix0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARMatrixOps_GetPosition_m1153858439_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector4_t2243707581  L_0 = Matrix4x4_GetColumn_m1367096730((&___matrix0), 3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector4_t2243707581_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_1 = Vector4_op_Implicit_m1902992875(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = (&V_0)->get_z_3();
		(&V_0)->set_z_3(((-L_2)));
		Vector3_t2243707580  L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Quaternion UnityEngine.XR.iOS.UnityARMatrixOps::GetRotation(UnityEngine.Matrix4x4)
extern "C"  Quaternion_t4030073918  UnityARMatrixOps_GetRotation_m1002641986 (RuntimeObject * __this /* static, unused */, Matrix4x4_t2933234003  ___matrix0, const RuntimeMethod* method)
{
	Quaternion_t4030073918  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Matrix4x4_t2933234003  L_0 = ___matrix0;
		Quaternion_t4030073918  L_1 = UnityARMatrixOps_QuaternionFromMatrix_m1686577287(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = (&V_0)->get_z_2();
		(&V_0)->set_z_2(((-L_2)));
		float L_3 = (&V_0)->get_w_3();
		(&V_0)->set_w_3(((-L_3)));
		Quaternion_t4030073918  L_4 = V_0;
		return L_4;
	}
}
// UnityEngine.Quaternion UnityEngine.XR.iOS.UnityARMatrixOps::QuaternionFromMatrix(UnityEngine.Matrix4x4)
extern "C"  Quaternion_t4030073918  UnityARMatrixOps_QuaternionFromMatrix_m1686577287 (RuntimeObject * __this /* static, unused */, Matrix4x4_t2933234003  ___m0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARMatrixOps_QuaternionFromMatrix_m1686577287_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Quaternion_t4030073918  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (Quaternion_t4030073918_il2cpp_TypeInfo_var, (&V_0));
		float L_0 = Matrix4x4_get_Item_m312280350((&___m0), 0, 0, /*hidden argument*/NULL);
		float L_1 = Matrix4x4_get_Item_m312280350((&___m0), 1, 1, /*hidden argument*/NULL);
		float L_2 = Matrix4x4_get_Item_m312280350((&___m0), 2, 2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_3 = Mathf_Max_m2564622569(NULL /*static, unused*/, (0.0f), ((float)((float)((float)((float)((float)((float)(1.0f)+(float)L_0))+(float)L_1))+(float)L_2)), /*hidden argument*/NULL);
		float L_4 = sqrtf(L_3);
		(&V_0)->set_w_3(((float)((float)L_4/(float)(2.0f))));
		float L_5 = Matrix4x4_get_Item_m312280350((&___m0), 0, 0, /*hidden argument*/NULL);
		float L_6 = Matrix4x4_get_Item_m312280350((&___m0), 1, 1, /*hidden argument*/NULL);
		float L_7 = Matrix4x4_get_Item_m312280350((&___m0), 2, 2, /*hidden argument*/NULL);
		float L_8 = Mathf_Max_m2564622569(NULL /*static, unused*/, (0.0f), ((float)((float)((float)((float)((float)((float)(1.0f)+(float)L_5))-(float)L_6))-(float)L_7)), /*hidden argument*/NULL);
		float L_9 = sqrtf(L_8);
		(&V_0)->set_x_0(((float)((float)L_9/(float)(2.0f))));
		float L_10 = Matrix4x4_get_Item_m312280350((&___m0), 0, 0, /*hidden argument*/NULL);
		float L_11 = Matrix4x4_get_Item_m312280350((&___m0), 1, 1, /*hidden argument*/NULL);
		float L_12 = Matrix4x4_get_Item_m312280350((&___m0), 2, 2, /*hidden argument*/NULL);
		float L_13 = Mathf_Max_m2564622569(NULL /*static, unused*/, (0.0f), ((float)((float)((float)((float)((float)((float)(1.0f)-(float)L_10))+(float)L_11))-(float)L_12)), /*hidden argument*/NULL);
		float L_14 = sqrtf(L_13);
		(&V_0)->set_y_1(((float)((float)L_14/(float)(2.0f))));
		float L_15 = Matrix4x4_get_Item_m312280350((&___m0), 0, 0, /*hidden argument*/NULL);
		float L_16 = Matrix4x4_get_Item_m312280350((&___m0), 1, 1, /*hidden argument*/NULL);
		float L_17 = Matrix4x4_get_Item_m312280350((&___m0), 2, 2, /*hidden argument*/NULL);
		float L_18 = Mathf_Max_m2564622569(NULL /*static, unused*/, (0.0f), ((float)((float)((float)((float)((float)((float)(1.0f)-(float)L_15))-(float)L_16))+(float)L_17)), /*hidden argument*/NULL);
		float L_19 = sqrtf(L_18);
		(&V_0)->set_z_2(((float)((float)L_19/(float)(2.0f))));
		Quaternion_t4030073918 * L_20 = (&V_0);
		float L_21 = L_20->get_x_0();
		float L_22 = (&V_0)->get_x_0();
		float L_23 = Matrix4x4_get_Item_m312280350((&___m0), 2, 1, /*hidden argument*/NULL);
		float L_24 = Matrix4x4_get_Item_m312280350((&___m0), 1, 2, /*hidden argument*/NULL);
		float L_25 = Mathf_Sign_m2039143327(NULL /*static, unused*/, ((float)((float)L_22*(float)((float)((float)L_23-(float)L_24)))), /*hidden argument*/NULL);
		L_20->set_x_0(((float)((float)L_21*(float)L_25)));
		Quaternion_t4030073918 * L_26 = (&V_0);
		float L_27 = L_26->get_y_1();
		float L_28 = (&V_0)->get_y_1();
		float L_29 = Matrix4x4_get_Item_m312280350((&___m0), 0, 2, /*hidden argument*/NULL);
		float L_30 = Matrix4x4_get_Item_m312280350((&___m0), 2, 0, /*hidden argument*/NULL);
		float L_31 = Mathf_Sign_m2039143327(NULL /*static, unused*/, ((float)((float)L_28*(float)((float)((float)L_29-(float)L_30)))), /*hidden argument*/NULL);
		L_26->set_y_1(((float)((float)L_27*(float)L_31)));
		Quaternion_t4030073918 * L_32 = (&V_0);
		float L_33 = L_32->get_z_2();
		float L_34 = (&V_0)->get_z_2();
		float L_35 = Matrix4x4_get_Item_m312280350((&___m0), 1, 0, /*hidden argument*/NULL);
		float L_36 = Matrix4x4_get_Item_m312280350((&___m0), 0, 1, /*hidden argument*/NULL);
		float L_37 = Mathf_Sign_m2039143327(NULL /*static, unused*/, ((float)((float)L_34*(float)((float)((float)L_35-(float)L_36)))), /*hidden argument*/NULL);
		L_32->set_z_2(((float)((float)L_33*(float)L_37)));
		Quaternion_t4030073918  L_38 = V_0;
		return L_38;
	}
}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_UnityARSessionNativeInterface__frame_update_m127370(internal_UnityARCamera_t2580192745  ___camera0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Managed method invocation
	UnityARSessionNativeInterface__frame_update_m127370(NULL, ___camera0, NULL);

}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_UnityARSessionNativeInterface__anchor_added_m3584567327(UnityARAnchorData_t2901866349  ___anchor0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Managed method invocation
	UnityARSessionNativeInterface__anchor_added_m3584567327(NULL, ___anchor0, NULL);

}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_UnityARSessionNativeInterface__anchor_updated_m1970308864(UnityARAnchorData_t2901866349  ___anchor0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Managed method invocation
	UnityARSessionNativeInterface__anchor_updated_m1970308864(NULL, ___anchor0, NULL);

}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_UnityARSessionNativeInterface__anchor_removed_m4240378233(UnityARAnchorData_t2901866349  ___anchor0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Managed method invocation
	UnityARSessionNativeInterface__anchor_removed_m4240378233(NULL, ___anchor0, NULL);

}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_UnityARSessionNativeInterface__ar_session_failed_m2515310284(char* ___error0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___error0' to managed representation
	String_t* ____error0_unmarshaled = NULL;
	____error0_unmarshaled = il2cpp_codegen_marshal_string_result(___error0);

	// Managed method invocation
	UnityARSessionNativeInterface__ar_session_failed_m2515310284(NULL, ____error0_unmarshaled, NULL);

}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::.ctor()
extern "C"  void UnityARSessionNativeInterface__ctor_m2294513111 (UnityARSessionNativeInterface_t1130867170 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARSessionNativeInterface__ctor_m2294513111_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UnityARSessionNativeInterface_t1130867170 * G_B2_0 = NULL;
	UnityARSessionNativeInterface_t1130867170 * G_B1_0 = NULL;
	internal_ARFrameUpdate_t3296518558 * G_B4_0 = NULL;
	UnityARSessionNativeInterface_t1130867170 * G_B4_1 = NULL;
	internal_ARFrameUpdate_t3296518558 * G_B3_0 = NULL;
	UnityARSessionNativeInterface_t1130867170 * G_B3_1 = NULL;
	internal_ARAnchorAdded_t1622117597 * G_B6_0 = NULL;
	internal_ARFrameUpdate_t3296518558 * G_B6_1 = NULL;
	UnityARSessionNativeInterface_t1130867170 * G_B6_2 = NULL;
	internal_ARAnchorAdded_t1622117597 * G_B5_0 = NULL;
	internal_ARFrameUpdate_t3296518558 * G_B5_1 = NULL;
	UnityARSessionNativeInterface_t1130867170 * G_B5_2 = NULL;
	internal_ARAnchorUpdated_t3705772742 * G_B8_0 = NULL;
	internal_ARAnchorAdded_t1622117597 * G_B8_1 = NULL;
	internal_ARFrameUpdate_t3296518558 * G_B8_2 = NULL;
	UnityARSessionNativeInterface_t1130867170 * G_B8_3 = NULL;
	internal_ARAnchorUpdated_t3705772742 * G_B7_0 = NULL;
	internal_ARAnchorAdded_t1622117597 * G_B7_1 = NULL;
	internal_ARFrameUpdate_t3296518558 * G_B7_2 = NULL;
	UnityARSessionNativeInterface_t1130867170 * G_B7_3 = NULL;
	internal_ARAnchorRemoved_t3189755211 * G_B10_0 = NULL;
	internal_ARAnchorUpdated_t3705772742 * G_B10_1 = NULL;
	internal_ARAnchorAdded_t1622117597 * G_B10_2 = NULL;
	internal_ARFrameUpdate_t3296518558 * G_B10_3 = NULL;
	UnityARSessionNativeInterface_t1130867170 * G_B10_4 = NULL;
	internal_ARAnchorRemoved_t3189755211 * G_B9_0 = NULL;
	internal_ARAnchorUpdated_t3705772742 * G_B9_1 = NULL;
	internal_ARAnchorAdded_t1622117597 * G_B9_2 = NULL;
	internal_ARFrameUpdate_t3296518558 * G_B9_3 = NULL;
	UnityARSessionNativeInterface_t1130867170 * G_B9_4 = NULL;
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		internal_ARFrameUpdate_t3296518558 * L_0 = ((UnityARSessionNativeInterface_t1130867170_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache0_8();
		G_B1_0 = __this;
		if (L_0)
		{
			G_B2_0 = __this;
			goto IL_001f;
		}
	}
	{
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)UnityARSessionNativeInterface__frame_update_m127370_RuntimeMethod_var);
		internal_ARFrameUpdate_t3296518558 * L_2 = (internal_ARFrameUpdate_t3296518558 *)il2cpp_codegen_object_new(internal_ARFrameUpdate_t3296518558_il2cpp_TypeInfo_var);
		internal_ARFrameUpdate__ctor_m36448827(L_2, NULL, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		((UnityARSessionNativeInterface_t1130867170_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var))->set_U3CU3Ef__mgU24cache0_8(L_2);
		G_B2_0 = G_B1_0;
	}

IL_001f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		internal_ARFrameUpdate_t3296518558 * L_3 = ((UnityARSessionNativeInterface_t1130867170_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache0_8();
		internal_ARAnchorAdded_t1622117597 * L_4 = ((UnityARSessionNativeInterface_t1130867170_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache1_9();
		G_B3_0 = L_3;
		G_B3_1 = G_B2_0;
		if (L_4)
		{
			G_B4_0 = L_3;
			G_B4_1 = G_B2_0;
			goto IL_003c;
		}
	}
	{
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)UnityARSessionNativeInterface__anchor_added_m3584567327_RuntimeMethod_var);
		internal_ARAnchorAdded_t1622117597 * L_6 = (internal_ARAnchorAdded_t1622117597 *)il2cpp_codegen_object_new(internal_ARAnchorAdded_t1622117597_il2cpp_TypeInfo_var);
		internal_ARAnchorAdded__ctor_m875644652(L_6, NULL, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		((UnityARSessionNativeInterface_t1130867170_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var))->set_U3CU3Ef__mgU24cache1_9(L_6);
		G_B4_0 = G_B3_0;
		G_B4_1 = G_B3_1;
	}

IL_003c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		internal_ARAnchorAdded_t1622117597 * L_7 = ((UnityARSessionNativeInterface_t1130867170_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache1_9();
		internal_ARAnchorUpdated_t3705772742 * L_8 = ((UnityARSessionNativeInterface_t1130867170_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache2_10();
		G_B5_0 = L_7;
		G_B5_1 = G_B4_0;
		G_B5_2 = G_B4_1;
		if (L_8)
		{
			G_B6_0 = L_7;
			G_B6_1 = G_B4_0;
			G_B6_2 = G_B4_1;
			goto IL_0059;
		}
	}
	{
		IntPtr_t L_9;
		L_9.set_m_value_0((void*)(void*)UnityARSessionNativeInterface__anchor_updated_m1970308864_RuntimeMethod_var);
		internal_ARAnchorUpdated_t3705772742 * L_10 = (internal_ARAnchorUpdated_t3705772742 *)il2cpp_codegen_object_new(internal_ARAnchorUpdated_t3705772742_il2cpp_TypeInfo_var);
		internal_ARAnchorUpdated__ctor_m2904213987(L_10, NULL, L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		((UnityARSessionNativeInterface_t1130867170_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var))->set_U3CU3Ef__mgU24cache2_10(L_10);
		G_B6_0 = G_B5_0;
		G_B6_1 = G_B5_1;
		G_B6_2 = G_B5_2;
	}

IL_0059:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		internal_ARAnchorUpdated_t3705772742 * L_11 = ((UnityARSessionNativeInterface_t1130867170_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache2_10();
		internal_ARAnchorRemoved_t3189755211 * L_12 = ((UnityARSessionNativeInterface_t1130867170_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache3_11();
		G_B7_0 = L_11;
		G_B7_1 = G_B6_0;
		G_B7_2 = G_B6_1;
		G_B7_3 = G_B6_2;
		if (L_12)
		{
			G_B8_0 = L_11;
			G_B8_1 = G_B6_0;
			G_B8_2 = G_B6_1;
			G_B8_3 = G_B6_2;
			goto IL_0076;
		}
	}
	{
		IntPtr_t L_13;
		L_13.set_m_value_0((void*)(void*)UnityARSessionNativeInterface__anchor_removed_m4240378233_RuntimeMethod_var);
		internal_ARAnchorRemoved_t3189755211 * L_14 = (internal_ARAnchorRemoved_t3189755211 *)il2cpp_codegen_object_new(internal_ARAnchorRemoved_t3189755211_il2cpp_TypeInfo_var);
		internal_ARAnchorRemoved__ctor_m2856503254(L_14, NULL, L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		((UnityARSessionNativeInterface_t1130867170_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var))->set_U3CU3Ef__mgU24cache3_11(L_14);
		G_B8_0 = G_B7_0;
		G_B8_1 = G_B7_1;
		G_B8_2 = G_B7_2;
		G_B8_3 = G_B7_3;
	}

IL_0076:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		internal_ARAnchorRemoved_t3189755211 * L_15 = ((UnityARSessionNativeInterface_t1130867170_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache3_11();
		ARSessionFailed_t872580813 * L_16 = ((UnityARSessionNativeInterface_t1130867170_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache4_12();
		G_B9_0 = L_15;
		G_B9_1 = G_B8_0;
		G_B9_2 = G_B8_1;
		G_B9_3 = G_B8_2;
		G_B9_4 = G_B8_3;
		if (L_16)
		{
			G_B10_0 = L_15;
			G_B10_1 = G_B8_0;
			G_B10_2 = G_B8_1;
			G_B10_3 = G_B8_2;
			G_B10_4 = G_B8_3;
			goto IL_0093;
		}
	}
	{
		IntPtr_t L_17;
		L_17.set_m_value_0((void*)(void*)UnityARSessionNativeInterface__ar_session_failed_m2515310284_RuntimeMethod_var);
		ARSessionFailed_t872580813 * L_18 = (ARSessionFailed_t872580813 *)il2cpp_codegen_object_new(ARSessionFailed_t872580813_il2cpp_TypeInfo_var);
		ARSessionFailed__ctor_m3600321544(L_18, NULL, L_17, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		((UnityARSessionNativeInterface_t1130867170_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var))->set_U3CU3Ef__mgU24cache4_12(L_18);
		G_B10_0 = G_B9_0;
		G_B10_1 = G_B9_1;
		G_B10_2 = G_B9_2;
		G_B10_3 = G_B9_3;
		G_B10_4 = G_B9_4;
	}

IL_0093:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		ARSessionFailed_t872580813 * L_19 = ((UnityARSessionNativeInterface_t1130867170_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache4_12();
		IntPtr_t L_20 = UnityARSessionNativeInterface_unity_CreateNativeARSession_m1019965107(NULL /*static, unused*/, G_B10_3, G_B10_2, G_B10_1, G_B10_0, L_19, /*hidden argument*/NULL);
		NullCheck(G_B10_4);
		G_B10_4->set_m_NativeARSession_5(L_20);
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::add_ARFrameUpdatedEvent(UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARFrameUpdate)
extern "C"  void UnityARSessionNativeInterface_add_ARFrameUpdatedEvent_m2850773202 (RuntimeObject * __this /* static, unused */, ARFrameUpdate_t496507918 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARSessionNativeInterface_add_ARFrameUpdatedEvent_m2850773202_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ARFrameUpdate_t496507918 * V_0 = NULL;
	ARFrameUpdate_t496507918 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		ARFrameUpdate_t496507918 * L_0 = ((UnityARSessionNativeInterface_t1130867170_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var))->get_ARFrameUpdatedEvent_0();
		V_0 = L_0;
	}

IL_0006:
	{
		ARFrameUpdate_t496507918 * L_1 = V_0;
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		ARFrameUpdate_t496507918 * L_2 = V_1;
		ARFrameUpdate_t496507918 * L_3 = ___value0;
		Delegate_t3022476291 * L_4 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		ARFrameUpdate_t496507918 * L_5 = V_0;
		ARFrameUpdate_t496507918 * L_6 = InterlockedCompareExchangeImpl<ARFrameUpdate_t496507918 *>((((UnityARSessionNativeInterface_t1130867170_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var))->get_address_of_ARFrameUpdatedEvent_0()), ((ARFrameUpdate_t496507918 *)CastclassSealed((RuntimeObject*)L_4, ARFrameUpdate_t496507918_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		ARFrameUpdate_t496507918 * L_7 = V_0;
		ARFrameUpdate_t496507918 * L_8 = V_1;
		if ((!(((RuntimeObject*)(ARFrameUpdate_t496507918 *)L_7) == ((RuntimeObject*)(ARFrameUpdate_t496507918 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::remove_ARFrameUpdatedEvent(UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARFrameUpdate)
extern "C"  void UnityARSessionNativeInterface_remove_ARFrameUpdatedEvent_m415315455 (RuntimeObject * __this /* static, unused */, ARFrameUpdate_t496507918 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARSessionNativeInterface_remove_ARFrameUpdatedEvent_m415315455_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ARFrameUpdate_t496507918 * V_0 = NULL;
	ARFrameUpdate_t496507918 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		ARFrameUpdate_t496507918 * L_0 = ((UnityARSessionNativeInterface_t1130867170_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var))->get_ARFrameUpdatedEvent_0();
		V_0 = L_0;
	}

IL_0006:
	{
		ARFrameUpdate_t496507918 * L_1 = V_0;
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		ARFrameUpdate_t496507918 * L_2 = V_1;
		ARFrameUpdate_t496507918 * L_3 = ___value0;
		Delegate_t3022476291 * L_4 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		ARFrameUpdate_t496507918 * L_5 = V_0;
		ARFrameUpdate_t496507918 * L_6 = InterlockedCompareExchangeImpl<ARFrameUpdate_t496507918 *>((((UnityARSessionNativeInterface_t1130867170_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var))->get_address_of_ARFrameUpdatedEvent_0()), ((ARFrameUpdate_t496507918 *)CastclassSealed((RuntimeObject*)L_4, ARFrameUpdate_t496507918_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		ARFrameUpdate_t496507918 * L_7 = V_0;
		ARFrameUpdate_t496507918 * L_8 = V_1;
		if ((!(((RuntimeObject*)(ARFrameUpdate_t496507918 *)L_7) == ((RuntimeObject*)(ARFrameUpdate_t496507918 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::add_ARAnchorAddedEvent(UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorAdded)
extern "C"  void UnityARSessionNativeInterface_add_ARAnchorAddedEvent_m658032036 (RuntimeObject * __this /* static, unused */, ARAnchorAdded_t2646854145 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARSessionNativeInterface_add_ARAnchorAddedEvent_m658032036_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ARAnchorAdded_t2646854145 * V_0 = NULL;
	ARAnchorAdded_t2646854145 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		ARAnchorAdded_t2646854145 * L_0 = ((UnityARSessionNativeInterface_t1130867170_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var))->get_ARAnchorAddedEvent_1();
		V_0 = L_0;
	}

IL_0006:
	{
		ARAnchorAdded_t2646854145 * L_1 = V_0;
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		ARAnchorAdded_t2646854145 * L_2 = V_1;
		ARAnchorAdded_t2646854145 * L_3 = ___value0;
		Delegate_t3022476291 * L_4 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		ARAnchorAdded_t2646854145 * L_5 = V_0;
		ARAnchorAdded_t2646854145 * L_6 = InterlockedCompareExchangeImpl<ARAnchorAdded_t2646854145 *>((((UnityARSessionNativeInterface_t1130867170_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var))->get_address_of_ARAnchorAddedEvent_1()), ((ARAnchorAdded_t2646854145 *)CastclassSealed((RuntimeObject*)L_4, ARAnchorAdded_t2646854145_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		ARAnchorAdded_t2646854145 * L_7 = V_0;
		ARAnchorAdded_t2646854145 * L_8 = V_1;
		if ((!(((RuntimeObject*)(ARAnchorAdded_t2646854145 *)L_7) == ((RuntimeObject*)(ARAnchorAdded_t2646854145 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::remove_ARAnchorAddedEvent(UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorAdded)
extern "C"  void UnityARSessionNativeInterface_remove_ARAnchorAddedEvent_m1622386471 (RuntimeObject * __this /* static, unused */, ARAnchorAdded_t2646854145 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARSessionNativeInterface_remove_ARAnchorAddedEvent_m1622386471_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ARAnchorAdded_t2646854145 * V_0 = NULL;
	ARAnchorAdded_t2646854145 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		ARAnchorAdded_t2646854145 * L_0 = ((UnityARSessionNativeInterface_t1130867170_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var))->get_ARAnchorAddedEvent_1();
		V_0 = L_0;
	}

IL_0006:
	{
		ARAnchorAdded_t2646854145 * L_1 = V_0;
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		ARAnchorAdded_t2646854145 * L_2 = V_1;
		ARAnchorAdded_t2646854145 * L_3 = ___value0;
		Delegate_t3022476291 * L_4 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		ARAnchorAdded_t2646854145 * L_5 = V_0;
		ARAnchorAdded_t2646854145 * L_6 = InterlockedCompareExchangeImpl<ARAnchorAdded_t2646854145 *>((((UnityARSessionNativeInterface_t1130867170_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var))->get_address_of_ARAnchorAddedEvent_1()), ((ARAnchorAdded_t2646854145 *)CastclassSealed((RuntimeObject*)L_4, ARAnchorAdded_t2646854145_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		ARAnchorAdded_t2646854145 * L_7 = V_0;
		ARAnchorAdded_t2646854145 * L_8 = V_1;
		if ((!(((RuntimeObject*)(ARAnchorAdded_t2646854145 *)L_7) == ((RuntimeObject*)(ARAnchorAdded_t2646854145 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::add_ARAnchorUpdatedEvent(UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorUpdated)
extern "C"  void UnityARSessionNativeInterface_add_ARAnchorUpdatedEvent_m1485763364 (RuntimeObject * __this /* static, unused */, ARAnchorUpdated_t3886071158 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARSessionNativeInterface_add_ARAnchorUpdatedEvent_m1485763364_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ARAnchorUpdated_t3886071158 * V_0 = NULL;
	ARAnchorUpdated_t3886071158 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		ARAnchorUpdated_t3886071158 * L_0 = ((UnityARSessionNativeInterface_t1130867170_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var))->get_ARAnchorUpdatedEvent_2();
		V_0 = L_0;
	}

IL_0006:
	{
		ARAnchorUpdated_t3886071158 * L_1 = V_0;
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		ARAnchorUpdated_t3886071158 * L_2 = V_1;
		ARAnchorUpdated_t3886071158 * L_3 = ___value0;
		Delegate_t3022476291 * L_4 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		ARAnchorUpdated_t3886071158 * L_5 = V_0;
		ARAnchorUpdated_t3886071158 * L_6 = InterlockedCompareExchangeImpl<ARAnchorUpdated_t3886071158 *>((((UnityARSessionNativeInterface_t1130867170_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var))->get_address_of_ARAnchorUpdatedEvent_2()), ((ARAnchorUpdated_t3886071158 *)CastclassSealed((RuntimeObject*)L_4, ARAnchorUpdated_t3886071158_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		ARAnchorUpdated_t3886071158 * L_7 = V_0;
		ARAnchorUpdated_t3886071158 * L_8 = V_1;
		if ((!(((RuntimeObject*)(ARAnchorUpdated_t3886071158 *)L_7) == ((RuntimeObject*)(ARAnchorUpdated_t3886071158 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::remove_ARAnchorUpdatedEvent(UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorUpdated)
extern "C"  void UnityARSessionNativeInterface_remove_ARAnchorUpdatedEvent_m2137207143 (RuntimeObject * __this /* static, unused */, ARAnchorUpdated_t3886071158 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARSessionNativeInterface_remove_ARAnchorUpdatedEvent_m2137207143_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ARAnchorUpdated_t3886071158 * V_0 = NULL;
	ARAnchorUpdated_t3886071158 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		ARAnchorUpdated_t3886071158 * L_0 = ((UnityARSessionNativeInterface_t1130867170_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var))->get_ARAnchorUpdatedEvent_2();
		V_0 = L_0;
	}

IL_0006:
	{
		ARAnchorUpdated_t3886071158 * L_1 = V_0;
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		ARAnchorUpdated_t3886071158 * L_2 = V_1;
		ARAnchorUpdated_t3886071158 * L_3 = ___value0;
		Delegate_t3022476291 * L_4 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		ARAnchorUpdated_t3886071158 * L_5 = V_0;
		ARAnchorUpdated_t3886071158 * L_6 = InterlockedCompareExchangeImpl<ARAnchorUpdated_t3886071158 *>((((UnityARSessionNativeInterface_t1130867170_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var))->get_address_of_ARAnchorUpdatedEvent_2()), ((ARAnchorUpdated_t3886071158 *)CastclassSealed((RuntimeObject*)L_4, ARAnchorUpdated_t3886071158_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		ARAnchorUpdated_t3886071158 * L_7 = V_0;
		ARAnchorUpdated_t3886071158 * L_8 = V_1;
		if ((!(((RuntimeObject*)(ARAnchorUpdated_t3886071158 *)L_7) == ((RuntimeObject*)(ARAnchorUpdated_t3886071158 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::add_ARAnchorRemovedEvent(UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorRemoved)
extern "C"  void UnityARSessionNativeInterface_add_ARAnchorRemovedEvent_m2970646020 (RuntimeObject * __this /* static, unused */, ARAnchorRemoved_t142665927 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARSessionNativeInterface_add_ARAnchorRemovedEvent_m2970646020_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ARAnchorRemoved_t142665927 * V_0 = NULL;
	ARAnchorRemoved_t142665927 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		ARAnchorRemoved_t142665927 * L_0 = ((UnityARSessionNativeInterface_t1130867170_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var))->get_ARAnchorRemovedEvent_3();
		V_0 = L_0;
	}

IL_0006:
	{
		ARAnchorRemoved_t142665927 * L_1 = V_0;
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		ARAnchorRemoved_t142665927 * L_2 = V_1;
		ARAnchorRemoved_t142665927 * L_3 = ___value0;
		Delegate_t3022476291 * L_4 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		ARAnchorRemoved_t142665927 * L_5 = V_0;
		ARAnchorRemoved_t142665927 * L_6 = InterlockedCompareExchangeImpl<ARAnchorRemoved_t142665927 *>((((UnityARSessionNativeInterface_t1130867170_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var))->get_address_of_ARAnchorRemovedEvent_3()), ((ARAnchorRemoved_t142665927 *)CastclassSealed((RuntimeObject*)L_4, ARAnchorRemoved_t142665927_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		ARAnchorRemoved_t142665927 * L_7 = V_0;
		ARAnchorRemoved_t142665927 * L_8 = V_1;
		if ((!(((RuntimeObject*)(ARAnchorRemoved_t142665927 *)L_7) == ((RuntimeObject*)(ARAnchorRemoved_t142665927 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::remove_ARAnchorRemovedEvent(UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorRemoved)
extern "C"  void UnityARSessionNativeInterface_remove_ARAnchorRemovedEvent_m1176445575 (RuntimeObject * __this /* static, unused */, ARAnchorRemoved_t142665927 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARSessionNativeInterface_remove_ARAnchorRemovedEvent_m1176445575_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ARAnchorRemoved_t142665927 * V_0 = NULL;
	ARAnchorRemoved_t142665927 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		ARAnchorRemoved_t142665927 * L_0 = ((UnityARSessionNativeInterface_t1130867170_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var))->get_ARAnchorRemovedEvent_3();
		V_0 = L_0;
	}

IL_0006:
	{
		ARAnchorRemoved_t142665927 * L_1 = V_0;
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		ARAnchorRemoved_t142665927 * L_2 = V_1;
		ARAnchorRemoved_t142665927 * L_3 = ___value0;
		Delegate_t3022476291 * L_4 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		ARAnchorRemoved_t142665927 * L_5 = V_0;
		ARAnchorRemoved_t142665927 * L_6 = InterlockedCompareExchangeImpl<ARAnchorRemoved_t142665927 *>((((UnityARSessionNativeInterface_t1130867170_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var))->get_address_of_ARAnchorRemovedEvent_3()), ((ARAnchorRemoved_t142665927 *)CastclassSealed((RuntimeObject*)L_4, ARAnchorRemoved_t142665927_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		ARAnchorRemoved_t142665927 * L_7 = V_0;
		ARAnchorRemoved_t142665927 * L_8 = V_1;
		if ((!(((RuntimeObject*)(ARAnchorRemoved_t142665927 *)L_7) == ((RuntimeObject*)(ARAnchorRemoved_t142665927 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::add_ARSessionFailedEvent(UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARSessionFailed)
extern "C"  void UnityARSessionNativeInterface_add_ARSessionFailedEvent_m1972153860 (RuntimeObject * __this /* static, unused */, ARSessionFailed_t872580813 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARSessionNativeInterface_add_ARSessionFailedEvent_m1972153860_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ARSessionFailed_t872580813 * V_0 = NULL;
	ARSessionFailed_t872580813 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		ARSessionFailed_t872580813 * L_0 = ((UnityARSessionNativeInterface_t1130867170_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var))->get_ARSessionFailedEvent_4();
		V_0 = L_0;
	}

IL_0006:
	{
		ARSessionFailed_t872580813 * L_1 = V_0;
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		ARSessionFailed_t872580813 * L_2 = V_1;
		ARSessionFailed_t872580813 * L_3 = ___value0;
		Delegate_t3022476291 * L_4 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		ARSessionFailed_t872580813 * L_5 = V_0;
		ARSessionFailed_t872580813 * L_6 = InterlockedCompareExchangeImpl<ARSessionFailed_t872580813 *>((((UnityARSessionNativeInterface_t1130867170_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var))->get_address_of_ARSessionFailedEvent_4()), ((ARSessionFailed_t872580813 *)CastclassSealed((RuntimeObject*)L_4, ARSessionFailed_t872580813_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		ARSessionFailed_t872580813 * L_7 = V_0;
		ARSessionFailed_t872580813 * L_8 = V_1;
		if ((!(((RuntimeObject*)(ARSessionFailed_t872580813 *)L_7) == ((RuntimeObject*)(ARSessionFailed_t872580813 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::remove_ARSessionFailedEvent(UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARSessionFailed)
extern "C"  void UnityARSessionNativeInterface_remove_ARSessionFailedEvent_m2378491719 (RuntimeObject * __this /* static, unused */, ARSessionFailed_t872580813 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARSessionNativeInterface_remove_ARSessionFailedEvent_m2378491719_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ARSessionFailed_t872580813 * V_0 = NULL;
	ARSessionFailed_t872580813 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		ARSessionFailed_t872580813 * L_0 = ((UnityARSessionNativeInterface_t1130867170_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var))->get_ARSessionFailedEvent_4();
		V_0 = L_0;
	}

IL_0006:
	{
		ARSessionFailed_t872580813 * L_1 = V_0;
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		ARSessionFailed_t872580813 * L_2 = V_1;
		ARSessionFailed_t872580813 * L_3 = ___value0;
		Delegate_t3022476291 * L_4 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		ARSessionFailed_t872580813 * L_5 = V_0;
		ARSessionFailed_t872580813 * L_6 = InterlockedCompareExchangeImpl<ARSessionFailed_t872580813 *>((((UnityARSessionNativeInterface_t1130867170_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var))->get_address_of_ARSessionFailedEvent_4()), ((ARSessionFailed_t872580813 *)CastclassSealed((RuntimeObject*)L_4, ARSessionFailed_t872580813_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		ARSessionFailed_t872580813 * L_7 = V_0;
		ARSessionFailed_t872580813 * L_8 = V_1;
		if ((!(((RuntimeObject*)(ARSessionFailed_t872580813 *)L_7) == ((RuntimeObject*)(ARSessionFailed_t872580813 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
extern "C" intptr_t DEFAULT_CALL unity_CreateNativeARSession(Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer);
// System.IntPtr UnityEngine.XR.iOS.UnityARSessionNativeInterface::unity_CreateNativeARSession(UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFrameUpdate,UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorAdded,UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorUpdated,UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorRemoved,UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARSessionFailed)
extern "C"  IntPtr_t UnityARSessionNativeInterface_unity_CreateNativeARSession_m1019965107 (RuntimeObject * __this /* static, unused */, internal_ARFrameUpdate_t3296518558 * ___frameUpdate0, internal_ARAnchorAdded_t1622117597 * ___anchorAdded1, internal_ARAnchorUpdated_t3705772742 * ___anchorUpdated2, internal_ARAnchorRemoved_t3189755211 * ___anchorRemoved3, ARSessionFailed_t872580813 * ___sessionFailed4, const RuntimeMethod* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer);

	// Marshaling of parameter '___frameUpdate0' to native representation
	Il2CppMethodPointer ____frameUpdate0_marshaled = NULL;
	____frameUpdate0_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___frameUpdate0));

	// Marshaling of parameter '___anchorAdded1' to native representation
	Il2CppMethodPointer ____anchorAdded1_marshaled = NULL;
	____anchorAdded1_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___anchorAdded1));

	// Marshaling of parameter '___anchorUpdated2' to native representation
	Il2CppMethodPointer ____anchorUpdated2_marshaled = NULL;
	____anchorUpdated2_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___anchorUpdated2));

	// Marshaling of parameter '___anchorRemoved3' to native representation
	Il2CppMethodPointer ____anchorRemoved3_marshaled = NULL;
	____anchorRemoved3_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___anchorRemoved3));

	// Marshaling of parameter '___sessionFailed4' to native representation
	Il2CppMethodPointer ____sessionFailed4_marshaled = NULL;
	____sessionFailed4_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___sessionFailed4));

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(unity_CreateNativeARSession)(____frameUpdate0_marshaled, ____anchorAdded1_marshaled, ____anchorUpdated2_marshaled, ____anchorRemoved3_marshaled, ____sessionFailed4_marshaled);

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)(returnValue)));

	return _returnValue_unmarshaled;
}
extern "C" void DEFAULT_CALL StartWorldTrackingSession(intptr_t, ARKitWorldTackingSessionConfiguration_t1821734930_marshaled_pinvoke);
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::StartWorldTrackingSession(System.IntPtr,UnityEngine.XR.iOS.ARKitWorldTackingSessionConfiguration)
extern "C"  void UnityARSessionNativeInterface_StartWorldTrackingSession_m3140261726 (RuntimeObject * __this /* static, unused */, IntPtr_t ___nativeSession0, ARKitWorldTackingSessionConfiguration_t1821734930  ___configuration1, const RuntimeMethod* method)
{


	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t, ARKitWorldTackingSessionConfiguration_t1821734930_marshaled_pinvoke);

	// Marshaling of parameter '___configuration1' to native representation
	ARKitWorldTackingSessionConfiguration_t1821734930_marshaled_pinvoke ____configuration1_marshaled = {};
	ARKitWorldTackingSessionConfiguration_t1821734930_marshal_pinvoke(___configuration1, ____configuration1_marshaled);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(StartWorldTrackingSession)(reinterpret_cast<intptr_t>((___nativeSession0).get_m_value_0()), ____configuration1_marshaled);

	// Marshaling cleanup of parameter '___configuration1' native representation
	ARKitWorldTackingSessionConfiguration_t1821734930_marshal_pinvoke_cleanup(____configuration1_marshaled);

}
extern "C" void DEFAULT_CALL StartWorldTrackingSessionWithOptions(intptr_t, ARKitWorldTackingSessionConfiguration_t1821734930_marshaled_pinvoke, int32_t);
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::StartWorldTrackingSessionWithOptions(System.IntPtr,UnityEngine.XR.iOS.ARKitWorldTackingSessionConfiguration,UnityEngine.XR.iOS.UnityARSessionRunOption)
extern "C"  void UnityARSessionNativeInterface_StartWorldTrackingSessionWithOptions_m3150342870 (RuntimeObject * __this /* static, unused */, IntPtr_t ___nativeSession0, ARKitWorldTackingSessionConfiguration_t1821734930  ___configuration1, int32_t ___runOptions2, const RuntimeMethod* method)
{


	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t, ARKitWorldTackingSessionConfiguration_t1821734930_marshaled_pinvoke, int32_t);

	// Marshaling of parameter '___configuration1' to native representation
	ARKitWorldTackingSessionConfiguration_t1821734930_marshaled_pinvoke ____configuration1_marshaled = {};
	ARKitWorldTackingSessionConfiguration_t1821734930_marshal_pinvoke(___configuration1, ____configuration1_marshaled);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(StartWorldTrackingSessionWithOptions)(reinterpret_cast<intptr_t>((___nativeSession0).get_m_value_0()), ____configuration1_marshaled, ___runOptions2);

	// Marshaling cleanup of parameter '___configuration1' native representation
	ARKitWorldTackingSessionConfiguration_t1821734930_marshal_pinvoke_cleanup(____configuration1_marshaled);

}
extern "C" void DEFAULT_CALL StartSession(intptr_t, ARKitSessionConfiguration_t318899795_marshaled_pinvoke);
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::StartSession(System.IntPtr,UnityEngine.XR.iOS.ARKitSessionConfiguration)
extern "C"  void UnityARSessionNativeInterface_StartSession_m227007524 (RuntimeObject * __this /* static, unused */, IntPtr_t ___nativeSession0, ARKitSessionConfiguration_t318899795  ___configuration1, const RuntimeMethod* method)
{


	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t, ARKitSessionConfiguration_t318899795_marshaled_pinvoke);

	// Marshaling of parameter '___configuration1' to native representation
	ARKitSessionConfiguration_t318899795_marshaled_pinvoke ____configuration1_marshaled = {};
	ARKitSessionConfiguration_t318899795_marshal_pinvoke(___configuration1, ____configuration1_marshaled);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(StartSession)(reinterpret_cast<intptr_t>((___nativeSession0).get_m_value_0()), ____configuration1_marshaled);

	// Marshaling cleanup of parameter '___configuration1' native representation
	ARKitSessionConfiguration_t318899795_marshal_pinvoke_cleanup(____configuration1_marshaled);

}
extern "C" void DEFAULT_CALL StartSessionWithOptions(intptr_t, ARKitSessionConfiguration_t318899795_marshaled_pinvoke, int32_t);
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::StartSessionWithOptions(System.IntPtr,UnityEngine.XR.iOS.ARKitSessionConfiguration,UnityEngine.XR.iOS.UnityARSessionRunOption)
extern "C"  void UnityARSessionNativeInterface_StartSessionWithOptions_m1815473114 (RuntimeObject * __this /* static, unused */, IntPtr_t ___nativeSession0, ARKitSessionConfiguration_t318899795  ___configuration1, int32_t ___runOptions2, const RuntimeMethod* method)
{


	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t, ARKitSessionConfiguration_t318899795_marshaled_pinvoke, int32_t);

	// Marshaling of parameter '___configuration1' to native representation
	ARKitSessionConfiguration_t318899795_marshaled_pinvoke ____configuration1_marshaled = {};
	ARKitSessionConfiguration_t318899795_marshal_pinvoke(___configuration1, ____configuration1_marshaled);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(StartSessionWithOptions)(reinterpret_cast<intptr_t>((___nativeSession0).get_m_value_0()), ____configuration1_marshaled, ___runOptions2);

	// Marshaling cleanup of parameter '___configuration1' native representation
	ARKitSessionConfiguration_t318899795_marshal_pinvoke_cleanup(____configuration1_marshaled);

}
extern "C" void DEFAULT_CALL PauseSession(intptr_t);
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::PauseSession(System.IntPtr)
extern "C"  void UnityARSessionNativeInterface_PauseSession_m2717865525 (RuntimeObject * __this /* static, unused */, IntPtr_t ___nativeSession0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(PauseSession)(reinterpret_cast<intptr_t>((___nativeSession0).get_m_value_0()));

}
extern "C" int32_t DEFAULT_CALL HitTest(intptr_t, ARPoint_t3436811567 , int64_t);
// System.Int32 UnityEngine.XR.iOS.UnityARSessionNativeInterface::HitTest(System.IntPtr,UnityEngine.XR.iOS.ARPoint,UnityEngine.XR.iOS.ARHitTestResultType)
extern "C"  int32_t UnityARSessionNativeInterface_HitTest_m1115034644 (RuntimeObject * __this /* static, unused */, IntPtr_t ___nativeSession0, ARPoint_t3436811567  ___point1, int64_t ___types2, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, ARPoint_t3436811567 , int64_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(HitTest)(reinterpret_cast<intptr_t>((___nativeSession0).get_m_value_0()), ___point1, ___types2);

	return returnValue;
}
extern "C" UnityARHitTestResult_t4129824344_marshaled_pinvoke DEFAULT_CALL GetLastHitTestResult(int32_t);
// UnityEngine.XR.iOS.UnityARHitTestResult UnityEngine.XR.iOS.UnityARSessionNativeInterface::GetLastHitTestResult(System.Int32)
extern "C"  UnityARHitTestResult_t4129824344  UnityARSessionNativeInterface_GetLastHitTestResult_m1489387413 (RuntimeObject * __this /* static, unused */, int32_t ___index0, const RuntimeMethod* method)
{


	typedef UnityARHitTestResult_t4129824344_marshaled_pinvoke (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	UnityARHitTestResult_t4129824344_marshaled_pinvoke returnValue = reinterpret_cast<PInvokeFunc>(GetLastHitTestResult)(___index0);

	// Marshaling of return value back from native representation
	UnityARHitTestResult_t4129824344  _returnValue_unmarshaled;
	memset(&_returnValue_unmarshaled, 0, sizeof(_returnValue_unmarshaled));
	UnityARHitTestResult_t4129824344_marshal_pinvoke_back(returnValue, _returnValue_unmarshaled);

	// Marshaling cleanup of return value native representation
	UnityARHitTestResult_t4129824344_marshal_pinvoke_cleanup(returnValue);

	return _returnValue_unmarshaled;
}
extern "C" ARTextureHandles_t3764914833  DEFAULT_CALL GetVideoTextureHandles();
// UnityEngine.XR.iOS.ARTextureHandles UnityEngine.XR.iOS.UnityARSessionNativeInterface::GetVideoTextureHandles()
extern "C"  ARTextureHandles_t3764914833  UnityARSessionNativeInterface_GetVideoTextureHandles_m4073538006 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef ARTextureHandles_t3764914833  (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	ARTextureHandles_t3764914833  returnValue = reinterpret_cast<PInvokeFunc>(GetVideoTextureHandles)();

	return returnValue;
}
extern "C" float DEFAULT_CALL GetAmbientIntensity();
// System.Single UnityEngine.XR.iOS.UnityARSessionNativeInterface::GetAmbientIntensity()
extern "C"  float UnityARSessionNativeInterface_GetAmbientIntensity_m3132982210 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef float (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	float returnValue = reinterpret_cast<PInvokeFunc>(GetAmbientIntensity)();

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL GetTrackingQuality();
// System.Int32 UnityEngine.XR.iOS.UnityARSessionNativeInterface::GetTrackingQuality()
extern "C"  int32_t UnityARSessionNativeInterface_GetTrackingQuality_m3473680033 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(GetTrackingQuality)();

	return returnValue;
}
extern "C" float DEFAULT_CALL GetYUVTexCoordScale();
// System.Single UnityEngine.XR.iOS.UnityARSessionNativeInterface::GetYUVTexCoordScale()
extern "C"  float UnityARSessionNativeInterface_GetYUVTexCoordScale_m671637809 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef float (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	float returnValue = reinterpret_cast<PInvokeFunc>(GetYUVTexCoordScale)();

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL GetARPointCloud(intptr_t*, uint32_t*);
// System.Boolean UnityEngine.XR.iOS.UnityARSessionNativeInterface::GetARPointCloud(System.IntPtr&,System.UInt32&)
extern "C"  bool UnityARSessionNativeInterface_GetARPointCloud_m2491076785 (RuntimeObject * __this /* static, unused */, IntPtr_t* ___verts0, uint32_t* ___vertLength1, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t*, uint32_t*);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(GetARPointCloud)(reinterpret_cast<intptr_t*>(___verts0), ___vertLength1);

	// Marshaling of parameter '___verts0' back from native representation
	___verts0 = reinterpret_cast<IntPtr_t*>(reinterpret_cast<intptr_t*>(___verts0));

	return static_cast<bool>(returnValue);
}
extern "C" void DEFAULT_CALL SetCameraNearFar(float, float);
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::SetCameraNearFar(System.Single,System.Single)
extern "C"  void UnityARSessionNativeInterface_SetCameraNearFar_m1247563277 (RuntimeObject * __this /* static, unused */, float ___nearZ0, float ___farZ1, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (float, float);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(SetCameraNearFar)(___nearZ0, ___farZ1);

}
// UnityEngine.XR.iOS.UnityARSessionNativeInterface UnityEngine.XR.iOS.UnityARSessionNativeInterface::GetARSessionNativeInterface()
extern "C"  UnityARSessionNativeInterface_t1130867170 * UnityARSessionNativeInterface_GetARSessionNativeInterface_m3174488657 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARSessionNativeInterface_GetARSessionNativeInterface_m3174488657_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		UnityARSessionNativeInterface_t1130867170 * L_0 = ((UnityARSessionNativeInterface_t1130867170_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var))->get_s_UnityARSessionNativeInterface_7();
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		UnityARSessionNativeInterface_t1130867170 * L_1 = (UnityARSessionNativeInterface_t1130867170 *)il2cpp_codegen_object_new(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		UnityARSessionNativeInterface__ctor_m2294513111(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		((UnityARSessionNativeInterface_t1130867170_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var))->set_s_UnityARSessionNativeInterface_7(L_1);
	}

IL_0014:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		UnityARSessionNativeInterface_t1130867170 * L_2 = ((UnityARSessionNativeInterface_t1130867170_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var))->get_s_UnityARSessionNativeInterface_7();
		return L_2;
	}
}
// UnityEngine.Matrix4x4 UnityEngine.XR.iOS.UnityARSessionNativeInterface::GetCameraPose()
extern "C"  Matrix4x4_t2933234003  UnityARSessionNativeInterface_GetCameraPose_m3046824030 (UnityARSessionNativeInterface_t1130867170 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARSessionNativeInterface_GetCameraPose_m3046824030_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Matrix4x4_t2933234003  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (Matrix4x4_t2933234003_il2cpp_TypeInfo_var, (&V_0));
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		UnityARMatrix4x4_t100931615 * L_0 = (((UnityARSessionNativeInterface_t1130867170_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var))->get_address_of_s_Camera_6())->get_address_of_worldTransform_0();
		Vector4_t2243707581  L_1 = L_0->get_column0_0();
		Matrix4x4_SetColumn_m3120649749((&V_0), 0, L_1, /*hidden argument*/NULL);
		UnityARMatrix4x4_t100931615 * L_2 = (((UnityARSessionNativeInterface_t1130867170_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var))->get_address_of_s_Camera_6())->get_address_of_worldTransform_0();
		Vector4_t2243707581  L_3 = L_2->get_column1_1();
		Matrix4x4_SetColumn_m3120649749((&V_0), 1, L_3, /*hidden argument*/NULL);
		UnityARMatrix4x4_t100931615 * L_4 = (((UnityARSessionNativeInterface_t1130867170_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var))->get_address_of_s_Camera_6())->get_address_of_worldTransform_0();
		Vector4_t2243707581  L_5 = L_4->get_column2_2();
		Matrix4x4_SetColumn_m3120649749((&V_0), 2, L_5, /*hidden argument*/NULL);
		UnityARMatrix4x4_t100931615 * L_6 = (((UnityARSessionNativeInterface_t1130867170_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var))->get_address_of_s_Camera_6())->get_address_of_worldTransform_0();
		Vector4_t2243707581  L_7 = L_6->get_column3_3();
		Matrix4x4_SetColumn_m3120649749((&V_0), 3, L_7, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_8 = V_0;
		return L_8;
	}
}
// UnityEngine.Matrix4x4 UnityEngine.XR.iOS.UnityARSessionNativeInterface::GetCameraProjection()
extern "C"  Matrix4x4_t2933234003  UnityARSessionNativeInterface_GetCameraProjection_m3168017698 (UnityARSessionNativeInterface_t1130867170 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARSessionNativeInterface_GetCameraProjection_m3168017698_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Matrix4x4_t2933234003  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (Matrix4x4_t2933234003_il2cpp_TypeInfo_var, (&V_0));
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		UnityARMatrix4x4_t100931615 * L_0 = (((UnityARSessionNativeInterface_t1130867170_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var))->get_address_of_s_Camera_6())->get_address_of_projectionMatrix_1();
		Vector4_t2243707581  L_1 = L_0->get_column0_0();
		Matrix4x4_SetColumn_m3120649749((&V_0), 0, L_1, /*hidden argument*/NULL);
		UnityARMatrix4x4_t100931615 * L_2 = (((UnityARSessionNativeInterface_t1130867170_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var))->get_address_of_s_Camera_6())->get_address_of_projectionMatrix_1();
		Vector4_t2243707581  L_3 = L_2->get_column1_1();
		Matrix4x4_SetColumn_m3120649749((&V_0), 1, L_3, /*hidden argument*/NULL);
		UnityARMatrix4x4_t100931615 * L_4 = (((UnityARSessionNativeInterface_t1130867170_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var))->get_address_of_s_Camera_6())->get_address_of_projectionMatrix_1();
		Vector4_t2243707581  L_5 = L_4->get_column2_2();
		Matrix4x4_SetColumn_m3120649749((&V_0), 2, L_5, /*hidden argument*/NULL);
		UnityARMatrix4x4_t100931615 * L_6 = (((UnityARSessionNativeInterface_t1130867170_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var))->get_address_of_s_Camera_6())->get_address_of_projectionMatrix_1();
		Vector4_t2243707581  L_7 = L_6->get_column3_3();
		Matrix4x4_SetColumn_m3120649749((&V_0), 3, L_7, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_8 = V_0;
		return L_8;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::SetCameraClipPlanes(System.Single,System.Single)
extern "C"  void UnityARSessionNativeInterface_SetCameraClipPlanes_m149558303 (UnityARSessionNativeInterface_t1130867170 * __this, float ___nearZ0, float ___farZ1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARSessionNativeInterface_SetCameraClipPlanes_m149558303_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = ___nearZ0;
		float L_1 = ___farZ1;
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		UnityARSessionNativeInterface_SetCameraNearFar_m1247563277(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::_frame_update(UnityEngine.XR.iOS.internal_UnityARCamera)
extern "C"  void UnityARSessionNativeInterface__frame_update_m127370 (RuntimeObject * __this /* static, unused */, internal_UnityARCamera_t2580192745  ___camera0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARSessionNativeInterface__frame_update_m127370_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UnityARCamera_t4198559457  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (UnityARCamera_t4198559457_il2cpp_TypeInfo_var, (&V_0));
		UnityARMatrix4x4_t100931615  L_0 = (&___camera0)->get_projectionMatrix_1();
		(&V_0)->set_projectionMatrix_1(L_0);
		UnityARMatrix4x4_t100931615  L_1 = (&___camera0)->get_worldTransform_0();
		(&V_0)->set_worldTransform_0(L_1);
		int32_t L_2 = (&___camera0)->get_trackingState_2();
		(&V_0)->set_trackingState_2(L_2);
		int32_t L_3 = (&___camera0)->get_trackingReason_3();
		(&V_0)->set_trackingReason_3(L_3);
		UnityARCamera_t4198559457  L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		((UnityARSessionNativeInterface_t1130867170_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var))->set_s_Camera_6(L_4);
		uint32_t L_5 = (&___camera0)->get_getPointCloudData_4();
		if ((!(((uint32_t)L_5) == ((uint32_t)1))))
		{
			goto IL_005d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		UnityARSessionNativeInterface_UpdatePointCloudData_m2452168060(NULL /*static, unused*/, (((UnityARSessionNativeInterface_t1130867170_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var))->get_address_of_s_Camera_6()), /*hidden argument*/NULL);
	}

IL_005d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		ARFrameUpdate_t496507918 * L_6 = ((UnityARSessionNativeInterface_t1130867170_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var))->get_ARFrameUpdatedEvent_0();
		if (!L_6)
		{
			goto IL_0076;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		ARFrameUpdate_t496507918 * L_7 = ((UnityARSessionNativeInterface_t1130867170_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var))->get_ARFrameUpdatedEvent_0();
		UnityARCamera_t4198559457  L_8 = ((UnityARSessionNativeInterface_t1130867170_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var))->get_s_Camera_6();
		NullCheck(L_7);
		ARFrameUpdate_Invoke_m3133737564(L_7, L_8, /*hidden argument*/NULL);
	}

IL_0076:
	{
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::UpdatePointCloudData(UnityEngine.XR.iOS.UnityARCamera&)
extern "C"  void UnityARSessionNativeInterface_UpdatePointCloudData_m2452168060 (RuntimeObject * __this /* static, unused */, UnityARCamera_t4198559457 * ___camera0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARSessionNativeInterface_UpdatePointCloudData_m2452168060_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IntPtr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	uint32_t V_1 = 0;
	bool V_2 = false;
	SingleU5BU5D_t577127397* V_3 = NULL;
	Vector3U5BU5D_t1172311765* V_4 = NULL;
	int32_t V_5 = 0;
	{
		IntPtr_t L_0 = ((IntPtr_t_StaticFields*)il2cpp_codegen_static_fields_for(IntPtr_t_il2cpp_TypeInfo_var))->get_Zero_1();
		V_0 = L_0;
		V_1 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		bool L_1 = UnityARSessionNativeInterface_GetARPointCloud_m2491076785(NULL /*static, unused*/, (&V_0), (&V_1), /*hidden argument*/NULL);
		V_2 = L_1;
		V_3 = (SingleU5BU5D_t577127397*)NULL;
		bool L_2 = V_2;
		if (!L_2)
		{
			goto IL_00a2;
		}
	}
	{
		uint32_t L_3 = V_1;
		V_3 = ((SingleU5BU5D_t577127397*)SZArrayNew(SingleU5BU5D_t577127397_il2cpp_TypeInfo_var, (uint32_t)(((uintptr_t)L_3))));
		IntPtr_t L_4 = V_0;
		SingleU5BU5D_t577127397* L_5 = V_3;
		uint32_t L_6 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t785896760_il2cpp_TypeInfo_var);
		Marshal_Copy_m2353359830(NULL /*static, unused*/, L_4, L_5, 0, L_6, /*hidden argument*/NULL);
		uint32_t L_7 = V_1;
		V_4 = ((Vector3U5BU5D_t1172311765*)SZArrayNew(Vector3U5BU5D_t1172311765_il2cpp_TypeInfo_var, (uint32_t)(((uintptr_t)((int32_t)((uint32_t)(int32_t)L_7/(uint32_t)(int32_t)4))))));
		V_5 = 0;
		goto IL_0090;
	}

IL_003e:
	{
		Vector3U5BU5D_t1172311765* L_8 = V_4;
		int32_t L_9 = V_5;
		NullCheck(L_8);
		SingleU5BU5D_t577127397* L_10 = V_3;
		int32_t L_11 = V_5;
		int32_t L_12 = L_11;
		V_5 = ((int32_t)((int32_t)L_12+(int32_t)1));
		NullCheck(L_10);
		int32_t L_13 = L_12;
		float L_14 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_9/(int32_t)4)))))->set_x_1(L_14);
		Vector3U5BU5D_t1172311765* L_15 = V_4;
		int32_t L_16 = V_5;
		NullCheck(L_15);
		SingleU5BU5D_t577127397* L_17 = V_3;
		int32_t L_18 = V_5;
		int32_t L_19 = L_18;
		V_5 = ((int32_t)((int32_t)L_19+(int32_t)1));
		NullCheck(L_17);
		int32_t L_20 = L_19;
		float L_21 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		((L_15)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_16/(int32_t)4)))))->set_y_2(L_21);
		Vector3U5BU5D_t1172311765* L_22 = V_4;
		int32_t L_23 = V_5;
		NullCheck(L_22);
		SingleU5BU5D_t577127397* L_24 = V_3;
		int32_t L_25 = V_5;
		int32_t L_26 = L_25;
		V_5 = ((int32_t)((int32_t)L_26+(int32_t)1));
		NullCheck(L_24);
		int32_t L_27 = L_26;
		float L_28 = (L_24)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		((L_22)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_23/(int32_t)4)))))->set_z_3(((-L_28)));
		int32_t L_29 = V_5;
		V_5 = ((int32_t)((int32_t)L_29+(int32_t)1));
	}

IL_0090:
	{
		int32_t L_30 = V_5;
		uint32_t L_31 = V_1;
		if ((((int64_t)(((int64_t)((int64_t)L_30)))) < ((int64_t)(((int64_t)((uint64_t)L_31))))))
		{
			goto IL_003e;
		}
	}
	{
		UnityARCamera_t4198559457 * L_32 = ___camera0;
		Vector3U5BU5D_t1172311765* L_33 = V_4;
		L_32->set_pointCloudData_4(L_33);
	}

IL_00a2:
	{
		return;
	}
}
// UnityEngine.XR.iOS.ARPlaneAnchor UnityEngine.XR.iOS.UnityARSessionNativeInterface::GetPlaneAnchorFromAnchorData(UnityEngine.XR.iOS.UnityARAnchorData)
extern "C"  ARPlaneAnchor_t1439520888  UnityARSessionNativeInterface_GetPlaneAnchorFromAnchorData_m3105060455 (RuntimeObject * __this /* static, unused */, UnityARAnchorData_t2901866349  ___anchor0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARSessionNativeInterface_GetPlaneAnchorFromAnchorData_m3105060455_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ARPlaneAnchor_t1439520888  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Matrix4x4_t2933234003  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Initobj (ARPlaneAnchor_t1439520888_il2cpp_TypeInfo_var, (&V_0));
		IntPtr_t L_0 = (&___anchor0)->get_ptrIdentifier_0();
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t785896760_il2cpp_TypeInfo_var);
		String_t* L_1 = Marshal_PtrToStringAuto_m3496615756(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		(&V_0)->set_identifier_0(L_1);
		Initobj (Matrix4x4_t2933234003_il2cpp_TypeInfo_var, (&V_1));
		UnityARMatrix4x4_t100931615 * L_2 = (&___anchor0)->get_address_of_transform_1();
		Vector4_t2243707581  L_3 = L_2->get_column0_0();
		Matrix4x4_SetColumn_m3120649749((&V_1), 0, L_3, /*hidden argument*/NULL);
		UnityARMatrix4x4_t100931615 * L_4 = (&___anchor0)->get_address_of_transform_1();
		Vector4_t2243707581  L_5 = L_4->get_column1_1();
		Matrix4x4_SetColumn_m3120649749((&V_1), 1, L_5, /*hidden argument*/NULL);
		UnityARMatrix4x4_t100931615 * L_6 = (&___anchor0)->get_address_of_transform_1();
		Vector4_t2243707581  L_7 = L_6->get_column2_2();
		Matrix4x4_SetColumn_m3120649749((&V_1), 2, L_7, /*hidden argument*/NULL);
		UnityARMatrix4x4_t100931615 * L_8 = (&___anchor0)->get_address_of_transform_1();
		Vector4_t2243707581  L_9 = L_8->get_column3_3();
		Matrix4x4_SetColumn_m3120649749((&V_1), 3, L_9, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_10 = V_1;
		(&V_0)->set_transform_1(L_10);
		int64_t L_11 = (&___anchor0)->get_alignment_2();
		(&V_0)->set_alignment_2(L_11);
		Vector4_t2243707581 * L_12 = (&___anchor0)->get_address_of_center_3();
		float L_13 = L_12->get_x_1();
		Vector4_t2243707581 * L_14 = (&___anchor0)->get_address_of_center_3();
		float L_15 = L_14->get_y_2();
		Vector4_t2243707581 * L_16 = (&___anchor0)->get_address_of_center_3();
		float L_17 = L_16->get_z_3();
		Vector3_t2243707580  L_18;
		memset(&L_18, 0, sizeof(L_18));
		Vector3__ctor_m2638739322((&L_18), L_13, L_15, L_17, /*hidden argument*/NULL);
		(&V_0)->set_center_3(L_18);
		Vector4_t2243707581 * L_19 = (&___anchor0)->get_address_of_extent_4();
		float L_20 = L_19->get_x_1();
		Vector4_t2243707581 * L_21 = (&___anchor0)->get_address_of_extent_4();
		float L_22 = L_21->get_y_2();
		Vector4_t2243707581 * L_23 = (&___anchor0)->get_address_of_extent_4();
		float L_24 = L_23->get_z_3();
		Vector3_t2243707580  L_25;
		memset(&L_25, 0, sizeof(L_25));
		Vector3__ctor_m2638739322((&L_25), L_20, L_22, L_24, /*hidden argument*/NULL);
		(&V_0)->set_extent_4(L_25);
		ARPlaneAnchor_t1439520888  L_26 = V_0;
		return L_26;
	}
}
// UnityEngine.XR.iOS.ARHitTestResult UnityEngine.XR.iOS.UnityARSessionNativeInterface::GetHitTestResultFromResultData(UnityEngine.XR.iOS.UnityARHitTestResult)
extern "C"  ARHitTestResult_t3275513025  UnityARSessionNativeInterface_GetHitTestResultFromResultData_m1356947160 (RuntimeObject * __this /* static, unused */, UnityARHitTestResult_t4129824344  ___resultData0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARSessionNativeInterface_GetHitTestResultFromResultData_m1356947160_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ARHitTestResult_t3275513025  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (ARHitTestResult_t3275513025_il2cpp_TypeInfo_var, (&V_0));
		int64_t L_0 = (&___resultData0)->get_type_0();
		(&V_0)->set_type_0(L_0);
		double L_1 = (&___resultData0)->get_distance_1();
		(&V_0)->set_distance_1(L_1);
		Matrix4x4_t2933234003  L_2 = (&___resultData0)->get_localTransform_2();
		(&V_0)->set_localTransform_2(L_2);
		Matrix4x4_t2933234003  L_3 = (&___resultData0)->get_worldTransform_3();
		(&V_0)->set_worldTransform_3(L_3);
		bool L_4 = (&___resultData0)->get_isValid_5();
		(&V_0)->set_isValid_5(L_4);
		IntPtr_t L_5 = (&___resultData0)->get_anchor_4();
		IntPtr_t L_6 = ((IntPtr_t_StaticFields*)il2cpp_codegen_static_fields_for(IntPtr_t_il2cpp_TypeInfo_var))->get_Zero_1();
		bool L_7 = IntPtr_op_Inequality_m3044532593(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0077;
		}
	}
	{
		IntPtr_t L_8 = (&___resultData0)->get_anchor_4();
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t785896760_il2cpp_TypeInfo_var);
		String_t* L_9 = Marshal_PtrToStringAuto_m3496615756(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		(&V_0)->set_anchorIdentifier_4(L_9);
	}

IL_0077:
	{
		ARHitTestResult_t3275513025  L_10 = V_0;
		return L_10;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::_anchor_added(UnityEngine.XR.iOS.UnityARAnchorData)
extern "C"  void UnityARSessionNativeInterface__anchor_added_m3584567327 (RuntimeObject * __this /* static, unused */, UnityARAnchorData_t2901866349  ___anchor0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARSessionNativeInterface__anchor_added_m3584567327_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ARPlaneAnchor_t1439520888  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		ARAnchorAdded_t2646854145 * L_0 = ((UnityARSessionNativeInterface_t1130867170_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var))->get_ARAnchorAddedEvent_1();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		UnityARAnchorData_t2901866349  L_1 = ___anchor0;
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		ARPlaneAnchor_t1439520888  L_2 = UnityARSessionNativeInterface_GetPlaneAnchorFromAnchorData_m3105060455(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		ARAnchorAdded_t2646854145 * L_3 = ((UnityARSessionNativeInterface_t1130867170_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var))->get_ARAnchorAddedEvent_1();
		ARPlaneAnchor_t1439520888  L_4 = V_0;
		NullCheck(L_3);
		ARAnchorAdded_Invoke_m3296517664(L_3, L_4, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::_anchor_updated(UnityEngine.XR.iOS.UnityARAnchorData)
extern "C"  void UnityARSessionNativeInterface__anchor_updated_m1970308864 (RuntimeObject * __this /* static, unused */, UnityARAnchorData_t2901866349  ___anchor0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARSessionNativeInterface__anchor_updated_m1970308864_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ARPlaneAnchor_t1439520888  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		ARAnchorUpdated_t3886071158 * L_0 = ((UnityARSessionNativeInterface_t1130867170_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var))->get_ARAnchorUpdatedEvent_2();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		UnityARAnchorData_t2901866349  L_1 = ___anchor0;
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		ARPlaneAnchor_t1439520888  L_2 = UnityARSessionNativeInterface_GetPlaneAnchorFromAnchorData_m3105060455(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		ARAnchorUpdated_t3886071158 * L_3 = ((UnityARSessionNativeInterface_t1130867170_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var))->get_ARAnchorUpdatedEvent_2();
		ARPlaneAnchor_t1439520888  L_4 = V_0;
		NullCheck(L_3);
		ARAnchorUpdated_Invoke_m1018775699(L_3, L_4, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::_anchor_removed(UnityEngine.XR.iOS.UnityARAnchorData)
extern "C"  void UnityARSessionNativeInterface__anchor_removed_m4240378233 (RuntimeObject * __this /* static, unused */, UnityARAnchorData_t2901866349  ___anchor0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARSessionNativeInterface__anchor_removed_m4240378233_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ARPlaneAnchor_t1439520888  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		ARAnchorRemoved_t142665927 * L_0 = ((UnityARSessionNativeInterface_t1130867170_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var))->get_ARAnchorRemovedEvent_3();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		UnityARAnchorData_t2901866349  L_1 = ___anchor0;
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		ARPlaneAnchor_t1439520888  L_2 = UnityARSessionNativeInterface_GetPlaneAnchorFromAnchorData_m3105060455(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		ARAnchorRemoved_t142665927 * L_3 = ((UnityARSessionNativeInterface_t1130867170_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var))->get_ARAnchorRemovedEvent_3();
		ARPlaneAnchor_t1439520888  L_4 = V_0;
		NullCheck(L_3);
		ARAnchorRemoved_Invoke_m4120555414(L_3, L_4, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::_ar_session_failed(System.String)
extern "C"  void UnityARSessionNativeInterface__ar_session_failed_m2515310284 (RuntimeObject * __this /* static, unused */, String_t* ___error0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARSessionNativeInterface__ar_session_failed_m2515310284_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral2733749110, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		ARSessionFailed_t872580813 * L_0 = ((UnityARSessionNativeInterface_t1130867170_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var))->get_ARSessionFailedEvent_4();
		if (!L_0)
		{
			goto IL_001f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		ARSessionFailed_t872580813 * L_1 = ((UnityARSessionNativeInterface_t1130867170_StaticFields*)il2cpp_codegen_static_fields_for(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var))->get_ARSessionFailedEvent_4();
		String_t* L_2 = ___error0;
		NullCheck(L_1);
		ARSessionFailed_Invoke_m3664045560(L_1, L_2, /*hidden argument*/NULL);
	}

IL_001f:
	{
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::RunWithConfigAndOptions(UnityEngine.XR.iOS.ARKitWorldTackingSessionConfiguration,UnityEngine.XR.iOS.UnityARSessionRunOption)
extern "C"  void UnityARSessionNativeInterface_RunWithConfigAndOptions_m375276821 (UnityARSessionNativeInterface_t1130867170 * __this, ARKitWorldTackingSessionConfiguration_t1821734930  ___config0, int32_t ___runOptions1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARSessionNativeInterface_RunWithConfigAndOptions_m375276821_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0 = __this->get_m_NativeARSession_5();
		ARKitWorldTackingSessionConfiguration_t1821734930  L_1 = ___config0;
		int32_t L_2 = ___runOptions1;
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		UnityARSessionNativeInterface_StartWorldTrackingSessionWithOptions_m3150342870(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::RunWithConfig(UnityEngine.XR.iOS.ARKitWorldTackingSessionConfiguration)
extern "C"  void UnityARSessionNativeInterface_RunWithConfig_m3195925270 (UnityARSessionNativeInterface_t1130867170 * __this, ARKitWorldTackingSessionConfiguration_t1821734930  ___config0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARSessionNativeInterface_RunWithConfig_m3195925270_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0 = __this->get_m_NativeARSession_5();
		ARKitWorldTackingSessionConfiguration_t1821734930  L_1 = ___config0;
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		UnityARSessionNativeInterface_StartWorldTrackingSession_m3140261726(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::Run()
extern "C"  void UnityARSessionNativeInterface_Run_m3916396736 (UnityARSessionNativeInterface_t1130867170 * __this, const RuntimeMethod* method)
{
	{
		ARKitWorldTackingSessionConfiguration_t1821734930  L_0;
		memset(&L_0, 0, sizeof(L_0));
		ARKitWorldTackingSessionConfiguration__ctor_m4281543687((&L_0), 0, 1, (bool)0, (bool)0, /*hidden argument*/NULL);
		UnityARSessionNativeInterface_RunWithConfig_m3195925270(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::RunWithConfigAndOptions(UnityEngine.XR.iOS.ARKitSessionConfiguration,UnityEngine.XR.iOS.UnityARSessionRunOption)
extern "C"  void UnityARSessionNativeInterface_RunWithConfigAndOptions_m4179857830 (UnityARSessionNativeInterface_t1130867170 * __this, ARKitSessionConfiguration_t318899795  ___config0, int32_t ___runOptions1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARSessionNativeInterface_RunWithConfigAndOptions_m4179857830_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0 = __this->get_m_NativeARSession_5();
		ARKitSessionConfiguration_t318899795  L_1 = ___config0;
		int32_t L_2 = ___runOptions1;
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		UnityARSessionNativeInterface_StartSessionWithOptions_m1815473114(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::RunWithConfig(UnityEngine.XR.iOS.ARKitSessionConfiguration)
extern "C"  void UnityARSessionNativeInterface_RunWithConfig_m2478060541 (UnityARSessionNativeInterface_t1130867170 * __this, ARKitSessionConfiguration_t318899795  ___config0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARSessionNativeInterface_RunWithConfig_m2478060541_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0 = __this->get_m_NativeARSession_5();
		ARKitSessionConfiguration_t318899795  L_1 = ___config0;
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		UnityARSessionNativeInterface_StartSession_m227007524(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::Pause()
extern "C"  void UnityARSessionNativeInterface_Pause_m2220930613 (UnityARSessionNativeInterface_t1130867170 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARSessionNativeInterface_Pause_m2220930613_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0 = __this->get_m_NativeARSession_5();
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		UnityARSessionNativeInterface_PauseSession_m2717865525(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1<UnityEngine.XR.iOS.ARHitTestResult> UnityEngine.XR.iOS.UnityARSessionNativeInterface::HitTest(UnityEngine.XR.iOS.ARPoint,UnityEngine.XR.iOS.ARHitTestResultType)
extern "C"  List_1_t2644634157 * UnityARSessionNativeInterface_HitTest_m388588674 (UnityARSessionNativeInterface_t1130867170 * __this, ARPoint_t3436811567  ___point0, int64_t ___types1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARSessionNativeInterface_HitTest_m388588674_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	List_1_t2644634157 * V_1 = NULL;
	int32_t V_2 = 0;
	UnityARHitTestResult_t4129824344  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		IntPtr_t L_0 = __this->get_m_NativeARSession_5();
		ARPoint_t3436811567  L_1 = ___point0;
		int64_t L_2 = ___types1;
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		int32_t L_3 = UnityARSessionNativeInterface_HitTest_m1115034644(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = V_0;
		int32_t L_5 = L_4;
		RuntimeObject * L_6 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_5);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral3977806437, L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		List_1_t2644634157 * L_8 = (List_1_t2644634157 *)il2cpp_codegen_object_new(List_1_t2644634157_il2cpp_TypeInfo_var);
		List_1__ctor_m3856316316(L_8, /*hidden argument*/List_1__ctor_m3856316316_RuntimeMethod_var);
		V_1 = L_8;
		V_2 = 0;
		goto IL_0047;
	}

IL_0030:
	{
		int32_t L_9 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		UnityARHitTestResult_t4129824344  L_10 = UnityARSessionNativeInterface_GetLastHitTestResult_m1489387413(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		V_3 = L_10;
		List_1_t2644634157 * L_11 = V_1;
		UnityARHitTestResult_t4129824344  L_12 = V_3;
		ARHitTestResult_t3275513025  L_13 = UnityARSessionNativeInterface_GetHitTestResultFromResultData_m1356947160(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		NullCheck(L_11);
		List_1_Add_m1918634088(L_11, L_13, /*hidden argument*/List_1_Add_m1918634088_RuntimeMethod_var);
		int32_t L_14 = V_2;
		V_2 = ((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_0047:
	{
		int32_t L_15 = V_2;
		int32_t L_16 = V_0;
		if ((((int32_t)L_15) < ((int32_t)L_16)))
		{
			goto IL_0030;
		}
	}
	{
		List_1_t2644634157 * L_17 = V_1;
		return L_17;
	}
}
// UnityEngine.XR.iOS.ARTextureHandles UnityEngine.XR.iOS.UnityARSessionNativeInterface::GetARVideoTextureHandles()
extern "C"  ARTextureHandles_t3764914833  UnityARSessionNativeInterface_GetARVideoTextureHandles_m2905358883 (UnityARSessionNativeInterface_t1130867170 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARSessionNativeInterface_GetARVideoTextureHandles_m2905358883_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		ARTextureHandles_t3764914833  L_0 = UnityARSessionNativeInterface_GetVideoTextureHandles_m4073538006(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Single UnityEngine.XR.iOS.UnityARSessionNativeInterface::GetARAmbientIntensity()
extern "C"  float UnityARSessionNativeInterface_GetARAmbientIntensity_m3261179343 (UnityARSessionNativeInterface_t1130867170 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARSessionNativeInterface_GetARAmbientIntensity_m3261179343_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		float L_0 = UnityARSessionNativeInterface_GetAmbientIntensity_m3132982210(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Int32 UnityEngine.XR.iOS.UnityARSessionNativeInterface::GetARTrackingQuality()
extern "C"  int32_t UnityARSessionNativeInterface_GetARTrackingQuality_m2957489224 (UnityARSessionNativeInterface_t1130867170 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARSessionNativeInterface_GetARTrackingQuality_m2957489224_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		int32_t L_0 = UnityARSessionNativeInterface_GetTrackingQuality_m3473680033(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Single UnityEngine.XR.iOS.UnityARSessionNativeInterface::GetARYUVTexCoordScale()
extern "C"  float UnityARSessionNativeInterface_GetARYUVTexCoordScale_m1823875812 (UnityARSessionNativeInterface_t1130867170 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARSessionNativeInterface_GetARYUVTexCoordScale_m1823875812_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		float L_0 = UnityARSessionNativeInterface_GetYUVTexCoordScale_m671637809(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface::.cctor()
extern "C"  void UnityARSessionNativeInterface__cctor_m638256452 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		return;
	}
}
extern "C"  void DelegatePInvokeWrapper_ARAnchorAdded_t2646854145 (ARAnchorAdded_t2646854145 * __this, ARPlaneAnchor_t1439520888  ___anchorData0, const RuntimeMethod* method)
{


	typedef void (STDCALL *PInvokeFunc)(ARPlaneAnchor_t1439520888_marshaled_pinvoke);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((Il2CppDelegate*)__this)->method));

	// Marshaling of parameter '___anchorData0' to native representation
	ARPlaneAnchor_t1439520888_marshaled_pinvoke ____anchorData0_marshaled = {};
	ARPlaneAnchor_t1439520888_marshal_pinvoke(___anchorData0, ____anchorData0_marshaled);

	// Native function invocation
	il2cppPInvokeFunc(____anchorData0_marshaled);

	// Marshaling cleanup of parameter '___anchorData0' native representation
	ARPlaneAnchor_t1439520888_marshal_pinvoke_cleanup(____anchorData0_marshaled);

}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorAdded::.ctor(System.Object,System.IntPtr)
extern "C"  void ARAnchorAdded__ctor_m3844186700 (ARAnchorAdded_t2646854145 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorAdded::Invoke(UnityEngine.XR.iOS.ARPlaneAnchor)
extern "C"  void ARAnchorAdded_Invoke_m3296517664 (ARAnchorAdded_t2646854145 * __this, ARPlaneAnchor_t1439520888  ___anchorData0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ARAnchorAdded_Invoke_m3296517664((ARAnchorAdded_t2646854145 *)__this->get_prev_9(),___anchorData0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, ARPlaneAnchor_t1439520888  ___anchorData0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___anchorData0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, ARPlaneAnchor_t1439520888  ___anchorData0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___anchorData0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorAdded::BeginInvoke(UnityEngine.XR.iOS.ARPlaneAnchor,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* ARAnchorAdded_BeginInvoke_m1705105833 (ARAnchorAdded_t2646854145 * __this, ARPlaneAnchor_t1439520888  ___anchorData0, AsyncCallback_t163412349 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARAnchorAdded_BeginInvoke_m1705105833_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(ARPlaneAnchor_t1439520888_il2cpp_TypeInfo_var, &___anchorData0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorAdded::EndInvoke(System.IAsyncResult)
extern "C"  void ARAnchorAdded_EndInvoke_m3547789254 (ARAnchorAdded_t2646854145 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C"  void DelegatePInvokeWrapper_ARAnchorRemoved_t142665927 (ARAnchorRemoved_t142665927 * __this, ARPlaneAnchor_t1439520888  ___anchorData0, const RuntimeMethod* method)
{


	typedef void (STDCALL *PInvokeFunc)(ARPlaneAnchor_t1439520888_marshaled_pinvoke);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((Il2CppDelegate*)__this)->method));

	// Marshaling of parameter '___anchorData0' to native representation
	ARPlaneAnchor_t1439520888_marshaled_pinvoke ____anchorData0_marshaled = {};
	ARPlaneAnchor_t1439520888_marshal_pinvoke(___anchorData0, ____anchorData0_marshaled);

	// Native function invocation
	il2cppPInvokeFunc(____anchorData0_marshaled);

	// Marshaling cleanup of parameter '___anchorData0' native representation
	ARPlaneAnchor_t1439520888_marshal_pinvoke_cleanup(____anchorData0_marshaled);

}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorRemoved::.ctor(System.Object,System.IntPtr)
extern "C"  void ARAnchorRemoved__ctor_m535635486 (ARAnchorRemoved_t142665927 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorRemoved::Invoke(UnityEngine.XR.iOS.ARPlaneAnchor)
extern "C"  void ARAnchorRemoved_Invoke_m4120555414 (ARAnchorRemoved_t142665927 * __this, ARPlaneAnchor_t1439520888  ___anchorData0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ARAnchorRemoved_Invoke_m4120555414((ARAnchorRemoved_t142665927 *)__this->get_prev_9(),___anchorData0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, ARPlaneAnchor_t1439520888  ___anchorData0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___anchorData0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, ARPlaneAnchor_t1439520888  ___anchorData0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___anchorData0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorRemoved::BeginInvoke(UnityEngine.XR.iOS.ARPlaneAnchor,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* ARAnchorRemoved_BeginInvoke_m810761227 (ARAnchorRemoved_t142665927 * __this, ARPlaneAnchor_t1439520888  ___anchorData0, AsyncCallback_t163412349 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARAnchorRemoved_BeginInvoke_m810761227_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(ARPlaneAnchor_t1439520888_il2cpp_TypeInfo_var, &___anchorData0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorRemoved::EndInvoke(System.IAsyncResult)
extern "C"  void ARAnchorRemoved_EndInvoke_m341407908 (ARAnchorRemoved_t142665927 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C"  void DelegatePInvokeWrapper_ARAnchorUpdated_t3886071158 (ARAnchorUpdated_t3886071158 * __this, ARPlaneAnchor_t1439520888  ___anchorData0, const RuntimeMethod* method)
{


	typedef void (STDCALL *PInvokeFunc)(ARPlaneAnchor_t1439520888_marshaled_pinvoke);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((Il2CppDelegate*)__this)->method));

	// Marshaling of parameter '___anchorData0' to native representation
	ARPlaneAnchor_t1439520888_marshaled_pinvoke ____anchorData0_marshaled = {};
	ARPlaneAnchor_t1439520888_marshal_pinvoke(___anchorData0, ____anchorData0_marshaled);

	// Native function invocation
	il2cppPInvokeFunc(____anchorData0_marshaled);

	// Marshaling cleanup of parameter '___anchorData0' native representation
	ARPlaneAnchor_t1439520888_marshal_pinvoke_cleanup(____anchorData0_marshaled);

}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorUpdated::.ctor(System.Object,System.IntPtr)
extern "C"  void ARAnchorUpdated__ctor_m1158457407 (ARAnchorUpdated_t3886071158 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorUpdated::Invoke(UnityEngine.XR.iOS.ARPlaneAnchor)
extern "C"  void ARAnchorUpdated_Invoke_m1018775699 (ARAnchorUpdated_t3886071158 * __this, ARPlaneAnchor_t1439520888  ___anchorData0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ARAnchorUpdated_Invoke_m1018775699((ARAnchorUpdated_t3886071158 *)__this->get_prev_9(),___anchorData0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, ARPlaneAnchor_t1439520888  ___anchorData0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___anchorData0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, ARPlaneAnchor_t1439520888  ___anchorData0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___anchorData0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorUpdated::BeginInvoke(UnityEngine.XR.iOS.ARPlaneAnchor,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* ARAnchorUpdated_BeginInvoke_m2718860606 (ARAnchorUpdated_t3886071158 * __this, ARPlaneAnchor_t1439520888  ___anchorData0, AsyncCallback_t163412349 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARAnchorUpdated_BeginInvoke_m2718860606_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(ARPlaneAnchor_t1439520888_il2cpp_TypeInfo_var, &___anchorData0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorUpdated::EndInvoke(System.IAsyncResult)
extern "C"  void ARAnchorUpdated_EndInvoke_m398669553 (ARAnchorUpdated_t3886071158 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARFrameUpdate::.ctor(System.Object,System.IntPtr)
extern "C"  void ARFrameUpdate__ctor_m1399217559 (ARFrameUpdate_t496507918 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARFrameUpdate::Invoke(UnityEngine.XR.iOS.UnityARCamera)
extern "C"  void ARFrameUpdate_Invoke_m3133737564 (ARFrameUpdate_t496507918 * __this, UnityARCamera_t4198559457  ___camera0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ARFrameUpdate_Invoke_m3133737564((ARFrameUpdate_t496507918 *)__this->get_prev_9(),___camera0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, UnityARCamera_t4198559457  ___camera0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___camera0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, UnityARCamera_t4198559457  ___camera0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___camera0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARFrameUpdate::BeginInvoke(UnityEngine.XR.iOS.UnityARCamera,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* ARFrameUpdate_BeginInvoke_m4062970305 (ARFrameUpdate_t496507918 * __this, UnityARCamera_t4198559457  ___camera0, AsyncCallback_t163412349 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ARFrameUpdate_BeginInvoke_m4062970305_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UnityARCamera_t4198559457_il2cpp_TypeInfo_var, &___camera0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARFrameUpdate::EndInvoke(System.IAsyncResult)
extern "C"  void ARFrameUpdate_EndInvoke_m2107935669 (ARFrameUpdate_t496507918 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C"  void DelegatePInvokeWrapper_ARSessionFailed_t872580813 (ARSessionFailed_t872580813 * __this, String_t* ___error0, const RuntimeMethod* method)
{
	typedef void (STDCALL *PInvokeFunc)(char*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((Il2CppDelegate*)__this)->method));

	// Marshaling of parameter '___error0' to native representation
	char* ____error0_marshaled = NULL;
	____error0_marshaled = il2cpp_codegen_marshal_string(___error0);

	// Native function invocation
	il2cppPInvokeFunc(____error0_marshaled);

	// Marshaling cleanup of parameter '___error0' native representation
	il2cpp_codegen_marshal_free(____error0_marshaled);
	____error0_marshaled = NULL;

}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARSessionFailed::.ctor(System.Object,System.IntPtr)
extern "C"  void ARSessionFailed__ctor_m3600321544 (ARSessionFailed_t872580813 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARSessionFailed::Invoke(System.String)
extern "C"  void ARSessionFailed_Invoke_m3664045560 (ARSessionFailed_t872580813 * __this, String_t* ___error0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ARSessionFailed_Invoke_m3664045560((ARSessionFailed_t872580813 *)__this->get_prev_9(),___error0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, String_t* ___error0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___error0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___error0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___error0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___error0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARSessionFailed::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* ARSessionFailed_BeginInvoke_m2937386027 (ARSessionFailed_t872580813 * __this, String_t* ___error0, AsyncCallback_t163412349 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___error0;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARSessionFailed::EndInvoke(System.IAsyncResult)
extern "C"  void ARSessionFailed_EndInvoke_m2375002614 (ARSessionFailed_t872580813 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C"  void DelegatePInvokeWrapper_internal_ARAnchorAdded_t1622117597 (internal_ARAnchorAdded_t1622117597 * __this, UnityARAnchorData_t2901866349  ___anchorData0, const RuntimeMethod* method)
{
	typedef void (STDCALL *PInvokeFunc)(UnityARAnchorData_t2901866349 );
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((Il2CppDelegate*)__this)->method));

	// Native function invocation
	il2cppPInvokeFunc(___anchorData0);

}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorAdded::.ctor(System.Object,System.IntPtr)
extern "C"  void internal_ARAnchorAdded__ctor_m875644652 (internal_ARAnchorAdded_t1622117597 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorAdded::Invoke(UnityEngine.XR.iOS.UnityARAnchorData)
extern "C"  void internal_ARAnchorAdded_Invoke_m4273916311 (internal_ARAnchorAdded_t1622117597 * __this, UnityARAnchorData_t2901866349  ___anchorData0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		internal_ARAnchorAdded_Invoke_m4273916311((internal_ARAnchorAdded_t1622117597 *)__this->get_prev_9(),___anchorData0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, UnityARAnchorData_t2901866349  ___anchorData0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___anchorData0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, UnityARAnchorData_t2901866349  ___anchorData0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___anchorData0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorAdded::BeginInvoke(UnityEngine.XR.iOS.UnityARAnchorData,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* internal_ARAnchorAdded_BeginInvoke_m105627114 (internal_ARAnchorAdded_t1622117597 * __this, UnityARAnchorData_t2901866349  ___anchorData0, AsyncCallback_t163412349 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (internal_ARAnchorAdded_BeginInvoke_m105627114_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UnityARAnchorData_t2901866349_il2cpp_TypeInfo_var, &___anchorData0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorAdded::EndInvoke(System.IAsyncResult)
extern "C"  void internal_ARAnchorAdded_EndInvoke_m2146016718 (internal_ARAnchorAdded_t1622117597 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C"  void DelegatePInvokeWrapper_internal_ARAnchorRemoved_t3189755211 (internal_ARAnchorRemoved_t3189755211 * __this, UnityARAnchorData_t2901866349  ___anchorData0, const RuntimeMethod* method)
{
	typedef void (STDCALL *PInvokeFunc)(UnityARAnchorData_t2901866349 );
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((Il2CppDelegate*)__this)->method));

	// Native function invocation
	il2cppPInvokeFunc(___anchorData0);

}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorRemoved::.ctor(System.Object,System.IntPtr)
extern "C"  void internal_ARAnchorRemoved__ctor_m2856503254 (internal_ARAnchorRemoved_t3189755211 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorRemoved::Invoke(UnityEngine.XR.iOS.UnityARAnchorData)
extern "C"  void internal_ARAnchorRemoved_Invoke_m3299031849 (internal_ARAnchorRemoved_t3189755211 * __this, UnityARAnchorData_t2901866349  ___anchorData0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		internal_ARAnchorRemoved_Invoke_m3299031849((internal_ARAnchorRemoved_t3189755211 *)__this->get_prev_9(),___anchorData0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, UnityARAnchorData_t2901866349  ___anchorData0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___anchorData0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, UnityARAnchorData_t2901866349  ___anchorData0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___anchorData0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorRemoved::BeginInvoke(UnityEngine.XR.iOS.UnityARAnchorData,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* internal_ARAnchorRemoved_BeginInvoke_m3654189460 (internal_ARAnchorRemoved_t3189755211 * __this, UnityARAnchorData_t2901866349  ___anchorData0, AsyncCallback_t163412349 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (internal_ARAnchorRemoved_BeginInvoke_m3654189460_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UnityARAnchorData_t2901866349_il2cpp_TypeInfo_var, &___anchorData0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorRemoved::EndInvoke(System.IAsyncResult)
extern "C"  void internal_ARAnchorRemoved_EndInvoke_m1183866612 (internal_ARAnchorRemoved_t3189755211 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C"  void DelegatePInvokeWrapper_internal_ARAnchorUpdated_t3705772742 (internal_ARAnchorUpdated_t3705772742 * __this, UnityARAnchorData_t2901866349  ___anchorData0, const RuntimeMethod* method)
{
	typedef void (STDCALL *PInvokeFunc)(UnityARAnchorData_t2901866349 );
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((Il2CppDelegate*)__this)->method));

	// Native function invocation
	il2cppPInvokeFunc(___anchorData0);

}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorUpdated::.ctor(System.Object,System.IntPtr)
extern "C"  void internal_ARAnchorUpdated__ctor_m2904213987 (internal_ARAnchorUpdated_t3705772742 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorUpdated::Invoke(UnityEngine.XR.iOS.UnityARAnchorData)
extern "C"  void internal_ARAnchorUpdated_Invoke_m609656910 (internal_ARAnchorUpdated_t3705772742 * __this, UnityARAnchorData_t2901866349  ___anchorData0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		internal_ARAnchorUpdated_Invoke_m609656910((internal_ARAnchorUpdated_t3705772742 *)__this->get_prev_9(),___anchorData0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, UnityARAnchorData_t2901866349  ___anchorData0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___anchorData0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, UnityARAnchorData_t2901866349  ___anchorData0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___anchorData0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorUpdated::BeginInvoke(UnityEngine.XR.iOS.UnityARAnchorData,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* internal_ARAnchorUpdated_BeginInvoke_m879269121 (internal_ARAnchorUpdated_t3705772742 * __this, UnityARAnchorData_t2901866349  ___anchorData0, AsyncCallback_t163412349 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (internal_ARAnchorUpdated_BeginInvoke_m879269121_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UnityARAnchorData_t2901866349_il2cpp_TypeInfo_var, &___anchorData0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorUpdated::EndInvoke(System.IAsyncResult)
extern "C"  void internal_ARAnchorUpdated_EndInvoke_m1726786221 (internal_ARAnchorUpdated_t3705772742 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C"  void DelegatePInvokeWrapper_internal_ARFrameUpdate_t3296518558 (internal_ARFrameUpdate_t3296518558 * __this, internal_UnityARCamera_t2580192745  ___camera0, const RuntimeMethod* method)
{
	typedef void (STDCALL *PInvokeFunc)(internal_UnityARCamera_t2580192745 );
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((Il2CppDelegate*)__this)->method));

	// Native function invocation
	il2cppPInvokeFunc(___camera0);

}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFrameUpdate::.ctor(System.Object,System.IntPtr)
extern "C"  void internal_ARFrameUpdate__ctor_m36448827 (internal_ARFrameUpdate_t3296518558 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFrameUpdate::Invoke(UnityEngine.XR.iOS.internal_UnityARCamera)
extern "C"  void internal_ARFrameUpdate_Invoke_m3624040046 (internal_ARFrameUpdate_t3296518558 * __this, internal_UnityARCamera_t2580192745  ___camera0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		internal_ARFrameUpdate_Invoke_m3624040046((internal_ARFrameUpdate_t3296518558 *)__this->get_prev_9(),___camera0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, internal_UnityARCamera_t2580192745  ___camera0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___camera0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, internal_UnityARCamera_t2580192745  ___camera0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___camera0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFrameUpdate::BeginInvoke(UnityEngine.XR.iOS.internal_UnityARCamera,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* internal_ARFrameUpdate_BeginInvoke_m2703452769 (internal_ARFrameUpdate_t3296518558 * __this, internal_UnityARCamera_t2580192745  ___camera0, AsyncCallback_t163412349 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (internal_ARFrameUpdate_BeginInvoke_m2703452769_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(internal_UnityARCamera_t2580192745_il2cpp_TypeInfo_var, &___camera0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFrameUpdate::EndInvoke(System.IAsyncResult)
extern "C"  void internal_ARFrameUpdate_EndInvoke_m3230055713 (internal_ARFrameUpdate_t3296518558 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.XR.iOS.UnityARUtility::.ctor()
extern "C"  void UnityARUtility__ctor_m783816525 (UnityARUtility_t3608388148 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARUtility::InitializePlanePrefab(UnityEngine.GameObject)
extern "C"  void UnityARUtility_InitializePlanePrefab_m2887188869 (RuntimeObject * __this /* static, unused */, GameObject_t1756533147 * ___go0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARUtility_InitializePlanePrefab_m2887188869_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = ___go0;
		IL2CPP_RUNTIME_CLASS_INIT(UnityARUtility_t3608388148_il2cpp_TypeInfo_var);
		((UnityARUtility_t3608388148_StaticFields*)il2cpp_codegen_static_fields_for(UnityARUtility_t3608388148_il2cpp_TypeInfo_var))->set_planePrefab_2(L_0);
		return;
	}
}
// UnityEngine.GameObject UnityEngine.XR.iOS.UnityARUtility::CreatePlaneInScene(UnityEngine.XR.iOS.ARPlaneAnchor)
extern "C"  GameObject_t1756533147 * UnityARUtility_CreatePlaneInScene_m836370693 (RuntimeObject * __this /* static, unused */, ARPlaneAnchor_t1439520888  ___arPlaneAnchor0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARUtility_CreatePlaneInScene_m836370693_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityARUtility_t3608388148_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_0 = ((UnityARUtility_t3608388148_StaticFields*)il2cpp_codegen_static_fields_for(UnityARUtility_t3608388148_il2cpp_TypeInfo_var))->get_planePrefab_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityARUtility_t3608388148_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_2 = ((UnityARUtility_t3608388148_StaticFields*)il2cpp_codegen_static_fields_for(UnityARUtility_t3608388148_il2cpp_TypeInfo_var))->get_planePrefab_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_3 = Object_Instantiate_TisGameObject_t1756533147_m3664764861(NULL /*static, unused*/, L_2, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3664764861_RuntimeMethod_var);
		V_0 = L_3;
		goto IL_0026;
	}

IL_0020:
	{
		GameObject_t1756533147 * L_4 = (GameObject_t1756533147 *)il2cpp_codegen_object_new(GameObject_t1756533147_il2cpp_TypeInfo_var);
		GameObject__ctor_m498247354(L_4, /*hidden argument*/NULL);
		V_0 = L_4;
	}

IL_0026:
	{
		GameObject_t1756533147 * L_5 = V_0;
		String_t* L_6 = (&___arPlaneAnchor0)->get_identifier_0();
		NullCheck(L_5);
		Object_set_name_m4157836998(L_5, L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_7 = V_0;
		ARPlaneAnchor_t1439520888  L_8 = ___arPlaneAnchor0;
		IL2CPP_RUNTIME_CLASS_INIT(UnityARUtility_t3608388148_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_9 = UnityARUtility_UpdatePlaneWithAnchorTransform_m639257622(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// UnityEngine.GameObject UnityEngine.XR.iOS.UnityARUtility::UpdatePlaneWithAnchorTransform(UnityEngine.GameObject,UnityEngine.XR.iOS.ARPlaneAnchor)
extern "C"  GameObject_t1756533147 * UnityARUtility_UpdatePlaneWithAnchorTransform_m639257622 (RuntimeObject * __this /* static, unused */, GameObject_t1756533147 * ___plane0, ARPlaneAnchor_t1439520888  ___arPlaneAnchor1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARUtility_UpdatePlaneWithAnchorTransform_m639257622_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MeshFilter_t3026937449 * V_0 = NULL;
	{
		GameObject_t1756533147 * L_0 = ___plane0;
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = GameObject_get_transform_m909382139(L_0, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_2 = (&___arPlaneAnchor1)->get_transform_1();
		Vector3_t2243707580  L_3 = UnityARMatrixOps_GetPosition_m1153858439(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_set_position_m2469242620(L_1, L_3, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_4 = ___plane0;
		NullCheck(L_4);
		Transform_t3275118058 * L_5 = GameObject_get_transform_m909382139(L_4, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_6 = (&___arPlaneAnchor1)->get_transform_1();
		Quaternion_t4030073918  L_7 = UnityARMatrixOps_GetRotation_m1002641986(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_set_rotation_m3411284563(L_5, L_7, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = ___plane0;
		NullCheck(L_8);
		MeshFilter_t3026937449 * L_9 = GameObject_GetComponentInChildren_TisMeshFilter_t3026937449_m1346841033(L_8, /*hidden argument*/GameObject_GetComponentInChildren_TisMeshFilter_t3026937449_m1346841033_RuntimeMethod_var);
		V_0 = L_9;
		MeshFilter_t3026937449 * L_10 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_11 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_10, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_00c6;
		}
	}
	{
		MeshFilter_t3026937449 * L_12 = V_0;
		NullCheck(L_12);
		GameObject_t1756533147 * L_13 = Component_get_gameObject_m3105766835(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		Transform_t3275118058 * L_14 = GameObject_get_transform_m909382139(L_13, /*hidden argument*/NULL);
		Vector3_t2243707580 * L_15 = (&___arPlaneAnchor1)->get_address_of_extent_4();
		float L_16 = L_15->get_x_1();
		Vector3_t2243707580 * L_17 = (&___arPlaneAnchor1)->get_address_of_extent_4();
		float L_18 = L_17->get_y_2();
		Vector3_t2243707580 * L_19 = (&___arPlaneAnchor1)->get_address_of_extent_4();
		float L_20 = L_19->get_z_3();
		Vector3_t2243707580  L_21;
		memset(&L_21, 0, sizeof(L_21));
		Vector3__ctor_m2638739322((&L_21), ((float)((float)L_16*(float)(0.1f))), ((float)((float)L_18*(float)(0.1f))), ((float)((float)L_20*(float)(0.1f))), /*hidden argument*/NULL);
		NullCheck(L_14);
		Transform_set_localScale_m2325460848(L_14, L_21, /*hidden argument*/NULL);
		MeshFilter_t3026937449 * L_22 = V_0;
		NullCheck(L_22);
		GameObject_t1756533147 * L_23 = Component_get_gameObject_m3105766835(L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		Transform_t3275118058 * L_24 = GameObject_get_transform_m909382139(L_23, /*hidden argument*/NULL);
		Vector3_t2243707580 * L_25 = (&___arPlaneAnchor1)->get_address_of_center_3();
		float L_26 = L_25->get_x_1();
		Vector3_t2243707580 * L_27 = (&___arPlaneAnchor1)->get_address_of_center_3();
		float L_28 = L_27->get_y_2();
		Vector3_t2243707580 * L_29 = (&___arPlaneAnchor1)->get_address_of_center_3();
		float L_30 = L_29->get_z_3();
		Vector3_t2243707580  L_31;
		memset(&L_31, 0, sizeof(L_31));
		Vector3__ctor_m2638739322((&L_31), L_26, L_28, ((-L_30)), /*hidden argument*/NULL);
		NullCheck(L_24);
		Transform_set_localPosition_m1026930133(L_24, L_31, /*hidden argument*/NULL);
	}

IL_00c6:
	{
		GameObject_t1756533147 * L_32 = ___plane0;
		return L_32;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARUtility::.cctor()
extern "C"  void UnityARUtility__cctor_m1506337406 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARVideo::.ctor()
extern "C"  void UnityARVideo__ctor_m55801374 (UnityARVideo_t2351297253 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARVideo::Start()
extern "C"  void UnityARVideo_Start_m474328190 (UnityARVideo_t2351297253 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARVideo_Start_m474328190_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		UnityARSessionNativeInterface_t1130867170 * L_0 = UnityARSessionNativeInterface_GetARSessionNativeInterface_m3174488657(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_m_Session_6(L_0);
		__this->set_bCommandBufferInitialized_7((bool)0);
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARVideo::InitializeCommandBuffer()
extern "C"  void UnityARVideo_InitializeCommandBuffer_m2800566831 (UnityARVideo_t2351297253 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARVideo_InitializeCommandBuffer_m2800566831_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		CommandBuffer_t1204166949 * L_0 = (CommandBuffer_t1204166949 *)il2cpp_codegen_object_new(CommandBuffer_t1204166949_il2cpp_TypeInfo_var);
		CommandBuffer__ctor_m3893953450(L_0, /*hidden argument*/NULL);
		__this->set_m_VideoCommandBuffer_3(L_0);
		CommandBuffer_t1204166949 * L_1 = __this->get_m_VideoCommandBuffer_3();
		RenderTargetIdentifier_t772440638  L_2 = RenderTargetIdentifier_op_Implicit_m1621446097(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		Material_t193706927 * L_3 = __this->get_m_ClearMaterial_2();
		NullCheck(L_1);
		CommandBuffer_Blit_m4282817560(L_1, (Texture_t2243626319 *)NULL, L_2, L_3, /*hidden argument*/NULL);
		Camera_t189460977 * L_4 = Component_GetComponent_TisCamera_t189460977_m3276577584(__this, /*hidden argument*/Component_GetComponent_TisCamera_t189460977_m3276577584_RuntimeMethod_var);
		CommandBuffer_t1204166949 * L_5 = __this->get_m_VideoCommandBuffer_3();
		NullCheck(L_4);
		Camera_AddCommandBuffer_m2569587168(L_4, ((int32_t)10), L_5, /*hidden argument*/NULL);
		__this->set_bCommandBufferInitialized_7((bool)1);
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARVideo::OnDestroy()
extern "C"  void UnityARVideo_OnDestroy_m4088618451 (UnityARVideo_t2351297253 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARVideo_OnDestroy_m4088618451_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Camera_t189460977 * L_0 = Component_GetComponent_TisCamera_t189460977_m3276577584(__this, /*hidden argument*/Component_GetComponent_TisCamera_t189460977_m3276577584_RuntimeMethod_var);
		CommandBuffer_t1204166949 * L_1 = __this->get_m_VideoCommandBuffer_3();
		NullCheck(L_0);
		Camera_RemoveCommandBuffer_m2103408695(L_0, ((int32_t)10), L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.XR.iOS.UnityARVideo::OnPreRender()
extern "C"  void UnityARVideo_OnPreRender_m3078128848 (UnityARVideo_t2351297253 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityARVideo_OnPreRender_m3078128848_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ARTextureHandles_t3764914833  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Resolution_t3693662728  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t V_2 = 0;
	float V_3 = 0.0f;
	Matrix4x4_t2933234003  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		UnityARSessionNativeInterface_t1130867170 * L_0 = __this->get_m_Session_6();
		NullCheck(L_0);
		ARTextureHandles_t3764914833  L_1 = UnityARSessionNativeInterface_GetARVideoTextureHandles_m2905358883(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		IntPtr_t L_2 = (&V_0)->get_textureY_0();
		IntPtr_t L_3 = ((IntPtr_t_StaticFields*)il2cpp_codegen_static_fields_for(IntPtr_t_il2cpp_TypeInfo_var))->get_Zero_1();
		bool L_4 = IntPtr_op_Equality_m1573482188(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0038;
		}
	}
	{
		IntPtr_t L_5 = (&V_0)->get_textureCbCr_1();
		IntPtr_t L_6 = ((IntPtr_t_StaticFields*)il2cpp_codegen_static_fields_for(IntPtr_t_il2cpp_TypeInfo_var))->get_Zero_1();
		bool L_7 = IntPtr_op_Equality_m1573482188(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0039;
		}
	}

IL_0038:
	{
		return;
	}

IL_0039:
	{
		bool L_8 = __this->get_bCommandBufferInitialized_7();
		if (L_8)
		{
			goto IL_004a;
		}
	}
	{
		UnityARVideo_InitializeCommandBuffer_m2800566831(__this, /*hidden argument*/NULL);
	}

IL_004a:
	{
		Resolution_t3693662728  L_9 = Screen_get_currentResolution_m2361090437(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_9;
		int32_t L_10 = Resolution_get_width_m1438273472((&V_1), /*hidden argument*/NULL);
		int32_t L_11 = Resolution_get_height_m882683003((&V_1), /*hidden argument*/NULL);
		IntPtr_t L_12 = (&V_0)->get_textureY_0();
		Texture2D_t3542995729 * L_13 = Texture2D_CreateExternalTexture_m3402112250(NULL /*static, unused*/, L_10, L_11, ((int32_t)63), (bool)0, (bool)0, L_12, /*hidden argument*/NULL);
		__this->set__videoTextureY_4(L_13);
		Texture2D_t3542995729 * L_14 = __this->get__videoTextureY_4();
		NullCheck(L_14);
		Texture_set_filterMode_m3838996656(L_14, 1, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_15 = __this->get__videoTextureY_4();
		NullCheck(L_15);
		Texture_set_wrapMode_m333956747(L_15, 0, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_16 = __this->get__videoTextureY_4();
		IntPtr_t L_17 = (&V_0)->get_textureY_0();
		NullCheck(L_16);
		Texture2D_UpdateExternalTexture_m1701322565(L_16, L_17, /*hidden argument*/NULL);
		int32_t L_18 = Resolution_get_width_m1438273472((&V_1), /*hidden argument*/NULL);
		int32_t L_19 = Resolution_get_height_m882683003((&V_1), /*hidden argument*/NULL);
		IntPtr_t L_20 = (&V_0)->get_textureCbCr_1();
		Texture2D_t3542995729 * L_21 = Texture2D_CreateExternalTexture_m3402112250(NULL /*static, unused*/, L_18, L_19, ((int32_t)62), (bool)0, (bool)0, L_20, /*hidden argument*/NULL);
		__this->set__videoTextureCbCr_5(L_21);
		Texture2D_t3542995729 * L_22 = __this->get__videoTextureCbCr_5();
		NullCheck(L_22);
		Texture_set_filterMode_m3838996656(L_22, 1, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_23 = __this->get__videoTextureCbCr_5();
		NullCheck(L_23);
		Texture_set_wrapMode_m333956747(L_23, 0, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_24 = __this->get__videoTextureCbCr_5();
		IntPtr_t L_25 = (&V_0)->get_textureCbCr_1();
		NullCheck(L_24);
		Texture2D_UpdateExternalTexture_m1701322565(L_24, L_25, /*hidden argument*/NULL);
		Material_t193706927 * L_26 = __this->get_m_ClearMaterial_2();
		Texture2D_t3542995729 * L_27 = __this->get__videoTextureY_4();
		NullCheck(L_26);
		Material_SetTexture_m141095205(L_26, _stringLiteral1152424733, L_27, /*hidden argument*/NULL);
		Material_t193706927 * L_28 = __this->get_m_ClearMaterial_2();
		Texture2D_t3542995729 * L_29 = __this->get__videoTextureCbCr_5();
		NullCheck(L_28);
		Material_SetTexture_m141095205(L_28, _stringLiteral2391281062, L_29, /*hidden argument*/NULL);
		V_2 = 0;
		V_3 = (0.0f);
		int32_t L_30 = Screen_get_orientation_m879255848(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_30) == ((uint32_t)1))))
		{
			goto IL_0138;
		}
	}
	{
		V_3 = (-90.0f);
		V_2 = 1;
		goto IL_0161;
	}

IL_0138:
	{
		int32_t L_31 = Screen_get_orientation_m879255848(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_31) == ((uint32_t)2))))
		{
			goto IL_0150;
		}
	}
	{
		V_3 = (90.0f);
		V_2 = 1;
		goto IL_0161;
	}

IL_0150:
	{
		int32_t L_32 = Screen_get_orientation_m879255848(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_32) == ((uint32_t)4))))
		{
			goto IL_0161;
		}
	}
	{
		V_3 = (-180.0f);
	}

IL_0161:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2243707580_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_33 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_34 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t4030073918_il2cpp_TypeInfo_var);
		Quaternion_t4030073918  L_35 = Quaternion_Euler_m2887458175(NULL /*static, unused*/, (0.0f), (0.0f), L_34, /*hidden argument*/NULL);
		Vector3_t2243707580  L_36 = Vector3_get_one_m627547232(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Matrix4x4_t2933234003_il2cpp_TypeInfo_var);
		Matrix4x4_t2933234003  L_37 = Matrix4x4_TRS_m1913765359(NULL /*static, unused*/, L_33, L_35, L_36, /*hidden argument*/NULL);
		V_4 = L_37;
		Material_t193706927 * L_38 = __this->get_m_ClearMaterial_2();
		Matrix4x4_t2933234003  L_39 = V_4;
		NullCheck(L_38);
		Material_SetMatrix_m1387972957(L_38, _stringLiteral2946740916, L_39, /*hidden argument*/NULL);
		Material_t193706927 * L_40 = __this->get_m_ClearMaterial_2();
		UnityARSessionNativeInterface_t1130867170 * L_41 = __this->get_m_Session_6();
		NullCheck(L_41);
		float L_42 = UnityARSessionNativeInterface_GetARYUVTexCoordScale_m1823875812(L_41, /*hidden argument*/NULL);
		NullCheck(L_40);
		Material_SetFloat_m1926275467(L_40, _stringLiteral1387439463, L_42, /*hidden argument*/NULL);
		Material_t193706927 * L_43 = __this->get_m_ClearMaterial_2();
		int32_t L_44 = V_2;
		NullCheck(L_43);
		Material_SetInt_m522302436(L_43, _stringLiteral2454363760, L_44, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityPointCloudExample::.ctor()
extern "C"  void UnityPointCloudExample__ctor_m4118737579 (UnityPointCloudExample_t3196264220 * __this, const RuntimeMethod* method)
{
	{
		__this->set_numPointsToShow_2(((int32_t)100));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityPointCloudExample::Start()
extern "C"  void UnityPointCloudExample_Start_m3369543775 (UnityPointCloudExample_t3196264220 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityPointCloudExample_Start_m3369543775_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)UnityPointCloudExample_ARFrameUpdated_m3631038593_RuntimeMethod_var);
		ARFrameUpdate_t496507918 * L_1 = (ARFrameUpdate_t496507918 *)il2cpp_codegen_object_new(ARFrameUpdate_t496507918_il2cpp_TypeInfo_var);
		ARFrameUpdate__ctor_m1399217559(L_1, __this, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UnityARSessionNativeInterface_t1130867170_il2cpp_TypeInfo_var);
		UnityARSessionNativeInterface_add_ARFrameUpdatedEvent_m2850773202(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_2 = __this->get_PointCloudPrefab_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_2, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_005c;
		}
	}
	{
		List_1_t1125654279 * L_4 = (List_1_t1125654279 *)il2cpp_codegen_object_new(List_1_t1125654279_il2cpp_TypeInfo_var);
		List_1__ctor_m704351054(L_4, /*hidden argument*/List_1__ctor_m704351054_RuntimeMethod_var);
		__this->set_pointCloudObjects_4(L_4);
		V_0 = 0;
		goto IL_004e;
	}

IL_0034:
	{
		List_1_t1125654279 * L_5 = __this->get_pointCloudObjects_4();
		GameObject_t1756533147 * L_6 = __this->get_PointCloudPrefab_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_7 = Object_Instantiate_TisGameObject_t1756533147_m3664764861(NULL /*static, unused*/, L_6, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3664764861_RuntimeMethod_var);
		NullCheck(L_5);
		List_1_Add_m3441471442(L_5, L_7, /*hidden argument*/List_1_Add_m3441471442_RuntimeMethod_var);
		int32_t L_8 = V_0;
		V_0 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_004e:
	{
		int32_t L_9 = V_0;
		uint32_t L_10 = __this->get_numPointsToShow_2();
		if ((((int64_t)(((int64_t)((int64_t)L_9)))) < ((int64_t)(((int64_t)((uint64_t)L_10))))))
		{
			goto IL_0034;
		}
	}

IL_005c:
	{
		return;
	}
}
// System.Void UnityPointCloudExample::ARFrameUpdated(UnityEngine.XR.iOS.UnityARCamera)
extern "C"  void UnityPointCloudExample_ARFrameUpdated_m3631038593 (UnityPointCloudExample_t3196264220 * __this, UnityARCamera_t4198559457  ___camera0, const RuntimeMethod* method)
{
	{
		Vector3U5BU5D_t1172311765* L_0 = (&___camera0)->get_pointCloudData_4();
		__this->set_m_PointCloudData_5(L_0);
		return;
	}
}
// System.Void UnityPointCloudExample::Update()
extern "C"  void UnityPointCloudExample_Update_m1416789468 (UnityPointCloudExample_t3196264220 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityPointCloudExample_Update_m1416789468_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Vector4_t2243707581  V_1;
	memset(&V_1, 0, sizeof(V_1));
	GameObject_t1756533147 * V_2 = NULL;
	{
		GameObject_t1756533147 * L_0 = __this->get_PointCloudPrefab_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_008c;
		}
	}
	{
		Vector3U5BU5D_t1172311765* L_2 = __this->get_m_PointCloudData_5();
		if (!L_2)
		{
			goto IL_008c;
		}
	}
	{
		V_0 = 0;
		goto IL_0070;
	}

IL_0023:
	{
		Vector3U5BU5D_t1172311765* L_3 = __this->get_m_PointCloudData_5();
		int32_t L_4 = V_0;
		NullCheck(L_3);
		IL2CPP_RUNTIME_CLASS_INIT(Vector4_t2243707581_il2cpp_TypeInfo_var);
		Vector4_t2243707581  L_5 = Vector4_op_Implicit_m1059320239(NULL /*static, unused*/, (*(Vector3_t2243707580 *)((L_3)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_4)))), /*hidden argument*/NULL);
		V_1 = L_5;
		List_1_t1125654279 * L_6 = __this->get_pointCloudObjects_4();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		GameObject_t1756533147 * L_8 = List_1_get_Item_m939767277(L_6, L_7, /*hidden argument*/List_1_get_Item_m939767277_RuntimeMethod_var);
		V_2 = L_8;
		GameObject_t1756533147 * L_9 = V_2;
		NullCheck(L_9);
		Transform_t3275118058 * L_10 = GameObject_get_transform_m909382139(L_9, /*hidden argument*/NULL);
		float L_11 = (&V_1)->get_x_1();
		float L_12 = (&V_1)->get_y_2();
		float L_13 = (&V_1)->get_z_3();
		Vector3_t2243707580  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Vector3__ctor_m2638739322((&L_14), L_11, L_12, L_13, /*hidden argument*/NULL);
		NullCheck(L_10);
		Transform_set_position_m2469242620(L_10, L_14, /*hidden argument*/NULL);
		int32_t L_15 = V_0;
		V_0 = ((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_0070:
	{
		int32_t L_16 = V_0;
		Vector3U5BU5D_t1172311765* L_17 = __this->get_m_PointCloudData_5();
		NullCheck(L_17);
		uint32_t L_18 = __this->get_numPointsToShow_2();
		int64_t L_19 = Math_Min_m301707792(NULL /*static, unused*/, (((int64_t)((int64_t)(((int32_t)((int32_t)(((RuntimeArray *)L_17)->max_length))))))), (((int64_t)((uint64_t)L_18))), /*hidden argument*/NULL);
		if ((((int64_t)(((int64_t)((int64_t)L_16)))) < ((int64_t)L_19)))
		{
			goto IL_0023;
		}
	}

IL_008c:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
