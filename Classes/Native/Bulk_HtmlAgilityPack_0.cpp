﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// HtmlAgilityPack.Crc32
struct Crc32_t83196531;
// HtmlAgilityPack.EncodingFoundException
struct EncodingFoundException_t1471015948;
// System.Text.Encoding
struct Encoding_t663144255;
// System.Exception
struct Exception_t1927440687;
// HtmlAgilityPack.HtmlAttribute
struct HtmlAttribute_t1804523403;
// HtmlAgilityPack.HtmlDocument
struct HtmlDocument_t556432108;
// System.String
struct String_t;
// System.ArgumentNullException
struct ArgumentNullException_t628810857;
// System.ArgumentException
struct ArgumentException_t3259014390;
// HtmlAgilityPack.HtmlAttributeCollection
struct HtmlAttributeCollection_t1787476631;
// HtmlAgilityPack.HtmlNode
struct HtmlNode_t2048434459;
// System.Collections.Generic.Dictionary`2<System.String,HtmlAgilityPack.HtmlAttribute>
struct Dictionary_2_t3719302665;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t2281509423;
// System.Collections.Generic.List`1<HtmlAgilityPack.HtmlAttribute>
struct List_1_t1173644535;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;
// HtmlAgilityPack.HtmlAttribute[]
struct HtmlAttributeU5BU5D_t425089514;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Collections.Generic.IEnumerator`1<HtmlAgilityPack.HtmlAttribute>
struct IEnumerator_1_t3575014526;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// HtmlAgilityPack.HtmlCommentNode
struct HtmlCommentNode_t1992371332;
// System.Collections.Generic.Dictionary`2<System.String,HtmlAgilityPack.HtmlNode>
struct Dictionary_2_t3963213721;
// System.Collections.Generic.List`1<HtmlAgilityPack.HtmlParseError>
struct List_1_t484300294;
// System.Text.RegularExpressions.Regex
struct Regex_t1803876613;
// System.IO.TextReader
struct TextReader_t1561828458;
// System.Collections.Generic.Dictionary`2<System.Int32,HtmlAgilityPack.HtmlNode>
struct Dictionary_2_t1056260094;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Object>
struct Dictionary_2_t1697274930;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,HtmlAgilityPack.HtmlNode>
struct ValueCollection_t4054287233;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Object>
struct ValueCollection_t400334773;
// System.IO.StringReader
struct StringReader_t1480123486;
// HtmlAgilityPack.HtmlTextNode
struct HtmlTextNode_t2710098554;
// HtmlAgilityPack.HtmlParseError
struct HtmlParseError_t1115179162;
// System.Collections.Generic.Stack`1<HtmlAgilityPack.HtmlNode>
struct Stack_1_t3136162613;
// System.Collections.Generic.Stack`1<System.Object>
struct Stack_1_t3777177449;
// System.String[]
struct StringU5BU5D_t1642385972;
// HtmlAgilityPack.HtmlNameTable
struct HtmlNameTable_t3848610378;
// System.Xml.NameTable
struct NameTable_t594386929;
// System.Xml.XmlNameTable
struct XmlNameTable_t1345805268;
// HtmlAgilityPack.HtmlNodeCollection
struct HtmlNodeCollection_t2542734491;
// HtmlAgilityPack.HtmlNodeNavigator
struct HtmlNodeNavigator_t2752606360;
// System.Collections.Generic.Dictionary`2<System.String,HtmlAgilityPack.HtmlElementFlag>
struct Dictionary_2_t2175053619;
// System.Collections.Generic.Dictionary`2<System.Object,HtmlAgilityPack.HtmlElementFlag>
struct Dictionary_2_t4147301781;
// System.IO.TextWriter
struct TextWriter_t4027217640;
// System.IO.StringWriter
struct StringWriter_t4139609088;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,HtmlAgilityPack.HtmlAttribute>
struct ValueCollection_t2422362508;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>
struct ValueCollection_t984569266;
// System.Collections.Generic.List`1<HtmlAgilityPack.HtmlNode>
struct List_1_t1417555591;
// System.ArgumentOutOfRangeException
struct ArgumentOutOfRangeException_t279959794;
// HtmlAgilityPack.HtmlNode[]
struct HtmlNodeU5BU5D_t1365436442;
// System.Collections.Generic.IEnumerator`1<HtmlAgilityPack.HtmlNode>
struct IEnumerator_1_t3818925582;
// System.InvalidProgramException
struct InvalidProgramException_t3776992292;
// System.Xml.XPath.XPathNavigator
struct XPathNavigator_t3981235968;
// System.NotImplementedException
struct NotImplementedException_t2785117854;
// HtmlAgilityPack.NameValuePairList
struct NameValuePairList_t1351137418;
// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>
struct List_1_t1070465849;
// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct List_1_t3702943073;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>>
struct Dictionary_2_t2985245111;
// System.IFormatProvider
struct IFormatProvider_t2849799027;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.Collections.Generic.Link[]
struct LinkU5BU5D_t62501539;
// HtmlAgilityPack.HtmlElementFlag[]
struct HtmlElementFlagU5BU5D_t614255832;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_t1241853011;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.Collections.Generic.Dictionary`2/Transform`1<System.String,HtmlAgilityPack.HtmlElementFlag,System.Collections.DictionaryEntry>
struct Transform_1_t4033062772;
// System.Collections.Generic.Dictionary`2/Transform`1<System.String,HtmlAgilityPack.HtmlAttribute,System.Collections.DictionaryEntry>
struct Transform_1_t3843896454;
// System.Collections.Generic.IEqualityComparer`1<System.Int32>
struct IEqualityComparer_1_t1284510226;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,HtmlAgilityPack.HtmlNode,System.Collections.DictionaryEntry>
struct Transform_1_t2049833757;
// HtmlAgilityPack.HtmlParseError[]
struct HtmlParseErrorU5BU5D_t3107688383;
// System.Collections.Generic.Dictionary`2/Transform`1<System.String,HtmlAgilityPack.HtmlNode,System.Collections.DictionaryEntry>
struct Transform_1_t489276086;
// System.UInt32[]
struct UInt32U5BU5D_t59386216;
// System.Text.DecoderFallback
struct DecoderFallback_t1715117820;
// System.Text.EncoderFallback
struct EncoderFallback_t1756452756;
// System.Reflection.Assembly
struct Assembly_t4268412390;
// System.Collections.Generic.KeyValuePair`2<System.String,System.String>[]
struct KeyValuePair_2U5BU5D_t1360691296;
// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>[]
struct List_1U5BU5D_t1425068996;
// System.Collections.Generic.Dictionary`2/Transform`1<System.String,System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>,System.Collections.DictionaryEntry>
struct Transform_1_t548908640;
// System.IntPtr[]
struct IntPtrU5BU5D_t169632028;
// System.Collections.IDictionary
struct IDictionary_t596158605;
// System.Xml.NameTable/Entry[]
struct EntryU5BU5D_t180042139;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;
// System.Void
struct Void_t1841601450;
// System.Byte
struct Byte_t3683104436;
// System.Double
struct Double_t4078015681;
// System.UInt16
struct UInt16_t986882611;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Text.Decoder
struct Decoder_t3792697818;
// System.IO.Stream
struct Stream_t3255436806;
// System.Text.RegularExpressions.FactoryCache
struct FactoryCache_t2051534610;
// System.Text.RegularExpressions.IMachineFactory
struct IMachineFactory_t633643314;

extern RuntimeClass* Crc32_t83196531_il2cpp_TypeInfo_var;
extern const uint32_t Crc32_AddToCRC32_m3943154140_MetadataUsageId;
extern const uint32_t Crc32_UPDC32_m1407286487_MetadataUsageId;
extern RuntimeClass* UInt32U5BU5D_t59386216_il2cpp_TypeInfo_var;
extern RuntimeField* U3CPrivateImplementationDetailsU3EU7B83185D3BU2D3939U2D439CU2DA54FU2D260F9279D9C8U7D_t691081255____U24U24method0x6000287U2D1_1_FieldInfo_var;
extern const uint32_t Crc32__cctor_m518075841_MetadataUsageId;
extern RuntimeClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1803325615;
extern const uint32_t HtmlAttribute_set_Name_m1231032678_MetadataUsageId;
extern RuntimeClass* HtmlDocument_t556432108_il2cpp_TypeInfo_var;
extern const uint32_t HtmlAttribute_get_XmlName_m2621022904_MetadataUsageId;
extern RuntimeClass* HtmlAttribute_t1804523403_il2cpp_TypeInfo_var;
extern RuntimeClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1099314147;
extern const uint32_t HtmlAttribute_CompareTo_m486975030_MetadataUsageId;
extern const uint32_t HtmlAttribute_Clone_m1011466360_MetadataUsageId;
extern RuntimeClass* Dictionary_2_t3719302665_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t1173644535_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Dictionary_2__ctor_m1450246241_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m2829932878_RuntimeMethod_var;
extern const uint32_t HtmlAttributeCollection__ctor_m1353566068_MetadataUsageId;
extern const RuntimeMethod* Dictionary_2_TryGetValue_m2659214584_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral2328218955;
extern const uint32_t HtmlAttributeCollection_get_Item_m3848708643_MetadataUsageId;
extern const RuntimeMethod* List_1_get_Count_m1117934718_RuntimeMethod_var;
extern const uint32_t HtmlAttributeCollection_get_Count_m1097499500_MetadataUsageId;
extern const RuntimeMethod* List_1_get_Item_m2219204117_RuntimeMethod_var;
extern const uint32_t HtmlAttributeCollection_get_Item_m4090043702_MetadataUsageId;
extern const RuntimeMethod* List_1_set_Item_m1351812348_RuntimeMethod_var;
extern const uint32_t HtmlAttributeCollection_set_Item_m3012978195_MetadataUsageId;
extern const RuntimeMethod* List_1_Clear_m3486920423_RuntimeMethod_var;
extern const uint32_t HtmlAttributeCollection_System_Collections_Generic_ICollectionU3CHtmlAgilityPack_HtmlAttributeU3E_Clear_m2105867863_MetadataUsageId;
extern const RuntimeMethod* List_1_Contains_m3092939846_RuntimeMethod_var;
extern const uint32_t HtmlAttributeCollection_Contains_m2621677193_MetadataUsageId;
extern const RuntimeMethod* List_1_CopyTo_m2629566870_RuntimeMethod_var;
extern const uint32_t HtmlAttributeCollection_CopyTo_m3645938983_MetadataUsageId;
extern RuntimeClass* Enumerator_t708374209_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1_GetEnumerator_m4221754371_RuntimeMethod_var;
extern const uint32_t HtmlAttributeCollection_System_Collections_Generic_IEnumerableU3CHtmlAgilityPack_HtmlAttributeU3E_GetEnumerator_m3662386570_MetadataUsageId;
extern const uint32_t HtmlAttributeCollection_System_Collections_IEnumerable_GetEnumerator_m1208243079_MetadataUsageId;
extern const RuntimeMethod* List_1_IndexOf_m946964392_RuntimeMethod_var;
extern const uint32_t HtmlAttributeCollection_IndexOf_m2095767915_MetadataUsageId;
extern const RuntimeMethod* Dictionary_2_set_Item_m1171915658_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Insert_m341283965_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral3168545717;
extern const uint32_t HtmlAttributeCollection_Insert_m3852709710_MetadataUsageId;
extern const RuntimeMethod* List_1_Remove_m302429225_RuntimeMethod_var;
extern const uint32_t HtmlAttributeCollection_System_Collections_Generic_ICollectionU3CHtmlAgilityPack_HtmlAttributeU3E_Remove_m2989135424_MetadataUsageId;
extern const RuntimeMethod* Dictionary_2_Remove_m2474169460_RuntimeMethod_var;
extern const RuntimeMethod* List_1_RemoveAt_m680506192_RuntimeMethod_var;
extern const uint32_t HtmlAttributeCollection_RemoveAt_m2525279158_MetadataUsageId;
extern const RuntimeMethod* List_1_Add_m2897938714_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral3583247784;
extern const uint32_t HtmlAttributeCollection_Append_m138838623_MetadataUsageId;
extern const uint32_t HtmlAttributeCollection_Contains_m1548048803_MetadataUsageId;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t HtmlAttributeCollection_GetAttributeIndex_m1301279406_MetadataUsageId;
extern RuntimeClass* HtmlNode_t2048434459_il2cpp_TypeInfo_var;
extern const uint32_t HtmlCommentNode__ctor_m3831299973_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2972645825;
extern Il2CppCodeGenString* _stringLiteral1220271454;
extern const uint32_t HtmlCommentNode_get_OuterHtml_m868039327_MetadataUsageId;
extern RuntimeClass* Dictionary_2_t3963213721_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t484300294_il2cpp_TypeInfo_var;
extern RuntimeClass* Encoding_t663144255_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Dictionary_2__ctor_m850897185_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m3187661903_RuntimeMethod_var;
extern const uint32_t HtmlDocument__ctor_m2934097387_MetadataUsageId;
extern RuntimeClass* Char_t3454481338_il2cpp_TypeInfo_var;
extern RuntimeClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3231012720;
extern Il2CppCodeGenString* _stringLiteral372029427;
extern const uint32_t HtmlDocument_GetXmlName_m448645048_MetadataUsageId;
extern RuntimeClass* Regex_t1803876613_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral439662321;
extern Il2CppCodeGenString* _stringLiteral241646909;
extern Il2CppCodeGenString* _stringLiteral2206518777;
extern Il2CppCodeGenString* _stringLiteral372029330;
extern Il2CppCodeGenString* _stringLiteral352199793;
extern Il2CppCodeGenString* _stringLiteral372029332;
extern Il2CppCodeGenString* _stringLiteral1259525396;
extern Il2CppCodeGenString* _stringLiteral372029312;
extern Il2CppCodeGenString* _stringLiteral1693198914;
extern const uint32_t HtmlDocument_HtmlEncode_m2155231231_MetadataUsageId;
extern const uint32_t HtmlDocument_CreateAttribute_m674678432_MetadataUsageId;
extern const uint32_t HtmlDocument_CreateAttribute_m4240580628_MetadataUsageId;
extern RuntimeClass* Exception_t1927440687_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Dictionary_2_ContainsKey_m3415399212_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_get_Item_m3051461655_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral287061489;
extern const uint32_t HtmlDocument_GetElementbyId_m1591120990_MetadataUsageId;
extern RuntimeClass* Dictionary_2_t1056260094_il2cpp_TypeInfo_var;
extern RuntimeClass* StreamReader_t2360341767_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Dictionary_2__ctor_m3832368222_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_get_Values_m2956437804_RuntimeMethod_var;
extern const RuntimeMethod* ValueCollection_GetEnumerator_m2005408300_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m3107603046_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m3092086129_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m393799376_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_Clear_m3754984071_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral51169761;
extern Il2CppCodeGenString* _stringLiteral2008880196;
extern Il2CppCodeGenString* _stringLiteral929783012;
extern const uint32_t HtmlDocument_Load_m511797890_MetadataUsageId;
extern RuntimeClass* StringReader_t1480123486_il2cpp_TypeInfo_var;
extern RuntimeClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t HtmlDocument_LoadHtml_m4284235418_MetadataUsageId;
extern const uint32_t HtmlDocument_CreateAttribute_m1400299982_MetadataUsageId;
extern RuntimeClass* HtmlCommentNode_t1992371332_il2cpp_TypeInfo_var;
extern RuntimeClass* HtmlTextNode_t2710098554_il2cpp_TypeInfo_var;
extern const uint32_t HtmlDocument_CreateNode_m538784785_MetadataUsageId;
extern RuntimeClass* IEnumerable_1_t2340561504_il2cpp_TypeInfo_var;
extern RuntimeClass* IEnumerator_1_t3818925582_il2cpp_TypeInfo_var;
extern RuntimeClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3202301804;
extern const uint32_t HtmlDocument_GetXmlDeclaration_m3305216451_MetadataUsageId;
extern const RuntimeMethod* Dictionary_2_Remove_m4126380600_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_set_Item_m3521183286_RuntimeMethod_var;
extern const uint32_t HtmlDocument_SetIdForNode_m403125345_MetadataUsageId;
extern RuntimeClass* HtmlParseError_t1115179162_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1_Add_m3464026123_RuntimeMethod_var;
extern const uint32_t HtmlDocument_AddError_m2023821899_MetadataUsageId;
extern RuntimeClass* Stack_1_t3136162613_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Utilities_GetDictionaryValueOrNull_TisString_t_TisHtmlNode_t2048434459_m2137127104_RuntimeMethod_var;
extern const RuntimeMethod* Stack_1__ctor_m1173151588_RuntimeMethod_var;
extern const RuntimeMethod* Stack_1_Push_m4199651461_RuntimeMethod_var;
extern const RuntimeMethod* Stack_1_Pop_m1876234527_RuntimeMethod_var;
extern const RuntimeMethod* Stack_1_get_Count_m133251284_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral620822474;
extern Il2CppCodeGenString* _stringLiteral3776025430;
extern Il2CppCodeGenString* _stringLiteral2209855717;
extern const uint32_t HtmlDocument_CloseCurrentNode_m2823362304_MetadataUsageId;
extern const uint32_t HtmlDocument_FindResetterNode_m2182858297_MetadataUsageId;
extern const uint32_t HtmlDocument_FixNestedTag_m2725238597_MetadataUsageId;
extern RuntimeClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral334115653;
extern Il2CppCodeGenString* _stringLiteral381169828;
extern Il2CppCodeGenString* _stringLiteral1900199602;
extern Il2CppCodeGenString* _stringLiteral287061494;
extern Il2CppCodeGenString* _stringLiteral4225798429;
extern Il2CppCodeGenString* _stringLiteral593465182;
extern const uint32_t HtmlDocument_GetResetters_m3862259821_MetadataUsageId;
extern const RuntimeMethod* Dictionary_2_Clear_m3645269862_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral51790603;
extern const uint32_t HtmlDocument_Parse_m3513675482_MetadataUsageId;
extern const uint32_t HtmlDocument_PushNodeEnd_m1567272216_MetadataUsageId;
extern RuntimeClass* EncodingFoundException_t1471015948_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3732937281;
extern Il2CppCodeGenString* _stringLiteral2582021199;
extern Il2CppCodeGenString* _stringLiteral4165124290;
extern Il2CppCodeGenString* _stringLiteral1561970665;
extern Il2CppCodeGenString* _stringLiteral3651008258;
extern Il2CppCodeGenString* _stringLiteral627879171;
extern Il2CppCodeGenString* _stringLiteral506264602;
extern Il2CppCodeGenString* _stringLiteral3504293556;
extern Il2CppCodeGenString* _stringLiteral3303919300;
extern const uint32_t HtmlDocument_ReadDocumentEncoding_m3190387121_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1591976195;
extern Il2CppCodeGenString* _stringLiteral4276579882;
extern const uint32_t HtmlDocument__cctor_m716884824_MetadataUsageId;
extern RuntimeClass* NameTable_t594386929_il2cpp_TypeInfo_var;
extern const uint32_t HtmlNameTable__ctor_m4091390005_MetadataUsageId;
extern RuntimeClass* HtmlNodeCollection_t2542734491_il2cpp_TypeInfo_var;
extern RuntimeClass* HtmlNodeNavigator_t2752606360_il2cpp_TypeInfo_var;
extern const uint32_t HtmlNode_SelectNodes_m3648812346_MetadataUsageId;
extern RuntimeClass* Dictionary_2_t2175053619_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Dictionary_2__ctor_m3138784843_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_Add_m722374771_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral3582779276;
extern Il2CppCodeGenString* _stringLiteral2940645458;
extern Il2CppCodeGenString* _stringLiteral3665202948;
extern Il2CppCodeGenString* _stringLiteral1574591451;
extern Il2CppCodeGenString* _stringLiteral627234459;
extern Il2CppCodeGenString* _stringLiteral1239101558;
extern Il2CppCodeGenString* _stringLiteral2328218841;
extern Il2CppCodeGenString* _stringLiteral325350890;
extern Il2CppCodeGenString* _stringLiteral182431658;
extern Il2CppCodeGenString* _stringLiteral381169824;
extern Il2CppCodeGenString* _stringLiteral1502598556;
extern Il2CppCodeGenString* _stringLiteral2665398035;
extern Il2CppCodeGenString* _stringLiteral190184483;
extern Il2CppCodeGenString* _stringLiteral2724182727;
extern Il2CppCodeGenString* _stringLiteral3154582249;
extern Il2CppCodeGenString* _stringLiteral1099314403;
extern Il2CppCodeGenString* _stringLiteral798818612;
extern Il2CppCodeGenString* _stringLiteral529978982;
extern Il2CppCodeGenString* _stringLiteral2632234911;
extern Il2CppCodeGenString* _stringLiteral997663887;
extern Il2CppCodeGenString* _stringLiteral748179678;
extern Il2CppCodeGenString* _stringLiteral310459614;
extern Il2CppCodeGenString* _stringLiteral1770475252;
extern Il2CppCodeGenString* _stringLiteral3910960075;
extern Il2CppCodeGenString* _stringLiteral381169818;
extern Il2CppCodeGenString* _stringLiteral372029390;
extern const uint32_t HtmlNode__cctor_m4269639443_MetadataUsageId;
extern const RuntimeMethod* Dictionary_2_Add_m1845366966_RuntimeMethod_var;
extern const uint32_t HtmlNode__ctor_m2584575018_MetadataUsageId;
extern RuntimeClass* HtmlAttributeCollection_t1787476631_il2cpp_TypeInfo_var;
extern const uint32_t HtmlNode_get_Attributes_m2935417207_MetadataUsageId;
extern const uint32_t HtmlNode_get_ChildNodes_m1556126725_MetadataUsageId;
extern const uint32_t HtmlNode_get_InnerHtml_m590077535_MetadataUsageId;
extern const uint32_t HtmlNode_get_InnerText_m2107610091_MetadataUsageId;
extern const uint32_t HtmlNode_get_Name_m702134575_MetadataUsageId;
extern const uint32_t HtmlNode_get_OuterHtml_m1376347664_MetadataUsageId;
extern const RuntimeMethod* Dictionary_2_ContainsKey_m2772236242_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_get_Item_m1423585693_RuntimeMethod_var;
extern const uint32_t HtmlNode_CanOverlapElement_m1178344375_MetadataUsageId;
extern const uint32_t HtmlNode_IsCDataElement_m1574064561_MetadataUsageId;
extern const uint32_t HtmlNode_IsClosedElement_m2492183536_MetadataUsageId;
extern const uint32_t HtmlNode_IsEmptyElement_m2918938855_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3533616638;
extern const uint32_t HtmlNode_AppendChild_m2583227907_MetadataUsageId;
extern RuntimeClass* IEnumerable_1_t2096650448_il2cpp_TypeInfo_var;
extern RuntimeClass* IEnumerator_1_t3575014526_il2cpp_TypeInfo_var;
extern const uint32_t HtmlNode_CloneNode_m2027347983_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral4234618797;
extern const uint32_t HtmlNode_RemoveChild_m1945208939_MetadataUsageId;
extern const uint32_t HtmlNode_WriteContentTo_m83799726_MetadataUsageId;
extern RuntimeClass* StringWriter_t4139609088_il2cpp_TypeInfo_var;
extern const uint32_t HtmlNode_WriteContentTo_m2088782300_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral452181462;
extern Il2CppCodeGenString* _stringLiteral2722536972;
extern Il2CppCodeGenString* _stringLiteral3639978523;
extern Il2CppCodeGenString* _stringLiteral4162708532;
extern Il2CppCodeGenString* _stringLiteral2020566873;
extern Il2CppCodeGenString* _stringLiteral1346227060;
extern Il2CppCodeGenString* _stringLiteral3667687257;
extern Il2CppCodeGenString* _stringLiteral2633345324;
extern Il2CppCodeGenString* _stringLiteral1351854812;
extern Il2CppCodeGenString* _stringLiteral57471885;
extern Il2CppCodeGenString* _stringLiteral372029331;
extern Il2CppCodeGenString* _stringLiteral2073895217;
extern const uint32_t HtmlNode_WriteTo_m15807155_MetadataUsageId;
extern const uint32_t HtmlNode_WriteTo_m2203281867_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1214590000;
extern Il2CppCodeGenString* _stringLiteral3893525534;
extern const uint32_t HtmlNode_GetXmlComment_m2603063516_MetadataUsageId;
extern const RuntimeMethod* Dictionary_2_Remove_m1113698489_RuntimeMethod_var;
extern const uint32_t HtmlNode_CloseNode_m1160950766_MetadataUsageId;
extern const uint32_t HtmlNode_GetId_m3597009408_MetadataUsageId;
extern RuntimeField* U3CPrivateImplementationDetailsU3EU7B83185D3BU2D3939U2D439CU2DA54FU2D260F9279D9C8U7D_t691081255____U24U24method0x600012fU2D1_0_FieldInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029307;
extern Il2CppCodeGenString* _stringLiteral372029310;
extern Il2CppCodeGenString* _stringLiteral372029329;
extern const uint32_t HtmlNode_WriteAttribute_m3827491122_MetadataUsageId;
extern RuntimeClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Dictionary_2_get_Values_m1907592043_RuntimeMethod_var;
extern const RuntimeMethod* ValueCollection_GetEnumerator_m2331812445_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m4000267011_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m3599701188_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m1803819355_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral9172989;
extern Il2CppCodeGenString* _stringLiteral1029384328;
extern Il2CppCodeGenString* _stringLiteral4183177304;
extern const uint32_t HtmlNode_WriteAttributes_m2802695934_MetadataUsageId;
extern RuntimeClass* List_1_t1417555591_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1__ctor_m3864602216_RuntimeMethod_var;
extern const uint32_t HtmlNodeCollection__ctor_m429744290_MetadataUsageId;
extern RuntimeClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1414245146;
extern Il2CppCodeGenString* _stringLiteral3458327600;
extern Il2CppCodeGenString* _stringLiteral2190564348;
extern const uint32_t HtmlNodeCollection_get_Item_m1795494390_MetadataUsageId;
extern const RuntimeMethod* List_1_get_Count_m3121849784_RuntimeMethod_var;
extern const uint32_t HtmlNodeCollection_get_Count_m3249441714_MetadataUsageId;
extern const RuntimeMethod* List_1_get_Item_m3127542065_RuntimeMethod_var;
extern const uint32_t HtmlNodeCollection_get_Item_m2496957206_MetadataUsageId;
extern const RuntimeMethod* List_1_set_Item_m1335896754_RuntimeMethod_var;
extern const uint32_t HtmlNodeCollection_set_Item_m981694791_MetadataUsageId;
extern const RuntimeMethod* List_1_Add_m2851003676_RuntimeMethod_var;
extern const uint32_t HtmlNodeCollection_Add_m2652620775_MetadataUsageId;
extern const RuntimeMethod* List_1_GetEnumerator_m2960830019_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m1132692067_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m2573550943_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m850670244_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Clear_m1310845251_RuntimeMethod_var;
extern const uint32_t HtmlNodeCollection_Clear_m3979270339_MetadataUsageId;
extern const RuntimeMethod* List_1_Contains_m530296848_RuntimeMethod_var;
extern const uint32_t HtmlNodeCollection_Contains_m984356797_MetadataUsageId;
extern const RuntimeMethod* List_1_CopyTo_m3109553416_RuntimeMethod_var;
extern const uint32_t HtmlNodeCollection_CopyTo_m3586893767_MetadataUsageId;
extern RuntimeClass* Enumerator_t952285265_il2cpp_TypeInfo_var;
extern const uint32_t HtmlNodeCollection_System_Collections_Generic_IEnumerableU3CHtmlAgilityPack_HtmlNodeU3E_GetEnumerator_m2789742012_MetadataUsageId;
extern const uint32_t HtmlNodeCollection_System_Collections_IEnumerable_GetEnumerator_m1953627531_MetadataUsageId;
extern const RuntimeMethod* List_1_IndexOf_m1714659438_RuntimeMethod_var;
extern const uint32_t HtmlNodeCollection_IndexOf_m2163239543_MetadataUsageId;
extern RuntimeClass* InvalidProgramException_t3776992292_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1_Insert_m1073570933_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral757822431;
extern const uint32_t HtmlNodeCollection_Insert_m1187451694_MetadataUsageId;
extern const uint32_t HtmlNodeCollection_Remove_m4176503848_MetadataUsageId;
extern const RuntimeMethod* List_1_RemoveAt_m1889740798_RuntimeMethod_var;
extern const uint32_t HtmlNodeCollection_RemoveAt_m3305657552_MetadataUsageId;
extern const uint32_t HtmlNodeCollection_Append_m717126292_MetadataUsageId;
extern const uint32_t HtmlNodeCollection_GetNodeIndex_m1699874364_MetadataUsageId;
extern RuntimeClass* HtmlNameTable_t3848610378_il2cpp_TypeInfo_var;
extern RuntimeClass* XPathNavigator_t3981235968_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4293744237;
extern const uint32_t HtmlNodeNavigator__ctor_m1439946702_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral696029617;
extern const uint32_t HtmlNodeNavigator__ctor_m2984146728_MetadataUsageId;
extern const uint32_t HtmlNodeNavigator_get_NamespaceURI_m1178786970_MetadataUsageId;
extern RuntimeClass* HtmlNodeType_t1941241835_il2cpp_TypeInfo_var;
extern RuntimeClass* NotImplementedException_t2785117854_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3216010707;
extern const uint32_t HtmlNodeNavigator_get_NodeType_m3863942019_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral371857150;
extern const uint32_t HtmlNodeNavigator_get_Value_m1270508592_MetadataUsageId;
extern const uint32_t HtmlNodeNavigator_get_XmlLang_m1732116456_MetadataUsageId;
extern const uint32_t HtmlNodeNavigator_Clone_m2948743814_MetadataUsageId;
extern const uint32_t HtmlNodeNavigator_IsSamePosition_m26408627_MetadataUsageId;
extern const uint32_t HtmlNodeNavigator_MoveTo_m3713757330_MetadataUsageId;
extern const uint32_t HtmlTextNode__ctor_m3649714099_MetadataUsageId;
extern RuntimeClass* List_1_t1070465849_il2cpp_TypeInfo_var;
extern RuntimeClass* Dictionary_2_t2985245111_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1__ctor_m973626735_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2__ctor_m3512899986_RuntimeMethod_var;
extern const uint32_t NameValuePairList__ctor_m1093178387_MetadataUsageId;
extern RuntimeClass* NameValuePairList_t1351137418_il2cpp_TypeInfo_var;
extern const uint32_t NameValuePairList_GetNameValuePairsValue_m1108671514_MetadataUsageId;
extern const RuntimeMethod* Dictionary_2_ContainsKey_m838899279_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_get_Item_m441479114_RuntimeMethod_var;
extern const uint32_t NameValuePairList_GetNameValuePairs_m1788854520_MetadataUsageId;
extern const RuntimeMethod* List_1_get_Count_m256450699_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_m3174779882_RuntimeMethod_var;
extern const RuntimeMethod* KeyValuePair_2_get_Value_m1710042386_RuntimeMethod_var;
extern const uint32_t NameValuePairList_GetNameValuePairValue_m2853234185_MetadataUsageId;
extern const RuntimeMethod* List_1_Clear_m3390312458_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_Clear_m1802158027_RuntimeMethod_var;
extern const RuntimeMethod* KeyValuePair_2__ctor_m2274627104_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m1754637603_RuntimeMethod_var;
extern const RuntimeMethod* KeyValuePair_2_get_Key_m1372024679_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_set_Item_m764567519_RuntimeMethod_var;
extern const uint32_t NameValuePairList_Parse_m4220937304_MetadataUsageId;

struct UInt32U5BU5D_t59386216;
struct HtmlAttributeU5BU5D_t425089514;
struct ByteU5BU5D_t3397334013;
struct CharU5BU5D_t1328083999;
struct StringU5BU5D_t1642385972;
struct HtmlNodeU5BU5D_t1365436442;


#ifndef U3CMODULEU3E_T3783534222_H
#define U3CMODULEU3E_T3783534222_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t3783534222 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T3783534222_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef LIST_1_T1173644535_H
#define LIST_1_T1173644535_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<HtmlAgilityPack.HtmlAttribute>
struct  List_1_t1173644535  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	HtmlAttributeU5BU5D_t425089514* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t1173644535, ____items_1)); }
	inline HtmlAttributeU5BU5D_t425089514* get__items_1() const { return ____items_1; }
	inline HtmlAttributeU5BU5D_t425089514** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(HtmlAttributeU5BU5D_t425089514* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t1173644535, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t1173644535, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t1173644535_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	HtmlAttributeU5BU5D_t425089514* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t1173644535_StaticFields, ___EmptyArray_4)); }
	inline HtmlAttributeU5BU5D_t425089514* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline HtmlAttributeU5BU5D_t425089514** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(HtmlAttributeU5BU5D_t425089514* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T1173644535_H
#ifndef TEXTWRITER_T4027217640_H
#define TEXTWRITER_T4027217640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.TextWriter
struct  TextWriter_t4027217640  : public RuntimeObject
{
public:
	// System.Char[] System.IO.TextWriter::CoreNewLine
	CharU5BU5D_t1328083999* ___CoreNewLine_0;
	// System.IFormatProvider System.IO.TextWriter::internalFormatProvider
	RuntimeObject* ___internalFormatProvider_1;

public:
	inline static int32_t get_offset_of_CoreNewLine_0() { return static_cast<int32_t>(offsetof(TextWriter_t4027217640, ___CoreNewLine_0)); }
	inline CharU5BU5D_t1328083999* get_CoreNewLine_0() const { return ___CoreNewLine_0; }
	inline CharU5BU5D_t1328083999** get_address_of_CoreNewLine_0() { return &___CoreNewLine_0; }
	inline void set_CoreNewLine_0(CharU5BU5D_t1328083999* value)
	{
		___CoreNewLine_0 = value;
		Il2CppCodeGenWriteBarrier((&___CoreNewLine_0), value);
	}

	inline static int32_t get_offset_of_internalFormatProvider_1() { return static_cast<int32_t>(offsetof(TextWriter_t4027217640, ___internalFormatProvider_1)); }
	inline RuntimeObject* get_internalFormatProvider_1() const { return ___internalFormatProvider_1; }
	inline RuntimeObject** get_address_of_internalFormatProvider_1() { return &___internalFormatProvider_1; }
	inline void set_internalFormatProvider_1(RuntimeObject* value)
	{
		___internalFormatProvider_1 = value;
		Il2CppCodeGenWriteBarrier((&___internalFormatProvider_1), value);
	}
};

struct TextWriter_t4027217640_StaticFields
{
public:
	// System.IO.TextWriter System.IO.TextWriter::Null
	TextWriter_t4027217640 * ___Null_2;

public:
	inline static int32_t get_offset_of_Null_2() { return static_cast<int32_t>(offsetof(TextWriter_t4027217640_StaticFields, ___Null_2)); }
	inline TextWriter_t4027217640 * get_Null_2() const { return ___Null_2; }
	inline TextWriter_t4027217640 ** get_address_of_Null_2() { return &___Null_2; }
	inline void set_Null_2(TextWriter_t4027217640 * value)
	{
		___Null_2 = value;
		Il2CppCodeGenWriteBarrier((&___Null_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTWRITER_T4027217640_H
#ifndef DICTIONARY_2_T2175053619_H
#define DICTIONARY_2_T2175053619_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.String,HtmlAgilityPack.HtmlElementFlag>
struct  Dictionary_2_t2175053619  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t3030399641* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t62501539* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	StringU5BU5D_t1642385972* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	HtmlElementFlagU5BU5D_t614255832* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t228987430 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t2175053619, ___table_4)); }
	inline Int32U5BU5D_t3030399641* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t3030399641** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t3030399641* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t2175053619, ___linkSlots_5)); }
	inline LinkU5BU5D_t62501539* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t62501539** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t62501539* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t2175053619, ___keySlots_6)); }
	inline StringU5BU5D_t1642385972* get_keySlots_6() const { return ___keySlots_6; }
	inline StringU5BU5D_t1642385972** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(StringU5BU5D_t1642385972* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t2175053619, ___valueSlots_7)); }
	inline HtmlElementFlagU5BU5D_t614255832* get_valueSlots_7() const { return ___valueSlots_7; }
	inline HtmlElementFlagU5BU5D_t614255832** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(HtmlElementFlagU5BU5D_t614255832* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t2175053619, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t2175053619, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t2175053619, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t2175053619, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t2175053619, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t2175053619, ___serialization_info_13)); }
	inline SerializationInfo_t228987430 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t228987430 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t228987430 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t2175053619, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t2175053619_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t4033062772 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t2175053619_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t4033062772 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t4033062772 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t4033062772 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T2175053619_H
#ifndef DICTIONARY_2_T3719302665_H
#define DICTIONARY_2_T3719302665_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.String,HtmlAgilityPack.HtmlAttribute>
struct  Dictionary_2_t3719302665  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t3030399641* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t62501539* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	StringU5BU5D_t1642385972* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	HtmlAttributeU5BU5D_t425089514* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t228987430 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t3719302665, ___table_4)); }
	inline Int32U5BU5D_t3030399641* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t3030399641** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t3030399641* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t3719302665, ___linkSlots_5)); }
	inline LinkU5BU5D_t62501539* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t62501539** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t62501539* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t3719302665, ___keySlots_6)); }
	inline StringU5BU5D_t1642385972* get_keySlots_6() const { return ___keySlots_6; }
	inline StringU5BU5D_t1642385972** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(StringU5BU5D_t1642385972* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t3719302665, ___valueSlots_7)); }
	inline HtmlAttributeU5BU5D_t425089514* get_valueSlots_7() const { return ___valueSlots_7; }
	inline HtmlAttributeU5BU5D_t425089514** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(HtmlAttributeU5BU5D_t425089514* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t3719302665, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t3719302665, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t3719302665, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t3719302665, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t3719302665, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t3719302665, ___serialization_info_13)); }
	inline SerializationInfo_t228987430 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t228987430 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t228987430 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t3719302665, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t3719302665_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t3843896454 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t3719302665_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t3843896454 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t3843896454 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t3843896454 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T3719302665_H
#ifndef LIST_1_T1417555591_H
#define LIST_1_T1417555591_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<HtmlAgilityPack.HtmlNode>
struct  List_1_t1417555591  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	HtmlNodeU5BU5D_t1365436442* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t1417555591, ____items_1)); }
	inline HtmlNodeU5BU5D_t1365436442* get__items_1() const { return ____items_1; }
	inline HtmlNodeU5BU5D_t1365436442** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(HtmlNodeU5BU5D_t1365436442* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t1417555591, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t1417555591, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t1417555591_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	HtmlNodeU5BU5D_t1365436442* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t1417555591_StaticFields, ___EmptyArray_4)); }
	inline HtmlNodeU5BU5D_t1365436442* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline HtmlNodeU5BU5D_t1365436442** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(HtmlNodeU5BU5D_t1365436442* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T1417555591_H
#ifndef NAMEVALUEPAIRLIST_T1351137418_H
#define NAMEVALUEPAIRLIST_T1351137418_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HtmlAgilityPack.NameValuePairList
struct  NameValuePairList_t1351137418  : public RuntimeObject
{
public:
	// System.String HtmlAgilityPack.NameValuePairList::Text
	String_t* ___Text_0;
	// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>> HtmlAgilityPack.NameValuePairList::_allPairs
	List_1_t1070465849 * ____allPairs_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>> HtmlAgilityPack.NameValuePairList::_pairsWithName
	Dictionary_2_t2985245111 * ____pairsWithName_2;

public:
	inline static int32_t get_offset_of_Text_0() { return static_cast<int32_t>(offsetof(NameValuePairList_t1351137418, ___Text_0)); }
	inline String_t* get_Text_0() const { return ___Text_0; }
	inline String_t** get_address_of_Text_0() { return &___Text_0; }
	inline void set_Text_0(String_t* value)
	{
		___Text_0 = value;
		Il2CppCodeGenWriteBarrier((&___Text_0), value);
	}

	inline static int32_t get_offset_of__allPairs_1() { return static_cast<int32_t>(offsetof(NameValuePairList_t1351137418, ____allPairs_1)); }
	inline List_1_t1070465849 * get__allPairs_1() const { return ____allPairs_1; }
	inline List_1_t1070465849 ** get_address_of__allPairs_1() { return &____allPairs_1; }
	inline void set__allPairs_1(List_1_t1070465849 * value)
	{
		____allPairs_1 = value;
		Il2CppCodeGenWriteBarrier((&____allPairs_1), value);
	}

	inline static int32_t get_offset_of__pairsWithName_2() { return static_cast<int32_t>(offsetof(NameValuePairList_t1351137418, ____pairsWithName_2)); }
	inline Dictionary_2_t2985245111 * get__pairsWithName_2() const { return ____pairsWithName_2; }
	inline Dictionary_2_t2985245111 ** get_address_of__pairsWithName_2() { return &____pairsWithName_2; }
	inline void set__pairsWithName_2(Dictionary_2_t2985245111 * value)
	{
		____pairsWithName_2 = value;
		Il2CppCodeGenWriteBarrier((&____pairsWithName_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMEVALUEPAIRLIST_T1351137418_H
#ifndef HTMLATTRIBUTECOLLECTION_T1787476631_H
#define HTMLATTRIBUTECOLLECTION_T1787476631_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HtmlAgilityPack.HtmlAttributeCollection
struct  HtmlAttributeCollection_t1787476631  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,HtmlAgilityPack.HtmlAttribute> HtmlAgilityPack.HtmlAttributeCollection::Hashitems
	Dictionary_2_t3719302665 * ___Hashitems_0;
	// HtmlAgilityPack.HtmlNode HtmlAgilityPack.HtmlAttributeCollection::_ownernode
	HtmlNode_t2048434459 * ____ownernode_1;
	// System.Collections.Generic.List`1<HtmlAgilityPack.HtmlAttribute> HtmlAgilityPack.HtmlAttributeCollection::items
	List_1_t1173644535 * ___items_2;

public:
	inline static int32_t get_offset_of_Hashitems_0() { return static_cast<int32_t>(offsetof(HtmlAttributeCollection_t1787476631, ___Hashitems_0)); }
	inline Dictionary_2_t3719302665 * get_Hashitems_0() const { return ___Hashitems_0; }
	inline Dictionary_2_t3719302665 ** get_address_of_Hashitems_0() { return &___Hashitems_0; }
	inline void set_Hashitems_0(Dictionary_2_t3719302665 * value)
	{
		___Hashitems_0 = value;
		Il2CppCodeGenWriteBarrier((&___Hashitems_0), value);
	}

	inline static int32_t get_offset_of__ownernode_1() { return static_cast<int32_t>(offsetof(HtmlAttributeCollection_t1787476631, ____ownernode_1)); }
	inline HtmlNode_t2048434459 * get__ownernode_1() const { return ____ownernode_1; }
	inline HtmlNode_t2048434459 ** get_address_of__ownernode_1() { return &____ownernode_1; }
	inline void set__ownernode_1(HtmlNode_t2048434459 * value)
	{
		____ownernode_1 = value;
		Il2CppCodeGenWriteBarrier((&____ownernode_1), value);
	}

	inline static int32_t get_offset_of_items_2() { return static_cast<int32_t>(offsetof(HtmlAttributeCollection_t1787476631, ___items_2)); }
	inline List_1_t1173644535 * get_items_2() const { return ___items_2; }
	inline List_1_t1173644535 ** get_address_of_items_2() { return &___items_2; }
	inline void set_items_2(List_1_t1173644535 * value)
	{
		___items_2 = value;
		Il2CppCodeGenWriteBarrier((&___items_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTMLATTRIBUTECOLLECTION_T1787476631_H
#ifndef VALUECOLLECTION_T2422362508_H
#define VALUECOLLECTION_T2422362508_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,HtmlAgilityPack.HtmlAttribute>
struct  ValueCollection_t2422362508  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection::dictionary
	Dictionary_2_t3719302665 * ___dictionary_0;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(ValueCollection_t2422362508, ___dictionary_0)); }
	inline Dictionary_2_t3719302665 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t3719302665 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t3719302665 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALUECOLLECTION_T2422362508_H
#ifndef HTMLNODECOLLECTION_T2542734491_H
#define HTMLNODECOLLECTION_T2542734491_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HtmlAgilityPack.HtmlNodeCollection
struct  HtmlNodeCollection_t2542734491  : public RuntimeObject
{
public:
	// HtmlAgilityPack.HtmlNode HtmlAgilityPack.HtmlNodeCollection::_parentnode
	HtmlNode_t2048434459 * ____parentnode_0;
	// System.Collections.Generic.List`1<HtmlAgilityPack.HtmlNode> HtmlAgilityPack.HtmlNodeCollection::_items
	List_1_t1417555591 * ____items_1;

public:
	inline static int32_t get_offset_of__parentnode_0() { return static_cast<int32_t>(offsetof(HtmlNodeCollection_t2542734491, ____parentnode_0)); }
	inline HtmlNode_t2048434459 * get__parentnode_0() const { return ____parentnode_0; }
	inline HtmlNode_t2048434459 ** get_address_of__parentnode_0() { return &____parentnode_0; }
	inline void set__parentnode_0(HtmlNode_t2048434459 * value)
	{
		____parentnode_0 = value;
		Il2CppCodeGenWriteBarrier((&____parentnode_0), value);
	}

	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(HtmlNodeCollection_t2542734491, ____items_1)); }
	inline List_1_t1417555591 * get__items_1() const { return ____items_1; }
	inline List_1_t1417555591 ** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(List_1_t1417555591 * value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTMLNODECOLLECTION_T2542734491_H
#ifndef STACK_1_T3136162613_H
#define STACK_1_T3136162613_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Stack`1<HtmlAgilityPack.HtmlNode>
struct  Stack_1_t3136162613  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.Stack`1::_array
	HtmlNodeU5BU5D_t1365436442* ____array_1;
	// System.Int32 System.Collections.Generic.Stack`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.Stack`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__array_1() { return static_cast<int32_t>(offsetof(Stack_1_t3136162613, ____array_1)); }
	inline HtmlNodeU5BU5D_t1365436442* get__array_1() const { return ____array_1; }
	inline HtmlNodeU5BU5D_t1365436442** get_address_of__array_1() { return &____array_1; }
	inline void set__array_1(HtmlNodeU5BU5D_t1365436442* value)
	{
		____array_1 = value;
		Il2CppCodeGenWriteBarrier((&____array_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(Stack_1_t3136162613, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(Stack_1_t3136162613, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STACK_1_T3136162613_H
#ifndef VALUECOLLECTION_T4054287233_H
#define VALUECOLLECTION_T4054287233_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,HtmlAgilityPack.HtmlNode>
struct  ValueCollection_t4054287233  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection::dictionary
	Dictionary_2_t1056260094 * ___dictionary_0;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(ValueCollection_t4054287233, ___dictionary_0)); }
	inline Dictionary_2_t1056260094 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t1056260094 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t1056260094 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALUECOLLECTION_T4054287233_H
#ifndef DICTIONARY_2_T1056260094_H
#define DICTIONARY_2_T1056260094_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.Int32,HtmlAgilityPack.HtmlNode>
struct  Dictionary_2_t1056260094  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t3030399641* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t62501539* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	Int32U5BU5D_t3030399641* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	HtmlNodeU5BU5D_t1365436442* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t228987430 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t1056260094, ___table_4)); }
	inline Int32U5BU5D_t3030399641* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t3030399641** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t3030399641* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t1056260094, ___linkSlots_5)); }
	inline LinkU5BU5D_t62501539* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t62501539** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t62501539* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t1056260094, ___keySlots_6)); }
	inline Int32U5BU5D_t3030399641* get_keySlots_6() const { return ___keySlots_6; }
	inline Int32U5BU5D_t3030399641** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(Int32U5BU5D_t3030399641* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t1056260094, ___valueSlots_7)); }
	inline HtmlNodeU5BU5D_t1365436442* get_valueSlots_7() const { return ___valueSlots_7; }
	inline HtmlNodeU5BU5D_t1365436442** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(HtmlNodeU5BU5D_t1365436442* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t1056260094, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t1056260094, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t1056260094, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t1056260094, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t1056260094, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t1056260094, ___serialization_info_13)); }
	inline SerializationInfo_t228987430 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t228987430 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t228987430 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t1056260094, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t1056260094_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t2049833757 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t1056260094_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t2049833757 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t2049833757 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t2049833757 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T1056260094_H
#ifndef TEXTREADER_T1561828458_H
#define TEXTREADER_T1561828458_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.TextReader
struct  TextReader_t1561828458  : public RuntimeObject
{
public:

public:
};

struct TextReader_t1561828458_StaticFields
{
public:
	// System.IO.TextReader System.IO.TextReader::Null
	TextReader_t1561828458 * ___Null_0;

public:
	inline static int32_t get_offset_of_Null_0() { return static_cast<int32_t>(offsetof(TextReader_t1561828458_StaticFields, ___Null_0)); }
	inline TextReader_t1561828458 * get_Null_0() const { return ___Null_0; }
	inline TextReader_t1561828458 ** get_address_of_Null_0() { return &___Null_0; }
	inline void set_Null_0(TextReader_t1561828458 * value)
	{
		___Null_0 = value;
		Il2CppCodeGenWriteBarrier((&___Null_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTREADER_T1561828458_H
#ifndef LIST_1_T484300294_H
#define LIST_1_T484300294_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<HtmlAgilityPack.HtmlParseError>
struct  List_1_t484300294  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	HtmlParseErrorU5BU5D_t3107688383* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t484300294, ____items_1)); }
	inline HtmlParseErrorU5BU5D_t3107688383* get__items_1() const { return ____items_1; }
	inline HtmlParseErrorU5BU5D_t3107688383** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(HtmlParseErrorU5BU5D_t3107688383* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t484300294, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t484300294, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t484300294_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	HtmlParseErrorU5BU5D_t3107688383* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t484300294_StaticFields, ___EmptyArray_4)); }
	inline HtmlParseErrorU5BU5D_t3107688383* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline HtmlParseErrorU5BU5D_t3107688383** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(HtmlParseErrorU5BU5D_t3107688383* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T484300294_H
#ifndef DICTIONARY_2_T3963213721_H
#define DICTIONARY_2_T3963213721_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.String,HtmlAgilityPack.HtmlNode>
struct  Dictionary_2_t3963213721  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t3030399641* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t62501539* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	StringU5BU5D_t1642385972* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	HtmlNodeU5BU5D_t1365436442* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t228987430 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t3963213721, ___table_4)); }
	inline Int32U5BU5D_t3030399641* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t3030399641** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t3030399641* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t3963213721, ___linkSlots_5)); }
	inline LinkU5BU5D_t62501539* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t62501539** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t62501539* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t3963213721, ___keySlots_6)); }
	inline StringU5BU5D_t1642385972* get_keySlots_6() const { return ___keySlots_6; }
	inline StringU5BU5D_t1642385972** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(StringU5BU5D_t1642385972* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t3963213721, ___valueSlots_7)); }
	inline HtmlNodeU5BU5D_t1365436442* get_valueSlots_7() const { return ___valueSlots_7; }
	inline HtmlNodeU5BU5D_t1365436442** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(HtmlNodeU5BU5D_t1365436442* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t3963213721, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t3963213721, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t3963213721, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t3963213721, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t3963213721, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t3963213721, ___serialization_info_13)); }
	inline SerializationInfo_t228987430 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t228987430 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t228987430 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t3963213721, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t3963213721_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t489276086 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t3963213721_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t489276086 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t489276086 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t489276086 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T3963213721_H
#ifndef XMLNAMETABLE_T1345805268_H
#define XMLNAMETABLE_T1345805268_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNameTable
struct  XmlNameTable_t1345805268  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNAMETABLE_T1345805268_H
#ifndef XPATHNODEITERATOR_T3192332357_H
#define XPATHNODEITERATOR_T3192332357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathNodeIterator
struct  XPathNodeIterator_t3192332357  : public RuntimeObject
{
public:
	// System.Int32 System.Xml.XPath.XPathNodeIterator::_count
	int32_t ____count_0;

public:
	inline static int32_t get_offset_of__count_0() { return static_cast<int32_t>(offsetof(XPathNodeIterator_t3192332357, ____count_0)); }
	inline int32_t get__count_0() const { return ____count_0; }
	inline int32_t* get_address_of__count_0() { return &____count_0; }
	inline void set__count_0(int32_t value)
	{
		____count_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHNODEITERATOR_T3192332357_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef UTILITIES_T3593287176_H
#define UTILITIES_T3593287176_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HtmlAgilityPack.Utilities
struct  Utilities_t3593287176  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UTILITIES_T3593287176_H
#ifndef CRC32_T83196531_H
#define CRC32_T83196531_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HtmlAgilityPack.Crc32
struct  Crc32_t83196531  : public RuntimeObject
{
public:
	// System.UInt32 HtmlAgilityPack.Crc32::_crc32
	uint32_t ____crc32_0;

public:
	inline static int32_t get_offset_of__crc32_0() { return static_cast<int32_t>(offsetof(Crc32_t83196531, ____crc32_0)); }
	inline uint32_t get__crc32_0() const { return ____crc32_0; }
	inline uint32_t* get_address_of__crc32_0() { return &____crc32_0; }
	inline void set__crc32_0(uint32_t value)
	{
		____crc32_0 = value;
	}
};

struct Crc32_t83196531_StaticFields
{
public:
	// System.UInt32[] HtmlAgilityPack.Crc32::crc_32_tab
	UInt32U5BU5D_t59386216* ___crc_32_tab_1;

public:
	inline static int32_t get_offset_of_crc_32_tab_1() { return static_cast<int32_t>(offsetof(Crc32_t83196531_StaticFields, ___crc_32_tab_1)); }
	inline UInt32U5BU5D_t59386216* get_crc_32_tab_1() const { return ___crc_32_tab_1; }
	inline UInt32U5BU5D_t59386216** get_address_of_crc_32_tab_1() { return &___crc_32_tab_1; }
	inline void set_crc_32_tab_1(UInt32U5BU5D_t59386216* value)
	{
		___crc_32_tab_1 = value;
		Il2CppCodeGenWriteBarrier((&___crc_32_tab_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRC32_T83196531_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef XPATHITEM_T3130801258_H
#define XPATHITEM_T3130801258_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathItem
struct  XPathItem_t3130801258  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHITEM_T3130801258_H
#ifndef ENCODING_T663144255_H
#define ENCODING_T663144255_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.Encoding
struct  Encoding_t663144255  : public RuntimeObject
{
public:
	// System.Int32 System.Text.Encoding::codePage
	int32_t ___codePage_0;
	// System.Int32 System.Text.Encoding::windows_code_page
	int32_t ___windows_code_page_1;
	// System.Boolean System.Text.Encoding::is_readonly
	bool ___is_readonly_2;
	// System.Text.DecoderFallback System.Text.Encoding::decoder_fallback
	DecoderFallback_t1715117820 * ___decoder_fallback_3;
	// System.Text.EncoderFallback System.Text.Encoding::encoder_fallback
	EncoderFallback_t1756452756 * ___encoder_fallback_4;
	// System.String System.Text.Encoding::body_name
	String_t* ___body_name_8;
	// System.String System.Text.Encoding::encoding_name
	String_t* ___encoding_name_9;
	// System.String System.Text.Encoding::header_name
	String_t* ___header_name_10;
	// System.Boolean System.Text.Encoding::is_mail_news_display
	bool ___is_mail_news_display_11;
	// System.Boolean System.Text.Encoding::is_mail_news_save
	bool ___is_mail_news_save_12;
	// System.Boolean System.Text.Encoding::is_browser_save
	bool ___is_browser_save_13;
	// System.Boolean System.Text.Encoding::is_browser_display
	bool ___is_browser_display_14;
	// System.String System.Text.Encoding::web_name
	String_t* ___web_name_15;

public:
	inline static int32_t get_offset_of_codePage_0() { return static_cast<int32_t>(offsetof(Encoding_t663144255, ___codePage_0)); }
	inline int32_t get_codePage_0() const { return ___codePage_0; }
	inline int32_t* get_address_of_codePage_0() { return &___codePage_0; }
	inline void set_codePage_0(int32_t value)
	{
		___codePage_0 = value;
	}

	inline static int32_t get_offset_of_windows_code_page_1() { return static_cast<int32_t>(offsetof(Encoding_t663144255, ___windows_code_page_1)); }
	inline int32_t get_windows_code_page_1() const { return ___windows_code_page_1; }
	inline int32_t* get_address_of_windows_code_page_1() { return &___windows_code_page_1; }
	inline void set_windows_code_page_1(int32_t value)
	{
		___windows_code_page_1 = value;
	}

	inline static int32_t get_offset_of_is_readonly_2() { return static_cast<int32_t>(offsetof(Encoding_t663144255, ___is_readonly_2)); }
	inline bool get_is_readonly_2() const { return ___is_readonly_2; }
	inline bool* get_address_of_is_readonly_2() { return &___is_readonly_2; }
	inline void set_is_readonly_2(bool value)
	{
		___is_readonly_2 = value;
	}

	inline static int32_t get_offset_of_decoder_fallback_3() { return static_cast<int32_t>(offsetof(Encoding_t663144255, ___decoder_fallback_3)); }
	inline DecoderFallback_t1715117820 * get_decoder_fallback_3() const { return ___decoder_fallback_3; }
	inline DecoderFallback_t1715117820 ** get_address_of_decoder_fallback_3() { return &___decoder_fallback_3; }
	inline void set_decoder_fallback_3(DecoderFallback_t1715117820 * value)
	{
		___decoder_fallback_3 = value;
		Il2CppCodeGenWriteBarrier((&___decoder_fallback_3), value);
	}

	inline static int32_t get_offset_of_encoder_fallback_4() { return static_cast<int32_t>(offsetof(Encoding_t663144255, ___encoder_fallback_4)); }
	inline EncoderFallback_t1756452756 * get_encoder_fallback_4() const { return ___encoder_fallback_4; }
	inline EncoderFallback_t1756452756 ** get_address_of_encoder_fallback_4() { return &___encoder_fallback_4; }
	inline void set_encoder_fallback_4(EncoderFallback_t1756452756 * value)
	{
		___encoder_fallback_4 = value;
		Il2CppCodeGenWriteBarrier((&___encoder_fallback_4), value);
	}

	inline static int32_t get_offset_of_body_name_8() { return static_cast<int32_t>(offsetof(Encoding_t663144255, ___body_name_8)); }
	inline String_t* get_body_name_8() const { return ___body_name_8; }
	inline String_t** get_address_of_body_name_8() { return &___body_name_8; }
	inline void set_body_name_8(String_t* value)
	{
		___body_name_8 = value;
		Il2CppCodeGenWriteBarrier((&___body_name_8), value);
	}

	inline static int32_t get_offset_of_encoding_name_9() { return static_cast<int32_t>(offsetof(Encoding_t663144255, ___encoding_name_9)); }
	inline String_t* get_encoding_name_9() const { return ___encoding_name_9; }
	inline String_t** get_address_of_encoding_name_9() { return &___encoding_name_9; }
	inline void set_encoding_name_9(String_t* value)
	{
		___encoding_name_9 = value;
		Il2CppCodeGenWriteBarrier((&___encoding_name_9), value);
	}

	inline static int32_t get_offset_of_header_name_10() { return static_cast<int32_t>(offsetof(Encoding_t663144255, ___header_name_10)); }
	inline String_t* get_header_name_10() const { return ___header_name_10; }
	inline String_t** get_address_of_header_name_10() { return &___header_name_10; }
	inline void set_header_name_10(String_t* value)
	{
		___header_name_10 = value;
		Il2CppCodeGenWriteBarrier((&___header_name_10), value);
	}

	inline static int32_t get_offset_of_is_mail_news_display_11() { return static_cast<int32_t>(offsetof(Encoding_t663144255, ___is_mail_news_display_11)); }
	inline bool get_is_mail_news_display_11() const { return ___is_mail_news_display_11; }
	inline bool* get_address_of_is_mail_news_display_11() { return &___is_mail_news_display_11; }
	inline void set_is_mail_news_display_11(bool value)
	{
		___is_mail_news_display_11 = value;
	}

	inline static int32_t get_offset_of_is_mail_news_save_12() { return static_cast<int32_t>(offsetof(Encoding_t663144255, ___is_mail_news_save_12)); }
	inline bool get_is_mail_news_save_12() const { return ___is_mail_news_save_12; }
	inline bool* get_address_of_is_mail_news_save_12() { return &___is_mail_news_save_12; }
	inline void set_is_mail_news_save_12(bool value)
	{
		___is_mail_news_save_12 = value;
	}

	inline static int32_t get_offset_of_is_browser_save_13() { return static_cast<int32_t>(offsetof(Encoding_t663144255, ___is_browser_save_13)); }
	inline bool get_is_browser_save_13() const { return ___is_browser_save_13; }
	inline bool* get_address_of_is_browser_save_13() { return &___is_browser_save_13; }
	inline void set_is_browser_save_13(bool value)
	{
		___is_browser_save_13 = value;
	}

	inline static int32_t get_offset_of_is_browser_display_14() { return static_cast<int32_t>(offsetof(Encoding_t663144255, ___is_browser_display_14)); }
	inline bool get_is_browser_display_14() const { return ___is_browser_display_14; }
	inline bool* get_address_of_is_browser_display_14() { return &___is_browser_display_14; }
	inline void set_is_browser_display_14(bool value)
	{
		___is_browser_display_14 = value;
	}

	inline static int32_t get_offset_of_web_name_15() { return static_cast<int32_t>(offsetof(Encoding_t663144255, ___web_name_15)); }
	inline String_t* get_web_name_15() const { return ___web_name_15; }
	inline String_t** get_address_of_web_name_15() { return &___web_name_15; }
	inline void set_web_name_15(String_t* value)
	{
		___web_name_15 = value;
		Il2CppCodeGenWriteBarrier((&___web_name_15), value);
	}
};

struct Encoding_t663144255_StaticFields
{
public:
	// System.Reflection.Assembly System.Text.Encoding::i18nAssembly
	Assembly_t4268412390 * ___i18nAssembly_5;
	// System.Boolean System.Text.Encoding::i18nDisabled
	bool ___i18nDisabled_6;
	// System.Object[] System.Text.Encoding::encodings
	ObjectU5BU5D_t3614634134* ___encodings_7;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::asciiEncoding
	Encoding_t663144255 * ___asciiEncoding_16;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::bigEndianEncoding
	Encoding_t663144255 * ___bigEndianEncoding_17;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::defaultEncoding
	Encoding_t663144255 * ___defaultEncoding_18;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf7Encoding
	Encoding_t663144255 * ___utf7Encoding_19;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf8EncodingWithMarkers
	Encoding_t663144255 * ___utf8EncodingWithMarkers_20;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf8EncodingWithoutMarkers
	Encoding_t663144255 * ___utf8EncodingWithoutMarkers_21;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::unicodeEncoding
	Encoding_t663144255 * ___unicodeEncoding_22;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::isoLatin1Encoding
	Encoding_t663144255 * ___isoLatin1Encoding_23;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf8EncodingUnsafe
	Encoding_t663144255 * ___utf8EncodingUnsafe_24;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf32Encoding
	Encoding_t663144255 * ___utf32Encoding_25;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::bigEndianUTF32Encoding
	Encoding_t663144255 * ___bigEndianUTF32Encoding_26;
	// System.Object System.Text.Encoding::lockobj
	RuntimeObject * ___lockobj_27;

public:
	inline static int32_t get_offset_of_i18nAssembly_5() { return static_cast<int32_t>(offsetof(Encoding_t663144255_StaticFields, ___i18nAssembly_5)); }
	inline Assembly_t4268412390 * get_i18nAssembly_5() const { return ___i18nAssembly_5; }
	inline Assembly_t4268412390 ** get_address_of_i18nAssembly_5() { return &___i18nAssembly_5; }
	inline void set_i18nAssembly_5(Assembly_t4268412390 * value)
	{
		___i18nAssembly_5 = value;
		Il2CppCodeGenWriteBarrier((&___i18nAssembly_5), value);
	}

	inline static int32_t get_offset_of_i18nDisabled_6() { return static_cast<int32_t>(offsetof(Encoding_t663144255_StaticFields, ___i18nDisabled_6)); }
	inline bool get_i18nDisabled_6() const { return ___i18nDisabled_6; }
	inline bool* get_address_of_i18nDisabled_6() { return &___i18nDisabled_6; }
	inline void set_i18nDisabled_6(bool value)
	{
		___i18nDisabled_6 = value;
	}

	inline static int32_t get_offset_of_encodings_7() { return static_cast<int32_t>(offsetof(Encoding_t663144255_StaticFields, ___encodings_7)); }
	inline ObjectU5BU5D_t3614634134* get_encodings_7() const { return ___encodings_7; }
	inline ObjectU5BU5D_t3614634134** get_address_of_encodings_7() { return &___encodings_7; }
	inline void set_encodings_7(ObjectU5BU5D_t3614634134* value)
	{
		___encodings_7 = value;
		Il2CppCodeGenWriteBarrier((&___encodings_7), value);
	}

	inline static int32_t get_offset_of_asciiEncoding_16() { return static_cast<int32_t>(offsetof(Encoding_t663144255_StaticFields, ___asciiEncoding_16)); }
	inline Encoding_t663144255 * get_asciiEncoding_16() const { return ___asciiEncoding_16; }
	inline Encoding_t663144255 ** get_address_of_asciiEncoding_16() { return &___asciiEncoding_16; }
	inline void set_asciiEncoding_16(Encoding_t663144255 * value)
	{
		___asciiEncoding_16 = value;
		Il2CppCodeGenWriteBarrier((&___asciiEncoding_16), value);
	}

	inline static int32_t get_offset_of_bigEndianEncoding_17() { return static_cast<int32_t>(offsetof(Encoding_t663144255_StaticFields, ___bigEndianEncoding_17)); }
	inline Encoding_t663144255 * get_bigEndianEncoding_17() const { return ___bigEndianEncoding_17; }
	inline Encoding_t663144255 ** get_address_of_bigEndianEncoding_17() { return &___bigEndianEncoding_17; }
	inline void set_bigEndianEncoding_17(Encoding_t663144255 * value)
	{
		___bigEndianEncoding_17 = value;
		Il2CppCodeGenWriteBarrier((&___bigEndianEncoding_17), value);
	}

	inline static int32_t get_offset_of_defaultEncoding_18() { return static_cast<int32_t>(offsetof(Encoding_t663144255_StaticFields, ___defaultEncoding_18)); }
	inline Encoding_t663144255 * get_defaultEncoding_18() const { return ___defaultEncoding_18; }
	inline Encoding_t663144255 ** get_address_of_defaultEncoding_18() { return &___defaultEncoding_18; }
	inline void set_defaultEncoding_18(Encoding_t663144255 * value)
	{
		___defaultEncoding_18 = value;
		Il2CppCodeGenWriteBarrier((&___defaultEncoding_18), value);
	}

	inline static int32_t get_offset_of_utf7Encoding_19() { return static_cast<int32_t>(offsetof(Encoding_t663144255_StaticFields, ___utf7Encoding_19)); }
	inline Encoding_t663144255 * get_utf7Encoding_19() const { return ___utf7Encoding_19; }
	inline Encoding_t663144255 ** get_address_of_utf7Encoding_19() { return &___utf7Encoding_19; }
	inline void set_utf7Encoding_19(Encoding_t663144255 * value)
	{
		___utf7Encoding_19 = value;
		Il2CppCodeGenWriteBarrier((&___utf7Encoding_19), value);
	}

	inline static int32_t get_offset_of_utf8EncodingWithMarkers_20() { return static_cast<int32_t>(offsetof(Encoding_t663144255_StaticFields, ___utf8EncodingWithMarkers_20)); }
	inline Encoding_t663144255 * get_utf8EncodingWithMarkers_20() const { return ___utf8EncodingWithMarkers_20; }
	inline Encoding_t663144255 ** get_address_of_utf8EncodingWithMarkers_20() { return &___utf8EncodingWithMarkers_20; }
	inline void set_utf8EncodingWithMarkers_20(Encoding_t663144255 * value)
	{
		___utf8EncodingWithMarkers_20 = value;
		Il2CppCodeGenWriteBarrier((&___utf8EncodingWithMarkers_20), value);
	}

	inline static int32_t get_offset_of_utf8EncodingWithoutMarkers_21() { return static_cast<int32_t>(offsetof(Encoding_t663144255_StaticFields, ___utf8EncodingWithoutMarkers_21)); }
	inline Encoding_t663144255 * get_utf8EncodingWithoutMarkers_21() const { return ___utf8EncodingWithoutMarkers_21; }
	inline Encoding_t663144255 ** get_address_of_utf8EncodingWithoutMarkers_21() { return &___utf8EncodingWithoutMarkers_21; }
	inline void set_utf8EncodingWithoutMarkers_21(Encoding_t663144255 * value)
	{
		___utf8EncodingWithoutMarkers_21 = value;
		Il2CppCodeGenWriteBarrier((&___utf8EncodingWithoutMarkers_21), value);
	}

	inline static int32_t get_offset_of_unicodeEncoding_22() { return static_cast<int32_t>(offsetof(Encoding_t663144255_StaticFields, ___unicodeEncoding_22)); }
	inline Encoding_t663144255 * get_unicodeEncoding_22() const { return ___unicodeEncoding_22; }
	inline Encoding_t663144255 ** get_address_of_unicodeEncoding_22() { return &___unicodeEncoding_22; }
	inline void set_unicodeEncoding_22(Encoding_t663144255 * value)
	{
		___unicodeEncoding_22 = value;
		Il2CppCodeGenWriteBarrier((&___unicodeEncoding_22), value);
	}

	inline static int32_t get_offset_of_isoLatin1Encoding_23() { return static_cast<int32_t>(offsetof(Encoding_t663144255_StaticFields, ___isoLatin1Encoding_23)); }
	inline Encoding_t663144255 * get_isoLatin1Encoding_23() const { return ___isoLatin1Encoding_23; }
	inline Encoding_t663144255 ** get_address_of_isoLatin1Encoding_23() { return &___isoLatin1Encoding_23; }
	inline void set_isoLatin1Encoding_23(Encoding_t663144255 * value)
	{
		___isoLatin1Encoding_23 = value;
		Il2CppCodeGenWriteBarrier((&___isoLatin1Encoding_23), value);
	}

	inline static int32_t get_offset_of_utf8EncodingUnsafe_24() { return static_cast<int32_t>(offsetof(Encoding_t663144255_StaticFields, ___utf8EncodingUnsafe_24)); }
	inline Encoding_t663144255 * get_utf8EncodingUnsafe_24() const { return ___utf8EncodingUnsafe_24; }
	inline Encoding_t663144255 ** get_address_of_utf8EncodingUnsafe_24() { return &___utf8EncodingUnsafe_24; }
	inline void set_utf8EncodingUnsafe_24(Encoding_t663144255 * value)
	{
		___utf8EncodingUnsafe_24 = value;
		Il2CppCodeGenWriteBarrier((&___utf8EncodingUnsafe_24), value);
	}

	inline static int32_t get_offset_of_utf32Encoding_25() { return static_cast<int32_t>(offsetof(Encoding_t663144255_StaticFields, ___utf32Encoding_25)); }
	inline Encoding_t663144255 * get_utf32Encoding_25() const { return ___utf32Encoding_25; }
	inline Encoding_t663144255 ** get_address_of_utf32Encoding_25() { return &___utf32Encoding_25; }
	inline void set_utf32Encoding_25(Encoding_t663144255 * value)
	{
		___utf32Encoding_25 = value;
		Il2CppCodeGenWriteBarrier((&___utf32Encoding_25), value);
	}

	inline static int32_t get_offset_of_bigEndianUTF32Encoding_26() { return static_cast<int32_t>(offsetof(Encoding_t663144255_StaticFields, ___bigEndianUTF32Encoding_26)); }
	inline Encoding_t663144255 * get_bigEndianUTF32Encoding_26() const { return ___bigEndianUTF32Encoding_26; }
	inline Encoding_t663144255 ** get_address_of_bigEndianUTF32Encoding_26() { return &___bigEndianUTF32Encoding_26; }
	inline void set_bigEndianUTF32Encoding_26(Encoding_t663144255 * value)
	{
		___bigEndianUTF32Encoding_26 = value;
		Il2CppCodeGenWriteBarrier((&___bigEndianUTF32Encoding_26), value);
	}

	inline static int32_t get_offset_of_lockobj_27() { return static_cast<int32_t>(offsetof(Encoding_t663144255_StaticFields, ___lockobj_27)); }
	inline RuntimeObject * get_lockobj_27() const { return ___lockobj_27; }
	inline RuntimeObject ** get_address_of_lockobj_27() { return &___lockobj_27; }
	inline void set_lockobj_27(RuntimeObject * value)
	{
		___lockobj_27 = value;
		Il2CppCodeGenWriteBarrier((&___lockobj_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCODING_T663144255_H
#ifndef LIST_1_T1070465849_H
#define LIST_1_T1070465849_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>
struct  List_1_t1070465849  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	KeyValuePair_2U5BU5D_t1360691296* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t1070465849, ____items_1)); }
	inline KeyValuePair_2U5BU5D_t1360691296* get__items_1() const { return ____items_1; }
	inline KeyValuePair_2U5BU5D_t1360691296** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(KeyValuePair_2U5BU5D_t1360691296* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t1070465849, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t1070465849, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t1070465849_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	KeyValuePair_2U5BU5D_t1360691296* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t1070465849_StaticFields, ___EmptyArray_4)); }
	inline KeyValuePair_2U5BU5D_t1360691296* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline KeyValuePair_2U5BU5D_t1360691296** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(KeyValuePair_2U5BU5D_t1360691296* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T1070465849_H
#ifndef DICTIONARY_2_T2985245111_H
#define DICTIONARY_2_T2985245111_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>>
struct  Dictionary_2_t2985245111  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t3030399641* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t62501539* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	StringU5BU5D_t1642385972* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	List_1U5BU5D_t1425068996* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t228987430 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t2985245111, ___table_4)); }
	inline Int32U5BU5D_t3030399641* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t3030399641** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t3030399641* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t2985245111, ___linkSlots_5)); }
	inline LinkU5BU5D_t62501539* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t62501539** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t62501539* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t2985245111, ___keySlots_6)); }
	inline StringU5BU5D_t1642385972* get_keySlots_6() const { return ___keySlots_6; }
	inline StringU5BU5D_t1642385972** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(StringU5BU5D_t1642385972* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t2985245111, ___valueSlots_7)); }
	inline List_1U5BU5D_t1425068996* get_valueSlots_7() const { return ___valueSlots_7; }
	inline List_1U5BU5D_t1425068996** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(List_1U5BU5D_t1425068996* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t2985245111, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t2985245111, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t2985245111, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t2985245111, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t2985245111, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t2985245111, ___serialization_info_13)); }
	inline SerializationInfo_t228987430 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t228987430 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t228987430 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t2985245111, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t2985245111_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t548908640 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t2985245111_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t548908640 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t548908640 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t548908640 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T2985245111_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t1328083999* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t1328083999* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t1328083999** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t1328083999* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef EXCEPTION_T1927440687_H
#define EXCEPTION_T1927440687_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t1927440687  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t169632028* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t1927440687 * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t169632028* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t169632028** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t169632028* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___inner_exception_1)); }
	inline Exception_t1927440687 * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t1927440687 ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t1927440687 * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T1927440687_H
#ifndef HTMLNAMETABLE_T3848610378_H
#define HTMLNAMETABLE_T3848610378_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HtmlAgilityPack.HtmlNameTable
struct  HtmlNameTable_t3848610378  : public XmlNameTable_t1345805268
{
public:
	// System.Xml.NameTable HtmlAgilityPack.HtmlNameTable::_nametable
	NameTable_t594386929 * ____nametable_0;

public:
	inline static int32_t get_offset_of__nametable_0() { return static_cast<int32_t>(offsetof(HtmlNameTable_t3848610378, ____nametable_0)); }
	inline NameTable_t594386929 * get__nametable_0() const { return ____nametable_0; }
	inline NameTable_t594386929 ** get_address_of__nametable_0() { return &____nametable_0; }
	inline void set__nametable_0(NameTable_t594386929 * value)
	{
		____nametable_0 = value;
		Il2CppCodeGenWriteBarrier((&____nametable_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTMLNAMETABLE_T3848610378_H
#ifndef NAMETABLE_T594386929_H
#define NAMETABLE_T594386929_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.NameTable
struct  NameTable_t594386929  : public XmlNameTable_t1345805268
{
public:
	// System.Int32 System.Xml.NameTable::count
	int32_t ___count_0;
	// System.Xml.NameTable/Entry[] System.Xml.NameTable::buckets
	EntryU5BU5D_t180042139* ___buckets_1;
	// System.Int32 System.Xml.NameTable::size
	int32_t ___size_2;

public:
	inline static int32_t get_offset_of_count_0() { return static_cast<int32_t>(offsetof(NameTable_t594386929, ___count_0)); }
	inline int32_t get_count_0() const { return ___count_0; }
	inline int32_t* get_address_of_count_0() { return &___count_0; }
	inline void set_count_0(int32_t value)
	{
		___count_0 = value;
	}

	inline static int32_t get_offset_of_buckets_1() { return static_cast<int32_t>(offsetof(NameTable_t594386929, ___buckets_1)); }
	inline EntryU5BU5D_t180042139* get_buckets_1() const { return ___buckets_1; }
	inline EntryU5BU5D_t180042139** get_address_of_buckets_1() { return &___buckets_1; }
	inline void set_buckets_1(EntryU5BU5D_t180042139* value)
	{
		___buckets_1 = value;
		Il2CppCodeGenWriteBarrier((&___buckets_1), value);
	}

	inline static int32_t get_offset_of_size_2() { return static_cast<int32_t>(offsetof(NameTable_t594386929, ___size_2)); }
	inline int32_t get_size_2() const { return ___size_2; }
	inline int32_t* get_address_of_size_2() { return &___size_2; }
	inline void set_size_2(int32_t value)
	{
		___size_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMETABLE_T594386929_H
#ifndef KEYVALUEPAIR_2_T1476647887_H
#define KEYVALUEPAIR_2_T1476647887_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.String,HtmlAgilityPack.HtmlAttribute>
struct  KeyValuePair_2_t1476647887 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	HtmlAttribute_t1804523403 * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t1476647887, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t1476647887, ___value_1)); }
	inline HtmlAttribute_t1804523403 * get_value_1() const { return ___value_1; }
	inline HtmlAttribute_t1804523403 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(HtmlAttribute_t1804523403 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T1476647887_H
#ifndef KEYVALUEPAIR_2_T3108572612_H
#define KEYVALUEPAIR_2_T3108572612_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Int32,HtmlAgilityPack.HtmlNode>
struct  KeyValuePair_2_t3108572612 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	int32_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	HtmlNode_t2048434459 * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3108572612, ___key_0)); }
	inline int32_t get_key_0() const { return ___key_0; }
	inline int32_t* get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(int32_t value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3108572612, ___value_1)); }
	inline HtmlNode_t2048434459 * get_value_1() const { return ___value_1; }
	inline HtmlNode_t2048434459 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(HtmlNode_t2048434459 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T3108572612_H
#ifndef KEYVALUEPAIR_2_T3749587448_H
#define KEYVALUEPAIR_2_T3749587448_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>
struct  KeyValuePair_2_t3749587448 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	int32_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3749587448, ___key_0)); }
	inline int32_t get_key_0() const { return ___key_0; }
	inline int32_t* get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(int32_t value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3749587448, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T3749587448_H
#ifndef ENUMERATOR_T952285265_H
#define ENUMERATOR_T952285265_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<HtmlAgilityPack.HtmlNode>
struct  Enumerator_t952285265 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t1417555591 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	HtmlNode_t2048434459 * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t952285265, ___l_0)); }
	inline List_1_t1417555591 * get_l_0() const { return ___l_0; }
	inline List_1_t1417555591 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t1417555591 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t952285265, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t952285265, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t952285265, ___current_3)); }
	inline HtmlNode_t2048434459 * get_current_3() const { return ___current_3; }
	inline HtmlNode_t2048434459 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(HtmlNode_t2048434459 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T952285265_H
#ifndef STRINGWRITER_T4139609088_H
#define STRINGWRITER_T4139609088_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.StringWriter
struct  StringWriter_t4139609088  : public TextWriter_t4027217640
{
public:
	// System.Text.StringBuilder System.IO.StringWriter::internalString
	StringBuilder_t1221177846 * ___internalString_3;
	// System.Boolean System.IO.StringWriter::disposed
	bool ___disposed_4;

public:
	inline static int32_t get_offset_of_internalString_3() { return static_cast<int32_t>(offsetof(StringWriter_t4139609088, ___internalString_3)); }
	inline StringBuilder_t1221177846 * get_internalString_3() const { return ___internalString_3; }
	inline StringBuilder_t1221177846 ** get_address_of_internalString_3() { return &___internalString_3; }
	inline void set_internalString_3(StringBuilder_t1221177846 * value)
	{
		___internalString_3 = value;
		Il2CppCodeGenWriteBarrier((&___internalString_3), value);
	}

	inline static int32_t get_offset_of_disposed_4() { return static_cast<int32_t>(offsetof(StringWriter_t4139609088, ___disposed_4)); }
	inline bool get_disposed_4() const { return ___disposed_4; }
	inline bool* get_address_of_disposed_4() { return &___disposed_4; }
	inline void set_disposed_4(bool value)
	{
		___disposed_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGWRITER_T4139609088_H
#ifndef KEYVALUEPAIR_2_T38854645_H
#define KEYVALUEPAIR_2_T38854645_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
struct  KeyValuePair_2_t38854645 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	RuntimeObject * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t38854645, ___key_0)); }
	inline RuntimeObject * get_key_0() const { return ___key_0; }
	inline RuntimeObject ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RuntimeObject * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t38854645, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T38854645_H
#ifndef KEYVALUEPAIR_2_T1701344717_H
#define KEYVALUEPAIR_2_T1701344717_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.String,System.String>
struct  KeyValuePair_2_t1701344717 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	String_t* ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t1701344717, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t1701344717, ___value_1)); }
	inline String_t* get_value_1() const { return ___value_1; }
	inline String_t** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(String_t* value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T1701344717_H
#ifndef INT64_T909078037_H
#define INT64_T909078037_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int64
struct  Int64_t909078037 
{
public:
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int64_t909078037, ___m_value_2)); }
	inline int64_t get_m_value_2() const { return ___m_value_2; }
	inline int64_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int64_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT64_T909078037_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	IntPtr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline IntPtr_t get_Zero_1() const { return ___Zero_1; }
	inline IntPtr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(IntPtr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SYSTEMEXCEPTION_T3877406272_H
#define SYSTEMEXCEPTION_T3877406272_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t3877406272  : public Exception_t1927440687
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T3877406272_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t1328083999* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t1328083999* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t1328083999** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t1328083999* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef XPATHNAVIGATOR_T3981235968_H
#define XPATHNAVIGATOR_T3981235968_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathNavigator
struct  XPathNavigator_t3981235968  : public XPathItem_t3130801258
{
public:

public:
};

struct XPathNavigator_t3981235968_StaticFields
{
public:
	// System.Char[] System.Xml.XPath.XPathNavigator::escape_text_chars
	CharU5BU5D_t1328083999* ___escape_text_chars_0;
	// System.Char[] System.Xml.XPath.XPathNavigator::escape_attr_chars
	CharU5BU5D_t1328083999* ___escape_attr_chars_1;

public:
	inline static int32_t get_offset_of_escape_text_chars_0() { return static_cast<int32_t>(offsetof(XPathNavigator_t3981235968_StaticFields, ___escape_text_chars_0)); }
	inline CharU5BU5D_t1328083999* get_escape_text_chars_0() const { return ___escape_text_chars_0; }
	inline CharU5BU5D_t1328083999** get_address_of_escape_text_chars_0() { return &___escape_text_chars_0; }
	inline void set_escape_text_chars_0(CharU5BU5D_t1328083999* value)
	{
		___escape_text_chars_0 = value;
		Il2CppCodeGenWriteBarrier((&___escape_text_chars_0), value);
	}

	inline static int32_t get_offset_of_escape_attr_chars_1() { return static_cast<int32_t>(offsetof(XPathNavigator_t3981235968_StaticFields, ___escape_attr_chars_1)); }
	inline CharU5BU5D_t1328083999* get_escape_attr_chars_1() const { return ___escape_attr_chars_1; }
	inline CharU5BU5D_t1328083999** get_address_of_escape_attr_chars_1() { return &___escape_attr_chars_1; }
	inline void set_escape_attr_chars_1(CharU5BU5D_t1328083999* value)
	{
		___escape_attr_chars_1 = value;
		Il2CppCodeGenWriteBarrier((&___escape_attr_chars_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHNAVIGATOR_T3981235968_H
#ifndef BOOLEAN_T3825574718_H
#define BOOLEAN_T3825574718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t3825574718 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t3825574718, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t3825574718_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t3825574718_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t3825574718_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T3825574718_H
#ifndef UINT32_T2149682021_H
#define UINT32_T2149682021_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt32
struct  UInt32_t2149682021 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(UInt32_t2149682021, ___m_value_2)); }
	inline uint32_t get_m_value_2() const { return ___m_value_2; }
	inline uint32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(uint32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32_T2149682021_H
#ifndef ENUMERATOR_T708374209_H
#define ENUMERATOR_T708374209_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<HtmlAgilityPack.HtmlAttribute>
struct  Enumerator_t708374209 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t1173644535 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	HtmlAttribute_t1804523403 * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t708374209, ___l_0)); }
	inline List_1_t1173644535 * get_l_0() const { return ___l_0; }
	inline List_1_t1173644535 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t1173644535 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t708374209, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t708374209, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t708374209, ___current_3)); }
	inline HtmlAttribute_t1804523403 * get_current_3() const { return ___current_3; }
	inline HtmlAttribute_t1804523403 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(HtmlAttribute_t1804523403 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T708374209_H
#ifndef CHAR_T3454481338_H
#define CHAR_T3454481338_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Char
struct  Char_t3454481338 
{
public:
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Char_t3454481338, ___m_value_2)); }
	inline Il2CppChar get_m_value_2() const { return ___m_value_2; }
	inline Il2CppChar* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(Il2CppChar value)
	{
		___m_value_2 = value;
	}
};

struct Char_t3454481338_StaticFields
{
public:
	// System.Byte* System.Char::category_data
	uint8_t* ___category_data_3;
	// System.Byte* System.Char::numeric_data
	uint8_t* ___numeric_data_4;
	// System.Double* System.Char::numeric_data_values
	double* ___numeric_data_values_5;
	// System.UInt16* System.Char::to_lower_data_low
	uint16_t* ___to_lower_data_low_6;
	// System.UInt16* System.Char::to_lower_data_high
	uint16_t* ___to_lower_data_high_7;
	// System.UInt16* System.Char::to_upper_data_low
	uint16_t* ___to_upper_data_low_8;
	// System.UInt16* System.Char::to_upper_data_high
	uint16_t* ___to_upper_data_high_9;

public:
	inline static int32_t get_offset_of_category_data_3() { return static_cast<int32_t>(offsetof(Char_t3454481338_StaticFields, ___category_data_3)); }
	inline uint8_t* get_category_data_3() const { return ___category_data_3; }
	inline uint8_t** get_address_of_category_data_3() { return &___category_data_3; }
	inline void set_category_data_3(uint8_t* value)
	{
		___category_data_3 = value;
	}

	inline static int32_t get_offset_of_numeric_data_4() { return static_cast<int32_t>(offsetof(Char_t3454481338_StaticFields, ___numeric_data_4)); }
	inline uint8_t* get_numeric_data_4() const { return ___numeric_data_4; }
	inline uint8_t** get_address_of_numeric_data_4() { return &___numeric_data_4; }
	inline void set_numeric_data_4(uint8_t* value)
	{
		___numeric_data_4 = value;
	}

	inline static int32_t get_offset_of_numeric_data_values_5() { return static_cast<int32_t>(offsetof(Char_t3454481338_StaticFields, ___numeric_data_values_5)); }
	inline double* get_numeric_data_values_5() const { return ___numeric_data_values_5; }
	inline double** get_address_of_numeric_data_values_5() { return &___numeric_data_values_5; }
	inline void set_numeric_data_values_5(double* value)
	{
		___numeric_data_values_5 = value;
	}

	inline static int32_t get_offset_of_to_lower_data_low_6() { return static_cast<int32_t>(offsetof(Char_t3454481338_StaticFields, ___to_lower_data_low_6)); }
	inline uint16_t* get_to_lower_data_low_6() const { return ___to_lower_data_low_6; }
	inline uint16_t** get_address_of_to_lower_data_low_6() { return &___to_lower_data_low_6; }
	inline void set_to_lower_data_low_6(uint16_t* value)
	{
		___to_lower_data_low_6 = value;
	}

	inline static int32_t get_offset_of_to_lower_data_high_7() { return static_cast<int32_t>(offsetof(Char_t3454481338_StaticFields, ___to_lower_data_high_7)); }
	inline uint16_t* get_to_lower_data_high_7() const { return ___to_lower_data_high_7; }
	inline uint16_t** get_address_of_to_lower_data_high_7() { return &___to_lower_data_high_7; }
	inline void set_to_lower_data_high_7(uint16_t* value)
	{
		___to_lower_data_high_7 = value;
	}

	inline static int32_t get_offset_of_to_upper_data_low_8() { return static_cast<int32_t>(offsetof(Char_t3454481338_StaticFields, ___to_upper_data_low_8)); }
	inline uint16_t* get_to_upper_data_low_8() const { return ___to_upper_data_low_8; }
	inline uint16_t** get_address_of_to_upper_data_low_8() { return &___to_upper_data_low_8; }
	inline void set_to_upper_data_low_8(uint16_t* value)
	{
		___to_upper_data_low_8 = value;
	}

	inline static int32_t get_offset_of_to_upper_data_high_9() { return static_cast<int32_t>(offsetof(Char_t3454481338_StaticFields, ___to_upper_data_high_9)); }
	inline uint16_t* get_to_upper_data_high_9() const { return ___to_upper_data_high_9; }
	inline uint16_t** get_address_of_to_upper_data_high_9() { return &___to_upper_data_high_9; }
	inline void set_to_upper_data_high_9(uint16_t* value)
	{
		___to_upper_data_high_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHAR_T3454481338_H
#ifndef ENUMERATOR_T1593300101_H
#define ENUMERATOR_T1593300101_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<System.Object>
struct  Enumerator_t1593300101 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t2058570427 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	RuntimeObject * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t1593300101, ___l_0)); }
	inline List_1_t2058570427 * get_l_0() const { return ___l_0; }
	inline List_1_t2058570427 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t2058570427 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t1593300101, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t1593300101, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t1593300101, ___current_3)); }
	inline RuntimeObject * get_current_3() const { return ___current_3; }
	inline RuntimeObject ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T1593300101_H
#ifndef VOID_T1841601450_H
#define VOID_T1841601450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1841601450 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1841601450_H
#ifndef ENCODINGFOUNDEXCEPTION_T1471015948_H
#define ENCODINGFOUNDEXCEPTION_T1471015948_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HtmlAgilityPack.EncodingFoundException
struct  EncodingFoundException_t1471015948  : public Exception_t1927440687
{
public:
	// System.Text.Encoding HtmlAgilityPack.EncodingFoundException::_encoding
	Encoding_t663144255 * ____encoding_11;

public:
	inline static int32_t get_offset_of__encoding_11() { return static_cast<int32_t>(offsetof(EncodingFoundException_t1471015948, ____encoding_11)); }
	inline Encoding_t663144255 * get__encoding_11() const { return ____encoding_11; }
	inline Encoding_t663144255 ** get_address_of__encoding_11() { return &____encoding_11; }
	inline void set__encoding_11(Encoding_t663144255 * value)
	{
		____encoding_11 = value;
		Il2CppCodeGenWriteBarrier((&____encoding_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCODINGFOUNDEXCEPTION_T1471015948_H
#ifndef BYTE_T3683104436_H
#define BYTE_T3683104436_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Byte
struct  Byte_t3683104436 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Byte_t3683104436, ___m_value_2)); }
	inline uint8_t get_m_value_2() const { return ___m_value_2; }
	inline uint8_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(uint8_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTE_T3683104436_H
#ifndef INT32_T2071877448_H
#define INT32_T2071877448_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2071877448 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2071877448, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2071877448_H
#ifndef UINT16_T986882611_H
#define UINT16_T986882611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt16
struct  UInt16_t986882611 
{
public:
	// System.UInt16 System.UInt16::m_value
	uint16_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(UInt16_t986882611, ___m_value_2)); }
	inline uint16_t get_m_value_2() const { return ___m_value_2; }
	inline uint16_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(uint16_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT16_T986882611_H
#ifndef STREAMREADER_T2360341767_H
#define STREAMREADER_T2360341767_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.StreamReader
struct  StreamReader_t2360341767  : public TextReader_t1561828458
{
public:
	// System.Byte[] System.IO.StreamReader::input_buffer
	ByteU5BU5D_t3397334013* ___input_buffer_1;
	// System.Char[] System.IO.StreamReader::decoded_buffer
	CharU5BU5D_t1328083999* ___decoded_buffer_2;
	// System.Int32 System.IO.StreamReader::decoded_count
	int32_t ___decoded_count_3;
	// System.Int32 System.IO.StreamReader::pos
	int32_t ___pos_4;
	// System.Int32 System.IO.StreamReader::buffer_size
	int32_t ___buffer_size_5;
	// System.Int32 System.IO.StreamReader::do_checks
	int32_t ___do_checks_6;
	// System.Text.Encoding System.IO.StreamReader::encoding
	Encoding_t663144255 * ___encoding_7;
	// System.Text.Decoder System.IO.StreamReader::decoder
	Decoder_t3792697818 * ___decoder_8;
	// System.IO.Stream System.IO.StreamReader::base_stream
	Stream_t3255436806 * ___base_stream_9;
	// System.Boolean System.IO.StreamReader::mayBlock
	bool ___mayBlock_10;
	// System.Text.StringBuilder System.IO.StreamReader::line_builder
	StringBuilder_t1221177846 * ___line_builder_11;
	// System.Boolean System.IO.StreamReader::foundCR
	bool ___foundCR_13;

public:
	inline static int32_t get_offset_of_input_buffer_1() { return static_cast<int32_t>(offsetof(StreamReader_t2360341767, ___input_buffer_1)); }
	inline ByteU5BU5D_t3397334013* get_input_buffer_1() const { return ___input_buffer_1; }
	inline ByteU5BU5D_t3397334013** get_address_of_input_buffer_1() { return &___input_buffer_1; }
	inline void set_input_buffer_1(ByteU5BU5D_t3397334013* value)
	{
		___input_buffer_1 = value;
		Il2CppCodeGenWriteBarrier((&___input_buffer_1), value);
	}

	inline static int32_t get_offset_of_decoded_buffer_2() { return static_cast<int32_t>(offsetof(StreamReader_t2360341767, ___decoded_buffer_2)); }
	inline CharU5BU5D_t1328083999* get_decoded_buffer_2() const { return ___decoded_buffer_2; }
	inline CharU5BU5D_t1328083999** get_address_of_decoded_buffer_2() { return &___decoded_buffer_2; }
	inline void set_decoded_buffer_2(CharU5BU5D_t1328083999* value)
	{
		___decoded_buffer_2 = value;
		Il2CppCodeGenWriteBarrier((&___decoded_buffer_2), value);
	}

	inline static int32_t get_offset_of_decoded_count_3() { return static_cast<int32_t>(offsetof(StreamReader_t2360341767, ___decoded_count_3)); }
	inline int32_t get_decoded_count_3() const { return ___decoded_count_3; }
	inline int32_t* get_address_of_decoded_count_3() { return &___decoded_count_3; }
	inline void set_decoded_count_3(int32_t value)
	{
		___decoded_count_3 = value;
	}

	inline static int32_t get_offset_of_pos_4() { return static_cast<int32_t>(offsetof(StreamReader_t2360341767, ___pos_4)); }
	inline int32_t get_pos_4() const { return ___pos_4; }
	inline int32_t* get_address_of_pos_4() { return &___pos_4; }
	inline void set_pos_4(int32_t value)
	{
		___pos_4 = value;
	}

	inline static int32_t get_offset_of_buffer_size_5() { return static_cast<int32_t>(offsetof(StreamReader_t2360341767, ___buffer_size_5)); }
	inline int32_t get_buffer_size_5() const { return ___buffer_size_5; }
	inline int32_t* get_address_of_buffer_size_5() { return &___buffer_size_5; }
	inline void set_buffer_size_5(int32_t value)
	{
		___buffer_size_5 = value;
	}

	inline static int32_t get_offset_of_do_checks_6() { return static_cast<int32_t>(offsetof(StreamReader_t2360341767, ___do_checks_6)); }
	inline int32_t get_do_checks_6() const { return ___do_checks_6; }
	inline int32_t* get_address_of_do_checks_6() { return &___do_checks_6; }
	inline void set_do_checks_6(int32_t value)
	{
		___do_checks_6 = value;
	}

	inline static int32_t get_offset_of_encoding_7() { return static_cast<int32_t>(offsetof(StreamReader_t2360341767, ___encoding_7)); }
	inline Encoding_t663144255 * get_encoding_7() const { return ___encoding_7; }
	inline Encoding_t663144255 ** get_address_of_encoding_7() { return &___encoding_7; }
	inline void set_encoding_7(Encoding_t663144255 * value)
	{
		___encoding_7 = value;
		Il2CppCodeGenWriteBarrier((&___encoding_7), value);
	}

	inline static int32_t get_offset_of_decoder_8() { return static_cast<int32_t>(offsetof(StreamReader_t2360341767, ___decoder_8)); }
	inline Decoder_t3792697818 * get_decoder_8() const { return ___decoder_8; }
	inline Decoder_t3792697818 ** get_address_of_decoder_8() { return &___decoder_8; }
	inline void set_decoder_8(Decoder_t3792697818 * value)
	{
		___decoder_8 = value;
		Il2CppCodeGenWriteBarrier((&___decoder_8), value);
	}

	inline static int32_t get_offset_of_base_stream_9() { return static_cast<int32_t>(offsetof(StreamReader_t2360341767, ___base_stream_9)); }
	inline Stream_t3255436806 * get_base_stream_9() const { return ___base_stream_9; }
	inline Stream_t3255436806 ** get_address_of_base_stream_9() { return &___base_stream_9; }
	inline void set_base_stream_9(Stream_t3255436806 * value)
	{
		___base_stream_9 = value;
		Il2CppCodeGenWriteBarrier((&___base_stream_9), value);
	}

	inline static int32_t get_offset_of_mayBlock_10() { return static_cast<int32_t>(offsetof(StreamReader_t2360341767, ___mayBlock_10)); }
	inline bool get_mayBlock_10() const { return ___mayBlock_10; }
	inline bool* get_address_of_mayBlock_10() { return &___mayBlock_10; }
	inline void set_mayBlock_10(bool value)
	{
		___mayBlock_10 = value;
	}

	inline static int32_t get_offset_of_line_builder_11() { return static_cast<int32_t>(offsetof(StreamReader_t2360341767, ___line_builder_11)); }
	inline StringBuilder_t1221177846 * get_line_builder_11() const { return ___line_builder_11; }
	inline StringBuilder_t1221177846 ** get_address_of_line_builder_11() { return &___line_builder_11; }
	inline void set_line_builder_11(StringBuilder_t1221177846 * value)
	{
		___line_builder_11 = value;
		Il2CppCodeGenWriteBarrier((&___line_builder_11), value);
	}

	inline static int32_t get_offset_of_foundCR_13() { return static_cast<int32_t>(offsetof(StreamReader_t2360341767, ___foundCR_13)); }
	inline bool get_foundCR_13() const { return ___foundCR_13; }
	inline bool* get_address_of_foundCR_13() { return &___foundCR_13; }
	inline void set_foundCR_13(bool value)
	{
		___foundCR_13 = value;
	}
};

struct StreamReader_t2360341767_StaticFields
{
public:
	// System.IO.StreamReader System.IO.StreamReader::Null
	StreamReader_t2360341767 * ___Null_12;

public:
	inline static int32_t get_offset_of_Null_12() { return static_cast<int32_t>(offsetof(StreamReader_t2360341767_StaticFields, ___Null_12)); }
	inline StreamReader_t2360341767 * get_Null_12() const { return ___Null_12; }
	inline StreamReader_t2360341767 ** get_address_of_Null_12() { return &___Null_12; }
	inline void set_Null_12(StreamReader_t2360341767 * value)
	{
		___Null_12 = value;
		Il2CppCodeGenWriteBarrier((&___Null_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAMREADER_T2360341767_H
#ifndef STRINGREADER_T1480123486_H
#define STRINGREADER_T1480123486_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.StringReader
struct  StringReader_t1480123486  : public TextReader_t1561828458
{
public:
	// System.String System.IO.StringReader::source
	String_t* ___source_1;
	// System.Int32 System.IO.StringReader::nextChar
	int32_t ___nextChar_2;
	// System.Int32 System.IO.StringReader::sourceLength
	int32_t ___sourceLength_3;

public:
	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(StringReader_t1480123486, ___source_1)); }
	inline String_t* get_source_1() const { return ___source_1; }
	inline String_t** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(String_t* value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier((&___source_1), value);
	}

	inline static int32_t get_offset_of_nextChar_2() { return static_cast<int32_t>(offsetof(StringReader_t1480123486, ___nextChar_2)); }
	inline int32_t get_nextChar_2() const { return ___nextChar_2; }
	inline int32_t* get_address_of_nextChar_2() { return &___nextChar_2; }
	inline void set_nextChar_2(int32_t value)
	{
		___nextChar_2 = value;
	}

	inline static int32_t get_offset_of_sourceLength_3() { return static_cast<int32_t>(offsetof(StringReader_t1480123486, ___sourceLength_3)); }
	inline int32_t get_sourceLength_3() const { return ___sourceLength_3; }
	inline int32_t* get_address_of_sourceLength_3() { return &___sourceLength_3; }
	inline void set_sourceLength_3(int32_t value)
	{
		___sourceLength_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGREADER_T1480123486_H
#ifndef __STATICARRAYINITTYPESIZEU3D1024_T2165070692_H
#define __STATICARRAYINITTYPESIZEU3D1024_T2165070692_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>{83185D3B-3939-439C-A54F-260F9279D9C8}/__StaticArrayInitTypeSize=1024
struct  __StaticArrayInitTypeSizeU3D1024_t2165070692 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D1024_t2165070692__padding[1024];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D1024_T2165070692_H
#ifndef NOTIMPLEMENTEDEXCEPTION_T2785117854_H
#define NOTIMPLEMENTEDEXCEPTION_T2785117854_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.NotImplementedException
struct  NotImplementedException_t2785117854  : public SystemException_t3877406272
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTIMPLEMENTEDEXCEPTION_T2785117854_H
#ifndef PARSESTATE_T1851939866_H
#define PARSESTATE_T1851939866_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HtmlAgilityPack.HtmlDocument/ParseState
struct  ParseState_t1851939866 
{
public:
	// System.Int32 HtmlAgilityPack.HtmlDocument/ParseState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ParseState_t1851939866, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARSESTATE_T1851939866_H
#ifndef INVALIDPROGRAMEXCEPTION_T3776992292_H
#define INVALIDPROGRAMEXCEPTION_T3776992292_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.InvalidProgramException
struct  InvalidProgramException_t3776992292  : public SystemException_t3877406272
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVALIDPROGRAMEXCEPTION_T3776992292_H
#ifndef XPATHNODETYPE_T817388867_H
#define XPATHNODETYPE_T817388867_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathNodeType
struct  XPathNodeType_t817388867 
{
public:
	// System.Int32 System.Xml.XPath.XPathNodeType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XPathNodeType_t817388867, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHNODETYPE_T817388867_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3EU7B83185D3BU2D3939U2D439CU2DA54FU2D260F9279D9C8U7D_T691081255_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3EU7B83185D3BU2D3939U2D439CU2DA54FU2D260F9279D9C8U7D_T691081255_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>{83185D3B-3939-439C-A54F-260F9279D9C8}
struct  U3CPrivateImplementationDetailsU3EU7B83185D3BU2D3939U2D439CU2DA54FU2D260F9279D9C8U7D_t691081255  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3EU7B83185D3BU2D3939U2D439CU2DA54FU2D260F9279D9C8U7D_t691081255_StaticFields
{
public:
	// System.Int64 <PrivateImplementationDetails>{83185D3B-3939-439C-A54F-260F9279D9C8}::$$method0x600012f-1
	int64_t ___U24U24method0x600012fU2D1_0;
	// <PrivateImplementationDetails>{83185D3B-3939-439C-A54F-260F9279D9C8}/__StaticArrayInitTypeSize=1024 <PrivateImplementationDetails>{83185D3B-3939-439C-A54F-260F9279D9C8}::$$method0x6000287-1
	__StaticArrayInitTypeSizeU3D1024_t2165070692  ___U24U24method0x6000287U2D1_1;

public:
	inline static int32_t get_offset_of_U24U24method0x600012fU2D1_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7B83185D3BU2D3939U2D439CU2DA54FU2D260F9279D9C8U7D_t691081255_StaticFields, ___U24U24method0x600012fU2D1_0)); }
	inline int64_t get_U24U24method0x600012fU2D1_0() const { return ___U24U24method0x600012fU2D1_0; }
	inline int64_t* get_address_of_U24U24method0x600012fU2D1_0() { return &___U24U24method0x600012fU2D1_0; }
	inline void set_U24U24method0x600012fU2D1_0(int64_t value)
	{
		___U24U24method0x600012fU2D1_0 = value;
	}

	inline static int32_t get_offset_of_U24U24method0x6000287U2D1_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7B83185D3BU2D3939U2D439CU2DA54FU2D260F9279D9C8U7D_t691081255_StaticFields, ___U24U24method0x6000287U2D1_1)); }
	inline __StaticArrayInitTypeSizeU3D1024_t2165070692  get_U24U24method0x6000287U2D1_1() const { return ___U24U24method0x6000287U2D1_1; }
	inline __StaticArrayInitTypeSizeU3D1024_t2165070692 * get_address_of_U24U24method0x6000287U2D1_1() { return &___U24U24method0x6000287U2D1_1; }
	inline void set_U24U24method0x6000287U2D1_1(__StaticArrayInitTypeSizeU3D1024_t2165070692  value)
	{
		___U24U24method0x6000287U2D1_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3EU7B83185D3BU2D3939U2D439CU2DA54FU2D260F9279D9C8U7D_T691081255_H
#ifndef RUNTIMEFIELDHANDLE_T2331729674_H
#define RUNTIMEFIELDHANDLE_T2331729674_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeFieldHandle
struct  RuntimeFieldHandle_t2331729674 
{
public:
	// System.IntPtr System.RuntimeFieldHandle::value
	IntPtr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeFieldHandle_t2331729674, ___value_0)); }
	inline IntPtr_t get_value_0() const { return ___value_0; }
	inline IntPtr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(IntPtr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEFIELDHANDLE_T2331729674_H
#ifndef XPATHNAMESPACESCOPE_T3601604274_H
#define XPATHNAMESPACESCOPE_T3601604274_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathNamespaceScope
struct  XPathNamespaceScope_t3601604274 
{
public:
	// System.Int32 System.Xml.XPath.XPathNamespaceScope::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XPathNamespaceScope_t3601604274, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHNAMESPACESCOPE_T3601604274_H
#ifndef HTMLPARSEERRORCODE_T3491517481_H
#define HTMLPARSEERRORCODE_T3491517481_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HtmlAgilityPack.HtmlParseErrorCode
struct  HtmlParseErrorCode_t3491517481 
{
public:
	// System.Int32 HtmlAgilityPack.HtmlParseErrorCode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HtmlParseErrorCode_t3491517481, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTMLPARSEERRORCODE_T3491517481_H
#ifndef STRINGCOMPARISON_T2376310518_H
#define STRINGCOMPARISON_T2376310518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.StringComparison
struct  StringComparison_t2376310518 
{
public:
	// System.Int32 System.StringComparison::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(StringComparison_t2376310518, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGCOMPARISON_T2376310518_H
#ifndef HTMLNODETYPE_T1941241835_H
#define HTMLNODETYPE_T1941241835_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HtmlAgilityPack.HtmlNodeType
struct  HtmlNodeType_t1941241835 
{
public:
	// System.Int32 HtmlAgilityPack.HtmlNodeType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HtmlNodeType_t1941241835, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTMLNODETYPE_T1941241835_H
#ifndef ATTRIBUTEVALUEQUOTE_T3949719843_H
#define ATTRIBUTEVALUEQUOTE_T3949719843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HtmlAgilityPack.AttributeValueQuote
struct  AttributeValueQuote_t3949719843 
{
public:
	// System.Int32 HtmlAgilityPack.AttributeValueQuote::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AttributeValueQuote_t3949719843, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTEVALUEQUOTE_T3949719843_H
#ifndef ENUMERATOR_T744360071_H
#define ENUMERATOR_T744360071_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Enumerator<System.String,HtmlAgilityPack.HtmlAttribute>
struct  Enumerator_t744360071 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::dictionary
	Dictionary_2_t3719302665 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::stamp
	int32_t ___stamp_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::current
	KeyValuePair_2_t1476647887  ___current_3;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_t744360071, ___dictionary_0)); }
	inline Dictionary_2_t3719302665 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t3719302665 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t3719302665 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t744360071, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_stamp_2() { return static_cast<int32_t>(offsetof(Enumerator_t744360071, ___stamp_2)); }
	inline int32_t get_stamp_2() const { return ___stamp_2; }
	inline int32_t* get_address_of_stamp_2() { return &___stamp_2; }
	inline void set_stamp_2(int32_t value)
	{
		___stamp_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t744360071, ___current_3)); }
	inline KeyValuePair_2_t1476647887  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t1476647887 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t1476647887  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T744360071_H
#ifndef HTMLNODENAVIGATOR_T2752606360_H
#define HTMLNODENAVIGATOR_T2752606360_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HtmlAgilityPack.HtmlNodeNavigator
struct  HtmlNodeNavigator_t2752606360  : public XPathNavigator_t3981235968
{
public:
	// System.Int32 HtmlAgilityPack.HtmlNodeNavigator::_attindex
	int32_t ____attindex_2;
	// HtmlAgilityPack.HtmlNode HtmlAgilityPack.HtmlNodeNavigator::_currentnode
	HtmlNode_t2048434459 * ____currentnode_3;
	// HtmlAgilityPack.HtmlDocument HtmlAgilityPack.HtmlNodeNavigator::_doc
	HtmlDocument_t556432108 * ____doc_4;
	// HtmlAgilityPack.HtmlNameTable HtmlAgilityPack.HtmlNodeNavigator::_nametable
	HtmlNameTable_t3848610378 * ____nametable_5;

public:
	inline static int32_t get_offset_of__attindex_2() { return static_cast<int32_t>(offsetof(HtmlNodeNavigator_t2752606360, ____attindex_2)); }
	inline int32_t get__attindex_2() const { return ____attindex_2; }
	inline int32_t* get_address_of__attindex_2() { return &____attindex_2; }
	inline void set__attindex_2(int32_t value)
	{
		____attindex_2 = value;
	}

	inline static int32_t get_offset_of__currentnode_3() { return static_cast<int32_t>(offsetof(HtmlNodeNavigator_t2752606360, ____currentnode_3)); }
	inline HtmlNode_t2048434459 * get__currentnode_3() const { return ____currentnode_3; }
	inline HtmlNode_t2048434459 ** get_address_of__currentnode_3() { return &____currentnode_3; }
	inline void set__currentnode_3(HtmlNode_t2048434459 * value)
	{
		____currentnode_3 = value;
		Il2CppCodeGenWriteBarrier((&____currentnode_3), value);
	}

	inline static int32_t get_offset_of__doc_4() { return static_cast<int32_t>(offsetof(HtmlNodeNavigator_t2752606360, ____doc_4)); }
	inline HtmlDocument_t556432108 * get__doc_4() const { return ____doc_4; }
	inline HtmlDocument_t556432108 ** get_address_of__doc_4() { return &____doc_4; }
	inline void set__doc_4(HtmlDocument_t556432108 * value)
	{
		____doc_4 = value;
		Il2CppCodeGenWriteBarrier((&____doc_4), value);
	}

	inline static int32_t get_offset_of__nametable_5() { return static_cast<int32_t>(offsetof(HtmlNodeNavigator_t2752606360, ____nametable_5)); }
	inline HtmlNameTable_t3848610378 * get__nametable_5() const { return ____nametable_5; }
	inline HtmlNameTable_t3848610378 ** get_address_of__nametable_5() { return &____nametable_5; }
	inline void set__nametable_5(HtmlNameTable_t3848610378 * value)
	{
		____nametable_5 = value;
		Il2CppCodeGenWriteBarrier((&____nametable_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTMLNODENAVIGATOR_T2752606360_H
#ifndef REGEXOPTIONS_T2418259727_H
#define REGEXOPTIONS_T2418259727_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexOptions
struct  RegexOptions_t2418259727 
{
public:
	// System.Int32 System.Text.RegularExpressions.RegexOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RegexOptions_t2418259727, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEXOPTIONS_T2418259727_H
#ifndef ARGUMENTEXCEPTION_T3259014390_H
#define ARGUMENTEXCEPTION_T3259014390_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArgumentException
struct  ArgumentException_t3259014390  : public SystemException_t3877406272
{
public:
	// System.String System.ArgumentException::param_name
	String_t* ___param_name_12;

public:
	inline static int32_t get_offset_of_param_name_12() { return static_cast<int32_t>(offsetof(ArgumentException_t3259014390, ___param_name_12)); }
	inline String_t* get_param_name_12() const { return ___param_name_12; }
	inline String_t** get_address_of_param_name_12() { return &___param_name_12; }
	inline void set_param_name_12(String_t* value)
	{
		___param_name_12 = value;
		Il2CppCodeGenWriteBarrier((&___param_name_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTEXCEPTION_T3259014390_H
#ifndef HTMLELEMENTFLAG_T260274357_H
#define HTMLELEMENTFLAG_T260274357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HtmlAgilityPack.HtmlElementFlag
struct  HtmlElementFlag_t260274357 
{
public:
	// System.Int32 HtmlAgilityPack.HtmlElementFlag::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HtmlElementFlag_t260274357, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTMLELEMENTFLAG_T260274357_H
#ifndef ENUMERATOR_T2376284796_H
#define ENUMERATOR_T2376284796_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,HtmlAgilityPack.HtmlNode>
struct  Enumerator_t2376284796 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::dictionary
	Dictionary_2_t1056260094 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::stamp
	int32_t ___stamp_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::current
	KeyValuePair_2_t3108572612  ___current_3;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_t2376284796, ___dictionary_0)); }
	inline Dictionary_2_t1056260094 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t1056260094 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t1056260094 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t2376284796, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_stamp_2() { return static_cast<int32_t>(offsetof(Enumerator_t2376284796, ___stamp_2)); }
	inline int32_t get_stamp_2() const { return ___stamp_2; }
	inline int32_t* get_address_of_stamp_2() { return &___stamp_2; }
	inline void set_stamp_2(int32_t value)
	{
		___stamp_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t2376284796, ___current_3)); }
	inline KeyValuePair_2_t3108572612  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t3108572612 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t3108572612  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2376284796_H
#ifndef ENUMERATOR_T3601534125_H
#define ENUMERATOR_T3601534125_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>
struct  Enumerator_t3601534125 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::dictionary
	Dictionary_2_t2281509423 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::stamp
	int32_t ___stamp_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::current
	KeyValuePair_2_t38854645  ___current_3;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_t3601534125, ___dictionary_0)); }
	inline Dictionary_2_t2281509423 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t2281509423 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t2281509423 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t3601534125, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_stamp_2() { return static_cast<int32_t>(offsetof(Enumerator_t3601534125, ___stamp_2)); }
	inline int32_t get_stamp_2() const { return ___stamp_2; }
	inline int32_t* get_address_of_stamp_2() { return &___stamp_2; }
	inline void set_stamp_2(int32_t value)
	{
		___stamp_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t3601534125, ___current_3)); }
	inline KeyValuePair_2_t38854645  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t38854645 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t38854645  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T3601534125_H
#ifndef ENUMERATOR_T3017299632_H
#define ENUMERATOR_T3017299632_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>
struct  Enumerator_t3017299632 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::dictionary
	Dictionary_2_t1697274930 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::stamp
	int32_t ___stamp_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::current
	KeyValuePair_2_t3749587448  ___current_3;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_t3017299632, ___dictionary_0)); }
	inline Dictionary_2_t1697274930 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t1697274930 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t1697274930 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t3017299632, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_stamp_2() { return static_cast<int32_t>(offsetof(Enumerator_t3017299632, ___stamp_2)); }
	inline int32_t get_stamp_2() const { return ___stamp_2; }
	inline int32_t* get_address_of_stamp_2() { return &___stamp_2; }
	inline void set_stamp_2(int32_t value)
	{
		___stamp_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t3017299632, ___current_3)); }
	inline KeyValuePair_2_t3749587448  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t3749587448 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t3749587448  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T3017299632_H
#ifndef REGEX_T1803876613_H
#define REGEX_T1803876613_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Regex
struct  Regex_t1803876613  : public RuntimeObject
{
public:
	// System.Text.RegularExpressions.IMachineFactory System.Text.RegularExpressions.Regex::machineFactory
	RuntimeObject* ___machineFactory_1;
	// System.Collections.IDictionary System.Text.RegularExpressions.Regex::mapping
	RuntimeObject* ___mapping_2;
	// System.Int32 System.Text.RegularExpressions.Regex::group_count
	int32_t ___group_count_3;
	// System.Int32 System.Text.RegularExpressions.Regex::gap
	int32_t ___gap_4;
	// System.String[] System.Text.RegularExpressions.Regex::group_names
	StringU5BU5D_t1642385972* ___group_names_5;
	// System.Int32[] System.Text.RegularExpressions.Regex::group_numbers
	Int32U5BU5D_t3030399641* ___group_numbers_6;
	// System.String System.Text.RegularExpressions.Regex::pattern
	String_t* ___pattern_7;
	// System.Text.RegularExpressions.RegexOptions System.Text.RegularExpressions.Regex::roptions
	int32_t ___roptions_8;

public:
	inline static int32_t get_offset_of_machineFactory_1() { return static_cast<int32_t>(offsetof(Regex_t1803876613, ___machineFactory_1)); }
	inline RuntimeObject* get_machineFactory_1() const { return ___machineFactory_1; }
	inline RuntimeObject** get_address_of_machineFactory_1() { return &___machineFactory_1; }
	inline void set_machineFactory_1(RuntimeObject* value)
	{
		___machineFactory_1 = value;
		Il2CppCodeGenWriteBarrier((&___machineFactory_1), value);
	}

	inline static int32_t get_offset_of_mapping_2() { return static_cast<int32_t>(offsetof(Regex_t1803876613, ___mapping_2)); }
	inline RuntimeObject* get_mapping_2() const { return ___mapping_2; }
	inline RuntimeObject** get_address_of_mapping_2() { return &___mapping_2; }
	inline void set_mapping_2(RuntimeObject* value)
	{
		___mapping_2 = value;
		Il2CppCodeGenWriteBarrier((&___mapping_2), value);
	}

	inline static int32_t get_offset_of_group_count_3() { return static_cast<int32_t>(offsetof(Regex_t1803876613, ___group_count_3)); }
	inline int32_t get_group_count_3() const { return ___group_count_3; }
	inline int32_t* get_address_of_group_count_3() { return &___group_count_3; }
	inline void set_group_count_3(int32_t value)
	{
		___group_count_3 = value;
	}

	inline static int32_t get_offset_of_gap_4() { return static_cast<int32_t>(offsetof(Regex_t1803876613, ___gap_4)); }
	inline int32_t get_gap_4() const { return ___gap_4; }
	inline int32_t* get_address_of_gap_4() { return &___gap_4; }
	inline void set_gap_4(int32_t value)
	{
		___gap_4 = value;
	}

	inline static int32_t get_offset_of_group_names_5() { return static_cast<int32_t>(offsetof(Regex_t1803876613, ___group_names_5)); }
	inline StringU5BU5D_t1642385972* get_group_names_5() const { return ___group_names_5; }
	inline StringU5BU5D_t1642385972** get_address_of_group_names_5() { return &___group_names_5; }
	inline void set_group_names_5(StringU5BU5D_t1642385972* value)
	{
		___group_names_5 = value;
		Il2CppCodeGenWriteBarrier((&___group_names_5), value);
	}

	inline static int32_t get_offset_of_group_numbers_6() { return static_cast<int32_t>(offsetof(Regex_t1803876613, ___group_numbers_6)); }
	inline Int32U5BU5D_t3030399641* get_group_numbers_6() const { return ___group_numbers_6; }
	inline Int32U5BU5D_t3030399641** get_address_of_group_numbers_6() { return &___group_numbers_6; }
	inline void set_group_numbers_6(Int32U5BU5D_t3030399641* value)
	{
		___group_numbers_6 = value;
		Il2CppCodeGenWriteBarrier((&___group_numbers_6), value);
	}

	inline static int32_t get_offset_of_pattern_7() { return static_cast<int32_t>(offsetof(Regex_t1803876613, ___pattern_7)); }
	inline String_t* get_pattern_7() const { return ___pattern_7; }
	inline String_t** get_address_of_pattern_7() { return &___pattern_7; }
	inline void set_pattern_7(String_t* value)
	{
		___pattern_7 = value;
		Il2CppCodeGenWriteBarrier((&___pattern_7), value);
	}

	inline static int32_t get_offset_of_roptions_8() { return static_cast<int32_t>(offsetof(Regex_t1803876613, ___roptions_8)); }
	inline int32_t get_roptions_8() const { return ___roptions_8; }
	inline int32_t* get_address_of_roptions_8() { return &___roptions_8; }
	inline void set_roptions_8(int32_t value)
	{
		___roptions_8 = value;
	}
};

struct Regex_t1803876613_StaticFields
{
public:
	// System.Text.RegularExpressions.FactoryCache System.Text.RegularExpressions.Regex::cache
	FactoryCache_t2051534610 * ___cache_0;

public:
	inline static int32_t get_offset_of_cache_0() { return static_cast<int32_t>(offsetof(Regex_t1803876613_StaticFields, ___cache_0)); }
	inline FactoryCache_t2051534610 * get_cache_0() const { return ___cache_0; }
	inline FactoryCache_t2051534610 ** get_address_of_cache_0() { return &___cache_0; }
	inline void set_cache_0(FactoryCache_t2051534610 * value)
	{
		___cache_0 = value;
		Il2CppCodeGenWriteBarrier((&___cache_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEX_T1803876613_H
#ifndef ENUMERATOR_T1110868133_H
#define ENUMERATOR_T1110868133_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,HtmlAgilityPack.HtmlAttribute>
struct  Enumerator_t1110868133 
{
public:
	// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator::host_enumerator
	Enumerator_t744360071  ___host_enumerator_0;

public:
	inline static int32_t get_offset_of_host_enumerator_0() { return static_cast<int32_t>(offsetof(Enumerator_t1110868133, ___host_enumerator_0)); }
	inline Enumerator_t744360071  get_host_enumerator_0() const { return ___host_enumerator_0; }
	inline Enumerator_t744360071 * get_address_of_host_enumerator_0() { return &___host_enumerator_0; }
	inline void set_host_enumerator_0(Enumerator_t744360071  value)
	{
		___host_enumerator_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T1110868133_H
#ifndef ENUMERATOR_T2742792858_H
#define ENUMERATOR_T2742792858_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,HtmlAgilityPack.HtmlNode>
struct  Enumerator_t2742792858 
{
public:
	// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator::host_enumerator
	Enumerator_t2376284796  ___host_enumerator_0;

public:
	inline static int32_t get_offset_of_host_enumerator_0() { return static_cast<int32_t>(offsetof(Enumerator_t2742792858, ___host_enumerator_0)); }
	inline Enumerator_t2376284796  get_host_enumerator_0() const { return ___host_enumerator_0; }
	inline Enumerator_t2376284796 * get_address_of_host_enumerator_0() { return &___host_enumerator_0; }
	inline void set_host_enumerator_0(Enumerator_t2376284796  value)
	{
		___host_enumerator_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2742792858_H
#ifndef HTMLPARSEERROR_T1115179162_H
#define HTMLPARSEERROR_T1115179162_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HtmlAgilityPack.HtmlParseError
struct  HtmlParseError_t1115179162  : public RuntimeObject
{
public:
	// HtmlAgilityPack.HtmlParseErrorCode HtmlAgilityPack.HtmlParseError::_code
	int32_t ____code_0;
	// System.Int32 HtmlAgilityPack.HtmlParseError::_line
	int32_t ____line_1;
	// System.Int32 HtmlAgilityPack.HtmlParseError::_linePosition
	int32_t ____linePosition_2;
	// System.String HtmlAgilityPack.HtmlParseError::_reason
	String_t* ____reason_3;
	// System.String HtmlAgilityPack.HtmlParseError::_sourceText
	String_t* ____sourceText_4;
	// System.Int32 HtmlAgilityPack.HtmlParseError::_streamPosition
	int32_t ____streamPosition_5;

public:
	inline static int32_t get_offset_of__code_0() { return static_cast<int32_t>(offsetof(HtmlParseError_t1115179162, ____code_0)); }
	inline int32_t get__code_0() const { return ____code_0; }
	inline int32_t* get_address_of__code_0() { return &____code_0; }
	inline void set__code_0(int32_t value)
	{
		____code_0 = value;
	}

	inline static int32_t get_offset_of__line_1() { return static_cast<int32_t>(offsetof(HtmlParseError_t1115179162, ____line_1)); }
	inline int32_t get__line_1() const { return ____line_1; }
	inline int32_t* get_address_of__line_1() { return &____line_1; }
	inline void set__line_1(int32_t value)
	{
		____line_1 = value;
	}

	inline static int32_t get_offset_of__linePosition_2() { return static_cast<int32_t>(offsetof(HtmlParseError_t1115179162, ____linePosition_2)); }
	inline int32_t get__linePosition_2() const { return ____linePosition_2; }
	inline int32_t* get_address_of__linePosition_2() { return &____linePosition_2; }
	inline void set__linePosition_2(int32_t value)
	{
		____linePosition_2 = value;
	}

	inline static int32_t get_offset_of__reason_3() { return static_cast<int32_t>(offsetof(HtmlParseError_t1115179162, ____reason_3)); }
	inline String_t* get__reason_3() const { return ____reason_3; }
	inline String_t** get_address_of__reason_3() { return &____reason_3; }
	inline void set__reason_3(String_t* value)
	{
		____reason_3 = value;
		Il2CppCodeGenWriteBarrier((&____reason_3), value);
	}

	inline static int32_t get_offset_of__sourceText_4() { return static_cast<int32_t>(offsetof(HtmlParseError_t1115179162, ____sourceText_4)); }
	inline String_t* get__sourceText_4() const { return ____sourceText_4; }
	inline String_t** get_address_of__sourceText_4() { return &____sourceText_4; }
	inline void set__sourceText_4(String_t* value)
	{
		____sourceText_4 = value;
		Il2CppCodeGenWriteBarrier((&____sourceText_4), value);
	}

	inline static int32_t get_offset_of__streamPosition_5() { return static_cast<int32_t>(offsetof(HtmlParseError_t1115179162, ____streamPosition_5)); }
	inline int32_t get__streamPosition_5() const { return ____streamPosition_5; }
	inline int32_t* get_address_of__streamPosition_5() { return &____streamPosition_5; }
	inline void set__streamPosition_5(int32_t value)
	{
		____streamPosition_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTMLPARSEERROR_T1115179162_H
#ifndef ENUMERATOR_T3383807694_H
#define ENUMERATOR_T3383807694_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>
struct  Enumerator_t3383807694 
{
public:
	// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator::host_enumerator
	Enumerator_t3017299632  ___host_enumerator_0;

public:
	inline static int32_t get_offset_of_host_enumerator_0() { return static_cast<int32_t>(offsetof(Enumerator_t3383807694, ___host_enumerator_0)); }
	inline Enumerator_t3017299632  get_host_enumerator_0() const { return ___host_enumerator_0; }
	inline Enumerator_t3017299632 * get_address_of_host_enumerator_0() { return &___host_enumerator_0; }
	inline void set_host_enumerator_0(Enumerator_t3017299632  value)
	{
		___host_enumerator_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T3383807694_H
#ifndef ENUMERATOR_T3968042187_H
#define ENUMERATOR_T3968042187_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>
struct  Enumerator_t3968042187 
{
public:
	// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator::host_enumerator
	Enumerator_t3601534125  ___host_enumerator_0;

public:
	inline static int32_t get_offset_of_host_enumerator_0() { return static_cast<int32_t>(offsetof(Enumerator_t3968042187, ___host_enumerator_0)); }
	inline Enumerator_t3601534125  get_host_enumerator_0() const { return ___host_enumerator_0; }
	inline Enumerator_t3601534125 * get_address_of_host_enumerator_0() { return &___host_enumerator_0; }
	inline void set_host_enumerator_0(Enumerator_t3601534125  value)
	{
		___host_enumerator_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T3968042187_H
#ifndef ARGUMENTNULLEXCEPTION_T628810857_H
#define ARGUMENTNULLEXCEPTION_T628810857_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArgumentNullException
struct  ArgumentNullException_t628810857  : public ArgumentException_t3259014390
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTNULLEXCEPTION_T628810857_H
#ifndef HTMLDOCUMENT_T556432108_H
#define HTMLDOCUMENT_T556432108_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HtmlAgilityPack.HtmlDocument
struct  HtmlDocument_t556432108  : public RuntimeObject
{
public:
	// System.Int32 HtmlAgilityPack.HtmlDocument::_c
	int32_t ____c_0;
	// HtmlAgilityPack.Crc32 HtmlAgilityPack.HtmlDocument::_crc32
	Crc32_t83196531 * ____crc32_1;
	// HtmlAgilityPack.HtmlAttribute HtmlAgilityPack.HtmlDocument::_currentattribute
	HtmlAttribute_t1804523403 * ____currentattribute_2;
	// HtmlAgilityPack.HtmlNode HtmlAgilityPack.HtmlDocument::_currentnode
	HtmlNode_t2048434459 * ____currentnode_3;
	// System.Text.Encoding HtmlAgilityPack.HtmlDocument::_declaredencoding
	Encoding_t663144255 * ____declaredencoding_4;
	// HtmlAgilityPack.HtmlNode HtmlAgilityPack.HtmlDocument::_documentnode
	HtmlNode_t2048434459 * ____documentnode_5;
	// System.Boolean HtmlAgilityPack.HtmlDocument::_fullcomment
	bool ____fullcomment_6;
	// System.Int32 HtmlAgilityPack.HtmlDocument::_index
	int32_t ____index_7;
	// System.Collections.Generic.Dictionary`2<System.String,HtmlAgilityPack.HtmlNode> HtmlAgilityPack.HtmlDocument::Lastnodes
	Dictionary_2_t3963213721 * ___Lastnodes_8;
	// HtmlAgilityPack.HtmlNode HtmlAgilityPack.HtmlDocument::_lastparentnode
	HtmlNode_t2048434459 * ____lastparentnode_9;
	// System.Int32 HtmlAgilityPack.HtmlDocument::_line
	int32_t ____line_10;
	// System.Int32 HtmlAgilityPack.HtmlDocument::_lineposition
	int32_t ____lineposition_11;
	// System.Int32 HtmlAgilityPack.HtmlDocument::_maxlineposition
	int32_t ____maxlineposition_12;
	// System.Collections.Generic.Dictionary`2<System.String,HtmlAgilityPack.HtmlNode> HtmlAgilityPack.HtmlDocument::Nodesid
	Dictionary_2_t3963213721 * ___Nodesid_13;
	// HtmlAgilityPack.HtmlDocument/ParseState HtmlAgilityPack.HtmlDocument::_oldstate
	int32_t ____oldstate_14;
	// System.Boolean HtmlAgilityPack.HtmlDocument::_onlyDetectEncoding
	bool ____onlyDetectEncoding_15;
	// System.Collections.Generic.Dictionary`2<System.Int32,HtmlAgilityPack.HtmlNode> HtmlAgilityPack.HtmlDocument::Openednodes
	Dictionary_2_t1056260094 * ___Openednodes_16;
	// System.Collections.Generic.List`1<HtmlAgilityPack.HtmlParseError> HtmlAgilityPack.HtmlDocument::_parseerrors
	List_1_t484300294 * ____parseerrors_17;
	// System.String HtmlAgilityPack.HtmlDocument::_remainder
	String_t* ____remainder_18;
	// System.Int32 HtmlAgilityPack.HtmlDocument::_remainderOffset
	int32_t ____remainderOffset_19;
	// HtmlAgilityPack.HtmlDocument/ParseState HtmlAgilityPack.HtmlDocument::_state
	int32_t ____state_20;
	// System.Text.Encoding HtmlAgilityPack.HtmlDocument::_streamencoding
	Encoding_t663144255 * ____streamencoding_21;
	// System.String HtmlAgilityPack.HtmlDocument::Text
	String_t* ___Text_22;
	// System.Boolean HtmlAgilityPack.HtmlDocument::OptionAddDebuggingAttributes
	bool ___OptionAddDebuggingAttributes_23;
	// System.Boolean HtmlAgilityPack.HtmlDocument::OptionAutoCloseOnEnd
	bool ___OptionAutoCloseOnEnd_24;
	// System.Boolean HtmlAgilityPack.HtmlDocument::OptionCheckSyntax
	bool ___OptionCheckSyntax_25;
	// System.Boolean HtmlAgilityPack.HtmlDocument::OptionComputeChecksum
	bool ___OptionComputeChecksum_26;
	// System.Text.Encoding HtmlAgilityPack.HtmlDocument::OptionDefaultStreamEncoding
	Encoding_t663144255 * ___OptionDefaultStreamEncoding_27;
	// System.Boolean HtmlAgilityPack.HtmlDocument::OptionExtractErrorSourceText
	bool ___OptionExtractErrorSourceText_28;
	// System.Int32 HtmlAgilityPack.HtmlDocument::OptionExtractErrorSourceTextMaxLength
	int32_t ___OptionExtractErrorSourceTextMaxLength_29;
	// System.Boolean HtmlAgilityPack.HtmlDocument::OptionFixNestedTags
	bool ___OptionFixNestedTags_30;
	// System.Boolean HtmlAgilityPack.HtmlDocument::OptionOutputAsXml
	bool ___OptionOutputAsXml_31;
	// System.Boolean HtmlAgilityPack.HtmlDocument::OptionOutputOptimizeAttributeValues
	bool ___OptionOutputOptimizeAttributeValues_32;
	// System.Boolean HtmlAgilityPack.HtmlDocument::OptionOutputOriginalCase
	bool ___OptionOutputOriginalCase_33;
	// System.Boolean HtmlAgilityPack.HtmlDocument::OptionOutputUpperCase
	bool ___OptionOutputUpperCase_34;
	// System.Boolean HtmlAgilityPack.HtmlDocument::OptionReadEncoding
	bool ___OptionReadEncoding_35;
	// System.String HtmlAgilityPack.HtmlDocument::OptionStopperNodeName
	String_t* ___OptionStopperNodeName_36;
	// System.Boolean HtmlAgilityPack.HtmlDocument::OptionUseIdAttribute
	bool ___OptionUseIdAttribute_37;
	// System.Boolean HtmlAgilityPack.HtmlDocument::OptionWriteEmptyNodes
	bool ___OptionWriteEmptyNodes_38;

public:
	inline static int32_t get_offset_of__c_0() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ____c_0)); }
	inline int32_t get__c_0() const { return ____c_0; }
	inline int32_t* get_address_of__c_0() { return &____c_0; }
	inline void set__c_0(int32_t value)
	{
		____c_0 = value;
	}

	inline static int32_t get_offset_of__crc32_1() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ____crc32_1)); }
	inline Crc32_t83196531 * get__crc32_1() const { return ____crc32_1; }
	inline Crc32_t83196531 ** get_address_of__crc32_1() { return &____crc32_1; }
	inline void set__crc32_1(Crc32_t83196531 * value)
	{
		____crc32_1 = value;
		Il2CppCodeGenWriteBarrier((&____crc32_1), value);
	}

	inline static int32_t get_offset_of__currentattribute_2() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ____currentattribute_2)); }
	inline HtmlAttribute_t1804523403 * get__currentattribute_2() const { return ____currentattribute_2; }
	inline HtmlAttribute_t1804523403 ** get_address_of__currentattribute_2() { return &____currentattribute_2; }
	inline void set__currentattribute_2(HtmlAttribute_t1804523403 * value)
	{
		____currentattribute_2 = value;
		Il2CppCodeGenWriteBarrier((&____currentattribute_2), value);
	}

	inline static int32_t get_offset_of__currentnode_3() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ____currentnode_3)); }
	inline HtmlNode_t2048434459 * get__currentnode_3() const { return ____currentnode_3; }
	inline HtmlNode_t2048434459 ** get_address_of__currentnode_3() { return &____currentnode_3; }
	inline void set__currentnode_3(HtmlNode_t2048434459 * value)
	{
		____currentnode_3 = value;
		Il2CppCodeGenWriteBarrier((&____currentnode_3), value);
	}

	inline static int32_t get_offset_of__declaredencoding_4() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ____declaredencoding_4)); }
	inline Encoding_t663144255 * get__declaredencoding_4() const { return ____declaredencoding_4; }
	inline Encoding_t663144255 ** get_address_of__declaredencoding_4() { return &____declaredencoding_4; }
	inline void set__declaredencoding_4(Encoding_t663144255 * value)
	{
		____declaredencoding_4 = value;
		Il2CppCodeGenWriteBarrier((&____declaredencoding_4), value);
	}

	inline static int32_t get_offset_of__documentnode_5() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ____documentnode_5)); }
	inline HtmlNode_t2048434459 * get__documentnode_5() const { return ____documentnode_5; }
	inline HtmlNode_t2048434459 ** get_address_of__documentnode_5() { return &____documentnode_5; }
	inline void set__documentnode_5(HtmlNode_t2048434459 * value)
	{
		____documentnode_5 = value;
		Il2CppCodeGenWriteBarrier((&____documentnode_5), value);
	}

	inline static int32_t get_offset_of__fullcomment_6() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ____fullcomment_6)); }
	inline bool get__fullcomment_6() const { return ____fullcomment_6; }
	inline bool* get_address_of__fullcomment_6() { return &____fullcomment_6; }
	inline void set__fullcomment_6(bool value)
	{
		____fullcomment_6 = value;
	}

	inline static int32_t get_offset_of__index_7() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ____index_7)); }
	inline int32_t get__index_7() const { return ____index_7; }
	inline int32_t* get_address_of__index_7() { return &____index_7; }
	inline void set__index_7(int32_t value)
	{
		____index_7 = value;
	}

	inline static int32_t get_offset_of_Lastnodes_8() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ___Lastnodes_8)); }
	inline Dictionary_2_t3963213721 * get_Lastnodes_8() const { return ___Lastnodes_8; }
	inline Dictionary_2_t3963213721 ** get_address_of_Lastnodes_8() { return &___Lastnodes_8; }
	inline void set_Lastnodes_8(Dictionary_2_t3963213721 * value)
	{
		___Lastnodes_8 = value;
		Il2CppCodeGenWriteBarrier((&___Lastnodes_8), value);
	}

	inline static int32_t get_offset_of__lastparentnode_9() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ____lastparentnode_9)); }
	inline HtmlNode_t2048434459 * get__lastparentnode_9() const { return ____lastparentnode_9; }
	inline HtmlNode_t2048434459 ** get_address_of__lastparentnode_9() { return &____lastparentnode_9; }
	inline void set__lastparentnode_9(HtmlNode_t2048434459 * value)
	{
		____lastparentnode_9 = value;
		Il2CppCodeGenWriteBarrier((&____lastparentnode_9), value);
	}

	inline static int32_t get_offset_of__line_10() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ____line_10)); }
	inline int32_t get__line_10() const { return ____line_10; }
	inline int32_t* get_address_of__line_10() { return &____line_10; }
	inline void set__line_10(int32_t value)
	{
		____line_10 = value;
	}

	inline static int32_t get_offset_of__lineposition_11() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ____lineposition_11)); }
	inline int32_t get__lineposition_11() const { return ____lineposition_11; }
	inline int32_t* get_address_of__lineposition_11() { return &____lineposition_11; }
	inline void set__lineposition_11(int32_t value)
	{
		____lineposition_11 = value;
	}

	inline static int32_t get_offset_of__maxlineposition_12() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ____maxlineposition_12)); }
	inline int32_t get__maxlineposition_12() const { return ____maxlineposition_12; }
	inline int32_t* get_address_of__maxlineposition_12() { return &____maxlineposition_12; }
	inline void set__maxlineposition_12(int32_t value)
	{
		____maxlineposition_12 = value;
	}

	inline static int32_t get_offset_of_Nodesid_13() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ___Nodesid_13)); }
	inline Dictionary_2_t3963213721 * get_Nodesid_13() const { return ___Nodesid_13; }
	inline Dictionary_2_t3963213721 ** get_address_of_Nodesid_13() { return &___Nodesid_13; }
	inline void set_Nodesid_13(Dictionary_2_t3963213721 * value)
	{
		___Nodesid_13 = value;
		Il2CppCodeGenWriteBarrier((&___Nodesid_13), value);
	}

	inline static int32_t get_offset_of__oldstate_14() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ____oldstate_14)); }
	inline int32_t get__oldstate_14() const { return ____oldstate_14; }
	inline int32_t* get_address_of__oldstate_14() { return &____oldstate_14; }
	inline void set__oldstate_14(int32_t value)
	{
		____oldstate_14 = value;
	}

	inline static int32_t get_offset_of__onlyDetectEncoding_15() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ____onlyDetectEncoding_15)); }
	inline bool get__onlyDetectEncoding_15() const { return ____onlyDetectEncoding_15; }
	inline bool* get_address_of__onlyDetectEncoding_15() { return &____onlyDetectEncoding_15; }
	inline void set__onlyDetectEncoding_15(bool value)
	{
		____onlyDetectEncoding_15 = value;
	}

	inline static int32_t get_offset_of_Openednodes_16() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ___Openednodes_16)); }
	inline Dictionary_2_t1056260094 * get_Openednodes_16() const { return ___Openednodes_16; }
	inline Dictionary_2_t1056260094 ** get_address_of_Openednodes_16() { return &___Openednodes_16; }
	inline void set_Openednodes_16(Dictionary_2_t1056260094 * value)
	{
		___Openednodes_16 = value;
		Il2CppCodeGenWriteBarrier((&___Openednodes_16), value);
	}

	inline static int32_t get_offset_of__parseerrors_17() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ____parseerrors_17)); }
	inline List_1_t484300294 * get__parseerrors_17() const { return ____parseerrors_17; }
	inline List_1_t484300294 ** get_address_of__parseerrors_17() { return &____parseerrors_17; }
	inline void set__parseerrors_17(List_1_t484300294 * value)
	{
		____parseerrors_17 = value;
		Il2CppCodeGenWriteBarrier((&____parseerrors_17), value);
	}

	inline static int32_t get_offset_of__remainder_18() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ____remainder_18)); }
	inline String_t* get__remainder_18() const { return ____remainder_18; }
	inline String_t** get_address_of__remainder_18() { return &____remainder_18; }
	inline void set__remainder_18(String_t* value)
	{
		____remainder_18 = value;
		Il2CppCodeGenWriteBarrier((&____remainder_18), value);
	}

	inline static int32_t get_offset_of__remainderOffset_19() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ____remainderOffset_19)); }
	inline int32_t get__remainderOffset_19() const { return ____remainderOffset_19; }
	inline int32_t* get_address_of__remainderOffset_19() { return &____remainderOffset_19; }
	inline void set__remainderOffset_19(int32_t value)
	{
		____remainderOffset_19 = value;
	}

	inline static int32_t get_offset_of__state_20() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ____state_20)); }
	inline int32_t get__state_20() const { return ____state_20; }
	inline int32_t* get_address_of__state_20() { return &____state_20; }
	inline void set__state_20(int32_t value)
	{
		____state_20 = value;
	}

	inline static int32_t get_offset_of__streamencoding_21() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ____streamencoding_21)); }
	inline Encoding_t663144255 * get__streamencoding_21() const { return ____streamencoding_21; }
	inline Encoding_t663144255 ** get_address_of__streamencoding_21() { return &____streamencoding_21; }
	inline void set__streamencoding_21(Encoding_t663144255 * value)
	{
		____streamencoding_21 = value;
		Il2CppCodeGenWriteBarrier((&____streamencoding_21), value);
	}

	inline static int32_t get_offset_of_Text_22() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ___Text_22)); }
	inline String_t* get_Text_22() const { return ___Text_22; }
	inline String_t** get_address_of_Text_22() { return &___Text_22; }
	inline void set_Text_22(String_t* value)
	{
		___Text_22 = value;
		Il2CppCodeGenWriteBarrier((&___Text_22), value);
	}

	inline static int32_t get_offset_of_OptionAddDebuggingAttributes_23() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ___OptionAddDebuggingAttributes_23)); }
	inline bool get_OptionAddDebuggingAttributes_23() const { return ___OptionAddDebuggingAttributes_23; }
	inline bool* get_address_of_OptionAddDebuggingAttributes_23() { return &___OptionAddDebuggingAttributes_23; }
	inline void set_OptionAddDebuggingAttributes_23(bool value)
	{
		___OptionAddDebuggingAttributes_23 = value;
	}

	inline static int32_t get_offset_of_OptionAutoCloseOnEnd_24() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ___OptionAutoCloseOnEnd_24)); }
	inline bool get_OptionAutoCloseOnEnd_24() const { return ___OptionAutoCloseOnEnd_24; }
	inline bool* get_address_of_OptionAutoCloseOnEnd_24() { return &___OptionAutoCloseOnEnd_24; }
	inline void set_OptionAutoCloseOnEnd_24(bool value)
	{
		___OptionAutoCloseOnEnd_24 = value;
	}

	inline static int32_t get_offset_of_OptionCheckSyntax_25() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ___OptionCheckSyntax_25)); }
	inline bool get_OptionCheckSyntax_25() const { return ___OptionCheckSyntax_25; }
	inline bool* get_address_of_OptionCheckSyntax_25() { return &___OptionCheckSyntax_25; }
	inline void set_OptionCheckSyntax_25(bool value)
	{
		___OptionCheckSyntax_25 = value;
	}

	inline static int32_t get_offset_of_OptionComputeChecksum_26() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ___OptionComputeChecksum_26)); }
	inline bool get_OptionComputeChecksum_26() const { return ___OptionComputeChecksum_26; }
	inline bool* get_address_of_OptionComputeChecksum_26() { return &___OptionComputeChecksum_26; }
	inline void set_OptionComputeChecksum_26(bool value)
	{
		___OptionComputeChecksum_26 = value;
	}

	inline static int32_t get_offset_of_OptionDefaultStreamEncoding_27() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ___OptionDefaultStreamEncoding_27)); }
	inline Encoding_t663144255 * get_OptionDefaultStreamEncoding_27() const { return ___OptionDefaultStreamEncoding_27; }
	inline Encoding_t663144255 ** get_address_of_OptionDefaultStreamEncoding_27() { return &___OptionDefaultStreamEncoding_27; }
	inline void set_OptionDefaultStreamEncoding_27(Encoding_t663144255 * value)
	{
		___OptionDefaultStreamEncoding_27 = value;
		Il2CppCodeGenWriteBarrier((&___OptionDefaultStreamEncoding_27), value);
	}

	inline static int32_t get_offset_of_OptionExtractErrorSourceText_28() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ___OptionExtractErrorSourceText_28)); }
	inline bool get_OptionExtractErrorSourceText_28() const { return ___OptionExtractErrorSourceText_28; }
	inline bool* get_address_of_OptionExtractErrorSourceText_28() { return &___OptionExtractErrorSourceText_28; }
	inline void set_OptionExtractErrorSourceText_28(bool value)
	{
		___OptionExtractErrorSourceText_28 = value;
	}

	inline static int32_t get_offset_of_OptionExtractErrorSourceTextMaxLength_29() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ___OptionExtractErrorSourceTextMaxLength_29)); }
	inline int32_t get_OptionExtractErrorSourceTextMaxLength_29() const { return ___OptionExtractErrorSourceTextMaxLength_29; }
	inline int32_t* get_address_of_OptionExtractErrorSourceTextMaxLength_29() { return &___OptionExtractErrorSourceTextMaxLength_29; }
	inline void set_OptionExtractErrorSourceTextMaxLength_29(int32_t value)
	{
		___OptionExtractErrorSourceTextMaxLength_29 = value;
	}

	inline static int32_t get_offset_of_OptionFixNestedTags_30() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ___OptionFixNestedTags_30)); }
	inline bool get_OptionFixNestedTags_30() const { return ___OptionFixNestedTags_30; }
	inline bool* get_address_of_OptionFixNestedTags_30() { return &___OptionFixNestedTags_30; }
	inline void set_OptionFixNestedTags_30(bool value)
	{
		___OptionFixNestedTags_30 = value;
	}

	inline static int32_t get_offset_of_OptionOutputAsXml_31() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ___OptionOutputAsXml_31)); }
	inline bool get_OptionOutputAsXml_31() const { return ___OptionOutputAsXml_31; }
	inline bool* get_address_of_OptionOutputAsXml_31() { return &___OptionOutputAsXml_31; }
	inline void set_OptionOutputAsXml_31(bool value)
	{
		___OptionOutputAsXml_31 = value;
	}

	inline static int32_t get_offset_of_OptionOutputOptimizeAttributeValues_32() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ___OptionOutputOptimizeAttributeValues_32)); }
	inline bool get_OptionOutputOptimizeAttributeValues_32() const { return ___OptionOutputOptimizeAttributeValues_32; }
	inline bool* get_address_of_OptionOutputOptimizeAttributeValues_32() { return &___OptionOutputOptimizeAttributeValues_32; }
	inline void set_OptionOutputOptimizeAttributeValues_32(bool value)
	{
		___OptionOutputOptimizeAttributeValues_32 = value;
	}

	inline static int32_t get_offset_of_OptionOutputOriginalCase_33() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ___OptionOutputOriginalCase_33)); }
	inline bool get_OptionOutputOriginalCase_33() const { return ___OptionOutputOriginalCase_33; }
	inline bool* get_address_of_OptionOutputOriginalCase_33() { return &___OptionOutputOriginalCase_33; }
	inline void set_OptionOutputOriginalCase_33(bool value)
	{
		___OptionOutputOriginalCase_33 = value;
	}

	inline static int32_t get_offset_of_OptionOutputUpperCase_34() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ___OptionOutputUpperCase_34)); }
	inline bool get_OptionOutputUpperCase_34() const { return ___OptionOutputUpperCase_34; }
	inline bool* get_address_of_OptionOutputUpperCase_34() { return &___OptionOutputUpperCase_34; }
	inline void set_OptionOutputUpperCase_34(bool value)
	{
		___OptionOutputUpperCase_34 = value;
	}

	inline static int32_t get_offset_of_OptionReadEncoding_35() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ___OptionReadEncoding_35)); }
	inline bool get_OptionReadEncoding_35() const { return ___OptionReadEncoding_35; }
	inline bool* get_address_of_OptionReadEncoding_35() { return &___OptionReadEncoding_35; }
	inline void set_OptionReadEncoding_35(bool value)
	{
		___OptionReadEncoding_35 = value;
	}

	inline static int32_t get_offset_of_OptionStopperNodeName_36() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ___OptionStopperNodeName_36)); }
	inline String_t* get_OptionStopperNodeName_36() const { return ___OptionStopperNodeName_36; }
	inline String_t** get_address_of_OptionStopperNodeName_36() { return &___OptionStopperNodeName_36; }
	inline void set_OptionStopperNodeName_36(String_t* value)
	{
		___OptionStopperNodeName_36 = value;
		Il2CppCodeGenWriteBarrier((&___OptionStopperNodeName_36), value);
	}

	inline static int32_t get_offset_of_OptionUseIdAttribute_37() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ___OptionUseIdAttribute_37)); }
	inline bool get_OptionUseIdAttribute_37() const { return ___OptionUseIdAttribute_37; }
	inline bool* get_address_of_OptionUseIdAttribute_37() { return &___OptionUseIdAttribute_37; }
	inline void set_OptionUseIdAttribute_37(bool value)
	{
		___OptionUseIdAttribute_37 = value;
	}

	inline static int32_t get_offset_of_OptionWriteEmptyNodes_38() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108, ___OptionWriteEmptyNodes_38)); }
	inline bool get_OptionWriteEmptyNodes_38() const { return ___OptionWriteEmptyNodes_38; }
	inline bool* get_address_of_OptionWriteEmptyNodes_38() { return &___OptionWriteEmptyNodes_38; }
	inline void set_OptionWriteEmptyNodes_38(bool value)
	{
		___OptionWriteEmptyNodes_38 = value;
	}
};

struct HtmlDocument_t556432108_StaticFields
{
public:
	// System.String HtmlAgilityPack.HtmlDocument::HtmlExceptionRefNotChild
	String_t* ___HtmlExceptionRefNotChild_39;
	// System.String HtmlAgilityPack.HtmlDocument::HtmlExceptionUseIdAttributeFalse
	String_t* ___HtmlExceptionUseIdAttributeFalse_40;

public:
	inline static int32_t get_offset_of_HtmlExceptionRefNotChild_39() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108_StaticFields, ___HtmlExceptionRefNotChild_39)); }
	inline String_t* get_HtmlExceptionRefNotChild_39() const { return ___HtmlExceptionRefNotChild_39; }
	inline String_t** get_address_of_HtmlExceptionRefNotChild_39() { return &___HtmlExceptionRefNotChild_39; }
	inline void set_HtmlExceptionRefNotChild_39(String_t* value)
	{
		___HtmlExceptionRefNotChild_39 = value;
		Il2CppCodeGenWriteBarrier((&___HtmlExceptionRefNotChild_39), value);
	}

	inline static int32_t get_offset_of_HtmlExceptionUseIdAttributeFalse_40() { return static_cast<int32_t>(offsetof(HtmlDocument_t556432108_StaticFields, ___HtmlExceptionUseIdAttributeFalse_40)); }
	inline String_t* get_HtmlExceptionUseIdAttributeFalse_40() const { return ___HtmlExceptionUseIdAttributeFalse_40; }
	inline String_t** get_address_of_HtmlExceptionUseIdAttributeFalse_40() { return &___HtmlExceptionUseIdAttributeFalse_40; }
	inline void set_HtmlExceptionUseIdAttributeFalse_40(String_t* value)
	{
		___HtmlExceptionUseIdAttributeFalse_40 = value;
		Il2CppCodeGenWriteBarrier((&___HtmlExceptionUseIdAttributeFalse_40), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTMLDOCUMENT_T556432108_H
#ifndef HTMLATTRIBUTE_T1804523403_H
#define HTMLATTRIBUTE_T1804523403_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HtmlAgilityPack.HtmlAttribute
struct  HtmlAttribute_t1804523403  : public RuntimeObject
{
public:
	// System.Int32 HtmlAgilityPack.HtmlAttribute::_line
	int32_t ____line_0;
	// System.Int32 HtmlAgilityPack.HtmlAttribute::_lineposition
	int32_t ____lineposition_1;
	// System.String HtmlAgilityPack.HtmlAttribute::_name
	String_t* ____name_2;
	// System.Int32 HtmlAgilityPack.HtmlAttribute::_namelength
	int32_t ____namelength_3;
	// System.Int32 HtmlAgilityPack.HtmlAttribute::_namestartindex
	int32_t ____namestartindex_4;
	// HtmlAgilityPack.HtmlDocument HtmlAgilityPack.HtmlAttribute::_ownerdocument
	HtmlDocument_t556432108 * ____ownerdocument_5;
	// HtmlAgilityPack.HtmlNode HtmlAgilityPack.HtmlAttribute::_ownernode
	HtmlNode_t2048434459 * ____ownernode_6;
	// HtmlAgilityPack.AttributeValueQuote HtmlAgilityPack.HtmlAttribute::_quoteType
	int32_t ____quoteType_7;
	// System.Int32 HtmlAgilityPack.HtmlAttribute::_streamposition
	int32_t ____streamposition_8;
	// System.String HtmlAgilityPack.HtmlAttribute::_value
	String_t* ____value_9;
	// System.Int32 HtmlAgilityPack.HtmlAttribute::_valuelength
	int32_t ____valuelength_10;
	// System.Int32 HtmlAgilityPack.HtmlAttribute::_valuestartindex
	int32_t ____valuestartindex_11;

public:
	inline static int32_t get_offset_of__line_0() { return static_cast<int32_t>(offsetof(HtmlAttribute_t1804523403, ____line_0)); }
	inline int32_t get__line_0() const { return ____line_0; }
	inline int32_t* get_address_of__line_0() { return &____line_0; }
	inline void set__line_0(int32_t value)
	{
		____line_0 = value;
	}

	inline static int32_t get_offset_of__lineposition_1() { return static_cast<int32_t>(offsetof(HtmlAttribute_t1804523403, ____lineposition_1)); }
	inline int32_t get__lineposition_1() const { return ____lineposition_1; }
	inline int32_t* get_address_of__lineposition_1() { return &____lineposition_1; }
	inline void set__lineposition_1(int32_t value)
	{
		____lineposition_1 = value;
	}

	inline static int32_t get_offset_of__name_2() { return static_cast<int32_t>(offsetof(HtmlAttribute_t1804523403, ____name_2)); }
	inline String_t* get__name_2() const { return ____name_2; }
	inline String_t** get_address_of__name_2() { return &____name_2; }
	inline void set__name_2(String_t* value)
	{
		____name_2 = value;
		Il2CppCodeGenWriteBarrier((&____name_2), value);
	}

	inline static int32_t get_offset_of__namelength_3() { return static_cast<int32_t>(offsetof(HtmlAttribute_t1804523403, ____namelength_3)); }
	inline int32_t get__namelength_3() const { return ____namelength_3; }
	inline int32_t* get_address_of__namelength_3() { return &____namelength_3; }
	inline void set__namelength_3(int32_t value)
	{
		____namelength_3 = value;
	}

	inline static int32_t get_offset_of__namestartindex_4() { return static_cast<int32_t>(offsetof(HtmlAttribute_t1804523403, ____namestartindex_4)); }
	inline int32_t get__namestartindex_4() const { return ____namestartindex_4; }
	inline int32_t* get_address_of__namestartindex_4() { return &____namestartindex_4; }
	inline void set__namestartindex_4(int32_t value)
	{
		____namestartindex_4 = value;
	}

	inline static int32_t get_offset_of__ownerdocument_5() { return static_cast<int32_t>(offsetof(HtmlAttribute_t1804523403, ____ownerdocument_5)); }
	inline HtmlDocument_t556432108 * get__ownerdocument_5() const { return ____ownerdocument_5; }
	inline HtmlDocument_t556432108 ** get_address_of__ownerdocument_5() { return &____ownerdocument_5; }
	inline void set__ownerdocument_5(HtmlDocument_t556432108 * value)
	{
		____ownerdocument_5 = value;
		Il2CppCodeGenWriteBarrier((&____ownerdocument_5), value);
	}

	inline static int32_t get_offset_of__ownernode_6() { return static_cast<int32_t>(offsetof(HtmlAttribute_t1804523403, ____ownernode_6)); }
	inline HtmlNode_t2048434459 * get__ownernode_6() const { return ____ownernode_6; }
	inline HtmlNode_t2048434459 ** get_address_of__ownernode_6() { return &____ownernode_6; }
	inline void set__ownernode_6(HtmlNode_t2048434459 * value)
	{
		____ownernode_6 = value;
		Il2CppCodeGenWriteBarrier((&____ownernode_6), value);
	}

	inline static int32_t get_offset_of__quoteType_7() { return static_cast<int32_t>(offsetof(HtmlAttribute_t1804523403, ____quoteType_7)); }
	inline int32_t get__quoteType_7() const { return ____quoteType_7; }
	inline int32_t* get_address_of__quoteType_7() { return &____quoteType_7; }
	inline void set__quoteType_7(int32_t value)
	{
		____quoteType_7 = value;
	}

	inline static int32_t get_offset_of__streamposition_8() { return static_cast<int32_t>(offsetof(HtmlAttribute_t1804523403, ____streamposition_8)); }
	inline int32_t get__streamposition_8() const { return ____streamposition_8; }
	inline int32_t* get_address_of__streamposition_8() { return &____streamposition_8; }
	inline void set__streamposition_8(int32_t value)
	{
		____streamposition_8 = value;
	}

	inline static int32_t get_offset_of__value_9() { return static_cast<int32_t>(offsetof(HtmlAttribute_t1804523403, ____value_9)); }
	inline String_t* get__value_9() const { return ____value_9; }
	inline String_t** get_address_of__value_9() { return &____value_9; }
	inline void set__value_9(String_t* value)
	{
		____value_9 = value;
		Il2CppCodeGenWriteBarrier((&____value_9), value);
	}

	inline static int32_t get_offset_of__valuelength_10() { return static_cast<int32_t>(offsetof(HtmlAttribute_t1804523403, ____valuelength_10)); }
	inline int32_t get__valuelength_10() const { return ____valuelength_10; }
	inline int32_t* get_address_of__valuelength_10() { return &____valuelength_10; }
	inline void set__valuelength_10(int32_t value)
	{
		____valuelength_10 = value;
	}

	inline static int32_t get_offset_of__valuestartindex_11() { return static_cast<int32_t>(offsetof(HtmlAttribute_t1804523403, ____valuestartindex_11)); }
	inline int32_t get__valuestartindex_11() const { return ____valuestartindex_11; }
	inline int32_t* get_address_of__valuestartindex_11() { return &____valuestartindex_11; }
	inline void set__valuestartindex_11(int32_t value)
	{
		____valuestartindex_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTMLATTRIBUTE_T1804523403_H
#ifndef ARGUMENTOUTOFRANGEEXCEPTION_T279959794_H
#define ARGUMENTOUTOFRANGEEXCEPTION_T279959794_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArgumentOutOfRangeException
struct  ArgumentOutOfRangeException_t279959794  : public ArgumentException_t3259014390
{
public:
	// System.Object System.ArgumentOutOfRangeException::actual_value
	RuntimeObject * ___actual_value_13;

public:
	inline static int32_t get_offset_of_actual_value_13() { return static_cast<int32_t>(offsetof(ArgumentOutOfRangeException_t279959794, ___actual_value_13)); }
	inline RuntimeObject * get_actual_value_13() const { return ___actual_value_13; }
	inline RuntimeObject ** get_address_of_actual_value_13() { return &___actual_value_13; }
	inline void set_actual_value_13(RuntimeObject * value)
	{
		___actual_value_13 = value;
		Il2CppCodeGenWriteBarrier((&___actual_value_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTOUTOFRANGEEXCEPTION_T279959794_H
#ifndef HTMLNODE_T2048434459_H
#define HTMLNODE_T2048434459_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HtmlAgilityPack.HtmlNode
struct  HtmlNode_t2048434459  : public RuntimeObject
{
public:
	// HtmlAgilityPack.HtmlAttributeCollection HtmlAgilityPack.HtmlNode::_attributes
	HtmlAttributeCollection_t1787476631 * ____attributes_0;
	// HtmlAgilityPack.HtmlNodeCollection HtmlAgilityPack.HtmlNode::_childnodes
	HtmlNodeCollection_t2542734491 * ____childnodes_1;
	// HtmlAgilityPack.HtmlNode HtmlAgilityPack.HtmlNode::_endnode
	HtmlNode_t2048434459 * ____endnode_2;
	// System.Boolean HtmlAgilityPack.HtmlNode::_innerchanged
	bool ____innerchanged_3;
	// System.String HtmlAgilityPack.HtmlNode::_innerhtml
	String_t* ____innerhtml_4;
	// System.Int32 HtmlAgilityPack.HtmlNode::_innerlength
	int32_t ____innerlength_5;
	// System.Int32 HtmlAgilityPack.HtmlNode::_innerstartindex
	int32_t ____innerstartindex_6;
	// System.Int32 HtmlAgilityPack.HtmlNode::_line
	int32_t ____line_7;
	// System.Int32 HtmlAgilityPack.HtmlNode::_lineposition
	int32_t ____lineposition_8;
	// System.String HtmlAgilityPack.HtmlNode::_name
	String_t* ____name_9;
	// System.Int32 HtmlAgilityPack.HtmlNode::_namelength
	int32_t ____namelength_10;
	// System.Int32 HtmlAgilityPack.HtmlNode::_namestartindex
	int32_t ____namestartindex_11;
	// HtmlAgilityPack.HtmlNode HtmlAgilityPack.HtmlNode::_nextnode
	HtmlNode_t2048434459 * ____nextnode_12;
	// HtmlAgilityPack.HtmlNodeType HtmlAgilityPack.HtmlNode::_nodetype
	int32_t ____nodetype_13;
	// System.Boolean HtmlAgilityPack.HtmlNode::_outerchanged
	bool ____outerchanged_14;
	// System.String HtmlAgilityPack.HtmlNode::_outerhtml
	String_t* ____outerhtml_15;
	// System.Int32 HtmlAgilityPack.HtmlNode::_outerlength
	int32_t ____outerlength_16;
	// System.Int32 HtmlAgilityPack.HtmlNode::_outerstartindex
	int32_t ____outerstartindex_17;
	// System.String HtmlAgilityPack.HtmlNode::_optimizedName
	String_t* ____optimizedName_18;
	// HtmlAgilityPack.HtmlDocument HtmlAgilityPack.HtmlNode::_ownerdocument
	HtmlDocument_t556432108 * ____ownerdocument_19;
	// HtmlAgilityPack.HtmlNode HtmlAgilityPack.HtmlNode::_parentnode
	HtmlNode_t2048434459 * ____parentnode_20;
	// HtmlAgilityPack.HtmlNode HtmlAgilityPack.HtmlNode::_prevnode
	HtmlNode_t2048434459 * ____prevnode_21;
	// HtmlAgilityPack.HtmlNode HtmlAgilityPack.HtmlNode::_prevwithsamename
	HtmlNode_t2048434459 * ____prevwithsamename_22;
	// System.Boolean HtmlAgilityPack.HtmlNode::_starttag
	bool ____starttag_23;
	// System.Int32 HtmlAgilityPack.HtmlNode::_streamposition
	int32_t ____streamposition_24;

public:
	inline static int32_t get_offset_of__attributes_0() { return static_cast<int32_t>(offsetof(HtmlNode_t2048434459, ____attributes_0)); }
	inline HtmlAttributeCollection_t1787476631 * get__attributes_0() const { return ____attributes_0; }
	inline HtmlAttributeCollection_t1787476631 ** get_address_of__attributes_0() { return &____attributes_0; }
	inline void set__attributes_0(HtmlAttributeCollection_t1787476631 * value)
	{
		____attributes_0 = value;
		Il2CppCodeGenWriteBarrier((&____attributes_0), value);
	}

	inline static int32_t get_offset_of__childnodes_1() { return static_cast<int32_t>(offsetof(HtmlNode_t2048434459, ____childnodes_1)); }
	inline HtmlNodeCollection_t2542734491 * get__childnodes_1() const { return ____childnodes_1; }
	inline HtmlNodeCollection_t2542734491 ** get_address_of__childnodes_1() { return &____childnodes_1; }
	inline void set__childnodes_1(HtmlNodeCollection_t2542734491 * value)
	{
		____childnodes_1 = value;
		Il2CppCodeGenWriteBarrier((&____childnodes_1), value);
	}

	inline static int32_t get_offset_of__endnode_2() { return static_cast<int32_t>(offsetof(HtmlNode_t2048434459, ____endnode_2)); }
	inline HtmlNode_t2048434459 * get__endnode_2() const { return ____endnode_2; }
	inline HtmlNode_t2048434459 ** get_address_of__endnode_2() { return &____endnode_2; }
	inline void set__endnode_2(HtmlNode_t2048434459 * value)
	{
		____endnode_2 = value;
		Il2CppCodeGenWriteBarrier((&____endnode_2), value);
	}

	inline static int32_t get_offset_of__innerchanged_3() { return static_cast<int32_t>(offsetof(HtmlNode_t2048434459, ____innerchanged_3)); }
	inline bool get__innerchanged_3() const { return ____innerchanged_3; }
	inline bool* get_address_of__innerchanged_3() { return &____innerchanged_3; }
	inline void set__innerchanged_3(bool value)
	{
		____innerchanged_3 = value;
	}

	inline static int32_t get_offset_of__innerhtml_4() { return static_cast<int32_t>(offsetof(HtmlNode_t2048434459, ____innerhtml_4)); }
	inline String_t* get__innerhtml_4() const { return ____innerhtml_4; }
	inline String_t** get_address_of__innerhtml_4() { return &____innerhtml_4; }
	inline void set__innerhtml_4(String_t* value)
	{
		____innerhtml_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerhtml_4), value);
	}

	inline static int32_t get_offset_of__innerlength_5() { return static_cast<int32_t>(offsetof(HtmlNode_t2048434459, ____innerlength_5)); }
	inline int32_t get__innerlength_5() const { return ____innerlength_5; }
	inline int32_t* get_address_of__innerlength_5() { return &____innerlength_5; }
	inline void set__innerlength_5(int32_t value)
	{
		____innerlength_5 = value;
	}

	inline static int32_t get_offset_of__innerstartindex_6() { return static_cast<int32_t>(offsetof(HtmlNode_t2048434459, ____innerstartindex_6)); }
	inline int32_t get__innerstartindex_6() const { return ____innerstartindex_6; }
	inline int32_t* get_address_of__innerstartindex_6() { return &____innerstartindex_6; }
	inline void set__innerstartindex_6(int32_t value)
	{
		____innerstartindex_6 = value;
	}

	inline static int32_t get_offset_of__line_7() { return static_cast<int32_t>(offsetof(HtmlNode_t2048434459, ____line_7)); }
	inline int32_t get__line_7() const { return ____line_7; }
	inline int32_t* get_address_of__line_7() { return &____line_7; }
	inline void set__line_7(int32_t value)
	{
		____line_7 = value;
	}

	inline static int32_t get_offset_of__lineposition_8() { return static_cast<int32_t>(offsetof(HtmlNode_t2048434459, ____lineposition_8)); }
	inline int32_t get__lineposition_8() const { return ____lineposition_8; }
	inline int32_t* get_address_of__lineposition_8() { return &____lineposition_8; }
	inline void set__lineposition_8(int32_t value)
	{
		____lineposition_8 = value;
	}

	inline static int32_t get_offset_of__name_9() { return static_cast<int32_t>(offsetof(HtmlNode_t2048434459, ____name_9)); }
	inline String_t* get__name_9() const { return ____name_9; }
	inline String_t** get_address_of__name_9() { return &____name_9; }
	inline void set__name_9(String_t* value)
	{
		____name_9 = value;
		Il2CppCodeGenWriteBarrier((&____name_9), value);
	}

	inline static int32_t get_offset_of__namelength_10() { return static_cast<int32_t>(offsetof(HtmlNode_t2048434459, ____namelength_10)); }
	inline int32_t get__namelength_10() const { return ____namelength_10; }
	inline int32_t* get_address_of__namelength_10() { return &____namelength_10; }
	inline void set__namelength_10(int32_t value)
	{
		____namelength_10 = value;
	}

	inline static int32_t get_offset_of__namestartindex_11() { return static_cast<int32_t>(offsetof(HtmlNode_t2048434459, ____namestartindex_11)); }
	inline int32_t get__namestartindex_11() const { return ____namestartindex_11; }
	inline int32_t* get_address_of__namestartindex_11() { return &____namestartindex_11; }
	inline void set__namestartindex_11(int32_t value)
	{
		____namestartindex_11 = value;
	}

	inline static int32_t get_offset_of__nextnode_12() { return static_cast<int32_t>(offsetof(HtmlNode_t2048434459, ____nextnode_12)); }
	inline HtmlNode_t2048434459 * get__nextnode_12() const { return ____nextnode_12; }
	inline HtmlNode_t2048434459 ** get_address_of__nextnode_12() { return &____nextnode_12; }
	inline void set__nextnode_12(HtmlNode_t2048434459 * value)
	{
		____nextnode_12 = value;
		Il2CppCodeGenWriteBarrier((&____nextnode_12), value);
	}

	inline static int32_t get_offset_of__nodetype_13() { return static_cast<int32_t>(offsetof(HtmlNode_t2048434459, ____nodetype_13)); }
	inline int32_t get__nodetype_13() const { return ____nodetype_13; }
	inline int32_t* get_address_of__nodetype_13() { return &____nodetype_13; }
	inline void set__nodetype_13(int32_t value)
	{
		____nodetype_13 = value;
	}

	inline static int32_t get_offset_of__outerchanged_14() { return static_cast<int32_t>(offsetof(HtmlNode_t2048434459, ____outerchanged_14)); }
	inline bool get__outerchanged_14() const { return ____outerchanged_14; }
	inline bool* get_address_of__outerchanged_14() { return &____outerchanged_14; }
	inline void set__outerchanged_14(bool value)
	{
		____outerchanged_14 = value;
	}

	inline static int32_t get_offset_of__outerhtml_15() { return static_cast<int32_t>(offsetof(HtmlNode_t2048434459, ____outerhtml_15)); }
	inline String_t* get__outerhtml_15() const { return ____outerhtml_15; }
	inline String_t** get_address_of__outerhtml_15() { return &____outerhtml_15; }
	inline void set__outerhtml_15(String_t* value)
	{
		____outerhtml_15 = value;
		Il2CppCodeGenWriteBarrier((&____outerhtml_15), value);
	}

	inline static int32_t get_offset_of__outerlength_16() { return static_cast<int32_t>(offsetof(HtmlNode_t2048434459, ____outerlength_16)); }
	inline int32_t get__outerlength_16() const { return ____outerlength_16; }
	inline int32_t* get_address_of__outerlength_16() { return &____outerlength_16; }
	inline void set__outerlength_16(int32_t value)
	{
		____outerlength_16 = value;
	}

	inline static int32_t get_offset_of__outerstartindex_17() { return static_cast<int32_t>(offsetof(HtmlNode_t2048434459, ____outerstartindex_17)); }
	inline int32_t get__outerstartindex_17() const { return ____outerstartindex_17; }
	inline int32_t* get_address_of__outerstartindex_17() { return &____outerstartindex_17; }
	inline void set__outerstartindex_17(int32_t value)
	{
		____outerstartindex_17 = value;
	}

	inline static int32_t get_offset_of__optimizedName_18() { return static_cast<int32_t>(offsetof(HtmlNode_t2048434459, ____optimizedName_18)); }
	inline String_t* get__optimizedName_18() const { return ____optimizedName_18; }
	inline String_t** get_address_of__optimizedName_18() { return &____optimizedName_18; }
	inline void set__optimizedName_18(String_t* value)
	{
		____optimizedName_18 = value;
		Il2CppCodeGenWriteBarrier((&____optimizedName_18), value);
	}

	inline static int32_t get_offset_of__ownerdocument_19() { return static_cast<int32_t>(offsetof(HtmlNode_t2048434459, ____ownerdocument_19)); }
	inline HtmlDocument_t556432108 * get__ownerdocument_19() const { return ____ownerdocument_19; }
	inline HtmlDocument_t556432108 ** get_address_of__ownerdocument_19() { return &____ownerdocument_19; }
	inline void set__ownerdocument_19(HtmlDocument_t556432108 * value)
	{
		____ownerdocument_19 = value;
		Il2CppCodeGenWriteBarrier((&____ownerdocument_19), value);
	}

	inline static int32_t get_offset_of__parentnode_20() { return static_cast<int32_t>(offsetof(HtmlNode_t2048434459, ____parentnode_20)); }
	inline HtmlNode_t2048434459 * get__parentnode_20() const { return ____parentnode_20; }
	inline HtmlNode_t2048434459 ** get_address_of__parentnode_20() { return &____parentnode_20; }
	inline void set__parentnode_20(HtmlNode_t2048434459 * value)
	{
		____parentnode_20 = value;
		Il2CppCodeGenWriteBarrier((&____parentnode_20), value);
	}

	inline static int32_t get_offset_of__prevnode_21() { return static_cast<int32_t>(offsetof(HtmlNode_t2048434459, ____prevnode_21)); }
	inline HtmlNode_t2048434459 * get__prevnode_21() const { return ____prevnode_21; }
	inline HtmlNode_t2048434459 ** get_address_of__prevnode_21() { return &____prevnode_21; }
	inline void set__prevnode_21(HtmlNode_t2048434459 * value)
	{
		____prevnode_21 = value;
		Il2CppCodeGenWriteBarrier((&____prevnode_21), value);
	}

	inline static int32_t get_offset_of__prevwithsamename_22() { return static_cast<int32_t>(offsetof(HtmlNode_t2048434459, ____prevwithsamename_22)); }
	inline HtmlNode_t2048434459 * get__prevwithsamename_22() const { return ____prevwithsamename_22; }
	inline HtmlNode_t2048434459 ** get_address_of__prevwithsamename_22() { return &____prevwithsamename_22; }
	inline void set__prevwithsamename_22(HtmlNode_t2048434459 * value)
	{
		____prevwithsamename_22 = value;
		Il2CppCodeGenWriteBarrier((&____prevwithsamename_22), value);
	}

	inline static int32_t get_offset_of__starttag_23() { return static_cast<int32_t>(offsetof(HtmlNode_t2048434459, ____starttag_23)); }
	inline bool get__starttag_23() const { return ____starttag_23; }
	inline bool* get_address_of__starttag_23() { return &____starttag_23; }
	inline void set__starttag_23(bool value)
	{
		____starttag_23 = value;
	}

	inline static int32_t get_offset_of__streamposition_24() { return static_cast<int32_t>(offsetof(HtmlNode_t2048434459, ____streamposition_24)); }
	inline int32_t get__streamposition_24() const { return ____streamposition_24; }
	inline int32_t* get_address_of__streamposition_24() { return &____streamposition_24; }
	inline void set__streamposition_24(int32_t value)
	{
		____streamposition_24 = value;
	}
};

struct HtmlNode_t2048434459_StaticFields
{
public:
	// System.String HtmlAgilityPack.HtmlNode::HtmlNodeTypeNameComment
	String_t* ___HtmlNodeTypeNameComment_25;
	// System.String HtmlAgilityPack.HtmlNode::HtmlNodeTypeNameDocument
	String_t* ___HtmlNodeTypeNameDocument_26;
	// System.String HtmlAgilityPack.HtmlNode::HtmlNodeTypeNameText
	String_t* ___HtmlNodeTypeNameText_27;
	// System.Collections.Generic.Dictionary`2<System.String,HtmlAgilityPack.HtmlElementFlag> HtmlAgilityPack.HtmlNode::ElementsFlags
	Dictionary_2_t2175053619 * ___ElementsFlags_28;

public:
	inline static int32_t get_offset_of_HtmlNodeTypeNameComment_25() { return static_cast<int32_t>(offsetof(HtmlNode_t2048434459_StaticFields, ___HtmlNodeTypeNameComment_25)); }
	inline String_t* get_HtmlNodeTypeNameComment_25() const { return ___HtmlNodeTypeNameComment_25; }
	inline String_t** get_address_of_HtmlNodeTypeNameComment_25() { return &___HtmlNodeTypeNameComment_25; }
	inline void set_HtmlNodeTypeNameComment_25(String_t* value)
	{
		___HtmlNodeTypeNameComment_25 = value;
		Il2CppCodeGenWriteBarrier((&___HtmlNodeTypeNameComment_25), value);
	}

	inline static int32_t get_offset_of_HtmlNodeTypeNameDocument_26() { return static_cast<int32_t>(offsetof(HtmlNode_t2048434459_StaticFields, ___HtmlNodeTypeNameDocument_26)); }
	inline String_t* get_HtmlNodeTypeNameDocument_26() const { return ___HtmlNodeTypeNameDocument_26; }
	inline String_t** get_address_of_HtmlNodeTypeNameDocument_26() { return &___HtmlNodeTypeNameDocument_26; }
	inline void set_HtmlNodeTypeNameDocument_26(String_t* value)
	{
		___HtmlNodeTypeNameDocument_26 = value;
		Il2CppCodeGenWriteBarrier((&___HtmlNodeTypeNameDocument_26), value);
	}

	inline static int32_t get_offset_of_HtmlNodeTypeNameText_27() { return static_cast<int32_t>(offsetof(HtmlNode_t2048434459_StaticFields, ___HtmlNodeTypeNameText_27)); }
	inline String_t* get_HtmlNodeTypeNameText_27() const { return ___HtmlNodeTypeNameText_27; }
	inline String_t** get_address_of_HtmlNodeTypeNameText_27() { return &___HtmlNodeTypeNameText_27; }
	inline void set_HtmlNodeTypeNameText_27(String_t* value)
	{
		___HtmlNodeTypeNameText_27 = value;
		Il2CppCodeGenWriteBarrier((&___HtmlNodeTypeNameText_27), value);
	}

	inline static int32_t get_offset_of_ElementsFlags_28() { return static_cast<int32_t>(offsetof(HtmlNode_t2048434459_StaticFields, ___ElementsFlags_28)); }
	inline Dictionary_2_t2175053619 * get_ElementsFlags_28() const { return ___ElementsFlags_28; }
	inline Dictionary_2_t2175053619 ** get_address_of_ElementsFlags_28() { return &___ElementsFlags_28; }
	inline void set_ElementsFlags_28(Dictionary_2_t2175053619 * value)
	{
		___ElementsFlags_28 = value;
		Il2CppCodeGenWriteBarrier((&___ElementsFlags_28), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTMLNODE_T2048434459_H
#ifndef HTMLTEXTNODE_T2710098554_H
#define HTMLTEXTNODE_T2710098554_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HtmlAgilityPack.HtmlTextNode
struct  HtmlTextNode_t2710098554  : public HtmlNode_t2048434459
{
public:
	// System.String HtmlAgilityPack.HtmlTextNode::_text
	String_t* ____text_29;

public:
	inline static int32_t get_offset_of__text_29() { return static_cast<int32_t>(offsetof(HtmlTextNode_t2710098554, ____text_29)); }
	inline String_t* get__text_29() const { return ____text_29; }
	inline String_t** get_address_of__text_29() { return &____text_29; }
	inline void set__text_29(String_t* value)
	{
		____text_29 = value;
		Il2CppCodeGenWriteBarrier((&____text_29), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTMLTEXTNODE_T2710098554_H
#ifndef HTMLCOMMENTNODE_T1992371332_H
#define HTMLCOMMENTNODE_T1992371332_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HtmlAgilityPack.HtmlCommentNode
struct  HtmlCommentNode_t1992371332  : public HtmlNode_t2048434459
{
public:
	// System.String HtmlAgilityPack.HtmlCommentNode::_comment
	String_t* ____comment_29;

public:
	inline static int32_t get_offset_of__comment_29() { return static_cast<int32_t>(offsetof(HtmlCommentNode_t1992371332, ____comment_29)); }
	inline String_t* get__comment_29() const { return ____comment_29; }
	inline String_t** get_address_of__comment_29() { return &____comment_29; }
	inline void set__comment_29(String_t* value)
	{
		____comment_29 = value;
		Il2CppCodeGenWriteBarrier((&____comment_29), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTMLCOMMENTNODE_T1992371332_H
// System.UInt32[]
struct UInt32U5BU5D_t59386216  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint32_t m_Items[1];

public:
	inline uint32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint32_t value)
	{
		m_Items[index] = value;
	}
};
// HtmlAgilityPack.HtmlAttribute[]
struct HtmlAttributeU5BU5D_t425089514  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) HtmlAttribute_t1804523403 * m_Items[1];

public:
	inline HtmlAttribute_t1804523403 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline HtmlAttribute_t1804523403 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, HtmlAttribute_t1804523403 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline HtmlAttribute_t1804523403 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline HtmlAttribute_t1804523403 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, HtmlAttribute_t1804523403 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Byte[]
struct ByteU5BU5D_t3397334013  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
// System.Char[]
struct CharU5BU5D_t1328083999  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Il2CppChar m_Items[1];

public:
	inline Il2CppChar GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppChar value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Il2CppChar GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppChar value)
	{
		m_Items[index] = value;
	}
};
// System.String[]
struct StringU5BU5D_t1642385972  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HtmlAgilityPack.HtmlNode[]
struct HtmlNodeU5BU5D_t1365436442  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) HtmlNode_t2048434459 * m_Items[1];

public:
	inline HtmlNode_t2048434459 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline HtmlNode_t2048434459 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, HtmlNode_t2048434459 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline HtmlNode_t2048434459 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline HtmlNode_t2048434459 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, HtmlNode_t2048434459 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};


// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m584589095_gshared (Dictionary_2_t2281509423 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C"  void List_1__ctor_m310736118_gshared (List_1_t2058570427 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::TryGetValue(!0,!1&)
extern "C"  bool Dictionary_2_TryGetValue_m3975825838_gshared (Dictionary_2_t2281509423 * __this, RuntimeObject * p0, RuntimeObject ** p1, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C"  int32_t List_1_get_Count_m2375293942_gshared (List_1_t2058570427 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern "C"  RuntimeObject * List_1_get_Item_m2062981835_gshared (List_1_t2058570427 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::set_Item(System.Int32,!0)
extern "C"  void List_1_set_Item_m4246197648_gshared (List_1_t2058570427 * __this, int32_t p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
extern "C"  void List_1_Clear_m4254626809_gshared (List_1_t2058570427 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<System.Object>::Contains(!0)
extern "C"  bool List_1_Contains_m1658838094_gshared (List_1_t2058570427 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::CopyTo(!0[],System.Int32)
extern "C"  void List_1_CopyTo_m2123980726_gshared (List_1_t2058570427 * __this, ObjectU5BU5D_t3614634134* p0, int32_t p1, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
extern "C"  Enumerator_t1593300101  List_1_GetEnumerator_m2837081829_gshared (List_1_t2058570427 * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::IndexOf(!0)
extern "C"  int32_t List_1_IndexOf_m3376464728_gshared (List_1_t2058570427 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::set_Item(!0,!1)
extern "C"  void Dictionary_2_set_Item_m1004257024_gshared (Dictionary_2_t2281509423 * __this, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Insert(System.Int32,!0)
extern "C"  void List_1_Insert_m4101600027_gshared (List_1_t2058570427 * __this, int32_t p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<System.Object>::Remove(!0)
extern "C"  bool List_1_Remove_m3164383811_gshared (List_1_t2058570427 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Remove(!0)
extern "C"  bool Dictionary_2_Remove_m112127646_gshared (Dictionary_2_t2281509423 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m3615096820_gshared (List_1_t2058570427 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
extern "C"  void List_1_Add_m4157722533_gshared (List_1_t2058570427 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::ContainsKey(!0)
extern "C"  bool Dictionary_2_ContainsKey_m3321918434_gshared (Dictionary_2_t2281509423 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// !1 System.Collections.Generic.Dictionary`2<System.Object,System.Object>::get_Item(!0)
extern "C"  RuntimeObject * Dictionary_2_get_Item_m4062719145_gshared (Dictionary_2_t2281509423 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m1868603968_gshared (Dictionary_2_t1697274930 * __this, const RuntimeMethod* method);
// System.Collections.Generic.Dictionary`2/ValueCollection<!0,!1> System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::get_Values()
extern "C"  ValueCollection_t400334773 * Dictionary_2_get_Values_m41521588_gshared (Dictionary_2_t1697274930 * __this, const RuntimeMethod* method);
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<!0,!1> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Object>::GetEnumerator()
extern "C"  Enumerator_t3383807694  ValueCollection_GetEnumerator_m520082450_gshared (ValueCollection_t400334773 * __this, const RuntimeMethod* method);
// !1 System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::get_Current()
extern "C"  RuntimeObject * Enumerator_get_Current_m3006348140_gshared (Enumerator_t3383807694 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1091131935_gshared (Enumerator_t3383807694 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m2369319718_gshared (Enumerator_t3383807694 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::Clear()
extern "C"  void Dictionary_2_Clear_m899854001_gshared (Dictionary_2_t1697274930 * __this, const RuntimeMethod* method);
// TValue HtmlAgilityPack.Utilities::GetDictionaryValueOrNull<System.Object,System.Object>(System.Collections.Generic.Dictionary`2<TKey,TValue>,TKey)
extern "C"  RuntimeObject * Utilities_GetDictionaryValueOrNull_TisRuntimeObject_TisRuntimeObject_m4214667614_gshared (RuntimeObject * __this /* static, unused */, Dictionary_2_t2281509423 * ___dict0, RuntimeObject * ___key1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Stack`1<System.Object>::.ctor()
extern "C"  void Stack_1__ctor_m1041657164_gshared (Stack_1_t3777177449 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Stack`1<System.Object>::Push(!0)
extern "C"  void Stack_1_Push_m1129365869_gshared (Stack_1_t3777177449 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// !0 System.Collections.Generic.Stack`1<System.Object>::Pop()
extern "C"  RuntimeObject * Stack_1_Pop_m1289567471_gshared (Stack_1_t3777177449 * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.Stack`1<System.Object>::get_Count()
extern "C"  int32_t Stack_1_get_Count_m4101767244_gshared (Stack_1_t3777177449 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Clear()
extern "C"  void Dictionary_2_Clear_m2325793156_gshared (Dictionary_2_t2281509423 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,HtmlAgilityPack.HtmlElementFlag>::.ctor()
extern "C"  void Dictionary_2__ctor_m1609179347_gshared (Dictionary_2_t4147301781 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,HtmlAgilityPack.HtmlElementFlag>::Add(!0,!1)
extern "C"  void Dictionary_2_Add_m817801035_gshared (Dictionary_2_t4147301781 * __this, RuntimeObject * p0, int32_t p1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::Add(!0,!1)
extern "C"  void Dictionary_2_Add_m1296007576_gshared (Dictionary_2_t1697274930 * __this, int32_t p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,HtmlAgilityPack.HtmlElementFlag>::ContainsKey(!0)
extern "C"  bool Dictionary_2_ContainsKey_m526420838_gshared (Dictionary_2_t4147301781 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// !1 System.Collections.Generic.Dictionary`2<System.Object,HtmlAgilityPack.HtmlElementFlag>::get_Item(!0)
extern "C"  int32_t Dictionary_2_get_Item_m2347457829_gshared (Dictionary_2_t4147301781 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::Remove(!0)
extern "C"  bool Dictionary_2_Remove_m2771612799_gshared (Dictionary_2_t1697274930 * __this, int32_t p0, const RuntimeMethod* method);
// System.Collections.Generic.Dictionary`2/ValueCollection<!0,!1> System.Collections.Generic.Dictionary`2<System.Object,System.Object>::get_Values()
extern "C"  ValueCollection_t984569266 * Dictionary_2_get_Values_m2233445381_gshared (Dictionary_2_t2281509423 * __this, const RuntimeMethod* method);
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<!0,!1> System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>::GetEnumerator()
extern "C"  Enumerator_t3968042187  ValueCollection_GetEnumerator_m379930731_gshared (ValueCollection_t984569266 * __this, const RuntimeMethod* method);
// !1 System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::get_Current()
extern "C"  RuntimeObject * Enumerator_get_Current_m1976109889_gshared (Enumerator_t3968042187 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m335649778_gshared (Enumerator_t3968042187 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m4238653081_gshared (Enumerator_t3968042187 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
extern "C"  RuntimeObject * Enumerator_get_Current_m2577424081_gshared (Enumerator_t1593300101 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m44995089_gshared (Enumerator_t1593300101 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3736175406_gshared (Enumerator_t1593300101 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor()
extern "C"  void List_1__ctor_m3395850371_gshared (List_1_t3702943073 * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Count()
extern "C"  int32_t List_1_get_Count_m1224543263_gshared (List_1_t3702943073 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Item(System.Int32)
extern "C"  KeyValuePair_2_t38854645  List_1_get_Item_m325947224_gshared (List_1_t3702943073 * __this, int32_t p0, const RuntimeMethod* method);
// !1 System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Value()
extern "C"  RuntimeObject * KeyValuePair_2_get_Value_m1251901674_gshared (KeyValuePair_2_t38854645 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Clear()
extern "C"  void List_1_Clear_m4050912532_gshared (List_1_t3702943073 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::.ctor(!0,!1)
extern "C"  void KeyValuePair_2__ctor_m3464331946_gshared (KeyValuePair_2_t38854645 * __this, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Add(!0)
extern "C"  void List_1_Add_m2037007271_gshared (List_1_t3702943073 * __this, KeyValuePair_2_t38854645  p0, const RuntimeMethod* method);
// !0 System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Key()
extern "C"  RuntimeObject * KeyValuePair_2_get_Key_m3385717033_gshared (KeyValuePair_2_t38854645 * __this, const RuntimeMethod* method);

// System.UInt32 HtmlAgilityPack.Crc32::AddToCRC32(System.UInt16)
extern "C"  uint32_t Crc32_AddToCRC32_m3943154140 (Crc32_t83196531 * __this, uint16_t ___c0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.UInt32 HtmlAgilityPack.Crc32::UPDC32(System.Byte,System.UInt32)
extern "C"  uint32_t Crc32_UPDC32_m1407286487 (RuntimeObject * __this /* static, unused */, uint8_t ___octet0, uint32_t ___crc1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m2551263788 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.CompilerServices.RuntimeHelpers::InitializeArray(System.Array,System.RuntimeFieldHandle)
extern "C"  void RuntimeHelpers_InitializeArray_m3920580167 (RuntimeObject * __this /* static, unused */, RuntimeArray * p0, RuntimeFieldHandle_t2331729674  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Exception::.ctor()
extern "C"  void Exception__ctor_m3886110570 (Exception_t1927440687 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Substring(System.Int32,System.Int32)
extern "C"  String_t* String_Substring_m12482732 (String_t* __this, int32_t p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::ToLower()
extern "C"  String_t* String_ToLower_m2994460523 (String_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentNullException::.ctor(System.String)
extern "C"  void ArgumentNullException__ctor_m3380712306 (ArgumentNullException_t628810857 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String HtmlAgilityPack.HtmlAttribute::get_Name()
extern "C"  String_t* HtmlAttribute_get_Name_m3956631639 (HtmlAttribute_t1804523403 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String HtmlAgilityPack.HtmlDocument::GetXmlName(System.String)
extern "C"  String_t* HtmlDocument_GetXmlName_m448645048 (RuntimeObject * __this /* static, unused */, String_t* ___name0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String HtmlAgilityPack.HtmlAttribute::get_Value()
extern "C"  String_t* HtmlAttribute_get_Value_m3933521327 (HtmlAttribute_t1804523403 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentException::.ctor(System.String)
extern "C"  void ArgumentException__ctor_m3739475201 (ArgumentException_t3259014390 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.String::CompareTo(System.String)
extern "C"  int32_t String_CompareTo_m3879609894 (String_t* __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void HtmlAgilityPack.HtmlAttribute::.ctor(HtmlAgilityPack.HtmlDocument)
extern "C"  void HtmlAttribute__ctor_m3510814191 (HtmlAttribute_t1804523403 * __this, HtmlDocument_t556432108 * ___ownerdocument0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void HtmlAgilityPack.HtmlAttribute::set_Name(System.String)
extern "C"  void HtmlAttribute_set_Name_m1231032678 (HtmlAttribute_t1804523403 * __this, String_t* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void HtmlAgilityPack.HtmlAttribute::set_Value(System.String)
extern "C"  void HtmlAttribute_set_Value_m3484292440 (HtmlAttribute_t1804523403 * __this, String_t* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<System.String,HtmlAgilityPack.HtmlAttribute>::.ctor()
#define Dictionary_2__ctor_m1450246241(__this, method) ((  void (*) (Dictionary_2_t3719302665 *, const RuntimeMethod*))Dictionary_2__ctor_m584589095_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<HtmlAgilityPack.HtmlAttribute>::.ctor()
#define List_1__ctor_m2829932878(__this, method) ((  void (*) (List_1_t1173644535 *, const RuntimeMethod*))List_1__ctor_m310736118_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,HtmlAgilityPack.HtmlAttribute>::TryGetValue(!0,!1&)
#define Dictionary_2_TryGetValue_m2659214584(__this, p0, p1, method) ((  bool (*) (Dictionary_2_t3719302665 *, String_t*, HtmlAttribute_t1804523403 **, const RuntimeMethod*))Dictionary_2_TryGetValue_m3975825838_gshared)(__this, p0, p1, method)
// System.Int32 System.Collections.Generic.List`1<HtmlAgilityPack.HtmlAttribute>::get_Count()
#define List_1_get_Count_m1117934718(__this, method) ((  int32_t (*) (List_1_t1173644535 *, const RuntimeMethod*))List_1_get_Count_m2375293942_gshared)(__this, method)
// !0 System.Collections.Generic.List`1<HtmlAgilityPack.HtmlAttribute>::get_Item(System.Int32)
#define List_1_get_Item_m2219204117(__this, p0, method) ((  HtmlAttribute_t1804523403 * (*) (List_1_t1173644535 *, int32_t, const RuntimeMethod*))List_1_get_Item_m2062981835_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.List`1<HtmlAgilityPack.HtmlAttribute>::set_Item(System.Int32,!0)
#define List_1_set_Item_m1351812348(__this, p0, p1, method) ((  void (*) (List_1_t1173644535 *, int32_t, HtmlAttribute_t1804523403 *, const RuntimeMethod*))List_1_set_Item_m4246197648_gshared)(__this, p0, p1, method)
// HtmlAgilityPack.HtmlAttribute HtmlAgilityPack.HtmlAttributeCollection::Append(HtmlAgilityPack.HtmlAttribute)
extern "C"  HtmlAttribute_t1804523403 * HtmlAttributeCollection_Append_m138838623 (HtmlAttributeCollection_t1787476631 * __this, HtmlAttribute_t1804523403 * ___newAttribute0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<HtmlAgilityPack.HtmlAttribute>::Clear()
#define List_1_Clear_m3486920423(__this, method) ((  void (*) (List_1_t1173644535 *, const RuntimeMethod*))List_1_Clear_m4254626809_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<HtmlAgilityPack.HtmlAttribute>::Contains(!0)
#define List_1_Contains_m3092939846(__this, p0, method) ((  bool (*) (List_1_t1173644535 *, HtmlAttribute_t1804523403 *, const RuntimeMethod*))List_1_Contains_m1658838094_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.List`1<HtmlAgilityPack.HtmlAttribute>::CopyTo(!0[],System.Int32)
#define List_1_CopyTo_m2629566870(__this, p0, p1, method) ((  void (*) (List_1_t1173644535 *, HtmlAttributeU5BU5D_t425089514*, int32_t, const RuntimeMethod*))List_1_CopyTo_m2123980726_gshared)(__this, p0, p1, method)
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<HtmlAgilityPack.HtmlAttribute>::GetEnumerator()
#define List_1_GetEnumerator_m4221754371(__this, method) ((  Enumerator_t708374209  (*) (List_1_t1173644535 *, const RuntimeMethod*))List_1_GetEnumerator_m2837081829_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<HtmlAgilityPack.HtmlAttribute>::IndexOf(!0)
#define List_1_IndexOf_m946964392(__this, p0, method) ((  int32_t (*) (List_1_t1173644535 *, HtmlAttribute_t1804523403 *, const RuntimeMethod*))List_1_IndexOf_m3376464728_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,HtmlAgilityPack.HtmlAttribute>::set_Item(!0,!1)
#define Dictionary_2_set_Item_m1171915658(__this, p0, p1, method) ((  void (*) (Dictionary_2_t3719302665 *, String_t*, HtmlAttribute_t1804523403 *, const RuntimeMethod*))Dictionary_2_set_Item_m1004257024_gshared)(__this, p0, p1, method)
// System.Void System.Collections.Generic.List`1<HtmlAgilityPack.HtmlAttribute>::Insert(System.Int32,!0)
#define List_1_Insert_m341283965(__this, p0, p1, method) ((  void (*) (List_1_t1173644535 *, int32_t, HtmlAttribute_t1804523403 *, const RuntimeMethod*))List_1_Insert_m4101600027_gshared)(__this, p0, p1, method)
// System.Boolean System.Collections.Generic.List`1<HtmlAgilityPack.HtmlAttribute>::Remove(!0)
#define List_1_Remove_m302429225(__this, p0, method) ((  bool (*) (List_1_t1173644535 *, HtmlAttribute_t1804523403 *, const RuntimeMethod*))List_1_Remove_m3164383811_gshared)(__this, p0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,HtmlAgilityPack.HtmlAttribute>::Remove(!0)
#define Dictionary_2_Remove_m2474169460(__this, p0, method) ((  bool (*) (Dictionary_2_t3719302665 *, String_t*, const RuntimeMethod*))Dictionary_2_Remove_m112127646_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.List`1<HtmlAgilityPack.HtmlAttribute>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m680506192(__this, p0, method) ((  void (*) (List_1_t1173644535 *, int32_t, const RuntimeMethod*))List_1_RemoveAt_m3615096820_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.List`1<HtmlAgilityPack.HtmlAttribute>::Add(!0)
#define List_1_Add_m2897938714(__this, p0, method) ((  void (*) (List_1_t1173644535 *, HtmlAttribute_t1804523403 *, const RuntimeMethod*))List_1_Add_m4157722533_gshared)(__this, p0, method)
// System.Boolean System.String::Equals(System.String)
extern "C"  bool String_Equals_m2633592423 (String_t* __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::op_Equality(System.String,System.String)
extern "C"  bool String_op_Equality_m1790663636 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void HtmlAgilityPack.HtmlNode::.ctor(HtmlAgilityPack.HtmlNodeType,HtmlAgilityPack.HtmlDocument,System.Int32)
extern "C"  void HtmlNode__ctor_m2584575018 (HtmlNode_t2048434459 * __this, int32_t ___type0, HtmlDocument_t556432108 * ___ownerdocument1, int32_t ___index2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String HtmlAgilityPack.HtmlNode::get_InnerHtml()
extern "C"  String_t* HtmlNode_get_InnerHtml_m590077535 (HtmlNode_t2048434459 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String HtmlAgilityPack.HtmlNode::get_OuterHtml()
extern "C"  String_t* HtmlNode_get_OuterHtml_m1376347664 (HtmlNode_t2048434459 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String,System.String)
extern "C"  String_t* String_Concat_m612901809 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, String_t* p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<System.String,HtmlAgilityPack.HtmlNode>::.ctor()
#define Dictionary_2__ctor_m850897185(__this, method) ((  void (*) (Dictionary_2_t3963213721 *, const RuntimeMethod*))Dictionary_2__ctor_m584589095_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<HtmlAgilityPack.HtmlParseError>::.ctor()
#define List_1__ctor_m3187661903(__this, method) ((  void (*) (List_1_t484300294 *, const RuntimeMethod*))List_1__ctor_m310736118_gshared)(__this, method)
// HtmlAgilityPack.HtmlNode HtmlAgilityPack.HtmlDocument::CreateNode(HtmlAgilityPack.HtmlNodeType,System.Int32)
extern "C"  HtmlNode_t2048434459 * HtmlDocument_CreateNode_m538784785 (HtmlDocument_t556432108 * __this, int32_t ___type0, int32_t ___index1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Text.Encoding System.Text.Encoding::get_Default()
extern "C"  Encoding_t663144255 * Encoding_get_Default_m908538569 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Char System.String::get_Chars(System.Int32)
extern "C"  Il2CppChar String_get_Chars_m4230566705 (String_t* __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.Object,System.Object)
extern "C"  String_t* String_Concat_m56707527 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Text.Encoding System.Text.Encoding::get_UTF8()
extern "C"  Encoding_t663144255 * Encoding_get_UTF8_m1752852937 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Byte::ToString(System.String)
extern "C"  String_t* Byte_ToString_m1309661918 (uint8_t* __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String)
extern "C"  String_t* String_Concat_m2596409543 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.String::get_Length()
extern "C"  int32_t String_get_Length_m1606060069 (String_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.RegularExpressions.Regex::.ctor(System.String,System.Text.RegularExpressions.RegexOptions)
extern "C"  void Regex__ctor_m2521903438 (Regex_t1803876613 * __this, String_t* p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Text.RegularExpressions.Regex::Replace(System.String,System.String)
extern "C"  String_t* Regex_Replace_m2510734387 (Regex_t1803876613 * __this, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Replace(System.String,System.String)
extern "C"  String_t* String_Replace_m1941156251 (String_t* __this, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// HtmlAgilityPack.HtmlAttribute HtmlAgilityPack.HtmlDocument::CreateAttribute()
extern "C"  HtmlAttribute_t1804523403 * HtmlDocument_CreateAttribute_m1400299982 (HtmlDocument_t556432108 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// HtmlAgilityPack.HtmlAttribute HtmlAgilityPack.HtmlDocument::CreateAttribute(System.String)
extern "C"  HtmlAttribute_t1804523403 * HtmlDocument_CreateAttribute_m674678432 (HtmlDocument_t556432108 * __this, String_t* ___name0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Exception::.ctor(System.String)
extern "C"  void Exception__ctor_m485833136 (Exception_t1927440687 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,HtmlAgilityPack.HtmlNode>::ContainsKey(!0)
#define Dictionary_2_ContainsKey_m3415399212(__this, p0, method) ((  bool (*) (Dictionary_2_t3963213721 *, String_t*, const RuntimeMethod*))Dictionary_2_ContainsKey_m3321918434_gshared)(__this, p0, method)
// !1 System.Collections.Generic.Dictionary`2<System.String,HtmlAgilityPack.HtmlNode>::get_Item(!0)
#define Dictionary_2_get_Item_m3051461655(__this, p0, method) ((  HtmlNode_t2048434459 * (*) (Dictionary_2_t3963213721 *, String_t*, const RuntimeMethod*))Dictionary_2_get_Item_m4062719145_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,HtmlAgilityPack.HtmlNode>::.ctor()
#define Dictionary_2__ctor_m3832368222(__this, method) ((  void (*) (Dictionary_2_t1056260094 *, const RuntimeMethod*))Dictionary_2__ctor_m1868603968_gshared)(__this, method)
// System.Void HtmlAgilityPack.HtmlDocument::Parse()
extern "C"  void HtmlDocument_Parse_m3513675482 (HtmlDocument_t556432108 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2/ValueCollection<!0,!1> System.Collections.Generic.Dictionary`2<System.Int32,HtmlAgilityPack.HtmlNode>::get_Values()
#define Dictionary_2_get_Values_m2956437804(__this, method) ((  ValueCollection_t4054287233 * (*) (Dictionary_2_t1056260094 *, const RuntimeMethod*))Dictionary_2_get_Values_m41521588_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<!0,!1> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,HtmlAgilityPack.HtmlNode>::GetEnumerator()
#define ValueCollection_GetEnumerator_m2005408300(__this, method) ((  Enumerator_t2742792858  (*) (ValueCollection_t4054287233 *, const RuntimeMethod*))ValueCollection_GetEnumerator_m520082450_gshared)(__this, method)
// !1 System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,HtmlAgilityPack.HtmlNode>::get_Current()
#define Enumerator_get_Current_m3107603046(__this, method) ((  HtmlNode_t2048434459 * (*) (Enumerator_t2742792858 *, const RuntimeMethod*))Enumerator_get_Current_m3006348140_gshared)(__this, method)
// System.String HtmlAgilityPack.HtmlNode::get_Name()
extern "C"  String_t* HtmlNode_get_Name_m702134575 (HtmlNode_t2048434459 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void HtmlAgilityPack.HtmlDocument::AddError(HtmlAgilityPack.HtmlParseErrorCode,System.Int32,System.Int32,System.Int32,System.String,System.String)
extern "C"  void HtmlDocument_AddError_m2023821899 (HtmlDocument_t556432108 * __this, int32_t ___code0, int32_t ___line1, int32_t ___linePosition2, int32_t ___streamPosition3, String_t* ___sourceText4, String_t* ___reason5, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,HtmlAgilityPack.HtmlNode>::MoveNext()
#define Enumerator_MoveNext_m3092086129(__this, method) ((  bool (*) (Enumerator_t2742792858 *, const RuntimeMethod*))Enumerator_MoveNext_m1091131935_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,HtmlAgilityPack.HtmlNode>::Dispose()
#define Enumerator_Dispose_m393799376(__this, method) ((  void (*) (Enumerator_t2742792858 *, const RuntimeMethod*))Enumerator_Dispose_m2369319718_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,HtmlAgilityPack.HtmlNode>::Clear()
#define Dictionary_2_Clear_m3754984071(__this, method) ((  void (*) (Dictionary_2_t1056260094 *, const RuntimeMethod*))Dictionary_2_Clear_m899854001_gshared)(__this, method)
// System.Void System.IO.StringReader::.ctor(System.String)
extern "C"  void StringReader__ctor_m643998729 (StringReader_t1480123486 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void HtmlAgilityPack.HtmlDocument::Load(System.IO.TextReader)
extern "C"  void HtmlDocument_Load_m511797890 (HtmlDocument_t556432108 * __this, TextReader_t1561828458 * ___reader0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void HtmlAgilityPack.HtmlCommentNode::.ctor(HtmlAgilityPack.HtmlDocument,System.Int32)
extern "C"  void HtmlCommentNode__ctor_m3831299973 (HtmlCommentNode_t1992371332 * __this, HtmlDocument_t556432108 * ___ownerdocument0, int32_t ___index1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void HtmlAgilityPack.HtmlTextNode::.ctor(HtmlAgilityPack.HtmlDocument,System.Int32)
extern "C"  void HtmlTextNode__ctor_m3649714099 (HtmlTextNode_t2710098554 * __this, HtmlDocument_t556432108 * ___ownerdocument0, int32_t ___index1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean HtmlAgilityPack.HtmlNode::get_HasChildNodes()
extern "C"  bool HtmlNode_get_HasChildNodes_m1218535674 (HtmlNode_t2048434459 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,HtmlAgilityPack.HtmlNode>::Remove(!0)
#define Dictionary_2_Remove_m4126380600(__this, p0, method) ((  bool (*) (Dictionary_2_t3963213721 *, String_t*, const RuntimeMethod*))Dictionary_2_Remove_m112127646_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,HtmlAgilityPack.HtmlNode>::set_Item(!0,!1)
#define Dictionary_2_set_Item_m3521183286(__this, p0, p1, method) ((  void (*) (Dictionary_2_t3963213721 *, String_t*, HtmlNode_t2048434459 *, const RuntimeMethod*))Dictionary_2_set_Item_m1004257024_gshared)(__this, p0, p1, method)
// System.Boolean HtmlAgilityPack.HtmlNode::get_Closed()
extern "C"  bool HtmlNode_get_Closed_m650109995 (HtmlNode_t2048434459 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// HtmlAgilityPack.HtmlNode HtmlAgilityPack.HtmlNode::get_ParentNode()
extern "C"  HtmlNode_t2048434459 * HtmlNode_get_ParentNode_m1888227040 (HtmlNode_t2048434459 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void HtmlAgilityPack.HtmlParseError::.ctor(HtmlAgilityPack.HtmlParseErrorCode,System.Int32,System.Int32,System.Int32,System.String,System.String)
extern "C"  void HtmlParseError__ctor_m3785891130 (HtmlParseError_t1115179162 * __this, int32_t ___code0, int32_t ___line1, int32_t ___linePosition2, int32_t ___streamPosition3, String_t* ___sourceText4, String_t* ___reason5, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<HtmlAgilityPack.HtmlParseError>::Add(!0)
#define List_1_Add_m3464026123(__this, p0, method) ((  void (*) (List_1_t484300294 *, HtmlParseError_t1115179162 *, const RuntimeMethod*))List_1_Add_m4157722533_gshared)(__this, p0, method)
// TValue HtmlAgilityPack.Utilities::GetDictionaryValueOrNull<System.String,HtmlAgilityPack.HtmlNode>(System.Collections.Generic.Dictionary`2<TKey,TValue>,TKey)
#define Utilities_GetDictionaryValueOrNull_TisString_t_TisHtmlNode_t2048434459_m2137127104(__this /* static, unused */, ___dict0, ___key1, method) ((  HtmlNode_t2048434459 * (*) (RuntimeObject * /* static, unused */, Dictionary_2_t3963213721 *, String_t*, const RuntimeMethod*))Utilities_GetDictionaryValueOrNull_TisRuntimeObject_TisRuntimeObject_m4214667614_gshared)(__this /* static, unused */, ___dict0, ___key1, method)
// System.Boolean HtmlAgilityPack.HtmlNode::IsClosedElement(System.String)
extern "C"  bool HtmlNode_IsClosedElement_m2492183536 (RuntimeObject * __this /* static, unused */, String_t* ___name0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void HtmlAgilityPack.HtmlNode::CloseNode(HtmlAgilityPack.HtmlNode)
extern "C"  void HtmlNode_CloseNode_m1160950766 (HtmlNode_t2048434459 * __this, HtmlNode_t2048434459 * ___endnode0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Stack`1<HtmlAgilityPack.HtmlNode>::.ctor()
#define Stack_1__ctor_m1173151588(__this, method) ((  void (*) (Stack_1_t3136162613 *, const RuntimeMethod*))Stack_1__ctor_m1041657164_gshared)(__this, method)
// HtmlAgilityPack.HtmlNode HtmlAgilityPack.HtmlNode::get_LastChild()
extern "C"  HtmlNode_t2048434459 * HtmlNode_get_LastChild_m2408860244 (HtmlNode_t2048434459 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Stack`1<HtmlAgilityPack.HtmlNode>::Push(!0)
#define Stack_1_Push_m4199651461(__this, p0, method) ((  void (*) (Stack_1_t3136162613 *, HtmlNode_t2048434459 *, const RuntimeMethod*))Stack_1_Push_m1129365869_gshared)(__this, p0, method)
// HtmlAgilityPack.HtmlNode HtmlAgilityPack.HtmlNode::get_PreviousSibling()
extern "C"  HtmlNode_t2048434459 * HtmlNode_get_PreviousSibling_m3912767171 (HtmlNode_t2048434459 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !0 System.Collections.Generic.Stack`1<HtmlAgilityPack.HtmlNode>::Pop()
#define Stack_1_Pop_m1876234527(__this, method) ((  HtmlNode_t2048434459 * (*) (Stack_1_t3136162613 *, const RuntimeMethod*))Stack_1_Pop_m1289567471_gshared)(__this, method)
// HtmlAgilityPack.HtmlNode HtmlAgilityPack.HtmlNode::RemoveChild(HtmlAgilityPack.HtmlNode)
extern "C"  HtmlNode_t2048434459 * HtmlNode_RemoveChild_m1945208939 (HtmlNode_t2048434459 * __this, HtmlNode_t2048434459 * ___oldChild0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// HtmlAgilityPack.HtmlNode HtmlAgilityPack.HtmlNode::AppendChild(HtmlAgilityPack.HtmlNode)
extern "C"  HtmlNode_t2048434459 * HtmlNode_AppendChild_m2583227907 (HtmlNode_t2048434459 * __this, HtmlNode_t2048434459 * ___newChild0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.Stack`1<HtmlAgilityPack.HtmlNode>::get_Count()
#define Stack_1_get_Count_m133251284(__this, method) ((  int32_t (*) (Stack_1_t3136162613 *, const RuntimeMethod*))Stack_1_get_Count_m4101767244_gshared)(__this, method)
// System.Boolean HtmlAgilityPack.HtmlNode::CanOverlapElement(System.String)
extern "C"  bool HtmlNode_CanOverlapElement_m1178344375 (RuntimeObject * __this /* static, unused */, String_t* ___name0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String HtmlAgilityPack.HtmlTextNode::get_Text()
extern "C"  String_t* HtmlTextNode_get_Text_m1199112604 (HtmlTextNode_t2710098554 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void HtmlAgilityPack.HtmlTextNode::set_Text(System.String)
extern "C"  void HtmlTextNode_set_Text_m279687427 (HtmlTextNode_t2710098554 * __this, String_t* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean HtmlAgilityPack.HtmlNode::IsEmptyElement(System.String)
extern "C"  bool HtmlNode_IsEmptyElement_m2918938855 (RuntimeObject * __this /* static, unused */, String_t* ___name0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String[] HtmlAgilityPack.HtmlDocument::GetResetters(System.String)
extern "C"  StringU5BU5D_t1642385972* HtmlDocument_GetResetters_m3862259821 (HtmlDocument_t556432108 * __this, String_t* ___name0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean HtmlAgilityPack.HtmlDocument::FindResetterNodes(HtmlAgilityPack.HtmlNode,System.String[])
extern "C"  bool HtmlDocument_FindResetterNodes_m2911361875 (HtmlDocument_t556432108 * __this, HtmlNode_t2048434459 * ___node0, StringU5BU5D_t1642385972* ___names1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void HtmlAgilityPack.HtmlDocument::UpdateLastParentNode()
extern "C"  void HtmlDocument_UpdateLastParentNode_m3368866332 (HtmlDocument_t556432108 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// HtmlAgilityPack.HtmlNode HtmlAgilityPack.HtmlDocument::FindResetterNode(HtmlAgilityPack.HtmlNode,System.String)
extern "C"  HtmlNode_t2048434459 * HtmlDocument_FindResetterNode_m2182858297 (HtmlDocument_t556432108 * __this, HtmlNode_t2048434459 * ___node0, String_t* ___name1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// HtmlAgilityPack.HtmlNodeType HtmlAgilityPack.HtmlNode::get_NodeType()
extern "C"  int32_t HtmlNode_get_NodeType_m270476988 (HtmlNode_t2048434459 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String HtmlAgilityPack.HtmlDocument::CurrentNodeName()
extern "C"  String_t* HtmlDocument_CurrentNodeName_m112987032 (HtmlDocument_t556432108 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void HtmlAgilityPack.HtmlDocument::FixNestedTag(System.String,System.String[])
extern "C"  void HtmlDocument_FixNestedTag_m2725238597 (HtmlDocument_t556432108 * __this, String_t* ___name0, StringU5BU5D_t1642385972* ___resetters1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.UInt32 HtmlAgilityPack.Crc32::AddToCRC32(System.Int32)
extern "C"  uint32_t Crc32_AddToCRC32_m2370959785 (Crc32_t83196531 * __this, int32_t ___c0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void HtmlAgilityPack.HtmlDocument::PushAttributeValueStart(System.Int32)
extern "C"  void HtmlDocument_PushAttributeValueStart_m2091657843 (HtmlDocument_t556432108 * __this, int32_t ___index0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void HtmlAgilityPack.HtmlDocument::PushAttributeNameStart(System.Int32)
extern "C"  void HtmlDocument_PushAttributeNameStart_m687133679 (HtmlDocument_t556432108 * __this, int32_t ___index0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void HtmlAgilityPack.HtmlDocument::PushNodeNameStart(System.Boolean,System.Int32)
extern "C"  void HtmlDocument_PushNodeNameStart_m2396823204 (HtmlDocument_t556432108 * __this, bool ___starttag0, int32_t ___index1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean HtmlAgilityPack.HtmlDocument::PushNodeEnd(System.Int32,System.Boolean)
extern "C"  bool HtmlDocument_PushNodeEnd_m1567272216 (HtmlDocument_t556432108 * __this, int32_t ___index0, bool ___close1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void HtmlAgilityPack.HtmlDocument::PushNodeStart(HtmlAgilityPack.HtmlNodeType,System.Int32)
extern "C"  void HtmlDocument_PushNodeStart_m1529103168 (HtmlDocument_t556432108 * __this, int32_t ___type0, int32_t ___index1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void HtmlAgilityPack.HtmlDocument::PushNodeNameEnd(System.Int32)
extern "C"  void HtmlDocument_PushNodeNameEnd_m3862249348 (HtmlDocument_t556432108 * __this, int32_t ___index0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void HtmlAgilityPack.Crc32::.ctor()
extern "C"  void Crc32__ctor_m1474328800 (Crc32_t83196531 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void HtmlAgilityPack.HtmlDocument::IncrementPosition()
extern "C"  void HtmlDocument_IncrementPosition_m421960747 (HtmlDocument_t556432108 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean HtmlAgilityPack.HtmlDocument::NewCheck()
extern "C"  bool HtmlDocument_NewCheck_m3661304845 (HtmlDocument_t556432108 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void HtmlAgilityPack.HtmlDocument::DecrementPosition()
extern "C"  void HtmlDocument_DecrementPosition_m1311188097 (HtmlDocument_t556432108 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean HtmlAgilityPack.HtmlDocument::IsWhiteSpace(System.Int32)
extern "C"  bool HtmlDocument_IsWhiteSpace_m1502326855 (RuntimeObject * __this /* static, unused */, int32_t ___c0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void HtmlAgilityPack.HtmlDocument::PushAttributeNameEnd(System.Int32)
extern "C"  void HtmlDocument_PushAttributeNameEnd_m1257735076 (HtmlDocument_t556432108 * __this, int32_t ___index0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void HtmlAgilityPack.HtmlDocument::PushAttributeValueStart(System.Int32,System.Int32)
extern "C"  void HtmlDocument_PushAttributeValueStart_m1913776826 (HtmlDocument_t556432108 * __this, int32_t ___index0, int32_t ___quote1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void HtmlAgilityPack.HtmlDocument::PushAttributeValueEnd(System.Int32)
extern "C"  void HtmlDocument_PushAttributeValueEnd_m1973000602 (HtmlDocument_t556432108 * __this, int32_t ___index0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.String::Compare(System.String,System.String,System.StringComparison)
extern "C"  int32_t String_Compare_m3288062998 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, int32_t p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<System.String,HtmlAgilityPack.HtmlNode>::Clear()
#define Dictionary_2_Clear_m3645269862(__this, method) ((  void (*) (Dictionary_2_t3963213721 *, const RuntimeMethod*))Dictionary_2_Clear_m2325793156_gshared)(__this, method)
// HtmlAgilityPack.HtmlAttributeCollection HtmlAgilityPack.HtmlNode::get_Attributes()
extern "C"  HtmlAttributeCollection_t1787476631 * HtmlNode_get_Attributes_m2935417207 (HtmlNode_t2048434459 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void HtmlAgilityPack.HtmlAttribute::set_Line(System.Int32)
extern "C"  void HtmlAttribute_set_Line_m153524436 (HtmlAttribute_t1804523403 * __this, int32_t ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void HtmlAgilityPack.HtmlAttribute::set_QuoteType(HtmlAgilityPack.AttributeValueQuote)
extern "C"  void HtmlAttribute_set_QuoteType_m1504249919 (HtmlAttribute_t1804523403 * __this, int32_t ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void HtmlAgilityPack.HtmlDocument::ReadDocumentEncoding(HtmlAgilityPack.HtmlNode)
extern "C"  void HtmlDocument_ReadDocumentEncoding_m3190387121 (HtmlDocument_t556432108 * __this, HtmlNode_t2048434459 * ___node0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean HtmlAgilityPack.HtmlNode::IsCDataElement(System.String)
extern "C"  bool HtmlNode_IsCDataElement_m1574064561 (RuntimeObject * __this /* static, unused */, String_t* ___name0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Substring(System.Int32)
extern "C"  String_t* String_Substring_m2032624251 (String_t* __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void HtmlAgilityPack.HtmlDocument::CloseCurrentNode()
extern "C"  void HtmlDocument_CloseCurrentNode_m2823362304 (HtmlDocument_t556432108 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void HtmlAgilityPack.HtmlDocument::FixNestedTags()
extern "C"  void HtmlDocument_FixNestedTags_m2961550712 (HtmlDocument_t556432108 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::op_Inequality(System.String,System.String)
extern "C"  bool String_op_Inequality_m304203149 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// HtmlAgilityPack.HtmlAttribute HtmlAgilityPack.HtmlAttributeCollection::get_Item(System.String)
extern "C"  HtmlAttribute_t1804523403 * HtmlAttributeCollection_get_Item_m3848708643 (HtmlAttributeCollection_t1787476631 * __this, String_t* ___name0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String HtmlAgilityPack.NameValuePairList::GetNameValuePairsValue(System.String,System.String)
extern "C"  String_t* NameValuePairList_GetNameValuePairsValue_m1108671514 (RuntimeObject * __this /* static, unused */, String_t* ___text0, String_t* ___name1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::IsNullOrEmpty(System.String)
extern "C"  bool String_IsNullOrEmpty_m2802126737 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::Equals(System.String,System.String,System.StringComparison)
extern "C"  bool String_Equals_m2950069882 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, int32_t p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Text.Encoding System.Text.Encoding::GetEncoding(System.String)
extern "C"  Encoding_t663144255 * Encoding_GetEncoding_m2475966878 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void HtmlAgilityPack.EncodingFoundException::.ctor(System.Text.Encoding)
extern "C"  void EncodingFoundException__ctor_m308448534 (EncodingFoundException_t1471015948 * __this, Encoding_t663144255 * ___encoding0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String,System.String,System.String)
extern "C"  String_t* String_Concat_m1561703559 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, String_t* p2, String_t* p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.NameTable::.ctor()
extern "C"  void NameTable__ctor_m2766031050 (NameTable_t594386929 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XmlNameTable::.ctor()
extern "C"  void XmlNameTable__ctor_m684419949 (XmlNameTable_t1345805268 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void HtmlAgilityPack.HtmlNodeCollection::.ctor(HtmlAgilityPack.HtmlNode)
extern "C"  void HtmlNodeCollection__ctor_m429744290 (HtmlNodeCollection_t2542734491 * __this, HtmlNode_t2048434459 * ___parentnode0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// HtmlAgilityPack.HtmlDocument HtmlAgilityPack.HtmlNode::get_OwnerDocument()
extern "C"  HtmlDocument_t556432108 * HtmlNode_get_OwnerDocument_m2055696179 (HtmlNode_t2048434459 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void HtmlAgilityPack.HtmlNodeNavigator::.ctor(HtmlAgilityPack.HtmlDocument,HtmlAgilityPack.HtmlNode)
extern "C"  void HtmlNodeNavigator__ctor_m1439946702 (HtmlNodeNavigator_t2752606360 * __this, HtmlDocument_t556432108 * ___doc0, HtmlNode_t2048434459 * ___currentNode1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// HtmlAgilityPack.HtmlNode HtmlAgilityPack.HtmlNodeNavigator::get_CurrentNode()
extern "C"  HtmlNode_t2048434459 * HtmlNodeNavigator_get_CurrentNode_m1848014476 (HtmlNodeNavigator_t2752606360 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void HtmlAgilityPack.HtmlNodeCollection::Add(HtmlAgilityPack.HtmlNode)
extern "C"  void HtmlNodeCollection_Add_m2652620775 (HtmlNodeCollection_t2542734491 * __this, HtmlNode_t2048434459 * ___node0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 HtmlAgilityPack.HtmlNodeCollection::get_Count()
extern "C"  int32_t HtmlNodeCollection_get_Count_m3249441714 (HtmlNodeCollection_t2542734491 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<System.String,HtmlAgilityPack.HtmlElementFlag>::.ctor()
#define Dictionary_2__ctor_m3138784843(__this, method) ((  void (*) (Dictionary_2_t2175053619 *, const RuntimeMethod*))Dictionary_2__ctor_m1609179347_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,HtmlAgilityPack.HtmlElementFlag>::Add(!0,!1)
#define Dictionary_2_Add_m722374771(__this, p0, p1, method) ((  void (*) (Dictionary_2_t2175053619 *, String_t*, int32_t, const RuntimeMethod*))Dictionary_2_Add_m817801035_gshared)(__this, p0, p1, method)
// System.Void HtmlAgilityPack.HtmlNode::set_Name(System.String)
extern "C"  void HtmlNode_set_Name_m2475967416 (HtmlNode_t2048434459 * __this, String_t* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,HtmlAgilityPack.HtmlNode>::Add(!0,!1)
#define Dictionary_2_Add_m1845366966(__this, p0, p1, method) ((  void (*) (Dictionary_2_t1056260094 *, int32_t, HtmlNode_t2048434459 *, const RuntimeMethod*))Dictionary_2_Add_m1296007576_gshared)(__this, p0, p1, method)
// System.Boolean HtmlAgilityPack.HtmlNode::get_HasAttributes()
extern "C"  bool HtmlNode_get_HasAttributes_m2494625680 (HtmlNode_t2048434459 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void HtmlAgilityPack.HtmlAttributeCollection::.ctor(HtmlAgilityPack.HtmlNode)
extern "C"  void HtmlAttributeCollection__ctor_m1353566068 (HtmlAttributeCollection_t1787476631 * __this, HtmlNode_t2048434459 * ___ownernode0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// HtmlAgilityPack.HtmlNode HtmlAgilityPack.HtmlNodeCollection::get_Item(System.Int32)
extern "C"  HtmlNode_t2048434459 * HtmlNodeCollection_get_Item_m2496957206 (HtmlNodeCollection_t2542734491 * __this, int32_t ___index0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 HtmlAgilityPack.HtmlAttributeCollection::get_Count()
extern "C"  int32_t HtmlAttributeCollection_get_Count_m1097499500 (HtmlAttributeCollection_t1787476631 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String HtmlAgilityPack.HtmlNode::WriteContentTo()
extern "C"  String_t* HtmlNode_WriteContentTo_m2088782300 (HtmlNode_t2048434459 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String HtmlAgilityPack.HtmlCommentNode::get_Comment()
extern "C"  String_t* HtmlCommentNode_get_Comment_m3847846688 (HtmlCommentNode_t1992371332 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// HtmlAgilityPack.HtmlNodeCollection HtmlAgilityPack.HtmlNode::get_ChildNodes()
extern "C"  HtmlNodeCollection_t2542734491 * HtmlNode_get_ChildNodes_m1556126725 (HtmlNode_t2048434459 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String HtmlAgilityPack.HtmlNode::WriteTo()
extern "C"  String_t* HtmlNode_WriteTo_m2203281867 (HtmlNode_t2048434459 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,HtmlAgilityPack.HtmlElementFlag>::ContainsKey(!0)
#define Dictionary_2_ContainsKey_m2772236242(__this, p0, method) ((  bool (*) (Dictionary_2_t2175053619 *, String_t*, const RuntimeMethod*))Dictionary_2_ContainsKey_m526420838_gshared)(__this, p0, method)
// !1 System.Collections.Generic.Dictionary`2<System.String,HtmlAgilityPack.HtmlElementFlag>::get_Item(!0)
#define Dictionary_2_get_Item_m1423585693(__this, p0, method) ((  int32_t (*) (Dictionary_2_t2175053619 *, String_t*, const RuntimeMethod*))Dictionary_2_get_Item_m2347457829_gshared)(__this, p0, method)
// System.Void HtmlAgilityPack.HtmlNodeCollection::Append(HtmlAgilityPack.HtmlNode)
extern "C"  void HtmlNodeCollection_Append_m717126292 (HtmlNodeCollection_t2542734491 * __this, HtmlNode_t2048434459 * ___node0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String HtmlAgilityPack.HtmlNode::GetId()
extern "C"  String_t* HtmlNode_GetId_m3597009408 (HtmlNode_t2048434459 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void HtmlAgilityPack.HtmlDocument::SetIdForNode(HtmlAgilityPack.HtmlNode,System.String)
extern "C"  void HtmlDocument_SetIdForNode_m403125345 (HtmlDocument_t556432108 * __this, HtmlNode_t2048434459 * ___node0, String_t* ___id1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// HtmlAgilityPack.HtmlNode HtmlAgilityPack.HtmlNode::CloneNode(System.Boolean)
extern "C"  HtmlNode_t2048434459 * HtmlNode_CloneNode_m2027347983 (HtmlNode_t2048434459 * __this, bool ___deep0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// HtmlAgilityPack.HtmlNode HtmlAgilityPack.HtmlDocument::CreateNode(HtmlAgilityPack.HtmlNodeType)
extern "C"  HtmlNode_t2048434459 * HtmlDocument_CreateNode_m690978292 (HtmlDocument_t556432108 * __this, int32_t ___type0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void HtmlAgilityPack.HtmlCommentNode::set_Comment(System.String)
extern "C"  void HtmlCommentNode_set_Comment_m4142312819 (HtmlCommentNode_t1992371332 * __this, String_t* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// HtmlAgilityPack.HtmlAttribute HtmlAgilityPack.HtmlAttribute::Clone()
extern "C"  HtmlAttribute_t1804523403 * HtmlAttribute_Clone_m1011466360 (HtmlAttribute_t1804523403 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean HtmlAgilityPack.HtmlNode::get_HasClosingAttributes()
extern "C"  bool HtmlNode_get_HasClosingAttributes_m3757206011 (HtmlNode_t2048434459 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// HtmlAgilityPack.HtmlNode HtmlAgilityPack.HtmlNode::Clone()
extern "C"  HtmlNode_t2048434459 * HtmlNode_Clone_m1529145592 (HtmlNode_t2048434459 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 HtmlAgilityPack.HtmlNodeCollection::get_Item(HtmlAgilityPack.HtmlNode)
extern "C"  int32_t HtmlNodeCollection_get_Item_m1795494390 (HtmlNodeCollection_t2542734491 * __this, HtmlNode_t2048434459 * ___node0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean HtmlAgilityPack.HtmlNodeCollection::Remove(System.Int32)
extern "C"  bool HtmlNodeCollection_Remove_m46965641 (HtmlNodeCollection_t2542734491 * __this, int32_t ___index0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void HtmlAgilityPack.HtmlNode::WriteTo(System.IO.TextWriter)
extern "C"  void HtmlNode_WriteTo_m15807155 (HtmlNode_t2048434459 * __this, TextWriter_t4027217640 * ___outText0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.StringWriter::.ctor()
extern "C"  void StringWriter__ctor_m59456937 (StringWriter_t4139609088 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void HtmlAgilityPack.HtmlNode::WriteContentTo(System.IO.TextWriter)
extern "C"  void HtmlNode_WriteContentTo_m83799726 (HtmlNode_t2048434459 * __this, TextWriter_t4027217640 * ___outText0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String HtmlAgilityPack.HtmlNode::GetXmlComment(HtmlAgilityPack.HtmlCommentNode)
extern "C"  String_t* HtmlNode_GetXmlComment_m2603063516 (RuntimeObject * __this /* static, unused */, HtmlCommentNode_t1992371332 * ___comment0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Text.Encoding HtmlAgilityPack.HtmlDocument::GetOutEncoding()
extern "C"  Encoding_t663144255 * HtmlDocument_GetOutEncoding_m2726542450 (HtmlDocument_t556432108 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// HtmlAgilityPack.HtmlNode HtmlAgilityPack.HtmlDocument::get_DocumentNode()
extern "C"  HtmlNode_t2048434459 * HtmlDocument_get_DocumentNode_m1723003156 (HtmlDocument_t556432108 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// HtmlAgilityPack.HtmlNode HtmlAgilityPack.HtmlDocument::GetXmlDeclaration()
extern "C"  HtmlNode_t2048434459 * HtmlDocument_GetXmlDeclaration_m3305216451 (HtmlDocument_t556432108 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String HtmlAgilityPack.HtmlDocument::HtmlEncode(System.String)
extern "C"  String_t* HtmlDocument_HtmlEncode_m2155231231 (RuntimeObject * __this /* static, unused */, String_t* ___html0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::ToUpper()
extern "C"  String_t* String_ToUpper_m3715743312 (String_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String HtmlAgilityPack.HtmlNode::get_OriginalName()
extern "C"  String_t* HtmlNode_get_OriginalName_m1151328084 (HtmlNode_t2048434459 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Trim()
extern "C"  String_t* String_Trim_m2668767713 (String_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void HtmlAgilityPack.HtmlNode::WriteAttributes(System.IO.TextWriter,System.Boolean)
extern "C"  void HtmlNode_WriteAttributes_m2802695934 (HtmlNode_t2048434459 * __this, TextWriter_t4027217640 * ___outText0, bool ___closing1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,HtmlAgilityPack.HtmlNode>::Remove(!0)
#define Dictionary_2_Remove_m1113698489(__this, p0, method) ((  bool (*) (Dictionary_2_t1056260094 *, int32_t, const RuntimeMethod*))Dictionary_2_Remove_m2771612799_gshared)(__this, p0, method)
// HtmlAgilityPack.AttributeValueQuote HtmlAgilityPack.HtmlAttribute::get_QuoteType()
extern "C"  int32_t HtmlAttribute_get_QuoteType_m789997534 (HtmlAttribute_t1804523403 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String HtmlAgilityPack.HtmlAttribute::get_XmlName()
extern "C"  String_t* HtmlAttribute_get_XmlName_m2621022904 (HtmlAttribute_t1804523403 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String HtmlAgilityPack.HtmlAttribute::get_OriginalName()
extern "C"  String_t* HtmlAttribute_get_OriginalName_m3080906880 (HtmlAttribute_t1804523403 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String HtmlAgilityPack.HtmlAttribute::get_XmlValue()
extern "C"  String_t* HtmlAttribute_get_XmlValue_m2411235454 (HtmlAttribute_t1804523403 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String[])
extern "C"  String_t* String_Concat_m626692867 (RuntimeObject * __this /* static, unused */, StringU5BU5D_t1642385972* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.String::IndexOfAny(System.Char[])
extern "C"  int32_t String_IndexOfAny_m2016554902 (String_t* __this, CharU5BU5D_t1328083999* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2/ValueCollection<!0,!1> System.Collections.Generic.Dictionary`2<System.String,HtmlAgilityPack.HtmlAttribute>::get_Values()
#define Dictionary_2_get_Values_m1907592043(__this, method) ((  ValueCollection_t2422362508 * (*) (Dictionary_2_t3719302665 *, const RuntimeMethod*))Dictionary_2_get_Values_m2233445381_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<!0,!1> System.Collections.Generic.Dictionary`2/ValueCollection<System.String,HtmlAgilityPack.HtmlAttribute>::GetEnumerator()
#define ValueCollection_GetEnumerator_m2331812445(__this, method) ((  Enumerator_t1110868133  (*) (ValueCollection_t2422362508 *, const RuntimeMethod*))ValueCollection_GetEnumerator_m379930731_gshared)(__this, method)
// !1 System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,HtmlAgilityPack.HtmlAttribute>::get_Current()
#define Enumerator_get_Current_m4000267011(__this, method) ((  HtmlAttribute_t1804523403 * (*) (Enumerator_t1110868133 *, const RuntimeMethod*))Enumerator_get_Current_m1976109889_gshared)(__this, method)
// System.Void HtmlAgilityPack.HtmlNode::WriteAttribute(System.IO.TextWriter,HtmlAgilityPack.HtmlAttribute)
extern "C"  void HtmlNode_WriteAttribute_m3827491122 (HtmlNode_t2048434459 * __this, TextWriter_t4027217640 * ___outText0, HtmlAttribute_t1804523403 * ___att1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,HtmlAgilityPack.HtmlAttribute>::MoveNext()
#define Enumerator_MoveNext_m3599701188(__this, method) ((  bool (*) (Enumerator_t1110868133 *, const RuntimeMethod*))Enumerator_MoveNext_m335649778_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,HtmlAgilityPack.HtmlAttribute>::Dispose()
#define Enumerator_Dispose_m1803819355(__this, method) ((  void (*) (Enumerator_t1110868133 *, const RuntimeMethod*))Enumerator_Dispose_m4238653081_gshared)(__this, method)
// System.String System.Boolean::ToString()
extern "C"  String_t* Boolean_ToString_m1253164328 (bool* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// HtmlAgilityPack.HtmlAttribute HtmlAgilityPack.HtmlDocument::CreateAttribute(System.String,System.String)
extern "C"  HtmlAttribute_t1804523403 * HtmlDocument_CreateAttribute_m4240580628 (HtmlDocument_t556432108 * __this, String_t* ___name0, String_t* ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Int32::ToString()
extern "C"  String_t* Int32_ToString_m2960866144 (int32_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<HtmlAgilityPack.HtmlNode>::.ctor()
#define List_1__ctor_m3864602216(__this, method) ((  void (*) (List_1_t1417555591 *, const RuntimeMethod*))List_1__ctor_m310736118_gshared)(__this, method)
// System.Int32 HtmlAgilityPack.HtmlNodeCollection::GetNodeIndex(HtmlAgilityPack.HtmlNode)
extern "C"  int32_t HtmlNodeCollection_GetNodeIndex_m1699874364 (HtmlNodeCollection_t2542734491 * __this, HtmlNode_t2048434459 * ___node0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentOutOfRangeException::.ctor(System.String,System.String)
extern "C"  void ArgumentOutOfRangeException__ctor_m4234257711 (ArgumentOutOfRangeException_t279959794 * __this, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.List`1<HtmlAgilityPack.HtmlNode>::get_Count()
#define List_1_get_Count_m3121849784(__this, method) ((  int32_t (*) (List_1_t1417555591 *, const RuntimeMethod*))List_1_get_Count_m2375293942_gshared)(__this, method)
// !0 System.Collections.Generic.List`1<HtmlAgilityPack.HtmlNode>::get_Item(System.Int32)
#define List_1_get_Item_m3127542065(__this, p0, method) ((  HtmlNode_t2048434459 * (*) (List_1_t1417555591 *, int32_t, const RuntimeMethod*))List_1_get_Item_m2062981835_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.List`1<HtmlAgilityPack.HtmlNode>::set_Item(System.Int32,!0)
#define List_1_set_Item_m1335896754(__this, p0, p1, method) ((  void (*) (List_1_t1417555591 *, int32_t, HtmlNode_t2048434459 *, const RuntimeMethod*))List_1_set_Item_m4246197648_gshared)(__this, p0, p1, method)
// System.Void System.Collections.Generic.List`1<HtmlAgilityPack.HtmlNode>::Add(!0)
#define List_1_Add_m2851003676(__this, p0, method) ((  void (*) (List_1_t1417555591 *, HtmlNode_t2048434459 *, const RuntimeMethod*))List_1_Add_m4157722533_gshared)(__this, p0, method)
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<HtmlAgilityPack.HtmlNode>::GetEnumerator()
#define List_1_GetEnumerator_m2960830019(__this, method) ((  Enumerator_t952285265  (*) (List_1_t1417555591 *, const RuntimeMethod*))List_1_GetEnumerator_m2837081829_gshared)(__this, method)
// !0 System.Collections.Generic.List`1/Enumerator<HtmlAgilityPack.HtmlNode>::get_Current()
#define Enumerator_get_Current_m1132692067(__this, method) ((  HtmlNode_t2048434459 * (*) (Enumerator_t952285265 *, const RuntimeMethod*))Enumerator_get_Current_m2577424081_gshared)(__this, method)
// System.Void HtmlAgilityPack.HtmlNode::set_ParentNode(HtmlAgilityPack.HtmlNode)
extern "C"  void HtmlNode_set_ParentNode_m3958666249 (HtmlNode_t2048434459 * __this, HtmlNode_t2048434459 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void HtmlAgilityPack.HtmlNode::set_NextSibling(HtmlAgilityPack.HtmlNode)
extern "C"  void HtmlNode_set_NextSibling_m3096270636 (HtmlNode_t2048434459 * __this, HtmlNode_t2048434459 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void HtmlAgilityPack.HtmlNode::set_PreviousSibling(HtmlAgilityPack.HtmlNode)
extern "C"  void HtmlNode_set_PreviousSibling_m3540866112 (HtmlNode_t2048434459 * __this, HtmlNode_t2048434459 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.List`1/Enumerator<HtmlAgilityPack.HtmlNode>::MoveNext()
#define Enumerator_MoveNext_m2573550943(__this, method) ((  bool (*) (Enumerator_t952285265 *, const RuntimeMethod*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HtmlAgilityPack.HtmlNode>::Dispose()
#define Enumerator_Dispose_m850670244(__this, method) ((  void (*) (Enumerator_t952285265 *, const RuntimeMethod*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<HtmlAgilityPack.HtmlNode>::Clear()
#define List_1_Clear_m1310845251(__this, method) ((  void (*) (List_1_t1417555591 *, const RuntimeMethod*))List_1_Clear_m4254626809_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<HtmlAgilityPack.HtmlNode>::Contains(!0)
#define List_1_Contains_m530296848(__this, p0, method) ((  bool (*) (List_1_t1417555591 *, HtmlNode_t2048434459 *, const RuntimeMethod*))List_1_Contains_m1658838094_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.List`1<HtmlAgilityPack.HtmlNode>::CopyTo(!0[],System.Int32)
#define List_1_CopyTo_m3109553416(__this, p0, p1, method) ((  void (*) (List_1_t1417555591 *, HtmlNodeU5BU5D_t1365436442*, int32_t, const RuntimeMethod*))List_1_CopyTo_m2123980726_gshared)(__this, p0, p1, method)
// System.Int32 System.Collections.Generic.List`1<HtmlAgilityPack.HtmlNode>::IndexOf(!0)
#define List_1_IndexOf_m1714659438(__this, p0, method) ((  int32_t (*) (List_1_t1417555591 *, HtmlNode_t2048434459 *, const RuntimeMethod*))List_1_IndexOf_m3376464728_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.List`1<HtmlAgilityPack.HtmlNode>::Insert(System.Int32,!0)
#define List_1_Insert_m1073570933(__this, p0, p1, method) ((  void (*) (List_1_t1417555591 *, int32_t, HtmlNode_t2048434459 *, const RuntimeMethod*))List_1_Insert_m4101600027_gshared)(__this, p0, p1, method)
// System.Void System.InvalidProgramException::.ctor(System.String)
extern "C"  void InvalidProgramException__ctor_m4163582411 (InvalidProgramException_t3776992292 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void HtmlAgilityPack.HtmlNodeCollection::RemoveAt(System.Int32)
extern "C"  void HtmlNodeCollection_RemoveAt_m3305657552 (HtmlNodeCollection_t2542734491 * __this, int32_t ___index0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<HtmlAgilityPack.HtmlNode>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m1889740798(__this, p0, method) ((  void (*) (List_1_t1417555591 *, int32_t, const RuntimeMethod*))List_1_RemoveAt_m3615096820_gshared)(__this, p0, method)
// System.Void HtmlAgilityPack.HtmlDocument::.ctor()
extern "C"  void HtmlDocument__ctor_m2934097387 (HtmlDocument_t556432108 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void HtmlAgilityPack.HtmlNameTable::.ctor()
extern "C"  void HtmlNameTable__ctor_m4091390005 (HtmlNameTable_t3848610378 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XPath.XPathNavigator::.ctor()
extern "C"  void XPathNavigator__ctor_m3760155520 (XPathNavigator_t3981235968 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void HtmlAgilityPack.HtmlNodeNavigator::Reset()
extern "C"  void HtmlNodeNavigator_Reset_m1651173616 (HtmlNodeNavigator_t2752606360 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// HtmlAgilityPack.HtmlAttribute HtmlAgilityPack.HtmlAttributeCollection::get_Item(System.Int32)
extern "C"  HtmlAttribute_t1804523403 * HtmlAttributeCollection_get_Item_m4090043702 (HtmlAttributeCollection_t1787476631 * __this, int32_t ___index0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String HtmlAgilityPack.HtmlNameTable::GetOrAdd(System.String)
extern "C"  String_t* HtmlNameTable_GetOrAdd_m3409817290 (HtmlNameTable_t3848610378 * __this, String_t* ___array0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.NotImplementedException::.ctor(System.String)
extern "C"  void NotImplementedException__ctor_m1795163961 (NotImplementedException_t2785117854 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void HtmlAgilityPack.HtmlNodeNavigator::.ctor(HtmlAgilityPack.HtmlNodeNavigator)
extern "C"  void HtmlNodeNavigator__ctor_m2984146728 (HtmlNodeNavigator_t2752606360 * __this, HtmlNodeNavigator_t2752606360 * ___nav0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 HtmlAgilityPack.HtmlAttributeCollection::GetAttributeIndex(System.String)
extern "C"  int32_t HtmlAttributeCollection_GetAttributeIndex_m1301279406 (HtmlAttributeCollection_t1787476631 * __this, String_t* ___name0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// HtmlAgilityPack.HtmlNode HtmlAgilityPack.HtmlNode::get_FirstChild()
extern "C"  HtmlNode_t2048434459 * HtmlNode_get_FirstChild_m2198425556 (HtmlNode_t2048434459 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// HtmlAgilityPack.HtmlNode HtmlAgilityPack.HtmlDocument::GetElementbyId(System.String)
extern "C"  HtmlNode_t2048434459 * HtmlDocument_GetElementbyId_m1591120990 (HtmlDocument_t556432108 * __this, String_t* ___id0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// HtmlAgilityPack.HtmlNode HtmlAgilityPack.HtmlNode::get_NextSibling()
extern "C"  HtmlNode_t2048434459 * HtmlNode_get_NextSibling_m1378195553 (HtmlNode_t2048434459 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>::.ctor()
#define List_1__ctor_m973626735(__this, method) ((  void (*) (List_1_t1070465849 *, const RuntimeMethod*))List_1__ctor_m3395850371_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>>::.ctor()
#define Dictionary_2__ctor_m3512899986(__this, method) ((  void (*) (Dictionary_2_t2985245111 *, const RuntimeMethod*))Dictionary_2__ctor_m584589095_gshared)(__this, method)
// System.Void HtmlAgilityPack.NameValuePairList::Parse(System.String)
extern "C"  void NameValuePairList_Parse_m4220937304 (NameValuePairList_t1351137418 * __this, String_t* ___text0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void HtmlAgilityPack.NameValuePairList::.ctor(System.String)
extern "C"  void NameValuePairList__ctor_m1093178387 (NameValuePairList_t1351137418 * __this, String_t* ___text0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String HtmlAgilityPack.NameValuePairList::GetNameValuePairValue(System.String)
extern "C"  String_t* NameValuePairList_GetNameValuePairValue_m2853234185 (NameValuePairList_t1351137418 * __this, String_t* ___name0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>>::ContainsKey(!0)
#define Dictionary_2_ContainsKey_m838899279(__this, p0, method) ((  bool (*) (Dictionary_2_t2985245111 *, String_t*, const RuntimeMethod*))Dictionary_2_ContainsKey_m3321918434_gshared)(__this, p0, method)
// !1 System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>>::get_Item(!0)
#define Dictionary_2_get_Item_m441479114(__this, p0, method) ((  List_1_t1070465849 * (*) (Dictionary_2_t2985245111 *, String_t*, const RuntimeMethod*))Dictionary_2_get_Item_m4062719145_gshared)(__this, p0, method)
// System.Void System.ArgumentNullException::.ctor()
extern "C"  void ArgumentNullException__ctor_m911049464 (ArgumentNullException_t628810857 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>> HtmlAgilityPack.NameValuePairList::GetNameValuePairs(System.String)
extern "C"  List_1_t1070465849 * NameValuePairList_GetNameValuePairs_m1788854520 (NameValuePairList_t1351137418 * __this, String_t* ___name0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>::get_Count()
#define List_1_get_Count_m256450699(__this, method) ((  int32_t (*) (List_1_t1070465849 *, const RuntimeMethod*))List_1_get_Count_m1224543263_gshared)(__this, method)
// !0 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>::get_Item(System.Int32)
#define List_1_get_Item_m3174779882(__this, p0, method) ((  KeyValuePair_2_t1701344717  (*) (List_1_t1070465849 *, int32_t, const RuntimeMethod*))List_1_get_Item_m325947224_gshared)(__this, p0, method)
// !1 System.Collections.Generic.KeyValuePair`2<System.String,System.String>::get_Value()
#define KeyValuePair_2_get_Value_m1710042386(__this, method) ((  String_t* (*) (KeyValuePair_2_t1701344717 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m1251901674_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>::Clear()
#define List_1_Clear_m3390312458(__this, method) ((  void (*) (List_1_t1070465849 *, const RuntimeMethod*))List_1_Clear_m4050912532_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>>::Clear()
#define Dictionary_2_Clear_m1802158027(__this, method) ((  void (*) (Dictionary_2_t2985245111 *, const RuntimeMethod*))Dictionary_2_Clear_m2325793156_gshared)(__this, method)
// System.String[] System.String::Split(System.Char[])
extern "C"  StringU5BU5D_t1642385972* String_Split_m3326265864 (String_t* __this, CharU5BU5D_t1328083999* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String[] System.String::Split(System.Char[],System.Int32)
extern "C"  StringU5BU5D_t1642385972* String_Split_m3350696563 (String_t* __this, CharU5BU5D_t1328083999* p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.String>::.ctor(!0,!1)
#define KeyValuePair_2__ctor_m2274627104(__this, p0, p1, method) ((  void (*) (KeyValuePair_2_t1701344717 *, String_t*, String_t*, const RuntimeMethod*))KeyValuePair_2__ctor_m3464331946_gshared)(__this, p0, p1, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>::Add(!0)
#define List_1_Add_m1754637603(__this, p0, method) ((  void (*) (List_1_t1070465849 *, KeyValuePair_2_t1701344717 , const RuntimeMethod*))List_1_Add_m2037007271_gshared)(__this, p0, method)
// !0 System.Collections.Generic.KeyValuePair`2<System.String,System.String>::get_Key()
#define KeyValuePair_2_get_Key_m1372024679(__this, method) ((  String_t* (*) (KeyValuePair_2_t1701344717 *, const RuntimeMethod*))KeyValuePair_2_get_Key_m3385717033_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>>::set_Item(!0,!1)
#define Dictionary_2_set_Item_m764567519(__this, p0, p1, method) ((  void (*) (Dictionary_2_t2985245111 *, String_t*, List_1_t1070465849 *, const RuntimeMethod*))Dictionary_2_set_Item_m1004257024_gshared)(__this, p0, p1, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.UInt32 HtmlAgilityPack.Crc32::AddToCRC32(System.Int32)
extern "C"  uint32_t Crc32_AddToCRC32_m2370959785 (Crc32_t83196531 * __this, int32_t ___c0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___c0;
		uint32_t L_1 = Crc32_AddToCRC32_m3943154140(__this, (uint16_t)(((int32_t)((uint16_t)L_0))), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.UInt32 HtmlAgilityPack.Crc32::AddToCRC32(System.UInt16)
extern "C"  uint32_t Crc32_AddToCRC32_m3943154140 (Crc32_t83196531 * __this, uint16_t ___c0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Crc32_AddToCRC32_m3943154140_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint8_t V_0 = 0x0;
	uint8_t V_1 = 0x0;
	{
		uint16_t L_0 = ___c0;
		V_0 = (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_0&(int32_t)((int32_t)255))))));
		uint16_t L_1 = ___c0;
		V_1 = (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_1>>(int32_t)8)))));
		uint8_t L_2 = V_1;
		uint32_t L_3 = __this->get__crc32_0();
		IL2CPP_RUNTIME_CLASS_INIT(Crc32_t83196531_il2cpp_TypeInfo_var);
		uint32_t L_4 = Crc32_UPDC32_m1407286487(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		__this->set__crc32_0(L_4);
		uint8_t L_5 = V_0;
		uint32_t L_6 = __this->get__crc32_0();
		uint32_t L_7 = Crc32_UPDC32_m1407286487(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		__this->set__crc32_0(L_7);
		uint32_t L_8 = __this->get__crc32_0();
		return ((~L_8));
	}
}
// System.UInt32 HtmlAgilityPack.Crc32::UPDC32(System.Byte,System.UInt32)
extern "C"  uint32_t Crc32_UPDC32_m1407286487 (RuntimeObject * __this /* static, unused */, uint8_t ___octet0, uint32_t ___crc1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Crc32_UPDC32_m1407286487_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Crc32_t83196531_il2cpp_TypeInfo_var);
		UInt32U5BU5D_t59386216* L_0 = ((Crc32_t83196531_StaticFields*)il2cpp_codegen_static_fields_for(Crc32_t83196531_il2cpp_TypeInfo_var))->get_crc_32_tab_1();
		uint32_t L_1 = ___crc1;
		uint8_t L_2 = ___octet0;
		NullCheck(L_0);
		uintptr_t L_3 = (((uintptr_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)L_2))&(int32_t)((int32_t)255)))));
		uint32_t L_4 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		uint32_t L_5 = ___crc1;
		return ((int32_t)((int32_t)L_4^(int32_t)((int32_t)((uint32_t)L_5>>8))));
	}
}
// System.Void HtmlAgilityPack.Crc32::.ctor()
extern "C"  void Crc32__ctor_m1474328800 (Crc32_t83196531 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HtmlAgilityPack.Crc32::.cctor()
extern "C"  void Crc32__cctor_m518075841 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Crc32__cctor_m518075841_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UInt32U5BU5D_t59386216* L_0 = ((UInt32U5BU5D_t59386216*)SZArrayNew(UInt32U5BU5D_t59386216_il2cpp_TypeInfo_var, (uint32_t)((int32_t)256)));
		RuntimeHelpers_InitializeArray_m3920580167(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_0, LoadFieldToken(U3CPrivateImplementationDetailsU3EU7B83185D3BU2D3939U2D439CU2DA54FU2D260F9279D9C8U7D_t691081255____U24U24method0x6000287U2D1_1_FieldInfo_var), /*hidden argument*/NULL);
		((Crc32_t83196531_StaticFields*)il2cpp_codegen_static_fields_for(Crc32_t83196531_il2cpp_TypeInfo_var))->set_crc_32_tab_1(L_0);
		return;
	}
}
// System.Void HtmlAgilityPack.EncodingFoundException::.ctor(System.Text.Encoding)
extern "C"  void EncodingFoundException__ctor_m308448534 (EncodingFoundException_t1471015948 * __this, Encoding_t663144255 * ___encoding0, const RuntimeMethod* method)
{
	{
		Exception__ctor_m3886110570(__this, /*hidden argument*/NULL);
		Encoding_t663144255 * L_0 = ___encoding0;
		__this->set__encoding_11(L_0);
		return;
	}
}
// System.Void HtmlAgilityPack.HtmlAttribute::.ctor(HtmlAgilityPack.HtmlDocument)
extern "C"  void HtmlAttribute__ctor_m3510814191 (HtmlAttribute_t1804523403 * __this, HtmlDocument_t556432108 * ___ownerdocument0, const RuntimeMethod* method)
{
	{
		__this->set__quoteType_7(1);
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		HtmlDocument_t556432108 * L_0 = ___ownerdocument0;
		__this->set__ownerdocument_5(L_0);
		return;
	}
}
// System.Void HtmlAgilityPack.HtmlAttribute::set_Line(System.Int32)
extern "C"  void HtmlAttribute_set_Line_m153524436 (HtmlAttribute_t1804523403 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set__line_0(L_0);
		return;
	}
}
// System.String HtmlAgilityPack.HtmlAttribute::get_Name()
extern "C"  String_t* HtmlAttribute_get_Name_m3956631639 (HtmlAttribute_t1804523403 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get__name_2();
		if (L_0)
		{
			goto IL_002a;
		}
	}
	{
		HtmlDocument_t556432108 * L_1 = __this->get__ownerdocument_5();
		NullCheck(L_1);
		String_t* L_2 = L_1->get_Text_22();
		int32_t L_3 = __this->get__namestartindex_4();
		int32_t L_4 = __this->get__namelength_3();
		NullCheck(L_2);
		String_t* L_5 = String_Substring_m12482732(L_2, L_3, L_4, /*hidden argument*/NULL);
		__this->set__name_2(L_5);
	}

IL_002a:
	{
		String_t* L_6 = __this->get__name_2();
		NullCheck(L_6);
		String_t* L_7 = String_ToLower_m2994460523(L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void HtmlAgilityPack.HtmlAttribute::set_Name(System.String)
extern "C"  void HtmlAttribute_set_Name_m1231032678 (HtmlAttribute_t1804523403 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlAttribute_set_Name_m1231032678_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___value0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, _stringLiteral1803325615, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_000e:
	{
		String_t* L_2 = ___value0;
		__this->set__name_2(L_2);
		HtmlNode_t2048434459 * L_3 = __this->get__ownernode_6();
		if (!L_3)
		{
			goto IL_0035;
		}
	}
	{
		HtmlNode_t2048434459 * L_4 = __this->get__ownernode_6();
		NullCheck(L_4);
		L_4->set__innerchanged_3((bool)1);
		HtmlNode_t2048434459 * L_5 = __this->get__ownernode_6();
		NullCheck(L_5);
		L_5->set__outerchanged_14((bool)1);
	}

IL_0035:
	{
		return;
	}
}
// System.String HtmlAgilityPack.HtmlAttribute::get_OriginalName()
extern "C"  String_t* HtmlAttribute_get_OriginalName_m3080906880 (HtmlAttribute_t1804523403 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get__name_2();
		return L_0;
	}
}
// HtmlAgilityPack.AttributeValueQuote HtmlAgilityPack.HtmlAttribute::get_QuoteType()
extern "C"  int32_t HtmlAttribute_get_QuoteType_m789997534 (HtmlAttribute_t1804523403 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get__quoteType_7();
		return L_0;
	}
}
// System.Void HtmlAgilityPack.HtmlAttribute::set_QuoteType(HtmlAgilityPack.AttributeValueQuote)
extern "C"  void HtmlAttribute_set_QuoteType_m1504249919 (HtmlAttribute_t1804523403 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set__quoteType_7(L_0);
		return;
	}
}
// System.String HtmlAgilityPack.HtmlAttribute::get_Value()
extern "C"  String_t* HtmlAttribute_get_Value_m3933521327 (HtmlAttribute_t1804523403 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get__value_9();
		if (L_0)
		{
			goto IL_002a;
		}
	}
	{
		HtmlDocument_t556432108 * L_1 = __this->get__ownerdocument_5();
		NullCheck(L_1);
		String_t* L_2 = L_1->get_Text_22();
		int32_t L_3 = __this->get__valuestartindex_11();
		int32_t L_4 = __this->get__valuelength_10();
		NullCheck(L_2);
		String_t* L_5 = String_Substring_m12482732(L_2, L_3, L_4, /*hidden argument*/NULL);
		__this->set__value_9(L_5);
	}

IL_002a:
	{
		String_t* L_6 = __this->get__value_9();
		return L_6;
	}
}
// System.Void HtmlAgilityPack.HtmlAttribute::set_Value(System.String)
extern "C"  void HtmlAttribute_set_Value_m3484292440 (HtmlAttribute_t1804523403 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set__value_9(L_0);
		HtmlNode_t2048434459 * L_1 = __this->get__ownernode_6();
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		HtmlNode_t2048434459 * L_2 = __this->get__ownernode_6();
		NullCheck(L_2);
		L_2->set__innerchanged_3((bool)1);
		HtmlNode_t2048434459 * L_3 = __this->get__ownernode_6();
		NullCheck(L_3);
		L_3->set__outerchanged_14((bool)1);
	}

IL_0027:
	{
		return;
	}
}
// System.String HtmlAgilityPack.HtmlAttribute::get_XmlName()
extern "C"  String_t* HtmlAttribute_get_XmlName_m2621022904 (HtmlAttribute_t1804523403 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlAttribute_get_XmlName_m2621022904_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = HtmlAttribute_get_Name_m3956631639(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(HtmlDocument_t556432108_il2cpp_TypeInfo_var);
		String_t* L_1 = HtmlDocument_GetXmlName_m448645048(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String HtmlAgilityPack.HtmlAttribute::get_XmlValue()
extern "C"  String_t* HtmlAttribute_get_XmlValue_m2411235454 (HtmlAttribute_t1804523403 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = HtmlAttribute_get_Value_m3933521327(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Int32 HtmlAgilityPack.HtmlAttribute::CompareTo(System.Object)
extern "C"  int32_t HtmlAttribute_CompareTo_m486975030 (HtmlAttribute_t1804523403 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlAttribute_CompareTo_m486975030_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	HtmlAttribute_t1804523403 * V_0 = NULL;
	{
		RuntimeObject * L_0 = ___obj0;
		V_0 = ((HtmlAttribute_t1804523403 *)IsInstClass((RuntimeObject*)L_0, HtmlAttribute_t1804523403_il2cpp_TypeInfo_var));
		HtmlAttribute_t1804523403 * L_1 = V_0;
		if (L_1)
		{
			goto IL_0015;
		}
	}
	{
		ArgumentException_t3259014390 * L_2 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_2, _stringLiteral1099314147, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0015:
	{
		String_t* L_3 = HtmlAttribute_get_Name_m3956631639(__this, /*hidden argument*/NULL);
		HtmlAttribute_t1804523403 * L_4 = V_0;
		NullCheck(L_4);
		String_t* L_5 = HtmlAttribute_get_Name_m3956631639(L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_6 = String_CompareTo_m3879609894(L_3, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// HtmlAgilityPack.HtmlAttribute HtmlAgilityPack.HtmlAttribute::Clone()
extern "C"  HtmlAttribute_t1804523403 * HtmlAttribute_Clone_m1011466360 (HtmlAttribute_t1804523403 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlAttribute_Clone_m1011466360_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	HtmlAttribute_t1804523403 * V_0 = NULL;
	{
		HtmlDocument_t556432108 * L_0 = __this->get__ownerdocument_5();
		HtmlAttribute_t1804523403 * L_1 = (HtmlAttribute_t1804523403 *)il2cpp_codegen_object_new(HtmlAttribute_t1804523403_il2cpp_TypeInfo_var);
		HtmlAttribute__ctor_m3510814191(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		HtmlAttribute_t1804523403 * L_2 = V_0;
		String_t* L_3 = HtmlAttribute_get_Name_m3956631639(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		HtmlAttribute_set_Name_m1231032678(L_2, L_3, /*hidden argument*/NULL);
		HtmlAttribute_t1804523403 * L_4 = V_0;
		String_t* L_5 = HtmlAttribute_get_Value_m3933521327(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		HtmlAttribute_set_Value_m3484292440(L_4, L_5, /*hidden argument*/NULL);
		HtmlAttribute_t1804523403 * L_6 = V_0;
		return L_6;
	}
}
// System.Void HtmlAgilityPack.HtmlAttributeCollection::.ctor(HtmlAgilityPack.HtmlNode)
extern "C"  void HtmlAttributeCollection__ctor_m1353566068 (HtmlAttributeCollection_t1787476631 * __this, HtmlNode_t2048434459 * ___ownernode0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlAttributeCollection__ctor_m1353566068_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t3719302665 * L_0 = (Dictionary_2_t3719302665 *)il2cpp_codegen_object_new(Dictionary_2_t3719302665_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1450246241(L_0, /*hidden argument*/Dictionary_2__ctor_m1450246241_RuntimeMethod_var);
		__this->set_Hashitems_0(L_0);
		List_1_t1173644535 * L_1 = (List_1_t1173644535 *)il2cpp_codegen_object_new(List_1_t1173644535_il2cpp_TypeInfo_var);
		List_1__ctor_m2829932878(L_1, /*hidden argument*/List_1__ctor_m2829932878_RuntimeMethod_var);
		__this->set_items_2(L_1);
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		HtmlNode_t2048434459 * L_2 = ___ownernode0;
		__this->set__ownernode_1(L_2);
		return;
	}
}
// HtmlAgilityPack.HtmlAttribute HtmlAgilityPack.HtmlAttributeCollection::get_Item(System.String)
extern "C"  HtmlAttribute_t1804523403 * HtmlAttributeCollection_get_Item_m3848708643 (HtmlAttributeCollection_t1787476631 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlAttributeCollection_get_Item_m3848708643_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	HtmlAttribute_t1804523403 * V_0 = NULL;
	{
		String_t* L_0 = ___name0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, _stringLiteral2328218955, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_000e:
	{
		Dictionary_2_t3719302665 * L_2 = __this->get_Hashitems_0();
		String_t* L_3 = ___name0;
		NullCheck(L_3);
		String_t* L_4 = String_ToLower_m2994460523(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		bool L_5 = Dictionary_2_TryGetValue_m2659214584(L_2, L_4, (&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m2659214584_RuntimeMethod_var);
		if (L_5)
		{
			goto IL_0025;
		}
	}
	{
		return (HtmlAttribute_t1804523403 *)NULL;
	}

IL_0025:
	{
		HtmlAttribute_t1804523403 * L_6 = V_0;
		return L_6;
	}
}
// System.Int32 HtmlAgilityPack.HtmlAttributeCollection::get_Count()
extern "C"  int32_t HtmlAttributeCollection_get_Count_m1097499500 (HtmlAttributeCollection_t1787476631 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlAttributeCollection_get_Count_m1097499500_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1173644535 * L_0 = __this->get_items_2();
		NullCheck(L_0);
		int32_t L_1 = List_1_get_Count_m1117934718(L_0, /*hidden argument*/List_1_get_Count_m1117934718_RuntimeMethod_var);
		return L_1;
	}
}
// System.Boolean HtmlAgilityPack.HtmlAttributeCollection::get_IsReadOnly()
extern "C"  bool HtmlAttributeCollection_get_IsReadOnly_m2086128675 (HtmlAttributeCollection_t1787476631 * __this, const RuntimeMethod* method)
{
	{
		return (bool)0;
	}
}
// HtmlAgilityPack.HtmlAttribute HtmlAgilityPack.HtmlAttributeCollection::get_Item(System.Int32)
extern "C"  HtmlAttribute_t1804523403 * HtmlAttributeCollection_get_Item_m4090043702 (HtmlAttributeCollection_t1787476631 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlAttributeCollection_get_Item_m4090043702_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1173644535 * L_0 = __this->get_items_2();
		int32_t L_1 = ___index0;
		NullCheck(L_0);
		HtmlAttribute_t1804523403 * L_2 = List_1_get_Item_m2219204117(L_0, L_1, /*hidden argument*/List_1_get_Item_m2219204117_RuntimeMethod_var);
		return L_2;
	}
}
// System.Void HtmlAgilityPack.HtmlAttributeCollection::set_Item(System.Int32,HtmlAgilityPack.HtmlAttribute)
extern "C"  void HtmlAttributeCollection_set_Item_m3012978195 (HtmlAttributeCollection_t1787476631 * __this, int32_t ___index0, HtmlAttribute_t1804523403 * ___value1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlAttributeCollection_set_Item_m3012978195_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1173644535 * L_0 = __this->get_items_2();
		int32_t L_1 = ___index0;
		HtmlAttribute_t1804523403 * L_2 = ___value1;
		NullCheck(L_0);
		List_1_set_Item_m1351812348(L_0, L_1, L_2, /*hidden argument*/List_1_set_Item_m1351812348_RuntimeMethod_var);
		return;
	}
}
// System.Void HtmlAgilityPack.HtmlAttributeCollection::Add(HtmlAgilityPack.HtmlAttribute)
extern "C"  void HtmlAttributeCollection_Add_m1330962371 (HtmlAttributeCollection_t1787476631 * __this, HtmlAttribute_t1804523403 * ___item0, const RuntimeMethod* method)
{
	{
		HtmlAttribute_t1804523403 * L_0 = ___item0;
		HtmlAttributeCollection_Append_m138838623(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HtmlAgilityPack.HtmlAttributeCollection::System.Collections.Generic.ICollection<HtmlAgilityPack.HtmlAttribute>.Clear()
extern "C"  void HtmlAttributeCollection_System_Collections_Generic_ICollectionU3CHtmlAgilityPack_HtmlAttributeU3E_Clear_m2105867863 (HtmlAttributeCollection_t1787476631 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlAttributeCollection_System_Collections_Generic_ICollectionU3CHtmlAgilityPack_HtmlAttributeU3E_Clear_m2105867863_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1173644535 * L_0 = __this->get_items_2();
		NullCheck(L_0);
		List_1_Clear_m3486920423(L_0, /*hidden argument*/List_1_Clear_m3486920423_RuntimeMethod_var);
		return;
	}
}
// System.Boolean HtmlAgilityPack.HtmlAttributeCollection::Contains(HtmlAgilityPack.HtmlAttribute)
extern "C"  bool HtmlAttributeCollection_Contains_m2621677193 (HtmlAttributeCollection_t1787476631 * __this, HtmlAttribute_t1804523403 * ___item0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlAttributeCollection_Contains_m2621677193_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1173644535 * L_0 = __this->get_items_2();
		HtmlAttribute_t1804523403 * L_1 = ___item0;
		NullCheck(L_0);
		bool L_2 = List_1_Contains_m3092939846(L_0, L_1, /*hidden argument*/List_1_Contains_m3092939846_RuntimeMethod_var);
		return L_2;
	}
}
// System.Void HtmlAgilityPack.HtmlAttributeCollection::CopyTo(HtmlAgilityPack.HtmlAttribute[],System.Int32)
extern "C"  void HtmlAttributeCollection_CopyTo_m3645938983 (HtmlAttributeCollection_t1787476631 * __this, HtmlAttributeU5BU5D_t425089514* ___array0, int32_t ___arrayIndex1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlAttributeCollection_CopyTo_m3645938983_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1173644535 * L_0 = __this->get_items_2();
		HtmlAttributeU5BU5D_t425089514* L_1 = ___array0;
		int32_t L_2 = ___arrayIndex1;
		NullCheck(L_0);
		List_1_CopyTo_m2629566870(L_0, L_1, L_2, /*hidden argument*/List_1_CopyTo_m2629566870_RuntimeMethod_var);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<HtmlAgilityPack.HtmlAttribute> HtmlAgilityPack.HtmlAttributeCollection::System.Collections.Generic.IEnumerable<HtmlAgilityPack.HtmlAttribute>.GetEnumerator()
extern "C"  RuntimeObject* HtmlAttributeCollection_System_Collections_Generic_IEnumerableU3CHtmlAgilityPack_HtmlAttributeU3E_GetEnumerator_m3662386570 (HtmlAttributeCollection_t1787476631 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlAttributeCollection_System_Collections_Generic_IEnumerableU3CHtmlAgilityPack_HtmlAttributeU3E_GetEnumerator_m3662386570_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1173644535 * L_0 = __this->get_items_2();
		NullCheck(L_0);
		Enumerator_t708374209  L_1 = List_1_GetEnumerator_m4221754371(L_0, /*hidden argument*/List_1_GetEnumerator_m4221754371_RuntimeMethod_var);
		Enumerator_t708374209  L_2 = L_1;
		RuntimeObject * L_3 = Box(Enumerator_t708374209_il2cpp_TypeInfo_var, &L_2);
		return (RuntimeObject*)L_3;
	}
}
// System.Collections.IEnumerator HtmlAgilityPack.HtmlAttributeCollection::System.Collections.IEnumerable.GetEnumerator()
extern "C"  RuntimeObject* HtmlAttributeCollection_System_Collections_IEnumerable_GetEnumerator_m1208243079 (HtmlAttributeCollection_t1787476631 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlAttributeCollection_System_Collections_IEnumerable_GetEnumerator_m1208243079_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1173644535 * L_0 = __this->get_items_2();
		NullCheck(L_0);
		Enumerator_t708374209  L_1 = List_1_GetEnumerator_m4221754371(L_0, /*hidden argument*/List_1_GetEnumerator_m4221754371_RuntimeMethod_var);
		Enumerator_t708374209  L_2 = L_1;
		RuntimeObject * L_3 = Box(Enumerator_t708374209_il2cpp_TypeInfo_var, &L_2);
		return (RuntimeObject*)L_3;
	}
}
// System.Int32 HtmlAgilityPack.HtmlAttributeCollection::IndexOf(HtmlAgilityPack.HtmlAttribute)
extern "C"  int32_t HtmlAttributeCollection_IndexOf_m2095767915 (HtmlAttributeCollection_t1787476631 * __this, HtmlAttribute_t1804523403 * ___item0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlAttributeCollection_IndexOf_m2095767915_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1173644535 * L_0 = __this->get_items_2();
		HtmlAttribute_t1804523403 * L_1 = ___item0;
		NullCheck(L_0);
		int32_t L_2 = List_1_IndexOf_m946964392(L_0, L_1, /*hidden argument*/List_1_IndexOf_m946964392_RuntimeMethod_var);
		return L_2;
	}
}
// System.Void HtmlAgilityPack.HtmlAttributeCollection::Insert(System.Int32,HtmlAgilityPack.HtmlAttribute)
extern "C"  void HtmlAttributeCollection_Insert_m3852709710 (HtmlAttributeCollection_t1787476631 * __this, int32_t ___index0, HtmlAttribute_t1804523403 * ___item1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlAttributeCollection_Insert_m3852709710_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		HtmlAttribute_t1804523403 * L_0 = ___item1;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, _stringLiteral3168545717, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_000e:
	{
		Dictionary_2_t3719302665 * L_2 = __this->get_Hashitems_0();
		HtmlAttribute_t1804523403 * L_3 = ___item1;
		NullCheck(L_3);
		String_t* L_4 = HtmlAttribute_get_Name_m3956631639(L_3, /*hidden argument*/NULL);
		HtmlAttribute_t1804523403 * L_5 = ___item1;
		NullCheck(L_2);
		Dictionary_2_set_Item_m1171915658(L_2, L_4, L_5, /*hidden argument*/Dictionary_2_set_Item_m1171915658_RuntimeMethod_var);
		HtmlAttribute_t1804523403 * L_6 = ___item1;
		HtmlNode_t2048434459 * L_7 = __this->get__ownernode_1();
		NullCheck(L_6);
		L_6->set__ownernode_6(L_7);
		List_1_t1173644535 * L_8 = __this->get_items_2();
		int32_t L_9 = ___index0;
		HtmlAttribute_t1804523403 * L_10 = ___item1;
		NullCheck(L_8);
		List_1_Insert_m341283965(L_8, L_9, L_10, /*hidden argument*/List_1_Insert_m341283965_RuntimeMethod_var);
		HtmlNode_t2048434459 * L_11 = __this->get__ownernode_1();
		NullCheck(L_11);
		L_11->set__innerchanged_3((bool)1);
		HtmlNode_t2048434459 * L_12 = __this->get__ownernode_1();
		NullCheck(L_12);
		L_12->set__outerchanged_14((bool)1);
		return;
	}
}
// System.Boolean HtmlAgilityPack.HtmlAttributeCollection::System.Collections.Generic.ICollection<HtmlAgilityPack.HtmlAttribute>.Remove(HtmlAgilityPack.HtmlAttribute)
extern "C"  bool HtmlAttributeCollection_System_Collections_Generic_ICollectionU3CHtmlAgilityPack_HtmlAttributeU3E_Remove_m2989135424 (HtmlAttributeCollection_t1787476631 * __this, HtmlAttribute_t1804523403 * ___item0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlAttributeCollection_System_Collections_Generic_ICollectionU3CHtmlAgilityPack_HtmlAttributeU3E_Remove_m2989135424_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1173644535 * L_0 = __this->get_items_2();
		HtmlAttribute_t1804523403 * L_1 = ___item0;
		NullCheck(L_0);
		bool L_2 = List_1_Remove_m302429225(L_0, L_1, /*hidden argument*/List_1_Remove_m302429225_RuntimeMethod_var);
		return L_2;
	}
}
// System.Void HtmlAgilityPack.HtmlAttributeCollection::RemoveAt(System.Int32)
extern "C"  void HtmlAttributeCollection_RemoveAt_m2525279158 (HtmlAttributeCollection_t1787476631 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlAttributeCollection_RemoveAt_m2525279158_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	HtmlAttribute_t1804523403 * V_0 = NULL;
	{
		List_1_t1173644535 * L_0 = __this->get_items_2();
		int32_t L_1 = ___index0;
		NullCheck(L_0);
		HtmlAttribute_t1804523403 * L_2 = List_1_get_Item_m2219204117(L_0, L_1, /*hidden argument*/List_1_get_Item_m2219204117_RuntimeMethod_var);
		V_0 = L_2;
		Dictionary_2_t3719302665 * L_3 = __this->get_Hashitems_0();
		HtmlAttribute_t1804523403 * L_4 = V_0;
		NullCheck(L_4);
		String_t* L_5 = HtmlAttribute_get_Name_m3956631639(L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		Dictionary_2_Remove_m2474169460(L_3, L_5, /*hidden argument*/Dictionary_2_Remove_m2474169460_RuntimeMethod_var);
		List_1_t1173644535 * L_6 = __this->get_items_2();
		int32_t L_7 = ___index0;
		NullCheck(L_6);
		List_1_RemoveAt_m680506192(L_6, L_7, /*hidden argument*/List_1_RemoveAt_m680506192_RuntimeMethod_var);
		HtmlNode_t2048434459 * L_8 = __this->get__ownernode_1();
		NullCheck(L_8);
		L_8->set__innerchanged_3((bool)1);
		HtmlNode_t2048434459 * L_9 = __this->get__ownernode_1();
		NullCheck(L_9);
		L_9->set__outerchanged_14((bool)1);
		return;
	}
}
// HtmlAgilityPack.HtmlAttribute HtmlAgilityPack.HtmlAttributeCollection::Append(HtmlAgilityPack.HtmlAttribute)
extern "C"  HtmlAttribute_t1804523403 * HtmlAttributeCollection_Append_m138838623 (HtmlAttributeCollection_t1787476631 * __this, HtmlAttribute_t1804523403 * ___newAttribute0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlAttributeCollection_Append_m138838623_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		HtmlAttribute_t1804523403 * L_0 = ___newAttribute0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, _stringLiteral3583247784, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_000e:
	{
		Dictionary_2_t3719302665 * L_2 = __this->get_Hashitems_0();
		HtmlAttribute_t1804523403 * L_3 = ___newAttribute0;
		NullCheck(L_3);
		String_t* L_4 = HtmlAttribute_get_Name_m3956631639(L_3, /*hidden argument*/NULL);
		HtmlAttribute_t1804523403 * L_5 = ___newAttribute0;
		NullCheck(L_2);
		Dictionary_2_set_Item_m1171915658(L_2, L_4, L_5, /*hidden argument*/Dictionary_2_set_Item_m1171915658_RuntimeMethod_var);
		HtmlAttribute_t1804523403 * L_6 = ___newAttribute0;
		HtmlNode_t2048434459 * L_7 = __this->get__ownernode_1();
		NullCheck(L_6);
		L_6->set__ownernode_6(L_7);
		List_1_t1173644535 * L_8 = __this->get_items_2();
		HtmlAttribute_t1804523403 * L_9 = ___newAttribute0;
		NullCheck(L_8);
		List_1_Add_m2897938714(L_8, L_9, /*hidden argument*/List_1_Add_m2897938714_RuntimeMethod_var);
		HtmlNode_t2048434459 * L_10 = __this->get__ownernode_1();
		NullCheck(L_10);
		L_10->set__innerchanged_3((bool)1);
		HtmlNode_t2048434459 * L_11 = __this->get__ownernode_1();
		NullCheck(L_11);
		L_11->set__outerchanged_14((bool)1);
		HtmlAttribute_t1804523403 * L_12 = ___newAttribute0;
		return L_12;
	}
}
// System.Boolean HtmlAgilityPack.HtmlAttributeCollection::Contains(System.String)
extern "C"  bool HtmlAttributeCollection_Contains_m1548048803 (HtmlAttributeCollection_t1787476631 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlAttributeCollection_Contains_m1548048803_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0028;
	}

IL_0004:
	{
		List_1_t1173644535 * L_0 = __this->get_items_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		HtmlAttribute_t1804523403 * L_2 = List_1_get_Item_m2219204117(L_0, L_1, /*hidden argument*/List_1_get_Item_m2219204117_RuntimeMethod_var);
		NullCheck(L_2);
		String_t* L_3 = HtmlAttribute_get_Name_m3956631639(L_2, /*hidden argument*/NULL);
		String_t* L_4 = ___name0;
		NullCheck(L_4);
		String_t* L_5 = String_ToLower_m2994460523(L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		bool L_6 = String_Equals_m2633592423(L_3, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0024;
		}
	}
	{
		return (bool)1;
	}

IL_0024:
	{
		int32_t L_7 = V_0;
		V_0 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0028:
	{
		int32_t L_8 = V_0;
		List_1_t1173644535 * L_9 = __this->get_items_2();
		NullCheck(L_9);
		int32_t L_10 = List_1_get_Count_m1117934718(L_9, /*hidden argument*/List_1_get_Count_m1117934718_RuntimeMethod_var);
		if ((((int32_t)L_8) < ((int32_t)L_10)))
		{
			goto IL_0004;
		}
	}
	{
		return (bool)0;
	}
}
// System.Int32 HtmlAgilityPack.HtmlAttributeCollection::GetAttributeIndex(System.String)
extern "C"  int32_t HtmlAttributeCollection_GetAttributeIndex_m1301279406 (HtmlAttributeCollection_t1787476631 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlAttributeCollection_GetAttributeIndex_m1301279406_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	{
		String_t* L_0 = ___name0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, _stringLiteral2328218955, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_000e:
	{
		String_t* L_2 = ___name0;
		NullCheck(L_2);
		String_t* L_3 = String_ToLower_m2994460523(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		V_1 = 0;
		goto IL_0038;
	}

IL_0019:
	{
		List_1_t1173644535 * L_4 = __this->get_items_2();
		int32_t L_5 = V_1;
		NullCheck(L_4);
		HtmlAttribute_t1804523403 * L_6 = List_1_get_Item_m2219204117(L_4, L_5, /*hidden argument*/List_1_get_Item_m2219204117_RuntimeMethod_var);
		NullCheck(L_6);
		String_t* L_7 = HtmlAttribute_get_Name_m3956631639(L_6, /*hidden argument*/NULL);
		String_t* L_8 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_9 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0034;
		}
	}
	{
		int32_t L_10 = V_1;
		return L_10;
	}

IL_0034:
	{
		int32_t L_11 = V_1;
		V_1 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_0038:
	{
		int32_t L_12 = V_1;
		List_1_t1173644535 * L_13 = __this->get_items_2();
		NullCheck(L_13);
		int32_t L_14 = List_1_get_Count_m1117934718(L_13, /*hidden argument*/List_1_get_Count_m1117934718_RuntimeMethod_var);
		if ((((int32_t)L_12) < ((int32_t)L_14)))
		{
			goto IL_0019;
		}
	}
	{
		return (-1);
	}
}
// System.Void HtmlAgilityPack.HtmlCommentNode::.ctor(HtmlAgilityPack.HtmlDocument,System.Int32)
extern "C"  void HtmlCommentNode__ctor_m3831299973 (HtmlCommentNode_t1992371332 * __this, HtmlDocument_t556432108 * ___ownerdocument0, int32_t ___index1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlCommentNode__ctor_m3831299973_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		HtmlDocument_t556432108 * L_0 = ___ownerdocument0;
		int32_t L_1 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(HtmlNode_t2048434459_il2cpp_TypeInfo_var);
		HtmlNode__ctor_m2584575018(__this, 2, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String HtmlAgilityPack.HtmlCommentNode::get_Comment()
extern "C"  String_t* HtmlCommentNode_get_Comment_m3847846688 (HtmlCommentNode_t1992371332 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get__comment_29();
		if (L_0)
		{
			goto IL_000f;
		}
	}
	{
		String_t* L_1 = HtmlNode_get_InnerHtml_m590077535(__this, /*hidden argument*/NULL);
		return L_1;
	}

IL_000f:
	{
		String_t* L_2 = __this->get__comment_29();
		return L_2;
	}
}
// System.Void HtmlAgilityPack.HtmlCommentNode::set_Comment(System.String)
extern "C"  void HtmlCommentNode_set_Comment_m4142312819 (HtmlCommentNode_t1992371332 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set__comment_29(L_0);
		return;
	}
}
// System.String HtmlAgilityPack.HtmlCommentNode::get_InnerHtml()
extern "C"  String_t* HtmlCommentNode_get_InnerHtml_m254783644 (HtmlCommentNode_t1992371332 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get__comment_29();
		if (L_0)
		{
			goto IL_000f;
		}
	}
	{
		String_t* L_1 = HtmlNode_get_InnerHtml_m590077535(__this, /*hidden argument*/NULL);
		return L_1;
	}

IL_000f:
	{
		String_t* L_2 = __this->get__comment_29();
		return L_2;
	}
}
// System.String HtmlAgilityPack.HtmlCommentNode::get_OuterHtml()
extern "C"  String_t* HtmlCommentNode_get_OuterHtml_m868039327 (HtmlCommentNode_t1992371332 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlCommentNode_get_OuterHtml_m868039327_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = __this->get__comment_29();
		if (L_0)
		{
			goto IL_000f;
		}
	}
	{
		String_t* L_1 = HtmlNode_get_OuterHtml_m1376347664(__this, /*hidden argument*/NULL);
		return L_1;
	}

IL_000f:
	{
		String_t* L_2 = __this->get__comment_29();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral2972645825, L_2, _stringLiteral1220271454, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void HtmlAgilityPack.HtmlDocument::.ctor()
extern "C"  void HtmlDocument__ctor_m2934097387 (HtmlDocument_t556432108 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlDocument__ctor_m2934097387_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t3963213721 * L_0 = (Dictionary_2_t3963213721 *)il2cpp_codegen_object_new(Dictionary_2_t3963213721_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m850897185(L_0, /*hidden argument*/Dictionary_2__ctor_m850897185_RuntimeMethod_var);
		__this->set_Lastnodes_8(L_0);
		List_1_t484300294 * L_1 = (List_1_t484300294 *)il2cpp_codegen_object_new(List_1_t484300294_il2cpp_TypeInfo_var);
		List_1__ctor_m3187661903(L_1, /*hidden argument*/List_1__ctor_m3187661903_RuntimeMethod_var);
		__this->set__parseerrors_17(L_1);
		__this->set_OptionCheckSyntax_25((bool)1);
		__this->set_OptionExtractErrorSourceTextMaxLength_29(((int32_t)100));
		__this->set_OptionReadEncoding_35((bool)1);
		__this->set_OptionUseIdAttribute_37((bool)1);
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		HtmlNode_t2048434459 * L_2 = HtmlDocument_CreateNode_m538784785(__this, 0, 0, /*hidden argument*/NULL);
		__this->set__documentnode_5(L_2);
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
		Encoding_t663144255 * L_3 = Encoding_get_Default_m908538569(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_OptionDefaultStreamEncoding_27(L_3);
		return;
	}
}
// HtmlAgilityPack.HtmlNode HtmlAgilityPack.HtmlDocument::get_DocumentNode()
extern "C"  HtmlNode_t2048434459 * HtmlDocument_get_DocumentNode_m1723003156 (HtmlDocument_t556432108 * __this, const RuntimeMethod* method)
{
	{
		HtmlNode_t2048434459 * L_0 = __this->get__documentnode_5();
		return L_0;
	}
}
// System.String HtmlAgilityPack.HtmlDocument::GetXmlName(System.String)
extern "C"  String_t* HtmlDocument_GetXmlName_m448645048 (RuntimeObject * __this /* static, unused */, String_t* ___name0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlDocument_GetXmlName_m448645048_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	bool V_1 = false;
	int32_t V_2 = 0;
	ByteU5BU5D_t3397334013* V_3 = NULL;
	int32_t V_4 = 0;
	CharU5BU5D_t1328083999* V_5 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		V_0 = L_0;
		V_1 = (bool)1;
		V_2 = 0;
		goto IL_00ce;
	}

IL_000f:
	{
		String_t* L_1 = ___name0;
		int32_t L_2 = V_2;
		NullCheck(L_1);
		Il2CppChar L_3 = String_get_Chars_m4230566705(L_1, L_2, /*hidden argument*/NULL);
		if ((((int32_t)L_3) < ((int32_t)((int32_t)97))))
		{
			goto IL_0025;
		}
	}
	{
		String_t* L_4 = ___name0;
		int32_t L_5 = V_2;
		NullCheck(L_4);
		Il2CppChar L_6 = String_get_Chars_m4230566705(L_4, L_5, /*hidden argument*/NULL);
		if ((((int32_t)L_6) <= ((int32_t)((int32_t)122))))
		{
			goto IL_005c;
		}
	}

IL_0025:
	{
		String_t* L_7 = ___name0;
		int32_t L_8 = V_2;
		NullCheck(L_7);
		Il2CppChar L_9 = String_get_Chars_m4230566705(L_7, L_8, /*hidden argument*/NULL);
		if ((((int32_t)L_9) < ((int32_t)((int32_t)48))))
		{
			goto IL_003b;
		}
	}
	{
		String_t* L_10 = ___name0;
		int32_t L_11 = V_2;
		NullCheck(L_10);
		Il2CppChar L_12 = String_get_Chars_m4230566705(L_10, L_11, /*hidden argument*/NULL);
		if ((((int32_t)L_12) <= ((int32_t)((int32_t)57))))
		{
			goto IL_005c;
		}
	}

IL_003b:
	{
		String_t* L_13 = ___name0;
		int32_t L_14 = V_2;
		NullCheck(L_13);
		Il2CppChar L_15 = String_get_Chars_m4230566705(L_13, L_14, /*hidden argument*/NULL);
		if ((((int32_t)L_15) == ((int32_t)((int32_t)95))))
		{
			goto IL_005c;
		}
	}
	{
		String_t* L_16 = ___name0;
		int32_t L_17 = V_2;
		NullCheck(L_16);
		Il2CppChar L_18 = String_get_Chars_m4230566705(L_16, L_17, /*hidden argument*/NULL);
		if ((((int32_t)L_18) == ((int32_t)((int32_t)45))))
		{
			goto IL_005c;
		}
	}
	{
		String_t* L_19 = ___name0;
		int32_t L_20 = V_2;
		NullCheck(L_19);
		Il2CppChar L_21 = String_get_Chars_m4230566705(L_19, L_20, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_21) == ((uint32_t)((int32_t)46)))))
		{
			goto IL_0071;
		}
	}

IL_005c:
	{
		String_t* L_22 = V_0;
		String_t* L_23 = ___name0;
		int32_t L_24 = V_2;
		NullCheck(L_23);
		Il2CppChar L_25 = String_get_Chars_m4230566705(L_23, L_24, /*hidden argument*/NULL);
		Il2CppChar L_26 = L_25;
		RuntimeObject * L_27 = Box(Char_t3454481338_il2cpp_TypeInfo_var, &L_26);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_28 = String_Concat_m56707527(NULL /*static, unused*/, L_22, L_27, /*hidden argument*/NULL);
		V_0 = L_28;
		goto IL_00ca;
	}

IL_0071:
	{
		V_1 = (bool)0;
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
		Encoding_t663144255 * L_29 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_5 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
		CharU5BU5D_t1328083999* L_30 = V_5;
		String_t* L_31 = ___name0;
		int32_t L_32 = V_2;
		NullCheck(L_31);
		Il2CppChar L_33 = String_get_Chars_m4230566705(L_31, L_32, /*hidden argument*/NULL);
		NullCheck(L_30);
		(L_30)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)L_33);
		CharU5BU5D_t1328083999* L_34 = V_5;
		NullCheck(L_29);
		ByteU5BU5D_t3397334013* L_35 = VirtFuncInvoker1< ByteU5BU5D_t3397334013*, CharU5BU5D_t1328083999* >::Invoke(12 /* System.Byte[] System.Text.Encoding::GetBytes(System.Char[]) */, L_29, L_34);
		V_3 = L_35;
		V_4 = 0;
		goto IL_00b7;
	}

IL_0098:
	{
		String_t* L_36 = V_0;
		ByteU5BU5D_t3397334013* L_37 = V_3;
		int32_t L_38 = V_4;
		NullCheck(L_37);
		String_t* L_39 = Byte_ToString_m1309661918(((L_37)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))), _stringLiteral3231012720, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_40 = String_Concat_m2596409543(NULL /*static, unused*/, L_36, L_39, /*hidden argument*/NULL);
		V_0 = L_40;
		int32_t L_41 = V_4;
		V_4 = ((int32_t)((int32_t)L_41+(int32_t)1));
	}

IL_00b7:
	{
		int32_t L_42 = V_4;
		ByteU5BU5D_t3397334013* L_43 = V_3;
		NullCheck(L_43);
		if ((((int32_t)L_42) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_43)->max_length)))))))
		{
			goto IL_0098;
		}
	}
	{
		String_t* L_44 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_45 = String_Concat_m2596409543(NULL /*static, unused*/, L_44, _stringLiteral372029427, /*hidden argument*/NULL);
		V_0 = L_45;
	}

IL_00ca:
	{
		int32_t L_46 = V_2;
		V_2 = ((int32_t)((int32_t)L_46+(int32_t)1));
	}

IL_00ce:
	{
		int32_t L_47 = V_2;
		String_t* L_48 = ___name0;
		NullCheck(L_48);
		int32_t L_49 = String_get_Length_m1606060069(L_48, /*hidden argument*/NULL);
		if ((((int32_t)L_47) < ((int32_t)L_49)))
		{
			goto IL_000f;
		}
	}
	{
		bool L_50 = V_1;
		if (!L_50)
		{
			goto IL_00df;
		}
	}
	{
		String_t* L_51 = V_0;
		return L_51;
	}

IL_00df:
	{
		String_t* L_52 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_53 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral372029427, L_52, /*hidden argument*/NULL);
		return L_53;
	}
}
// System.String HtmlAgilityPack.HtmlDocument::HtmlEncode(System.String)
extern "C"  String_t* HtmlDocument_HtmlEncode_m2155231231 (RuntimeObject * __this /* static, unused */, String_t* ___html0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlDocument_HtmlEncode_m2155231231_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Regex_t1803876613 * V_0 = NULL;
	{
		String_t* L_0 = ___html0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, _stringLiteral439662321, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_000e:
	{
		Regex_t1803876613 * L_2 = (Regex_t1803876613 *)il2cpp_codegen_object_new(Regex_t1803876613_il2cpp_TypeInfo_var);
		Regex__ctor_m2521903438(L_2, _stringLiteral241646909, 1, /*hidden argument*/NULL);
		V_0 = L_2;
		Regex_t1803876613 * L_3 = V_0;
		String_t* L_4 = ___html0;
		NullCheck(L_3);
		String_t* L_5 = Regex_Replace_m2510734387(L_3, L_4, _stringLiteral2206518777, /*hidden argument*/NULL);
		NullCheck(L_5);
		String_t* L_6 = String_Replace_m1941156251(L_5, _stringLiteral372029330, _stringLiteral352199793, /*hidden argument*/NULL);
		NullCheck(L_6);
		String_t* L_7 = String_Replace_m1941156251(L_6, _stringLiteral372029332, _stringLiteral1259525396, /*hidden argument*/NULL);
		NullCheck(L_7);
		String_t* L_8 = String_Replace_m1941156251(L_7, _stringLiteral372029312, _stringLiteral1693198914, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Boolean HtmlAgilityPack.HtmlDocument::IsWhiteSpace(System.Int32)
extern "C"  bool HtmlDocument_IsWhiteSpace_m1502326855 (RuntimeObject * __this /* static, unused */, int32_t ___c0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___c0;
		if ((((int32_t)L_0) == ((int32_t)((int32_t)10))))
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_1 = ___c0;
		if ((((int32_t)L_1) == ((int32_t)((int32_t)13))))
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_2 = ___c0;
		if ((((int32_t)L_2) == ((int32_t)((int32_t)32))))
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_3 = ___c0;
		if ((!(((uint32_t)L_3) == ((uint32_t)((int32_t)9)))))
		{
			goto IL_0016;
		}
	}

IL_0014:
	{
		return (bool)1;
	}

IL_0016:
	{
		return (bool)0;
	}
}
// HtmlAgilityPack.HtmlAttribute HtmlAgilityPack.HtmlDocument::CreateAttribute(System.String)
extern "C"  HtmlAttribute_t1804523403 * HtmlDocument_CreateAttribute_m674678432 (HtmlDocument_t556432108 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlDocument_CreateAttribute_m674678432_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	HtmlAttribute_t1804523403 * V_0 = NULL;
	{
		String_t* L_0 = ___name0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, _stringLiteral2328218955, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_000e:
	{
		HtmlAttribute_t1804523403 * L_2 = HtmlDocument_CreateAttribute_m1400299982(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		HtmlAttribute_t1804523403 * L_3 = V_0;
		String_t* L_4 = ___name0;
		NullCheck(L_3);
		HtmlAttribute_set_Name_m1231032678(L_3, L_4, /*hidden argument*/NULL);
		HtmlAttribute_t1804523403 * L_5 = V_0;
		return L_5;
	}
}
// HtmlAgilityPack.HtmlAttribute HtmlAgilityPack.HtmlDocument::CreateAttribute(System.String,System.String)
extern "C"  HtmlAttribute_t1804523403 * HtmlDocument_CreateAttribute_m4240580628 (HtmlDocument_t556432108 * __this, String_t* ___name0, String_t* ___value1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlDocument_CreateAttribute_m4240580628_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	HtmlAttribute_t1804523403 * V_0 = NULL;
	{
		String_t* L_0 = ___name0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, _stringLiteral2328218955, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_000e:
	{
		String_t* L_2 = ___name0;
		HtmlAttribute_t1804523403 * L_3 = HtmlDocument_CreateAttribute_m674678432(__this, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		HtmlAttribute_t1804523403 * L_4 = V_0;
		String_t* L_5 = ___value1;
		NullCheck(L_4);
		HtmlAttribute_set_Value_m3484292440(L_4, L_5, /*hidden argument*/NULL);
		HtmlAttribute_t1804523403 * L_6 = V_0;
		return L_6;
	}
}
// HtmlAgilityPack.HtmlNode HtmlAgilityPack.HtmlDocument::GetElementbyId(System.String)
extern "C"  HtmlNode_t2048434459 * HtmlDocument_GetElementbyId_m1591120990 (HtmlDocument_t556432108 * __this, String_t* ___id0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlDocument_GetElementbyId_m1591120990_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___id0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, _stringLiteral287061489, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_000e:
	{
		Dictionary_2_t3963213721 * L_2 = __this->get_Nodesid_13();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(HtmlDocument_t556432108_il2cpp_TypeInfo_var);
		String_t* L_3 = ((HtmlDocument_t556432108_StaticFields*)il2cpp_codegen_static_fields_for(HtmlDocument_t556432108_il2cpp_TypeInfo_var))->get_HtmlExceptionUseIdAttributeFalse_40();
		Exception_t1927440687 * L_4 = (Exception_t1927440687 *)il2cpp_codegen_object_new(Exception_t1927440687_il2cpp_TypeInfo_var);
		Exception__ctor_m485833136(L_4, L_3, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0021:
	{
		Dictionary_2_t3963213721 * L_5 = __this->get_Nodesid_13();
		String_t* L_6 = ___id0;
		NullCheck(L_6);
		String_t* L_7 = String_ToLower_m2994460523(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		bool L_8 = Dictionary_2_ContainsKey_m3415399212(L_5, L_7, /*hidden argument*/Dictionary_2_ContainsKey_m3415399212_RuntimeMethod_var);
		if (L_8)
		{
			goto IL_0036;
		}
	}
	{
		return (HtmlNode_t2048434459 *)NULL;
	}

IL_0036:
	{
		Dictionary_2_t3963213721 * L_9 = __this->get_Nodesid_13();
		String_t* L_10 = ___id0;
		NullCheck(L_10);
		String_t* L_11 = String_ToLower_m2994460523(L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		HtmlNode_t2048434459 * L_12 = Dictionary_2_get_Item_m3051461655(L_9, L_11, /*hidden argument*/Dictionary_2_get_Item_m3051461655_RuntimeMethod_var);
		return L_12;
	}
}
// System.Void HtmlAgilityPack.HtmlDocument::Load(System.IO.TextReader)
extern "C"  void HtmlDocument_Load_m511797890 (HtmlDocument_t556432108 * __this, TextReader_t1561828458 * ___reader0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlDocument_Load_m511797890_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StreamReader_t2360341767 * V_0 = NULL;
	HtmlNode_t2048434459 * V_1 = NULL;
	String_t* V_2 = NULL;
	Enumerator_t2742792858  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		TextReader_t1561828458 * L_0 = ___reader0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, _stringLiteral51169761, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_000e:
	{
		__this->set__onlyDetectEncoding_15((bool)0);
		bool L_2 = __this->get_OptionCheckSyntax_25();
		if (!L_2)
		{
			goto IL_002a;
		}
	}
	{
		Dictionary_2_t1056260094 * L_3 = (Dictionary_2_t1056260094 *)il2cpp_codegen_object_new(Dictionary_2_t1056260094_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3832368222(L_3, /*hidden argument*/Dictionary_2__ctor_m3832368222_RuntimeMethod_var);
		__this->set_Openednodes_16(L_3);
		goto IL_0031;
	}

IL_002a:
	{
		__this->set_Openednodes_16((Dictionary_2_t1056260094 *)NULL);
	}

IL_0031:
	{
		bool L_4 = __this->get_OptionUseIdAttribute_37();
		if (!L_4)
		{
			goto IL_0046;
		}
	}
	{
		Dictionary_2_t3963213721 * L_5 = (Dictionary_2_t3963213721 *)il2cpp_codegen_object_new(Dictionary_2_t3963213721_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m850897185(L_5, /*hidden argument*/Dictionary_2__ctor_m850897185_RuntimeMethod_var);
		__this->set_Nodesid_13(L_5);
		goto IL_004d;
	}

IL_0046:
	{
		__this->set_Nodesid_13((Dictionary_2_t3963213721 *)NULL);
	}

IL_004d:
	{
		TextReader_t1561828458 * L_6 = ___reader0;
		V_0 = ((StreamReader_t2360341767 *)IsInstClass((RuntimeObject*)L_6, StreamReader_t2360341767_il2cpp_TypeInfo_var));
		StreamReader_t2360341767 * L_7 = V_0;
		if (!L_7)
		{
			goto IL_0071;
		}
	}

IL_0057:
	try
	{ // begin try (depth: 1)
		StreamReader_t2360341767 * L_8 = V_0;
		NullCheck(L_8);
		VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 System.IO.TextReader::Peek() */, L_8);
		goto IL_0063;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1927440687_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0060;
		throw e;
	}

CATCH_0060:
	{ // begin catch(System.Exception)
		goto IL_0063;
	} // end catch (depth: 1)

IL_0063:
	{
		StreamReader_t2360341767 * L_9 = V_0;
		NullCheck(L_9);
		Encoding_t663144255 * L_10 = VirtFuncInvoker0< Encoding_t663144255 * >::Invoke(11 /* System.Text.Encoding System.IO.StreamReader::get_CurrentEncoding() */, L_9);
		__this->set__streamencoding_21(L_10);
		goto IL_0078;
	}

IL_0071:
	{
		__this->set__streamencoding_21((Encoding_t663144255 *)NULL);
	}

IL_0078:
	{
		__this->set__declaredencoding_4((Encoding_t663144255 *)NULL);
		TextReader_t1561828458 * L_11 = ___reader0;
		NullCheck(L_11);
		String_t* L_12 = VirtFuncInvoker0< String_t* >::Invoke(10 /* System.String System.IO.TextReader::ReadToEnd() */, L_11);
		__this->set_Text_22(L_12);
		HtmlNode_t2048434459 * L_13 = HtmlDocument_CreateNode_m538784785(__this, 0, 0, /*hidden argument*/NULL);
		__this->set__documentnode_5(L_13);
		HtmlDocument_Parse_m3513675482(__this, /*hidden argument*/NULL);
		bool L_14 = __this->get_OptionCheckSyntax_25();
		if (!L_14)
		{
			goto IL_00af;
		}
	}
	{
		Dictionary_2_t1056260094 * L_15 = __this->get_Openednodes_16();
		if (L_15)
		{
			goto IL_00b0;
		}
	}

IL_00af:
	{
		return;
	}

IL_00b0:
	{
		Dictionary_2_t1056260094 * L_16 = __this->get_Openednodes_16();
		NullCheck(L_16);
		ValueCollection_t4054287233 * L_17 = Dictionary_2_get_Values_m2956437804(L_16, /*hidden argument*/Dictionary_2_get_Values_m2956437804_RuntimeMethod_var);
		NullCheck(L_17);
		Enumerator_t2742792858  L_18 = ValueCollection_GetEnumerator_m2005408300(L_17, /*hidden argument*/ValueCollection_GetEnumerator_m2005408300_RuntimeMethod_var);
		V_3 = L_18;
	}

IL_00c1:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0135;
		}

IL_00c3:
		{
			HtmlNode_t2048434459 * L_19 = Enumerator_get_Current_m3107603046((&V_3), /*hidden argument*/Enumerator_get_Current_m3107603046_RuntimeMethod_var);
			V_1 = L_19;
			HtmlNode_t2048434459 * L_20 = V_1;
			NullCheck(L_20);
			bool L_21 = L_20->get__starttag_23();
			if (!L_21)
			{
				goto IL_0135;
			}
		}

IL_00d3:
		{
			bool L_22 = __this->get_OptionExtractErrorSourceText_28();
			if (!L_22)
			{
				goto IL_0100;
			}
		}

IL_00db:
		{
			HtmlNode_t2048434459 * L_23 = V_1;
			NullCheck(L_23);
			String_t* L_24 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String HtmlAgilityPack.HtmlNode::get_OuterHtml() */, L_23);
			V_2 = L_24;
			String_t* L_25 = V_2;
			NullCheck(L_25);
			int32_t L_26 = String_get_Length_m1606060069(L_25, /*hidden argument*/NULL);
			int32_t L_27 = __this->get_OptionExtractErrorSourceTextMaxLength_29();
			if ((((int32_t)L_26) <= ((int32_t)L_27)))
			{
				goto IL_0106;
			}
		}

IL_00f0:
		{
			String_t* L_28 = V_2;
			int32_t L_29 = __this->get_OptionExtractErrorSourceTextMaxLength_29();
			NullCheck(L_28);
			String_t* L_30 = String_Substring_m12482732(L_28, 0, L_29, /*hidden argument*/NULL);
			V_2 = L_30;
			goto IL_0106;
		}

IL_0100:
		{
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_31 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
			V_2 = L_31;
		}

IL_0106:
		{
			HtmlNode_t2048434459 * L_32 = V_1;
			NullCheck(L_32);
			int32_t L_33 = L_32->get__line_7();
			HtmlNode_t2048434459 * L_34 = V_1;
			NullCheck(L_34);
			int32_t L_35 = L_34->get__lineposition_8();
			HtmlNode_t2048434459 * L_36 = V_1;
			NullCheck(L_36);
			int32_t L_37 = L_36->get__streamposition_24();
			String_t* L_38 = V_2;
			HtmlNode_t2048434459 * L_39 = V_1;
			NullCheck(L_39);
			String_t* L_40 = HtmlNode_get_Name_m702134575(L_39, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_41 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral2008880196, L_40, _stringLiteral929783012, /*hidden argument*/NULL);
			HtmlDocument_AddError_m2023821899(__this, 0, L_33, L_35, L_37, L_38, L_41, /*hidden argument*/NULL);
		}

IL_0135:
		{
			bool L_42 = Enumerator_MoveNext_m3092086129((&V_3), /*hidden argument*/Enumerator_MoveNext_m3092086129_RuntimeMethod_var);
			if (L_42)
			{
				goto IL_00c3;
			}
		}

IL_013e:
		{
			IL2CPP_LEAVE(0x14E, FINALLY_0140);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0140;
	}

FINALLY_0140:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m393799376((&V_3), /*hidden argument*/Enumerator_Dispose_m393799376_RuntimeMethod_var);
		IL2CPP_END_FINALLY(320)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(320)
	{
		IL2CPP_JUMP_TBL(0x14E, IL_014e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_014e:
	{
		Dictionary_2_t1056260094 * L_43 = __this->get_Openednodes_16();
		NullCheck(L_43);
		Dictionary_2_Clear_m3754984071(L_43, /*hidden argument*/Dictionary_2_Clear_m3754984071_RuntimeMethod_var);
		return;
	}
}
// System.Void HtmlAgilityPack.HtmlDocument::LoadHtml(System.String)
extern "C"  void HtmlDocument_LoadHtml_m4284235418 (HtmlDocument_t556432108 * __this, String_t* ___html0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlDocument_LoadHtml_m4284235418_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringReader_t1480123486 * V_0 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = ___html0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, _stringLiteral439662321, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_000e:
	{
		String_t* L_2 = ___html0;
		StringReader_t1480123486 * L_3 = (StringReader_t1480123486 *)il2cpp_codegen_object_new(StringReader_t1480123486_il2cpp_TypeInfo_var);
		StringReader__ctor_m643998729(L_3, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
	}

IL_0015:
	try
	{ // begin try (depth: 1)
		StringReader_t1480123486 * L_4 = V_0;
		HtmlDocument_Load_m511797890(__this, L_4, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x28, FINALLY_001e);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_001e;
	}

FINALLY_001e:
	{ // begin finally (depth: 1)
		{
			StringReader_t1480123486 * L_5 = V_0;
			if (!L_5)
			{
				goto IL_0027;
			}
		}

IL_0021:
		{
			StringReader_t1480123486 * L_6 = V_0;
			NullCheck(L_6);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_6);
		}

IL_0027:
		{
			IL2CPP_END_FINALLY(30)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(30)
	{
		IL2CPP_JUMP_TBL(0x28, IL_0028)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0028:
	{
		return;
	}
}
// HtmlAgilityPack.HtmlAttribute HtmlAgilityPack.HtmlDocument::CreateAttribute()
extern "C"  HtmlAttribute_t1804523403 * HtmlDocument_CreateAttribute_m1400299982 (HtmlDocument_t556432108 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlDocument_CreateAttribute_m1400299982_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		HtmlAttribute_t1804523403 * L_0 = (HtmlAttribute_t1804523403 *)il2cpp_codegen_object_new(HtmlAttribute_t1804523403_il2cpp_TypeInfo_var);
		HtmlAttribute__ctor_m3510814191(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// HtmlAgilityPack.HtmlNode HtmlAgilityPack.HtmlDocument::CreateNode(HtmlAgilityPack.HtmlNodeType)
extern "C"  HtmlNode_t2048434459 * HtmlDocument_CreateNode_m690978292 (HtmlDocument_t556432108 * __this, int32_t ___type0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___type0;
		HtmlNode_t2048434459 * L_1 = HtmlDocument_CreateNode_m538784785(__this, L_0, (-1), /*hidden argument*/NULL);
		return L_1;
	}
}
// HtmlAgilityPack.HtmlNode HtmlAgilityPack.HtmlDocument::CreateNode(HtmlAgilityPack.HtmlNodeType,System.Int32)
extern "C"  HtmlNode_t2048434459 * HtmlDocument_CreateNode_m538784785 (HtmlDocument_t556432108 * __this, int32_t ___type0, int32_t ___index1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlDocument_CreateNode_m538784785_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___type0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (((int32_t)((int32_t)L_1-(int32_t)2)))
		{
			case 0:
			{
				goto IL_0014;
			}
			case 1:
			{
				goto IL_001c;
			}
		}
	}
	{
		goto IL_0024;
	}

IL_0014:
	{
		int32_t L_2 = ___index1;
		HtmlCommentNode_t1992371332 * L_3 = (HtmlCommentNode_t1992371332 *)il2cpp_codegen_object_new(HtmlCommentNode_t1992371332_il2cpp_TypeInfo_var);
		HtmlCommentNode__ctor_m3831299973(L_3, __this, L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_001c:
	{
		int32_t L_4 = ___index1;
		HtmlTextNode_t2710098554 * L_5 = (HtmlTextNode_t2710098554 *)il2cpp_codegen_object_new(HtmlTextNode_t2710098554_il2cpp_TypeInfo_var);
		HtmlTextNode__ctor_m3649714099(L_5, __this, L_4, /*hidden argument*/NULL);
		return L_5;
	}

IL_0024:
	{
		int32_t L_6 = ___type0;
		int32_t L_7 = ___index1;
		HtmlNode_t2048434459 * L_8 = (HtmlNode_t2048434459 *)il2cpp_codegen_object_new(HtmlNode_t2048434459_il2cpp_TypeInfo_var);
		HtmlNode__ctor_m2584575018(L_8, L_6, __this, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Text.Encoding HtmlAgilityPack.HtmlDocument::GetOutEncoding()
extern "C"  Encoding_t663144255 * HtmlDocument_GetOutEncoding_m2726542450 (HtmlDocument_t556432108 * __this, const RuntimeMethod* method)
{
	Encoding_t663144255 * G_B3_0 = NULL;
	Encoding_t663144255 * G_B1_0 = NULL;
	Encoding_t663144255 * G_B2_0 = NULL;
	{
		Encoding_t663144255 * L_0 = __this->get__declaredencoding_4();
		Encoding_t663144255 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B3_0 = L_1;
			goto IL_001a;
		}
	}
	{
		Encoding_t663144255 * L_2 = __this->get__streamencoding_21();
		Encoding_t663144255 * L_3 = L_2;
		G_B2_0 = L_3;
		if (L_3)
		{
			G_B3_0 = L_3;
			goto IL_001a;
		}
	}
	{
		Encoding_t663144255 * L_4 = __this->get_OptionDefaultStreamEncoding_27();
		G_B3_0 = L_4;
	}

IL_001a:
	{
		return G_B3_0;
	}
}
// HtmlAgilityPack.HtmlNode HtmlAgilityPack.HtmlDocument::GetXmlDeclaration()
extern "C"  HtmlNode_t2048434459 * HtmlDocument_GetXmlDeclaration_m3305216451 (HtmlDocument_t556432108 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlDocument_GetXmlDeclaration_m3305216451_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	HtmlNode_t2048434459 * V_0 = NULL;
	HtmlNode_t2048434459 * V_1 = NULL;
	RuntimeObject* V_2 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		HtmlNode_t2048434459 * L_0 = __this->get__documentnode_5();
		NullCheck(L_0);
		bool L_1 = HtmlNode_get_HasChildNodes_m1218535674(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_000f;
		}
	}
	{
		return (HtmlNode_t2048434459 *)NULL;
	}

IL_000f:
	{
		HtmlNode_t2048434459 * L_2 = __this->get__documentnode_5();
		NullCheck(L_2);
		HtmlNodeCollection_t2542734491 * L_3 = L_2->get__childnodes_1();
		NullCheck(L_3);
		RuntimeObject* L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<HtmlAgilityPack.HtmlNode>::GetEnumerator() */, IEnumerable_1_t2340561504_il2cpp_TypeInfo_var, L_3);
		V_2 = L_4;
	}

IL_0020:
	try
	{ // begin try (depth: 1)
		{
			goto IL_003f;
		}

IL_0022:
		{
			RuntimeObject* L_5 = V_2;
			NullCheck(L_5);
			HtmlNode_t2048434459 * L_6 = InterfaceFuncInvoker0< HtmlNode_t2048434459 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<HtmlAgilityPack.HtmlNode>::get_Current() */, IEnumerator_1_t3818925582_il2cpp_TypeInfo_var, L_5);
			V_0 = L_6;
			HtmlNode_t2048434459 * L_7 = V_0;
			NullCheck(L_7);
			String_t* L_8 = HtmlNode_get_Name_m702134575(L_7, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_9 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_8, _stringLiteral3202301804, /*hidden argument*/NULL);
			if (!L_9)
			{
				goto IL_003f;
			}
		}

IL_003b:
		{
			HtmlNode_t2048434459 * L_10 = V_0;
			V_1 = L_10;
			IL2CPP_LEAVE(0x55, FINALLY_0049);
		}

IL_003f:
		{
			RuntimeObject* L_11 = V_2;
			NullCheck(L_11);
			bool L_12 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_11);
			if (L_12)
			{
				goto IL_0022;
			}
		}

IL_0047:
		{
			IL2CPP_LEAVE(0x53, FINALLY_0049);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0049;
	}

FINALLY_0049:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_13 = V_2;
			if (!L_13)
			{
				goto IL_0052;
			}
		}

IL_004c:
		{
			RuntimeObject* L_14 = V_2;
			NullCheck(L_14);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_14);
		}

IL_0052:
		{
			IL2CPP_END_FINALLY(73)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(73)
	{
		IL2CPP_JUMP_TBL(0x55, IL_0055)
		IL2CPP_JUMP_TBL(0x53, IL_0053)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0053:
	{
		return (HtmlNode_t2048434459 *)NULL;
	}

IL_0055:
	{
		HtmlNode_t2048434459 * L_15 = V_1;
		return L_15;
	}
}
// System.Void HtmlAgilityPack.HtmlDocument::SetIdForNode(HtmlAgilityPack.HtmlNode,System.String)
extern "C"  void HtmlDocument_SetIdForNode_m403125345 (HtmlDocument_t556432108 * __this, HtmlNode_t2048434459 * ___node0, String_t* ___id1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlDocument_SetIdForNode_m403125345_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_OptionUseIdAttribute_37();
		if (L_0)
		{
			goto IL_0009;
		}
	}
	{
		return;
	}

IL_0009:
	{
		Dictionary_2_t3963213721 * L_1 = __this->get_Nodesid_13();
		if (!L_1)
		{
			goto IL_0014;
		}
	}
	{
		String_t* L_2 = ___id1;
		if (L_2)
		{
			goto IL_0015;
		}
	}

IL_0014:
	{
		return;
	}

IL_0015:
	{
		HtmlNode_t2048434459 * L_3 = ___node0;
		if (L_3)
		{
			goto IL_002b;
		}
	}
	{
		Dictionary_2_t3963213721 * L_4 = __this->get_Nodesid_13();
		String_t* L_5 = ___id1;
		NullCheck(L_5);
		String_t* L_6 = String_ToLower_m2994460523(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		Dictionary_2_Remove_m4126380600(L_4, L_6, /*hidden argument*/Dictionary_2_Remove_m4126380600_RuntimeMethod_var);
		return;
	}

IL_002b:
	{
		Dictionary_2_t3963213721 * L_7 = __this->get_Nodesid_13();
		String_t* L_8 = ___id1;
		NullCheck(L_8);
		String_t* L_9 = String_ToLower_m2994460523(L_8, /*hidden argument*/NULL);
		HtmlNode_t2048434459 * L_10 = ___node0;
		NullCheck(L_7);
		Dictionary_2_set_Item_m3521183286(L_7, L_9, L_10, /*hidden argument*/Dictionary_2_set_Item_m3521183286_RuntimeMethod_var);
		return;
	}
}
// System.Void HtmlAgilityPack.HtmlDocument::UpdateLastParentNode()
extern "C"  void HtmlDocument_UpdateLastParentNode_m3368866332 (HtmlDocument_t556432108 * __this, const RuntimeMethod* method)
{

IL_0000:
	{
		HtmlNode_t2048434459 * L_0 = __this->get__lastparentnode_9();
		NullCheck(L_0);
		bool L_1 = HtmlNode_get_Closed_m650109995(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001e;
		}
	}
	{
		HtmlNode_t2048434459 * L_2 = __this->get__lastparentnode_9();
		NullCheck(L_2);
		HtmlNode_t2048434459 * L_3 = HtmlNode_get_ParentNode_m1888227040(L_2, /*hidden argument*/NULL);
		__this->set__lastparentnode_9(L_3);
	}

IL_001e:
	{
		HtmlNode_t2048434459 * L_4 = __this->get__lastparentnode_9();
		if (!L_4)
		{
			goto IL_0033;
		}
	}
	{
		HtmlNode_t2048434459 * L_5 = __this->get__lastparentnode_9();
		NullCheck(L_5);
		bool L_6 = HtmlNode_get_Closed_m650109995(L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0000;
		}
	}

IL_0033:
	{
		HtmlNode_t2048434459 * L_7 = __this->get__lastparentnode_9();
		if (L_7)
		{
			goto IL_0047;
		}
	}
	{
		HtmlNode_t2048434459 * L_8 = __this->get__documentnode_5();
		__this->set__lastparentnode_9(L_8);
	}

IL_0047:
	{
		return;
	}
}
// System.Void HtmlAgilityPack.HtmlDocument::AddError(HtmlAgilityPack.HtmlParseErrorCode,System.Int32,System.Int32,System.Int32,System.String,System.String)
extern "C"  void HtmlDocument_AddError_m2023821899 (HtmlDocument_t556432108 * __this, int32_t ___code0, int32_t ___line1, int32_t ___linePosition2, int32_t ___streamPosition3, String_t* ___sourceText4, String_t* ___reason5, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlDocument_AddError_m2023821899_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	HtmlParseError_t1115179162 * V_0 = NULL;
	{
		int32_t L_0 = ___code0;
		int32_t L_1 = ___line1;
		int32_t L_2 = ___linePosition2;
		int32_t L_3 = ___streamPosition3;
		String_t* L_4 = ___sourceText4;
		String_t* L_5 = ___reason5;
		HtmlParseError_t1115179162 * L_6 = (HtmlParseError_t1115179162 *)il2cpp_codegen_object_new(HtmlParseError_t1115179162_il2cpp_TypeInfo_var);
		HtmlParseError__ctor_m3785891130(L_6, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		List_1_t484300294 * L_7 = __this->get__parseerrors_17();
		HtmlParseError_t1115179162 * L_8 = V_0;
		NullCheck(L_7);
		List_1_Add_m3464026123(L_7, L_8, /*hidden argument*/List_1_Add_m3464026123_RuntimeMethod_var);
		return;
	}
}
// System.Void HtmlAgilityPack.HtmlDocument::CloseCurrentNode()
extern "C"  void HtmlDocument_CloseCurrentNode_m2823362304 (HtmlDocument_t556432108 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlDocument_CloseCurrentNode_m2823362304_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	HtmlNode_t2048434459 * V_1 = NULL;
	HtmlNode_t2048434459 * V_2 = NULL;
	Stack_1_t3136162613 * V_3 = NULL;
	HtmlNode_t2048434459 * V_4 = NULL;
	HtmlNode_t2048434459 * V_5 = NULL;
	HtmlNode_t2048434459 * V_6 = NULL;
	{
		HtmlNode_t2048434459 * L_0 = __this->get__currentnode_3();
		NullCheck(L_0);
		bool L_1 = HtmlNode_get_Closed_m650109995(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000e;
		}
	}
	{
		return;
	}

IL_000e:
	{
		V_0 = (bool)0;
		Dictionary_2_t3963213721 * L_2 = __this->get_Lastnodes_8();
		HtmlNode_t2048434459 * L_3 = __this->get__currentnode_3();
		NullCheck(L_3);
		String_t* L_4 = HtmlNode_get_Name_m702134575(L_3, /*hidden argument*/NULL);
		HtmlNode_t2048434459 * L_5 = Utilities_GetDictionaryValueOrNull_TisString_t_TisHtmlNode_t2048434459_m2137127104(NULL /*static, unused*/, L_2, L_4, /*hidden argument*/Utilities_GetDictionaryValueOrNull_TisString_t_TisHtmlNode_t2048434459_m2137127104_RuntimeMethod_var);
		V_1 = L_5;
		HtmlNode_t2048434459 * L_6 = V_1;
		if (L_6)
		{
			goto IL_0224;
		}
	}
	{
		HtmlNode_t2048434459 * L_7 = __this->get__currentnode_3();
		NullCheck(L_7);
		String_t* L_8 = HtmlNode_get_Name_m702134575(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(HtmlNode_t2048434459_il2cpp_TypeInfo_var);
		bool L_9 = HtmlNode_IsClosedElement_m2492183536(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_00f9;
		}
	}
	{
		HtmlNode_t2048434459 * L_10 = __this->get__currentnode_3();
		HtmlNode_t2048434459 * L_11 = __this->get__currentnode_3();
		NullCheck(L_10);
		HtmlNode_CloseNode_m1160950766(L_10, L_11, /*hidden argument*/NULL);
		HtmlNode_t2048434459 * L_12 = __this->get__lastparentnode_9();
		if (!L_12)
		{
			goto IL_02c0;
		}
	}
	{
		V_2 = (HtmlNode_t2048434459 *)NULL;
		Stack_1_t3136162613 * L_13 = (Stack_1_t3136162613 *)il2cpp_codegen_object_new(Stack_1_t3136162613_il2cpp_TypeInfo_var);
		Stack_1__ctor_m1173151588(L_13, /*hidden argument*/Stack_1__ctor_m1173151588_RuntimeMethod_var);
		V_3 = L_13;
		HtmlNode_t2048434459 * L_14 = __this->get__lastparentnode_9();
		NullCheck(L_14);
		HtmlNode_t2048434459 * L_15 = HtmlNode_get_LastChild_m2408860244(L_14, /*hidden argument*/NULL);
		V_4 = L_15;
		goto IL_00ad;
	}

IL_0075:
	{
		HtmlNode_t2048434459 * L_16 = V_4;
		NullCheck(L_16);
		String_t* L_17 = HtmlNode_get_Name_m702134575(L_16, /*hidden argument*/NULL);
		HtmlNode_t2048434459 * L_18 = __this->get__currentnode_3();
		NullCheck(L_18);
		String_t* L_19 = HtmlNode_get_Name_m702134575(L_18, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_20 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_17, L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_009c;
		}
	}
	{
		HtmlNode_t2048434459 * L_21 = V_4;
		NullCheck(L_21);
		bool L_22 = HtmlNode_get_HasChildNodes_m1218535674(L_21, /*hidden argument*/NULL);
		if (L_22)
		{
			goto IL_009c;
		}
	}
	{
		HtmlNode_t2048434459 * L_23 = V_4;
		V_2 = L_23;
		goto IL_00b1;
	}

IL_009c:
	{
		Stack_1_t3136162613 * L_24 = V_3;
		HtmlNode_t2048434459 * L_25 = V_4;
		NullCheck(L_24);
		Stack_1_Push_m4199651461(L_24, L_25, /*hidden argument*/Stack_1_Push_m4199651461_RuntimeMethod_var);
		HtmlNode_t2048434459 * L_26 = V_4;
		NullCheck(L_26);
		HtmlNode_t2048434459 * L_27 = HtmlNode_get_PreviousSibling_m3912767171(L_26, /*hidden argument*/NULL);
		V_4 = L_27;
	}

IL_00ad:
	{
		HtmlNode_t2048434459 * L_28 = V_4;
		if (L_28)
		{
			goto IL_0075;
		}
	}

IL_00b1:
	{
		HtmlNode_t2048434459 * L_29 = V_2;
		if (!L_29)
		{
			goto IL_00e2;
		}
	}
	{
		goto IL_00d5;
	}

IL_00b6:
	{
		Stack_1_t3136162613 * L_30 = V_3;
		NullCheck(L_30);
		HtmlNode_t2048434459 * L_31 = Stack_1_Pop_m1876234527(L_30, /*hidden argument*/Stack_1_Pop_m1876234527_RuntimeMethod_var);
		V_5 = L_31;
		HtmlNode_t2048434459 * L_32 = __this->get__lastparentnode_9();
		HtmlNode_t2048434459 * L_33 = V_5;
		NullCheck(L_32);
		HtmlNode_RemoveChild_m1945208939(L_32, L_33, /*hidden argument*/NULL);
		HtmlNode_t2048434459 * L_34 = V_2;
		HtmlNode_t2048434459 * L_35 = V_5;
		NullCheck(L_34);
		HtmlNode_AppendChild_m2583227907(L_34, L_35, /*hidden argument*/NULL);
	}

IL_00d5:
	{
		Stack_1_t3136162613 * L_36 = V_3;
		NullCheck(L_36);
		int32_t L_37 = Stack_1_get_Count_m133251284(L_36, /*hidden argument*/Stack_1_get_Count_m133251284_RuntimeMethod_var);
		if (L_37)
		{
			goto IL_00b6;
		}
	}
	{
		goto IL_02c0;
	}

IL_00e2:
	{
		HtmlNode_t2048434459 * L_38 = __this->get__lastparentnode_9();
		HtmlNode_t2048434459 * L_39 = __this->get__currentnode_3();
		NullCheck(L_38);
		HtmlNode_AppendChild_m2583227907(L_38, L_39, /*hidden argument*/NULL);
		goto IL_02c0;
	}

IL_00f9:
	{
		HtmlNode_t2048434459 * L_40 = __this->get__currentnode_3();
		NullCheck(L_40);
		String_t* L_41 = HtmlNode_get_Name_m702134575(L_40, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(HtmlNode_t2048434459_il2cpp_TypeInfo_var);
		bool L_42 = HtmlNode_CanOverlapElement_m1178344375(NULL /*static, unused*/, L_41, /*hidden argument*/NULL);
		if (!L_42)
		{
			goto IL_016c;
		}
	}
	{
		HtmlNode_t2048434459 * L_43 = __this->get__currentnode_3();
		NullCheck(L_43);
		int32_t L_44 = L_43->get__outerstartindex_17();
		HtmlNode_t2048434459 * L_45 = HtmlDocument_CreateNode_m538784785(__this, 3, L_44, /*hidden argument*/NULL);
		V_6 = L_45;
		HtmlNode_t2048434459 * L_46 = V_6;
		HtmlNode_t2048434459 * L_47 = __this->get__currentnode_3();
		NullCheck(L_47);
		int32_t L_48 = L_47->get__outerlength_16();
		NullCheck(L_46);
		L_46->set__outerlength_16(L_48);
		HtmlNode_t2048434459 * L_49 = V_6;
		HtmlNode_t2048434459 * L_50 = V_6;
		NullCheck(((HtmlTextNode_t2710098554 *)CastclassClass((RuntimeObject*)L_50, HtmlTextNode_t2710098554_il2cpp_TypeInfo_var)));
		String_t* L_51 = HtmlTextNode_get_Text_m1199112604(((HtmlTextNode_t2710098554 *)CastclassClass((RuntimeObject*)L_50, HtmlTextNode_t2710098554_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		NullCheck(L_51);
		String_t* L_52 = String_ToLower_m2994460523(L_51, /*hidden argument*/NULL);
		NullCheck(((HtmlTextNode_t2710098554 *)CastclassClass((RuntimeObject*)L_49, HtmlTextNode_t2710098554_il2cpp_TypeInfo_var)));
		HtmlTextNode_set_Text_m279687427(((HtmlTextNode_t2710098554 *)CastclassClass((RuntimeObject*)L_49, HtmlTextNode_t2710098554_il2cpp_TypeInfo_var)), L_52, /*hidden argument*/NULL);
		HtmlNode_t2048434459 * L_53 = __this->get__lastparentnode_9();
		if (!L_53)
		{
			goto IL_02c0;
		}
	}
	{
		HtmlNode_t2048434459 * L_54 = __this->get__lastparentnode_9();
		HtmlNode_t2048434459 * L_55 = V_6;
		NullCheck(L_54);
		HtmlNode_AppendChild_m2583227907(L_54, L_55, /*hidden argument*/NULL);
		goto IL_02c0;
	}

IL_016c:
	{
		HtmlNode_t2048434459 * L_56 = __this->get__currentnode_3();
		NullCheck(L_56);
		String_t* L_57 = HtmlNode_get_Name_m702134575(L_56, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(HtmlNode_t2048434459_il2cpp_TypeInfo_var);
		bool L_58 = HtmlNode_IsEmptyElement_m2918938855(NULL /*static, unused*/, L_57, /*hidden argument*/NULL);
		if (!L_58)
		{
			goto IL_01d0;
		}
	}
	{
		HtmlNode_t2048434459 * L_59 = __this->get__currentnode_3();
		NullCheck(L_59);
		int32_t L_60 = L_59->get__line_7();
		HtmlNode_t2048434459 * L_61 = __this->get__currentnode_3();
		NullCheck(L_61);
		int32_t L_62 = L_61->get__lineposition_8();
		HtmlNode_t2048434459 * L_63 = __this->get__currentnode_3();
		NullCheck(L_63);
		int32_t L_64 = L_63->get__streamposition_24();
		HtmlNode_t2048434459 * L_65 = __this->get__currentnode_3();
		NullCheck(L_65);
		String_t* L_66 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String HtmlAgilityPack.HtmlNode::get_OuterHtml() */, L_65);
		HtmlNode_t2048434459 * L_67 = __this->get__currentnode_3();
		NullCheck(L_67);
		String_t* L_68 = HtmlNode_get_Name_m702134575(L_67, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_69 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral2008880196, L_68, _stringLiteral620822474, /*hidden argument*/NULL);
		HtmlDocument_AddError_m2023821899(__this, 3, L_60, L_62, L_64, L_66, L_69, /*hidden argument*/NULL);
		goto IL_02c0;
	}

IL_01d0:
	{
		HtmlNode_t2048434459 * L_70 = __this->get__currentnode_3();
		NullCheck(L_70);
		int32_t L_71 = L_70->get__line_7();
		HtmlNode_t2048434459 * L_72 = __this->get__currentnode_3();
		NullCheck(L_72);
		int32_t L_73 = L_72->get__lineposition_8();
		HtmlNode_t2048434459 * L_74 = __this->get__currentnode_3();
		NullCheck(L_74);
		int32_t L_75 = L_74->get__streamposition_24();
		HtmlNode_t2048434459 * L_76 = __this->get__currentnode_3();
		NullCheck(L_76);
		String_t* L_77 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String HtmlAgilityPack.HtmlNode::get_OuterHtml() */, L_76);
		HtmlNode_t2048434459 * L_78 = __this->get__currentnode_3();
		NullCheck(L_78);
		String_t* L_79 = HtmlNode_get_Name_m702134575(L_78, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_80 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral3776025430, L_79, _stringLiteral929783012, /*hidden argument*/NULL);
		HtmlDocument_AddError_m2023821899(__this, 1, L_71, L_73, L_75, L_77, L_80, /*hidden argument*/NULL);
		V_0 = (bool)1;
		goto IL_02c0;
	}

IL_0224:
	{
		bool L_81 = __this->get_OptionFixNestedTags_30();
		if (!L_81)
		{
			goto IL_0295;
		}
	}
	{
		HtmlNode_t2048434459 * L_82 = V_1;
		HtmlNode_t2048434459 * L_83 = __this->get__currentnode_3();
		NullCheck(L_83);
		String_t* L_84 = HtmlNode_get_Name_m702134575(L_83, /*hidden argument*/NULL);
		StringU5BU5D_t1642385972* L_85 = HtmlDocument_GetResetters_m3862259821(__this, L_84, /*hidden argument*/NULL);
		bool L_86 = HtmlDocument_FindResetterNodes_m2911361875(__this, L_82, L_85, /*hidden argument*/NULL);
		if (!L_86)
		{
			goto IL_0295;
		}
	}
	{
		HtmlNode_t2048434459 * L_87 = __this->get__currentnode_3();
		NullCheck(L_87);
		int32_t L_88 = L_87->get__line_7();
		HtmlNode_t2048434459 * L_89 = __this->get__currentnode_3();
		NullCheck(L_89);
		int32_t L_90 = L_89->get__lineposition_8();
		HtmlNode_t2048434459 * L_91 = __this->get__currentnode_3();
		NullCheck(L_91);
		int32_t L_92 = L_91->get__streamposition_24();
		HtmlNode_t2048434459 * L_93 = __this->get__currentnode_3();
		NullCheck(L_93);
		String_t* L_94 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String HtmlAgilityPack.HtmlNode::get_OuterHtml() */, L_93);
		HtmlNode_t2048434459 * L_95 = __this->get__currentnode_3();
		NullCheck(L_95);
		String_t* L_96 = HtmlNode_get_Name_m702134575(L_95, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_97 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral2008880196, L_96, _stringLiteral2209855717, /*hidden argument*/NULL);
		HtmlDocument_AddError_m2023821899(__this, 4, L_88, L_90, L_92, L_94, L_97, /*hidden argument*/NULL);
		V_0 = (bool)1;
	}

IL_0295:
	{
		bool L_98 = V_0;
		if (L_98)
		{
			goto IL_02c0;
		}
	}
	{
		Dictionary_2_t3963213721 * L_99 = __this->get_Lastnodes_8();
		HtmlNode_t2048434459 * L_100 = __this->get__currentnode_3();
		NullCheck(L_100);
		String_t* L_101 = HtmlNode_get_Name_m702134575(L_100, /*hidden argument*/NULL);
		HtmlNode_t2048434459 * L_102 = V_1;
		NullCheck(L_102);
		HtmlNode_t2048434459 * L_103 = L_102->get__prevwithsamename_22();
		NullCheck(L_99);
		Dictionary_2_set_Item_m3521183286(L_99, L_101, L_103, /*hidden argument*/Dictionary_2_set_Item_m3521183286_RuntimeMethod_var);
		HtmlNode_t2048434459 * L_104 = V_1;
		HtmlNode_t2048434459 * L_105 = __this->get__currentnode_3();
		NullCheck(L_104);
		HtmlNode_CloseNode_m1160950766(L_104, L_105, /*hidden argument*/NULL);
	}

IL_02c0:
	{
		bool L_106 = V_0;
		if (L_106)
		{
			goto IL_02f0;
		}
	}
	{
		HtmlNode_t2048434459 * L_107 = __this->get__lastparentnode_9();
		if (!L_107)
		{
			goto IL_02f0;
		}
	}
	{
		HtmlNode_t2048434459 * L_108 = __this->get__currentnode_3();
		NullCheck(L_108);
		String_t* L_109 = HtmlNode_get_Name_m702134575(L_108, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(HtmlNode_t2048434459_il2cpp_TypeInfo_var);
		bool L_110 = HtmlNode_IsClosedElement_m2492183536(NULL /*static, unused*/, L_109, /*hidden argument*/NULL);
		if (!L_110)
		{
			goto IL_02ea;
		}
	}
	{
		HtmlNode_t2048434459 * L_111 = __this->get__currentnode_3();
		NullCheck(L_111);
		bool L_112 = L_111->get__starttag_23();
		if (!L_112)
		{
			goto IL_02f0;
		}
	}

IL_02ea:
	{
		HtmlDocument_UpdateLastParentNode_m3368866332(__this, /*hidden argument*/NULL);
	}

IL_02f0:
	{
		return;
	}
}
// System.String HtmlAgilityPack.HtmlDocument::CurrentNodeName()
extern "C"  String_t* HtmlDocument_CurrentNodeName_m112987032 (HtmlDocument_t556432108 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_Text_22();
		HtmlNode_t2048434459 * L_1 = __this->get__currentnode_3();
		NullCheck(L_1);
		int32_t L_2 = L_1->get__namestartindex_11();
		HtmlNode_t2048434459 * L_3 = __this->get__currentnode_3();
		NullCheck(L_3);
		int32_t L_4 = L_3->get__namelength_10();
		NullCheck(L_0);
		String_t* L_5 = String_Substring_m12482732(L_0, L_2, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void HtmlAgilityPack.HtmlDocument::DecrementPosition()
extern "C"  void HtmlDocument_DecrementPosition_m1311188097 (HtmlDocument_t556432108 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get__index_7();
		__this->set__index_7(((int32_t)((int32_t)L_0-(int32_t)1)));
		int32_t L_1 = __this->get__lineposition_11();
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_0032;
		}
	}
	{
		int32_t L_2 = __this->get__maxlineposition_12();
		__this->set__lineposition_11(L_2);
		int32_t L_3 = __this->get__line_10();
		__this->set__line_10(((int32_t)((int32_t)L_3-(int32_t)1)));
		return;
	}

IL_0032:
	{
		int32_t L_4 = __this->get__lineposition_11();
		__this->set__lineposition_11(((int32_t)((int32_t)L_4-(int32_t)1)));
		return;
	}
}
// HtmlAgilityPack.HtmlNode HtmlAgilityPack.HtmlDocument::FindResetterNode(HtmlAgilityPack.HtmlNode,System.String)
extern "C"  HtmlNode_t2048434459 * HtmlDocument_FindResetterNode_m2182858297 (HtmlDocument_t556432108 * __this, HtmlNode_t2048434459 * ___node0, String_t* ___name1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlDocument_FindResetterNode_m2182858297_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	HtmlNode_t2048434459 * V_0 = NULL;
	{
		Dictionary_2_t3963213721 * L_0 = __this->get_Lastnodes_8();
		String_t* L_1 = ___name1;
		HtmlNode_t2048434459 * L_2 = Utilities_GetDictionaryValueOrNull_TisString_t_TisHtmlNode_t2048434459_m2137127104(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/Utilities_GetDictionaryValueOrNull_TisString_t_TisHtmlNode_t2048434459_m2137127104_RuntimeMethod_var);
		V_0 = L_2;
		HtmlNode_t2048434459 * L_3 = V_0;
		if (L_3)
		{
			goto IL_0012;
		}
	}
	{
		return (HtmlNode_t2048434459 *)NULL;
	}

IL_0012:
	{
		HtmlNode_t2048434459 * L_4 = V_0;
		NullCheck(L_4);
		bool L_5 = HtmlNode_get_Closed_m650109995(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_001c;
		}
	}
	{
		return (HtmlNode_t2048434459 *)NULL;
	}

IL_001c:
	{
		HtmlNode_t2048434459 * L_6 = V_0;
		NullCheck(L_6);
		int32_t L_7 = L_6->get__streamposition_24();
		HtmlNode_t2048434459 * L_8 = ___node0;
		NullCheck(L_8);
		int32_t L_9 = L_8->get__streamposition_24();
		if ((((int32_t)L_7) >= ((int32_t)L_9)))
		{
			goto IL_002c;
		}
	}
	{
		return (HtmlNode_t2048434459 *)NULL;
	}

IL_002c:
	{
		HtmlNode_t2048434459 * L_10 = V_0;
		return L_10;
	}
}
// System.Boolean HtmlAgilityPack.HtmlDocument::FindResetterNodes(HtmlAgilityPack.HtmlNode,System.String[])
extern "C"  bool HtmlDocument_FindResetterNodes_m2911361875 (HtmlDocument_t556432108 * __this, HtmlNode_t2048434459 * ___node0, StringU5BU5D_t1642385972* ___names1, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		StringU5BU5D_t1642385972* L_0 = ___names1;
		if (L_0)
		{
			goto IL_0005;
		}
	}
	{
		return (bool)0;
	}

IL_0005:
	{
		V_0 = 0;
		goto IL_001b;
	}

IL_0009:
	{
		HtmlNode_t2048434459 * L_1 = ___node0;
		StringU5BU5D_t1642385972* L_2 = ___names1;
		int32_t L_3 = V_0;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		String_t* L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		HtmlNode_t2048434459 * L_6 = HtmlDocument_FindResetterNode_m2182858297(__this, L_1, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0017;
		}
	}
	{
		return (bool)1;
	}

IL_0017:
	{
		int32_t L_7 = V_0;
		V_0 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_001b:
	{
		int32_t L_8 = V_0;
		StringU5BU5D_t1642385972* L_9 = ___names1;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_9)->max_length)))))))
		{
			goto IL_0009;
		}
	}
	{
		return (bool)0;
	}
}
// System.Void HtmlAgilityPack.HtmlDocument::FixNestedTag(System.String,System.String[])
extern "C"  void HtmlDocument_FixNestedTag_m2725238597 (HtmlDocument_t556432108 * __this, String_t* ___name0, StringU5BU5D_t1642385972* ___resetters1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlDocument_FixNestedTag_m2725238597_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	HtmlNode_t2048434459 * V_0 = NULL;
	HtmlNode_t2048434459 * V_1 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = ___resetters1;
		if (L_0)
		{
			goto IL_0004;
		}
	}
	{
		return;
	}

IL_0004:
	{
		Dictionary_2_t3963213721 * L_1 = __this->get_Lastnodes_8();
		HtmlNode_t2048434459 * L_2 = __this->get__currentnode_3();
		NullCheck(L_2);
		String_t* L_3 = HtmlNode_get_Name_m702134575(L_2, /*hidden argument*/NULL);
		HtmlNode_t2048434459 * L_4 = Utilities_GetDictionaryValueOrNull_TisString_t_TisHtmlNode_t2048434459_m2137127104(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/Utilities_GetDictionaryValueOrNull_TisString_t_TisHtmlNode_t2048434459_m2137127104_RuntimeMethod_var);
		V_0 = L_4;
		HtmlNode_t2048434459 * L_5 = V_0;
		if (!L_5)
		{
			goto IL_0031;
		}
	}
	{
		Dictionary_2_t3963213721 * L_6 = __this->get_Lastnodes_8();
		String_t* L_7 = ___name0;
		NullCheck(L_6);
		HtmlNode_t2048434459 * L_8 = Dictionary_2_get_Item_m3051461655(L_6, L_7, /*hidden argument*/Dictionary_2_get_Item_m3051461655_RuntimeMethod_var);
		NullCheck(L_8);
		bool L_9 = HtmlNode_get_Closed_m650109995(L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0032;
		}
	}

IL_0031:
	{
		return;
	}

IL_0032:
	{
		HtmlNode_t2048434459 * L_10 = V_0;
		StringU5BU5D_t1642385972* L_11 = ___resetters1;
		bool L_12 = HtmlDocument_FindResetterNodes_m2911361875(__this, L_10, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_003d;
		}
	}
	{
		return;
	}

IL_003d:
	{
		HtmlNode_t2048434459 * L_13 = V_0;
		NullCheck(L_13);
		int32_t L_14 = HtmlNode_get_NodeType_m270476988(L_13, /*hidden argument*/NULL);
		HtmlNode_t2048434459 * L_15 = (HtmlNode_t2048434459 *)il2cpp_codegen_object_new(HtmlNode_t2048434459_il2cpp_TypeInfo_var);
		HtmlNode__ctor_m2584575018(L_15, L_14, __this, (-1), /*hidden argument*/NULL);
		V_1 = L_15;
		HtmlNode_t2048434459 * L_16 = V_1;
		HtmlNode_t2048434459 * L_17 = V_1;
		NullCheck(L_16);
		L_16->set__endnode_2(L_17);
		HtmlNode_t2048434459 * L_18 = V_0;
		HtmlNode_t2048434459 * L_19 = V_1;
		NullCheck(L_18);
		HtmlNode_CloseNode_m1160950766(L_18, L_19, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HtmlAgilityPack.HtmlDocument::FixNestedTags()
extern "C"  void HtmlDocument_FixNestedTags_m2961550712 (HtmlDocument_t556432108 * __this, const RuntimeMethod* method)
{
	String_t* V_0 = NULL;
	{
		HtmlNode_t2048434459 * L_0 = __this->get__currentnode_3();
		NullCheck(L_0);
		bool L_1 = L_0->get__starttag_23();
		if (L_1)
		{
			goto IL_000e;
		}
	}
	{
		return;
	}

IL_000e:
	{
		String_t* L_2 = HtmlDocument_CurrentNodeName_m112987032(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		String_t* L_3 = V_0;
		String_t* L_4 = V_0;
		StringU5BU5D_t1642385972* L_5 = HtmlDocument_GetResetters_m3862259821(__this, L_4, /*hidden argument*/NULL);
		HtmlDocument_FixNestedTag_m2725238597(__this, L_3, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.String[] HtmlAgilityPack.HtmlDocument::GetResetters(System.String)
extern "C"  StringU5BU5D_t1642385972* HtmlDocument_GetResetters_m3862259821 (HtmlDocument_t556432108 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlDocument_GetResetters_m3862259821_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	StringU5BU5D_t1642385972* V_1 = NULL;
	StringU5BU5D_t1642385972* V_2 = NULL;
	StringU5BU5D_t1642385972* V_3 = NULL;
	{
		String_t* L_0 = ___name0;
		String_t* L_1 = L_0;
		V_0 = L_1;
		if (!L_1)
		{
			goto IL_0076;
		}
	}
	{
		String_t* L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_2, _stringLiteral334115653, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_003b;
		}
	}
	{
		String_t* L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_4, _stringLiteral381169828, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_004c;
		}
	}
	{
		String_t* L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_7 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_6, _stringLiteral1900199602, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_005d;
		}
	}
	{
		String_t* L_8 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_9 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_8, _stringLiteral287061494, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_005d;
		}
	}
	{
		goto IL_0076;
	}

IL_003b:
	{
		V_1 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)1));
		StringU5BU5D_t1642385972* L_10 = V_1;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral4225798429);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral4225798429);
		StringU5BU5D_t1642385972* L_11 = V_1;
		return L_11;
	}

IL_004c:
	{
		V_2 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)1));
		StringU5BU5D_t1642385972* L_12 = V_2;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral593465182);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral593465182);
		StringU5BU5D_t1642385972* L_13 = V_2;
		return L_13;
	}

IL_005d:
	{
		V_3 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)2));
		StringU5BU5D_t1642385972* L_14 = V_3;
		NullCheck(L_14);
		ArrayElementTypeCheck (L_14, _stringLiteral381169828);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral381169828);
		StringU5BU5D_t1642385972* L_15 = V_3;
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, _stringLiteral593465182);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral593465182);
		StringU5BU5D_t1642385972* L_16 = V_3;
		return L_16;
	}

IL_0076:
	{
		return (StringU5BU5D_t1642385972*)NULL;
	}
}
// System.Void HtmlAgilityPack.HtmlDocument::IncrementPosition()
extern "C"  void HtmlDocument_IncrementPosition_m421960747 (HtmlDocument_t556432108 * __this, const RuntimeMethod* method)
{
	{
		Crc32_t83196531 * L_0 = __this->get__crc32_1();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		Crc32_t83196531 * L_1 = __this->get__crc32_1();
		int32_t L_2 = __this->get__c_0();
		NullCheck(L_1);
		Crc32_AddToCRC32_m2370959785(L_1, L_2, /*hidden argument*/NULL);
	}

IL_001a:
	{
		int32_t L_3 = __this->get__index_7();
		__this->set__index_7(((int32_t)((int32_t)L_3+(int32_t)1)));
		int32_t L_4 = __this->get__lineposition_11();
		__this->set__maxlineposition_12(L_4);
		int32_t L_5 = __this->get__c_0();
		if ((!(((uint32_t)L_5) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_0054;
		}
	}
	{
		__this->set__lineposition_11(1);
		int32_t L_6 = __this->get__line_10();
		__this->set__line_10(((int32_t)((int32_t)L_6+(int32_t)1)));
		return;
	}

IL_0054:
	{
		int32_t L_7 = __this->get__lineposition_11();
		__this->set__lineposition_11(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Boolean HtmlAgilityPack.HtmlDocument::NewCheck()
extern "C"  bool HtmlDocument_NewCheck_m3661304845 (HtmlDocument_t556432108 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get__c_0();
		if ((((int32_t)L_0) == ((int32_t)((int32_t)60))))
		{
			goto IL_000c;
		}
	}
	{
		return (bool)0;
	}

IL_000c:
	{
		int32_t L_1 = __this->get__index_7();
		String_t* L_2 = __this->get_Text_22();
		NullCheck(L_2);
		int32_t L_3 = String_get_Length_m1606060069(L_2, /*hidden argument*/NULL);
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_00a2;
		}
	}
	{
		String_t* L_4 = __this->get_Text_22();
		int32_t L_5 = __this->get__index_7();
		NullCheck(L_4);
		Il2CppChar L_6 = String_get_Chars_m4230566705(L_4, L_5, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_6) == ((uint32_t)((int32_t)37)))))
		{
			goto IL_00a2;
		}
	}
	{
		int32_t L_7 = __this->get__state_20();
		V_0 = L_7;
		int32_t L_8 = V_0;
		switch (((int32_t)((int32_t)L_8-(int32_t)1)))
		{
			case 0:
			{
				goto IL_0076;
			}
			case 1:
			{
				goto IL_008c;
			}
			case 2:
			{
				goto IL_0066;
			}
		}
	}
	{
		int32_t L_9 = V_0;
		if ((!(((uint32_t)L_9) == ((uint32_t)7))))
		{
			goto IL_008c;
		}
	}
	{
		int32_t L_10 = __this->get__index_7();
		HtmlDocument_PushAttributeValueStart_m2091657843(__this, ((int32_t)((int32_t)L_10-(int32_t)1)), /*hidden argument*/NULL);
		goto IL_008c;
	}

IL_0066:
	{
		int32_t L_11 = __this->get__index_7();
		HtmlDocument_PushAttributeNameStart_m687133679(__this, ((int32_t)((int32_t)L_11-(int32_t)1)), /*hidden argument*/NULL);
		goto IL_008c;
	}

IL_0076:
	{
		int32_t L_12 = __this->get__index_7();
		HtmlDocument_PushNodeNameStart_m2396823204(__this, (bool)1, ((int32_t)((int32_t)L_12-(int32_t)1)), /*hidden argument*/NULL);
		__this->set__state_20(2);
	}

IL_008c:
	{
		int32_t L_13 = __this->get__state_20();
		__this->set__oldstate_14(L_13);
		__this->set__state_20(((int32_t)11));
		return (bool)1;
	}

IL_00a2:
	{
		int32_t L_14 = __this->get__index_7();
		bool L_15 = HtmlDocument_PushNodeEnd_m1567272216(__this, ((int32_t)((int32_t)L_14-(int32_t)1)), (bool)1, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_00c6;
		}
	}
	{
		String_t* L_16 = __this->get_Text_22();
		NullCheck(L_16);
		int32_t L_17 = String_get_Length_m1606060069(L_16, /*hidden argument*/NULL);
		__this->set__index_7(L_17);
		return (bool)1;
	}

IL_00c6:
	{
		__this->set__state_20(1);
		int32_t L_18 = __this->get__index_7();
		String_t* L_19 = __this->get_Text_22();
		NullCheck(L_19);
		int32_t L_20 = String_get_Length_m1606060069(L_19, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_18-(int32_t)1))) > ((int32_t)((int32_t)((int32_t)L_20-(int32_t)2)))))
		{
			goto IL_0186;
		}
	}
	{
		String_t* L_21 = __this->get_Text_22();
		int32_t L_22 = __this->get__index_7();
		NullCheck(L_21);
		Il2CppChar L_23 = String_get_Chars_m4230566705(L_21, L_22, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_23) == ((uint32_t)((int32_t)33)))))
		{
			goto IL_0186;
		}
	}
	{
		int32_t L_24 = __this->get__index_7();
		HtmlDocument_PushNodeStart_m1529103168(__this, 2, ((int32_t)((int32_t)L_24-(int32_t)1)), /*hidden argument*/NULL);
		int32_t L_25 = __this->get__index_7();
		HtmlDocument_PushNodeNameStart_m2396823204(__this, (bool)1, L_25, /*hidden argument*/NULL);
		int32_t L_26 = __this->get__index_7();
		HtmlDocument_PushNodeNameEnd_m3862249348(__this, ((int32_t)((int32_t)L_26+(int32_t)1)), /*hidden argument*/NULL);
		__this->set__state_20(((int32_t)9));
		int32_t L_27 = __this->get__index_7();
		String_t* L_28 = __this->get_Text_22();
		NullCheck(L_28);
		int32_t L_29 = String_get_Length_m1606060069(L_28, /*hidden argument*/NULL);
		if ((((int32_t)L_27) >= ((int32_t)((int32_t)((int32_t)L_29-(int32_t)2)))))
		{
			goto IL_0184;
		}
	}
	{
		String_t* L_30 = __this->get_Text_22();
		int32_t L_31 = __this->get__index_7();
		NullCheck(L_30);
		Il2CppChar L_32 = String_get_Chars_m4230566705(L_30, ((int32_t)((int32_t)L_31+(int32_t)1)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_32) == ((uint32_t)((int32_t)45)))))
		{
			goto IL_017d;
		}
	}
	{
		String_t* L_33 = __this->get_Text_22();
		int32_t L_34 = __this->get__index_7();
		NullCheck(L_33);
		Il2CppChar L_35 = String_get_Chars_m4230566705(L_33, ((int32_t)((int32_t)L_34+(int32_t)2)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_35) == ((uint32_t)((int32_t)45)))))
		{
			goto IL_017d;
		}
	}
	{
		__this->set__fullcomment_6((bool)1);
		goto IL_0184;
	}

IL_017d:
	{
		__this->set__fullcomment_6((bool)0);
	}

IL_0184:
	{
		return (bool)1;
	}

IL_0186:
	{
		int32_t L_36 = __this->get__index_7();
		HtmlDocument_PushNodeStart_m1529103168(__this, 1, ((int32_t)((int32_t)L_36-(int32_t)1)), /*hidden argument*/NULL);
		return (bool)1;
	}
}
// System.Void HtmlAgilityPack.HtmlDocument::Parse()
extern "C"  void HtmlDocument_Parse_m3513675482 (HtmlDocument_t556432108 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlDocument_Parse_m3513675482_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	HtmlNode_t2048434459 * V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		V_0 = 0;
		bool L_0 = __this->get_OptionComputeChecksum_26();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		Crc32_t83196531 * L_1 = (Crc32_t83196531 *)il2cpp_codegen_object_new(Crc32_t83196531_il2cpp_TypeInfo_var);
		Crc32__ctor_m1474328800(L_1, /*hidden argument*/NULL);
		__this->set__crc32_1(L_1);
	}

IL_0015:
	{
		Dictionary_2_t3963213721 * L_2 = (Dictionary_2_t3963213721 *)il2cpp_codegen_object_new(Dictionary_2_t3963213721_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m850897185(L_2, /*hidden argument*/Dictionary_2__ctor_m850897185_RuntimeMethod_var);
		__this->set_Lastnodes_8(L_2);
		__this->set__c_0(0);
		__this->set__fullcomment_6((bool)0);
		List_1_t484300294 * L_3 = (List_1_t484300294 *)il2cpp_codegen_object_new(List_1_t484300294_il2cpp_TypeInfo_var);
		List_1__ctor_m3187661903(L_3, /*hidden argument*/List_1__ctor_m3187661903_RuntimeMethod_var);
		__this->set__parseerrors_17(L_3);
		__this->set__line_10(1);
		__this->set__lineposition_11(1);
		__this->set__maxlineposition_12(1);
		__this->set__state_20(0);
		int32_t L_4 = __this->get__state_20();
		__this->set__oldstate_14(L_4);
		HtmlNode_t2048434459 * L_5 = __this->get__documentnode_5();
		String_t* L_6 = __this->get_Text_22();
		NullCheck(L_6);
		int32_t L_7 = String_get_Length_m1606060069(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		L_5->set__innerlength_5(L_7);
		HtmlNode_t2048434459 * L_8 = __this->get__documentnode_5();
		String_t* L_9 = __this->get_Text_22();
		NullCheck(L_9);
		int32_t L_10 = String_get_Length_m1606060069(L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		L_8->set__outerlength_16(L_10);
		String_t* L_11 = __this->get_Text_22();
		NullCheck(L_11);
		int32_t L_12 = String_get_Length_m1606060069(L_11, /*hidden argument*/NULL);
		__this->set__remainderOffset_19(L_12);
		HtmlNode_t2048434459 * L_13 = __this->get__documentnode_5();
		__this->set__lastparentnode_9(L_13);
		HtmlNode_t2048434459 * L_14 = HtmlDocument_CreateNode_m538784785(__this, 3, 0, /*hidden argument*/NULL);
		__this->set__currentnode_3(L_14);
		__this->set__currentattribute_2((HtmlAttribute_t1804523403 *)NULL);
		__this->set__index_7(0);
		HtmlDocument_PushNodeStart_m1529103168(__this, 3, 0, /*hidden argument*/NULL);
		goto IL_08b4;
	}

IL_00d3:
	{
		String_t* L_15 = __this->get_Text_22();
		int32_t L_16 = __this->get__index_7();
		NullCheck(L_15);
		Il2CppChar L_17 = String_get_Chars_m4230566705(L_15, L_16, /*hidden argument*/NULL);
		__this->set__c_0(L_17);
		HtmlDocument_IncrementPosition_m421960747(__this, /*hidden argument*/NULL);
		int32_t L_18 = __this->get__state_20();
		V_3 = L_18;
		int32_t L_19 = V_3;
		switch (L_19)
		{
			case 0:
			{
				goto IL_0136;
			}
			case 1:
			{
				goto IL_0146;
			}
			case 2:
			{
				goto IL_018b;
			}
			case 3:
			{
				goto IL_026a;
			}
			case 4:
			{
				goto IL_0313;
			}
			case 5:
			{
				goto IL_037e;
			}
			case 6:
			{
				goto IL_0439;
			}
			case 7:
			{
				goto IL_04d0;
			}
			case 8:
			{
				goto IL_0593;
			}
			case 9:
			{
				goto IL_06a1;
			}
			case 10:
			{
				goto IL_062a;
			}
			case 11:
			{
				goto IL_0728;
			}
			case 12:
			{
				goto IL_07ac;
			}
		}
	}
	{
		goto IL_08b4;
	}

IL_0136:
	{
		bool L_20 = HtmlDocument_NewCheck_m3661304845(__this, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_08b4;
		}
	}
	{
		goto IL_08b4;
	}

IL_0146:
	{
		bool L_21 = HtmlDocument_NewCheck_m3661304845(__this, /*hidden argument*/NULL);
		if (L_21)
		{
			goto IL_08b4;
		}
	}
	{
		int32_t L_22 = __this->get__c_0();
		if ((!(((uint32_t)L_22) == ((uint32_t)((int32_t)47)))))
		{
			goto IL_016a;
		}
	}
	{
		int32_t L_23 = __this->get__index_7();
		HtmlDocument_PushNodeNameStart_m2396823204(__this, (bool)0, L_23, /*hidden argument*/NULL);
		goto IL_017f;
	}

IL_016a:
	{
		int32_t L_24 = __this->get__index_7();
		HtmlDocument_PushNodeNameStart_m2396823204(__this, (bool)1, ((int32_t)((int32_t)L_24-(int32_t)1)), /*hidden argument*/NULL);
		HtmlDocument_DecrementPosition_m1311188097(__this, /*hidden argument*/NULL);
	}

IL_017f:
	{
		__this->set__state_20(2);
		goto IL_08b4;
	}

IL_018b:
	{
		bool L_25 = HtmlDocument_NewCheck_m3661304845(__this, /*hidden argument*/NULL);
		if (L_25)
		{
			goto IL_08b4;
		}
	}
	{
		int32_t L_26 = __this->get__c_0();
		IL2CPP_RUNTIME_CLASS_INIT(HtmlDocument_t556432108_il2cpp_TypeInfo_var);
		bool L_27 = HtmlDocument_IsWhiteSpace_m1502326855(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_01c9;
		}
	}
	{
		int32_t L_28 = __this->get__index_7();
		HtmlDocument_PushNodeNameEnd_m3862249348(__this, ((int32_t)((int32_t)L_28-(int32_t)1)), /*hidden argument*/NULL);
		int32_t L_29 = __this->get__state_20();
		if ((!(((uint32_t)L_29) == ((uint32_t)2))))
		{
			goto IL_08b4;
		}
	}
	{
		__this->set__state_20(3);
		goto IL_08b4;
	}

IL_01c9:
	{
		int32_t L_30 = __this->get__c_0();
		if ((!(((uint32_t)L_30) == ((uint32_t)((int32_t)47)))))
		{
			goto IL_01f9;
		}
	}
	{
		int32_t L_31 = __this->get__index_7();
		HtmlDocument_PushNodeNameEnd_m3862249348(__this, ((int32_t)((int32_t)L_31-(int32_t)1)), /*hidden argument*/NULL);
		int32_t L_32 = __this->get__state_20();
		if ((!(((uint32_t)L_32) == ((uint32_t)2))))
		{
			goto IL_08b4;
		}
	}
	{
		__this->set__state_20(4);
		goto IL_08b4;
	}

IL_01f9:
	{
		int32_t L_33 = __this->get__c_0();
		if ((!(((uint32_t)L_33) == ((uint32_t)((int32_t)62)))))
		{
			goto IL_08b4;
		}
	}
	{
		int32_t L_34 = __this->get__index_7();
		HtmlDocument_PushNodeNameEnd_m3862249348(__this, ((int32_t)((int32_t)L_34-(int32_t)1)), /*hidden argument*/NULL);
		int32_t L_35 = __this->get__state_20();
		if ((!(((uint32_t)L_35) == ((uint32_t)2))))
		{
			goto IL_08b4;
		}
	}
	{
		int32_t L_36 = __this->get__index_7();
		bool L_37 = HtmlDocument_PushNodeEnd_m1567272216(__this, L_36, (bool)0, /*hidden argument*/NULL);
		if (L_37)
		{
			goto IL_0245;
		}
	}
	{
		String_t* L_38 = __this->get_Text_22();
		NullCheck(L_38);
		int32_t L_39 = String_get_Length_m1606060069(L_38, /*hidden argument*/NULL);
		__this->set__index_7(L_39);
		goto IL_08b4;
	}

IL_0245:
	{
		int32_t L_40 = __this->get__state_20();
		if ((!(((uint32_t)L_40) == ((uint32_t)2))))
		{
			goto IL_08b4;
		}
	}
	{
		__this->set__state_20(0);
		int32_t L_41 = __this->get__index_7();
		HtmlDocument_PushNodeStart_m1529103168(__this, 3, L_41, /*hidden argument*/NULL);
		goto IL_08b4;
	}

IL_026a:
	{
		bool L_42 = HtmlDocument_NewCheck_m3661304845(__this, /*hidden argument*/NULL);
		if (L_42)
		{
			goto IL_08b4;
		}
	}
	{
		int32_t L_43 = __this->get__c_0();
		IL2CPP_RUNTIME_CLASS_INIT(HtmlDocument_t556432108_il2cpp_TypeInfo_var);
		bool L_44 = HtmlDocument_IsWhiteSpace_m1502326855(NULL /*static, unused*/, L_43, /*hidden argument*/NULL);
		if (L_44)
		{
			goto IL_08b4;
		}
	}
	{
		int32_t L_45 = __this->get__c_0();
		if ((((int32_t)L_45) == ((int32_t)((int32_t)47))))
		{
			goto IL_0299;
		}
	}
	{
		int32_t L_46 = __this->get__c_0();
		if ((!(((uint32_t)L_46) == ((uint32_t)((int32_t)63)))))
		{
			goto IL_02a5;
		}
	}

IL_0299:
	{
		__this->set__state_20(4);
		goto IL_08b4;
	}

IL_02a5:
	{
		int32_t L_47 = __this->get__c_0();
		if ((!(((uint32_t)L_47) == ((uint32_t)((int32_t)62)))))
		{
			goto IL_02f9;
		}
	}
	{
		int32_t L_48 = __this->get__index_7();
		bool L_49 = HtmlDocument_PushNodeEnd_m1567272216(__this, L_48, (bool)0, /*hidden argument*/NULL);
		if (L_49)
		{
			goto IL_02d4;
		}
	}
	{
		String_t* L_50 = __this->get_Text_22();
		NullCheck(L_50);
		int32_t L_51 = String_get_Length_m1606060069(L_50, /*hidden argument*/NULL);
		__this->set__index_7(L_51);
		goto IL_08b4;
	}

IL_02d4:
	{
		int32_t L_52 = __this->get__state_20();
		if ((!(((uint32_t)L_52) == ((uint32_t)3))))
		{
			goto IL_08b4;
		}
	}
	{
		__this->set__state_20(0);
		int32_t L_53 = __this->get__index_7();
		HtmlDocument_PushNodeStart_m1529103168(__this, 3, L_53, /*hidden argument*/NULL);
		goto IL_08b4;
	}

IL_02f9:
	{
		int32_t L_54 = __this->get__index_7();
		HtmlDocument_PushAttributeNameStart_m687133679(__this, ((int32_t)((int32_t)L_54-(int32_t)1)), /*hidden argument*/NULL);
		__this->set__state_20(5);
		goto IL_08b4;
	}

IL_0313:
	{
		bool L_55 = HtmlDocument_NewCheck_m3661304845(__this, /*hidden argument*/NULL);
		if (L_55)
		{
			goto IL_08b4;
		}
	}
	{
		int32_t L_56 = __this->get__c_0();
		if ((!(((uint32_t)L_56) == ((uint32_t)((int32_t)62)))))
		{
			goto IL_0372;
		}
	}
	{
		int32_t L_57 = __this->get__index_7();
		bool L_58 = HtmlDocument_PushNodeEnd_m1567272216(__this, L_57, (bool)1, /*hidden argument*/NULL);
		if (L_58)
		{
			goto IL_034d;
		}
	}
	{
		String_t* L_59 = __this->get_Text_22();
		NullCheck(L_59);
		int32_t L_60 = String_get_Length_m1606060069(L_59, /*hidden argument*/NULL);
		__this->set__index_7(L_60);
		goto IL_08b4;
	}

IL_034d:
	{
		int32_t L_61 = __this->get__state_20();
		if ((!(((uint32_t)L_61) == ((uint32_t)4))))
		{
			goto IL_08b4;
		}
	}
	{
		__this->set__state_20(0);
		int32_t L_62 = __this->get__index_7();
		HtmlDocument_PushNodeStart_m1529103168(__this, 3, L_62, /*hidden argument*/NULL);
		goto IL_08b4;
	}

IL_0372:
	{
		__this->set__state_20(3);
		goto IL_08b4;
	}

IL_037e:
	{
		bool L_63 = HtmlDocument_NewCheck_m3661304845(__this, /*hidden argument*/NULL);
		if (L_63)
		{
			goto IL_08b4;
		}
	}
	{
		int32_t L_64 = __this->get__c_0();
		IL2CPP_RUNTIME_CLASS_INIT(HtmlDocument_t556432108_il2cpp_TypeInfo_var);
		bool L_65 = HtmlDocument_IsWhiteSpace_m1502326855(NULL /*static, unused*/, L_64, /*hidden argument*/NULL);
		if (!L_65)
		{
			goto IL_03b0;
		}
	}
	{
		int32_t L_66 = __this->get__index_7();
		HtmlDocument_PushAttributeNameEnd_m1257735076(__this, ((int32_t)((int32_t)L_66-(int32_t)1)), /*hidden argument*/NULL);
		__this->set__state_20(6);
		goto IL_08b4;
	}

IL_03b0:
	{
		int32_t L_67 = __this->get__c_0();
		if ((!(((uint32_t)L_67) == ((uint32_t)((int32_t)61)))))
		{
			goto IL_03d4;
		}
	}
	{
		int32_t L_68 = __this->get__index_7();
		HtmlDocument_PushAttributeNameEnd_m1257735076(__this, ((int32_t)((int32_t)L_68-(int32_t)1)), /*hidden argument*/NULL);
		__this->set__state_20(7);
		goto IL_08b4;
	}

IL_03d4:
	{
		int32_t L_69 = __this->get__c_0();
		if ((!(((uint32_t)L_69) == ((uint32_t)((int32_t)62)))))
		{
			goto IL_08b4;
		}
	}
	{
		int32_t L_70 = __this->get__index_7();
		HtmlDocument_PushAttributeNameEnd_m1257735076(__this, ((int32_t)((int32_t)L_70-(int32_t)1)), /*hidden argument*/NULL);
		int32_t L_71 = __this->get__index_7();
		bool L_72 = HtmlDocument_PushNodeEnd_m1567272216(__this, L_71, (bool)0, /*hidden argument*/NULL);
		if (L_72)
		{
			goto IL_0414;
		}
	}
	{
		String_t* L_73 = __this->get_Text_22();
		NullCheck(L_73);
		int32_t L_74 = String_get_Length_m1606060069(L_73, /*hidden argument*/NULL);
		__this->set__index_7(L_74);
		goto IL_08b4;
	}

IL_0414:
	{
		int32_t L_75 = __this->get__state_20();
		if ((!(((uint32_t)L_75) == ((uint32_t)5))))
		{
			goto IL_08b4;
		}
	}
	{
		__this->set__state_20(0);
		int32_t L_76 = __this->get__index_7();
		HtmlDocument_PushNodeStart_m1529103168(__this, 3, L_76, /*hidden argument*/NULL);
		goto IL_08b4;
	}

IL_0439:
	{
		bool L_77 = HtmlDocument_NewCheck_m3661304845(__this, /*hidden argument*/NULL);
		if (L_77)
		{
			goto IL_08b4;
		}
	}
	{
		int32_t L_78 = __this->get__c_0();
		IL2CPP_RUNTIME_CLASS_INIT(HtmlDocument_t556432108_il2cpp_TypeInfo_var);
		bool L_79 = HtmlDocument_IsWhiteSpace_m1502326855(NULL /*static, unused*/, L_78, /*hidden argument*/NULL);
		if (L_79)
		{
			goto IL_08b4;
		}
	}
	{
		int32_t L_80 = __this->get__c_0();
		if ((!(((uint32_t)L_80) == ((uint32_t)((int32_t)62)))))
		{
			goto IL_04a8;
		}
	}
	{
		int32_t L_81 = __this->get__index_7();
		bool L_82 = HtmlDocument_PushNodeEnd_m1567272216(__this, L_81, (bool)0, /*hidden argument*/NULL);
		if (L_82)
		{
			goto IL_0483;
		}
	}
	{
		String_t* L_83 = __this->get_Text_22();
		NullCheck(L_83);
		int32_t L_84 = String_get_Length_m1606060069(L_83, /*hidden argument*/NULL);
		__this->set__index_7(L_84);
		goto IL_08b4;
	}

IL_0483:
	{
		int32_t L_85 = __this->get__state_20();
		if ((!(((uint32_t)L_85) == ((uint32_t)6))))
		{
			goto IL_08b4;
		}
	}
	{
		__this->set__state_20(0);
		int32_t L_86 = __this->get__index_7();
		HtmlDocument_PushNodeStart_m1529103168(__this, 3, L_86, /*hidden argument*/NULL);
		goto IL_08b4;
	}

IL_04a8:
	{
		int32_t L_87 = __this->get__c_0();
		if ((!(((uint32_t)L_87) == ((uint32_t)((int32_t)61)))))
		{
			goto IL_04be;
		}
	}
	{
		__this->set__state_20(7);
		goto IL_08b4;
	}

IL_04be:
	{
		__this->set__state_20(3);
		HtmlDocument_DecrementPosition_m1311188097(__this, /*hidden argument*/NULL);
		goto IL_08b4;
	}

IL_04d0:
	{
		bool L_88 = HtmlDocument_NewCheck_m3661304845(__this, /*hidden argument*/NULL);
		if (L_88)
		{
			goto IL_08b4;
		}
	}
	{
		int32_t L_89 = __this->get__c_0();
		IL2CPP_RUNTIME_CLASS_INIT(HtmlDocument_t556432108_il2cpp_TypeInfo_var);
		bool L_90 = HtmlDocument_IsWhiteSpace_m1502326855(NULL /*static, unused*/, L_89, /*hidden argument*/NULL);
		if (L_90)
		{
			goto IL_08b4;
		}
	}
	{
		int32_t L_91 = __this->get__c_0();
		if ((((int32_t)L_91) == ((int32_t)((int32_t)39))))
		{
			goto IL_04ff;
		}
	}
	{
		int32_t L_92 = __this->get__c_0();
		if ((!(((uint32_t)L_92) == ((uint32_t)((int32_t)34)))))
		{
			goto IL_0525;
		}
	}

IL_04ff:
	{
		__this->set__state_20(((int32_t)10));
		int32_t L_93 = __this->get__index_7();
		int32_t L_94 = __this->get__c_0();
		HtmlDocument_PushAttributeValueStart_m1913776826(__this, L_93, L_94, /*hidden argument*/NULL);
		int32_t L_95 = __this->get__c_0();
		V_0 = L_95;
		goto IL_08b4;
	}

IL_0525:
	{
		int32_t L_96 = __this->get__c_0();
		if ((!(((uint32_t)L_96) == ((uint32_t)((int32_t)62)))))
		{
			goto IL_0579;
		}
	}
	{
		int32_t L_97 = __this->get__index_7();
		bool L_98 = HtmlDocument_PushNodeEnd_m1567272216(__this, L_97, (bool)0, /*hidden argument*/NULL);
		if (L_98)
		{
			goto IL_0554;
		}
	}
	{
		String_t* L_99 = __this->get_Text_22();
		NullCheck(L_99);
		int32_t L_100 = String_get_Length_m1606060069(L_99, /*hidden argument*/NULL);
		__this->set__index_7(L_100);
		goto IL_08b4;
	}

IL_0554:
	{
		int32_t L_101 = __this->get__state_20();
		if ((!(((uint32_t)L_101) == ((uint32_t)7))))
		{
			goto IL_08b4;
		}
	}
	{
		__this->set__state_20(0);
		int32_t L_102 = __this->get__index_7();
		HtmlDocument_PushNodeStart_m1529103168(__this, 3, L_102, /*hidden argument*/NULL);
		goto IL_08b4;
	}

IL_0579:
	{
		int32_t L_103 = __this->get__index_7();
		HtmlDocument_PushAttributeValueStart_m2091657843(__this, ((int32_t)((int32_t)L_103-(int32_t)1)), /*hidden argument*/NULL);
		__this->set__state_20(8);
		goto IL_08b4;
	}

IL_0593:
	{
		bool L_104 = HtmlDocument_NewCheck_m3661304845(__this, /*hidden argument*/NULL);
		if (L_104)
		{
			goto IL_08b4;
		}
	}
	{
		int32_t L_105 = __this->get__c_0();
		IL2CPP_RUNTIME_CLASS_INIT(HtmlDocument_t556432108_il2cpp_TypeInfo_var);
		bool L_106 = HtmlDocument_IsWhiteSpace_m1502326855(NULL /*static, unused*/, L_105, /*hidden argument*/NULL);
		if (!L_106)
		{
			goto IL_05c5;
		}
	}
	{
		int32_t L_107 = __this->get__index_7();
		HtmlDocument_PushAttributeValueEnd_m1973000602(__this, ((int32_t)((int32_t)L_107-(int32_t)1)), /*hidden argument*/NULL);
		__this->set__state_20(3);
		goto IL_08b4;
	}

IL_05c5:
	{
		int32_t L_108 = __this->get__c_0();
		if ((!(((uint32_t)L_108) == ((uint32_t)((int32_t)62)))))
		{
			goto IL_08b4;
		}
	}
	{
		int32_t L_109 = __this->get__index_7();
		HtmlDocument_PushAttributeValueEnd_m1973000602(__this, ((int32_t)((int32_t)L_109-(int32_t)1)), /*hidden argument*/NULL);
		int32_t L_110 = __this->get__index_7();
		bool L_111 = HtmlDocument_PushNodeEnd_m1567272216(__this, L_110, (bool)0, /*hidden argument*/NULL);
		if (L_111)
		{
			goto IL_0605;
		}
	}
	{
		String_t* L_112 = __this->get_Text_22();
		NullCheck(L_112);
		int32_t L_113 = String_get_Length_m1606060069(L_112, /*hidden argument*/NULL);
		__this->set__index_7(L_113);
		goto IL_08b4;
	}

IL_0605:
	{
		int32_t L_114 = __this->get__state_20();
		if ((!(((uint32_t)L_114) == ((uint32_t)8))))
		{
			goto IL_08b4;
		}
	}
	{
		__this->set__state_20(0);
		int32_t L_115 = __this->get__index_7();
		HtmlDocument_PushNodeStart_m1529103168(__this, 3, L_115, /*hidden argument*/NULL);
		goto IL_08b4;
	}

IL_062a:
	{
		int32_t L_116 = __this->get__c_0();
		int32_t L_117 = V_0;
		if ((!(((uint32_t)L_116) == ((uint32_t)L_117))))
		{
			goto IL_064d;
		}
	}
	{
		int32_t L_118 = __this->get__index_7();
		HtmlDocument_PushAttributeValueEnd_m1973000602(__this, ((int32_t)((int32_t)L_118-(int32_t)1)), /*hidden argument*/NULL);
		__this->set__state_20(3);
		goto IL_08b4;
	}

IL_064d:
	{
		int32_t L_119 = __this->get__c_0();
		if ((!(((uint32_t)L_119) == ((uint32_t)((int32_t)60)))))
		{
			goto IL_08b4;
		}
	}
	{
		int32_t L_120 = __this->get__index_7();
		String_t* L_121 = __this->get_Text_22();
		NullCheck(L_121);
		int32_t L_122 = String_get_Length_m1606060069(L_121, /*hidden argument*/NULL);
		if ((((int32_t)L_120) >= ((int32_t)L_122)))
		{
			goto IL_08b4;
		}
	}
	{
		String_t* L_123 = __this->get_Text_22();
		int32_t L_124 = __this->get__index_7();
		NullCheck(L_123);
		Il2CppChar L_125 = String_get_Chars_m4230566705(L_123, L_124, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_125) == ((uint32_t)((int32_t)37)))))
		{
			goto IL_08b4;
		}
	}
	{
		int32_t L_126 = __this->get__state_20();
		__this->set__oldstate_14(L_126);
		__this->set__state_20(((int32_t)11));
		goto IL_08b4;
	}

IL_06a1:
	{
		int32_t L_127 = __this->get__c_0();
		if ((!(((uint32_t)L_127) == ((uint32_t)((int32_t)62)))))
		{
			goto IL_08b4;
		}
	}
	{
		bool L_128 = __this->get__fullcomment_6();
		if (!L_128)
		{
			goto IL_06ea;
		}
	}
	{
		String_t* L_129 = __this->get_Text_22();
		int32_t L_130 = __this->get__index_7();
		NullCheck(L_129);
		Il2CppChar L_131 = String_get_Chars_m4230566705(L_129, ((int32_t)((int32_t)L_130-(int32_t)2)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_131) == ((uint32_t)((int32_t)45)))))
		{
			goto IL_08b4;
		}
	}
	{
		String_t* L_132 = __this->get_Text_22();
		int32_t L_133 = __this->get__index_7();
		NullCheck(L_132);
		Il2CppChar L_134 = String_get_Chars_m4230566705(L_132, ((int32_t)((int32_t)L_133-(int32_t)3)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_134) == ((uint32_t)((int32_t)45)))))
		{
			goto IL_08b4;
		}
	}

IL_06ea:
	{
		int32_t L_135 = __this->get__index_7();
		bool L_136 = HtmlDocument_PushNodeEnd_m1567272216(__this, L_135, (bool)0, /*hidden argument*/NULL);
		if (L_136)
		{
			goto IL_070f;
		}
	}
	{
		String_t* L_137 = __this->get_Text_22();
		NullCheck(L_137);
		int32_t L_138 = String_get_Length_m1606060069(L_137, /*hidden argument*/NULL);
		__this->set__index_7(L_138);
		goto IL_08b4;
	}

IL_070f:
	{
		__this->set__state_20(0);
		int32_t L_139 = __this->get__index_7();
		HtmlDocument_PushNodeStart_m1529103168(__this, 3, L_139, /*hidden argument*/NULL);
		goto IL_08b4;
	}

IL_0728:
	{
		int32_t L_140 = __this->get__c_0();
		if ((!(((uint32_t)L_140) == ((uint32_t)((int32_t)37)))))
		{
			goto IL_08b4;
		}
	}
	{
		int32_t L_141 = __this->get__index_7();
		String_t* L_142 = __this->get_Text_22();
		NullCheck(L_142);
		int32_t L_143 = String_get_Length_m1606060069(L_142, /*hidden argument*/NULL);
		if ((((int32_t)L_141) >= ((int32_t)L_143)))
		{
			goto IL_08b4;
		}
	}
	{
		String_t* L_144 = __this->get_Text_22();
		int32_t L_145 = __this->get__index_7();
		NullCheck(L_144);
		Il2CppChar L_146 = String_get_Chars_m4230566705(L_144, L_145, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_146) == ((uint32_t)((int32_t)62)))))
		{
			goto IL_08b4;
		}
	}
	{
		int32_t L_147 = __this->get__oldstate_14();
		V_4 = L_147;
		int32_t L_148 = V_4;
		if ((((int32_t)L_148) == ((int32_t)3)))
		{
			goto IL_077e;
		}
	}
	{
		int32_t L_149 = V_4;
		if ((!(((uint32_t)L_149) == ((uint32_t)7))))
		{
			goto IL_0795;
		}
	}
	{
		__this->set__state_20(8);
		goto IL_07a1;
	}

IL_077e:
	{
		int32_t L_150 = __this->get__index_7();
		HtmlDocument_PushAttributeNameEnd_m1257735076(__this, ((int32_t)((int32_t)L_150+(int32_t)1)), /*hidden argument*/NULL);
		__this->set__state_20(3);
		goto IL_07a1;
	}

IL_0795:
	{
		int32_t L_151 = __this->get__oldstate_14();
		__this->set__state_20(L_151);
	}

IL_07a1:
	{
		HtmlDocument_IncrementPosition_m421960747(__this, /*hidden argument*/NULL);
		goto IL_08b4;
	}

IL_07ac:
	{
		HtmlNode_t2048434459 * L_152 = __this->get__currentnode_3();
		NullCheck(L_152);
		int32_t L_153 = L_152->get__namelength_10();
		String_t* L_154 = __this->get_Text_22();
		NullCheck(L_154);
		int32_t L_155 = String_get_Length_m1606060069(L_154, /*hidden argument*/NULL);
		int32_t L_156 = __this->get__index_7();
		if ((((int32_t)((int32_t)((int32_t)L_153+(int32_t)3))) > ((int32_t)((int32_t)((int32_t)L_155-(int32_t)((int32_t)((int32_t)L_156-(int32_t)1)))))))
		{
			goto IL_08b4;
		}
	}
	{
		String_t* L_157 = __this->get_Text_22();
		int32_t L_158 = __this->get__index_7();
		HtmlNode_t2048434459 * L_159 = __this->get__currentnode_3();
		NullCheck(L_159);
		int32_t L_160 = L_159->get__namelength_10();
		NullCheck(L_157);
		String_t* L_161 = String_Substring_m12482732(L_157, ((int32_t)((int32_t)L_158-(int32_t)1)), ((int32_t)((int32_t)L_160+(int32_t)2)), /*hidden argument*/NULL);
		HtmlNode_t2048434459 * L_162 = __this->get__currentnode_3();
		NullCheck(L_162);
		String_t* L_163 = HtmlNode_get_Name_m702134575(L_162, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_164 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral51790603, L_163, /*hidden argument*/NULL);
		int32_t L_165 = String_Compare_m3288062998(NULL /*static, unused*/, L_161, L_164, 5, /*hidden argument*/NULL);
		if (L_165)
		{
			goto IL_08b4;
		}
	}
	{
		String_t* L_166 = __this->get_Text_22();
		int32_t L_167 = __this->get__index_7();
		HtmlNode_t2048434459 * L_168 = __this->get__currentnode_3();
		NullCheck(L_168);
		String_t* L_169 = HtmlNode_get_Name_m702134575(L_168, /*hidden argument*/NULL);
		NullCheck(L_169);
		int32_t L_170 = String_get_Length_m1606060069(L_169, /*hidden argument*/NULL);
		NullCheck(L_166);
		Il2CppChar L_171 = String_get_Chars_m4230566705(L_166, ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_167-(int32_t)1))+(int32_t)2))+(int32_t)L_170)), /*hidden argument*/NULL);
		V_1 = L_171;
		int32_t L_172 = V_1;
		if ((((int32_t)L_172) == ((int32_t)((int32_t)62))))
		{
			goto IL_0846;
		}
	}
	{
		int32_t L_173 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(HtmlDocument_t556432108_il2cpp_TypeInfo_var);
		bool L_174 = HtmlDocument_IsWhiteSpace_m1502326855(NULL /*static, unused*/, L_173, /*hidden argument*/NULL);
		if (!L_174)
		{
			goto IL_08b4;
		}
	}

IL_0846:
	{
		HtmlNode_t2048434459 * L_175 = __this->get__currentnode_3();
		NullCheck(L_175);
		int32_t L_176 = L_175->get__outerstartindex_17();
		HtmlNode_t2048434459 * L_177 = __this->get__currentnode_3();
		NullCheck(L_177);
		int32_t L_178 = L_177->get__outerlength_16();
		HtmlNode_t2048434459 * L_179 = HtmlDocument_CreateNode_m538784785(__this, 3, ((int32_t)((int32_t)L_176+(int32_t)L_178)), /*hidden argument*/NULL);
		V_2 = L_179;
		HtmlNode_t2048434459 * L_180 = V_2;
		int32_t L_181 = __this->get__index_7();
		HtmlNode_t2048434459 * L_182 = V_2;
		NullCheck(L_182);
		int32_t L_183 = L_182->get__outerstartindex_17();
		NullCheck(L_180);
		L_180->set__outerlength_16(((int32_t)((int32_t)((int32_t)((int32_t)L_181-(int32_t)1))-(int32_t)L_183)));
		HtmlNode_t2048434459 * L_184 = __this->get__currentnode_3();
		HtmlNode_t2048434459 * L_185 = V_2;
		NullCheck(L_184);
		HtmlNode_AppendChild_m2583227907(L_184, L_185, /*hidden argument*/NULL);
		int32_t L_186 = __this->get__index_7();
		HtmlDocument_PushNodeStart_m1529103168(__this, 1, ((int32_t)((int32_t)L_186-(int32_t)1)), /*hidden argument*/NULL);
		int32_t L_187 = __this->get__index_7();
		HtmlDocument_PushNodeNameStart_m2396823204(__this, (bool)0, ((int32_t)((int32_t)((int32_t)((int32_t)L_187-(int32_t)1))+(int32_t)2)), /*hidden argument*/NULL);
		__this->set__state_20(2);
		HtmlDocument_IncrementPosition_m421960747(__this, /*hidden argument*/NULL);
	}

IL_08b4:
	{
		int32_t L_188 = __this->get__index_7();
		String_t* L_189 = __this->get_Text_22();
		NullCheck(L_189);
		int32_t L_190 = String_get_Length_m1606060069(L_189, /*hidden argument*/NULL);
		if ((((int32_t)L_188) < ((int32_t)L_190)))
		{
			goto IL_00d3;
		}
	}
	{
		HtmlNode_t2048434459 * L_191 = __this->get__currentnode_3();
		NullCheck(L_191);
		int32_t L_192 = L_191->get__namestartindex_11();
		if ((((int32_t)L_192) <= ((int32_t)0)))
		{
			goto IL_08e4;
		}
	}
	{
		int32_t L_193 = __this->get__index_7();
		HtmlDocument_PushNodeNameEnd_m3862249348(__this, L_193, /*hidden argument*/NULL);
	}

IL_08e4:
	{
		int32_t L_194 = __this->get__index_7();
		HtmlDocument_PushNodeEnd_m1567272216(__this, L_194, (bool)0, /*hidden argument*/NULL);
		Dictionary_2_t3963213721 * L_195 = __this->get_Lastnodes_8();
		NullCheck(L_195);
		Dictionary_2_Clear_m3645269862(L_195, /*hidden argument*/Dictionary_2_Clear_m3645269862_RuntimeMethod_var);
		return;
	}
}
// System.Void HtmlAgilityPack.HtmlDocument::PushAttributeNameEnd(System.Int32)
extern "C"  void HtmlDocument_PushAttributeNameEnd_m1257735076 (HtmlDocument_t556432108 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		HtmlAttribute_t1804523403 * L_0 = __this->get__currentattribute_2();
		int32_t L_1 = ___index0;
		HtmlAttribute_t1804523403 * L_2 = __this->get__currentattribute_2();
		NullCheck(L_2);
		int32_t L_3 = L_2->get__namestartindex_4();
		NullCheck(L_0);
		L_0->set__namelength_3(((int32_t)((int32_t)L_1-(int32_t)L_3)));
		HtmlNode_t2048434459 * L_4 = __this->get__currentnode_3();
		NullCheck(L_4);
		HtmlAttributeCollection_t1787476631 * L_5 = HtmlNode_get_Attributes_m2935417207(L_4, /*hidden argument*/NULL);
		HtmlAttribute_t1804523403 * L_6 = __this->get__currentattribute_2();
		NullCheck(L_5);
		HtmlAttributeCollection_Append_m138838623(L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HtmlAgilityPack.HtmlDocument::PushAttributeNameStart(System.Int32)
extern "C"  void HtmlDocument_PushAttributeNameStart_m687133679 (HtmlDocument_t556432108 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		HtmlAttribute_t1804523403 * L_0 = HtmlDocument_CreateAttribute_m1400299982(__this, /*hidden argument*/NULL);
		__this->set__currentattribute_2(L_0);
		HtmlAttribute_t1804523403 * L_1 = __this->get__currentattribute_2();
		int32_t L_2 = ___index0;
		NullCheck(L_1);
		L_1->set__namestartindex_4(L_2);
		HtmlAttribute_t1804523403 * L_3 = __this->get__currentattribute_2();
		int32_t L_4 = __this->get__line_10();
		NullCheck(L_3);
		HtmlAttribute_set_Line_m153524436(L_3, L_4, /*hidden argument*/NULL);
		HtmlAttribute_t1804523403 * L_5 = __this->get__currentattribute_2();
		int32_t L_6 = __this->get__lineposition_11();
		NullCheck(L_5);
		L_5->set__lineposition_1(L_6);
		HtmlAttribute_t1804523403 * L_7 = __this->get__currentattribute_2();
		int32_t L_8 = ___index0;
		NullCheck(L_7);
		L_7->set__streamposition_8(L_8);
		return;
	}
}
// System.Void HtmlAgilityPack.HtmlDocument::PushAttributeValueEnd(System.Int32)
extern "C"  void HtmlDocument_PushAttributeValueEnd_m1973000602 (HtmlDocument_t556432108 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		HtmlAttribute_t1804523403 * L_0 = __this->get__currentattribute_2();
		int32_t L_1 = ___index0;
		HtmlAttribute_t1804523403 * L_2 = __this->get__currentattribute_2();
		NullCheck(L_2);
		int32_t L_3 = L_2->get__valuestartindex_11();
		NullCheck(L_0);
		L_0->set__valuelength_10(((int32_t)((int32_t)L_1-(int32_t)L_3)));
		return;
	}
}
// System.Void HtmlAgilityPack.HtmlDocument::PushAttributeValueStart(System.Int32)
extern "C"  void HtmlDocument_PushAttributeValueStart_m2091657843 (HtmlDocument_t556432108 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		HtmlDocument_PushAttributeValueStart_m1913776826(__this, L_0, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HtmlAgilityPack.HtmlDocument::PushAttributeValueStart(System.Int32,System.Int32)
extern "C"  void HtmlDocument_PushAttributeValueStart_m1913776826 (HtmlDocument_t556432108 * __this, int32_t ___index0, int32_t ___quote1, const RuntimeMethod* method)
{
	{
		HtmlAttribute_t1804523403 * L_0 = __this->get__currentattribute_2();
		int32_t L_1 = ___index0;
		NullCheck(L_0);
		L_0->set__valuestartindex_11(L_1);
		int32_t L_2 = ___quote1;
		if ((!(((uint32_t)L_2) == ((uint32_t)((int32_t)39)))))
		{
			goto IL_001d;
		}
	}
	{
		HtmlAttribute_t1804523403 * L_3 = __this->get__currentattribute_2();
		NullCheck(L_3);
		HtmlAttribute_set_QuoteType_m1504249919(L_3, 0, /*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.Boolean HtmlAgilityPack.HtmlDocument::PushNodeEnd(System.Int32,System.Boolean)
extern "C"  bool HtmlDocument_PushNodeEnd_m1567272216 (HtmlDocument_t556432108 * __this, int32_t ___index0, bool ___close1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlDocument_PushNodeEnd_m1567272216_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	HtmlNode_t2048434459 * V_0 = NULL;
	{
		HtmlNode_t2048434459 * L_0 = __this->get__currentnode_3();
		int32_t L_1 = ___index0;
		HtmlNode_t2048434459 * L_2 = __this->get__currentnode_3();
		NullCheck(L_2);
		int32_t L_3 = L_2->get__outerstartindex_17();
		NullCheck(L_0);
		L_0->set__outerlength_16(((int32_t)((int32_t)L_1-(int32_t)L_3)));
		HtmlNode_t2048434459 * L_4 = __this->get__currentnode_3();
		NullCheck(L_4);
		int32_t L_5 = L_4->get__nodetype_13();
		if ((((int32_t)L_5) == ((int32_t)3)))
		{
			goto IL_0034;
		}
	}
	{
		HtmlNode_t2048434459 * L_6 = __this->get__currentnode_3();
		NullCheck(L_6);
		int32_t L_7 = L_6->get__nodetype_13();
		if ((!(((uint32_t)L_7) == ((uint32_t)2))))
		{
			goto IL_0093;
		}
	}

IL_0034:
	{
		HtmlNode_t2048434459 * L_8 = __this->get__currentnode_3();
		NullCheck(L_8);
		int32_t L_9 = L_8->get__outerlength_16();
		if ((((int32_t)L_9) <= ((int32_t)0)))
		{
			goto IL_017e;
		}
	}
	{
		HtmlNode_t2048434459 * L_10 = __this->get__currentnode_3();
		HtmlNode_t2048434459 * L_11 = __this->get__currentnode_3();
		NullCheck(L_11);
		int32_t L_12 = L_11->get__outerlength_16();
		NullCheck(L_10);
		L_10->set__innerlength_5(L_12);
		HtmlNode_t2048434459 * L_13 = __this->get__currentnode_3();
		HtmlNode_t2048434459 * L_14 = __this->get__currentnode_3();
		NullCheck(L_14);
		int32_t L_15 = L_14->get__outerstartindex_17();
		NullCheck(L_13);
		L_13->set__innerstartindex_6(L_15);
		HtmlNode_t2048434459 * L_16 = __this->get__lastparentnode_9();
		if (!L_16)
		{
			goto IL_017e;
		}
	}
	{
		HtmlNode_t2048434459 * L_17 = __this->get__lastparentnode_9();
		HtmlNode_t2048434459 * L_18 = __this->get__currentnode_3();
		NullCheck(L_17);
		HtmlNode_AppendChild_m2583227907(L_17, L_18, /*hidden argument*/NULL);
		goto IL_017e;
	}

IL_0093:
	{
		HtmlNode_t2048434459 * L_19 = __this->get__currentnode_3();
		NullCheck(L_19);
		bool L_20 = L_19->get__starttag_23();
		if (!L_20)
		{
			goto IL_017e;
		}
	}
	{
		HtmlNode_t2048434459 * L_21 = __this->get__lastparentnode_9();
		HtmlNode_t2048434459 * L_22 = __this->get__currentnode_3();
		if ((((RuntimeObject*)(HtmlNode_t2048434459 *)L_21) == ((RuntimeObject*)(HtmlNode_t2048434459 *)L_22)))
		{
			goto IL_017e;
		}
	}
	{
		HtmlNode_t2048434459 * L_23 = __this->get__lastparentnode_9();
		if (!L_23)
		{
			goto IL_00ce;
		}
	}
	{
		HtmlNode_t2048434459 * L_24 = __this->get__lastparentnode_9();
		HtmlNode_t2048434459 * L_25 = __this->get__currentnode_3();
		NullCheck(L_24);
		HtmlNode_AppendChild_m2583227907(L_24, L_25, /*hidden argument*/NULL);
	}

IL_00ce:
	{
		HtmlNode_t2048434459 * L_26 = __this->get__currentnode_3();
		HtmlDocument_ReadDocumentEncoding_m3190387121(__this, L_26, /*hidden argument*/NULL);
		Dictionary_2_t3963213721 * L_27 = __this->get_Lastnodes_8();
		HtmlNode_t2048434459 * L_28 = __this->get__currentnode_3();
		NullCheck(L_28);
		String_t* L_29 = HtmlNode_get_Name_m702134575(L_28, /*hidden argument*/NULL);
		HtmlNode_t2048434459 * L_30 = Utilities_GetDictionaryValueOrNull_TisString_t_TisHtmlNode_t2048434459_m2137127104(NULL /*static, unused*/, L_27, L_29, /*hidden argument*/Utilities_GetDictionaryValueOrNull_TisString_t_TisHtmlNode_t2048434459_m2137127104_RuntimeMethod_var);
		V_0 = L_30;
		HtmlNode_t2048434459 * L_31 = __this->get__currentnode_3();
		HtmlNode_t2048434459 * L_32 = V_0;
		NullCheck(L_31);
		L_31->set__prevwithsamename_22(L_32);
		Dictionary_2_t3963213721 * L_33 = __this->get_Lastnodes_8();
		HtmlNode_t2048434459 * L_34 = __this->get__currentnode_3();
		NullCheck(L_34);
		String_t* L_35 = HtmlNode_get_Name_m702134575(L_34, /*hidden argument*/NULL);
		HtmlNode_t2048434459 * L_36 = __this->get__currentnode_3();
		NullCheck(L_33);
		Dictionary_2_set_Item_m3521183286(L_33, L_35, L_36, /*hidden argument*/Dictionary_2_set_Item_m3521183286_RuntimeMethod_var);
		HtmlNode_t2048434459 * L_37 = __this->get__currentnode_3();
		NullCheck(L_37);
		int32_t L_38 = HtmlNode_get_NodeType_m270476988(L_37, /*hidden argument*/NULL);
		if (!L_38)
		{
			goto IL_0134;
		}
	}
	{
		HtmlNode_t2048434459 * L_39 = __this->get__currentnode_3();
		NullCheck(L_39);
		int32_t L_40 = HtmlNode_get_NodeType_m270476988(L_39, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_40) == ((uint32_t)1))))
		{
			goto IL_0140;
		}
	}

IL_0134:
	{
		HtmlNode_t2048434459 * L_41 = __this->get__currentnode_3();
		__this->set__lastparentnode_9(L_41);
	}

IL_0140:
	{
		String_t* L_42 = HtmlDocument_CurrentNodeName_m112987032(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(HtmlNode_t2048434459_il2cpp_TypeInfo_var);
		bool L_43 = HtmlNode_IsCDataElement_m1574064561(NULL /*static, unused*/, L_42, /*hidden argument*/NULL);
		if (!L_43)
		{
			goto IL_0157;
		}
	}
	{
		__this->set__state_20(((int32_t)12));
		return (bool)1;
	}

IL_0157:
	{
		HtmlNode_t2048434459 * L_44 = __this->get__currentnode_3();
		NullCheck(L_44);
		String_t* L_45 = HtmlNode_get_Name_m702134575(L_44, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(HtmlNode_t2048434459_il2cpp_TypeInfo_var);
		bool L_46 = HtmlNode_IsClosedElement_m2492183536(NULL /*static, unused*/, L_45, /*hidden argument*/NULL);
		if (L_46)
		{
			goto IL_017b;
		}
	}
	{
		HtmlNode_t2048434459 * L_47 = __this->get__currentnode_3();
		NullCheck(L_47);
		String_t* L_48 = HtmlNode_get_Name_m702134575(L_47, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(HtmlNode_t2048434459_il2cpp_TypeInfo_var);
		bool L_49 = HtmlNode_IsEmptyElement_m2918938855(NULL /*static, unused*/, L_48, /*hidden argument*/NULL);
		if (!L_49)
		{
			goto IL_017e;
		}
	}

IL_017b:
	{
		___close1 = (bool)1;
	}

IL_017e:
	{
		bool L_50 = ___close1;
		if (L_50)
		{
			goto IL_018e;
		}
	}
	{
		HtmlNode_t2048434459 * L_51 = __this->get__currentnode_3();
		NullCheck(L_51);
		bool L_52 = L_51->get__starttag_23();
		if (L_52)
		{
			goto IL_01e3;
		}
	}

IL_018e:
	{
		String_t* L_53 = __this->get_OptionStopperNodeName_36();
		if (!L_53)
		{
			goto IL_01dd;
		}
	}
	{
		String_t* L_54 = __this->get__remainder_18();
		if (L_54)
		{
			goto IL_01dd;
		}
	}
	{
		HtmlNode_t2048434459 * L_55 = __this->get__currentnode_3();
		NullCheck(L_55);
		String_t* L_56 = HtmlNode_get_Name_m702134575(L_55, /*hidden argument*/NULL);
		String_t* L_57 = __this->get_OptionStopperNodeName_36();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		int32_t L_58 = String_Compare_m3288062998(NULL /*static, unused*/, L_56, L_57, 5, /*hidden argument*/NULL);
		if (L_58)
		{
			goto IL_01dd;
		}
	}
	{
		int32_t L_59 = ___index0;
		__this->set__remainderOffset_19(L_59);
		String_t* L_60 = __this->get_Text_22();
		int32_t L_61 = __this->get__remainderOffset_19();
		NullCheck(L_60);
		String_t* L_62 = String_Substring_m2032624251(L_60, L_61, /*hidden argument*/NULL);
		__this->set__remainder_18(L_62);
		HtmlDocument_CloseCurrentNode_m2823362304(__this, /*hidden argument*/NULL);
		return (bool)0;
	}

IL_01dd:
	{
		HtmlDocument_CloseCurrentNode_m2823362304(__this, /*hidden argument*/NULL);
	}

IL_01e3:
	{
		return (bool)1;
	}
}
// System.Void HtmlAgilityPack.HtmlDocument::PushNodeNameEnd(System.Int32)
extern "C"  void HtmlDocument_PushNodeNameEnd_m3862249348 (HtmlDocument_t556432108 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		HtmlNode_t2048434459 * L_0 = __this->get__currentnode_3();
		int32_t L_1 = ___index0;
		HtmlNode_t2048434459 * L_2 = __this->get__currentnode_3();
		NullCheck(L_2);
		int32_t L_3 = L_2->get__namestartindex_11();
		NullCheck(L_0);
		L_0->set__namelength_10(((int32_t)((int32_t)L_1-(int32_t)L_3)));
		bool L_4 = __this->get_OptionFixNestedTags_30();
		if (!L_4)
		{
			goto IL_0026;
		}
	}
	{
		HtmlDocument_FixNestedTags_m2961550712(__this, /*hidden argument*/NULL);
	}

IL_0026:
	{
		return;
	}
}
// System.Void HtmlAgilityPack.HtmlDocument::PushNodeNameStart(System.Boolean,System.Int32)
extern "C"  void HtmlDocument_PushNodeNameStart_m2396823204 (HtmlDocument_t556432108 * __this, bool ___starttag0, int32_t ___index1, const RuntimeMethod* method)
{
	{
		HtmlNode_t2048434459 * L_0 = __this->get__currentnode_3();
		bool L_1 = ___starttag0;
		NullCheck(L_0);
		L_0->set__starttag_23(L_1);
		HtmlNode_t2048434459 * L_2 = __this->get__currentnode_3();
		int32_t L_3 = ___index1;
		NullCheck(L_2);
		L_2->set__namestartindex_11(L_3);
		return;
	}
}
// System.Void HtmlAgilityPack.HtmlDocument::PushNodeStart(HtmlAgilityPack.HtmlNodeType,System.Int32)
extern "C"  void HtmlDocument_PushNodeStart_m1529103168 (HtmlDocument_t556432108 * __this, int32_t ___type0, int32_t ___index1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___type0;
		int32_t L_1 = ___index1;
		HtmlNode_t2048434459 * L_2 = HtmlDocument_CreateNode_m538784785(__this, L_0, L_1, /*hidden argument*/NULL);
		__this->set__currentnode_3(L_2);
		HtmlNode_t2048434459 * L_3 = __this->get__currentnode_3();
		int32_t L_4 = __this->get__line_10();
		NullCheck(L_3);
		L_3->set__line_7(L_4);
		HtmlNode_t2048434459 * L_5 = __this->get__currentnode_3();
		int32_t L_6 = __this->get__lineposition_11();
		NullCheck(L_5);
		L_5->set__lineposition_8(L_6);
		int32_t L_7 = ___type0;
		if ((!(((uint32_t)L_7) == ((uint32_t)1))))
		{
			goto IL_0047;
		}
	}
	{
		HtmlNode_t2048434459 * L_8 = __this->get__currentnode_3();
		HtmlNode_t2048434459 * L_9 = L_8;
		NullCheck(L_9);
		int32_t L_10 = L_9->get__lineposition_8();
		NullCheck(L_9);
		L_9->set__lineposition_8(((int32_t)((int32_t)L_10-(int32_t)1)));
	}

IL_0047:
	{
		HtmlNode_t2048434459 * L_11 = __this->get__currentnode_3();
		int32_t L_12 = ___index1;
		NullCheck(L_11);
		L_11->set__streamposition_24(L_12);
		return;
	}
}
// System.Void HtmlAgilityPack.HtmlDocument::ReadDocumentEncoding(HtmlAgilityPack.HtmlNode)
extern "C"  void HtmlDocument_ReadDocumentEncoding_m3190387121 (HtmlDocument_t556432108 * __this, HtmlNode_t2048434459 * ___node0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlDocument_ReadDocumentEncoding_m3190387121_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	HtmlAttribute_t1804523403 * V_0 = NULL;
	HtmlAttribute_t1804523403 * V_1 = NULL;
	String_t* V_2 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = __this->get_OptionReadEncoding_35();
		if (L_0)
		{
			goto IL_0009;
		}
	}
	{
		return;
	}

IL_0009:
	{
		HtmlNode_t2048434459 * L_1 = ___node0;
		NullCheck(L_1);
		int32_t L_2 = L_1->get__namelength_10();
		if ((((int32_t)L_2) == ((int32_t)4)))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}

IL_0013:
	{
		HtmlNode_t2048434459 * L_3 = ___node0;
		NullCheck(L_3);
		String_t* L_4 = HtmlNode_get_Name_m702134575(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_4, _stringLiteral3732937281, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0026;
		}
	}
	{
		return;
	}

IL_0026:
	{
		HtmlNode_t2048434459 * L_6 = ___node0;
		NullCheck(L_6);
		HtmlAttributeCollection_t1787476631 * L_7 = HtmlNode_get_Attributes_m2935417207(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		HtmlAttribute_t1804523403 * L_8 = HtmlAttributeCollection_get_Item_m3848708643(L_7, _stringLiteral2582021199, /*hidden argument*/NULL);
		V_0 = L_8;
		HtmlAttribute_t1804523403 * L_9 = V_0;
		if (L_9)
		{
			goto IL_003b;
		}
	}
	{
		return;
	}

IL_003b:
	{
		HtmlAttribute_t1804523403 * L_10 = V_0;
		NullCheck(L_10);
		String_t* L_11 = HtmlAttribute_get_Value_m3933521327(L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		int32_t L_12 = String_Compare_m3288062998(NULL /*static, unused*/, L_11, _stringLiteral4165124290, 5, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_004f;
		}
	}
	{
		return;
	}

IL_004f:
	{
		HtmlNode_t2048434459 * L_13 = ___node0;
		NullCheck(L_13);
		HtmlAttributeCollection_t1787476631 * L_14 = HtmlNode_get_Attributes_m2935417207(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		HtmlAttribute_t1804523403 * L_15 = HtmlAttributeCollection_get_Item_m3848708643(L_14, _stringLiteral1561970665, /*hidden argument*/NULL);
		V_1 = L_15;
		HtmlAttribute_t1804523403 * L_16 = V_1;
		if (!L_16)
		{
			goto IL_012e;
		}
	}
	{
		HtmlAttribute_t1804523403 * L_17 = V_1;
		NullCheck(L_17);
		String_t* L_18 = HtmlAttribute_get_Value_m3933521327(L_17, /*hidden argument*/NULL);
		String_t* L_19 = NameValuePairList_GetNameValuePairsValue_m1108671514(NULL /*static, unused*/, L_18, _stringLiteral3651008258, /*hidden argument*/NULL);
		V_2 = L_19;
		String_t* L_20 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_21 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		if (L_21)
		{
			goto IL_012e;
		}
	}
	{
		String_t* L_22 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_23 = String_Equals_m2950069882(NULL /*static, unused*/, L_22, _stringLiteral627879171, 5, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_0096;
		}
	}
	{
		V_2 = _stringLiteral506264602;
	}

IL_0096:
	try
	{ // begin try (depth: 1)
		String_t* L_24 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
		Encoding_t663144255 * L_25 = Encoding_GetEncoding_m2475966878(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		__this->set__declaredencoding_4(L_25);
		goto IL_00ae;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (ArgumentException_t3259014390_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_00a4;
		throw e;
	}

CATCH_00a4:
	{ // begin catch(System.ArgumentException)
		__this->set__declaredencoding_4((Encoding_t663144255 *)NULL);
		goto IL_00ae;
	} // end catch (depth: 1)

IL_00ae:
	{
		bool L_26 = __this->get__onlyDetectEncoding_15();
		if (!L_26)
		{
			goto IL_00c2;
		}
	}
	{
		Encoding_t663144255 * L_27 = __this->get__declaredencoding_4();
		EncodingFoundException_t1471015948 * L_28 = (EncodingFoundException_t1471015948 *)il2cpp_codegen_object_new(EncodingFoundException_t1471015948_il2cpp_TypeInfo_var);
		EncodingFoundException__ctor_m308448534(L_28, L_27, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_28);
	}

IL_00c2:
	{
		Encoding_t663144255 * L_29 = __this->get__streamencoding_21();
		if (!L_29)
		{
			goto IL_012e;
		}
	}
	{
		Encoding_t663144255 * L_30 = __this->get__declaredencoding_4();
		if (!L_30)
		{
			goto IL_012e;
		}
	}
	{
		Encoding_t663144255 * L_31 = __this->get__declaredencoding_4();
		NullCheck(L_31);
		int32_t L_32 = VirtFuncInvoker0< int32_t >::Invoke(26 /* System.Int32 System.Text.Encoding::get_WindowsCodePage() */, L_31);
		Encoding_t663144255 * L_33 = __this->get__streamencoding_21();
		NullCheck(L_33);
		int32_t L_34 = VirtFuncInvoker0< int32_t >::Invoke(26 /* System.Int32 System.Text.Encoding::get_WindowsCodePage() */, L_33);
		if ((((int32_t)L_32) == ((int32_t)L_34)))
		{
			goto IL_012e;
		}
	}
	{
		int32_t L_35 = __this->get__line_10();
		int32_t L_36 = __this->get__lineposition_11();
		int32_t L_37 = __this->get__index_7();
		HtmlNode_t2048434459 * L_38 = ___node0;
		NullCheck(L_38);
		String_t* L_39 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String HtmlAgilityPack.HtmlNode::get_OuterHtml() */, L_38);
		Encoding_t663144255 * L_40 = __this->get__streamencoding_21();
		NullCheck(L_40);
		String_t* L_41 = VirtFuncInvoker0< String_t* >::Invoke(25 /* System.String System.Text.Encoding::get_WebName() */, L_40);
		Encoding_t663144255 * L_42 = __this->get__declaredencoding_4();
		NullCheck(L_42);
		String_t* L_43 = VirtFuncInvoker0< String_t* >::Invoke(25 /* System.String System.Text.Encoding::get_WebName() */, L_42);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_44 = String_Concat_m1561703559(NULL /*static, unused*/, _stringLiteral3504293556, L_41, _stringLiteral3303919300, L_43, /*hidden argument*/NULL);
		HtmlDocument_AddError_m2023821899(__this, 2, L_35, L_36, L_37, L_39, L_44, /*hidden argument*/NULL);
	}

IL_012e:
	{
		return;
	}
}
// System.Void HtmlAgilityPack.HtmlDocument::.cctor()
extern "C"  void HtmlDocument__cctor_m716884824 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlDocument__cctor_m716884824_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((HtmlDocument_t556432108_StaticFields*)il2cpp_codegen_static_fields_for(HtmlDocument_t556432108_il2cpp_TypeInfo_var))->set_HtmlExceptionRefNotChild_39(_stringLiteral1591976195);
		((HtmlDocument_t556432108_StaticFields*)il2cpp_codegen_static_fields_for(HtmlDocument_t556432108_il2cpp_TypeInfo_var))->set_HtmlExceptionUseIdAttributeFalse_40(_stringLiteral4276579882);
		return;
	}
}
// System.String HtmlAgilityPack.HtmlNameTable::Add(System.String)
extern "C"  String_t* HtmlNameTable_Add_m3043747575 (HtmlNameTable_t3848610378 * __this, String_t* ___array0, const RuntimeMethod* method)
{
	{
		NameTable_t594386929 * L_0 = __this->get__nametable_0();
		String_t* L_1 = ___array0;
		NullCheck(L_0);
		String_t* L_2 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(4 /* System.String System.Xml.XmlNameTable::Add(System.String) */, L_0, L_1);
		return L_2;
	}
}
// System.String HtmlAgilityPack.HtmlNameTable::Get(System.String)
extern "C"  String_t* HtmlNameTable_Get_m914382696 (HtmlNameTable_t3848610378 * __this, String_t* ___array0, const RuntimeMethod* method)
{
	{
		NameTable_t594386929 * L_0 = __this->get__nametable_0();
		String_t* L_1 = ___array0;
		NullCheck(L_0);
		String_t* L_2 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(5 /* System.String System.Xml.XmlNameTable::Get(System.String) */, L_0, L_1);
		return L_2;
	}
}
// System.String HtmlAgilityPack.HtmlNameTable::GetOrAdd(System.String)
extern "C"  String_t* HtmlNameTable_GetOrAdd_m3409817290 (HtmlNameTable_t3848610378 * __this, String_t* ___array0, const RuntimeMethod* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = ___array0;
		String_t* L_1 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(5 /* System.String System.Xml.XmlNameTable::Get(System.String) */, __this, L_0);
		V_0 = L_1;
		String_t* L_2 = V_0;
		if (L_2)
		{
			goto IL_0013;
		}
	}
	{
		String_t* L_3 = ___array0;
		String_t* L_4 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(4 /* System.String System.Xml.XmlNameTable::Add(System.String) */, __this, L_3);
		return L_4;
	}

IL_0013:
	{
		String_t* L_5 = V_0;
		return L_5;
	}
}
// System.Void HtmlAgilityPack.HtmlNameTable::.ctor()
extern "C"  void HtmlNameTable__ctor_m4091390005 (HtmlNameTable_t3848610378 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlNameTable__ctor_m4091390005_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NameTable_t594386929 * L_0 = (NameTable_t594386929 *)il2cpp_codegen_object_new(NameTable_t594386929_il2cpp_TypeInfo_var);
		NameTable__ctor_m2766031050(L_0, /*hidden argument*/NULL);
		__this->set__nametable_0(L_0);
		XmlNameTable__ctor_m684419949(__this, /*hidden argument*/NULL);
		return;
	}
}
// HtmlAgilityPack.HtmlNodeCollection HtmlAgilityPack.HtmlNode::SelectNodes(System.String)
extern "C"  HtmlNodeCollection_t2542734491 * HtmlNode_SelectNodes_m3648812346 (HtmlNode_t2048434459 * __this, String_t* ___xpath0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlNode_SelectNodes_m3648812346_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	HtmlNodeCollection_t2542734491 * V_0 = NULL;
	HtmlNodeNavigator_t2752606360 * V_1 = NULL;
	XPathNodeIterator_t3192332357 * V_2 = NULL;
	HtmlNodeNavigator_t2752606360 * V_3 = NULL;
	{
		HtmlNodeCollection_t2542734491 * L_0 = (HtmlNodeCollection_t2542734491 *)il2cpp_codegen_object_new(HtmlNodeCollection_t2542734491_il2cpp_TypeInfo_var);
		HtmlNodeCollection__ctor_m429744290(L_0, (HtmlNode_t2048434459 *)NULL, /*hidden argument*/NULL);
		V_0 = L_0;
		HtmlDocument_t556432108 * L_1 = HtmlNode_get_OwnerDocument_m2055696179(__this, /*hidden argument*/NULL);
		HtmlNodeNavigator_t2752606360 * L_2 = (HtmlNodeNavigator_t2752606360 *)il2cpp_codegen_object_new(HtmlNodeNavigator_t2752606360_il2cpp_TypeInfo_var);
		HtmlNodeNavigator__ctor_m1439946702(L_2, L_1, __this, /*hidden argument*/NULL);
		V_1 = L_2;
		HtmlNodeNavigator_t2752606360 * L_3 = V_1;
		String_t* L_4 = ___xpath0;
		NullCheck(L_3);
		XPathNodeIterator_t3192332357 * L_5 = VirtFuncInvoker1< XPathNodeIterator_t3192332357 *, String_t* >::Invoke(32 /* System.Xml.XPath.XPathNodeIterator System.Xml.XPath.XPathNavigator::Select(System.String) */, L_3, L_4);
		V_2 = L_5;
		goto IL_0036;
	}

IL_001e:
	{
		XPathNodeIterator_t3192332357 * L_6 = V_2;
		NullCheck(L_6);
		XPathNavigator_t3981235968 * L_7 = VirtFuncInvoker0< XPathNavigator_t3981235968 * >::Invoke(7 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNodeIterator::get_Current() */, L_6);
		V_3 = ((HtmlNodeNavigator_t2752606360 *)CastclassClass((RuntimeObject*)L_7, HtmlNodeNavigator_t2752606360_il2cpp_TypeInfo_var));
		HtmlNodeCollection_t2542734491 * L_8 = V_0;
		HtmlNodeNavigator_t2752606360 * L_9 = V_3;
		NullCheck(L_9);
		HtmlNode_t2048434459 * L_10 = HtmlNodeNavigator_get_CurrentNode_m1848014476(L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		HtmlNodeCollection_Add_m2652620775(L_8, L_10, /*hidden argument*/NULL);
	}

IL_0036:
	{
		XPathNodeIterator_t3192332357 * L_11 = V_2;
		NullCheck(L_11);
		bool L_12 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Xml.XPath.XPathNodeIterator::MoveNext() */, L_11);
		if (L_12)
		{
			goto IL_001e;
		}
	}
	{
		HtmlNodeCollection_t2542734491 * L_13 = V_0;
		NullCheck(L_13);
		int32_t L_14 = HtmlNodeCollection_get_Count_m3249441714(L_13, /*hidden argument*/NULL);
		if (L_14)
		{
			goto IL_0048;
		}
	}
	{
		return (HtmlNodeCollection_t2542734491 *)NULL;
	}

IL_0048:
	{
		HtmlNodeCollection_t2542734491 * L_15 = V_0;
		return L_15;
	}
}
// System.Void HtmlAgilityPack.HtmlNode::.cctor()
extern "C"  void HtmlNode__cctor_m4269639443 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlNode__cctor_m4269639443_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((HtmlNode_t2048434459_StaticFields*)il2cpp_codegen_static_fields_for(HtmlNode_t2048434459_il2cpp_TypeInfo_var))->set_HtmlNodeTypeNameComment_25(_stringLiteral3582779276);
		((HtmlNode_t2048434459_StaticFields*)il2cpp_codegen_static_fields_for(HtmlNode_t2048434459_il2cpp_TypeInfo_var))->set_HtmlNodeTypeNameDocument_26(_stringLiteral2940645458);
		((HtmlNode_t2048434459_StaticFields*)il2cpp_codegen_static_fields_for(HtmlNode_t2048434459_il2cpp_TypeInfo_var))->set_HtmlNodeTypeNameText_27(_stringLiteral3665202948);
		Dictionary_2_t2175053619 * L_0 = (Dictionary_2_t2175053619 *)il2cpp_codegen_object_new(Dictionary_2_t2175053619_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3138784843(L_0, /*hidden argument*/Dictionary_2__ctor_m3138784843_RuntimeMethod_var);
		((HtmlNode_t2048434459_StaticFields*)il2cpp_codegen_static_fields_for(HtmlNode_t2048434459_il2cpp_TypeInfo_var))->set_ElementsFlags_28(L_0);
		Dictionary_2_t2175053619 * L_1 = ((HtmlNode_t2048434459_StaticFields*)il2cpp_codegen_static_fields_for(HtmlNode_t2048434459_il2cpp_TypeInfo_var))->get_ElementsFlags_28();
		NullCheck(L_1);
		Dictionary_2_Add_m722374771(L_1, _stringLiteral1574591451, 1, /*hidden argument*/Dictionary_2_Add_m722374771_RuntimeMethod_var);
		Dictionary_2_t2175053619 * L_2 = ((HtmlNode_t2048434459_StaticFields*)il2cpp_codegen_static_fields_for(HtmlNode_t2048434459_il2cpp_TypeInfo_var))->get_ElementsFlags_28();
		NullCheck(L_2);
		Dictionary_2_Add_m722374771(L_2, _stringLiteral627234459, 1, /*hidden argument*/Dictionary_2_Add_m722374771_RuntimeMethod_var);
		Dictionary_2_t2175053619 * L_3 = ((HtmlNode_t2048434459_StaticFields*)il2cpp_codegen_static_fields_for(HtmlNode_t2048434459_il2cpp_TypeInfo_var))->get_ElementsFlags_28();
		NullCheck(L_3);
		Dictionary_2_Add_m722374771(L_3, _stringLiteral1239101558, 1, /*hidden argument*/Dictionary_2_Add_m722374771_RuntimeMethod_var);
		Dictionary_2_t2175053619 * L_4 = ((HtmlNode_t2048434459_StaticFields*)il2cpp_codegen_static_fields_for(HtmlNode_t2048434459_il2cpp_TypeInfo_var))->get_ElementsFlags_28();
		NullCheck(L_4);
		Dictionary_2_Add_m722374771(L_4, _stringLiteral2328218841, 2, /*hidden argument*/Dictionary_2_Add_m722374771_RuntimeMethod_var);
		Dictionary_2_t2175053619 * L_5 = ((HtmlNode_t2048434459_StaticFields*)il2cpp_codegen_static_fields_for(HtmlNode_t2048434459_il2cpp_TypeInfo_var))->get_ElementsFlags_28();
		NullCheck(L_5);
		Dictionary_2_Add_m722374771(L_5, _stringLiteral325350890, 2, /*hidden argument*/Dictionary_2_Add_m722374771_RuntimeMethod_var);
		Dictionary_2_t2175053619 * L_6 = ((HtmlNode_t2048434459_StaticFields*)il2cpp_codegen_static_fields_for(HtmlNode_t2048434459_il2cpp_TypeInfo_var))->get_ElementsFlags_28();
		NullCheck(L_6);
		Dictionary_2_Add_m722374771(L_6, _stringLiteral3732937281, 2, /*hidden argument*/Dictionary_2_Add_m722374771_RuntimeMethod_var);
		Dictionary_2_t2175053619 * L_7 = ((HtmlNode_t2048434459_StaticFields*)il2cpp_codegen_static_fields_for(HtmlNode_t2048434459_il2cpp_TypeInfo_var))->get_ElementsFlags_28();
		NullCheck(L_7);
		Dictionary_2_Add_m722374771(L_7, _stringLiteral182431658, 2, /*hidden argument*/Dictionary_2_Add_m722374771_RuntimeMethod_var);
		Dictionary_2_t2175053619 * L_8 = ((HtmlNode_t2048434459_StaticFields*)il2cpp_codegen_static_fields_for(HtmlNode_t2048434459_il2cpp_TypeInfo_var))->get_ElementsFlags_28();
		NullCheck(L_8);
		Dictionary_2_Add_m722374771(L_8, _stringLiteral381169824, 2, /*hidden argument*/Dictionary_2_Add_m722374771_RuntimeMethod_var);
		Dictionary_2_t2175053619 * L_9 = ((HtmlNode_t2048434459_StaticFields*)il2cpp_codegen_static_fields_for(HtmlNode_t2048434459_il2cpp_TypeInfo_var))->get_ElementsFlags_28();
		NullCheck(L_9);
		Dictionary_2_Add_m722374771(L_9, _stringLiteral1502598556, 2, /*hidden argument*/Dictionary_2_Add_m722374771_RuntimeMethod_var);
		Dictionary_2_t2175053619 * L_10 = ((HtmlNode_t2048434459_StaticFields*)il2cpp_codegen_static_fields_for(HtmlNode_t2048434459_il2cpp_TypeInfo_var))->get_ElementsFlags_28();
		NullCheck(L_10);
		Dictionary_2_Add_m722374771(L_10, _stringLiteral2665398035, 2, /*hidden argument*/Dictionary_2_Add_m722374771_RuntimeMethod_var);
		Dictionary_2_t2175053619 * L_11 = ((HtmlNode_t2048434459_StaticFields*)il2cpp_codegen_static_fields_for(HtmlNode_t2048434459_il2cpp_TypeInfo_var))->get_ElementsFlags_28();
		NullCheck(L_11);
		Dictionary_2_Add_m722374771(L_11, _stringLiteral190184483, 2, /*hidden argument*/Dictionary_2_Add_m722374771_RuntimeMethod_var);
		Dictionary_2_t2175053619 * L_12 = ((HtmlNode_t2048434459_StaticFields*)il2cpp_codegen_static_fields_for(HtmlNode_t2048434459_il2cpp_TypeInfo_var))->get_ElementsFlags_28();
		NullCheck(L_12);
		Dictionary_2_Add_m722374771(L_12, _stringLiteral2724182727, 2, /*hidden argument*/Dictionary_2_Add_m722374771_RuntimeMethod_var);
		Dictionary_2_t2175053619 * L_13 = ((HtmlNode_t2048434459_StaticFields*)il2cpp_codegen_static_fields_for(HtmlNode_t2048434459_il2cpp_TypeInfo_var))->get_ElementsFlags_28();
		NullCheck(L_13);
		Dictionary_2_Add_m722374771(L_13, _stringLiteral3154582249, 2, /*hidden argument*/Dictionary_2_Add_m722374771_RuntimeMethod_var);
		Dictionary_2_t2175053619 * L_14 = ((HtmlNode_t2048434459_StaticFields*)il2cpp_codegen_static_fields_for(HtmlNode_t2048434459_il2cpp_TypeInfo_var))->get_ElementsFlags_28();
		NullCheck(L_14);
		Dictionary_2_Add_m722374771(L_14, _stringLiteral1099314403, 2, /*hidden argument*/Dictionary_2_Add_m722374771_RuntimeMethod_var);
		Dictionary_2_t2175053619 * L_15 = ((HtmlNode_t2048434459_StaticFields*)il2cpp_codegen_static_fields_for(HtmlNode_t2048434459_il2cpp_TypeInfo_var))->get_ElementsFlags_28();
		NullCheck(L_15);
		Dictionary_2_Add_m722374771(L_15, _stringLiteral798818612, 2, /*hidden argument*/Dictionary_2_Add_m722374771_RuntimeMethod_var);
		Dictionary_2_t2175053619 * L_16 = ((HtmlNode_t2048434459_StaticFields*)il2cpp_codegen_static_fields_for(HtmlNode_t2048434459_il2cpp_TypeInfo_var))->get_ElementsFlags_28();
		NullCheck(L_16);
		Dictionary_2_Add_m722374771(L_16, _stringLiteral529978982, 2, /*hidden argument*/Dictionary_2_Add_m722374771_RuntimeMethod_var);
		Dictionary_2_t2175053619 * L_17 = ((HtmlNode_t2048434459_StaticFields*)il2cpp_codegen_static_fields_for(HtmlNode_t2048434459_il2cpp_TypeInfo_var))->get_ElementsFlags_28();
		NullCheck(L_17);
		Dictionary_2_Add_m722374771(L_17, _stringLiteral2632234911, 2, /*hidden argument*/Dictionary_2_Add_m722374771_RuntimeMethod_var);
		Dictionary_2_t2175053619 * L_18 = ((HtmlNode_t2048434459_StaticFields*)il2cpp_codegen_static_fields_for(HtmlNode_t2048434459_il2cpp_TypeInfo_var))->get_ElementsFlags_28();
		NullCheck(L_18);
		Dictionary_2_Add_m722374771(L_18, _stringLiteral997663887, 2, /*hidden argument*/Dictionary_2_Add_m722374771_RuntimeMethod_var);
		Dictionary_2_t2175053619 * L_19 = ((HtmlNode_t2048434459_StaticFields*)il2cpp_codegen_static_fields_for(HtmlNode_t2048434459_il2cpp_TypeInfo_var))->get_ElementsFlags_28();
		NullCheck(L_19);
		Dictionary_2_Add_m722374771(L_19, _stringLiteral748179678, 2, /*hidden argument*/Dictionary_2_Add_m722374771_RuntimeMethod_var);
		Dictionary_2_t2175053619 * L_20 = ((HtmlNode_t2048434459_StaticFields*)il2cpp_codegen_static_fields_for(HtmlNode_t2048434459_il2cpp_TypeInfo_var))->get_ElementsFlags_28();
		NullCheck(L_20);
		Dictionary_2_Add_m722374771(L_20, _stringLiteral310459614, 2, /*hidden argument*/Dictionary_2_Add_m722374771_RuntimeMethod_var);
		Dictionary_2_t2175053619 * L_21 = ((HtmlNode_t2048434459_StaticFields*)il2cpp_codegen_static_fields_for(HtmlNode_t2048434459_il2cpp_TypeInfo_var))->get_ElementsFlags_28();
		NullCheck(L_21);
		Dictionary_2_Add_m722374771(L_21, _stringLiteral1770475252, ((int32_t)10), /*hidden argument*/Dictionary_2_Add_m722374771_RuntimeMethod_var);
		Dictionary_2_t2175053619 * L_22 = ((HtmlNode_t2048434459_StaticFields*)il2cpp_codegen_static_fields_for(HtmlNode_t2048434459_il2cpp_TypeInfo_var))->get_ElementsFlags_28();
		NullCheck(L_22);
		Dictionary_2_Add_m722374771(L_22, _stringLiteral3910960075, 2, /*hidden argument*/Dictionary_2_Add_m722374771_RuntimeMethod_var);
		Dictionary_2_t2175053619 * L_23 = ((HtmlNode_t2048434459_StaticFields*)il2cpp_codegen_static_fields_for(HtmlNode_t2048434459_il2cpp_TypeInfo_var))->get_ElementsFlags_28();
		NullCheck(L_23);
		Dictionary_2_Add_m722374771(L_23, _stringLiteral381169818, 6, /*hidden argument*/Dictionary_2_Add_m722374771_RuntimeMethod_var);
		Dictionary_2_t2175053619 * L_24 = ((HtmlNode_t2048434459_StaticFields*)il2cpp_codegen_static_fields_for(HtmlNode_t2048434459_il2cpp_TypeInfo_var))->get_ElementsFlags_28();
		NullCheck(L_24);
		Dictionary_2_Add_m722374771(L_24, _stringLiteral372029390, 6, /*hidden argument*/Dictionary_2_Add_m722374771_RuntimeMethod_var);
		return;
	}
}
// System.Void HtmlAgilityPack.HtmlNode::.ctor(HtmlAgilityPack.HtmlNodeType,HtmlAgilityPack.HtmlDocument,System.Int32)
extern "C"  void HtmlNode__ctor_m2584575018 (HtmlNode_t2048434459 * __this, int32_t ___type0, HtmlDocument_t556432108 * ___ownerdocument1, int32_t ___index2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlNode__ctor_m2584575018_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___type0;
		__this->set__nodetype_13(L_0);
		HtmlDocument_t556432108 * L_1 = ___ownerdocument1;
		__this->set__ownerdocument_19(L_1);
		int32_t L_2 = ___index2;
		__this->set__outerstartindex_17(L_2);
		int32_t L_3 = ___type0;
		V_0 = L_3;
		int32_t L_4 = V_0;
		switch (L_4)
		{
			case 0:
			{
				goto IL_0049;
			}
			case 1:
			{
				goto IL_006f;
			}
			case 2:
			{
				goto IL_0035;
			}
			case 3:
			{
				goto IL_005d;
			}
		}
	}
	{
		goto IL_006f;
	}

IL_0035:
	{
		IL2CPP_RUNTIME_CLASS_INIT(HtmlNode_t2048434459_il2cpp_TypeInfo_var);
		String_t* L_5 = ((HtmlNode_t2048434459_StaticFields*)il2cpp_codegen_static_fields_for(HtmlNode_t2048434459_il2cpp_TypeInfo_var))->get_HtmlNodeTypeNameComment_25();
		HtmlNode_set_Name_m2475967416(__this, L_5, /*hidden argument*/NULL);
		__this->set__endnode_2(__this);
		goto IL_006f;
	}

IL_0049:
	{
		IL2CPP_RUNTIME_CLASS_INIT(HtmlNode_t2048434459_il2cpp_TypeInfo_var);
		String_t* L_6 = ((HtmlNode_t2048434459_StaticFields*)il2cpp_codegen_static_fields_for(HtmlNode_t2048434459_il2cpp_TypeInfo_var))->get_HtmlNodeTypeNameDocument_26();
		HtmlNode_set_Name_m2475967416(__this, L_6, /*hidden argument*/NULL);
		__this->set__endnode_2(__this);
		goto IL_006f;
	}

IL_005d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(HtmlNode_t2048434459_il2cpp_TypeInfo_var);
		String_t* L_7 = ((HtmlNode_t2048434459_StaticFields*)il2cpp_codegen_static_fields_for(HtmlNode_t2048434459_il2cpp_TypeInfo_var))->get_HtmlNodeTypeNameText_27();
		HtmlNode_set_Name_m2475967416(__this, L_7, /*hidden argument*/NULL);
		__this->set__endnode_2(__this);
	}

IL_006f:
	{
		HtmlDocument_t556432108 * L_8 = __this->get__ownerdocument_19();
		NullCheck(L_8);
		Dictionary_2_t1056260094 * L_9 = L_8->get_Openednodes_16();
		if (!L_9)
		{
			goto IL_009a;
		}
	}
	{
		bool L_10 = HtmlNode_get_Closed_m650109995(__this, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_009a;
		}
	}
	{
		int32_t L_11 = ___index2;
		if ((((int32_t)(-1)) == ((int32_t)L_11)))
		{
			goto IL_009a;
		}
	}
	{
		HtmlDocument_t556432108 * L_12 = __this->get__ownerdocument_19();
		NullCheck(L_12);
		Dictionary_2_t1056260094 * L_13 = L_12->get_Openednodes_16();
		int32_t L_14 = ___index2;
		NullCheck(L_13);
		Dictionary_2_Add_m1845366966(L_13, L_14, __this, /*hidden argument*/Dictionary_2_Add_m1845366966_RuntimeMethod_var);
	}

IL_009a:
	{
		int32_t L_15 = ___index2;
		if ((!(((uint32_t)(-1)) == ((uint32_t)L_15))))
		{
			goto IL_00a6;
		}
	}
	{
		int32_t L_16 = ___type0;
		if ((((int32_t)L_16) == ((int32_t)2)))
		{
			goto IL_00a6;
		}
	}
	{
		int32_t L_17 = ___type0;
		if ((!(((uint32_t)L_17) == ((uint32_t)3))))
		{
			goto IL_00a7;
		}
	}

IL_00a6:
	{
		return;
	}

IL_00a7:
	{
		__this->set__outerchanged_14((bool)1);
		__this->set__innerchanged_3((bool)1);
		return;
	}
}
// HtmlAgilityPack.HtmlAttributeCollection HtmlAgilityPack.HtmlNode::get_Attributes()
extern "C"  HtmlAttributeCollection_t1787476631 * HtmlNode_get_Attributes_m2935417207 (HtmlNode_t2048434459 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlNode_get_Attributes_m2935417207_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = HtmlNode_get_HasAttributes_m2494625680(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		HtmlAttributeCollection_t1787476631 * L_1 = (HtmlAttributeCollection_t1787476631 *)il2cpp_codegen_object_new(HtmlAttributeCollection_t1787476631_il2cpp_TypeInfo_var);
		HtmlAttributeCollection__ctor_m1353566068(L_1, __this, /*hidden argument*/NULL);
		__this->set__attributes_0(L_1);
	}

IL_0014:
	{
		HtmlAttributeCollection_t1787476631 * L_2 = __this->get__attributes_0();
		return L_2;
	}
}
// HtmlAgilityPack.HtmlNodeCollection HtmlAgilityPack.HtmlNode::get_ChildNodes()
extern "C"  HtmlNodeCollection_t2542734491 * HtmlNode_get_ChildNodes_m1556126725 (HtmlNode_t2048434459 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlNode_get_ChildNodes_m1556126725_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	HtmlNodeCollection_t2542734491 * V_0 = NULL;
	HtmlNodeCollection_t2542734491 * G_B2_0 = NULL;
	HtmlNodeCollection_t2542734491 * G_B1_0 = NULL;
	{
		HtmlNodeCollection_t2542734491 * L_0 = __this->get__childnodes_1();
		HtmlNodeCollection_t2542734491 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_0019;
		}
	}
	{
		HtmlNodeCollection_t2542734491 * L_2 = (HtmlNodeCollection_t2542734491 *)il2cpp_codegen_object_new(HtmlNodeCollection_t2542734491_il2cpp_TypeInfo_var);
		HtmlNodeCollection__ctor_m429744290(L_2, __this, /*hidden argument*/NULL);
		HtmlNodeCollection_t2542734491 * L_3 = L_2;
		V_0 = L_3;
		__this->set__childnodes_1(L_3);
		HtmlNodeCollection_t2542734491 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_0019:
	{
		return G_B2_0;
	}
}
// System.Boolean HtmlAgilityPack.HtmlNode::get_Closed()
extern "C"  bool HtmlNode_get_Closed_m650109995 (HtmlNode_t2048434459 * __this, const RuntimeMethod* method)
{
	{
		HtmlNode_t2048434459 * L_0 = __this->get__endnode_2();
		return (bool)((((int32_t)((((RuntimeObject*)(HtmlNode_t2048434459 *)L_0) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// HtmlAgilityPack.HtmlNode HtmlAgilityPack.HtmlNode::get_FirstChild()
extern "C"  HtmlNode_t2048434459 * HtmlNode_get_FirstChild_m2198425556 (HtmlNode_t2048434459 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = HtmlNode_get_HasChildNodes_m1218535674(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		HtmlNodeCollection_t2542734491 * L_1 = __this->get__childnodes_1();
		NullCheck(L_1);
		HtmlNode_t2048434459 * L_2 = HtmlNodeCollection_get_Item_m2496957206(L_1, 0, /*hidden argument*/NULL);
		return L_2;
	}

IL_0015:
	{
		return (HtmlNode_t2048434459 *)NULL;
	}
}
// System.Boolean HtmlAgilityPack.HtmlNode::get_HasAttributes()
extern "C"  bool HtmlNode_get_HasAttributes_m2494625680 (HtmlNode_t2048434459 * __this, const RuntimeMethod* method)
{
	{
		HtmlAttributeCollection_t1787476631 * L_0 = __this->get__attributes_0();
		if (L_0)
		{
			goto IL_000a;
		}
	}
	{
		return (bool)0;
	}

IL_000a:
	{
		HtmlAttributeCollection_t1787476631 * L_1 = __this->get__attributes_0();
		NullCheck(L_1);
		int32_t L_2 = HtmlAttributeCollection_get_Count_m1097499500(L_1, /*hidden argument*/NULL);
		if ((((int32_t)L_2) > ((int32_t)0)))
		{
			goto IL_001a;
		}
	}
	{
		return (bool)0;
	}

IL_001a:
	{
		return (bool)1;
	}
}
// System.Boolean HtmlAgilityPack.HtmlNode::get_HasChildNodes()
extern "C"  bool HtmlNode_get_HasChildNodes_m1218535674 (HtmlNode_t2048434459 * __this, const RuntimeMethod* method)
{
	{
		HtmlNodeCollection_t2542734491 * L_0 = __this->get__childnodes_1();
		if (L_0)
		{
			goto IL_000a;
		}
	}
	{
		return (bool)0;
	}

IL_000a:
	{
		HtmlNodeCollection_t2542734491 * L_1 = __this->get__childnodes_1();
		NullCheck(L_1);
		int32_t L_2 = HtmlNodeCollection_get_Count_m3249441714(L_1, /*hidden argument*/NULL);
		if ((((int32_t)L_2) > ((int32_t)0)))
		{
			goto IL_001a;
		}
	}
	{
		return (bool)0;
	}

IL_001a:
	{
		return (bool)1;
	}
}
// System.Boolean HtmlAgilityPack.HtmlNode::get_HasClosingAttributes()
extern "C"  bool HtmlNode_get_HasClosingAttributes_m3757206011 (HtmlNode_t2048434459 * __this, const RuntimeMethod* method)
{
	{
		HtmlNode_t2048434459 * L_0 = __this->get__endnode_2();
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		HtmlNode_t2048434459 * L_1 = __this->get__endnode_2();
		if ((!(((RuntimeObject*)(HtmlNode_t2048434459 *)L_1) == ((RuntimeObject*)(HtmlNode_t2048434459 *)__this))))
		{
			goto IL_0013;
		}
	}

IL_0011:
	{
		return (bool)0;
	}

IL_0013:
	{
		HtmlNode_t2048434459 * L_2 = __this->get__endnode_2();
		NullCheck(L_2);
		HtmlAttributeCollection_t1787476631 * L_3 = L_2->get__attributes_0();
		if (L_3)
		{
			goto IL_0022;
		}
	}
	{
		return (bool)0;
	}

IL_0022:
	{
		HtmlNode_t2048434459 * L_4 = __this->get__endnode_2();
		NullCheck(L_4);
		HtmlAttributeCollection_t1787476631 * L_5 = L_4->get__attributes_0();
		NullCheck(L_5);
		int32_t L_6 = HtmlAttributeCollection_get_Count_m1097499500(L_5, /*hidden argument*/NULL);
		if ((((int32_t)L_6) > ((int32_t)0)))
		{
			goto IL_0037;
		}
	}
	{
		return (bool)0;
	}

IL_0037:
	{
		return (bool)1;
	}
}
// System.String HtmlAgilityPack.HtmlNode::get_InnerHtml()
extern "C"  String_t* HtmlNode_get_InnerHtml_m590077535 (HtmlNode_t2048434459 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlNode_get_InnerHtml_m590077535_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get__innerchanged_3();
		if (!L_0)
		{
			goto IL_0022;
		}
	}
	{
		String_t* L_1 = HtmlNode_WriteContentTo_m2088782300(__this, /*hidden argument*/NULL);
		__this->set__innerhtml_4(L_1);
		__this->set__innerchanged_3((bool)0);
		String_t* L_2 = __this->get__innerhtml_4();
		return L_2;
	}

IL_0022:
	{
		String_t* L_3 = __this->get__innerhtml_4();
		if (!L_3)
		{
			goto IL_0031;
		}
	}
	{
		String_t* L_4 = __this->get__innerhtml_4();
		return L_4;
	}

IL_0031:
	{
		int32_t L_5 = __this->get__innerstartindex_6();
		if ((((int32_t)L_5) >= ((int32_t)0)))
		{
			goto IL_0040;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		return L_6;
	}

IL_0040:
	{
		HtmlDocument_t556432108 * L_7 = __this->get__ownerdocument_19();
		NullCheck(L_7);
		String_t* L_8 = L_7->get_Text_22();
		int32_t L_9 = __this->get__innerstartindex_6();
		int32_t L_10 = __this->get__innerlength_5();
		NullCheck(L_8);
		String_t* L_11 = String_Substring_m12482732(L_8, L_9, L_10, /*hidden argument*/NULL);
		return L_11;
	}
}
// System.String HtmlAgilityPack.HtmlNode::get_InnerText()
extern "C"  String_t* HtmlNode_get_InnerText_m2107610091 (HtmlNode_t2048434459 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlNode_get_InnerText_m2107610091_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	HtmlNode_t2048434459 * V_1 = NULL;
	RuntimeObject* V_2 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get__nodetype_13();
		if ((!(((uint32_t)L_0) == ((uint32_t)3))))
		{
			goto IL_0015;
		}
	}
	{
		NullCheck(((HtmlTextNode_t2710098554 *)CastclassClass((RuntimeObject*)__this, HtmlTextNode_t2710098554_il2cpp_TypeInfo_var)));
		String_t* L_1 = HtmlTextNode_get_Text_m1199112604(((HtmlTextNode_t2710098554 *)CastclassClass((RuntimeObject*)__this, HtmlTextNode_t2710098554_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return L_1;
	}

IL_0015:
	{
		int32_t L_2 = __this->get__nodetype_13();
		if ((!(((uint32_t)L_2) == ((uint32_t)2))))
		{
			goto IL_002a;
		}
	}
	{
		NullCheck(((HtmlCommentNode_t1992371332 *)CastclassClass((RuntimeObject*)__this, HtmlCommentNode_t1992371332_il2cpp_TypeInfo_var)));
		String_t* L_3 = HtmlCommentNode_get_Comment_m3847846688(((HtmlCommentNode_t1992371332 *)CastclassClass((RuntimeObject*)__this, HtmlCommentNode_t1992371332_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return L_3;
	}

IL_002a:
	{
		bool L_4 = HtmlNode_get_HasChildNodes_m1218535674(__this, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0038;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		return L_5;
	}

IL_0038:
	{
		V_0 = (String_t*)NULL;
		HtmlNodeCollection_t2542734491 * L_6 = HtmlNode_get_ChildNodes_m1556126725(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		RuntimeObject* L_7 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<HtmlAgilityPack.HtmlNode>::GetEnumerator() */, IEnumerable_1_t2340561504_il2cpp_TypeInfo_var, L_6);
		V_2 = L_7;
	}

IL_0046:
	try
	{ // begin try (depth: 1)
		{
			goto IL_005c;
		}

IL_0048:
		{
			RuntimeObject* L_8 = V_2;
			NullCheck(L_8);
			HtmlNode_t2048434459 * L_9 = InterfaceFuncInvoker0< HtmlNode_t2048434459 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<HtmlAgilityPack.HtmlNode>::get_Current() */, IEnumerator_1_t3818925582_il2cpp_TypeInfo_var, L_8);
			V_1 = L_9;
			String_t* L_10 = V_0;
			HtmlNode_t2048434459 * L_11 = V_1;
			NullCheck(L_11);
			String_t* L_12 = VirtFuncInvoker0< String_t* >::Invoke(5 /* System.String HtmlAgilityPack.HtmlNode::get_InnerText() */, L_11);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_13 = String_Concat_m2596409543(NULL /*static, unused*/, L_10, L_12, /*hidden argument*/NULL);
			V_0 = L_13;
		}

IL_005c:
		{
			RuntimeObject* L_14 = V_2;
			NullCheck(L_14);
			bool L_15 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_14);
			if (L_15)
			{
				goto IL_0048;
			}
		}

IL_0064:
		{
			IL2CPP_LEAVE(0x70, FINALLY_0066);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0066;
	}

FINALLY_0066:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_16 = V_2;
			if (!L_16)
			{
				goto IL_006f;
			}
		}

IL_0069:
		{
			RuntimeObject* L_17 = V_2;
			NullCheck(L_17);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_17);
		}

IL_006f:
		{
			IL2CPP_END_FINALLY(102)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(102)
	{
		IL2CPP_JUMP_TBL(0x70, IL_0070)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0070:
	{
		String_t* L_18 = V_0;
		return L_18;
	}
}
// HtmlAgilityPack.HtmlNode HtmlAgilityPack.HtmlNode::get_LastChild()
extern "C"  HtmlNode_t2048434459 * HtmlNode_get_LastChild_m2408860244 (HtmlNode_t2048434459 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = HtmlNode_get_HasChildNodes_m1218535674(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		HtmlNodeCollection_t2542734491 * L_1 = __this->get__childnodes_1();
		HtmlNodeCollection_t2542734491 * L_2 = __this->get__childnodes_1();
		NullCheck(L_2);
		int32_t L_3 = HtmlNodeCollection_get_Count_m3249441714(L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		HtmlNode_t2048434459 * L_4 = HtmlNodeCollection_get_Item_m2496957206(L_1, ((int32_t)((int32_t)L_3-(int32_t)1)), /*hidden argument*/NULL);
		return L_4;
	}

IL_0021:
	{
		return (HtmlNode_t2048434459 *)NULL;
	}
}
// System.String HtmlAgilityPack.HtmlNode::get_Name()
extern "C"  String_t* HtmlNode_get_Name_m702134575 (HtmlNode_t2048434459 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlNode_get_Name_m702134575_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = __this->get__optimizedName_18();
		if (L_0)
		{
			goto IL_0058;
		}
	}
	{
		String_t* L_1 = __this->get__name_9();
		if (L_1)
		{
			goto IL_0032;
		}
	}
	{
		HtmlDocument_t556432108 * L_2 = __this->get__ownerdocument_19();
		NullCheck(L_2);
		String_t* L_3 = L_2->get_Text_22();
		int32_t L_4 = __this->get__namestartindex_11();
		int32_t L_5 = __this->get__namelength_10();
		NullCheck(L_3);
		String_t* L_6 = String_Substring_m12482732(L_3, L_4, L_5, /*hidden argument*/NULL);
		HtmlNode_set_Name_m2475967416(__this, L_6, /*hidden argument*/NULL);
	}

IL_0032:
	{
		String_t* L_7 = __this->get__name_9();
		if (L_7)
		{
			goto IL_0047;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		__this->set__optimizedName_18(L_8);
		goto IL_0058;
	}

IL_0047:
	{
		String_t* L_9 = __this->get__name_9();
		NullCheck(L_9);
		String_t* L_10 = String_ToLower_m2994460523(L_9, /*hidden argument*/NULL);
		__this->set__optimizedName_18(L_10);
	}

IL_0058:
	{
		String_t* L_11 = __this->get__optimizedName_18();
		return L_11;
	}
}
// System.Void HtmlAgilityPack.HtmlNode::set_Name(System.String)
extern "C"  void HtmlNode_set_Name_m2475967416 (HtmlNode_t2048434459 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set__name_9(L_0);
		__this->set__optimizedName_18((String_t*)NULL);
		return;
	}
}
// HtmlAgilityPack.HtmlNode HtmlAgilityPack.HtmlNode::get_NextSibling()
extern "C"  HtmlNode_t2048434459 * HtmlNode_get_NextSibling_m1378195553 (HtmlNode_t2048434459 * __this, const RuntimeMethod* method)
{
	{
		HtmlNode_t2048434459 * L_0 = __this->get__nextnode_12();
		return L_0;
	}
}
// System.Void HtmlAgilityPack.HtmlNode::set_NextSibling(HtmlAgilityPack.HtmlNode)
extern "C"  void HtmlNode_set_NextSibling_m3096270636 (HtmlNode_t2048434459 * __this, HtmlNode_t2048434459 * ___value0, const RuntimeMethod* method)
{
	{
		HtmlNode_t2048434459 * L_0 = ___value0;
		__this->set__nextnode_12(L_0);
		return;
	}
}
// HtmlAgilityPack.HtmlNodeType HtmlAgilityPack.HtmlNode::get_NodeType()
extern "C"  int32_t HtmlNode_get_NodeType_m270476988 (HtmlNode_t2048434459 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get__nodetype_13();
		return L_0;
	}
}
// System.String HtmlAgilityPack.HtmlNode::get_OriginalName()
extern "C"  String_t* HtmlNode_get_OriginalName_m1151328084 (HtmlNode_t2048434459 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get__name_9();
		return L_0;
	}
}
// System.String HtmlAgilityPack.HtmlNode::get_OuterHtml()
extern "C"  String_t* HtmlNode_get_OuterHtml_m1376347664 (HtmlNode_t2048434459 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlNode_get_OuterHtml_m1376347664_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get__outerchanged_14();
		if (!L_0)
		{
			goto IL_0022;
		}
	}
	{
		String_t* L_1 = HtmlNode_WriteTo_m2203281867(__this, /*hidden argument*/NULL);
		__this->set__outerhtml_15(L_1);
		__this->set__outerchanged_14((bool)0);
		String_t* L_2 = __this->get__outerhtml_15();
		return L_2;
	}

IL_0022:
	{
		String_t* L_3 = __this->get__outerhtml_15();
		if (!L_3)
		{
			goto IL_0031;
		}
	}
	{
		String_t* L_4 = __this->get__outerhtml_15();
		return L_4;
	}

IL_0031:
	{
		int32_t L_5 = __this->get__outerstartindex_17();
		if ((((int32_t)L_5) >= ((int32_t)0)))
		{
			goto IL_0040;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		return L_6;
	}

IL_0040:
	{
		HtmlDocument_t556432108 * L_7 = __this->get__ownerdocument_19();
		NullCheck(L_7);
		String_t* L_8 = L_7->get_Text_22();
		int32_t L_9 = __this->get__outerstartindex_17();
		int32_t L_10 = __this->get__outerlength_16();
		NullCheck(L_8);
		String_t* L_11 = String_Substring_m12482732(L_8, L_9, L_10, /*hidden argument*/NULL);
		return L_11;
	}
}
// HtmlAgilityPack.HtmlDocument HtmlAgilityPack.HtmlNode::get_OwnerDocument()
extern "C"  HtmlDocument_t556432108 * HtmlNode_get_OwnerDocument_m2055696179 (HtmlNode_t2048434459 * __this, const RuntimeMethod* method)
{
	{
		HtmlDocument_t556432108 * L_0 = __this->get__ownerdocument_19();
		return L_0;
	}
}
// HtmlAgilityPack.HtmlNode HtmlAgilityPack.HtmlNode::get_ParentNode()
extern "C"  HtmlNode_t2048434459 * HtmlNode_get_ParentNode_m1888227040 (HtmlNode_t2048434459 * __this, const RuntimeMethod* method)
{
	{
		HtmlNode_t2048434459 * L_0 = __this->get__parentnode_20();
		return L_0;
	}
}
// System.Void HtmlAgilityPack.HtmlNode::set_ParentNode(HtmlAgilityPack.HtmlNode)
extern "C"  void HtmlNode_set_ParentNode_m3958666249 (HtmlNode_t2048434459 * __this, HtmlNode_t2048434459 * ___value0, const RuntimeMethod* method)
{
	{
		HtmlNode_t2048434459 * L_0 = ___value0;
		__this->set__parentnode_20(L_0);
		return;
	}
}
// HtmlAgilityPack.HtmlNode HtmlAgilityPack.HtmlNode::get_PreviousSibling()
extern "C"  HtmlNode_t2048434459 * HtmlNode_get_PreviousSibling_m3912767171 (HtmlNode_t2048434459 * __this, const RuntimeMethod* method)
{
	{
		HtmlNode_t2048434459 * L_0 = __this->get__prevnode_21();
		return L_0;
	}
}
// System.Void HtmlAgilityPack.HtmlNode::set_PreviousSibling(HtmlAgilityPack.HtmlNode)
extern "C"  void HtmlNode_set_PreviousSibling_m3540866112 (HtmlNode_t2048434459 * __this, HtmlNode_t2048434459 * ___value0, const RuntimeMethod* method)
{
	{
		HtmlNode_t2048434459 * L_0 = ___value0;
		__this->set__prevnode_21(L_0);
		return;
	}
}
// System.Boolean HtmlAgilityPack.HtmlNode::CanOverlapElement(System.String)
extern "C"  bool HtmlNode_CanOverlapElement_m1178344375 (RuntimeObject * __this /* static, unused */, String_t* ___name0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlNode_CanOverlapElement_m1178344375_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		String_t* L_0 = ___name0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, _stringLiteral2328218955, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_000e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(HtmlNode_t2048434459_il2cpp_TypeInfo_var);
		Dictionary_2_t2175053619 * L_2 = ((HtmlNode_t2048434459_StaticFields*)il2cpp_codegen_static_fields_for(HtmlNode_t2048434459_il2cpp_TypeInfo_var))->get_ElementsFlags_28();
		String_t* L_3 = ___name0;
		NullCheck(L_3);
		String_t* L_4 = String_ToLower_m2994460523(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		bool L_5 = Dictionary_2_ContainsKey_m2772236242(L_2, L_4, /*hidden argument*/Dictionary_2_ContainsKey_m2772236242_RuntimeMethod_var);
		if (L_5)
		{
			goto IL_0022;
		}
	}
	{
		return (bool)0;
	}

IL_0022:
	{
		IL2CPP_RUNTIME_CLASS_INIT(HtmlNode_t2048434459_il2cpp_TypeInfo_var);
		Dictionary_2_t2175053619 * L_6 = ((HtmlNode_t2048434459_StaticFields*)il2cpp_codegen_static_fields_for(HtmlNode_t2048434459_il2cpp_TypeInfo_var))->get_ElementsFlags_28();
		String_t* L_7 = ___name0;
		NullCheck(L_7);
		String_t* L_8 = String_ToLower_m2994460523(L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		int32_t L_9 = Dictionary_2_get_Item_m1423585693(L_6, L_8, /*hidden argument*/Dictionary_2_get_Item_m1423585693_RuntimeMethod_var);
		V_0 = L_9;
		int32_t L_10 = V_0;
		return (bool)((((int32_t)((((int32_t)((int32_t)((int32_t)L_10&(int32_t)8))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean HtmlAgilityPack.HtmlNode::IsCDataElement(System.String)
extern "C"  bool HtmlNode_IsCDataElement_m1574064561 (RuntimeObject * __this /* static, unused */, String_t* ___name0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlNode_IsCDataElement_m1574064561_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		String_t* L_0 = ___name0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, _stringLiteral2328218955, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_000e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(HtmlNode_t2048434459_il2cpp_TypeInfo_var);
		Dictionary_2_t2175053619 * L_2 = ((HtmlNode_t2048434459_StaticFields*)il2cpp_codegen_static_fields_for(HtmlNode_t2048434459_il2cpp_TypeInfo_var))->get_ElementsFlags_28();
		String_t* L_3 = ___name0;
		NullCheck(L_3);
		String_t* L_4 = String_ToLower_m2994460523(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		bool L_5 = Dictionary_2_ContainsKey_m2772236242(L_2, L_4, /*hidden argument*/Dictionary_2_ContainsKey_m2772236242_RuntimeMethod_var);
		if (L_5)
		{
			goto IL_0022;
		}
	}
	{
		return (bool)0;
	}

IL_0022:
	{
		IL2CPP_RUNTIME_CLASS_INIT(HtmlNode_t2048434459_il2cpp_TypeInfo_var);
		Dictionary_2_t2175053619 * L_6 = ((HtmlNode_t2048434459_StaticFields*)il2cpp_codegen_static_fields_for(HtmlNode_t2048434459_il2cpp_TypeInfo_var))->get_ElementsFlags_28();
		String_t* L_7 = ___name0;
		NullCheck(L_7);
		String_t* L_8 = String_ToLower_m2994460523(L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		int32_t L_9 = Dictionary_2_get_Item_m1423585693(L_6, L_8, /*hidden argument*/Dictionary_2_get_Item_m1423585693_RuntimeMethod_var);
		V_0 = L_9;
		int32_t L_10 = V_0;
		return (bool)((((int32_t)((((int32_t)((int32_t)((int32_t)L_10&(int32_t)1))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean HtmlAgilityPack.HtmlNode::IsClosedElement(System.String)
extern "C"  bool HtmlNode_IsClosedElement_m2492183536 (RuntimeObject * __this /* static, unused */, String_t* ___name0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlNode_IsClosedElement_m2492183536_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		String_t* L_0 = ___name0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, _stringLiteral2328218955, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_000e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(HtmlNode_t2048434459_il2cpp_TypeInfo_var);
		Dictionary_2_t2175053619 * L_2 = ((HtmlNode_t2048434459_StaticFields*)il2cpp_codegen_static_fields_for(HtmlNode_t2048434459_il2cpp_TypeInfo_var))->get_ElementsFlags_28();
		String_t* L_3 = ___name0;
		NullCheck(L_3);
		String_t* L_4 = String_ToLower_m2994460523(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		bool L_5 = Dictionary_2_ContainsKey_m2772236242(L_2, L_4, /*hidden argument*/Dictionary_2_ContainsKey_m2772236242_RuntimeMethod_var);
		if (L_5)
		{
			goto IL_0022;
		}
	}
	{
		return (bool)0;
	}

IL_0022:
	{
		IL2CPP_RUNTIME_CLASS_INIT(HtmlNode_t2048434459_il2cpp_TypeInfo_var);
		Dictionary_2_t2175053619 * L_6 = ((HtmlNode_t2048434459_StaticFields*)il2cpp_codegen_static_fields_for(HtmlNode_t2048434459_il2cpp_TypeInfo_var))->get_ElementsFlags_28();
		String_t* L_7 = ___name0;
		NullCheck(L_7);
		String_t* L_8 = String_ToLower_m2994460523(L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		int32_t L_9 = Dictionary_2_get_Item_m1423585693(L_6, L_8, /*hidden argument*/Dictionary_2_get_Item_m1423585693_RuntimeMethod_var);
		V_0 = L_9;
		int32_t L_10 = V_0;
		return (bool)((((int32_t)((((int32_t)((int32_t)((int32_t)L_10&(int32_t)4))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean HtmlAgilityPack.HtmlNode::IsEmptyElement(System.String)
extern "C"  bool HtmlNode_IsEmptyElement_m2918938855 (RuntimeObject * __this /* static, unused */, String_t* ___name0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlNode_IsEmptyElement_m2918938855_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		String_t* L_0 = ___name0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, _stringLiteral2328218955, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_000e:
	{
		String_t* L_2 = ___name0;
		NullCheck(L_2);
		int32_t L_3 = String_get_Length_m1606060069(L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0018;
		}
	}
	{
		return (bool)1;
	}

IL_0018:
	{
		String_t* L_4 = ___name0;
		NullCheck(L_4);
		Il2CppChar L_5 = String_get_Chars_m4230566705(L_4, 0, /*hidden argument*/NULL);
		if ((!(((uint32_t)((int32_t)33)) == ((uint32_t)L_5))))
		{
			goto IL_0025;
		}
	}
	{
		return (bool)1;
	}

IL_0025:
	{
		String_t* L_6 = ___name0;
		NullCheck(L_6);
		Il2CppChar L_7 = String_get_Chars_m4230566705(L_6, 0, /*hidden argument*/NULL);
		if ((!(((uint32_t)((int32_t)63)) == ((uint32_t)L_7))))
		{
			goto IL_0032;
		}
	}
	{
		return (bool)1;
	}

IL_0032:
	{
		IL2CPP_RUNTIME_CLASS_INIT(HtmlNode_t2048434459_il2cpp_TypeInfo_var);
		Dictionary_2_t2175053619 * L_8 = ((HtmlNode_t2048434459_StaticFields*)il2cpp_codegen_static_fields_for(HtmlNode_t2048434459_il2cpp_TypeInfo_var))->get_ElementsFlags_28();
		String_t* L_9 = ___name0;
		NullCheck(L_9);
		String_t* L_10 = String_ToLower_m2994460523(L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		bool L_11 = Dictionary_2_ContainsKey_m2772236242(L_8, L_10, /*hidden argument*/Dictionary_2_ContainsKey_m2772236242_RuntimeMethod_var);
		if (L_11)
		{
			goto IL_0046;
		}
	}
	{
		return (bool)0;
	}

IL_0046:
	{
		IL2CPP_RUNTIME_CLASS_INIT(HtmlNode_t2048434459_il2cpp_TypeInfo_var);
		Dictionary_2_t2175053619 * L_12 = ((HtmlNode_t2048434459_StaticFields*)il2cpp_codegen_static_fields_for(HtmlNode_t2048434459_il2cpp_TypeInfo_var))->get_ElementsFlags_28();
		String_t* L_13 = ___name0;
		NullCheck(L_13);
		String_t* L_14 = String_ToLower_m2994460523(L_13, /*hidden argument*/NULL);
		NullCheck(L_12);
		int32_t L_15 = Dictionary_2_get_Item_m1423585693(L_12, L_14, /*hidden argument*/Dictionary_2_get_Item_m1423585693_RuntimeMethod_var);
		V_0 = L_15;
		int32_t L_16 = V_0;
		return (bool)((((int32_t)((((int32_t)((int32_t)((int32_t)L_16&(int32_t)2))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// HtmlAgilityPack.HtmlNode HtmlAgilityPack.HtmlNode::AppendChild(HtmlAgilityPack.HtmlNode)
extern "C"  HtmlNode_t2048434459 * HtmlNode_AppendChild_m2583227907 (HtmlNode_t2048434459 * __this, HtmlNode_t2048434459 * ___newChild0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlNode_AppendChild_m2583227907_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		HtmlNode_t2048434459 * L_0 = ___newChild0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, _stringLiteral3533616638, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_000e:
	{
		HtmlNodeCollection_t2542734491 * L_2 = HtmlNode_get_ChildNodes_m1556126725(__this, /*hidden argument*/NULL);
		HtmlNode_t2048434459 * L_3 = ___newChild0;
		NullCheck(L_2);
		HtmlNodeCollection_Append_m717126292(L_2, L_3, /*hidden argument*/NULL);
		HtmlDocument_t556432108 * L_4 = __this->get__ownerdocument_19();
		HtmlNode_t2048434459 * L_5 = ___newChild0;
		HtmlNode_t2048434459 * L_6 = ___newChild0;
		NullCheck(L_6);
		String_t* L_7 = HtmlNode_GetId_m3597009408(L_6, /*hidden argument*/NULL);
		NullCheck(L_4);
		HtmlDocument_SetIdForNode_m403125345(L_4, L_5, L_7, /*hidden argument*/NULL);
		__this->set__outerchanged_14((bool)1);
		__this->set__innerchanged_3((bool)1);
		HtmlNode_t2048434459 * L_8 = ___newChild0;
		return L_8;
	}
}
// HtmlAgilityPack.HtmlNode HtmlAgilityPack.HtmlNode::Clone()
extern "C"  HtmlNode_t2048434459 * HtmlNode_Clone_m1529145592 (HtmlNode_t2048434459 * __this, const RuntimeMethod* method)
{
	{
		HtmlNode_t2048434459 * L_0 = HtmlNode_CloneNode_m2027347983(__this, (bool)1, /*hidden argument*/NULL);
		return L_0;
	}
}
// HtmlAgilityPack.HtmlNode HtmlAgilityPack.HtmlNode::CloneNode(System.Boolean)
extern "C"  HtmlNode_t2048434459 * HtmlNode_CloneNode_m2027347983 (HtmlNode_t2048434459 * __this, bool ___deep0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlNode_CloneNode_m2027347983_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	HtmlNode_t2048434459 * V_0 = NULL;
	HtmlAttribute_t1804523403 * V_1 = NULL;
	HtmlAttribute_t1804523403 * V_2 = NULL;
	HtmlAttribute_t1804523403 * V_3 = NULL;
	HtmlAttribute_t1804523403 * V_4 = NULL;
	HtmlNode_t2048434459 * V_5 = NULL;
	HtmlNode_t2048434459 * V_6 = NULL;
	int32_t V_7 = 0;
	RuntimeObject* V_8 = NULL;
	RuntimeObject* V_9 = NULL;
	RuntimeObject* V_10 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		HtmlDocument_t556432108 * L_0 = __this->get__ownerdocument_19();
		int32_t L_1 = __this->get__nodetype_13();
		NullCheck(L_0);
		HtmlNode_t2048434459 * L_2 = HtmlDocument_CreateNode_m690978292(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		HtmlNode_t2048434459 * L_3 = V_0;
		String_t* L_4 = HtmlNode_get_Name_m702134575(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		HtmlNode_set_Name_m2475967416(L_3, L_4, /*hidden argument*/NULL);
		int32_t L_5 = __this->get__nodetype_13();
		V_7 = L_5;
		int32_t L_6 = V_7;
		switch (((int32_t)((int32_t)L_6-(int32_t)2)))
		{
			case 0:
			{
				goto IL_0039;
			}
			case 1:
			{
				goto IL_0051;
			}
		}
	}
	{
		goto IL_0069;
	}

IL_0039:
	{
		HtmlNode_t2048434459 * L_7 = V_0;
		NullCheck(((HtmlCommentNode_t1992371332 *)CastclassClass((RuntimeObject*)__this, HtmlCommentNode_t1992371332_il2cpp_TypeInfo_var)));
		String_t* L_8 = HtmlCommentNode_get_Comment_m3847846688(((HtmlCommentNode_t1992371332 *)CastclassClass((RuntimeObject*)__this, HtmlCommentNode_t1992371332_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		NullCheck(((HtmlCommentNode_t1992371332 *)CastclassClass((RuntimeObject*)L_7, HtmlCommentNode_t1992371332_il2cpp_TypeInfo_var)));
		HtmlCommentNode_set_Comment_m4142312819(((HtmlCommentNode_t1992371332 *)CastclassClass((RuntimeObject*)L_7, HtmlCommentNode_t1992371332_il2cpp_TypeInfo_var)), L_8, /*hidden argument*/NULL);
		HtmlNode_t2048434459 * L_9 = V_0;
		return L_9;
	}

IL_0051:
	{
		HtmlNode_t2048434459 * L_10 = V_0;
		NullCheck(((HtmlTextNode_t2710098554 *)CastclassClass((RuntimeObject*)__this, HtmlTextNode_t2710098554_il2cpp_TypeInfo_var)));
		String_t* L_11 = HtmlTextNode_get_Text_m1199112604(((HtmlTextNode_t2710098554 *)CastclassClass((RuntimeObject*)__this, HtmlTextNode_t2710098554_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		NullCheck(((HtmlTextNode_t2710098554 *)CastclassClass((RuntimeObject*)L_10, HtmlTextNode_t2710098554_il2cpp_TypeInfo_var)));
		HtmlTextNode_set_Text_m279687427(((HtmlTextNode_t2710098554 *)CastclassClass((RuntimeObject*)L_10, HtmlTextNode_t2710098554_il2cpp_TypeInfo_var)), L_11, /*hidden argument*/NULL);
		HtmlNode_t2048434459 * L_12 = V_0;
		return L_12;
	}

IL_0069:
	{
		bool L_13 = HtmlNode_get_HasAttributes_m2494625680(__this, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_00b3;
		}
	}
	{
		HtmlAttributeCollection_t1787476631 * L_14 = __this->get__attributes_0();
		NullCheck(L_14);
		RuntimeObject* L_15 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<HtmlAgilityPack.HtmlAttribute>::GetEnumerator() */, IEnumerable_1_t2096650448_il2cpp_TypeInfo_var, L_14);
		V_8 = L_15;
	}

IL_007e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_009c;
		}

IL_0080:
		{
			RuntimeObject* L_16 = V_8;
			NullCheck(L_16);
			HtmlAttribute_t1804523403 * L_17 = InterfaceFuncInvoker0< HtmlAttribute_t1804523403 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<HtmlAgilityPack.HtmlAttribute>::get_Current() */, IEnumerator_1_t3575014526_il2cpp_TypeInfo_var, L_16);
			V_1 = L_17;
			HtmlAttribute_t1804523403 * L_18 = V_1;
			NullCheck(L_18);
			HtmlAttribute_t1804523403 * L_19 = HtmlAttribute_Clone_m1011466360(L_18, /*hidden argument*/NULL);
			V_2 = L_19;
			HtmlNode_t2048434459 * L_20 = V_0;
			NullCheck(L_20);
			HtmlAttributeCollection_t1787476631 * L_21 = HtmlNode_get_Attributes_m2935417207(L_20, /*hidden argument*/NULL);
			HtmlAttribute_t1804523403 * L_22 = V_2;
			NullCheck(L_21);
			HtmlAttributeCollection_Append_m138838623(L_21, L_22, /*hidden argument*/NULL);
		}

IL_009c:
		{
			RuntimeObject* L_23 = V_8;
			NullCheck(L_23);
			bool L_24 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_23);
			if (L_24)
			{
				goto IL_0080;
			}
		}

IL_00a5:
		{
			IL2CPP_LEAVE(0xB3, FINALLY_00a7);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00a7;
	}

FINALLY_00a7:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_25 = V_8;
			if (!L_25)
			{
				goto IL_00b2;
			}
		}

IL_00ab:
		{
			RuntimeObject* L_26 = V_8;
			NullCheck(L_26);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_26);
		}

IL_00b2:
		{
			IL2CPP_END_FINALLY(167)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(167)
	{
		IL2CPP_JUMP_TBL(0xB3, IL_00b3)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00b3:
	{
		bool L_27 = HtmlNode_get_HasClosingAttributes_m3757206011(__this, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_011b;
		}
	}
	{
		HtmlNode_t2048434459 * L_28 = V_0;
		HtmlNode_t2048434459 * L_29 = __this->get__endnode_2();
		NullCheck(L_29);
		HtmlNode_t2048434459 * L_30 = HtmlNode_CloneNode_m2027347983(L_29, (bool)0, /*hidden argument*/NULL);
		NullCheck(L_28);
		L_28->set__endnode_2(L_30);
		HtmlNode_t2048434459 * L_31 = __this->get__endnode_2();
		NullCheck(L_31);
		HtmlAttributeCollection_t1787476631 * L_32 = L_31->get__attributes_0();
		NullCheck(L_32);
		RuntimeObject* L_33 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<HtmlAgilityPack.HtmlAttribute>::GetEnumerator() */, IEnumerable_1_t2096650448_il2cpp_TypeInfo_var, L_32);
		V_9 = L_33;
	}

IL_00df:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0104;
		}

IL_00e1:
		{
			RuntimeObject* L_34 = V_9;
			NullCheck(L_34);
			HtmlAttribute_t1804523403 * L_35 = InterfaceFuncInvoker0< HtmlAttribute_t1804523403 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<HtmlAgilityPack.HtmlAttribute>::get_Current() */, IEnumerator_1_t3575014526_il2cpp_TypeInfo_var, L_34);
			V_3 = L_35;
			HtmlAttribute_t1804523403 * L_36 = V_3;
			NullCheck(L_36);
			HtmlAttribute_t1804523403 * L_37 = HtmlAttribute_Clone_m1011466360(L_36, /*hidden argument*/NULL);
			V_4 = L_37;
			HtmlNode_t2048434459 * L_38 = V_0;
			NullCheck(L_38);
			HtmlNode_t2048434459 * L_39 = L_38->get__endnode_2();
			NullCheck(L_39);
			HtmlAttributeCollection_t1787476631 * L_40 = L_39->get__attributes_0();
			HtmlAttribute_t1804523403 * L_41 = V_4;
			NullCheck(L_40);
			HtmlAttributeCollection_Append_m138838623(L_40, L_41, /*hidden argument*/NULL);
		}

IL_0104:
		{
			RuntimeObject* L_42 = V_9;
			NullCheck(L_42);
			bool L_43 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_42);
			if (L_43)
			{
				goto IL_00e1;
			}
		}

IL_010d:
		{
			IL2CPP_LEAVE(0x11B, FINALLY_010f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_010f;
	}

FINALLY_010f:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_44 = V_9;
			if (!L_44)
			{
				goto IL_011a;
			}
		}

IL_0113:
		{
			RuntimeObject* L_45 = V_9;
			NullCheck(L_45);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_45);
		}

IL_011a:
		{
			IL2CPP_END_FINALLY(271)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(271)
	{
		IL2CPP_JUMP_TBL(0x11B, IL_011b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_011b:
	{
		bool L_46 = ___deep0;
		if (L_46)
		{
			goto IL_0120;
		}
	}
	{
		HtmlNode_t2048434459 * L_47 = V_0;
		return L_47;
	}

IL_0120:
	{
		bool L_48 = HtmlNode_get_HasChildNodes_m1218535674(__this, /*hidden argument*/NULL);
		if (L_48)
		{
			goto IL_012a;
		}
	}
	{
		HtmlNode_t2048434459 * L_49 = V_0;
		return L_49;
	}

IL_012a:
	{
		HtmlNodeCollection_t2542734491 * L_50 = __this->get__childnodes_1();
		NullCheck(L_50);
		RuntimeObject* L_51 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<HtmlAgilityPack.HtmlNode>::GetEnumerator() */, IEnumerable_1_t2340561504_il2cpp_TypeInfo_var, L_50);
		V_10 = L_51;
	}

IL_0137:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0154;
		}

IL_0139:
		{
			RuntimeObject* L_52 = V_10;
			NullCheck(L_52);
			HtmlNode_t2048434459 * L_53 = InterfaceFuncInvoker0< HtmlNode_t2048434459 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<HtmlAgilityPack.HtmlNode>::get_Current() */, IEnumerator_1_t3818925582_il2cpp_TypeInfo_var, L_52);
			V_5 = L_53;
			HtmlNode_t2048434459 * L_54 = V_5;
			NullCheck(L_54);
			HtmlNode_t2048434459 * L_55 = HtmlNode_Clone_m1529145592(L_54, /*hidden argument*/NULL);
			V_6 = L_55;
			HtmlNode_t2048434459 * L_56 = V_0;
			HtmlNode_t2048434459 * L_57 = V_6;
			NullCheck(L_56);
			HtmlNode_AppendChild_m2583227907(L_56, L_57, /*hidden argument*/NULL);
		}

IL_0154:
		{
			RuntimeObject* L_58 = V_10;
			NullCheck(L_58);
			bool L_59 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_58);
			if (L_59)
			{
				goto IL_0139;
			}
		}

IL_015d:
		{
			IL2CPP_LEAVE(0x16B, FINALLY_015f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_015f;
	}

FINALLY_015f:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_60 = V_10;
			if (!L_60)
			{
				goto IL_016a;
			}
		}

IL_0163:
		{
			RuntimeObject* L_61 = V_10;
			NullCheck(L_61);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_61);
		}

IL_016a:
		{
			IL2CPP_END_FINALLY(351)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(351)
	{
		IL2CPP_JUMP_TBL(0x16B, IL_016b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_016b:
	{
		HtmlNode_t2048434459 * L_62 = V_0;
		return L_62;
	}
}
// HtmlAgilityPack.HtmlNode HtmlAgilityPack.HtmlNode::RemoveChild(HtmlAgilityPack.HtmlNode)
extern "C"  HtmlNode_t2048434459 * HtmlNode_RemoveChild_m1945208939 (HtmlNode_t2048434459 * __this, HtmlNode_t2048434459 * ___oldChild0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlNode_RemoveChild_m1945208939_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		HtmlNode_t2048434459 * L_0 = ___oldChild0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, _stringLiteral4234618797, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_000e:
	{
		V_0 = (-1);
		HtmlNodeCollection_t2542734491 * L_2 = __this->get__childnodes_1();
		if (!L_2)
		{
			goto IL_0025;
		}
	}
	{
		HtmlNodeCollection_t2542734491 * L_3 = __this->get__childnodes_1();
		HtmlNode_t2048434459 * L_4 = ___oldChild0;
		NullCheck(L_3);
		int32_t L_5 = HtmlNodeCollection_get_Item_m1795494390(L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
	}

IL_0025:
	{
		int32_t L_6 = V_0;
		if ((!(((uint32_t)L_6) == ((uint32_t)(-1)))))
		{
			goto IL_0034;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(HtmlDocument_t556432108_il2cpp_TypeInfo_var);
		String_t* L_7 = ((HtmlDocument_t556432108_StaticFields*)il2cpp_codegen_static_fields_for(HtmlDocument_t556432108_il2cpp_TypeInfo_var))->get_HtmlExceptionRefNotChild_39();
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_8, L_7, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_0034:
	{
		HtmlNodeCollection_t2542734491 * L_9 = __this->get__childnodes_1();
		if (!L_9)
		{
			goto IL_0049;
		}
	}
	{
		HtmlNodeCollection_t2542734491 * L_10 = __this->get__childnodes_1();
		int32_t L_11 = V_0;
		NullCheck(L_10);
		HtmlNodeCollection_Remove_m46965641(L_10, L_11, /*hidden argument*/NULL);
	}

IL_0049:
	{
		HtmlDocument_t556432108 * L_12 = __this->get__ownerdocument_19();
		HtmlNode_t2048434459 * L_13 = ___oldChild0;
		NullCheck(L_13);
		String_t* L_14 = HtmlNode_GetId_m3597009408(L_13, /*hidden argument*/NULL);
		NullCheck(L_12);
		HtmlDocument_SetIdForNode_m403125345(L_12, (HtmlNode_t2048434459 *)NULL, L_14, /*hidden argument*/NULL);
		__this->set__outerchanged_14((bool)1);
		__this->set__innerchanged_3((bool)1);
		HtmlNode_t2048434459 * L_15 = ___oldChild0;
		return L_15;
	}
}
// System.Void HtmlAgilityPack.HtmlNode::WriteContentTo(System.IO.TextWriter)
extern "C"  void HtmlNode_WriteContentTo_m83799726 (HtmlNode_t2048434459 * __this, TextWriter_t4027217640 * ___outText0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlNode_WriteContentTo_m83799726_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	HtmlNode_t2048434459 * V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		HtmlNodeCollection_t2542734491 * L_0 = __this->get__childnodes_1();
		if (L_0)
		{
			goto IL_0009;
		}
	}
	{
		return;
	}

IL_0009:
	{
		HtmlNodeCollection_t2542734491 * L_1 = __this->get__childnodes_1();
		NullCheck(L_1);
		RuntimeObject* L_2 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<HtmlAgilityPack.HtmlNode>::GetEnumerator() */, IEnumerable_1_t2340561504_il2cpp_TypeInfo_var, L_1);
		V_1 = L_2;
	}

IL_0015:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0025;
		}

IL_0017:
		{
			RuntimeObject* L_3 = V_1;
			NullCheck(L_3);
			HtmlNode_t2048434459 * L_4 = InterfaceFuncInvoker0< HtmlNode_t2048434459 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<HtmlAgilityPack.HtmlNode>::get_Current() */, IEnumerator_1_t3818925582_il2cpp_TypeInfo_var, L_3);
			V_0 = L_4;
			HtmlNode_t2048434459 * L_5 = V_0;
			TextWriter_t4027217640 * L_6 = ___outText0;
			NullCheck(L_5);
			HtmlNode_WriteTo_m15807155(L_5, L_6, /*hidden argument*/NULL);
		}

IL_0025:
		{
			RuntimeObject* L_7 = V_1;
			NullCheck(L_7);
			bool L_8 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_7);
			if (L_8)
			{
				goto IL_0017;
			}
		}

IL_002d:
		{
			IL2CPP_LEAVE(0x39, FINALLY_002f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_002f;
	}

FINALLY_002f:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_9 = V_1;
			if (!L_9)
			{
				goto IL_0038;
			}
		}

IL_0032:
		{
			RuntimeObject* L_10 = V_1;
			NullCheck(L_10);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_10);
		}

IL_0038:
		{
			IL2CPP_END_FINALLY(47)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(47)
	{
		IL2CPP_JUMP_TBL(0x39, IL_0039)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0039:
	{
		return;
	}
}
// System.String HtmlAgilityPack.HtmlNode::WriteContentTo()
extern "C"  String_t* HtmlNode_WriteContentTo_m2088782300 (HtmlNode_t2048434459 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlNode_WriteContentTo_m2088782300_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringWriter_t4139609088 * V_0 = NULL;
	{
		StringWriter_t4139609088 * L_0 = (StringWriter_t4139609088 *)il2cpp_codegen_object_new(StringWriter_t4139609088_il2cpp_TypeInfo_var);
		StringWriter__ctor_m59456937(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		StringWriter_t4139609088 * L_1 = V_0;
		HtmlNode_WriteContentTo_m83799726(__this, L_1, /*hidden argument*/NULL);
		StringWriter_t4139609088 * L_2 = V_0;
		NullCheck(L_2);
		VirtActionInvoker0::Invoke(7 /* System.Void System.IO.TextWriter::Flush() */, L_2);
		StringWriter_t4139609088 * L_3 = V_0;
		NullCheck(L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_3);
		return L_4;
	}
}
// System.Void HtmlAgilityPack.HtmlNode::WriteTo(System.IO.TextWriter)
extern "C"  void HtmlNode_WriteTo_m15807155 (HtmlNode_t2048434459 * __this, TextWriter_t4027217640 * ___outText0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlNode_WriteTo_m15807155_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	HtmlNode_t2048434459 * V_2 = NULL;
	String_t* V_3 = NULL;
	bool V_4 = false;
	int32_t V_5 = 0;
	TextWriter_t4027217640 * G_B17_0 = NULL;
	TextWriter_t4027217640 * G_B16_0 = NULL;
	String_t* G_B18_0 = NULL;
	TextWriter_t4027217640 * G_B18_1 = NULL;
	String_t* G_B22_0 = NULL;
	{
		int32_t L_0 = __this->get__nodetype_13();
		V_5 = L_0;
		int32_t L_1 = V_5;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0062;
			}
			case 1:
			{
				goto IL_0154;
			}
			case 2:
			{
				goto IL_0020;
			}
			case 3:
			{
				goto IL_012b;
			}
		}
	}
	{
		return;
	}

IL_0020:
	{
		NullCheck(((HtmlCommentNode_t1992371332 *)CastclassClass((RuntimeObject*)__this, HtmlCommentNode_t1992371332_il2cpp_TypeInfo_var)));
		String_t* L_2 = HtmlCommentNode_get_Comment_m3847846688(((HtmlCommentNode_t1992371332 *)CastclassClass((RuntimeObject*)__this, HtmlCommentNode_t1992371332_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		V_0 = L_2;
		HtmlDocument_t556432108 * L_3 = __this->get__ownerdocument_19();
		NullCheck(L_3);
		bool L_4 = L_3->get_OptionOutputAsXml_31();
		if (!L_4)
		{
			goto IL_005a;
		}
	}
	{
		TextWriter_t4027217640 * L_5 = ___outText0;
		IL2CPP_RUNTIME_CLASS_INIT(HtmlNode_t2048434459_il2cpp_TypeInfo_var);
		String_t* L_6 = HtmlNode_GetXmlComment_m2603063516(NULL /*static, unused*/, ((HtmlCommentNode_t1992371332 *)CastclassClass((RuntimeObject*)__this, HtmlCommentNode_t1992371332_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral2972645825, L_6, _stringLiteral452181462, /*hidden argument*/NULL);
		NullCheck(L_5);
		VirtActionInvoker1< String_t* >::Invoke(11 /* System.Void System.IO.TextWriter::Write(System.String) */, L_5, L_7);
		return;
	}

IL_005a:
	{
		TextWriter_t4027217640 * L_8 = ___outText0;
		String_t* L_9 = V_0;
		NullCheck(L_8);
		VirtActionInvoker1< String_t* >::Invoke(11 /* System.Void System.IO.TextWriter::Write(System.String) */, L_8, L_9);
		return;
	}

IL_0062:
	{
		HtmlDocument_t556432108 * L_10 = __this->get__ownerdocument_19();
		NullCheck(L_10);
		bool L_11 = L_10->get_OptionOutputAsXml_31();
		if (!L_11)
		{
			goto IL_0123;
		}
	}
	{
		TextWriter_t4027217640 * L_12 = ___outText0;
		HtmlDocument_t556432108 * L_13 = __this->get__ownerdocument_19();
		NullCheck(L_13);
		Encoding_t663144255 * L_14 = HtmlDocument_GetOutEncoding_m2726542450(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		String_t* L_15 = VirtFuncInvoker0< String_t* >::Invoke(23 /* System.String System.Text.Encoding::get_BodyName() */, L_14);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral2722536972, L_15, _stringLiteral3639978523, /*hidden argument*/NULL);
		NullCheck(L_12);
		VirtActionInvoker1< String_t* >::Invoke(11 /* System.Void System.IO.TextWriter::Write(System.String) */, L_12, L_16);
		HtmlDocument_t556432108 * L_17 = __this->get__ownerdocument_19();
		NullCheck(L_17);
		HtmlNode_t2048434459 * L_18 = HtmlDocument_get_DocumentNode_m1723003156(L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		bool L_19 = HtmlNode_get_HasChildNodes_m1218535674(L_18, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_0123;
		}
	}
	{
		HtmlDocument_t556432108 * L_20 = __this->get__ownerdocument_19();
		NullCheck(L_20);
		HtmlNode_t2048434459 * L_21 = HtmlDocument_get_DocumentNode_m1723003156(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		HtmlNodeCollection_t2542734491 * L_22 = L_21->get__childnodes_1();
		NullCheck(L_22);
		int32_t L_23 = HtmlNodeCollection_get_Count_m3249441714(L_22, /*hidden argument*/NULL);
		V_1 = L_23;
		int32_t L_24 = V_1;
		if ((((int32_t)L_24) <= ((int32_t)0)))
		{
			goto IL_0123;
		}
	}
	{
		HtmlDocument_t556432108 * L_25 = __this->get__ownerdocument_19();
		NullCheck(L_25);
		HtmlNode_t2048434459 * L_26 = HtmlDocument_GetXmlDeclaration_m3305216451(L_25, /*hidden argument*/NULL);
		V_2 = L_26;
		HtmlNode_t2048434459 * L_27 = V_2;
		if (!L_27)
		{
			goto IL_00d6;
		}
	}
	{
		int32_t L_28 = V_1;
		V_1 = ((int32_t)((int32_t)L_28-(int32_t)1));
	}

IL_00d6:
	{
		int32_t L_29 = V_1;
		if ((((int32_t)L_29) <= ((int32_t)1)))
		{
			goto IL_0123;
		}
	}
	{
		HtmlDocument_t556432108 * L_30 = __this->get__ownerdocument_19();
		NullCheck(L_30);
		bool L_31 = L_30->get_OptionOutputUpperCase_34();
		if (!L_31)
		{
			goto IL_0105;
		}
	}
	{
		TextWriter_t4027217640 * L_32 = ___outText0;
		NullCheck(L_32);
		VirtActionInvoker1< String_t* >::Invoke(11 /* System.Void System.IO.TextWriter::Write(System.String) */, L_32, _stringLiteral4162708532);
		TextWriter_t4027217640 * L_33 = ___outText0;
		HtmlNode_WriteContentTo_m83799726(__this, L_33, /*hidden argument*/NULL);
		TextWriter_t4027217640 * L_34 = ___outText0;
		NullCheck(L_34);
		VirtActionInvoker1< String_t* >::Invoke(11 /* System.Void System.IO.TextWriter::Write(System.String) */, L_34, _stringLiteral2020566873);
		return;
	}

IL_0105:
	{
		TextWriter_t4027217640 * L_35 = ___outText0;
		NullCheck(L_35);
		VirtActionInvoker1< String_t* >::Invoke(11 /* System.Void System.IO.TextWriter::Write(System.String) */, L_35, _stringLiteral1346227060);
		TextWriter_t4027217640 * L_36 = ___outText0;
		HtmlNode_WriteContentTo_m83799726(__this, L_36, /*hidden argument*/NULL);
		TextWriter_t4027217640 * L_37 = ___outText0;
		NullCheck(L_37);
		VirtActionInvoker1< String_t* >::Invoke(11 /* System.Void System.IO.TextWriter::Write(System.String) */, L_37, _stringLiteral3667687257);
		return;
	}

IL_0123:
	{
		TextWriter_t4027217640 * L_38 = ___outText0;
		HtmlNode_WriteContentTo_m83799726(__this, L_38, /*hidden argument*/NULL);
		return;
	}

IL_012b:
	{
		NullCheck(((HtmlTextNode_t2710098554 *)CastclassClass((RuntimeObject*)__this, HtmlTextNode_t2710098554_il2cpp_TypeInfo_var)));
		String_t* L_39 = HtmlTextNode_get_Text_m1199112604(((HtmlTextNode_t2710098554 *)CastclassClass((RuntimeObject*)__this, HtmlTextNode_t2710098554_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		V_0 = L_39;
		TextWriter_t4027217640 * L_40 = ___outText0;
		HtmlDocument_t556432108 * L_41 = __this->get__ownerdocument_19();
		NullCheck(L_41);
		bool L_42 = L_41->get_OptionOutputAsXml_31();
		G_B16_0 = L_40;
		if (L_42)
		{
			G_B17_0 = L_40;
			goto IL_0148;
		}
	}
	{
		String_t* L_43 = V_0;
		G_B18_0 = L_43;
		G_B18_1 = G_B16_0;
		goto IL_014e;
	}

IL_0148:
	{
		String_t* L_44 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(HtmlDocument_t556432108_il2cpp_TypeInfo_var);
		String_t* L_45 = HtmlDocument_HtmlEncode_m2155231231(NULL /*static, unused*/, L_44, /*hidden argument*/NULL);
		G_B18_0 = L_45;
		G_B18_1 = G_B17_0;
	}

IL_014e:
	{
		NullCheck(G_B18_1);
		VirtActionInvoker1< String_t* >::Invoke(11 /* System.Void System.IO.TextWriter::Write(System.String) */, G_B18_1, G_B18_0);
		return;
	}

IL_0154:
	{
		HtmlDocument_t556432108 * L_46 = __this->get__ownerdocument_19();
		NullCheck(L_46);
		bool L_47 = L_46->get_OptionOutputUpperCase_34();
		if (L_47)
		{
			goto IL_0169;
		}
	}
	{
		String_t* L_48 = HtmlNode_get_Name_m702134575(__this, /*hidden argument*/NULL);
		G_B22_0 = L_48;
		goto IL_0174;
	}

IL_0169:
	{
		String_t* L_49 = HtmlNode_get_Name_m702134575(__this, /*hidden argument*/NULL);
		NullCheck(L_49);
		String_t* L_50 = String_ToUpper_m3715743312(L_49, /*hidden argument*/NULL);
		G_B22_0 = L_50;
	}

IL_0174:
	{
		V_3 = G_B22_0;
		HtmlDocument_t556432108 * L_51 = __this->get__ownerdocument_19();
		NullCheck(L_51);
		bool L_52 = L_51->get_OptionOutputOriginalCase_33();
		if (!L_52)
		{
			goto IL_0189;
		}
	}
	{
		String_t* L_53 = HtmlNode_get_OriginalName_m1151328084(__this, /*hidden argument*/NULL);
		V_3 = L_53;
	}

IL_0189:
	{
		HtmlDocument_t556432108 * L_54 = __this->get__ownerdocument_19();
		NullCheck(L_54);
		bool L_55 = L_54->get_OptionOutputAsXml_31();
		if (!L_55)
		{
			goto IL_01c3;
		}
	}
	{
		String_t* L_56 = V_3;
		NullCheck(L_56);
		int32_t L_57 = String_get_Length_m1606060069(L_56, /*hidden argument*/NULL);
		if ((((int32_t)L_57) <= ((int32_t)0)))
		{
			goto IL_02ff;
		}
	}
	{
		String_t* L_58 = V_3;
		NullCheck(L_58);
		Il2CppChar L_59 = String_get_Chars_m4230566705(L_58, 0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_59) == ((uint32_t)((int32_t)63)))))
		{
			goto IL_01ae;
		}
	}
	{
		return;
	}

IL_01ae:
	{
		String_t* L_60 = V_3;
		NullCheck(L_60);
		String_t* L_61 = String_Trim_m2668767713(L_60, /*hidden argument*/NULL);
		NullCheck(L_61);
		int32_t L_62 = String_get_Length_m1606060069(L_61, /*hidden argument*/NULL);
		if (L_62)
		{
			goto IL_01bc;
		}
	}
	{
		return;
	}

IL_01bc:
	{
		String_t* L_63 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(HtmlDocument_t556432108_il2cpp_TypeInfo_var);
		String_t* L_64 = HtmlDocument_GetXmlName_m448645048(NULL /*static, unused*/, L_63, /*hidden argument*/NULL);
		V_3 = L_64;
	}

IL_01c3:
	{
		TextWriter_t4027217640 * L_65 = ___outText0;
		String_t* L_66 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_67 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral372029330, L_66, /*hidden argument*/NULL);
		NullCheck(L_65);
		VirtActionInvoker1< String_t* >::Invoke(11 /* System.Void System.IO.TextWriter::Write(System.String) */, L_65, L_67);
		TextWriter_t4027217640 * L_68 = ___outText0;
		HtmlNode_WriteAttributes_m2802695934(__this, L_68, (bool)0, /*hidden argument*/NULL);
		bool L_69 = HtmlNode_get_HasChildNodes_m1218535674(__this, /*hidden argument*/NULL);
		if (!L_69)
		{
			goto IL_0281;
		}
	}
	{
		TextWriter_t4027217640 * L_70 = ___outText0;
		NullCheck(L_70);
		VirtActionInvoker1< String_t* >::Invoke(11 /* System.Void System.IO.TextWriter::Write(System.String) */, L_70, _stringLiteral372029332);
		V_4 = (bool)0;
		HtmlDocument_t556432108 * L_71 = __this->get__ownerdocument_19();
		NullCheck(L_71);
		bool L_72 = L_71->get_OptionOutputAsXml_31();
		if (!L_72)
		{
			goto IL_021d;
		}
	}
	{
		String_t* L_73 = HtmlNode_get_Name_m702134575(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(HtmlNode_t2048434459_il2cpp_TypeInfo_var);
		bool L_74 = HtmlNode_IsCDataElement_m1574064561(NULL /*static, unused*/, L_73, /*hidden argument*/NULL);
		if (!L_74)
		{
			goto IL_021d;
		}
	}
	{
		V_4 = (bool)1;
		TextWriter_t4027217640 * L_75 = ___outText0;
		NullCheck(L_75);
		VirtActionInvoker1< String_t* >::Invoke(11 /* System.Void System.IO.TextWriter::Write(System.String) */, L_75, _stringLiteral2633345324);
	}

IL_021d:
	{
		bool L_76 = V_4;
		if (!L_76)
		{
			goto IL_0248;
		}
	}
	{
		bool L_77 = HtmlNode_get_HasChildNodes_m1218535674(__this, /*hidden argument*/NULL);
		if (!L_77)
		{
			goto IL_023b;
		}
	}
	{
		HtmlNodeCollection_t2542734491 * L_78 = HtmlNode_get_ChildNodes_m1556126725(__this, /*hidden argument*/NULL);
		NullCheck(L_78);
		HtmlNode_t2048434459 * L_79 = HtmlNodeCollection_get_Item_m2496957206(L_78, 0, /*hidden argument*/NULL);
		TextWriter_t4027217640 * L_80 = ___outText0;
		NullCheck(L_79);
		HtmlNode_WriteTo_m15807155(L_79, L_80, /*hidden argument*/NULL);
	}

IL_023b:
	{
		TextWriter_t4027217640 * L_81 = ___outText0;
		NullCheck(L_81);
		VirtActionInvoker1< String_t* >::Invoke(11 /* System.Void System.IO.TextWriter::Write(System.String) */, L_81, _stringLiteral1351854812);
		goto IL_024f;
	}

IL_0248:
	{
		TextWriter_t4027217640 * L_82 = ___outText0;
		HtmlNode_WriteContentTo_m83799726(__this, L_82, /*hidden argument*/NULL);
	}

IL_024f:
	{
		TextWriter_t4027217640 * L_83 = ___outText0;
		String_t* L_84 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_85 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral51790603, L_84, /*hidden argument*/NULL);
		NullCheck(L_83);
		VirtActionInvoker1< String_t* >::Invoke(11 /* System.Void System.IO.TextWriter::Write(System.String) */, L_83, L_85);
		HtmlDocument_t556432108 * L_86 = __this->get__ownerdocument_19();
		NullCheck(L_86);
		bool L_87 = L_86->get_OptionOutputAsXml_31();
		if (L_87)
		{
			goto IL_0275;
		}
	}
	{
		TextWriter_t4027217640 * L_88 = ___outText0;
		HtmlNode_WriteAttributes_m2802695934(__this, L_88, (bool)1, /*hidden argument*/NULL);
	}

IL_0275:
	{
		TextWriter_t4027217640 * L_89 = ___outText0;
		NullCheck(L_89);
		VirtActionInvoker1< String_t* >::Invoke(11 /* System.Void System.IO.TextWriter::Write(System.String) */, L_89, _stringLiteral372029332);
		return;
	}

IL_0281:
	{
		String_t* L_90 = HtmlNode_get_Name_m702134575(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(HtmlNode_t2048434459_il2cpp_TypeInfo_var);
		bool L_91 = HtmlNode_IsEmptyElement_m2918938855(NULL /*static, unused*/, L_90, /*hidden argument*/NULL);
		if (!L_91)
		{
			goto IL_02e9;
		}
	}
	{
		HtmlDocument_t556432108 * L_92 = __this->get__ownerdocument_19();
		NullCheck(L_92);
		bool L_93 = L_92->get_OptionWriteEmptyNodes_38();
		if (L_93)
		{
			goto IL_02a8;
		}
	}
	{
		HtmlDocument_t556432108 * L_94 = __this->get__ownerdocument_19();
		NullCheck(L_94);
		bool L_95 = L_94->get_OptionOutputAsXml_31();
		if (!L_95)
		{
			goto IL_02b4;
		}
	}

IL_02a8:
	{
		TextWriter_t4027217640 * L_96 = ___outText0;
		NullCheck(L_96);
		VirtActionInvoker1< String_t* >::Invoke(11 /* System.Void System.IO.TextWriter::Write(System.String) */, L_96, _stringLiteral57471885);
		return;
	}

IL_02b4:
	{
		String_t* L_97 = HtmlNode_get_Name_m702134575(__this, /*hidden argument*/NULL);
		NullCheck(L_97);
		int32_t L_98 = String_get_Length_m1606060069(L_97, /*hidden argument*/NULL);
		if ((((int32_t)L_98) <= ((int32_t)0)))
		{
			goto IL_02dd;
		}
	}
	{
		String_t* L_99 = HtmlNode_get_Name_m702134575(__this, /*hidden argument*/NULL);
		NullCheck(L_99);
		Il2CppChar L_100 = String_get_Chars_m4230566705(L_99, 0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_100) == ((uint32_t)((int32_t)63)))))
		{
			goto IL_02dd;
		}
	}
	{
		TextWriter_t4027217640 * L_101 = ___outText0;
		NullCheck(L_101);
		VirtActionInvoker1< String_t* >::Invoke(11 /* System.Void System.IO.TextWriter::Write(System.String) */, L_101, _stringLiteral372029331);
	}

IL_02dd:
	{
		TextWriter_t4027217640 * L_102 = ___outText0;
		NullCheck(L_102);
		VirtActionInvoker1< String_t* >::Invoke(11 /* System.Void System.IO.TextWriter::Write(System.String) */, L_102, _stringLiteral372029332);
		return;
	}

IL_02e9:
	{
		TextWriter_t4027217640 * L_103 = ___outText0;
		String_t* L_104 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_105 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral2073895217, L_104, _stringLiteral372029332, /*hidden argument*/NULL);
		NullCheck(L_103);
		VirtActionInvoker1< String_t* >::Invoke(11 /* System.Void System.IO.TextWriter::Write(System.String) */, L_103, L_105);
	}

IL_02ff:
	{
		return;
	}
}
// System.String HtmlAgilityPack.HtmlNode::WriteTo()
extern "C"  String_t* HtmlNode_WriteTo_m2203281867 (HtmlNode_t2048434459 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlNode_WriteTo_m2203281867_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringWriter_t4139609088 * V_0 = NULL;
	String_t* V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		StringWriter_t4139609088 * L_0 = (StringWriter_t4139609088 *)il2cpp_codegen_object_new(StringWriter_t4139609088_il2cpp_TypeInfo_var);
		StringWriter__ctor_m59456937(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
	}

IL_0006:
	try
	{ // begin try (depth: 1)
		StringWriter_t4139609088 * L_1 = V_0;
		HtmlNode_WriteTo_m15807155(__this, L_1, /*hidden argument*/NULL);
		StringWriter_t4139609088 * L_2 = V_0;
		NullCheck(L_2);
		VirtActionInvoker0::Invoke(7 /* System.Void System.IO.TextWriter::Flush() */, L_2);
		StringWriter_t4139609088 * L_3 = V_0;
		NullCheck(L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_3);
		V_1 = L_4;
		IL2CPP_LEAVE(0x26, FINALLY_001c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_001c;
	}

FINALLY_001c:
	{ // begin finally (depth: 1)
		{
			StringWriter_t4139609088 * L_5 = V_0;
			if (!L_5)
			{
				goto IL_0025;
			}
		}

IL_001f:
		{
			StringWriter_t4139609088 * L_6 = V_0;
			NullCheck(L_6);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_6);
		}

IL_0025:
		{
			IL2CPP_END_FINALLY(28)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(28)
	{
		IL2CPP_JUMP_TBL(0x26, IL_0026)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0026:
	{
		String_t* L_7 = V_1;
		return L_7;
	}
}
// System.String HtmlAgilityPack.HtmlNode::GetXmlComment(HtmlAgilityPack.HtmlCommentNode)
extern "C"  String_t* HtmlNode_GetXmlComment_m2603063516 (RuntimeObject * __this /* static, unused */, HtmlCommentNode_t1992371332 * ___comment0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlNode_GetXmlComment_m2603063516_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		HtmlCommentNode_t1992371332 * L_0 = ___comment0;
		NullCheck(L_0);
		String_t* L_1 = HtmlCommentNode_get_Comment_m3847846688(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = V_0;
		String_t* L_3 = V_0;
		NullCheck(L_3);
		int32_t L_4 = String_get_Length_m1606060069(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		String_t* L_5 = String_Substring_m12482732(L_2, 4, ((int32_t)((int32_t)L_4-(int32_t)7)), /*hidden argument*/NULL);
		NullCheck(L_5);
		String_t* L_6 = String_Replace_m1941156251(L_5, _stringLiteral1214590000, _stringLiteral3893525534, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void HtmlAgilityPack.HtmlNode::CloseNode(HtmlAgilityPack.HtmlNode)
extern "C"  void HtmlNode_CloseNode_m1160950766 (HtmlNode_t2048434459 * __this, HtmlNode_t2048434459 * ___endnode0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlNode_CloseNode_m1160950766_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	HtmlNode_t2048434459 * V_0 = NULL;
	HtmlNode_t2048434459 * V_1 = NULL;
	HtmlNode_t2048434459 * V_2 = NULL;
	RuntimeObject* V_3 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		HtmlDocument_t556432108 * L_0 = __this->get__ownerdocument_19();
		NullCheck(L_0);
		bool L_1 = L_0->get_OptionAutoCloseOnEnd_24();
		if (L_1)
		{
			goto IL_0067;
		}
	}
	{
		HtmlNodeCollection_t2542734491 * L_2 = __this->get__childnodes_1();
		if (!L_2)
		{
			goto IL_0067;
		}
	}
	{
		HtmlNodeCollection_t2542734491 * L_3 = __this->get__childnodes_1();
		NullCheck(L_3);
		RuntimeObject* L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<HtmlAgilityPack.HtmlNode>::GetEnumerator() */, IEnumerable_1_t2340561504_il2cpp_TypeInfo_var, L_3);
		V_3 = L_4;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0053;
		}

IL_0023:
		{
			RuntimeObject* L_5 = V_3;
			NullCheck(L_5);
			HtmlNode_t2048434459 * L_6 = InterfaceFuncInvoker0< HtmlNode_t2048434459 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<HtmlAgilityPack.HtmlNode>::get_Current() */, IEnumerator_1_t3818925582_il2cpp_TypeInfo_var, L_5);
			V_0 = L_6;
			HtmlNode_t2048434459 * L_7 = V_0;
			NullCheck(L_7);
			bool L_8 = HtmlNode_get_Closed_m650109995(L_7, /*hidden argument*/NULL);
			if (L_8)
			{
				goto IL_0053;
			}
		}

IL_0032:
		{
			int32_t L_9 = HtmlNode_get_NodeType_m270476988(__this, /*hidden argument*/NULL);
			HtmlDocument_t556432108 * L_10 = __this->get__ownerdocument_19();
			HtmlNode_t2048434459 * L_11 = (HtmlNode_t2048434459 *)il2cpp_codegen_object_new(HtmlNode_t2048434459_il2cpp_TypeInfo_var);
			HtmlNode__ctor_m2584575018(L_11, L_9, L_10, (-1), /*hidden argument*/NULL);
			V_1 = L_11;
			HtmlNode_t2048434459 * L_12 = V_1;
			HtmlNode_t2048434459 * L_13 = V_1;
			NullCheck(L_12);
			L_12->set__endnode_2(L_13);
			HtmlNode_t2048434459 * L_14 = V_0;
			HtmlNode_t2048434459 * L_15 = V_1;
			NullCheck(L_14);
			HtmlNode_CloseNode_m1160950766(L_14, L_15, /*hidden argument*/NULL);
		}

IL_0053:
		{
			RuntimeObject* L_16 = V_3;
			NullCheck(L_16);
			bool L_17 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_16);
			if (L_17)
			{
				goto IL_0023;
			}
		}

IL_005b:
		{
			IL2CPP_LEAVE(0x67, FINALLY_005d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_005d;
	}

FINALLY_005d:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_18 = V_3;
			if (!L_18)
			{
				goto IL_0066;
			}
		}

IL_0060:
		{
			RuntimeObject* L_19 = V_3;
			NullCheck(L_19);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_19);
		}

IL_0066:
		{
			IL2CPP_END_FINALLY(93)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(93)
	{
		IL2CPP_JUMP_TBL(0x67, IL_0067)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0067:
	{
		bool L_20 = HtmlNode_get_Closed_m650109995(__this, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_011f;
		}
	}
	{
		HtmlNode_t2048434459 * L_21 = ___endnode0;
		__this->set__endnode_2(L_21);
		HtmlDocument_t556432108 * L_22 = __this->get__ownerdocument_19();
		NullCheck(L_22);
		Dictionary_2_t1056260094 * L_23 = L_22->get_Openednodes_16();
		if (!L_23)
		{
			goto IL_009d;
		}
	}
	{
		HtmlDocument_t556432108 * L_24 = __this->get__ownerdocument_19();
		NullCheck(L_24);
		Dictionary_2_t1056260094 * L_25 = L_24->get_Openednodes_16();
		int32_t L_26 = __this->get__outerstartindex_17();
		NullCheck(L_25);
		Dictionary_2_Remove_m1113698489(L_25, L_26, /*hidden argument*/Dictionary_2_Remove_m1113698489_RuntimeMethod_var);
	}

IL_009d:
	{
		HtmlDocument_t556432108 * L_27 = __this->get__ownerdocument_19();
		NullCheck(L_27);
		Dictionary_2_t3963213721 * L_28 = L_27->get_Lastnodes_8();
		String_t* L_29 = HtmlNode_get_Name_m702134575(__this, /*hidden argument*/NULL);
		HtmlNode_t2048434459 * L_30 = Utilities_GetDictionaryValueOrNull_TisString_t_TisHtmlNode_t2048434459_m2137127104(NULL /*static, unused*/, L_28, L_29, /*hidden argument*/Utilities_GetDictionaryValueOrNull_TisString_t_TisHtmlNode_t2048434459_m2137127104_RuntimeMethod_var);
		V_2 = L_30;
		HtmlNode_t2048434459 * L_31 = V_2;
		if ((!(((RuntimeObject*)(HtmlNode_t2048434459 *)L_31) == ((RuntimeObject*)(HtmlNode_t2048434459 *)__this))))
		{
			goto IL_00da;
		}
	}
	{
		HtmlDocument_t556432108 * L_32 = __this->get__ownerdocument_19();
		NullCheck(L_32);
		Dictionary_2_t3963213721 * L_33 = L_32->get_Lastnodes_8();
		String_t* L_34 = HtmlNode_get_Name_m702134575(__this, /*hidden argument*/NULL);
		NullCheck(L_33);
		Dictionary_2_Remove_m4126380600(L_33, L_34, /*hidden argument*/Dictionary_2_Remove_m4126380600_RuntimeMethod_var);
		HtmlDocument_t556432108 * L_35 = __this->get__ownerdocument_19();
		NullCheck(L_35);
		HtmlDocument_UpdateLastParentNode_m3368866332(L_35, /*hidden argument*/NULL);
	}

IL_00da:
	{
		HtmlNode_t2048434459 * L_36 = ___endnode0;
		if ((!(((RuntimeObject*)(HtmlNode_t2048434459 *)L_36) == ((RuntimeObject*)(HtmlNode_t2048434459 *)__this))))
		{
			goto IL_00df;
		}
	}
	{
		return;
	}

IL_00df:
	{
		int32_t L_37 = __this->get__outerstartindex_17();
		int32_t L_38 = __this->get__outerlength_16();
		__this->set__innerstartindex_6(((int32_t)((int32_t)L_37+(int32_t)L_38)));
		HtmlNode_t2048434459 * L_39 = ___endnode0;
		NullCheck(L_39);
		int32_t L_40 = L_39->get__outerstartindex_17();
		int32_t L_41 = __this->get__innerstartindex_6();
		__this->set__innerlength_5(((int32_t)((int32_t)L_40-(int32_t)L_41)));
		HtmlNode_t2048434459 * L_42 = ___endnode0;
		NullCheck(L_42);
		int32_t L_43 = L_42->get__outerstartindex_17();
		HtmlNode_t2048434459 * L_44 = ___endnode0;
		NullCheck(L_44);
		int32_t L_45 = L_44->get__outerlength_16();
		int32_t L_46 = __this->get__outerstartindex_17();
		__this->set__outerlength_16(((int32_t)((int32_t)((int32_t)((int32_t)L_43+(int32_t)L_45))-(int32_t)L_46)));
	}

IL_011f:
	{
		return;
	}
}
// System.String HtmlAgilityPack.HtmlNode::GetId()
extern "C"  String_t* HtmlNode_GetId_m3597009408 (HtmlNode_t2048434459 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlNode_GetId_m3597009408_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	HtmlAttribute_t1804523403 * V_0 = NULL;
	{
		HtmlAttributeCollection_t1787476631 * L_0 = HtmlNode_get_Attributes_m2935417207(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		HtmlAttribute_t1804523403 * L_1 = HtmlAttributeCollection_get_Item_m3848708643(L_0, _stringLiteral287061489, /*hidden argument*/NULL);
		V_0 = L_1;
		HtmlAttribute_t1804523403 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_001b;
		}
	}
	{
		HtmlAttribute_t1804523403 * L_3 = V_0;
		NullCheck(L_3);
		String_t* L_4 = HtmlAttribute_get_Value_m3933521327(L_3, /*hidden argument*/NULL);
		return L_4;
	}

IL_001b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		return L_5;
	}
}
// System.Void HtmlAgilityPack.HtmlNode::WriteAttribute(System.IO.TextWriter,HtmlAgilityPack.HtmlAttribute)
extern "C"  void HtmlNode_WriteAttribute_m3827491122 (HtmlNode_t2048434459 * __this, TextWriter_t4027217640 * ___outText0, HtmlAttribute_t1804523403 * ___att1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlNode_WriteAttribute_m3827491122_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	StringU5BU5D_t1642385972* V_2 = NULL;
	StringU5BU5D_t1642385972* V_3 = NULL;
	StringU5BU5D_t1642385972* V_4 = NULL;
	String_t* G_B3_0 = NULL;
	String_t* G_B7_0 = NULL;
	String_t* G_B13_0 = NULL;
	{
		HtmlAttribute_t1804523403 * L_0 = ___att1;
		NullCheck(L_0);
		int32_t L_1 = HtmlAttribute_get_QuoteType_m789997534(L_0, /*hidden argument*/NULL);
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0010;
		}
	}
	{
		G_B3_0 = _stringLiteral372029307;
		goto IL_0015;
	}

IL_0010:
	{
		G_B3_0 = _stringLiteral372029312;
	}

IL_0015:
	{
		V_1 = G_B3_0;
		HtmlDocument_t556432108 * L_2 = __this->get__ownerdocument_19();
		NullCheck(L_2);
		bool L_3 = L_2->get_OptionOutputAsXml_31();
		if (!L_3)
		{
			goto IL_0096;
		}
	}
	{
		HtmlDocument_t556432108 * L_4 = __this->get__ownerdocument_19();
		NullCheck(L_4);
		bool L_5 = L_4->get_OptionOutputUpperCase_34();
		if (L_5)
		{
			goto IL_0038;
		}
	}
	{
		HtmlAttribute_t1804523403 * L_6 = ___att1;
		NullCheck(L_6);
		String_t* L_7 = HtmlAttribute_get_XmlName_m2621022904(L_6, /*hidden argument*/NULL);
		G_B7_0 = L_7;
		goto IL_0043;
	}

IL_0038:
	{
		HtmlAttribute_t1804523403 * L_8 = ___att1;
		NullCheck(L_8);
		String_t* L_9 = HtmlAttribute_get_XmlName_m2621022904(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		String_t* L_10 = String_ToUpper_m3715743312(L_9, /*hidden argument*/NULL);
		G_B7_0 = L_10;
	}

IL_0043:
	{
		V_0 = G_B7_0;
		HtmlDocument_t556432108 * L_11 = __this->get__ownerdocument_19();
		NullCheck(L_11);
		bool L_12 = L_11->get_OptionOutputOriginalCase_33();
		if (!L_12)
		{
			goto IL_0058;
		}
	}
	{
		HtmlAttribute_t1804523403 * L_13 = ___att1;
		NullCheck(L_13);
		String_t* L_14 = HtmlAttribute_get_OriginalName_m3080906880(L_13, /*hidden argument*/NULL);
		V_0 = L_14;
	}

IL_0058:
	{
		TextWriter_t4027217640 * L_15 = ___outText0;
		V_2 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)6));
		StringU5BU5D_t1642385972* L_16 = V_2;
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, _stringLiteral372029310);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029310);
		StringU5BU5D_t1642385972* L_17 = V_2;
		String_t* L_18 = V_0;
		NullCheck(L_17);
		ArrayElementTypeCheck (L_17, L_18);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_18);
		StringU5BU5D_t1642385972* L_19 = V_2;
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, _stringLiteral372029329);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral372029329);
		StringU5BU5D_t1642385972* L_20 = V_2;
		String_t* L_21 = V_1;
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, L_21);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_21);
		StringU5BU5D_t1642385972* L_22 = V_2;
		HtmlAttribute_t1804523403 * L_23 = ___att1;
		NullCheck(L_23);
		String_t* L_24 = HtmlAttribute_get_XmlValue_m2411235454(L_23, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(HtmlDocument_t556432108_il2cpp_TypeInfo_var);
		String_t* L_25 = HtmlDocument_HtmlEncode_m2155231231(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, L_25);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)L_25);
		StringU5BU5D_t1642385972* L_26 = V_2;
		String_t* L_27 = V_1;
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, L_27);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)L_27);
		StringU5BU5D_t1642385972* L_28 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_29 = String_Concat_m626692867(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
		NullCheck(L_15);
		VirtActionInvoker1< String_t* >::Invoke(11 /* System.Void System.IO.TextWriter::Write(System.String) */, L_15, L_29);
		return;
	}

IL_0096:
	{
		HtmlDocument_t556432108 * L_30 = __this->get__ownerdocument_19();
		NullCheck(L_30);
		bool L_31 = L_30->get_OptionOutputUpperCase_34();
		if (L_31)
		{
			goto IL_00ab;
		}
	}
	{
		HtmlAttribute_t1804523403 * L_32 = ___att1;
		NullCheck(L_32);
		String_t* L_33 = HtmlAttribute_get_Name_m3956631639(L_32, /*hidden argument*/NULL);
		G_B13_0 = L_33;
		goto IL_00b6;
	}

IL_00ab:
	{
		HtmlAttribute_t1804523403 * L_34 = ___att1;
		NullCheck(L_34);
		String_t* L_35 = HtmlAttribute_get_Name_m3956631639(L_34, /*hidden argument*/NULL);
		NullCheck(L_35);
		String_t* L_36 = String_ToUpper_m3715743312(L_35, /*hidden argument*/NULL);
		G_B13_0 = L_36;
	}

IL_00b6:
	{
		V_0 = G_B13_0;
		HtmlAttribute_t1804523403 * L_37 = ___att1;
		NullCheck(L_37);
		String_t* L_38 = HtmlAttribute_get_Name_m3956631639(L_37, /*hidden argument*/NULL);
		NullCheck(L_38);
		int32_t L_39 = String_get_Length_m1606060069(L_38, /*hidden argument*/NULL);
		if ((((int32_t)L_39) < ((int32_t)4)))
		{
			goto IL_012f;
		}
	}
	{
		HtmlAttribute_t1804523403 * L_40 = ___att1;
		NullCheck(L_40);
		String_t* L_41 = HtmlAttribute_get_Name_m3956631639(L_40, /*hidden argument*/NULL);
		NullCheck(L_41);
		Il2CppChar L_42 = String_get_Chars_m4230566705(L_41, 0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_42) == ((uint32_t)((int32_t)60)))))
		{
			goto IL_012f;
		}
	}
	{
		HtmlAttribute_t1804523403 * L_43 = ___att1;
		NullCheck(L_43);
		String_t* L_44 = HtmlAttribute_get_Name_m3956631639(L_43, /*hidden argument*/NULL);
		NullCheck(L_44);
		Il2CppChar L_45 = String_get_Chars_m4230566705(L_44, 1, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_45) == ((uint32_t)((int32_t)37)))))
		{
			goto IL_012f;
		}
	}
	{
		HtmlAttribute_t1804523403 * L_46 = ___att1;
		NullCheck(L_46);
		String_t* L_47 = HtmlAttribute_get_Name_m3956631639(L_46, /*hidden argument*/NULL);
		HtmlAttribute_t1804523403 * L_48 = ___att1;
		NullCheck(L_48);
		String_t* L_49 = HtmlAttribute_get_Name_m3956631639(L_48, /*hidden argument*/NULL);
		NullCheck(L_49);
		int32_t L_50 = String_get_Length_m1606060069(L_49, /*hidden argument*/NULL);
		NullCheck(L_47);
		Il2CppChar L_51 = String_get_Chars_m4230566705(L_47, ((int32_t)((int32_t)L_50-(int32_t)1)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_51) == ((uint32_t)((int32_t)62)))))
		{
			goto IL_012f;
		}
	}
	{
		HtmlAttribute_t1804523403 * L_52 = ___att1;
		NullCheck(L_52);
		String_t* L_53 = HtmlAttribute_get_Name_m3956631639(L_52, /*hidden argument*/NULL);
		HtmlAttribute_t1804523403 * L_54 = ___att1;
		NullCheck(L_54);
		String_t* L_55 = HtmlAttribute_get_Name_m3956631639(L_54, /*hidden argument*/NULL);
		NullCheck(L_55);
		int32_t L_56 = String_get_Length_m1606060069(L_55, /*hidden argument*/NULL);
		NullCheck(L_53);
		Il2CppChar L_57 = String_get_Chars_m4230566705(L_53, ((int32_t)((int32_t)L_56-(int32_t)2)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_57) == ((uint32_t)((int32_t)37)))))
		{
			goto IL_012f;
		}
	}
	{
		TextWriter_t4027217640 * L_58 = ___outText0;
		String_t* L_59 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_60 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral372029310, L_59, /*hidden argument*/NULL);
		NullCheck(L_58);
		VirtActionInvoker1< String_t* >::Invoke(11 /* System.Void System.IO.TextWriter::Write(System.String) */, L_58, L_60);
		return;
	}

IL_012f:
	{
		HtmlDocument_t556432108 * L_61 = __this->get__ownerdocument_19();
		NullCheck(L_61);
		bool L_62 = L_61->get_OptionOutputOptimizeAttributeValues_32();
		if (!L_62)
		{
			goto IL_01b1;
		}
	}
	{
		HtmlAttribute_t1804523403 * L_63 = ___att1;
		NullCheck(L_63);
		String_t* L_64 = HtmlAttribute_get_Value_m3933521327(L_63, /*hidden argument*/NULL);
		CharU5BU5D_t1328083999* L_65 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)4));
		RuntimeHelpers_InitializeArray_m3920580167(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_65, LoadFieldToken(U3CPrivateImplementationDetailsU3EU7B83185D3BU2D3939U2D439CU2DA54FU2D260F9279D9C8U7D_t691081255____U24U24method0x600012fU2D1_0_FieldInfo_var), /*hidden argument*/NULL);
		NullCheck(L_64);
		int32_t L_66 = String_IndexOfAny_m2016554902(L_64, L_65, /*hidden argument*/NULL);
		if ((((int32_t)L_66) >= ((int32_t)0)))
		{
			goto IL_0178;
		}
	}
	{
		TextWriter_t4027217640 * L_67 = ___outText0;
		String_t* L_68 = V_0;
		HtmlAttribute_t1804523403 * L_69 = ___att1;
		NullCheck(L_69);
		String_t* L_70 = HtmlAttribute_get_Value_m3933521327(L_69, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_71 = String_Concat_m1561703559(NULL /*static, unused*/, _stringLiteral372029310, L_68, _stringLiteral372029329, L_70, /*hidden argument*/NULL);
		NullCheck(L_67);
		VirtActionInvoker1< String_t* >::Invoke(11 /* System.Void System.IO.TextWriter::Write(System.String) */, L_67, L_71);
		return;
	}

IL_0178:
	{
		TextWriter_t4027217640 * L_72 = ___outText0;
		V_3 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)6));
		StringU5BU5D_t1642385972* L_73 = V_3;
		NullCheck(L_73);
		ArrayElementTypeCheck (L_73, _stringLiteral372029310);
		(L_73)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029310);
		StringU5BU5D_t1642385972* L_74 = V_3;
		String_t* L_75 = V_0;
		NullCheck(L_74);
		ArrayElementTypeCheck (L_74, L_75);
		(L_74)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_75);
		StringU5BU5D_t1642385972* L_76 = V_3;
		NullCheck(L_76);
		ArrayElementTypeCheck (L_76, _stringLiteral372029329);
		(L_76)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral372029329);
		StringU5BU5D_t1642385972* L_77 = V_3;
		String_t* L_78 = V_1;
		NullCheck(L_77);
		ArrayElementTypeCheck (L_77, L_78);
		(L_77)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_78);
		StringU5BU5D_t1642385972* L_79 = V_3;
		HtmlAttribute_t1804523403 * L_80 = ___att1;
		NullCheck(L_80);
		String_t* L_81 = HtmlAttribute_get_Value_m3933521327(L_80, /*hidden argument*/NULL);
		NullCheck(L_79);
		ArrayElementTypeCheck (L_79, L_81);
		(L_79)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)L_81);
		StringU5BU5D_t1642385972* L_82 = V_3;
		String_t* L_83 = V_1;
		NullCheck(L_82);
		ArrayElementTypeCheck (L_82, L_83);
		(L_82)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)L_83);
		StringU5BU5D_t1642385972* L_84 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_85 = String_Concat_m626692867(NULL /*static, unused*/, L_84, /*hidden argument*/NULL);
		NullCheck(L_72);
		VirtActionInvoker1< String_t* >::Invoke(11 /* System.Void System.IO.TextWriter::Write(System.String) */, L_72, L_85);
		return;
	}

IL_01b1:
	{
		TextWriter_t4027217640 * L_86 = ___outText0;
		V_4 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)6));
		StringU5BU5D_t1642385972* L_87 = V_4;
		NullCheck(L_87);
		ArrayElementTypeCheck (L_87, _stringLiteral372029310);
		(L_87)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029310);
		StringU5BU5D_t1642385972* L_88 = V_4;
		String_t* L_89 = V_0;
		NullCheck(L_88);
		ArrayElementTypeCheck (L_88, L_89);
		(L_88)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_89);
		StringU5BU5D_t1642385972* L_90 = V_4;
		NullCheck(L_90);
		ArrayElementTypeCheck (L_90, _stringLiteral372029329);
		(L_90)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral372029329);
		StringU5BU5D_t1642385972* L_91 = V_4;
		String_t* L_92 = V_1;
		NullCheck(L_91);
		ArrayElementTypeCheck (L_91, L_92);
		(L_91)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_92);
		StringU5BU5D_t1642385972* L_93 = V_4;
		HtmlAttribute_t1804523403 * L_94 = ___att1;
		NullCheck(L_94);
		String_t* L_95 = HtmlAttribute_get_Value_m3933521327(L_94, /*hidden argument*/NULL);
		NullCheck(L_93);
		ArrayElementTypeCheck (L_93, L_95);
		(L_93)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)L_95);
		StringU5BU5D_t1642385972* L_96 = V_4;
		String_t* L_97 = V_1;
		NullCheck(L_96);
		ArrayElementTypeCheck (L_96, L_97);
		(L_96)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)L_97);
		StringU5BU5D_t1642385972* L_98 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_99 = String_Concat_m626692867(NULL /*static, unused*/, L_98, /*hidden argument*/NULL);
		NullCheck(L_86);
		VirtActionInvoker1< String_t* >::Invoke(11 /* System.Void System.IO.TextWriter::Write(System.String) */, L_86, L_99);
		return;
	}
}
// System.Void HtmlAgilityPack.HtmlNode::WriteAttributes(System.IO.TextWriter,System.Boolean)
extern "C"  void HtmlNode_WriteAttributes_m2802695934 (HtmlNode_t2048434459 * __this, TextWriter_t4027217640 * ___outText0, bool ___closing1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlNode_WriteAttributes_m2802695934_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	HtmlAttribute_t1804523403 * V_0 = NULL;
	HtmlAttribute_t1804523403 * V_1 = NULL;
	int32_t V_2 = 0;
	HtmlNode_t2048434459 * V_3 = NULL;
	HtmlAttribute_t1804523403 * V_4 = NULL;
	Enumerator_t1110868133  V_5;
	memset(&V_5, 0, sizeof(V_5));
	RuntimeObject* V_6 = NULL;
	bool V_7 = false;
	int32_t V_8 = 0;
	RuntimeObject* V_9 = NULL;
	RuntimeObject* V_10 = NULL;
	bool V_11 = false;
	int32_t V_12 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		HtmlDocument_t556432108 * L_0 = __this->get__ownerdocument_19();
		NullCheck(L_0);
		bool L_1 = L_0->get_OptionOutputAsXml_31();
		if (!L_1)
		{
			goto IL_0059;
		}
	}
	{
		HtmlAttributeCollection_t1787476631 * L_2 = __this->get__attributes_0();
		if (L_2)
		{
			goto IL_0016;
		}
	}
	{
		return;
	}

IL_0016:
	{
		HtmlAttributeCollection_t1787476631 * L_3 = __this->get__attributes_0();
		NullCheck(L_3);
		Dictionary_2_t3719302665 * L_4 = L_3->get_Hashitems_0();
		NullCheck(L_4);
		ValueCollection_t2422362508 * L_5 = Dictionary_2_get_Values_m1907592043(L_4, /*hidden argument*/Dictionary_2_get_Values_m1907592043_RuntimeMethod_var);
		NullCheck(L_5);
		Enumerator_t1110868133  L_6 = ValueCollection_GetEnumerator_m2331812445(L_5, /*hidden argument*/ValueCollection_GetEnumerator_m2331812445_RuntimeMethod_var);
		V_5 = L_6;
	}

IL_002d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_003f;
		}

IL_002f:
		{
			HtmlAttribute_t1804523403 * L_7 = Enumerator_get_Current_m4000267011((&V_5), /*hidden argument*/Enumerator_get_Current_m4000267011_RuntimeMethod_var);
			V_0 = L_7;
			TextWriter_t4027217640 * L_8 = ___outText0;
			HtmlAttribute_t1804523403 * L_9 = V_0;
			HtmlNode_WriteAttribute_m3827491122(__this, L_8, L_9, /*hidden argument*/NULL);
		}

IL_003f:
		{
			bool L_10 = Enumerator_MoveNext_m3599701188((&V_5), /*hidden argument*/Enumerator_MoveNext_m3599701188_RuntimeMethod_var);
			if (L_10)
			{
				goto IL_002f;
			}
		}

IL_0048:
		{
			IL2CPP_LEAVE(0x58, FINALLY_004a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_004a;
	}

FINALLY_004a:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m1803819355((&V_5), /*hidden argument*/Enumerator_Dispose_m1803819355_RuntimeMethod_var);
		IL2CPP_END_FINALLY(74)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(74)
	{
		IL2CPP_JUMP_TBL(0x58, IL_0058)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0058:
	{
		return;
	}

IL_0059:
	{
		bool L_11 = ___closing1;
		if (L_11)
		{
			goto IL_015b;
		}
	}
	{
		HtmlAttributeCollection_t1787476631 * L_12 = __this->get__attributes_0();
		if (!L_12)
		{
			goto IL_009d;
		}
	}
	{
		HtmlAttributeCollection_t1787476631 * L_13 = __this->get__attributes_0();
		NullCheck(L_13);
		RuntimeObject* L_14 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<HtmlAgilityPack.HtmlAttribute>::GetEnumerator() */, IEnumerable_1_t2096650448_il2cpp_TypeInfo_var, L_13);
		V_6 = L_14;
	}

IL_0074:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0086;
		}

IL_0076:
		{
			RuntimeObject* L_15 = V_6;
			NullCheck(L_15);
			HtmlAttribute_t1804523403 * L_16 = InterfaceFuncInvoker0< HtmlAttribute_t1804523403 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<HtmlAgilityPack.HtmlAttribute>::get_Current() */, IEnumerator_1_t3575014526_il2cpp_TypeInfo_var, L_15);
			V_1 = L_16;
			TextWriter_t4027217640 * L_17 = ___outText0;
			HtmlAttribute_t1804523403 * L_18 = V_1;
			HtmlNode_WriteAttribute_m3827491122(__this, L_17, L_18, /*hidden argument*/NULL);
		}

IL_0086:
		{
			RuntimeObject* L_19 = V_6;
			NullCheck(L_19);
			bool L_20 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_19);
			if (L_20)
			{
				goto IL_0076;
			}
		}

IL_008f:
		{
			IL2CPP_LEAVE(0x9D, FINALLY_0091);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0091;
	}

FINALLY_0091:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_21 = V_6;
			if (!L_21)
			{
				goto IL_009c;
			}
		}

IL_0095:
		{
			RuntimeObject* L_22 = V_6;
			NullCheck(L_22);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_22);
		}

IL_009c:
		{
			IL2CPP_END_FINALLY(145)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(145)
	{
		IL2CPP_JUMP_TBL(0x9D, IL_009d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_009d:
	{
		HtmlDocument_t556432108 * L_23 = __this->get__ownerdocument_19();
		NullCheck(L_23);
		bool L_24 = L_23->get_OptionAddDebuggingAttributes_23();
		if (L_24)
		{
			goto IL_00ab;
		}
	}
	{
		return;
	}

IL_00ab:
	{
		TextWriter_t4027217640 * L_25 = ___outText0;
		HtmlDocument_t556432108 * L_26 = __this->get__ownerdocument_19();
		bool L_27 = HtmlNode_get_Closed_m650109995(__this, /*hidden argument*/NULL);
		V_7 = L_27;
		String_t* L_28 = Boolean_ToString_m1253164328((&V_7), /*hidden argument*/NULL);
		NullCheck(L_26);
		HtmlAttribute_t1804523403 * L_29 = HtmlDocument_CreateAttribute_m4240580628(L_26, _stringLiteral9172989, L_28, /*hidden argument*/NULL);
		HtmlNode_WriteAttribute_m3827491122(__this, L_25, L_29, /*hidden argument*/NULL);
		TextWriter_t4027217640 * L_30 = ___outText0;
		HtmlDocument_t556432108 * L_31 = __this->get__ownerdocument_19();
		HtmlNodeCollection_t2542734491 * L_32 = HtmlNode_get_ChildNodes_m1556126725(__this, /*hidden argument*/NULL);
		NullCheck(L_32);
		int32_t L_33 = HtmlNodeCollection_get_Count_m3249441714(L_32, /*hidden argument*/NULL);
		V_8 = L_33;
		String_t* L_34 = Int32_ToString_m2960866144((&V_8), /*hidden argument*/NULL);
		NullCheck(L_31);
		HtmlAttribute_t1804523403 * L_35 = HtmlDocument_CreateAttribute_m4240580628(L_31, _stringLiteral1029384328, L_34, /*hidden argument*/NULL);
		HtmlNode_WriteAttribute_m3827491122(__this, L_30, L_35, /*hidden argument*/NULL);
		V_2 = 0;
		HtmlNodeCollection_t2542734491 * L_36 = HtmlNode_get_ChildNodes_m1556126725(__this, /*hidden argument*/NULL);
		NullCheck(L_36);
		RuntimeObject* L_37 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<HtmlAgilityPack.HtmlNode>::GetEnumerator() */, IEnumerable_1_t2340561504_il2cpp_TypeInfo_var, L_36);
		V_9 = L_37;
	}

IL_010b:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0141;
		}

IL_010d:
		{
			RuntimeObject* L_38 = V_9;
			NullCheck(L_38);
			HtmlNode_t2048434459 * L_39 = InterfaceFuncInvoker0< HtmlNode_t2048434459 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<HtmlAgilityPack.HtmlNode>::get_Current() */, IEnumerator_1_t3818925582_il2cpp_TypeInfo_var, L_38);
			V_3 = L_39;
			TextWriter_t4027217640 * L_40 = ___outText0;
			HtmlDocument_t556432108 * L_41 = __this->get__ownerdocument_19();
			int32_t L_42 = V_2;
			int32_t L_43 = L_42;
			RuntimeObject * L_44 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_43);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_45 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral4183177304, L_44, /*hidden argument*/NULL);
			HtmlNode_t2048434459 * L_46 = V_3;
			NullCheck(L_46);
			String_t* L_47 = HtmlNode_get_Name_m702134575(L_46, /*hidden argument*/NULL);
			NullCheck(L_41);
			HtmlAttribute_t1804523403 * L_48 = HtmlDocument_CreateAttribute_m4240580628(L_41, L_45, L_47, /*hidden argument*/NULL);
			HtmlNode_WriteAttribute_m3827491122(__this, L_40, L_48, /*hidden argument*/NULL);
			int32_t L_49 = V_2;
			V_2 = ((int32_t)((int32_t)L_49+(int32_t)1));
		}

IL_0141:
		{
			RuntimeObject* L_50 = V_9;
			NullCheck(L_50);
			bool L_51 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_50);
			if (L_51)
			{
				goto IL_010d;
			}
		}

IL_014a:
		{
			IL2CPP_LEAVE(0x216, FINALLY_014f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_014f;
	}

FINALLY_014f:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_52 = V_9;
			if (!L_52)
			{
				goto IL_015a;
			}
		}

IL_0153:
		{
			RuntimeObject* L_53 = V_9;
			NullCheck(L_53);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_53);
		}

IL_015a:
		{
			IL2CPP_END_FINALLY(335)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(335)
	{
		IL2CPP_JUMP_TBL(0x216, IL_0216)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_015b:
	{
		HtmlNode_t2048434459 * L_54 = __this->get__endnode_2();
		if (!L_54)
		{
			goto IL_0179;
		}
	}
	{
		HtmlNode_t2048434459 * L_55 = __this->get__endnode_2();
		NullCheck(L_55);
		HtmlAttributeCollection_t1787476631 * L_56 = L_55->get__attributes_0();
		if (!L_56)
		{
			goto IL_0179;
		}
	}
	{
		HtmlNode_t2048434459 * L_57 = __this->get__endnode_2();
		if ((!(((RuntimeObject*)(HtmlNode_t2048434459 *)L_57) == ((RuntimeObject*)(HtmlNode_t2048434459 *)__this))))
		{
			goto IL_017a;
		}
	}

IL_0179:
	{
		return;
	}

IL_017a:
	{
		HtmlNode_t2048434459 * L_58 = __this->get__endnode_2();
		NullCheck(L_58);
		HtmlAttributeCollection_t1787476631 * L_59 = L_58->get__attributes_0();
		NullCheck(L_59);
		RuntimeObject* L_60 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<HtmlAgilityPack.HtmlAttribute>::GetEnumerator() */, IEnumerable_1_t2096650448_il2cpp_TypeInfo_var, L_59);
		V_10 = L_60;
	}

IL_018c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_01a0;
		}

IL_018e:
		{
			RuntimeObject* L_61 = V_10;
			NullCheck(L_61);
			HtmlAttribute_t1804523403 * L_62 = InterfaceFuncInvoker0< HtmlAttribute_t1804523403 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<HtmlAgilityPack.HtmlAttribute>::get_Current() */, IEnumerator_1_t3575014526_il2cpp_TypeInfo_var, L_61);
			V_4 = L_62;
			TextWriter_t4027217640 * L_63 = ___outText0;
			HtmlAttribute_t1804523403 * L_64 = V_4;
			HtmlNode_WriteAttribute_m3827491122(__this, L_63, L_64, /*hidden argument*/NULL);
		}

IL_01a0:
		{
			RuntimeObject* L_65 = V_10;
			NullCheck(L_65);
			bool L_66 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_65);
			if (L_66)
			{
				goto IL_018e;
			}
		}

IL_01a9:
		{
			IL2CPP_LEAVE(0x1B7, FINALLY_01ab);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_01ab;
	}

FINALLY_01ab:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_67 = V_10;
			if (!L_67)
			{
				goto IL_01b6;
			}
		}

IL_01af:
		{
			RuntimeObject* L_68 = V_10;
			NullCheck(L_68);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_68);
		}

IL_01b6:
		{
			IL2CPP_END_FINALLY(427)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(427)
	{
		IL2CPP_JUMP_TBL(0x1B7, IL_01b7)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_01b7:
	{
		HtmlDocument_t556432108 * L_69 = __this->get__ownerdocument_19();
		NullCheck(L_69);
		bool L_70 = L_69->get_OptionAddDebuggingAttributes_23();
		if (L_70)
		{
			goto IL_01c5;
		}
	}
	{
		return;
	}

IL_01c5:
	{
		TextWriter_t4027217640 * L_71 = ___outText0;
		HtmlDocument_t556432108 * L_72 = __this->get__ownerdocument_19();
		bool L_73 = HtmlNode_get_Closed_m650109995(__this, /*hidden argument*/NULL);
		V_11 = L_73;
		String_t* L_74 = Boolean_ToString_m1253164328((&V_11), /*hidden argument*/NULL);
		NullCheck(L_72);
		HtmlAttribute_t1804523403 * L_75 = HtmlDocument_CreateAttribute_m4240580628(L_72, _stringLiteral9172989, L_74, /*hidden argument*/NULL);
		HtmlNode_WriteAttribute_m3827491122(__this, L_71, L_75, /*hidden argument*/NULL);
		TextWriter_t4027217640 * L_76 = ___outText0;
		HtmlDocument_t556432108 * L_77 = __this->get__ownerdocument_19();
		HtmlNodeCollection_t2542734491 * L_78 = HtmlNode_get_ChildNodes_m1556126725(__this, /*hidden argument*/NULL);
		NullCheck(L_78);
		int32_t L_79 = HtmlNodeCollection_get_Count_m3249441714(L_78, /*hidden argument*/NULL);
		V_12 = L_79;
		String_t* L_80 = Int32_ToString_m2960866144((&V_12), /*hidden argument*/NULL);
		NullCheck(L_77);
		HtmlAttribute_t1804523403 * L_81 = HtmlDocument_CreateAttribute_m4240580628(L_77, _stringLiteral1029384328, L_80, /*hidden argument*/NULL);
		HtmlNode_WriteAttribute_m3827491122(__this, L_76, L_81, /*hidden argument*/NULL);
	}

IL_0216:
	{
		return;
	}
}
// System.Void HtmlAgilityPack.HtmlNodeCollection::.ctor(HtmlAgilityPack.HtmlNode)
extern "C"  void HtmlNodeCollection__ctor_m429744290 (HtmlNodeCollection_t2542734491 * __this, HtmlNode_t2048434459 * ___parentnode0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlNodeCollection__ctor_m429744290_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1417555591 * L_0 = (List_1_t1417555591 *)il2cpp_codegen_object_new(List_1_t1417555591_il2cpp_TypeInfo_var);
		List_1__ctor_m3864602216(L_0, /*hidden argument*/List_1__ctor_m3864602216_RuntimeMethod_var);
		__this->set__items_1(L_0);
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		HtmlNode_t2048434459 * L_1 = ___parentnode0;
		__this->set__parentnode_0(L_1);
		return;
	}
}
// System.Int32 HtmlAgilityPack.HtmlNodeCollection::get_Item(HtmlAgilityPack.HtmlNode)
extern "C"  int32_t HtmlNodeCollection_get_Item_m1795494390 (HtmlNodeCollection_t2542734491 * __this, HtmlNode_t2048434459 * ___node0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlNodeCollection_get_Item_m1795494390_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		HtmlNode_t2048434459 * L_0 = ___node0;
		int32_t L_1 = HtmlNodeCollection_GetNodeIndex_m1699874364(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_0032;
		}
	}
	{
		HtmlNode_t2048434459 * L_3 = ___node0;
		NullCheck(L_3);
		HtmlNode_t2048434459 * L_4 = HtmlNode_CloneNode_m2027347983(L_3, (bool)0, /*hidden argument*/NULL);
		NullCheck(L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String HtmlAgilityPack.HtmlNode::get_OuterHtml() */, L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral3458327600, L_5, _stringLiteral2190564348, /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t279959794 * L_7 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m4234257711(L_7, _stringLiteral1414245146, L_6, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_0032:
	{
		int32_t L_8 = V_0;
		return L_8;
	}
}
// System.Int32 HtmlAgilityPack.HtmlNodeCollection::get_Count()
extern "C"  int32_t HtmlNodeCollection_get_Count_m3249441714 (HtmlNodeCollection_t2542734491 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlNodeCollection_get_Count_m3249441714_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1417555591 * L_0 = __this->get__items_1();
		NullCheck(L_0);
		int32_t L_1 = List_1_get_Count_m3121849784(L_0, /*hidden argument*/List_1_get_Count_m3121849784_RuntimeMethod_var);
		return L_1;
	}
}
// System.Boolean HtmlAgilityPack.HtmlNodeCollection::get_IsReadOnly()
extern "C"  bool HtmlNodeCollection_get_IsReadOnly_m3389690231 (HtmlNodeCollection_t2542734491 * __this, const RuntimeMethod* method)
{
	{
		return (bool)0;
	}
}
// HtmlAgilityPack.HtmlNode HtmlAgilityPack.HtmlNodeCollection::get_Item(System.Int32)
extern "C"  HtmlNode_t2048434459 * HtmlNodeCollection_get_Item_m2496957206 (HtmlNodeCollection_t2542734491 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlNodeCollection_get_Item_m2496957206_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1417555591 * L_0 = __this->get__items_1();
		int32_t L_1 = ___index0;
		NullCheck(L_0);
		HtmlNode_t2048434459 * L_2 = List_1_get_Item_m3127542065(L_0, L_1, /*hidden argument*/List_1_get_Item_m3127542065_RuntimeMethod_var);
		return L_2;
	}
}
// System.Void HtmlAgilityPack.HtmlNodeCollection::set_Item(System.Int32,HtmlAgilityPack.HtmlNode)
extern "C"  void HtmlNodeCollection_set_Item_m981694791 (HtmlNodeCollection_t2542734491 * __this, int32_t ___index0, HtmlNode_t2048434459 * ___value1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlNodeCollection_set_Item_m981694791_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1417555591 * L_0 = __this->get__items_1();
		int32_t L_1 = ___index0;
		HtmlNode_t2048434459 * L_2 = ___value1;
		NullCheck(L_0);
		List_1_set_Item_m1335896754(L_0, L_1, L_2, /*hidden argument*/List_1_set_Item_m1335896754_RuntimeMethod_var);
		return;
	}
}
// System.Void HtmlAgilityPack.HtmlNodeCollection::Add(HtmlAgilityPack.HtmlNode)
extern "C"  void HtmlNodeCollection_Add_m2652620775 (HtmlNodeCollection_t2542734491 * __this, HtmlNode_t2048434459 * ___node0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlNodeCollection_Add_m2652620775_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1417555591 * L_0 = __this->get__items_1();
		HtmlNode_t2048434459 * L_1 = ___node0;
		NullCheck(L_0);
		List_1_Add_m2851003676(L_0, L_1, /*hidden argument*/List_1_Add_m2851003676_RuntimeMethod_var);
		return;
	}
}
// System.Void HtmlAgilityPack.HtmlNodeCollection::Clear()
extern "C"  void HtmlNodeCollection_Clear_m3979270339 (HtmlNodeCollection_t2542734491 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlNodeCollection_Clear_m3979270339_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	HtmlNode_t2048434459 * V_0 = NULL;
	Enumerator_t952285265  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t1417555591 * L_0 = __this->get__items_1();
		NullCheck(L_0);
		Enumerator_t952285265  L_1 = List_1_GetEnumerator_m2960830019(L_0, /*hidden argument*/List_1_GetEnumerator_m2960830019_RuntimeMethod_var);
		V_1 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_002b;
		}

IL_000e:
		{
			HtmlNode_t2048434459 * L_2 = Enumerator_get_Current_m1132692067((&V_1), /*hidden argument*/Enumerator_get_Current_m1132692067_RuntimeMethod_var);
			V_0 = L_2;
			HtmlNode_t2048434459 * L_3 = V_0;
			NullCheck(L_3);
			HtmlNode_set_ParentNode_m3958666249(L_3, (HtmlNode_t2048434459 *)NULL, /*hidden argument*/NULL);
			HtmlNode_t2048434459 * L_4 = V_0;
			NullCheck(L_4);
			HtmlNode_set_NextSibling_m3096270636(L_4, (HtmlNode_t2048434459 *)NULL, /*hidden argument*/NULL);
			HtmlNode_t2048434459 * L_5 = V_0;
			NullCheck(L_5);
			HtmlNode_set_PreviousSibling_m3540866112(L_5, (HtmlNode_t2048434459 *)NULL, /*hidden argument*/NULL);
		}

IL_002b:
		{
			bool L_6 = Enumerator_MoveNext_m2573550943((&V_1), /*hidden argument*/Enumerator_MoveNext_m2573550943_RuntimeMethod_var);
			if (L_6)
			{
				goto IL_000e;
			}
		}

IL_0034:
		{
			IL2CPP_LEAVE(0x44, FINALLY_0036);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0036;
	}

FINALLY_0036:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m850670244((&V_1), /*hidden argument*/Enumerator_Dispose_m850670244_RuntimeMethod_var);
		IL2CPP_END_FINALLY(54)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(54)
	{
		IL2CPP_JUMP_TBL(0x44, IL_0044)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0044:
	{
		List_1_t1417555591 * L_7 = __this->get__items_1();
		NullCheck(L_7);
		List_1_Clear_m1310845251(L_7, /*hidden argument*/List_1_Clear_m1310845251_RuntimeMethod_var);
		return;
	}
}
// System.Boolean HtmlAgilityPack.HtmlNodeCollection::Contains(HtmlAgilityPack.HtmlNode)
extern "C"  bool HtmlNodeCollection_Contains_m984356797 (HtmlNodeCollection_t2542734491 * __this, HtmlNode_t2048434459 * ___item0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlNodeCollection_Contains_m984356797_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1417555591 * L_0 = __this->get__items_1();
		HtmlNode_t2048434459 * L_1 = ___item0;
		NullCheck(L_0);
		bool L_2 = List_1_Contains_m530296848(L_0, L_1, /*hidden argument*/List_1_Contains_m530296848_RuntimeMethod_var);
		return L_2;
	}
}
// System.Void HtmlAgilityPack.HtmlNodeCollection::CopyTo(HtmlAgilityPack.HtmlNode[],System.Int32)
extern "C"  void HtmlNodeCollection_CopyTo_m3586893767 (HtmlNodeCollection_t2542734491 * __this, HtmlNodeU5BU5D_t1365436442* ___array0, int32_t ___arrayIndex1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlNodeCollection_CopyTo_m3586893767_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1417555591 * L_0 = __this->get__items_1();
		HtmlNodeU5BU5D_t1365436442* L_1 = ___array0;
		int32_t L_2 = ___arrayIndex1;
		NullCheck(L_0);
		List_1_CopyTo_m3109553416(L_0, L_1, L_2, /*hidden argument*/List_1_CopyTo_m3109553416_RuntimeMethod_var);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<HtmlAgilityPack.HtmlNode> HtmlAgilityPack.HtmlNodeCollection::System.Collections.Generic.IEnumerable<HtmlAgilityPack.HtmlNode>.GetEnumerator()
extern "C"  RuntimeObject* HtmlNodeCollection_System_Collections_Generic_IEnumerableU3CHtmlAgilityPack_HtmlNodeU3E_GetEnumerator_m2789742012 (HtmlNodeCollection_t2542734491 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlNodeCollection_System_Collections_Generic_IEnumerableU3CHtmlAgilityPack_HtmlNodeU3E_GetEnumerator_m2789742012_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1417555591 * L_0 = __this->get__items_1();
		NullCheck(L_0);
		Enumerator_t952285265  L_1 = List_1_GetEnumerator_m2960830019(L_0, /*hidden argument*/List_1_GetEnumerator_m2960830019_RuntimeMethod_var);
		Enumerator_t952285265  L_2 = L_1;
		RuntimeObject * L_3 = Box(Enumerator_t952285265_il2cpp_TypeInfo_var, &L_2);
		return (RuntimeObject*)L_3;
	}
}
// System.Collections.IEnumerator HtmlAgilityPack.HtmlNodeCollection::System.Collections.IEnumerable.GetEnumerator()
extern "C"  RuntimeObject* HtmlNodeCollection_System_Collections_IEnumerable_GetEnumerator_m1953627531 (HtmlNodeCollection_t2542734491 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlNodeCollection_System_Collections_IEnumerable_GetEnumerator_m1953627531_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1417555591 * L_0 = __this->get__items_1();
		NullCheck(L_0);
		Enumerator_t952285265  L_1 = List_1_GetEnumerator_m2960830019(L_0, /*hidden argument*/List_1_GetEnumerator_m2960830019_RuntimeMethod_var);
		Enumerator_t952285265  L_2 = L_1;
		RuntimeObject * L_3 = Box(Enumerator_t952285265_il2cpp_TypeInfo_var, &L_2);
		return (RuntimeObject*)L_3;
	}
}
// System.Int32 HtmlAgilityPack.HtmlNodeCollection::IndexOf(HtmlAgilityPack.HtmlNode)
extern "C"  int32_t HtmlNodeCollection_IndexOf_m2163239543 (HtmlNodeCollection_t2542734491 * __this, HtmlNode_t2048434459 * ___item0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlNodeCollection_IndexOf_m2163239543_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1417555591 * L_0 = __this->get__items_1();
		HtmlNode_t2048434459 * L_1 = ___item0;
		NullCheck(L_0);
		int32_t L_2 = List_1_IndexOf_m1714659438(L_0, L_1, /*hidden argument*/List_1_IndexOf_m1714659438_RuntimeMethod_var);
		return L_2;
	}
}
// System.Void HtmlAgilityPack.HtmlNodeCollection::Insert(System.Int32,HtmlAgilityPack.HtmlNode)
extern "C"  void HtmlNodeCollection_Insert_m1187451694 (HtmlNodeCollection_t2542734491 * __this, int32_t ___index0, HtmlNode_t2048434459 * ___node1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlNodeCollection_Insert_m1187451694_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	HtmlNode_t2048434459 * V_0 = NULL;
	HtmlNode_t2048434459 * V_1 = NULL;
	{
		V_0 = (HtmlNode_t2048434459 *)NULL;
		V_1 = (HtmlNode_t2048434459 *)NULL;
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_0017;
		}
	}
	{
		List_1_t1417555591 * L_1 = __this->get__items_1();
		int32_t L_2 = ___index0;
		NullCheck(L_1);
		HtmlNode_t2048434459 * L_3 = List_1_get_Item_m3127542065(L_1, ((int32_t)((int32_t)L_2-(int32_t)1)), /*hidden argument*/List_1_get_Item_m3127542065_RuntimeMethod_var);
		V_1 = L_3;
	}

IL_0017:
	{
		int32_t L_4 = ___index0;
		List_1_t1417555591 * L_5 = __this->get__items_1();
		NullCheck(L_5);
		int32_t L_6 = List_1_get_Count_m3121849784(L_5, /*hidden argument*/List_1_get_Count_m3121849784_RuntimeMethod_var);
		if ((((int32_t)L_4) >= ((int32_t)L_6)))
		{
			goto IL_0032;
		}
	}
	{
		List_1_t1417555591 * L_7 = __this->get__items_1();
		int32_t L_8 = ___index0;
		NullCheck(L_7);
		HtmlNode_t2048434459 * L_9 = List_1_get_Item_m3127542065(L_7, L_8, /*hidden argument*/List_1_get_Item_m3127542065_RuntimeMethod_var);
		V_0 = L_9;
	}

IL_0032:
	{
		List_1_t1417555591 * L_10 = __this->get__items_1();
		int32_t L_11 = ___index0;
		HtmlNode_t2048434459 * L_12 = ___node1;
		NullCheck(L_10);
		List_1_Insert_m1073570933(L_10, L_11, L_12, /*hidden argument*/List_1_Insert_m1073570933_RuntimeMethod_var);
		HtmlNode_t2048434459 * L_13 = V_1;
		if (!L_13)
		{
			goto IL_0058;
		}
	}
	{
		HtmlNode_t2048434459 * L_14 = ___node1;
		HtmlNode_t2048434459 * L_15 = V_1;
		if ((!(((RuntimeObject*)(HtmlNode_t2048434459 *)L_14) == ((RuntimeObject*)(HtmlNode_t2048434459 *)L_15))))
		{
			goto IL_0051;
		}
	}
	{
		InvalidProgramException_t3776992292 * L_16 = (InvalidProgramException_t3776992292 *)il2cpp_codegen_object_new(InvalidProgramException_t3776992292_il2cpp_TypeInfo_var);
		InvalidProgramException__ctor_m4163582411(L_16, _stringLiteral757822431, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_16);
	}

IL_0051:
	{
		HtmlNode_t2048434459 * L_17 = V_1;
		HtmlNode_t2048434459 * L_18 = ___node1;
		NullCheck(L_17);
		L_17->set__nextnode_12(L_18);
	}

IL_0058:
	{
		HtmlNode_t2048434459 * L_19 = V_0;
		if (!L_19)
		{
			goto IL_0062;
		}
	}
	{
		HtmlNode_t2048434459 * L_20 = V_0;
		HtmlNode_t2048434459 * L_21 = ___node1;
		NullCheck(L_20);
		L_20->set__prevnode_21(L_21);
	}

IL_0062:
	{
		HtmlNode_t2048434459 * L_22 = ___node1;
		HtmlNode_t2048434459 * L_23 = V_1;
		NullCheck(L_22);
		L_22->set__prevnode_21(L_23);
		HtmlNode_t2048434459 * L_24 = V_0;
		HtmlNode_t2048434459 * L_25 = ___node1;
		if ((!(((RuntimeObject*)(HtmlNode_t2048434459 *)L_24) == ((RuntimeObject*)(HtmlNode_t2048434459 *)L_25))))
		{
			goto IL_0078;
		}
	}
	{
		InvalidProgramException_t3776992292 * L_26 = (InvalidProgramException_t3776992292 *)il2cpp_codegen_object_new(InvalidProgramException_t3776992292_il2cpp_TypeInfo_var);
		InvalidProgramException__ctor_m4163582411(L_26, _stringLiteral757822431, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_26);
	}

IL_0078:
	{
		HtmlNode_t2048434459 * L_27 = ___node1;
		HtmlNode_t2048434459 * L_28 = V_0;
		NullCheck(L_27);
		L_27->set__nextnode_12(L_28);
		HtmlNode_t2048434459 * L_29 = ___node1;
		HtmlNode_t2048434459 * L_30 = __this->get__parentnode_0();
		NullCheck(L_29);
		L_29->set__parentnode_20(L_30);
		return;
	}
}
// System.Boolean HtmlAgilityPack.HtmlNodeCollection::Remove(HtmlAgilityPack.HtmlNode)
extern "C"  bool HtmlNodeCollection_Remove_m4176503848 (HtmlNodeCollection_t2542734491 * __this, HtmlNode_t2048434459 * ___item0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlNodeCollection_Remove_m4176503848_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		List_1_t1417555591 * L_0 = __this->get__items_1();
		HtmlNode_t2048434459 * L_1 = ___item0;
		NullCheck(L_0);
		int32_t L_2 = List_1_IndexOf_m1714659438(L_0, L_1, /*hidden argument*/List_1_IndexOf_m1714659438_RuntimeMethod_var);
		V_0 = L_2;
		int32_t L_3 = V_0;
		HtmlNodeCollection_RemoveAt_m3305657552(__this, L_3, /*hidden argument*/NULL);
		return (bool)1;
	}
}
// System.Void HtmlAgilityPack.HtmlNodeCollection::RemoveAt(System.Int32)
extern "C"  void HtmlNodeCollection_RemoveAt_m3305657552 (HtmlNodeCollection_t2542734491 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlNodeCollection_RemoveAt_m3305657552_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	HtmlNode_t2048434459 * V_0 = NULL;
	HtmlNode_t2048434459 * V_1 = NULL;
	HtmlNode_t2048434459 * V_2 = NULL;
	{
		V_0 = (HtmlNode_t2048434459 *)NULL;
		V_1 = (HtmlNode_t2048434459 *)NULL;
		List_1_t1417555591 * L_0 = __this->get__items_1();
		int32_t L_1 = ___index0;
		NullCheck(L_0);
		HtmlNode_t2048434459 * L_2 = List_1_get_Item_m3127542065(L_0, L_1, /*hidden argument*/List_1_get_Item_m3127542065_RuntimeMethod_var);
		V_2 = L_2;
		int32_t L_3 = ___index0;
		if ((((int32_t)L_3) <= ((int32_t)0)))
		{
			goto IL_0024;
		}
	}
	{
		List_1_t1417555591 * L_4 = __this->get__items_1();
		int32_t L_5 = ___index0;
		NullCheck(L_4);
		HtmlNode_t2048434459 * L_6 = List_1_get_Item_m3127542065(L_4, ((int32_t)((int32_t)L_5-(int32_t)1)), /*hidden argument*/List_1_get_Item_m3127542065_RuntimeMethod_var);
		V_1 = L_6;
	}

IL_0024:
	{
		int32_t L_7 = ___index0;
		List_1_t1417555591 * L_8 = __this->get__items_1();
		NullCheck(L_8);
		int32_t L_9 = List_1_get_Count_m3121849784(L_8, /*hidden argument*/List_1_get_Count_m3121849784_RuntimeMethod_var);
		if ((((int32_t)L_7) >= ((int32_t)((int32_t)((int32_t)L_9-(int32_t)1)))))
		{
			goto IL_0043;
		}
	}
	{
		List_1_t1417555591 * L_10 = __this->get__items_1();
		int32_t L_11 = ___index0;
		NullCheck(L_10);
		HtmlNode_t2048434459 * L_12 = List_1_get_Item_m3127542065(L_10, ((int32_t)((int32_t)L_11+(int32_t)1)), /*hidden argument*/List_1_get_Item_m3127542065_RuntimeMethod_var);
		V_0 = L_12;
	}

IL_0043:
	{
		List_1_t1417555591 * L_13 = __this->get__items_1();
		int32_t L_14 = ___index0;
		NullCheck(L_13);
		List_1_RemoveAt_m1889740798(L_13, L_14, /*hidden argument*/List_1_RemoveAt_m1889740798_RuntimeMethod_var);
		HtmlNode_t2048434459 * L_15 = V_1;
		if (!L_15)
		{
			goto IL_0068;
		}
	}
	{
		HtmlNode_t2048434459 * L_16 = V_0;
		HtmlNode_t2048434459 * L_17 = V_1;
		if ((!(((RuntimeObject*)(HtmlNode_t2048434459 *)L_16) == ((RuntimeObject*)(HtmlNode_t2048434459 *)L_17))))
		{
			goto IL_0061;
		}
	}
	{
		InvalidProgramException_t3776992292 * L_18 = (InvalidProgramException_t3776992292 *)il2cpp_codegen_object_new(InvalidProgramException_t3776992292_il2cpp_TypeInfo_var);
		InvalidProgramException__ctor_m4163582411(L_18, _stringLiteral757822431, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_18);
	}

IL_0061:
	{
		HtmlNode_t2048434459 * L_19 = V_1;
		HtmlNode_t2048434459 * L_20 = V_0;
		NullCheck(L_19);
		L_19->set__nextnode_12(L_20);
	}

IL_0068:
	{
		HtmlNode_t2048434459 * L_21 = V_0;
		if (!L_21)
		{
			goto IL_0072;
		}
	}
	{
		HtmlNode_t2048434459 * L_22 = V_0;
		HtmlNode_t2048434459 * L_23 = V_1;
		NullCheck(L_22);
		L_22->set__prevnode_21(L_23);
	}

IL_0072:
	{
		HtmlNode_t2048434459 * L_24 = V_2;
		NullCheck(L_24);
		L_24->set__prevnode_21((HtmlNode_t2048434459 *)NULL);
		HtmlNode_t2048434459 * L_25 = V_2;
		NullCheck(L_25);
		L_25->set__nextnode_12((HtmlNode_t2048434459 *)NULL);
		HtmlNode_t2048434459 * L_26 = V_2;
		NullCheck(L_26);
		L_26->set__parentnode_20((HtmlNode_t2048434459 *)NULL);
		return;
	}
}
// System.Void HtmlAgilityPack.HtmlNodeCollection::Append(HtmlAgilityPack.HtmlNode)
extern "C"  void HtmlNodeCollection_Append_m717126292 (HtmlNodeCollection_t2542734491 * __this, HtmlNode_t2048434459 * ___node0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlNodeCollection_Append_m717126292_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	HtmlNode_t2048434459 * V_0 = NULL;
	{
		V_0 = (HtmlNode_t2048434459 *)NULL;
		List_1_t1417555591 * L_0 = __this->get__items_1();
		NullCheck(L_0);
		int32_t L_1 = List_1_get_Count_m3121849784(L_0, /*hidden argument*/List_1_get_Count_m3121849784_RuntimeMethod_var);
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_0029;
		}
	}
	{
		List_1_t1417555591 * L_2 = __this->get__items_1();
		List_1_t1417555591 * L_3 = __this->get__items_1();
		NullCheck(L_3);
		int32_t L_4 = List_1_get_Count_m3121849784(L_3, /*hidden argument*/List_1_get_Count_m3121849784_RuntimeMethod_var);
		NullCheck(L_2);
		HtmlNode_t2048434459 * L_5 = List_1_get_Item_m3127542065(L_2, ((int32_t)((int32_t)L_4-(int32_t)1)), /*hidden argument*/List_1_get_Item_m3127542065_RuntimeMethod_var);
		V_0 = L_5;
	}

IL_0029:
	{
		List_1_t1417555591 * L_6 = __this->get__items_1();
		HtmlNode_t2048434459 * L_7 = ___node0;
		NullCheck(L_6);
		List_1_Add_m2851003676(L_6, L_7, /*hidden argument*/List_1_Add_m2851003676_RuntimeMethod_var);
		HtmlNode_t2048434459 * L_8 = ___node0;
		HtmlNode_t2048434459 * L_9 = V_0;
		NullCheck(L_8);
		L_8->set__prevnode_21(L_9);
		HtmlNode_t2048434459 * L_10 = ___node0;
		NullCheck(L_10);
		L_10->set__nextnode_12((HtmlNode_t2048434459 *)NULL);
		HtmlNode_t2048434459 * L_11 = ___node0;
		HtmlNode_t2048434459 * L_12 = __this->get__parentnode_0();
		NullCheck(L_11);
		L_11->set__parentnode_20(L_12);
		HtmlNode_t2048434459 * L_13 = V_0;
		if (L_13)
		{
			goto IL_0053;
		}
	}
	{
		return;
	}

IL_0053:
	{
		HtmlNode_t2048434459 * L_14 = V_0;
		HtmlNode_t2048434459 * L_15 = ___node0;
		if ((!(((RuntimeObject*)(HtmlNode_t2048434459 *)L_14) == ((RuntimeObject*)(HtmlNode_t2048434459 *)L_15))))
		{
			goto IL_0062;
		}
	}
	{
		InvalidProgramException_t3776992292 * L_16 = (InvalidProgramException_t3776992292 *)il2cpp_codegen_object_new(InvalidProgramException_t3776992292_il2cpp_TypeInfo_var);
		InvalidProgramException__ctor_m4163582411(L_16, _stringLiteral757822431, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_16);
	}

IL_0062:
	{
		HtmlNode_t2048434459 * L_17 = V_0;
		HtmlNode_t2048434459 * L_18 = ___node0;
		NullCheck(L_17);
		L_17->set__nextnode_12(L_18);
		return;
	}
}
// System.Int32 HtmlAgilityPack.HtmlNodeCollection::GetNodeIndex(HtmlAgilityPack.HtmlNode)
extern "C"  int32_t HtmlNodeCollection_GetNodeIndex_m1699874364 (HtmlNodeCollection_t2542734491 * __this, HtmlNode_t2048434459 * ___node0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlNodeCollection_GetNodeIndex_m1699874364_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0019;
	}

IL_0004:
	{
		HtmlNode_t2048434459 * L_0 = ___node0;
		List_1_t1417555591 * L_1 = __this->get__items_1();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		HtmlNode_t2048434459 * L_3 = List_1_get_Item_m3127542065(L_1, L_2, /*hidden argument*/List_1_get_Item_m3127542065_RuntimeMethod_var);
		if ((!(((RuntimeObject*)(HtmlNode_t2048434459 *)L_0) == ((RuntimeObject*)(HtmlNode_t2048434459 *)L_3))))
		{
			goto IL_0015;
		}
	}
	{
		int32_t L_4 = V_0;
		return L_4;
	}

IL_0015:
	{
		int32_t L_5 = V_0;
		V_0 = ((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_0019:
	{
		int32_t L_6 = V_0;
		List_1_t1417555591 * L_7 = __this->get__items_1();
		NullCheck(L_7);
		int32_t L_8 = List_1_get_Count_m3121849784(L_7, /*hidden argument*/List_1_get_Count_m3121849784_RuntimeMethod_var);
		if ((((int32_t)L_6) < ((int32_t)L_8)))
		{
			goto IL_0004;
		}
	}
	{
		return (-1);
	}
}
// System.Boolean HtmlAgilityPack.HtmlNodeCollection::Remove(System.Int32)
extern "C"  bool HtmlNodeCollection_Remove_m46965641 (HtmlNodeCollection_t2542734491 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		HtmlNodeCollection_RemoveAt_m3305657552(__this, L_0, /*hidden argument*/NULL);
		return (bool)1;
	}
}
// System.Void HtmlAgilityPack.HtmlNodeNavigator::.ctor(HtmlAgilityPack.HtmlDocument,HtmlAgilityPack.HtmlNode)
extern "C"  void HtmlNodeNavigator__ctor_m1439946702 (HtmlNodeNavigator_t2752606360 * __this, HtmlDocument_t556432108 * ___doc0, HtmlNode_t2048434459 * ___currentNode1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlNodeNavigator__ctor_m1439946702_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		HtmlDocument_t556432108 * L_0 = (HtmlDocument_t556432108 *)il2cpp_codegen_object_new(HtmlDocument_t556432108_il2cpp_TypeInfo_var);
		HtmlDocument__ctor_m2934097387(L_0, /*hidden argument*/NULL);
		__this->set__doc_4(L_0);
		HtmlNameTable_t3848610378 * L_1 = (HtmlNameTable_t3848610378 *)il2cpp_codegen_object_new(HtmlNameTable_t3848610378_il2cpp_TypeInfo_var);
		HtmlNameTable__ctor_m4091390005(L_1, /*hidden argument*/NULL);
		__this->set__nametable_5(L_1);
		IL2CPP_RUNTIME_CLASS_INIT(XPathNavigator_t3981235968_il2cpp_TypeInfo_var);
		XPathNavigator__ctor_m3760155520(__this, /*hidden argument*/NULL);
		HtmlNode_t2048434459 * L_2 = ___currentNode1;
		if (L_2)
		{
			goto IL_002a;
		}
	}
	{
		ArgumentNullException_t628810857 * L_3 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_3, _stringLiteral4293744237, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002a:
	{
		HtmlNode_t2048434459 * L_4 = ___currentNode1;
		NullCheck(L_4);
		HtmlDocument_t556432108 * L_5 = HtmlNode_get_OwnerDocument_m2055696179(L_4, /*hidden argument*/NULL);
		HtmlDocument_t556432108 * L_6 = ___doc0;
		if ((((RuntimeObject*)(HtmlDocument_t556432108 *)L_5) == ((RuntimeObject*)(HtmlDocument_t556432108 *)L_6)))
		{
			goto IL_003e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(HtmlDocument_t556432108_il2cpp_TypeInfo_var);
		String_t* L_7 = ((HtmlDocument_t556432108_StaticFields*)il2cpp_codegen_static_fields_for(HtmlDocument_t556432108_il2cpp_TypeInfo_var))->get_HtmlExceptionRefNotChild_39();
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_8, L_7, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_003e:
	{
		HtmlDocument_t556432108 * L_9 = ___doc0;
		__this->set__doc_4(L_9);
		HtmlNodeNavigator_Reset_m1651173616(__this, /*hidden argument*/NULL);
		HtmlNode_t2048434459 * L_10 = ___currentNode1;
		__this->set__currentnode_3(L_10);
		return;
	}
}
// System.Void HtmlAgilityPack.HtmlNodeNavigator::.ctor(HtmlAgilityPack.HtmlNodeNavigator)
extern "C"  void HtmlNodeNavigator__ctor_m2984146728 (HtmlNodeNavigator_t2752606360 * __this, HtmlNodeNavigator_t2752606360 * ___nav0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlNodeNavigator__ctor_m2984146728_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		HtmlDocument_t556432108 * L_0 = (HtmlDocument_t556432108 *)il2cpp_codegen_object_new(HtmlDocument_t556432108_il2cpp_TypeInfo_var);
		HtmlDocument__ctor_m2934097387(L_0, /*hidden argument*/NULL);
		__this->set__doc_4(L_0);
		HtmlNameTable_t3848610378 * L_1 = (HtmlNameTable_t3848610378 *)il2cpp_codegen_object_new(HtmlNameTable_t3848610378_il2cpp_TypeInfo_var);
		HtmlNameTable__ctor_m4091390005(L_1, /*hidden argument*/NULL);
		__this->set__nametable_5(L_1);
		IL2CPP_RUNTIME_CLASS_INIT(XPathNavigator_t3981235968_il2cpp_TypeInfo_var);
		XPathNavigator__ctor_m3760155520(__this, /*hidden argument*/NULL);
		HtmlNodeNavigator_t2752606360 * L_2 = ___nav0;
		if (L_2)
		{
			goto IL_002a;
		}
	}
	{
		ArgumentNullException_t628810857 * L_3 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_3, _stringLiteral696029617, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002a:
	{
		HtmlNodeNavigator_t2752606360 * L_4 = ___nav0;
		NullCheck(L_4);
		HtmlDocument_t556432108 * L_5 = L_4->get__doc_4();
		__this->set__doc_4(L_5);
		HtmlNodeNavigator_t2752606360 * L_6 = ___nav0;
		NullCheck(L_6);
		HtmlNode_t2048434459 * L_7 = L_6->get__currentnode_3();
		__this->set__currentnode_3(L_7);
		HtmlNodeNavigator_t2752606360 * L_8 = ___nav0;
		NullCheck(L_8);
		int32_t L_9 = L_8->get__attindex_2();
		__this->set__attindex_2(L_9);
		HtmlNodeNavigator_t2752606360 * L_10 = ___nav0;
		NullCheck(L_10);
		HtmlNameTable_t3848610378 * L_11 = L_10->get__nametable_5();
		__this->set__nametable_5(L_11);
		return;
	}
}
// HtmlAgilityPack.HtmlNode HtmlAgilityPack.HtmlNodeNavigator::get_CurrentNode()
extern "C"  HtmlNode_t2048434459 * HtmlNodeNavigator_get_CurrentNode_m1848014476 (HtmlNodeNavigator_t2752606360 * __this, const RuntimeMethod* method)
{
	{
		HtmlNode_t2048434459 * L_0 = __this->get__currentnode_3();
		return L_0;
	}
}
// System.Boolean HtmlAgilityPack.HtmlNodeNavigator::get_HasAttributes()
extern "C"  bool HtmlNodeNavigator_get_HasAttributes_m2927900587 (HtmlNodeNavigator_t2752606360 * __this, const RuntimeMethod* method)
{
	{
		HtmlNode_t2048434459 * L_0 = __this->get__currentnode_3();
		NullCheck(L_0);
		HtmlAttributeCollection_t1787476631 * L_1 = HtmlNode_get_Attributes_m2935417207(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_2 = HtmlAttributeCollection_get_Count_m1097499500(L_1, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_2) > ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean HtmlAgilityPack.HtmlNodeNavigator::get_HasChildren()
extern "C"  bool HtmlNodeNavigator_get_HasChildren_m435615147 (HtmlNodeNavigator_t2752606360 * __this, const RuntimeMethod* method)
{
	{
		HtmlNode_t2048434459 * L_0 = __this->get__currentnode_3();
		NullCheck(L_0);
		HtmlNodeCollection_t2542734491 * L_1 = HtmlNode_get_ChildNodes_m1556126725(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_2 = HtmlNodeCollection_get_Count_m3249441714(L_1, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_2) > ((int32_t)0))? 1 : 0);
	}
}
// System.String HtmlAgilityPack.HtmlNodeNavigator::get_LocalName()
extern "C"  String_t* HtmlNodeNavigator_get_LocalName_m271976001 (HtmlNodeNavigator_t2752606360 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get__attindex_2();
		if ((((int32_t)L_0) == ((int32_t)(-1))))
		{
			goto IL_0030;
		}
	}
	{
		HtmlNameTable_t3848610378 * L_1 = __this->get__nametable_5();
		HtmlNode_t2048434459 * L_2 = __this->get__currentnode_3();
		NullCheck(L_2);
		HtmlAttributeCollection_t1787476631 * L_3 = HtmlNode_get_Attributes_m2935417207(L_2, /*hidden argument*/NULL);
		int32_t L_4 = __this->get__attindex_2();
		NullCheck(L_3);
		HtmlAttribute_t1804523403 * L_5 = HtmlAttributeCollection_get_Item_m4090043702(L_3, L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		String_t* L_6 = HtmlAttribute_get_Name_m3956631639(L_5, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_7 = HtmlNameTable_GetOrAdd_m3409817290(L_1, L_6, /*hidden argument*/NULL);
		return L_7;
	}

IL_0030:
	{
		HtmlNameTable_t3848610378 * L_8 = __this->get__nametable_5();
		HtmlNode_t2048434459 * L_9 = __this->get__currentnode_3();
		NullCheck(L_9);
		String_t* L_10 = HtmlNode_get_Name_m702134575(L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		String_t* L_11 = HtmlNameTable_GetOrAdd_m3409817290(L_8, L_10, /*hidden argument*/NULL);
		return L_11;
	}
}
// System.String HtmlAgilityPack.HtmlNodeNavigator::get_Name()
extern "C"  String_t* HtmlNodeNavigator_get_Name_m3597729874 (HtmlNodeNavigator_t2752606360 * __this, const RuntimeMethod* method)
{
	{
		HtmlNameTable_t3848610378 * L_0 = __this->get__nametable_5();
		HtmlNode_t2048434459 * L_1 = __this->get__currentnode_3();
		NullCheck(L_1);
		String_t* L_2 = HtmlNode_get_Name_m702134575(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_3 = HtmlNameTable_GetOrAdd_m3409817290(L_0, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.String HtmlAgilityPack.HtmlNodeNavigator::get_NamespaceURI()
extern "C"  String_t* HtmlNodeNavigator_get_NamespaceURI_m1178786970 (HtmlNodeNavigator_t2752606360 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlNodeNavigator_get_NamespaceURI_m1178786970_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		HtmlNameTable_t3848610378 * L_0 = __this->get__nametable_5();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		NullCheck(L_0);
		String_t* L_2 = HtmlNameTable_GetOrAdd_m3409817290(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Xml.XPath.XPathNodeType HtmlAgilityPack.HtmlNodeNavigator::get_NodeType()
extern "C"  int32_t HtmlNodeNavigator_get_NodeType_m3863942019 (HtmlNodeNavigator_t2752606360 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlNodeNavigator_get_NodeType_m3863942019_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		HtmlNode_t2048434459 * L_0 = __this->get__currentnode_3();
		NullCheck(L_0);
		int32_t L_1 = HtmlNode_get_NodeType_m270476988(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		switch (L_2)
		{
			case 0:
			{
				goto IL_0026;
			}
			case 1:
			{
				goto IL_002a;
			}
			case 2:
			{
				goto IL_0024;
			}
			case 3:
			{
				goto IL_0028;
			}
		}
	}
	{
		goto IL_0037;
	}

IL_0024:
	{
		return (int32_t)(8);
	}

IL_0026:
	{
		return (int32_t)(0);
	}

IL_0028:
	{
		return (int32_t)(4);
	}

IL_002a:
	{
		int32_t L_3 = __this->get__attindex_2();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0035;
		}
	}
	{
		return (int32_t)(2);
	}

IL_0035:
	{
		return (int32_t)(1);
	}

IL_0037:
	{
		HtmlNode_t2048434459 * L_4 = __this->get__currentnode_3();
		NullCheck(L_4);
		int32_t L_5 = HtmlNode_get_NodeType_m270476988(L_4, /*hidden argument*/NULL);
		int32_t L_6 = L_5;
		RuntimeObject * L_7 = Box(HtmlNodeType_t1941241835_il2cpp_TypeInfo_var, &L_6);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral3216010707, L_7, /*hidden argument*/NULL);
		NotImplementedException_t2785117854 * L_9 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1795163961(L_9, L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}
}
// System.String HtmlAgilityPack.HtmlNodeNavigator::get_Value()
extern "C"  String_t* HtmlNodeNavigator_get_Value_m1270508592 (HtmlNodeNavigator_t2752606360 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlNodeNavigator_get_Value_m1270508592_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		HtmlNode_t2048434459 * L_0 = __this->get__currentnode_3();
		NullCheck(L_0);
		int32_t L_1 = HtmlNode_get_NodeType_m270476988(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		switch (L_2)
		{
			case 0:
			{
				goto IL_0035;
			}
			case 1:
			{
				goto IL_004c;
			}
			case 2:
			{
				goto IL_0024;
			}
			case 3:
			{
				goto IL_003b;
			}
		}
	}
	{
		goto IL_007d;
	}

IL_0024:
	{
		HtmlNode_t2048434459 * L_3 = __this->get__currentnode_3();
		NullCheck(((HtmlCommentNode_t1992371332 *)CastclassClass((RuntimeObject*)L_3, HtmlCommentNode_t1992371332_il2cpp_TypeInfo_var)));
		String_t* L_4 = HtmlCommentNode_get_Comment_m3847846688(((HtmlCommentNode_t1992371332 *)CastclassClass((RuntimeObject*)L_3, HtmlCommentNode_t1992371332_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return L_4;
	}

IL_0035:
	{
		return _stringLiteral371857150;
	}

IL_003b:
	{
		HtmlNode_t2048434459 * L_5 = __this->get__currentnode_3();
		NullCheck(((HtmlTextNode_t2710098554 *)CastclassClass((RuntimeObject*)L_5, HtmlTextNode_t2710098554_il2cpp_TypeInfo_var)));
		String_t* L_6 = HtmlTextNode_get_Text_m1199112604(((HtmlTextNode_t2710098554 *)CastclassClass((RuntimeObject*)L_5, HtmlTextNode_t2710098554_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return L_6;
	}

IL_004c:
	{
		int32_t L_7 = __this->get__attindex_2();
		if ((((int32_t)L_7) == ((int32_t)(-1))))
		{
			goto IL_0071;
		}
	}
	{
		HtmlNode_t2048434459 * L_8 = __this->get__currentnode_3();
		NullCheck(L_8);
		HtmlAttributeCollection_t1787476631 * L_9 = HtmlNode_get_Attributes_m2935417207(L_8, /*hidden argument*/NULL);
		int32_t L_10 = __this->get__attindex_2();
		NullCheck(L_9);
		HtmlAttribute_t1804523403 * L_11 = HtmlAttributeCollection_get_Item_m4090043702(L_9, L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		String_t* L_12 = HtmlAttribute_get_Value_m3933521327(L_11, /*hidden argument*/NULL);
		return L_12;
	}

IL_0071:
	{
		HtmlNode_t2048434459 * L_13 = __this->get__currentnode_3();
		NullCheck(L_13);
		String_t* L_14 = VirtFuncInvoker0< String_t* >::Invoke(5 /* System.String HtmlAgilityPack.HtmlNode::get_InnerText() */, L_13);
		return L_14;
	}

IL_007d:
	{
		HtmlNode_t2048434459 * L_15 = __this->get__currentnode_3();
		NullCheck(L_15);
		int32_t L_16 = HtmlNode_get_NodeType_m270476988(L_15, /*hidden argument*/NULL);
		int32_t L_17 = L_16;
		RuntimeObject * L_18 = Box(HtmlNodeType_t1941241835_il2cpp_TypeInfo_var, &L_17);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral3216010707, L_18, /*hidden argument*/NULL);
		NotImplementedException_t2785117854 * L_20 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1795163961(L_20, L_19, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_20);
	}
}
// System.String HtmlAgilityPack.HtmlNodeNavigator::get_XmlLang()
extern "C"  String_t* HtmlNodeNavigator_get_XmlLang_m1732116456 (HtmlNodeNavigator_t2752606360 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlNodeNavigator_get_XmlLang_m1732116456_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		HtmlNameTable_t3848610378 * L_0 = __this->get__nametable_5();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		NullCheck(L_0);
		String_t* L_2 = HtmlNameTable_GetOrAdd_m3409817290(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Xml.XPath.XPathNavigator HtmlAgilityPack.HtmlNodeNavigator::Clone()
extern "C"  XPathNavigator_t3981235968 * HtmlNodeNavigator_Clone_m2948743814 (HtmlNodeNavigator_t2752606360 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlNodeNavigator_Clone_m2948743814_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		HtmlNodeNavigator_t2752606360 * L_0 = (HtmlNodeNavigator_t2752606360 *)il2cpp_codegen_object_new(HtmlNodeNavigator_t2752606360_il2cpp_TypeInfo_var);
		HtmlNodeNavigator__ctor_m2984146728(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean HtmlAgilityPack.HtmlNodeNavigator::IsSamePosition(System.Xml.XPath.XPathNavigator)
extern "C"  bool HtmlNodeNavigator_IsSamePosition_m26408627 (HtmlNodeNavigator_t2752606360 * __this, XPathNavigator_t3981235968 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlNodeNavigator_IsSamePosition_m26408627_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	HtmlNodeNavigator_t2752606360 * V_0 = NULL;
	{
		XPathNavigator_t3981235968 * L_0 = ___other0;
		V_0 = ((HtmlNodeNavigator_t2752606360 *)IsInstClass((RuntimeObject*)L_0, HtmlNodeNavigator_t2752606360_il2cpp_TypeInfo_var));
		HtmlNodeNavigator_t2752606360 * L_1 = V_0;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (bool)0;
	}

IL_000c:
	{
		HtmlNodeNavigator_t2752606360 * L_2 = V_0;
		NullCheck(L_2);
		HtmlNode_t2048434459 * L_3 = L_2->get__currentnode_3();
		HtmlNode_t2048434459 * L_4 = __this->get__currentnode_3();
		return (bool)((((RuntimeObject*)(HtmlNode_t2048434459 *)L_3) == ((RuntimeObject*)(HtmlNode_t2048434459 *)L_4))? 1 : 0);
	}
}
// System.Boolean HtmlAgilityPack.HtmlNodeNavigator::MoveTo(System.Xml.XPath.XPathNavigator)
extern "C"  bool HtmlNodeNavigator_MoveTo_m3713757330 (HtmlNodeNavigator_t2752606360 * __this, XPathNavigator_t3981235968 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlNodeNavigator_MoveTo_m3713757330_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	HtmlNodeNavigator_t2752606360 * V_0 = NULL;
	{
		XPathNavigator_t3981235968 * L_0 = ___other0;
		V_0 = ((HtmlNodeNavigator_t2752606360 *)IsInstClass((RuntimeObject*)L_0, HtmlNodeNavigator_t2752606360_il2cpp_TypeInfo_var));
		HtmlNodeNavigator_t2752606360 * L_1 = V_0;
		if (L_1)
		{
			goto IL_000c;
		}
	}
	{
		return (bool)0;
	}

IL_000c:
	{
		HtmlNodeNavigator_t2752606360 * L_2 = V_0;
		NullCheck(L_2);
		HtmlDocument_t556432108 * L_3 = L_2->get__doc_4();
		HtmlDocument_t556432108 * L_4 = __this->get__doc_4();
		if ((!(((RuntimeObject*)(HtmlDocument_t556432108 *)L_3) == ((RuntimeObject*)(HtmlDocument_t556432108 *)L_4))))
		{
			goto IL_0034;
		}
	}
	{
		HtmlNodeNavigator_t2752606360 * L_5 = V_0;
		NullCheck(L_5);
		HtmlNode_t2048434459 * L_6 = L_5->get__currentnode_3();
		__this->set__currentnode_3(L_6);
		HtmlNodeNavigator_t2752606360 * L_7 = V_0;
		NullCheck(L_7);
		int32_t L_8 = L_7->get__attindex_2();
		__this->set__attindex_2(L_8);
		return (bool)1;
	}

IL_0034:
	{
		return (bool)0;
	}
}
// System.Boolean HtmlAgilityPack.HtmlNodeNavigator::MoveToAttribute(System.String,System.String)
extern "C"  bool HtmlNodeNavigator_MoveToAttribute_m1623090033 (HtmlNodeNavigator_t2752606360 * __this, String_t* ___localName0, String_t* ___namespaceURI1, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		HtmlNode_t2048434459 * L_0 = __this->get__currentnode_3();
		NullCheck(L_0);
		HtmlAttributeCollection_t1787476631 * L_1 = HtmlNode_get_Attributes_m2935417207(L_0, /*hidden argument*/NULL);
		String_t* L_2 = ___localName0;
		NullCheck(L_1);
		int32_t L_3 = HtmlAttributeCollection_GetAttributeIndex_m1301279406(L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = V_0;
		if ((!(((uint32_t)L_4) == ((uint32_t)(-1)))))
		{
			goto IL_0018;
		}
	}
	{
		return (bool)0;
	}

IL_0018:
	{
		int32_t L_5 = V_0;
		__this->set__attindex_2(L_5);
		return (bool)1;
	}
}
// System.Boolean HtmlAgilityPack.HtmlNodeNavigator::MoveToFirst()
extern "C"  bool HtmlNodeNavigator_MoveToFirst_m4039668127 (HtmlNodeNavigator_t2752606360 * __this, const RuntimeMethod* method)
{
	{
		HtmlNode_t2048434459 * L_0 = __this->get__currentnode_3();
		NullCheck(L_0);
		HtmlNode_t2048434459 * L_1 = HtmlNode_get_ParentNode_m1888227040(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_000f;
		}
	}
	{
		return (bool)0;
	}

IL_000f:
	{
		HtmlNode_t2048434459 * L_2 = __this->get__currentnode_3();
		NullCheck(L_2);
		HtmlNode_t2048434459 * L_3 = HtmlNode_get_ParentNode_m1888227040(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		HtmlNode_t2048434459 * L_4 = HtmlNode_get_FirstChild_m2198425556(L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0023;
		}
	}
	{
		return (bool)0;
	}

IL_0023:
	{
		HtmlNode_t2048434459 * L_5 = __this->get__currentnode_3();
		NullCheck(L_5);
		HtmlNode_t2048434459 * L_6 = HtmlNode_get_ParentNode_m1888227040(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		HtmlNode_t2048434459 * L_7 = HtmlNode_get_FirstChild_m2198425556(L_6, /*hidden argument*/NULL);
		__this->set__currentnode_3(L_7);
		return (bool)1;
	}
}
// System.Boolean HtmlAgilityPack.HtmlNodeNavigator::MoveToFirstAttribute()
extern "C"  bool HtmlNodeNavigator_MoveToFirstAttribute_m4106923527 (HtmlNodeNavigator_t2752606360 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Xml.XPath.XPathNavigator::get_HasAttributes() */, __this);
		if (L_0)
		{
			goto IL_000a;
		}
	}
	{
		return (bool)0;
	}

IL_000a:
	{
		__this->set__attindex_2(0);
		return (bool)1;
	}
}
// System.Boolean HtmlAgilityPack.HtmlNodeNavigator::MoveToFirstChild()
extern "C"  bool HtmlNodeNavigator_MoveToFirstChild_m4030774257 (HtmlNodeNavigator_t2752606360 * __this, const RuntimeMethod* method)
{
	{
		HtmlNode_t2048434459 * L_0 = __this->get__currentnode_3();
		NullCheck(L_0);
		bool L_1 = HtmlNode_get_HasChildNodes_m1218535674(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_000f;
		}
	}
	{
		return (bool)0;
	}

IL_000f:
	{
		HtmlNode_t2048434459 * L_2 = __this->get__currentnode_3();
		NullCheck(L_2);
		HtmlNodeCollection_t2542734491 * L_3 = HtmlNode_get_ChildNodes_m1556126725(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		HtmlNode_t2048434459 * L_4 = HtmlNodeCollection_get_Item_m2496957206(L_3, 0, /*hidden argument*/NULL);
		__this->set__currentnode_3(L_4);
		return (bool)1;
	}
}
// System.Boolean HtmlAgilityPack.HtmlNodeNavigator::MoveToFirstNamespace(System.Xml.XPath.XPathNamespaceScope)
extern "C"  bool HtmlNodeNavigator_MoveToFirstNamespace_m2133831 (HtmlNodeNavigator_t2752606360 * __this, int32_t ___scope0, const RuntimeMethod* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean HtmlAgilityPack.HtmlNodeNavigator::MoveToId(System.String)
extern "C"  bool HtmlNodeNavigator_MoveToId_m3197380724 (HtmlNodeNavigator_t2752606360 * __this, String_t* ___id0, const RuntimeMethod* method)
{
	HtmlNode_t2048434459 * V_0 = NULL;
	{
		HtmlDocument_t556432108 * L_0 = __this->get__doc_4();
		String_t* L_1 = ___id0;
		NullCheck(L_0);
		HtmlNode_t2048434459 * L_2 = HtmlDocument_GetElementbyId_m1591120990(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		HtmlNode_t2048434459 * L_3 = V_0;
		if (L_3)
		{
			goto IL_0012;
		}
	}
	{
		return (bool)0;
	}

IL_0012:
	{
		HtmlNode_t2048434459 * L_4 = V_0;
		__this->set__currentnode_3(L_4);
		return (bool)1;
	}
}
// System.Boolean HtmlAgilityPack.HtmlNodeNavigator::MoveToNamespace(System.String)
extern "C"  bool HtmlNodeNavigator_MoveToNamespace_m536474158 (HtmlNodeNavigator_t2752606360 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean HtmlAgilityPack.HtmlNodeNavigator::MoveToNext()
extern "C"  bool HtmlNodeNavigator_MoveToNext_m1863754258 (HtmlNodeNavigator_t2752606360 * __this, const RuntimeMethod* method)
{
	{
		HtmlNode_t2048434459 * L_0 = __this->get__currentnode_3();
		NullCheck(L_0);
		HtmlNode_t2048434459 * L_1 = HtmlNode_get_NextSibling_m1378195553(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_000f;
		}
	}
	{
		return (bool)0;
	}

IL_000f:
	{
		HtmlNode_t2048434459 * L_2 = __this->get__currentnode_3();
		NullCheck(L_2);
		HtmlNode_t2048434459 * L_3 = HtmlNode_get_NextSibling_m1378195553(L_2, /*hidden argument*/NULL);
		__this->set__currentnode_3(L_3);
		return (bool)1;
	}
}
// System.Boolean HtmlAgilityPack.HtmlNodeNavigator::MoveToNextAttribute()
extern "C"  bool HtmlNodeNavigator_MoveToNextAttribute_m3403000620 (HtmlNodeNavigator_t2752606360 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get__attindex_2();
		HtmlNode_t2048434459 * L_1 = __this->get__currentnode_3();
		NullCheck(L_1);
		HtmlAttributeCollection_t1787476631 * L_2 = HtmlNode_get_Attributes_m2935417207(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = HtmlAttributeCollection_get_Count_m1097499500(L_2, /*hidden argument*/NULL);
		if ((((int32_t)L_0) < ((int32_t)((int32_t)((int32_t)L_3-(int32_t)1)))))
		{
			goto IL_001c;
		}
	}
	{
		return (bool)0;
	}

IL_001c:
	{
		int32_t L_4 = __this->get__attindex_2();
		__this->set__attindex_2(((int32_t)((int32_t)L_4+(int32_t)1)));
		return (bool)1;
	}
}
// System.Boolean HtmlAgilityPack.HtmlNodeNavigator::MoveToNextNamespace(System.Xml.XPath.XPathNamespaceScope)
extern "C"  bool HtmlNodeNavigator_MoveToNextNamespace_m222049378 (HtmlNodeNavigator_t2752606360 * __this, int32_t ___scope0, const RuntimeMethod* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean HtmlAgilityPack.HtmlNodeNavigator::MoveToParent()
extern "C"  bool HtmlNodeNavigator_MoveToParent_m299830349 (HtmlNodeNavigator_t2752606360 * __this, const RuntimeMethod* method)
{
	{
		HtmlNode_t2048434459 * L_0 = __this->get__currentnode_3();
		NullCheck(L_0);
		HtmlNode_t2048434459 * L_1 = HtmlNode_get_ParentNode_m1888227040(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_000f;
		}
	}
	{
		return (bool)0;
	}

IL_000f:
	{
		HtmlNode_t2048434459 * L_2 = __this->get__currentnode_3();
		NullCheck(L_2);
		HtmlNode_t2048434459 * L_3 = HtmlNode_get_ParentNode_m1888227040(L_2, /*hidden argument*/NULL);
		__this->set__currentnode_3(L_3);
		return (bool)1;
	}
}
// System.Void HtmlAgilityPack.HtmlNodeNavigator::MoveToRoot()
extern "C"  void HtmlNodeNavigator_MoveToRoot_m894331495 (HtmlNodeNavigator_t2752606360 * __this, const RuntimeMethod* method)
{
	{
		HtmlDocument_t556432108 * L_0 = __this->get__doc_4();
		NullCheck(L_0);
		HtmlNode_t2048434459 * L_1 = HtmlDocument_get_DocumentNode_m1723003156(L_0, /*hidden argument*/NULL);
		__this->set__currentnode_3(L_1);
		return;
	}
}
// System.Void HtmlAgilityPack.HtmlNodeNavigator::Reset()
extern "C"  void HtmlNodeNavigator_Reset_m1651173616 (HtmlNodeNavigator_t2752606360 * __this, const RuntimeMethod* method)
{
	{
		HtmlDocument_t556432108 * L_0 = __this->get__doc_4();
		NullCheck(L_0);
		HtmlNode_t2048434459 * L_1 = HtmlDocument_get_DocumentNode_m1723003156(L_0, /*hidden argument*/NULL);
		__this->set__currentnode_3(L_1);
		__this->set__attindex_2((-1));
		return;
	}
}
// System.Void HtmlAgilityPack.HtmlParseError::.ctor(HtmlAgilityPack.HtmlParseErrorCode,System.Int32,System.Int32,System.Int32,System.String,System.String)
extern "C"  void HtmlParseError__ctor_m3785891130 (HtmlParseError_t1115179162 * __this, int32_t ___code0, int32_t ___line1, int32_t ___linePosition2, int32_t ___streamPosition3, String_t* ___sourceText4, String_t* ___reason5, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___code0;
		__this->set__code_0(L_0);
		int32_t L_1 = ___line1;
		__this->set__line_1(L_1);
		int32_t L_2 = ___linePosition2;
		__this->set__linePosition_2(L_2);
		int32_t L_3 = ___streamPosition3;
		__this->set__streamPosition_5(L_3);
		String_t* L_4 = ___sourceText4;
		__this->set__sourceText_4(L_4);
		String_t* L_5 = ___reason5;
		__this->set__reason_3(L_5);
		return;
	}
}
// System.Void HtmlAgilityPack.HtmlTextNode::.ctor(HtmlAgilityPack.HtmlDocument,System.Int32)
extern "C"  void HtmlTextNode__ctor_m3649714099 (HtmlTextNode_t2710098554 * __this, HtmlDocument_t556432108 * ___ownerdocument0, int32_t ___index1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HtmlTextNode__ctor_m3649714099_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		HtmlDocument_t556432108 * L_0 = ___ownerdocument0;
		int32_t L_1 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(HtmlNode_t2048434459_il2cpp_TypeInfo_var);
		HtmlNode__ctor_m2584575018(__this, 3, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String HtmlAgilityPack.HtmlTextNode::get_InnerHtml()
extern "C"  String_t* HtmlTextNode_get_InnerHtml_m3278354302 (HtmlTextNode_t2710098554 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String HtmlAgilityPack.HtmlNode::get_OuterHtml() */, __this);
		return L_0;
	}
}
// System.String HtmlAgilityPack.HtmlTextNode::get_OuterHtml()
extern "C"  String_t* HtmlTextNode_get_OuterHtml_m2117722481 (HtmlTextNode_t2710098554 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get__text_29();
		if (L_0)
		{
			goto IL_000f;
		}
	}
	{
		String_t* L_1 = HtmlNode_get_OuterHtml_m1376347664(__this, /*hidden argument*/NULL);
		return L_1;
	}

IL_000f:
	{
		String_t* L_2 = __this->get__text_29();
		return L_2;
	}
}
// System.String HtmlAgilityPack.HtmlTextNode::get_Text()
extern "C"  String_t* HtmlTextNode_get_Text_m1199112604 (HtmlTextNode_t2710098554 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get__text_29();
		if (L_0)
		{
			goto IL_000f;
		}
	}
	{
		String_t* L_1 = HtmlNode_get_OuterHtml_m1376347664(__this, /*hidden argument*/NULL);
		return L_1;
	}

IL_000f:
	{
		String_t* L_2 = __this->get__text_29();
		return L_2;
	}
}
// System.Void HtmlAgilityPack.HtmlTextNode::set_Text(System.String)
extern "C"  void HtmlTextNode_set_Text_m279687427 (HtmlTextNode_t2710098554 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set__text_29(L_0);
		return;
	}
}
// System.Void HtmlAgilityPack.NameValuePairList::.ctor(System.String)
extern "C"  void NameValuePairList__ctor_m1093178387 (NameValuePairList_t1351137418 * __this, String_t* ___text0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NameValuePairList__ctor_m1093178387_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___text0;
		__this->set_Text_0(L_0);
		List_1_t1070465849 * L_1 = (List_1_t1070465849 *)il2cpp_codegen_object_new(List_1_t1070465849_il2cpp_TypeInfo_var);
		List_1__ctor_m973626735(L_1, /*hidden argument*/List_1__ctor_m973626735_RuntimeMethod_var);
		__this->set__allPairs_1(L_1);
		Dictionary_2_t2985245111 * L_2 = (Dictionary_2_t2985245111 *)il2cpp_codegen_object_new(Dictionary_2_t2985245111_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3512899986(L_2, /*hidden argument*/Dictionary_2__ctor_m3512899986_RuntimeMethod_var);
		__this->set__pairsWithName_2(L_2);
		String_t* L_3 = ___text0;
		NameValuePairList_Parse_m4220937304(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.String HtmlAgilityPack.NameValuePairList::GetNameValuePairsValue(System.String,System.String)
extern "C"  String_t* NameValuePairList_GetNameValuePairsValue_m1108671514 (RuntimeObject * __this /* static, unused */, String_t* ___text0, String_t* ___name1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NameValuePairList_GetNameValuePairsValue_m1108671514_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	NameValuePairList_t1351137418 * V_0 = NULL;
	{
		String_t* L_0 = ___text0;
		NameValuePairList_t1351137418 * L_1 = (NameValuePairList_t1351137418 *)il2cpp_codegen_object_new(NameValuePairList_t1351137418_il2cpp_TypeInfo_var);
		NameValuePairList__ctor_m1093178387(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		NameValuePairList_t1351137418 * L_2 = V_0;
		String_t* L_3 = ___name1;
		NullCheck(L_2);
		String_t* L_4 = NameValuePairList_GetNameValuePairValue_m2853234185(L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>> HtmlAgilityPack.NameValuePairList::GetNameValuePairs(System.String)
extern "C"  List_1_t1070465849 * NameValuePairList_GetNameValuePairs_m1788854520 (NameValuePairList_t1351137418 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NameValuePairList_GetNameValuePairs_m1788854520_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___name0;
		if (L_0)
		{
			goto IL_000a;
		}
	}
	{
		List_1_t1070465849 * L_1 = __this->get__allPairs_1();
		return L_1;
	}

IL_000a:
	{
		Dictionary_2_t2985245111 * L_2 = __this->get__pairsWithName_2();
		String_t* L_3 = ___name0;
		NullCheck(L_2);
		bool L_4 = Dictionary_2_ContainsKey_m838899279(L_2, L_3, /*hidden argument*/Dictionary_2_ContainsKey_m838899279_RuntimeMethod_var);
		if (L_4)
		{
			goto IL_001e;
		}
	}
	{
		List_1_t1070465849 * L_5 = (List_1_t1070465849 *)il2cpp_codegen_object_new(List_1_t1070465849_il2cpp_TypeInfo_var);
		List_1__ctor_m973626735(L_5, /*hidden argument*/List_1__ctor_m973626735_RuntimeMethod_var);
		return L_5;
	}

IL_001e:
	{
		Dictionary_2_t2985245111 * L_6 = __this->get__pairsWithName_2();
		String_t* L_7 = ___name0;
		NullCheck(L_6);
		List_1_t1070465849 * L_8 = Dictionary_2_get_Item_m441479114(L_6, L_7, /*hidden argument*/Dictionary_2_get_Item_m441479114_RuntimeMethod_var);
		return L_8;
	}
}
// System.String HtmlAgilityPack.NameValuePairList::GetNameValuePairValue(System.String)
extern "C"  String_t* NameValuePairList_GetNameValuePairValue_m2853234185 (NameValuePairList_t1351137418 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NameValuePairList_GetNameValuePairValue_m2853234185_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t1070465849 * V_0 = NULL;
	KeyValuePair_2_t1701344717  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		String_t* L_0 = ___name0;
		if (L_0)
		{
			goto IL_0009;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m911049464(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0009:
	{
		String_t* L_2 = ___name0;
		List_1_t1070465849 * L_3 = NameValuePairList_GetNameValuePairs_m1788854520(__this, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		List_1_t1070465849 * L_4 = V_0;
		NullCheck(L_4);
		int32_t L_5 = List_1_get_Count_m256450699(L_4, /*hidden argument*/List_1_get_Count_m256450699_RuntimeMethod_var);
		if (L_5)
		{
			goto IL_001f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		return L_6;
	}

IL_001f:
	{
		List_1_t1070465849 * L_7 = V_0;
		NullCheck(L_7);
		KeyValuePair_2_t1701344717  L_8 = List_1_get_Item_m3174779882(L_7, 0, /*hidden argument*/List_1_get_Item_m3174779882_RuntimeMethod_var);
		V_1 = L_8;
		String_t* L_9 = KeyValuePair_2_get_Value_m1710042386((&V_1), /*hidden argument*/KeyValuePair_2_get_Value_m1710042386_RuntimeMethod_var);
		NullCheck(L_9);
		String_t* L_10 = String_Trim_m2668767713(L_9, /*hidden argument*/NULL);
		return L_10;
	}
}
// System.Void HtmlAgilityPack.NameValuePairList::Parse(System.String)
extern "C"  void NameValuePairList_Parse_m4220937304 (NameValuePairList_t1351137418 * __this, String_t* ___text0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NameValuePairList_Parse_m4220937304_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringU5BU5D_t1642385972* V_0 = NULL;
	String_t* V_1 = NULL;
	StringU5BU5D_t1642385972* V_2 = NULL;
	KeyValuePair_2_t1701344717  V_3;
	memset(&V_3, 0, sizeof(V_3));
	List_1_t1070465849 * V_4 = NULL;
	CharU5BU5D_t1328083999* V_5 = NULL;
	StringU5BU5D_t1642385972* V_6 = NULL;
	int32_t V_7 = 0;
	CharU5BU5D_t1328083999* V_8 = NULL;
	String_t* G_B7_0 = NULL;
	KeyValuePair_2_t1701344717 * G_B7_1 = NULL;
	String_t* G_B6_0 = NULL;
	KeyValuePair_2_t1701344717 * G_B6_1 = NULL;
	String_t* G_B8_0 = NULL;
	String_t* G_B8_1 = NULL;
	KeyValuePair_2_t1701344717 * G_B8_2 = NULL;
	{
		List_1_t1070465849 * L_0 = __this->get__allPairs_1();
		NullCheck(L_0);
		List_1_Clear_m3390312458(L_0, /*hidden argument*/List_1_Clear_m3390312458_RuntimeMethod_var);
		Dictionary_2_t2985245111 * L_1 = __this->get__pairsWithName_2();
		NullCheck(L_1);
		Dictionary_2_Clear_m1802158027(L_1, /*hidden argument*/Dictionary_2_Clear_m1802158027_RuntimeMethod_var);
		String_t* L_2 = ___text0;
		if (L_2)
		{
			goto IL_001a;
		}
	}
	{
		return;
	}

IL_001a:
	{
		String_t* L_3 = ___text0;
		V_5 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
		CharU5BU5D_t1328083999* L_4 = V_5;
		NullCheck(L_4);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)59));
		CharU5BU5D_t1328083999* L_5 = V_5;
		NullCheck(L_3);
		StringU5BU5D_t1642385972* L_6 = String_Split_m3326265864(L_3, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		StringU5BU5D_t1642385972* L_7 = V_0;
		V_6 = L_7;
		V_7 = 0;
		goto IL_00ed;
	}

IL_003c:
	{
		StringU5BU5D_t1642385972* L_8 = V_6;
		int32_t L_9 = V_7;
		NullCheck(L_8);
		int32_t L_10 = L_9;
		String_t* L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		V_1 = L_11;
		String_t* L_12 = V_1;
		NullCheck(L_12);
		int32_t L_13 = String_get_Length_m1606060069(L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_00e7;
		}
	}
	{
		String_t* L_14 = V_1;
		V_8 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
		CharU5BU5D_t1328083999* L_15 = V_8;
		NullCheck(L_15);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)61));
		CharU5BU5D_t1328083999* L_16 = V_8;
		NullCheck(L_14);
		StringU5BU5D_t1642385972* L_17 = String_Split_m3350696563(L_14, L_16, 2, /*hidden argument*/NULL);
		V_2 = L_17;
		StringU5BU5D_t1642385972* L_18 = V_2;
		NullCheck(L_18);
		if (!(((int32_t)((int32_t)(((RuntimeArray *)L_18)->max_length)))))
		{
			goto IL_00e7;
		}
	}
	{
		StringU5BU5D_t1642385972* L_19 = V_2;
		NullCheck(L_19);
		int32_t L_20 = 0;
		String_t* L_21 = (L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		NullCheck(L_21);
		String_t* L_22 = String_Trim_m2668767713(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		String_t* L_23 = String_ToLower_m2994460523(L_22, /*hidden argument*/NULL);
		StringU5BU5D_t1642385972* L_24 = V_2;
		NullCheck(L_24);
		G_B6_0 = L_23;
		G_B6_1 = (&V_3);
		if ((((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_24)->max_length))))) < ((int32_t)2)))
		{
			G_B7_0 = L_23;
			G_B7_1 = (&V_3);
			goto IL_0084;
		}
	}
	{
		StringU5BU5D_t1642385972* L_25 = V_2;
		NullCheck(L_25);
		int32_t L_26 = 1;
		String_t* L_27 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_26));
		G_B8_0 = L_27;
		G_B8_1 = G_B6_0;
		G_B8_2 = G_B6_1;
		goto IL_0089;
	}

IL_0084:
	{
		G_B8_0 = _stringLiteral371857150;
		G_B8_1 = G_B7_0;
		G_B8_2 = G_B7_1;
	}

IL_0089:
	{
		KeyValuePair_2__ctor_m2274627104(G_B8_2, G_B8_1, G_B8_0, /*hidden argument*/KeyValuePair_2__ctor_m2274627104_RuntimeMethod_var);
		List_1_t1070465849 * L_28 = __this->get__allPairs_1();
		KeyValuePair_2_t1701344717  L_29 = V_3;
		NullCheck(L_28);
		List_1_Add_m1754637603(L_28, L_29, /*hidden argument*/List_1_Add_m1754637603_RuntimeMethod_var);
		Dictionary_2_t2985245111 * L_30 = __this->get__pairsWithName_2();
		String_t* L_31 = KeyValuePair_2_get_Key_m1372024679((&V_3), /*hidden argument*/KeyValuePair_2_get_Key_m1372024679_RuntimeMethod_var);
		NullCheck(L_30);
		bool L_32 = Dictionary_2_ContainsKey_m838899279(L_30, L_31, /*hidden argument*/Dictionary_2_ContainsKey_m838899279_RuntimeMethod_var);
		if (L_32)
		{
			goto IL_00cb;
		}
	}
	{
		List_1_t1070465849 * L_33 = (List_1_t1070465849 *)il2cpp_codegen_object_new(List_1_t1070465849_il2cpp_TypeInfo_var);
		List_1__ctor_m973626735(L_33, /*hidden argument*/List_1__ctor_m973626735_RuntimeMethod_var);
		V_4 = L_33;
		Dictionary_2_t2985245111 * L_34 = __this->get__pairsWithName_2();
		String_t* L_35 = KeyValuePair_2_get_Key_m1372024679((&V_3), /*hidden argument*/KeyValuePair_2_get_Key_m1372024679_RuntimeMethod_var);
		List_1_t1070465849 * L_36 = V_4;
		NullCheck(L_34);
		Dictionary_2_set_Item_m764567519(L_34, L_35, L_36, /*hidden argument*/Dictionary_2_set_Item_m764567519_RuntimeMethod_var);
		goto IL_00df;
	}

IL_00cb:
	{
		Dictionary_2_t2985245111 * L_37 = __this->get__pairsWithName_2();
		String_t* L_38 = KeyValuePair_2_get_Key_m1372024679((&V_3), /*hidden argument*/KeyValuePair_2_get_Key_m1372024679_RuntimeMethod_var);
		NullCheck(L_37);
		List_1_t1070465849 * L_39 = Dictionary_2_get_Item_m441479114(L_37, L_38, /*hidden argument*/Dictionary_2_get_Item_m441479114_RuntimeMethod_var);
		V_4 = L_39;
	}

IL_00df:
	{
		List_1_t1070465849 * L_40 = V_4;
		KeyValuePair_2_t1701344717  L_41 = V_3;
		NullCheck(L_40);
		List_1_Add_m1754637603(L_40, L_41, /*hidden argument*/List_1_Add_m1754637603_RuntimeMethod_var);
	}

IL_00e7:
	{
		int32_t L_42 = V_7;
		V_7 = ((int32_t)((int32_t)L_42+(int32_t)1));
	}

IL_00ed:
	{
		int32_t L_43 = V_7;
		StringU5BU5D_t1642385972* L_44 = V_6;
		NullCheck(L_44);
		if ((((int32_t)L_43) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_44)->max_length)))))))
		{
			goto IL_003c;
		}
	}
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
