﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.XR.iOS.ARPlaneAnchorGameObject>
struct Dictionary_2_t4220005149;
// System.String
struct String_t;
// IBM.Watson.DeveloperCloud.Utilities.Credentials
struct Credentials_t1494051450;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;
// System.Collections.Generic.Dictionary`2<System.String,IBM.Watson.DeveloperCloud.Connection.RESTConnector>
struct Dictionary_2_t1324914213;
// System.Collections.Generic.Queue`1<IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request>
struct Queue_1_t286473815;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// MainUI
struct MainUI_t541415437;
// UnityEngine.MeshCollider
struct MeshCollider_t2718867283;
// UnityEngine.MeshFilter
struct MeshFilter_t3026937449;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;
// System.Collections.Generic.Dictionary`2<System.String,IBM.Watson.DeveloperCloud.Connection.RESTConnector/Form>
struct Dictionary_2_t2694055125;
// IBM.Watson.DeveloperCloud.Connection.RESTConnector/ResponseEvent
struct ResponseEvent_t1616568356;
// IBM.Watson.DeveloperCloud.Connection.RESTConnector/ProgressEvent
struct ProgressEvent_t4185145044;
// UnityEngine.AsyncOperation
struct AsyncOperation_t3814632279;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// WebCamRecognition
struct WebCamRecognition_t2299961224;
// System.Single[]
struct SingleU5BU5D_t577127397;
// ExampleStreaming
struct ExampleStreaming_t2645177802;
// ConfigLoader
struct ConfigLoader_t1246716173;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t2295673753;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t339478082;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.Void
struct Void_t1841601450;
// UnityEngine.Sprite
struct Sprite_t309593783;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1572802995;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;
// UnityEngine.UI.Selectable
struct Selectable_t1490392188;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request
struct Request_t466816980;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;
// IBM.Watson.DeveloperCloud.Connection.RESTConnector/Response
struct Response_t429319368;
// UnityEngine.WWW
struct WWW_t2919945039;
// IBM.Watson.DeveloperCloud.Connection.RESTConnector
struct RESTConnector_t3705102247;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.Rendering.CommandBuffer
struct CommandBuffer_t1204166949;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface
struct UnityARSessionNativeInterface_t1130867170;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;
// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery
struct Discovery_t1478121420;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.XR.iOS.UnityARSessionRunOption[]
struct UnityARSessionRunOptionU5BU5D_t3114965901;
// UnityEngine.XR.iOS.UnityARAlignment[]
struct UnityARAlignmentU5BU5D_t218994990;
// UnityEngine.XR.iOS.UnityARPlaneDetection[]
struct UnityARPlaneDetectionU5BU5D_t191549612;
// UnityEngine.XR.iOS.UnityARAnchorManager
struct UnityARAnchorManager_t1086564192;
// IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.ConversationExperimental
struct ConversationExperimental_t215658501;
// UnityEngine.ParticleSystem
struct ParticleSystem_t3394631041;
// ColorPicker
struct ColorPicker_t3035206225;
// UnityEngine.ParticleSystem/Particle[]
struct ParticleU5BU5D_t574222242;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1612828712;
// System.Collections.Generic.List`1<UnityEngine.ParticleSystem>
struct List_1_t2763752173;
// UnityEngine.MaterialPropertyBlock
struct MaterialPropertyBlock_t3303648957;
// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank
struct RetrieveAndRank_t1381045327;
// UnityEngine.AudioClip
struct AudioClip_t1932558630;
// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText
struct SpeechToText_t2713896346;
// IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v3.PersonalityInsights
struct PersonalityInsights_t2503525937;
// IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.LanguageTranslator
struct LanguageTranslator_t2981742234;
// IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.NaturalLanguageClassifier
struct NaturalLanguageClassifier_t2492034444;
// IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v2.PersonalityInsights
struct PersonalityInsights_t582046669;
// IBM.Watson.DeveloperCloud.Services.DocumentConversion.v1.DocumentConversion
struct DocumentConversion_t1610681923;
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics
struct TradeoffAnalytics_t100002595;
// UnityEngine.Camera
struct Camera_t189460977;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech
struct TextToSpeech_t3349357562;
// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzer
struct ToneAnalyzer_t1356110496;
// UnityEngine.UI.InputField
struct InputField_t1631627530;
// UnityEngine.UI.BoxSlider
struct BoxSlider_t1871650694;
// UnityEngine.UI.RawImage
struct RawImage_t2749640213;
// UnityEngine.Light
struct Light_t494725636;
// IBM.Watson.DeveloperCloud.Camera.WatsonCamera
struct WatsonCamera_t824577261;
// UnityEngine.Transform[]
struct TransformU5BU5D_t3764228911;
// UnityEngine.Canvas
struct Canvas_t209405766;
// UnityEngine.Animator[]
struct AnimatorU5BU5D_t4224369806;
// UnityEngine.UI.LayoutGroup
struct LayoutGroup_t3962498969;
// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// MenuScene[]
struct MenuSceneU5BU5D_t1862341966;
// System.Collections.Generic.List`1<IBM.Watson.DeveloperCloud.Camera.CameraTarget>
struct List_1_t4216267824;
// IBM.Watson.DeveloperCloud.Camera.CameraTarget
struct CameraTarget_t552179396;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t1158329972;
// UnityEngine.Renderer
struct Renderer_t257310565;
// UnityEngine.UI.Image
struct Image_t2042527209;
// IBM.Watson.DeveloperCloud.Widgets.WebCamWidget
struct WebCamWidget_t58556181;
// IBM.Watson.DeveloperCloud.Widgets.WebCamDisplayWidget
struct WebCamDisplayWidget_t3233697851;
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition
struct VisualRecognition_t3119563755;
// IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.LanguageTranslation
struct LanguageTranslation_t3099415401;
// UnityEngine.UI.Text
struct Text_t356221433;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI
struct AlchemyAPI_t2955839919;
// IBM.Watson.DeveloperCloud.Services.Conversation.v1.Conversation
struct Conversation_t105466997;
// UnityEngine.UI.Slider
struct Slider_t297367283;
// ColorChangedEvent
struct ColorChangedEvent_t2990895397;
// HSVChangedEvent
struct HSVChangedEvent_t1170297569;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// System.Collections.Generic.List`1<UnityEngine.UI.Selectable>
struct List_1_t859513320;
// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_t3244928895;
// UnityEngine.UI.Graphic
struct Graphic_t2426225576;
// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_t2665681875;
// UnityEngine.UI.BoxSlider/BoxSliderEvent
struct BoxSliderEvent_t1774115848;

struct Vector3_t2243707580 ;



#ifndef U3CMODULEU3E_T3783534226_H
#define U3CMODULEU3E_T3783534226_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t3783534226 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T3783534226_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef UNITYARMATRIXOPS_T4039665643_H
#define UNITYARMATRIXOPS_T4039665643_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARMatrixOps
struct  UnityARMatrixOps_t4039665643  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARMATRIXOPS_T4039665643_H
#ifndef UNITYARANCHORMANAGER_T1086564192_H
#define UNITYARANCHORMANAGER_T1086564192_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARAnchorManager
struct  UnityARAnchorManager_t1086564192  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.XR.iOS.ARPlaneAnchorGameObject> UnityEngine.XR.iOS.UnityARAnchorManager::planeAnchorMap
	Dictionary_2_t4220005149 * ___planeAnchorMap_0;

public:
	inline static int32_t get_offset_of_planeAnchorMap_0() { return static_cast<int32_t>(offsetof(UnityARAnchorManager_t1086564192, ___planeAnchorMap_0)); }
	inline Dictionary_2_t4220005149 * get_planeAnchorMap_0() const { return ___planeAnchorMap_0; }
	inline Dictionary_2_t4220005149 ** get_address_of_planeAnchorMap_0() { return &___planeAnchorMap_0; }
	inline void set_planeAnchorMap_0(Dictionary_2_t4220005149 * value)
	{
		___planeAnchorMap_0 = value;
		Il2CppCodeGenWriteBarrier((&___planeAnchorMap_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARANCHORMANAGER_T1086564192_H
#ifndef RESTCONNECTOR_T3705102247_H
#define RESTCONNECTOR_T3705102247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Connection.RESTConnector
struct  RESTConnector_t3705102247  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Connection.RESTConnector::<URL>k__BackingField
	String_t* ___U3CURLU3Ek__BackingField_1;
	// IBM.Watson.DeveloperCloud.Utilities.Credentials IBM.Watson.DeveloperCloud.Connection.RESTConnector::<Authentication>k__BackingField
	Credentials_t1494051450 * ___U3CAuthenticationU3Ek__BackingField_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> IBM.Watson.DeveloperCloud.Connection.RESTConnector::<Headers>k__BackingField
	Dictionary_2_t3943999495 * ___U3CHeadersU3Ek__BackingField_3;
	// System.Int32 IBM.Watson.DeveloperCloud.Connection.RESTConnector::m_ActiveConnections
	int32_t ___m_ActiveConnections_5;
	// System.Collections.Generic.Queue`1<IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request> IBM.Watson.DeveloperCloud.Connection.RESTConnector::m_Requests
	Queue_1_t286473815 * ___m_Requests_6;

public:
	inline static int32_t get_offset_of_U3CURLU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RESTConnector_t3705102247, ___U3CURLU3Ek__BackingField_1)); }
	inline String_t* get_U3CURLU3Ek__BackingField_1() const { return ___U3CURLU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CURLU3Ek__BackingField_1() { return &___U3CURLU3Ek__BackingField_1; }
	inline void set_U3CURLU3Ek__BackingField_1(String_t* value)
	{
		___U3CURLU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CURLU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CAuthenticationU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(RESTConnector_t3705102247, ___U3CAuthenticationU3Ek__BackingField_2)); }
	inline Credentials_t1494051450 * get_U3CAuthenticationU3Ek__BackingField_2() const { return ___U3CAuthenticationU3Ek__BackingField_2; }
	inline Credentials_t1494051450 ** get_address_of_U3CAuthenticationU3Ek__BackingField_2() { return &___U3CAuthenticationU3Ek__BackingField_2; }
	inline void set_U3CAuthenticationU3Ek__BackingField_2(Credentials_t1494051450 * value)
	{
		___U3CAuthenticationU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAuthenticationU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CHeadersU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(RESTConnector_t3705102247, ___U3CHeadersU3Ek__BackingField_3)); }
	inline Dictionary_2_t3943999495 * get_U3CHeadersU3Ek__BackingField_3() const { return ___U3CHeadersU3Ek__BackingField_3; }
	inline Dictionary_2_t3943999495 ** get_address_of_U3CHeadersU3Ek__BackingField_3() { return &___U3CHeadersU3Ek__BackingField_3; }
	inline void set_U3CHeadersU3Ek__BackingField_3(Dictionary_2_t3943999495 * value)
	{
		___U3CHeadersU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CHeadersU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_m_ActiveConnections_5() { return static_cast<int32_t>(offsetof(RESTConnector_t3705102247, ___m_ActiveConnections_5)); }
	inline int32_t get_m_ActiveConnections_5() const { return ___m_ActiveConnections_5; }
	inline int32_t* get_address_of_m_ActiveConnections_5() { return &___m_ActiveConnections_5; }
	inline void set_m_ActiveConnections_5(int32_t value)
	{
		___m_ActiveConnections_5 = value;
	}

	inline static int32_t get_offset_of_m_Requests_6() { return static_cast<int32_t>(offsetof(RESTConnector_t3705102247, ___m_Requests_6)); }
	inline Queue_1_t286473815 * get_m_Requests_6() const { return ___m_Requests_6; }
	inline Queue_1_t286473815 ** get_address_of_m_Requests_6() { return &___m_Requests_6; }
	inline void set_m_Requests_6(Queue_1_t286473815 * value)
	{
		___m_Requests_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Requests_6), value);
	}
};

struct RESTConnector_t3705102247_StaticFields
{
public:
	// System.Single IBM.Watson.DeveloperCloud.Connection.RESTConnector::sm_LogResponseTime
	float ___sm_LogResponseTime_0;
	// System.Collections.Generic.Dictionary`2<System.String,IBM.Watson.DeveloperCloud.Connection.RESTConnector> IBM.Watson.DeveloperCloud.Connection.RESTConnector::sm_Connectors
	Dictionary_2_t1324914213 * ___sm_Connectors_4;

public:
	inline static int32_t get_offset_of_sm_LogResponseTime_0() { return static_cast<int32_t>(offsetof(RESTConnector_t3705102247_StaticFields, ___sm_LogResponseTime_0)); }
	inline float get_sm_LogResponseTime_0() const { return ___sm_LogResponseTime_0; }
	inline float* get_address_of_sm_LogResponseTime_0() { return &___sm_LogResponseTime_0; }
	inline void set_sm_LogResponseTime_0(float value)
	{
		___sm_LogResponseTime_0 = value;
	}

	inline static int32_t get_offset_of_sm_Connectors_4() { return static_cast<int32_t>(offsetof(RESTConnector_t3705102247_StaticFields, ___sm_Connectors_4)); }
	inline Dictionary_2_t1324914213 * get_sm_Connectors_4() const { return ___sm_Connectors_4; }
	inline Dictionary_2_t1324914213 ** get_address_of_sm_Connectors_4() { return &___sm_Connectors_4; }
	inline void set_sm_Connectors_4(Dictionary_2_t1324914213 * value)
	{
		___sm_Connectors_4 = value;
		Il2CppCodeGenWriteBarrier((&___sm_Connectors_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESTCONNECTOR_T3705102247_H
#ifndef RESPONSE_T429319368_H
#define RESPONSE_T429319368_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Connection.RESTConnector/Response
struct  Response_t429319368  : public RuntimeObject
{
public:
	// System.Boolean IBM.Watson.DeveloperCloud.Connection.RESTConnector/Response::<Success>k__BackingField
	bool ___U3CSuccessU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Connection.RESTConnector/Response::<Error>k__BackingField
	String_t* ___U3CErrorU3Ek__BackingField_1;
	// System.Byte[] IBM.Watson.DeveloperCloud.Connection.RESTConnector/Response::<Data>k__BackingField
	ByteU5BU5D_t3397334013* ___U3CDataU3Ek__BackingField_2;
	// System.Single IBM.Watson.DeveloperCloud.Connection.RESTConnector/Response::<ElapsedTime>k__BackingField
	float ___U3CElapsedTimeU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CSuccessU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Response_t429319368, ___U3CSuccessU3Ek__BackingField_0)); }
	inline bool get_U3CSuccessU3Ek__BackingField_0() const { return ___U3CSuccessU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CSuccessU3Ek__BackingField_0() { return &___U3CSuccessU3Ek__BackingField_0; }
	inline void set_U3CSuccessU3Ek__BackingField_0(bool value)
	{
		___U3CSuccessU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CErrorU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Response_t429319368, ___U3CErrorU3Ek__BackingField_1)); }
	inline String_t* get_U3CErrorU3Ek__BackingField_1() const { return ___U3CErrorU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CErrorU3Ek__BackingField_1() { return &___U3CErrorU3Ek__BackingField_1; }
	inline void set_U3CErrorU3Ek__BackingField_1(String_t* value)
	{
		___U3CErrorU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CErrorU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Response_t429319368, ___U3CDataU3Ek__BackingField_2)); }
	inline ByteU5BU5D_t3397334013* get_U3CDataU3Ek__BackingField_2() const { return ___U3CDataU3Ek__BackingField_2; }
	inline ByteU5BU5D_t3397334013** get_address_of_U3CDataU3Ek__BackingField_2() { return &___U3CDataU3Ek__BackingField_2; }
	inline void set_U3CDataU3Ek__BackingField_2(ByteU5BU5D_t3397334013* value)
	{
		___U3CDataU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CElapsedTimeU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Response_t429319368, ___U3CElapsedTimeU3Ek__BackingField_3)); }
	inline float get_U3CElapsedTimeU3Ek__BackingField_3() const { return ___U3CElapsedTimeU3Ek__BackingField_3; }
	inline float* get_address_of_U3CElapsedTimeU3Ek__BackingField_3() { return &___U3CElapsedTimeU3Ek__BackingField_3; }
	inline void set_U3CElapsedTimeU3Ek__BackingField_3(float value)
	{
		___U3CElapsedTimeU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESPONSE_T429319368_H
#ifndef U3CMAKEACTIVEEVENTSYSTEMWITHDELAYU3EC__ITERATOR2_T3156884139_H
#define U3CMAKEACTIVEEVENTSYSTEMWITHDELAYU3EC__ITERATOR2_T3156884139_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MainUI/<MakeActiveEventSystemWithDelay>c__Iterator2
struct  U3CMakeActiveEventSystemWithDelayU3Ec__Iterator2_t3156884139  : public RuntimeObject
{
public:
	// System.Boolean MainUI/<MakeActiveEventSystemWithDelay>c__Iterator2::active
	bool ___active_0;
	// MainUI MainUI/<MakeActiveEventSystemWithDelay>c__Iterator2::$this
	MainUI_t541415437 * ___U24this_1;
	// System.Object MainUI/<MakeActiveEventSystemWithDelay>c__Iterator2::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean MainUI/<MakeActiveEventSystemWithDelay>c__Iterator2::$disposing
	bool ___U24disposing_3;
	// System.Int32 MainUI/<MakeActiveEventSystemWithDelay>c__Iterator2::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_active_0() { return static_cast<int32_t>(offsetof(U3CMakeActiveEventSystemWithDelayU3Ec__Iterator2_t3156884139, ___active_0)); }
	inline bool get_active_0() const { return ___active_0; }
	inline bool* get_address_of_active_0() { return &___active_0; }
	inline void set_active_0(bool value)
	{
		___active_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CMakeActiveEventSystemWithDelayU3Ec__Iterator2_t3156884139, ___U24this_1)); }
	inline MainUI_t541415437 * get_U24this_1() const { return ___U24this_1; }
	inline MainUI_t541415437 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(MainUI_t541415437 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CMakeActiveEventSystemWithDelayU3Ec__Iterator2_t3156884139, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CMakeActiveEventSystemWithDelayU3Ec__Iterator2_t3156884139, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CMakeActiveEventSystemWithDelayU3Ec__Iterator2_t3156884139, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMAKEACTIVEEVENTSYSTEMWITHDELAYU3EC__ITERATOR2_T3156884139_H
#ifndef UNITYARUTILITY_T3608388148_H
#define UNITYARUTILITY_T3608388148_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARUtility
struct  UnityARUtility_t3608388148  : public RuntimeObject
{
public:
	// UnityEngine.MeshCollider UnityEngine.XR.iOS.UnityARUtility::meshCollider
	MeshCollider_t2718867283 * ___meshCollider_0;
	// UnityEngine.MeshFilter UnityEngine.XR.iOS.UnityARUtility::meshFilter
	MeshFilter_t3026937449 * ___meshFilter_1;

public:
	inline static int32_t get_offset_of_meshCollider_0() { return static_cast<int32_t>(offsetof(UnityARUtility_t3608388148, ___meshCollider_0)); }
	inline MeshCollider_t2718867283 * get_meshCollider_0() const { return ___meshCollider_0; }
	inline MeshCollider_t2718867283 ** get_address_of_meshCollider_0() { return &___meshCollider_0; }
	inline void set_meshCollider_0(MeshCollider_t2718867283 * value)
	{
		___meshCollider_0 = value;
		Il2CppCodeGenWriteBarrier((&___meshCollider_0), value);
	}

	inline static int32_t get_offset_of_meshFilter_1() { return static_cast<int32_t>(offsetof(UnityARUtility_t3608388148, ___meshFilter_1)); }
	inline MeshFilter_t3026937449 * get_meshFilter_1() const { return ___meshFilter_1; }
	inline MeshFilter_t3026937449 ** get_address_of_meshFilter_1() { return &___meshFilter_1; }
	inline void set_meshFilter_1(MeshFilter_t3026937449 * value)
	{
		___meshFilter_1 = value;
		Il2CppCodeGenWriteBarrier((&___meshFilter_1), value);
	}
};

struct UnityARUtility_t3608388148_StaticFields
{
public:
	// UnityEngine.GameObject UnityEngine.XR.iOS.UnityARUtility::planePrefab
	GameObject_t1756533147 * ___planePrefab_2;

public:
	inline static int32_t get_offset_of_planePrefab_2() { return static_cast<int32_t>(offsetof(UnityARUtility_t3608388148_StaticFields, ___planePrefab_2)); }
	inline GameObject_t1756533147 * get_planePrefab_2() const { return ___planePrefab_2; }
	inline GameObject_t1756533147 ** get_address_of_planePrefab_2() { return &___planePrefab_2; }
	inline void set_planePrefab_2(GameObject_t1756533147 * value)
	{
		___planePrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___planePrefab_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARUTILITY_T3608388148_H
#ifndef APPLICATIONDATA_T1885408412_H
#define APPLICATIONDATA_T1885408412_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.ApplicationData
struct  ApplicationData_t1885408412  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPLICATIONDATA_T1885408412_H
#ifndef APPLICATIONDATAVALUE_T1721671971_H
#define APPLICATIONDATAVALUE_T1721671971_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.ApplicationDataValue
struct  ApplicationDataValue_t1721671971  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPLICATIONDATAVALUE_T1721671971_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef FORM_T779275863_H
#define FORM_T779275863_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Connection.RESTConnector/Form
struct  Form_t779275863  : public RuntimeObject
{
public:
	// System.Boolean IBM.Watson.DeveloperCloud.Connection.RESTConnector/Form::<IsBinary>k__BackingField
	bool ___U3CIsBinaryU3Ek__BackingField_0;
	// System.Object IBM.Watson.DeveloperCloud.Connection.RESTConnector/Form::<BoxedObject>k__BackingField
	RuntimeObject * ___U3CBoxedObjectU3Ek__BackingField_1;
	// System.Byte[] IBM.Watson.DeveloperCloud.Connection.RESTConnector/Form::<Contents>k__BackingField
	ByteU5BU5D_t3397334013* ___U3CContentsU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Connection.RESTConnector/Form::<FileName>k__BackingField
	String_t* ___U3CFileNameU3Ek__BackingField_3;
	// System.String IBM.Watson.DeveloperCloud.Connection.RESTConnector/Form::<MimeType>k__BackingField
	String_t* ___U3CMimeTypeU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CIsBinaryU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Form_t779275863, ___U3CIsBinaryU3Ek__BackingField_0)); }
	inline bool get_U3CIsBinaryU3Ek__BackingField_0() const { return ___U3CIsBinaryU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CIsBinaryU3Ek__BackingField_0() { return &___U3CIsBinaryU3Ek__BackingField_0; }
	inline void set_U3CIsBinaryU3Ek__BackingField_0(bool value)
	{
		___U3CIsBinaryU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CBoxedObjectU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Form_t779275863, ___U3CBoxedObjectU3Ek__BackingField_1)); }
	inline RuntimeObject * get_U3CBoxedObjectU3Ek__BackingField_1() const { return ___U3CBoxedObjectU3Ek__BackingField_1; }
	inline RuntimeObject ** get_address_of_U3CBoxedObjectU3Ek__BackingField_1() { return &___U3CBoxedObjectU3Ek__BackingField_1; }
	inline void set_U3CBoxedObjectU3Ek__BackingField_1(RuntimeObject * value)
	{
		___U3CBoxedObjectU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBoxedObjectU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CContentsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Form_t779275863, ___U3CContentsU3Ek__BackingField_2)); }
	inline ByteU5BU5D_t3397334013* get_U3CContentsU3Ek__BackingField_2() const { return ___U3CContentsU3Ek__BackingField_2; }
	inline ByteU5BU5D_t3397334013** get_address_of_U3CContentsU3Ek__BackingField_2() { return &___U3CContentsU3Ek__BackingField_2; }
	inline void set_U3CContentsU3Ek__BackingField_2(ByteU5BU5D_t3397334013* value)
	{
		___U3CContentsU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CContentsU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CFileNameU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Form_t779275863, ___U3CFileNameU3Ek__BackingField_3)); }
	inline String_t* get_U3CFileNameU3Ek__BackingField_3() const { return ___U3CFileNameU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CFileNameU3Ek__BackingField_3() { return &___U3CFileNameU3Ek__BackingField_3; }
	inline void set_U3CFileNameU3Ek__BackingField_3(String_t* value)
	{
		___U3CFileNameU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFileNameU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CMimeTypeU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Form_t779275863, ___U3CMimeTypeU3Ek__BackingField_4)); }
	inline String_t* get_U3CMimeTypeU3Ek__BackingField_4() const { return ___U3CMimeTypeU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CMimeTypeU3Ek__BackingField_4() { return &___U3CMimeTypeU3Ek__BackingField_4; }
	inline void set_U3CMimeTypeU3Ek__BackingField_4(String_t* value)
	{
		___U3CMimeTypeU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMimeTypeU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORM_T779275863_H
#ifndef REQUEST_T466816980_H
#define REQUEST_T466816980_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request
struct  Request_t466816980  : public RuntimeObject
{
public:
	// System.Single IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Timeout>k__BackingField
	float ___U3CTimeoutU3Ek__BackingField_0;
	// System.Boolean IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Cancel>k__BackingField
	bool ___U3CCancelU3Ek__BackingField_1;
	// System.Boolean IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Delete>k__BackingField
	bool ___U3CDeleteU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Function>k__BackingField
	String_t* ___U3CFunctionU3Ek__BackingField_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Parameters>k__BackingField
	Dictionary_2_t309261261 * ___U3CParametersU3Ek__BackingField_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Headers>k__BackingField
	Dictionary_2_t3943999495 * ___U3CHeadersU3Ek__BackingField_5;
	// System.Byte[] IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Send>k__BackingField
	ByteU5BU5D_t3397334013* ___U3CSendU3Ek__BackingField_6;
	// System.Collections.Generic.Dictionary`2<System.String,IBM.Watson.DeveloperCloud.Connection.RESTConnector/Form> IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Forms>k__BackingField
	Dictionary_2_t2694055125 * ___U3CFormsU3Ek__BackingField_7;
	// IBM.Watson.DeveloperCloud.Connection.RESTConnector/ResponseEvent IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<OnResponse>k__BackingField
	ResponseEvent_t1616568356 * ___U3COnResponseU3Ek__BackingField_8;
	// IBM.Watson.DeveloperCloud.Connection.RESTConnector/ProgressEvent IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<OnDownloadProgress>k__BackingField
	ProgressEvent_t4185145044 * ___U3COnDownloadProgressU3Ek__BackingField_9;
	// IBM.Watson.DeveloperCloud.Connection.RESTConnector/ProgressEvent IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<OnUploadProgress>k__BackingField
	ProgressEvent_t4185145044 * ___U3COnUploadProgressU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of_U3CTimeoutU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CTimeoutU3Ek__BackingField_0)); }
	inline float get_U3CTimeoutU3Ek__BackingField_0() const { return ___U3CTimeoutU3Ek__BackingField_0; }
	inline float* get_address_of_U3CTimeoutU3Ek__BackingField_0() { return &___U3CTimeoutU3Ek__BackingField_0; }
	inline void set_U3CTimeoutU3Ek__BackingField_0(float value)
	{
		___U3CTimeoutU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CCancelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CCancelU3Ek__BackingField_1)); }
	inline bool get_U3CCancelU3Ek__BackingField_1() const { return ___U3CCancelU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CCancelU3Ek__BackingField_1() { return &___U3CCancelU3Ek__BackingField_1; }
	inline void set_U3CCancelU3Ek__BackingField_1(bool value)
	{
		___U3CCancelU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CDeleteU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CDeleteU3Ek__BackingField_2)); }
	inline bool get_U3CDeleteU3Ek__BackingField_2() const { return ___U3CDeleteU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CDeleteU3Ek__BackingField_2() { return &___U3CDeleteU3Ek__BackingField_2; }
	inline void set_U3CDeleteU3Ek__BackingField_2(bool value)
	{
		___U3CDeleteU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CFunctionU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CFunctionU3Ek__BackingField_3)); }
	inline String_t* get_U3CFunctionU3Ek__BackingField_3() const { return ___U3CFunctionU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CFunctionU3Ek__BackingField_3() { return &___U3CFunctionU3Ek__BackingField_3; }
	inline void set_U3CFunctionU3Ek__BackingField_3(String_t* value)
	{
		___U3CFunctionU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFunctionU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CParametersU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CParametersU3Ek__BackingField_4)); }
	inline Dictionary_2_t309261261 * get_U3CParametersU3Ek__BackingField_4() const { return ___U3CParametersU3Ek__BackingField_4; }
	inline Dictionary_2_t309261261 ** get_address_of_U3CParametersU3Ek__BackingField_4() { return &___U3CParametersU3Ek__BackingField_4; }
	inline void set_U3CParametersU3Ek__BackingField_4(Dictionary_2_t309261261 * value)
	{
		___U3CParametersU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CParametersU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CHeadersU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CHeadersU3Ek__BackingField_5)); }
	inline Dictionary_2_t3943999495 * get_U3CHeadersU3Ek__BackingField_5() const { return ___U3CHeadersU3Ek__BackingField_5; }
	inline Dictionary_2_t3943999495 ** get_address_of_U3CHeadersU3Ek__BackingField_5() { return &___U3CHeadersU3Ek__BackingField_5; }
	inline void set_U3CHeadersU3Ek__BackingField_5(Dictionary_2_t3943999495 * value)
	{
		___U3CHeadersU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CHeadersU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CSendU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CSendU3Ek__BackingField_6)); }
	inline ByteU5BU5D_t3397334013* get_U3CSendU3Ek__BackingField_6() const { return ___U3CSendU3Ek__BackingField_6; }
	inline ByteU5BU5D_t3397334013** get_address_of_U3CSendU3Ek__BackingField_6() { return &___U3CSendU3Ek__BackingField_6; }
	inline void set_U3CSendU3Ek__BackingField_6(ByteU5BU5D_t3397334013* value)
	{
		___U3CSendU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSendU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CFormsU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CFormsU3Ek__BackingField_7)); }
	inline Dictionary_2_t2694055125 * get_U3CFormsU3Ek__BackingField_7() const { return ___U3CFormsU3Ek__BackingField_7; }
	inline Dictionary_2_t2694055125 ** get_address_of_U3CFormsU3Ek__BackingField_7() { return &___U3CFormsU3Ek__BackingField_7; }
	inline void set_U3CFormsU3Ek__BackingField_7(Dictionary_2_t2694055125 * value)
	{
		___U3CFormsU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFormsU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3COnResponseU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3COnResponseU3Ek__BackingField_8)); }
	inline ResponseEvent_t1616568356 * get_U3COnResponseU3Ek__BackingField_8() const { return ___U3COnResponseU3Ek__BackingField_8; }
	inline ResponseEvent_t1616568356 ** get_address_of_U3COnResponseU3Ek__BackingField_8() { return &___U3COnResponseU3Ek__BackingField_8; }
	inline void set_U3COnResponseU3Ek__BackingField_8(ResponseEvent_t1616568356 * value)
	{
		___U3COnResponseU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3COnResponseU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3COnDownloadProgressU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3COnDownloadProgressU3Ek__BackingField_9)); }
	inline ProgressEvent_t4185145044 * get_U3COnDownloadProgressU3Ek__BackingField_9() const { return ___U3COnDownloadProgressU3Ek__BackingField_9; }
	inline ProgressEvent_t4185145044 ** get_address_of_U3COnDownloadProgressU3Ek__BackingField_9() { return &___U3COnDownloadProgressU3Ek__BackingField_9; }
	inline void set_U3COnDownloadProgressU3Ek__BackingField_9(ProgressEvent_t4185145044 * value)
	{
		___U3COnDownloadProgressU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3COnDownloadProgressU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3COnUploadProgressU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3COnUploadProgressU3Ek__BackingField_10)); }
	inline ProgressEvent_t4185145044 * get_U3COnUploadProgressU3Ek__BackingField_10() const { return ___U3COnUploadProgressU3Ek__BackingField_10; }
	inline ProgressEvent_t4185145044 ** get_address_of_U3COnUploadProgressU3Ek__BackingField_10() { return &___U3COnUploadProgressU3Ek__BackingField_10; }
	inline void set_U3COnUploadProgressU3Ek__BackingField_10(ProgressEvent_t4185145044 * value)
	{
		___U3COnUploadProgressU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3COnUploadProgressU3Ek__BackingField_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUEST_T466816980_H
#ifndef U3CLOADLEVELASYNCU3EC__ITERATOR1_T3234328056_H
#define U3CLOADLEVELASYNCU3EC__ITERATOR1_T3234328056_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MainUI/<LoadLevelAsync>c__Iterator1
struct  U3CLoadLevelAsyncU3Ec__Iterator1_t3234328056  : public RuntimeObject
{
public:
	// System.String MainUI/<LoadLevelAsync>c__Iterator1::name
	String_t* ___name_0;
	// UnityEngine.AsyncOperation MainUI/<LoadLevelAsync>c__Iterator1::<asyncOperation>__0
	AsyncOperation_t3814632279 * ___U3CasyncOperationU3E__0_1;
	// MainUI MainUI/<LoadLevelAsync>c__Iterator1::$this
	MainUI_t541415437 * ___U24this_2;
	// System.Object MainUI/<LoadLevelAsync>c__Iterator1::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean MainUI/<LoadLevelAsync>c__Iterator1::$disposing
	bool ___U24disposing_4;
	// System.Int32 MainUI/<LoadLevelAsync>c__Iterator1::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(U3CLoadLevelAsyncU3Ec__Iterator1_t3234328056, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_U3CasyncOperationU3E__0_1() { return static_cast<int32_t>(offsetof(U3CLoadLevelAsyncU3Ec__Iterator1_t3234328056, ___U3CasyncOperationU3E__0_1)); }
	inline AsyncOperation_t3814632279 * get_U3CasyncOperationU3E__0_1() const { return ___U3CasyncOperationU3E__0_1; }
	inline AsyncOperation_t3814632279 ** get_address_of_U3CasyncOperationU3E__0_1() { return &___U3CasyncOperationU3E__0_1; }
	inline void set_U3CasyncOperationU3E__0_1(AsyncOperation_t3814632279 * value)
	{
		___U3CasyncOperationU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CasyncOperationU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CLoadLevelAsyncU3Ec__Iterator1_t3234328056, ___U24this_2)); }
	inline MainUI_t541415437 * get_U24this_2() const { return ___U24this_2; }
	inline MainUI_t541415437 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(MainUI_t541415437 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CLoadLevelAsyncU3Ec__Iterator1_t3234328056, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CLoadLevelAsyncU3Ec__Iterator1_t3234328056, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CLoadLevelAsyncU3Ec__Iterator1_t3234328056, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADLEVELASYNCU3EC__ITERATOR1_T3234328056_H
#ifndef U3CDETECTFACESINIMAGEU3EC__ITERATOR2_T2044188333_H
#define U3CDETECTFACESINIMAGEU3EC__ITERATOR2_T2044188333_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebCamRecognition/<DetectFacesInImage>c__Iterator2
struct  U3CDetectFacesInImageU3Ec__Iterator2_t2044188333  : public RuntimeObject
{
public:
	// UnityEngine.Texture2D WebCamRecognition/<DetectFacesInImage>c__Iterator2::<image>__0
	Texture2D_t3542995729 * ___U3CimageU3E__0_0;
	// System.Byte[] WebCamRecognition/<DetectFacesInImage>c__Iterator2::<imageData>__0
	ByteU5BU5D_t3397334013* ___U3CimageDataU3E__0_1;
	// WebCamRecognition WebCamRecognition/<DetectFacesInImage>c__Iterator2::$this
	WebCamRecognition_t2299961224 * ___U24this_2;
	// System.Object WebCamRecognition/<DetectFacesInImage>c__Iterator2::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean WebCamRecognition/<DetectFacesInImage>c__Iterator2::$disposing
	bool ___U24disposing_4;
	// System.Int32 WebCamRecognition/<DetectFacesInImage>c__Iterator2::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CimageU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDetectFacesInImageU3Ec__Iterator2_t2044188333, ___U3CimageU3E__0_0)); }
	inline Texture2D_t3542995729 * get_U3CimageU3E__0_0() const { return ___U3CimageU3E__0_0; }
	inline Texture2D_t3542995729 ** get_address_of_U3CimageU3E__0_0() { return &___U3CimageU3E__0_0; }
	inline void set_U3CimageU3E__0_0(Texture2D_t3542995729 * value)
	{
		___U3CimageU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CimageU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CimageDataU3E__0_1() { return static_cast<int32_t>(offsetof(U3CDetectFacesInImageU3Ec__Iterator2_t2044188333, ___U3CimageDataU3E__0_1)); }
	inline ByteU5BU5D_t3397334013* get_U3CimageDataU3E__0_1() const { return ___U3CimageDataU3E__0_1; }
	inline ByteU5BU5D_t3397334013** get_address_of_U3CimageDataU3E__0_1() { return &___U3CimageDataU3E__0_1; }
	inline void set_U3CimageDataU3E__0_1(ByteU5BU5D_t3397334013* value)
	{
		___U3CimageDataU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CimageDataU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CDetectFacesInImageU3Ec__Iterator2_t2044188333, ___U24this_2)); }
	inline WebCamRecognition_t2299961224 * get_U24this_2() const { return ___U24this_2; }
	inline WebCamRecognition_t2299961224 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(WebCamRecognition_t2299961224 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CDetectFacesInImageU3Ec__Iterator2_t2044188333, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CDetectFacesInImageU3Ec__Iterator2_t2044188333, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CDetectFacesInImageU3Ec__Iterator2_t2044188333, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDETECTFACESINIMAGEU3EC__ITERATOR2_T2044188333_H
#ifndef U3CRECOGNIZETEXTINIMAGEU3EC__ITERATOR3_T1348868334_H
#define U3CRECOGNIZETEXTINIMAGEU3EC__ITERATOR3_T1348868334_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebCamRecognition/<RecognizeTextInImage>c__Iterator3
struct  U3CRecognizeTextInImageU3Ec__Iterator3_t1348868334  : public RuntimeObject
{
public:
	// UnityEngine.Texture2D WebCamRecognition/<RecognizeTextInImage>c__Iterator3::<image>__0
	Texture2D_t3542995729 * ___U3CimageU3E__0_0;
	// System.Byte[] WebCamRecognition/<RecognizeTextInImage>c__Iterator3::<imageData>__0
	ByteU5BU5D_t3397334013* ___U3CimageDataU3E__0_1;
	// WebCamRecognition WebCamRecognition/<RecognizeTextInImage>c__Iterator3::$this
	WebCamRecognition_t2299961224 * ___U24this_2;
	// System.Object WebCamRecognition/<RecognizeTextInImage>c__Iterator3::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean WebCamRecognition/<RecognizeTextInImage>c__Iterator3::$disposing
	bool ___U24disposing_4;
	// System.Int32 WebCamRecognition/<RecognizeTextInImage>c__Iterator3::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CimageU3E__0_0() { return static_cast<int32_t>(offsetof(U3CRecognizeTextInImageU3Ec__Iterator3_t1348868334, ___U3CimageU3E__0_0)); }
	inline Texture2D_t3542995729 * get_U3CimageU3E__0_0() const { return ___U3CimageU3E__0_0; }
	inline Texture2D_t3542995729 ** get_address_of_U3CimageU3E__0_0() { return &___U3CimageU3E__0_0; }
	inline void set_U3CimageU3E__0_0(Texture2D_t3542995729 * value)
	{
		___U3CimageU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CimageU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CimageDataU3E__0_1() { return static_cast<int32_t>(offsetof(U3CRecognizeTextInImageU3Ec__Iterator3_t1348868334, ___U3CimageDataU3E__0_1)); }
	inline ByteU5BU5D_t3397334013* get_U3CimageDataU3E__0_1() const { return ___U3CimageDataU3E__0_1; }
	inline ByteU5BU5D_t3397334013** get_address_of_U3CimageDataU3E__0_1() { return &___U3CimageDataU3E__0_1; }
	inline void set_U3CimageDataU3E__0_1(ByteU5BU5D_t3397334013* value)
	{
		___U3CimageDataU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CimageDataU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CRecognizeTextInImageU3Ec__Iterator3_t1348868334, ___U24this_2)); }
	inline WebCamRecognition_t2299961224 * get_U24this_2() const { return ___U24this_2; }
	inline WebCamRecognition_t2299961224 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(WebCamRecognition_t2299961224 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CRecognizeTextInImageU3Ec__Iterator3_t1348868334, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CRecognizeTextInImageU3Ec__Iterator3_t1348868334, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CRecognizeTextInImageU3Ec__Iterator3_t1348868334, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRECOGNIZETEXTINIMAGEU3EC__ITERATOR3_T1348868334_H
#ifndef U3CCLASSIFYIMAGEU3EC__ITERATOR1_T4288026660_H
#define U3CCLASSIFYIMAGEU3EC__ITERATOR1_T4288026660_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebCamRecognition/<ClassifyImage>c__Iterator1
struct  U3CClassifyImageU3Ec__Iterator1_t4288026660  : public RuntimeObject
{
public:
	// UnityEngine.Texture2D WebCamRecognition/<ClassifyImage>c__Iterator1::<image>__0
	Texture2D_t3542995729 * ___U3CimageU3E__0_0;
	// System.Byte[] WebCamRecognition/<ClassifyImage>c__Iterator1::<imageData>__0
	ByteU5BU5D_t3397334013* ___U3CimageDataU3E__0_1;
	// WebCamRecognition WebCamRecognition/<ClassifyImage>c__Iterator1::$this
	WebCamRecognition_t2299961224 * ___U24this_2;
	// System.Object WebCamRecognition/<ClassifyImage>c__Iterator1::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean WebCamRecognition/<ClassifyImage>c__Iterator1::$disposing
	bool ___U24disposing_4;
	// System.Int32 WebCamRecognition/<ClassifyImage>c__Iterator1::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CimageU3E__0_0() { return static_cast<int32_t>(offsetof(U3CClassifyImageU3Ec__Iterator1_t4288026660, ___U3CimageU3E__0_0)); }
	inline Texture2D_t3542995729 * get_U3CimageU3E__0_0() const { return ___U3CimageU3E__0_0; }
	inline Texture2D_t3542995729 ** get_address_of_U3CimageU3E__0_0() { return &___U3CimageU3E__0_0; }
	inline void set_U3CimageU3E__0_0(Texture2D_t3542995729 * value)
	{
		___U3CimageU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CimageU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CimageDataU3E__0_1() { return static_cast<int32_t>(offsetof(U3CClassifyImageU3Ec__Iterator1_t4288026660, ___U3CimageDataU3E__0_1)); }
	inline ByteU5BU5D_t3397334013* get_U3CimageDataU3E__0_1() const { return ___U3CimageDataU3E__0_1; }
	inline ByteU5BU5D_t3397334013** get_address_of_U3CimageDataU3E__0_1() { return &___U3CimageDataU3E__0_1; }
	inline void set_U3CimageDataU3E__0_1(ByteU5BU5D_t3397334013* value)
	{
		___U3CimageDataU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CimageDataU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CClassifyImageU3Ec__Iterator1_t4288026660, ___U24this_2)); }
	inline WebCamRecognition_t2299961224 * get_U24this_2() const { return ___U24this_2; }
	inline WebCamRecognition_t2299961224 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(WebCamRecognition_t2299961224 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CClassifyImageU3Ec__Iterator1_t4288026660, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CClassifyImageU3Ec__Iterator1_t4288026660, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CClassifyImageU3Ec__Iterator1_t4288026660, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCLASSIFYIMAGEU3EC__ITERATOR1_T4288026660_H
#ifndef U3CRECORDINGHANDLERU3EC__ITERATOR0_T3836051059_H
#define U3CRECORDINGHANDLERU3EC__ITERATOR0_T3836051059_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExampleStreaming/<RecordingHandler>c__Iterator0
struct  U3CRecordingHandlerU3Ec__Iterator0_t3836051059  : public RuntimeObject
{
public:
	// System.Boolean ExampleStreaming/<RecordingHandler>c__Iterator0::<bFirstBlock>__0
	bool ___U3CbFirstBlockU3E__0_0;
	// System.Int32 ExampleStreaming/<RecordingHandler>c__Iterator0::<midPoint>__0
	int32_t ___U3CmidPointU3E__0_1;
	// System.Single[] ExampleStreaming/<RecordingHandler>c__Iterator0::<samples>__0
	SingleU5BU5D_t577127397* ___U3CsamplesU3E__0_2;
	// System.Int32 ExampleStreaming/<RecordingHandler>c__Iterator0::<writePos>__1
	int32_t ___U3CwritePosU3E__1_3;
	// System.Int32 ExampleStreaming/<RecordingHandler>c__Iterator0::<remaining>__2
	int32_t ___U3CremainingU3E__2_4;
	// System.Single ExampleStreaming/<RecordingHandler>c__Iterator0::<timeRemaining>__2
	float ___U3CtimeRemainingU3E__2_5;
	// ExampleStreaming ExampleStreaming/<RecordingHandler>c__Iterator0::$this
	ExampleStreaming_t2645177802 * ___U24this_6;
	// System.Object ExampleStreaming/<RecordingHandler>c__Iterator0::$current
	RuntimeObject * ___U24current_7;
	// System.Boolean ExampleStreaming/<RecordingHandler>c__Iterator0::$disposing
	bool ___U24disposing_8;
	// System.Int32 ExampleStreaming/<RecordingHandler>c__Iterator0::$PC
	int32_t ___U24PC_9;

public:
	inline static int32_t get_offset_of_U3CbFirstBlockU3E__0_0() { return static_cast<int32_t>(offsetof(U3CRecordingHandlerU3Ec__Iterator0_t3836051059, ___U3CbFirstBlockU3E__0_0)); }
	inline bool get_U3CbFirstBlockU3E__0_0() const { return ___U3CbFirstBlockU3E__0_0; }
	inline bool* get_address_of_U3CbFirstBlockU3E__0_0() { return &___U3CbFirstBlockU3E__0_0; }
	inline void set_U3CbFirstBlockU3E__0_0(bool value)
	{
		___U3CbFirstBlockU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CmidPointU3E__0_1() { return static_cast<int32_t>(offsetof(U3CRecordingHandlerU3Ec__Iterator0_t3836051059, ___U3CmidPointU3E__0_1)); }
	inline int32_t get_U3CmidPointU3E__0_1() const { return ___U3CmidPointU3E__0_1; }
	inline int32_t* get_address_of_U3CmidPointU3E__0_1() { return &___U3CmidPointU3E__0_1; }
	inline void set_U3CmidPointU3E__0_1(int32_t value)
	{
		___U3CmidPointU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CsamplesU3E__0_2() { return static_cast<int32_t>(offsetof(U3CRecordingHandlerU3Ec__Iterator0_t3836051059, ___U3CsamplesU3E__0_2)); }
	inline SingleU5BU5D_t577127397* get_U3CsamplesU3E__0_2() const { return ___U3CsamplesU3E__0_2; }
	inline SingleU5BU5D_t577127397** get_address_of_U3CsamplesU3E__0_2() { return &___U3CsamplesU3E__0_2; }
	inline void set_U3CsamplesU3E__0_2(SingleU5BU5D_t577127397* value)
	{
		___U3CsamplesU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsamplesU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CwritePosU3E__1_3() { return static_cast<int32_t>(offsetof(U3CRecordingHandlerU3Ec__Iterator0_t3836051059, ___U3CwritePosU3E__1_3)); }
	inline int32_t get_U3CwritePosU3E__1_3() const { return ___U3CwritePosU3E__1_3; }
	inline int32_t* get_address_of_U3CwritePosU3E__1_3() { return &___U3CwritePosU3E__1_3; }
	inline void set_U3CwritePosU3E__1_3(int32_t value)
	{
		___U3CwritePosU3E__1_3 = value;
	}

	inline static int32_t get_offset_of_U3CremainingU3E__2_4() { return static_cast<int32_t>(offsetof(U3CRecordingHandlerU3Ec__Iterator0_t3836051059, ___U3CremainingU3E__2_4)); }
	inline int32_t get_U3CremainingU3E__2_4() const { return ___U3CremainingU3E__2_4; }
	inline int32_t* get_address_of_U3CremainingU3E__2_4() { return &___U3CremainingU3E__2_4; }
	inline void set_U3CremainingU3E__2_4(int32_t value)
	{
		___U3CremainingU3E__2_4 = value;
	}

	inline static int32_t get_offset_of_U3CtimeRemainingU3E__2_5() { return static_cast<int32_t>(offsetof(U3CRecordingHandlerU3Ec__Iterator0_t3836051059, ___U3CtimeRemainingU3E__2_5)); }
	inline float get_U3CtimeRemainingU3E__2_5() const { return ___U3CtimeRemainingU3E__2_5; }
	inline float* get_address_of_U3CtimeRemainingU3E__2_5() { return &___U3CtimeRemainingU3E__2_5; }
	inline void set_U3CtimeRemainingU3E__2_5(float value)
	{
		___U3CtimeRemainingU3E__2_5 = value;
	}

	inline static int32_t get_offset_of_U24this_6() { return static_cast<int32_t>(offsetof(U3CRecordingHandlerU3Ec__Iterator0_t3836051059, ___U24this_6)); }
	inline ExampleStreaming_t2645177802 * get_U24this_6() const { return ___U24this_6; }
	inline ExampleStreaming_t2645177802 ** get_address_of_U24this_6() { return &___U24this_6; }
	inline void set_U24this_6(ExampleStreaming_t2645177802 * value)
	{
		___U24this_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_6), value);
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CRecordingHandlerU3Ec__Iterator0_t3836051059, ___U24current_7)); }
	inline RuntimeObject * get_U24current_7() const { return ___U24current_7; }
	inline RuntimeObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(RuntimeObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_7), value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3CRecordingHandlerU3Ec__Iterator0_t3836051059, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CRecordingHandlerU3Ec__Iterator0_t3836051059, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRECORDINGHANDLERU3EC__ITERATOR0_T3836051059_H
#ifndef U3CSAVEIMAGEFILEU3EC__ITERATOR0_T3286239818_H
#define U3CSAVEIMAGEFILEU3EC__ITERATOR0_T3286239818_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebCamRecognition/<SaveImageFile>c__Iterator0
struct  U3CSaveImageFileU3Ec__Iterator0_t3286239818  : public RuntimeObject
{
public:
	// System.String WebCamRecognition/<SaveImageFile>c__Iterator0::filePath
	String_t* ___filePath_0;
	// System.String WebCamRecognition/<SaveImageFile>c__Iterator0::fileName
	String_t* ___fileName_1;
	// UnityEngine.Texture2D WebCamRecognition/<SaveImageFile>c__Iterator0::<image>__0
	Texture2D_t3542995729 * ___U3CimageU3E__0_2;
	// WebCamRecognition WebCamRecognition/<SaveImageFile>c__Iterator0::$this
	WebCamRecognition_t2299961224 * ___U24this_3;
	// System.Object WebCamRecognition/<SaveImageFile>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean WebCamRecognition/<SaveImageFile>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 WebCamRecognition/<SaveImageFile>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_filePath_0() { return static_cast<int32_t>(offsetof(U3CSaveImageFileU3Ec__Iterator0_t3286239818, ___filePath_0)); }
	inline String_t* get_filePath_0() const { return ___filePath_0; }
	inline String_t** get_address_of_filePath_0() { return &___filePath_0; }
	inline void set_filePath_0(String_t* value)
	{
		___filePath_0 = value;
		Il2CppCodeGenWriteBarrier((&___filePath_0), value);
	}

	inline static int32_t get_offset_of_fileName_1() { return static_cast<int32_t>(offsetof(U3CSaveImageFileU3Ec__Iterator0_t3286239818, ___fileName_1)); }
	inline String_t* get_fileName_1() const { return ___fileName_1; }
	inline String_t** get_address_of_fileName_1() { return &___fileName_1; }
	inline void set_fileName_1(String_t* value)
	{
		___fileName_1 = value;
		Il2CppCodeGenWriteBarrier((&___fileName_1), value);
	}

	inline static int32_t get_offset_of_U3CimageU3E__0_2() { return static_cast<int32_t>(offsetof(U3CSaveImageFileU3Ec__Iterator0_t3286239818, ___U3CimageU3E__0_2)); }
	inline Texture2D_t3542995729 * get_U3CimageU3E__0_2() const { return ___U3CimageU3E__0_2; }
	inline Texture2D_t3542995729 ** get_address_of_U3CimageU3E__0_2() { return &___U3CimageU3E__0_2; }
	inline void set_U3CimageU3E__0_2(Texture2D_t3542995729 * value)
	{
		___U3CimageU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CimageU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CSaveImageFileU3Ec__Iterator0_t3286239818, ___U24this_3)); }
	inline WebCamRecognition_t2299961224 * get_U24this_3() const { return ___U24this_3; }
	inline WebCamRecognition_t2299961224 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(WebCamRecognition_t2299961224 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CSaveImageFileU3Ec__Iterator0_t3286239818, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CSaveImageFileU3Ec__Iterator0_t3286239818, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CSaveImageFileU3Ec__Iterator0_t3286239818, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSAVEIMAGEFILEU3EC__ITERATOR0_T3286239818_H
#ifndef U3CSTARTU3EC__ITERATOR0_T2669888877_H
#define U3CSTARTU3EC__ITERATOR0_T2669888877_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConfigLoader/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t2669888877  : public RuntimeObject
{
public:
	// ConfigLoader ConfigLoader/<Start>c__Iterator0::$this
	ConfigLoader_t1246716173 * ___U24this_0;
	// System.Object ConfigLoader/<Start>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean ConfigLoader/<Start>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 ConfigLoader/<Start>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2669888877, ___U24this_0)); }
	inline ConfigLoader_t1246716173 * get_U24this_0() const { return ___U24this_0; }
	inline ConfigLoader_t1246716173 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(ConfigLoader_t1246716173 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2669888877, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2669888877, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2669888877, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T2669888877_H
#ifndef U3CUPDATEBUTTONSU3EC__ANONSTOREY3_T2231175942_H
#define U3CUPDATEBUTTONSU3EC__ANONSTOREY3_T2231175942_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MainUI/<UpdateButtons>c__AnonStorey3
struct  U3CUpdateButtonsU3Ec__AnonStorey3_t2231175942  : public RuntimeObject
{
public:
	// System.String MainUI/<UpdateButtons>c__AnonStorey3::captured
	String_t* ___captured_0;
	// MainUI MainUI/<UpdateButtons>c__AnonStorey3::$this
	MainUI_t541415437 * ___U24this_1;

public:
	inline static int32_t get_offset_of_captured_0() { return static_cast<int32_t>(offsetof(U3CUpdateButtonsU3Ec__AnonStorey3_t2231175942, ___captured_0)); }
	inline String_t* get_captured_0() const { return ___captured_0; }
	inline String_t** get_address_of_captured_0() { return &___captured_0; }
	inline void set_captured_0(String_t* value)
	{
		___captured_0 = value;
		Il2CppCodeGenWriteBarrier((&___captured_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CUpdateButtonsU3Ec__AnonStorey3_t2231175942, ___U24this_1)); }
	inline MainUI_t541415437 * get_U24this_1() const { return ___U24this_1; }
	inline MainUI_t541415437 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(MainUI_t541415437 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUPDATEBUTTONSU3EC__ANONSTOREY3_T2231175942_H
#ifndef HSVUTIL_T3885028383_H
#define HSVUTIL_T3885028383_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HSVUtil
struct  HSVUtil_t3885028383  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HSVUTIL_T3885028383_H
#ifndef U3CSTARTU3EC__ITERATOR0_T1305459389_H
#define U3CSTARTU3EC__ITERATOR0_T1305459389_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MainUI/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t1305459389  : public RuntimeObject
{
public:
	// MainUI MainUI/<Start>c__Iterator0::$this
	MainUI_t541415437 * ___U24this_0;
	// System.Object MainUI/<Start>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean MainUI/<Start>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 MainUI/<Start>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1305459389, ___U24this_0)); }
	inline MainUI_t541415437 * get_U24this_0() const { return ___U24this_0; }
	inline MainUI_t541415437 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(MainUI_t541415437 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1305459389, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1305459389, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1305459389, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T1305459389_H
#ifndef UNITYEVENTBASE_T828812576_H
#define UNITYEVENTBASE_T828812576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t828812576  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t2295673753 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t339478082 * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t828812576, ___m_Calls_0)); }
	inline InvokableCallList_t2295673753 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t2295673753 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t2295673753 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t828812576, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t339478082 * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t339478082 ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t339478082 * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t828812576, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t828812576, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T828812576_H
#ifndef COLOR_T2020392075_H
#define COLOR_T2020392075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2020392075 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2020392075_H
#ifndef UNITYEVENT_1_T2058742090_H
#define UNITYEVENT_1_T2058742090_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<UnityEngine.Color>
struct  UnityEvent_1_t2058742090  : public UnityEventBase_t828812576
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3614634134* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t2058742090, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3614634134* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3614634134* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T2058742090_H
#ifndef UNITYEVENT_3_T4197061729_H
#define UNITYEVENT_3_T4197061729_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`3<System.Single,System.Single,System.Single>
struct  UnityEvent_3_t4197061729  : public UnityEventBase_t828812576
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`3::m_InvokeArray
	ObjectU5BU5D_t3614634134* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_3_t4197061729, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3614634134* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3614634134* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_3_T4197061729_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t1328083999* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t1328083999* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t1328083999** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t1328083999* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef VECTOR3_T2243707580_H
#define VECTOR3_T2243707580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t2243707580 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t2243707580_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t2243707580  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t2243707580  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t2243707580  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t2243707580  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t2243707580  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t2243707580  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t2243707580  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t2243707580  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t2243707580  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t2243707580  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___zeroVector_4)); }
	inline Vector3_t2243707580  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t2243707580 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t2243707580  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___oneVector_5)); }
	inline Vector3_t2243707580  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t2243707580 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t2243707580  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___upVector_6)); }
	inline Vector3_t2243707580  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t2243707580 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t2243707580  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___downVector_7)); }
	inline Vector3_t2243707580  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t2243707580 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t2243707580  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___leftVector_8)); }
	inline Vector3_t2243707580  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t2243707580 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t2243707580  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___rightVector_9)); }
	inline Vector3_t2243707580  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t2243707580 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t2243707580  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___forwardVector_10)); }
	inline Vector3_t2243707580  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t2243707580 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t2243707580  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___backVector_11)); }
	inline Vector3_t2243707580  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t2243707580 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t2243707580  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t2243707580  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t2243707580 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t2243707580  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t2243707580  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t2243707580 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t2243707580  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T2243707580_H
#ifndef VOID_T1841601450_H
#define VOID_T1841601450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1841601450 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1841601450_H
#ifndef HSVCOLOR_T1057062332_H
#define HSVCOLOR_T1057062332_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HsvColor
struct  HsvColor_t1057062332 
{
public:
	// System.Double HsvColor::H
	double ___H_0;
	// System.Double HsvColor::S
	double ___S_1;
	// System.Double HsvColor::V
	double ___V_2;

public:
	inline static int32_t get_offset_of_H_0() { return static_cast<int32_t>(offsetof(HsvColor_t1057062332, ___H_0)); }
	inline double get_H_0() const { return ___H_0; }
	inline double* get_address_of_H_0() { return &___H_0; }
	inline void set_H_0(double value)
	{
		___H_0 = value;
	}

	inline static int32_t get_offset_of_S_1() { return static_cast<int32_t>(offsetof(HsvColor_t1057062332, ___S_1)); }
	inline double get_S_1() const { return ___S_1; }
	inline double* get_address_of_S_1() { return &___S_1; }
	inline void set_S_1(double value)
	{
		___S_1 = value;
	}

	inline static int32_t get_offset_of_V_2() { return static_cast<int32_t>(offsetof(HsvColor_t1057062332, ___V_2)); }
	inline double get_V_2() const { return ___V_2; }
	inline double* get_address_of_V_2() { return &___V_2; }
	inline void set_V_2(double value)
	{
		___V_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HSVCOLOR_T1057062332_H
#ifndef TESTDATA_T1138889280_H
#define TESTDATA_T1138889280_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExampleTradeoffAnalytics/TestData
struct  TestData_t1138889280  : public ApplicationData_t1885408412
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TESTDATA_T1138889280_H
#ifndef TESTDATAVALUE_T1171208003_H
#define TESTDATAVALUE_T1171208003_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExampleTradeoffAnalytics/TestDataValue
struct  TestDataValue_t1171208003  : public ApplicationDataValue_t1721671971
{
public:
	// System.Double ExampleTradeoffAnalytics/TestDataValue::<price>k__BackingField
	double ___U3CpriceU3Ek__BackingField_0;
	// System.Double ExampleTradeoffAnalytics/TestDataValue::<weight>k__BackingField
	double ___U3CweightU3Ek__BackingField_1;
	// System.String ExampleTradeoffAnalytics/TestDataValue::<brand>k__BackingField
	String_t* ___U3CbrandU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CpriceU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TestDataValue_t1171208003, ___U3CpriceU3Ek__BackingField_0)); }
	inline double get_U3CpriceU3Ek__BackingField_0() const { return ___U3CpriceU3Ek__BackingField_0; }
	inline double* get_address_of_U3CpriceU3Ek__BackingField_0() { return &___U3CpriceU3Ek__BackingField_0; }
	inline void set_U3CpriceU3Ek__BackingField_0(double value)
	{
		___U3CpriceU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CweightU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TestDataValue_t1171208003, ___U3CweightU3Ek__BackingField_1)); }
	inline double get_U3CweightU3Ek__BackingField_1() const { return ___U3CweightU3Ek__BackingField_1; }
	inline double* get_address_of_U3CweightU3Ek__BackingField_1() { return &___U3CweightU3Ek__BackingField_1; }
	inline void set_U3CweightU3Ek__BackingField_1(double value)
	{
		___U3CweightU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CbrandU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(TestDataValue_t1171208003, ___U3CbrandU3Ek__BackingField_2)); }
	inline String_t* get_U3CbrandU3Ek__BackingField_2() const { return ___U3CbrandU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CbrandU3Ek__BackingField_2() { return &___U3CbrandU3Ek__BackingField_2; }
	inline void set_U3CbrandU3Ek__BackingField_2(String_t* value)
	{
		___U3CbrandU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CbrandU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TESTDATAVALUE_T1171208003_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	IntPtr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline IntPtr_t get_Zero_1() const { return ___Zero_1; }
	inline IntPtr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(IntPtr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef MATRIX4X4_T2933234003_H
#define MATRIX4X4_T2933234003_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t2933234003 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t2933234003_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t2933234003  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t2933234003  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t2933234003  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t2933234003 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t2933234003  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t2933234003  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t2933234003 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t2933234003  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T2933234003_H
#ifndef U24ARRAYTYPEU3D24_T762068664_H
#define U24ARRAYTYPEU3D24_T762068664_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=24
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D24_t762068664 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D24_t762068664__padding[24];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D24_T762068664_H
#ifndef VECTOR4_T2243707581_H
#define VECTOR4_T2243707581_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t2243707581 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t2243707581, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t2243707581, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t2243707581, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t2243707581, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t2243707581_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t2243707581  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t2243707581  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t2243707581  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t2243707581  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t2243707581_StaticFields, ___zeroVector_5)); }
	inline Vector4_t2243707581  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t2243707581 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t2243707581  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t2243707581_StaticFields, ___oneVector_6)); }
	inline Vector4_t2243707581  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t2243707581 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t2243707581  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t2243707581_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t2243707581  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t2243707581 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t2243707581  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t2243707581_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t2243707581  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t2243707581 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t2243707581  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T2243707581_H
#ifndef TIMESPAN_T3430258949_H
#define TIMESPAN_T3430258949_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t3430258949 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_8;

public:
	inline static int32_t get_offset_of__ticks_8() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949, ____ticks_8)); }
	inline int64_t get__ticks_8() const { return ____ticks_8; }
	inline int64_t* get_address_of__ticks_8() { return &____ticks_8; }
	inline void set__ticks_8(int64_t value)
	{
		____ticks_8 = value;
	}
};

struct TimeSpan_t3430258949_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t3430258949  ___MaxValue_5;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t3430258949  ___MinValue_6;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t3430258949  ___Zero_7;

public:
	inline static int32_t get_offset_of_MaxValue_5() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949_StaticFields, ___MaxValue_5)); }
	inline TimeSpan_t3430258949  get_MaxValue_5() const { return ___MaxValue_5; }
	inline TimeSpan_t3430258949 * get_address_of_MaxValue_5() { return &___MaxValue_5; }
	inline void set_MaxValue_5(TimeSpan_t3430258949  value)
	{
		___MaxValue_5 = value;
	}

	inline static int32_t get_offset_of_MinValue_6() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949_StaticFields, ___MinValue_6)); }
	inline TimeSpan_t3430258949  get_MinValue_6() const { return ___MinValue_6; }
	inline TimeSpan_t3430258949 * get_address_of_MinValue_6() { return &___MinValue_6; }
	inline void set_MinValue_6(TimeSpan_t3430258949  value)
	{
		___MinValue_6 = value;
	}

	inline static int32_t get_offset_of_Zero_7() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949_StaticFields, ___Zero_7)); }
	inline TimeSpan_t3430258949  get_Zero_7() const { return ___Zero_7; }
	inline TimeSpan_t3430258949 * get_address_of_Zero_7() { return &___Zero_7; }
	inline void set_Zero_7(TimeSpan_t3430258949  value)
	{
		___Zero_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T3430258949_H
#ifndef KEYVALUEPAIR_2_T2361573779_H
#define KEYVALUEPAIR_2_T2361573779_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.String,System.Object>
struct  KeyValuePair_2_t2361573779 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2361573779, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2361573779, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T2361573779_H
#ifndef SPRITESTATE_T1353336012_H
#define SPRITESTATE_T1353336012_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.SpriteState
struct  SpriteState_t1353336012 
{
public:
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_t309593783 * ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_t309593783 * ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_t309593783 * ___m_DisabledSprite_2;

public:
	inline static int32_t get_offset_of_m_HighlightedSprite_0() { return static_cast<int32_t>(offsetof(SpriteState_t1353336012, ___m_HighlightedSprite_0)); }
	inline Sprite_t309593783 * get_m_HighlightedSprite_0() const { return ___m_HighlightedSprite_0; }
	inline Sprite_t309593783 ** get_address_of_m_HighlightedSprite_0() { return &___m_HighlightedSprite_0; }
	inline void set_m_HighlightedSprite_0(Sprite_t309593783 * value)
	{
		___m_HighlightedSprite_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_HighlightedSprite_0), value);
	}

	inline static int32_t get_offset_of_m_PressedSprite_1() { return static_cast<int32_t>(offsetof(SpriteState_t1353336012, ___m_PressedSprite_1)); }
	inline Sprite_t309593783 * get_m_PressedSprite_1() const { return ___m_PressedSprite_1; }
	inline Sprite_t309593783 ** get_address_of_m_PressedSprite_1() { return &___m_PressedSprite_1; }
	inline void set_m_PressedSprite_1(Sprite_t309593783 * value)
	{
		___m_PressedSprite_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PressedSprite_1), value);
	}

	inline static int32_t get_offset_of_m_DisabledSprite_2() { return static_cast<int32_t>(offsetof(SpriteState_t1353336012, ___m_DisabledSprite_2)); }
	inline Sprite_t309593783 * get_m_DisabledSprite_2() const { return ___m_DisabledSprite_2; }
	inline Sprite_t309593783 ** get_address_of_m_DisabledSprite_2() { return &___m_DisabledSprite_2; }
	inline void set_m_DisabledSprite_2(Sprite_t309593783 * value)
	{
		___m_DisabledSprite_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_DisabledSprite_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t1353336012_marshaled_pinvoke
{
	Sprite_t309593783 * ___m_HighlightedSprite_0;
	Sprite_t309593783 * ___m_PressedSprite_1;
	Sprite_t309593783 * ___m_DisabledSprite_2;
};
// Native definition for COM marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t1353336012_marshaled_com
{
	Sprite_t309593783 * ___m_HighlightedSprite_0;
	Sprite_t309593783 * ___m_PressedSprite_1;
	Sprite_t309593783 * ___m_DisabledSprite_2;
};
#endif // SPRITESTATE_T1353336012_H
#ifndef VECTOR2_T2243707579_H
#define VECTOR2_T2243707579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2243707579 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2243707579, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2243707579, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2243707579_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2243707579  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2243707579  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2243707579  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2243707579  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2243707579  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2243707579  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2243707579  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2243707579  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2243707579  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2243707579 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2243707579  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___oneVector_3)); }
	inline Vector2_t2243707579  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2243707579 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2243707579  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___upVector_4)); }
	inline Vector2_t2243707579  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2243707579 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2243707579  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___downVector_5)); }
	inline Vector2_t2243707579  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2243707579 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2243707579  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___leftVector_6)); }
	inline Vector2_t2243707579  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2243707579 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2243707579  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___rightVector_7)); }
	inline Vector2_t2243707579  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2243707579 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2243707579  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2243707579  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2243707579 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2243707579  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2243707579  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2243707579 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2243707579  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2243707579_H
#ifndef QUATERNION_T4030073918_H
#define QUATERNION_T4030073918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t4030073918 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t4030073918_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t4030073918  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t4030073918  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t4030073918 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t4030073918  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T4030073918_H
#ifndef UNITYEVENT_2_T2016657100_H
#define UNITYEVENT_2_T2016657100_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`2<System.Single,System.Single>
struct  UnityEvent_2_t2016657100  : public UnityEventBase_t828812576
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`2::m_InvokeArray
	ObjectU5BU5D_t3614634134* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_2_t2016657100, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3614634134* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3614634134* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_2_T2016657100_H
#ifndef DRIVENRECTTRANSFORMTRACKER_T154385424_H
#define DRIVENRECTTRANSFORMTRACKER_T154385424_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.DrivenRectTransformTracker
struct  DrivenRectTransformTracker_t154385424 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRIVENRECTTRANSFORMTRACKER_T154385424_H
#ifndef SINGLE_T2076509932_H
#define SINGLE_T2076509932_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t2076509932 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t2076509932, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T2076509932_H
#ifndef COLORBLOCK_T2652774230_H
#define COLORBLOCK_T2652774230_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ColorBlock
struct  ColorBlock_t2652774230 
{
public:
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_NormalColor
	Color_t2020392075  ___m_NormalColor_0;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_HighlightedColor
	Color_t2020392075  ___m_HighlightedColor_1;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_PressedColor
	Color_t2020392075  ___m_PressedColor_2;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_DisabledColor
	Color_t2020392075  ___m_DisabledColor_3;
	// System.Single UnityEngine.UI.ColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_4;
	// System.Single UnityEngine.UI.ColorBlock::m_FadeDuration
	float ___m_FadeDuration_5;

public:
	inline static int32_t get_offset_of_m_NormalColor_0() { return static_cast<int32_t>(offsetof(ColorBlock_t2652774230, ___m_NormalColor_0)); }
	inline Color_t2020392075  get_m_NormalColor_0() const { return ___m_NormalColor_0; }
	inline Color_t2020392075 * get_address_of_m_NormalColor_0() { return &___m_NormalColor_0; }
	inline void set_m_NormalColor_0(Color_t2020392075  value)
	{
		___m_NormalColor_0 = value;
	}

	inline static int32_t get_offset_of_m_HighlightedColor_1() { return static_cast<int32_t>(offsetof(ColorBlock_t2652774230, ___m_HighlightedColor_1)); }
	inline Color_t2020392075  get_m_HighlightedColor_1() const { return ___m_HighlightedColor_1; }
	inline Color_t2020392075 * get_address_of_m_HighlightedColor_1() { return &___m_HighlightedColor_1; }
	inline void set_m_HighlightedColor_1(Color_t2020392075  value)
	{
		___m_HighlightedColor_1 = value;
	}

	inline static int32_t get_offset_of_m_PressedColor_2() { return static_cast<int32_t>(offsetof(ColorBlock_t2652774230, ___m_PressedColor_2)); }
	inline Color_t2020392075  get_m_PressedColor_2() const { return ___m_PressedColor_2; }
	inline Color_t2020392075 * get_address_of_m_PressedColor_2() { return &___m_PressedColor_2; }
	inline void set_m_PressedColor_2(Color_t2020392075  value)
	{
		___m_PressedColor_2 = value;
	}

	inline static int32_t get_offset_of_m_DisabledColor_3() { return static_cast<int32_t>(offsetof(ColorBlock_t2652774230, ___m_DisabledColor_3)); }
	inline Color_t2020392075  get_m_DisabledColor_3() const { return ___m_DisabledColor_3; }
	inline Color_t2020392075 * get_address_of_m_DisabledColor_3() { return &___m_DisabledColor_3; }
	inline void set_m_DisabledColor_3(Color_t2020392075  value)
	{
		___m_DisabledColor_3 = value;
	}

	inline static int32_t get_offset_of_m_ColorMultiplier_4() { return static_cast<int32_t>(offsetof(ColorBlock_t2652774230, ___m_ColorMultiplier_4)); }
	inline float get_m_ColorMultiplier_4() const { return ___m_ColorMultiplier_4; }
	inline float* get_address_of_m_ColorMultiplier_4() { return &___m_ColorMultiplier_4; }
	inline void set_m_ColorMultiplier_4(float value)
	{
		___m_ColorMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_m_FadeDuration_5() { return static_cast<int32_t>(offsetof(ColorBlock_t2652774230, ___m_FadeDuration_5)); }
	inline float get_m_FadeDuration_5() const { return ___m_FadeDuration_5; }
	inline float* get_address_of_m_FadeDuration_5() { return &___m_FadeDuration_5; }
	inline void set_m_FadeDuration_5(float value)
	{
		___m_FadeDuration_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORBLOCK_T2652774230_H
#ifndef DIRECTION_T1525323322_H
#define DIRECTION_T1525323322_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Slider/Direction
struct  Direction_t1525323322 
{
public:
	// System.Int32 UnityEngine.UI.Slider/Direction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Direction_t1525323322, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTION_T1525323322_H
#ifndef SELECTIONSTATE_T3187567897_H
#define SELECTIONSTATE_T3187567897_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/SelectionState
struct  SelectionState_t3187567897 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/SelectionState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SelectionState_t3187567897, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONSTATE_T3187567897_H
#ifndef OBJECT_T1021602117_H
#define OBJECT_T1021602117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1021602117  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	IntPtr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1021602117, ___m_CachedPtr_0)); }
	inline IntPtr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline IntPtr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(IntPtr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1021602117_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1021602117_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1021602117_H
#ifndef MODE_T1081683921_H
#define MODE_T1081683921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation/Mode
struct  Mode_t1081683921 
{
public:
	// System.Int32 UnityEngine.UI.Navigation/Mode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Mode_t1081683921, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T1081683921_H
#ifndef DATETIMEKIND_T2186819611_H
#define DATETIMEKIND_T2186819611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeKind
struct  DateTimeKind_t2186819611 
{
public:
	// System.Int32 System.DateTimeKind::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeKind_t2186819611, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEKIND_T2186819611_H
#ifndef MENUSCENE_T1232739575_H
#define MENUSCENE_T1232739575_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MenuScene
struct  MenuScene_t1232739575  : public RuntimeObject
{
public:
	// System.String MenuScene::m_SceneName
	String_t* ___m_SceneName_0;
	// System.String MenuScene::m_SceneDesc
	String_t* ___m_SceneDesc_1;
	// UnityEngine.Vector3 MenuScene::m_CustomBackButtonPosition
	Vector3_t2243707580  ___m_CustomBackButtonPosition_2;
	// UnityEngine.Vector2 MenuScene::m_CustomBackButtonScale
	Vector2_t2243707579  ___m_CustomBackButtonScale_3;
	// System.Boolean MenuScene::m_IsVisibleBackButton
	bool ___m_IsVisibleBackButton_4;

public:
	inline static int32_t get_offset_of_m_SceneName_0() { return static_cast<int32_t>(offsetof(MenuScene_t1232739575, ___m_SceneName_0)); }
	inline String_t* get_m_SceneName_0() const { return ___m_SceneName_0; }
	inline String_t** get_address_of_m_SceneName_0() { return &___m_SceneName_0; }
	inline void set_m_SceneName_0(String_t* value)
	{
		___m_SceneName_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_SceneName_0), value);
	}

	inline static int32_t get_offset_of_m_SceneDesc_1() { return static_cast<int32_t>(offsetof(MenuScene_t1232739575, ___m_SceneDesc_1)); }
	inline String_t* get_m_SceneDesc_1() const { return ___m_SceneDesc_1; }
	inline String_t** get_address_of_m_SceneDesc_1() { return &___m_SceneDesc_1; }
	inline void set_m_SceneDesc_1(String_t* value)
	{
		___m_SceneDesc_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_SceneDesc_1), value);
	}

	inline static int32_t get_offset_of_m_CustomBackButtonPosition_2() { return static_cast<int32_t>(offsetof(MenuScene_t1232739575, ___m_CustomBackButtonPosition_2)); }
	inline Vector3_t2243707580  get_m_CustomBackButtonPosition_2() const { return ___m_CustomBackButtonPosition_2; }
	inline Vector3_t2243707580 * get_address_of_m_CustomBackButtonPosition_2() { return &___m_CustomBackButtonPosition_2; }
	inline void set_m_CustomBackButtonPosition_2(Vector3_t2243707580  value)
	{
		___m_CustomBackButtonPosition_2 = value;
	}

	inline static int32_t get_offset_of_m_CustomBackButtonScale_3() { return static_cast<int32_t>(offsetof(MenuScene_t1232739575, ___m_CustomBackButtonScale_3)); }
	inline Vector2_t2243707579  get_m_CustomBackButtonScale_3() const { return ___m_CustomBackButtonScale_3; }
	inline Vector2_t2243707579 * get_address_of_m_CustomBackButtonScale_3() { return &___m_CustomBackButtonScale_3; }
	inline void set_m_CustomBackButtonScale_3(Vector2_t2243707579  value)
	{
		___m_CustomBackButtonScale_3 = value;
	}

	inline static int32_t get_offset_of_m_IsVisibleBackButton_4() { return static_cast<int32_t>(offsetof(MenuScene_t1232739575, ___m_IsVisibleBackButton_4)); }
	inline bool get_m_IsVisibleBackButton_4() const { return ___m_IsVisibleBackButton_4; }
	inline bool* get_address_of_m_IsVisibleBackButton_4() { return &___m_IsVisibleBackButton_4; }
	inline void set_m_IsVisibleBackButton_4(bool value)
	{
		___m_IsVisibleBackButton_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MENUSCENE_T1232739575_H
#ifndef ARTRACKINGSTATE_T2048880995_H
#define ARTRACKINGSTATE_T2048880995_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARTrackingState
struct  ARTrackingState_t2048880995 
{
public:
	// System.Int32 UnityEngine.XR.iOS.ARTrackingState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ARTrackingState_t2048880995, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARTRACKINGSTATE_T2048880995_H
#ifndef ARTRACKINGSTATEREASON_T4227173799_H
#define ARTRACKINGSTATEREASON_T4227173799_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARTrackingStateReason
struct  ARTrackingStateReason_t4227173799 
{
public:
	// System.Int32 UnityEngine.XR.iOS.ARTrackingStateReason::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ARTrackingStateReason_t4227173799, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARTRACKINGSTATEREASON_T4227173799_H
#ifndef DELEGATE_T3022476291_H
#define DELEGATE_T3022476291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t3022476291  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	IntPtr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	IntPtr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	IntPtr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	IntPtr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1572802995 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___invoke_impl_1)); }
	inline IntPtr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline IntPtr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(IntPtr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_3)); }
	inline IntPtr_t get_method_3() const { return ___method_3; }
	inline IntPtr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(IntPtr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___delegate_trampoline_4)); }
	inline IntPtr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline IntPtr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(IntPtr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_code_5)); }
	inline IntPtr_t get_method_code_5() const { return ___method_code_5; }
	inline IntPtr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(IntPtr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___data_8)); }
	inline DelegateData_t1572802995 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1572802995 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1572802995 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T3022476291_H
#ifndef UNITYARMATRIX4X4_T100931615_H
#define UNITYARMATRIX4X4_T100931615_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARMatrix4x4
struct  UnityARMatrix4x4_t100931615 
{
public:
	// UnityEngine.Vector4 UnityEngine.XR.iOS.UnityARMatrix4x4::column0
	Vector4_t2243707581  ___column0_0;
	// UnityEngine.Vector4 UnityEngine.XR.iOS.UnityARMatrix4x4::column1
	Vector4_t2243707581  ___column1_1;
	// UnityEngine.Vector4 UnityEngine.XR.iOS.UnityARMatrix4x4::column2
	Vector4_t2243707581  ___column2_2;
	// UnityEngine.Vector4 UnityEngine.XR.iOS.UnityARMatrix4x4::column3
	Vector4_t2243707581  ___column3_3;

public:
	inline static int32_t get_offset_of_column0_0() { return static_cast<int32_t>(offsetof(UnityARMatrix4x4_t100931615, ___column0_0)); }
	inline Vector4_t2243707581  get_column0_0() const { return ___column0_0; }
	inline Vector4_t2243707581 * get_address_of_column0_0() { return &___column0_0; }
	inline void set_column0_0(Vector4_t2243707581  value)
	{
		___column0_0 = value;
	}

	inline static int32_t get_offset_of_column1_1() { return static_cast<int32_t>(offsetof(UnityARMatrix4x4_t100931615, ___column1_1)); }
	inline Vector4_t2243707581  get_column1_1() const { return ___column1_1; }
	inline Vector4_t2243707581 * get_address_of_column1_1() { return &___column1_1; }
	inline void set_column1_1(Vector4_t2243707581  value)
	{
		___column1_1 = value;
	}

	inline static int32_t get_offset_of_column2_2() { return static_cast<int32_t>(offsetof(UnityARMatrix4x4_t100931615, ___column2_2)); }
	inline Vector4_t2243707581  get_column2_2() const { return ___column2_2; }
	inline Vector4_t2243707581 * get_address_of_column2_2() { return &___column2_2; }
	inline void set_column2_2(Vector4_t2243707581  value)
	{
		___column2_2 = value;
	}

	inline static int32_t get_offset_of_column3_3() { return static_cast<int32_t>(offsetof(UnityARMatrix4x4_t100931615, ___column3_3)); }
	inline Vector4_t2243707581  get_column3_3() const { return ___column3_3; }
	inline Vector4_t2243707581 * get_address_of_column3_3() { return &___column3_3; }
	inline void set_column3_3(Vector4_t2243707581  value)
	{
		___column3_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARMATRIX4X4_T100931615_H
#ifndef ENUMERATOR_T1629285963_H
#define ENUMERATOR_T1629285963_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Object>
struct  Enumerator_t1629285963 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::dictionary
	Dictionary_2_t309261261 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::stamp
	int32_t ___stamp_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::current
	KeyValuePair_2_t2361573779  ___current_3;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_t1629285963, ___dictionary_0)); }
	inline Dictionary_2_t309261261 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t309261261 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t309261261 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t1629285963, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_stamp_2() { return static_cast<int32_t>(offsetof(Enumerator_t1629285963, ___stamp_2)); }
	inline int32_t get_stamp_2() const { return ___stamp_2; }
	inline int32_t* get_address_of_stamp_2() { return &___stamp_2; }
	inline void set_stamp_2(int32_t value)
	{
		___stamp_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t1629285963, ___current_3)); }
	inline KeyValuePair_2_t2361573779  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t2361573779 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t2361573779  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T1629285963_H
#ifndef TRANSITION_T605142169_H
#define TRANSITION_T605142169_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/Transition
struct  Transition_t605142169 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/Transition::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Transition_t605142169, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSITION_T605142169_H
#ifndef LOGLEVEL_T3918900206_H
#define LOGLEVEL_T3918900206_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Logging.LogLevel
struct  LogLevel_t3918900206 
{
public:
	// System.Int32 IBM.Watson.DeveloperCloud.Logging.LogLevel::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LogLevel_t3918900206, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGLEVEL_T3918900206_H
#ifndef ARPLANEANCHORALIGNMENT_T2379298071_H
#define ARPLANEANCHORALIGNMENT_T2379298071_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARPlaneAnchorAlignment
struct  ARPlaneAnchorAlignment_t2379298071 
{
public:
	// System.Int64 UnityEngine.XR.iOS.ARPlaneAnchorAlignment::value__
	int64_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ARPlaneAnchorAlignment_t2379298071, ___value___1)); }
	inline int64_t get_value___1() const { return ___value___1; }
	inline int64_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int64_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARPLANEANCHORALIGNMENT_T2379298071_H
#ifndef HSVCHANGEDEVENT_T1170297569_H
#define HSVCHANGEDEVENT_T1170297569_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HSVChangedEvent
struct  HSVChangedEvent_t1170297569  : public UnityEvent_3_t4197061729
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HSVCHANGEDEVENT_T1170297569_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1486305143_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1486305143_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t1486305143  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=24 <PrivateImplementationDetails>::$field-8E7629AD5AF686202B8CB7C014505C432FFE31E6
	U24ArrayTypeU3D24_t762068664  ___U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0;

public:
	inline static int32_t get_offset_of_U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields, ___U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0)); }
	inline U24ArrayTypeU3D24_t762068664  get_U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0() const { return ___U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0; }
	inline U24ArrayTypeU3D24_t762068664 * get_address_of_U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0() { return &___U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0; }
	inline void set_U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0(U24ArrayTypeU3D24_t762068664  value)
	{
		___U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1486305143_H
#ifndef COLORVALUES_T3063098635_H
#define COLORVALUES_T3063098635_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorValues
struct  ColorValues_t3063098635 
{
public:
	// System.Int32 ColorValues::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ColorValues_t3063098635, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORVALUES_T3063098635_H
#ifndef COLORCHANGEDEVENT_T2990895397_H
#define COLORCHANGEDEVENT_T2990895397_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorChangedEvent
struct  ColorChangedEvent_t2990895397  : public UnityEvent_1_t2058742090
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORCHANGEDEVENT_T2990895397_H
#ifndef DIRECTION_T1632189177_H
#define DIRECTION_T1632189177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BoxSlider/Direction
struct  Direction_t1632189177 
{
public:
	// System.Int32 UnityEngine.UI.BoxSlider/Direction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Direction_t1632189177, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTION_T1632189177_H
#ifndef AXIS_T3966514019_H
#define AXIS_T3966514019_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BoxSlider/Axis
struct  Axis_t3966514019 
{
public:
	// System.Int32 UnityEngine.UI.BoxSlider/Axis::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Axis_t3966514019, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXIS_T3966514019_H
#ifndef BOXSLIDEREVENT_T1774115848_H
#define BOXSLIDEREVENT_T1774115848_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BoxSlider/BoxSliderEvent
struct  BoxSliderEvent_t1774115848  : public UnityEvent_2_t2016657100
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOXSLIDEREVENT_T1774115848_H
#ifndef INTERNAL_UNITYARCAMERA_T2580192745_H
#define INTERNAL_UNITYARCAMERA_T2580192745_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.internal_UnityARCamera
struct  internal_UnityARCamera_t2580192745 
{
public:
	// UnityEngine.XR.iOS.UnityARMatrix4x4 UnityEngine.XR.iOS.internal_UnityARCamera::worldTransform
	UnityARMatrix4x4_t100931615  ___worldTransform_0;
	// UnityEngine.XR.iOS.UnityARMatrix4x4 UnityEngine.XR.iOS.internal_UnityARCamera::projectionMatrix
	UnityARMatrix4x4_t100931615  ___projectionMatrix_1;
	// UnityEngine.XR.iOS.ARTrackingState UnityEngine.XR.iOS.internal_UnityARCamera::trackingState
	int32_t ___trackingState_2;
	// UnityEngine.XR.iOS.ARTrackingStateReason UnityEngine.XR.iOS.internal_UnityARCamera::trackingReason
	int32_t ___trackingReason_3;
	// System.UInt32 UnityEngine.XR.iOS.internal_UnityARCamera::getPointCloudData
	uint32_t ___getPointCloudData_4;

public:
	inline static int32_t get_offset_of_worldTransform_0() { return static_cast<int32_t>(offsetof(internal_UnityARCamera_t2580192745, ___worldTransform_0)); }
	inline UnityARMatrix4x4_t100931615  get_worldTransform_0() const { return ___worldTransform_0; }
	inline UnityARMatrix4x4_t100931615 * get_address_of_worldTransform_0() { return &___worldTransform_0; }
	inline void set_worldTransform_0(UnityARMatrix4x4_t100931615  value)
	{
		___worldTransform_0 = value;
	}

	inline static int32_t get_offset_of_projectionMatrix_1() { return static_cast<int32_t>(offsetof(internal_UnityARCamera_t2580192745, ___projectionMatrix_1)); }
	inline UnityARMatrix4x4_t100931615  get_projectionMatrix_1() const { return ___projectionMatrix_1; }
	inline UnityARMatrix4x4_t100931615 * get_address_of_projectionMatrix_1() { return &___projectionMatrix_1; }
	inline void set_projectionMatrix_1(UnityARMatrix4x4_t100931615  value)
	{
		___projectionMatrix_1 = value;
	}

	inline static int32_t get_offset_of_trackingState_2() { return static_cast<int32_t>(offsetof(internal_UnityARCamera_t2580192745, ___trackingState_2)); }
	inline int32_t get_trackingState_2() const { return ___trackingState_2; }
	inline int32_t* get_address_of_trackingState_2() { return &___trackingState_2; }
	inline void set_trackingState_2(int32_t value)
	{
		___trackingState_2 = value;
	}

	inline static int32_t get_offset_of_trackingReason_3() { return static_cast<int32_t>(offsetof(internal_UnityARCamera_t2580192745, ___trackingReason_3)); }
	inline int32_t get_trackingReason_3() const { return ___trackingReason_3; }
	inline int32_t* get_address_of_trackingReason_3() { return &___trackingReason_3; }
	inline void set_trackingReason_3(int32_t value)
	{
		___trackingReason_3 = value;
	}

	inline static int32_t get_offset_of_getPointCloudData_4() { return static_cast<int32_t>(offsetof(internal_UnityARCamera_t2580192745, ___getPointCloudData_4)); }
	inline uint32_t get_getPointCloudData_4() const { return ___getPointCloudData_4; }
	inline uint32_t* get_address_of_getPointCloudData_4() { return &___getPointCloudData_4; }
	inline void set_getPointCloudData_4(uint32_t value)
	{
		___getPointCloudData_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNAL_UNITYARCAMERA_T2580192745_H
#ifndef DATETIME_T693205669_H
#define DATETIME_T693205669_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t693205669 
{
public:
	// System.TimeSpan System.DateTime::ticks
	TimeSpan_t3430258949  ___ticks_10;
	// System.DateTimeKind System.DateTime::kind
	int32_t ___kind_11;

public:
	inline static int32_t get_offset_of_ticks_10() { return static_cast<int32_t>(offsetof(DateTime_t693205669, ___ticks_10)); }
	inline TimeSpan_t3430258949  get_ticks_10() const { return ___ticks_10; }
	inline TimeSpan_t3430258949 * get_address_of_ticks_10() { return &___ticks_10; }
	inline void set_ticks_10(TimeSpan_t3430258949  value)
	{
		___ticks_10 = value;
	}

	inline static int32_t get_offset_of_kind_11() { return static_cast<int32_t>(offsetof(DateTime_t693205669, ___kind_11)); }
	inline int32_t get_kind_11() const { return ___kind_11; }
	inline int32_t* get_address_of_kind_11() { return &___kind_11; }
	inline void set_kind_11(int32_t value)
	{
		___kind_11 = value;
	}
};

struct DateTime_t693205669_StaticFields
{
public:
	// System.DateTime System.DateTime::MaxValue
	DateTime_t693205669  ___MaxValue_12;
	// System.DateTime System.DateTime::MinValue
	DateTime_t693205669  ___MinValue_13;
	// System.String[] System.DateTime::ParseTimeFormats
	StringU5BU5D_t1642385972* ___ParseTimeFormats_14;
	// System.String[] System.DateTime::ParseYearDayMonthFormats
	StringU5BU5D_t1642385972* ___ParseYearDayMonthFormats_15;
	// System.String[] System.DateTime::ParseYearMonthDayFormats
	StringU5BU5D_t1642385972* ___ParseYearMonthDayFormats_16;
	// System.String[] System.DateTime::ParseDayMonthYearFormats
	StringU5BU5D_t1642385972* ___ParseDayMonthYearFormats_17;
	// System.String[] System.DateTime::ParseMonthDayYearFormats
	StringU5BU5D_t1642385972* ___ParseMonthDayYearFormats_18;
	// System.String[] System.DateTime::MonthDayShortFormats
	StringU5BU5D_t1642385972* ___MonthDayShortFormats_19;
	// System.String[] System.DateTime::DayMonthShortFormats
	StringU5BU5D_t1642385972* ___DayMonthShortFormats_20;
	// System.Int32[] System.DateTime::daysmonth
	Int32U5BU5D_t3030399641* ___daysmonth_21;
	// System.Int32[] System.DateTime::daysmonthleap
	Int32U5BU5D_t3030399641* ___daysmonthleap_22;
	// System.Object System.DateTime::to_local_time_span_object
	RuntimeObject * ___to_local_time_span_object_23;
	// System.Int64 System.DateTime::last_now
	int64_t ___last_now_24;

public:
	inline static int32_t get_offset_of_MaxValue_12() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___MaxValue_12)); }
	inline DateTime_t693205669  get_MaxValue_12() const { return ___MaxValue_12; }
	inline DateTime_t693205669 * get_address_of_MaxValue_12() { return &___MaxValue_12; }
	inline void set_MaxValue_12(DateTime_t693205669  value)
	{
		___MaxValue_12 = value;
	}

	inline static int32_t get_offset_of_MinValue_13() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___MinValue_13)); }
	inline DateTime_t693205669  get_MinValue_13() const { return ___MinValue_13; }
	inline DateTime_t693205669 * get_address_of_MinValue_13() { return &___MinValue_13; }
	inline void set_MinValue_13(DateTime_t693205669  value)
	{
		___MinValue_13 = value;
	}

	inline static int32_t get_offset_of_ParseTimeFormats_14() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseTimeFormats_14)); }
	inline StringU5BU5D_t1642385972* get_ParseTimeFormats_14() const { return ___ParseTimeFormats_14; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseTimeFormats_14() { return &___ParseTimeFormats_14; }
	inline void set_ParseTimeFormats_14(StringU5BU5D_t1642385972* value)
	{
		___ParseTimeFormats_14 = value;
		Il2CppCodeGenWriteBarrier((&___ParseTimeFormats_14), value);
	}

	inline static int32_t get_offset_of_ParseYearDayMonthFormats_15() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseYearDayMonthFormats_15)); }
	inline StringU5BU5D_t1642385972* get_ParseYearDayMonthFormats_15() const { return ___ParseYearDayMonthFormats_15; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseYearDayMonthFormats_15() { return &___ParseYearDayMonthFormats_15; }
	inline void set_ParseYearDayMonthFormats_15(StringU5BU5D_t1642385972* value)
	{
		___ParseYearDayMonthFormats_15 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearDayMonthFormats_15), value);
	}

	inline static int32_t get_offset_of_ParseYearMonthDayFormats_16() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseYearMonthDayFormats_16)); }
	inline StringU5BU5D_t1642385972* get_ParseYearMonthDayFormats_16() const { return ___ParseYearMonthDayFormats_16; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseYearMonthDayFormats_16() { return &___ParseYearMonthDayFormats_16; }
	inline void set_ParseYearMonthDayFormats_16(StringU5BU5D_t1642385972* value)
	{
		___ParseYearMonthDayFormats_16 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearMonthDayFormats_16), value);
	}

	inline static int32_t get_offset_of_ParseDayMonthYearFormats_17() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseDayMonthYearFormats_17)); }
	inline StringU5BU5D_t1642385972* get_ParseDayMonthYearFormats_17() const { return ___ParseDayMonthYearFormats_17; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseDayMonthYearFormats_17() { return &___ParseDayMonthYearFormats_17; }
	inline void set_ParseDayMonthYearFormats_17(StringU5BU5D_t1642385972* value)
	{
		___ParseDayMonthYearFormats_17 = value;
		Il2CppCodeGenWriteBarrier((&___ParseDayMonthYearFormats_17), value);
	}

	inline static int32_t get_offset_of_ParseMonthDayYearFormats_18() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseMonthDayYearFormats_18)); }
	inline StringU5BU5D_t1642385972* get_ParseMonthDayYearFormats_18() const { return ___ParseMonthDayYearFormats_18; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseMonthDayYearFormats_18() { return &___ParseMonthDayYearFormats_18; }
	inline void set_ParseMonthDayYearFormats_18(StringU5BU5D_t1642385972* value)
	{
		___ParseMonthDayYearFormats_18 = value;
		Il2CppCodeGenWriteBarrier((&___ParseMonthDayYearFormats_18), value);
	}

	inline static int32_t get_offset_of_MonthDayShortFormats_19() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___MonthDayShortFormats_19)); }
	inline StringU5BU5D_t1642385972* get_MonthDayShortFormats_19() const { return ___MonthDayShortFormats_19; }
	inline StringU5BU5D_t1642385972** get_address_of_MonthDayShortFormats_19() { return &___MonthDayShortFormats_19; }
	inline void set_MonthDayShortFormats_19(StringU5BU5D_t1642385972* value)
	{
		___MonthDayShortFormats_19 = value;
		Il2CppCodeGenWriteBarrier((&___MonthDayShortFormats_19), value);
	}

	inline static int32_t get_offset_of_DayMonthShortFormats_20() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___DayMonthShortFormats_20)); }
	inline StringU5BU5D_t1642385972* get_DayMonthShortFormats_20() const { return ___DayMonthShortFormats_20; }
	inline StringU5BU5D_t1642385972** get_address_of_DayMonthShortFormats_20() { return &___DayMonthShortFormats_20; }
	inline void set_DayMonthShortFormats_20(StringU5BU5D_t1642385972* value)
	{
		___DayMonthShortFormats_20 = value;
		Il2CppCodeGenWriteBarrier((&___DayMonthShortFormats_20), value);
	}

	inline static int32_t get_offset_of_daysmonth_21() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___daysmonth_21)); }
	inline Int32U5BU5D_t3030399641* get_daysmonth_21() const { return ___daysmonth_21; }
	inline Int32U5BU5D_t3030399641** get_address_of_daysmonth_21() { return &___daysmonth_21; }
	inline void set_daysmonth_21(Int32U5BU5D_t3030399641* value)
	{
		___daysmonth_21 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonth_21), value);
	}

	inline static int32_t get_offset_of_daysmonthleap_22() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___daysmonthleap_22)); }
	inline Int32U5BU5D_t3030399641* get_daysmonthleap_22() const { return ___daysmonthleap_22; }
	inline Int32U5BU5D_t3030399641** get_address_of_daysmonthleap_22() { return &___daysmonthleap_22; }
	inline void set_daysmonthleap_22(Int32U5BU5D_t3030399641* value)
	{
		___daysmonthleap_22 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonthleap_22), value);
	}

	inline static int32_t get_offset_of_to_local_time_span_object_23() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___to_local_time_span_object_23)); }
	inline RuntimeObject * get_to_local_time_span_object_23() const { return ___to_local_time_span_object_23; }
	inline RuntimeObject ** get_address_of_to_local_time_span_object_23() { return &___to_local_time_span_object_23; }
	inline void set_to_local_time_span_object_23(RuntimeObject * value)
	{
		___to_local_time_span_object_23 = value;
		Il2CppCodeGenWriteBarrier((&___to_local_time_span_object_23), value);
	}

	inline static int32_t get_offset_of_last_now_24() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___last_now_24)); }
	inline int64_t get_last_now_24() const { return ___last_now_24; }
	inline int64_t* get_address_of_last_now_24() { return &___last_now_24; }
	inline void set_last_now_24(int64_t value)
	{
		___last_now_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T693205669_H
#ifndef ARPLANEANCHOR_T1439520888_H
#define ARPLANEANCHOR_T1439520888_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARPlaneAnchor
struct  ARPlaneAnchor_t1439520888 
{
public:
	// System.String UnityEngine.XR.iOS.ARPlaneAnchor::identifier
	String_t* ___identifier_0;
	// UnityEngine.Matrix4x4 UnityEngine.XR.iOS.ARPlaneAnchor::transform
	Matrix4x4_t2933234003  ___transform_1;
	// UnityEngine.XR.iOS.ARPlaneAnchorAlignment UnityEngine.XR.iOS.ARPlaneAnchor::alignment
	int64_t ___alignment_2;
	// UnityEngine.Vector3 UnityEngine.XR.iOS.ARPlaneAnchor::center
	Vector3_t2243707580  ___center_3;
	// UnityEngine.Vector3 UnityEngine.XR.iOS.ARPlaneAnchor::extent
	Vector3_t2243707580  ___extent_4;

public:
	inline static int32_t get_offset_of_identifier_0() { return static_cast<int32_t>(offsetof(ARPlaneAnchor_t1439520888, ___identifier_0)); }
	inline String_t* get_identifier_0() const { return ___identifier_0; }
	inline String_t** get_address_of_identifier_0() { return &___identifier_0; }
	inline void set_identifier_0(String_t* value)
	{
		___identifier_0 = value;
		Il2CppCodeGenWriteBarrier((&___identifier_0), value);
	}

	inline static int32_t get_offset_of_transform_1() { return static_cast<int32_t>(offsetof(ARPlaneAnchor_t1439520888, ___transform_1)); }
	inline Matrix4x4_t2933234003  get_transform_1() const { return ___transform_1; }
	inline Matrix4x4_t2933234003 * get_address_of_transform_1() { return &___transform_1; }
	inline void set_transform_1(Matrix4x4_t2933234003  value)
	{
		___transform_1 = value;
	}

	inline static int32_t get_offset_of_alignment_2() { return static_cast<int32_t>(offsetof(ARPlaneAnchor_t1439520888, ___alignment_2)); }
	inline int64_t get_alignment_2() const { return ___alignment_2; }
	inline int64_t* get_address_of_alignment_2() { return &___alignment_2; }
	inline void set_alignment_2(int64_t value)
	{
		___alignment_2 = value;
	}

	inline static int32_t get_offset_of_center_3() { return static_cast<int32_t>(offsetof(ARPlaneAnchor_t1439520888, ___center_3)); }
	inline Vector3_t2243707580  get_center_3() const { return ___center_3; }
	inline Vector3_t2243707580 * get_address_of_center_3() { return &___center_3; }
	inline void set_center_3(Vector3_t2243707580  value)
	{
		___center_3 = value;
	}

	inline static int32_t get_offset_of_extent_4() { return static_cast<int32_t>(offsetof(ARPlaneAnchor_t1439520888, ___extent_4)); }
	inline Vector3_t2243707580  get_extent_4() const { return ___extent_4; }
	inline Vector3_t2243707580 * get_address_of_extent_4() { return &___extent_4; }
	inline void set_extent_4(Vector3_t2243707580  value)
	{
		___extent_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.XR.iOS.ARPlaneAnchor
struct ARPlaneAnchor_t1439520888_marshaled_pinvoke
{
	char* ___identifier_0;
	Matrix4x4_t2933234003  ___transform_1;
	int64_t ___alignment_2;
	Vector3_t2243707580  ___center_3;
	Vector3_t2243707580  ___extent_4;
};
// Native definition for COM marshalling of UnityEngine.XR.iOS.ARPlaneAnchor
struct ARPlaneAnchor_t1439520888_marshaled_com
{
	Il2CppChar* ___identifier_0;
	Matrix4x4_t2933234003  ___transform_1;
	int64_t ___alignment_2;
	Vector3_t2243707580  ___center_3;
	Vector3_t2243707580  ___extent_4;
};
#endif // ARPLANEANCHOR_T1439520888_H
#ifndef UNITYARCAMERA_T4198559457_H
#define UNITYARCAMERA_T4198559457_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARCamera
struct  UnityARCamera_t4198559457 
{
public:
	// UnityEngine.XR.iOS.UnityARMatrix4x4 UnityEngine.XR.iOS.UnityARCamera::worldTransform
	UnityARMatrix4x4_t100931615  ___worldTransform_0;
	// UnityEngine.XR.iOS.UnityARMatrix4x4 UnityEngine.XR.iOS.UnityARCamera::projectionMatrix
	UnityARMatrix4x4_t100931615  ___projectionMatrix_1;
	// UnityEngine.XR.iOS.ARTrackingState UnityEngine.XR.iOS.UnityARCamera::trackingState
	int32_t ___trackingState_2;
	// UnityEngine.XR.iOS.ARTrackingStateReason UnityEngine.XR.iOS.UnityARCamera::trackingReason
	int32_t ___trackingReason_3;
	// UnityEngine.Vector3[] UnityEngine.XR.iOS.UnityARCamera::pointCloudData
	Vector3U5BU5D_t1172311765* ___pointCloudData_4;

public:
	inline static int32_t get_offset_of_worldTransform_0() { return static_cast<int32_t>(offsetof(UnityARCamera_t4198559457, ___worldTransform_0)); }
	inline UnityARMatrix4x4_t100931615  get_worldTransform_0() const { return ___worldTransform_0; }
	inline UnityARMatrix4x4_t100931615 * get_address_of_worldTransform_0() { return &___worldTransform_0; }
	inline void set_worldTransform_0(UnityARMatrix4x4_t100931615  value)
	{
		___worldTransform_0 = value;
	}

	inline static int32_t get_offset_of_projectionMatrix_1() { return static_cast<int32_t>(offsetof(UnityARCamera_t4198559457, ___projectionMatrix_1)); }
	inline UnityARMatrix4x4_t100931615  get_projectionMatrix_1() const { return ___projectionMatrix_1; }
	inline UnityARMatrix4x4_t100931615 * get_address_of_projectionMatrix_1() { return &___projectionMatrix_1; }
	inline void set_projectionMatrix_1(UnityARMatrix4x4_t100931615  value)
	{
		___projectionMatrix_1 = value;
	}

	inline static int32_t get_offset_of_trackingState_2() { return static_cast<int32_t>(offsetof(UnityARCamera_t4198559457, ___trackingState_2)); }
	inline int32_t get_trackingState_2() const { return ___trackingState_2; }
	inline int32_t* get_address_of_trackingState_2() { return &___trackingState_2; }
	inline void set_trackingState_2(int32_t value)
	{
		___trackingState_2 = value;
	}

	inline static int32_t get_offset_of_trackingReason_3() { return static_cast<int32_t>(offsetof(UnityARCamera_t4198559457, ___trackingReason_3)); }
	inline int32_t get_trackingReason_3() const { return ___trackingReason_3; }
	inline int32_t* get_address_of_trackingReason_3() { return &___trackingReason_3; }
	inline void set_trackingReason_3(int32_t value)
	{
		___trackingReason_3 = value;
	}

	inline static int32_t get_offset_of_pointCloudData_4() { return static_cast<int32_t>(offsetof(UnityARCamera_t4198559457, ___pointCloudData_4)); }
	inline Vector3U5BU5D_t1172311765* get_pointCloudData_4() const { return ___pointCloudData_4; }
	inline Vector3U5BU5D_t1172311765** get_address_of_pointCloudData_4() { return &___pointCloudData_4; }
	inline void set_pointCloudData_4(Vector3U5BU5D_t1172311765* value)
	{
		___pointCloudData_4 = value;
		Il2CppCodeGenWriteBarrier((&___pointCloudData_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.XR.iOS.UnityARCamera
struct UnityARCamera_t4198559457_marshaled_pinvoke
{
	UnityARMatrix4x4_t100931615  ___worldTransform_0;
	UnityARMatrix4x4_t100931615  ___projectionMatrix_1;
	int32_t ___trackingState_2;
	int32_t ___trackingReason_3;
	Vector3_t2243707580 * ___pointCloudData_4;
};
// Native definition for COM marshalling of UnityEngine.XR.iOS.UnityARCamera
struct UnityARCamera_t4198559457_marshaled_com
{
	UnityARMatrix4x4_t100931615  ___worldTransform_0;
	UnityARMatrix4x4_t100931615  ___projectionMatrix_1;
	int32_t ___trackingState_2;
	int32_t ___trackingReason_3;
	Vector3_t2243707580 * ___pointCloudData_4;
};
#endif // UNITYARCAMERA_T4198559457_H
#ifndef UNITYARANCHORDATA_T2901866349_H
#define UNITYARANCHORDATA_T2901866349_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARAnchorData
struct  UnityARAnchorData_t2901866349 
{
public:
	// System.IntPtr UnityEngine.XR.iOS.UnityARAnchorData::ptrIdentifier
	IntPtr_t ___ptrIdentifier_0;
	// UnityEngine.XR.iOS.UnityARMatrix4x4 UnityEngine.XR.iOS.UnityARAnchorData::transform
	UnityARMatrix4x4_t100931615  ___transform_1;
	// UnityEngine.XR.iOS.ARPlaneAnchorAlignment UnityEngine.XR.iOS.UnityARAnchorData::alignment
	int64_t ___alignment_2;
	// UnityEngine.Vector4 UnityEngine.XR.iOS.UnityARAnchorData::center
	Vector4_t2243707581  ___center_3;
	// UnityEngine.Vector4 UnityEngine.XR.iOS.UnityARAnchorData::extent
	Vector4_t2243707581  ___extent_4;

public:
	inline static int32_t get_offset_of_ptrIdentifier_0() { return static_cast<int32_t>(offsetof(UnityARAnchorData_t2901866349, ___ptrIdentifier_0)); }
	inline IntPtr_t get_ptrIdentifier_0() const { return ___ptrIdentifier_0; }
	inline IntPtr_t* get_address_of_ptrIdentifier_0() { return &___ptrIdentifier_0; }
	inline void set_ptrIdentifier_0(IntPtr_t value)
	{
		___ptrIdentifier_0 = value;
	}

	inline static int32_t get_offset_of_transform_1() { return static_cast<int32_t>(offsetof(UnityARAnchorData_t2901866349, ___transform_1)); }
	inline UnityARMatrix4x4_t100931615  get_transform_1() const { return ___transform_1; }
	inline UnityARMatrix4x4_t100931615 * get_address_of_transform_1() { return &___transform_1; }
	inline void set_transform_1(UnityARMatrix4x4_t100931615  value)
	{
		___transform_1 = value;
	}

	inline static int32_t get_offset_of_alignment_2() { return static_cast<int32_t>(offsetof(UnityARAnchorData_t2901866349, ___alignment_2)); }
	inline int64_t get_alignment_2() const { return ___alignment_2; }
	inline int64_t* get_address_of_alignment_2() { return &___alignment_2; }
	inline void set_alignment_2(int64_t value)
	{
		___alignment_2 = value;
	}

	inline static int32_t get_offset_of_center_3() { return static_cast<int32_t>(offsetof(UnityARAnchorData_t2901866349, ___center_3)); }
	inline Vector4_t2243707581  get_center_3() const { return ___center_3; }
	inline Vector4_t2243707581 * get_address_of_center_3() { return &___center_3; }
	inline void set_center_3(Vector4_t2243707581  value)
	{
		___center_3 = value;
	}

	inline static int32_t get_offset_of_extent_4() { return static_cast<int32_t>(offsetof(UnityARAnchorData_t2901866349, ___extent_4)); }
	inline Vector4_t2243707581  get_extent_4() const { return ___extent_4; }
	inline Vector4_t2243707581 * get_address_of_extent_4() { return &___extent_4; }
	inline void set_extent_4(Vector4_t2243707581  value)
	{
		___extent_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARANCHORDATA_T2901866349_H
#ifndef MULTICASTDELEGATE_T3201952435_H
#define MULTICASTDELEGATE_T3201952435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t3201952435  : public Delegate_t3022476291
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t3201952435 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t3201952435 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___prev_9)); }
	inline MulticastDelegate_t3201952435 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t3201952435 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t3201952435 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___kpm_next_10)); }
	inline MulticastDelegate_t3201952435 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t3201952435 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t3201952435 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T3201952435_H
#ifndef NAVIGATION_T1571958496_H
#define NAVIGATION_T1571958496_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation
struct  Navigation_t1571958496 
{
public:
	// UnityEngine.UI.Navigation/Mode UnityEngine.UI.Navigation::m_Mode
	int32_t ___m_Mode_0;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnUp
	Selectable_t1490392188 * ___m_SelectOnUp_1;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnDown
	Selectable_t1490392188 * ___m_SelectOnDown_2;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnLeft
	Selectable_t1490392188 * ___m_SelectOnLeft_3;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnRight
	Selectable_t1490392188 * ___m_SelectOnRight_4;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(Navigation_t1571958496, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_SelectOnUp_1() { return static_cast<int32_t>(offsetof(Navigation_t1571958496, ___m_SelectOnUp_1)); }
	inline Selectable_t1490392188 * get_m_SelectOnUp_1() const { return ___m_SelectOnUp_1; }
	inline Selectable_t1490392188 ** get_address_of_m_SelectOnUp_1() { return &___m_SelectOnUp_1; }
	inline void set_m_SelectOnUp_1(Selectable_t1490392188 * value)
	{
		___m_SelectOnUp_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnUp_1), value);
	}

	inline static int32_t get_offset_of_m_SelectOnDown_2() { return static_cast<int32_t>(offsetof(Navigation_t1571958496, ___m_SelectOnDown_2)); }
	inline Selectable_t1490392188 * get_m_SelectOnDown_2() const { return ___m_SelectOnDown_2; }
	inline Selectable_t1490392188 ** get_address_of_m_SelectOnDown_2() { return &___m_SelectOnDown_2; }
	inline void set_m_SelectOnDown_2(Selectable_t1490392188 * value)
	{
		___m_SelectOnDown_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnDown_2), value);
	}

	inline static int32_t get_offset_of_m_SelectOnLeft_3() { return static_cast<int32_t>(offsetof(Navigation_t1571958496, ___m_SelectOnLeft_3)); }
	inline Selectable_t1490392188 * get_m_SelectOnLeft_3() const { return ___m_SelectOnLeft_3; }
	inline Selectable_t1490392188 ** get_address_of_m_SelectOnLeft_3() { return &___m_SelectOnLeft_3; }
	inline void set_m_SelectOnLeft_3(Selectable_t1490392188 * value)
	{
		___m_SelectOnLeft_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnLeft_3), value);
	}

	inline static int32_t get_offset_of_m_SelectOnRight_4() { return static_cast<int32_t>(offsetof(Navigation_t1571958496, ___m_SelectOnRight_4)); }
	inline Selectable_t1490392188 * get_m_SelectOnRight_4() const { return ___m_SelectOnRight_4; }
	inline Selectable_t1490392188 ** get_address_of_m_SelectOnRight_4() { return &___m_SelectOnRight_4; }
	inline void set_m_SelectOnRight_4(Selectable_t1490392188 * value)
	{
		___m_SelectOnRight_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnRight_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.Navigation
struct Navigation_t1571958496_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	Selectable_t1490392188 * ___m_SelectOnUp_1;
	Selectable_t1490392188 * ___m_SelectOnDown_2;
	Selectable_t1490392188 * ___m_SelectOnLeft_3;
	Selectable_t1490392188 * ___m_SelectOnRight_4;
};
// Native definition for COM marshalling of UnityEngine.UI.Navigation
struct Navigation_t1571958496_marshaled_com
{
	int32_t ___m_Mode_0;
	Selectable_t1490392188 * ___m_SelectOnUp_1;
	Selectable_t1490392188 * ___m_SelectOnDown_2;
	Selectable_t1490392188 * ___m_SelectOnLeft_3;
	Selectable_t1490392188 * ___m_SelectOnRight_4;
};
#endif // NAVIGATION_T1571958496_H
#ifndef COMPONENT_T3819376471_H
#define COMPONENT_T3819376471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t3819376471  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T3819376471_H
#ifndef PROGRESSEVENT_T4185145044_H
#define PROGRESSEVENT_T4185145044_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Connection.RESTConnector/ProgressEvent
struct  ProgressEvent_t4185145044  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROGRESSEVENT_T4185145044_H
#ifndef U3CPROCESSREQUESTQUEUEU3EC__ITERATOR0_T3628487913_H
#define U3CPROCESSREQUESTQUEUEU3EC__ITERATOR0_T3628487913_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Connection.RESTConnector/<ProcessRequestQueue>c__Iterator0
struct  U3CProcessRequestQueueU3Ec__Iterator0_t3628487913  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request IBM.Watson.DeveloperCloud.Connection.RESTConnector/<ProcessRequestQueue>c__Iterator0::<req>__1
	Request_t466816980 * ___U3CreqU3E__1_0;
	// System.String IBM.Watson.DeveloperCloud.Connection.RESTConnector/<ProcessRequestQueue>c__Iterator0::<url>__1
	String_t* ___U3CurlU3E__1_1;
	// System.Text.StringBuilder IBM.Watson.DeveloperCloud.Connection.RESTConnector/<ProcessRequestQueue>c__Iterator0::<args>__1
	StringBuilder_t1221177846 * ___U3CargsU3E__1_2;
	// System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Object> IBM.Watson.DeveloperCloud.Connection.RESTConnector/<ProcessRequestQueue>c__Iterator0::$locvar0
	Enumerator_t1629285963  ___U24locvar0_3;
	// IBM.Watson.DeveloperCloud.Connection.RESTConnector/Response IBM.Watson.DeveloperCloud.Connection.RESTConnector/<ProcessRequestQueue>c__Iterator0::<resp>__1
	Response_t429319368 * ___U3CrespU3E__1_4;
	// System.DateTime IBM.Watson.DeveloperCloud.Connection.RESTConnector/<ProcessRequestQueue>c__Iterator0::<startTime>__1
	DateTime_t693205669  ___U3CstartTimeU3E__1_5;
	// UnityEngine.WWW IBM.Watson.DeveloperCloud.Connection.RESTConnector/<ProcessRequestQueue>c__Iterator0::<www>__2
	WWW_t2919945039 * ___U3CwwwU3E__2_6;
	// System.Single IBM.Watson.DeveloperCloud.Connection.RESTConnector/<ProcessRequestQueue>c__Iterator0::<timeout>__2
	float ___U3CtimeoutU3E__2_7;
	// System.Boolean IBM.Watson.DeveloperCloud.Connection.RESTConnector/<ProcessRequestQueue>c__Iterator0::<bError>__2
	bool ___U3CbErrorU3E__2_8;
	// IBM.Watson.DeveloperCloud.Connection.RESTConnector IBM.Watson.DeveloperCloud.Connection.RESTConnector/<ProcessRequestQueue>c__Iterator0::$this
	RESTConnector_t3705102247 * ___U24this_9;
	// System.Object IBM.Watson.DeveloperCloud.Connection.RESTConnector/<ProcessRequestQueue>c__Iterator0::$current
	RuntimeObject * ___U24current_10;
	// System.Boolean IBM.Watson.DeveloperCloud.Connection.RESTConnector/<ProcessRequestQueue>c__Iterator0::$disposing
	bool ___U24disposing_11;
	// System.Int32 IBM.Watson.DeveloperCloud.Connection.RESTConnector/<ProcessRequestQueue>c__Iterator0::$PC
	int32_t ___U24PC_12;

public:
	inline static int32_t get_offset_of_U3CreqU3E__1_0() { return static_cast<int32_t>(offsetof(U3CProcessRequestQueueU3Ec__Iterator0_t3628487913, ___U3CreqU3E__1_0)); }
	inline Request_t466816980 * get_U3CreqU3E__1_0() const { return ___U3CreqU3E__1_0; }
	inline Request_t466816980 ** get_address_of_U3CreqU3E__1_0() { return &___U3CreqU3E__1_0; }
	inline void set_U3CreqU3E__1_0(Request_t466816980 * value)
	{
		___U3CreqU3E__1_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CreqU3E__1_0), value);
	}

	inline static int32_t get_offset_of_U3CurlU3E__1_1() { return static_cast<int32_t>(offsetof(U3CProcessRequestQueueU3Ec__Iterator0_t3628487913, ___U3CurlU3E__1_1)); }
	inline String_t* get_U3CurlU3E__1_1() const { return ___U3CurlU3E__1_1; }
	inline String_t** get_address_of_U3CurlU3E__1_1() { return &___U3CurlU3E__1_1; }
	inline void set_U3CurlU3E__1_1(String_t* value)
	{
		___U3CurlU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CurlU3E__1_1), value);
	}

	inline static int32_t get_offset_of_U3CargsU3E__1_2() { return static_cast<int32_t>(offsetof(U3CProcessRequestQueueU3Ec__Iterator0_t3628487913, ___U3CargsU3E__1_2)); }
	inline StringBuilder_t1221177846 * get_U3CargsU3E__1_2() const { return ___U3CargsU3E__1_2; }
	inline StringBuilder_t1221177846 ** get_address_of_U3CargsU3E__1_2() { return &___U3CargsU3E__1_2; }
	inline void set_U3CargsU3E__1_2(StringBuilder_t1221177846 * value)
	{
		___U3CargsU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CargsU3E__1_2), value);
	}

	inline static int32_t get_offset_of_U24locvar0_3() { return static_cast<int32_t>(offsetof(U3CProcessRequestQueueU3Ec__Iterator0_t3628487913, ___U24locvar0_3)); }
	inline Enumerator_t1629285963  get_U24locvar0_3() const { return ___U24locvar0_3; }
	inline Enumerator_t1629285963 * get_address_of_U24locvar0_3() { return &___U24locvar0_3; }
	inline void set_U24locvar0_3(Enumerator_t1629285963  value)
	{
		___U24locvar0_3 = value;
	}

	inline static int32_t get_offset_of_U3CrespU3E__1_4() { return static_cast<int32_t>(offsetof(U3CProcessRequestQueueU3Ec__Iterator0_t3628487913, ___U3CrespU3E__1_4)); }
	inline Response_t429319368 * get_U3CrespU3E__1_4() const { return ___U3CrespU3E__1_4; }
	inline Response_t429319368 ** get_address_of_U3CrespU3E__1_4() { return &___U3CrespU3E__1_4; }
	inline void set_U3CrespU3E__1_4(Response_t429319368 * value)
	{
		___U3CrespU3E__1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrespU3E__1_4), value);
	}

	inline static int32_t get_offset_of_U3CstartTimeU3E__1_5() { return static_cast<int32_t>(offsetof(U3CProcessRequestQueueU3Ec__Iterator0_t3628487913, ___U3CstartTimeU3E__1_5)); }
	inline DateTime_t693205669  get_U3CstartTimeU3E__1_5() const { return ___U3CstartTimeU3E__1_5; }
	inline DateTime_t693205669 * get_address_of_U3CstartTimeU3E__1_5() { return &___U3CstartTimeU3E__1_5; }
	inline void set_U3CstartTimeU3E__1_5(DateTime_t693205669  value)
	{
		___U3CstartTimeU3E__1_5 = value;
	}

	inline static int32_t get_offset_of_U3CwwwU3E__2_6() { return static_cast<int32_t>(offsetof(U3CProcessRequestQueueU3Ec__Iterator0_t3628487913, ___U3CwwwU3E__2_6)); }
	inline WWW_t2919945039 * get_U3CwwwU3E__2_6() const { return ___U3CwwwU3E__2_6; }
	inline WWW_t2919945039 ** get_address_of_U3CwwwU3E__2_6() { return &___U3CwwwU3E__2_6; }
	inline void set_U3CwwwU3E__2_6(WWW_t2919945039 * value)
	{
		___U3CwwwU3E__2_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__2_6), value);
	}

	inline static int32_t get_offset_of_U3CtimeoutU3E__2_7() { return static_cast<int32_t>(offsetof(U3CProcessRequestQueueU3Ec__Iterator0_t3628487913, ___U3CtimeoutU3E__2_7)); }
	inline float get_U3CtimeoutU3E__2_7() const { return ___U3CtimeoutU3E__2_7; }
	inline float* get_address_of_U3CtimeoutU3E__2_7() { return &___U3CtimeoutU3E__2_7; }
	inline void set_U3CtimeoutU3E__2_7(float value)
	{
		___U3CtimeoutU3E__2_7 = value;
	}

	inline static int32_t get_offset_of_U3CbErrorU3E__2_8() { return static_cast<int32_t>(offsetof(U3CProcessRequestQueueU3Ec__Iterator0_t3628487913, ___U3CbErrorU3E__2_8)); }
	inline bool get_U3CbErrorU3E__2_8() const { return ___U3CbErrorU3E__2_8; }
	inline bool* get_address_of_U3CbErrorU3E__2_8() { return &___U3CbErrorU3E__2_8; }
	inline void set_U3CbErrorU3E__2_8(bool value)
	{
		___U3CbErrorU3E__2_8 = value;
	}

	inline static int32_t get_offset_of_U24this_9() { return static_cast<int32_t>(offsetof(U3CProcessRequestQueueU3Ec__Iterator0_t3628487913, ___U24this_9)); }
	inline RESTConnector_t3705102247 * get_U24this_9() const { return ___U24this_9; }
	inline RESTConnector_t3705102247 ** get_address_of_U24this_9() { return &___U24this_9; }
	inline void set_U24this_9(RESTConnector_t3705102247 * value)
	{
		___U24this_9 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_9), value);
	}

	inline static int32_t get_offset_of_U24current_10() { return static_cast<int32_t>(offsetof(U3CProcessRequestQueueU3Ec__Iterator0_t3628487913, ___U24current_10)); }
	inline RuntimeObject * get_U24current_10() const { return ___U24current_10; }
	inline RuntimeObject ** get_address_of_U24current_10() { return &___U24current_10; }
	inline void set_U24current_10(RuntimeObject * value)
	{
		___U24current_10 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_10), value);
	}

	inline static int32_t get_offset_of_U24disposing_11() { return static_cast<int32_t>(offsetof(U3CProcessRequestQueueU3Ec__Iterator0_t3628487913, ___U24disposing_11)); }
	inline bool get_U24disposing_11() const { return ___U24disposing_11; }
	inline bool* get_address_of_U24disposing_11() { return &___U24disposing_11; }
	inline void set_U24disposing_11(bool value)
	{
		___U24disposing_11 = value;
	}

	inline static int32_t get_offset_of_U24PC_12() { return static_cast<int32_t>(offsetof(U3CProcessRequestQueueU3Ec__Iterator0_t3628487913, ___U24PC_12)); }
	inline int32_t get_U24PC_12() const { return ___U24PC_12; }
	inline int32_t* get_address_of_U24PC_12() { return &___U24PC_12; }
	inline void set_U24PC_12(int32_t value)
	{
		___U24PC_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPROCESSREQUESTQUEUEU3EC__ITERATOR0_T3628487913_H
#ifndef ARANCHORREMOVED_T142665927_H
#define ARANCHORREMOVED_T142665927_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorRemoved
struct  ARAnchorRemoved_t142665927  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARANCHORREMOVED_T142665927_H
#ifndef ARANCHORADDED_T2646854145_H
#define ARANCHORADDED_T2646854145_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorAdded
struct  ARAnchorAdded_t2646854145  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARANCHORADDED_T2646854145_H
#ifndef ARANCHORUPDATED_T3886071158_H
#define ARANCHORUPDATED_T3886071158_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorUpdated
struct  ARAnchorUpdated_t3886071158  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARANCHORUPDATED_T3886071158_H
#ifndef RESPONSEEVENT_T1616568356_H
#define RESPONSEEVENT_T1616568356_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Connection.RESTConnector/ResponseEvent
struct  ResponseEvent_t1616568356  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESPONSEEVENT_T1616568356_H
#ifndef INTERNAL_ARANCHORUPDATED_T3705772742_H
#define INTERNAL_ARANCHORUPDATED_T3705772742_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorUpdated
struct  internal_ARAnchorUpdated_t3705772742  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNAL_ARANCHORUPDATED_T3705772742_H
#ifndef INTERNAL_ARANCHORREMOVED_T3189755211_H
#define INTERNAL_ARANCHORREMOVED_T3189755211_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorRemoved
struct  internal_ARAnchorRemoved_t3189755211  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNAL_ARANCHORREMOVED_T3189755211_H
#ifndef BEHAVIOUR_T955675639_H
#define BEHAVIOUR_T955675639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t955675639  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T955675639_H
#ifndef INTERNAL_ARANCHORADDED_T1622117597_H
#define INTERNAL_ARANCHORADDED_T1622117597_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorAdded
struct  internal_ARAnchorAdded_t1622117597  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNAL_ARANCHORADDED_T1622117597_H
#ifndef ARSESSIONFAILED_T872580813_H
#define ARSESSIONFAILED_T872580813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARSessionFailed
struct  ARSessionFailed_t872580813  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARSESSIONFAILED_T872580813_H
#ifndef ARFRAMEUPDATE_T496507918_H
#define ARFRAMEUPDATE_T496507918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARFrameUpdate
struct  ARFrameUpdate_t496507918  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARFRAMEUPDATE_T496507918_H
#ifndef INTERNAL_ARFRAMEUPDATE_T3296518558_H
#define INTERNAL_ARFRAMEUPDATE_T3296518558_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFrameUpdate
struct  internal_ARFrameUpdate_t3296518558  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNAL_ARFRAMEUPDATE_T3296518558_H
#ifndef MONOBEHAVIOUR_T1158329972_H
#define MONOBEHAVIOUR_T1158329972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t1158329972  : public Behaviour_t955675639
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T1158329972_H
#ifndef UNITYARVIDEO_T2351297253_H
#define UNITYARVIDEO_T2351297253_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARVideo
struct  UnityARVideo_t2351297253  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Material UnityEngine.XR.iOS.UnityARVideo::m_ClearMaterial
	Material_t193706927 * ___m_ClearMaterial_2;
	// UnityEngine.Rendering.CommandBuffer UnityEngine.XR.iOS.UnityARVideo::m_VideoCommandBuffer
	CommandBuffer_t1204166949 * ___m_VideoCommandBuffer_3;
	// UnityEngine.Texture2D UnityEngine.XR.iOS.UnityARVideo::_videoTextureY
	Texture2D_t3542995729 * ____videoTextureY_4;
	// UnityEngine.Texture2D UnityEngine.XR.iOS.UnityARVideo::_videoTextureCbCr
	Texture2D_t3542995729 * ____videoTextureCbCr_5;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface UnityEngine.XR.iOS.UnityARVideo::m_Session
	UnityARSessionNativeInterface_t1130867170 * ___m_Session_6;
	// System.Boolean UnityEngine.XR.iOS.UnityARVideo::bCommandBufferInitialized
	bool ___bCommandBufferInitialized_7;

public:
	inline static int32_t get_offset_of_m_ClearMaterial_2() { return static_cast<int32_t>(offsetof(UnityARVideo_t2351297253, ___m_ClearMaterial_2)); }
	inline Material_t193706927 * get_m_ClearMaterial_2() const { return ___m_ClearMaterial_2; }
	inline Material_t193706927 ** get_address_of_m_ClearMaterial_2() { return &___m_ClearMaterial_2; }
	inline void set_m_ClearMaterial_2(Material_t193706927 * value)
	{
		___m_ClearMaterial_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ClearMaterial_2), value);
	}

	inline static int32_t get_offset_of_m_VideoCommandBuffer_3() { return static_cast<int32_t>(offsetof(UnityARVideo_t2351297253, ___m_VideoCommandBuffer_3)); }
	inline CommandBuffer_t1204166949 * get_m_VideoCommandBuffer_3() const { return ___m_VideoCommandBuffer_3; }
	inline CommandBuffer_t1204166949 ** get_address_of_m_VideoCommandBuffer_3() { return &___m_VideoCommandBuffer_3; }
	inline void set_m_VideoCommandBuffer_3(CommandBuffer_t1204166949 * value)
	{
		___m_VideoCommandBuffer_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_VideoCommandBuffer_3), value);
	}

	inline static int32_t get_offset_of__videoTextureY_4() { return static_cast<int32_t>(offsetof(UnityARVideo_t2351297253, ____videoTextureY_4)); }
	inline Texture2D_t3542995729 * get__videoTextureY_4() const { return ____videoTextureY_4; }
	inline Texture2D_t3542995729 ** get_address_of__videoTextureY_4() { return &____videoTextureY_4; }
	inline void set__videoTextureY_4(Texture2D_t3542995729 * value)
	{
		____videoTextureY_4 = value;
		Il2CppCodeGenWriteBarrier((&____videoTextureY_4), value);
	}

	inline static int32_t get_offset_of__videoTextureCbCr_5() { return static_cast<int32_t>(offsetof(UnityARVideo_t2351297253, ____videoTextureCbCr_5)); }
	inline Texture2D_t3542995729 * get__videoTextureCbCr_5() const { return ____videoTextureCbCr_5; }
	inline Texture2D_t3542995729 ** get_address_of__videoTextureCbCr_5() { return &____videoTextureCbCr_5; }
	inline void set__videoTextureCbCr_5(Texture2D_t3542995729 * value)
	{
		____videoTextureCbCr_5 = value;
		Il2CppCodeGenWriteBarrier((&____videoTextureCbCr_5), value);
	}

	inline static int32_t get_offset_of_m_Session_6() { return static_cast<int32_t>(offsetof(UnityARVideo_t2351297253, ___m_Session_6)); }
	inline UnityARSessionNativeInterface_t1130867170 * get_m_Session_6() const { return ___m_Session_6; }
	inline UnityARSessionNativeInterface_t1130867170 ** get_address_of_m_Session_6() { return &___m_Session_6; }
	inline void set_m_Session_6(UnityARSessionNativeInterface_t1130867170 * value)
	{
		___m_Session_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Session_6), value);
	}

	inline static int32_t get_offset_of_bCommandBufferInitialized_7() { return static_cast<int32_t>(offsetof(UnityARVideo_t2351297253, ___bCommandBufferInitialized_7)); }
	inline bool get_bCommandBufferInitialized_7() const { return ___bCommandBufferInitialized_7; }
	inline bool* get_address_of_bCommandBufferInitialized_7() { return &___bCommandBufferInitialized_7; }
	inline void set_bCommandBufferInitialized_7(bool value)
	{
		___bCommandBufferInitialized_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARVIDEO_T2351297253_H
#ifndef UNITYPOINTCLOUDEXAMPLE_T3196264220_H
#define UNITYPOINTCLOUDEXAMPLE_T3196264220_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityPointCloudExample
struct  UnityPointCloudExample_t3196264220  : public MonoBehaviour_t1158329972
{
public:
	// System.UInt32 UnityPointCloudExample::numPointsToShow
	uint32_t ___numPointsToShow_2;
	// UnityEngine.GameObject UnityPointCloudExample::PointCloudPrefab
	GameObject_t1756533147 * ___PointCloudPrefab_3;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> UnityPointCloudExample::pointCloudObjects
	List_1_t1125654279 * ___pointCloudObjects_4;
	// UnityEngine.Vector3[] UnityPointCloudExample::m_PointCloudData
	Vector3U5BU5D_t1172311765* ___m_PointCloudData_5;

public:
	inline static int32_t get_offset_of_numPointsToShow_2() { return static_cast<int32_t>(offsetof(UnityPointCloudExample_t3196264220, ___numPointsToShow_2)); }
	inline uint32_t get_numPointsToShow_2() const { return ___numPointsToShow_2; }
	inline uint32_t* get_address_of_numPointsToShow_2() { return &___numPointsToShow_2; }
	inline void set_numPointsToShow_2(uint32_t value)
	{
		___numPointsToShow_2 = value;
	}

	inline static int32_t get_offset_of_PointCloudPrefab_3() { return static_cast<int32_t>(offsetof(UnityPointCloudExample_t3196264220, ___PointCloudPrefab_3)); }
	inline GameObject_t1756533147 * get_PointCloudPrefab_3() const { return ___PointCloudPrefab_3; }
	inline GameObject_t1756533147 ** get_address_of_PointCloudPrefab_3() { return &___PointCloudPrefab_3; }
	inline void set_PointCloudPrefab_3(GameObject_t1756533147 * value)
	{
		___PointCloudPrefab_3 = value;
		Il2CppCodeGenWriteBarrier((&___PointCloudPrefab_3), value);
	}

	inline static int32_t get_offset_of_pointCloudObjects_4() { return static_cast<int32_t>(offsetof(UnityPointCloudExample_t3196264220, ___pointCloudObjects_4)); }
	inline List_1_t1125654279 * get_pointCloudObjects_4() const { return ___pointCloudObjects_4; }
	inline List_1_t1125654279 ** get_address_of_pointCloudObjects_4() { return &___pointCloudObjects_4; }
	inline void set_pointCloudObjects_4(List_1_t1125654279 * value)
	{
		___pointCloudObjects_4 = value;
		Il2CppCodeGenWriteBarrier((&___pointCloudObjects_4), value);
	}

	inline static int32_t get_offset_of_m_PointCloudData_5() { return static_cast<int32_t>(offsetof(UnityPointCloudExample_t3196264220, ___m_PointCloudData_5)); }
	inline Vector3U5BU5D_t1172311765* get_m_PointCloudData_5() const { return ___m_PointCloudData_5; }
	inline Vector3U5BU5D_t1172311765** get_address_of_m_PointCloudData_5() { return &___m_PointCloudData_5; }
	inline void set_m_PointCloudData_5(Vector3U5BU5D_t1172311765* value)
	{
		___m_PointCloudData_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_PointCloudData_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYPOINTCLOUDEXAMPLE_T3196264220_H
#ifndef EXAMPLEDISCOVERYV1_T2040126595_H
#define EXAMPLEDISCOVERYV1_T2040126595_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExampleDiscoveryV1
struct  ExampleDiscoveryV1_t2040126595  : public MonoBehaviour_t1158329972
{
public:
	// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery ExampleDiscoveryV1::m_Discovery
	Discovery_t1478121420 * ___m_Discovery_2;
	// System.String ExampleDiscoveryV1::m_CreatedEnvironmentID
	String_t* ___m_CreatedEnvironmentID_3;
	// System.String ExampleDiscoveryV1::m_DefaultEnvironmentID
	String_t* ___m_DefaultEnvironmentID_4;
	// System.String ExampleDiscoveryV1::m_DefaultConfigurationID
	String_t* ___m_DefaultConfigurationID_5;
	// System.String ExampleDiscoveryV1::m_DefaultCollectionID
	String_t* ___m_DefaultCollectionID_6;
	// System.String ExampleDiscoveryV1::m_ConfigurationJsonPath
	String_t* ___m_ConfigurationJsonPath_7;
	// System.String ExampleDiscoveryV1::m_CreatedConfigurationID
	String_t* ___m_CreatedConfigurationID_8;
	// System.String ExampleDiscoveryV1::m_FilePathToIngest
	String_t* ___m_FilePathToIngest_9;
	// System.String ExampleDiscoveryV1::m_Metadata
	String_t* ___m_Metadata_10;
	// System.String ExampleDiscoveryV1::m_CreatedCollectionID
	String_t* ___m_CreatedCollectionID_11;
	// System.String ExampleDiscoveryV1::m_CreatedCollectionName
	String_t* ___m_CreatedCollectionName_12;
	// System.String ExampleDiscoveryV1::m_CreatedCollectionDescription
	String_t* ___m_CreatedCollectionDescription_13;
	// System.String ExampleDiscoveryV1::m_CreatedDocumentID
	String_t* ___m_CreatedDocumentID_14;
	// System.String ExampleDiscoveryV1::m_DocumentFilePath
	String_t* ___m_DocumentFilePath_15;
	// System.String ExampleDiscoveryV1::m_Query
	String_t* ___m_Query_16;
	// System.Boolean ExampleDiscoveryV1::m_IsConfigDeleted
	bool ___m_IsConfigDeleted_17;
	// System.Boolean ExampleDiscoveryV1::m_IsCollectionDeleted
	bool ___m_IsCollectionDeleted_18;

public:
	inline static int32_t get_offset_of_m_Discovery_2() { return static_cast<int32_t>(offsetof(ExampleDiscoveryV1_t2040126595, ___m_Discovery_2)); }
	inline Discovery_t1478121420 * get_m_Discovery_2() const { return ___m_Discovery_2; }
	inline Discovery_t1478121420 ** get_address_of_m_Discovery_2() { return &___m_Discovery_2; }
	inline void set_m_Discovery_2(Discovery_t1478121420 * value)
	{
		___m_Discovery_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Discovery_2), value);
	}

	inline static int32_t get_offset_of_m_CreatedEnvironmentID_3() { return static_cast<int32_t>(offsetof(ExampleDiscoveryV1_t2040126595, ___m_CreatedEnvironmentID_3)); }
	inline String_t* get_m_CreatedEnvironmentID_3() const { return ___m_CreatedEnvironmentID_3; }
	inline String_t** get_address_of_m_CreatedEnvironmentID_3() { return &___m_CreatedEnvironmentID_3; }
	inline void set_m_CreatedEnvironmentID_3(String_t* value)
	{
		___m_CreatedEnvironmentID_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_CreatedEnvironmentID_3), value);
	}

	inline static int32_t get_offset_of_m_DefaultEnvironmentID_4() { return static_cast<int32_t>(offsetof(ExampleDiscoveryV1_t2040126595, ___m_DefaultEnvironmentID_4)); }
	inline String_t* get_m_DefaultEnvironmentID_4() const { return ___m_DefaultEnvironmentID_4; }
	inline String_t** get_address_of_m_DefaultEnvironmentID_4() { return &___m_DefaultEnvironmentID_4; }
	inline void set_m_DefaultEnvironmentID_4(String_t* value)
	{
		___m_DefaultEnvironmentID_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_DefaultEnvironmentID_4), value);
	}

	inline static int32_t get_offset_of_m_DefaultConfigurationID_5() { return static_cast<int32_t>(offsetof(ExampleDiscoveryV1_t2040126595, ___m_DefaultConfigurationID_5)); }
	inline String_t* get_m_DefaultConfigurationID_5() const { return ___m_DefaultConfigurationID_5; }
	inline String_t** get_address_of_m_DefaultConfigurationID_5() { return &___m_DefaultConfigurationID_5; }
	inline void set_m_DefaultConfigurationID_5(String_t* value)
	{
		___m_DefaultConfigurationID_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_DefaultConfigurationID_5), value);
	}

	inline static int32_t get_offset_of_m_DefaultCollectionID_6() { return static_cast<int32_t>(offsetof(ExampleDiscoveryV1_t2040126595, ___m_DefaultCollectionID_6)); }
	inline String_t* get_m_DefaultCollectionID_6() const { return ___m_DefaultCollectionID_6; }
	inline String_t** get_address_of_m_DefaultCollectionID_6() { return &___m_DefaultCollectionID_6; }
	inline void set_m_DefaultCollectionID_6(String_t* value)
	{
		___m_DefaultCollectionID_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_DefaultCollectionID_6), value);
	}

	inline static int32_t get_offset_of_m_ConfigurationJsonPath_7() { return static_cast<int32_t>(offsetof(ExampleDiscoveryV1_t2040126595, ___m_ConfigurationJsonPath_7)); }
	inline String_t* get_m_ConfigurationJsonPath_7() const { return ___m_ConfigurationJsonPath_7; }
	inline String_t** get_address_of_m_ConfigurationJsonPath_7() { return &___m_ConfigurationJsonPath_7; }
	inline void set_m_ConfigurationJsonPath_7(String_t* value)
	{
		___m_ConfigurationJsonPath_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_ConfigurationJsonPath_7), value);
	}

	inline static int32_t get_offset_of_m_CreatedConfigurationID_8() { return static_cast<int32_t>(offsetof(ExampleDiscoveryV1_t2040126595, ___m_CreatedConfigurationID_8)); }
	inline String_t* get_m_CreatedConfigurationID_8() const { return ___m_CreatedConfigurationID_8; }
	inline String_t** get_address_of_m_CreatedConfigurationID_8() { return &___m_CreatedConfigurationID_8; }
	inline void set_m_CreatedConfigurationID_8(String_t* value)
	{
		___m_CreatedConfigurationID_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_CreatedConfigurationID_8), value);
	}

	inline static int32_t get_offset_of_m_FilePathToIngest_9() { return static_cast<int32_t>(offsetof(ExampleDiscoveryV1_t2040126595, ___m_FilePathToIngest_9)); }
	inline String_t* get_m_FilePathToIngest_9() const { return ___m_FilePathToIngest_9; }
	inline String_t** get_address_of_m_FilePathToIngest_9() { return &___m_FilePathToIngest_9; }
	inline void set_m_FilePathToIngest_9(String_t* value)
	{
		___m_FilePathToIngest_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_FilePathToIngest_9), value);
	}

	inline static int32_t get_offset_of_m_Metadata_10() { return static_cast<int32_t>(offsetof(ExampleDiscoveryV1_t2040126595, ___m_Metadata_10)); }
	inline String_t* get_m_Metadata_10() const { return ___m_Metadata_10; }
	inline String_t** get_address_of_m_Metadata_10() { return &___m_Metadata_10; }
	inline void set_m_Metadata_10(String_t* value)
	{
		___m_Metadata_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_Metadata_10), value);
	}

	inline static int32_t get_offset_of_m_CreatedCollectionID_11() { return static_cast<int32_t>(offsetof(ExampleDiscoveryV1_t2040126595, ___m_CreatedCollectionID_11)); }
	inline String_t* get_m_CreatedCollectionID_11() const { return ___m_CreatedCollectionID_11; }
	inline String_t** get_address_of_m_CreatedCollectionID_11() { return &___m_CreatedCollectionID_11; }
	inline void set_m_CreatedCollectionID_11(String_t* value)
	{
		___m_CreatedCollectionID_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_CreatedCollectionID_11), value);
	}

	inline static int32_t get_offset_of_m_CreatedCollectionName_12() { return static_cast<int32_t>(offsetof(ExampleDiscoveryV1_t2040126595, ___m_CreatedCollectionName_12)); }
	inline String_t* get_m_CreatedCollectionName_12() const { return ___m_CreatedCollectionName_12; }
	inline String_t** get_address_of_m_CreatedCollectionName_12() { return &___m_CreatedCollectionName_12; }
	inline void set_m_CreatedCollectionName_12(String_t* value)
	{
		___m_CreatedCollectionName_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_CreatedCollectionName_12), value);
	}

	inline static int32_t get_offset_of_m_CreatedCollectionDescription_13() { return static_cast<int32_t>(offsetof(ExampleDiscoveryV1_t2040126595, ___m_CreatedCollectionDescription_13)); }
	inline String_t* get_m_CreatedCollectionDescription_13() const { return ___m_CreatedCollectionDescription_13; }
	inline String_t** get_address_of_m_CreatedCollectionDescription_13() { return &___m_CreatedCollectionDescription_13; }
	inline void set_m_CreatedCollectionDescription_13(String_t* value)
	{
		___m_CreatedCollectionDescription_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_CreatedCollectionDescription_13), value);
	}

	inline static int32_t get_offset_of_m_CreatedDocumentID_14() { return static_cast<int32_t>(offsetof(ExampleDiscoveryV1_t2040126595, ___m_CreatedDocumentID_14)); }
	inline String_t* get_m_CreatedDocumentID_14() const { return ___m_CreatedDocumentID_14; }
	inline String_t** get_address_of_m_CreatedDocumentID_14() { return &___m_CreatedDocumentID_14; }
	inline void set_m_CreatedDocumentID_14(String_t* value)
	{
		___m_CreatedDocumentID_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_CreatedDocumentID_14), value);
	}

	inline static int32_t get_offset_of_m_DocumentFilePath_15() { return static_cast<int32_t>(offsetof(ExampleDiscoveryV1_t2040126595, ___m_DocumentFilePath_15)); }
	inline String_t* get_m_DocumentFilePath_15() const { return ___m_DocumentFilePath_15; }
	inline String_t** get_address_of_m_DocumentFilePath_15() { return &___m_DocumentFilePath_15; }
	inline void set_m_DocumentFilePath_15(String_t* value)
	{
		___m_DocumentFilePath_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_DocumentFilePath_15), value);
	}

	inline static int32_t get_offset_of_m_Query_16() { return static_cast<int32_t>(offsetof(ExampleDiscoveryV1_t2040126595, ___m_Query_16)); }
	inline String_t* get_m_Query_16() const { return ___m_Query_16; }
	inline String_t** get_address_of_m_Query_16() { return &___m_Query_16; }
	inline void set_m_Query_16(String_t* value)
	{
		___m_Query_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_Query_16), value);
	}

	inline static int32_t get_offset_of_m_IsConfigDeleted_17() { return static_cast<int32_t>(offsetof(ExampleDiscoveryV1_t2040126595, ___m_IsConfigDeleted_17)); }
	inline bool get_m_IsConfigDeleted_17() const { return ___m_IsConfigDeleted_17; }
	inline bool* get_address_of_m_IsConfigDeleted_17() { return &___m_IsConfigDeleted_17; }
	inline void set_m_IsConfigDeleted_17(bool value)
	{
		___m_IsConfigDeleted_17 = value;
	}

	inline static int32_t get_offset_of_m_IsCollectionDeleted_18() { return static_cast<int32_t>(offsetof(ExampleDiscoveryV1_t2040126595, ___m_IsCollectionDeleted_18)); }
	inline bool get_m_IsCollectionDeleted_18() const { return ___m_IsCollectionDeleted_18; }
	inline bool* get_address_of_m_IsCollectionDeleted_18() { return &___m_IsCollectionDeleted_18; }
	inline void set_m_IsCollectionDeleted_18(bool value)
	{
		___m_IsCollectionDeleted_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXAMPLEDISCOVERYV1_T2040126595_H
#ifndef UNITYARHITTESTEXAMPLE_T146867607_H
#define UNITYARHITTESTEXAMPLE_T146867607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARHitTestExample
struct  UnityARHitTestExample_t146867607  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform UnityEngine.XR.iOS.UnityARHitTestExample::m_HitTransform
	Transform_t3275118058 * ___m_HitTransform_2;

public:
	inline static int32_t get_offset_of_m_HitTransform_2() { return static_cast<int32_t>(offsetof(UnityARHitTestExample_t146867607, ___m_HitTransform_2)); }
	inline Transform_t3275118058 * get_m_HitTransform_2() const { return ___m_HitTransform_2; }
	inline Transform_t3275118058 ** get_address_of_m_HitTransform_2() { return &___m_HitTransform_2; }
	inline void set_m_HitTransform_2(Transform_t3275118058 * value)
	{
		___m_HitTransform_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_HitTransform_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARHITTESTEXAMPLE_T146867607_H
#ifndef UNITYARKITCONTROL_T1698990409_H
#define UNITYARKITCONTROL_T1698990409_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARKitControl
struct  UnityARKitControl_t1698990409  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.XR.iOS.UnityARSessionRunOption[] UnityEngine.XR.iOS.UnityARKitControl::runOptions
	UnityARSessionRunOptionU5BU5D_t3114965901* ___runOptions_2;
	// UnityEngine.XR.iOS.UnityARAlignment[] UnityEngine.XR.iOS.UnityARKitControl::alignmentOptions
	UnityARAlignmentU5BU5D_t218994990* ___alignmentOptions_3;
	// UnityEngine.XR.iOS.UnityARPlaneDetection[] UnityEngine.XR.iOS.UnityARKitControl::planeOptions
	UnityARPlaneDetectionU5BU5D_t191549612* ___planeOptions_4;
	// System.Int32 UnityEngine.XR.iOS.UnityARKitControl::currentOptionIndex
	int32_t ___currentOptionIndex_5;
	// System.Int32 UnityEngine.XR.iOS.UnityARKitControl::currentAlignmentIndex
	int32_t ___currentAlignmentIndex_6;
	// System.Int32 UnityEngine.XR.iOS.UnityARKitControl::currentPlaneIndex
	int32_t ___currentPlaneIndex_7;

public:
	inline static int32_t get_offset_of_runOptions_2() { return static_cast<int32_t>(offsetof(UnityARKitControl_t1698990409, ___runOptions_2)); }
	inline UnityARSessionRunOptionU5BU5D_t3114965901* get_runOptions_2() const { return ___runOptions_2; }
	inline UnityARSessionRunOptionU5BU5D_t3114965901** get_address_of_runOptions_2() { return &___runOptions_2; }
	inline void set_runOptions_2(UnityARSessionRunOptionU5BU5D_t3114965901* value)
	{
		___runOptions_2 = value;
		Il2CppCodeGenWriteBarrier((&___runOptions_2), value);
	}

	inline static int32_t get_offset_of_alignmentOptions_3() { return static_cast<int32_t>(offsetof(UnityARKitControl_t1698990409, ___alignmentOptions_3)); }
	inline UnityARAlignmentU5BU5D_t218994990* get_alignmentOptions_3() const { return ___alignmentOptions_3; }
	inline UnityARAlignmentU5BU5D_t218994990** get_address_of_alignmentOptions_3() { return &___alignmentOptions_3; }
	inline void set_alignmentOptions_3(UnityARAlignmentU5BU5D_t218994990* value)
	{
		___alignmentOptions_3 = value;
		Il2CppCodeGenWriteBarrier((&___alignmentOptions_3), value);
	}

	inline static int32_t get_offset_of_planeOptions_4() { return static_cast<int32_t>(offsetof(UnityARKitControl_t1698990409, ___planeOptions_4)); }
	inline UnityARPlaneDetectionU5BU5D_t191549612* get_planeOptions_4() const { return ___planeOptions_4; }
	inline UnityARPlaneDetectionU5BU5D_t191549612** get_address_of_planeOptions_4() { return &___planeOptions_4; }
	inline void set_planeOptions_4(UnityARPlaneDetectionU5BU5D_t191549612* value)
	{
		___planeOptions_4 = value;
		Il2CppCodeGenWriteBarrier((&___planeOptions_4), value);
	}

	inline static int32_t get_offset_of_currentOptionIndex_5() { return static_cast<int32_t>(offsetof(UnityARKitControl_t1698990409, ___currentOptionIndex_5)); }
	inline int32_t get_currentOptionIndex_5() const { return ___currentOptionIndex_5; }
	inline int32_t* get_address_of_currentOptionIndex_5() { return &___currentOptionIndex_5; }
	inline void set_currentOptionIndex_5(int32_t value)
	{
		___currentOptionIndex_5 = value;
	}

	inline static int32_t get_offset_of_currentAlignmentIndex_6() { return static_cast<int32_t>(offsetof(UnityARKitControl_t1698990409, ___currentAlignmentIndex_6)); }
	inline int32_t get_currentAlignmentIndex_6() const { return ___currentAlignmentIndex_6; }
	inline int32_t* get_address_of_currentAlignmentIndex_6() { return &___currentAlignmentIndex_6; }
	inline void set_currentAlignmentIndex_6(int32_t value)
	{
		___currentAlignmentIndex_6 = value;
	}

	inline static int32_t get_offset_of_currentPlaneIndex_7() { return static_cast<int32_t>(offsetof(UnityARKitControl_t1698990409, ___currentPlaneIndex_7)); }
	inline int32_t get_currentPlaneIndex_7() const { return ___currentPlaneIndex_7; }
	inline int32_t* get_address_of_currentPlaneIndex_7() { return &___currentPlaneIndex_7; }
	inline void set_currentPlaneIndex_7(int32_t value)
	{
		___currentPlaneIndex_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARKITCONTROL_T1698990409_H
#ifndef UNITYARGENERATEPLANE_T3368998101_H
#define UNITYARGENERATEPLANE_T3368998101_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARGeneratePlane
struct  UnityARGeneratePlane_t3368998101  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject UnityEngine.XR.iOS.UnityARGeneratePlane::planePrefab
	GameObject_t1756533147 * ___planePrefab_2;
	// UnityEngine.XR.iOS.UnityARAnchorManager UnityEngine.XR.iOS.UnityARGeneratePlane::unityARAnchorManager
	UnityARAnchorManager_t1086564192 * ___unityARAnchorManager_3;

public:
	inline static int32_t get_offset_of_planePrefab_2() { return static_cast<int32_t>(offsetof(UnityARGeneratePlane_t3368998101, ___planePrefab_2)); }
	inline GameObject_t1756533147 * get_planePrefab_2() const { return ___planePrefab_2; }
	inline GameObject_t1756533147 ** get_address_of_planePrefab_2() { return &___planePrefab_2; }
	inline void set_planePrefab_2(GameObject_t1756533147 * value)
	{
		___planePrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___planePrefab_2), value);
	}

	inline static int32_t get_offset_of_unityARAnchorManager_3() { return static_cast<int32_t>(offsetof(UnityARGeneratePlane_t3368998101, ___unityARAnchorManager_3)); }
	inline UnityARAnchorManager_t1086564192 * get_unityARAnchorManager_3() const { return ___unityARAnchorManager_3; }
	inline UnityARAnchorManager_t1086564192 ** get_address_of_unityARAnchorManager_3() { return &___unityARAnchorManager_3; }
	inline void set_unityARAnchorManager_3(UnityARAnchorManager_t1086564192 * value)
	{
		___unityARAnchorManager_3 = value;
		Il2CppCodeGenWriteBarrier((&___unityARAnchorManager_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARGENERATEPLANE_T3368998101_H
#ifndef EXAMPLECONVERSATIONEXPERIMENTAL_T3981692631_H
#define EXAMPLECONVERSATIONEXPERIMENTAL_T3981692631_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExampleConversationExperimental
struct  ExampleConversationExperimental_t3981692631  : public MonoBehaviour_t1158329972
{
public:
	// IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.ConversationExperimental ExampleConversationExperimental::m_Conversation
	ConversationExperimental_t215658501 * ___m_Conversation_2;
	// System.String ExampleConversationExperimental::m_WorkspaceID
	String_t* ___m_WorkspaceID_3;
	// System.String ExampleConversationExperimental::m_Input
	String_t* ___m_Input_4;

public:
	inline static int32_t get_offset_of_m_Conversation_2() { return static_cast<int32_t>(offsetof(ExampleConversationExperimental_t3981692631, ___m_Conversation_2)); }
	inline ConversationExperimental_t215658501 * get_m_Conversation_2() const { return ___m_Conversation_2; }
	inline ConversationExperimental_t215658501 ** get_address_of_m_Conversation_2() { return &___m_Conversation_2; }
	inline void set_m_Conversation_2(ConversationExperimental_t215658501 * value)
	{
		___m_Conversation_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Conversation_2), value);
	}

	inline static int32_t get_offset_of_m_WorkspaceID_3() { return static_cast<int32_t>(offsetof(ExampleConversationExperimental_t3981692631, ___m_WorkspaceID_3)); }
	inline String_t* get_m_WorkspaceID_3() const { return ___m_WorkspaceID_3; }
	inline String_t** get_address_of_m_WorkspaceID_3() { return &___m_WorkspaceID_3; }
	inline void set_m_WorkspaceID_3(String_t* value)
	{
		___m_WorkspaceID_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_WorkspaceID_3), value);
	}

	inline static int32_t get_offset_of_m_Input_4() { return static_cast<int32_t>(offsetof(ExampleConversationExperimental_t3981692631, ___m_Input_4)); }
	inline String_t* get_m_Input_4() const { return ___m_Input_4; }
	inline String_t** get_address_of_m_Input_4() { return &___m_Input_4; }
	inline void set_m_Input_4(String_t* value)
	{
		___m_Input_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Input_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXAMPLECONVERSATIONEXPERIMENTAL_T3981692631_H
#ifndef MODESWITCHER_T223874984_H
#define MODESWITCHER_T223874984_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModeSwitcher
struct  ModeSwitcher_t223874984  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject ModeSwitcher::ballMake
	GameObject_t1756533147 * ___ballMake_2;
	// UnityEngine.GameObject ModeSwitcher::ballMove
	GameObject_t1756533147 * ___ballMove_3;
	// System.Int32 ModeSwitcher::appMode
	int32_t ___appMode_4;

public:
	inline static int32_t get_offset_of_ballMake_2() { return static_cast<int32_t>(offsetof(ModeSwitcher_t223874984, ___ballMake_2)); }
	inline GameObject_t1756533147 * get_ballMake_2() const { return ___ballMake_2; }
	inline GameObject_t1756533147 ** get_address_of_ballMake_2() { return &___ballMake_2; }
	inline void set_ballMake_2(GameObject_t1756533147 * value)
	{
		___ballMake_2 = value;
		Il2CppCodeGenWriteBarrier((&___ballMake_2), value);
	}

	inline static int32_t get_offset_of_ballMove_3() { return static_cast<int32_t>(offsetof(ModeSwitcher_t223874984, ___ballMove_3)); }
	inline GameObject_t1756533147 * get_ballMove_3() const { return ___ballMove_3; }
	inline GameObject_t1756533147 ** get_address_of_ballMove_3() { return &___ballMove_3; }
	inline void set_ballMove_3(GameObject_t1756533147 * value)
	{
		___ballMove_3 = value;
		Il2CppCodeGenWriteBarrier((&___ballMove_3), value);
	}

	inline static int32_t get_offset_of_appMode_4() { return static_cast<int32_t>(offsetof(ModeSwitcher_t223874984, ___appMode_4)); }
	inline int32_t get_appMode_4() const { return ___appMode_4; }
	inline int32_t* get_address_of_appMode_4() { return &___appMode_4; }
	inline void set_appMode_4(int32_t value)
	{
		___appMode_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODESWITCHER_T223874984_H
#ifndef FACECAMERA_T2774504826_H
#define FACECAMERA_T2774504826_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FaceCamera
struct  FaceCamera_t2774504826  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject FaceCamera::MainCam
	GameObject_t1756533147 * ___MainCam_2;

public:
	inline static int32_t get_offset_of_MainCam_2() { return static_cast<int32_t>(offsetof(FaceCamera_t2774504826, ___MainCam_2)); }
	inline GameObject_t1756533147 * get_MainCam_2() const { return ___MainCam_2; }
	inline GameObject_t1756533147 ** get_address_of_MainCam_2() { return &___MainCam_2; }
	inline void set_MainCam_2(GameObject_t1756533147 * value)
	{
		___MainCam_2 = value;
		Il2CppCodeGenWriteBarrier((&___MainCam_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACECAMERA_T2774504826_H
#ifndef FLIPANDALIGNTOCAMERA_T3743279677_H
#define FLIPANDALIGNTOCAMERA_T3743279677_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FlipAndAlignToCamera
struct  FlipAndAlignToCamera_t3743279677  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject FlipAndAlignToCamera::MainCam
	GameObject_t1756533147 * ___MainCam_2;

public:
	inline static int32_t get_offset_of_MainCam_2() { return static_cast<int32_t>(offsetof(FlipAndAlignToCamera_t3743279677, ___MainCam_2)); }
	inline GameObject_t1756533147 * get_MainCam_2() const { return ___MainCam_2; }
	inline GameObject_t1756533147 ** get_address_of_MainCam_2() { return &___MainCam_2; }
	inline void set_MainCam_2(GameObject_t1756533147 * value)
	{
		___MainCam_2 = value;
		Il2CppCodeGenWriteBarrier((&___MainCam_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLIPANDALIGNTOCAMERA_T3743279677_H
#ifndef BALLZ_T4160380291_H
#define BALLZ_T4160380291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ballz
struct  Ballz_t4160380291  : public MonoBehaviour_t1158329972
{
public:
	// System.Single Ballz::yDistanceThreshold
	float ___yDistanceThreshold_2;
	// System.Single Ballz::startingY
	float ___startingY_3;

public:
	inline static int32_t get_offset_of_yDistanceThreshold_2() { return static_cast<int32_t>(offsetof(Ballz_t4160380291, ___yDistanceThreshold_2)); }
	inline float get_yDistanceThreshold_2() const { return ___yDistanceThreshold_2; }
	inline float* get_address_of_yDistanceThreshold_2() { return &___yDistanceThreshold_2; }
	inline void set_yDistanceThreshold_2(float value)
	{
		___yDistanceThreshold_2 = value;
	}

	inline static int32_t get_offset_of_startingY_3() { return static_cast<int32_t>(offsetof(Ballz_t4160380291, ___startingY_3)); }
	inline float get_startingY_3() const { return ___startingY_3; }
	inline float* get_address_of_startingY_3() { return &___startingY_3; }
	inline void set_startingY_3(float value)
	{
		___startingY_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BALLZ_T4160380291_H
#ifndef PARTICLEPAINTER_T1073897267_H
#define PARTICLEPAINTER_T1073897267_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ParticlePainter
struct  ParticlePainter_t1073897267  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.ParticleSystem ParticlePainter::painterParticlePrefab
	ParticleSystem_t3394631041 * ___painterParticlePrefab_2;
	// System.Single ParticlePainter::minDistanceThreshold
	float ___minDistanceThreshold_3;
	// System.Single ParticlePainter::maxDistanceThreshold
	float ___maxDistanceThreshold_4;
	// System.Boolean ParticlePainter::frameUpdated
	bool ___frameUpdated_5;
	// System.Single ParticlePainter::particleSize
	float ___particleSize_6;
	// System.Single ParticlePainter::penDistance
	float ___penDistance_7;
	// ColorPicker ParticlePainter::colorPicker
	ColorPicker_t3035206225 * ___colorPicker_8;
	// UnityEngine.ParticleSystem ParticlePainter::currentPS
	ParticleSystem_t3394631041 * ___currentPS_9;
	// UnityEngine.ParticleSystem/Particle[] ParticlePainter::particles
	ParticleU5BU5D_t574222242* ___particles_10;
	// UnityEngine.Vector3 ParticlePainter::previousPosition
	Vector3_t2243707580  ___previousPosition_11;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> ParticlePainter::currentPaintVertices
	List_1_t1612828712 * ___currentPaintVertices_12;
	// UnityEngine.Color ParticlePainter::currentColor
	Color_t2020392075  ___currentColor_13;
	// System.Collections.Generic.List`1<UnityEngine.ParticleSystem> ParticlePainter::paintSystems
	List_1_t2763752173 * ___paintSystems_14;
	// System.Int32 ParticlePainter::paintMode
	int32_t ___paintMode_15;

public:
	inline static int32_t get_offset_of_painterParticlePrefab_2() { return static_cast<int32_t>(offsetof(ParticlePainter_t1073897267, ___painterParticlePrefab_2)); }
	inline ParticleSystem_t3394631041 * get_painterParticlePrefab_2() const { return ___painterParticlePrefab_2; }
	inline ParticleSystem_t3394631041 ** get_address_of_painterParticlePrefab_2() { return &___painterParticlePrefab_2; }
	inline void set_painterParticlePrefab_2(ParticleSystem_t3394631041 * value)
	{
		___painterParticlePrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___painterParticlePrefab_2), value);
	}

	inline static int32_t get_offset_of_minDistanceThreshold_3() { return static_cast<int32_t>(offsetof(ParticlePainter_t1073897267, ___minDistanceThreshold_3)); }
	inline float get_minDistanceThreshold_3() const { return ___minDistanceThreshold_3; }
	inline float* get_address_of_minDistanceThreshold_3() { return &___minDistanceThreshold_3; }
	inline void set_minDistanceThreshold_3(float value)
	{
		___minDistanceThreshold_3 = value;
	}

	inline static int32_t get_offset_of_maxDistanceThreshold_4() { return static_cast<int32_t>(offsetof(ParticlePainter_t1073897267, ___maxDistanceThreshold_4)); }
	inline float get_maxDistanceThreshold_4() const { return ___maxDistanceThreshold_4; }
	inline float* get_address_of_maxDistanceThreshold_4() { return &___maxDistanceThreshold_4; }
	inline void set_maxDistanceThreshold_4(float value)
	{
		___maxDistanceThreshold_4 = value;
	}

	inline static int32_t get_offset_of_frameUpdated_5() { return static_cast<int32_t>(offsetof(ParticlePainter_t1073897267, ___frameUpdated_5)); }
	inline bool get_frameUpdated_5() const { return ___frameUpdated_5; }
	inline bool* get_address_of_frameUpdated_5() { return &___frameUpdated_5; }
	inline void set_frameUpdated_5(bool value)
	{
		___frameUpdated_5 = value;
	}

	inline static int32_t get_offset_of_particleSize_6() { return static_cast<int32_t>(offsetof(ParticlePainter_t1073897267, ___particleSize_6)); }
	inline float get_particleSize_6() const { return ___particleSize_6; }
	inline float* get_address_of_particleSize_6() { return &___particleSize_6; }
	inline void set_particleSize_6(float value)
	{
		___particleSize_6 = value;
	}

	inline static int32_t get_offset_of_penDistance_7() { return static_cast<int32_t>(offsetof(ParticlePainter_t1073897267, ___penDistance_7)); }
	inline float get_penDistance_7() const { return ___penDistance_7; }
	inline float* get_address_of_penDistance_7() { return &___penDistance_7; }
	inline void set_penDistance_7(float value)
	{
		___penDistance_7 = value;
	}

	inline static int32_t get_offset_of_colorPicker_8() { return static_cast<int32_t>(offsetof(ParticlePainter_t1073897267, ___colorPicker_8)); }
	inline ColorPicker_t3035206225 * get_colorPicker_8() const { return ___colorPicker_8; }
	inline ColorPicker_t3035206225 ** get_address_of_colorPicker_8() { return &___colorPicker_8; }
	inline void set_colorPicker_8(ColorPicker_t3035206225 * value)
	{
		___colorPicker_8 = value;
		Il2CppCodeGenWriteBarrier((&___colorPicker_8), value);
	}

	inline static int32_t get_offset_of_currentPS_9() { return static_cast<int32_t>(offsetof(ParticlePainter_t1073897267, ___currentPS_9)); }
	inline ParticleSystem_t3394631041 * get_currentPS_9() const { return ___currentPS_9; }
	inline ParticleSystem_t3394631041 ** get_address_of_currentPS_9() { return &___currentPS_9; }
	inline void set_currentPS_9(ParticleSystem_t3394631041 * value)
	{
		___currentPS_9 = value;
		Il2CppCodeGenWriteBarrier((&___currentPS_9), value);
	}

	inline static int32_t get_offset_of_particles_10() { return static_cast<int32_t>(offsetof(ParticlePainter_t1073897267, ___particles_10)); }
	inline ParticleU5BU5D_t574222242* get_particles_10() const { return ___particles_10; }
	inline ParticleU5BU5D_t574222242** get_address_of_particles_10() { return &___particles_10; }
	inline void set_particles_10(ParticleU5BU5D_t574222242* value)
	{
		___particles_10 = value;
		Il2CppCodeGenWriteBarrier((&___particles_10), value);
	}

	inline static int32_t get_offset_of_previousPosition_11() { return static_cast<int32_t>(offsetof(ParticlePainter_t1073897267, ___previousPosition_11)); }
	inline Vector3_t2243707580  get_previousPosition_11() const { return ___previousPosition_11; }
	inline Vector3_t2243707580 * get_address_of_previousPosition_11() { return &___previousPosition_11; }
	inline void set_previousPosition_11(Vector3_t2243707580  value)
	{
		___previousPosition_11 = value;
	}

	inline static int32_t get_offset_of_currentPaintVertices_12() { return static_cast<int32_t>(offsetof(ParticlePainter_t1073897267, ___currentPaintVertices_12)); }
	inline List_1_t1612828712 * get_currentPaintVertices_12() const { return ___currentPaintVertices_12; }
	inline List_1_t1612828712 ** get_address_of_currentPaintVertices_12() { return &___currentPaintVertices_12; }
	inline void set_currentPaintVertices_12(List_1_t1612828712 * value)
	{
		___currentPaintVertices_12 = value;
		Il2CppCodeGenWriteBarrier((&___currentPaintVertices_12), value);
	}

	inline static int32_t get_offset_of_currentColor_13() { return static_cast<int32_t>(offsetof(ParticlePainter_t1073897267, ___currentColor_13)); }
	inline Color_t2020392075  get_currentColor_13() const { return ___currentColor_13; }
	inline Color_t2020392075 * get_address_of_currentColor_13() { return &___currentColor_13; }
	inline void set_currentColor_13(Color_t2020392075  value)
	{
		___currentColor_13 = value;
	}

	inline static int32_t get_offset_of_paintSystems_14() { return static_cast<int32_t>(offsetof(ParticlePainter_t1073897267, ___paintSystems_14)); }
	inline List_1_t2763752173 * get_paintSystems_14() const { return ___paintSystems_14; }
	inline List_1_t2763752173 ** get_address_of_paintSystems_14() { return &___paintSystems_14; }
	inline void set_paintSystems_14(List_1_t2763752173 * value)
	{
		___paintSystems_14 = value;
		Il2CppCodeGenWriteBarrier((&___paintSystems_14), value);
	}

	inline static int32_t get_offset_of_paintMode_15() { return static_cast<int32_t>(offsetof(ParticlePainter_t1073897267, ___paintMode_15)); }
	inline int32_t get_paintMode_15() const { return ___paintMode_15; }
	inline int32_t* get_address_of_paintMode_15() { return &___paintMode_15; }
	inline void set_paintMode_15(int32_t value)
	{
		___paintMode_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLEPAINTER_T1073897267_H
#ifndef BALLMAKER_T2085518213_H
#define BALLMAKER_T2085518213_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BallMaker
struct  BallMaker_t2085518213  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject BallMaker::ballPrefab
	GameObject_t1756533147 * ___ballPrefab_2;
	// System.Single BallMaker::createHeight
	float ___createHeight_3;
	// UnityEngine.MaterialPropertyBlock BallMaker::props
	MaterialPropertyBlock_t3303648957 * ___props_4;

public:
	inline static int32_t get_offset_of_ballPrefab_2() { return static_cast<int32_t>(offsetof(BallMaker_t2085518213, ___ballPrefab_2)); }
	inline GameObject_t1756533147 * get_ballPrefab_2() const { return ___ballPrefab_2; }
	inline GameObject_t1756533147 ** get_address_of_ballPrefab_2() { return &___ballPrefab_2; }
	inline void set_ballPrefab_2(GameObject_t1756533147 * value)
	{
		___ballPrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___ballPrefab_2), value);
	}

	inline static int32_t get_offset_of_createHeight_3() { return static_cast<int32_t>(offsetof(BallMaker_t2085518213, ___createHeight_3)); }
	inline float get_createHeight_3() const { return ___createHeight_3; }
	inline float* get_address_of_createHeight_3() { return &___createHeight_3; }
	inline void set_createHeight_3(float value)
	{
		___createHeight_3 = value;
	}

	inline static int32_t get_offset_of_props_4() { return static_cast<int32_t>(offsetof(BallMaker_t2085518213, ___props_4)); }
	inline MaterialPropertyBlock_t3303648957 * get_props_4() const { return ___props_4; }
	inline MaterialPropertyBlock_t3303648957 ** get_address_of_props_4() { return &___props_4; }
	inline void set_props_4(MaterialPropertyBlock_t3303648957 * value)
	{
		___props_4 = value;
		Il2CppCodeGenWriteBarrier((&___props_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BALLMAKER_T2085518213_H
#ifndef BALLMOVER_T754704982_H
#define BALLMOVER_T754704982_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BallMover
struct  BallMover_t754704982  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject BallMover::collBallPrefab
	GameObject_t1756533147 * ___collBallPrefab_2;
	// UnityEngine.GameObject BallMover::collBallGO
	GameObject_t1756533147 * ___collBallGO_3;

public:
	inline static int32_t get_offset_of_collBallPrefab_2() { return static_cast<int32_t>(offsetof(BallMover_t754704982, ___collBallPrefab_2)); }
	inline GameObject_t1756533147 * get_collBallPrefab_2() const { return ___collBallPrefab_2; }
	inline GameObject_t1756533147 ** get_address_of_collBallPrefab_2() { return &___collBallPrefab_2; }
	inline void set_collBallPrefab_2(GameObject_t1756533147 * value)
	{
		___collBallPrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___collBallPrefab_2), value);
	}

	inline static int32_t get_offset_of_collBallGO_3() { return static_cast<int32_t>(offsetof(BallMover_t754704982, ___collBallGO_3)); }
	inline GameObject_t1756533147 * get_collBallGO_3() const { return ___collBallGO_3; }
	inline GameObject_t1756533147 ** get_address_of_collBallGO_3() { return &___collBallGO_3; }
	inline void set_collBallGO_3(GameObject_t1756533147 * value)
	{
		___collBallGO_3 = value;
		Il2CppCodeGenWriteBarrier((&___collBallGO_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BALLMOVER_T754704982_H
#ifndef EXAMPLERETRIEVEANDRANK_T500619117_H
#define EXAMPLERETRIEVEANDRANK_T500619117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExampleRetrieveAndRank
struct  ExampleRetrieveAndRank_t500619117  : public MonoBehaviour_t1158329972
{
public:
	// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank ExampleRetrieveAndRank::m_RetrieveAndRank
	RetrieveAndRank_t1381045327 * ___m_RetrieveAndRank_2;

public:
	inline static int32_t get_offset_of_m_RetrieveAndRank_2() { return static_cast<int32_t>(offsetof(ExampleRetrieveAndRank_t500619117, ___m_RetrieveAndRank_2)); }
	inline RetrieveAndRank_t1381045327 * get_m_RetrieveAndRank_2() const { return ___m_RetrieveAndRank_2; }
	inline RetrieveAndRank_t1381045327 ** get_address_of_m_RetrieveAndRank_2() { return &___m_RetrieveAndRank_2; }
	inline void set_m_RetrieveAndRank_2(RetrieveAndRank_t1381045327 * value)
	{
		___m_RetrieveAndRank_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_RetrieveAndRank_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXAMPLERETRIEVEANDRANK_T500619117_H
#ifndef EXAMPLESPEECHTOTEXT_T2083741878_H
#define EXAMPLESPEECHTOTEXT_T2083741878_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExampleSpeechToText
struct  ExampleSpeechToText_t2083741878  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.AudioClip ExampleSpeechToText::m_AudioClip
	AudioClip_t1932558630 * ___m_AudioClip_2;
	// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText ExampleSpeechToText::m_SpeechToText
	SpeechToText_t2713896346 * ___m_SpeechToText_3;
	// System.String ExampleSpeechToText::m_CreatedCustomizationID
	String_t* ___m_CreatedCustomizationID_4;
	// System.String ExampleSpeechToText::m_CreatedCorpusName
	String_t* ___m_CreatedCorpusName_5;
	// System.String ExampleSpeechToText::m_CustomCorpusFilePath
	String_t* ___m_CustomCorpusFilePath_6;

public:
	inline static int32_t get_offset_of_m_AudioClip_2() { return static_cast<int32_t>(offsetof(ExampleSpeechToText_t2083741878, ___m_AudioClip_2)); }
	inline AudioClip_t1932558630 * get_m_AudioClip_2() const { return ___m_AudioClip_2; }
	inline AudioClip_t1932558630 ** get_address_of_m_AudioClip_2() { return &___m_AudioClip_2; }
	inline void set_m_AudioClip_2(AudioClip_t1932558630 * value)
	{
		___m_AudioClip_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_AudioClip_2), value);
	}

	inline static int32_t get_offset_of_m_SpeechToText_3() { return static_cast<int32_t>(offsetof(ExampleSpeechToText_t2083741878, ___m_SpeechToText_3)); }
	inline SpeechToText_t2713896346 * get_m_SpeechToText_3() const { return ___m_SpeechToText_3; }
	inline SpeechToText_t2713896346 ** get_address_of_m_SpeechToText_3() { return &___m_SpeechToText_3; }
	inline void set_m_SpeechToText_3(SpeechToText_t2713896346 * value)
	{
		___m_SpeechToText_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_SpeechToText_3), value);
	}

	inline static int32_t get_offset_of_m_CreatedCustomizationID_4() { return static_cast<int32_t>(offsetof(ExampleSpeechToText_t2083741878, ___m_CreatedCustomizationID_4)); }
	inline String_t* get_m_CreatedCustomizationID_4() const { return ___m_CreatedCustomizationID_4; }
	inline String_t** get_address_of_m_CreatedCustomizationID_4() { return &___m_CreatedCustomizationID_4; }
	inline void set_m_CreatedCustomizationID_4(String_t* value)
	{
		___m_CreatedCustomizationID_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_CreatedCustomizationID_4), value);
	}

	inline static int32_t get_offset_of_m_CreatedCorpusName_5() { return static_cast<int32_t>(offsetof(ExampleSpeechToText_t2083741878, ___m_CreatedCorpusName_5)); }
	inline String_t* get_m_CreatedCorpusName_5() const { return ___m_CreatedCorpusName_5; }
	inline String_t** get_address_of_m_CreatedCorpusName_5() { return &___m_CreatedCorpusName_5; }
	inline void set_m_CreatedCorpusName_5(String_t* value)
	{
		___m_CreatedCorpusName_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_CreatedCorpusName_5), value);
	}

	inline static int32_t get_offset_of_m_CustomCorpusFilePath_6() { return static_cast<int32_t>(offsetof(ExampleSpeechToText_t2083741878, ___m_CustomCorpusFilePath_6)); }
	inline String_t* get_m_CustomCorpusFilePath_6() const { return ___m_CustomCorpusFilePath_6; }
	inline String_t** get_address_of_m_CustomCorpusFilePath_6() { return &___m_CustomCorpusFilePath_6; }
	inline void set_m_CustomCorpusFilePath_6(String_t* value)
	{
		___m_CustomCorpusFilePath_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_CustomCorpusFilePath_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXAMPLESPEECHTOTEXT_T2083741878_H
#ifndef EXAMPLESTREAMING_T2645177802_H
#define EXAMPLESTREAMING_T2645177802_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExampleStreaming
struct  ExampleStreaming_t2645177802  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 ExampleStreaming::m_RecordingRoutine
	int32_t ___m_RecordingRoutine_2;
	// System.String ExampleStreaming::m_MicrophoneID
	String_t* ___m_MicrophoneID_3;
	// UnityEngine.AudioClip ExampleStreaming::m_Recording
	AudioClip_t1932558630 * ___m_Recording_4;
	// System.Int32 ExampleStreaming::m_RecordingBufferSize
	int32_t ___m_RecordingBufferSize_5;
	// System.Int32 ExampleStreaming::m_RecordingHZ
	int32_t ___m_RecordingHZ_6;
	// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText ExampleStreaming::m_SpeechToText
	SpeechToText_t2713896346 * ___m_SpeechToText_7;

public:
	inline static int32_t get_offset_of_m_RecordingRoutine_2() { return static_cast<int32_t>(offsetof(ExampleStreaming_t2645177802, ___m_RecordingRoutine_2)); }
	inline int32_t get_m_RecordingRoutine_2() const { return ___m_RecordingRoutine_2; }
	inline int32_t* get_address_of_m_RecordingRoutine_2() { return &___m_RecordingRoutine_2; }
	inline void set_m_RecordingRoutine_2(int32_t value)
	{
		___m_RecordingRoutine_2 = value;
	}

	inline static int32_t get_offset_of_m_MicrophoneID_3() { return static_cast<int32_t>(offsetof(ExampleStreaming_t2645177802, ___m_MicrophoneID_3)); }
	inline String_t* get_m_MicrophoneID_3() const { return ___m_MicrophoneID_3; }
	inline String_t** get_address_of_m_MicrophoneID_3() { return &___m_MicrophoneID_3; }
	inline void set_m_MicrophoneID_3(String_t* value)
	{
		___m_MicrophoneID_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_MicrophoneID_3), value);
	}

	inline static int32_t get_offset_of_m_Recording_4() { return static_cast<int32_t>(offsetof(ExampleStreaming_t2645177802, ___m_Recording_4)); }
	inline AudioClip_t1932558630 * get_m_Recording_4() const { return ___m_Recording_4; }
	inline AudioClip_t1932558630 ** get_address_of_m_Recording_4() { return &___m_Recording_4; }
	inline void set_m_Recording_4(AudioClip_t1932558630 * value)
	{
		___m_Recording_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Recording_4), value);
	}

	inline static int32_t get_offset_of_m_RecordingBufferSize_5() { return static_cast<int32_t>(offsetof(ExampleStreaming_t2645177802, ___m_RecordingBufferSize_5)); }
	inline int32_t get_m_RecordingBufferSize_5() const { return ___m_RecordingBufferSize_5; }
	inline int32_t* get_address_of_m_RecordingBufferSize_5() { return &___m_RecordingBufferSize_5; }
	inline void set_m_RecordingBufferSize_5(int32_t value)
	{
		___m_RecordingBufferSize_5 = value;
	}

	inline static int32_t get_offset_of_m_RecordingHZ_6() { return static_cast<int32_t>(offsetof(ExampleStreaming_t2645177802, ___m_RecordingHZ_6)); }
	inline int32_t get_m_RecordingHZ_6() const { return ___m_RecordingHZ_6; }
	inline int32_t* get_address_of_m_RecordingHZ_6() { return &___m_RecordingHZ_6; }
	inline void set_m_RecordingHZ_6(int32_t value)
	{
		___m_RecordingHZ_6 = value;
	}

	inline static int32_t get_offset_of_m_SpeechToText_7() { return static_cast<int32_t>(offsetof(ExampleStreaming_t2645177802, ___m_SpeechToText_7)); }
	inline SpeechToText_t2713896346 * get_m_SpeechToText_7() const { return ___m_SpeechToText_7; }
	inline SpeechToText_t2713896346 ** get_address_of_m_SpeechToText_7() { return &___m_SpeechToText_7; }
	inline void set_m_SpeechToText_7(SpeechToText_t2713896346 * value)
	{
		___m_SpeechToText_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_SpeechToText_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXAMPLESTREAMING_T2645177802_H
#ifndef EXAMPLEPERSONALITYINSIGHTSV3_T4226304928_H
#define EXAMPLEPERSONALITYINSIGHTSV3_T4226304928_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExamplePersonalityInsightsV3
struct  ExamplePersonalityInsightsV3_t4226304928  : public MonoBehaviour_t1158329972
{
public:
	// IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v3.PersonalityInsights ExamplePersonalityInsightsV3::m_personalityInsights
	PersonalityInsights_t2503525937 * ___m_personalityInsights_2;
	// System.String ExamplePersonalityInsightsV3::testString
	String_t* ___testString_3;
	// System.String ExamplePersonalityInsightsV3::dataPath
	String_t* ___dataPath_4;

public:
	inline static int32_t get_offset_of_m_personalityInsights_2() { return static_cast<int32_t>(offsetof(ExamplePersonalityInsightsV3_t4226304928, ___m_personalityInsights_2)); }
	inline PersonalityInsights_t2503525937 * get_m_personalityInsights_2() const { return ___m_personalityInsights_2; }
	inline PersonalityInsights_t2503525937 ** get_address_of_m_personalityInsights_2() { return &___m_personalityInsights_2; }
	inline void set_m_personalityInsights_2(PersonalityInsights_t2503525937 * value)
	{
		___m_personalityInsights_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_personalityInsights_2), value);
	}

	inline static int32_t get_offset_of_testString_3() { return static_cast<int32_t>(offsetof(ExamplePersonalityInsightsV3_t4226304928, ___testString_3)); }
	inline String_t* get_testString_3() const { return ___testString_3; }
	inline String_t** get_address_of_testString_3() { return &___testString_3; }
	inline void set_testString_3(String_t* value)
	{
		___testString_3 = value;
		Il2CppCodeGenWriteBarrier((&___testString_3), value);
	}

	inline static int32_t get_offset_of_dataPath_4() { return static_cast<int32_t>(offsetof(ExamplePersonalityInsightsV3_t4226304928, ___dataPath_4)); }
	inline String_t* get_dataPath_4() const { return ___dataPath_4; }
	inline String_t** get_address_of_dataPath_4() { return &___dataPath_4; }
	inline void set_dataPath_4(String_t* value)
	{
		___dataPath_4 = value;
		Il2CppCodeGenWriteBarrier((&___dataPath_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXAMPLEPERSONALITYINSIGHTSV3_T4226304928_H
#ifndef EXAMPLELANGUAGETRANSLATOR_T1714284830_H
#define EXAMPLELANGUAGETRANSLATOR_T1714284830_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExampleLanguageTranslator
struct  ExampleLanguageTranslator_t1714284830  : public MonoBehaviour_t1158329972
{
public:
	// IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.LanguageTranslator ExampleLanguageTranslator::m_Translate
	LanguageTranslator_t2981742234 * ___m_Translate_2;
	// System.String ExampleLanguageTranslator::m_PharseToTranslate
	String_t* ___m_PharseToTranslate_3;

public:
	inline static int32_t get_offset_of_m_Translate_2() { return static_cast<int32_t>(offsetof(ExampleLanguageTranslator_t1714284830, ___m_Translate_2)); }
	inline LanguageTranslator_t2981742234 * get_m_Translate_2() const { return ___m_Translate_2; }
	inline LanguageTranslator_t2981742234 ** get_address_of_m_Translate_2() { return &___m_Translate_2; }
	inline void set_m_Translate_2(LanguageTranslator_t2981742234 * value)
	{
		___m_Translate_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Translate_2), value);
	}

	inline static int32_t get_offset_of_m_PharseToTranslate_3() { return static_cast<int32_t>(offsetof(ExampleLanguageTranslator_t1714284830, ___m_PharseToTranslate_3)); }
	inline String_t* get_m_PharseToTranslate_3() const { return ___m_PharseToTranslate_3; }
	inline String_t** get_address_of_m_PharseToTranslate_3() { return &___m_PharseToTranslate_3; }
	inline void set_m_PharseToTranslate_3(String_t* value)
	{
		___m_PharseToTranslate_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_PharseToTranslate_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXAMPLELANGUAGETRANSLATOR_T1714284830_H
#ifndef EXAMPLENATURALLANGUAGECLASSIFIER_T1512206848_H
#define EXAMPLENATURALLANGUAGECLASSIFIER_T1512206848_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExampleNaturalLanguageClassifier
struct  ExampleNaturalLanguageClassifier_t1512206848  : public MonoBehaviour_t1158329972
{
public:
	// IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.NaturalLanguageClassifier ExampleNaturalLanguageClassifier::m_NaturalLanguageClassifier
	NaturalLanguageClassifier_t2492034444 * ___m_NaturalLanguageClassifier_2;
	// System.String ExampleNaturalLanguageClassifier::m_ClassifierId
	String_t* ___m_ClassifierId_3;
	// System.String ExampleNaturalLanguageClassifier::m_InputString
	String_t* ___m_InputString_4;

public:
	inline static int32_t get_offset_of_m_NaturalLanguageClassifier_2() { return static_cast<int32_t>(offsetof(ExampleNaturalLanguageClassifier_t1512206848, ___m_NaturalLanguageClassifier_2)); }
	inline NaturalLanguageClassifier_t2492034444 * get_m_NaturalLanguageClassifier_2() const { return ___m_NaturalLanguageClassifier_2; }
	inline NaturalLanguageClassifier_t2492034444 ** get_address_of_m_NaturalLanguageClassifier_2() { return &___m_NaturalLanguageClassifier_2; }
	inline void set_m_NaturalLanguageClassifier_2(NaturalLanguageClassifier_t2492034444 * value)
	{
		___m_NaturalLanguageClassifier_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_NaturalLanguageClassifier_2), value);
	}

	inline static int32_t get_offset_of_m_ClassifierId_3() { return static_cast<int32_t>(offsetof(ExampleNaturalLanguageClassifier_t1512206848, ___m_ClassifierId_3)); }
	inline String_t* get_m_ClassifierId_3() const { return ___m_ClassifierId_3; }
	inline String_t** get_address_of_m_ClassifierId_3() { return &___m_ClassifierId_3; }
	inline void set_m_ClassifierId_3(String_t* value)
	{
		___m_ClassifierId_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_ClassifierId_3), value);
	}

	inline static int32_t get_offset_of_m_InputString_4() { return static_cast<int32_t>(offsetof(ExampleNaturalLanguageClassifier_t1512206848, ___m_InputString_4)); }
	inline String_t* get_m_InputString_4() const { return ___m_InputString_4; }
	inline String_t** get_address_of_m_InputString_4() { return &___m_InputString_4; }
	inline void set_m_InputString_4(String_t* value)
	{
		___m_InputString_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InputString_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXAMPLENATURALLANGUAGECLASSIFIER_T1512206848_H
#ifndef EXAMPLEPERSONALITYINSIGHTSV2_T2660220987_H
#define EXAMPLEPERSONALITYINSIGHTSV2_T2660220987_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExamplePersonalityInsightsV2
struct  ExamplePersonalityInsightsV2_t2660220987  : public MonoBehaviour_t1158329972
{
public:
	// IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v2.PersonalityInsights ExamplePersonalityInsightsV2::m_personalityInsights
	PersonalityInsights_t582046669 * ___m_personalityInsights_2;

public:
	inline static int32_t get_offset_of_m_personalityInsights_2() { return static_cast<int32_t>(offsetof(ExamplePersonalityInsightsV2_t2660220987, ___m_personalityInsights_2)); }
	inline PersonalityInsights_t582046669 * get_m_personalityInsights_2() const { return ___m_personalityInsights_2; }
	inline PersonalityInsights_t582046669 ** get_address_of_m_personalityInsights_2() { return &___m_personalityInsights_2; }
	inline void set_m_personalityInsights_2(PersonalityInsights_t582046669 * value)
	{
		___m_personalityInsights_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_personalityInsights_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXAMPLEPERSONALITYINSIGHTSV2_T2660220987_H
#ifndef EXAMPLEDOCUMENTCONVERSION_T3759077945_H
#define EXAMPLEDOCUMENTCONVERSION_T3759077945_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExampleDocumentConversion
struct  ExampleDocumentConversion_t3759077945  : public MonoBehaviour_t1158329972
{
public:
	// IBM.Watson.DeveloperCloud.Services.DocumentConversion.v1.DocumentConversion ExampleDocumentConversion::m_DocumentConversion
	DocumentConversion_t1610681923 * ___m_DocumentConversion_2;

public:
	inline static int32_t get_offset_of_m_DocumentConversion_2() { return static_cast<int32_t>(offsetof(ExampleDocumentConversion_t3759077945, ___m_DocumentConversion_2)); }
	inline DocumentConversion_t1610681923 * get_m_DocumentConversion_2() const { return ___m_DocumentConversion_2; }
	inline DocumentConversion_t1610681923 ** get_address_of_m_DocumentConversion_2() { return &___m_DocumentConversion_2; }
	inline void set_m_DocumentConversion_2(DocumentConversion_t1610681923 * value)
	{
		___m_DocumentConversion_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_DocumentConversion_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXAMPLEDOCUMENTCONVERSION_T3759077945_H
#ifndef EXAMPLETRADEOFFANALYTICS_T2994175413_H
#define EXAMPLETRADEOFFANALYTICS_T2994175413_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExampleTradeoffAnalytics
struct  ExampleTradeoffAnalytics_t2994175413  : public MonoBehaviour_t1158329972
{
public:
	// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics ExampleTradeoffAnalytics::m_TradeoffAnalytics
	TradeoffAnalytics_t100002595 * ___m_TradeoffAnalytics_2;

public:
	inline static int32_t get_offset_of_m_TradeoffAnalytics_2() { return static_cast<int32_t>(offsetof(ExampleTradeoffAnalytics_t2994175413, ___m_TradeoffAnalytics_2)); }
	inline TradeoffAnalytics_t100002595 * get_m_TradeoffAnalytics_2() const { return ___m_TradeoffAnalytics_2; }
	inline TradeoffAnalytics_t100002595 ** get_address_of_m_TradeoffAnalytics_2() { return &___m_TradeoffAnalytics_2; }
	inline void set_m_TradeoffAnalytics_2(TradeoffAnalytics_t100002595 * value)
	{
		___m_TradeoffAnalytics_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TradeoffAnalytics_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXAMPLETRADEOFFANALYTICS_T2994175413_H
#ifndef UNITYARCAMERAMANAGER_T2138856896_H
#define UNITYARCAMERAMANAGER_T2138856896_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityARCameraManager
struct  UnityARCameraManager_t2138856896  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Camera UnityARCameraManager::m_camera
	Camera_t189460977 * ___m_camera_2;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface UnityARCameraManager::m_session
	UnityARSessionNativeInterface_t1130867170 * ___m_session_3;
	// UnityEngine.Material UnityARCameraManager::savedClearMaterial
	Material_t193706927 * ___savedClearMaterial_4;

public:
	inline static int32_t get_offset_of_m_camera_2() { return static_cast<int32_t>(offsetof(UnityARCameraManager_t2138856896, ___m_camera_2)); }
	inline Camera_t189460977 * get_m_camera_2() const { return ___m_camera_2; }
	inline Camera_t189460977 ** get_address_of_m_camera_2() { return &___m_camera_2; }
	inline void set_m_camera_2(Camera_t189460977 * value)
	{
		___m_camera_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_camera_2), value);
	}

	inline static int32_t get_offset_of_m_session_3() { return static_cast<int32_t>(offsetof(UnityARCameraManager_t2138856896, ___m_session_3)); }
	inline UnityARSessionNativeInterface_t1130867170 * get_m_session_3() const { return ___m_session_3; }
	inline UnityARSessionNativeInterface_t1130867170 ** get_address_of_m_session_3() { return &___m_session_3; }
	inline void set_m_session_3(UnityARSessionNativeInterface_t1130867170 * value)
	{
		___m_session_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_session_3), value);
	}

	inline static int32_t get_offset_of_savedClearMaterial_4() { return static_cast<int32_t>(offsetof(UnityARCameraManager_t2138856896, ___savedClearMaterial_4)); }
	inline Material_t193706927 * get_savedClearMaterial_4() const { return ___savedClearMaterial_4; }
	inline Material_t193706927 ** get_address_of_savedClearMaterial_4() { return &___savedClearMaterial_4; }
	inline void set_savedClearMaterial_4(Material_t193706927 * value)
	{
		___savedClearMaterial_4 = value;
		Il2CppCodeGenWriteBarrier((&___savedClearMaterial_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARCAMERAMANAGER_T2138856896_H
#ifndef UNITYARCAMERANEARFAR_T519802600_H
#define UNITYARCAMERANEARFAR_T519802600_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityARCameraNearFar
struct  UnityARCameraNearFar_t519802600  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Camera UnityARCameraNearFar::attachedCamera
	Camera_t189460977 * ___attachedCamera_2;
	// System.Single UnityARCameraNearFar::currentNearZ
	float ___currentNearZ_3;
	// System.Single UnityARCameraNearFar::currentFarZ
	float ___currentFarZ_4;

public:
	inline static int32_t get_offset_of_attachedCamera_2() { return static_cast<int32_t>(offsetof(UnityARCameraNearFar_t519802600, ___attachedCamera_2)); }
	inline Camera_t189460977 * get_attachedCamera_2() const { return ___attachedCamera_2; }
	inline Camera_t189460977 ** get_address_of_attachedCamera_2() { return &___attachedCamera_2; }
	inline void set_attachedCamera_2(Camera_t189460977 * value)
	{
		___attachedCamera_2 = value;
		Il2CppCodeGenWriteBarrier((&___attachedCamera_2), value);
	}

	inline static int32_t get_offset_of_currentNearZ_3() { return static_cast<int32_t>(offsetof(UnityARCameraNearFar_t519802600, ___currentNearZ_3)); }
	inline float get_currentNearZ_3() const { return ___currentNearZ_3; }
	inline float* get_address_of_currentNearZ_3() { return &___currentNearZ_3; }
	inline void set_currentNearZ_3(float value)
	{
		___currentNearZ_3 = value;
	}

	inline static int32_t get_offset_of_currentFarZ_4() { return static_cast<int32_t>(offsetof(UnityARCameraNearFar_t519802600, ___currentFarZ_4)); }
	inline float get_currentFarZ_4() const { return ___currentFarZ_4; }
	inline float* get_address_of_currentFarZ_4() { return &___currentFarZ_4; }
	inline void set_currentFarZ_4(float value)
	{
		___currentFarZ_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARCAMERANEARFAR_T519802600_H
#ifndef POINTCLOUDPARTICLEEXAMPLE_T986756623_H
#define POINTCLOUDPARTICLEEXAMPLE_T986756623_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PointCloudParticleExample
struct  PointCloudParticleExample_t986756623  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.ParticleSystem PointCloudParticleExample::pointCloudParticlePrefab
	ParticleSystem_t3394631041 * ___pointCloudParticlePrefab_2;
	// System.Int32 PointCloudParticleExample::maxPointsToShow
	int32_t ___maxPointsToShow_3;
	// System.Single PointCloudParticleExample::particleSize
	float ___particleSize_4;
	// UnityEngine.Vector3[] PointCloudParticleExample::m_PointCloudData
	Vector3U5BU5D_t1172311765* ___m_PointCloudData_5;
	// System.Boolean PointCloudParticleExample::frameUpdated
	bool ___frameUpdated_6;
	// UnityEngine.ParticleSystem PointCloudParticleExample::currentPS
	ParticleSystem_t3394631041 * ___currentPS_7;
	// UnityEngine.ParticleSystem/Particle[] PointCloudParticleExample::particles
	ParticleU5BU5D_t574222242* ___particles_8;

public:
	inline static int32_t get_offset_of_pointCloudParticlePrefab_2() { return static_cast<int32_t>(offsetof(PointCloudParticleExample_t986756623, ___pointCloudParticlePrefab_2)); }
	inline ParticleSystem_t3394631041 * get_pointCloudParticlePrefab_2() const { return ___pointCloudParticlePrefab_2; }
	inline ParticleSystem_t3394631041 ** get_address_of_pointCloudParticlePrefab_2() { return &___pointCloudParticlePrefab_2; }
	inline void set_pointCloudParticlePrefab_2(ParticleSystem_t3394631041 * value)
	{
		___pointCloudParticlePrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___pointCloudParticlePrefab_2), value);
	}

	inline static int32_t get_offset_of_maxPointsToShow_3() { return static_cast<int32_t>(offsetof(PointCloudParticleExample_t986756623, ___maxPointsToShow_3)); }
	inline int32_t get_maxPointsToShow_3() const { return ___maxPointsToShow_3; }
	inline int32_t* get_address_of_maxPointsToShow_3() { return &___maxPointsToShow_3; }
	inline void set_maxPointsToShow_3(int32_t value)
	{
		___maxPointsToShow_3 = value;
	}

	inline static int32_t get_offset_of_particleSize_4() { return static_cast<int32_t>(offsetof(PointCloudParticleExample_t986756623, ___particleSize_4)); }
	inline float get_particleSize_4() const { return ___particleSize_4; }
	inline float* get_address_of_particleSize_4() { return &___particleSize_4; }
	inline void set_particleSize_4(float value)
	{
		___particleSize_4 = value;
	}

	inline static int32_t get_offset_of_m_PointCloudData_5() { return static_cast<int32_t>(offsetof(PointCloudParticleExample_t986756623, ___m_PointCloudData_5)); }
	inline Vector3U5BU5D_t1172311765* get_m_PointCloudData_5() const { return ___m_PointCloudData_5; }
	inline Vector3U5BU5D_t1172311765** get_address_of_m_PointCloudData_5() { return &___m_PointCloudData_5; }
	inline void set_m_PointCloudData_5(Vector3U5BU5D_t1172311765* value)
	{
		___m_PointCloudData_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_PointCloudData_5), value);
	}

	inline static int32_t get_offset_of_frameUpdated_6() { return static_cast<int32_t>(offsetof(PointCloudParticleExample_t986756623, ___frameUpdated_6)); }
	inline bool get_frameUpdated_6() const { return ___frameUpdated_6; }
	inline bool* get_address_of_frameUpdated_6() { return &___frameUpdated_6; }
	inline void set_frameUpdated_6(bool value)
	{
		___frameUpdated_6 = value;
	}

	inline static int32_t get_offset_of_currentPS_7() { return static_cast<int32_t>(offsetof(PointCloudParticleExample_t986756623, ___currentPS_7)); }
	inline ParticleSystem_t3394631041 * get_currentPS_7() const { return ___currentPS_7; }
	inline ParticleSystem_t3394631041 ** get_address_of_currentPS_7() { return &___currentPS_7; }
	inline void set_currentPS_7(ParticleSystem_t3394631041 * value)
	{
		___currentPS_7 = value;
		Il2CppCodeGenWriteBarrier((&___currentPS_7), value);
	}

	inline static int32_t get_offset_of_particles_8() { return static_cast<int32_t>(offsetof(PointCloudParticleExample_t986756623, ___particles_8)); }
	inline ParticleU5BU5D_t574222242* get_particles_8() const { return ___particles_8; }
	inline ParticleU5BU5D_t574222242** get_address_of_particles_8() { return &___particles_8; }
	inline void set_particles_8(ParticleU5BU5D_t574222242* value)
	{
		___particles_8 = value;
		Il2CppCodeGenWriteBarrier((&___particles_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTCLOUDPARTICLEEXAMPLE_T986756623_H
#ifndef EXAMPLETEXTTOSPEECH_T3969242902_H
#define EXAMPLETEXTTOSPEECH_T3969242902_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExampleTextToSpeech
struct  ExampleTextToSpeech_t3969242902  : public MonoBehaviour_t1158329972
{
public:
	// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech ExampleTextToSpeech::m_TextToSpeech
	TextToSpeech_t3349357562 * ___m_TextToSpeech_2;
	// System.String ExampleTextToSpeech::m_TestString
	String_t* ___m_TestString_3;

public:
	inline static int32_t get_offset_of_m_TextToSpeech_2() { return static_cast<int32_t>(offsetof(ExampleTextToSpeech_t3969242902, ___m_TextToSpeech_2)); }
	inline TextToSpeech_t3349357562 * get_m_TextToSpeech_2() const { return ___m_TextToSpeech_2; }
	inline TextToSpeech_t3349357562 ** get_address_of_m_TextToSpeech_2() { return &___m_TextToSpeech_2; }
	inline void set_m_TextToSpeech_2(TextToSpeech_t3349357562 * value)
	{
		___m_TextToSpeech_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextToSpeech_2), value);
	}

	inline static int32_t get_offset_of_m_TestString_3() { return static_cast<int32_t>(offsetof(ExampleTextToSpeech_t3969242902, ___m_TestString_3)); }
	inline String_t* get_m_TestString_3() const { return ___m_TestString_3; }
	inline String_t** get_address_of_m_TestString_3() { return &___m_TestString_3; }
	inline void set_m_TestString_3(String_t* value)
	{
		___m_TestString_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_TestString_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXAMPLETEXTTOSPEECH_T3969242902_H
#ifndef EXAMPLETONEANALYZER_T3568742368_H
#define EXAMPLETONEANALYZER_T3568742368_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExampleToneAnalyzer
struct  ExampleToneAnalyzer_t3568742368  : public MonoBehaviour_t1158329972
{
public:
	// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzer ExampleToneAnalyzer::m_ToneAnalyzer
	ToneAnalyzer_t1356110496 * ___m_ToneAnalyzer_2;
	// System.String ExampleToneAnalyzer::m_StringToTestTone
	String_t* ___m_StringToTestTone_3;

public:
	inline static int32_t get_offset_of_m_ToneAnalyzer_2() { return static_cast<int32_t>(offsetof(ExampleToneAnalyzer_t3568742368, ___m_ToneAnalyzer_2)); }
	inline ToneAnalyzer_t1356110496 * get_m_ToneAnalyzer_2() const { return ___m_ToneAnalyzer_2; }
	inline ToneAnalyzer_t1356110496 ** get_address_of_m_ToneAnalyzer_2() { return &___m_ToneAnalyzer_2; }
	inline void set_m_ToneAnalyzer_2(ToneAnalyzer_t1356110496 * value)
	{
		___m_ToneAnalyzer_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ToneAnalyzer_2), value);
	}

	inline static int32_t get_offset_of_m_StringToTestTone_3() { return static_cast<int32_t>(offsetof(ExampleToneAnalyzer_t3568742368, ___m_StringToTestTone_3)); }
	inline String_t* get_m_StringToTestTone_3() const { return ___m_StringToTestTone_3; }
	inline String_t** get_address_of_m_StringToTestTone_3() { return &___m_StringToTestTone_3; }
	inline void set_m_StringToTestTone_3(String_t* value)
	{
		___m_StringToTestTone_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_StringToTestTone_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXAMPLETONEANALYZER_T3568742368_H
#ifndef UIBEHAVIOUR_T3960014691_H
#define UIBEHAVIOUR_T3960014691_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3960014691  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3960014691_H
#ifndef HEXCOLORFIELD_T4192118964_H
#define HEXCOLORFIELD_T4192118964_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HexColorField
struct  HexColorField_t4192118964  : public MonoBehaviour_t1158329972
{
public:
	// ColorPicker HexColorField::hsvpicker
	ColorPicker_t3035206225 * ___hsvpicker_2;
	// System.Boolean HexColorField::displayAlpha
	bool ___displayAlpha_3;
	// UnityEngine.UI.InputField HexColorField::hexInputField
	InputField_t1631627530 * ___hexInputField_4;

public:
	inline static int32_t get_offset_of_hsvpicker_2() { return static_cast<int32_t>(offsetof(HexColorField_t4192118964, ___hsvpicker_2)); }
	inline ColorPicker_t3035206225 * get_hsvpicker_2() const { return ___hsvpicker_2; }
	inline ColorPicker_t3035206225 ** get_address_of_hsvpicker_2() { return &___hsvpicker_2; }
	inline void set_hsvpicker_2(ColorPicker_t3035206225 * value)
	{
		___hsvpicker_2 = value;
		Il2CppCodeGenWriteBarrier((&___hsvpicker_2), value);
	}

	inline static int32_t get_offset_of_displayAlpha_3() { return static_cast<int32_t>(offsetof(HexColorField_t4192118964, ___displayAlpha_3)); }
	inline bool get_displayAlpha_3() const { return ___displayAlpha_3; }
	inline bool* get_address_of_displayAlpha_3() { return &___displayAlpha_3; }
	inline void set_displayAlpha_3(bool value)
	{
		___displayAlpha_3 = value;
	}

	inline static int32_t get_offset_of_hexInputField_4() { return static_cast<int32_t>(offsetof(HexColorField_t4192118964, ___hexInputField_4)); }
	inline InputField_t1631627530 * get_hexInputField_4() const { return ___hexInputField_4; }
	inline InputField_t1631627530 ** get_address_of_hexInputField_4() { return &___hexInputField_4; }
	inline void set_hexInputField_4(InputField_t1631627530 * value)
	{
		___hexInputField_4 = value;
		Il2CppCodeGenWriteBarrier((&___hexInputField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEXCOLORFIELD_T4192118964_H
#ifndef SVBOXSLIDER_T1173082351_H
#define SVBOXSLIDER_T1173082351_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SVBoxSlider
struct  SVBoxSlider_t1173082351  : public MonoBehaviour_t1158329972
{
public:
	// ColorPicker SVBoxSlider::picker
	ColorPicker_t3035206225 * ___picker_2;
	// UnityEngine.UI.BoxSlider SVBoxSlider::slider
	BoxSlider_t1871650694 * ___slider_3;
	// UnityEngine.UI.RawImage SVBoxSlider::image
	RawImage_t2749640213 * ___image_4;
	// System.Single SVBoxSlider::lastH
	float ___lastH_5;
	// System.Boolean SVBoxSlider::listen
	bool ___listen_6;

public:
	inline static int32_t get_offset_of_picker_2() { return static_cast<int32_t>(offsetof(SVBoxSlider_t1173082351, ___picker_2)); }
	inline ColorPicker_t3035206225 * get_picker_2() const { return ___picker_2; }
	inline ColorPicker_t3035206225 ** get_address_of_picker_2() { return &___picker_2; }
	inline void set_picker_2(ColorPicker_t3035206225 * value)
	{
		___picker_2 = value;
		Il2CppCodeGenWriteBarrier((&___picker_2), value);
	}

	inline static int32_t get_offset_of_slider_3() { return static_cast<int32_t>(offsetof(SVBoxSlider_t1173082351, ___slider_3)); }
	inline BoxSlider_t1871650694 * get_slider_3() const { return ___slider_3; }
	inline BoxSlider_t1871650694 ** get_address_of_slider_3() { return &___slider_3; }
	inline void set_slider_3(BoxSlider_t1871650694 * value)
	{
		___slider_3 = value;
		Il2CppCodeGenWriteBarrier((&___slider_3), value);
	}

	inline static int32_t get_offset_of_image_4() { return static_cast<int32_t>(offsetof(SVBoxSlider_t1173082351, ___image_4)); }
	inline RawImage_t2749640213 * get_image_4() const { return ___image_4; }
	inline RawImage_t2749640213 ** get_address_of_image_4() { return &___image_4; }
	inline void set_image_4(RawImage_t2749640213 * value)
	{
		___image_4 = value;
		Il2CppCodeGenWriteBarrier((&___image_4), value);
	}

	inline static int32_t get_offset_of_lastH_5() { return static_cast<int32_t>(offsetof(SVBoxSlider_t1173082351, ___lastH_5)); }
	inline float get_lastH_5() const { return ___lastH_5; }
	inline float* get_address_of_lastH_5() { return &___lastH_5; }
	inline void set_lastH_5(float value)
	{
		___lastH_5 = value;
	}

	inline static int32_t get_offset_of_listen_6() { return static_cast<int32_t>(offsetof(SVBoxSlider_t1173082351, ___listen_6)); }
	inline bool get_listen_6() const { return ___listen_6; }
	inline bool* get_address_of_listen_6() { return &___listen_6; }
	inline void set_listen_6(bool value)
	{
		___listen_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SVBOXSLIDER_T1173082351_H
#ifndef LOGSYSTEMINITILIZATION_T33207982_H
#define LOGSYSTEMINITILIZATION_T33207982_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Logging.LogSystemInitilization
struct  LogSystemInitilization_t33207982  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 IBM.Watson.DeveloperCloud.Logging.LogSystemInitilization::m_LogHistory
	int32_t ___m_LogHistory_2;
	// IBM.Watson.DeveloperCloud.Logging.LogLevel IBM.Watson.DeveloperCloud.Logging.LogSystemInitilization::m_LogLevelOnFile
	int32_t ___m_LogLevelOnFile_3;

public:
	inline static int32_t get_offset_of_m_LogHistory_2() { return static_cast<int32_t>(offsetof(LogSystemInitilization_t33207982, ___m_LogHistory_2)); }
	inline int32_t get_m_LogHistory_2() const { return ___m_LogHistory_2; }
	inline int32_t* get_address_of_m_LogHistory_2() { return &___m_LogHistory_2; }
	inline void set_m_LogHistory_2(int32_t value)
	{
		___m_LogHistory_2 = value;
	}

	inline static int32_t get_offset_of_m_LogLevelOnFile_3() { return static_cast<int32_t>(offsetof(LogSystemInitilization_t33207982, ___m_LogLevelOnFile_3)); }
	inline int32_t get_m_LogLevelOnFile_3() const { return ___m_LogLevelOnFile_3; }
	inline int32_t* get_address_of_m_LogLevelOnFile_3() { return &___m_LogLevelOnFile_3; }
	inline void set_m_LogLevelOnFile_3(int32_t value)
	{
		___m_LogLevelOnFile_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGSYSTEMINITILIZATION_T33207982_H
#ifndef TEXTORIENTATION_T742127405_H
#define TEXTORIENTATION_T742127405_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextOrientation
struct  TextOrientation_t742127405  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform TextOrientation::player
	Transform_t3275118058 * ___player_2;

public:
	inline static int32_t get_offset_of_player_2() { return static_cast<int32_t>(offsetof(TextOrientation_t742127405, ___player_2)); }
	inline Transform_t3275118058 * get_player_2() const { return ___player_2; }
	inline Transform_t3275118058 ** get_address_of_player_2() { return &___player_2; }
	inline void set_player_2(Transform_t3275118058 * value)
	{
		___player_2 = value;
		Il2CppCodeGenWriteBarrier((&___player_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTORIENTATION_T742127405_H
#ifndef UNITYARAMBIENT_T680084560_H
#define UNITYARAMBIENT_T680084560_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARAmbient
struct  UnityARAmbient_t680084560  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Light UnityEngine.XR.iOS.UnityARAmbient::l
	Light_t494725636 * ___l_2;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface UnityEngine.XR.iOS.UnityARAmbient::m_Session
	UnityARSessionNativeInterface_t1130867170 * ___m_Session_3;

public:
	inline static int32_t get_offset_of_l_2() { return static_cast<int32_t>(offsetof(UnityARAmbient_t680084560, ___l_2)); }
	inline Light_t494725636 * get_l_2() const { return ___l_2; }
	inline Light_t494725636 ** get_address_of_l_2() { return &___l_2; }
	inline void set_l_2(Light_t494725636 * value)
	{
		___l_2 = value;
		Il2CppCodeGenWriteBarrier((&___l_2), value);
	}

	inline static int32_t get_offset_of_m_Session_3() { return static_cast<int32_t>(offsetof(UnityARAmbient_t680084560, ___m_Session_3)); }
	inline UnityARSessionNativeInterface_t1130867170 * get_m_Session_3() const { return ___m_Session_3; }
	inline UnityARSessionNativeInterface_t1130867170 ** get_address_of_m_Session_3() { return &___m_Session_3; }
	inline void set_m_Session_3(UnityARSessionNativeInterface_t1130867170 * value)
	{
		___m_Session_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Session_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARAMBIENT_T680084560_H
#ifndef COLORSLIDERIMAGE_T376502149_H
#define COLORSLIDERIMAGE_T376502149_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorSliderImage
struct  ColorSliderImage_t376502149  : public MonoBehaviour_t1158329972
{
public:
	// ColorPicker ColorSliderImage::picker
	ColorPicker_t3035206225 * ___picker_2;
	// ColorValues ColorSliderImage::type
	int32_t ___type_3;
	// UnityEngine.UI.Slider/Direction ColorSliderImage::direction
	int32_t ___direction_4;
	// UnityEngine.UI.RawImage ColorSliderImage::image
	RawImage_t2749640213 * ___image_5;

public:
	inline static int32_t get_offset_of_picker_2() { return static_cast<int32_t>(offsetof(ColorSliderImage_t376502149, ___picker_2)); }
	inline ColorPicker_t3035206225 * get_picker_2() const { return ___picker_2; }
	inline ColorPicker_t3035206225 ** get_address_of_picker_2() { return &___picker_2; }
	inline void set_picker_2(ColorPicker_t3035206225 * value)
	{
		___picker_2 = value;
		Il2CppCodeGenWriteBarrier((&___picker_2), value);
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(ColorSliderImage_t376502149, ___type_3)); }
	inline int32_t get_type_3() const { return ___type_3; }
	inline int32_t* get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(int32_t value)
	{
		___type_3 = value;
	}

	inline static int32_t get_offset_of_direction_4() { return static_cast<int32_t>(offsetof(ColorSliderImage_t376502149, ___direction_4)); }
	inline int32_t get_direction_4() const { return ___direction_4; }
	inline int32_t* get_address_of_direction_4() { return &___direction_4; }
	inline void set_direction_4(int32_t value)
	{
		___direction_4 = value;
	}

	inline static int32_t get_offset_of_image_5() { return static_cast<int32_t>(offsetof(ColorSliderImage_t376502149, ___image_5)); }
	inline RawImage_t2749640213 * get_image_5() const { return ___image_5; }
	inline RawImage_t2749640213 ** get_address_of_image_5() { return &___image_5; }
	inline void set_image_5(RawImage_t2749640213 * value)
	{
		___image_5 = value;
		Il2CppCodeGenWriteBarrier((&___image_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORSLIDERIMAGE_T376502149_H
#ifndef CONFIGLOADER_T1246716173_H
#define CONFIGLOADER_T1246716173_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConfigLoader
struct  ConfigLoader_t1246716173  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject ConfigLoader::m_Prefab
	GameObject_t1756533147 * ___m_Prefab_2;
	// UnityEngine.GameObject ConfigLoader::m_CreatedObject
	GameObject_t1756533147 * ___m_CreatedObject_3;

public:
	inline static int32_t get_offset_of_m_Prefab_2() { return static_cast<int32_t>(offsetof(ConfigLoader_t1246716173, ___m_Prefab_2)); }
	inline GameObject_t1756533147 * get_m_Prefab_2() const { return ___m_Prefab_2; }
	inline GameObject_t1756533147 ** get_address_of_m_Prefab_2() { return &___m_Prefab_2; }
	inline void set_m_Prefab_2(GameObject_t1756533147 * value)
	{
		___m_Prefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Prefab_2), value);
	}

	inline static int32_t get_offset_of_m_CreatedObject_3() { return static_cast<int32_t>(offsetof(ConfigLoader_t1246716173, ___m_CreatedObject_3)); }
	inline GameObject_t1756533147 * get_m_CreatedObject_3() const { return ___m_CreatedObject_3; }
	inline GameObject_t1756533147 ** get_address_of_m_CreatedObject_3() { return &___m_CreatedObject_3; }
	inline void set_m_CreatedObject_3(GameObject_t1756533147 * value)
	{
		___m_CreatedObject_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_CreatedObject_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGLOADER_T1246716173_H
#ifndef CAMERATARGET_T552179396_H
#define CAMERATARGET_T552179396_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Camera.CameraTarget
struct  CameraTarget_t552179396  : public MonoBehaviour_t1158329972
{
public:
	// IBM.Watson.DeveloperCloud.Camera.WatsonCamera IBM.Watson.DeveloperCloud.Camera.CameraTarget::mp_WatsonCamera
	WatsonCamera_t824577261 * ___mp_WatsonCamera_2;
	// UnityEngine.Camera IBM.Watson.DeveloperCloud.Camera.CameraTarget::mp_CameraAttached
	Camera_t189460977 * ___mp_CameraAttached_3;
	// System.Boolean IBM.Watson.DeveloperCloud.Camera.CameraTarget::m_UseCustomPosition
	bool ___m_UseCustomPosition_4;
	// UnityEngine.Vector3 IBM.Watson.DeveloperCloud.Camera.CameraTarget::m_CustomPosition
	Vector3_t2243707580  ___m_CustomPosition_5;
	// UnityEngine.Vector3 IBM.Watson.DeveloperCloud.Camera.CameraTarget::m_OffsetPosition
	Vector3_t2243707580  ___m_OffsetPosition_6;
	// UnityEngine.Quaternion IBM.Watson.DeveloperCloud.Camera.CameraTarget::m_OffsetPositionRotation
	Quaternion_t4030073918  ___m_OffsetPositionRotation_7;
	// System.Boolean IBM.Watson.DeveloperCloud.Camera.CameraTarget::m_UseCustomRotation
	bool ___m_UseCustomRotation_8;
	// UnityEngine.Quaternion IBM.Watson.DeveloperCloud.Camera.CameraTarget::m_CustomRotation
	Quaternion_t4030073918  ___m_CustomRotation_9;
	// System.Boolean IBM.Watson.DeveloperCloud.Camera.CameraTarget::m_UseTargetObjectToRotate
	bool ___m_UseTargetObjectToRotate_10;
	// UnityEngine.GameObject IBM.Watson.DeveloperCloud.Camera.CameraTarget::m_CustomTargetObjectToLookAt
	GameObject_t1756533147 * ___m_CustomTargetObjectToLookAt_11;
	// UnityEngine.GameObject IBM.Watson.DeveloperCloud.Camera.CameraTarget::m_CameraPathRootObject
	GameObject_t1756533147 * ___m_CameraPathRootObject_12;
	// System.Single IBM.Watson.DeveloperCloud.Camera.CameraTarget::m_RatioAtCameraPath
	float ___m_RatioAtCameraPath_13;
	// UnityEngine.Vector3 IBM.Watson.DeveloperCloud.Camera.CameraTarget::m_DistanceFromCamera
	Vector3_t2243707580  ___m_DistanceFromCamera_14;
	// UnityEngine.Transform[] IBM.Watson.DeveloperCloud.Camera.CameraTarget::m_PathTransforms
	TransformU5BU5D_t3764228911* ___m_PathTransforms_15;
	// System.Boolean IBM.Watson.DeveloperCloud.Camera.CameraTarget::m_TextEnableCamera
	bool ___m_TextEnableCamera_16;
	// System.Boolean IBM.Watson.DeveloperCloud.Camera.CameraTarget::m_TestToMakeItCurrent
	bool ___m_TestToMakeItCurrent_17;

public:
	inline static int32_t get_offset_of_mp_WatsonCamera_2() { return static_cast<int32_t>(offsetof(CameraTarget_t552179396, ___mp_WatsonCamera_2)); }
	inline WatsonCamera_t824577261 * get_mp_WatsonCamera_2() const { return ___mp_WatsonCamera_2; }
	inline WatsonCamera_t824577261 ** get_address_of_mp_WatsonCamera_2() { return &___mp_WatsonCamera_2; }
	inline void set_mp_WatsonCamera_2(WatsonCamera_t824577261 * value)
	{
		___mp_WatsonCamera_2 = value;
		Il2CppCodeGenWriteBarrier((&___mp_WatsonCamera_2), value);
	}

	inline static int32_t get_offset_of_mp_CameraAttached_3() { return static_cast<int32_t>(offsetof(CameraTarget_t552179396, ___mp_CameraAttached_3)); }
	inline Camera_t189460977 * get_mp_CameraAttached_3() const { return ___mp_CameraAttached_3; }
	inline Camera_t189460977 ** get_address_of_mp_CameraAttached_3() { return &___mp_CameraAttached_3; }
	inline void set_mp_CameraAttached_3(Camera_t189460977 * value)
	{
		___mp_CameraAttached_3 = value;
		Il2CppCodeGenWriteBarrier((&___mp_CameraAttached_3), value);
	}

	inline static int32_t get_offset_of_m_UseCustomPosition_4() { return static_cast<int32_t>(offsetof(CameraTarget_t552179396, ___m_UseCustomPosition_4)); }
	inline bool get_m_UseCustomPosition_4() const { return ___m_UseCustomPosition_4; }
	inline bool* get_address_of_m_UseCustomPosition_4() { return &___m_UseCustomPosition_4; }
	inline void set_m_UseCustomPosition_4(bool value)
	{
		___m_UseCustomPosition_4 = value;
	}

	inline static int32_t get_offset_of_m_CustomPosition_5() { return static_cast<int32_t>(offsetof(CameraTarget_t552179396, ___m_CustomPosition_5)); }
	inline Vector3_t2243707580  get_m_CustomPosition_5() const { return ___m_CustomPosition_5; }
	inline Vector3_t2243707580 * get_address_of_m_CustomPosition_5() { return &___m_CustomPosition_5; }
	inline void set_m_CustomPosition_5(Vector3_t2243707580  value)
	{
		___m_CustomPosition_5 = value;
	}

	inline static int32_t get_offset_of_m_OffsetPosition_6() { return static_cast<int32_t>(offsetof(CameraTarget_t552179396, ___m_OffsetPosition_6)); }
	inline Vector3_t2243707580  get_m_OffsetPosition_6() const { return ___m_OffsetPosition_6; }
	inline Vector3_t2243707580 * get_address_of_m_OffsetPosition_6() { return &___m_OffsetPosition_6; }
	inline void set_m_OffsetPosition_6(Vector3_t2243707580  value)
	{
		___m_OffsetPosition_6 = value;
	}

	inline static int32_t get_offset_of_m_OffsetPositionRotation_7() { return static_cast<int32_t>(offsetof(CameraTarget_t552179396, ___m_OffsetPositionRotation_7)); }
	inline Quaternion_t4030073918  get_m_OffsetPositionRotation_7() const { return ___m_OffsetPositionRotation_7; }
	inline Quaternion_t4030073918 * get_address_of_m_OffsetPositionRotation_7() { return &___m_OffsetPositionRotation_7; }
	inline void set_m_OffsetPositionRotation_7(Quaternion_t4030073918  value)
	{
		___m_OffsetPositionRotation_7 = value;
	}

	inline static int32_t get_offset_of_m_UseCustomRotation_8() { return static_cast<int32_t>(offsetof(CameraTarget_t552179396, ___m_UseCustomRotation_8)); }
	inline bool get_m_UseCustomRotation_8() const { return ___m_UseCustomRotation_8; }
	inline bool* get_address_of_m_UseCustomRotation_8() { return &___m_UseCustomRotation_8; }
	inline void set_m_UseCustomRotation_8(bool value)
	{
		___m_UseCustomRotation_8 = value;
	}

	inline static int32_t get_offset_of_m_CustomRotation_9() { return static_cast<int32_t>(offsetof(CameraTarget_t552179396, ___m_CustomRotation_9)); }
	inline Quaternion_t4030073918  get_m_CustomRotation_9() const { return ___m_CustomRotation_9; }
	inline Quaternion_t4030073918 * get_address_of_m_CustomRotation_9() { return &___m_CustomRotation_9; }
	inline void set_m_CustomRotation_9(Quaternion_t4030073918  value)
	{
		___m_CustomRotation_9 = value;
	}

	inline static int32_t get_offset_of_m_UseTargetObjectToRotate_10() { return static_cast<int32_t>(offsetof(CameraTarget_t552179396, ___m_UseTargetObjectToRotate_10)); }
	inline bool get_m_UseTargetObjectToRotate_10() const { return ___m_UseTargetObjectToRotate_10; }
	inline bool* get_address_of_m_UseTargetObjectToRotate_10() { return &___m_UseTargetObjectToRotate_10; }
	inline void set_m_UseTargetObjectToRotate_10(bool value)
	{
		___m_UseTargetObjectToRotate_10 = value;
	}

	inline static int32_t get_offset_of_m_CustomTargetObjectToLookAt_11() { return static_cast<int32_t>(offsetof(CameraTarget_t552179396, ___m_CustomTargetObjectToLookAt_11)); }
	inline GameObject_t1756533147 * get_m_CustomTargetObjectToLookAt_11() const { return ___m_CustomTargetObjectToLookAt_11; }
	inline GameObject_t1756533147 ** get_address_of_m_CustomTargetObjectToLookAt_11() { return &___m_CustomTargetObjectToLookAt_11; }
	inline void set_m_CustomTargetObjectToLookAt_11(GameObject_t1756533147 * value)
	{
		___m_CustomTargetObjectToLookAt_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_CustomTargetObjectToLookAt_11), value);
	}

	inline static int32_t get_offset_of_m_CameraPathRootObject_12() { return static_cast<int32_t>(offsetof(CameraTarget_t552179396, ___m_CameraPathRootObject_12)); }
	inline GameObject_t1756533147 * get_m_CameraPathRootObject_12() const { return ___m_CameraPathRootObject_12; }
	inline GameObject_t1756533147 ** get_address_of_m_CameraPathRootObject_12() { return &___m_CameraPathRootObject_12; }
	inline void set_m_CameraPathRootObject_12(GameObject_t1756533147 * value)
	{
		___m_CameraPathRootObject_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_CameraPathRootObject_12), value);
	}

	inline static int32_t get_offset_of_m_RatioAtCameraPath_13() { return static_cast<int32_t>(offsetof(CameraTarget_t552179396, ___m_RatioAtCameraPath_13)); }
	inline float get_m_RatioAtCameraPath_13() const { return ___m_RatioAtCameraPath_13; }
	inline float* get_address_of_m_RatioAtCameraPath_13() { return &___m_RatioAtCameraPath_13; }
	inline void set_m_RatioAtCameraPath_13(float value)
	{
		___m_RatioAtCameraPath_13 = value;
	}

	inline static int32_t get_offset_of_m_DistanceFromCamera_14() { return static_cast<int32_t>(offsetof(CameraTarget_t552179396, ___m_DistanceFromCamera_14)); }
	inline Vector3_t2243707580  get_m_DistanceFromCamera_14() const { return ___m_DistanceFromCamera_14; }
	inline Vector3_t2243707580 * get_address_of_m_DistanceFromCamera_14() { return &___m_DistanceFromCamera_14; }
	inline void set_m_DistanceFromCamera_14(Vector3_t2243707580  value)
	{
		___m_DistanceFromCamera_14 = value;
	}

	inline static int32_t get_offset_of_m_PathTransforms_15() { return static_cast<int32_t>(offsetof(CameraTarget_t552179396, ___m_PathTransforms_15)); }
	inline TransformU5BU5D_t3764228911* get_m_PathTransforms_15() const { return ___m_PathTransforms_15; }
	inline TransformU5BU5D_t3764228911** get_address_of_m_PathTransforms_15() { return &___m_PathTransforms_15; }
	inline void set_m_PathTransforms_15(TransformU5BU5D_t3764228911* value)
	{
		___m_PathTransforms_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_PathTransforms_15), value);
	}

	inline static int32_t get_offset_of_m_TextEnableCamera_16() { return static_cast<int32_t>(offsetof(CameraTarget_t552179396, ___m_TextEnableCamera_16)); }
	inline bool get_m_TextEnableCamera_16() const { return ___m_TextEnableCamera_16; }
	inline bool* get_address_of_m_TextEnableCamera_16() { return &___m_TextEnableCamera_16; }
	inline void set_m_TextEnableCamera_16(bool value)
	{
		___m_TextEnableCamera_16 = value;
	}

	inline static int32_t get_offset_of_m_TestToMakeItCurrent_17() { return static_cast<int32_t>(offsetof(CameraTarget_t552179396, ___m_TestToMakeItCurrent_17)); }
	inline bool get_m_TestToMakeItCurrent_17() const { return ___m_TestToMakeItCurrent_17; }
	inline bool* get_address_of_m_TestToMakeItCurrent_17() { return &___m_TestToMakeItCurrent_17; }
	inline void set_m_TestToMakeItCurrent_17(bool value)
	{
		___m_TestToMakeItCurrent_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERATARGET_T552179396_H
#ifndef MODELCOLLISION_T1120807151_H
#define MODELCOLLISION_T1120807151_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModelCollision
struct  ModelCollision_t1120807151  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Canvas ModelCollision::canvas
	Canvas_t209405766 * ___canvas_2;

public:
	inline static int32_t get_offset_of_canvas_2() { return static_cast<int32_t>(offsetof(ModelCollision_t1120807151, ___canvas_2)); }
	inline Canvas_t209405766 * get_canvas_2() const { return ___canvas_2; }
	inline Canvas_t209405766 ** get_address_of_canvas_2() { return &___canvas_2; }
	inline void set_canvas_2(Canvas_t209405766 * value)
	{
		___canvas_2 = value;
		Il2CppCodeGenWriteBarrier((&___canvas_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODELCOLLISION_T1120807151_H
#ifndef LOOKATCAMERA_T3167693141_H
#define LOOKATCAMERA_T3167693141_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LookAtCamera
struct  LookAtCamera_t3167693141  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOOKATCAMERA_T3167693141_H
#ifndef HOTCORNER_T121384184_H
#define HOTCORNER_T121384184_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Camera.HotCorner
struct  HotCorner_t121384184  : public MonoBehaviour_t1158329972
{
public:
	// System.Single IBM.Watson.DeveloperCloud.Camera.HotCorner::m_NormalizedThresholdWidth
	float ___m_NormalizedThresholdWidth_2;
	// System.Single IBM.Watson.DeveloperCloud.Camera.HotCorner::m_NormalizedThresholdHeight
	float ___m_NormalizedThresholdHeight_3;

public:
	inline static int32_t get_offset_of_m_NormalizedThresholdWidth_2() { return static_cast<int32_t>(offsetof(HotCorner_t121384184, ___m_NormalizedThresholdWidth_2)); }
	inline float get_m_NormalizedThresholdWidth_2() const { return ___m_NormalizedThresholdWidth_2; }
	inline float* get_address_of_m_NormalizedThresholdWidth_2() { return &___m_NormalizedThresholdWidth_2; }
	inline void set_m_NormalizedThresholdWidth_2(float value)
	{
		___m_NormalizedThresholdWidth_2 = value;
	}

	inline static int32_t get_offset_of_m_NormalizedThresholdHeight_3() { return static_cast<int32_t>(offsetof(HotCorner_t121384184, ___m_NormalizedThresholdHeight_3)); }
	inline float get_m_NormalizedThresholdHeight_3() const { return ___m_NormalizedThresholdHeight_3; }
	inline float* get_address_of_m_NormalizedThresholdHeight_3() { return &___m_NormalizedThresholdHeight_3; }
	inline void set_m_NormalizedThresholdHeight_3(float value)
	{
		___m_NormalizedThresholdHeight_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HOTCORNER_T121384184_H
#ifndef PLAYMANAGER_T1724130965_H
#define PLAYMANAGER_T1724130965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayManager
struct  PlayManager_t1724130965  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Animator[] PlayManager::playerGroup
	AnimatorU5BU5D_t4224369806* ___playerGroup_2;
	// System.String[] PlayManager::animClipNameGroup
	StringU5BU5D_t1642385972* ___animClipNameGroup_3;
	// System.Int32 PlayManager::currentNumber
	int32_t ___currentNumber_4;

public:
	inline static int32_t get_offset_of_playerGroup_2() { return static_cast<int32_t>(offsetof(PlayManager_t1724130965, ___playerGroup_2)); }
	inline AnimatorU5BU5D_t4224369806* get_playerGroup_2() const { return ___playerGroup_2; }
	inline AnimatorU5BU5D_t4224369806** get_address_of_playerGroup_2() { return &___playerGroup_2; }
	inline void set_playerGroup_2(AnimatorU5BU5D_t4224369806* value)
	{
		___playerGroup_2 = value;
		Il2CppCodeGenWriteBarrier((&___playerGroup_2), value);
	}

	inline static int32_t get_offset_of_animClipNameGroup_3() { return static_cast<int32_t>(offsetof(PlayManager_t1724130965, ___animClipNameGroup_3)); }
	inline StringU5BU5D_t1642385972* get_animClipNameGroup_3() const { return ___animClipNameGroup_3; }
	inline StringU5BU5D_t1642385972** get_address_of_animClipNameGroup_3() { return &___animClipNameGroup_3; }
	inline void set_animClipNameGroup_3(StringU5BU5D_t1642385972* value)
	{
		___animClipNameGroup_3 = value;
		Il2CppCodeGenWriteBarrier((&___animClipNameGroup_3), value);
	}

	inline static int32_t get_offset_of_currentNumber_4() { return static_cast<int32_t>(offsetof(PlayManager_t1724130965, ___currentNumber_4)); }
	inline int32_t get_currentNumber_4() const { return ___currentNumber_4; }
	inline int32_t* get_address_of_currentNumber_4() { return &___currentNumber_4; }
	inline void set_currentNumber_4(int32_t value)
	{
		___currentNumber_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMANAGER_T1724130965_H
#ifndef MAINUI_T541415437_H
#define MAINUI_T541415437_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MainUI
struct  MainUI_t541415437  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.LayoutGroup MainUI::m_ButtonLayout
	LayoutGroup_t3962498969 * ___m_ButtonLayout_2;
	// UnityEngine.UI.Button MainUI::m_ButtonPrefab
	Button_t2872111280 * ___m_ButtonPrefab_3;
	// UnityEngine.GameObject MainUI::m_BackgroundUI
	GameObject_t1756533147 * ___m_BackgroundUI_4;
	// UnityEngine.RectTransform MainUI::m_ButtonBack
	RectTransform_t3349966182 * ___m_ButtonBack_5;
	// UnityEngine.Vector3 MainUI::m_InitialBackButtonPosition
	Vector3_t2243707580  ___m_InitialBackButtonPosition_6;
	// UnityEngine.Vector3 MainUI::m_InitialBackButtonScale
	Vector3_t2243707580  ___m_InitialBackButtonScale_7;
	// UnityEngine.Color MainUI::m_InitialBackButtonColor
	Color_t2020392075  ___m_InitialBackButtonColor_8;
	// MenuScene[] MainUI::m_Scenes
	MenuSceneU5BU5D_t1862341966* ___m_Scenes_9;

public:
	inline static int32_t get_offset_of_m_ButtonLayout_2() { return static_cast<int32_t>(offsetof(MainUI_t541415437, ___m_ButtonLayout_2)); }
	inline LayoutGroup_t3962498969 * get_m_ButtonLayout_2() const { return ___m_ButtonLayout_2; }
	inline LayoutGroup_t3962498969 ** get_address_of_m_ButtonLayout_2() { return &___m_ButtonLayout_2; }
	inline void set_m_ButtonLayout_2(LayoutGroup_t3962498969 * value)
	{
		___m_ButtonLayout_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ButtonLayout_2), value);
	}

	inline static int32_t get_offset_of_m_ButtonPrefab_3() { return static_cast<int32_t>(offsetof(MainUI_t541415437, ___m_ButtonPrefab_3)); }
	inline Button_t2872111280 * get_m_ButtonPrefab_3() const { return ___m_ButtonPrefab_3; }
	inline Button_t2872111280 ** get_address_of_m_ButtonPrefab_3() { return &___m_ButtonPrefab_3; }
	inline void set_m_ButtonPrefab_3(Button_t2872111280 * value)
	{
		___m_ButtonPrefab_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_ButtonPrefab_3), value);
	}

	inline static int32_t get_offset_of_m_BackgroundUI_4() { return static_cast<int32_t>(offsetof(MainUI_t541415437, ___m_BackgroundUI_4)); }
	inline GameObject_t1756533147 * get_m_BackgroundUI_4() const { return ___m_BackgroundUI_4; }
	inline GameObject_t1756533147 ** get_address_of_m_BackgroundUI_4() { return &___m_BackgroundUI_4; }
	inline void set_m_BackgroundUI_4(GameObject_t1756533147 * value)
	{
		___m_BackgroundUI_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_BackgroundUI_4), value);
	}

	inline static int32_t get_offset_of_m_ButtonBack_5() { return static_cast<int32_t>(offsetof(MainUI_t541415437, ___m_ButtonBack_5)); }
	inline RectTransform_t3349966182 * get_m_ButtonBack_5() const { return ___m_ButtonBack_5; }
	inline RectTransform_t3349966182 ** get_address_of_m_ButtonBack_5() { return &___m_ButtonBack_5; }
	inline void set_m_ButtonBack_5(RectTransform_t3349966182 * value)
	{
		___m_ButtonBack_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_ButtonBack_5), value);
	}

	inline static int32_t get_offset_of_m_InitialBackButtonPosition_6() { return static_cast<int32_t>(offsetof(MainUI_t541415437, ___m_InitialBackButtonPosition_6)); }
	inline Vector3_t2243707580  get_m_InitialBackButtonPosition_6() const { return ___m_InitialBackButtonPosition_6; }
	inline Vector3_t2243707580 * get_address_of_m_InitialBackButtonPosition_6() { return &___m_InitialBackButtonPosition_6; }
	inline void set_m_InitialBackButtonPosition_6(Vector3_t2243707580  value)
	{
		___m_InitialBackButtonPosition_6 = value;
	}

	inline static int32_t get_offset_of_m_InitialBackButtonScale_7() { return static_cast<int32_t>(offsetof(MainUI_t541415437, ___m_InitialBackButtonScale_7)); }
	inline Vector3_t2243707580  get_m_InitialBackButtonScale_7() const { return ___m_InitialBackButtonScale_7; }
	inline Vector3_t2243707580 * get_address_of_m_InitialBackButtonScale_7() { return &___m_InitialBackButtonScale_7; }
	inline void set_m_InitialBackButtonScale_7(Vector3_t2243707580  value)
	{
		___m_InitialBackButtonScale_7 = value;
	}

	inline static int32_t get_offset_of_m_InitialBackButtonColor_8() { return static_cast<int32_t>(offsetof(MainUI_t541415437, ___m_InitialBackButtonColor_8)); }
	inline Color_t2020392075  get_m_InitialBackButtonColor_8() const { return ___m_InitialBackButtonColor_8; }
	inline Color_t2020392075 * get_address_of_m_InitialBackButtonColor_8() { return &___m_InitialBackButtonColor_8; }
	inline void set_m_InitialBackButtonColor_8(Color_t2020392075  value)
	{
		___m_InitialBackButtonColor_8 = value;
	}

	inline static int32_t get_offset_of_m_Scenes_9() { return static_cast<int32_t>(offsetof(MainUI_t541415437, ___m_Scenes_9)); }
	inline MenuSceneU5BU5D_t1862341966* get_m_Scenes_9() const { return ___m_Scenes_9; }
	inline MenuSceneU5BU5D_t1862341966** get_address_of_m_Scenes_9() { return &___m_Scenes_9; }
	inline void set_m_Scenes_9(MenuSceneU5BU5D_t1862341966* value)
	{
		___m_Scenes_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Scenes_9), value);
	}
};

struct MainUI_t541415437_StaticFields
{
public:
	// MainUI MainUI::_instance
	MainUI_t541415437 * ____instance_11;

public:
	inline static int32_t get_offset_of__instance_11() { return static_cast<int32_t>(offsetof(MainUI_t541415437_StaticFields, ____instance_11)); }
	inline MainUI_t541415437 * get__instance_11() const { return ____instance_11; }
	inline MainUI_t541415437 ** get_address_of__instance_11() { return &____instance_11; }
	inline void set__instance_11(MainUI_t541415437 * value)
	{
		____instance_11 = value;
		Il2CppCodeGenWriteBarrier((&____instance_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAINUI_T541415437_H
#ifndef WATSONCAMERA_T824577261_H
#define WATSONCAMERA_T824577261_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Camera.WatsonCamera
struct  WatsonCamera_t824577261  : public MonoBehaviour_t1158329972
{
public:
	// System.Collections.Generic.List`1<IBM.Watson.DeveloperCloud.Camera.CameraTarget> IBM.Watson.DeveloperCloud.Camera.WatsonCamera::m_ListCameraTarget
	List_1_t4216267824 * ___m_ListCameraTarget_3;
	// IBM.Watson.DeveloperCloud.Camera.CameraTarget IBM.Watson.DeveloperCloud.Camera.WatsonCamera::m_TargetCamera
	CameraTarget_t552179396 * ___m_TargetCamera_4;
	// UnityEngine.Vector3 IBM.Watson.DeveloperCloud.Camera.WatsonCamera::m_CameraInitialLocation
	Vector3_t2243707580  ___m_CameraInitialLocation_5;
	// UnityEngine.Quaternion IBM.Watson.DeveloperCloud.Camera.WatsonCamera::m_CameraInitialRotation
	Quaternion_t4030073918  ___m_CameraInitialRotation_6;
	// System.Single IBM.Watson.DeveloperCloud.Camera.WatsonCamera::m_PanSpeed
	float ___m_PanSpeed_7;
	// System.Single IBM.Watson.DeveloperCloud.Camera.WatsonCamera::m_ZoomSpeed
	float ___m_ZoomSpeed_8;
	// System.Single IBM.Watson.DeveloperCloud.Camera.WatsonCamera::m_SpeedForCameraAnimation
	float ___m_SpeedForCameraAnimation_9;
	// System.Single IBM.Watson.DeveloperCloud.Camera.WatsonCamera::m_CommandMovementModifier
	float ___m_CommandMovementModifier_10;
	// UnityEngine.MonoBehaviour IBM.Watson.DeveloperCloud.Camera.WatsonCamera::m_AntiAliasing
	MonoBehaviour_t1158329972 * ___m_AntiAliasing_11;
	// UnityEngine.MonoBehaviour IBM.Watson.DeveloperCloud.Camera.WatsonCamera::m_DepthOfField
	MonoBehaviour_t1158329972 * ___m_DepthOfField_12;
	// System.Boolean IBM.Watson.DeveloperCloud.Camera.WatsonCamera::m_DisableInteractivity
	bool ___m_DisableInteractivity_13;

public:
	inline static int32_t get_offset_of_m_ListCameraTarget_3() { return static_cast<int32_t>(offsetof(WatsonCamera_t824577261, ___m_ListCameraTarget_3)); }
	inline List_1_t4216267824 * get_m_ListCameraTarget_3() const { return ___m_ListCameraTarget_3; }
	inline List_1_t4216267824 ** get_address_of_m_ListCameraTarget_3() { return &___m_ListCameraTarget_3; }
	inline void set_m_ListCameraTarget_3(List_1_t4216267824 * value)
	{
		___m_ListCameraTarget_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_ListCameraTarget_3), value);
	}

	inline static int32_t get_offset_of_m_TargetCamera_4() { return static_cast<int32_t>(offsetof(WatsonCamera_t824577261, ___m_TargetCamera_4)); }
	inline CameraTarget_t552179396 * get_m_TargetCamera_4() const { return ___m_TargetCamera_4; }
	inline CameraTarget_t552179396 ** get_address_of_m_TargetCamera_4() { return &___m_TargetCamera_4; }
	inline void set_m_TargetCamera_4(CameraTarget_t552179396 * value)
	{
		___m_TargetCamera_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_TargetCamera_4), value);
	}

	inline static int32_t get_offset_of_m_CameraInitialLocation_5() { return static_cast<int32_t>(offsetof(WatsonCamera_t824577261, ___m_CameraInitialLocation_5)); }
	inline Vector3_t2243707580  get_m_CameraInitialLocation_5() const { return ___m_CameraInitialLocation_5; }
	inline Vector3_t2243707580 * get_address_of_m_CameraInitialLocation_5() { return &___m_CameraInitialLocation_5; }
	inline void set_m_CameraInitialLocation_5(Vector3_t2243707580  value)
	{
		___m_CameraInitialLocation_5 = value;
	}

	inline static int32_t get_offset_of_m_CameraInitialRotation_6() { return static_cast<int32_t>(offsetof(WatsonCamera_t824577261, ___m_CameraInitialRotation_6)); }
	inline Quaternion_t4030073918  get_m_CameraInitialRotation_6() const { return ___m_CameraInitialRotation_6; }
	inline Quaternion_t4030073918 * get_address_of_m_CameraInitialRotation_6() { return &___m_CameraInitialRotation_6; }
	inline void set_m_CameraInitialRotation_6(Quaternion_t4030073918  value)
	{
		___m_CameraInitialRotation_6 = value;
	}

	inline static int32_t get_offset_of_m_PanSpeed_7() { return static_cast<int32_t>(offsetof(WatsonCamera_t824577261, ___m_PanSpeed_7)); }
	inline float get_m_PanSpeed_7() const { return ___m_PanSpeed_7; }
	inline float* get_address_of_m_PanSpeed_7() { return &___m_PanSpeed_7; }
	inline void set_m_PanSpeed_7(float value)
	{
		___m_PanSpeed_7 = value;
	}

	inline static int32_t get_offset_of_m_ZoomSpeed_8() { return static_cast<int32_t>(offsetof(WatsonCamera_t824577261, ___m_ZoomSpeed_8)); }
	inline float get_m_ZoomSpeed_8() const { return ___m_ZoomSpeed_8; }
	inline float* get_address_of_m_ZoomSpeed_8() { return &___m_ZoomSpeed_8; }
	inline void set_m_ZoomSpeed_8(float value)
	{
		___m_ZoomSpeed_8 = value;
	}

	inline static int32_t get_offset_of_m_SpeedForCameraAnimation_9() { return static_cast<int32_t>(offsetof(WatsonCamera_t824577261, ___m_SpeedForCameraAnimation_9)); }
	inline float get_m_SpeedForCameraAnimation_9() const { return ___m_SpeedForCameraAnimation_9; }
	inline float* get_address_of_m_SpeedForCameraAnimation_9() { return &___m_SpeedForCameraAnimation_9; }
	inline void set_m_SpeedForCameraAnimation_9(float value)
	{
		___m_SpeedForCameraAnimation_9 = value;
	}

	inline static int32_t get_offset_of_m_CommandMovementModifier_10() { return static_cast<int32_t>(offsetof(WatsonCamera_t824577261, ___m_CommandMovementModifier_10)); }
	inline float get_m_CommandMovementModifier_10() const { return ___m_CommandMovementModifier_10; }
	inline float* get_address_of_m_CommandMovementModifier_10() { return &___m_CommandMovementModifier_10; }
	inline void set_m_CommandMovementModifier_10(float value)
	{
		___m_CommandMovementModifier_10 = value;
	}

	inline static int32_t get_offset_of_m_AntiAliasing_11() { return static_cast<int32_t>(offsetof(WatsonCamera_t824577261, ___m_AntiAliasing_11)); }
	inline MonoBehaviour_t1158329972 * get_m_AntiAliasing_11() const { return ___m_AntiAliasing_11; }
	inline MonoBehaviour_t1158329972 ** get_address_of_m_AntiAliasing_11() { return &___m_AntiAliasing_11; }
	inline void set_m_AntiAliasing_11(MonoBehaviour_t1158329972 * value)
	{
		___m_AntiAliasing_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_AntiAliasing_11), value);
	}

	inline static int32_t get_offset_of_m_DepthOfField_12() { return static_cast<int32_t>(offsetof(WatsonCamera_t824577261, ___m_DepthOfField_12)); }
	inline MonoBehaviour_t1158329972 * get_m_DepthOfField_12() const { return ___m_DepthOfField_12; }
	inline MonoBehaviour_t1158329972 ** get_address_of_m_DepthOfField_12() { return &___m_DepthOfField_12; }
	inline void set_m_DepthOfField_12(MonoBehaviour_t1158329972 * value)
	{
		___m_DepthOfField_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_DepthOfField_12), value);
	}

	inline static int32_t get_offset_of_m_DisableInteractivity_13() { return static_cast<int32_t>(offsetof(WatsonCamera_t824577261, ___m_DisableInteractivity_13)); }
	inline bool get_m_DisableInteractivity_13() const { return ___m_DisableInteractivity_13; }
	inline bool* get_address_of_m_DisableInteractivity_13() { return &___m_DisableInteractivity_13; }
	inline void set_m_DisableInteractivity_13(bool value)
	{
		___m_DisableInteractivity_13 = value;
	}
};

struct WatsonCamera_t824577261_StaticFields
{
public:
	// IBM.Watson.DeveloperCloud.Camera.WatsonCamera IBM.Watson.DeveloperCloud.Camera.WatsonCamera::mp_Instance
	WatsonCamera_t824577261 * ___mp_Instance_2;

public:
	inline static int32_t get_offset_of_mp_Instance_2() { return static_cast<int32_t>(offsetof(WatsonCamera_t824577261_StaticFields, ___mp_Instance_2)); }
	inline WatsonCamera_t824577261 * get_mp_Instance_2() const { return ___mp_Instance_2; }
	inline WatsonCamera_t824577261 ** get_address_of_mp_Instance_2() { return &___mp_Instance_2; }
	inline void set_mp_Instance_2(WatsonCamera_t824577261 * value)
	{
		___mp_Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___mp_Instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WATSONCAMERA_T824577261_H
#ifndef COLORPICKERTESTER_T1006114474_H
#define COLORPICKERTESTER_T1006114474_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorPickerTester
struct  ColorPickerTester_t1006114474  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Renderer ColorPickerTester::renderer
	Renderer_t257310565 * ___renderer_2;
	// ColorPicker ColorPickerTester::picker
	ColorPicker_t3035206225 * ___picker_3;

public:
	inline static int32_t get_offset_of_renderer_2() { return static_cast<int32_t>(offsetof(ColorPickerTester_t1006114474, ___renderer_2)); }
	inline Renderer_t257310565 * get_renderer_2() const { return ___renderer_2; }
	inline Renderer_t257310565 ** get_address_of_renderer_2() { return &___renderer_2; }
	inline void set_renderer_2(Renderer_t257310565 * value)
	{
		___renderer_2 = value;
		Il2CppCodeGenWriteBarrier((&___renderer_2), value);
	}

	inline static int32_t get_offset_of_picker_3() { return static_cast<int32_t>(offsetof(ColorPickerTester_t1006114474, ___picker_3)); }
	inline ColorPicker_t3035206225 * get_picker_3() const { return ___picker_3; }
	inline ColorPicker_t3035206225 ** get_address_of_picker_3() { return &___picker_3; }
	inline void set_picker_3(ColorPicker_t3035206225 * value)
	{
		___picker_3 = value;
		Il2CppCodeGenWriteBarrier((&___picker_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORPICKERTESTER_T1006114474_H
#ifndef TILTWINDOW_T1839185375_H
#define TILTWINDOW_T1839185375_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TiltWindow
struct  TiltWindow_t1839185375  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Vector2 TiltWindow::range
	Vector2_t2243707579  ___range_2;
	// UnityEngine.Transform TiltWindow::mTrans
	Transform_t3275118058 * ___mTrans_3;
	// UnityEngine.Quaternion TiltWindow::mStart
	Quaternion_t4030073918  ___mStart_4;
	// UnityEngine.Vector2 TiltWindow::mRot
	Vector2_t2243707579  ___mRot_5;

public:
	inline static int32_t get_offset_of_range_2() { return static_cast<int32_t>(offsetof(TiltWindow_t1839185375, ___range_2)); }
	inline Vector2_t2243707579  get_range_2() const { return ___range_2; }
	inline Vector2_t2243707579 * get_address_of_range_2() { return &___range_2; }
	inline void set_range_2(Vector2_t2243707579  value)
	{
		___range_2 = value;
	}

	inline static int32_t get_offset_of_mTrans_3() { return static_cast<int32_t>(offsetof(TiltWindow_t1839185375, ___mTrans_3)); }
	inline Transform_t3275118058 * get_mTrans_3() const { return ___mTrans_3; }
	inline Transform_t3275118058 ** get_address_of_mTrans_3() { return &___mTrans_3; }
	inline void set_mTrans_3(Transform_t3275118058 * value)
	{
		___mTrans_3 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_3), value);
	}

	inline static int32_t get_offset_of_mStart_4() { return static_cast<int32_t>(offsetof(TiltWindow_t1839185375, ___mStart_4)); }
	inline Quaternion_t4030073918  get_mStart_4() const { return ___mStart_4; }
	inline Quaternion_t4030073918 * get_address_of_mStart_4() { return &___mStart_4; }
	inline void set_mStart_4(Quaternion_t4030073918  value)
	{
		___mStart_4 = value;
	}

	inline static int32_t get_offset_of_mRot_5() { return static_cast<int32_t>(offsetof(TiltWindow_t1839185375, ___mRot_5)); }
	inline Vector2_t2243707579  get_mRot_5() const { return ___mRot_5; }
	inline Vector2_t2243707579 * get_address_of_mRot_5() { return &___mRot_5; }
	inline void set_mRot_5(Vector2_t2243707579  value)
	{
		___mRot_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TILTWINDOW_T1839185375_H
#ifndef COLORIMAGE_T3157136356_H
#define COLORIMAGE_T3157136356_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorImage
struct  ColorImage_t3157136356  : public MonoBehaviour_t1158329972
{
public:
	// ColorPicker ColorImage::picker
	ColorPicker_t3035206225 * ___picker_2;
	// UnityEngine.UI.Image ColorImage::image
	Image_t2042527209 * ___image_3;

public:
	inline static int32_t get_offset_of_picker_2() { return static_cast<int32_t>(offsetof(ColorImage_t3157136356, ___picker_2)); }
	inline ColorPicker_t3035206225 * get_picker_2() const { return ___picker_2; }
	inline ColorPicker_t3035206225 ** get_address_of_picker_2() { return &___picker_2; }
	inline void set_picker_2(ColorPicker_t3035206225 * value)
	{
		___picker_2 = value;
		Il2CppCodeGenWriteBarrier((&___picker_2), value);
	}

	inline static int32_t get_offset_of_image_3() { return static_cast<int32_t>(offsetof(ColorImage_t3157136356, ___image_3)); }
	inline Image_t2042527209 * get_image_3() const { return ___image_3; }
	inline Image_t2042527209 ** get_address_of_image_3() { return &___image_3; }
	inline void set_image_3(Image_t2042527209 * value)
	{
		___image_3 = value;
		Il2CppCodeGenWriteBarrier((&___image_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORIMAGE_T3157136356_H
#ifndef WEBCAMRECOGNITION_T2299961224_H
#define WEBCAMRECOGNITION_T2299961224_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebCamRecognition
struct  WebCamRecognition_t2299961224  : public MonoBehaviour_t1158329972
{
public:
	// IBM.Watson.DeveloperCloud.Widgets.WebCamWidget WebCamRecognition::m_WebCamWidget
	WebCamWidget_t58556181 * ___m_WebCamWidget_2;
	// IBM.Watson.DeveloperCloud.Widgets.WebCamDisplayWidget WebCamRecognition::m_WebCamDisplayWidget
	WebCamDisplayWidget_t3233697851 * ___m_WebCamDisplayWidget_3;
	// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition WebCamRecognition::m_VisualRecognition
	VisualRecognition_t3119563755 * ___m_VisualRecognition_4;

public:
	inline static int32_t get_offset_of_m_WebCamWidget_2() { return static_cast<int32_t>(offsetof(WebCamRecognition_t2299961224, ___m_WebCamWidget_2)); }
	inline WebCamWidget_t58556181 * get_m_WebCamWidget_2() const { return ___m_WebCamWidget_2; }
	inline WebCamWidget_t58556181 ** get_address_of_m_WebCamWidget_2() { return &___m_WebCamWidget_2; }
	inline void set_m_WebCamWidget_2(WebCamWidget_t58556181 * value)
	{
		___m_WebCamWidget_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_WebCamWidget_2), value);
	}

	inline static int32_t get_offset_of_m_WebCamDisplayWidget_3() { return static_cast<int32_t>(offsetof(WebCamRecognition_t2299961224, ___m_WebCamDisplayWidget_3)); }
	inline WebCamDisplayWidget_t3233697851 * get_m_WebCamDisplayWidget_3() const { return ___m_WebCamDisplayWidget_3; }
	inline WebCamDisplayWidget_t3233697851 ** get_address_of_m_WebCamDisplayWidget_3() { return &___m_WebCamDisplayWidget_3; }
	inline void set_m_WebCamDisplayWidget_3(WebCamDisplayWidget_t3233697851 * value)
	{
		___m_WebCamDisplayWidget_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_WebCamDisplayWidget_3), value);
	}

	inline static int32_t get_offset_of_m_VisualRecognition_4() { return static_cast<int32_t>(offsetof(WebCamRecognition_t2299961224, ___m_VisualRecognition_4)); }
	inline VisualRecognition_t3119563755 * get_m_VisualRecognition_4() const { return ___m_VisualRecognition_4; }
	inline VisualRecognition_t3119563755 ** get_address_of_m_VisualRecognition_4() { return &___m_VisualRecognition_4; }
	inline void set_m_VisualRecognition_4(VisualRecognition_t3119563755 * value)
	{
		___m_VisualRecognition_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_VisualRecognition_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBCAMRECOGNITION_T2299961224_H
#ifndef EXAMPLELANGUAGETRANSLATION_T4280728515_H
#define EXAMPLELANGUAGETRANSLATION_T4280728515_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExampleLanguageTranslation
struct  ExampleLanguageTranslation_t4280728515  : public MonoBehaviour_t1158329972
{
public:
	// IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.LanguageTranslation ExampleLanguageTranslation::m_Translate
	LanguageTranslation_t3099415401 * ___m_Translate_2;
	// System.String ExampleLanguageTranslation::m_PharseToTranslate
	String_t* ___m_PharseToTranslate_3;

public:
	inline static int32_t get_offset_of_m_Translate_2() { return static_cast<int32_t>(offsetof(ExampleLanguageTranslation_t4280728515, ___m_Translate_2)); }
	inline LanguageTranslation_t3099415401 * get_m_Translate_2() const { return ___m_Translate_2; }
	inline LanguageTranslation_t3099415401 ** get_address_of_m_Translate_2() { return &___m_Translate_2; }
	inline void set_m_Translate_2(LanguageTranslation_t3099415401 * value)
	{
		___m_Translate_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Translate_2), value);
	}

	inline static int32_t get_offset_of_m_PharseToTranslate_3() { return static_cast<int32_t>(offsetof(ExampleLanguageTranslation_t4280728515, ___m_PharseToTranslate_3)); }
	inline String_t* get_m_PharseToTranslate_3() const { return ___m_PharseToTranslate_3; }
	inline String_t** get_address_of_m_PharseToTranslate_3() { return &___m_PharseToTranslate_3; }
	inline void set_m_PharseToTranslate_3(String_t* value)
	{
		___m_PharseToTranslate_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_PharseToTranslate_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXAMPLELANGUAGETRANSLATION_T4280728515_H
#ifndef EXAMPLEVISUALRECOGNITION_T1110120389_H
#define EXAMPLEVISUALRECOGNITION_T1110120389_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExampleVisualRecognition
struct  ExampleVisualRecognition_t1110120389  : public MonoBehaviour_t1158329972
{
public:
	// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition ExampleVisualRecognition::m_VisualRecognition
	VisualRecognition_t3119563755 * ___m_VisualRecognition_2;
	// System.String ExampleVisualRecognition::m_classifierName
	String_t* ___m_classifierName_3;
	// System.String ExampleVisualRecognition::m_classifierID
	String_t* ___m_classifierID_4;
	// System.String ExampleVisualRecognition::m_classifierToDelete
	String_t* ___m_classifierToDelete_5;
	// System.String ExampleVisualRecognition::m_imageURL
	String_t* ___m_imageURL_6;
	// System.String ExampleVisualRecognition::m_imageTextURL
	String_t* ___m_imageTextURL_7;

public:
	inline static int32_t get_offset_of_m_VisualRecognition_2() { return static_cast<int32_t>(offsetof(ExampleVisualRecognition_t1110120389, ___m_VisualRecognition_2)); }
	inline VisualRecognition_t3119563755 * get_m_VisualRecognition_2() const { return ___m_VisualRecognition_2; }
	inline VisualRecognition_t3119563755 ** get_address_of_m_VisualRecognition_2() { return &___m_VisualRecognition_2; }
	inline void set_m_VisualRecognition_2(VisualRecognition_t3119563755 * value)
	{
		___m_VisualRecognition_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_VisualRecognition_2), value);
	}

	inline static int32_t get_offset_of_m_classifierName_3() { return static_cast<int32_t>(offsetof(ExampleVisualRecognition_t1110120389, ___m_classifierName_3)); }
	inline String_t* get_m_classifierName_3() const { return ___m_classifierName_3; }
	inline String_t** get_address_of_m_classifierName_3() { return &___m_classifierName_3; }
	inline void set_m_classifierName_3(String_t* value)
	{
		___m_classifierName_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_classifierName_3), value);
	}

	inline static int32_t get_offset_of_m_classifierID_4() { return static_cast<int32_t>(offsetof(ExampleVisualRecognition_t1110120389, ___m_classifierID_4)); }
	inline String_t* get_m_classifierID_4() const { return ___m_classifierID_4; }
	inline String_t** get_address_of_m_classifierID_4() { return &___m_classifierID_4; }
	inline void set_m_classifierID_4(String_t* value)
	{
		___m_classifierID_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_classifierID_4), value);
	}

	inline static int32_t get_offset_of_m_classifierToDelete_5() { return static_cast<int32_t>(offsetof(ExampleVisualRecognition_t1110120389, ___m_classifierToDelete_5)); }
	inline String_t* get_m_classifierToDelete_5() const { return ___m_classifierToDelete_5; }
	inline String_t** get_address_of_m_classifierToDelete_5() { return &___m_classifierToDelete_5; }
	inline void set_m_classifierToDelete_5(String_t* value)
	{
		___m_classifierToDelete_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_classifierToDelete_5), value);
	}

	inline static int32_t get_offset_of_m_imageURL_6() { return static_cast<int32_t>(offsetof(ExampleVisualRecognition_t1110120389, ___m_imageURL_6)); }
	inline String_t* get_m_imageURL_6() const { return ___m_imageURL_6; }
	inline String_t** get_address_of_m_imageURL_6() { return &___m_imageURL_6; }
	inline void set_m_imageURL_6(String_t* value)
	{
		___m_imageURL_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_imageURL_6), value);
	}

	inline static int32_t get_offset_of_m_imageTextURL_7() { return static_cast<int32_t>(offsetof(ExampleVisualRecognition_t1110120389, ___m_imageTextURL_7)); }
	inline String_t* get_m_imageTextURL_7() const { return ___m_imageTextURL_7; }
	inline String_t** get_address_of_m_imageTextURL_7() { return &___m_imageTextURL_7; }
	inline void set_m_imageTextURL_7(String_t* value)
	{
		___m_imageTextURL_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_imageTextURL_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXAMPLEVISUALRECOGNITION_T1110120389_H
#ifndef REVERSENORMALS_T3342799322_H
#define REVERSENORMALS_T3342799322_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReverseNormals
struct  ReverseNormals_t3342799322  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REVERSENORMALS_T3342799322_H
#ifndef COLORLABEL_T1884607337_H
#define COLORLABEL_T1884607337_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorLabel
struct  ColorLabel_t1884607337  : public MonoBehaviour_t1158329972
{
public:
	// ColorPicker ColorLabel::picker
	ColorPicker_t3035206225 * ___picker_2;
	// ColorValues ColorLabel::type
	int32_t ___type_3;
	// System.String ColorLabel::prefix
	String_t* ___prefix_4;
	// System.Single ColorLabel::minValue
	float ___minValue_5;
	// System.Single ColorLabel::maxValue
	float ___maxValue_6;
	// System.Int32 ColorLabel::precision
	int32_t ___precision_7;
	// UnityEngine.UI.Text ColorLabel::label
	Text_t356221433 * ___label_8;

public:
	inline static int32_t get_offset_of_picker_2() { return static_cast<int32_t>(offsetof(ColorLabel_t1884607337, ___picker_2)); }
	inline ColorPicker_t3035206225 * get_picker_2() const { return ___picker_2; }
	inline ColorPicker_t3035206225 ** get_address_of_picker_2() { return &___picker_2; }
	inline void set_picker_2(ColorPicker_t3035206225 * value)
	{
		___picker_2 = value;
		Il2CppCodeGenWriteBarrier((&___picker_2), value);
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(ColorLabel_t1884607337, ___type_3)); }
	inline int32_t get_type_3() const { return ___type_3; }
	inline int32_t* get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(int32_t value)
	{
		___type_3 = value;
	}

	inline static int32_t get_offset_of_prefix_4() { return static_cast<int32_t>(offsetof(ColorLabel_t1884607337, ___prefix_4)); }
	inline String_t* get_prefix_4() const { return ___prefix_4; }
	inline String_t** get_address_of_prefix_4() { return &___prefix_4; }
	inline void set_prefix_4(String_t* value)
	{
		___prefix_4 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_4), value);
	}

	inline static int32_t get_offset_of_minValue_5() { return static_cast<int32_t>(offsetof(ColorLabel_t1884607337, ___minValue_5)); }
	inline float get_minValue_5() const { return ___minValue_5; }
	inline float* get_address_of_minValue_5() { return &___minValue_5; }
	inline void set_minValue_5(float value)
	{
		___minValue_5 = value;
	}

	inline static int32_t get_offset_of_maxValue_6() { return static_cast<int32_t>(offsetof(ColorLabel_t1884607337, ___maxValue_6)); }
	inline float get_maxValue_6() const { return ___maxValue_6; }
	inline float* get_address_of_maxValue_6() { return &___maxValue_6; }
	inline void set_maxValue_6(float value)
	{
		___maxValue_6 = value;
	}

	inline static int32_t get_offset_of_precision_7() { return static_cast<int32_t>(offsetof(ColorLabel_t1884607337, ___precision_7)); }
	inline int32_t get_precision_7() const { return ___precision_7; }
	inline int32_t* get_address_of_precision_7() { return &___precision_7; }
	inline void set_precision_7(int32_t value)
	{
		___precision_7 = value;
	}

	inline static int32_t get_offset_of_label_8() { return static_cast<int32_t>(offsetof(ColorLabel_t1884607337, ___label_8)); }
	inline Text_t356221433 * get_label_8() const { return ___label_8; }
	inline Text_t356221433 ** get_address_of_label_8() { return &___label_8; }
	inline void set_label_8(Text_t356221433 * value)
	{
		___label_8 = value;
		Il2CppCodeGenWriteBarrier((&___label_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORLABEL_T1884607337_H
#ifndef EXAMPLEALCHEMYDATANEWS_T4207199522_H
#define EXAMPLEALCHEMYDATANEWS_T4207199522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExampleAlchemyDataNews
struct  ExampleAlchemyDataNews_t4207199522  : public MonoBehaviour_t1158329972
{
public:
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI ExampleAlchemyDataNews::m_AlchemyAPI
	AlchemyAPI_t2955839919 * ___m_AlchemyAPI_2;

public:
	inline static int32_t get_offset_of_m_AlchemyAPI_2() { return static_cast<int32_t>(offsetof(ExampleAlchemyDataNews_t4207199522, ___m_AlchemyAPI_2)); }
	inline AlchemyAPI_t2955839919 * get_m_AlchemyAPI_2() const { return ___m_AlchemyAPI_2; }
	inline AlchemyAPI_t2955839919 ** get_address_of_m_AlchemyAPI_2() { return &___m_AlchemyAPI_2; }
	inline void set_m_AlchemyAPI_2(AlchemyAPI_t2955839919 * value)
	{
		___m_AlchemyAPI_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_AlchemyAPI_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXAMPLEALCHEMYDATANEWS_T4207199522_H
#ifndef EXAMPLECONVERSATION_T1371389615_H
#define EXAMPLECONVERSATION_T1371389615_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExampleConversation
struct  ExampleConversation_t1371389615  : public MonoBehaviour_t1158329972
{
public:
	// IBM.Watson.DeveloperCloud.Services.Conversation.v1.Conversation ExampleConversation::m_Conversation
	Conversation_t105466997 * ___m_Conversation_2;
	// System.String ExampleConversation::m_WorkspaceID
	String_t* ___m_WorkspaceID_3;
	// System.Boolean ExampleConversation::m_UseAlternateIntents
	bool ___m_UseAlternateIntents_4;
	// System.String[] ExampleConversation::questionArray
	StringU5BU5D_t1642385972* ___questionArray_5;

public:
	inline static int32_t get_offset_of_m_Conversation_2() { return static_cast<int32_t>(offsetof(ExampleConversation_t1371389615, ___m_Conversation_2)); }
	inline Conversation_t105466997 * get_m_Conversation_2() const { return ___m_Conversation_2; }
	inline Conversation_t105466997 ** get_address_of_m_Conversation_2() { return &___m_Conversation_2; }
	inline void set_m_Conversation_2(Conversation_t105466997 * value)
	{
		___m_Conversation_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Conversation_2), value);
	}

	inline static int32_t get_offset_of_m_WorkspaceID_3() { return static_cast<int32_t>(offsetof(ExampleConversation_t1371389615, ___m_WorkspaceID_3)); }
	inline String_t* get_m_WorkspaceID_3() const { return ___m_WorkspaceID_3; }
	inline String_t** get_address_of_m_WorkspaceID_3() { return &___m_WorkspaceID_3; }
	inline void set_m_WorkspaceID_3(String_t* value)
	{
		___m_WorkspaceID_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_WorkspaceID_3), value);
	}

	inline static int32_t get_offset_of_m_UseAlternateIntents_4() { return static_cast<int32_t>(offsetof(ExampleConversation_t1371389615, ___m_UseAlternateIntents_4)); }
	inline bool get_m_UseAlternateIntents_4() const { return ___m_UseAlternateIntents_4; }
	inline bool* get_address_of_m_UseAlternateIntents_4() { return &___m_UseAlternateIntents_4; }
	inline void set_m_UseAlternateIntents_4(bool value)
	{
		___m_UseAlternateIntents_4 = value;
	}

	inline static int32_t get_offset_of_questionArray_5() { return static_cast<int32_t>(offsetof(ExampleConversation_t1371389615, ___questionArray_5)); }
	inline StringU5BU5D_t1642385972* get_questionArray_5() const { return ___questionArray_5; }
	inline StringU5BU5D_t1642385972** get_address_of_questionArray_5() { return &___questionArray_5; }
	inline void set_questionArray_5(StringU5BU5D_t1642385972* value)
	{
		___questionArray_5 = value;
		Il2CppCodeGenWriteBarrier((&___questionArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXAMPLECONVERSATION_T1371389615_H
#ifndef EXAMPLEALCHEMYLANGUAGE_T1060437047_H
#define EXAMPLEALCHEMYLANGUAGE_T1060437047_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExampleAlchemyLanguage
struct  ExampleAlchemyLanguage_t1060437047  : public MonoBehaviour_t1158329972
{
public:
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI ExampleAlchemyLanguage::m_AlchemyAPI
	AlchemyAPI_t2955839919 * ___m_AlchemyAPI_2;
	// System.String ExampleAlchemyLanguage::m_ExampleURL_unitySDK
	String_t* ___m_ExampleURL_unitySDK_3;
	// System.String ExampleAlchemyLanguage::m_ExampleURL_watsonJeopardy
	String_t* ___m_ExampleURL_watsonJeopardy_4;
	// System.String ExampleAlchemyLanguage::m_ExampleURL_microformats
	String_t* ___m_ExampleURL_microformats_5;
	// System.String ExampleAlchemyLanguage::m_ExampleText_unitySDK
	String_t* ___m_ExampleText_unitySDK_6;
	// System.String ExampleAlchemyLanguage::m_ExampleText_watsonJeopardy
	String_t* ___m_ExampleText_watsonJeopardy_7;

public:
	inline static int32_t get_offset_of_m_AlchemyAPI_2() { return static_cast<int32_t>(offsetof(ExampleAlchemyLanguage_t1060437047, ___m_AlchemyAPI_2)); }
	inline AlchemyAPI_t2955839919 * get_m_AlchemyAPI_2() const { return ___m_AlchemyAPI_2; }
	inline AlchemyAPI_t2955839919 ** get_address_of_m_AlchemyAPI_2() { return &___m_AlchemyAPI_2; }
	inline void set_m_AlchemyAPI_2(AlchemyAPI_t2955839919 * value)
	{
		___m_AlchemyAPI_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_AlchemyAPI_2), value);
	}

	inline static int32_t get_offset_of_m_ExampleURL_unitySDK_3() { return static_cast<int32_t>(offsetof(ExampleAlchemyLanguage_t1060437047, ___m_ExampleURL_unitySDK_3)); }
	inline String_t* get_m_ExampleURL_unitySDK_3() const { return ___m_ExampleURL_unitySDK_3; }
	inline String_t** get_address_of_m_ExampleURL_unitySDK_3() { return &___m_ExampleURL_unitySDK_3; }
	inline void set_m_ExampleURL_unitySDK_3(String_t* value)
	{
		___m_ExampleURL_unitySDK_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_ExampleURL_unitySDK_3), value);
	}

	inline static int32_t get_offset_of_m_ExampleURL_watsonJeopardy_4() { return static_cast<int32_t>(offsetof(ExampleAlchemyLanguage_t1060437047, ___m_ExampleURL_watsonJeopardy_4)); }
	inline String_t* get_m_ExampleURL_watsonJeopardy_4() const { return ___m_ExampleURL_watsonJeopardy_4; }
	inline String_t** get_address_of_m_ExampleURL_watsonJeopardy_4() { return &___m_ExampleURL_watsonJeopardy_4; }
	inline void set_m_ExampleURL_watsonJeopardy_4(String_t* value)
	{
		___m_ExampleURL_watsonJeopardy_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_ExampleURL_watsonJeopardy_4), value);
	}

	inline static int32_t get_offset_of_m_ExampleURL_microformats_5() { return static_cast<int32_t>(offsetof(ExampleAlchemyLanguage_t1060437047, ___m_ExampleURL_microformats_5)); }
	inline String_t* get_m_ExampleURL_microformats_5() const { return ___m_ExampleURL_microformats_5; }
	inline String_t** get_address_of_m_ExampleURL_microformats_5() { return &___m_ExampleURL_microformats_5; }
	inline void set_m_ExampleURL_microformats_5(String_t* value)
	{
		___m_ExampleURL_microformats_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_ExampleURL_microformats_5), value);
	}

	inline static int32_t get_offset_of_m_ExampleText_unitySDK_6() { return static_cast<int32_t>(offsetof(ExampleAlchemyLanguage_t1060437047, ___m_ExampleText_unitySDK_6)); }
	inline String_t* get_m_ExampleText_unitySDK_6() const { return ___m_ExampleText_unitySDK_6; }
	inline String_t** get_address_of_m_ExampleText_unitySDK_6() { return &___m_ExampleText_unitySDK_6; }
	inline void set_m_ExampleText_unitySDK_6(String_t* value)
	{
		___m_ExampleText_unitySDK_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_ExampleText_unitySDK_6), value);
	}

	inline static int32_t get_offset_of_m_ExampleText_watsonJeopardy_7() { return static_cast<int32_t>(offsetof(ExampleAlchemyLanguage_t1060437047, ___m_ExampleText_watsonJeopardy_7)); }
	inline String_t* get_m_ExampleText_watsonJeopardy_7() const { return ___m_ExampleText_watsonJeopardy_7; }
	inline String_t** get_address_of_m_ExampleText_watsonJeopardy_7() { return &___m_ExampleText_watsonJeopardy_7; }
	inline void set_m_ExampleText_watsonJeopardy_7(String_t* value)
	{
		___m_ExampleText_watsonJeopardy_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_ExampleText_watsonJeopardy_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXAMPLEALCHEMYLANGUAGE_T1060437047_H
#ifndef COLORSLIDER_T2729134766_H
#define COLORSLIDER_T2729134766_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorSlider
struct  ColorSlider_t2729134766  : public MonoBehaviour_t1158329972
{
public:
	// ColorPicker ColorSlider::hsvpicker
	ColorPicker_t3035206225 * ___hsvpicker_2;
	// ColorValues ColorSlider::type
	int32_t ___type_3;
	// UnityEngine.UI.Slider ColorSlider::slider
	Slider_t297367283 * ___slider_4;
	// System.Boolean ColorSlider::listen
	bool ___listen_5;

public:
	inline static int32_t get_offset_of_hsvpicker_2() { return static_cast<int32_t>(offsetof(ColorSlider_t2729134766, ___hsvpicker_2)); }
	inline ColorPicker_t3035206225 * get_hsvpicker_2() const { return ___hsvpicker_2; }
	inline ColorPicker_t3035206225 ** get_address_of_hsvpicker_2() { return &___hsvpicker_2; }
	inline void set_hsvpicker_2(ColorPicker_t3035206225 * value)
	{
		___hsvpicker_2 = value;
		Il2CppCodeGenWriteBarrier((&___hsvpicker_2), value);
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(ColorSlider_t2729134766, ___type_3)); }
	inline int32_t get_type_3() const { return ___type_3; }
	inline int32_t* get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(int32_t value)
	{
		___type_3 = value;
	}

	inline static int32_t get_offset_of_slider_4() { return static_cast<int32_t>(offsetof(ColorSlider_t2729134766, ___slider_4)); }
	inline Slider_t297367283 * get_slider_4() const { return ___slider_4; }
	inline Slider_t297367283 ** get_address_of_slider_4() { return &___slider_4; }
	inline void set_slider_4(Slider_t297367283 * value)
	{
		___slider_4 = value;
		Il2CppCodeGenWriteBarrier((&___slider_4), value);
	}

	inline static int32_t get_offset_of_listen_5() { return static_cast<int32_t>(offsetof(ColorSlider_t2729134766, ___listen_5)); }
	inline bool get_listen_5() const { return ___listen_5; }
	inline bool* get_address_of_listen_5() { return &___listen_5; }
	inline void set_listen_5(bool value)
	{
		___listen_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORSLIDER_T2729134766_H
#ifndef COLORPICKER_T3035206225_H
#define COLORPICKER_T3035206225_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorPicker
struct  ColorPicker_t3035206225  : public MonoBehaviour_t1158329972
{
public:
	// System.Single ColorPicker::_hue
	float ____hue_2;
	// System.Single ColorPicker::_saturation
	float ____saturation_3;
	// System.Single ColorPicker::_brightness
	float ____brightness_4;
	// System.Single ColorPicker::_red
	float ____red_5;
	// System.Single ColorPicker::_green
	float ____green_6;
	// System.Single ColorPicker::_blue
	float ____blue_7;
	// System.Single ColorPicker::_alpha
	float ____alpha_8;
	// ColorChangedEvent ColorPicker::onValueChanged
	ColorChangedEvent_t2990895397 * ___onValueChanged_9;
	// HSVChangedEvent ColorPicker::onHSVChanged
	HSVChangedEvent_t1170297569 * ___onHSVChanged_10;

public:
	inline static int32_t get_offset_of__hue_2() { return static_cast<int32_t>(offsetof(ColorPicker_t3035206225, ____hue_2)); }
	inline float get__hue_2() const { return ____hue_2; }
	inline float* get_address_of__hue_2() { return &____hue_2; }
	inline void set__hue_2(float value)
	{
		____hue_2 = value;
	}

	inline static int32_t get_offset_of__saturation_3() { return static_cast<int32_t>(offsetof(ColorPicker_t3035206225, ____saturation_3)); }
	inline float get__saturation_3() const { return ____saturation_3; }
	inline float* get_address_of__saturation_3() { return &____saturation_3; }
	inline void set__saturation_3(float value)
	{
		____saturation_3 = value;
	}

	inline static int32_t get_offset_of__brightness_4() { return static_cast<int32_t>(offsetof(ColorPicker_t3035206225, ____brightness_4)); }
	inline float get__brightness_4() const { return ____brightness_4; }
	inline float* get_address_of__brightness_4() { return &____brightness_4; }
	inline void set__brightness_4(float value)
	{
		____brightness_4 = value;
	}

	inline static int32_t get_offset_of__red_5() { return static_cast<int32_t>(offsetof(ColorPicker_t3035206225, ____red_5)); }
	inline float get__red_5() const { return ____red_5; }
	inline float* get_address_of__red_5() { return &____red_5; }
	inline void set__red_5(float value)
	{
		____red_5 = value;
	}

	inline static int32_t get_offset_of__green_6() { return static_cast<int32_t>(offsetof(ColorPicker_t3035206225, ____green_6)); }
	inline float get__green_6() const { return ____green_6; }
	inline float* get_address_of__green_6() { return &____green_6; }
	inline void set__green_6(float value)
	{
		____green_6 = value;
	}

	inline static int32_t get_offset_of__blue_7() { return static_cast<int32_t>(offsetof(ColorPicker_t3035206225, ____blue_7)); }
	inline float get__blue_7() const { return ____blue_7; }
	inline float* get_address_of__blue_7() { return &____blue_7; }
	inline void set__blue_7(float value)
	{
		____blue_7 = value;
	}

	inline static int32_t get_offset_of__alpha_8() { return static_cast<int32_t>(offsetof(ColorPicker_t3035206225, ____alpha_8)); }
	inline float get__alpha_8() const { return ____alpha_8; }
	inline float* get_address_of__alpha_8() { return &____alpha_8; }
	inline void set__alpha_8(float value)
	{
		____alpha_8 = value;
	}

	inline static int32_t get_offset_of_onValueChanged_9() { return static_cast<int32_t>(offsetof(ColorPicker_t3035206225, ___onValueChanged_9)); }
	inline ColorChangedEvent_t2990895397 * get_onValueChanged_9() const { return ___onValueChanged_9; }
	inline ColorChangedEvent_t2990895397 ** get_address_of_onValueChanged_9() { return &___onValueChanged_9; }
	inline void set_onValueChanged_9(ColorChangedEvent_t2990895397 * value)
	{
		___onValueChanged_9 = value;
		Il2CppCodeGenWriteBarrier((&___onValueChanged_9), value);
	}

	inline static int32_t get_offset_of_onHSVChanged_10() { return static_cast<int32_t>(offsetof(ColorPicker_t3035206225, ___onHSVChanged_10)); }
	inline HSVChangedEvent_t1170297569 * get_onHSVChanged_10() const { return ___onHSVChanged_10; }
	inline HSVChangedEvent_t1170297569 ** get_address_of_onHSVChanged_10() { return &___onHSVChanged_10; }
	inline void set_onHSVChanged_10(HSVChangedEvent_t1170297569 * value)
	{
		___onHSVChanged_10 = value;
		Il2CppCodeGenWriteBarrier((&___onHSVChanged_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORPICKER_T3035206225_H
#ifndef COLORPRESETS_T4120623669_H
#define COLORPRESETS_T4120623669_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorPresets
struct  ColorPresets_t4120623669  : public MonoBehaviour_t1158329972
{
public:
	// ColorPicker ColorPresets::picker
	ColorPicker_t3035206225 * ___picker_2;
	// UnityEngine.GameObject[] ColorPresets::presets
	GameObjectU5BU5D_t3057952154* ___presets_3;
	// UnityEngine.UI.Image ColorPresets::createPresetImage
	Image_t2042527209 * ___createPresetImage_4;

public:
	inline static int32_t get_offset_of_picker_2() { return static_cast<int32_t>(offsetof(ColorPresets_t4120623669, ___picker_2)); }
	inline ColorPicker_t3035206225 * get_picker_2() const { return ___picker_2; }
	inline ColorPicker_t3035206225 ** get_address_of_picker_2() { return &___picker_2; }
	inline void set_picker_2(ColorPicker_t3035206225 * value)
	{
		___picker_2 = value;
		Il2CppCodeGenWriteBarrier((&___picker_2), value);
	}

	inline static int32_t get_offset_of_presets_3() { return static_cast<int32_t>(offsetof(ColorPresets_t4120623669, ___presets_3)); }
	inline GameObjectU5BU5D_t3057952154* get_presets_3() const { return ___presets_3; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_presets_3() { return &___presets_3; }
	inline void set_presets_3(GameObjectU5BU5D_t3057952154* value)
	{
		___presets_3 = value;
		Il2CppCodeGenWriteBarrier((&___presets_3), value);
	}

	inline static int32_t get_offset_of_createPresetImage_4() { return static_cast<int32_t>(offsetof(ColorPresets_t4120623669, ___createPresetImage_4)); }
	inline Image_t2042527209 * get_createPresetImage_4() const { return ___createPresetImage_4; }
	inline Image_t2042527209 ** get_address_of_createPresetImage_4() { return &___createPresetImage_4; }
	inline void set_createPresetImage_4(Image_t2042527209 * value)
	{
		___createPresetImage_4 = value;
		Il2CppCodeGenWriteBarrier((&___createPresetImage_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORPRESETS_T4120623669_H
#ifndef SELECTABLE_T1490392188_H
#define SELECTABLE_T1490392188_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable
struct  Selectable_t1490392188  : public UIBehaviour_t3960014691
{
public:
	// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::m_Navigation
	Navigation_t1571958496  ___m_Navigation_3;
	// UnityEngine.UI.Selectable/Transition UnityEngine.UI.Selectable::m_Transition
	int32_t ___m_Transition_4;
	// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::m_Colors
	ColorBlock_t2652774230  ___m_Colors_5;
	// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::m_SpriteState
	SpriteState_t1353336012  ___m_SpriteState_6;
	// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::m_AnimationTriggers
	AnimationTriggers_t3244928895 * ___m_AnimationTriggers_7;
	// System.Boolean UnityEngine.UI.Selectable::m_Interactable
	bool ___m_Interactable_8;
	// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::m_TargetGraphic
	Graphic_t2426225576 * ___m_TargetGraphic_9;
	// System.Boolean UnityEngine.UI.Selectable::m_GroupsAllowInteraction
	bool ___m_GroupsAllowInteraction_10;
	// UnityEngine.UI.Selectable/SelectionState UnityEngine.UI.Selectable::m_CurrentSelectionState
	int32_t ___m_CurrentSelectionState_11;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerInside>k__BackingField
	bool ___U3CisPointerInsideU3Ek__BackingField_12;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerDown>k__BackingField
	bool ___U3CisPointerDownU3Ek__BackingField_13;
	// System.Boolean UnityEngine.UI.Selectable::<hasSelection>k__BackingField
	bool ___U3ChasSelectionU3Ek__BackingField_14;
	// System.Collections.Generic.List`1<UnityEngine.CanvasGroup> UnityEngine.UI.Selectable::m_CanvasGroupCache
	List_1_t2665681875 * ___m_CanvasGroupCache_15;

public:
	inline static int32_t get_offset_of_m_Navigation_3() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___m_Navigation_3)); }
	inline Navigation_t1571958496  get_m_Navigation_3() const { return ___m_Navigation_3; }
	inline Navigation_t1571958496 * get_address_of_m_Navigation_3() { return &___m_Navigation_3; }
	inline void set_m_Navigation_3(Navigation_t1571958496  value)
	{
		___m_Navigation_3 = value;
	}

	inline static int32_t get_offset_of_m_Transition_4() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___m_Transition_4)); }
	inline int32_t get_m_Transition_4() const { return ___m_Transition_4; }
	inline int32_t* get_address_of_m_Transition_4() { return &___m_Transition_4; }
	inline void set_m_Transition_4(int32_t value)
	{
		___m_Transition_4 = value;
	}

	inline static int32_t get_offset_of_m_Colors_5() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___m_Colors_5)); }
	inline ColorBlock_t2652774230  get_m_Colors_5() const { return ___m_Colors_5; }
	inline ColorBlock_t2652774230 * get_address_of_m_Colors_5() { return &___m_Colors_5; }
	inline void set_m_Colors_5(ColorBlock_t2652774230  value)
	{
		___m_Colors_5 = value;
	}

	inline static int32_t get_offset_of_m_SpriteState_6() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___m_SpriteState_6)); }
	inline SpriteState_t1353336012  get_m_SpriteState_6() const { return ___m_SpriteState_6; }
	inline SpriteState_t1353336012 * get_address_of_m_SpriteState_6() { return &___m_SpriteState_6; }
	inline void set_m_SpriteState_6(SpriteState_t1353336012  value)
	{
		___m_SpriteState_6 = value;
	}

	inline static int32_t get_offset_of_m_AnimationTriggers_7() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___m_AnimationTriggers_7)); }
	inline AnimationTriggers_t3244928895 * get_m_AnimationTriggers_7() const { return ___m_AnimationTriggers_7; }
	inline AnimationTriggers_t3244928895 ** get_address_of_m_AnimationTriggers_7() { return &___m_AnimationTriggers_7; }
	inline void set_m_AnimationTriggers_7(AnimationTriggers_t3244928895 * value)
	{
		___m_AnimationTriggers_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_AnimationTriggers_7), value);
	}

	inline static int32_t get_offset_of_m_Interactable_8() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___m_Interactable_8)); }
	inline bool get_m_Interactable_8() const { return ___m_Interactable_8; }
	inline bool* get_address_of_m_Interactable_8() { return &___m_Interactable_8; }
	inline void set_m_Interactable_8(bool value)
	{
		___m_Interactable_8 = value;
	}

	inline static int32_t get_offset_of_m_TargetGraphic_9() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___m_TargetGraphic_9)); }
	inline Graphic_t2426225576 * get_m_TargetGraphic_9() const { return ___m_TargetGraphic_9; }
	inline Graphic_t2426225576 ** get_address_of_m_TargetGraphic_9() { return &___m_TargetGraphic_9; }
	inline void set_m_TargetGraphic_9(Graphic_t2426225576 * value)
	{
		___m_TargetGraphic_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_TargetGraphic_9), value);
	}

	inline static int32_t get_offset_of_m_GroupsAllowInteraction_10() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___m_GroupsAllowInteraction_10)); }
	inline bool get_m_GroupsAllowInteraction_10() const { return ___m_GroupsAllowInteraction_10; }
	inline bool* get_address_of_m_GroupsAllowInteraction_10() { return &___m_GroupsAllowInteraction_10; }
	inline void set_m_GroupsAllowInteraction_10(bool value)
	{
		___m_GroupsAllowInteraction_10 = value;
	}

	inline static int32_t get_offset_of_m_CurrentSelectionState_11() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___m_CurrentSelectionState_11)); }
	inline int32_t get_m_CurrentSelectionState_11() const { return ___m_CurrentSelectionState_11; }
	inline int32_t* get_address_of_m_CurrentSelectionState_11() { return &___m_CurrentSelectionState_11; }
	inline void set_m_CurrentSelectionState_11(int32_t value)
	{
		___m_CurrentSelectionState_11 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerInsideU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___U3CisPointerInsideU3Ek__BackingField_12)); }
	inline bool get_U3CisPointerInsideU3Ek__BackingField_12() const { return ___U3CisPointerInsideU3Ek__BackingField_12; }
	inline bool* get_address_of_U3CisPointerInsideU3Ek__BackingField_12() { return &___U3CisPointerInsideU3Ek__BackingField_12; }
	inline void set_U3CisPointerInsideU3Ek__BackingField_12(bool value)
	{
		___U3CisPointerInsideU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerDownU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___U3CisPointerDownU3Ek__BackingField_13)); }
	inline bool get_U3CisPointerDownU3Ek__BackingField_13() const { return ___U3CisPointerDownU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CisPointerDownU3Ek__BackingField_13() { return &___U3CisPointerDownU3Ek__BackingField_13; }
	inline void set_U3CisPointerDownU3Ek__BackingField_13(bool value)
	{
		___U3CisPointerDownU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3ChasSelectionU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___U3ChasSelectionU3Ek__BackingField_14)); }
	inline bool get_U3ChasSelectionU3Ek__BackingField_14() const { return ___U3ChasSelectionU3Ek__BackingField_14; }
	inline bool* get_address_of_U3ChasSelectionU3Ek__BackingField_14() { return &___U3ChasSelectionU3Ek__BackingField_14; }
	inline void set_U3ChasSelectionU3Ek__BackingField_14(bool value)
	{
		___U3ChasSelectionU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_m_CanvasGroupCache_15() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___m_CanvasGroupCache_15)); }
	inline List_1_t2665681875 * get_m_CanvasGroupCache_15() const { return ___m_CanvasGroupCache_15; }
	inline List_1_t2665681875 ** get_address_of_m_CanvasGroupCache_15() { return &___m_CanvasGroupCache_15; }
	inline void set_m_CanvasGroupCache_15(List_1_t2665681875 * value)
	{
		___m_CanvasGroupCache_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasGroupCache_15), value);
	}
};

struct Selectable_t1490392188_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.UI.Selectable> UnityEngine.UI.Selectable::s_List
	List_1_t859513320 * ___s_List_2;

public:
	inline static int32_t get_offset_of_s_List_2() { return static_cast<int32_t>(offsetof(Selectable_t1490392188_StaticFields, ___s_List_2)); }
	inline List_1_t859513320 * get_s_List_2() const { return ___s_List_2; }
	inline List_1_t859513320 ** get_address_of_s_List_2() { return &___s_List_2; }
	inline void set_s_List_2(List_1_t859513320 * value)
	{
		___s_List_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_List_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTABLE_T1490392188_H
#ifndef BOXSLIDER_T1871650694_H
#define BOXSLIDER_T1871650694_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BoxSlider
struct  BoxSlider_t1871650694  : public Selectable_t1490392188
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.BoxSlider::m_HandleRect
	RectTransform_t3349966182 * ___m_HandleRect_16;
	// System.Single UnityEngine.UI.BoxSlider::m_MinValue
	float ___m_MinValue_17;
	// System.Single UnityEngine.UI.BoxSlider::m_MaxValue
	float ___m_MaxValue_18;
	// System.Boolean UnityEngine.UI.BoxSlider::m_WholeNumbers
	bool ___m_WholeNumbers_19;
	// System.Single UnityEngine.UI.BoxSlider::m_Value
	float ___m_Value_20;
	// System.Single UnityEngine.UI.BoxSlider::m_ValueY
	float ___m_ValueY_21;
	// UnityEngine.UI.BoxSlider/BoxSliderEvent UnityEngine.UI.BoxSlider::m_OnValueChanged
	BoxSliderEvent_t1774115848 * ___m_OnValueChanged_22;
	// UnityEngine.Transform UnityEngine.UI.BoxSlider::m_HandleTransform
	Transform_t3275118058 * ___m_HandleTransform_23;
	// UnityEngine.RectTransform UnityEngine.UI.BoxSlider::m_HandleContainerRect
	RectTransform_t3349966182 * ___m_HandleContainerRect_24;
	// UnityEngine.Vector2 UnityEngine.UI.BoxSlider::m_Offset
	Vector2_t2243707579  ___m_Offset_25;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.BoxSlider::m_Tracker
	DrivenRectTransformTracker_t154385424  ___m_Tracker_26;

public:
	inline static int32_t get_offset_of_m_HandleRect_16() { return static_cast<int32_t>(offsetof(BoxSlider_t1871650694, ___m_HandleRect_16)); }
	inline RectTransform_t3349966182 * get_m_HandleRect_16() const { return ___m_HandleRect_16; }
	inline RectTransform_t3349966182 ** get_address_of_m_HandleRect_16() { return &___m_HandleRect_16; }
	inline void set_m_HandleRect_16(RectTransform_t3349966182 * value)
	{
		___m_HandleRect_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_HandleRect_16), value);
	}

	inline static int32_t get_offset_of_m_MinValue_17() { return static_cast<int32_t>(offsetof(BoxSlider_t1871650694, ___m_MinValue_17)); }
	inline float get_m_MinValue_17() const { return ___m_MinValue_17; }
	inline float* get_address_of_m_MinValue_17() { return &___m_MinValue_17; }
	inline void set_m_MinValue_17(float value)
	{
		___m_MinValue_17 = value;
	}

	inline static int32_t get_offset_of_m_MaxValue_18() { return static_cast<int32_t>(offsetof(BoxSlider_t1871650694, ___m_MaxValue_18)); }
	inline float get_m_MaxValue_18() const { return ___m_MaxValue_18; }
	inline float* get_address_of_m_MaxValue_18() { return &___m_MaxValue_18; }
	inline void set_m_MaxValue_18(float value)
	{
		___m_MaxValue_18 = value;
	}

	inline static int32_t get_offset_of_m_WholeNumbers_19() { return static_cast<int32_t>(offsetof(BoxSlider_t1871650694, ___m_WholeNumbers_19)); }
	inline bool get_m_WholeNumbers_19() const { return ___m_WholeNumbers_19; }
	inline bool* get_address_of_m_WholeNumbers_19() { return &___m_WholeNumbers_19; }
	inline void set_m_WholeNumbers_19(bool value)
	{
		___m_WholeNumbers_19 = value;
	}

	inline static int32_t get_offset_of_m_Value_20() { return static_cast<int32_t>(offsetof(BoxSlider_t1871650694, ___m_Value_20)); }
	inline float get_m_Value_20() const { return ___m_Value_20; }
	inline float* get_address_of_m_Value_20() { return &___m_Value_20; }
	inline void set_m_Value_20(float value)
	{
		___m_Value_20 = value;
	}

	inline static int32_t get_offset_of_m_ValueY_21() { return static_cast<int32_t>(offsetof(BoxSlider_t1871650694, ___m_ValueY_21)); }
	inline float get_m_ValueY_21() const { return ___m_ValueY_21; }
	inline float* get_address_of_m_ValueY_21() { return &___m_ValueY_21; }
	inline void set_m_ValueY_21(float value)
	{
		___m_ValueY_21 = value;
	}

	inline static int32_t get_offset_of_m_OnValueChanged_22() { return static_cast<int32_t>(offsetof(BoxSlider_t1871650694, ___m_OnValueChanged_22)); }
	inline BoxSliderEvent_t1774115848 * get_m_OnValueChanged_22() const { return ___m_OnValueChanged_22; }
	inline BoxSliderEvent_t1774115848 ** get_address_of_m_OnValueChanged_22() { return &___m_OnValueChanged_22; }
	inline void set_m_OnValueChanged_22(BoxSliderEvent_t1774115848 * value)
	{
		___m_OnValueChanged_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnValueChanged_22), value);
	}

	inline static int32_t get_offset_of_m_HandleTransform_23() { return static_cast<int32_t>(offsetof(BoxSlider_t1871650694, ___m_HandleTransform_23)); }
	inline Transform_t3275118058 * get_m_HandleTransform_23() const { return ___m_HandleTransform_23; }
	inline Transform_t3275118058 ** get_address_of_m_HandleTransform_23() { return &___m_HandleTransform_23; }
	inline void set_m_HandleTransform_23(Transform_t3275118058 * value)
	{
		___m_HandleTransform_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_HandleTransform_23), value);
	}

	inline static int32_t get_offset_of_m_HandleContainerRect_24() { return static_cast<int32_t>(offsetof(BoxSlider_t1871650694, ___m_HandleContainerRect_24)); }
	inline RectTransform_t3349966182 * get_m_HandleContainerRect_24() const { return ___m_HandleContainerRect_24; }
	inline RectTransform_t3349966182 ** get_address_of_m_HandleContainerRect_24() { return &___m_HandleContainerRect_24; }
	inline void set_m_HandleContainerRect_24(RectTransform_t3349966182 * value)
	{
		___m_HandleContainerRect_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_HandleContainerRect_24), value);
	}

	inline static int32_t get_offset_of_m_Offset_25() { return static_cast<int32_t>(offsetof(BoxSlider_t1871650694, ___m_Offset_25)); }
	inline Vector2_t2243707579  get_m_Offset_25() const { return ___m_Offset_25; }
	inline Vector2_t2243707579 * get_address_of_m_Offset_25() { return &___m_Offset_25; }
	inline void set_m_Offset_25(Vector2_t2243707579  value)
	{
		___m_Offset_25 = value;
	}

	inline static int32_t get_offset_of_m_Tracker_26() { return static_cast<int32_t>(offsetof(BoxSlider_t1871650694, ___m_Tracker_26)); }
	inline DrivenRectTransformTracker_t154385424  get_m_Tracker_26() const { return ___m_Tracker_26; }
	inline DrivenRectTransformTracker_t154385424 * get_address_of_m_Tracker_26() { return &___m_Tracker_26; }
	inline void set_m_Tracker_26(DrivenRectTransformTracker_t154385424  value)
	{
		___m_Tracker_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOXSLIDER_T1871650694_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2200 = { sizeof (ARFrameUpdate_t496507918), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2201 = { sizeof (ARAnchorAdded_t2646854145), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2202 = { sizeof (ARAnchorUpdated_t3886071158), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2203 = { sizeof (ARAnchorRemoved_t142665927), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2204 = { sizeof (ARSessionFailed_t872580813), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2205 = { sizeof (internal_ARFrameUpdate_t3296518558), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2206 = { sizeof (internal_ARAnchorAdded_t1622117597), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2207 = { sizeof (internal_ARAnchorUpdated_t3705772742), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2208 = { sizeof (internal_ARAnchorRemoved_t3189755211), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2209 = { sizeof (PointCloudParticleExample_t986756623), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2209[7] = 
{
	PointCloudParticleExample_t986756623::get_offset_of_pointCloudParticlePrefab_2(),
	PointCloudParticleExample_t986756623::get_offset_of_maxPointsToShow_3(),
	PointCloudParticleExample_t986756623::get_offset_of_particleSize_4(),
	PointCloudParticleExample_t986756623::get_offset_of_m_PointCloudData_5(),
	PointCloudParticleExample_t986756623::get_offset_of_frameUpdated_6(),
	PointCloudParticleExample_t986756623::get_offset_of_currentPS_7(),
	PointCloudParticleExample_t986756623::get_offset_of_particles_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2210 = { sizeof (UnityARCameraManager_t2138856896), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2210[3] = 
{
	UnityARCameraManager_t2138856896::get_offset_of_m_camera_2(),
	UnityARCameraManager_t2138856896::get_offset_of_m_session_3(),
	UnityARCameraManager_t2138856896::get_offset_of_savedClearMaterial_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2211 = { sizeof (UnityARCameraNearFar_t519802600), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2211[3] = 
{
	UnityARCameraNearFar_t519802600::get_offset_of_attachedCamera_2(),
	UnityARCameraNearFar_t519802600::get_offset_of_currentNearZ_3(),
	UnityARCameraNearFar_t519802600::get_offset_of_currentFarZ_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2212 = { sizeof (UnityARGeneratePlane_t3368998101), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2212[2] = 
{
	UnityARGeneratePlane_t3368998101::get_offset_of_planePrefab_2(),
	UnityARGeneratePlane_t3368998101::get_offset_of_unityARAnchorManager_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2213 = { sizeof (UnityARHitTestExample_t146867607), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2213[1] = 
{
	UnityARHitTestExample_t146867607::get_offset_of_m_HitTransform_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2214 = { sizeof (UnityARKitControl_t1698990409), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2214[6] = 
{
	UnityARKitControl_t1698990409::get_offset_of_runOptions_2(),
	UnityARKitControl_t1698990409::get_offset_of_alignmentOptions_3(),
	UnityARKitControl_t1698990409::get_offset_of_planeOptions_4(),
	UnityARKitControl_t1698990409::get_offset_of_currentOptionIndex_5(),
	UnityARKitControl_t1698990409::get_offset_of_currentAlignmentIndex_6(),
	UnityARKitControl_t1698990409::get_offset_of_currentPlaneIndex_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2215 = { sizeof (UnityARVideo_t2351297253), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2215[6] = 
{
	UnityARVideo_t2351297253::get_offset_of_m_ClearMaterial_2(),
	UnityARVideo_t2351297253::get_offset_of_m_VideoCommandBuffer_3(),
	UnityARVideo_t2351297253::get_offset_of__videoTextureY_4(),
	UnityARVideo_t2351297253::get_offset_of__videoTextureCbCr_5(),
	UnityARVideo_t2351297253::get_offset_of_m_Session_6(),
	UnityARVideo_t2351297253::get_offset_of_bCommandBufferInitialized_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2216 = { sizeof (UnityPointCloudExample_t3196264220), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2216[4] = 
{
	UnityPointCloudExample_t3196264220::get_offset_of_numPointsToShow_2(),
	UnityPointCloudExample_t3196264220::get_offset_of_PointCloudPrefab_3(),
	UnityPointCloudExample_t3196264220::get_offset_of_pointCloudObjects_4(),
	UnityPointCloudExample_t3196264220::get_offset_of_m_PointCloudData_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2217 = { sizeof (UnityARAnchorManager_t1086564192), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2217[1] = 
{
	UnityARAnchorManager_t1086564192::get_offset_of_planeAnchorMap_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2218 = { sizeof (UnityARMatrixOps_t4039665643), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2219 = { sizeof (UnityARUtility_t3608388148), -1, sizeof(UnityARUtility_t3608388148_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2219[3] = 
{
	UnityARUtility_t3608388148::get_offset_of_meshCollider_0(),
	UnityARUtility_t3608388148::get_offset_of_meshFilter_1(),
	UnityARUtility_t3608388148_StaticFields::get_offset_of_planePrefab_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2220 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305143), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2220[1] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2221 = { sizeof (U24ArrayTypeU3D24_t762068664)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D24_t762068664 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2222 = { sizeof (U3CModuleU3E_t3783534226), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2223 = { sizeof (ParticlePainter_t1073897267), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2223[14] = 
{
	ParticlePainter_t1073897267::get_offset_of_painterParticlePrefab_2(),
	ParticlePainter_t1073897267::get_offset_of_minDistanceThreshold_3(),
	ParticlePainter_t1073897267::get_offset_of_maxDistanceThreshold_4(),
	ParticlePainter_t1073897267::get_offset_of_frameUpdated_5(),
	ParticlePainter_t1073897267::get_offset_of_particleSize_6(),
	ParticlePainter_t1073897267::get_offset_of_penDistance_7(),
	ParticlePainter_t1073897267::get_offset_of_colorPicker_8(),
	ParticlePainter_t1073897267::get_offset_of_currentPS_9(),
	ParticlePainter_t1073897267::get_offset_of_particles_10(),
	ParticlePainter_t1073897267::get_offset_of_previousPosition_11(),
	ParticlePainter_t1073897267::get_offset_of_currentPaintVertices_12(),
	ParticlePainter_t1073897267::get_offset_of_currentColor_13(),
	ParticlePainter_t1073897267::get_offset_of_paintSystems_14(),
	ParticlePainter_t1073897267::get_offset_of_paintMode_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2224 = { sizeof (BallMaker_t2085518213), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2224[3] = 
{
	BallMaker_t2085518213::get_offset_of_ballPrefab_2(),
	BallMaker_t2085518213::get_offset_of_createHeight_3(),
	BallMaker_t2085518213::get_offset_of_props_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2225 = { sizeof (BallMover_t754704982), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2225[2] = 
{
	BallMover_t754704982::get_offset_of_collBallPrefab_2(),
	BallMover_t754704982::get_offset_of_collBallGO_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2226 = { sizeof (Ballz_t4160380291), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2226[2] = 
{
	Ballz_t4160380291::get_offset_of_yDistanceThreshold_2(),
	Ballz_t4160380291::get_offset_of_startingY_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2227 = { sizeof (ModeSwitcher_t223874984), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2227[3] = 
{
	ModeSwitcher_t223874984::get_offset_of_ballMake_2(),
	ModeSwitcher_t223874984::get_offset_of_ballMove_3(),
	ModeSwitcher_t223874984::get_offset_of_appMode_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2228 = { sizeof (FaceCamera_t2774504826), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2228[1] = 
{
	FaceCamera_t2774504826::get_offset_of_MainCam_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2229 = { sizeof (FlipAndAlignToCamera_t3743279677), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2229[1] = 
{
	FlipAndAlignToCamera_t3743279677::get_offset_of_MainCam_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2230 = { sizeof (ColorValues_t3063098635)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2230[8] = 
{
	ColorValues_t3063098635::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2231 = { sizeof (ColorChangedEvent_t2990895397), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2232 = { sizeof (HSVChangedEvent_t1170297569), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2233 = { sizeof (ColorPickerTester_t1006114474), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2233[2] = 
{
	ColorPickerTester_t1006114474::get_offset_of_renderer_2(),
	ColorPickerTester_t1006114474::get_offset_of_picker_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2234 = { sizeof (TiltWindow_t1839185375), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2234[4] = 
{
	TiltWindow_t1839185375::get_offset_of_range_2(),
	TiltWindow_t1839185375::get_offset_of_mTrans_3(),
	TiltWindow_t1839185375::get_offset_of_mStart_4(),
	TiltWindow_t1839185375::get_offset_of_mRot_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2235 = { sizeof (ColorImage_t3157136356), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2235[2] = 
{
	ColorImage_t3157136356::get_offset_of_picker_2(),
	ColorImage_t3157136356::get_offset_of_image_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2236 = { sizeof (ColorLabel_t1884607337), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2236[7] = 
{
	ColorLabel_t1884607337::get_offset_of_picker_2(),
	ColorLabel_t1884607337::get_offset_of_type_3(),
	ColorLabel_t1884607337::get_offset_of_prefix_4(),
	ColorLabel_t1884607337::get_offset_of_minValue_5(),
	ColorLabel_t1884607337::get_offset_of_maxValue_6(),
	ColorLabel_t1884607337::get_offset_of_precision_7(),
	ColorLabel_t1884607337::get_offset_of_label_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2237 = { sizeof (ColorPicker_t3035206225), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2237[9] = 
{
	ColorPicker_t3035206225::get_offset_of__hue_2(),
	ColorPicker_t3035206225::get_offset_of__saturation_3(),
	ColorPicker_t3035206225::get_offset_of__brightness_4(),
	ColorPicker_t3035206225::get_offset_of__red_5(),
	ColorPicker_t3035206225::get_offset_of__green_6(),
	ColorPicker_t3035206225::get_offset_of__blue_7(),
	ColorPicker_t3035206225::get_offset_of__alpha_8(),
	ColorPicker_t3035206225::get_offset_of_onValueChanged_9(),
	ColorPicker_t3035206225::get_offset_of_onHSVChanged_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2238 = { sizeof (ColorPresets_t4120623669), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2238[3] = 
{
	ColorPresets_t4120623669::get_offset_of_picker_2(),
	ColorPresets_t4120623669::get_offset_of_presets_3(),
	ColorPresets_t4120623669::get_offset_of_createPresetImage_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2239 = { sizeof (ColorSlider_t2729134766), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2239[4] = 
{
	ColorSlider_t2729134766::get_offset_of_hsvpicker_2(),
	ColorSlider_t2729134766::get_offset_of_type_3(),
	ColorSlider_t2729134766::get_offset_of_slider_4(),
	ColorSlider_t2729134766::get_offset_of_listen_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2240 = { sizeof (ColorSliderImage_t376502149), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2240[4] = 
{
	ColorSliderImage_t376502149::get_offset_of_picker_2(),
	ColorSliderImage_t376502149::get_offset_of_type_3(),
	ColorSliderImage_t376502149::get_offset_of_direction_4(),
	ColorSliderImage_t376502149::get_offset_of_image_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2241 = { sizeof (HexColorField_t4192118964), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2241[4] = 
{
	HexColorField_t4192118964::get_offset_of_hsvpicker_2(),
	HexColorField_t4192118964::get_offset_of_displayAlpha_3(),
	HexColorField_t4192118964::get_offset_of_hexInputField_4(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2242 = { sizeof (SVBoxSlider_t1173082351), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2242[5] = 
{
	SVBoxSlider_t1173082351::get_offset_of_picker_2(),
	SVBoxSlider_t1173082351::get_offset_of_slider_3(),
	SVBoxSlider_t1173082351::get_offset_of_image_4(),
	SVBoxSlider_t1173082351::get_offset_of_lastH_5(),
	SVBoxSlider_t1173082351::get_offset_of_listen_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2243 = { sizeof (BoxSlider_t1871650694), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2243[11] = 
{
	BoxSlider_t1871650694::get_offset_of_m_HandleRect_16(),
	BoxSlider_t1871650694::get_offset_of_m_MinValue_17(),
	BoxSlider_t1871650694::get_offset_of_m_MaxValue_18(),
	BoxSlider_t1871650694::get_offset_of_m_WholeNumbers_19(),
	BoxSlider_t1871650694::get_offset_of_m_Value_20(),
	BoxSlider_t1871650694::get_offset_of_m_ValueY_21(),
	BoxSlider_t1871650694::get_offset_of_m_OnValueChanged_22(),
	BoxSlider_t1871650694::get_offset_of_m_HandleTransform_23(),
	BoxSlider_t1871650694::get_offset_of_m_HandleContainerRect_24(),
	BoxSlider_t1871650694::get_offset_of_m_Offset_25(),
	BoxSlider_t1871650694::get_offset_of_m_Tracker_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2244 = { sizeof (Direction_t1632189177)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2244[5] = 
{
	Direction_t1632189177::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2245 = { sizeof (BoxSliderEvent_t1774115848), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2246 = { sizeof (Axis_t3966514019)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2246[3] = 
{
	Axis_t3966514019::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2247 = { sizeof (HSVUtil_t3885028383), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2248 = { sizeof (HsvColor_t1057062332)+ sizeof (RuntimeObject), sizeof(HsvColor_t1057062332 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2248[3] = 
{
	HsvColor_t1057062332::get_offset_of_H_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HsvColor_t1057062332::get_offset_of_S_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HsvColor_t1057062332::get_offset_of_V_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2249 = { sizeof (LookAtCamera_t3167693141), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2250 = { sizeof (ModelCollision_t1120807151), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2250[1] = 
{
	ModelCollision_t1120807151::get_offset_of_canvas_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2251 = { sizeof (PlayManager_t1724130965), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2251[3] = 
{
	PlayManager_t1724130965::get_offset_of_playerGroup_2(),
	PlayManager_t1724130965::get_offset_of_animClipNameGroup_3(),
	PlayManager_t1724130965::get_offset_of_currentNumber_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2252 = { sizeof (TextOrientation_t742127405), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2252[1] = 
{
	TextOrientation_t742127405::get_offset_of_player_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2253 = { sizeof (UnityARAmbient_t680084560), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2253[2] = 
{
	UnityARAmbient_t680084560::get_offset_of_l_2(),
	UnityARAmbient_t680084560::get_offset_of_m_Session_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2254 = { sizeof (ExampleAlchemyDataNews_t4207199522), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2254[1] = 
{
	ExampleAlchemyDataNews_t4207199522::get_offset_of_m_AlchemyAPI_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2255 = { sizeof (ExampleAlchemyLanguage_t1060437047), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2255[6] = 
{
	ExampleAlchemyLanguage_t1060437047::get_offset_of_m_AlchemyAPI_2(),
	ExampleAlchemyLanguage_t1060437047::get_offset_of_m_ExampleURL_unitySDK_3(),
	ExampleAlchemyLanguage_t1060437047::get_offset_of_m_ExampleURL_watsonJeopardy_4(),
	ExampleAlchemyLanguage_t1060437047::get_offset_of_m_ExampleURL_microformats_5(),
	ExampleAlchemyLanguage_t1060437047::get_offset_of_m_ExampleText_unitySDK_6(),
	ExampleAlchemyLanguage_t1060437047::get_offset_of_m_ExampleText_watsonJeopardy_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2256 = { sizeof (ExampleConversation_t1371389615), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2256[4] = 
{
	ExampleConversation_t1371389615::get_offset_of_m_Conversation_2(),
	ExampleConversation_t1371389615::get_offset_of_m_WorkspaceID_3(),
	ExampleConversation_t1371389615::get_offset_of_m_UseAlternateIntents_4(),
	ExampleConversation_t1371389615::get_offset_of_questionArray_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2257 = { sizeof (ExampleConversationExperimental_t3981692631), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2257[3] = 
{
	ExampleConversationExperimental_t3981692631::get_offset_of_m_Conversation_2(),
	ExampleConversationExperimental_t3981692631::get_offset_of_m_WorkspaceID_3(),
	ExampleConversationExperimental_t3981692631::get_offset_of_m_Input_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2258 = { sizeof (ExampleDiscoveryV1_t2040126595), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2258[17] = 
{
	ExampleDiscoveryV1_t2040126595::get_offset_of_m_Discovery_2(),
	ExampleDiscoveryV1_t2040126595::get_offset_of_m_CreatedEnvironmentID_3(),
	ExampleDiscoveryV1_t2040126595::get_offset_of_m_DefaultEnvironmentID_4(),
	ExampleDiscoveryV1_t2040126595::get_offset_of_m_DefaultConfigurationID_5(),
	ExampleDiscoveryV1_t2040126595::get_offset_of_m_DefaultCollectionID_6(),
	ExampleDiscoveryV1_t2040126595::get_offset_of_m_ConfigurationJsonPath_7(),
	ExampleDiscoveryV1_t2040126595::get_offset_of_m_CreatedConfigurationID_8(),
	ExampleDiscoveryV1_t2040126595::get_offset_of_m_FilePathToIngest_9(),
	ExampleDiscoveryV1_t2040126595::get_offset_of_m_Metadata_10(),
	ExampleDiscoveryV1_t2040126595::get_offset_of_m_CreatedCollectionID_11(),
	ExampleDiscoveryV1_t2040126595::get_offset_of_m_CreatedCollectionName_12(),
	ExampleDiscoveryV1_t2040126595::get_offset_of_m_CreatedCollectionDescription_13(),
	ExampleDiscoveryV1_t2040126595::get_offset_of_m_CreatedDocumentID_14(),
	ExampleDiscoveryV1_t2040126595::get_offset_of_m_DocumentFilePath_15(),
	ExampleDiscoveryV1_t2040126595::get_offset_of_m_Query_16(),
	ExampleDiscoveryV1_t2040126595::get_offset_of_m_IsConfigDeleted_17(),
	ExampleDiscoveryV1_t2040126595::get_offset_of_m_IsCollectionDeleted_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2259 = { sizeof (ExampleDocumentConversion_t3759077945), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2259[1] = 
{
	ExampleDocumentConversion_t3759077945::get_offset_of_m_DocumentConversion_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2260 = { sizeof (ExampleLanguageTranslation_t4280728515), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2260[2] = 
{
	ExampleLanguageTranslation_t4280728515::get_offset_of_m_Translate_2(),
	ExampleLanguageTranslation_t4280728515::get_offset_of_m_PharseToTranslate_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2261 = { sizeof (ExampleLanguageTranslator_t1714284830), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2261[2] = 
{
	ExampleLanguageTranslator_t1714284830::get_offset_of_m_Translate_2(),
	ExampleLanguageTranslator_t1714284830::get_offset_of_m_PharseToTranslate_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2262 = { sizeof (ExampleNaturalLanguageClassifier_t1512206848), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2262[3] = 
{
	ExampleNaturalLanguageClassifier_t1512206848::get_offset_of_m_NaturalLanguageClassifier_2(),
	ExampleNaturalLanguageClassifier_t1512206848::get_offset_of_m_ClassifierId_3(),
	ExampleNaturalLanguageClassifier_t1512206848::get_offset_of_m_InputString_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2263 = { sizeof (ExamplePersonalityInsightsV2_t2660220987), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2263[1] = 
{
	ExamplePersonalityInsightsV2_t2660220987::get_offset_of_m_personalityInsights_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2264 = { sizeof (ExamplePersonalityInsightsV3_t4226304928), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2264[3] = 
{
	ExamplePersonalityInsightsV3_t4226304928::get_offset_of_m_personalityInsights_2(),
	ExamplePersonalityInsightsV3_t4226304928::get_offset_of_testString_3(),
	ExamplePersonalityInsightsV3_t4226304928::get_offset_of_dataPath_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2265 = { sizeof (ExampleRetrieveAndRank_t500619117), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2265[1] = 
{
	ExampleRetrieveAndRank_t500619117::get_offset_of_m_RetrieveAndRank_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2266 = { sizeof (ExampleSpeechToText_t2083741878), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2266[5] = 
{
	ExampleSpeechToText_t2083741878::get_offset_of_m_AudioClip_2(),
	ExampleSpeechToText_t2083741878::get_offset_of_m_SpeechToText_3(),
	ExampleSpeechToText_t2083741878::get_offset_of_m_CreatedCustomizationID_4(),
	ExampleSpeechToText_t2083741878::get_offset_of_m_CreatedCorpusName_5(),
	ExampleSpeechToText_t2083741878::get_offset_of_m_CustomCorpusFilePath_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2267 = { sizeof (ExampleStreaming_t2645177802), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2267[6] = 
{
	ExampleStreaming_t2645177802::get_offset_of_m_RecordingRoutine_2(),
	ExampleStreaming_t2645177802::get_offset_of_m_MicrophoneID_3(),
	ExampleStreaming_t2645177802::get_offset_of_m_Recording_4(),
	ExampleStreaming_t2645177802::get_offset_of_m_RecordingBufferSize_5(),
	ExampleStreaming_t2645177802::get_offset_of_m_RecordingHZ_6(),
	ExampleStreaming_t2645177802::get_offset_of_m_SpeechToText_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2268 = { sizeof (U3CRecordingHandlerU3Ec__Iterator0_t3836051059), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2268[10] = 
{
	U3CRecordingHandlerU3Ec__Iterator0_t3836051059::get_offset_of_U3CbFirstBlockU3E__0_0(),
	U3CRecordingHandlerU3Ec__Iterator0_t3836051059::get_offset_of_U3CmidPointU3E__0_1(),
	U3CRecordingHandlerU3Ec__Iterator0_t3836051059::get_offset_of_U3CsamplesU3E__0_2(),
	U3CRecordingHandlerU3Ec__Iterator0_t3836051059::get_offset_of_U3CwritePosU3E__1_3(),
	U3CRecordingHandlerU3Ec__Iterator0_t3836051059::get_offset_of_U3CremainingU3E__2_4(),
	U3CRecordingHandlerU3Ec__Iterator0_t3836051059::get_offset_of_U3CtimeRemainingU3E__2_5(),
	U3CRecordingHandlerU3Ec__Iterator0_t3836051059::get_offset_of_U24this_6(),
	U3CRecordingHandlerU3Ec__Iterator0_t3836051059::get_offset_of_U24current_7(),
	U3CRecordingHandlerU3Ec__Iterator0_t3836051059::get_offset_of_U24disposing_8(),
	U3CRecordingHandlerU3Ec__Iterator0_t3836051059::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2269 = { sizeof (ExampleTextToSpeech_t3969242902), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2269[2] = 
{
	ExampleTextToSpeech_t3969242902::get_offset_of_m_TextToSpeech_2(),
	ExampleTextToSpeech_t3969242902::get_offset_of_m_TestString_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2270 = { sizeof (ExampleToneAnalyzer_t3568742368), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2270[2] = 
{
	ExampleToneAnalyzer_t3568742368::get_offset_of_m_ToneAnalyzer_2(),
	ExampleToneAnalyzer_t3568742368::get_offset_of_m_StringToTestTone_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2271 = { sizeof (ExampleTradeoffAnalytics_t2994175413), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2271[1] = 
{
	ExampleTradeoffAnalytics_t2994175413::get_offset_of_m_TradeoffAnalytics_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2272 = { sizeof (TestDataValue_t1171208003), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2272[3] = 
{
	TestDataValue_t1171208003::get_offset_of_U3CpriceU3Ek__BackingField_0(),
	TestDataValue_t1171208003::get_offset_of_U3CweightU3Ek__BackingField_1(),
	TestDataValue_t1171208003::get_offset_of_U3CbrandU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2273 = { sizeof (TestData_t1138889280), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2274 = { sizeof (ExampleVisualRecognition_t1110120389), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2274[6] = 
{
	ExampleVisualRecognition_t1110120389::get_offset_of_m_VisualRecognition_2(),
	ExampleVisualRecognition_t1110120389::get_offset_of_m_classifierName_3(),
	ExampleVisualRecognition_t1110120389::get_offset_of_m_classifierID_4(),
	ExampleVisualRecognition_t1110120389::get_offset_of_m_classifierToDelete_5(),
	ExampleVisualRecognition_t1110120389::get_offset_of_m_imageURL_6(),
	ExampleVisualRecognition_t1110120389::get_offset_of_m_imageTextURL_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2275 = { sizeof (ReverseNormals_t3342799322), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2276 = { sizeof (WebCamRecognition_t2299961224), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2276[3] = 
{
	WebCamRecognition_t2299961224::get_offset_of_m_WebCamWidget_2(),
	WebCamRecognition_t2299961224::get_offset_of_m_WebCamDisplayWidget_3(),
	WebCamRecognition_t2299961224::get_offset_of_m_VisualRecognition_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2277 = { sizeof (U3CSaveImageFileU3Ec__Iterator0_t3286239818), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2277[7] = 
{
	U3CSaveImageFileU3Ec__Iterator0_t3286239818::get_offset_of_filePath_0(),
	U3CSaveImageFileU3Ec__Iterator0_t3286239818::get_offset_of_fileName_1(),
	U3CSaveImageFileU3Ec__Iterator0_t3286239818::get_offset_of_U3CimageU3E__0_2(),
	U3CSaveImageFileU3Ec__Iterator0_t3286239818::get_offset_of_U24this_3(),
	U3CSaveImageFileU3Ec__Iterator0_t3286239818::get_offset_of_U24current_4(),
	U3CSaveImageFileU3Ec__Iterator0_t3286239818::get_offset_of_U24disposing_5(),
	U3CSaveImageFileU3Ec__Iterator0_t3286239818::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2278 = { sizeof (U3CClassifyImageU3Ec__Iterator1_t4288026660), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2278[6] = 
{
	U3CClassifyImageU3Ec__Iterator1_t4288026660::get_offset_of_U3CimageU3E__0_0(),
	U3CClassifyImageU3Ec__Iterator1_t4288026660::get_offset_of_U3CimageDataU3E__0_1(),
	U3CClassifyImageU3Ec__Iterator1_t4288026660::get_offset_of_U24this_2(),
	U3CClassifyImageU3Ec__Iterator1_t4288026660::get_offset_of_U24current_3(),
	U3CClassifyImageU3Ec__Iterator1_t4288026660::get_offset_of_U24disposing_4(),
	U3CClassifyImageU3Ec__Iterator1_t4288026660::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2279 = { sizeof (U3CDetectFacesInImageU3Ec__Iterator2_t2044188333), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2279[6] = 
{
	U3CDetectFacesInImageU3Ec__Iterator2_t2044188333::get_offset_of_U3CimageU3E__0_0(),
	U3CDetectFacesInImageU3Ec__Iterator2_t2044188333::get_offset_of_U3CimageDataU3E__0_1(),
	U3CDetectFacesInImageU3Ec__Iterator2_t2044188333::get_offset_of_U24this_2(),
	U3CDetectFacesInImageU3Ec__Iterator2_t2044188333::get_offset_of_U24current_3(),
	U3CDetectFacesInImageU3Ec__Iterator2_t2044188333::get_offset_of_U24disposing_4(),
	U3CDetectFacesInImageU3Ec__Iterator2_t2044188333::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2280 = { sizeof (U3CRecognizeTextInImageU3Ec__Iterator3_t1348868334), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2280[6] = 
{
	U3CRecognizeTextInImageU3Ec__Iterator3_t1348868334::get_offset_of_U3CimageU3E__0_0(),
	U3CRecognizeTextInImageU3Ec__Iterator3_t1348868334::get_offset_of_U3CimageDataU3E__0_1(),
	U3CRecognizeTextInImageU3Ec__Iterator3_t1348868334::get_offset_of_U24this_2(),
	U3CRecognizeTextInImageU3Ec__Iterator3_t1348868334::get_offset_of_U24current_3(),
	U3CRecognizeTextInImageU3Ec__Iterator3_t1348868334::get_offset_of_U24disposing_4(),
	U3CRecognizeTextInImageU3Ec__Iterator3_t1348868334::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2281 = { sizeof (ConfigLoader_t1246716173), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2281[2] = 
{
	ConfigLoader_t1246716173::get_offset_of_m_Prefab_2(),
	ConfigLoader_t1246716173::get_offset_of_m_CreatedObject_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2282 = { sizeof (U3CStartU3Ec__Iterator0_t2669888877), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2282[4] = 
{
	U3CStartU3Ec__Iterator0_t2669888877::get_offset_of_U24this_0(),
	U3CStartU3Ec__Iterator0_t2669888877::get_offset_of_U24current_1(),
	U3CStartU3Ec__Iterator0_t2669888877::get_offset_of_U24disposing_2(),
	U3CStartU3Ec__Iterator0_t2669888877::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2283 = { sizeof (LogSystemInitilization_t33207982), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2283[2] = 
{
	LogSystemInitilization_t33207982::get_offset_of_m_LogHistory_2(),
	LogSystemInitilization_t33207982::get_offset_of_m_LogLevelOnFile_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2284 = { sizeof (MenuScene_t1232739575), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2284[5] = 
{
	MenuScene_t1232739575::get_offset_of_m_SceneName_0(),
	MenuScene_t1232739575::get_offset_of_m_SceneDesc_1(),
	MenuScene_t1232739575::get_offset_of_m_CustomBackButtonPosition_2(),
	MenuScene_t1232739575::get_offset_of_m_CustomBackButtonScale_3(),
	MenuScene_t1232739575::get_offset_of_m_IsVisibleBackButton_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2285 = { sizeof (MainUI_t541415437), -1, sizeof(MainUI_t541415437_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2285[10] = 
{
	MainUI_t541415437::get_offset_of_m_ButtonLayout_2(),
	MainUI_t541415437::get_offset_of_m_ButtonPrefab_3(),
	MainUI_t541415437::get_offset_of_m_BackgroundUI_4(),
	MainUI_t541415437::get_offset_of_m_ButtonBack_5(),
	MainUI_t541415437::get_offset_of_m_InitialBackButtonPosition_6(),
	MainUI_t541415437::get_offset_of_m_InitialBackButtonScale_7(),
	MainUI_t541415437::get_offset_of_m_InitialBackButtonColor_8(),
	MainUI_t541415437::get_offset_of_m_Scenes_9(),
	0,
	MainUI_t541415437_StaticFields::get_offset_of__instance_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2286 = { sizeof (U3CStartU3Ec__Iterator0_t1305459389), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2286[4] = 
{
	U3CStartU3Ec__Iterator0_t1305459389::get_offset_of_U24this_0(),
	U3CStartU3Ec__Iterator0_t1305459389::get_offset_of_U24current_1(),
	U3CStartU3Ec__Iterator0_t1305459389::get_offset_of_U24disposing_2(),
	U3CStartU3Ec__Iterator0_t1305459389::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2287 = { sizeof (U3CUpdateButtonsU3Ec__AnonStorey3_t2231175942), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2287[2] = 
{
	U3CUpdateButtonsU3Ec__AnonStorey3_t2231175942::get_offset_of_captured_0(),
	U3CUpdateButtonsU3Ec__AnonStorey3_t2231175942::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2288 = { sizeof (U3CLoadLevelAsyncU3Ec__Iterator1_t3234328056), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2288[6] = 
{
	U3CLoadLevelAsyncU3Ec__Iterator1_t3234328056::get_offset_of_name_0(),
	U3CLoadLevelAsyncU3Ec__Iterator1_t3234328056::get_offset_of_U3CasyncOperationU3E__0_1(),
	U3CLoadLevelAsyncU3Ec__Iterator1_t3234328056::get_offset_of_U24this_2(),
	U3CLoadLevelAsyncU3Ec__Iterator1_t3234328056::get_offset_of_U24current_3(),
	U3CLoadLevelAsyncU3Ec__Iterator1_t3234328056::get_offset_of_U24disposing_4(),
	U3CLoadLevelAsyncU3Ec__Iterator1_t3234328056::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2289 = { sizeof (U3CMakeActiveEventSystemWithDelayU3Ec__Iterator2_t3156884139), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2289[5] = 
{
	U3CMakeActiveEventSystemWithDelayU3Ec__Iterator2_t3156884139::get_offset_of_active_0(),
	U3CMakeActiveEventSystemWithDelayU3Ec__Iterator2_t3156884139::get_offset_of_U24this_1(),
	U3CMakeActiveEventSystemWithDelayU3Ec__Iterator2_t3156884139::get_offset_of_U24current_2(),
	U3CMakeActiveEventSystemWithDelayU3Ec__Iterator2_t3156884139::get_offset_of_U24disposing_3(),
	U3CMakeActiveEventSystemWithDelayU3Ec__Iterator2_t3156884139::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2290 = { sizeof (CameraTarget_t552179396), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2290[16] = 
{
	CameraTarget_t552179396::get_offset_of_mp_WatsonCamera_2(),
	CameraTarget_t552179396::get_offset_of_mp_CameraAttached_3(),
	CameraTarget_t552179396::get_offset_of_m_UseCustomPosition_4(),
	CameraTarget_t552179396::get_offset_of_m_CustomPosition_5(),
	CameraTarget_t552179396::get_offset_of_m_OffsetPosition_6(),
	CameraTarget_t552179396::get_offset_of_m_OffsetPositionRotation_7(),
	CameraTarget_t552179396::get_offset_of_m_UseCustomRotation_8(),
	CameraTarget_t552179396::get_offset_of_m_CustomRotation_9(),
	CameraTarget_t552179396::get_offset_of_m_UseTargetObjectToRotate_10(),
	CameraTarget_t552179396::get_offset_of_m_CustomTargetObjectToLookAt_11(),
	CameraTarget_t552179396::get_offset_of_m_CameraPathRootObject_12(),
	CameraTarget_t552179396::get_offset_of_m_RatioAtCameraPath_13(),
	CameraTarget_t552179396::get_offset_of_m_DistanceFromCamera_14(),
	CameraTarget_t552179396::get_offset_of_m_PathTransforms_15(),
	CameraTarget_t552179396::get_offset_of_m_TextEnableCamera_16(),
	CameraTarget_t552179396::get_offset_of_m_TestToMakeItCurrent_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2291 = { sizeof (HotCorner_t121384184), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2291[2] = 
{
	HotCorner_t121384184::get_offset_of_m_NormalizedThresholdWidth_2(),
	HotCorner_t121384184::get_offset_of_m_NormalizedThresholdHeight_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2292 = { sizeof (WatsonCamera_t824577261), -1, sizeof(WatsonCamera_t824577261_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2292[12] = 
{
	WatsonCamera_t824577261_StaticFields::get_offset_of_mp_Instance_2(),
	WatsonCamera_t824577261::get_offset_of_m_ListCameraTarget_3(),
	WatsonCamera_t824577261::get_offset_of_m_TargetCamera_4(),
	WatsonCamera_t824577261::get_offset_of_m_CameraInitialLocation_5(),
	WatsonCamera_t824577261::get_offset_of_m_CameraInitialRotation_6(),
	WatsonCamera_t824577261::get_offset_of_m_PanSpeed_7(),
	WatsonCamera_t824577261::get_offset_of_m_ZoomSpeed_8(),
	WatsonCamera_t824577261::get_offset_of_m_SpeedForCameraAnimation_9(),
	WatsonCamera_t824577261::get_offset_of_m_CommandMovementModifier_10(),
	WatsonCamera_t824577261::get_offset_of_m_AntiAliasing_11(),
	WatsonCamera_t824577261::get_offset_of_m_DepthOfField_12(),
	WatsonCamera_t824577261::get_offset_of_m_DisableInteractivity_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2293 = { sizeof (RESTConnector_t3705102247), -1, sizeof(RESTConnector_t3705102247_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2293[7] = 
{
	RESTConnector_t3705102247_StaticFields::get_offset_of_sm_LogResponseTime_0(),
	RESTConnector_t3705102247::get_offset_of_U3CURLU3Ek__BackingField_1(),
	RESTConnector_t3705102247::get_offset_of_U3CAuthenticationU3Ek__BackingField_2(),
	RESTConnector_t3705102247::get_offset_of_U3CHeadersU3Ek__BackingField_3(),
	RESTConnector_t3705102247_StaticFields::get_offset_of_sm_Connectors_4(),
	RESTConnector_t3705102247::get_offset_of_m_ActiveConnections_5(),
	RESTConnector_t3705102247::get_offset_of_m_Requests_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2294 = { sizeof (ResponseEvent_t1616568356), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2295 = { sizeof (ProgressEvent_t4185145044), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2296 = { sizeof (Response_t429319368), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2296[4] = 
{
	Response_t429319368::get_offset_of_U3CSuccessU3Ek__BackingField_0(),
	Response_t429319368::get_offset_of_U3CErrorU3Ek__BackingField_1(),
	Response_t429319368::get_offset_of_U3CDataU3Ek__BackingField_2(),
	Response_t429319368::get_offset_of_U3CElapsedTimeU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2297 = { sizeof (Form_t779275863), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2297[5] = 
{
	Form_t779275863::get_offset_of_U3CIsBinaryU3Ek__BackingField_0(),
	Form_t779275863::get_offset_of_U3CBoxedObjectU3Ek__BackingField_1(),
	Form_t779275863::get_offset_of_U3CContentsU3Ek__BackingField_2(),
	Form_t779275863::get_offset_of_U3CFileNameU3Ek__BackingField_3(),
	Form_t779275863::get_offset_of_U3CMimeTypeU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2298 = { sizeof (Request_t466816980), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2298[11] = 
{
	Request_t466816980::get_offset_of_U3CTimeoutU3Ek__BackingField_0(),
	Request_t466816980::get_offset_of_U3CCancelU3Ek__BackingField_1(),
	Request_t466816980::get_offset_of_U3CDeleteU3Ek__BackingField_2(),
	Request_t466816980::get_offset_of_U3CFunctionU3Ek__BackingField_3(),
	Request_t466816980::get_offset_of_U3CParametersU3Ek__BackingField_4(),
	Request_t466816980::get_offset_of_U3CHeadersU3Ek__BackingField_5(),
	Request_t466816980::get_offset_of_U3CSendU3Ek__BackingField_6(),
	Request_t466816980::get_offset_of_U3CFormsU3Ek__BackingField_7(),
	Request_t466816980::get_offset_of_U3COnResponseU3Ek__BackingField_8(),
	Request_t466816980::get_offset_of_U3COnDownloadProgressU3Ek__BackingField_9(),
	Request_t466816980::get_offset_of_U3COnUploadProgressU3Ek__BackingField_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2299 = { sizeof (U3CProcessRequestQueueU3Ec__Iterator0_t3628487913), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2299[13] = 
{
	U3CProcessRequestQueueU3Ec__Iterator0_t3628487913::get_offset_of_U3CreqU3E__1_0(),
	U3CProcessRequestQueueU3Ec__Iterator0_t3628487913::get_offset_of_U3CurlU3E__1_1(),
	U3CProcessRequestQueueU3Ec__Iterator0_t3628487913::get_offset_of_U3CargsU3E__1_2(),
	U3CProcessRequestQueueU3Ec__Iterator0_t3628487913::get_offset_of_U24locvar0_3(),
	U3CProcessRequestQueueU3Ec__Iterator0_t3628487913::get_offset_of_U3CrespU3E__1_4(),
	U3CProcessRequestQueueU3Ec__Iterator0_t3628487913::get_offset_of_U3CstartTimeU3E__1_5(),
	U3CProcessRequestQueueU3Ec__Iterator0_t3628487913::get_offset_of_U3CwwwU3E__2_6(),
	U3CProcessRequestQueueU3Ec__Iterator0_t3628487913::get_offset_of_U3CtimeoutU3E__2_7(),
	U3CProcessRequestQueueU3Ec__Iterator0_t3628487913::get_offset_of_U3CbErrorU3E__2_8(),
	U3CProcessRequestQueueU3Ec__Iterator0_t3628487913::get_offset_of_U24this_9(),
	U3CProcessRequestQueueU3Ec__Iterator0_t3628487913::get_offset_of_U24current_10(),
	U3CProcessRequestQueueU3Ec__Iterator0_t3628487913::get_offset_of_U24disposing_11(),
	U3CProcessRequestQueueU3Ec__Iterator0_t3628487913::get_offset_of_U24PC_12(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
