﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.String
struct String_t;
// System.Func`3<WebSocketSharp.Opcode,System.IO.Stream,System.Boolean>
struct Func_3_t3699793028;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;
// WebSocketSharp.WebSocket
struct WebSocket_t3268376029;
// System.Action
struct Action_t3226471752;
// System.Func`1<System.Boolean>
struct Func_1_t1485000104;
// WebSocketSharp.Server.WebSocketServiceManager
struct WebSocketServiceManager_t1683165547;
// System.Net.Sockets.TcpClient
struct TcpClient_t408947970;
// WebSocketSharp.Server.WebSocketServer
struct WebSocketServer_t2522399002;
// WebSocketSharp.Server.WebSocketSessionManager
struct WebSocketSessionManager_t2802512518;
// WebSocketSharp.MessageEventArgs
struct MessageEventArgs_t2890051726;
// System.Action`4<WebSocketSharp.CloseEventArgs,System.Boolean,System.Boolean,System.Boolean>
struct Action_4_t3723770032;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// WebSocketSharp.Net.Cookie
struct Cookie_t1826188460;
// System.IDisposable
struct IDisposable_t2427283555;
// VikingCrewTools.ScriptedDialogueBehaviour
struct ScriptedDialogueBehaviour_t2807853473;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// WebSocketSharp.WebSocketFrame
struct WebSocketFrame_t764750278;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.Collections.Specialized.NameObjectCollectionBase/_Item
struct _Item_t3244489099;
// System.Collections.ArrayList
struct ArrayList_t4252133567;
// System.Collections.IHashCodeProvider
struct IHashCodeProvider_t1980576455;
// System.Collections.IComparer
struct IComparer_t3952557350;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.Collections.Specialized.NameObjectCollectionBase/KeysCollection
struct KeysCollection_t633582367;
// System.Collections.IEqualityComparer
struct IEqualityComparer_t2716208158;
// System.IntPtr[]
struct IntPtrU5BU5D_t169632028;
// System.Collections.IDictionary
struct IDictionary_t596158605;
// System.Action`1<WebSocketSharp.WebSocketFrame>
struct Action_1_t566549660;
// WebSocketSharp.WebSocketFrame/<dump>c__AnonStorey1
struct U3CdumpU3Ec__AnonStorey1_t3897361314;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;
// System.IO.Stream
struct Stream_t3255436806;
// System.Action`1<System.Exception>
struct Action_1_t1729240069;
// WebSocketSharp.Net.HttpListenerContext
struct HttpListenerContext_t994708409;
// WebSocketSharp.Server.HttpServer
struct HttpServer_t490574305;
// System.Version
struct Version_t1755874712;
// WebSocketSharp.Net.WebHeaderCollection
struct WebHeaderCollection_t1932982249;
// WebSocketSharp.PayloadData
struct PayloadData_t3839327312;
// System.Collections.Generic.Dictionary`2<System.String,System.Char>
struct Dictionary_2_t1074293304;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.String[]
struct StringU5BU5D_t1642385972;
// WebSocketSharp.Net.WebSockets.HttpListenerWebSocketContext
struct HttpListenerWebSocketContext_t1778866096;
// WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext
struct TcpListenerWebSocketContext_t1695227117;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// System.Exception
struct Exception_t1927440687;
// System.Threading.ManualResetEvent
struct ManualResetEvent_t926074657;
// WebSocketSharp.Net.HttpStreamAsyncResult
struct HttpStreamAsyncResult_t782812803;
// System.IO.MemoryStream
struct MemoryStream_t743994179;
// WebSocketSharp.Net.HttpListenerResponse
struct HttpListenerResponse_t2223360553;
// System.Action`3<System.Byte[],System.Int32,System.Int32>
struct Action_3_t4193041497;
// WebSocketSharp.Net.HttpListenerRequest
struct HttpListenerRequest_t2316381291;
// WebSocketSharp.Net.CookieCollection
struct CookieCollection_t4248997468;
// WebSocketSharp.Logger
struct Logger_t2598199114;
// System.Collections.Specialized.NameValueCollection
struct NameValueCollection_t3047564564;
// WebSocketSharp.HttpRequest
struct HttpRequest_t1845443631;
// System.Uri
struct Uri_t19570940;
// System.Security.Principal.IPrincipal
struct IPrincipal_t783141777;
// System.Void
struct Void_t1841601450;
// System.Collections.Generic.Dictionary`2<System.String,System.Boolean>
struct Dictionary_2_t1445386684;
// System.Collections.Generic.Dictionary`2<System.String,WebSocketSharp.Net.HttpHeaderInfo>
struct Dictionary_2_t4011098823;
// System.Net.Security.LocalCertificateSelectionCallback
struct LocalCertificateSelectionCallback_t3696771181;
// System.Net.Security.RemoteCertificateValidationCallback
struct RemoteCertificateValidationCallback_t2756269959;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.Collections.Generic.Dictionary`2<System.String,WebSocketSharp.Server.IWebSocketSession>
struct Dictionary_2_t3346836010;
// System.Timers.Timer
struct Timer_t4294691011;
// WebSocketSharp.Net.AuthenticationChallenge
struct AuthenticationChallenge_t1146723439;
// WebSocketSharp.Net.WebSockets.WebSocketContext
struct WebSocketContext_t3488732344;
// WebSocketSharp.Net.NetworkCredential
struct NetworkCredential_t3911206805;
// System.Threading.AutoResetEvent
struct AutoResetEvent_t15112628;
// System.Func`2<WebSocketSharp.Net.WebSockets.WebSocketContext,System.String>
struct Func_2_t1909060290;
// System.Action`1<WebSocketSharp.MessageEventArgs>
struct Action_1_t2691851108;
// System.Collections.Generic.Queue`1<WebSocketSharp.MessageEventArgs>
struct Queue_1_t2709708561;
// WebSocketSharp.Net.ClientSslConfiguration
struct ClientSslConfiguration_t1159130081;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t2510243513;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;
// System.EventHandler`1<WebSocketSharp.CloseEventArgs>
struct EventHandler_1_t3230782241;
// System.EventHandler`1<WebSocketSharp.ErrorEventArgs>
struct EventHandler_1_t3388497467;
// System.EventHandler`1<WebSocketSharp.MessageEventArgs>
struct EventHandler_1_t1481358898;
// System.EventHandler
struct EventHandler_t277755526;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Net.IPAddress
struct IPAddress_t1399971723;
// System.Net.Sockets.TcpListener
struct TcpListener_t1551297625;
// System.Threading.Thread
struct Thread_t241561612;
// WebSocketSharp.Net.ServerSslConfiguration
struct ServerSslConfiguration_t204724213;
// System.Func`2<System.Security.Principal.IIdentity,WebSocketSharp.Net.NetworkCredential>
struct Func_2_t353436505;
// WebSocketSharp.Net.HttpListener
struct HttpListener_t4179429670;
// System.EventHandler`1<WebSocketSharp.Server.HttpRequestEventArgs>
struct EventHandler_1_t3804744336;
// System.Collections.Generic.Dictionary`2<System.String,WebSocketSharp.Server.WebSocketServiceHost>
struct Dictionary_2_t2406885756;
// System.Func`3<WebSocketSharp.Net.CookieCollection,WebSocketSharp.Net.CookieCollection,System.Boolean>
struct Func_3_t3137832106;
// System.Func`2<System.String,System.Boolean>
struct Func_2_t1989381442;
// System.Security.Cryptography.X509Certificates.X509Certificate2
struct X509Certificate2_t4056456767;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.UI.Text
struct Text_t356221433;
// VikingCrewTools.ScriptedDialogueBehaviour/DialogueLine[]
struct DialogueLineU5BU5D_t1619601905;
// UnityEngine.Animation
struct Animation_t2068071072;
// UnityEngine.UI.InputField
struct InputField_t1631627530;
// VikingCrewDevelopment.SayRandomThingsBehaviour
struct SayRandomThingsBehaviour_t3866205118;
// UnityEngine.UI.ToggleGroup
struct ToggleGroup_t1030026315;
// System.Collections.Generic.List`1<VikingCrewTools.SpeechbubbleManager/SpeechbubblePrefab>
struct List_1_t105400862;
// System.Collections.Generic.Dictionary`2<VikingCrewTools.SpeechbubbleManager/SpeechbubbleType,UnityEngine.GameObject>
struct Dictionary_2_t3281136550;
// System.Collections.Generic.Dictionary`2<VikingCrewTools.SpeechbubbleManager/SpeechbubbleType,System.Collections.Generic.Queue`1<VikingCrewTools.SpeechbubbleBehaviour>>
struct Dictionary_2_t69813731;
// IBM.Watson.DeveloperCloud.Editor.UnitTestManager/OnTestsComplete
struct OnTestsComplete_t1412473303;
// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;
// UnityEngine.RectOffset
struct RectOffset_t3387826427;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// System.Collections.Generic.List`1<UnityEngine.RectTransform>
struct List_1_t2719087314;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CVALIDATESECWEBSOCKETEXTENSIONSSERVERHEADERU3EC__ANONSTOREY5_T2469772577_H
#define U3CVALIDATESECWEBSOCKETEXTENSIONSSERVERHEADERU3EC__ANONSTOREY5_T2469772577_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.WebSocket/<validateSecWebSocketExtensionsServerHeader>c__AnonStorey5
struct  U3CvalidateSecWebSocketExtensionsServerHeaderU3Ec__AnonStorey5_t2469772577  : public RuntimeObject
{
public:
	// System.String WebSocketSharp.WebSocket/<validateSecWebSocketExtensionsServerHeader>c__AnonStorey5::method
	String_t* ___method_0;

public:
	inline static int32_t get_offset_of_method_0() { return static_cast<int32_t>(offsetof(U3CvalidateSecWebSocketExtensionsServerHeaderU3Ec__AnonStorey5_t2469772577, ___method_0)); }
	inline String_t* get_method_0() const { return ___method_0; }
	inline String_t** get_address_of_method_0() { return &___method_0; }
	inline void set_method_0(String_t* value)
	{
		___method_0 = value;
		Il2CppCodeGenWriteBarrier((&___method_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CVALIDATESECWEBSOCKETEXTENSIONSSERVERHEADERU3EC__ANONSTOREY5_T2469772577_H
#ifndef U3CSENDASYNCU3EC__ANONSTOREY3_T2886884107_H
#define U3CSENDASYNCU3EC__ANONSTOREY3_T2886884107_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.WebSocket/<sendAsync>c__AnonStorey3
struct  U3CsendAsyncU3Ec__AnonStorey3_t2886884107  : public RuntimeObject
{
public:
	// System.Func`3<WebSocketSharp.Opcode,System.IO.Stream,System.Boolean> WebSocketSharp.WebSocket/<sendAsync>c__AnonStorey3::sender
	Func_3_t3699793028 * ___sender_0;
	// System.Action`1<System.Boolean> WebSocketSharp.WebSocket/<sendAsync>c__AnonStorey3::completed
	Action_1_t3627374100 * ___completed_1;
	// WebSocketSharp.WebSocket WebSocketSharp.WebSocket/<sendAsync>c__AnonStorey3::$this
	WebSocket_t3268376029 * ___U24this_2;

public:
	inline static int32_t get_offset_of_sender_0() { return static_cast<int32_t>(offsetof(U3CsendAsyncU3Ec__AnonStorey3_t2886884107, ___sender_0)); }
	inline Func_3_t3699793028 * get_sender_0() const { return ___sender_0; }
	inline Func_3_t3699793028 ** get_address_of_sender_0() { return &___sender_0; }
	inline void set_sender_0(Func_3_t3699793028 * value)
	{
		___sender_0 = value;
		Il2CppCodeGenWriteBarrier((&___sender_0), value);
	}

	inline static int32_t get_offset_of_completed_1() { return static_cast<int32_t>(offsetof(U3CsendAsyncU3Ec__AnonStorey3_t2886884107, ___completed_1)); }
	inline Action_1_t3627374100 * get_completed_1() const { return ___completed_1; }
	inline Action_1_t3627374100 ** get_address_of_completed_1() { return &___completed_1; }
	inline void set_completed_1(Action_1_t3627374100 * value)
	{
		___completed_1 = value;
		Il2CppCodeGenWriteBarrier((&___completed_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CsendAsyncU3Ec__AnonStorey3_t2886884107, ___U24this_2)); }
	inline WebSocket_t3268376029 * get_U24this_2() const { return ___U24this_2; }
	inline WebSocket_t3268376029 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(WebSocket_t3268376029 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSENDASYNCU3EC__ANONSTOREY3_T2886884107_H
#ifndef U3CSTARTRECEIVINGU3EC__ANONSTOREY4_T506990866_H
#define U3CSTARTRECEIVINGU3EC__ANONSTOREY4_T506990866_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.WebSocket/<startReceiving>c__AnonStorey4
struct  U3CstartReceivingU3Ec__AnonStorey4_t506990866  : public RuntimeObject
{
public:
	// System.Action WebSocketSharp.WebSocket/<startReceiving>c__AnonStorey4::receive
	Action_t3226471752 * ___receive_0;
	// WebSocketSharp.WebSocket WebSocketSharp.WebSocket/<startReceiving>c__AnonStorey4::$this
	WebSocket_t3268376029 * ___U24this_1;

public:
	inline static int32_t get_offset_of_receive_0() { return static_cast<int32_t>(offsetof(U3CstartReceivingU3Ec__AnonStorey4_t506990866, ___receive_0)); }
	inline Action_t3226471752 * get_receive_0() const { return ___receive_0; }
	inline Action_t3226471752 ** get_address_of_receive_0() { return &___receive_0; }
	inline void set_receive_0(Action_t3226471752 * value)
	{
		___receive_0 = value;
		Il2CppCodeGenWriteBarrier((&___receive_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CstartReceivingU3Ec__AnonStorey4_t506990866, ___U24this_1)); }
	inline WebSocket_t3268376029 * get_U24this_1() const { return ___U24this_1; }
	inline WebSocket_t3268376029 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(WebSocket_t3268376029 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTRECEIVINGU3EC__ANONSTOREY4_T506990866_H
#ifndef U3CCONNECTASYNCU3EC__ANONSTOREY8_T2398335890_H
#define U3CCONNECTASYNCU3EC__ANONSTOREY8_T2398335890_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.WebSocket/<ConnectAsync>c__AnonStorey8
struct  U3CConnectAsyncU3Ec__AnonStorey8_t2398335890  : public RuntimeObject
{
public:
	// System.Func`1<System.Boolean> WebSocketSharp.WebSocket/<ConnectAsync>c__AnonStorey8::connector
	Func_1_t1485000104 * ___connector_0;
	// WebSocketSharp.WebSocket WebSocketSharp.WebSocket/<ConnectAsync>c__AnonStorey8::$this
	WebSocket_t3268376029 * ___U24this_1;

public:
	inline static int32_t get_offset_of_connector_0() { return static_cast<int32_t>(offsetof(U3CConnectAsyncU3Ec__AnonStorey8_t2398335890, ___connector_0)); }
	inline Func_1_t1485000104 * get_connector_0() const { return ___connector_0; }
	inline Func_1_t1485000104 ** get_address_of_connector_0() { return &___connector_0; }
	inline void set_connector_0(Func_1_t1485000104 * value)
	{
		___connector_0 = value;
		Il2CppCodeGenWriteBarrier((&___connector_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CConnectAsyncU3Ec__AnonStorey8_t2398335890, ___U24this_1)); }
	inline WebSocket_t3268376029 * get_U24this_1() const { return ___U24this_1; }
	inline WebSocket_t3268376029 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(WebSocket_t3268376029 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCONNECTASYNCU3EC__ANONSTOREY8_T2398335890_H
#ifndef U3CSENDASYNCU3EC__ANONSTOREY9_T1216469897_H
#define U3CSENDASYNCU3EC__ANONSTOREY9_T1216469897_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.WebSocket/<SendAsync>c__AnonStorey9
struct  U3CSendAsyncU3Ec__AnonStorey9_t1216469897  : public RuntimeObject
{
public:
	// System.Int32 WebSocketSharp.WebSocket/<SendAsync>c__AnonStorey9::length
	int32_t ___length_0;
	// System.Action`1<System.Boolean> WebSocketSharp.WebSocket/<SendAsync>c__AnonStorey9::completed
	Action_1_t3627374100 * ___completed_1;
	// WebSocketSharp.WebSocket WebSocketSharp.WebSocket/<SendAsync>c__AnonStorey9::$this
	WebSocket_t3268376029 * ___U24this_2;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(U3CSendAsyncU3Ec__AnonStorey9_t1216469897, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_completed_1() { return static_cast<int32_t>(offsetof(U3CSendAsyncU3Ec__AnonStorey9_t1216469897, ___completed_1)); }
	inline Action_1_t3627374100 * get_completed_1() const { return ___completed_1; }
	inline Action_1_t3627374100 ** get_address_of_completed_1() { return &___completed_1; }
	inline void set_completed_1(Action_1_t3627374100 * value)
	{
		___completed_1 = value;
		Il2CppCodeGenWriteBarrier((&___completed_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CSendAsyncU3Ec__AnonStorey9_t1216469897, ___U24this_2)); }
	inline WebSocket_t3268376029 * get_U24this_2() const { return ___U24this_2; }
	inline WebSocket_t3268376029 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(WebSocket_t3268376029 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSENDASYNCU3EC__ANONSTOREY9_T1216469897_H
#ifndef U3CVALIDATESECWEBSOCKETPROTOCOLSERVERHEADERU3EC__ANONSTOREY6_T1655596450_H
#define U3CVALIDATESECWEBSOCKETPROTOCOLSERVERHEADERU3EC__ANONSTOREY6_T1655596450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.WebSocket/<validateSecWebSocketProtocolServerHeader>c__AnonStorey6
struct  U3CvalidateSecWebSocketProtocolServerHeaderU3Ec__AnonStorey6_t1655596450  : public RuntimeObject
{
public:
	// System.String WebSocketSharp.WebSocket/<validateSecWebSocketProtocolServerHeader>c__AnonStorey6::value
	String_t* ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(U3CvalidateSecWebSocketProtocolServerHeaderU3Ec__AnonStorey6_t1655596450, ___value_0)); }
	inline String_t* get_value_0() const { return ___value_0; }
	inline String_t** get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(String_t* value)
	{
		___value_0 = value;
		Il2CppCodeGenWriteBarrier((&___value_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CVALIDATESECWEBSOCKETPROTOCOLSERVERHEADERU3EC__ANONSTOREY6_T1655596450_H
#ifndef U3CACCEPTASYNCU3EC__ANONSTOREY7_T3474718123_H
#define U3CACCEPTASYNCU3EC__ANONSTOREY7_T3474718123_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.WebSocket/<AcceptAsync>c__AnonStorey7
struct  U3CAcceptAsyncU3Ec__AnonStorey7_t3474718123  : public RuntimeObject
{
public:
	// System.Func`1<System.Boolean> WebSocketSharp.WebSocket/<AcceptAsync>c__AnonStorey7::acceptor
	Func_1_t1485000104 * ___acceptor_0;
	// WebSocketSharp.WebSocket WebSocketSharp.WebSocket/<AcceptAsync>c__AnonStorey7::$this
	WebSocket_t3268376029 * ___U24this_1;

public:
	inline static int32_t get_offset_of_acceptor_0() { return static_cast<int32_t>(offsetof(U3CAcceptAsyncU3Ec__AnonStorey7_t3474718123, ___acceptor_0)); }
	inline Func_1_t1485000104 * get_acceptor_0() const { return ___acceptor_0; }
	inline Func_1_t1485000104 ** get_address_of_acceptor_0() { return &___acceptor_0; }
	inline void set_acceptor_0(Func_1_t1485000104 * value)
	{
		___acceptor_0 = value;
		Il2CppCodeGenWriteBarrier((&___acceptor_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CAcceptAsyncU3Ec__AnonStorey7_t3474718123, ___U24this_1)); }
	inline WebSocket_t3268376029 * get_U24this_1() const { return ___U24this_1; }
	inline WebSocket_t3268376029 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(WebSocket_t3268376029 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CACCEPTASYNCU3EC__ANONSTOREY7_T3474718123_H
#ifndef U3CBROADCASTASYNCU3EC__ANONSTOREY2_T177987654_H
#define U3CBROADCASTASYNCU3EC__ANONSTOREY2_T177987654_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Server.WebSocketServiceManager/<BroadcastAsync>c__AnonStorey2
struct  U3CBroadcastAsyncU3Ec__AnonStorey2_t177987654  : public RuntimeObject
{
public:
	// System.Int32 WebSocketSharp.Server.WebSocketServiceManager/<BroadcastAsync>c__AnonStorey2::length
	int32_t ___length_0;
	// System.Action WebSocketSharp.Server.WebSocketServiceManager/<BroadcastAsync>c__AnonStorey2::completed
	Action_t3226471752 * ___completed_1;
	// WebSocketSharp.Server.WebSocketServiceManager WebSocketSharp.Server.WebSocketServiceManager/<BroadcastAsync>c__AnonStorey2::$this
	WebSocketServiceManager_t1683165547 * ___U24this_2;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(U3CBroadcastAsyncU3Ec__AnonStorey2_t177987654, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_completed_1() { return static_cast<int32_t>(offsetof(U3CBroadcastAsyncU3Ec__AnonStorey2_t177987654, ___completed_1)); }
	inline Action_t3226471752 * get_completed_1() const { return ___completed_1; }
	inline Action_t3226471752 ** get_address_of_completed_1() { return &___completed_1; }
	inline void set_completed_1(Action_t3226471752 * value)
	{
		___completed_1 = value;
		Il2CppCodeGenWriteBarrier((&___completed_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CBroadcastAsyncU3Ec__AnonStorey2_t177987654, ___U24this_2)); }
	inline WebSocketServiceManager_t1683165547 * get_U24this_2() const { return ___U24this_2; }
	inline WebSocketServiceManager_t1683165547 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(WebSocketServiceManager_t1683165547 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CBROADCASTASYNCU3EC__ANONSTOREY2_T177987654_H
#ifndef WEBSOCKETSERVICEHOST_T492106494_H
#define WEBSOCKETSERVICEHOST_T492106494_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Server.WebSocketServiceHost
struct  WebSocketServiceHost_t492106494  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBSOCKETSERVICEHOST_T492106494_H
#ifndef U3CRECEIVEREQUESTU3EC__ANONSTOREY0_T1358907818_H
#define U3CRECEIVEREQUESTU3EC__ANONSTOREY0_T1358907818_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Server.WebSocketServer/<receiveRequest>c__AnonStorey0
struct  U3CreceiveRequestU3Ec__AnonStorey0_t1358907818  : public RuntimeObject
{
public:
	// System.Net.Sockets.TcpClient WebSocketSharp.Server.WebSocketServer/<receiveRequest>c__AnonStorey0::cl
	TcpClient_t408947970 * ___cl_0;
	// WebSocketSharp.Server.WebSocketServer WebSocketSharp.Server.WebSocketServer/<receiveRequest>c__AnonStorey0::$this
	WebSocketServer_t2522399002 * ___U24this_1;

public:
	inline static int32_t get_offset_of_cl_0() { return static_cast<int32_t>(offsetof(U3CreceiveRequestU3Ec__AnonStorey0_t1358907818, ___cl_0)); }
	inline TcpClient_t408947970 * get_cl_0() const { return ___cl_0; }
	inline TcpClient_t408947970 ** get_address_of_cl_0() { return &___cl_0; }
	inline void set_cl_0(TcpClient_t408947970 * value)
	{
		___cl_0 = value;
		Il2CppCodeGenWriteBarrier((&___cl_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CreceiveRequestU3Ec__AnonStorey0_t1358907818, ___U24this_1)); }
	inline WebSocketServer_t2522399002 * get_U24this_1() const { return ___U24this_1; }
	inline WebSocketServer_t2522399002 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(WebSocketServer_t2522399002 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRECEIVEREQUESTU3EC__ANONSTOREY0_T1358907818_H
#ifndef U3CBROADCASTASYNCU3EC__ANONSTOREY4_T824600657_H
#define U3CBROADCASTASYNCU3EC__ANONSTOREY4_T824600657_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Server.WebSocketSessionManager/<BroadcastAsync>c__AnonStorey4
struct  U3CBroadcastAsyncU3Ec__AnonStorey4_t824600657  : public RuntimeObject
{
public:
	// System.Int32 WebSocketSharp.Server.WebSocketSessionManager/<BroadcastAsync>c__AnonStorey4::length
	int32_t ___length_0;
	// System.Action WebSocketSharp.Server.WebSocketSessionManager/<BroadcastAsync>c__AnonStorey4::completed
	Action_t3226471752 * ___completed_1;
	// WebSocketSharp.Server.WebSocketSessionManager WebSocketSharp.Server.WebSocketSessionManager/<BroadcastAsync>c__AnonStorey4::$this
	WebSocketSessionManager_t2802512518 * ___U24this_2;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(U3CBroadcastAsyncU3Ec__AnonStorey4_t824600657, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_completed_1() { return static_cast<int32_t>(offsetof(U3CBroadcastAsyncU3Ec__AnonStorey4_t824600657, ___completed_1)); }
	inline Action_t3226471752 * get_completed_1() const { return ___completed_1; }
	inline Action_t3226471752 ** get_address_of_completed_1() { return &___completed_1; }
	inline void set_completed_1(Action_t3226471752 * value)
	{
		___completed_1 = value;
		Il2CppCodeGenWriteBarrier((&___completed_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CBroadcastAsyncU3Ec__AnonStorey4_t824600657, ___U24this_2)); }
	inline WebSocketSessionManager_t2802512518 * get_U24this_2() const { return ___U24this_2; }
	inline WebSocketSessionManager_t2802512518 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(WebSocketSessionManager_t2802512518 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CBROADCASTASYNCU3EC__ANONSTOREY4_T824600657_H
#ifndef U3CMESSAGESU3EC__ANONSTOREY2_T2960445386_H
#define U3CMESSAGESU3EC__ANONSTOREY2_T2960445386_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.WebSocket/<messages>c__AnonStorey2
struct  U3CmessagesU3Ec__AnonStorey2_t2960445386  : public RuntimeObject
{
public:
	// WebSocketSharp.MessageEventArgs WebSocketSharp.WebSocket/<messages>c__AnonStorey2::e
	MessageEventArgs_t2890051726 * ___e_0;
	// WebSocketSharp.WebSocket WebSocketSharp.WebSocket/<messages>c__AnonStorey2::$this
	WebSocket_t3268376029 * ___U24this_1;

public:
	inline static int32_t get_offset_of_e_0() { return static_cast<int32_t>(offsetof(U3CmessagesU3Ec__AnonStorey2_t2960445386, ___e_0)); }
	inline MessageEventArgs_t2890051726 * get_e_0() const { return ___e_0; }
	inline MessageEventArgs_t2890051726 ** get_address_of_e_0() { return &___e_0; }
	inline void set_e_0(MessageEventArgs_t2890051726 * value)
	{
		___e_0 = value;
		Il2CppCodeGenWriteBarrier((&___e_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CmessagesU3Ec__AnonStorey2_t2960445386, ___U24this_1)); }
	inline WebSocket_t3268376029 * get_U24this_1() const { return ___U24this_1; }
	inline WebSocket_t3268376029 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(WebSocket_t3268376029 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMESSAGESU3EC__ANONSTOREY2_T2960445386_H
#ifndef U3CCLOSEASYNCU3EC__ANONSTOREY1_T144572335_H
#define U3CCLOSEASYNCU3EC__ANONSTOREY1_T144572335_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.WebSocket/<closeAsync>c__AnonStorey1
struct  U3CcloseAsyncU3Ec__AnonStorey1_t144572335  : public RuntimeObject
{
public:
	// System.Action`4<WebSocketSharp.CloseEventArgs,System.Boolean,System.Boolean,System.Boolean> WebSocketSharp.WebSocket/<closeAsync>c__AnonStorey1::closer
	Action_4_t3723770032 * ___closer_0;

public:
	inline static int32_t get_offset_of_closer_0() { return static_cast<int32_t>(offsetof(U3CcloseAsyncU3Ec__AnonStorey1_t144572335, ___closer_0)); }
	inline Action_4_t3723770032 * get_closer_0() const { return ___closer_0; }
	inline Action_4_t3723770032 ** get_address_of_closer_0() { return &___closer_0; }
	inline void set_closer_0(Action_4_t3723770032 * value)
	{
		___closer_0 = value;
		Il2CppCodeGenWriteBarrier((&___closer_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCLOSEASYNCU3EC__ANONSTOREY1_T144572335_H
#ifndef U3CU3EC__ITERATOR0_T147269308_H
#define U3CU3EC__ITERATOR0_T147269308_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.WebSocket/<>c__Iterator0
struct  U3CU3Ec__Iterator0_t147269308  : public RuntimeObject
{
public:
	// System.Object WebSocketSharp.WebSocket/<>c__Iterator0::$locvar0
	RuntimeObject * ___U24locvar0_0;
	// System.Collections.IEnumerator WebSocketSharp.WebSocket/<>c__Iterator0::$locvar1
	RuntimeObject* ___U24locvar1_1;
	// WebSocketSharp.Net.Cookie WebSocketSharp.WebSocket/<>c__Iterator0::<cookie>__1
	Cookie_t1826188460 * ___U3CcookieU3E__1_2;
	// System.IDisposable WebSocketSharp.WebSocket/<>c__Iterator0::$locvar2
	RuntimeObject* ___U24locvar2_3;
	// WebSocketSharp.WebSocket WebSocketSharp.WebSocket/<>c__Iterator0::$this
	WebSocket_t3268376029 * ___U24this_4;
	// WebSocketSharp.Net.Cookie WebSocketSharp.WebSocket/<>c__Iterator0::$current
	Cookie_t1826188460 * ___U24current_5;
	// System.Boolean WebSocketSharp.WebSocket/<>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 WebSocketSharp.WebSocket/<>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U24locvar0_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t147269308, ___U24locvar0_0)); }
	inline RuntimeObject * get_U24locvar0_0() const { return ___U24locvar0_0; }
	inline RuntimeObject ** get_address_of_U24locvar0_0() { return &___U24locvar0_0; }
	inline void set_U24locvar0_0(RuntimeObject * value)
	{
		___U24locvar0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_0), value);
	}

	inline static int32_t get_offset_of_U24locvar1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t147269308, ___U24locvar1_1)); }
	inline RuntimeObject* get_U24locvar1_1() const { return ___U24locvar1_1; }
	inline RuntimeObject** get_address_of_U24locvar1_1() { return &___U24locvar1_1; }
	inline void set_U24locvar1_1(RuntimeObject* value)
	{
		___U24locvar1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar1_1), value);
	}

	inline static int32_t get_offset_of_U3CcookieU3E__1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t147269308, ___U3CcookieU3E__1_2)); }
	inline Cookie_t1826188460 * get_U3CcookieU3E__1_2() const { return ___U3CcookieU3E__1_2; }
	inline Cookie_t1826188460 ** get_address_of_U3CcookieU3E__1_2() { return &___U3CcookieU3E__1_2; }
	inline void set_U3CcookieU3E__1_2(Cookie_t1826188460 * value)
	{
		___U3CcookieU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcookieU3E__1_2), value);
	}

	inline static int32_t get_offset_of_U24locvar2_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t147269308, ___U24locvar2_3)); }
	inline RuntimeObject* get_U24locvar2_3() const { return ___U24locvar2_3; }
	inline RuntimeObject** get_address_of_U24locvar2_3() { return &___U24locvar2_3; }
	inline void set_U24locvar2_3(RuntimeObject* value)
	{
		___U24locvar2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar2_3), value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t147269308, ___U24this_4)); }
	inline WebSocket_t3268376029 * get_U24this_4() const { return ___U24this_4; }
	inline WebSocket_t3268376029 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(WebSocket_t3268376029 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t147269308, ___U24current_5)); }
	inline Cookie_t1826188460 * get_U24current_5() const { return ___U24current_5; }
	inline Cookie_t1826188460 ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(Cookie_t1826188460 * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t147269308, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t147269308, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__ITERATOR0_T147269308_H
#ifndef STREAM_T3255436806_H
#define STREAM_T3255436806_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream
struct  Stream_t3255436806  : public RuntimeObject
{
public:

public:
};

struct Stream_t3255436806_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_t3255436806 * ___Null_0;

public:
	inline static int32_t get_offset_of_Null_0() { return static_cast<int32_t>(offsetof(Stream_t3255436806_StaticFields, ___Null_0)); }
	inline Stream_t3255436806 * get_Null_0() const { return ___Null_0; }
	inline Stream_t3255436806 ** get_address_of_Null_0() { return &___Null_0; }
	inline void set_Null_0(Stream_t3255436806 * value)
	{
		___Null_0 = value;
		Il2CppCodeGenWriteBarrier((&___Null_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAM_T3255436806_H
#ifndef U3CFOLLOWSCRIPTU3EC__ITERATOR0_T3449319141_H
#define U3CFOLLOWSCRIPTU3EC__ITERATOR0_T3449319141_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VikingCrewTools.ScriptedDialogueBehaviour/<FollowScript>c__Iterator0
struct  U3CFollowScriptU3Ec__Iterator0_t3449319141  : public RuntimeObject
{
public:
	// System.Int32 VikingCrewTools.ScriptedDialogueBehaviour/<FollowScript>c__Iterator0::<index>__0
	int32_t ___U3CindexU3E__0_0;
	// VikingCrewTools.ScriptedDialogueBehaviour VikingCrewTools.ScriptedDialogueBehaviour/<FollowScript>c__Iterator0::$this
	ScriptedDialogueBehaviour_t2807853473 * ___U24this_1;
	// System.Object VikingCrewTools.ScriptedDialogueBehaviour/<FollowScript>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean VikingCrewTools.ScriptedDialogueBehaviour/<FollowScript>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 VikingCrewTools.ScriptedDialogueBehaviour/<FollowScript>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CindexU3E__0_0() { return static_cast<int32_t>(offsetof(U3CFollowScriptU3Ec__Iterator0_t3449319141, ___U3CindexU3E__0_0)); }
	inline int32_t get_U3CindexU3E__0_0() const { return ___U3CindexU3E__0_0; }
	inline int32_t* get_address_of_U3CindexU3E__0_0() { return &___U3CindexU3E__0_0; }
	inline void set_U3CindexU3E__0_0(int32_t value)
	{
		___U3CindexU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CFollowScriptU3Ec__Iterator0_t3449319141, ___U24this_1)); }
	inline ScriptedDialogueBehaviour_t2807853473 * get_U24this_1() const { return ___U24this_1; }
	inline ScriptedDialogueBehaviour_t2807853473 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(ScriptedDialogueBehaviour_t2807853473 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CFollowScriptU3Ec__Iterator0_t3449319141, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CFollowScriptU3Ec__Iterator0_t3449319141, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CFollowScriptU3Ec__Iterator0_t3449319141, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFOLLOWSCRIPTU3EC__ITERATOR0_T3449319141_H
#ifndef U3CGETENUMERATORU3EC__ITERATOR0_T908706979_H
#define U3CGETENUMERATORU3EC__ITERATOR0_T908706979_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.WebSocketFrame/<GetEnumerator>c__Iterator0
struct  U3CGetEnumeratorU3Ec__Iterator0_t908706979  : public RuntimeObject
{
public:
	// System.Byte[] WebSocketSharp.WebSocketFrame/<GetEnumerator>c__Iterator0::$locvar0
	ByteU5BU5D_t3397334013* ___U24locvar0_0;
	// System.Int32 WebSocketSharp.WebSocketFrame/<GetEnumerator>c__Iterator0::$locvar1
	int32_t ___U24locvar1_1;
	// System.Byte WebSocketSharp.WebSocketFrame/<GetEnumerator>c__Iterator0::<b>__1
	uint8_t ___U3CbU3E__1_2;
	// WebSocketSharp.WebSocketFrame WebSocketSharp.WebSocketFrame/<GetEnumerator>c__Iterator0::$this
	WebSocketFrame_t764750278 * ___U24this_3;
	// System.Byte WebSocketSharp.WebSocketFrame/<GetEnumerator>c__Iterator0::$current
	uint8_t ___U24current_4;
	// System.Boolean WebSocketSharp.WebSocketFrame/<GetEnumerator>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 WebSocketSharp.WebSocketFrame/<GetEnumerator>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U24locvar0_0() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator0_t908706979, ___U24locvar0_0)); }
	inline ByteU5BU5D_t3397334013* get_U24locvar0_0() const { return ___U24locvar0_0; }
	inline ByteU5BU5D_t3397334013** get_address_of_U24locvar0_0() { return &___U24locvar0_0; }
	inline void set_U24locvar0_0(ByteU5BU5D_t3397334013* value)
	{
		___U24locvar0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_0), value);
	}

	inline static int32_t get_offset_of_U24locvar1_1() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator0_t908706979, ___U24locvar1_1)); }
	inline int32_t get_U24locvar1_1() const { return ___U24locvar1_1; }
	inline int32_t* get_address_of_U24locvar1_1() { return &___U24locvar1_1; }
	inline void set_U24locvar1_1(int32_t value)
	{
		___U24locvar1_1 = value;
	}

	inline static int32_t get_offset_of_U3CbU3E__1_2() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator0_t908706979, ___U3CbU3E__1_2)); }
	inline uint8_t get_U3CbU3E__1_2() const { return ___U3CbU3E__1_2; }
	inline uint8_t* get_address_of_U3CbU3E__1_2() { return &___U3CbU3E__1_2; }
	inline void set_U3CbU3E__1_2(uint8_t value)
	{
		___U3CbU3E__1_2 = value;
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator0_t908706979, ___U24this_3)); }
	inline WebSocketFrame_t764750278 * get_U24this_3() const { return ___U24this_3; }
	inline WebSocketFrame_t764750278 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(WebSocketFrame_t764750278 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator0_t908706979, ___U24current_4)); }
	inline uint8_t get_U24current_4() const { return ___U24current_4; }
	inline uint8_t* get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(uint8_t value)
	{
		___U24current_4 = value;
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator0_t908706979, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator0_t908706979, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETENUMERATORU3EC__ITERATOR0_T908706979_H
#ifndef EVENTARGS_T3289624707_H
#define EVENTARGS_T3289624707_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventArgs
struct  EventArgs_t3289624707  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t3289624707_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t3289624707 * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t3289624707_StaticFields, ___Empty_0)); }
	inline EventArgs_t3289624707 * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t3289624707 ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t3289624707 * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTARGS_T3289624707_H
#ifndef NAMEOBJECTCOLLECTIONBASE_T2034248631_H
#define NAMEOBJECTCOLLECTIONBASE_T2034248631_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Specialized.NameObjectCollectionBase
struct  NameObjectCollectionBase_t2034248631  : public RuntimeObject
{
public:
	// System.Collections.Hashtable System.Collections.Specialized.NameObjectCollectionBase::m_ItemsContainer
	Hashtable_t909839986 * ___m_ItemsContainer_0;
	// System.Collections.Specialized.NameObjectCollectionBase/_Item System.Collections.Specialized.NameObjectCollectionBase::m_NullKeyItem
	_Item_t3244489099 * ___m_NullKeyItem_1;
	// System.Collections.ArrayList System.Collections.Specialized.NameObjectCollectionBase::m_ItemsArray
	ArrayList_t4252133567 * ___m_ItemsArray_2;
	// System.Collections.IHashCodeProvider System.Collections.Specialized.NameObjectCollectionBase::m_hashprovider
	RuntimeObject* ___m_hashprovider_3;
	// System.Collections.IComparer System.Collections.Specialized.NameObjectCollectionBase::m_comparer
	RuntimeObject* ___m_comparer_4;
	// System.Int32 System.Collections.Specialized.NameObjectCollectionBase::m_defCapacity
	int32_t ___m_defCapacity_5;
	// System.Boolean System.Collections.Specialized.NameObjectCollectionBase::m_readonly
	bool ___m_readonly_6;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Specialized.NameObjectCollectionBase::infoCopy
	SerializationInfo_t228987430 * ___infoCopy_7;
	// System.Collections.Specialized.NameObjectCollectionBase/KeysCollection System.Collections.Specialized.NameObjectCollectionBase::keyscoll
	KeysCollection_t633582367 * ___keyscoll_8;
	// System.Collections.IEqualityComparer System.Collections.Specialized.NameObjectCollectionBase::equality_comparer
	RuntimeObject* ___equality_comparer_9;

public:
	inline static int32_t get_offset_of_m_ItemsContainer_0() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2034248631, ___m_ItemsContainer_0)); }
	inline Hashtable_t909839986 * get_m_ItemsContainer_0() const { return ___m_ItemsContainer_0; }
	inline Hashtable_t909839986 ** get_address_of_m_ItemsContainer_0() { return &___m_ItemsContainer_0; }
	inline void set_m_ItemsContainer_0(Hashtable_t909839986 * value)
	{
		___m_ItemsContainer_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemsContainer_0), value);
	}

	inline static int32_t get_offset_of_m_NullKeyItem_1() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2034248631, ___m_NullKeyItem_1)); }
	inline _Item_t3244489099 * get_m_NullKeyItem_1() const { return ___m_NullKeyItem_1; }
	inline _Item_t3244489099 ** get_address_of_m_NullKeyItem_1() { return &___m_NullKeyItem_1; }
	inline void set_m_NullKeyItem_1(_Item_t3244489099 * value)
	{
		___m_NullKeyItem_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_NullKeyItem_1), value);
	}

	inline static int32_t get_offset_of_m_ItemsArray_2() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2034248631, ___m_ItemsArray_2)); }
	inline ArrayList_t4252133567 * get_m_ItemsArray_2() const { return ___m_ItemsArray_2; }
	inline ArrayList_t4252133567 ** get_address_of_m_ItemsArray_2() { return &___m_ItemsArray_2; }
	inline void set_m_ItemsArray_2(ArrayList_t4252133567 * value)
	{
		___m_ItemsArray_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemsArray_2), value);
	}

	inline static int32_t get_offset_of_m_hashprovider_3() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2034248631, ___m_hashprovider_3)); }
	inline RuntimeObject* get_m_hashprovider_3() const { return ___m_hashprovider_3; }
	inline RuntimeObject** get_address_of_m_hashprovider_3() { return &___m_hashprovider_3; }
	inline void set_m_hashprovider_3(RuntimeObject* value)
	{
		___m_hashprovider_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_hashprovider_3), value);
	}

	inline static int32_t get_offset_of_m_comparer_4() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2034248631, ___m_comparer_4)); }
	inline RuntimeObject* get_m_comparer_4() const { return ___m_comparer_4; }
	inline RuntimeObject** get_address_of_m_comparer_4() { return &___m_comparer_4; }
	inline void set_m_comparer_4(RuntimeObject* value)
	{
		___m_comparer_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_comparer_4), value);
	}

	inline static int32_t get_offset_of_m_defCapacity_5() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2034248631, ___m_defCapacity_5)); }
	inline int32_t get_m_defCapacity_5() const { return ___m_defCapacity_5; }
	inline int32_t* get_address_of_m_defCapacity_5() { return &___m_defCapacity_5; }
	inline void set_m_defCapacity_5(int32_t value)
	{
		___m_defCapacity_5 = value;
	}

	inline static int32_t get_offset_of_m_readonly_6() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2034248631, ___m_readonly_6)); }
	inline bool get_m_readonly_6() const { return ___m_readonly_6; }
	inline bool* get_address_of_m_readonly_6() { return &___m_readonly_6; }
	inline void set_m_readonly_6(bool value)
	{
		___m_readonly_6 = value;
	}

	inline static int32_t get_offset_of_infoCopy_7() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2034248631, ___infoCopy_7)); }
	inline SerializationInfo_t228987430 * get_infoCopy_7() const { return ___infoCopy_7; }
	inline SerializationInfo_t228987430 ** get_address_of_infoCopy_7() { return &___infoCopy_7; }
	inline void set_infoCopy_7(SerializationInfo_t228987430 * value)
	{
		___infoCopy_7 = value;
		Il2CppCodeGenWriteBarrier((&___infoCopy_7), value);
	}

	inline static int32_t get_offset_of_keyscoll_8() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2034248631, ___keyscoll_8)); }
	inline KeysCollection_t633582367 * get_keyscoll_8() const { return ___keyscoll_8; }
	inline KeysCollection_t633582367 ** get_address_of_keyscoll_8() { return &___keyscoll_8; }
	inline void set_keyscoll_8(KeysCollection_t633582367 * value)
	{
		___keyscoll_8 = value;
		Il2CppCodeGenWriteBarrier((&___keyscoll_8), value);
	}

	inline static int32_t get_offset_of_equality_comparer_9() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2034248631, ___equality_comparer_9)); }
	inline RuntimeObject* get_equality_comparer_9() const { return ___equality_comparer_9; }
	inline RuntimeObject** get_address_of_equality_comparer_9() { return &___equality_comparer_9; }
	inline void set_equality_comparer_9(RuntimeObject* value)
	{
		___equality_comparer_9 = value;
		Il2CppCodeGenWriteBarrier((&___equality_comparer_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMEOBJECTCOLLECTIONBASE_T2034248631_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef EXCEPTION_T1927440687_H
#define EXCEPTION_T1927440687_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t1927440687  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t169632028* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t1927440687 * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t169632028* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t169632028** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t169632028* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___inner_exception_1)); }
	inline Exception_t1927440687 * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t1927440687 ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t1927440687 * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T1927440687_H
#ifndef U3CREADEXTENDEDPAYLOADLENGTHASYNCU3EC__ANONSTOREY3_T1521887925_H
#define U3CREADEXTENDEDPAYLOADLENGTHASYNCU3EC__ANONSTOREY3_T1521887925_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.WebSocketFrame/<readExtendedPayloadLengthAsync>c__AnonStorey3
struct  U3CreadExtendedPayloadLengthAsyncU3Ec__AnonStorey3_t1521887925  : public RuntimeObject
{
public:
	// System.Int32 WebSocketSharp.WebSocketFrame/<readExtendedPayloadLengthAsync>c__AnonStorey3::len
	int32_t ___len_0;
	// WebSocketSharp.WebSocketFrame WebSocketSharp.WebSocketFrame/<readExtendedPayloadLengthAsync>c__AnonStorey3::frame
	WebSocketFrame_t764750278 * ___frame_1;
	// System.Action`1<WebSocketSharp.WebSocketFrame> WebSocketSharp.WebSocketFrame/<readExtendedPayloadLengthAsync>c__AnonStorey3::completed
	Action_1_t566549660 * ___completed_2;

public:
	inline static int32_t get_offset_of_len_0() { return static_cast<int32_t>(offsetof(U3CreadExtendedPayloadLengthAsyncU3Ec__AnonStorey3_t1521887925, ___len_0)); }
	inline int32_t get_len_0() const { return ___len_0; }
	inline int32_t* get_address_of_len_0() { return &___len_0; }
	inline void set_len_0(int32_t value)
	{
		___len_0 = value;
	}

	inline static int32_t get_offset_of_frame_1() { return static_cast<int32_t>(offsetof(U3CreadExtendedPayloadLengthAsyncU3Ec__AnonStorey3_t1521887925, ___frame_1)); }
	inline WebSocketFrame_t764750278 * get_frame_1() const { return ___frame_1; }
	inline WebSocketFrame_t764750278 ** get_address_of_frame_1() { return &___frame_1; }
	inline void set_frame_1(WebSocketFrame_t764750278 * value)
	{
		___frame_1 = value;
		Il2CppCodeGenWriteBarrier((&___frame_1), value);
	}

	inline static int32_t get_offset_of_completed_2() { return static_cast<int32_t>(offsetof(U3CreadExtendedPayloadLengthAsyncU3Ec__AnonStorey3_t1521887925, ___completed_2)); }
	inline Action_1_t566549660 * get_completed_2() const { return ___completed_2; }
	inline Action_1_t566549660 ** get_address_of_completed_2() { return &___completed_2; }
	inline void set_completed_2(Action_1_t566549660 * value)
	{
		___completed_2 = value;
		Il2CppCodeGenWriteBarrier((&___completed_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREADEXTENDEDPAYLOADLENGTHASYNCU3EC__ANONSTOREY3_T1521887925_H
#ifndef U3CDUMPU3EC__ANONSTOREY2_T1475667190_H
#define U3CDUMPU3EC__ANONSTOREY2_T1475667190_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.WebSocketFrame/<dump>c__AnonStorey1/<dump>c__AnonStorey2
struct  U3CdumpU3Ec__AnonStorey2_t1475667190  : public RuntimeObject
{
public:
	// System.Int64 WebSocketSharp.WebSocketFrame/<dump>c__AnonStorey1/<dump>c__AnonStorey2::lineCnt
	int64_t ___lineCnt_0;
	// WebSocketSharp.WebSocketFrame/<dump>c__AnonStorey1 WebSocketSharp.WebSocketFrame/<dump>c__AnonStorey1/<dump>c__AnonStorey2::<>f__ref$1
	U3CdumpU3Ec__AnonStorey1_t3897361314 * ___U3CU3Ef__refU241_1;

public:
	inline static int32_t get_offset_of_lineCnt_0() { return static_cast<int32_t>(offsetof(U3CdumpU3Ec__AnonStorey2_t1475667190, ___lineCnt_0)); }
	inline int64_t get_lineCnt_0() const { return ___lineCnt_0; }
	inline int64_t* get_address_of_lineCnt_0() { return &___lineCnt_0; }
	inline void set_lineCnt_0(int64_t value)
	{
		___lineCnt_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU241_1() { return static_cast<int32_t>(offsetof(U3CdumpU3Ec__AnonStorey2_t1475667190, ___U3CU3Ef__refU241_1)); }
	inline U3CdumpU3Ec__AnonStorey1_t3897361314 * get_U3CU3Ef__refU241_1() const { return ___U3CU3Ef__refU241_1; }
	inline U3CdumpU3Ec__AnonStorey1_t3897361314 ** get_address_of_U3CU3Ef__refU241_1() { return &___U3CU3Ef__refU241_1; }
	inline void set_U3CU3Ef__refU241_1(U3CdumpU3Ec__AnonStorey1_t3897361314 * value)
	{
		___U3CU3Ef__refU241_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU241_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDUMPU3EC__ANONSTOREY2_T1475667190_H
#ifndef U3CDUMPU3EC__ANONSTOREY1_T3897361314_H
#define U3CDUMPU3EC__ANONSTOREY1_T3897361314_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.WebSocketFrame/<dump>c__AnonStorey1
struct  U3CdumpU3Ec__AnonStorey1_t3897361314  : public RuntimeObject
{
public:
	// System.Text.StringBuilder WebSocketSharp.WebSocketFrame/<dump>c__AnonStorey1::output
	StringBuilder_t1221177846 * ___output_0;
	// System.String WebSocketSharp.WebSocketFrame/<dump>c__AnonStorey1::lineFmt
	String_t* ___lineFmt_1;

public:
	inline static int32_t get_offset_of_output_0() { return static_cast<int32_t>(offsetof(U3CdumpU3Ec__AnonStorey1_t3897361314, ___output_0)); }
	inline StringBuilder_t1221177846 * get_output_0() const { return ___output_0; }
	inline StringBuilder_t1221177846 ** get_address_of_output_0() { return &___output_0; }
	inline void set_output_0(StringBuilder_t1221177846 * value)
	{
		___output_0 = value;
		Il2CppCodeGenWriteBarrier((&___output_0), value);
	}

	inline static int32_t get_offset_of_lineFmt_1() { return static_cast<int32_t>(offsetof(U3CdumpU3Ec__AnonStorey1_t3897361314, ___lineFmt_1)); }
	inline String_t* get_lineFmt_1() const { return ___lineFmt_1; }
	inline String_t** get_address_of_lineFmt_1() { return &___lineFmt_1; }
	inline void set_lineFmt_1(String_t* value)
	{
		___lineFmt_1 = value;
		Il2CppCodeGenWriteBarrier((&___lineFmt_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDUMPU3EC__ANONSTOREY1_T3897361314_H
#ifndef U3CREADHEADERASYNCU3EC__ANONSTOREY4_T1882858074_H
#define U3CREADHEADERASYNCU3EC__ANONSTOREY4_T1882858074_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.WebSocketFrame/<readHeaderAsync>c__AnonStorey4
struct  U3CreadHeaderAsyncU3Ec__AnonStorey4_t1882858074  : public RuntimeObject
{
public:
	// System.Action`1<WebSocketSharp.WebSocketFrame> WebSocketSharp.WebSocketFrame/<readHeaderAsync>c__AnonStorey4::completed
	Action_1_t566549660 * ___completed_0;

public:
	inline static int32_t get_offset_of_completed_0() { return static_cast<int32_t>(offsetof(U3CreadHeaderAsyncU3Ec__AnonStorey4_t1882858074, ___completed_0)); }
	inline Action_1_t566549660 * get_completed_0() const { return ___completed_0; }
	inline Action_1_t566549660 ** get_address_of_completed_0() { return &___completed_0; }
	inline void set_completed_0(Action_1_t566549660 * value)
	{
		___completed_0 = value;
		Il2CppCodeGenWriteBarrier((&___completed_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREADHEADERASYNCU3EC__ANONSTOREY4_T1882858074_H
#ifndef U3CREADFRAMEASYNCU3EC__ANONSTOREY7_T8184881_H
#define U3CREADFRAMEASYNCU3EC__ANONSTOREY7_T8184881_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.WebSocketFrame/<ReadFrameAsync>c__AnonStorey7
struct  U3CReadFrameAsyncU3Ec__AnonStorey7_t8184881  : public RuntimeObject
{
public:
	// System.IO.Stream WebSocketSharp.WebSocketFrame/<ReadFrameAsync>c__AnonStorey7::stream
	Stream_t3255436806 * ___stream_0;
	// System.Action`1<System.Exception> WebSocketSharp.WebSocketFrame/<ReadFrameAsync>c__AnonStorey7::error
	Action_1_t1729240069 * ___error_1;
	// System.Boolean WebSocketSharp.WebSocketFrame/<ReadFrameAsync>c__AnonStorey7::unmask
	bool ___unmask_2;
	// System.Action`1<WebSocketSharp.WebSocketFrame> WebSocketSharp.WebSocketFrame/<ReadFrameAsync>c__AnonStorey7::completed
	Action_1_t566549660 * ___completed_3;

public:
	inline static int32_t get_offset_of_stream_0() { return static_cast<int32_t>(offsetof(U3CReadFrameAsyncU3Ec__AnonStorey7_t8184881, ___stream_0)); }
	inline Stream_t3255436806 * get_stream_0() const { return ___stream_0; }
	inline Stream_t3255436806 ** get_address_of_stream_0() { return &___stream_0; }
	inline void set_stream_0(Stream_t3255436806 * value)
	{
		___stream_0 = value;
		Il2CppCodeGenWriteBarrier((&___stream_0), value);
	}

	inline static int32_t get_offset_of_error_1() { return static_cast<int32_t>(offsetof(U3CReadFrameAsyncU3Ec__AnonStorey7_t8184881, ___error_1)); }
	inline Action_1_t1729240069 * get_error_1() const { return ___error_1; }
	inline Action_1_t1729240069 ** get_address_of_error_1() { return &___error_1; }
	inline void set_error_1(Action_1_t1729240069 * value)
	{
		___error_1 = value;
		Il2CppCodeGenWriteBarrier((&___error_1), value);
	}

	inline static int32_t get_offset_of_unmask_2() { return static_cast<int32_t>(offsetof(U3CReadFrameAsyncU3Ec__AnonStorey7_t8184881, ___unmask_2)); }
	inline bool get_unmask_2() const { return ___unmask_2; }
	inline bool* get_address_of_unmask_2() { return &___unmask_2; }
	inline void set_unmask_2(bool value)
	{
		___unmask_2 = value;
	}

	inline static int32_t get_offset_of_completed_3() { return static_cast<int32_t>(offsetof(U3CReadFrameAsyncU3Ec__AnonStorey7_t8184881, ___completed_3)); }
	inline Action_1_t566549660 * get_completed_3() const { return ___completed_3; }
	inline Action_1_t566549660 ** get_address_of_completed_3() { return &___completed_3; }
	inline void set_completed_3(Action_1_t566549660 * value)
	{
		___completed_3 = value;
		Il2CppCodeGenWriteBarrier((&___completed_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREADFRAMEASYNCU3EC__ANONSTOREY7_T8184881_H
#ifndef U3CREADPAYLOADDATAASYNCU3EC__ANONSTOREY6_T983838767_H
#define U3CREADPAYLOADDATAASYNCU3EC__ANONSTOREY6_T983838767_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.WebSocketFrame/<readPayloadDataAsync>c__AnonStorey6
struct  U3CreadPayloadDataAsyncU3Ec__AnonStorey6_t983838767  : public RuntimeObject
{
public:
	// System.Int64 WebSocketSharp.WebSocketFrame/<readPayloadDataAsync>c__AnonStorey6::llen
	int64_t ___llen_0;
	// WebSocketSharp.WebSocketFrame WebSocketSharp.WebSocketFrame/<readPayloadDataAsync>c__AnonStorey6::frame
	WebSocketFrame_t764750278 * ___frame_1;
	// System.Action`1<WebSocketSharp.WebSocketFrame> WebSocketSharp.WebSocketFrame/<readPayloadDataAsync>c__AnonStorey6::completed
	Action_1_t566549660 * ___completed_2;

public:
	inline static int32_t get_offset_of_llen_0() { return static_cast<int32_t>(offsetof(U3CreadPayloadDataAsyncU3Ec__AnonStorey6_t983838767, ___llen_0)); }
	inline int64_t get_llen_0() const { return ___llen_0; }
	inline int64_t* get_address_of_llen_0() { return &___llen_0; }
	inline void set_llen_0(int64_t value)
	{
		___llen_0 = value;
	}

	inline static int32_t get_offset_of_frame_1() { return static_cast<int32_t>(offsetof(U3CreadPayloadDataAsyncU3Ec__AnonStorey6_t983838767, ___frame_1)); }
	inline WebSocketFrame_t764750278 * get_frame_1() const { return ___frame_1; }
	inline WebSocketFrame_t764750278 ** get_address_of_frame_1() { return &___frame_1; }
	inline void set_frame_1(WebSocketFrame_t764750278 * value)
	{
		___frame_1 = value;
		Il2CppCodeGenWriteBarrier((&___frame_1), value);
	}

	inline static int32_t get_offset_of_completed_2() { return static_cast<int32_t>(offsetof(U3CreadPayloadDataAsyncU3Ec__AnonStorey6_t983838767, ___completed_2)); }
	inline Action_1_t566549660 * get_completed_2() const { return ___completed_2; }
	inline Action_1_t566549660 ** get_address_of_completed_2() { return &___completed_2; }
	inline void set_completed_2(Action_1_t566549660 * value)
	{
		___completed_2 = value;
		Il2CppCodeGenWriteBarrier((&___completed_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREADPAYLOADDATAASYNCU3EC__ANONSTOREY6_T983838767_H
#ifndef U3CREADMASKINGKEYASYNCU3EC__ANONSTOREY5_T3451788521_H
#define U3CREADMASKINGKEYASYNCU3EC__ANONSTOREY5_T3451788521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.WebSocketFrame/<readMaskingKeyAsync>c__AnonStorey5
struct  U3CreadMaskingKeyAsyncU3Ec__AnonStorey5_t3451788521  : public RuntimeObject
{
public:
	// System.Int32 WebSocketSharp.WebSocketFrame/<readMaskingKeyAsync>c__AnonStorey5::len
	int32_t ___len_0;
	// WebSocketSharp.WebSocketFrame WebSocketSharp.WebSocketFrame/<readMaskingKeyAsync>c__AnonStorey5::frame
	WebSocketFrame_t764750278 * ___frame_1;
	// System.Action`1<WebSocketSharp.WebSocketFrame> WebSocketSharp.WebSocketFrame/<readMaskingKeyAsync>c__AnonStorey5::completed
	Action_1_t566549660 * ___completed_2;

public:
	inline static int32_t get_offset_of_len_0() { return static_cast<int32_t>(offsetof(U3CreadMaskingKeyAsyncU3Ec__AnonStorey5_t3451788521, ___len_0)); }
	inline int32_t get_len_0() const { return ___len_0; }
	inline int32_t* get_address_of_len_0() { return &___len_0; }
	inline void set_len_0(int32_t value)
	{
		___len_0 = value;
	}

	inline static int32_t get_offset_of_frame_1() { return static_cast<int32_t>(offsetof(U3CreadMaskingKeyAsyncU3Ec__AnonStorey5_t3451788521, ___frame_1)); }
	inline WebSocketFrame_t764750278 * get_frame_1() const { return ___frame_1; }
	inline WebSocketFrame_t764750278 ** get_address_of_frame_1() { return &___frame_1; }
	inline void set_frame_1(WebSocketFrame_t764750278 * value)
	{
		___frame_1 = value;
		Il2CppCodeGenWriteBarrier((&___frame_1), value);
	}

	inline static int32_t get_offset_of_completed_2() { return static_cast<int32_t>(offsetof(U3CreadMaskingKeyAsyncU3Ec__AnonStorey5_t3451788521, ___completed_2)); }
	inline Action_1_t566549660 * get_completed_2() const { return ___completed_2; }
	inline Action_1_t566549660 ** get_address_of_completed_2() { return &___completed_2; }
	inline void set_completed_2(Action_1_t566549660 * value)
	{
		___completed_2 = value;
		Il2CppCodeGenWriteBarrier((&___completed_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREADMASKINGKEYASYNCU3EC__ANONSTOREY5_T3451788521_H
#ifndef U3CRECEIVEREQUESTU3EC__ANONSTOREY0_T2614482259_H
#define U3CRECEIVEREQUESTU3EC__ANONSTOREY0_T2614482259_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Server.HttpServer/<receiveRequest>c__AnonStorey0
struct  U3CreceiveRequestU3Ec__AnonStorey0_t2614482259  : public RuntimeObject
{
public:
	// WebSocketSharp.Net.HttpListenerContext WebSocketSharp.Server.HttpServer/<receiveRequest>c__AnonStorey0::ctx
	HttpListenerContext_t994708409 * ___ctx_0;
	// WebSocketSharp.Server.HttpServer WebSocketSharp.Server.HttpServer/<receiveRequest>c__AnonStorey0::$this
	HttpServer_t490574305 * ___U24this_1;

public:
	inline static int32_t get_offset_of_ctx_0() { return static_cast<int32_t>(offsetof(U3CreceiveRequestU3Ec__AnonStorey0_t2614482259, ___ctx_0)); }
	inline HttpListenerContext_t994708409 * get_ctx_0() const { return ___ctx_0; }
	inline HttpListenerContext_t994708409 ** get_address_of_ctx_0() { return &___ctx_0; }
	inline void set_ctx_0(HttpListenerContext_t994708409 * value)
	{
		___ctx_0 = value;
		Il2CppCodeGenWriteBarrier((&___ctx_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CreceiveRequestU3Ec__AnonStorey0_t2614482259, ___U24this_1)); }
	inline HttpServer_t490574305 * get_U24this_1() const { return ___U24this_1; }
	inline HttpServer_t490574305 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(HttpServer_t490574305 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRECEIVEREQUESTU3EC__ANONSTOREY0_T2614482259_H
#ifndef HTTPVERSION_T4270509666_H
#define HTTPVERSION_T4270509666_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.HttpVersion
struct  HttpVersion_t4270509666  : public RuntimeObject
{
public:

public:
};

struct HttpVersion_t4270509666_StaticFields
{
public:
	// System.Version WebSocketSharp.Net.HttpVersion::Version10
	Version_t1755874712 * ___Version10_0;
	// System.Version WebSocketSharp.Net.HttpVersion::Version11
	Version_t1755874712 * ___Version11_1;

public:
	inline static int32_t get_offset_of_Version10_0() { return static_cast<int32_t>(offsetof(HttpVersion_t4270509666_StaticFields, ___Version10_0)); }
	inline Version_t1755874712 * get_Version10_0() const { return ___Version10_0; }
	inline Version_t1755874712 ** get_address_of_Version10_0() { return &___Version10_0; }
	inline void set_Version10_0(Version_t1755874712 * value)
	{
		___Version10_0 = value;
		Il2CppCodeGenWriteBarrier((&___Version10_0), value);
	}

	inline static int32_t get_offset_of_Version11_1() { return static_cast<int32_t>(offsetof(HttpVersion_t4270509666_StaticFields, ___Version11_1)); }
	inline Version_t1755874712 * get_Version11_1() const { return ___Version11_1; }
	inline Version_t1755874712 ** get_address_of_Version11_1() { return &___Version11_1; }
	inline void set_Version11_1(Version_t1755874712 * value)
	{
		___Version11_1 = value;
		Il2CppCodeGenWriteBarrier((&___Version11_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPVERSION_T4270509666_H
#ifndef WEBSOCKETCONTEXT_T3488732344_H
#define WEBSOCKETCONTEXT_T3488732344_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.WebSockets.WebSocketContext
struct  WebSocketContext_t3488732344  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBSOCKETCONTEXT_T3488732344_H
#ifndef U3CTOSTRINGU3EC__ANONSTOREY2_T1254512383_H
#define U3CTOSTRINGU3EC__ANONSTOREY2_T1254512383_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.WebHeaderCollection/<ToString>c__AnonStorey2
struct  U3CToStringU3Ec__AnonStorey2_t1254512383  : public RuntimeObject
{
public:
	// System.Text.StringBuilder WebSocketSharp.Net.WebHeaderCollection/<ToString>c__AnonStorey2::buff
	StringBuilder_t1221177846 * ___buff_0;
	// WebSocketSharp.Net.WebHeaderCollection WebSocketSharp.Net.WebHeaderCollection/<ToString>c__AnonStorey2::$this
	WebHeaderCollection_t1932982249 * ___U24this_1;

public:
	inline static int32_t get_offset_of_buff_0() { return static_cast<int32_t>(offsetof(U3CToStringU3Ec__AnonStorey2_t1254512383, ___buff_0)); }
	inline StringBuilder_t1221177846 * get_buff_0() const { return ___buff_0; }
	inline StringBuilder_t1221177846 ** get_address_of_buff_0() { return &___buff_0; }
	inline void set_buff_0(StringBuilder_t1221177846 * value)
	{
		___buff_0 = value;
		Il2CppCodeGenWriteBarrier((&___buff_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CToStringU3Ec__AnonStorey2_t1254512383, ___U24this_1)); }
	inline WebHeaderCollection_t1932982249 * get_U24this_1() const { return ___U24this_1; }
	inline WebHeaderCollection_t1932982249 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(WebHeaderCollection_t1932982249 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTOSTRINGU3EC__ANONSTOREY2_T1254512383_H
#ifndef PAYLOADDATA_T3839327312_H
#define PAYLOADDATA_T3839327312_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.PayloadData
struct  PayloadData_t3839327312  : public RuntimeObject
{
public:
	// System.Byte[] WebSocketSharp.PayloadData::_data
	ByteU5BU5D_t3397334013* ____data_0;
	// System.Int64 WebSocketSharp.PayloadData::_extDataLength
	int64_t ____extDataLength_1;
	// System.Int64 WebSocketSharp.PayloadData::_length
	int64_t ____length_2;

public:
	inline static int32_t get_offset_of__data_0() { return static_cast<int32_t>(offsetof(PayloadData_t3839327312, ____data_0)); }
	inline ByteU5BU5D_t3397334013* get__data_0() const { return ____data_0; }
	inline ByteU5BU5D_t3397334013** get_address_of__data_0() { return &____data_0; }
	inline void set__data_0(ByteU5BU5D_t3397334013* value)
	{
		____data_0 = value;
		Il2CppCodeGenWriteBarrier((&____data_0), value);
	}

	inline static int32_t get_offset_of__extDataLength_1() { return static_cast<int32_t>(offsetof(PayloadData_t3839327312, ____extDataLength_1)); }
	inline int64_t get__extDataLength_1() const { return ____extDataLength_1; }
	inline int64_t* get_address_of__extDataLength_1() { return &____extDataLength_1; }
	inline void set__extDataLength_1(int64_t value)
	{
		____extDataLength_1 = value;
	}

	inline static int32_t get_offset_of__length_2() { return static_cast<int32_t>(offsetof(PayloadData_t3839327312, ____length_2)); }
	inline int64_t get__length_2() const { return ____length_2; }
	inline int64_t* get_address_of__length_2() { return &____length_2; }
	inline void set__length_2(int64_t value)
	{
		____length_2 = value;
	}
};

struct PayloadData_t3839327312_StaticFields
{
public:
	// WebSocketSharp.PayloadData WebSocketSharp.PayloadData::Empty
	PayloadData_t3839327312 * ___Empty_3;
	// System.UInt64 WebSocketSharp.PayloadData::MaxLength
	uint64_t ___MaxLength_4;

public:
	inline static int32_t get_offset_of_Empty_3() { return static_cast<int32_t>(offsetof(PayloadData_t3839327312_StaticFields, ___Empty_3)); }
	inline PayloadData_t3839327312 * get_Empty_3() const { return ___Empty_3; }
	inline PayloadData_t3839327312 ** get_address_of_Empty_3() { return &___Empty_3; }
	inline void set_Empty_3(PayloadData_t3839327312 * value)
	{
		___Empty_3 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_3), value);
	}

	inline static int32_t get_offset_of_MaxLength_4() { return static_cast<int32_t>(offsetof(PayloadData_t3839327312_StaticFields, ___MaxLength_4)); }
	inline uint64_t get_MaxLength_4() const { return ___MaxLength_4; }
	inline uint64_t* get_address_of_MaxLength_4() { return &___MaxLength_4; }
	inline void set_MaxLength_4(uint64_t value)
	{
		___MaxLength_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAYLOADDATA_T3839327312_H
#ifndef U3CGETENUMERATORU3EC__ITERATOR0_T3664690781_H
#define U3CGETENUMERATORU3EC__ITERATOR0_T3664690781_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator0
struct  U3CGetEnumeratorU3Ec__Iterator0_t3664690781  : public RuntimeObject
{
public:
	// System.Byte[] WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator0::$locvar0
	ByteU5BU5D_t3397334013* ___U24locvar0_0;
	// System.Int32 WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator0::$locvar1
	int32_t ___U24locvar1_1;
	// System.Byte WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator0::<b>__1
	uint8_t ___U3CbU3E__1_2;
	// WebSocketSharp.PayloadData WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator0::$this
	PayloadData_t3839327312 * ___U24this_3;
	// System.Byte WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator0::$current
	uint8_t ___U24current_4;
	// System.Boolean WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U24locvar0_0() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator0_t3664690781, ___U24locvar0_0)); }
	inline ByteU5BU5D_t3397334013* get_U24locvar0_0() const { return ___U24locvar0_0; }
	inline ByteU5BU5D_t3397334013** get_address_of_U24locvar0_0() { return &___U24locvar0_0; }
	inline void set_U24locvar0_0(ByteU5BU5D_t3397334013* value)
	{
		___U24locvar0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_0), value);
	}

	inline static int32_t get_offset_of_U24locvar1_1() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator0_t3664690781, ___U24locvar1_1)); }
	inline int32_t get_U24locvar1_1() const { return ___U24locvar1_1; }
	inline int32_t* get_address_of_U24locvar1_1() { return &___U24locvar1_1; }
	inline void set_U24locvar1_1(int32_t value)
	{
		___U24locvar1_1 = value;
	}

	inline static int32_t get_offset_of_U3CbU3E__1_2() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator0_t3664690781, ___U3CbU3E__1_2)); }
	inline uint8_t get_U3CbU3E__1_2() const { return ___U3CbU3E__1_2; }
	inline uint8_t* get_address_of_U3CbU3E__1_2() { return &___U3CbU3E__1_2; }
	inline void set_U3CbU3E__1_2(uint8_t value)
	{
		___U3CbU3E__1_2 = value;
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator0_t3664690781, ___U24this_3)); }
	inline PayloadData_t3839327312 * get_U24this_3() const { return ___U24this_3; }
	inline PayloadData_t3839327312 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(PayloadData_t3839327312 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator0_t3664690781, ___U24current_4)); }
	inline uint8_t get_U24current_4() const { return ___U24current_4; }
	inline uint8_t* get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(uint8_t value)
	{
		___U24current_4 = value;
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator0_t3664690781, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator0_t3664690781, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETENUMERATORU3EC__ITERATOR0_T3664690781_H
#ifndef U3CGETOBJECTDATAU3EC__ANONSTOREY1_T2338822889_H
#define U3CGETOBJECTDATAU3EC__ANONSTOREY1_T2338822889_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.WebHeaderCollection/<GetObjectData>c__AnonStorey1
struct  U3CGetObjectDataU3Ec__AnonStorey1_t2338822889  : public RuntimeObject
{
public:
	// System.Runtime.Serialization.SerializationInfo WebSocketSharp.Net.WebHeaderCollection/<GetObjectData>c__AnonStorey1::serializationInfo
	SerializationInfo_t228987430 * ___serializationInfo_0;
	// System.Int32 WebSocketSharp.Net.WebHeaderCollection/<GetObjectData>c__AnonStorey1::cnt
	int32_t ___cnt_1;
	// WebSocketSharp.Net.WebHeaderCollection WebSocketSharp.Net.WebHeaderCollection/<GetObjectData>c__AnonStorey1::$this
	WebHeaderCollection_t1932982249 * ___U24this_2;

public:
	inline static int32_t get_offset_of_serializationInfo_0() { return static_cast<int32_t>(offsetof(U3CGetObjectDataU3Ec__AnonStorey1_t2338822889, ___serializationInfo_0)); }
	inline SerializationInfo_t228987430 * get_serializationInfo_0() const { return ___serializationInfo_0; }
	inline SerializationInfo_t228987430 ** get_address_of_serializationInfo_0() { return &___serializationInfo_0; }
	inline void set_serializationInfo_0(SerializationInfo_t228987430 * value)
	{
		___serializationInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___serializationInfo_0), value);
	}

	inline static int32_t get_offset_of_cnt_1() { return static_cast<int32_t>(offsetof(U3CGetObjectDataU3Ec__AnonStorey1_t2338822889, ___cnt_1)); }
	inline int32_t get_cnt_1() const { return ___cnt_1; }
	inline int32_t* get_address_of_cnt_1() { return &___cnt_1; }
	inline void set_cnt_1(int32_t value)
	{
		___cnt_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CGetObjectDataU3Ec__AnonStorey1_t2338822889, ___U24this_2)); }
	inline WebHeaderCollection_t1932982249 * get_U24this_2() const { return ___U24this_2; }
	inline WebHeaderCollection_t1932982249 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(WebHeaderCollection_t1932982249 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETOBJECTDATAU3EC__ANONSTOREY1_T2338822889_H
#ifndef U3CTOSTRINGMULTIVALUEU3EC__ANONSTOREY0_T1328261099_H
#define U3CTOSTRINGMULTIVALUEU3EC__ANONSTOREY0_T1328261099_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.WebHeaderCollection/<ToStringMultiValue>c__AnonStorey0
struct  U3CToStringMultiValueU3Ec__AnonStorey0_t1328261099  : public RuntimeObject
{
public:
	// System.Boolean WebSocketSharp.Net.WebHeaderCollection/<ToStringMultiValue>c__AnonStorey0::response
	bool ___response_0;
	// System.Text.StringBuilder WebSocketSharp.Net.WebHeaderCollection/<ToStringMultiValue>c__AnonStorey0::buff
	StringBuilder_t1221177846 * ___buff_1;
	// WebSocketSharp.Net.WebHeaderCollection WebSocketSharp.Net.WebHeaderCollection/<ToStringMultiValue>c__AnonStorey0::$this
	WebHeaderCollection_t1932982249 * ___U24this_2;

public:
	inline static int32_t get_offset_of_response_0() { return static_cast<int32_t>(offsetof(U3CToStringMultiValueU3Ec__AnonStorey0_t1328261099, ___response_0)); }
	inline bool get_response_0() const { return ___response_0; }
	inline bool* get_address_of_response_0() { return &___response_0; }
	inline void set_response_0(bool value)
	{
		___response_0 = value;
	}

	inline static int32_t get_offset_of_buff_1() { return static_cast<int32_t>(offsetof(U3CToStringMultiValueU3Ec__AnonStorey0_t1328261099, ___buff_1)); }
	inline StringBuilder_t1221177846 * get_buff_1() const { return ___buff_1; }
	inline StringBuilder_t1221177846 ** get_address_of_buff_1() { return &___buff_1; }
	inline void set_buff_1(StringBuilder_t1221177846 * value)
	{
		___buff_1 = value;
		Il2CppCodeGenWriteBarrier((&___buff_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CToStringMultiValueU3Ec__AnonStorey0_t1328261099, ___U24this_2)); }
	inline WebHeaderCollection_t1932982249 * get_U24this_2() const { return ___U24this_2; }
	inline WebHeaderCollection_t1932982249 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(WebHeaderCollection_t1932982249 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTOSTRINGMULTIVALUEU3EC__ANONSTOREY0_T1328261099_H
#ifndef HTTPUTILITY_T3363705102_H
#define HTTPUTILITY_T3363705102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.HttpUtility
struct  HttpUtility_t3363705102  : public RuntimeObject
{
public:

public:
};

struct HttpUtility_t3363705102_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Char> WebSocketSharp.Net.HttpUtility::_entities
	Dictionary_2_t1074293304 * ____entities_0;
	// System.Char[] WebSocketSharp.Net.HttpUtility::_hexChars
	CharU5BU5D_t1328083999* ____hexChars_1;
	// System.Object WebSocketSharp.Net.HttpUtility::_sync
	RuntimeObject * ____sync_2;

public:
	inline static int32_t get_offset_of__entities_0() { return static_cast<int32_t>(offsetof(HttpUtility_t3363705102_StaticFields, ____entities_0)); }
	inline Dictionary_2_t1074293304 * get__entities_0() const { return ____entities_0; }
	inline Dictionary_2_t1074293304 ** get_address_of__entities_0() { return &____entities_0; }
	inline void set__entities_0(Dictionary_2_t1074293304 * value)
	{
		____entities_0 = value;
		Il2CppCodeGenWriteBarrier((&____entities_0), value);
	}

	inline static int32_t get_offset_of__hexChars_1() { return static_cast<int32_t>(offsetof(HttpUtility_t3363705102_StaticFields, ____hexChars_1)); }
	inline CharU5BU5D_t1328083999* get__hexChars_1() const { return ____hexChars_1; }
	inline CharU5BU5D_t1328083999** get_address_of__hexChars_1() { return &____hexChars_1; }
	inline void set__hexChars_1(CharU5BU5D_t1328083999* value)
	{
		____hexChars_1 = value;
		Il2CppCodeGenWriteBarrier((&____hexChars_1), value);
	}

	inline static int32_t get_offset_of__sync_2() { return static_cast<int32_t>(offsetof(HttpUtility_t3363705102_StaticFields, ____sync_2)); }
	inline RuntimeObject * get__sync_2() const { return ____sync_2; }
	inline RuntimeObject ** get_address_of__sync_2() { return &____sync_2; }
	inline void set__sync_2(RuntimeObject * value)
	{
		____sync_2 = value;
		Il2CppCodeGenWriteBarrier((&____sync_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPUTILITY_T3363705102_H
#ifndef U3CU3EC__ITERATOR0_T1818225242_H
#define U3CU3EC__ITERATOR0_T1818225242_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.WebSockets.HttpListenerWebSocketContext/<>c__Iterator0
struct  U3CU3Ec__Iterator0_t1818225242  : public RuntimeObject
{
public:
	// System.String WebSocketSharp.Net.WebSockets.HttpListenerWebSocketContext/<>c__Iterator0::<protocols>__0
	String_t* ___U3CprotocolsU3E__0_0;
	// System.String[] WebSocketSharp.Net.WebSockets.HttpListenerWebSocketContext/<>c__Iterator0::$locvar0
	StringU5BU5D_t1642385972* ___U24locvar0_1;
	// System.Int32 WebSocketSharp.Net.WebSockets.HttpListenerWebSocketContext/<>c__Iterator0::$locvar1
	int32_t ___U24locvar1_2;
	// System.String WebSocketSharp.Net.WebSockets.HttpListenerWebSocketContext/<>c__Iterator0::<protocol>__1
	String_t* ___U3CprotocolU3E__1_3;
	// WebSocketSharp.Net.WebSockets.HttpListenerWebSocketContext WebSocketSharp.Net.WebSockets.HttpListenerWebSocketContext/<>c__Iterator0::$this
	HttpListenerWebSocketContext_t1778866096 * ___U24this_4;
	// System.String WebSocketSharp.Net.WebSockets.HttpListenerWebSocketContext/<>c__Iterator0::$current
	String_t* ___U24current_5;
	// System.Boolean WebSocketSharp.Net.WebSockets.HttpListenerWebSocketContext/<>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 WebSocketSharp.Net.WebSockets.HttpListenerWebSocketContext/<>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CprotocolsU3E__0_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t1818225242, ___U3CprotocolsU3E__0_0)); }
	inline String_t* get_U3CprotocolsU3E__0_0() const { return ___U3CprotocolsU3E__0_0; }
	inline String_t** get_address_of_U3CprotocolsU3E__0_0() { return &___U3CprotocolsU3E__0_0; }
	inline void set_U3CprotocolsU3E__0_0(String_t* value)
	{
		___U3CprotocolsU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CprotocolsU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24locvar0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t1818225242, ___U24locvar0_1)); }
	inline StringU5BU5D_t1642385972* get_U24locvar0_1() const { return ___U24locvar0_1; }
	inline StringU5BU5D_t1642385972** get_address_of_U24locvar0_1() { return &___U24locvar0_1; }
	inline void set_U24locvar0_1(StringU5BU5D_t1642385972* value)
	{
		___U24locvar0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_1), value);
	}

	inline static int32_t get_offset_of_U24locvar1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t1818225242, ___U24locvar1_2)); }
	inline int32_t get_U24locvar1_2() const { return ___U24locvar1_2; }
	inline int32_t* get_address_of_U24locvar1_2() { return &___U24locvar1_2; }
	inline void set_U24locvar1_2(int32_t value)
	{
		___U24locvar1_2 = value;
	}

	inline static int32_t get_offset_of_U3CprotocolU3E__1_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t1818225242, ___U3CprotocolU3E__1_3)); }
	inline String_t* get_U3CprotocolU3E__1_3() const { return ___U3CprotocolU3E__1_3; }
	inline String_t** get_address_of_U3CprotocolU3E__1_3() { return &___U3CprotocolU3E__1_3; }
	inline void set_U3CprotocolU3E__1_3(String_t* value)
	{
		___U3CprotocolU3E__1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CprotocolU3E__1_3), value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t1818225242, ___U24this_4)); }
	inline HttpListenerWebSocketContext_t1778866096 * get_U24this_4() const { return ___U24this_4; }
	inline HttpListenerWebSocketContext_t1778866096 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(HttpListenerWebSocketContext_t1778866096 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t1818225242, ___U24current_5)); }
	inline String_t* get_U24current_5() const { return ___U24current_5; }
	inline String_t** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(String_t* value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t1818225242, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t1818225242, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__ITERATOR0_T1818225242_H
#ifndef NETWORKCREDENTIAL_T3911206805_H
#define NETWORKCREDENTIAL_T3911206805_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.NetworkCredential
struct  NetworkCredential_t3911206805  : public RuntimeObject
{
public:
	// System.String WebSocketSharp.Net.NetworkCredential::_domain
	String_t* ____domain_0;
	// System.String WebSocketSharp.Net.NetworkCredential::_password
	String_t* ____password_1;
	// System.String[] WebSocketSharp.Net.NetworkCredential::_roles
	StringU5BU5D_t1642385972* ____roles_2;
	// System.String WebSocketSharp.Net.NetworkCredential::_userName
	String_t* ____userName_3;

public:
	inline static int32_t get_offset_of__domain_0() { return static_cast<int32_t>(offsetof(NetworkCredential_t3911206805, ____domain_0)); }
	inline String_t* get__domain_0() const { return ____domain_0; }
	inline String_t** get_address_of__domain_0() { return &____domain_0; }
	inline void set__domain_0(String_t* value)
	{
		____domain_0 = value;
		Il2CppCodeGenWriteBarrier((&____domain_0), value);
	}

	inline static int32_t get_offset_of__password_1() { return static_cast<int32_t>(offsetof(NetworkCredential_t3911206805, ____password_1)); }
	inline String_t* get__password_1() const { return ____password_1; }
	inline String_t** get_address_of__password_1() { return &____password_1; }
	inline void set__password_1(String_t* value)
	{
		____password_1 = value;
		Il2CppCodeGenWriteBarrier((&____password_1), value);
	}

	inline static int32_t get_offset_of__roles_2() { return static_cast<int32_t>(offsetof(NetworkCredential_t3911206805, ____roles_2)); }
	inline StringU5BU5D_t1642385972* get__roles_2() const { return ____roles_2; }
	inline StringU5BU5D_t1642385972** get_address_of__roles_2() { return &____roles_2; }
	inline void set__roles_2(StringU5BU5D_t1642385972* value)
	{
		____roles_2 = value;
		Il2CppCodeGenWriteBarrier((&____roles_2), value);
	}

	inline static int32_t get_offset_of__userName_3() { return static_cast<int32_t>(offsetof(NetworkCredential_t3911206805, ____userName_3)); }
	inline String_t* get__userName_3() const { return ____userName_3; }
	inline String_t** get_address_of__userName_3() { return &____userName_3; }
	inline void set__userName_3(String_t* value)
	{
		____userName_3 = value;
		Il2CppCodeGenWriteBarrier((&____userName_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKCREDENTIAL_T3911206805_H
#ifndef U3CU3EC__ITERATOR0_T1986573215_H
#define U3CU3EC__ITERATOR0_T1986573215_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext/<>c__Iterator0
struct  U3CU3Ec__Iterator0_t1986573215  : public RuntimeObject
{
public:
	// System.String WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext/<>c__Iterator0::<protocols>__0
	String_t* ___U3CprotocolsU3E__0_0;
	// System.String[] WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext/<>c__Iterator0::$locvar0
	StringU5BU5D_t1642385972* ___U24locvar0_1;
	// System.Int32 WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext/<>c__Iterator0::$locvar1
	int32_t ___U24locvar1_2;
	// System.String WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext/<>c__Iterator0::<protocol>__1
	String_t* ___U3CprotocolU3E__1_3;
	// WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext/<>c__Iterator0::$this
	TcpListenerWebSocketContext_t1695227117 * ___U24this_4;
	// System.String WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext/<>c__Iterator0::$current
	String_t* ___U24current_5;
	// System.Boolean WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext/<>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext/<>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CprotocolsU3E__0_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t1986573215, ___U3CprotocolsU3E__0_0)); }
	inline String_t* get_U3CprotocolsU3E__0_0() const { return ___U3CprotocolsU3E__0_0; }
	inline String_t** get_address_of_U3CprotocolsU3E__0_0() { return &___U3CprotocolsU3E__0_0; }
	inline void set_U3CprotocolsU3E__0_0(String_t* value)
	{
		___U3CprotocolsU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CprotocolsU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24locvar0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t1986573215, ___U24locvar0_1)); }
	inline StringU5BU5D_t1642385972* get_U24locvar0_1() const { return ___U24locvar0_1; }
	inline StringU5BU5D_t1642385972** get_address_of_U24locvar0_1() { return &___U24locvar0_1; }
	inline void set_U24locvar0_1(StringU5BU5D_t1642385972* value)
	{
		___U24locvar0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_1), value);
	}

	inline static int32_t get_offset_of_U24locvar1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t1986573215, ___U24locvar1_2)); }
	inline int32_t get_U24locvar1_2() const { return ___U24locvar1_2; }
	inline int32_t* get_address_of_U24locvar1_2() { return &___U24locvar1_2; }
	inline void set_U24locvar1_2(int32_t value)
	{
		___U24locvar1_2 = value;
	}

	inline static int32_t get_offset_of_U3CprotocolU3E__1_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t1986573215, ___U3CprotocolU3E__1_3)); }
	inline String_t* get_U3CprotocolU3E__1_3() const { return ___U3CprotocolU3E__1_3; }
	inline String_t** get_address_of_U3CprotocolU3E__1_3() { return &___U3CprotocolU3E__1_3; }
	inline void set_U3CprotocolU3E__1_3(String_t* value)
	{
		___U3CprotocolU3E__1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CprotocolU3E__1_3), value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t1986573215, ___U24this_4)); }
	inline TcpListenerWebSocketContext_t1695227117 * get_U24this_4() const { return ___U24this_4; }
	inline TcpListenerWebSocketContext_t1695227117 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(TcpListenerWebSocketContext_t1695227117 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t1986573215, ___U24current_5)); }
	inline String_t* get_U24current_5() const { return ___U24current_5; }
	inline String_t** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(String_t* value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t1986573215, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t1986573215, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__ITERATOR0_T1986573215_H
#ifndef HTTPSTREAMASYNCRESULT_T782812803_H
#define HTTPSTREAMASYNCRESULT_T782812803_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.HttpStreamAsyncResult
struct  HttpStreamAsyncResult_t782812803  : public RuntimeObject
{
public:
	// System.Byte[] WebSocketSharp.Net.HttpStreamAsyncResult::_buffer
	ByteU5BU5D_t3397334013* ____buffer_0;
	// System.AsyncCallback WebSocketSharp.Net.HttpStreamAsyncResult::_callback
	AsyncCallback_t163412349 * ____callback_1;
	// System.Boolean WebSocketSharp.Net.HttpStreamAsyncResult::_completed
	bool ____completed_2;
	// System.Int32 WebSocketSharp.Net.HttpStreamAsyncResult::_count
	int32_t ____count_3;
	// System.Exception WebSocketSharp.Net.HttpStreamAsyncResult::_exception
	Exception_t1927440687 * ____exception_4;
	// System.Int32 WebSocketSharp.Net.HttpStreamAsyncResult::_offset
	int32_t ____offset_5;
	// System.Object WebSocketSharp.Net.HttpStreamAsyncResult::_state
	RuntimeObject * ____state_6;
	// System.Object WebSocketSharp.Net.HttpStreamAsyncResult::_sync
	RuntimeObject * ____sync_7;
	// System.Int32 WebSocketSharp.Net.HttpStreamAsyncResult::_syncRead
	int32_t ____syncRead_8;
	// System.Threading.ManualResetEvent WebSocketSharp.Net.HttpStreamAsyncResult::_waitHandle
	ManualResetEvent_t926074657 * ____waitHandle_9;

public:
	inline static int32_t get_offset_of__buffer_0() { return static_cast<int32_t>(offsetof(HttpStreamAsyncResult_t782812803, ____buffer_0)); }
	inline ByteU5BU5D_t3397334013* get__buffer_0() const { return ____buffer_0; }
	inline ByteU5BU5D_t3397334013** get_address_of__buffer_0() { return &____buffer_0; }
	inline void set__buffer_0(ByteU5BU5D_t3397334013* value)
	{
		____buffer_0 = value;
		Il2CppCodeGenWriteBarrier((&____buffer_0), value);
	}

	inline static int32_t get_offset_of__callback_1() { return static_cast<int32_t>(offsetof(HttpStreamAsyncResult_t782812803, ____callback_1)); }
	inline AsyncCallback_t163412349 * get__callback_1() const { return ____callback_1; }
	inline AsyncCallback_t163412349 ** get_address_of__callback_1() { return &____callback_1; }
	inline void set__callback_1(AsyncCallback_t163412349 * value)
	{
		____callback_1 = value;
		Il2CppCodeGenWriteBarrier((&____callback_1), value);
	}

	inline static int32_t get_offset_of__completed_2() { return static_cast<int32_t>(offsetof(HttpStreamAsyncResult_t782812803, ____completed_2)); }
	inline bool get__completed_2() const { return ____completed_2; }
	inline bool* get_address_of__completed_2() { return &____completed_2; }
	inline void set__completed_2(bool value)
	{
		____completed_2 = value;
	}

	inline static int32_t get_offset_of__count_3() { return static_cast<int32_t>(offsetof(HttpStreamAsyncResult_t782812803, ____count_3)); }
	inline int32_t get__count_3() const { return ____count_3; }
	inline int32_t* get_address_of__count_3() { return &____count_3; }
	inline void set__count_3(int32_t value)
	{
		____count_3 = value;
	}

	inline static int32_t get_offset_of__exception_4() { return static_cast<int32_t>(offsetof(HttpStreamAsyncResult_t782812803, ____exception_4)); }
	inline Exception_t1927440687 * get__exception_4() const { return ____exception_4; }
	inline Exception_t1927440687 ** get_address_of__exception_4() { return &____exception_4; }
	inline void set__exception_4(Exception_t1927440687 * value)
	{
		____exception_4 = value;
		Il2CppCodeGenWriteBarrier((&____exception_4), value);
	}

	inline static int32_t get_offset_of__offset_5() { return static_cast<int32_t>(offsetof(HttpStreamAsyncResult_t782812803, ____offset_5)); }
	inline int32_t get__offset_5() const { return ____offset_5; }
	inline int32_t* get_address_of__offset_5() { return &____offset_5; }
	inline void set__offset_5(int32_t value)
	{
		____offset_5 = value;
	}

	inline static int32_t get_offset_of__state_6() { return static_cast<int32_t>(offsetof(HttpStreamAsyncResult_t782812803, ____state_6)); }
	inline RuntimeObject * get__state_6() const { return ____state_6; }
	inline RuntimeObject ** get_address_of__state_6() { return &____state_6; }
	inline void set__state_6(RuntimeObject * value)
	{
		____state_6 = value;
		Il2CppCodeGenWriteBarrier((&____state_6), value);
	}

	inline static int32_t get_offset_of__sync_7() { return static_cast<int32_t>(offsetof(HttpStreamAsyncResult_t782812803, ____sync_7)); }
	inline RuntimeObject * get__sync_7() const { return ____sync_7; }
	inline RuntimeObject ** get_address_of__sync_7() { return &____sync_7; }
	inline void set__sync_7(RuntimeObject * value)
	{
		____sync_7 = value;
		Il2CppCodeGenWriteBarrier((&____sync_7), value);
	}

	inline static int32_t get_offset_of__syncRead_8() { return static_cast<int32_t>(offsetof(HttpStreamAsyncResult_t782812803, ____syncRead_8)); }
	inline int32_t get__syncRead_8() const { return ____syncRead_8; }
	inline int32_t* get_address_of__syncRead_8() { return &____syncRead_8; }
	inline void set__syncRead_8(int32_t value)
	{
		____syncRead_8 = value;
	}

	inline static int32_t get_offset_of__waitHandle_9() { return static_cast<int32_t>(offsetof(HttpStreamAsyncResult_t782812803, ____waitHandle_9)); }
	inline ManualResetEvent_t926074657 * get__waitHandle_9() const { return ____waitHandle_9; }
	inline ManualResetEvent_t926074657 ** get_address_of__waitHandle_9() { return &____waitHandle_9; }
	inline void set__waitHandle_9(ManualResetEvent_t926074657 * value)
	{
		____waitHandle_9 = value;
		Il2CppCodeGenWriteBarrier((&____waitHandle_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPSTREAMASYNCRESULT_T782812803_H
#ifndef READBUFFERSTATE_T2596755373_H
#define READBUFFERSTATE_T2596755373_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.ReadBufferState
struct  ReadBufferState_t2596755373  : public RuntimeObject
{
public:
	// WebSocketSharp.Net.HttpStreamAsyncResult WebSocketSharp.Net.ReadBufferState::_asyncResult
	HttpStreamAsyncResult_t782812803 * ____asyncResult_0;
	// System.Byte[] WebSocketSharp.Net.ReadBufferState::_buffer
	ByteU5BU5D_t3397334013* ____buffer_1;
	// System.Int32 WebSocketSharp.Net.ReadBufferState::_count
	int32_t ____count_2;
	// System.Int32 WebSocketSharp.Net.ReadBufferState::_initialCount
	int32_t ____initialCount_3;
	// System.Int32 WebSocketSharp.Net.ReadBufferState::_offset
	int32_t ____offset_4;

public:
	inline static int32_t get_offset_of__asyncResult_0() { return static_cast<int32_t>(offsetof(ReadBufferState_t2596755373, ____asyncResult_0)); }
	inline HttpStreamAsyncResult_t782812803 * get__asyncResult_0() const { return ____asyncResult_0; }
	inline HttpStreamAsyncResult_t782812803 ** get_address_of__asyncResult_0() { return &____asyncResult_0; }
	inline void set__asyncResult_0(HttpStreamAsyncResult_t782812803 * value)
	{
		____asyncResult_0 = value;
		Il2CppCodeGenWriteBarrier((&____asyncResult_0), value);
	}

	inline static int32_t get_offset_of__buffer_1() { return static_cast<int32_t>(offsetof(ReadBufferState_t2596755373, ____buffer_1)); }
	inline ByteU5BU5D_t3397334013* get__buffer_1() const { return ____buffer_1; }
	inline ByteU5BU5D_t3397334013** get_address_of__buffer_1() { return &____buffer_1; }
	inline void set__buffer_1(ByteU5BU5D_t3397334013* value)
	{
		____buffer_1 = value;
		Il2CppCodeGenWriteBarrier((&____buffer_1), value);
	}

	inline static int32_t get_offset_of__count_2() { return static_cast<int32_t>(offsetof(ReadBufferState_t2596755373, ____count_2)); }
	inline int32_t get__count_2() const { return ____count_2; }
	inline int32_t* get_address_of__count_2() { return &____count_2; }
	inline void set__count_2(int32_t value)
	{
		____count_2 = value;
	}

	inline static int32_t get_offset_of__initialCount_3() { return static_cast<int32_t>(offsetof(ReadBufferState_t2596755373, ____initialCount_3)); }
	inline int32_t get__initialCount_3() const { return ____initialCount_3; }
	inline int32_t* get_address_of__initialCount_3() { return &____initialCount_3; }
	inline void set__initialCount_3(int32_t value)
	{
		____initialCount_3 = value;
	}

	inline static int32_t get_offset_of__offset_4() { return static_cast<int32_t>(offsetof(ReadBufferState_t2596755373, ____offset_4)); }
	inline int32_t get__offset_4() const { return ____offset_4; }
	inline int32_t* get_address_of__offset_4() { return &____offset_4; }
	inline void set__offset_4(int32_t value)
	{
		____offset_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READBUFFERSTATE_T2596755373_H
#ifndef HTTPLISTENERWEBSOCKETCONTEXT_T1778866096_H
#define HTTPLISTENERWEBSOCKETCONTEXT_T1778866096_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.WebSockets.HttpListenerWebSocketContext
struct  HttpListenerWebSocketContext_t1778866096  : public WebSocketContext_t3488732344
{
public:
	// WebSocketSharp.Net.HttpListenerContext WebSocketSharp.Net.WebSockets.HttpListenerWebSocketContext::_context
	HttpListenerContext_t994708409 * ____context_0;
	// WebSocketSharp.WebSocket WebSocketSharp.Net.WebSockets.HttpListenerWebSocketContext::_websocket
	WebSocket_t3268376029 * ____websocket_1;

public:
	inline static int32_t get_offset_of__context_0() { return static_cast<int32_t>(offsetof(HttpListenerWebSocketContext_t1778866096, ____context_0)); }
	inline HttpListenerContext_t994708409 * get__context_0() const { return ____context_0; }
	inline HttpListenerContext_t994708409 ** get_address_of__context_0() { return &____context_0; }
	inline void set__context_0(HttpListenerContext_t994708409 * value)
	{
		____context_0 = value;
		Il2CppCodeGenWriteBarrier((&____context_0), value);
	}

	inline static int32_t get_offset_of__websocket_1() { return static_cast<int32_t>(offsetof(HttpListenerWebSocketContext_t1778866096, ____websocket_1)); }
	inline WebSocket_t3268376029 * get__websocket_1() const { return ____websocket_1; }
	inline WebSocket_t3268376029 ** get_address_of__websocket_1() { return &____websocket_1; }
	inline void set__websocket_1(WebSocket_t3268376029 * value)
	{
		____websocket_1 = value;
		Il2CppCodeGenWriteBarrier((&____websocket_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPLISTENERWEBSOCKETCONTEXT_T1778866096_H
#ifndef U24ARRAYTYPEU3D8_T1459944466_H
#define U24ARRAYTYPEU3D8_T1459944466_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=8
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D8_t1459944466 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D8_t1459944466__padding[8];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D8_T1459944466_H
#ifndef NAMEVALUECOLLECTION_T3047564564_H
#define NAMEVALUECOLLECTION_T3047564564_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Specialized.NameValueCollection
struct  NameValueCollection_t3047564564  : public NameObjectCollectionBase_t2034248631
{
public:
	// System.String[] System.Collections.Specialized.NameValueCollection::cachedAllKeys
	StringU5BU5D_t1642385972* ___cachedAllKeys_10;
	// System.String[] System.Collections.Specialized.NameValueCollection::cachedAll
	StringU5BU5D_t1642385972* ___cachedAll_11;

public:
	inline static int32_t get_offset_of_cachedAllKeys_10() { return static_cast<int32_t>(offsetof(NameValueCollection_t3047564564, ___cachedAllKeys_10)); }
	inline StringU5BU5D_t1642385972* get_cachedAllKeys_10() const { return ___cachedAllKeys_10; }
	inline StringU5BU5D_t1642385972** get_address_of_cachedAllKeys_10() { return &___cachedAllKeys_10; }
	inline void set_cachedAllKeys_10(StringU5BU5D_t1642385972* value)
	{
		___cachedAllKeys_10 = value;
		Il2CppCodeGenWriteBarrier((&___cachedAllKeys_10), value);
	}

	inline static int32_t get_offset_of_cachedAll_11() { return static_cast<int32_t>(offsetof(NameValueCollection_t3047564564, ___cachedAll_11)); }
	inline StringU5BU5D_t1642385972* get_cachedAll_11() const { return ___cachedAll_11; }
	inline StringU5BU5D_t1642385972** get_address_of_cachedAll_11() { return &___cachedAll_11; }
	inline void set_cachedAll_11(StringU5BU5D_t1642385972* value)
	{
		___cachedAll_11 = value;
		Il2CppCodeGenWriteBarrier((&___cachedAll_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMEVALUECOLLECTION_T3047564564_H
#ifndef REQUESTSTREAM_T775716369_H
#define REQUESTSTREAM_T775716369_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.RequestStream
struct  RequestStream_t775716369  : public Stream_t3255436806
{
public:
	// System.Int64 WebSocketSharp.Net.RequestStream::_bodyLeft
	int64_t ____bodyLeft_1;
	// System.Byte[] WebSocketSharp.Net.RequestStream::_buffer
	ByteU5BU5D_t3397334013* ____buffer_2;
	// System.Int32 WebSocketSharp.Net.RequestStream::_count
	int32_t ____count_3;
	// System.Boolean WebSocketSharp.Net.RequestStream::_disposed
	bool ____disposed_4;
	// System.Int32 WebSocketSharp.Net.RequestStream::_offset
	int32_t ____offset_5;
	// System.IO.Stream WebSocketSharp.Net.RequestStream::_stream
	Stream_t3255436806 * ____stream_6;

public:
	inline static int32_t get_offset_of__bodyLeft_1() { return static_cast<int32_t>(offsetof(RequestStream_t775716369, ____bodyLeft_1)); }
	inline int64_t get__bodyLeft_1() const { return ____bodyLeft_1; }
	inline int64_t* get_address_of__bodyLeft_1() { return &____bodyLeft_1; }
	inline void set__bodyLeft_1(int64_t value)
	{
		____bodyLeft_1 = value;
	}

	inline static int32_t get_offset_of__buffer_2() { return static_cast<int32_t>(offsetof(RequestStream_t775716369, ____buffer_2)); }
	inline ByteU5BU5D_t3397334013* get__buffer_2() const { return ____buffer_2; }
	inline ByteU5BU5D_t3397334013** get_address_of__buffer_2() { return &____buffer_2; }
	inline void set__buffer_2(ByteU5BU5D_t3397334013* value)
	{
		____buffer_2 = value;
		Il2CppCodeGenWriteBarrier((&____buffer_2), value);
	}

	inline static int32_t get_offset_of__count_3() { return static_cast<int32_t>(offsetof(RequestStream_t775716369, ____count_3)); }
	inline int32_t get__count_3() const { return ____count_3; }
	inline int32_t* get_address_of__count_3() { return &____count_3; }
	inline void set__count_3(int32_t value)
	{
		____count_3 = value;
	}

	inline static int32_t get_offset_of__disposed_4() { return static_cast<int32_t>(offsetof(RequestStream_t775716369, ____disposed_4)); }
	inline bool get__disposed_4() const { return ____disposed_4; }
	inline bool* get_address_of__disposed_4() { return &____disposed_4; }
	inline void set__disposed_4(bool value)
	{
		____disposed_4 = value;
	}

	inline static int32_t get_offset_of__offset_5() { return static_cast<int32_t>(offsetof(RequestStream_t775716369, ____offset_5)); }
	inline int32_t get__offset_5() const { return ____offset_5; }
	inline int32_t* get_address_of__offset_5() { return &____offset_5; }
	inline void set__offset_5(int32_t value)
	{
		____offset_5 = value;
	}

	inline static int32_t get_offset_of__stream_6() { return static_cast<int32_t>(offsetof(RequestStream_t775716369, ____stream_6)); }
	inline Stream_t3255436806 * get__stream_6() const { return ____stream_6; }
	inline Stream_t3255436806 ** get_address_of__stream_6() { return &____stream_6; }
	inline void set__stream_6(Stream_t3255436806 * value)
	{
		____stream_6 = value;
		Il2CppCodeGenWriteBarrier((&____stream_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUESTSTREAM_T775716369_H
#ifndef U24ARRAYTYPEU3D16_T3894236545_H
#define U24ARRAYTYPEU3D16_T3894236545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=16
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D16_t3894236545 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D16_t3894236545__padding[16];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D16_T3894236545_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t1328083999* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t1328083999* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t1328083999** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t1328083999* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef TIMESPAN_T3430258949_H
#define TIMESPAN_T3430258949_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t3430258949 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_8;

public:
	inline static int32_t get_offset_of__ticks_8() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949, ____ticks_8)); }
	inline int64_t get__ticks_8() const { return ____ticks_8; }
	inline int64_t* get_address_of__ticks_8() { return &____ticks_8; }
	inline void set__ticks_8(int64_t value)
	{
		____ticks_8 = value;
	}
};

struct TimeSpan_t3430258949_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t3430258949  ___MaxValue_5;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t3430258949  ___MinValue_6;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t3430258949  ___Zero_7;

public:
	inline static int32_t get_offset_of_MaxValue_5() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949_StaticFields, ___MaxValue_5)); }
	inline TimeSpan_t3430258949  get_MaxValue_5() const { return ___MaxValue_5; }
	inline TimeSpan_t3430258949 * get_address_of_MaxValue_5() { return &___MaxValue_5; }
	inline void set_MaxValue_5(TimeSpan_t3430258949  value)
	{
		___MaxValue_5 = value;
	}

	inline static int32_t get_offset_of_MinValue_6() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949_StaticFields, ___MinValue_6)); }
	inline TimeSpan_t3430258949  get_MinValue_6() const { return ___MinValue_6; }
	inline TimeSpan_t3430258949 * get_address_of_MinValue_6() { return &___MinValue_6; }
	inline void set_MinValue_6(TimeSpan_t3430258949  value)
	{
		___MinValue_6 = value;
	}

	inline static int32_t get_offset_of_Zero_7() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949_StaticFields, ___Zero_7)); }
	inline TimeSpan_t3430258949  get_Zero_7() const { return ___Zero_7; }
	inline TimeSpan_t3430258949 * get_address_of_Zero_7() { return &___Zero_7; }
	inline void set_Zero_7(TimeSpan_t3430258949  value)
	{
		___Zero_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T3430258949_H
#ifndef KEYVALUEPAIR_2_T3497699202_H
#define KEYVALUEPAIR_2_T3497699202_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>
struct  KeyValuePair_2_t3497699202 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	bool ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3497699202, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3497699202, ___value_1)); }
	inline bool get_value_1() const { return ___value_1; }
	inline bool* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(bool value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T3497699202_H
#ifndef RESPONSESTREAM_T3200689523_H
#define RESPONSESTREAM_T3200689523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.ResponseStream
struct  ResponseStream_t3200689523  : public Stream_t3255436806
{
public:
	// System.IO.MemoryStream WebSocketSharp.Net.ResponseStream::_body
	MemoryStream_t743994179 * ____body_1;
	// System.Boolean WebSocketSharp.Net.ResponseStream::_disposed
	bool ____disposed_3;
	// WebSocketSharp.Net.HttpListenerResponse WebSocketSharp.Net.ResponseStream::_response
	HttpListenerResponse_t2223360553 * ____response_4;
	// System.Boolean WebSocketSharp.Net.ResponseStream::_sendChunked
	bool ____sendChunked_5;
	// System.IO.Stream WebSocketSharp.Net.ResponseStream::_stream
	Stream_t3255436806 * ____stream_6;
	// System.Action`3<System.Byte[],System.Int32,System.Int32> WebSocketSharp.Net.ResponseStream::_write
	Action_3_t4193041497 * ____write_7;
	// System.Action`3<System.Byte[],System.Int32,System.Int32> WebSocketSharp.Net.ResponseStream::_writeBody
	Action_3_t4193041497 * ____writeBody_8;
	// System.Action`3<System.Byte[],System.Int32,System.Int32> WebSocketSharp.Net.ResponseStream::_writeChunked
	Action_3_t4193041497 * ____writeChunked_9;

public:
	inline static int32_t get_offset_of__body_1() { return static_cast<int32_t>(offsetof(ResponseStream_t3200689523, ____body_1)); }
	inline MemoryStream_t743994179 * get__body_1() const { return ____body_1; }
	inline MemoryStream_t743994179 ** get_address_of__body_1() { return &____body_1; }
	inline void set__body_1(MemoryStream_t743994179 * value)
	{
		____body_1 = value;
		Il2CppCodeGenWriteBarrier((&____body_1), value);
	}

	inline static int32_t get_offset_of__disposed_3() { return static_cast<int32_t>(offsetof(ResponseStream_t3200689523, ____disposed_3)); }
	inline bool get__disposed_3() const { return ____disposed_3; }
	inline bool* get_address_of__disposed_3() { return &____disposed_3; }
	inline void set__disposed_3(bool value)
	{
		____disposed_3 = value;
	}

	inline static int32_t get_offset_of__response_4() { return static_cast<int32_t>(offsetof(ResponseStream_t3200689523, ____response_4)); }
	inline HttpListenerResponse_t2223360553 * get__response_4() const { return ____response_4; }
	inline HttpListenerResponse_t2223360553 ** get_address_of__response_4() { return &____response_4; }
	inline void set__response_4(HttpListenerResponse_t2223360553 * value)
	{
		____response_4 = value;
		Il2CppCodeGenWriteBarrier((&____response_4), value);
	}

	inline static int32_t get_offset_of__sendChunked_5() { return static_cast<int32_t>(offsetof(ResponseStream_t3200689523, ____sendChunked_5)); }
	inline bool get__sendChunked_5() const { return ____sendChunked_5; }
	inline bool* get_address_of__sendChunked_5() { return &____sendChunked_5; }
	inline void set__sendChunked_5(bool value)
	{
		____sendChunked_5 = value;
	}

	inline static int32_t get_offset_of__stream_6() { return static_cast<int32_t>(offsetof(ResponseStream_t3200689523, ____stream_6)); }
	inline Stream_t3255436806 * get__stream_6() const { return ____stream_6; }
	inline Stream_t3255436806 ** get_address_of__stream_6() { return &____stream_6; }
	inline void set__stream_6(Stream_t3255436806 * value)
	{
		____stream_6 = value;
		Il2CppCodeGenWriteBarrier((&____stream_6), value);
	}

	inline static int32_t get_offset_of__write_7() { return static_cast<int32_t>(offsetof(ResponseStream_t3200689523, ____write_7)); }
	inline Action_3_t4193041497 * get__write_7() const { return ____write_7; }
	inline Action_3_t4193041497 ** get_address_of__write_7() { return &____write_7; }
	inline void set__write_7(Action_3_t4193041497 * value)
	{
		____write_7 = value;
		Il2CppCodeGenWriteBarrier((&____write_7), value);
	}

	inline static int32_t get_offset_of__writeBody_8() { return static_cast<int32_t>(offsetof(ResponseStream_t3200689523, ____writeBody_8)); }
	inline Action_3_t4193041497 * get__writeBody_8() const { return ____writeBody_8; }
	inline Action_3_t4193041497 ** get_address_of__writeBody_8() { return &____writeBody_8; }
	inline void set__writeBody_8(Action_3_t4193041497 * value)
	{
		____writeBody_8 = value;
		Il2CppCodeGenWriteBarrier((&____writeBody_8), value);
	}

	inline static int32_t get_offset_of__writeChunked_9() { return static_cast<int32_t>(offsetof(ResponseStream_t3200689523, ____writeChunked_9)); }
	inline Action_3_t4193041497 * get__writeChunked_9() const { return ____writeChunked_9; }
	inline Action_3_t4193041497 ** get_address_of__writeChunked_9() { return &____writeChunked_9; }
	inline void set__writeChunked_9(Action_3_t4193041497 * value)
	{
		____writeChunked_9 = value;
		Il2CppCodeGenWriteBarrier((&____writeChunked_9), value);
	}
};

struct ResponseStream_t3200689523_StaticFields
{
public:
	// System.Byte[] WebSocketSharp.Net.ResponseStream::_crlf
	ByteU5BU5D_t3397334013* ____crlf_2;

public:
	inline static int32_t get_offset_of__crlf_2() { return static_cast<int32_t>(offsetof(ResponseStream_t3200689523_StaticFields, ____crlf_2)); }
	inline ByteU5BU5D_t3397334013* get__crlf_2() const { return ____crlf_2; }
	inline ByteU5BU5D_t3397334013** get_address_of__crlf_2() { return &____crlf_2; }
	inline void set__crlf_2(ByteU5BU5D_t3397334013* value)
	{
		____crlf_2 = value;
		Il2CppCodeGenWriteBarrier((&____crlf_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESPONSESTREAM_T3200689523_H
#ifndef BOOLEAN_T3825574718_H
#define BOOLEAN_T3825574718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t3825574718 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t3825574718, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t3825574718_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t3825574718_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t3825574718_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T3825574718_H
#ifndef VECTOR2_T2243707579_H
#define VECTOR2_T2243707579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2243707579 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2243707579, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2243707579, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2243707579_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2243707579  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2243707579  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2243707579  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2243707579  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2243707579  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2243707579  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2243707579  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2243707579  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2243707579  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2243707579 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2243707579  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___oneVector_3)); }
	inline Vector2_t2243707579  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2243707579 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2243707579  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___upVector_4)); }
	inline Vector2_t2243707579  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2243707579 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2243707579  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___downVector_5)); }
	inline Vector2_t2243707579  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2243707579 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2243707579  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___leftVector_6)); }
	inline Vector2_t2243707579  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2243707579 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2243707579  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___rightVector_7)); }
	inline Vector2_t2243707579  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2243707579 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2243707579  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2243707579  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2243707579 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2243707579  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2243707579  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2243707579 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2243707579  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2243707579_H
#ifndef RECT_T3681755626_H
#define RECT_T3681755626_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t3681755626 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t3681755626, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t3681755626, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t3681755626, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t3681755626, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T3681755626_H
#ifndef VECTOR3_T2243707580_H
#define VECTOR3_T2243707580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t2243707580 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t2243707580_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t2243707580  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t2243707580  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t2243707580  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t2243707580  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t2243707580  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t2243707580  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t2243707580  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t2243707580  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t2243707580  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t2243707580  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___zeroVector_4)); }
	inline Vector3_t2243707580  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t2243707580 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t2243707580  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___oneVector_5)); }
	inline Vector3_t2243707580  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t2243707580 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t2243707580  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___upVector_6)); }
	inline Vector3_t2243707580  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t2243707580 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t2243707580  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___downVector_7)); }
	inline Vector3_t2243707580  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t2243707580 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t2243707580  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___leftVector_8)); }
	inline Vector3_t2243707580  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t2243707580 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t2243707580  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___rightVector_9)); }
	inline Vector3_t2243707580  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t2243707580 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t2243707580  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___forwardVector_10)); }
	inline Vector3_t2243707580  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t2243707580 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t2243707580  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___backVector_11)); }
	inline Vector3_t2243707580  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t2243707580 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t2243707580  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t2243707580  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t2243707580 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t2243707580  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t2243707580  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t2243707580 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t2243707580  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T2243707580_H
#ifndef HTTPREQUESTEVENTARGS_T918469868_H
#define HTTPREQUESTEVENTARGS_T918469868_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Server.HttpRequestEventArgs
struct  HttpRequestEventArgs_t918469868  : public EventArgs_t3289624707
{
public:
	// WebSocketSharp.Net.HttpListenerRequest WebSocketSharp.Server.HttpRequestEventArgs::_request
	HttpListenerRequest_t2316381291 * ____request_1;
	// WebSocketSharp.Net.HttpListenerResponse WebSocketSharp.Server.HttpRequestEventArgs::_response
	HttpListenerResponse_t2223360553 * ____response_2;

public:
	inline static int32_t get_offset_of__request_1() { return static_cast<int32_t>(offsetof(HttpRequestEventArgs_t918469868, ____request_1)); }
	inline HttpListenerRequest_t2316381291 * get__request_1() const { return ____request_1; }
	inline HttpListenerRequest_t2316381291 ** get_address_of__request_1() { return &____request_1; }
	inline void set__request_1(HttpListenerRequest_t2316381291 * value)
	{
		____request_1 = value;
		Il2CppCodeGenWriteBarrier((&____request_1), value);
	}

	inline static int32_t get_offset_of__response_2() { return static_cast<int32_t>(offsetof(HttpRequestEventArgs_t918469868, ____response_2)); }
	inline HttpListenerResponse_t2223360553 * get__response_2() const { return ____response_2; }
	inline HttpListenerResponse_t2223360553 ** get_address_of__response_2() { return &____response_2; }
	inline void set__response_2(HttpListenerResponse_t2223360553 * value)
	{
		____response_2 = value;
		Il2CppCodeGenWriteBarrier((&____response_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPREQUESTEVENTARGS_T918469868_H
#ifndef COLOR_T2020392075_H
#define COLOR_T2020392075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2020392075 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2020392075_H
#ifndef TCPLISTENERWEBSOCKETCONTEXT_T1695227117_H
#define TCPLISTENERWEBSOCKETCONTEXT_T1695227117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext
struct  TcpListenerWebSocketContext_t1695227117  : public WebSocketContext_t3488732344
{
public:
	// WebSocketSharp.Net.CookieCollection WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext::_cookies
	CookieCollection_t4248997468 * ____cookies_0;
	// WebSocketSharp.Logger WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext::_logger
	Logger_t2598199114 * ____logger_1;
	// System.Collections.Specialized.NameValueCollection WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext::_queryString
	NameValueCollection_t3047564564 * ____queryString_2;
	// WebSocketSharp.HttpRequest WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext::_request
	HttpRequest_t1845443631 * ____request_3;
	// System.Boolean WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext::_secure
	bool ____secure_4;
	// System.IO.Stream WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext::_stream
	Stream_t3255436806 * ____stream_5;
	// System.Net.Sockets.TcpClient WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext::_tcpClient
	TcpClient_t408947970 * ____tcpClient_6;
	// System.Uri WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext::_uri
	Uri_t19570940 * ____uri_7;
	// System.Security.Principal.IPrincipal WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext::_user
	RuntimeObject* ____user_8;
	// WebSocketSharp.WebSocket WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext::_websocket
	WebSocket_t3268376029 * ____websocket_9;

public:
	inline static int32_t get_offset_of__cookies_0() { return static_cast<int32_t>(offsetof(TcpListenerWebSocketContext_t1695227117, ____cookies_0)); }
	inline CookieCollection_t4248997468 * get__cookies_0() const { return ____cookies_0; }
	inline CookieCollection_t4248997468 ** get_address_of__cookies_0() { return &____cookies_0; }
	inline void set__cookies_0(CookieCollection_t4248997468 * value)
	{
		____cookies_0 = value;
		Il2CppCodeGenWriteBarrier((&____cookies_0), value);
	}

	inline static int32_t get_offset_of__logger_1() { return static_cast<int32_t>(offsetof(TcpListenerWebSocketContext_t1695227117, ____logger_1)); }
	inline Logger_t2598199114 * get__logger_1() const { return ____logger_1; }
	inline Logger_t2598199114 ** get_address_of__logger_1() { return &____logger_1; }
	inline void set__logger_1(Logger_t2598199114 * value)
	{
		____logger_1 = value;
		Il2CppCodeGenWriteBarrier((&____logger_1), value);
	}

	inline static int32_t get_offset_of__queryString_2() { return static_cast<int32_t>(offsetof(TcpListenerWebSocketContext_t1695227117, ____queryString_2)); }
	inline NameValueCollection_t3047564564 * get__queryString_2() const { return ____queryString_2; }
	inline NameValueCollection_t3047564564 ** get_address_of__queryString_2() { return &____queryString_2; }
	inline void set__queryString_2(NameValueCollection_t3047564564 * value)
	{
		____queryString_2 = value;
		Il2CppCodeGenWriteBarrier((&____queryString_2), value);
	}

	inline static int32_t get_offset_of__request_3() { return static_cast<int32_t>(offsetof(TcpListenerWebSocketContext_t1695227117, ____request_3)); }
	inline HttpRequest_t1845443631 * get__request_3() const { return ____request_3; }
	inline HttpRequest_t1845443631 ** get_address_of__request_3() { return &____request_3; }
	inline void set__request_3(HttpRequest_t1845443631 * value)
	{
		____request_3 = value;
		Il2CppCodeGenWriteBarrier((&____request_3), value);
	}

	inline static int32_t get_offset_of__secure_4() { return static_cast<int32_t>(offsetof(TcpListenerWebSocketContext_t1695227117, ____secure_4)); }
	inline bool get__secure_4() const { return ____secure_4; }
	inline bool* get_address_of__secure_4() { return &____secure_4; }
	inline void set__secure_4(bool value)
	{
		____secure_4 = value;
	}

	inline static int32_t get_offset_of__stream_5() { return static_cast<int32_t>(offsetof(TcpListenerWebSocketContext_t1695227117, ____stream_5)); }
	inline Stream_t3255436806 * get__stream_5() const { return ____stream_5; }
	inline Stream_t3255436806 ** get_address_of__stream_5() { return &____stream_5; }
	inline void set__stream_5(Stream_t3255436806 * value)
	{
		____stream_5 = value;
		Il2CppCodeGenWriteBarrier((&____stream_5), value);
	}

	inline static int32_t get_offset_of__tcpClient_6() { return static_cast<int32_t>(offsetof(TcpListenerWebSocketContext_t1695227117, ____tcpClient_6)); }
	inline TcpClient_t408947970 * get__tcpClient_6() const { return ____tcpClient_6; }
	inline TcpClient_t408947970 ** get_address_of__tcpClient_6() { return &____tcpClient_6; }
	inline void set__tcpClient_6(TcpClient_t408947970 * value)
	{
		____tcpClient_6 = value;
		Il2CppCodeGenWriteBarrier((&____tcpClient_6), value);
	}

	inline static int32_t get_offset_of__uri_7() { return static_cast<int32_t>(offsetof(TcpListenerWebSocketContext_t1695227117, ____uri_7)); }
	inline Uri_t19570940 * get__uri_7() const { return ____uri_7; }
	inline Uri_t19570940 ** get_address_of__uri_7() { return &____uri_7; }
	inline void set__uri_7(Uri_t19570940 * value)
	{
		____uri_7 = value;
		Il2CppCodeGenWriteBarrier((&____uri_7), value);
	}

	inline static int32_t get_offset_of__user_8() { return static_cast<int32_t>(offsetof(TcpListenerWebSocketContext_t1695227117, ____user_8)); }
	inline RuntimeObject* get__user_8() const { return ____user_8; }
	inline RuntimeObject** get_address_of__user_8() { return &____user_8; }
	inline void set__user_8(RuntimeObject* value)
	{
		____user_8 = value;
		Il2CppCodeGenWriteBarrier((&____user_8), value);
	}

	inline static int32_t get_offset_of__websocket_9() { return static_cast<int32_t>(offsetof(TcpListenerWebSocketContext_t1695227117, ____websocket_9)); }
	inline WebSocket_t3268376029 * get__websocket_9() const { return ____websocket_9; }
	inline WebSocket_t3268376029 ** get_address_of__websocket_9() { return &____websocket_9; }
	inline void set__websocket_9(WebSocket_t3268376029 * value)
	{
		____websocket_9 = value;
		Il2CppCodeGenWriteBarrier((&____websocket_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TCPLISTENERWEBSOCKETCONTEXT_T1695227117_H
#ifndef DRIVENRECTTRANSFORMTRACKER_T154385424_H
#define DRIVENRECTTRANSFORMTRACKER_T154385424_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.DrivenRectTransformTracker
struct  DrivenRectTransformTracker_t154385424 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRIVENRECTTRANSFORMTRACKER_T154385424_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	IntPtr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline IntPtr_t get_Zero_1() const { return ___Zero_1; }
	inline IntPtr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(IntPtr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef OBJECT_T1021602117_H
#define OBJECT_T1021602117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1021602117  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	IntPtr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1021602117, ___m_CachedPtr_0)); }
	inline IntPtr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline IntPtr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(IntPtr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1021602117_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1021602117_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1021602117_H
#ifndef INPUTSTATE_T2016389849_H
#define INPUTSTATE_T2016389849_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.InputState
struct  InputState_t2016389849 
{
public:
	// System.Int32 WebSocketSharp.Net.InputState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InputState_t2016389849, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTSTATE_T2016389849_H
#ifndef SPEECHBUBBLETYPE_T1311649088_H
#define SPEECHBUBBLETYPE_T1311649088_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VikingCrewTools.SpeechbubbleManager/SpeechbubbleType
struct  SpeechbubbleType_t1311649088 
{
public:
	// System.Int32 VikingCrewTools.SpeechbubbleManager/SpeechbubbleType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SpeechbubbleType_t1311649088, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPEECHBUBBLETYPE_T1311649088_H
#ifndef TEXTANCHOR_T112990806_H
#define TEXTANCHOR_T112990806_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextAnchor
struct  TextAnchor_t112990806 
{
public:
	// System.Int32 UnityEngine.TextAnchor::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextAnchor_t112990806, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTANCHOR_T112990806_H
#ifndef LINESTATE_T825951569_H
#define LINESTATE_T825951569_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.LineState
struct  LineState_t825951569 
{
public:
	// System.Int32 WebSocketSharp.Net.LineState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LineState_t825951569, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINESTATE_T825951569_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1486305144_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1486305144_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t1486305144  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t1486305144_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=8 <PrivateImplementationDetails>::$field-4EEB46F720594D7409B9A8E1B6275694A0C4491D
	U24ArrayTypeU3D8_t1459944466  ___U24fieldU2D4EEB46F720594D7409B9A8E1B6275694A0C4491D_0;
	// <PrivateImplementationDetails>/$ArrayType=16 <PrivateImplementationDetails>::$field-EB8077FF0D4F4A927EB9224048538295DEF1227A
	U24ArrayTypeU3D16_t3894236545  ___U24fieldU2DEB8077FF0D4F4A927EB9224048538295DEF1227A_1;
	// <PrivateImplementationDetails>/$ArrayType=8 <PrivateImplementationDetails>::$field-6532575F81314AADD4BCFD96F1517D5BBFF2B60B
	U24ArrayTypeU3D8_t1459944466  ___U24fieldU2D6532575F81314AADD4BCFD96F1517D5BBFF2B60B_2;

public:
	inline static int32_t get_offset_of_U24fieldU2D4EEB46F720594D7409B9A8E1B6275694A0C4491D_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305144_StaticFields, ___U24fieldU2D4EEB46F720594D7409B9A8E1B6275694A0C4491D_0)); }
	inline U24ArrayTypeU3D8_t1459944466  get_U24fieldU2D4EEB46F720594D7409B9A8E1B6275694A0C4491D_0() const { return ___U24fieldU2D4EEB46F720594D7409B9A8E1B6275694A0C4491D_0; }
	inline U24ArrayTypeU3D8_t1459944466 * get_address_of_U24fieldU2D4EEB46F720594D7409B9A8E1B6275694A0C4491D_0() { return &___U24fieldU2D4EEB46F720594D7409B9A8E1B6275694A0C4491D_0; }
	inline void set_U24fieldU2D4EEB46F720594D7409B9A8E1B6275694A0C4491D_0(U24ArrayTypeU3D8_t1459944466  value)
	{
		___U24fieldU2D4EEB46F720594D7409B9A8E1B6275694A0C4491D_0 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DEB8077FF0D4F4A927EB9224048538295DEF1227A_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305144_StaticFields, ___U24fieldU2DEB8077FF0D4F4A927EB9224048538295DEF1227A_1)); }
	inline U24ArrayTypeU3D16_t3894236545  get_U24fieldU2DEB8077FF0D4F4A927EB9224048538295DEF1227A_1() const { return ___U24fieldU2DEB8077FF0D4F4A927EB9224048538295DEF1227A_1; }
	inline U24ArrayTypeU3D16_t3894236545 * get_address_of_U24fieldU2DEB8077FF0D4F4A927EB9224048538295DEF1227A_1() { return &___U24fieldU2DEB8077FF0D4F4A927EB9224048538295DEF1227A_1; }
	inline void set_U24fieldU2DEB8077FF0D4F4A927EB9224048538295DEF1227A_1(U24ArrayTypeU3D16_t3894236545  value)
	{
		___U24fieldU2DEB8077FF0D4F4A927EB9224048538295DEF1227A_1 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D6532575F81314AADD4BCFD96F1517D5BBFF2B60B_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305144_StaticFields, ___U24fieldU2D6532575F81314AADD4BCFD96F1517D5BBFF2B60B_2)); }
	inline U24ArrayTypeU3D8_t1459944466  get_U24fieldU2D6532575F81314AADD4BCFD96F1517D5BBFF2B60B_2() const { return ___U24fieldU2D6532575F81314AADD4BCFD96F1517D5BBFF2B60B_2; }
	inline U24ArrayTypeU3D8_t1459944466 * get_address_of_U24fieldU2D6532575F81314AADD4BCFD96F1517D5BBFF2B60B_2() { return &___U24fieldU2D6532575F81314AADD4BCFD96F1517D5BBFF2B60B_2; }
	inline void set_U24fieldU2D6532575F81314AADD4BCFD96F1517D5BBFF2B60B_2(U24ArrayTypeU3D8_t1459944466  value)
	{
		___U24fieldU2D6532575F81314AADD4BCFD96F1517D5BBFF2B60B_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1486305144_H
#ifndef SSLPROTOCOLS_T894678499_H
#define SSLPROTOCOLS_T894678499_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Authentication.SslProtocols
struct  SslProtocols_t894678499 
{
public:
	// System.Int32 System.Security.Authentication.SslProtocols::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SslProtocols_t894678499, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SSLPROTOCOLS_T894678499_H
#ifndef FIN_T2752139063_H
#define FIN_T2752139063_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Fin
struct  Fin_t2752139063 
{
public:
	// System.Byte WebSocketSharp.Fin::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Fin_t2752139063, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIN_T2752139063_H
#ifndef ENUMERATOR_T2765411386_H
#define ENUMERATOR_T2765411386_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>
struct  Enumerator_t2765411386 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::dictionary
	Dictionary_2_t1445386684 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::stamp
	int32_t ___stamp_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::current
	KeyValuePair_2_t3497699202  ___current_3;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_t2765411386, ___dictionary_0)); }
	inline Dictionary_2_t1445386684 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t1445386684 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t1445386684 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t2765411386, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_stamp_2() { return static_cast<int32_t>(offsetof(Enumerator_t2765411386, ___stamp_2)); }
	inline int32_t get_stamp_2() const { return ___stamp_2; }
	inline int32_t* get_address_of_stamp_2() { return &___stamp_2; }
	inline void set_stamp_2(int32_t value)
	{
		___stamp_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t2765411386, ___current_3)); }
	inline KeyValuePair_2_t3497699202  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t3497699202 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t3497699202  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2765411386_H
#ifndef COMPRESSIONMETHOD_T4066553457_H
#define COMPRESSIONMETHOD_T4066553457_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.CompressionMethod
struct  CompressionMethod_t4066553457 
{
public:
	// System.Byte WebSocketSharp.CompressionMethod::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CompressionMethod_t4066553457, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPRESSIONMETHOD_T4066553457_H
#ifndef CLOSESTATUSCODE_T2945181741_H
#define CLOSESTATUSCODE_T2945181741_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.CloseStatusCode
struct  CloseStatusCode_t2945181741 
{
public:
	// System.UInt16 WebSocketSharp.CloseStatusCode::value__
	uint16_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CloseStatusCode_t2945181741, ___value___1)); }
	inline uint16_t get_value___1() const { return ___value___1; }
	inline uint16_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint16_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLOSESTATUSCODE_T2945181741_H
#ifndef MASK_T1111889066_H
#define MASK_T1111889066_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Mask
struct  Mask_t1111889066 
{
public:
	// System.Byte WebSocketSharp.Mask::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Mask_t1111889066, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASK_T1111889066_H
#ifndef AUTHENTICATIONSCHEMES_T29593226_H
#define AUTHENTICATIONSCHEMES_T29593226_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.AuthenticationSchemes
struct  AuthenticationSchemes_t29593226 
{
public:
	// System.Int32 WebSocketSharp.Net.AuthenticationSchemes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AuthenticationSchemes_t29593226, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTHENTICATIONSCHEMES_T29593226_H
#ifndef HTTPHEADERTYPE_T1518115223_H
#define HTTPHEADERTYPE_T1518115223_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.HttpHeaderType
struct  HttpHeaderType_t1518115223 
{
public:
	// System.Int32 WebSocketSharp.Net.HttpHeaderType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HttpHeaderType_t1518115223, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPHEADERTYPE_T1518115223_H
#ifndef INPUTCHUNKSTATE_T126533044_H
#define INPUTCHUNKSTATE_T126533044_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.InputChunkState
struct  InputChunkState_t126533044 
{
public:
	// System.Int32 WebSocketSharp.Net.InputChunkState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InputChunkState_t126533044, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTCHUNKSTATE_T126533044_H
#ifndef DATETIMEKIND_T2186819611_H
#define DATETIMEKIND_T2186819611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeKind
struct  DateTimeKind_t2186819611 
{
public:
	// System.Int32 System.DateTimeKind::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeKind_t2186819611, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEKIND_T2186819611_H
#ifndef RSV_T1058189029_H
#define RSV_T1058189029_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Rsv
struct  Rsv_t1058189029 
{
public:
	// System.Byte WebSocketSharp.Rsv::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Rsv_t1058189029, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RSV_T1058189029_H
#ifndef SERVERSTATE_T3054640078_H
#define SERVERSTATE_T3054640078_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Server.ServerState
struct  ServerState_t3054640078 
{
public:
	// System.Int32 WebSocketSharp.Server.ServerState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ServerState_t3054640078, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERVERSTATE_T3054640078_H
#ifndef OPCODE_T2313788840_H
#define OPCODE_T2313788840_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Opcode
struct  Opcode_t2313788840 
{
public:
	// System.Byte WebSocketSharp.Opcode::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Opcode_t2313788840, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPCODE_T2313788840_H
#ifndef WEBSOCKETSTATE_T2935910988_H
#define WEBSOCKETSTATE_T2935910988_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.WebSocketState
struct  WebSocketState_t2935910988 
{
public:
	// System.UInt16 WebSocketSharp.WebSocketState::value__
	uint16_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(WebSocketState_t2935910988, ___value___1)); }
	inline uint16_t get_value___1() const { return ___value___1; }
	inline uint16_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint16_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBSOCKETSTATE_T2935910988_H
#ifndef QUERYSTRINGCOLLECTION_T595917821_H
#define QUERYSTRINGCOLLECTION_T595917821_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.QueryStringCollection
struct  QueryStringCollection_t595917821  : public NameValueCollection_t3047564564
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUERYSTRINGCOLLECTION_T595917821_H
#ifndef WEBSOCKETEXCEPTION_T1348391352_H
#define WEBSOCKETEXCEPTION_T1348391352_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.WebSocketException
struct  WebSocketException_t1348391352  : public Exception_t1927440687
{
public:
	// WebSocketSharp.CloseStatusCode WebSocketSharp.WebSocketException::_code
	uint16_t ____code_11;

public:
	inline static int32_t get_offset_of__code_11() { return static_cast<int32_t>(offsetof(WebSocketException_t1348391352, ____code_11)); }
	inline uint16_t get__code_11() const { return ____code_11; }
	inline uint16_t* get_address_of__code_11() { return &____code_11; }
	inline void set__code_11(uint16_t value)
	{
		____code_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBSOCKETEXCEPTION_T1348391352_H
#ifndef WEBHEADERCOLLECTION_T1932982249_H
#define WEBHEADERCOLLECTION_T1932982249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.WebHeaderCollection
struct  WebHeaderCollection_t1932982249  : public NameValueCollection_t3047564564
{
public:
	// System.Boolean WebSocketSharp.Net.WebHeaderCollection::_internallyUsed
	bool ____internallyUsed_13;
	// WebSocketSharp.Net.HttpHeaderType WebSocketSharp.Net.WebHeaderCollection::_state
	int32_t ____state_14;

public:
	inline static int32_t get_offset_of__internallyUsed_13() { return static_cast<int32_t>(offsetof(WebHeaderCollection_t1932982249, ____internallyUsed_13)); }
	inline bool get__internallyUsed_13() const { return ____internallyUsed_13; }
	inline bool* get_address_of__internallyUsed_13() { return &____internallyUsed_13; }
	inline void set__internallyUsed_13(bool value)
	{
		____internallyUsed_13 = value;
	}

	inline static int32_t get_offset_of__state_14() { return static_cast<int32_t>(offsetof(WebHeaderCollection_t1932982249, ____state_14)); }
	inline int32_t get__state_14() const { return ____state_14; }
	inline int32_t* get_address_of__state_14() { return &____state_14; }
	inline void set__state_14(int32_t value)
	{
		____state_14 = value;
	}
};

struct WebHeaderCollection_t1932982249_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,WebSocketSharp.Net.HttpHeaderInfo> WebSocketSharp.Net.WebHeaderCollection::_headers
	Dictionary_2_t4011098823 * ____headers_12;

public:
	inline static int32_t get_offset_of__headers_12() { return static_cast<int32_t>(offsetof(WebHeaderCollection_t1932982249_StaticFields, ____headers_12)); }
	inline Dictionary_2_t4011098823 * get__headers_12() const { return ____headers_12; }
	inline Dictionary_2_t4011098823 ** get_address_of__headers_12() { return &____headers_12; }
	inline void set__headers_12(Dictionary_2_t4011098823 * value)
	{
		____headers_12 = value;
		Il2CppCodeGenWriteBarrier((&____headers_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBHEADERCOLLECTION_T1932982249_H
#ifndef SSLCONFIGURATION_T760772650_H
#define SSLCONFIGURATION_T760772650_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.SslConfiguration
struct  SslConfiguration_t760772650  : public RuntimeObject
{
public:
	// System.Net.Security.LocalCertificateSelectionCallback WebSocketSharp.Net.SslConfiguration::_certSelectionCallback
	LocalCertificateSelectionCallback_t3696771181 * ____certSelectionCallback_0;
	// System.Net.Security.RemoteCertificateValidationCallback WebSocketSharp.Net.SslConfiguration::_certValidationCallback
	RemoteCertificateValidationCallback_t2756269959 * ____certValidationCallback_1;
	// System.Boolean WebSocketSharp.Net.SslConfiguration::_checkCertRevocation
	bool ____checkCertRevocation_2;
	// System.Security.Authentication.SslProtocols WebSocketSharp.Net.SslConfiguration::_enabledProtocols
	int32_t ____enabledProtocols_3;

public:
	inline static int32_t get_offset_of__certSelectionCallback_0() { return static_cast<int32_t>(offsetof(SslConfiguration_t760772650, ____certSelectionCallback_0)); }
	inline LocalCertificateSelectionCallback_t3696771181 * get__certSelectionCallback_0() const { return ____certSelectionCallback_0; }
	inline LocalCertificateSelectionCallback_t3696771181 ** get_address_of__certSelectionCallback_0() { return &____certSelectionCallback_0; }
	inline void set__certSelectionCallback_0(LocalCertificateSelectionCallback_t3696771181 * value)
	{
		____certSelectionCallback_0 = value;
		Il2CppCodeGenWriteBarrier((&____certSelectionCallback_0), value);
	}

	inline static int32_t get_offset_of__certValidationCallback_1() { return static_cast<int32_t>(offsetof(SslConfiguration_t760772650, ____certValidationCallback_1)); }
	inline RemoteCertificateValidationCallback_t2756269959 * get__certValidationCallback_1() const { return ____certValidationCallback_1; }
	inline RemoteCertificateValidationCallback_t2756269959 ** get_address_of__certValidationCallback_1() { return &____certValidationCallback_1; }
	inline void set__certValidationCallback_1(RemoteCertificateValidationCallback_t2756269959 * value)
	{
		____certValidationCallback_1 = value;
		Il2CppCodeGenWriteBarrier((&____certValidationCallback_1), value);
	}

	inline static int32_t get_offset_of__checkCertRevocation_2() { return static_cast<int32_t>(offsetof(SslConfiguration_t760772650, ____checkCertRevocation_2)); }
	inline bool get__checkCertRevocation_2() const { return ____checkCertRevocation_2; }
	inline bool* get_address_of__checkCertRevocation_2() { return &____checkCertRevocation_2; }
	inline void set__checkCertRevocation_2(bool value)
	{
		____checkCertRevocation_2 = value;
	}

	inline static int32_t get_offset_of__enabledProtocols_3() { return static_cast<int32_t>(offsetof(SslConfiguration_t760772650, ____enabledProtocols_3)); }
	inline int32_t get__enabledProtocols_3() const { return ____enabledProtocols_3; }
	inline int32_t* get_address_of__enabledProtocols_3() { return &____enabledProtocols_3; }
	inline void set__enabledProtocols_3(int32_t value)
	{
		____enabledProtocols_3 = value;
	}
};

struct SslConfiguration_t760772650_StaticFields
{
public:
	// System.Net.Security.LocalCertificateSelectionCallback WebSocketSharp.Net.SslConfiguration::<>f__am$cache0
	LocalCertificateSelectionCallback_t3696771181 * ___U3CU3Ef__amU24cache0_4;
	// System.Net.Security.RemoteCertificateValidationCallback WebSocketSharp.Net.SslConfiguration::<>f__am$cache1
	RemoteCertificateValidationCallback_t2756269959 * ___U3CU3Ef__amU24cache1_5;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_4() { return static_cast<int32_t>(offsetof(SslConfiguration_t760772650_StaticFields, ___U3CU3Ef__amU24cache0_4)); }
	inline LocalCertificateSelectionCallback_t3696771181 * get_U3CU3Ef__amU24cache0_4() const { return ___U3CU3Ef__amU24cache0_4; }
	inline LocalCertificateSelectionCallback_t3696771181 ** get_address_of_U3CU3Ef__amU24cache0_4() { return &___U3CU3Ef__amU24cache0_4; }
	inline void set_U3CU3Ef__amU24cache0_4(LocalCertificateSelectionCallback_t3696771181 * value)
	{
		___U3CU3Ef__amU24cache0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_5() { return static_cast<int32_t>(offsetof(SslConfiguration_t760772650_StaticFields, ___U3CU3Ef__amU24cache1_5)); }
	inline RemoteCertificateValidationCallback_t2756269959 * get_U3CU3Ef__amU24cache1_5() const { return ___U3CU3Ef__amU24cache1_5; }
	inline RemoteCertificateValidationCallback_t2756269959 ** get_address_of_U3CU3Ef__amU24cache1_5() { return &___U3CU3Ef__amU24cache1_5; }
	inline void set_U3CU3Ef__amU24cache1_5(RemoteCertificateValidationCallback_t2756269959 * value)
	{
		___U3CU3Ef__amU24cache1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SSLCONFIGURATION_T760772650_H
#ifndef DATETIME_T693205669_H
#define DATETIME_T693205669_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t693205669 
{
public:
	// System.TimeSpan System.DateTime::ticks
	TimeSpan_t3430258949  ___ticks_10;
	// System.DateTimeKind System.DateTime::kind
	int32_t ___kind_11;

public:
	inline static int32_t get_offset_of_ticks_10() { return static_cast<int32_t>(offsetof(DateTime_t693205669, ___ticks_10)); }
	inline TimeSpan_t3430258949  get_ticks_10() const { return ___ticks_10; }
	inline TimeSpan_t3430258949 * get_address_of_ticks_10() { return &___ticks_10; }
	inline void set_ticks_10(TimeSpan_t3430258949  value)
	{
		___ticks_10 = value;
	}

	inline static int32_t get_offset_of_kind_11() { return static_cast<int32_t>(offsetof(DateTime_t693205669, ___kind_11)); }
	inline int32_t get_kind_11() const { return ___kind_11; }
	inline int32_t* get_address_of_kind_11() { return &___kind_11; }
	inline void set_kind_11(int32_t value)
	{
		___kind_11 = value;
	}
};

struct DateTime_t693205669_StaticFields
{
public:
	// System.DateTime System.DateTime::MaxValue
	DateTime_t693205669  ___MaxValue_12;
	// System.DateTime System.DateTime::MinValue
	DateTime_t693205669  ___MinValue_13;
	// System.String[] System.DateTime::ParseTimeFormats
	StringU5BU5D_t1642385972* ___ParseTimeFormats_14;
	// System.String[] System.DateTime::ParseYearDayMonthFormats
	StringU5BU5D_t1642385972* ___ParseYearDayMonthFormats_15;
	// System.String[] System.DateTime::ParseYearMonthDayFormats
	StringU5BU5D_t1642385972* ___ParseYearMonthDayFormats_16;
	// System.String[] System.DateTime::ParseDayMonthYearFormats
	StringU5BU5D_t1642385972* ___ParseDayMonthYearFormats_17;
	// System.String[] System.DateTime::ParseMonthDayYearFormats
	StringU5BU5D_t1642385972* ___ParseMonthDayYearFormats_18;
	// System.String[] System.DateTime::MonthDayShortFormats
	StringU5BU5D_t1642385972* ___MonthDayShortFormats_19;
	// System.String[] System.DateTime::DayMonthShortFormats
	StringU5BU5D_t1642385972* ___DayMonthShortFormats_20;
	// System.Int32[] System.DateTime::daysmonth
	Int32U5BU5D_t3030399641* ___daysmonth_21;
	// System.Int32[] System.DateTime::daysmonthleap
	Int32U5BU5D_t3030399641* ___daysmonthleap_22;
	// System.Object System.DateTime::to_local_time_span_object
	RuntimeObject * ___to_local_time_span_object_23;
	// System.Int64 System.DateTime::last_now
	int64_t ___last_now_24;

public:
	inline static int32_t get_offset_of_MaxValue_12() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___MaxValue_12)); }
	inline DateTime_t693205669  get_MaxValue_12() const { return ___MaxValue_12; }
	inline DateTime_t693205669 * get_address_of_MaxValue_12() { return &___MaxValue_12; }
	inline void set_MaxValue_12(DateTime_t693205669  value)
	{
		___MaxValue_12 = value;
	}

	inline static int32_t get_offset_of_MinValue_13() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___MinValue_13)); }
	inline DateTime_t693205669  get_MinValue_13() const { return ___MinValue_13; }
	inline DateTime_t693205669 * get_address_of_MinValue_13() { return &___MinValue_13; }
	inline void set_MinValue_13(DateTime_t693205669  value)
	{
		___MinValue_13 = value;
	}

	inline static int32_t get_offset_of_ParseTimeFormats_14() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseTimeFormats_14)); }
	inline StringU5BU5D_t1642385972* get_ParseTimeFormats_14() const { return ___ParseTimeFormats_14; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseTimeFormats_14() { return &___ParseTimeFormats_14; }
	inline void set_ParseTimeFormats_14(StringU5BU5D_t1642385972* value)
	{
		___ParseTimeFormats_14 = value;
		Il2CppCodeGenWriteBarrier((&___ParseTimeFormats_14), value);
	}

	inline static int32_t get_offset_of_ParseYearDayMonthFormats_15() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseYearDayMonthFormats_15)); }
	inline StringU5BU5D_t1642385972* get_ParseYearDayMonthFormats_15() const { return ___ParseYearDayMonthFormats_15; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseYearDayMonthFormats_15() { return &___ParseYearDayMonthFormats_15; }
	inline void set_ParseYearDayMonthFormats_15(StringU5BU5D_t1642385972* value)
	{
		___ParseYearDayMonthFormats_15 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearDayMonthFormats_15), value);
	}

	inline static int32_t get_offset_of_ParseYearMonthDayFormats_16() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseYearMonthDayFormats_16)); }
	inline StringU5BU5D_t1642385972* get_ParseYearMonthDayFormats_16() const { return ___ParseYearMonthDayFormats_16; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseYearMonthDayFormats_16() { return &___ParseYearMonthDayFormats_16; }
	inline void set_ParseYearMonthDayFormats_16(StringU5BU5D_t1642385972* value)
	{
		___ParseYearMonthDayFormats_16 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearMonthDayFormats_16), value);
	}

	inline static int32_t get_offset_of_ParseDayMonthYearFormats_17() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseDayMonthYearFormats_17)); }
	inline StringU5BU5D_t1642385972* get_ParseDayMonthYearFormats_17() const { return ___ParseDayMonthYearFormats_17; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseDayMonthYearFormats_17() { return &___ParseDayMonthYearFormats_17; }
	inline void set_ParseDayMonthYearFormats_17(StringU5BU5D_t1642385972* value)
	{
		___ParseDayMonthYearFormats_17 = value;
		Il2CppCodeGenWriteBarrier((&___ParseDayMonthYearFormats_17), value);
	}

	inline static int32_t get_offset_of_ParseMonthDayYearFormats_18() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseMonthDayYearFormats_18)); }
	inline StringU5BU5D_t1642385972* get_ParseMonthDayYearFormats_18() const { return ___ParseMonthDayYearFormats_18; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseMonthDayYearFormats_18() { return &___ParseMonthDayYearFormats_18; }
	inline void set_ParseMonthDayYearFormats_18(StringU5BU5D_t1642385972* value)
	{
		___ParseMonthDayYearFormats_18 = value;
		Il2CppCodeGenWriteBarrier((&___ParseMonthDayYearFormats_18), value);
	}

	inline static int32_t get_offset_of_MonthDayShortFormats_19() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___MonthDayShortFormats_19)); }
	inline StringU5BU5D_t1642385972* get_MonthDayShortFormats_19() const { return ___MonthDayShortFormats_19; }
	inline StringU5BU5D_t1642385972** get_address_of_MonthDayShortFormats_19() { return &___MonthDayShortFormats_19; }
	inline void set_MonthDayShortFormats_19(StringU5BU5D_t1642385972* value)
	{
		___MonthDayShortFormats_19 = value;
		Il2CppCodeGenWriteBarrier((&___MonthDayShortFormats_19), value);
	}

	inline static int32_t get_offset_of_DayMonthShortFormats_20() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___DayMonthShortFormats_20)); }
	inline StringU5BU5D_t1642385972* get_DayMonthShortFormats_20() const { return ___DayMonthShortFormats_20; }
	inline StringU5BU5D_t1642385972** get_address_of_DayMonthShortFormats_20() { return &___DayMonthShortFormats_20; }
	inline void set_DayMonthShortFormats_20(StringU5BU5D_t1642385972* value)
	{
		___DayMonthShortFormats_20 = value;
		Il2CppCodeGenWriteBarrier((&___DayMonthShortFormats_20), value);
	}

	inline static int32_t get_offset_of_daysmonth_21() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___daysmonth_21)); }
	inline Int32U5BU5D_t3030399641* get_daysmonth_21() const { return ___daysmonth_21; }
	inline Int32U5BU5D_t3030399641** get_address_of_daysmonth_21() { return &___daysmonth_21; }
	inline void set_daysmonth_21(Int32U5BU5D_t3030399641* value)
	{
		___daysmonth_21 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonth_21), value);
	}

	inline static int32_t get_offset_of_daysmonthleap_22() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___daysmonthleap_22)); }
	inline Int32U5BU5D_t3030399641* get_daysmonthleap_22() const { return ___daysmonthleap_22; }
	inline Int32U5BU5D_t3030399641** get_address_of_daysmonthleap_22() { return &___daysmonthleap_22; }
	inline void set_daysmonthleap_22(Int32U5BU5D_t3030399641* value)
	{
		___daysmonthleap_22 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonthleap_22), value);
	}

	inline static int32_t get_offset_of_to_local_time_span_object_23() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___to_local_time_span_object_23)); }
	inline RuntimeObject * get_to_local_time_span_object_23() const { return ___to_local_time_span_object_23; }
	inline RuntimeObject ** get_address_of_to_local_time_span_object_23() { return &___to_local_time_span_object_23; }
	inline void set_to_local_time_span_object_23(RuntimeObject * value)
	{
		___to_local_time_span_object_23 = value;
		Il2CppCodeGenWriteBarrier((&___to_local_time_span_object_23), value);
	}

	inline static int32_t get_offset_of_last_now_24() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___last_now_24)); }
	inline int64_t get_last_now_24() const { return ___last_now_24; }
	inline int64_t* get_address_of_last_now_24() { return &___last_now_24; }
	inline void set_last_now_24(int64_t value)
	{
		___last_now_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T693205669_H
#ifndef WEBSOCKETFRAME_T764750278_H
#define WEBSOCKETFRAME_T764750278_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.WebSocketFrame
struct  WebSocketFrame_t764750278  : public RuntimeObject
{
public:
	// System.Byte[] WebSocketSharp.WebSocketFrame::_extPayloadLength
	ByteU5BU5D_t3397334013* ____extPayloadLength_0;
	// WebSocketSharp.Fin WebSocketSharp.WebSocketFrame::_fin
	uint8_t ____fin_1;
	// WebSocketSharp.Mask WebSocketSharp.WebSocketFrame::_mask
	uint8_t ____mask_2;
	// System.Byte[] WebSocketSharp.WebSocketFrame::_maskingKey
	ByteU5BU5D_t3397334013* ____maskingKey_3;
	// WebSocketSharp.Opcode WebSocketSharp.WebSocketFrame::_opcode
	uint8_t ____opcode_4;
	// WebSocketSharp.PayloadData WebSocketSharp.WebSocketFrame::_payloadData
	PayloadData_t3839327312 * ____payloadData_5;
	// System.Byte WebSocketSharp.WebSocketFrame::_payloadLength
	uint8_t ____payloadLength_6;
	// WebSocketSharp.Rsv WebSocketSharp.WebSocketFrame::_rsv1
	uint8_t ____rsv1_7;
	// WebSocketSharp.Rsv WebSocketSharp.WebSocketFrame::_rsv2
	uint8_t ____rsv2_8;
	// WebSocketSharp.Rsv WebSocketSharp.WebSocketFrame::_rsv3
	uint8_t ____rsv3_9;

public:
	inline static int32_t get_offset_of__extPayloadLength_0() { return static_cast<int32_t>(offsetof(WebSocketFrame_t764750278, ____extPayloadLength_0)); }
	inline ByteU5BU5D_t3397334013* get__extPayloadLength_0() const { return ____extPayloadLength_0; }
	inline ByteU5BU5D_t3397334013** get_address_of__extPayloadLength_0() { return &____extPayloadLength_0; }
	inline void set__extPayloadLength_0(ByteU5BU5D_t3397334013* value)
	{
		____extPayloadLength_0 = value;
		Il2CppCodeGenWriteBarrier((&____extPayloadLength_0), value);
	}

	inline static int32_t get_offset_of__fin_1() { return static_cast<int32_t>(offsetof(WebSocketFrame_t764750278, ____fin_1)); }
	inline uint8_t get__fin_1() const { return ____fin_1; }
	inline uint8_t* get_address_of__fin_1() { return &____fin_1; }
	inline void set__fin_1(uint8_t value)
	{
		____fin_1 = value;
	}

	inline static int32_t get_offset_of__mask_2() { return static_cast<int32_t>(offsetof(WebSocketFrame_t764750278, ____mask_2)); }
	inline uint8_t get__mask_2() const { return ____mask_2; }
	inline uint8_t* get_address_of__mask_2() { return &____mask_2; }
	inline void set__mask_2(uint8_t value)
	{
		____mask_2 = value;
	}

	inline static int32_t get_offset_of__maskingKey_3() { return static_cast<int32_t>(offsetof(WebSocketFrame_t764750278, ____maskingKey_3)); }
	inline ByteU5BU5D_t3397334013* get__maskingKey_3() const { return ____maskingKey_3; }
	inline ByteU5BU5D_t3397334013** get_address_of__maskingKey_3() { return &____maskingKey_3; }
	inline void set__maskingKey_3(ByteU5BU5D_t3397334013* value)
	{
		____maskingKey_3 = value;
		Il2CppCodeGenWriteBarrier((&____maskingKey_3), value);
	}

	inline static int32_t get_offset_of__opcode_4() { return static_cast<int32_t>(offsetof(WebSocketFrame_t764750278, ____opcode_4)); }
	inline uint8_t get__opcode_4() const { return ____opcode_4; }
	inline uint8_t* get_address_of__opcode_4() { return &____opcode_4; }
	inline void set__opcode_4(uint8_t value)
	{
		____opcode_4 = value;
	}

	inline static int32_t get_offset_of__payloadData_5() { return static_cast<int32_t>(offsetof(WebSocketFrame_t764750278, ____payloadData_5)); }
	inline PayloadData_t3839327312 * get__payloadData_5() const { return ____payloadData_5; }
	inline PayloadData_t3839327312 ** get_address_of__payloadData_5() { return &____payloadData_5; }
	inline void set__payloadData_5(PayloadData_t3839327312 * value)
	{
		____payloadData_5 = value;
		Il2CppCodeGenWriteBarrier((&____payloadData_5), value);
	}

	inline static int32_t get_offset_of__payloadLength_6() { return static_cast<int32_t>(offsetof(WebSocketFrame_t764750278, ____payloadLength_6)); }
	inline uint8_t get__payloadLength_6() const { return ____payloadLength_6; }
	inline uint8_t* get_address_of__payloadLength_6() { return &____payloadLength_6; }
	inline void set__payloadLength_6(uint8_t value)
	{
		____payloadLength_6 = value;
	}

	inline static int32_t get_offset_of__rsv1_7() { return static_cast<int32_t>(offsetof(WebSocketFrame_t764750278, ____rsv1_7)); }
	inline uint8_t get__rsv1_7() const { return ____rsv1_7; }
	inline uint8_t* get_address_of__rsv1_7() { return &____rsv1_7; }
	inline void set__rsv1_7(uint8_t value)
	{
		____rsv1_7 = value;
	}

	inline static int32_t get_offset_of__rsv2_8() { return static_cast<int32_t>(offsetof(WebSocketFrame_t764750278, ____rsv2_8)); }
	inline uint8_t get__rsv2_8() const { return ____rsv2_8; }
	inline uint8_t* get_address_of__rsv2_8() { return &____rsv2_8; }
	inline void set__rsv2_8(uint8_t value)
	{
		____rsv2_8 = value;
	}

	inline static int32_t get_offset_of__rsv3_9() { return static_cast<int32_t>(offsetof(WebSocketFrame_t764750278, ____rsv3_9)); }
	inline uint8_t get__rsv3_9() const { return ____rsv3_9; }
	inline uint8_t* get_address_of__rsv3_9() { return &____rsv3_9; }
	inline void set__rsv3_9(uint8_t value)
	{
		____rsv3_9 = value;
	}
};

struct WebSocketFrame_t764750278_StaticFields
{
public:
	// System.Byte[] WebSocketSharp.WebSocketFrame::EmptyPingBytes
	ByteU5BU5D_t3397334013* ___EmptyPingBytes_10;

public:
	inline static int32_t get_offset_of_EmptyPingBytes_10() { return static_cast<int32_t>(offsetof(WebSocketFrame_t764750278_StaticFields, ___EmptyPingBytes_10)); }
	inline ByteU5BU5D_t3397334013* get_EmptyPingBytes_10() const { return ___EmptyPingBytes_10; }
	inline ByteU5BU5D_t3397334013** get_address_of_EmptyPingBytes_10() { return &___EmptyPingBytes_10; }
	inline void set_EmptyPingBytes_10(ByteU5BU5D_t3397334013* value)
	{
		___EmptyPingBytes_10 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyPingBytes_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBSOCKETFRAME_T764750278_H
#ifndef U3CU3EC__ITERATOR1_T949754915_H
#define U3CU3EC__ITERATOR1_T949754915_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Server.WebSocketSessionManager/<>c__Iterator1
struct  U3CU3Ec__Iterator1_t949754915  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean> WebSocketSharp.Server.WebSocketSessionManager/<>c__Iterator1::$locvar0
	Enumerator_t2765411386  ___U24locvar0_0;
	// System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean> WebSocketSharp.Server.WebSocketSessionManager/<>c__Iterator1::<res>__1
	KeyValuePair_2_t3497699202  ___U3CresU3E__1_1;
	// WebSocketSharp.Server.WebSocketSessionManager WebSocketSharp.Server.WebSocketSessionManager/<>c__Iterator1::$this
	WebSocketSessionManager_t2802512518 * ___U24this_2;
	// System.String WebSocketSharp.Server.WebSocketSessionManager/<>c__Iterator1::$current
	String_t* ___U24current_3;
	// System.Boolean WebSocketSharp.Server.WebSocketSessionManager/<>c__Iterator1::$disposing
	bool ___U24disposing_4;
	// System.Int32 WebSocketSharp.Server.WebSocketSessionManager/<>c__Iterator1::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U24locvar0_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator1_t949754915, ___U24locvar0_0)); }
	inline Enumerator_t2765411386  get_U24locvar0_0() const { return ___U24locvar0_0; }
	inline Enumerator_t2765411386 * get_address_of_U24locvar0_0() { return &___U24locvar0_0; }
	inline void set_U24locvar0_0(Enumerator_t2765411386  value)
	{
		___U24locvar0_0 = value;
	}

	inline static int32_t get_offset_of_U3CresU3E__1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator1_t949754915, ___U3CresU3E__1_1)); }
	inline KeyValuePair_2_t3497699202  get_U3CresU3E__1_1() const { return ___U3CresU3E__1_1; }
	inline KeyValuePair_2_t3497699202 * get_address_of_U3CresU3E__1_1() { return &___U3CresU3E__1_1; }
	inline void set_U3CresU3E__1_1(KeyValuePair_2_t3497699202  value)
	{
		___U3CresU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator1_t949754915, ___U24this_2)); }
	inline WebSocketSessionManager_t2802512518 * get_U24this_2() const { return ___U24this_2; }
	inline WebSocketSessionManager_t2802512518 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(WebSocketSessionManager_t2802512518 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator1_t949754915, ___U24current_3)); }
	inline String_t* get_U24current_3() const { return ___U24current_3; }
	inline String_t** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(String_t* value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator1_t949754915, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator1_t949754915, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__ITERATOR1_T949754915_H
#ifndef U3CU3EC__ITERATOR0_T2515838856_H
#define U3CU3EC__ITERATOR0_T2515838856_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Server.WebSocketSessionManager/<>c__Iterator0
struct  U3CU3Ec__Iterator0_t2515838856  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean> WebSocketSharp.Server.WebSocketSessionManager/<>c__Iterator0::$locvar0
	Enumerator_t2765411386  ___U24locvar0_0;
	// System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean> WebSocketSharp.Server.WebSocketSessionManager/<>c__Iterator0::<res>__1
	KeyValuePair_2_t3497699202  ___U3CresU3E__1_1;
	// WebSocketSharp.Server.WebSocketSessionManager WebSocketSharp.Server.WebSocketSessionManager/<>c__Iterator0::$this
	WebSocketSessionManager_t2802512518 * ___U24this_2;
	// System.String WebSocketSharp.Server.WebSocketSessionManager/<>c__Iterator0::$current
	String_t* ___U24current_3;
	// System.Boolean WebSocketSharp.Server.WebSocketSessionManager/<>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 WebSocketSharp.Server.WebSocketSessionManager/<>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U24locvar0_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t2515838856, ___U24locvar0_0)); }
	inline Enumerator_t2765411386  get_U24locvar0_0() const { return ___U24locvar0_0; }
	inline Enumerator_t2765411386 * get_address_of_U24locvar0_0() { return &___U24locvar0_0; }
	inline void set_U24locvar0_0(Enumerator_t2765411386  value)
	{
		___U24locvar0_0 = value;
	}

	inline static int32_t get_offset_of_U3CresU3E__1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t2515838856, ___U3CresU3E__1_1)); }
	inline KeyValuePair_2_t3497699202  get_U3CresU3E__1_1() const { return ___U3CresU3E__1_1; }
	inline KeyValuePair_2_t3497699202 * get_address_of_U3CresU3E__1_1() { return &___U3CresU3E__1_1; }
	inline void set_U3CresU3E__1_1(KeyValuePair_2_t3497699202  value)
	{
		___U3CresU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t2515838856, ___U24this_2)); }
	inline WebSocketSessionManager_t2802512518 * get_U24this_2() const { return ___U24this_2; }
	inline WebSocketSessionManager_t2802512518 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(WebSocketSessionManager_t2802512518 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t2515838856, ___U24current_3)); }
	inline String_t* get_U24current_3() const { return ___U24current_3; }
	inline String_t** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(String_t* value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t2515838856, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t2515838856, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__ITERATOR0_T2515838856_H
#ifndef WEBSOCKETSESSIONMANAGER_T2802512518_H
#define WEBSOCKETSESSIONMANAGER_T2802512518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Server.WebSocketSessionManager
struct  WebSocketSessionManager_t2802512518  : public RuntimeObject
{
public:
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) WebSocketSharp.Server.WebSocketSessionManager::_clean
	bool ____clean_0;
	// System.Object WebSocketSharp.Server.WebSocketSessionManager::_forSweep
	RuntimeObject * ____forSweep_1;
	// WebSocketSharp.Logger WebSocketSharp.Server.WebSocketSessionManager::_logger
	Logger_t2598199114 * ____logger_2;
	// System.Collections.Generic.Dictionary`2<System.String,WebSocketSharp.Server.IWebSocketSession> WebSocketSharp.Server.WebSocketSessionManager::_sessions
	Dictionary_2_t3346836010 * ____sessions_3;
	// WebSocketSharp.Server.ServerState modreq(System.Runtime.CompilerServices.IsVolatile) WebSocketSharp.Server.WebSocketSessionManager::_state
	int32_t ____state_4;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) WebSocketSharp.Server.WebSocketSessionManager::_sweeping
	bool ____sweeping_5;
	// System.Timers.Timer WebSocketSharp.Server.WebSocketSessionManager::_sweepTimer
	Timer_t4294691011 * ____sweepTimer_6;
	// System.Object WebSocketSharp.Server.WebSocketSessionManager::_sync
	RuntimeObject * ____sync_7;
	// System.TimeSpan WebSocketSharp.Server.WebSocketSessionManager::_waitTime
	TimeSpan_t3430258949  ____waitTime_8;

public:
	inline static int32_t get_offset_of__clean_0() { return static_cast<int32_t>(offsetof(WebSocketSessionManager_t2802512518, ____clean_0)); }
	inline bool get__clean_0() const { return ____clean_0; }
	inline bool* get_address_of__clean_0() { return &____clean_0; }
	inline void set__clean_0(bool value)
	{
		____clean_0 = value;
	}

	inline static int32_t get_offset_of__forSweep_1() { return static_cast<int32_t>(offsetof(WebSocketSessionManager_t2802512518, ____forSweep_1)); }
	inline RuntimeObject * get__forSweep_1() const { return ____forSweep_1; }
	inline RuntimeObject ** get_address_of__forSweep_1() { return &____forSweep_1; }
	inline void set__forSweep_1(RuntimeObject * value)
	{
		____forSweep_1 = value;
		Il2CppCodeGenWriteBarrier((&____forSweep_1), value);
	}

	inline static int32_t get_offset_of__logger_2() { return static_cast<int32_t>(offsetof(WebSocketSessionManager_t2802512518, ____logger_2)); }
	inline Logger_t2598199114 * get__logger_2() const { return ____logger_2; }
	inline Logger_t2598199114 ** get_address_of__logger_2() { return &____logger_2; }
	inline void set__logger_2(Logger_t2598199114 * value)
	{
		____logger_2 = value;
		Il2CppCodeGenWriteBarrier((&____logger_2), value);
	}

	inline static int32_t get_offset_of__sessions_3() { return static_cast<int32_t>(offsetof(WebSocketSessionManager_t2802512518, ____sessions_3)); }
	inline Dictionary_2_t3346836010 * get__sessions_3() const { return ____sessions_3; }
	inline Dictionary_2_t3346836010 ** get_address_of__sessions_3() { return &____sessions_3; }
	inline void set__sessions_3(Dictionary_2_t3346836010 * value)
	{
		____sessions_3 = value;
		Il2CppCodeGenWriteBarrier((&____sessions_3), value);
	}

	inline static int32_t get_offset_of__state_4() { return static_cast<int32_t>(offsetof(WebSocketSessionManager_t2802512518, ____state_4)); }
	inline int32_t get__state_4() const { return ____state_4; }
	inline int32_t* get_address_of__state_4() { return &____state_4; }
	inline void set__state_4(int32_t value)
	{
		____state_4 = value;
	}

	inline static int32_t get_offset_of__sweeping_5() { return static_cast<int32_t>(offsetof(WebSocketSessionManager_t2802512518, ____sweeping_5)); }
	inline bool get__sweeping_5() const { return ____sweeping_5; }
	inline bool* get_address_of__sweeping_5() { return &____sweeping_5; }
	inline void set__sweeping_5(bool value)
	{
		____sweeping_5 = value;
	}

	inline static int32_t get_offset_of__sweepTimer_6() { return static_cast<int32_t>(offsetof(WebSocketSessionManager_t2802512518, ____sweepTimer_6)); }
	inline Timer_t4294691011 * get__sweepTimer_6() const { return ____sweepTimer_6; }
	inline Timer_t4294691011 ** get_address_of__sweepTimer_6() { return &____sweepTimer_6; }
	inline void set__sweepTimer_6(Timer_t4294691011 * value)
	{
		____sweepTimer_6 = value;
		Il2CppCodeGenWriteBarrier((&____sweepTimer_6), value);
	}

	inline static int32_t get_offset_of__sync_7() { return static_cast<int32_t>(offsetof(WebSocketSessionManager_t2802512518, ____sync_7)); }
	inline RuntimeObject * get__sync_7() const { return ____sync_7; }
	inline RuntimeObject ** get_address_of__sync_7() { return &____sync_7; }
	inline void set__sync_7(RuntimeObject * value)
	{
		____sync_7 = value;
		Il2CppCodeGenWriteBarrier((&____sync_7), value);
	}

	inline static int32_t get_offset_of__waitTime_8() { return static_cast<int32_t>(offsetof(WebSocketSessionManager_t2802512518, ____waitTime_8)); }
	inline TimeSpan_t3430258949  get__waitTime_8() const { return ____waitTime_8; }
	inline TimeSpan_t3430258949 * get_address_of__waitTime_8() { return &____waitTime_8; }
	inline void set__waitTime_8(TimeSpan_t3430258949  value)
	{
		____waitTime_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBSOCKETSESSIONMANAGER_T2802512518_H
#ifndef WEBSOCKET_T3268376029_H
#define WEBSOCKET_T3268376029_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.WebSocket
struct  WebSocket_t3268376029  : public RuntimeObject
{
public:
	// WebSocketSharp.Net.AuthenticationChallenge WebSocketSharp.WebSocket::_authChallenge
	AuthenticationChallenge_t1146723439 * ____authChallenge_0;
	// System.String WebSocketSharp.WebSocket::_base64Key
	String_t* ____base64Key_1;
	// System.Boolean WebSocketSharp.WebSocket::_client
	bool ____client_2;
	// System.Action WebSocketSharp.WebSocket::_closeContext
	Action_t3226471752 * ____closeContext_3;
	// WebSocketSharp.CompressionMethod WebSocketSharp.WebSocket::_compression
	uint8_t ____compression_4;
	// WebSocketSharp.Net.WebSockets.WebSocketContext WebSocketSharp.WebSocket::_context
	WebSocketContext_t3488732344 * ____context_5;
	// WebSocketSharp.Net.CookieCollection WebSocketSharp.WebSocket::_cookies
	CookieCollection_t4248997468 * ____cookies_6;
	// WebSocketSharp.Net.NetworkCredential WebSocketSharp.WebSocket::_credentials
	NetworkCredential_t3911206805 * ____credentials_7;
	// System.Boolean WebSocketSharp.WebSocket::_emitOnPing
	bool ____emitOnPing_8;
	// System.Boolean WebSocketSharp.WebSocket::_enableRedirection
	bool ____enableRedirection_9;
	// System.Threading.AutoResetEvent WebSocketSharp.WebSocket::_exitReceiving
	AutoResetEvent_t15112628 * ____exitReceiving_10;
	// System.String WebSocketSharp.WebSocket::_extensions
	String_t* ____extensions_11;
	// System.Boolean WebSocketSharp.WebSocket::_extensionsRequested
	bool ____extensionsRequested_12;
	// System.Object WebSocketSharp.WebSocket::_forConn
	RuntimeObject * ____forConn_13;
	// System.Object WebSocketSharp.WebSocket::_forMessageEventQueue
	RuntimeObject * ____forMessageEventQueue_14;
	// System.Object WebSocketSharp.WebSocket::_forSend
	RuntimeObject * ____forSend_15;
	// System.IO.MemoryStream WebSocketSharp.WebSocket::_fragmentsBuffer
	MemoryStream_t743994179 * ____fragmentsBuffer_16;
	// System.Boolean WebSocketSharp.WebSocket::_fragmentsCompressed
	bool ____fragmentsCompressed_17;
	// WebSocketSharp.Opcode WebSocketSharp.WebSocket::_fragmentsOpcode
	uint8_t ____fragmentsOpcode_18;
	// System.Func`2<WebSocketSharp.Net.WebSockets.WebSocketContext,System.String> WebSocketSharp.WebSocket::_handshakeRequestChecker
	Func_2_t1909060290 * ____handshakeRequestChecker_20;
	// System.Boolean WebSocketSharp.WebSocket::_ignoreExtensions
	bool ____ignoreExtensions_21;
	// System.Boolean WebSocketSharp.WebSocket::_inContinuation
	bool ____inContinuation_22;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) WebSocketSharp.WebSocket::_inMessage
	bool ____inMessage_23;
	// WebSocketSharp.Logger modreq(System.Runtime.CompilerServices.IsVolatile) WebSocketSharp.WebSocket::_logger
	Logger_t2598199114 * ____logger_24;
	// System.Action`1<WebSocketSharp.MessageEventArgs> WebSocketSharp.WebSocket::_message
	Action_1_t2691851108 * ____message_25;
	// System.Collections.Generic.Queue`1<WebSocketSharp.MessageEventArgs> WebSocketSharp.WebSocket::_messageEventQueue
	Queue_1_t2709708561 * ____messageEventQueue_26;
	// System.UInt32 WebSocketSharp.WebSocket::_nonceCount
	uint32_t ____nonceCount_27;
	// System.String WebSocketSharp.WebSocket::_origin
	String_t* ____origin_28;
	// System.Boolean WebSocketSharp.WebSocket::_preAuth
	bool ____preAuth_29;
	// System.String WebSocketSharp.WebSocket::_protocol
	String_t* ____protocol_30;
	// System.String[] WebSocketSharp.WebSocket::_protocols
	StringU5BU5D_t1642385972* ____protocols_31;
	// System.Boolean WebSocketSharp.WebSocket::_protocolsRequested
	bool ____protocolsRequested_32;
	// WebSocketSharp.Net.NetworkCredential WebSocketSharp.WebSocket::_proxyCredentials
	NetworkCredential_t3911206805 * ____proxyCredentials_33;
	// System.Uri WebSocketSharp.WebSocket::_proxyUri
	Uri_t19570940 * ____proxyUri_34;
	// WebSocketSharp.WebSocketState modreq(System.Runtime.CompilerServices.IsVolatile) WebSocketSharp.WebSocket::_readyState
	uint16_t ____readyState_35;
	// System.Threading.AutoResetEvent WebSocketSharp.WebSocket::_receivePong
	AutoResetEvent_t15112628 * ____receivePong_36;
	// System.Boolean WebSocketSharp.WebSocket::_secure
	bool ____secure_37;
	// WebSocketSharp.Net.ClientSslConfiguration WebSocketSharp.WebSocket::_sslConfig
	ClientSslConfiguration_t1159130081 * ____sslConfig_38;
	// System.IO.Stream WebSocketSharp.WebSocket::_stream
	Stream_t3255436806 * ____stream_39;
	// System.Net.Sockets.TcpClient WebSocketSharp.WebSocket::_tcpClient
	TcpClient_t408947970 * ____tcpClient_40;
	// System.Uri WebSocketSharp.WebSocket::_uri
	Uri_t19570940 * ____uri_41;
	// System.TimeSpan WebSocketSharp.WebSocket::_waitTime
	TimeSpan_t3430258949  ____waitTime_43;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> WebSocketSharp.WebSocket::<Headers>k__BackingField
	Dictionary_2_t3943999495 * ___U3CHeadersU3Ek__BackingField_47;
	// System.EventHandler`1<WebSocketSharp.CloseEventArgs> WebSocketSharp.WebSocket::OnClose
	EventHandler_1_t3230782241 * ___OnClose_48;
	// System.EventHandler`1<WebSocketSharp.ErrorEventArgs> WebSocketSharp.WebSocket::OnError
	EventHandler_1_t3388497467 * ___OnError_49;
	// System.EventHandler`1<WebSocketSharp.MessageEventArgs> WebSocketSharp.WebSocket::OnMessage
	EventHandler_1_t1481358898 * ___OnMessage_50;
	// System.EventHandler WebSocketSharp.WebSocket::OnOpen
	EventHandler_t277755526 * ___OnOpen_51;

public:
	inline static int32_t get_offset_of__authChallenge_0() { return static_cast<int32_t>(offsetof(WebSocket_t3268376029, ____authChallenge_0)); }
	inline AuthenticationChallenge_t1146723439 * get__authChallenge_0() const { return ____authChallenge_0; }
	inline AuthenticationChallenge_t1146723439 ** get_address_of__authChallenge_0() { return &____authChallenge_0; }
	inline void set__authChallenge_0(AuthenticationChallenge_t1146723439 * value)
	{
		____authChallenge_0 = value;
		Il2CppCodeGenWriteBarrier((&____authChallenge_0), value);
	}

	inline static int32_t get_offset_of__base64Key_1() { return static_cast<int32_t>(offsetof(WebSocket_t3268376029, ____base64Key_1)); }
	inline String_t* get__base64Key_1() const { return ____base64Key_1; }
	inline String_t** get_address_of__base64Key_1() { return &____base64Key_1; }
	inline void set__base64Key_1(String_t* value)
	{
		____base64Key_1 = value;
		Il2CppCodeGenWriteBarrier((&____base64Key_1), value);
	}

	inline static int32_t get_offset_of__client_2() { return static_cast<int32_t>(offsetof(WebSocket_t3268376029, ____client_2)); }
	inline bool get__client_2() const { return ____client_2; }
	inline bool* get_address_of__client_2() { return &____client_2; }
	inline void set__client_2(bool value)
	{
		____client_2 = value;
	}

	inline static int32_t get_offset_of__closeContext_3() { return static_cast<int32_t>(offsetof(WebSocket_t3268376029, ____closeContext_3)); }
	inline Action_t3226471752 * get__closeContext_3() const { return ____closeContext_3; }
	inline Action_t3226471752 ** get_address_of__closeContext_3() { return &____closeContext_3; }
	inline void set__closeContext_3(Action_t3226471752 * value)
	{
		____closeContext_3 = value;
		Il2CppCodeGenWriteBarrier((&____closeContext_3), value);
	}

	inline static int32_t get_offset_of__compression_4() { return static_cast<int32_t>(offsetof(WebSocket_t3268376029, ____compression_4)); }
	inline uint8_t get__compression_4() const { return ____compression_4; }
	inline uint8_t* get_address_of__compression_4() { return &____compression_4; }
	inline void set__compression_4(uint8_t value)
	{
		____compression_4 = value;
	}

	inline static int32_t get_offset_of__context_5() { return static_cast<int32_t>(offsetof(WebSocket_t3268376029, ____context_5)); }
	inline WebSocketContext_t3488732344 * get__context_5() const { return ____context_5; }
	inline WebSocketContext_t3488732344 ** get_address_of__context_5() { return &____context_5; }
	inline void set__context_5(WebSocketContext_t3488732344 * value)
	{
		____context_5 = value;
		Il2CppCodeGenWriteBarrier((&____context_5), value);
	}

	inline static int32_t get_offset_of__cookies_6() { return static_cast<int32_t>(offsetof(WebSocket_t3268376029, ____cookies_6)); }
	inline CookieCollection_t4248997468 * get__cookies_6() const { return ____cookies_6; }
	inline CookieCollection_t4248997468 ** get_address_of__cookies_6() { return &____cookies_6; }
	inline void set__cookies_6(CookieCollection_t4248997468 * value)
	{
		____cookies_6 = value;
		Il2CppCodeGenWriteBarrier((&____cookies_6), value);
	}

	inline static int32_t get_offset_of__credentials_7() { return static_cast<int32_t>(offsetof(WebSocket_t3268376029, ____credentials_7)); }
	inline NetworkCredential_t3911206805 * get__credentials_7() const { return ____credentials_7; }
	inline NetworkCredential_t3911206805 ** get_address_of__credentials_7() { return &____credentials_7; }
	inline void set__credentials_7(NetworkCredential_t3911206805 * value)
	{
		____credentials_7 = value;
		Il2CppCodeGenWriteBarrier((&____credentials_7), value);
	}

	inline static int32_t get_offset_of__emitOnPing_8() { return static_cast<int32_t>(offsetof(WebSocket_t3268376029, ____emitOnPing_8)); }
	inline bool get__emitOnPing_8() const { return ____emitOnPing_8; }
	inline bool* get_address_of__emitOnPing_8() { return &____emitOnPing_8; }
	inline void set__emitOnPing_8(bool value)
	{
		____emitOnPing_8 = value;
	}

	inline static int32_t get_offset_of__enableRedirection_9() { return static_cast<int32_t>(offsetof(WebSocket_t3268376029, ____enableRedirection_9)); }
	inline bool get__enableRedirection_9() const { return ____enableRedirection_9; }
	inline bool* get_address_of__enableRedirection_9() { return &____enableRedirection_9; }
	inline void set__enableRedirection_9(bool value)
	{
		____enableRedirection_9 = value;
	}

	inline static int32_t get_offset_of__exitReceiving_10() { return static_cast<int32_t>(offsetof(WebSocket_t3268376029, ____exitReceiving_10)); }
	inline AutoResetEvent_t15112628 * get__exitReceiving_10() const { return ____exitReceiving_10; }
	inline AutoResetEvent_t15112628 ** get_address_of__exitReceiving_10() { return &____exitReceiving_10; }
	inline void set__exitReceiving_10(AutoResetEvent_t15112628 * value)
	{
		____exitReceiving_10 = value;
		Il2CppCodeGenWriteBarrier((&____exitReceiving_10), value);
	}

	inline static int32_t get_offset_of__extensions_11() { return static_cast<int32_t>(offsetof(WebSocket_t3268376029, ____extensions_11)); }
	inline String_t* get__extensions_11() const { return ____extensions_11; }
	inline String_t** get_address_of__extensions_11() { return &____extensions_11; }
	inline void set__extensions_11(String_t* value)
	{
		____extensions_11 = value;
		Il2CppCodeGenWriteBarrier((&____extensions_11), value);
	}

	inline static int32_t get_offset_of__extensionsRequested_12() { return static_cast<int32_t>(offsetof(WebSocket_t3268376029, ____extensionsRequested_12)); }
	inline bool get__extensionsRequested_12() const { return ____extensionsRequested_12; }
	inline bool* get_address_of__extensionsRequested_12() { return &____extensionsRequested_12; }
	inline void set__extensionsRequested_12(bool value)
	{
		____extensionsRequested_12 = value;
	}

	inline static int32_t get_offset_of__forConn_13() { return static_cast<int32_t>(offsetof(WebSocket_t3268376029, ____forConn_13)); }
	inline RuntimeObject * get__forConn_13() const { return ____forConn_13; }
	inline RuntimeObject ** get_address_of__forConn_13() { return &____forConn_13; }
	inline void set__forConn_13(RuntimeObject * value)
	{
		____forConn_13 = value;
		Il2CppCodeGenWriteBarrier((&____forConn_13), value);
	}

	inline static int32_t get_offset_of__forMessageEventQueue_14() { return static_cast<int32_t>(offsetof(WebSocket_t3268376029, ____forMessageEventQueue_14)); }
	inline RuntimeObject * get__forMessageEventQueue_14() const { return ____forMessageEventQueue_14; }
	inline RuntimeObject ** get_address_of__forMessageEventQueue_14() { return &____forMessageEventQueue_14; }
	inline void set__forMessageEventQueue_14(RuntimeObject * value)
	{
		____forMessageEventQueue_14 = value;
		Il2CppCodeGenWriteBarrier((&____forMessageEventQueue_14), value);
	}

	inline static int32_t get_offset_of__forSend_15() { return static_cast<int32_t>(offsetof(WebSocket_t3268376029, ____forSend_15)); }
	inline RuntimeObject * get__forSend_15() const { return ____forSend_15; }
	inline RuntimeObject ** get_address_of__forSend_15() { return &____forSend_15; }
	inline void set__forSend_15(RuntimeObject * value)
	{
		____forSend_15 = value;
		Il2CppCodeGenWriteBarrier((&____forSend_15), value);
	}

	inline static int32_t get_offset_of__fragmentsBuffer_16() { return static_cast<int32_t>(offsetof(WebSocket_t3268376029, ____fragmentsBuffer_16)); }
	inline MemoryStream_t743994179 * get__fragmentsBuffer_16() const { return ____fragmentsBuffer_16; }
	inline MemoryStream_t743994179 ** get_address_of__fragmentsBuffer_16() { return &____fragmentsBuffer_16; }
	inline void set__fragmentsBuffer_16(MemoryStream_t743994179 * value)
	{
		____fragmentsBuffer_16 = value;
		Il2CppCodeGenWriteBarrier((&____fragmentsBuffer_16), value);
	}

	inline static int32_t get_offset_of__fragmentsCompressed_17() { return static_cast<int32_t>(offsetof(WebSocket_t3268376029, ____fragmentsCompressed_17)); }
	inline bool get__fragmentsCompressed_17() const { return ____fragmentsCompressed_17; }
	inline bool* get_address_of__fragmentsCompressed_17() { return &____fragmentsCompressed_17; }
	inline void set__fragmentsCompressed_17(bool value)
	{
		____fragmentsCompressed_17 = value;
	}

	inline static int32_t get_offset_of__fragmentsOpcode_18() { return static_cast<int32_t>(offsetof(WebSocket_t3268376029, ____fragmentsOpcode_18)); }
	inline uint8_t get__fragmentsOpcode_18() const { return ____fragmentsOpcode_18; }
	inline uint8_t* get_address_of__fragmentsOpcode_18() { return &____fragmentsOpcode_18; }
	inline void set__fragmentsOpcode_18(uint8_t value)
	{
		____fragmentsOpcode_18 = value;
	}

	inline static int32_t get_offset_of__handshakeRequestChecker_20() { return static_cast<int32_t>(offsetof(WebSocket_t3268376029, ____handshakeRequestChecker_20)); }
	inline Func_2_t1909060290 * get__handshakeRequestChecker_20() const { return ____handshakeRequestChecker_20; }
	inline Func_2_t1909060290 ** get_address_of__handshakeRequestChecker_20() { return &____handshakeRequestChecker_20; }
	inline void set__handshakeRequestChecker_20(Func_2_t1909060290 * value)
	{
		____handshakeRequestChecker_20 = value;
		Il2CppCodeGenWriteBarrier((&____handshakeRequestChecker_20), value);
	}

	inline static int32_t get_offset_of__ignoreExtensions_21() { return static_cast<int32_t>(offsetof(WebSocket_t3268376029, ____ignoreExtensions_21)); }
	inline bool get__ignoreExtensions_21() const { return ____ignoreExtensions_21; }
	inline bool* get_address_of__ignoreExtensions_21() { return &____ignoreExtensions_21; }
	inline void set__ignoreExtensions_21(bool value)
	{
		____ignoreExtensions_21 = value;
	}

	inline static int32_t get_offset_of__inContinuation_22() { return static_cast<int32_t>(offsetof(WebSocket_t3268376029, ____inContinuation_22)); }
	inline bool get__inContinuation_22() const { return ____inContinuation_22; }
	inline bool* get_address_of__inContinuation_22() { return &____inContinuation_22; }
	inline void set__inContinuation_22(bool value)
	{
		____inContinuation_22 = value;
	}

	inline static int32_t get_offset_of__inMessage_23() { return static_cast<int32_t>(offsetof(WebSocket_t3268376029, ____inMessage_23)); }
	inline bool get__inMessage_23() const { return ____inMessage_23; }
	inline bool* get_address_of__inMessage_23() { return &____inMessage_23; }
	inline void set__inMessage_23(bool value)
	{
		____inMessage_23 = value;
	}

	inline static int32_t get_offset_of__logger_24() { return static_cast<int32_t>(offsetof(WebSocket_t3268376029, ____logger_24)); }
	inline Logger_t2598199114 * get__logger_24() const { return ____logger_24; }
	inline Logger_t2598199114 ** get_address_of__logger_24() { return &____logger_24; }
	inline void set__logger_24(Logger_t2598199114 * value)
	{
		____logger_24 = value;
		Il2CppCodeGenWriteBarrier((&____logger_24), value);
	}

	inline static int32_t get_offset_of__message_25() { return static_cast<int32_t>(offsetof(WebSocket_t3268376029, ____message_25)); }
	inline Action_1_t2691851108 * get__message_25() const { return ____message_25; }
	inline Action_1_t2691851108 ** get_address_of__message_25() { return &____message_25; }
	inline void set__message_25(Action_1_t2691851108 * value)
	{
		____message_25 = value;
		Il2CppCodeGenWriteBarrier((&____message_25), value);
	}

	inline static int32_t get_offset_of__messageEventQueue_26() { return static_cast<int32_t>(offsetof(WebSocket_t3268376029, ____messageEventQueue_26)); }
	inline Queue_1_t2709708561 * get__messageEventQueue_26() const { return ____messageEventQueue_26; }
	inline Queue_1_t2709708561 ** get_address_of__messageEventQueue_26() { return &____messageEventQueue_26; }
	inline void set__messageEventQueue_26(Queue_1_t2709708561 * value)
	{
		____messageEventQueue_26 = value;
		Il2CppCodeGenWriteBarrier((&____messageEventQueue_26), value);
	}

	inline static int32_t get_offset_of__nonceCount_27() { return static_cast<int32_t>(offsetof(WebSocket_t3268376029, ____nonceCount_27)); }
	inline uint32_t get__nonceCount_27() const { return ____nonceCount_27; }
	inline uint32_t* get_address_of__nonceCount_27() { return &____nonceCount_27; }
	inline void set__nonceCount_27(uint32_t value)
	{
		____nonceCount_27 = value;
	}

	inline static int32_t get_offset_of__origin_28() { return static_cast<int32_t>(offsetof(WebSocket_t3268376029, ____origin_28)); }
	inline String_t* get__origin_28() const { return ____origin_28; }
	inline String_t** get_address_of__origin_28() { return &____origin_28; }
	inline void set__origin_28(String_t* value)
	{
		____origin_28 = value;
		Il2CppCodeGenWriteBarrier((&____origin_28), value);
	}

	inline static int32_t get_offset_of__preAuth_29() { return static_cast<int32_t>(offsetof(WebSocket_t3268376029, ____preAuth_29)); }
	inline bool get__preAuth_29() const { return ____preAuth_29; }
	inline bool* get_address_of__preAuth_29() { return &____preAuth_29; }
	inline void set__preAuth_29(bool value)
	{
		____preAuth_29 = value;
	}

	inline static int32_t get_offset_of__protocol_30() { return static_cast<int32_t>(offsetof(WebSocket_t3268376029, ____protocol_30)); }
	inline String_t* get__protocol_30() const { return ____protocol_30; }
	inline String_t** get_address_of__protocol_30() { return &____protocol_30; }
	inline void set__protocol_30(String_t* value)
	{
		____protocol_30 = value;
		Il2CppCodeGenWriteBarrier((&____protocol_30), value);
	}

	inline static int32_t get_offset_of__protocols_31() { return static_cast<int32_t>(offsetof(WebSocket_t3268376029, ____protocols_31)); }
	inline StringU5BU5D_t1642385972* get__protocols_31() const { return ____protocols_31; }
	inline StringU5BU5D_t1642385972** get_address_of__protocols_31() { return &____protocols_31; }
	inline void set__protocols_31(StringU5BU5D_t1642385972* value)
	{
		____protocols_31 = value;
		Il2CppCodeGenWriteBarrier((&____protocols_31), value);
	}

	inline static int32_t get_offset_of__protocolsRequested_32() { return static_cast<int32_t>(offsetof(WebSocket_t3268376029, ____protocolsRequested_32)); }
	inline bool get__protocolsRequested_32() const { return ____protocolsRequested_32; }
	inline bool* get_address_of__protocolsRequested_32() { return &____protocolsRequested_32; }
	inline void set__protocolsRequested_32(bool value)
	{
		____protocolsRequested_32 = value;
	}

	inline static int32_t get_offset_of__proxyCredentials_33() { return static_cast<int32_t>(offsetof(WebSocket_t3268376029, ____proxyCredentials_33)); }
	inline NetworkCredential_t3911206805 * get__proxyCredentials_33() const { return ____proxyCredentials_33; }
	inline NetworkCredential_t3911206805 ** get_address_of__proxyCredentials_33() { return &____proxyCredentials_33; }
	inline void set__proxyCredentials_33(NetworkCredential_t3911206805 * value)
	{
		____proxyCredentials_33 = value;
		Il2CppCodeGenWriteBarrier((&____proxyCredentials_33), value);
	}

	inline static int32_t get_offset_of__proxyUri_34() { return static_cast<int32_t>(offsetof(WebSocket_t3268376029, ____proxyUri_34)); }
	inline Uri_t19570940 * get__proxyUri_34() const { return ____proxyUri_34; }
	inline Uri_t19570940 ** get_address_of__proxyUri_34() { return &____proxyUri_34; }
	inline void set__proxyUri_34(Uri_t19570940 * value)
	{
		____proxyUri_34 = value;
		Il2CppCodeGenWriteBarrier((&____proxyUri_34), value);
	}

	inline static int32_t get_offset_of__readyState_35() { return static_cast<int32_t>(offsetof(WebSocket_t3268376029, ____readyState_35)); }
	inline uint16_t get__readyState_35() const { return ____readyState_35; }
	inline uint16_t* get_address_of__readyState_35() { return &____readyState_35; }
	inline void set__readyState_35(uint16_t value)
	{
		____readyState_35 = value;
	}

	inline static int32_t get_offset_of__receivePong_36() { return static_cast<int32_t>(offsetof(WebSocket_t3268376029, ____receivePong_36)); }
	inline AutoResetEvent_t15112628 * get__receivePong_36() const { return ____receivePong_36; }
	inline AutoResetEvent_t15112628 ** get_address_of__receivePong_36() { return &____receivePong_36; }
	inline void set__receivePong_36(AutoResetEvent_t15112628 * value)
	{
		____receivePong_36 = value;
		Il2CppCodeGenWriteBarrier((&____receivePong_36), value);
	}

	inline static int32_t get_offset_of__secure_37() { return static_cast<int32_t>(offsetof(WebSocket_t3268376029, ____secure_37)); }
	inline bool get__secure_37() const { return ____secure_37; }
	inline bool* get_address_of__secure_37() { return &____secure_37; }
	inline void set__secure_37(bool value)
	{
		____secure_37 = value;
	}

	inline static int32_t get_offset_of__sslConfig_38() { return static_cast<int32_t>(offsetof(WebSocket_t3268376029, ____sslConfig_38)); }
	inline ClientSslConfiguration_t1159130081 * get__sslConfig_38() const { return ____sslConfig_38; }
	inline ClientSslConfiguration_t1159130081 ** get_address_of__sslConfig_38() { return &____sslConfig_38; }
	inline void set__sslConfig_38(ClientSslConfiguration_t1159130081 * value)
	{
		____sslConfig_38 = value;
		Il2CppCodeGenWriteBarrier((&____sslConfig_38), value);
	}

	inline static int32_t get_offset_of__stream_39() { return static_cast<int32_t>(offsetof(WebSocket_t3268376029, ____stream_39)); }
	inline Stream_t3255436806 * get__stream_39() const { return ____stream_39; }
	inline Stream_t3255436806 ** get_address_of__stream_39() { return &____stream_39; }
	inline void set__stream_39(Stream_t3255436806 * value)
	{
		____stream_39 = value;
		Il2CppCodeGenWriteBarrier((&____stream_39), value);
	}

	inline static int32_t get_offset_of__tcpClient_40() { return static_cast<int32_t>(offsetof(WebSocket_t3268376029, ____tcpClient_40)); }
	inline TcpClient_t408947970 * get__tcpClient_40() const { return ____tcpClient_40; }
	inline TcpClient_t408947970 ** get_address_of__tcpClient_40() { return &____tcpClient_40; }
	inline void set__tcpClient_40(TcpClient_t408947970 * value)
	{
		____tcpClient_40 = value;
		Il2CppCodeGenWriteBarrier((&____tcpClient_40), value);
	}

	inline static int32_t get_offset_of__uri_41() { return static_cast<int32_t>(offsetof(WebSocket_t3268376029, ____uri_41)); }
	inline Uri_t19570940 * get__uri_41() const { return ____uri_41; }
	inline Uri_t19570940 ** get_address_of__uri_41() { return &____uri_41; }
	inline void set__uri_41(Uri_t19570940 * value)
	{
		____uri_41 = value;
		Il2CppCodeGenWriteBarrier((&____uri_41), value);
	}

	inline static int32_t get_offset_of__waitTime_43() { return static_cast<int32_t>(offsetof(WebSocket_t3268376029, ____waitTime_43)); }
	inline TimeSpan_t3430258949  get__waitTime_43() const { return ____waitTime_43; }
	inline TimeSpan_t3430258949 * get_address_of__waitTime_43() { return &____waitTime_43; }
	inline void set__waitTime_43(TimeSpan_t3430258949  value)
	{
		____waitTime_43 = value;
	}

	inline static int32_t get_offset_of_U3CHeadersU3Ek__BackingField_47() { return static_cast<int32_t>(offsetof(WebSocket_t3268376029, ___U3CHeadersU3Ek__BackingField_47)); }
	inline Dictionary_2_t3943999495 * get_U3CHeadersU3Ek__BackingField_47() const { return ___U3CHeadersU3Ek__BackingField_47; }
	inline Dictionary_2_t3943999495 ** get_address_of_U3CHeadersU3Ek__BackingField_47() { return &___U3CHeadersU3Ek__BackingField_47; }
	inline void set_U3CHeadersU3Ek__BackingField_47(Dictionary_2_t3943999495 * value)
	{
		___U3CHeadersU3Ek__BackingField_47 = value;
		Il2CppCodeGenWriteBarrier((&___U3CHeadersU3Ek__BackingField_47), value);
	}

	inline static int32_t get_offset_of_OnClose_48() { return static_cast<int32_t>(offsetof(WebSocket_t3268376029, ___OnClose_48)); }
	inline EventHandler_1_t3230782241 * get_OnClose_48() const { return ___OnClose_48; }
	inline EventHandler_1_t3230782241 ** get_address_of_OnClose_48() { return &___OnClose_48; }
	inline void set_OnClose_48(EventHandler_1_t3230782241 * value)
	{
		___OnClose_48 = value;
		Il2CppCodeGenWriteBarrier((&___OnClose_48), value);
	}

	inline static int32_t get_offset_of_OnError_49() { return static_cast<int32_t>(offsetof(WebSocket_t3268376029, ___OnError_49)); }
	inline EventHandler_1_t3388497467 * get_OnError_49() const { return ___OnError_49; }
	inline EventHandler_1_t3388497467 ** get_address_of_OnError_49() { return &___OnError_49; }
	inline void set_OnError_49(EventHandler_1_t3388497467 * value)
	{
		___OnError_49 = value;
		Il2CppCodeGenWriteBarrier((&___OnError_49), value);
	}

	inline static int32_t get_offset_of_OnMessage_50() { return static_cast<int32_t>(offsetof(WebSocket_t3268376029, ___OnMessage_50)); }
	inline EventHandler_1_t1481358898 * get_OnMessage_50() const { return ___OnMessage_50; }
	inline EventHandler_1_t1481358898 ** get_address_of_OnMessage_50() { return &___OnMessage_50; }
	inline void set_OnMessage_50(EventHandler_1_t1481358898 * value)
	{
		___OnMessage_50 = value;
		Il2CppCodeGenWriteBarrier((&___OnMessage_50), value);
	}

	inline static int32_t get_offset_of_OnOpen_51() { return static_cast<int32_t>(offsetof(WebSocket_t3268376029, ___OnOpen_51)); }
	inline EventHandler_t277755526 * get_OnOpen_51() const { return ___OnOpen_51; }
	inline EventHandler_t277755526 ** get_address_of_OnOpen_51() { return &___OnOpen_51; }
	inline void set_OnOpen_51(EventHandler_t277755526 * value)
	{
		___OnOpen_51 = value;
		Il2CppCodeGenWriteBarrier((&___OnOpen_51), value);
	}
};

struct WebSocket_t3268376029_StaticFields
{
public:
	// System.Byte[] WebSocketSharp.WebSocket::EmptyBytes
	ByteU5BU5D_t3397334013* ___EmptyBytes_44;
	// System.Int32 WebSocketSharp.WebSocket::FragmentLength
	int32_t ___FragmentLength_45;
	// System.Security.Cryptography.RandomNumberGenerator WebSocketSharp.WebSocket::RandomNumber
	RandomNumberGenerator_t2510243513 * ___RandomNumber_46;

public:
	inline static int32_t get_offset_of_EmptyBytes_44() { return static_cast<int32_t>(offsetof(WebSocket_t3268376029_StaticFields, ___EmptyBytes_44)); }
	inline ByteU5BU5D_t3397334013* get_EmptyBytes_44() const { return ___EmptyBytes_44; }
	inline ByteU5BU5D_t3397334013** get_address_of_EmptyBytes_44() { return &___EmptyBytes_44; }
	inline void set_EmptyBytes_44(ByteU5BU5D_t3397334013* value)
	{
		___EmptyBytes_44 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyBytes_44), value);
	}

	inline static int32_t get_offset_of_FragmentLength_45() { return static_cast<int32_t>(offsetof(WebSocket_t3268376029_StaticFields, ___FragmentLength_45)); }
	inline int32_t get_FragmentLength_45() const { return ___FragmentLength_45; }
	inline int32_t* get_address_of_FragmentLength_45() { return &___FragmentLength_45; }
	inline void set_FragmentLength_45(int32_t value)
	{
		___FragmentLength_45 = value;
	}

	inline static int32_t get_offset_of_RandomNumber_46() { return static_cast<int32_t>(offsetof(WebSocket_t3268376029_StaticFields, ___RandomNumber_46)); }
	inline RandomNumberGenerator_t2510243513 * get_RandomNumber_46() const { return ___RandomNumber_46; }
	inline RandomNumberGenerator_t2510243513 ** get_address_of_RandomNumber_46() { return &___RandomNumber_46; }
	inline void set_RandomNumber_46(RandomNumberGenerator_t2510243513 * value)
	{
		___RandomNumber_46 = value;
		Il2CppCodeGenWriteBarrier((&___RandomNumber_46), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBSOCKET_T3268376029_H
#ifndef U3CBROADCASTASYNCU3EC__ANONSTOREY3_T1290467094_H
#define U3CBROADCASTASYNCU3EC__ANONSTOREY3_T1290467094_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Server.WebSocketSessionManager/<broadcastAsync>c__AnonStorey3
struct  U3CbroadcastAsyncU3Ec__AnonStorey3_t1290467094  : public RuntimeObject
{
public:
	// WebSocketSharp.Opcode WebSocketSharp.Server.WebSocketSessionManager/<broadcastAsync>c__AnonStorey3::opcode
	uint8_t ___opcode_0;
	// System.IO.Stream WebSocketSharp.Server.WebSocketSessionManager/<broadcastAsync>c__AnonStorey3::stream
	Stream_t3255436806 * ___stream_1;
	// System.Action WebSocketSharp.Server.WebSocketSessionManager/<broadcastAsync>c__AnonStorey3::completed
	Action_t3226471752 * ___completed_2;
	// WebSocketSharp.Server.WebSocketSessionManager WebSocketSharp.Server.WebSocketSessionManager/<broadcastAsync>c__AnonStorey3::$this
	WebSocketSessionManager_t2802512518 * ___U24this_3;

public:
	inline static int32_t get_offset_of_opcode_0() { return static_cast<int32_t>(offsetof(U3CbroadcastAsyncU3Ec__AnonStorey3_t1290467094, ___opcode_0)); }
	inline uint8_t get_opcode_0() const { return ___opcode_0; }
	inline uint8_t* get_address_of_opcode_0() { return &___opcode_0; }
	inline void set_opcode_0(uint8_t value)
	{
		___opcode_0 = value;
	}

	inline static int32_t get_offset_of_stream_1() { return static_cast<int32_t>(offsetof(U3CbroadcastAsyncU3Ec__AnonStorey3_t1290467094, ___stream_1)); }
	inline Stream_t3255436806 * get_stream_1() const { return ___stream_1; }
	inline Stream_t3255436806 ** get_address_of_stream_1() { return &___stream_1; }
	inline void set_stream_1(Stream_t3255436806 * value)
	{
		___stream_1 = value;
		Il2CppCodeGenWriteBarrier((&___stream_1), value);
	}

	inline static int32_t get_offset_of_completed_2() { return static_cast<int32_t>(offsetof(U3CbroadcastAsyncU3Ec__AnonStorey3_t1290467094, ___completed_2)); }
	inline Action_t3226471752 * get_completed_2() const { return ___completed_2; }
	inline Action_t3226471752 ** get_address_of_completed_2() { return &___completed_2; }
	inline void set_completed_2(Action_t3226471752 * value)
	{
		___completed_2 = value;
		Il2CppCodeGenWriteBarrier((&___completed_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CbroadcastAsyncU3Ec__AnonStorey3_t1290467094, ___U24this_3)); }
	inline WebSocketSessionManager_t2802512518 * get_U24this_3() const { return ___U24this_3; }
	inline WebSocketSessionManager_t2802512518 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(WebSocketSessionManager_t2802512518 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CBROADCASTASYNCU3EC__ANONSTOREY3_T1290467094_H
#ifndef U3CBROADCASTASYNCU3EC__ANONSTOREY2_T2856551035_H
#define U3CBROADCASTASYNCU3EC__ANONSTOREY2_T2856551035_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Server.WebSocketSessionManager/<broadcastAsync>c__AnonStorey2
struct  U3CbroadcastAsyncU3Ec__AnonStorey2_t2856551035  : public RuntimeObject
{
public:
	// WebSocketSharp.Opcode WebSocketSharp.Server.WebSocketSessionManager/<broadcastAsync>c__AnonStorey2::opcode
	uint8_t ___opcode_0;
	// System.Byte[] WebSocketSharp.Server.WebSocketSessionManager/<broadcastAsync>c__AnonStorey2::data
	ByteU5BU5D_t3397334013* ___data_1;
	// System.Action WebSocketSharp.Server.WebSocketSessionManager/<broadcastAsync>c__AnonStorey2::completed
	Action_t3226471752 * ___completed_2;
	// WebSocketSharp.Server.WebSocketSessionManager WebSocketSharp.Server.WebSocketSessionManager/<broadcastAsync>c__AnonStorey2::$this
	WebSocketSessionManager_t2802512518 * ___U24this_3;

public:
	inline static int32_t get_offset_of_opcode_0() { return static_cast<int32_t>(offsetof(U3CbroadcastAsyncU3Ec__AnonStorey2_t2856551035, ___opcode_0)); }
	inline uint8_t get_opcode_0() const { return ___opcode_0; }
	inline uint8_t* get_address_of_opcode_0() { return &___opcode_0; }
	inline void set_opcode_0(uint8_t value)
	{
		___opcode_0 = value;
	}

	inline static int32_t get_offset_of_data_1() { return static_cast<int32_t>(offsetof(U3CbroadcastAsyncU3Ec__AnonStorey2_t2856551035, ___data_1)); }
	inline ByteU5BU5D_t3397334013* get_data_1() const { return ___data_1; }
	inline ByteU5BU5D_t3397334013** get_address_of_data_1() { return &___data_1; }
	inline void set_data_1(ByteU5BU5D_t3397334013* value)
	{
		___data_1 = value;
		Il2CppCodeGenWriteBarrier((&___data_1), value);
	}

	inline static int32_t get_offset_of_completed_2() { return static_cast<int32_t>(offsetof(U3CbroadcastAsyncU3Ec__AnonStorey2_t2856551035, ___completed_2)); }
	inline Action_t3226471752 * get_completed_2() const { return ___completed_2; }
	inline Action_t3226471752 ** get_address_of_completed_2() { return &___completed_2; }
	inline void set_completed_2(Action_t3226471752 * value)
	{
		___completed_2 = value;
		Il2CppCodeGenWriteBarrier((&___completed_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CbroadcastAsyncU3Ec__AnonStorey2_t2856551035, ___U24this_3)); }
	inline WebSocketSessionManager_t2802512518 * get_U24this_3() const { return ___U24this_3; }
	inline WebSocketSessionManager_t2802512518 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(WebSocketSessionManager_t2802512518 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CBROADCASTASYNCU3EC__ANONSTOREY2_T2856551035_H
#ifndef DIALOGUELINE_T768215248_H
#define DIALOGUELINE_T768215248_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VikingCrewTools.ScriptedDialogueBehaviour/DialogueLine
struct  DialogueLine_t768215248  : public RuntimeObject
{
public:
	// UnityEngine.Transform VikingCrewTools.ScriptedDialogueBehaviour/DialogueLine::speaker
	Transform_t3275118058 * ___speaker_0;
	// System.Single VikingCrewTools.ScriptedDialogueBehaviour/DialogueLine::delay
	float ___delay_1;
	// System.String VikingCrewTools.ScriptedDialogueBehaviour/DialogueLine::line
	String_t* ___line_2;
	// VikingCrewTools.SpeechbubbleManager/SpeechbubbleType VikingCrewTools.ScriptedDialogueBehaviour/DialogueLine::speechBubbleType
	int32_t ___speechBubbleType_3;

public:
	inline static int32_t get_offset_of_speaker_0() { return static_cast<int32_t>(offsetof(DialogueLine_t768215248, ___speaker_0)); }
	inline Transform_t3275118058 * get_speaker_0() const { return ___speaker_0; }
	inline Transform_t3275118058 ** get_address_of_speaker_0() { return &___speaker_0; }
	inline void set_speaker_0(Transform_t3275118058 * value)
	{
		___speaker_0 = value;
		Il2CppCodeGenWriteBarrier((&___speaker_0), value);
	}

	inline static int32_t get_offset_of_delay_1() { return static_cast<int32_t>(offsetof(DialogueLine_t768215248, ___delay_1)); }
	inline float get_delay_1() const { return ___delay_1; }
	inline float* get_address_of_delay_1() { return &___delay_1; }
	inline void set_delay_1(float value)
	{
		___delay_1 = value;
	}

	inline static int32_t get_offset_of_line_2() { return static_cast<int32_t>(offsetof(DialogueLine_t768215248, ___line_2)); }
	inline String_t* get_line_2() const { return ___line_2; }
	inline String_t** get_address_of_line_2() { return &___line_2; }
	inline void set_line_2(String_t* value)
	{
		___line_2 = value;
		Il2CppCodeGenWriteBarrier((&___line_2), value);
	}

	inline static int32_t get_offset_of_speechBubbleType_3() { return static_cast<int32_t>(offsetof(DialogueLine_t768215248, ___speechBubbleType_3)); }
	inline int32_t get_speechBubbleType_3() const { return ___speechBubbleType_3; }
	inline int32_t* get_address_of_speechBubbleType_3() { return &___speechBubbleType_3; }
	inline void set_speechBubbleType_3(int32_t value)
	{
		___speechBubbleType_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIALOGUELINE_T768215248_H
#ifndef SPEECHBUBBLEPREFAB_T736279730_H
#define SPEECHBUBBLEPREFAB_T736279730_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VikingCrewTools.SpeechbubbleManager/SpeechbubblePrefab
struct  SpeechbubblePrefab_t736279730  : public RuntimeObject
{
public:
	// VikingCrewTools.SpeechbubbleManager/SpeechbubbleType VikingCrewTools.SpeechbubbleManager/SpeechbubblePrefab::type
	int32_t ___type_0;
	// UnityEngine.GameObject VikingCrewTools.SpeechbubbleManager/SpeechbubblePrefab::prefab
	GameObject_t1756533147 * ___prefab_1;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(SpeechbubblePrefab_t736279730, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_prefab_1() { return static_cast<int32_t>(offsetof(SpeechbubblePrefab_t736279730, ___prefab_1)); }
	inline GameObject_t1756533147 * get_prefab_1() const { return ___prefab_1; }
	inline GameObject_t1756533147 ** get_address_of_prefab_1() { return &___prefab_1; }
	inline void set_prefab_1(GameObject_t1756533147 * value)
	{
		___prefab_1 = value;
		Il2CppCodeGenWriteBarrier((&___prefab_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPEECHBUBBLEPREFAB_T736279730_H
#ifndef WEBSOCKETSERVER_T2522399002_H
#define WEBSOCKETSERVER_T2522399002_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Server.WebSocketServer
struct  WebSocketServer_t2522399002  : public RuntimeObject
{
public:
	// System.Net.IPAddress WebSocketSharp.Server.WebSocketServer::_address
	IPAddress_t1399971723 * ____address_0;
	// WebSocketSharp.Net.AuthenticationSchemes WebSocketSharp.Server.WebSocketServer::_authSchemes
	int32_t ____authSchemes_1;
	// System.Boolean WebSocketSharp.Server.WebSocketServer::_dnsStyle
	bool ____dnsStyle_3;
	// System.String WebSocketSharp.Server.WebSocketServer::_hostname
	String_t* ____hostname_4;
	// System.Net.Sockets.TcpListener WebSocketSharp.Server.WebSocketServer::_listener
	TcpListener_t1551297625 * ____listener_5;
	// WebSocketSharp.Logger WebSocketSharp.Server.WebSocketServer::_logger
	Logger_t2598199114 * ____logger_6;
	// System.Int32 WebSocketSharp.Server.WebSocketServer::_port
	int32_t ____port_7;
	// System.String WebSocketSharp.Server.WebSocketServer::_realm
	String_t* ____realm_8;
	// System.Threading.Thread WebSocketSharp.Server.WebSocketServer::_receiveThread
	Thread_t241561612 * ____receiveThread_9;
	// System.Boolean WebSocketSharp.Server.WebSocketServer::_reuseAddress
	bool ____reuseAddress_10;
	// System.Boolean WebSocketSharp.Server.WebSocketServer::_secure
	bool ____secure_11;
	// WebSocketSharp.Server.WebSocketServiceManager WebSocketSharp.Server.WebSocketServer::_services
	WebSocketServiceManager_t1683165547 * ____services_12;
	// WebSocketSharp.Net.ServerSslConfiguration WebSocketSharp.Server.WebSocketServer::_sslConfig
	ServerSslConfiguration_t204724213 * ____sslConfig_13;
	// WebSocketSharp.Server.ServerState modreq(System.Runtime.CompilerServices.IsVolatile) WebSocketSharp.Server.WebSocketServer::_state
	int32_t ____state_14;
	// System.Object WebSocketSharp.Server.WebSocketServer::_sync
	RuntimeObject * ____sync_15;
	// System.Func`2<System.Security.Principal.IIdentity,WebSocketSharp.Net.NetworkCredential> WebSocketSharp.Server.WebSocketServer::_userCredFinder
	Func_2_t353436505 * ____userCredFinder_16;

public:
	inline static int32_t get_offset_of__address_0() { return static_cast<int32_t>(offsetof(WebSocketServer_t2522399002, ____address_0)); }
	inline IPAddress_t1399971723 * get__address_0() const { return ____address_0; }
	inline IPAddress_t1399971723 ** get_address_of__address_0() { return &____address_0; }
	inline void set__address_0(IPAddress_t1399971723 * value)
	{
		____address_0 = value;
		Il2CppCodeGenWriteBarrier((&____address_0), value);
	}

	inline static int32_t get_offset_of__authSchemes_1() { return static_cast<int32_t>(offsetof(WebSocketServer_t2522399002, ____authSchemes_1)); }
	inline int32_t get__authSchemes_1() const { return ____authSchemes_1; }
	inline int32_t* get_address_of__authSchemes_1() { return &____authSchemes_1; }
	inline void set__authSchemes_1(int32_t value)
	{
		____authSchemes_1 = value;
	}

	inline static int32_t get_offset_of__dnsStyle_3() { return static_cast<int32_t>(offsetof(WebSocketServer_t2522399002, ____dnsStyle_3)); }
	inline bool get__dnsStyle_3() const { return ____dnsStyle_3; }
	inline bool* get_address_of__dnsStyle_3() { return &____dnsStyle_3; }
	inline void set__dnsStyle_3(bool value)
	{
		____dnsStyle_3 = value;
	}

	inline static int32_t get_offset_of__hostname_4() { return static_cast<int32_t>(offsetof(WebSocketServer_t2522399002, ____hostname_4)); }
	inline String_t* get__hostname_4() const { return ____hostname_4; }
	inline String_t** get_address_of__hostname_4() { return &____hostname_4; }
	inline void set__hostname_4(String_t* value)
	{
		____hostname_4 = value;
		Il2CppCodeGenWriteBarrier((&____hostname_4), value);
	}

	inline static int32_t get_offset_of__listener_5() { return static_cast<int32_t>(offsetof(WebSocketServer_t2522399002, ____listener_5)); }
	inline TcpListener_t1551297625 * get__listener_5() const { return ____listener_5; }
	inline TcpListener_t1551297625 ** get_address_of__listener_5() { return &____listener_5; }
	inline void set__listener_5(TcpListener_t1551297625 * value)
	{
		____listener_5 = value;
		Il2CppCodeGenWriteBarrier((&____listener_5), value);
	}

	inline static int32_t get_offset_of__logger_6() { return static_cast<int32_t>(offsetof(WebSocketServer_t2522399002, ____logger_6)); }
	inline Logger_t2598199114 * get__logger_6() const { return ____logger_6; }
	inline Logger_t2598199114 ** get_address_of__logger_6() { return &____logger_6; }
	inline void set__logger_6(Logger_t2598199114 * value)
	{
		____logger_6 = value;
		Il2CppCodeGenWriteBarrier((&____logger_6), value);
	}

	inline static int32_t get_offset_of__port_7() { return static_cast<int32_t>(offsetof(WebSocketServer_t2522399002, ____port_7)); }
	inline int32_t get__port_7() const { return ____port_7; }
	inline int32_t* get_address_of__port_7() { return &____port_7; }
	inline void set__port_7(int32_t value)
	{
		____port_7 = value;
	}

	inline static int32_t get_offset_of__realm_8() { return static_cast<int32_t>(offsetof(WebSocketServer_t2522399002, ____realm_8)); }
	inline String_t* get__realm_8() const { return ____realm_8; }
	inline String_t** get_address_of__realm_8() { return &____realm_8; }
	inline void set__realm_8(String_t* value)
	{
		____realm_8 = value;
		Il2CppCodeGenWriteBarrier((&____realm_8), value);
	}

	inline static int32_t get_offset_of__receiveThread_9() { return static_cast<int32_t>(offsetof(WebSocketServer_t2522399002, ____receiveThread_9)); }
	inline Thread_t241561612 * get__receiveThread_9() const { return ____receiveThread_9; }
	inline Thread_t241561612 ** get_address_of__receiveThread_9() { return &____receiveThread_9; }
	inline void set__receiveThread_9(Thread_t241561612 * value)
	{
		____receiveThread_9 = value;
		Il2CppCodeGenWriteBarrier((&____receiveThread_9), value);
	}

	inline static int32_t get_offset_of__reuseAddress_10() { return static_cast<int32_t>(offsetof(WebSocketServer_t2522399002, ____reuseAddress_10)); }
	inline bool get__reuseAddress_10() const { return ____reuseAddress_10; }
	inline bool* get_address_of__reuseAddress_10() { return &____reuseAddress_10; }
	inline void set__reuseAddress_10(bool value)
	{
		____reuseAddress_10 = value;
	}

	inline static int32_t get_offset_of__secure_11() { return static_cast<int32_t>(offsetof(WebSocketServer_t2522399002, ____secure_11)); }
	inline bool get__secure_11() const { return ____secure_11; }
	inline bool* get_address_of__secure_11() { return &____secure_11; }
	inline void set__secure_11(bool value)
	{
		____secure_11 = value;
	}

	inline static int32_t get_offset_of__services_12() { return static_cast<int32_t>(offsetof(WebSocketServer_t2522399002, ____services_12)); }
	inline WebSocketServiceManager_t1683165547 * get__services_12() const { return ____services_12; }
	inline WebSocketServiceManager_t1683165547 ** get_address_of__services_12() { return &____services_12; }
	inline void set__services_12(WebSocketServiceManager_t1683165547 * value)
	{
		____services_12 = value;
		Il2CppCodeGenWriteBarrier((&____services_12), value);
	}

	inline static int32_t get_offset_of__sslConfig_13() { return static_cast<int32_t>(offsetof(WebSocketServer_t2522399002, ____sslConfig_13)); }
	inline ServerSslConfiguration_t204724213 * get__sslConfig_13() const { return ____sslConfig_13; }
	inline ServerSslConfiguration_t204724213 ** get_address_of__sslConfig_13() { return &____sslConfig_13; }
	inline void set__sslConfig_13(ServerSslConfiguration_t204724213 * value)
	{
		____sslConfig_13 = value;
		Il2CppCodeGenWriteBarrier((&____sslConfig_13), value);
	}

	inline static int32_t get_offset_of__state_14() { return static_cast<int32_t>(offsetof(WebSocketServer_t2522399002, ____state_14)); }
	inline int32_t get__state_14() const { return ____state_14; }
	inline int32_t* get_address_of__state_14() { return &____state_14; }
	inline void set__state_14(int32_t value)
	{
		____state_14 = value;
	}

	inline static int32_t get_offset_of__sync_15() { return static_cast<int32_t>(offsetof(WebSocketServer_t2522399002, ____sync_15)); }
	inline RuntimeObject * get__sync_15() const { return ____sync_15; }
	inline RuntimeObject ** get_address_of__sync_15() { return &____sync_15; }
	inline void set__sync_15(RuntimeObject * value)
	{
		____sync_15 = value;
		Il2CppCodeGenWriteBarrier((&____sync_15), value);
	}

	inline static int32_t get_offset_of__userCredFinder_16() { return static_cast<int32_t>(offsetof(WebSocketServer_t2522399002, ____userCredFinder_16)); }
	inline Func_2_t353436505 * get__userCredFinder_16() const { return ____userCredFinder_16; }
	inline Func_2_t353436505 ** get_address_of__userCredFinder_16() { return &____userCredFinder_16; }
	inline void set__userCredFinder_16(Func_2_t353436505 * value)
	{
		____userCredFinder_16 = value;
		Il2CppCodeGenWriteBarrier((&____userCredFinder_16), value);
	}
};

struct WebSocketServer_t2522399002_StaticFields
{
public:
	// System.String WebSocketSharp.Server.WebSocketServer::_defaultRealm
	String_t* ____defaultRealm_2;

public:
	inline static int32_t get_offset_of__defaultRealm_2() { return static_cast<int32_t>(offsetof(WebSocketServer_t2522399002_StaticFields, ____defaultRealm_2)); }
	inline String_t* get__defaultRealm_2() const { return ____defaultRealm_2; }
	inline String_t** get_address_of__defaultRealm_2() { return &____defaultRealm_2; }
	inline void set__defaultRealm_2(String_t* value)
	{
		____defaultRealm_2 = value;
		Il2CppCodeGenWriteBarrier((&____defaultRealm_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBSOCKETSERVER_T2522399002_H
#ifndef HTTPSERVER_T490574305_H
#define HTTPSERVER_T490574305_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Server.HttpServer
struct  HttpServer_t490574305  : public RuntimeObject
{
public:
	// System.Net.IPAddress WebSocketSharp.Server.HttpServer::_address
	IPAddress_t1399971723 * ____address_0;
	// System.String WebSocketSharp.Server.HttpServer::_hostname
	String_t* ____hostname_1;
	// WebSocketSharp.Net.HttpListener WebSocketSharp.Server.HttpServer::_listener
	HttpListener_t4179429670 * ____listener_2;
	// WebSocketSharp.Logger WebSocketSharp.Server.HttpServer::_logger
	Logger_t2598199114 * ____logger_3;
	// System.Int32 WebSocketSharp.Server.HttpServer::_port
	int32_t ____port_4;
	// System.Threading.Thread WebSocketSharp.Server.HttpServer::_receiveThread
	Thread_t241561612 * ____receiveThread_5;
	// System.String WebSocketSharp.Server.HttpServer::_rootPath
	String_t* ____rootPath_6;
	// System.Boolean WebSocketSharp.Server.HttpServer::_secure
	bool ____secure_7;
	// WebSocketSharp.Server.WebSocketServiceManager WebSocketSharp.Server.HttpServer::_services
	WebSocketServiceManager_t1683165547 * ____services_8;
	// WebSocketSharp.Server.ServerState modreq(System.Runtime.CompilerServices.IsVolatile) WebSocketSharp.Server.HttpServer::_state
	int32_t ____state_9;
	// System.Object WebSocketSharp.Server.HttpServer::_sync
	RuntimeObject * ____sync_10;
	// System.Boolean WebSocketSharp.Server.HttpServer::_windows
	bool ____windows_11;
	// System.EventHandler`1<WebSocketSharp.Server.HttpRequestEventArgs> WebSocketSharp.Server.HttpServer::OnConnect
	EventHandler_1_t3804744336 * ___OnConnect_12;
	// System.EventHandler`1<WebSocketSharp.Server.HttpRequestEventArgs> WebSocketSharp.Server.HttpServer::OnDelete
	EventHandler_1_t3804744336 * ___OnDelete_13;
	// System.EventHandler`1<WebSocketSharp.Server.HttpRequestEventArgs> WebSocketSharp.Server.HttpServer::OnGet
	EventHandler_1_t3804744336 * ___OnGet_14;
	// System.EventHandler`1<WebSocketSharp.Server.HttpRequestEventArgs> WebSocketSharp.Server.HttpServer::OnHead
	EventHandler_1_t3804744336 * ___OnHead_15;
	// System.EventHandler`1<WebSocketSharp.Server.HttpRequestEventArgs> WebSocketSharp.Server.HttpServer::OnOptions
	EventHandler_1_t3804744336 * ___OnOptions_16;
	// System.EventHandler`1<WebSocketSharp.Server.HttpRequestEventArgs> WebSocketSharp.Server.HttpServer::OnPatch
	EventHandler_1_t3804744336 * ___OnPatch_17;
	// System.EventHandler`1<WebSocketSharp.Server.HttpRequestEventArgs> WebSocketSharp.Server.HttpServer::OnPost
	EventHandler_1_t3804744336 * ___OnPost_18;
	// System.EventHandler`1<WebSocketSharp.Server.HttpRequestEventArgs> WebSocketSharp.Server.HttpServer::OnPut
	EventHandler_1_t3804744336 * ___OnPut_19;
	// System.EventHandler`1<WebSocketSharp.Server.HttpRequestEventArgs> WebSocketSharp.Server.HttpServer::OnTrace
	EventHandler_1_t3804744336 * ___OnTrace_20;

public:
	inline static int32_t get_offset_of__address_0() { return static_cast<int32_t>(offsetof(HttpServer_t490574305, ____address_0)); }
	inline IPAddress_t1399971723 * get__address_0() const { return ____address_0; }
	inline IPAddress_t1399971723 ** get_address_of__address_0() { return &____address_0; }
	inline void set__address_0(IPAddress_t1399971723 * value)
	{
		____address_0 = value;
		Il2CppCodeGenWriteBarrier((&____address_0), value);
	}

	inline static int32_t get_offset_of__hostname_1() { return static_cast<int32_t>(offsetof(HttpServer_t490574305, ____hostname_1)); }
	inline String_t* get__hostname_1() const { return ____hostname_1; }
	inline String_t** get_address_of__hostname_1() { return &____hostname_1; }
	inline void set__hostname_1(String_t* value)
	{
		____hostname_1 = value;
		Il2CppCodeGenWriteBarrier((&____hostname_1), value);
	}

	inline static int32_t get_offset_of__listener_2() { return static_cast<int32_t>(offsetof(HttpServer_t490574305, ____listener_2)); }
	inline HttpListener_t4179429670 * get__listener_2() const { return ____listener_2; }
	inline HttpListener_t4179429670 ** get_address_of__listener_2() { return &____listener_2; }
	inline void set__listener_2(HttpListener_t4179429670 * value)
	{
		____listener_2 = value;
		Il2CppCodeGenWriteBarrier((&____listener_2), value);
	}

	inline static int32_t get_offset_of__logger_3() { return static_cast<int32_t>(offsetof(HttpServer_t490574305, ____logger_3)); }
	inline Logger_t2598199114 * get__logger_3() const { return ____logger_3; }
	inline Logger_t2598199114 ** get_address_of__logger_3() { return &____logger_3; }
	inline void set__logger_3(Logger_t2598199114 * value)
	{
		____logger_3 = value;
		Il2CppCodeGenWriteBarrier((&____logger_3), value);
	}

	inline static int32_t get_offset_of__port_4() { return static_cast<int32_t>(offsetof(HttpServer_t490574305, ____port_4)); }
	inline int32_t get__port_4() const { return ____port_4; }
	inline int32_t* get_address_of__port_4() { return &____port_4; }
	inline void set__port_4(int32_t value)
	{
		____port_4 = value;
	}

	inline static int32_t get_offset_of__receiveThread_5() { return static_cast<int32_t>(offsetof(HttpServer_t490574305, ____receiveThread_5)); }
	inline Thread_t241561612 * get__receiveThread_5() const { return ____receiveThread_5; }
	inline Thread_t241561612 ** get_address_of__receiveThread_5() { return &____receiveThread_5; }
	inline void set__receiveThread_5(Thread_t241561612 * value)
	{
		____receiveThread_5 = value;
		Il2CppCodeGenWriteBarrier((&____receiveThread_5), value);
	}

	inline static int32_t get_offset_of__rootPath_6() { return static_cast<int32_t>(offsetof(HttpServer_t490574305, ____rootPath_6)); }
	inline String_t* get__rootPath_6() const { return ____rootPath_6; }
	inline String_t** get_address_of__rootPath_6() { return &____rootPath_6; }
	inline void set__rootPath_6(String_t* value)
	{
		____rootPath_6 = value;
		Il2CppCodeGenWriteBarrier((&____rootPath_6), value);
	}

	inline static int32_t get_offset_of__secure_7() { return static_cast<int32_t>(offsetof(HttpServer_t490574305, ____secure_7)); }
	inline bool get__secure_7() const { return ____secure_7; }
	inline bool* get_address_of__secure_7() { return &____secure_7; }
	inline void set__secure_7(bool value)
	{
		____secure_7 = value;
	}

	inline static int32_t get_offset_of__services_8() { return static_cast<int32_t>(offsetof(HttpServer_t490574305, ____services_8)); }
	inline WebSocketServiceManager_t1683165547 * get__services_8() const { return ____services_8; }
	inline WebSocketServiceManager_t1683165547 ** get_address_of__services_8() { return &____services_8; }
	inline void set__services_8(WebSocketServiceManager_t1683165547 * value)
	{
		____services_8 = value;
		Il2CppCodeGenWriteBarrier((&____services_8), value);
	}

	inline static int32_t get_offset_of__state_9() { return static_cast<int32_t>(offsetof(HttpServer_t490574305, ____state_9)); }
	inline int32_t get__state_9() const { return ____state_9; }
	inline int32_t* get_address_of__state_9() { return &____state_9; }
	inline void set__state_9(int32_t value)
	{
		____state_9 = value;
	}

	inline static int32_t get_offset_of__sync_10() { return static_cast<int32_t>(offsetof(HttpServer_t490574305, ____sync_10)); }
	inline RuntimeObject * get__sync_10() const { return ____sync_10; }
	inline RuntimeObject ** get_address_of__sync_10() { return &____sync_10; }
	inline void set__sync_10(RuntimeObject * value)
	{
		____sync_10 = value;
		Il2CppCodeGenWriteBarrier((&____sync_10), value);
	}

	inline static int32_t get_offset_of__windows_11() { return static_cast<int32_t>(offsetof(HttpServer_t490574305, ____windows_11)); }
	inline bool get__windows_11() const { return ____windows_11; }
	inline bool* get_address_of__windows_11() { return &____windows_11; }
	inline void set__windows_11(bool value)
	{
		____windows_11 = value;
	}

	inline static int32_t get_offset_of_OnConnect_12() { return static_cast<int32_t>(offsetof(HttpServer_t490574305, ___OnConnect_12)); }
	inline EventHandler_1_t3804744336 * get_OnConnect_12() const { return ___OnConnect_12; }
	inline EventHandler_1_t3804744336 ** get_address_of_OnConnect_12() { return &___OnConnect_12; }
	inline void set_OnConnect_12(EventHandler_1_t3804744336 * value)
	{
		___OnConnect_12 = value;
		Il2CppCodeGenWriteBarrier((&___OnConnect_12), value);
	}

	inline static int32_t get_offset_of_OnDelete_13() { return static_cast<int32_t>(offsetof(HttpServer_t490574305, ___OnDelete_13)); }
	inline EventHandler_1_t3804744336 * get_OnDelete_13() const { return ___OnDelete_13; }
	inline EventHandler_1_t3804744336 ** get_address_of_OnDelete_13() { return &___OnDelete_13; }
	inline void set_OnDelete_13(EventHandler_1_t3804744336 * value)
	{
		___OnDelete_13 = value;
		Il2CppCodeGenWriteBarrier((&___OnDelete_13), value);
	}

	inline static int32_t get_offset_of_OnGet_14() { return static_cast<int32_t>(offsetof(HttpServer_t490574305, ___OnGet_14)); }
	inline EventHandler_1_t3804744336 * get_OnGet_14() const { return ___OnGet_14; }
	inline EventHandler_1_t3804744336 ** get_address_of_OnGet_14() { return &___OnGet_14; }
	inline void set_OnGet_14(EventHandler_1_t3804744336 * value)
	{
		___OnGet_14 = value;
		Il2CppCodeGenWriteBarrier((&___OnGet_14), value);
	}

	inline static int32_t get_offset_of_OnHead_15() { return static_cast<int32_t>(offsetof(HttpServer_t490574305, ___OnHead_15)); }
	inline EventHandler_1_t3804744336 * get_OnHead_15() const { return ___OnHead_15; }
	inline EventHandler_1_t3804744336 ** get_address_of_OnHead_15() { return &___OnHead_15; }
	inline void set_OnHead_15(EventHandler_1_t3804744336 * value)
	{
		___OnHead_15 = value;
		Il2CppCodeGenWriteBarrier((&___OnHead_15), value);
	}

	inline static int32_t get_offset_of_OnOptions_16() { return static_cast<int32_t>(offsetof(HttpServer_t490574305, ___OnOptions_16)); }
	inline EventHandler_1_t3804744336 * get_OnOptions_16() const { return ___OnOptions_16; }
	inline EventHandler_1_t3804744336 ** get_address_of_OnOptions_16() { return &___OnOptions_16; }
	inline void set_OnOptions_16(EventHandler_1_t3804744336 * value)
	{
		___OnOptions_16 = value;
		Il2CppCodeGenWriteBarrier((&___OnOptions_16), value);
	}

	inline static int32_t get_offset_of_OnPatch_17() { return static_cast<int32_t>(offsetof(HttpServer_t490574305, ___OnPatch_17)); }
	inline EventHandler_1_t3804744336 * get_OnPatch_17() const { return ___OnPatch_17; }
	inline EventHandler_1_t3804744336 ** get_address_of_OnPatch_17() { return &___OnPatch_17; }
	inline void set_OnPatch_17(EventHandler_1_t3804744336 * value)
	{
		___OnPatch_17 = value;
		Il2CppCodeGenWriteBarrier((&___OnPatch_17), value);
	}

	inline static int32_t get_offset_of_OnPost_18() { return static_cast<int32_t>(offsetof(HttpServer_t490574305, ___OnPost_18)); }
	inline EventHandler_1_t3804744336 * get_OnPost_18() const { return ___OnPost_18; }
	inline EventHandler_1_t3804744336 ** get_address_of_OnPost_18() { return &___OnPost_18; }
	inline void set_OnPost_18(EventHandler_1_t3804744336 * value)
	{
		___OnPost_18 = value;
		Il2CppCodeGenWriteBarrier((&___OnPost_18), value);
	}

	inline static int32_t get_offset_of_OnPut_19() { return static_cast<int32_t>(offsetof(HttpServer_t490574305, ___OnPut_19)); }
	inline EventHandler_1_t3804744336 * get_OnPut_19() const { return ___OnPut_19; }
	inline EventHandler_1_t3804744336 ** get_address_of_OnPut_19() { return &___OnPut_19; }
	inline void set_OnPut_19(EventHandler_1_t3804744336 * value)
	{
		___OnPut_19 = value;
		Il2CppCodeGenWriteBarrier((&___OnPut_19), value);
	}

	inline static int32_t get_offset_of_OnTrace_20() { return static_cast<int32_t>(offsetof(HttpServer_t490574305, ___OnTrace_20)); }
	inline EventHandler_1_t3804744336 * get_OnTrace_20() const { return ___OnTrace_20; }
	inline EventHandler_1_t3804744336 ** get_address_of_OnTrace_20() { return &___OnTrace_20; }
	inline void set_OnTrace_20(EventHandler_1_t3804744336 * value)
	{
		___OnTrace_20 = value;
		Il2CppCodeGenWriteBarrier((&___OnTrace_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPSERVER_T490574305_H
#ifndef WEBSOCKETSERVICEMANAGER_T1683165547_H
#define WEBSOCKETSERVICEMANAGER_T1683165547_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Server.WebSocketServiceManager
struct  WebSocketServiceManager_t1683165547  : public RuntimeObject
{
public:
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) WebSocketSharp.Server.WebSocketServiceManager::_clean
	bool ____clean_0;
	// System.Collections.Generic.Dictionary`2<System.String,WebSocketSharp.Server.WebSocketServiceHost> WebSocketSharp.Server.WebSocketServiceManager::_hosts
	Dictionary_2_t2406885756 * ____hosts_1;
	// WebSocketSharp.Logger WebSocketSharp.Server.WebSocketServiceManager::_logger
	Logger_t2598199114 * ____logger_2;
	// WebSocketSharp.Server.ServerState modreq(System.Runtime.CompilerServices.IsVolatile) WebSocketSharp.Server.WebSocketServiceManager::_state
	int32_t ____state_3;
	// System.Object WebSocketSharp.Server.WebSocketServiceManager::_sync
	RuntimeObject * ____sync_4;
	// System.TimeSpan WebSocketSharp.Server.WebSocketServiceManager::_waitTime
	TimeSpan_t3430258949  ____waitTime_5;

public:
	inline static int32_t get_offset_of__clean_0() { return static_cast<int32_t>(offsetof(WebSocketServiceManager_t1683165547, ____clean_0)); }
	inline bool get__clean_0() const { return ____clean_0; }
	inline bool* get_address_of__clean_0() { return &____clean_0; }
	inline void set__clean_0(bool value)
	{
		____clean_0 = value;
	}

	inline static int32_t get_offset_of__hosts_1() { return static_cast<int32_t>(offsetof(WebSocketServiceManager_t1683165547, ____hosts_1)); }
	inline Dictionary_2_t2406885756 * get__hosts_1() const { return ____hosts_1; }
	inline Dictionary_2_t2406885756 ** get_address_of__hosts_1() { return &____hosts_1; }
	inline void set__hosts_1(Dictionary_2_t2406885756 * value)
	{
		____hosts_1 = value;
		Il2CppCodeGenWriteBarrier((&____hosts_1), value);
	}

	inline static int32_t get_offset_of__logger_2() { return static_cast<int32_t>(offsetof(WebSocketServiceManager_t1683165547, ____logger_2)); }
	inline Logger_t2598199114 * get__logger_2() const { return ____logger_2; }
	inline Logger_t2598199114 ** get_address_of__logger_2() { return &____logger_2; }
	inline void set__logger_2(Logger_t2598199114 * value)
	{
		____logger_2 = value;
		Il2CppCodeGenWriteBarrier((&____logger_2), value);
	}

	inline static int32_t get_offset_of__state_3() { return static_cast<int32_t>(offsetof(WebSocketServiceManager_t1683165547, ____state_3)); }
	inline int32_t get__state_3() const { return ____state_3; }
	inline int32_t* get_address_of__state_3() { return &____state_3; }
	inline void set__state_3(int32_t value)
	{
		____state_3 = value;
	}

	inline static int32_t get_offset_of__sync_4() { return static_cast<int32_t>(offsetof(WebSocketServiceManager_t1683165547, ____sync_4)); }
	inline RuntimeObject * get__sync_4() const { return ____sync_4; }
	inline RuntimeObject ** get_address_of__sync_4() { return &____sync_4; }
	inline void set__sync_4(RuntimeObject * value)
	{
		____sync_4 = value;
		Il2CppCodeGenWriteBarrier((&____sync_4), value);
	}

	inline static int32_t get_offset_of__waitTime_5() { return static_cast<int32_t>(offsetof(WebSocketServiceManager_t1683165547, ____waitTime_5)); }
	inline TimeSpan_t3430258949  get__waitTime_5() const { return ____waitTime_5; }
	inline TimeSpan_t3430258949 * get_address_of__waitTime_5() { return &____waitTime_5; }
	inline void set__waitTime_5(TimeSpan_t3430258949  value)
	{
		____waitTime_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBSOCKETSERVICEMANAGER_T1683165547_H
#ifndef COMPONENT_T3819376471_H
#define COMPONENT_T3819376471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t3819376471  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T3819376471_H
#ifndef U3CAUTHENTICATEU3EC__ANONSTOREY1_T657871041_H
#define U3CAUTHENTICATEU3EC__ANONSTOREY1_T657871041_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext/<Authenticate>c__AnonStorey1
struct  U3CAuthenticateU3Ec__AnonStorey1_t657871041  : public RuntimeObject
{
public:
	// System.Int32 WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext/<Authenticate>c__AnonStorey1::retry
	int32_t ___retry_0;
	// WebSocketSharp.Net.AuthenticationSchemes WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext/<Authenticate>c__AnonStorey1::scheme
	int32_t ___scheme_1;
	// System.String WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext/<Authenticate>c__AnonStorey1::realm
	String_t* ___realm_2;
	// System.Func`2<System.Security.Principal.IIdentity,WebSocketSharp.Net.NetworkCredential> WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext/<Authenticate>c__AnonStorey1::credentialsFinder
	Func_2_t353436505 * ___credentialsFinder_3;
	// System.String WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext/<Authenticate>c__AnonStorey1::chal
	String_t* ___chal_4;
	// System.Func`1<System.Boolean> WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext/<Authenticate>c__AnonStorey1::auth
	Func_1_t1485000104 * ___auth_5;
	// WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext/<Authenticate>c__AnonStorey1::$this
	TcpListenerWebSocketContext_t1695227117 * ___U24this_6;

public:
	inline static int32_t get_offset_of_retry_0() { return static_cast<int32_t>(offsetof(U3CAuthenticateU3Ec__AnonStorey1_t657871041, ___retry_0)); }
	inline int32_t get_retry_0() const { return ___retry_0; }
	inline int32_t* get_address_of_retry_0() { return &___retry_0; }
	inline void set_retry_0(int32_t value)
	{
		___retry_0 = value;
	}

	inline static int32_t get_offset_of_scheme_1() { return static_cast<int32_t>(offsetof(U3CAuthenticateU3Ec__AnonStorey1_t657871041, ___scheme_1)); }
	inline int32_t get_scheme_1() const { return ___scheme_1; }
	inline int32_t* get_address_of_scheme_1() { return &___scheme_1; }
	inline void set_scheme_1(int32_t value)
	{
		___scheme_1 = value;
	}

	inline static int32_t get_offset_of_realm_2() { return static_cast<int32_t>(offsetof(U3CAuthenticateU3Ec__AnonStorey1_t657871041, ___realm_2)); }
	inline String_t* get_realm_2() const { return ___realm_2; }
	inline String_t** get_address_of_realm_2() { return &___realm_2; }
	inline void set_realm_2(String_t* value)
	{
		___realm_2 = value;
		Il2CppCodeGenWriteBarrier((&___realm_2), value);
	}

	inline static int32_t get_offset_of_credentialsFinder_3() { return static_cast<int32_t>(offsetof(U3CAuthenticateU3Ec__AnonStorey1_t657871041, ___credentialsFinder_3)); }
	inline Func_2_t353436505 * get_credentialsFinder_3() const { return ___credentialsFinder_3; }
	inline Func_2_t353436505 ** get_address_of_credentialsFinder_3() { return &___credentialsFinder_3; }
	inline void set_credentialsFinder_3(Func_2_t353436505 * value)
	{
		___credentialsFinder_3 = value;
		Il2CppCodeGenWriteBarrier((&___credentialsFinder_3), value);
	}

	inline static int32_t get_offset_of_chal_4() { return static_cast<int32_t>(offsetof(U3CAuthenticateU3Ec__AnonStorey1_t657871041, ___chal_4)); }
	inline String_t* get_chal_4() const { return ___chal_4; }
	inline String_t** get_address_of_chal_4() { return &___chal_4; }
	inline void set_chal_4(String_t* value)
	{
		___chal_4 = value;
		Il2CppCodeGenWriteBarrier((&___chal_4), value);
	}

	inline static int32_t get_offset_of_auth_5() { return static_cast<int32_t>(offsetof(U3CAuthenticateU3Ec__AnonStorey1_t657871041, ___auth_5)); }
	inline Func_1_t1485000104 * get_auth_5() const { return ___auth_5; }
	inline Func_1_t1485000104 ** get_address_of_auth_5() { return &___auth_5; }
	inline void set_auth_5(Func_1_t1485000104 * value)
	{
		___auth_5 = value;
		Il2CppCodeGenWriteBarrier((&___auth_5), value);
	}

	inline static int32_t get_offset_of_U24this_6() { return static_cast<int32_t>(offsetof(U3CAuthenticateU3Ec__AnonStorey1_t657871041, ___U24this_6)); }
	inline TcpListenerWebSocketContext_t1695227117 * get_U24this_6() const { return ___U24this_6; }
	inline TcpListenerWebSocketContext_t1695227117 ** get_address_of_U24this_6() { return &___U24this_6; }
	inline void set_U24this_6(TcpListenerWebSocketContext_t1695227117 * value)
	{
		___U24this_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CAUTHENTICATEU3EC__ANONSTOREY1_T657871041_H
#ifndef U3CBROADCASTASYNCU3EC__ANONSTOREY1_T1184085879_H
#define U3CBROADCASTASYNCU3EC__ANONSTOREY1_T1184085879_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Server.WebSocketServiceManager/<broadcastAsync>c__AnonStorey1
struct  U3CbroadcastAsyncU3Ec__AnonStorey1_t1184085879  : public RuntimeObject
{
public:
	// WebSocketSharp.Opcode WebSocketSharp.Server.WebSocketServiceManager/<broadcastAsync>c__AnonStorey1::opcode
	uint8_t ___opcode_0;
	// System.IO.Stream WebSocketSharp.Server.WebSocketServiceManager/<broadcastAsync>c__AnonStorey1::stream
	Stream_t3255436806 * ___stream_1;
	// System.Action WebSocketSharp.Server.WebSocketServiceManager/<broadcastAsync>c__AnonStorey1::completed
	Action_t3226471752 * ___completed_2;
	// WebSocketSharp.Server.WebSocketServiceManager WebSocketSharp.Server.WebSocketServiceManager/<broadcastAsync>c__AnonStorey1::$this
	WebSocketServiceManager_t1683165547 * ___U24this_3;

public:
	inline static int32_t get_offset_of_opcode_0() { return static_cast<int32_t>(offsetof(U3CbroadcastAsyncU3Ec__AnonStorey1_t1184085879, ___opcode_0)); }
	inline uint8_t get_opcode_0() const { return ___opcode_0; }
	inline uint8_t* get_address_of_opcode_0() { return &___opcode_0; }
	inline void set_opcode_0(uint8_t value)
	{
		___opcode_0 = value;
	}

	inline static int32_t get_offset_of_stream_1() { return static_cast<int32_t>(offsetof(U3CbroadcastAsyncU3Ec__AnonStorey1_t1184085879, ___stream_1)); }
	inline Stream_t3255436806 * get_stream_1() const { return ___stream_1; }
	inline Stream_t3255436806 ** get_address_of_stream_1() { return &___stream_1; }
	inline void set_stream_1(Stream_t3255436806 * value)
	{
		___stream_1 = value;
		Il2CppCodeGenWriteBarrier((&___stream_1), value);
	}

	inline static int32_t get_offset_of_completed_2() { return static_cast<int32_t>(offsetof(U3CbroadcastAsyncU3Ec__AnonStorey1_t1184085879, ___completed_2)); }
	inline Action_t3226471752 * get_completed_2() const { return ___completed_2; }
	inline Action_t3226471752 ** get_address_of_completed_2() { return &___completed_2; }
	inline void set_completed_2(Action_t3226471752 * value)
	{
		___completed_2 = value;
		Il2CppCodeGenWriteBarrier((&___completed_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CbroadcastAsyncU3Ec__AnonStorey1_t1184085879, ___U24this_3)); }
	inline WebSocketServiceManager_t1683165547 * get_U24this_3() const { return ___U24this_3; }
	inline WebSocketServiceManager_t1683165547 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(WebSocketServiceManager_t1683165547 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CBROADCASTASYNCU3EC__ANONSTOREY1_T1184085879_H
#ifndef U3CBROADCASTASYNCU3EC__ANONSTOREY0_T2750169820_H
#define U3CBROADCASTASYNCU3EC__ANONSTOREY0_T2750169820_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Server.WebSocketServiceManager/<broadcastAsync>c__AnonStorey0
struct  U3CbroadcastAsyncU3Ec__AnonStorey0_t2750169820  : public RuntimeObject
{
public:
	// WebSocketSharp.Opcode WebSocketSharp.Server.WebSocketServiceManager/<broadcastAsync>c__AnonStorey0::opcode
	uint8_t ___opcode_0;
	// System.Byte[] WebSocketSharp.Server.WebSocketServiceManager/<broadcastAsync>c__AnonStorey0::data
	ByteU5BU5D_t3397334013* ___data_1;
	// System.Action WebSocketSharp.Server.WebSocketServiceManager/<broadcastAsync>c__AnonStorey0::completed
	Action_t3226471752 * ___completed_2;
	// WebSocketSharp.Server.WebSocketServiceManager WebSocketSharp.Server.WebSocketServiceManager/<broadcastAsync>c__AnonStorey0::$this
	WebSocketServiceManager_t1683165547 * ___U24this_3;

public:
	inline static int32_t get_offset_of_opcode_0() { return static_cast<int32_t>(offsetof(U3CbroadcastAsyncU3Ec__AnonStorey0_t2750169820, ___opcode_0)); }
	inline uint8_t get_opcode_0() const { return ___opcode_0; }
	inline uint8_t* get_address_of_opcode_0() { return &___opcode_0; }
	inline void set_opcode_0(uint8_t value)
	{
		___opcode_0 = value;
	}

	inline static int32_t get_offset_of_data_1() { return static_cast<int32_t>(offsetof(U3CbroadcastAsyncU3Ec__AnonStorey0_t2750169820, ___data_1)); }
	inline ByteU5BU5D_t3397334013* get_data_1() const { return ___data_1; }
	inline ByteU5BU5D_t3397334013** get_address_of_data_1() { return &___data_1; }
	inline void set_data_1(ByteU5BU5D_t3397334013* value)
	{
		___data_1 = value;
		Il2CppCodeGenWriteBarrier((&___data_1), value);
	}

	inline static int32_t get_offset_of_completed_2() { return static_cast<int32_t>(offsetof(U3CbroadcastAsyncU3Ec__AnonStorey0_t2750169820, ___completed_2)); }
	inline Action_t3226471752 * get_completed_2() const { return ___completed_2; }
	inline Action_t3226471752 ** get_address_of_completed_2() { return &___completed_2; }
	inline void set_completed_2(Action_t3226471752 * value)
	{
		___completed_2 = value;
		Il2CppCodeGenWriteBarrier((&___completed_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CbroadcastAsyncU3Ec__AnonStorey0_t2750169820, ___U24this_3)); }
	inline WebSocketServiceManager_t1683165547 * get_U24this_3() const { return ___U24this_3; }
	inline WebSocketServiceManager_t1683165547 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(WebSocketServiceManager_t1683165547 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CBROADCASTASYNCU3EC__ANONSTOREY0_T2750169820_H
#ifndef WEBSOCKETBEHAVIOR_T3021808769_H
#define WEBSOCKETBEHAVIOR_T3021808769_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Server.WebSocketBehavior
struct  WebSocketBehavior_t3021808769  : public RuntimeObject
{
public:
	// WebSocketSharp.Net.WebSockets.WebSocketContext WebSocketSharp.Server.WebSocketBehavior::_context
	WebSocketContext_t3488732344 * ____context_0;
	// System.Func`3<WebSocketSharp.Net.CookieCollection,WebSocketSharp.Net.CookieCollection,System.Boolean> WebSocketSharp.Server.WebSocketBehavior::_cookiesValidator
	Func_3_t3137832106 * ____cookiesValidator_1;
	// System.Boolean WebSocketSharp.Server.WebSocketBehavior::_emitOnPing
	bool ____emitOnPing_2;
	// System.String WebSocketSharp.Server.WebSocketBehavior::_id
	String_t* ____id_3;
	// System.Boolean WebSocketSharp.Server.WebSocketBehavior::_ignoreExtensions
	bool ____ignoreExtensions_4;
	// System.Func`2<System.String,System.Boolean> WebSocketSharp.Server.WebSocketBehavior::_originValidator
	Func_2_t1989381442 * ____originValidator_5;
	// System.String WebSocketSharp.Server.WebSocketBehavior::_protocol
	String_t* ____protocol_6;
	// WebSocketSharp.Server.WebSocketSessionManager WebSocketSharp.Server.WebSocketBehavior::_sessions
	WebSocketSessionManager_t2802512518 * ____sessions_7;
	// System.DateTime WebSocketSharp.Server.WebSocketBehavior::_startTime
	DateTime_t693205669  ____startTime_8;
	// WebSocketSharp.WebSocket WebSocketSharp.Server.WebSocketBehavior::_websocket
	WebSocket_t3268376029 * ____websocket_9;

public:
	inline static int32_t get_offset_of__context_0() { return static_cast<int32_t>(offsetof(WebSocketBehavior_t3021808769, ____context_0)); }
	inline WebSocketContext_t3488732344 * get__context_0() const { return ____context_0; }
	inline WebSocketContext_t3488732344 ** get_address_of__context_0() { return &____context_0; }
	inline void set__context_0(WebSocketContext_t3488732344 * value)
	{
		____context_0 = value;
		Il2CppCodeGenWriteBarrier((&____context_0), value);
	}

	inline static int32_t get_offset_of__cookiesValidator_1() { return static_cast<int32_t>(offsetof(WebSocketBehavior_t3021808769, ____cookiesValidator_1)); }
	inline Func_3_t3137832106 * get__cookiesValidator_1() const { return ____cookiesValidator_1; }
	inline Func_3_t3137832106 ** get_address_of__cookiesValidator_1() { return &____cookiesValidator_1; }
	inline void set__cookiesValidator_1(Func_3_t3137832106 * value)
	{
		____cookiesValidator_1 = value;
		Il2CppCodeGenWriteBarrier((&____cookiesValidator_1), value);
	}

	inline static int32_t get_offset_of__emitOnPing_2() { return static_cast<int32_t>(offsetof(WebSocketBehavior_t3021808769, ____emitOnPing_2)); }
	inline bool get__emitOnPing_2() const { return ____emitOnPing_2; }
	inline bool* get_address_of__emitOnPing_2() { return &____emitOnPing_2; }
	inline void set__emitOnPing_2(bool value)
	{
		____emitOnPing_2 = value;
	}

	inline static int32_t get_offset_of__id_3() { return static_cast<int32_t>(offsetof(WebSocketBehavior_t3021808769, ____id_3)); }
	inline String_t* get__id_3() const { return ____id_3; }
	inline String_t** get_address_of__id_3() { return &____id_3; }
	inline void set__id_3(String_t* value)
	{
		____id_3 = value;
		Il2CppCodeGenWriteBarrier((&____id_3), value);
	}

	inline static int32_t get_offset_of__ignoreExtensions_4() { return static_cast<int32_t>(offsetof(WebSocketBehavior_t3021808769, ____ignoreExtensions_4)); }
	inline bool get__ignoreExtensions_4() const { return ____ignoreExtensions_4; }
	inline bool* get_address_of__ignoreExtensions_4() { return &____ignoreExtensions_4; }
	inline void set__ignoreExtensions_4(bool value)
	{
		____ignoreExtensions_4 = value;
	}

	inline static int32_t get_offset_of__originValidator_5() { return static_cast<int32_t>(offsetof(WebSocketBehavior_t3021808769, ____originValidator_5)); }
	inline Func_2_t1989381442 * get__originValidator_5() const { return ____originValidator_5; }
	inline Func_2_t1989381442 ** get_address_of__originValidator_5() { return &____originValidator_5; }
	inline void set__originValidator_5(Func_2_t1989381442 * value)
	{
		____originValidator_5 = value;
		Il2CppCodeGenWriteBarrier((&____originValidator_5), value);
	}

	inline static int32_t get_offset_of__protocol_6() { return static_cast<int32_t>(offsetof(WebSocketBehavior_t3021808769, ____protocol_6)); }
	inline String_t* get__protocol_6() const { return ____protocol_6; }
	inline String_t** get_address_of__protocol_6() { return &____protocol_6; }
	inline void set__protocol_6(String_t* value)
	{
		____protocol_6 = value;
		Il2CppCodeGenWriteBarrier((&____protocol_6), value);
	}

	inline static int32_t get_offset_of__sessions_7() { return static_cast<int32_t>(offsetof(WebSocketBehavior_t3021808769, ____sessions_7)); }
	inline WebSocketSessionManager_t2802512518 * get__sessions_7() const { return ____sessions_7; }
	inline WebSocketSessionManager_t2802512518 ** get_address_of__sessions_7() { return &____sessions_7; }
	inline void set__sessions_7(WebSocketSessionManager_t2802512518 * value)
	{
		____sessions_7 = value;
		Il2CppCodeGenWriteBarrier((&____sessions_7), value);
	}

	inline static int32_t get_offset_of__startTime_8() { return static_cast<int32_t>(offsetof(WebSocketBehavior_t3021808769, ____startTime_8)); }
	inline DateTime_t693205669  get__startTime_8() const { return ____startTime_8; }
	inline DateTime_t693205669 * get_address_of__startTime_8() { return &____startTime_8; }
	inline void set__startTime_8(DateTime_t693205669  value)
	{
		____startTime_8 = value;
	}

	inline static int32_t get_offset_of__websocket_9() { return static_cast<int32_t>(offsetof(WebSocketBehavior_t3021808769, ____websocket_9)); }
	inline WebSocket_t3268376029 * get__websocket_9() const { return ____websocket_9; }
	inline WebSocket_t3268376029 ** get_address_of__websocket_9() { return &____websocket_9; }
	inline void set__websocket_9(WebSocket_t3268376029 * value)
	{
		____websocket_9 = value;
		Il2CppCodeGenWriteBarrier((&____websocket_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBSOCKETBEHAVIOR_T3021808769_H
#ifndef SERVERSSLCONFIGURATION_T204724213_H
#define SERVERSSLCONFIGURATION_T204724213_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.ServerSslConfiguration
struct  ServerSslConfiguration_t204724213  : public SslConfiguration_t760772650
{
public:
	// System.Security.Cryptography.X509Certificates.X509Certificate2 WebSocketSharp.Net.ServerSslConfiguration::_cert
	X509Certificate2_t4056456767 * ____cert_6;
	// System.Boolean WebSocketSharp.Net.ServerSslConfiguration::_clientCertRequired
	bool ____clientCertRequired_7;

public:
	inline static int32_t get_offset_of__cert_6() { return static_cast<int32_t>(offsetof(ServerSslConfiguration_t204724213, ____cert_6)); }
	inline X509Certificate2_t4056456767 * get__cert_6() const { return ____cert_6; }
	inline X509Certificate2_t4056456767 ** get_address_of__cert_6() { return &____cert_6; }
	inline void set__cert_6(X509Certificate2_t4056456767 * value)
	{
		____cert_6 = value;
		Il2CppCodeGenWriteBarrier((&____cert_6), value);
	}

	inline static int32_t get_offset_of__clientCertRequired_7() { return static_cast<int32_t>(offsetof(ServerSslConfiguration_t204724213, ____clientCertRequired_7)); }
	inline bool get__clientCertRequired_7() const { return ____clientCertRequired_7; }
	inline bool* get_address_of__clientCertRequired_7() { return &____clientCertRequired_7; }
	inline void set__clientCertRequired_7(bool value)
	{
		____clientCertRequired_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERVERSSLCONFIGURATION_T204724213_H
#ifndef BEHAVIOUR_T955675639_H
#define BEHAVIOUR_T955675639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t955675639  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T955675639_H
#ifndef MONOBEHAVIOUR_T1158329972_H
#define MONOBEHAVIOUR_T1158329972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t1158329972  : public Behaviour_t955675639
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T1158329972_H
#ifndef UIBEHAVIOUR_T3960014691_H
#define UIBEHAVIOUR_T3960014691_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3960014691  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3960014691_H
#ifndef SPEECHBUBBLEBEHAVIOUR_T3020520789_H
#define SPEECHBUBBLEBEHAVIOUR_T3020520789_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VikingCrewTools.SpeechbubbleBehaviour
struct  SpeechbubbleBehaviour_t3020520789  : public MonoBehaviour_t1158329972
{
public:
	// System.Single VikingCrewTools.SpeechbubbleBehaviour::timeToLive
	float ___timeToLive_2;
	// UnityEngine.Transform VikingCrewTools.SpeechbubbleBehaviour::objectToFollow
	Transform_t3275118058 * ___objectToFollow_3;
	// UnityEngine.Vector3 VikingCrewTools.SpeechbubbleBehaviour::offset
	Vector3_t2243707580  ___offset_4;
	// UnityEngine.UI.Image VikingCrewTools.SpeechbubbleBehaviour::image
	Image_t2042527209 * ___image_5;
	// UnityEngine.UI.Text VikingCrewTools.SpeechbubbleBehaviour::text
	Text_t356221433 * ___text_6;

public:
	inline static int32_t get_offset_of_timeToLive_2() { return static_cast<int32_t>(offsetof(SpeechbubbleBehaviour_t3020520789, ___timeToLive_2)); }
	inline float get_timeToLive_2() const { return ___timeToLive_2; }
	inline float* get_address_of_timeToLive_2() { return &___timeToLive_2; }
	inline void set_timeToLive_2(float value)
	{
		___timeToLive_2 = value;
	}

	inline static int32_t get_offset_of_objectToFollow_3() { return static_cast<int32_t>(offsetof(SpeechbubbleBehaviour_t3020520789, ___objectToFollow_3)); }
	inline Transform_t3275118058 * get_objectToFollow_3() const { return ___objectToFollow_3; }
	inline Transform_t3275118058 ** get_address_of_objectToFollow_3() { return &___objectToFollow_3; }
	inline void set_objectToFollow_3(Transform_t3275118058 * value)
	{
		___objectToFollow_3 = value;
		Il2CppCodeGenWriteBarrier((&___objectToFollow_3), value);
	}

	inline static int32_t get_offset_of_offset_4() { return static_cast<int32_t>(offsetof(SpeechbubbleBehaviour_t3020520789, ___offset_4)); }
	inline Vector3_t2243707580  get_offset_4() const { return ___offset_4; }
	inline Vector3_t2243707580 * get_address_of_offset_4() { return &___offset_4; }
	inline void set_offset_4(Vector3_t2243707580  value)
	{
		___offset_4 = value;
	}

	inline static int32_t get_offset_of_image_5() { return static_cast<int32_t>(offsetof(SpeechbubbleBehaviour_t3020520789, ___image_5)); }
	inline Image_t2042527209 * get_image_5() const { return ___image_5; }
	inline Image_t2042527209 ** get_address_of_image_5() { return &___image_5; }
	inline void set_image_5(Image_t2042527209 * value)
	{
		___image_5 = value;
		Il2CppCodeGenWriteBarrier((&___image_5), value);
	}

	inline static int32_t get_offset_of_text_6() { return static_cast<int32_t>(offsetof(SpeechbubbleBehaviour_t3020520789, ___text_6)); }
	inline Text_t356221433 * get_text_6() const { return ___text_6; }
	inline Text_t356221433 ** get_address_of_text_6() { return &___text_6; }
	inline void set_text_6(Text_t356221433 * value)
	{
		___text_6 = value;
		Il2CppCodeGenWriteBarrier((&___text_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPEECHBUBBLEBEHAVIOUR_T3020520789_H
#ifndef WORLDCOMPONENT_T24747827_H
#define WORLDCOMPONENT_T24747827_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WorldComponent
struct  WorldComponent_t24747827  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORLDCOMPONENT_T24747827_H
#ifndef SCRIPTEDDIALOGUEBEHAVIOUR_T2807853473_H
#define SCRIPTEDDIALOGUEBEHAVIOUR_T2807853473_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VikingCrewTools.ScriptedDialogueBehaviour
struct  ScriptedDialogueBehaviour_t2807853473  : public MonoBehaviour_t1158329972
{
public:
	// VikingCrewTools.ScriptedDialogueBehaviour/DialogueLine[] VikingCrewTools.ScriptedDialogueBehaviour::script
	DialogueLineU5BU5D_t1619601905* ___script_2;
	// System.Boolean VikingCrewTools.ScriptedDialogueBehaviour::doRestartAtEnd
	bool ___doRestartAtEnd_3;
	// System.Single VikingCrewTools.ScriptedDialogueBehaviour::bubbleTimeToLive
	float ___bubbleTimeToLive_4;

public:
	inline static int32_t get_offset_of_script_2() { return static_cast<int32_t>(offsetof(ScriptedDialogueBehaviour_t2807853473, ___script_2)); }
	inline DialogueLineU5BU5D_t1619601905* get_script_2() const { return ___script_2; }
	inline DialogueLineU5BU5D_t1619601905** get_address_of_script_2() { return &___script_2; }
	inline void set_script_2(DialogueLineU5BU5D_t1619601905* value)
	{
		___script_2 = value;
		Il2CppCodeGenWriteBarrier((&___script_2), value);
	}

	inline static int32_t get_offset_of_doRestartAtEnd_3() { return static_cast<int32_t>(offsetof(ScriptedDialogueBehaviour_t2807853473, ___doRestartAtEnd_3)); }
	inline bool get_doRestartAtEnd_3() const { return ___doRestartAtEnd_3; }
	inline bool* get_address_of_doRestartAtEnd_3() { return &___doRestartAtEnd_3; }
	inline void set_doRestartAtEnd_3(bool value)
	{
		___doRestartAtEnd_3 = value;
	}

	inline static int32_t get_offset_of_bubbleTimeToLive_4() { return static_cast<int32_t>(offsetof(ScriptedDialogueBehaviour_t2807853473, ___bubbleTimeToLive_4)); }
	inline float get_bubbleTimeToLive_4() const { return ___bubbleTimeToLive_4; }
	inline float* get_address_of_bubbleTimeToLive_4() { return &___bubbleTimeToLive_4; }
	inline void set_bubbleTimeToLive_4(float value)
	{
		___bubbleTimeToLive_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRIPTEDDIALOGUEBEHAVIOUR_T2807853473_H
#ifndef WORLDCONTROLLER_T1328477704_H
#define WORLDCONTROLLER_T1328477704_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WorldController
struct  WorldController_t1328477704  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Animation WorldController::anime
	Animation_t2068071072 * ___anime_2;

public:
	inline static int32_t get_offset_of_anime_2() { return static_cast<int32_t>(offsetof(WorldController_t1328477704, ___anime_2)); }
	inline Animation_t2068071072 * get_anime_2() const { return ___anime_2; }
	inline Animation_t2068071072 ** get_address_of_anime_2() { return &___anime_2; }
	inline void set_anime_2(Animation_t2068071072 * value)
	{
		___anime_2 = value;
		Il2CppCodeGenWriteBarrier((&___anime_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORLDCONTROLLER_T1328477704_H
#ifndef UISPEECHCONTROLLER_T1819804546_H
#define UISPEECHCONTROLLER_T1819804546_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VikingCrewDevelopment.UISpeechController
struct  UISpeechController_t1819804546  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.InputField VikingCrewDevelopment.UISpeechController::txtMessage
	InputField_t1631627530 * ___txtMessage_2;
	// VikingCrewDevelopment.SayRandomThingsBehaviour VikingCrewDevelopment.UISpeechController::talkBehaviour
	SayRandomThingsBehaviour_t3866205118 * ___talkBehaviour_3;
	// UnityEngine.UI.ToggleGroup VikingCrewDevelopment.UISpeechController::toggles
	ToggleGroup_t1030026315 * ___toggles_4;

public:
	inline static int32_t get_offset_of_txtMessage_2() { return static_cast<int32_t>(offsetof(UISpeechController_t1819804546, ___txtMessage_2)); }
	inline InputField_t1631627530 * get_txtMessage_2() const { return ___txtMessage_2; }
	inline InputField_t1631627530 ** get_address_of_txtMessage_2() { return &___txtMessage_2; }
	inline void set_txtMessage_2(InputField_t1631627530 * value)
	{
		___txtMessage_2 = value;
		Il2CppCodeGenWriteBarrier((&___txtMessage_2), value);
	}

	inline static int32_t get_offset_of_talkBehaviour_3() { return static_cast<int32_t>(offsetof(UISpeechController_t1819804546, ___talkBehaviour_3)); }
	inline SayRandomThingsBehaviour_t3866205118 * get_talkBehaviour_3() const { return ___talkBehaviour_3; }
	inline SayRandomThingsBehaviour_t3866205118 ** get_address_of_talkBehaviour_3() { return &___talkBehaviour_3; }
	inline void set_talkBehaviour_3(SayRandomThingsBehaviour_t3866205118 * value)
	{
		___talkBehaviour_3 = value;
		Il2CppCodeGenWriteBarrier((&___talkBehaviour_3), value);
	}

	inline static int32_t get_offset_of_toggles_4() { return static_cast<int32_t>(offsetof(UISpeechController_t1819804546, ___toggles_4)); }
	inline ToggleGroup_t1030026315 * get_toggles_4() const { return ___toggles_4; }
	inline ToggleGroup_t1030026315 ** get_address_of_toggles_4() { return &___toggles_4; }
	inline void set_toggles_4(ToggleGroup_t1030026315 * value)
	{
		___toggles_4 = value;
		Il2CppCodeGenWriteBarrier((&___toggles_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISPEECHCONTROLLER_T1819804546_H
#ifndef SAYRANDOMTHINGSBEHAVIOUR_T3866205118_H
#define SAYRANDOMTHINGSBEHAVIOUR_T3866205118_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VikingCrewDevelopment.SayRandomThingsBehaviour
struct  SayRandomThingsBehaviour_t3866205118  : public MonoBehaviour_t1158329972
{
public:
	// System.String[] VikingCrewDevelopment.SayRandomThingsBehaviour::thingsToSay
	StringU5BU5D_t1642385972* ___thingsToSay_2;
	// UnityEngine.Transform VikingCrewDevelopment.SayRandomThingsBehaviour::mouth
	Transform_t3275118058 * ___mouth_3;
	// System.Single VikingCrewDevelopment.SayRandomThingsBehaviour::timeBetweenSpeak
	float ___timeBetweenSpeak_4;
	// System.Boolean VikingCrewDevelopment.SayRandomThingsBehaviour::doTalkOnYourOwn
	bool ___doTalkOnYourOwn_5;
	// System.Single VikingCrewDevelopment.SayRandomThingsBehaviour::timeToNextSpeak
	float ___timeToNextSpeak_6;

public:
	inline static int32_t get_offset_of_thingsToSay_2() { return static_cast<int32_t>(offsetof(SayRandomThingsBehaviour_t3866205118, ___thingsToSay_2)); }
	inline StringU5BU5D_t1642385972* get_thingsToSay_2() const { return ___thingsToSay_2; }
	inline StringU5BU5D_t1642385972** get_address_of_thingsToSay_2() { return &___thingsToSay_2; }
	inline void set_thingsToSay_2(StringU5BU5D_t1642385972* value)
	{
		___thingsToSay_2 = value;
		Il2CppCodeGenWriteBarrier((&___thingsToSay_2), value);
	}

	inline static int32_t get_offset_of_mouth_3() { return static_cast<int32_t>(offsetof(SayRandomThingsBehaviour_t3866205118, ___mouth_3)); }
	inline Transform_t3275118058 * get_mouth_3() const { return ___mouth_3; }
	inline Transform_t3275118058 ** get_address_of_mouth_3() { return &___mouth_3; }
	inline void set_mouth_3(Transform_t3275118058 * value)
	{
		___mouth_3 = value;
		Il2CppCodeGenWriteBarrier((&___mouth_3), value);
	}

	inline static int32_t get_offset_of_timeBetweenSpeak_4() { return static_cast<int32_t>(offsetof(SayRandomThingsBehaviour_t3866205118, ___timeBetweenSpeak_4)); }
	inline float get_timeBetweenSpeak_4() const { return ___timeBetweenSpeak_4; }
	inline float* get_address_of_timeBetweenSpeak_4() { return &___timeBetweenSpeak_4; }
	inline void set_timeBetweenSpeak_4(float value)
	{
		___timeBetweenSpeak_4 = value;
	}

	inline static int32_t get_offset_of_doTalkOnYourOwn_5() { return static_cast<int32_t>(offsetof(SayRandomThingsBehaviour_t3866205118, ___doTalkOnYourOwn_5)); }
	inline bool get_doTalkOnYourOwn_5() const { return ___doTalkOnYourOwn_5; }
	inline bool* get_address_of_doTalkOnYourOwn_5() { return &___doTalkOnYourOwn_5; }
	inline void set_doTalkOnYourOwn_5(bool value)
	{
		___doTalkOnYourOwn_5 = value;
	}

	inline static int32_t get_offset_of_timeToNextSpeak_6() { return static_cast<int32_t>(offsetof(SayRandomThingsBehaviour_t3866205118, ___timeToNextSpeak_6)); }
	inline float get_timeToNextSpeak_6() const { return ___timeToNextSpeak_6; }
	inline float* get_address_of_timeToNextSpeak_6() { return &___timeToNextSpeak_6; }
	inline void set_timeToNextSpeak_6(float value)
	{
		___timeToNextSpeak_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAYRANDOMTHINGSBEHAVIOUR_T3866205118_H
#ifndef SPEECHBUBBLEMANAGER_T3871578363_H
#define SPEECHBUBBLEMANAGER_T3871578363_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VikingCrewTools.SpeechbubbleManager
struct  SpeechbubbleManager_t3871578363  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Color VikingCrewTools.SpeechbubbleManager::defaultColor
	Color_t2020392075  ___defaultColor_2;
	// System.Single VikingCrewTools.SpeechbubbleManager::defaultTimeToLive
	float ___defaultTimeToLive_3;
	// System.Boolean VikingCrewTools.SpeechbubbleManager::is2D
	bool ___is2D_4;
	// System.Single VikingCrewTools.SpeechbubbleManager::sizeMultiplier
	float ___sizeMultiplier_5;
	// System.Collections.Generic.List`1<VikingCrewTools.SpeechbubbleManager/SpeechbubblePrefab> VikingCrewTools.SpeechbubbleManager::prefabs
	List_1_t105400862 * ___prefabs_6;
	// System.Collections.Generic.Dictionary`2<VikingCrewTools.SpeechbubbleManager/SpeechbubbleType,UnityEngine.GameObject> VikingCrewTools.SpeechbubbleManager::prefabsDict
	Dictionary_2_t3281136550 * ___prefabsDict_7;
	// System.Collections.Generic.Dictionary`2<VikingCrewTools.SpeechbubbleManager/SpeechbubbleType,System.Collections.Generic.Queue`1<VikingCrewTools.SpeechbubbleBehaviour>> VikingCrewTools.SpeechbubbleManager::speechbubbleQueue
	Dictionary_2_t69813731 * ___speechbubbleQueue_9;

public:
	inline static int32_t get_offset_of_defaultColor_2() { return static_cast<int32_t>(offsetof(SpeechbubbleManager_t3871578363, ___defaultColor_2)); }
	inline Color_t2020392075  get_defaultColor_2() const { return ___defaultColor_2; }
	inline Color_t2020392075 * get_address_of_defaultColor_2() { return &___defaultColor_2; }
	inline void set_defaultColor_2(Color_t2020392075  value)
	{
		___defaultColor_2 = value;
	}

	inline static int32_t get_offset_of_defaultTimeToLive_3() { return static_cast<int32_t>(offsetof(SpeechbubbleManager_t3871578363, ___defaultTimeToLive_3)); }
	inline float get_defaultTimeToLive_3() const { return ___defaultTimeToLive_3; }
	inline float* get_address_of_defaultTimeToLive_3() { return &___defaultTimeToLive_3; }
	inline void set_defaultTimeToLive_3(float value)
	{
		___defaultTimeToLive_3 = value;
	}

	inline static int32_t get_offset_of_is2D_4() { return static_cast<int32_t>(offsetof(SpeechbubbleManager_t3871578363, ___is2D_4)); }
	inline bool get_is2D_4() const { return ___is2D_4; }
	inline bool* get_address_of_is2D_4() { return &___is2D_4; }
	inline void set_is2D_4(bool value)
	{
		___is2D_4 = value;
	}

	inline static int32_t get_offset_of_sizeMultiplier_5() { return static_cast<int32_t>(offsetof(SpeechbubbleManager_t3871578363, ___sizeMultiplier_5)); }
	inline float get_sizeMultiplier_5() const { return ___sizeMultiplier_5; }
	inline float* get_address_of_sizeMultiplier_5() { return &___sizeMultiplier_5; }
	inline void set_sizeMultiplier_5(float value)
	{
		___sizeMultiplier_5 = value;
	}

	inline static int32_t get_offset_of_prefabs_6() { return static_cast<int32_t>(offsetof(SpeechbubbleManager_t3871578363, ___prefabs_6)); }
	inline List_1_t105400862 * get_prefabs_6() const { return ___prefabs_6; }
	inline List_1_t105400862 ** get_address_of_prefabs_6() { return &___prefabs_6; }
	inline void set_prefabs_6(List_1_t105400862 * value)
	{
		___prefabs_6 = value;
		Il2CppCodeGenWriteBarrier((&___prefabs_6), value);
	}

	inline static int32_t get_offset_of_prefabsDict_7() { return static_cast<int32_t>(offsetof(SpeechbubbleManager_t3871578363, ___prefabsDict_7)); }
	inline Dictionary_2_t3281136550 * get_prefabsDict_7() const { return ___prefabsDict_7; }
	inline Dictionary_2_t3281136550 ** get_address_of_prefabsDict_7() { return &___prefabsDict_7; }
	inline void set_prefabsDict_7(Dictionary_2_t3281136550 * value)
	{
		___prefabsDict_7 = value;
		Il2CppCodeGenWriteBarrier((&___prefabsDict_7), value);
	}

	inline static int32_t get_offset_of_speechbubbleQueue_9() { return static_cast<int32_t>(offsetof(SpeechbubbleManager_t3871578363, ___speechbubbleQueue_9)); }
	inline Dictionary_2_t69813731 * get_speechbubbleQueue_9() const { return ___speechbubbleQueue_9; }
	inline Dictionary_2_t69813731 ** get_address_of_speechbubbleQueue_9() { return &___speechbubbleQueue_9; }
	inline void set_speechbubbleQueue_9(Dictionary_2_t69813731 * value)
	{
		___speechbubbleQueue_9 = value;
		Il2CppCodeGenWriteBarrier((&___speechbubbleQueue_9), value);
	}
};

struct SpeechbubbleManager_t3871578363_StaticFields
{
public:
	// VikingCrewTools.SpeechbubbleManager VikingCrewTools.SpeechbubbleManager::instance
	SpeechbubbleManager_t3871578363 * ___instance_8;

public:
	inline static int32_t get_offset_of_instance_8() { return static_cast<int32_t>(offsetof(SpeechbubbleManager_t3871578363_StaticFields, ___instance_8)); }
	inline SpeechbubbleManager_t3871578363 * get_instance_8() const { return ___instance_8; }
	inline SpeechbubbleManager_t3871578363 ** get_address_of_instance_8() { return &___instance_8; }
	inline void set_instance_8(SpeechbubbleManager_t3871578363 * value)
	{
		___instance_8 = value;
		Il2CppCodeGenWriteBarrier((&___instance_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPEECHBUBBLEMANAGER_T3871578363_H
#ifndef TRAVISINTEGRATIONTESTS_T539661000_H
#define TRAVISINTEGRATIONTESTS_T539661000_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Editor.TravisIntegrationTests
struct  TravisIntegrationTests_t539661000  : public MonoBehaviour_t1158329972
{
public:

public:
};

struct TravisIntegrationTests_t539661000_StaticFields
{
public:
	// IBM.Watson.DeveloperCloud.Editor.UnitTestManager/OnTestsComplete IBM.Watson.DeveloperCloud.Editor.TravisIntegrationTests::<>f__mg$cache0
	OnTestsComplete_t1412473303 * ___U3CU3Ef__mgU24cache0_2;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_2() { return static_cast<int32_t>(offsetof(TravisIntegrationTests_t539661000_StaticFields, ___U3CU3Ef__mgU24cache0_2)); }
	inline OnTestsComplete_t1412473303 * get_U3CU3Ef__mgU24cache0_2() const { return ___U3CU3Ef__mgU24cache0_2; }
	inline OnTestsComplete_t1412473303 ** get_address_of_U3CU3Ef__mgU24cache0_2() { return &___U3CU3Ef__mgU24cache0_2; }
	inline void set_U3CU3Ef__mgU24cache0_2(OnTestsComplete_t1412473303 * value)
	{
		___U3CU3Ef__mgU24cache0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRAVISINTEGRATIONTESTS_T539661000_H
#ifndef RANDOMMOVEMENTBEHAVIOUR_T3593270701_H
#define RANDOMMOVEMENTBEHAVIOUR_T3593270701_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VikingCrewDevelopment.RandomMovementBehaviour
struct  RandomMovementBehaviour_t3593270701  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Rect VikingCrewDevelopment.RandomMovementBehaviour::bounds
	Rect_t3681755626  ___bounds_2;
	// System.Single VikingCrewDevelopment.RandomMovementBehaviour::speed
	float ___speed_3;
	// UnityEngine.Vector2 VikingCrewDevelopment.RandomMovementBehaviour::nextWaypoint
	Vector2_t2243707579  ___nextWaypoint_4;

public:
	inline static int32_t get_offset_of_bounds_2() { return static_cast<int32_t>(offsetof(RandomMovementBehaviour_t3593270701, ___bounds_2)); }
	inline Rect_t3681755626  get_bounds_2() const { return ___bounds_2; }
	inline Rect_t3681755626 * get_address_of_bounds_2() { return &___bounds_2; }
	inline void set_bounds_2(Rect_t3681755626  value)
	{
		___bounds_2 = value;
	}

	inline static int32_t get_offset_of_speed_3() { return static_cast<int32_t>(offsetof(RandomMovementBehaviour_t3593270701, ___speed_3)); }
	inline float get_speed_3() const { return ___speed_3; }
	inline float* get_address_of_speed_3() { return &___speed_3; }
	inline void set_speed_3(float value)
	{
		___speed_3 = value;
	}

	inline static int32_t get_offset_of_nextWaypoint_4() { return static_cast<int32_t>(offsetof(RandomMovementBehaviour_t3593270701, ___nextWaypoint_4)); }
	inline Vector2_t2243707579  get_nextWaypoint_4() const { return ___nextWaypoint_4; }
	inline Vector2_t2243707579 * get_address_of_nextWaypoint_4() { return &___nextWaypoint_4; }
	inline void set_nextWaypoint_4(Vector2_t2243707579  value)
	{
		___nextWaypoint_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANDOMMOVEMENTBEHAVIOUR_T3593270701_H
#ifndef MOUSEORBITIMPROVED_T3290280027_H
#define MOUSEORBITIMPROVED_T3290280027_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VikingCrewTools.MouseOrbitImproved
struct  MouseOrbitImproved_t3290280027  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform VikingCrewTools.MouseOrbitImproved::target
	Transform_t3275118058 * ___target_2;
	// System.Single VikingCrewTools.MouseOrbitImproved::distance
	float ___distance_3;
	// System.Single VikingCrewTools.MouseOrbitImproved::xSpeed
	float ___xSpeed_4;
	// System.Single VikingCrewTools.MouseOrbitImproved::ySpeed
	float ___ySpeed_5;
	// System.Single VikingCrewTools.MouseOrbitImproved::yMinLimit
	float ___yMinLimit_6;
	// System.Single VikingCrewTools.MouseOrbitImproved::yMaxLimit
	float ___yMaxLimit_7;
	// System.Single VikingCrewTools.MouseOrbitImproved::distanceMin
	float ___distanceMin_8;
	// System.Single VikingCrewTools.MouseOrbitImproved::distanceMax
	float ___distanceMax_9;
	// UnityEngine.Rigidbody VikingCrewTools.MouseOrbitImproved::_rigidbody
	Rigidbody_t4233889191 * ____rigidbody_10;
	// System.Single VikingCrewTools.MouseOrbitImproved::x
	float ___x_11;
	// System.Single VikingCrewTools.MouseOrbitImproved::y
	float ___y_12;

public:
	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(MouseOrbitImproved_t3290280027, ___target_2)); }
	inline Transform_t3275118058 * get_target_2() const { return ___target_2; }
	inline Transform_t3275118058 ** get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(Transform_t3275118058 * value)
	{
		___target_2 = value;
		Il2CppCodeGenWriteBarrier((&___target_2), value);
	}

	inline static int32_t get_offset_of_distance_3() { return static_cast<int32_t>(offsetof(MouseOrbitImproved_t3290280027, ___distance_3)); }
	inline float get_distance_3() const { return ___distance_3; }
	inline float* get_address_of_distance_3() { return &___distance_3; }
	inline void set_distance_3(float value)
	{
		___distance_3 = value;
	}

	inline static int32_t get_offset_of_xSpeed_4() { return static_cast<int32_t>(offsetof(MouseOrbitImproved_t3290280027, ___xSpeed_4)); }
	inline float get_xSpeed_4() const { return ___xSpeed_4; }
	inline float* get_address_of_xSpeed_4() { return &___xSpeed_4; }
	inline void set_xSpeed_4(float value)
	{
		___xSpeed_4 = value;
	}

	inline static int32_t get_offset_of_ySpeed_5() { return static_cast<int32_t>(offsetof(MouseOrbitImproved_t3290280027, ___ySpeed_5)); }
	inline float get_ySpeed_5() const { return ___ySpeed_5; }
	inline float* get_address_of_ySpeed_5() { return &___ySpeed_5; }
	inline void set_ySpeed_5(float value)
	{
		___ySpeed_5 = value;
	}

	inline static int32_t get_offset_of_yMinLimit_6() { return static_cast<int32_t>(offsetof(MouseOrbitImproved_t3290280027, ___yMinLimit_6)); }
	inline float get_yMinLimit_6() const { return ___yMinLimit_6; }
	inline float* get_address_of_yMinLimit_6() { return &___yMinLimit_6; }
	inline void set_yMinLimit_6(float value)
	{
		___yMinLimit_6 = value;
	}

	inline static int32_t get_offset_of_yMaxLimit_7() { return static_cast<int32_t>(offsetof(MouseOrbitImproved_t3290280027, ___yMaxLimit_7)); }
	inline float get_yMaxLimit_7() const { return ___yMaxLimit_7; }
	inline float* get_address_of_yMaxLimit_7() { return &___yMaxLimit_7; }
	inline void set_yMaxLimit_7(float value)
	{
		___yMaxLimit_7 = value;
	}

	inline static int32_t get_offset_of_distanceMin_8() { return static_cast<int32_t>(offsetof(MouseOrbitImproved_t3290280027, ___distanceMin_8)); }
	inline float get_distanceMin_8() const { return ___distanceMin_8; }
	inline float* get_address_of_distanceMin_8() { return &___distanceMin_8; }
	inline void set_distanceMin_8(float value)
	{
		___distanceMin_8 = value;
	}

	inline static int32_t get_offset_of_distanceMax_9() { return static_cast<int32_t>(offsetof(MouseOrbitImproved_t3290280027, ___distanceMax_9)); }
	inline float get_distanceMax_9() const { return ___distanceMax_9; }
	inline float* get_address_of_distanceMax_9() { return &___distanceMax_9; }
	inline void set_distanceMax_9(float value)
	{
		___distanceMax_9 = value;
	}

	inline static int32_t get_offset_of__rigidbody_10() { return static_cast<int32_t>(offsetof(MouseOrbitImproved_t3290280027, ____rigidbody_10)); }
	inline Rigidbody_t4233889191 * get__rigidbody_10() const { return ____rigidbody_10; }
	inline Rigidbody_t4233889191 ** get_address_of__rigidbody_10() { return &____rigidbody_10; }
	inline void set__rigidbody_10(Rigidbody_t4233889191 * value)
	{
		____rigidbody_10 = value;
		Il2CppCodeGenWriteBarrier((&____rigidbody_10), value);
	}

	inline static int32_t get_offset_of_x_11() { return static_cast<int32_t>(offsetof(MouseOrbitImproved_t3290280027, ___x_11)); }
	inline float get_x_11() const { return ___x_11; }
	inline float* get_address_of_x_11() { return &___x_11; }
	inline void set_x_11(float value)
	{
		___x_11 = value;
	}

	inline static int32_t get_offset_of_y_12() { return static_cast<int32_t>(offsetof(MouseOrbitImproved_t3290280027, ___y_12)); }
	inline float get_y_12() const { return ___y_12; }
	inline float* get_address_of_y_12() { return &___y_12; }
	inline void set_y_12(float value)
	{
		___y_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSEORBITIMPROVED_T3290280027_H
#ifndef LAYOUTGROUP_T3962498969_H
#define LAYOUTGROUP_T3962498969_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.LayoutGroup
struct  LayoutGroup_t3962498969  : public UIBehaviour_t3960014691
{
public:
	// UnityEngine.RectOffset UnityEngine.UI.LayoutGroup::m_Padding
	RectOffset_t3387826427 * ___m_Padding_2;
	// UnityEngine.TextAnchor UnityEngine.UI.LayoutGroup::m_ChildAlignment
	int32_t ___m_ChildAlignment_3;
	// UnityEngine.RectTransform UnityEngine.UI.LayoutGroup::m_Rect
	RectTransform_t3349966182 * ___m_Rect_4;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.LayoutGroup::m_Tracker
	DrivenRectTransformTracker_t154385424  ___m_Tracker_5;
	// UnityEngine.Vector2 UnityEngine.UI.LayoutGroup::m_TotalMinSize
	Vector2_t2243707579  ___m_TotalMinSize_6;
	// UnityEngine.Vector2 UnityEngine.UI.LayoutGroup::m_TotalPreferredSize
	Vector2_t2243707579  ___m_TotalPreferredSize_7;
	// UnityEngine.Vector2 UnityEngine.UI.LayoutGroup::m_TotalFlexibleSize
	Vector2_t2243707579  ___m_TotalFlexibleSize_8;
	// System.Collections.Generic.List`1<UnityEngine.RectTransform> UnityEngine.UI.LayoutGroup::m_RectChildren
	List_1_t2719087314 * ___m_RectChildren_9;

public:
	inline static int32_t get_offset_of_m_Padding_2() { return static_cast<int32_t>(offsetof(LayoutGroup_t3962498969, ___m_Padding_2)); }
	inline RectOffset_t3387826427 * get_m_Padding_2() const { return ___m_Padding_2; }
	inline RectOffset_t3387826427 ** get_address_of_m_Padding_2() { return &___m_Padding_2; }
	inline void set_m_Padding_2(RectOffset_t3387826427 * value)
	{
		___m_Padding_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Padding_2), value);
	}

	inline static int32_t get_offset_of_m_ChildAlignment_3() { return static_cast<int32_t>(offsetof(LayoutGroup_t3962498969, ___m_ChildAlignment_3)); }
	inline int32_t get_m_ChildAlignment_3() const { return ___m_ChildAlignment_3; }
	inline int32_t* get_address_of_m_ChildAlignment_3() { return &___m_ChildAlignment_3; }
	inline void set_m_ChildAlignment_3(int32_t value)
	{
		___m_ChildAlignment_3 = value;
	}

	inline static int32_t get_offset_of_m_Rect_4() { return static_cast<int32_t>(offsetof(LayoutGroup_t3962498969, ___m_Rect_4)); }
	inline RectTransform_t3349966182 * get_m_Rect_4() const { return ___m_Rect_4; }
	inline RectTransform_t3349966182 ** get_address_of_m_Rect_4() { return &___m_Rect_4; }
	inline void set_m_Rect_4(RectTransform_t3349966182 * value)
	{
		___m_Rect_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rect_4), value);
	}

	inline static int32_t get_offset_of_m_Tracker_5() { return static_cast<int32_t>(offsetof(LayoutGroup_t3962498969, ___m_Tracker_5)); }
	inline DrivenRectTransformTracker_t154385424  get_m_Tracker_5() const { return ___m_Tracker_5; }
	inline DrivenRectTransformTracker_t154385424 * get_address_of_m_Tracker_5() { return &___m_Tracker_5; }
	inline void set_m_Tracker_5(DrivenRectTransformTracker_t154385424  value)
	{
		___m_Tracker_5 = value;
	}

	inline static int32_t get_offset_of_m_TotalMinSize_6() { return static_cast<int32_t>(offsetof(LayoutGroup_t3962498969, ___m_TotalMinSize_6)); }
	inline Vector2_t2243707579  get_m_TotalMinSize_6() const { return ___m_TotalMinSize_6; }
	inline Vector2_t2243707579 * get_address_of_m_TotalMinSize_6() { return &___m_TotalMinSize_6; }
	inline void set_m_TotalMinSize_6(Vector2_t2243707579  value)
	{
		___m_TotalMinSize_6 = value;
	}

	inline static int32_t get_offset_of_m_TotalPreferredSize_7() { return static_cast<int32_t>(offsetof(LayoutGroup_t3962498969, ___m_TotalPreferredSize_7)); }
	inline Vector2_t2243707579  get_m_TotalPreferredSize_7() const { return ___m_TotalPreferredSize_7; }
	inline Vector2_t2243707579 * get_address_of_m_TotalPreferredSize_7() { return &___m_TotalPreferredSize_7; }
	inline void set_m_TotalPreferredSize_7(Vector2_t2243707579  value)
	{
		___m_TotalPreferredSize_7 = value;
	}

	inline static int32_t get_offset_of_m_TotalFlexibleSize_8() { return static_cast<int32_t>(offsetof(LayoutGroup_t3962498969, ___m_TotalFlexibleSize_8)); }
	inline Vector2_t2243707579  get_m_TotalFlexibleSize_8() const { return ___m_TotalFlexibleSize_8; }
	inline Vector2_t2243707579 * get_address_of_m_TotalFlexibleSize_8() { return &___m_TotalFlexibleSize_8; }
	inline void set_m_TotalFlexibleSize_8(Vector2_t2243707579  value)
	{
		___m_TotalFlexibleSize_8 = value;
	}

	inline static int32_t get_offset_of_m_RectChildren_9() { return static_cast<int32_t>(offsetof(LayoutGroup_t3962498969, ___m_RectChildren_9)); }
	inline List_1_t2719087314 * get_m_RectChildren_9() const { return ___m_RectChildren_9; }
	inline List_1_t2719087314 ** get_address_of_m_RectChildren_9() { return &___m_RectChildren_9; }
	inline void set_m_RectChildren_9(List_1_t2719087314 * value)
	{
		___m_RectChildren_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectChildren_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYOUTGROUP_T3962498969_H
#ifndef HORIZONTALORVERTICALLAYOUTGROUP_T1968298610_H
#define HORIZONTALORVERTICALLAYOUTGROUP_T1968298610_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.HorizontalOrVerticalLayoutGroup
struct  HorizontalOrVerticalLayoutGroup_t1968298610  : public LayoutGroup_t3962498969
{
public:
	// System.Single UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_Spacing
	float ___m_Spacing_10;
	// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_ChildForceExpandWidth
	bool ___m_ChildForceExpandWidth_11;
	// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_ChildForceExpandHeight
	bool ___m_ChildForceExpandHeight_12;
	// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_ChildControlWidth
	bool ___m_ChildControlWidth_13;
	// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_ChildControlHeight
	bool ___m_ChildControlHeight_14;

public:
	inline static int32_t get_offset_of_m_Spacing_10() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_t1968298610, ___m_Spacing_10)); }
	inline float get_m_Spacing_10() const { return ___m_Spacing_10; }
	inline float* get_address_of_m_Spacing_10() { return &___m_Spacing_10; }
	inline void set_m_Spacing_10(float value)
	{
		___m_Spacing_10 = value;
	}

	inline static int32_t get_offset_of_m_ChildForceExpandWidth_11() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_t1968298610, ___m_ChildForceExpandWidth_11)); }
	inline bool get_m_ChildForceExpandWidth_11() const { return ___m_ChildForceExpandWidth_11; }
	inline bool* get_address_of_m_ChildForceExpandWidth_11() { return &___m_ChildForceExpandWidth_11; }
	inline void set_m_ChildForceExpandWidth_11(bool value)
	{
		___m_ChildForceExpandWidth_11 = value;
	}

	inline static int32_t get_offset_of_m_ChildForceExpandHeight_12() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_t1968298610, ___m_ChildForceExpandHeight_12)); }
	inline bool get_m_ChildForceExpandHeight_12() const { return ___m_ChildForceExpandHeight_12; }
	inline bool* get_address_of_m_ChildForceExpandHeight_12() { return &___m_ChildForceExpandHeight_12; }
	inline void set_m_ChildForceExpandHeight_12(bool value)
	{
		___m_ChildForceExpandHeight_12 = value;
	}

	inline static int32_t get_offset_of_m_ChildControlWidth_13() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_t1968298610, ___m_ChildControlWidth_13)); }
	inline bool get_m_ChildControlWidth_13() const { return ___m_ChildControlWidth_13; }
	inline bool* get_address_of_m_ChildControlWidth_13() { return &___m_ChildControlWidth_13; }
	inline void set_m_ChildControlWidth_13(bool value)
	{
		___m_ChildControlWidth_13 = value;
	}

	inline static int32_t get_offset_of_m_ChildControlHeight_14() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_t1968298610, ___m_ChildControlHeight_14)); }
	inline bool get_m_ChildControlHeight_14() const { return ___m_ChildControlHeight_14; }
	inline bool* get_address_of_m_ChildControlHeight_14() { return &___m_ChildControlHeight_14; }
	inline void set_m_ChildControlHeight_14(bool value)
	{
		___m_ChildControlHeight_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HORIZONTALORVERTICALLAYOUTGROUP_T1968298610_H
#ifndef HORIZONTALLAYOUTGROUP_T2875670365_H
#define HORIZONTALLAYOUTGROUP_T2875670365_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.HorizontalLayoutGroup
struct  HorizontalLayoutGroup_t2875670365  : public HorizontalOrVerticalLayoutGroup_t1968298610
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HORIZONTALLAYOUTGROUP_T2875670365_H
#ifndef RATIOLAYOUTFITTER_T2199399473_H
#define RATIOLAYOUTFITTER_T2199399473_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VikingCrewTools.RatioLayoutFitter
struct  RatioLayoutFitter_t2199399473  : public HorizontalLayoutGroup_t2875670365
{
public:
	// UnityEngine.Vector2 VikingCrewTools.RatioLayoutFitter::startPad
	Vector2_t2243707579  ___startPad_15;
	// UnityEngine.Vector2 VikingCrewTools.RatioLayoutFitter::stopPad
	Vector2_t2243707579  ___stopPad_16;
	// UnityEngine.RectTransform VikingCrewTools.RatioLayoutFitter::childToFit
	RectTransform_t3349966182 * ___childToFit_17;

public:
	inline static int32_t get_offset_of_startPad_15() { return static_cast<int32_t>(offsetof(RatioLayoutFitter_t2199399473, ___startPad_15)); }
	inline Vector2_t2243707579  get_startPad_15() const { return ___startPad_15; }
	inline Vector2_t2243707579 * get_address_of_startPad_15() { return &___startPad_15; }
	inline void set_startPad_15(Vector2_t2243707579  value)
	{
		___startPad_15 = value;
	}

	inline static int32_t get_offset_of_stopPad_16() { return static_cast<int32_t>(offsetof(RatioLayoutFitter_t2199399473, ___stopPad_16)); }
	inline Vector2_t2243707579  get_stopPad_16() const { return ___stopPad_16; }
	inline Vector2_t2243707579 * get_address_of_stopPad_16() { return &___stopPad_16; }
	inline void set_stopPad_16(Vector2_t2243707579  value)
	{
		___stopPad_16 = value;
	}

	inline static int32_t get_offset_of_childToFit_17() { return static_cast<int32_t>(offsetof(RatioLayoutFitter_t2199399473, ___childToFit_17)); }
	inline RectTransform_t3349966182 * get_childToFit_17() const { return ___childToFit_17; }
	inline RectTransform_t3349966182 ** get_address_of_childToFit_17() { return &___childToFit_17; }
	inline void set_childToFit_17(RectTransform_t3349966182 * value)
	{
		___childToFit_17 = value;
		Il2CppCodeGenWriteBarrier((&___childToFit_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RATIOLAYOUTFITTER_T2199399473_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3300 = { sizeof (HttpStreamAsyncResult_t782812803), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3300[10] = 
{
	HttpStreamAsyncResult_t782812803::get_offset_of__buffer_0(),
	HttpStreamAsyncResult_t782812803::get_offset_of__callback_1(),
	HttpStreamAsyncResult_t782812803::get_offset_of__completed_2(),
	HttpStreamAsyncResult_t782812803::get_offset_of__count_3(),
	HttpStreamAsyncResult_t782812803::get_offset_of__exception_4(),
	HttpStreamAsyncResult_t782812803::get_offset_of__offset_5(),
	HttpStreamAsyncResult_t782812803::get_offset_of__state_6(),
	HttpStreamAsyncResult_t782812803::get_offset_of__sync_7(),
	HttpStreamAsyncResult_t782812803::get_offset_of__syncRead_8(),
	HttpStreamAsyncResult_t782812803::get_offset_of__waitHandle_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3301 = { sizeof (HttpUtility_t3363705102), -1, sizeof(HttpUtility_t3363705102_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3301[3] = 
{
	HttpUtility_t3363705102_StaticFields::get_offset_of__entities_0(),
	HttpUtility_t3363705102_StaticFields::get_offset_of__hexChars_1(),
	HttpUtility_t3363705102_StaticFields::get_offset_of__sync_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3302 = { sizeof (HttpVersion_t4270509666), -1, sizeof(HttpVersion_t4270509666_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3302[2] = 
{
	HttpVersion_t4270509666_StaticFields::get_offset_of_Version10_0(),
	HttpVersion_t4270509666_StaticFields::get_offset_of_Version11_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3303 = { sizeof (InputChunkState_t126533044)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3303[6] = 
{
	InputChunkState_t126533044::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3304 = { sizeof (InputState_t2016389849)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3304[3] = 
{
	InputState_t2016389849::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3305 = { sizeof (LineState_t825951569)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3305[4] = 
{
	LineState_t825951569::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3306 = { sizeof (NetworkCredential_t3911206805), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3306[4] = 
{
	NetworkCredential_t3911206805::get_offset_of__domain_0(),
	NetworkCredential_t3911206805::get_offset_of__password_1(),
	NetworkCredential_t3911206805::get_offset_of__roles_2(),
	NetworkCredential_t3911206805::get_offset_of__userName_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3307 = { sizeof (QueryStringCollection_t595917821), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3308 = { sizeof (ReadBufferState_t2596755373), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3308[5] = 
{
	ReadBufferState_t2596755373::get_offset_of__asyncResult_0(),
	ReadBufferState_t2596755373::get_offset_of__buffer_1(),
	ReadBufferState_t2596755373::get_offset_of__count_2(),
	ReadBufferState_t2596755373::get_offset_of__initialCount_3(),
	ReadBufferState_t2596755373::get_offset_of__offset_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3309 = { sizeof (RequestStream_t775716369), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3309[6] = 
{
	RequestStream_t775716369::get_offset_of__bodyLeft_1(),
	RequestStream_t775716369::get_offset_of__buffer_2(),
	RequestStream_t775716369::get_offset_of__count_3(),
	RequestStream_t775716369::get_offset_of__disposed_4(),
	RequestStream_t775716369::get_offset_of__offset_5(),
	RequestStream_t775716369::get_offset_of__stream_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3310 = { sizeof (ResponseStream_t3200689523), -1, sizeof(ResponseStream_t3200689523_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3310[9] = 
{
	ResponseStream_t3200689523::get_offset_of__body_1(),
	ResponseStream_t3200689523_StaticFields::get_offset_of__crlf_2(),
	ResponseStream_t3200689523::get_offset_of__disposed_3(),
	ResponseStream_t3200689523::get_offset_of__response_4(),
	ResponseStream_t3200689523::get_offset_of__sendChunked_5(),
	ResponseStream_t3200689523::get_offset_of__stream_6(),
	ResponseStream_t3200689523::get_offset_of__write_7(),
	ResponseStream_t3200689523::get_offset_of__writeBody_8(),
	ResponseStream_t3200689523::get_offset_of__writeChunked_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3311 = { sizeof (ServerSslConfiguration_t204724213), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3311[2] = 
{
	ServerSslConfiguration_t204724213::get_offset_of__cert_6(),
	ServerSslConfiguration_t204724213::get_offset_of__clientCertRequired_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3312 = { sizeof (SslConfiguration_t760772650), -1, sizeof(SslConfiguration_t760772650_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3312[6] = 
{
	SslConfiguration_t760772650::get_offset_of__certSelectionCallback_0(),
	SslConfiguration_t760772650::get_offset_of__certValidationCallback_1(),
	SslConfiguration_t760772650::get_offset_of__checkCertRevocation_2(),
	SslConfiguration_t760772650::get_offset_of__enabledProtocols_3(),
	SslConfiguration_t760772650_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
	SslConfiguration_t760772650_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3313 = { sizeof (WebHeaderCollection_t1932982249), -1, sizeof(WebHeaderCollection_t1932982249_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3313[3] = 
{
	WebHeaderCollection_t1932982249_StaticFields::get_offset_of__headers_12(),
	WebHeaderCollection_t1932982249::get_offset_of__internallyUsed_13(),
	WebHeaderCollection_t1932982249::get_offset_of__state_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3314 = { sizeof (U3CToStringMultiValueU3Ec__AnonStorey0_t1328261099), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3314[3] = 
{
	U3CToStringMultiValueU3Ec__AnonStorey0_t1328261099::get_offset_of_response_0(),
	U3CToStringMultiValueU3Ec__AnonStorey0_t1328261099::get_offset_of_buff_1(),
	U3CToStringMultiValueU3Ec__AnonStorey0_t1328261099::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3315 = { sizeof (U3CGetObjectDataU3Ec__AnonStorey1_t2338822889), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3315[3] = 
{
	U3CGetObjectDataU3Ec__AnonStorey1_t2338822889::get_offset_of_serializationInfo_0(),
	U3CGetObjectDataU3Ec__AnonStorey1_t2338822889::get_offset_of_cnt_1(),
	U3CGetObjectDataU3Ec__AnonStorey1_t2338822889::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3316 = { sizeof (U3CToStringU3Ec__AnonStorey2_t1254512383), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3316[2] = 
{
	U3CToStringU3Ec__AnonStorey2_t1254512383::get_offset_of_buff_0(),
	U3CToStringU3Ec__AnonStorey2_t1254512383::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3317 = { sizeof (HttpListenerWebSocketContext_t1778866096), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3317[2] = 
{
	HttpListenerWebSocketContext_t1778866096::get_offset_of__context_0(),
	HttpListenerWebSocketContext_t1778866096::get_offset_of__websocket_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3318 = { sizeof (U3CU3Ec__Iterator0_t1818225242), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3318[8] = 
{
	U3CU3Ec__Iterator0_t1818225242::get_offset_of_U3CprotocolsU3E__0_0(),
	U3CU3Ec__Iterator0_t1818225242::get_offset_of_U24locvar0_1(),
	U3CU3Ec__Iterator0_t1818225242::get_offset_of_U24locvar1_2(),
	U3CU3Ec__Iterator0_t1818225242::get_offset_of_U3CprotocolU3E__1_3(),
	U3CU3Ec__Iterator0_t1818225242::get_offset_of_U24this_4(),
	U3CU3Ec__Iterator0_t1818225242::get_offset_of_U24current_5(),
	U3CU3Ec__Iterator0_t1818225242::get_offset_of_U24disposing_6(),
	U3CU3Ec__Iterator0_t1818225242::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3319 = { sizeof (TcpListenerWebSocketContext_t1695227117), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3319[10] = 
{
	TcpListenerWebSocketContext_t1695227117::get_offset_of__cookies_0(),
	TcpListenerWebSocketContext_t1695227117::get_offset_of__logger_1(),
	TcpListenerWebSocketContext_t1695227117::get_offset_of__queryString_2(),
	TcpListenerWebSocketContext_t1695227117::get_offset_of__request_3(),
	TcpListenerWebSocketContext_t1695227117::get_offset_of__secure_4(),
	TcpListenerWebSocketContext_t1695227117::get_offset_of__stream_5(),
	TcpListenerWebSocketContext_t1695227117::get_offset_of__tcpClient_6(),
	TcpListenerWebSocketContext_t1695227117::get_offset_of__uri_7(),
	TcpListenerWebSocketContext_t1695227117::get_offset_of__user_8(),
	TcpListenerWebSocketContext_t1695227117::get_offset_of__websocket_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3320 = { sizeof (U3CU3Ec__Iterator0_t1986573215), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3320[8] = 
{
	U3CU3Ec__Iterator0_t1986573215::get_offset_of_U3CprotocolsU3E__0_0(),
	U3CU3Ec__Iterator0_t1986573215::get_offset_of_U24locvar0_1(),
	U3CU3Ec__Iterator0_t1986573215::get_offset_of_U24locvar1_2(),
	U3CU3Ec__Iterator0_t1986573215::get_offset_of_U3CprotocolU3E__1_3(),
	U3CU3Ec__Iterator0_t1986573215::get_offset_of_U24this_4(),
	U3CU3Ec__Iterator0_t1986573215::get_offset_of_U24current_5(),
	U3CU3Ec__Iterator0_t1986573215::get_offset_of_U24disposing_6(),
	U3CU3Ec__Iterator0_t1986573215::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3321 = { sizeof (U3CAuthenticateU3Ec__AnonStorey1_t657871041), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3321[7] = 
{
	U3CAuthenticateU3Ec__AnonStorey1_t657871041::get_offset_of_retry_0(),
	U3CAuthenticateU3Ec__AnonStorey1_t657871041::get_offset_of_scheme_1(),
	U3CAuthenticateU3Ec__AnonStorey1_t657871041::get_offset_of_realm_2(),
	U3CAuthenticateU3Ec__AnonStorey1_t657871041::get_offset_of_credentialsFinder_3(),
	U3CAuthenticateU3Ec__AnonStorey1_t657871041::get_offset_of_chal_4(),
	U3CAuthenticateU3Ec__AnonStorey1_t657871041::get_offset_of_auth_5(),
	U3CAuthenticateU3Ec__AnonStorey1_t657871041::get_offset_of_U24this_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3322 = { sizeof (WebSocketContext_t3488732344), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3323 = { sizeof (Opcode_t2313788840)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3323[7] = 
{
	Opcode_t2313788840::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3324 = { sizeof (PayloadData_t3839327312), -1, sizeof(PayloadData_t3839327312_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3324[5] = 
{
	PayloadData_t3839327312::get_offset_of__data_0(),
	PayloadData_t3839327312::get_offset_of__extDataLength_1(),
	PayloadData_t3839327312::get_offset_of__length_2(),
	PayloadData_t3839327312_StaticFields::get_offset_of_Empty_3(),
	PayloadData_t3839327312_StaticFields::get_offset_of_MaxLength_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3325 = { sizeof (U3CGetEnumeratorU3Ec__Iterator0_t3664690781), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3325[7] = 
{
	U3CGetEnumeratorU3Ec__Iterator0_t3664690781::get_offset_of_U24locvar0_0(),
	U3CGetEnumeratorU3Ec__Iterator0_t3664690781::get_offset_of_U24locvar1_1(),
	U3CGetEnumeratorU3Ec__Iterator0_t3664690781::get_offset_of_U3CbU3E__1_2(),
	U3CGetEnumeratorU3Ec__Iterator0_t3664690781::get_offset_of_U24this_3(),
	U3CGetEnumeratorU3Ec__Iterator0_t3664690781::get_offset_of_U24current_4(),
	U3CGetEnumeratorU3Ec__Iterator0_t3664690781::get_offset_of_U24disposing_5(),
	U3CGetEnumeratorU3Ec__Iterator0_t3664690781::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3326 = { sizeof (Rsv_t1058189029)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3326[3] = 
{
	Rsv_t1058189029::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3327 = { sizeof (HttpRequestEventArgs_t918469868), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3327[2] = 
{
	HttpRequestEventArgs_t918469868::get_offset_of__request_1(),
	HttpRequestEventArgs_t918469868::get_offset_of__response_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3328 = { sizeof (HttpServer_t490574305), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3328[21] = 
{
	HttpServer_t490574305::get_offset_of__address_0(),
	HttpServer_t490574305::get_offset_of__hostname_1(),
	HttpServer_t490574305::get_offset_of__listener_2(),
	HttpServer_t490574305::get_offset_of__logger_3(),
	HttpServer_t490574305::get_offset_of__port_4(),
	HttpServer_t490574305::get_offset_of__receiveThread_5(),
	HttpServer_t490574305::get_offset_of__rootPath_6(),
	HttpServer_t490574305::get_offset_of__secure_7(),
	HttpServer_t490574305::get_offset_of__services_8(),
	HttpServer_t490574305::get_offset_of__state_9(),
	HttpServer_t490574305::get_offset_of__sync_10(),
	HttpServer_t490574305::get_offset_of__windows_11(),
	HttpServer_t490574305::get_offset_of_OnConnect_12(),
	HttpServer_t490574305::get_offset_of_OnDelete_13(),
	HttpServer_t490574305::get_offset_of_OnGet_14(),
	HttpServer_t490574305::get_offset_of_OnHead_15(),
	HttpServer_t490574305::get_offset_of_OnOptions_16(),
	HttpServer_t490574305::get_offset_of_OnPatch_17(),
	HttpServer_t490574305::get_offset_of_OnPost_18(),
	HttpServer_t490574305::get_offset_of_OnPut_19(),
	HttpServer_t490574305::get_offset_of_OnTrace_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3329 = { sizeof (U3CreceiveRequestU3Ec__AnonStorey0_t2614482259), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3329[2] = 
{
	U3CreceiveRequestU3Ec__AnonStorey0_t2614482259::get_offset_of_ctx_0(),
	U3CreceiveRequestU3Ec__AnonStorey0_t2614482259::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3330 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3331 = { sizeof (ServerState_t3054640078)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3331[5] = 
{
	ServerState_t3054640078::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3332 = { sizeof (WebSocketBehavior_t3021808769), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3332[10] = 
{
	WebSocketBehavior_t3021808769::get_offset_of__context_0(),
	WebSocketBehavior_t3021808769::get_offset_of__cookiesValidator_1(),
	WebSocketBehavior_t3021808769::get_offset_of__emitOnPing_2(),
	WebSocketBehavior_t3021808769::get_offset_of__id_3(),
	WebSocketBehavior_t3021808769::get_offset_of__ignoreExtensions_4(),
	WebSocketBehavior_t3021808769::get_offset_of__originValidator_5(),
	WebSocketBehavior_t3021808769::get_offset_of__protocol_6(),
	WebSocketBehavior_t3021808769::get_offset_of__sessions_7(),
	WebSocketBehavior_t3021808769::get_offset_of__startTime_8(),
	WebSocketBehavior_t3021808769::get_offset_of__websocket_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3333 = { sizeof (WebSocketServer_t2522399002), -1, sizeof(WebSocketServer_t2522399002_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3333[17] = 
{
	WebSocketServer_t2522399002::get_offset_of__address_0(),
	WebSocketServer_t2522399002::get_offset_of__authSchemes_1(),
	WebSocketServer_t2522399002_StaticFields::get_offset_of__defaultRealm_2(),
	WebSocketServer_t2522399002::get_offset_of__dnsStyle_3(),
	WebSocketServer_t2522399002::get_offset_of__hostname_4(),
	WebSocketServer_t2522399002::get_offset_of__listener_5(),
	WebSocketServer_t2522399002::get_offset_of__logger_6(),
	WebSocketServer_t2522399002::get_offset_of__port_7(),
	WebSocketServer_t2522399002::get_offset_of__realm_8(),
	WebSocketServer_t2522399002::get_offset_of__receiveThread_9(),
	WebSocketServer_t2522399002::get_offset_of__reuseAddress_10(),
	WebSocketServer_t2522399002::get_offset_of__secure_11(),
	WebSocketServer_t2522399002::get_offset_of__services_12(),
	WebSocketServer_t2522399002::get_offset_of__sslConfig_13(),
	WebSocketServer_t2522399002::get_offset_of__state_14(),
	WebSocketServer_t2522399002::get_offset_of__sync_15(),
	WebSocketServer_t2522399002::get_offset_of__userCredFinder_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3334 = { sizeof (U3CreceiveRequestU3Ec__AnonStorey0_t1358907818), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3334[2] = 
{
	U3CreceiveRequestU3Ec__AnonStorey0_t1358907818::get_offset_of_cl_0(),
	U3CreceiveRequestU3Ec__AnonStorey0_t1358907818::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3335 = { sizeof (WebSocketServiceHost_t492106494), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3336 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3336[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3337 = { sizeof (WebSocketServiceManager_t1683165547), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3337[6] = 
{
	WebSocketServiceManager_t1683165547::get_offset_of__clean_0(),
	WebSocketServiceManager_t1683165547::get_offset_of__hosts_1(),
	WebSocketServiceManager_t1683165547::get_offset_of__logger_2(),
	WebSocketServiceManager_t1683165547::get_offset_of__state_3(),
	WebSocketServiceManager_t1683165547::get_offset_of__sync_4(),
	WebSocketServiceManager_t1683165547::get_offset_of__waitTime_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3338 = { sizeof (U3CbroadcastAsyncU3Ec__AnonStorey0_t2750169820), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3338[4] = 
{
	U3CbroadcastAsyncU3Ec__AnonStorey0_t2750169820::get_offset_of_opcode_0(),
	U3CbroadcastAsyncU3Ec__AnonStorey0_t2750169820::get_offset_of_data_1(),
	U3CbroadcastAsyncU3Ec__AnonStorey0_t2750169820::get_offset_of_completed_2(),
	U3CbroadcastAsyncU3Ec__AnonStorey0_t2750169820::get_offset_of_U24this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3339 = { sizeof (U3CbroadcastAsyncU3Ec__AnonStorey1_t1184085879), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3339[4] = 
{
	U3CbroadcastAsyncU3Ec__AnonStorey1_t1184085879::get_offset_of_opcode_0(),
	U3CbroadcastAsyncU3Ec__AnonStorey1_t1184085879::get_offset_of_stream_1(),
	U3CbroadcastAsyncU3Ec__AnonStorey1_t1184085879::get_offset_of_completed_2(),
	U3CbroadcastAsyncU3Ec__AnonStorey1_t1184085879::get_offset_of_U24this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3340 = { sizeof (U3CBroadcastAsyncU3Ec__AnonStorey2_t177987654), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3340[3] = 
{
	U3CBroadcastAsyncU3Ec__AnonStorey2_t177987654::get_offset_of_length_0(),
	U3CBroadcastAsyncU3Ec__AnonStorey2_t177987654::get_offset_of_completed_1(),
	U3CBroadcastAsyncU3Ec__AnonStorey2_t177987654::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3341 = { sizeof (WebSocketSessionManager_t2802512518), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3341[9] = 
{
	WebSocketSessionManager_t2802512518::get_offset_of__clean_0(),
	WebSocketSessionManager_t2802512518::get_offset_of__forSweep_1(),
	WebSocketSessionManager_t2802512518::get_offset_of__logger_2(),
	WebSocketSessionManager_t2802512518::get_offset_of__sessions_3(),
	WebSocketSessionManager_t2802512518::get_offset_of__state_4(),
	WebSocketSessionManager_t2802512518::get_offset_of__sweeping_5(),
	WebSocketSessionManager_t2802512518::get_offset_of__sweepTimer_6(),
	WebSocketSessionManager_t2802512518::get_offset_of__sync_7(),
	WebSocketSessionManager_t2802512518::get_offset_of__waitTime_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3342 = { sizeof (U3CU3Ec__Iterator0_t2515838856), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3342[6] = 
{
	U3CU3Ec__Iterator0_t2515838856::get_offset_of_U24locvar0_0(),
	U3CU3Ec__Iterator0_t2515838856::get_offset_of_U3CresU3E__1_1(),
	U3CU3Ec__Iterator0_t2515838856::get_offset_of_U24this_2(),
	U3CU3Ec__Iterator0_t2515838856::get_offset_of_U24current_3(),
	U3CU3Ec__Iterator0_t2515838856::get_offset_of_U24disposing_4(),
	U3CU3Ec__Iterator0_t2515838856::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3343 = { sizeof (U3CU3Ec__Iterator1_t949754915), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3343[6] = 
{
	U3CU3Ec__Iterator1_t949754915::get_offset_of_U24locvar0_0(),
	U3CU3Ec__Iterator1_t949754915::get_offset_of_U3CresU3E__1_1(),
	U3CU3Ec__Iterator1_t949754915::get_offset_of_U24this_2(),
	U3CU3Ec__Iterator1_t949754915::get_offset_of_U24current_3(),
	U3CU3Ec__Iterator1_t949754915::get_offset_of_U24disposing_4(),
	U3CU3Ec__Iterator1_t949754915::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3344 = { sizeof (U3CbroadcastAsyncU3Ec__AnonStorey2_t2856551035), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3344[4] = 
{
	U3CbroadcastAsyncU3Ec__AnonStorey2_t2856551035::get_offset_of_opcode_0(),
	U3CbroadcastAsyncU3Ec__AnonStorey2_t2856551035::get_offset_of_data_1(),
	U3CbroadcastAsyncU3Ec__AnonStorey2_t2856551035::get_offset_of_completed_2(),
	U3CbroadcastAsyncU3Ec__AnonStorey2_t2856551035::get_offset_of_U24this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3345 = { sizeof (U3CbroadcastAsyncU3Ec__AnonStorey3_t1290467094), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3345[4] = 
{
	U3CbroadcastAsyncU3Ec__AnonStorey3_t1290467094::get_offset_of_opcode_0(),
	U3CbroadcastAsyncU3Ec__AnonStorey3_t1290467094::get_offset_of_stream_1(),
	U3CbroadcastAsyncU3Ec__AnonStorey3_t1290467094::get_offset_of_completed_2(),
	U3CbroadcastAsyncU3Ec__AnonStorey3_t1290467094::get_offset_of_U24this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3346 = { sizeof (U3CBroadcastAsyncU3Ec__AnonStorey4_t824600657), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3346[3] = 
{
	U3CBroadcastAsyncU3Ec__AnonStorey4_t824600657::get_offset_of_length_0(),
	U3CBroadcastAsyncU3Ec__AnonStorey4_t824600657::get_offset_of_completed_1(),
	U3CBroadcastAsyncU3Ec__AnonStorey4_t824600657::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3347 = { sizeof (WebSocket_t3268376029), -1, sizeof(WebSocket_t3268376029_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3347[52] = 
{
	WebSocket_t3268376029::get_offset_of__authChallenge_0(),
	WebSocket_t3268376029::get_offset_of__base64Key_1(),
	WebSocket_t3268376029::get_offset_of__client_2(),
	WebSocket_t3268376029::get_offset_of__closeContext_3(),
	WebSocket_t3268376029::get_offset_of__compression_4(),
	WebSocket_t3268376029::get_offset_of__context_5(),
	WebSocket_t3268376029::get_offset_of__cookies_6(),
	WebSocket_t3268376029::get_offset_of__credentials_7(),
	WebSocket_t3268376029::get_offset_of__emitOnPing_8(),
	WebSocket_t3268376029::get_offset_of__enableRedirection_9(),
	WebSocket_t3268376029::get_offset_of__exitReceiving_10(),
	WebSocket_t3268376029::get_offset_of__extensions_11(),
	WebSocket_t3268376029::get_offset_of__extensionsRequested_12(),
	WebSocket_t3268376029::get_offset_of__forConn_13(),
	WebSocket_t3268376029::get_offset_of__forMessageEventQueue_14(),
	WebSocket_t3268376029::get_offset_of__forSend_15(),
	WebSocket_t3268376029::get_offset_of__fragmentsBuffer_16(),
	WebSocket_t3268376029::get_offset_of__fragmentsCompressed_17(),
	WebSocket_t3268376029::get_offset_of__fragmentsOpcode_18(),
	0,
	WebSocket_t3268376029::get_offset_of__handshakeRequestChecker_20(),
	WebSocket_t3268376029::get_offset_of__ignoreExtensions_21(),
	WebSocket_t3268376029::get_offset_of__inContinuation_22(),
	WebSocket_t3268376029::get_offset_of__inMessage_23(),
	WebSocket_t3268376029::get_offset_of__logger_24(),
	WebSocket_t3268376029::get_offset_of__message_25(),
	WebSocket_t3268376029::get_offset_of__messageEventQueue_26(),
	WebSocket_t3268376029::get_offset_of__nonceCount_27(),
	WebSocket_t3268376029::get_offset_of__origin_28(),
	WebSocket_t3268376029::get_offset_of__preAuth_29(),
	WebSocket_t3268376029::get_offset_of__protocol_30(),
	WebSocket_t3268376029::get_offset_of__protocols_31(),
	WebSocket_t3268376029::get_offset_of__protocolsRequested_32(),
	WebSocket_t3268376029::get_offset_of__proxyCredentials_33(),
	WebSocket_t3268376029::get_offset_of__proxyUri_34(),
	WebSocket_t3268376029::get_offset_of__readyState_35(),
	WebSocket_t3268376029::get_offset_of__receivePong_36(),
	WebSocket_t3268376029::get_offset_of__secure_37(),
	WebSocket_t3268376029::get_offset_of__sslConfig_38(),
	WebSocket_t3268376029::get_offset_of__stream_39(),
	WebSocket_t3268376029::get_offset_of__tcpClient_40(),
	WebSocket_t3268376029::get_offset_of__uri_41(),
	0,
	WebSocket_t3268376029::get_offset_of__waitTime_43(),
	WebSocket_t3268376029_StaticFields::get_offset_of_EmptyBytes_44(),
	WebSocket_t3268376029_StaticFields::get_offset_of_FragmentLength_45(),
	WebSocket_t3268376029_StaticFields::get_offset_of_RandomNumber_46(),
	WebSocket_t3268376029::get_offset_of_U3CHeadersU3Ek__BackingField_47(),
	WebSocket_t3268376029::get_offset_of_OnClose_48(),
	WebSocket_t3268376029::get_offset_of_OnError_49(),
	WebSocket_t3268376029::get_offset_of_OnMessage_50(),
	WebSocket_t3268376029::get_offset_of_OnOpen_51(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3348 = { sizeof (U3CU3Ec__Iterator0_t147269308), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3348[8] = 
{
	U3CU3Ec__Iterator0_t147269308::get_offset_of_U24locvar0_0(),
	U3CU3Ec__Iterator0_t147269308::get_offset_of_U24locvar1_1(),
	U3CU3Ec__Iterator0_t147269308::get_offset_of_U3CcookieU3E__1_2(),
	U3CU3Ec__Iterator0_t147269308::get_offset_of_U24locvar2_3(),
	U3CU3Ec__Iterator0_t147269308::get_offset_of_U24this_4(),
	U3CU3Ec__Iterator0_t147269308::get_offset_of_U24current_5(),
	U3CU3Ec__Iterator0_t147269308::get_offset_of_U24disposing_6(),
	U3CU3Ec__Iterator0_t147269308::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3349 = { sizeof (U3CcloseAsyncU3Ec__AnonStorey1_t144572335), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3349[1] = 
{
	U3CcloseAsyncU3Ec__AnonStorey1_t144572335::get_offset_of_closer_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3350 = { sizeof (U3CmessagesU3Ec__AnonStorey2_t2960445386), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3350[2] = 
{
	U3CmessagesU3Ec__AnonStorey2_t2960445386::get_offset_of_e_0(),
	U3CmessagesU3Ec__AnonStorey2_t2960445386::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3351 = { sizeof (U3CsendAsyncU3Ec__AnonStorey3_t2886884107), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3351[3] = 
{
	U3CsendAsyncU3Ec__AnonStorey3_t2886884107::get_offset_of_sender_0(),
	U3CsendAsyncU3Ec__AnonStorey3_t2886884107::get_offset_of_completed_1(),
	U3CsendAsyncU3Ec__AnonStorey3_t2886884107::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3352 = { sizeof (U3CstartReceivingU3Ec__AnonStorey4_t506990866), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3352[2] = 
{
	U3CstartReceivingU3Ec__AnonStorey4_t506990866::get_offset_of_receive_0(),
	U3CstartReceivingU3Ec__AnonStorey4_t506990866::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3353 = { sizeof (U3CvalidateSecWebSocketExtensionsServerHeaderU3Ec__AnonStorey5_t2469772577), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3353[1] = 
{
	U3CvalidateSecWebSocketExtensionsServerHeaderU3Ec__AnonStorey5_t2469772577::get_offset_of_method_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3354 = { sizeof (U3CvalidateSecWebSocketProtocolServerHeaderU3Ec__AnonStorey6_t1655596450), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3354[1] = 
{
	U3CvalidateSecWebSocketProtocolServerHeaderU3Ec__AnonStorey6_t1655596450::get_offset_of_value_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3355 = { sizeof (U3CAcceptAsyncU3Ec__AnonStorey7_t3474718123), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3355[2] = 
{
	U3CAcceptAsyncU3Ec__AnonStorey7_t3474718123::get_offset_of_acceptor_0(),
	U3CAcceptAsyncU3Ec__AnonStorey7_t3474718123::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3356 = { sizeof (U3CConnectAsyncU3Ec__AnonStorey8_t2398335890), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3356[2] = 
{
	U3CConnectAsyncU3Ec__AnonStorey8_t2398335890::get_offset_of_connector_0(),
	U3CConnectAsyncU3Ec__AnonStorey8_t2398335890::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3357 = { sizeof (U3CSendAsyncU3Ec__AnonStorey9_t1216469897), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3357[3] = 
{
	U3CSendAsyncU3Ec__AnonStorey9_t1216469897::get_offset_of_length_0(),
	U3CSendAsyncU3Ec__AnonStorey9_t1216469897::get_offset_of_completed_1(),
	U3CSendAsyncU3Ec__AnonStorey9_t1216469897::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3358 = { sizeof (WebSocketException_t1348391352), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3358[1] = 
{
	WebSocketException_t1348391352::get_offset_of__code_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3359 = { sizeof (WebSocketFrame_t764750278), -1, sizeof(WebSocketFrame_t764750278_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3359[11] = 
{
	WebSocketFrame_t764750278::get_offset_of__extPayloadLength_0(),
	WebSocketFrame_t764750278::get_offset_of__fin_1(),
	WebSocketFrame_t764750278::get_offset_of__mask_2(),
	WebSocketFrame_t764750278::get_offset_of__maskingKey_3(),
	WebSocketFrame_t764750278::get_offset_of__opcode_4(),
	WebSocketFrame_t764750278::get_offset_of__payloadData_5(),
	WebSocketFrame_t764750278::get_offset_of__payloadLength_6(),
	WebSocketFrame_t764750278::get_offset_of__rsv1_7(),
	WebSocketFrame_t764750278::get_offset_of__rsv2_8(),
	WebSocketFrame_t764750278::get_offset_of__rsv3_9(),
	WebSocketFrame_t764750278_StaticFields::get_offset_of_EmptyPingBytes_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3360 = { sizeof (U3CdumpU3Ec__AnonStorey1_t3897361314), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3360[2] = 
{
	U3CdumpU3Ec__AnonStorey1_t3897361314::get_offset_of_output_0(),
	U3CdumpU3Ec__AnonStorey1_t3897361314::get_offset_of_lineFmt_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3361 = { sizeof (U3CdumpU3Ec__AnonStorey2_t1475667190), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3361[2] = 
{
	U3CdumpU3Ec__AnonStorey2_t1475667190::get_offset_of_lineCnt_0(),
	U3CdumpU3Ec__AnonStorey2_t1475667190::get_offset_of_U3CU3Ef__refU241_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3362 = { sizeof (U3CreadExtendedPayloadLengthAsyncU3Ec__AnonStorey3_t1521887925), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3362[3] = 
{
	U3CreadExtendedPayloadLengthAsyncU3Ec__AnonStorey3_t1521887925::get_offset_of_len_0(),
	U3CreadExtendedPayloadLengthAsyncU3Ec__AnonStorey3_t1521887925::get_offset_of_frame_1(),
	U3CreadExtendedPayloadLengthAsyncU3Ec__AnonStorey3_t1521887925::get_offset_of_completed_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3363 = { sizeof (U3CreadHeaderAsyncU3Ec__AnonStorey4_t1882858074), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3363[1] = 
{
	U3CreadHeaderAsyncU3Ec__AnonStorey4_t1882858074::get_offset_of_completed_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3364 = { sizeof (U3CreadMaskingKeyAsyncU3Ec__AnonStorey5_t3451788521), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3364[3] = 
{
	U3CreadMaskingKeyAsyncU3Ec__AnonStorey5_t3451788521::get_offset_of_len_0(),
	U3CreadMaskingKeyAsyncU3Ec__AnonStorey5_t3451788521::get_offset_of_frame_1(),
	U3CreadMaskingKeyAsyncU3Ec__AnonStorey5_t3451788521::get_offset_of_completed_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3365 = { sizeof (U3CreadPayloadDataAsyncU3Ec__AnonStorey6_t983838767), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3365[3] = 
{
	U3CreadPayloadDataAsyncU3Ec__AnonStorey6_t983838767::get_offset_of_llen_0(),
	U3CreadPayloadDataAsyncU3Ec__AnonStorey6_t983838767::get_offset_of_frame_1(),
	U3CreadPayloadDataAsyncU3Ec__AnonStorey6_t983838767::get_offset_of_completed_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3366 = { sizeof (U3CReadFrameAsyncU3Ec__AnonStorey7_t8184881), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3366[4] = 
{
	U3CReadFrameAsyncU3Ec__AnonStorey7_t8184881::get_offset_of_stream_0(),
	U3CReadFrameAsyncU3Ec__AnonStorey7_t8184881::get_offset_of_error_1(),
	U3CReadFrameAsyncU3Ec__AnonStorey7_t8184881::get_offset_of_unmask_2(),
	U3CReadFrameAsyncU3Ec__AnonStorey7_t8184881::get_offset_of_completed_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3367 = { sizeof (U3CGetEnumeratorU3Ec__Iterator0_t908706979), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3367[7] = 
{
	U3CGetEnumeratorU3Ec__Iterator0_t908706979::get_offset_of_U24locvar0_0(),
	U3CGetEnumeratorU3Ec__Iterator0_t908706979::get_offset_of_U24locvar1_1(),
	U3CGetEnumeratorU3Ec__Iterator0_t908706979::get_offset_of_U3CbU3E__1_2(),
	U3CGetEnumeratorU3Ec__Iterator0_t908706979::get_offset_of_U24this_3(),
	U3CGetEnumeratorU3Ec__Iterator0_t908706979::get_offset_of_U24current_4(),
	U3CGetEnumeratorU3Ec__Iterator0_t908706979::get_offset_of_U24disposing_5(),
	U3CGetEnumeratorU3Ec__Iterator0_t908706979::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3368 = { sizeof (WebSocketState_t2935910988)+ sizeof (RuntimeObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3368[5] = 
{
	WebSocketState_t2935910988::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3369 = { sizeof (TravisIntegrationTests_t539661000), -1, sizeof(TravisIntegrationTests_t539661000_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3369[1] = 
{
	TravisIntegrationTests_t539661000_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3370 = { sizeof (MouseOrbitImproved_t3290280027), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3370[11] = 
{
	MouseOrbitImproved_t3290280027::get_offset_of_target_2(),
	MouseOrbitImproved_t3290280027::get_offset_of_distance_3(),
	MouseOrbitImproved_t3290280027::get_offset_of_xSpeed_4(),
	MouseOrbitImproved_t3290280027::get_offset_of_ySpeed_5(),
	MouseOrbitImproved_t3290280027::get_offset_of_yMinLimit_6(),
	MouseOrbitImproved_t3290280027::get_offset_of_yMaxLimit_7(),
	MouseOrbitImproved_t3290280027::get_offset_of_distanceMin_8(),
	MouseOrbitImproved_t3290280027::get_offset_of_distanceMax_9(),
	MouseOrbitImproved_t3290280027::get_offset_of__rigidbody_10(),
	MouseOrbitImproved_t3290280027::get_offset_of_x_11(),
	MouseOrbitImproved_t3290280027::get_offset_of_y_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3371 = { sizeof (RandomMovementBehaviour_t3593270701), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3371[3] = 
{
	RandomMovementBehaviour_t3593270701::get_offset_of_bounds_2(),
	RandomMovementBehaviour_t3593270701::get_offset_of_speed_3(),
	RandomMovementBehaviour_t3593270701::get_offset_of_nextWaypoint_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3372 = { sizeof (SayRandomThingsBehaviour_t3866205118), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3372[5] = 
{
	SayRandomThingsBehaviour_t3866205118::get_offset_of_thingsToSay_2(),
	SayRandomThingsBehaviour_t3866205118::get_offset_of_mouth_3(),
	SayRandomThingsBehaviour_t3866205118::get_offset_of_timeBetweenSpeak_4(),
	SayRandomThingsBehaviour_t3866205118::get_offset_of_doTalkOnYourOwn_5(),
	SayRandomThingsBehaviour_t3866205118::get_offset_of_timeToNextSpeak_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3373 = { sizeof (ScriptedDialogueBehaviour_t2807853473), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3373[3] = 
{
	ScriptedDialogueBehaviour_t2807853473::get_offset_of_script_2(),
	ScriptedDialogueBehaviour_t2807853473::get_offset_of_doRestartAtEnd_3(),
	ScriptedDialogueBehaviour_t2807853473::get_offset_of_bubbleTimeToLive_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3374 = { sizeof (DialogueLine_t768215248), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3374[4] = 
{
	DialogueLine_t768215248::get_offset_of_speaker_0(),
	DialogueLine_t768215248::get_offset_of_delay_1(),
	DialogueLine_t768215248::get_offset_of_line_2(),
	DialogueLine_t768215248::get_offset_of_speechBubbleType_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3375 = { sizeof (U3CFollowScriptU3Ec__Iterator0_t3449319141), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3375[5] = 
{
	U3CFollowScriptU3Ec__Iterator0_t3449319141::get_offset_of_U3CindexU3E__0_0(),
	U3CFollowScriptU3Ec__Iterator0_t3449319141::get_offset_of_U24this_1(),
	U3CFollowScriptU3Ec__Iterator0_t3449319141::get_offset_of_U24current_2(),
	U3CFollowScriptU3Ec__Iterator0_t3449319141::get_offset_of_U24disposing_3(),
	U3CFollowScriptU3Ec__Iterator0_t3449319141::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3376 = { sizeof (UISpeechController_t1819804546), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3376[3] = 
{
	UISpeechController_t1819804546::get_offset_of_txtMessage_2(),
	UISpeechController_t1819804546::get_offset_of_talkBehaviour_3(),
	UISpeechController_t1819804546::get_offset_of_toggles_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3377 = { sizeof (RatioLayoutFitter_t2199399473), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3377[3] = 
{
	RatioLayoutFitter_t2199399473::get_offset_of_startPad_15(),
	RatioLayoutFitter_t2199399473::get_offset_of_stopPad_16(),
	RatioLayoutFitter_t2199399473::get_offset_of_childToFit_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3378 = { sizeof (SpeechbubbleBehaviour_t3020520789), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3378[5] = 
{
	SpeechbubbleBehaviour_t3020520789::get_offset_of_timeToLive_2(),
	SpeechbubbleBehaviour_t3020520789::get_offset_of_objectToFollow_3(),
	SpeechbubbleBehaviour_t3020520789::get_offset_of_offset_4(),
	SpeechbubbleBehaviour_t3020520789::get_offset_of_image_5(),
	SpeechbubbleBehaviour_t3020520789::get_offset_of_text_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3379 = { sizeof (SpeechbubbleManager_t3871578363), -1, sizeof(SpeechbubbleManager_t3871578363_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3379[8] = 
{
	SpeechbubbleManager_t3871578363::get_offset_of_defaultColor_2(),
	SpeechbubbleManager_t3871578363::get_offset_of_defaultTimeToLive_3(),
	SpeechbubbleManager_t3871578363::get_offset_of_is2D_4(),
	SpeechbubbleManager_t3871578363::get_offset_of_sizeMultiplier_5(),
	SpeechbubbleManager_t3871578363::get_offset_of_prefabs_6(),
	SpeechbubbleManager_t3871578363::get_offset_of_prefabsDict_7(),
	SpeechbubbleManager_t3871578363_StaticFields::get_offset_of_instance_8(),
	SpeechbubbleManager_t3871578363::get_offset_of_speechbubbleQueue_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3380 = { sizeof (SpeechbubbleType_t1311649088)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3380[5] = 
{
	SpeechbubbleType_t1311649088::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3381 = { sizeof (SpeechbubblePrefab_t736279730), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3381[2] = 
{
	SpeechbubblePrefab_t736279730::get_offset_of_type_0(),
	SpeechbubblePrefab_t736279730::get_offset_of_prefab_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3382 = { sizeof (WorldComponent_t24747827), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3383 = { sizeof (WorldController_t1328477704), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3383[1] = 
{
	WorldController_t1328477704::get_offset_of_anime_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3384 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305144), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305144_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3384[3] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305144_StaticFields::get_offset_of_U24fieldU2D4EEB46F720594D7409B9A8E1B6275694A0C4491D_0(),
	U3CPrivateImplementationDetailsU3E_t1486305144_StaticFields::get_offset_of_U24fieldU2DEB8077FF0D4F4A927EB9224048538295DEF1227A_1(),
	U3CPrivateImplementationDetailsU3E_t1486305144_StaticFields::get_offset_of_U24fieldU2D6532575F81314AADD4BCFD96F1517D5BBFF2B60B_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3385 = { sizeof (U24ArrayTypeU3D8_t1459944466)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D8_t1459944466 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3386 = { sizeof (U24ArrayTypeU3D16_t3894236545)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D16_t3894236545 ), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
