﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.ConversationExperimental
struct ConversationExperimental_t215658501;
// System.String
struct String_t;
// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Words
struct Words_t4062187109;
// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Word
struct Word_t3204175642;
// System.Collections.Generic.List`1<IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Word>
struct List_1_t2573296774;
// IBM.Watson.DeveloperCloud.UnitTests.TestSpeechToText
struct TestSpeechToText_t3601723574;
// IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery
struct TestDiscovery_t1320997924;
// IBM.Watson.DeveloperCloud.Services.Conversation.v1.MessageRequest
struct MessageRequest_t1934991468;
// TestConversation
struct TestConversation_t1577552265;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Word[]
struct WordU5BU5D_t3686373631;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomVoiceUpdate
struct CustomVoiceUpdate_t1706248840;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Words
struct Words_t2948214629;
// IBM.Watson.DeveloperCloud.UnitTests.TestTextToSpeech
struct TestTextToSpeech_t3466033430;
// IBM.Watson.DeveloperCloud.UnitTests.TestToneAnalyzer
struct TestToneAnalyzer_t1700654784;
// IBM.Watson.DeveloperCloud.UnitTests.TestPersonalityInsightsV3
struct TestPersonalityInsightsV3_t1756789132;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;
// IBM.Watson.DeveloperCloud.UnitTests.TestAlchemyAPI
struct TestAlchemyAPI_t3509857629;
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition
struct VisualRecognition_t3119563755;
// IBM.Watson.DeveloperCloud.Services.ServiceStatus
struct ServiceStatus_t1443707987;
// IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.LanguageTranslator
struct LanguageTranslator_t2981742234;
// IBM.Watson.DeveloperCloud.UnitTests.TestLanguageTranslation
struct TestLanguageTranslation_t1772800819;
// IBM.Watson.DeveloperCloud.UnitTests.TestRunnable
struct TestRunnable_t2118330963;
// IBM.Watson.DeveloperCloud.UnitTests.TestPersonalityInsightsV2
struct TestPersonalityInsightsV2_t1756789131;
// IBM.Watson.DeveloperCloud.UnitTests.TestRetrieveAndRank
struct TestRetrieveAndRank_t2134779753;
// IBM.Watson.DeveloperCloud.UnitTests.TestNaturalLanguageClassifier
struct TestNaturalLanguageClassifier_t1226850908;
// IBM.Watson.DeveloperCloud.UnitTests.TestEventManager
struct TestEventManager_t1784053145;
// IBM.Watson.DeveloperCloud.UnitTests.TestDocumentConversion
struct TestDocumentConversion_t3132176613;
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Problem
struct Problem_t2814813345;
// System.Collections.Generic.List`1<IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column>
struct List_1_t1981607956;
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column
struct Column_t2612486824;
// System.Collections.Generic.List`1<IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Option>
struct List_1_t1144248177;
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Option
struct Option_t1775127045;
// IBM.Watson.DeveloperCloud.UnitTests.TestTradeoffAnalytics
struct TestTradeoffAnalytics_t1911417121;
// UnityEngine.WWW
struct WWW_t2919945039;
// IBM.Watson.DeveloperCloud.Utilities.Config
struct Config_t3637807320;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Collections.Generic.Dictionary`2<System.String,IBM.Watson.DeveloperCloud.Connection.RESTConnector/Form>
struct Dictionary_2_t2694055125;
// IBM.Watson.DeveloperCloud.Connection.RESTConnector/ResponseEvent
struct ResponseEvent_t1616568356;
// IBM.Watson.DeveloperCloud.Connection.RESTConnector/ProgressEvent
struct ProgressEvent_t4185145044;
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/OnFindClassifier
struct OnFindClassifier_t2896727766;
// System.Collections.Generic.List`1<IBM.Watson.DeveloperCloud.Utilities.Config/CredentialInfo>
struct List_1_t3698242869;
// System.Collections.Generic.List`1<IBM.Watson.DeveloperCloud.Utilities.Config/Variable>
struct List_1_t4101323382;
// FullSerializer.fsSerializer
struct fsSerializer_t4193731081;
// IBM.Watson.DeveloperCloud.Utilities.EventManager
struct EventManager_t605335149;
// IBM.Watson.DeveloperCloud.UnitTests.TestVisualRecognition
struct TestVisualRecognition_t3349626417;
// System.Collections.Generic.Dictionary`2<System.String,IBM.Watson.DeveloperCloud.Utilities.DataCache/CacheItem>
struct Dictionary_2_t92195697;
// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.Dictionary`2<System.Object,System.String>>
struct Dictionary_2_t3558638258;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<IBM.Watson.DeveloperCloud.Utilities.EventManager/OnReceiveEvent>>
struct Dictionary_2_t1226040769;
// System.Collections.Generic.Queue`1<IBM.Watson.DeveloperCloud.Utilities.EventManager/AsyncEvent>
struct Queue_1_t2152331082;
// IBM.Watson.DeveloperCloud.Editor.UnitTestManager/OnTestsComplete
struct OnTestsComplete_t1412473303;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank
struct RetrieveAndRank_t1381045327;
// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics
struct TradeoffAnalytics_t100002595;
// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzer
struct ToneAnalyzer_t1356110496;
// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText
struct SpeechToText_t2713896346;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech
struct TextToSpeech_t3349357562;
// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Word
struct Word_t4274554970;
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/OnTrainClassifier
struct OnTrainClassifier_t294819893;
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/OnDeleteClassifier
struct OnDeleteClassifier_t917631666;
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/OnGetClassifiers
struct OnGetClassifiers_t226395526;
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/OnGetClassifier
struct OnGetClassifier_t713440359;
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/OnDeleteCollection
struct OnDeleteCollection_t1354946561;
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/OnGetCollection
struct OnGetCollection_t621465616;
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/OnGetCollections
struct OnGetCollections_t484200113;
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/OnCreateCollection
struct OnCreateCollection_t970139976;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.Void
struct Void_t1841601450;
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/OnDetectFaces
struct OnDetectFaces_t4020255763;
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/OnRecognizeText
struct OnRecognizeText_t793159563;
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/OnClassify
struct OnClassify_t311067478;
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/OnGetCollectionImages
struct OnGetCollectionImages_t2736783742;
// IBM.Watson.DeveloperCloud.Services.DocumentConversion.v1.DocumentConversion
struct DocumentConversion_t1610681923;
// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery
struct Discovery_t1478121420;
// IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.LanguageTranslation
struct LanguageTranslation_t3099415401;
// IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v3.PersonalityInsights
struct PersonalityInsights_t2503525937;
// IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v2.PersonalityInsights
struct PersonalityInsights_t582046669;
// IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.NaturalLanguageClassifier
struct NaturalLanguageClassifier_t2492034444;
// IBM.Watson.DeveloperCloud.Services.Conversation.v1.Conversation
struct Conversation_t105466997;
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/OnGetImageDetails
struct OnGetImageDetails_t1270200205;
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/OnDeleteCollectionImage
struct OnDeleteCollectionImage_t241020072;
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/OnAddCollectionImage
struct OnAddCollectionImage_t1084877344;
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/OnDeleteImageMetadata
struct OnDeleteImageMetadata_t2373116469;
// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI
struct AlchemyAPI_t2955839919;
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/OnFindSimilar
struct OnFindSimilar_t219840828;
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/OnGetImageMetadata
struct OnGetImageMetadata_t54004548;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1572802995;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.GetCollectionImages
struct GetCollectionImages_t1669456110;
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.CollectionsConfig
struct CollectionsConfig_t2493163663;
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.CreateCollection
struct CreateCollection_t3312638156;
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.GetCollections
struct GetCollections_t2586322795;
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.TextRecogTopLevelMultiple
struct TextRecogTopLevelMultiple_t1284479682;
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.GetCollectionsBrief
struct GetCollectionsBrief_t2166116487;
// System.Type
struct Type_t;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// IBM.Watson.DeveloperCloud.Editor.UnitTestManager
struct UnitTestManager_t2486317649;
// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.SimilarImagesConfig
struct SimilarImagesConfig_t2576720653;
// System.Collections.Generic.Dictionary`2<System.Int32,System.String>
struct Dictionary_2_t1037045868;
// UnityEngine.UI.Text
struct Text_t356221433;
// System.Collections.Generic.Queue`1<System.Type>
struct Queue_1_t1123460061;
// System.Type[]
struct TypeU5BU5D_t1664964607;
// IBM.Watson.DeveloperCloud.UnitTests.UnitTest
struct UnitTest_t237671048;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef TESTCONVERSATIONEXPERIMENTAL_T3721563105_H
#define TESTCONVERSATIONEXPERIMENTAL_T3721563105_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TestConversationExperimental
struct  TestConversationExperimental_t3721563105  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.ConversationExperimental.v1.ConversationExperimental TestConversationExperimental::m_Conversation
	ConversationExperimental_t215658501 * ___m_Conversation_0;
	// System.String TestConversationExperimental::m_WorkspaceID
	String_t* ___m_WorkspaceID_1;
	// System.String TestConversationExperimental::m_Input
	String_t* ___m_Input_2;
	// System.Boolean TestConversationExperimental::m_MessageTested
	bool ___m_MessageTested_3;

public:
	inline static int32_t get_offset_of_m_Conversation_0() { return static_cast<int32_t>(offsetof(TestConversationExperimental_t3721563105, ___m_Conversation_0)); }
	inline ConversationExperimental_t215658501 * get_m_Conversation_0() const { return ___m_Conversation_0; }
	inline ConversationExperimental_t215658501 ** get_address_of_m_Conversation_0() { return &___m_Conversation_0; }
	inline void set_m_Conversation_0(ConversationExperimental_t215658501 * value)
	{
		___m_Conversation_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Conversation_0), value);
	}

	inline static int32_t get_offset_of_m_WorkspaceID_1() { return static_cast<int32_t>(offsetof(TestConversationExperimental_t3721563105, ___m_WorkspaceID_1)); }
	inline String_t* get_m_WorkspaceID_1() const { return ___m_WorkspaceID_1; }
	inline String_t** get_address_of_m_WorkspaceID_1() { return &___m_WorkspaceID_1; }
	inline void set_m_WorkspaceID_1(String_t* value)
	{
		___m_WorkspaceID_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_WorkspaceID_1), value);
	}

	inline static int32_t get_offset_of_m_Input_2() { return static_cast<int32_t>(offsetof(TestConversationExperimental_t3721563105, ___m_Input_2)); }
	inline String_t* get_m_Input_2() const { return ___m_Input_2; }
	inline String_t** get_address_of_m_Input_2() { return &___m_Input_2; }
	inline void set_m_Input_2(String_t* value)
	{
		___m_Input_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Input_2), value);
	}

	inline static int32_t get_offset_of_m_MessageTested_3() { return static_cast<int32_t>(offsetof(TestConversationExperimental_t3721563105, ___m_MessageTested_3)); }
	inline bool get_m_MessageTested_3() const { return ___m_MessageTested_3; }
	inline bool* get_address_of_m_MessageTested_3() { return &___m_MessageTested_3; }
	inline void set_m_MessageTested_3(bool value)
	{
		___m_MessageTested_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TESTCONVERSATIONEXPERIMENTAL_T3721563105_H
#ifndef U3CRUNTESTU3EC__ITERATOR0_T1033965803_H
#define U3CRUNTESTU3EC__ITERATOR0_T1033965803_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.UnitTests.TestSpeechToText/<RunTest>c__Iterator0
struct  U3CRunTestU3Ec__Iterator0_t1033965803  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Words IBM.Watson.DeveloperCloud.UnitTests.TestSpeechToText/<RunTest>c__Iterator0::<words>__0
	Words_t4062187109 * ___U3CwordsU3E__0_0;
	// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Word IBM.Watson.DeveloperCloud.UnitTests.TestSpeechToText/<RunTest>c__Iterator0::<w0>__0
	Word_t3204175642 * ___U3Cw0U3E__0_1;
	// System.Collections.Generic.List`1<IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Word> IBM.Watson.DeveloperCloud.UnitTests.TestSpeechToText/<RunTest>c__Iterator0::<wordList>__0
	List_1_t2573296774 * ___U3CwordListU3E__0_2;
	// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Word IBM.Watson.DeveloperCloud.UnitTests.TestSpeechToText/<RunTest>c__Iterator0::<w1>__0
	Word_t3204175642 * ___U3Cw1U3E__0_3;
	// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.Word IBM.Watson.DeveloperCloud.UnitTests.TestSpeechToText/<RunTest>c__Iterator0::<w2>__0
	Word_t3204175642 * ___U3Cw2U3E__0_4;
	// IBM.Watson.DeveloperCloud.UnitTests.TestSpeechToText IBM.Watson.DeveloperCloud.UnitTests.TestSpeechToText/<RunTest>c__Iterator0::$this
	TestSpeechToText_t3601723574 * ___U24this_5;
	// System.Object IBM.Watson.DeveloperCloud.UnitTests.TestSpeechToText/<RunTest>c__Iterator0::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestSpeechToText/<RunTest>c__Iterator0::$disposing
	bool ___U24disposing_7;
	// System.Int32 IBM.Watson.DeveloperCloud.UnitTests.TestSpeechToText/<RunTest>c__Iterator0::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3CwordsU3E__0_0() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t1033965803, ___U3CwordsU3E__0_0)); }
	inline Words_t4062187109 * get_U3CwordsU3E__0_0() const { return ___U3CwordsU3E__0_0; }
	inline Words_t4062187109 ** get_address_of_U3CwordsU3E__0_0() { return &___U3CwordsU3E__0_0; }
	inline void set_U3CwordsU3E__0_0(Words_t4062187109 * value)
	{
		___U3CwordsU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwordsU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3Cw0U3E__0_1() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t1033965803, ___U3Cw0U3E__0_1)); }
	inline Word_t3204175642 * get_U3Cw0U3E__0_1() const { return ___U3Cw0U3E__0_1; }
	inline Word_t3204175642 ** get_address_of_U3Cw0U3E__0_1() { return &___U3Cw0U3E__0_1; }
	inline void set_U3Cw0U3E__0_1(Word_t3204175642 * value)
	{
		___U3Cw0U3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cw0U3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CwordListU3E__0_2() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t1033965803, ___U3CwordListU3E__0_2)); }
	inline List_1_t2573296774 * get_U3CwordListU3E__0_2() const { return ___U3CwordListU3E__0_2; }
	inline List_1_t2573296774 ** get_address_of_U3CwordListU3E__0_2() { return &___U3CwordListU3E__0_2; }
	inline void set_U3CwordListU3E__0_2(List_1_t2573296774 * value)
	{
		___U3CwordListU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwordListU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3Cw1U3E__0_3() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t1033965803, ___U3Cw1U3E__0_3)); }
	inline Word_t3204175642 * get_U3Cw1U3E__0_3() const { return ___U3Cw1U3E__0_3; }
	inline Word_t3204175642 ** get_address_of_U3Cw1U3E__0_3() { return &___U3Cw1U3E__0_3; }
	inline void set_U3Cw1U3E__0_3(Word_t3204175642 * value)
	{
		___U3Cw1U3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cw1U3E__0_3), value);
	}

	inline static int32_t get_offset_of_U3Cw2U3E__0_4() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t1033965803, ___U3Cw2U3E__0_4)); }
	inline Word_t3204175642 * get_U3Cw2U3E__0_4() const { return ___U3Cw2U3E__0_4; }
	inline Word_t3204175642 ** get_address_of_U3Cw2U3E__0_4() { return &___U3Cw2U3E__0_4; }
	inline void set_U3Cw2U3E__0_4(Word_t3204175642 * value)
	{
		___U3Cw2U3E__0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cw2U3E__0_4), value);
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t1033965803, ___U24this_5)); }
	inline TestSpeechToText_t3601723574 * get_U24this_5() const { return ___U24this_5; }
	inline TestSpeechToText_t3601723574 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(TestSpeechToText_t3601723574 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t1033965803, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t1033965803, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t1033965803, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRUNTESTU3EC__ITERATOR0_T1033965803_H
#ifndef U3CDELETECOLLECTIONU3EC__ITERATOR2_T3345212421_H
#define U3CDELETECOLLECTIONU3EC__ITERATOR2_T3345212421_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery/<DeleteCollection>c__Iterator2
struct  U3CDeleteCollectionU3Ec__Iterator2_t3345212421  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery/<DeleteCollection>c__Iterator2::$this
	TestDiscovery_t1320997924 * ___U24this_0;
	// System.Object IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery/<DeleteCollection>c__Iterator2::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery/<DeleteCollection>c__Iterator2::$disposing
	bool ___U24disposing_2;
	// System.Int32 IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery/<DeleteCollection>c__Iterator2::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CDeleteCollectionU3Ec__Iterator2_t3345212421, ___U24this_0)); }
	inline TestDiscovery_t1320997924 * get_U24this_0() const { return ___U24this_0; }
	inline TestDiscovery_t1320997924 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(TestDiscovery_t1320997924 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CDeleteCollectionU3Ec__Iterator2_t3345212421, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CDeleteCollectionU3Ec__Iterator2_t3345212421, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CDeleteCollectionU3Ec__Iterator2_t3345212421, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDELETECOLLECTIONU3EC__ITERATOR2_T3345212421_H
#ifndef U3CDELETEDOCUMENTU3EC__ITERATOR1_T1606296947_H
#define U3CDELETEDOCUMENTU3EC__ITERATOR1_T1606296947_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery/<DeleteDocument>c__Iterator1
struct  U3CDeleteDocumentU3Ec__Iterator1_t1606296947  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery/<DeleteDocument>c__Iterator1::$this
	TestDiscovery_t1320997924 * ___U24this_0;
	// System.Object IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery/<DeleteDocument>c__Iterator1::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery/<DeleteDocument>c__Iterator1::$disposing
	bool ___U24disposing_2;
	// System.Int32 IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery/<DeleteDocument>c__Iterator1::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CDeleteDocumentU3Ec__Iterator1_t1606296947, ___U24this_0)); }
	inline TestDiscovery_t1320997924 * get_U24this_0() const { return ___U24this_0; }
	inline TestDiscovery_t1320997924 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(TestDiscovery_t1320997924 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CDeleteDocumentU3Ec__Iterator1_t1606296947, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CDeleteDocumentU3Ec__Iterator1_t1606296947, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CDeleteDocumentU3Ec__Iterator1_t1606296947, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDELETEDOCUMENTU3EC__ITERATOR1_T1606296947_H
#ifndef U3CRUNTESTU3EC__ITERATOR0_T1695959971_H
#define U3CRUNTESTU3EC__ITERATOR0_T1695959971_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery/<RunTest>c__Iterator0
struct  U3CRunTestU3Ec__Iterator0_t1695959971  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery/<RunTest>c__Iterator0::$this
	TestDiscovery_t1320997924 * ___U24this_0;
	// System.Object IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery/<RunTest>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery/<RunTest>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery/<RunTest>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t1695959971, ___U24this_0)); }
	inline TestDiscovery_t1320997924 * get_U24this_0() const { return ___U24this_0; }
	inline TestDiscovery_t1320997924 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(TestDiscovery_t1320997924 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t1695959971, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t1695959971, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t1695959971, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRUNTESTU3EC__ITERATOR0_T1695959971_H
#ifndef U3CRUNTESTU3EC__ITERATOR0_T3048207354_H
#define U3CRUNTESTU3EC__ITERATOR0_T3048207354_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TestConversation/<RunTest>c__Iterator0
struct  U3CRunTestU3Ec__Iterator0_t3048207354  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.Conversation.v1.MessageRequest TestConversation/<RunTest>c__Iterator0::<messageRequest>__1
	MessageRequest_t1934991468 * ___U3CmessageRequestU3E__1_0;
	// TestConversation TestConversation/<RunTest>c__Iterator0::$this
	TestConversation_t1577552265 * ___U24this_1;
	// System.Object TestConversation/<RunTest>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean TestConversation/<RunTest>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 TestConversation/<RunTest>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CmessageRequestU3E__1_0() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t3048207354, ___U3CmessageRequestU3E__1_0)); }
	inline MessageRequest_t1934991468 * get_U3CmessageRequestU3E__1_0() const { return ___U3CmessageRequestU3E__1_0; }
	inline MessageRequest_t1934991468 ** get_address_of_U3CmessageRequestU3E__1_0() { return &___U3CmessageRequestU3E__1_0; }
	inline void set_U3CmessageRequestU3E__1_0(MessageRequest_t1934991468 * value)
	{
		___U3CmessageRequestU3E__1_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmessageRequestU3E__1_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t3048207354, ___U24this_1)); }
	inline TestConversation_t1577552265 * get_U24this_1() const { return ___U24this_1; }
	inline TestConversation_t1577552265 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(TestConversation_t1577552265 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t3048207354, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t3048207354, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t3048207354, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRUNTESTU3EC__ITERATOR0_T3048207354_H
#ifndef U3CRUNTESTU3EC__ITERATOR0_T2943339915_H
#define U3CRUNTESTU3EC__ITERATOR0_T2943339915_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.UnitTests.TestTextToSpeech/<RunTest>c__Iterator0
struct  U3CRunTestU3Ec__Iterator0_t2943339915  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestTextToSpeech/<RunTest>c__Iterator0::<testWord>__0
	String_t* ___U3CtestWordU3E__0_0;
	// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Word[] IBM.Watson.DeveloperCloud.UnitTests.TestTextToSpeech/<RunTest>c__Iterator0::<words>__0
	WordU5BU5D_t3686373631* ___U3CwordsU3E__0_1;
	// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.CustomVoiceUpdate IBM.Watson.DeveloperCloud.UnitTests.TestTextToSpeech/<RunTest>c__Iterator0::<customVoiceUpdate>__0
	CustomVoiceUpdate_t1706248840 * ___U3CcustomVoiceUpdateU3E__0_2;
	// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Word[] IBM.Watson.DeveloperCloud.UnitTests.TestTextToSpeech/<RunTest>c__Iterator0::<wordArray>__0
	WordU5BU5D_t3686373631* ___U3CwordArrayU3E__0_3;
	// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Words IBM.Watson.DeveloperCloud.UnitTests.TestTextToSpeech/<RunTest>c__Iterator0::<wordsObject>__0
	Words_t2948214629 * ___U3CwordsObjectU3E__0_4;
	// IBM.Watson.DeveloperCloud.UnitTests.TestTextToSpeech IBM.Watson.DeveloperCloud.UnitTests.TestTextToSpeech/<RunTest>c__Iterator0::$this
	TestTextToSpeech_t3466033430 * ___U24this_5;
	// System.Object IBM.Watson.DeveloperCloud.UnitTests.TestTextToSpeech/<RunTest>c__Iterator0::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestTextToSpeech/<RunTest>c__Iterator0::$disposing
	bool ___U24disposing_7;
	// System.Int32 IBM.Watson.DeveloperCloud.UnitTests.TestTextToSpeech/<RunTest>c__Iterator0::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3CtestWordU3E__0_0() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t2943339915, ___U3CtestWordU3E__0_0)); }
	inline String_t* get_U3CtestWordU3E__0_0() const { return ___U3CtestWordU3E__0_0; }
	inline String_t** get_address_of_U3CtestWordU3E__0_0() { return &___U3CtestWordU3E__0_0; }
	inline void set_U3CtestWordU3E__0_0(String_t* value)
	{
		___U3CtestWordU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtestWordU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CwordsU3E__0_1() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t2943339915, ___U3CwordsU3E__0_1)); }
	inline WordU5BU5D_t3686373631* get_U3CwordsU3E__0_1() const { return ___U3CwordsU3E__0_1; }
	inline WordU5BU5D_t3686373631** get_address_of_U3CwordsU3E__0_1() { return &___U3CwordsU3E__0_1; }
	inline void set_U3CwordsU3E__0_1(WordU5BU5D_t3686373631* value)
	{
		___U3CwordsU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwordsU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CcustomVoiceUpdateU3E__0_2() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t2943339915, ___U3CcustomVoiceUpdateU3E__0_2)); }
	inline CustomVoiceUpdate_t1706248840 * get_U3CcustomVoiceUpdateU3E__0_2() const { return ___U3CcustomVoiceUpdateU3E__0_2; }
	inline CustomVoiceUpdate_t1706248840 ** get_address_of_U3CcustomVoiceUpdateU3E__0_2() { return &___U3CcustomVoiceUpdateU3E__0_2; }
	inline void set_U3CcustomVoiceUpdateU3E__0_2(CustomVoiceUpdate_t1706248840 * value)
	{
		___U3CcustomVoiceUpdateU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcustomVoiceUpdateU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CwordArrayU3E__0_3() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t2943339915, ___U3CwordArrayU3E__0_3)); }
	inline WordU5BU5D_t3686373631* get_U3CwordArrayU3E__0_3() const { return ___U3CwordArrayU3E__0_3; }
	inline WordU5BU5D_t3686373631** get_address_of_U3CwordArrayU3E__0_3() { return &___U3CwordArrayU3E__0_3; }
	inline void set_U3CwordArrayU3E__0_3(WordU5BU5D_t3686373631* value)
	{
		___U3CwordArrayU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwordArrayU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U3CwordsObjectU3E__0_4() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t2943339915, ___U3CwordsObjectU3E__0_4)); }
	inline Words_t2948214629 * get_U3CwordsObjectU3E__0_4() const { return ___U3CwordsObjectU3E__0_4; }
	inline Words_t2948214629 ** get_address_of_U3CwordsObjectU3E__0_4() { return &___U3CwordsObjectU3E__0_4; }
	inline void set_U3CwordsObjectU3E__0_4(Words_t2948214629 * value)
	{
		___U3CwordsObjectU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwordsObjectU3E__0_4), value);
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t2943339915, ___U24this_5)); }
	inline TestTextToSpeech_t3466033430 * get_U24this_5() const { return ___U24this_5; }
	inline TestTextToSpeech_t3466033430 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(TestTextToSpeech_t3466033430 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t2943339915, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t2943339915, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t2943339915, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRUNTESTU3EC__ITERATOR0_T2943339915_H
#ifndef U3CRUNTESTU3EC__ITERATOR0_T464230963_H
#define U3CRUNTESTU3EC__ITERATOR0_T464230963_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.UnitTests.TestToneAnalyzer/<RunTest>c__Iterator0
struct  U3CRunTestU3Ec__Iterator0_t464230963  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.UnitTests.TestToneAnalyzer IBM.Watson.DeveloperCloud.UnitTests.TestToneAnalyzer/<RunTest>c__Iterator0::$this
	TestToneAnalyzer_t1700654784 * ___U24this_0;
	// System.Object IBM.Watson.DeveloperCloud.UnitTests.TestToneAnalyzer/<RunTest>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestToneAnalyzer/<RunTest>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 IBM.Watson.DeveloperCloud.UnitTests.TestToneAnalyzer/<RunTest>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t464230963, ___U24this_0)); }
	inline TestToneAnalyzer_t1700654784 * get_U24this_0() const { return ___U24this_0; }
	inline TestToneAnalyzer_t1700654784 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(TestToneAnalyzer_t1700654784 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t464230963, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t464230963, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t464230963, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRUNTESTU3EC__ITERATOR0_T464230963_H
#ifndef U3CRUNTESTU3EC__ITERATOR0_T1192622081_H
#define U3CRUNTESTU3EC__ITERATOR0_T1192622081_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.UnitTests.TestPersonalityInsightsV3/<RunTest>c__Iterator0
struct  U3CRunTestU3Ec__Iterator0_t1192622081  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestPersonalityInsightsV3/<RunTest>c__Iterator0::<dataPath>__0
	String_t* ___U3CdataPathU3E__0_0;
	// IBM.Watson.DeveloperCloud.UnitTests.TestPersonalityInsightsV3 IBM.Watson.DeveloperCloud.UnitTests.TestPersonalityInsightsV3/<RunTest>c__Iterator0::$this
	TestPersonalityInsightsV3_t1756789132 * ___U24this_1;
	// System.Object IBM.Watson.DeveloperCloud.UnitTests.TestPersonalityInsightsV3/<RunTest>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestPersonalityInsightsV3/<RunTest>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 IBM.Watson.DeveloperCloud.UnitTests.TestPersonalityInsightsV3/<RunTest>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CdataPathU3E__0_0() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t1192622081, ___U3CdataPathU3E__0_0)); }
	inline String_t* get_U3CdataPathU3E__0_0() const { return ___U3CdataPathU3E__0_0; }
	inline String_t** get_address_of_U3CdataPathU3E__0_0() { return &___U3CdataPathU3E__0_0; }
	inline void set_U3CdataPathU3E__0_0(String_t* value)
	{
		___U3CdataPathU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdataPathU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t1192622081, ___U24this_1)); }
	inline TestPersonalityInsightsV3_t1756789132 * get_U24this_1() const { return ___U24this_1; }
	inline TestPersonalityInsightsV3_t1756789132 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(TestPersonalityInsightsV3_t1756789132 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t1192622081, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t1192622081, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t1192622081, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRUNTESTU3EC__ITERATOR0_T1192622081_H
#ifndef U3CCHECKCUSTOMIZATIONSTATUSU3EC__ITERATOR1_T3285635976_H
#define U3CCHECKCUSTOMIZATIONSTATUSU3EC__ITERATOR1_T3285635976_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.UnitTests.TestSpeechToText/<CheckCustomizationStatus>c__Iterator1
struct  U3CCheckCustomizationStatusU3Ec__Iterator1_t3285635976  : public RuntimeObject
{
public:
	// System.Single IBM.Watson.DeveloperCloud.UnitTests.TestSpeechToText/<CheckCustomizationStatus>c__Iterator1::delay
	float ___delay_0;
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestSpeechToText/<CheckCustomizationStatus>c__Iterator1::customizationID
	String_t* ___customizationID_1;
	// IBM.Watson.DeveloperCloud.UnitTests.TestSpeechToText IBM.Watson.DeveloperCloud.UnitTests.TestSpeechToText/<CheckCustomizationStatus>c__Iterator1::$this
	TestSpeechToText_t3601723574 * ___U24this_2;
	// System.Object IBM.Watson.DeveloperCloud.UnitTests.TestSpeechToText/<CheckCustomizationStatus>c__Iterator1::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestSpeechToText/<CheckCustomizationStatus>c__Iterator1::$disposing
	bool ___U24disposing_4;
	// System.Int32 IBM.Watson.DeveloperCloud.UnitTests.TestSpeechToText/<CheckCustomizationStatus>c__Iterator1::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_delay_0() { return static_cast<int32_t>(offsetof(U3CCheckCustomizationStatusU3Ec__Iterator1_t3285635976, ___delay_0)); }
	inline float get_delay_0() const { return ___delay_0; }
	inline float* get_address_of_delay_0() { return &___delay_0; }
	inline void set_delay_0(float value)
	{
		___delay_0 = value;
	}

	inline static int32_t get_offset_of_customizationID_1() { return static_cast<int32_t>(offsetof(U3CCheckCustomizationStatusU3Ec__Iterator1_t3285635976, ___customizationID_1)); }
	inline String_t* get_customizationID_1() const { return ___customizationID_1; }
	inline String_t** get_address_of_customizationID_1() { return &___customizationID_1; }
	inline void set_customizationID_1(String_t* value)
	{
		___customizationID_1 = value;
		Il2CppCodeGenWriteBarrier((&___customizationID_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CCheckCustomizationStatusU3Ec__Iterator1_t3285635976, ___U24this_2)); }
	inline TestSpeechToText_t3601723574 * get_U24this_2() const { return ___U24this_2; }
	inline TestSpeechToText_t3601723574 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(TestSpeechToText_t3601723574 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CCheckCustomizationStatusU3Ec__Iterator1_t3285635976, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CCheckCustomizationStatusU3Ec__Iterator1_t3285635976, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CCheckCustomizationStatusU3Ec__Iterator1_t3285635976, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCHECKCUSTOMIZATIONSTATUSU3EC__ITERATOR1_T3285635976_H
#ifndef U3CRUNTESTU3EC__ITERATOR0_T406948982_H
#define U3CRUNTESTU3EC__ITERATOR0_T406948982_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.UnitTests.TestAlchemyAPI/<RunTest>c__Iterator0
struct  U3CRunTestU3Ec__Iterator0_t406948982  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestAlchemyAPI/<RunTest>c__Iterator0::<example_article_html>__0
	String_t* ___U3Cexample_article_htmlU3E__0_0;
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestAlchemyAPI/<RunTest>c__Iterator0::<example_microformats_html>__0
	String_t* ___U3Cexample_microformats_htmlU3E__0_1;
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestAlchemyAPI/<RunTest>c__Iterator0::<example_feed_html>__0
	String_t* ___U3Cexample_feed_htmlU3E__0_2;
	// System.String[] IBM.Watson.DeveloperCloud.UnitTests.TestAlchemyAPI/<RunTest>c__Iterator0::<returnFields>__0
	StringU5BU5D_t1642385972* ___U3CreturnFieldsU3E__0_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> IBM.Watson.DeveloperCloud.UnitTests.TestAlchemyAPI/<RunTest>c__Iterator0::<queryFields>__0
	Dictionary_2_t3943999495 * ___U3CqueryFieldsU3E__0_4;
	// IBM.Watson.DeveloperCloud.UnitTests.TestAlchemyAPI IBM.Watson.DeveloperCloud.UnitTests.TestAlchemyAPI/<RunTest>c__Iterator0::$this
	TestAlchemyAPI_t3509857629 * ___U24this_5;
	// System.Object IBM.Watson.DeveloperCloud.UnitTests.TestAlchemyAPI/<RunTest>c__Iterator0::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestAlchemyAPI/<RunTest>c__Iterator0::$disposing
	bool ___U24disposing_7;
	// System.Int32 IBM.Watson.DeveloperCloud.UnitTests.TestAlchemyAPI/<RunTest>c__Iterator0::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3Cexample_article_htmlU3E__0_0() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t406948982, ___U3Cexample_article_htmlU3E__0_0)); }
	inline String_t* get_U3Cexample_article_htmlU3E__0_0() const { return ___U3Cexample_article_htmlU3E__0_0; }
	inline String_t** get_address_of_U3Cexample_article_htmlU3E__0_0() { return &___U3Cexample_article_htmlU3E__0_0; }
	inline void set_U3Cexample_article_htmlU3E__0_0(String_t* value)
	{
		___U3Cexample_article_htmlU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cexample_article_htmlU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3Cexample_microformats_htmlU3E__0_1() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t406948982, ___U3Cexample_microformats_htmlU3E__0_1)); }
	inline String_t* get_U3Cexample_microformats_htmlU3E__0_1() const { return ___U3Cexample_microformats_htmlU3E__0_1; }
	inline String_t** get_address_of_U3Cexample_microformats_htmlU3E__0_1() { return &___U3Cexample_microformats_htmlU3E__0_1; }
	inline void set_U3Cexample_microformats_htmlU3E__0_1(String_t* value)
	{
		___U3Cexample_microformats_htmlU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cexample_microformats_htmlU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3Cexample_feed_htmlU3E__0_2() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t406948982, ___U3Cexample_feed_htmlU3E__0_2)); }
	inline String_t* get_U3Cexample_feed_htmlU3E__0_2() const { return ___U3Cexample_feed_htmlU3E__0_2; }
	inline String_t** get_address_of_U3Cexample_feed_htmlU3E__0_2() { return &___U3Cexample_feed_htmlU3E__0_2; }
	inline void set_U3Cexample_feed_htmlU3E__0_2(String_t* value)
	{
		___U3Cexample_feed_htmlU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cexample_feed_htmlU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CreturnFieldsU3E__0_3() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t406948982, ___U3CreturnFieldsU3E__0_3)); }
	inline StringU5BU5D_t1642385972* get_U3CreturnFieldsU3E__0_3() const { return ___U3CreturnFieldsU3E__0_3; }
	inline StringU5BU5D_t1642385972** get_address_of_U3CreturnFieldsU3E__0_3() { return &___U3CreturnFieldsU3E__0_3; }
	inline void set_U3CreturnFieldsU3E__0_3(StringU5BU5D_t1642385972* value)
	{
		___U3CreturnFieldsU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CreturnFieldsU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U3CqueryFieldsU3E__0_4() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t406948982, ___U3CqueryFieldsU3E__0_4)); }
	inline Dictionary_2_t3943999495 * get_U3CqueryFieldsU3E__0_4() const { return ___U3CqueryFieldsU3E__0_4; }
	inline Dictionary_2_t3943999495 ** get_address_of_U3CqueryFieldsU3E__0_4() { return &___U3CqueryFieldsU3E__0_4; }
	inline void set_U3CqueryFieldsU3E__0_4(Dictionary_2_t3943999495 * value)
	{
		___U3CqueryFieldsU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CqueryFieldsU3E__0_4), value);
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t406948982, ___U24this_5)); }
	inline TestAlchemyAPI_t3509857629 * get_U24this_5() const { return ___U24this_5; }
	inline TestAlchemyAPI_t3509857629 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(TestAlchemyAPI_t3509857629 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t406948982, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t406948982, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t406948982, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRUNTESTU3EC__ITERATOR0_T406948982_H
#ifndef CHECKSERVICESTATUS_T4150044828_H
#define CHECKSERVICESTATUS_T4150044828_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/CheckServiceStatus
struct  CheckServiceStatus_t4150044828  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/CheckServiceStatus::m_Service
	VisualRecognition_t3119563755 * ___m_Service_0;
	// IBM.Watson.DeveloperCloud.Services.ServiceStatus IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/CheckServiceStatus::m_Callback
	ServiceStatus_t1443707987 * ___m_Callback_1;
	// System.Int32 IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/CheckServiceStatus::m_GetClassifierCount
	int32_t ___m_GetClassifierCount_2;
	// System.Int32 IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/CheckServiceStatus::m_ClassifyCount
	int32_t ___m_ClassifyCount_3;

public:
	inline static int32_t get_offset_of_m_Service_0() { return static_cast<int32_t>(offsetof(CheckServiceStatus_t4150044828, ___m_Service_0)); }
	inline VisualRecognition_t3119563755 * get_m_Service_0() const { return ___m_Service_0; }
	inline VisualRecognition_t3119563755 ** get_address_of_m_Service_0() { return &___m_Service_0; }
	inline void set_m_Service_0(VisualRecognition_t3119563755 * value)
	{
		___m_Service_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Service_0), value);
	}

	inline static int32_t get_offset_of_m_Callback_1() { return static_cast<int32_t>(offsetof(CheckServiceStatus_t4150044828, ___m_Callback_1)); }
	inline ServiceStatus_t1443707987 * get_m_Callback_1() const { return ___m_Callback_1; }
	inline ServiceStatus_t1443707987 ** get_address_of_m_Callback_1() { return &___m_Callback_1; }
	inline void set_m_Callback_1(ServiceStatus_t1443707987 * value)
	{
		___m_Callback_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Callback_1), value);
	}

	inline static int32_t get_offset_of_m_GetClassifierCount_2() { return static_cast<int32_t>(offsetof(CheckServiceStatus_t4150044828, ___m_GetClassifierCount_2)); }
	inline int32_t get_m_GetClassifierCount_2() const { return ___m_GetClassifierCount_2; }
	inline int32_t* get_address_of_m_GetClassifierCount_2() { return &___m_GetClassifierCount_2; }
	inline void set_m_GetClassifierCount_2(int32_t value)
	{
		___m_GetClassifierCount_2 = value;
	}

	inline static int32_t get_offset_of_m_ClassifyCount_3() { return static_cast<int32_t>(offsetof(CheckServiceStatus_t4150044828, ___m_ClassifyCount_3)); }
	inline int32_t get_m_ClassifyCount_3() const { return ___m_ClassifyCount_3; }
	inline int32_t* get_address_of_m_ClassifyCount_3() { return &___m_ClassifyCount_3; }
	inline void set_m_ClassifyCount_3(int32_t value)
	{
		___m_ClassifyCount_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHECKSERVICESTATUS_T4150044828_H
#ifndef TESTLANGUAGETRANSLATOR_T3684829362_H
#define TESTLANGUAGETRANSLATOR_T3684829362_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.UnitTests.TestLanguageTranslator
struct  TestLanguageTranslator_t3684829362  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.LanguageTranslator.v1.LanguageTranslator IBM.Watson.DeveloperCloud.UnitTests.TestLanguageTranslator::m_Translate
	LanguageTranslator_t2981742234 * ___m_Translate_0;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestLanguageTranslator::m_GetModelTested
	bool ___m_GetModelTested_1;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestLanguageTranslator::m_GetModelsTested
	bool ___m_GetModelsTested_2;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestLanguageTranslator::m_GetLanguagesTested
	bool ___m_GetLanguagesTested_3;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestLanguageTranslator::m_IdentifyTested
	bool ___m_IdentifyTested_4;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestLanguageTranslator::m_TranslateTested
	bool ___m_TranslateTested_5;

public:
	inline static int32_t get_offset_of_m_Translate_0() { return static_cast<int32_t>(offsetof(TestLanguageTranslator_t3684829362, ___m_Translate_0)); }
	inline LanguageTranslator_t2981742234 * get_m_Translate_0() const { return ___m_Translate_0; }
	inline LanguageTranslator_t2981742234 ** get_address_of_m_Translate_0() { return &___m_Translate_0; }
	inline void set_m_Translate_0(LanguageTranslator_t2981742234 * value)
	{
		___m_Translate_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Translate_0), value);
	}

	inline static int32_t get_offset_of_m_GetModelTested_1() { return static_cast<int32_t>(offsetof(TestLanguageTranslator_t3684829362, ___m_GetModelTested_1)); }
	inline bool get_m_GetModelTested_1() const { return ___m_GetModelTested_1; }
	inline bool* get_address_of_m_GetModelTested_1() { return &___m_GetModelTested_1; }
	inline void set_m_GetModelTested_1(bool value)
	{
		___m_GetModelTested_1 = value;
	}

	inline static int32_t get_offset_of_m_GetModelsTested_2() { return static_cast<int32_t>(offsetof(TestLanguageTranslator_t3684829362, ___m_GetModelsTested_2)); }
	inline bool get_m_GetModelsTested_2() const { return ___m_GetModelsTested_2; }
	inline bool* get_address_of_m_GetModelsTested_2() { return &___m_GetModelsTested_2; }
	inline void set_m_GetModelsTested_2(bool value)
	{
		___m_GetModelsTested_2 = value;
	}

	inline static int32_t get_offset_of_m_GetLanguagesTested_3() { return static_cast<int32_t>(offsetof(TestLanguageTranslator_t3684829362, ___m_GetLanguagesTested_3)); }
	inline bool get_m_GetLanguagesTested_3() const { return ___m_GetLanguagesTested_3; }
	inline bool* get_address_of_m_GetLanguagesTested_3() { return &___m_GetLanguagesTested_3; }
	inline void set_m_GetLanguagesTested_3(bool value)
	{
		___m_GetLanguagesTested_3 = value;
	}

	inline static int32_t get_offset_of_m_IdentifyTested_4() { return static_cast<int32_t>(offsetof(TestLanguageTranslator_t3684829362, ___m_IdentifyTested_4)); }
	inline bool get_m_IdentifyTested_4() const { return ___m_IdentifyTested_4; }
	inline bool* get_address_of_m_IdentifyTested_4() { return &___m_IdentifyTested_4; }
	inline void set_m_IdentifyTested_4(bool value)
	{
		___m_IdentifyTested_4 = value;
	}

	inline static int32_t get_offset_of_m_TranslateTested_5() { return static_cast<int32_t>(offsetof(TestLanguageTranslator_t3684829362, ___m_TranslateTested_5)); }
	inline bool get_m_TranslateTested_5() const { return ___m_TranslateTested_5; }
	inline bool* get_address_of_m_TranslateTested_5() { return &___m_TranslateTested_5; }
	inline void set_m_TranslateTested_5(bool value)
	{
		___m_TranslateTested_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TESTLANGUAGETRANSLATOR_T3684829362_H
#ifndef U3CRUNTESTU3EC__ITERATOR0_T1152436222_H
#define U3CRUNTESTU3EC__ITERATOR0_T1152436222_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.UnitTests.TestLanguageTranslation/<RunTest>c__Iterator0
struct  U3CRunTestU3Ec__Iterator0_t1152436222  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.UnitTests.TestLanguageTranslation IBM.Watson.DeveloperCloud.UnitTests.TestLanguageTranslation/<RunTest>c__Iterator0::$this
	TestLanguageTranslation_t1772800819 * ___U24this_0;
	// System.Object IBM.Watson.DeveloperCloud.UnitTests.TestLanguageTranslation/<RunTest>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestLanguageTranslation/<RunTest>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 IBM.Watson.DeveloperCloud.UnitTests.TestLanguageTranslation/<RunTest>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t1152436222, ___U24this_0)); }
	inline TestLanguageTranslation_t1772800819 * get_U24this_0() const { return ___U24this_0; }
	inline TestLanguageTranslation_t1772800819 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(TestLanguageTranslation_t1772800819 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t1152436222, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t1152436222, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t1152436222, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRUNTESTU3EC__ITERATOR0_T1152436222_H
#ifndef U3CRUNTESTU3EC__ITERATOR0_T2458294274_H
#define U3CRUNTESTU3EC__ITERATOR0_T2458294274_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.UnitTests.TestRunnable/<RunTest>c__Iterator0
struct  U3CRunTestU3Ec__Iterator0_t2458294274  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.UnitTests.TestRunnable IBM.Watson.DeveloperCloud.UnitTests.TestRunnable/<RunTest>c__Iterator0::$this
	TestRunnable_t2118330963 * ___U24this_0;
	// System.Object IBM.Watson.DeveloperCloud.UnitTests.TestRunnable/<RunTest>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestRunnable/<RunTest>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 IBM.Watson.DeveloperCloud.UnitTests.TestRunnable/<RunTest>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t2458294274, ___U24this_0)); }
	inline TestRunnable_t2118330963 * get_U24this_0() const { return ___U24this_0; }
	inline TestRunnable_t2118330963 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(TestRunnable_t2118330963 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t2458294274, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t2458294274, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t2458294274, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRUNTESTU3EC__ITERATOR0_T2458294274_H
#ifndef U3CRUNTESTU3EC__ITERATOR0_T614343650_H
#define U3CRUNTESTU3EC__ITERATOR0_T614343650_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.UnitTests.TestPersonalityInsightsV2/<RunTest>c__Iterator0
struct  U3CRunTestU3Ec__Iterator0_t614343650  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestPersonalityInsightsV2/<RunTest>c__Iterator0::<dataPath>__0
	String_t* ___U3CdataPathU3E__0_0;
	// IBM.Watson.DeveloperCloud.UnitTests.TestPersonalityInsightsV2 IBM.Watson.DeveloperCloud.UnitTests.TestPersonalityInsightsV2/<RunTest>c__Iterator0::$this
	TestPersonalityInsightsV2_t1756789131 * ___U24this_1;
	// System.Object IBM.Watson.DeveloperCloud.UnitTests.TestPersonalityInsightsV2/<RunTest>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestPersonalityInsightsV2/<RunTest>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 IBM.Watson.DeveloperCloud.UnitTests.TestPersonalityInsightsV2/<RunTest>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CdataPathU3E__0_0() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t614343650, ___U3CdataPathU3E__0_0)); }
	inline String_t* get_U3CdataPathU3E__0_0() const { return ___U3CdataPathU3E__0_0; }
	inline String_t** get_address_of_U3CdataPathU3E__0_0() { return &___U3CdataPathU3E__0_0; }
	inline void set_U3CdataPathU3E__0_0(String_t* value)
	{
		___U3CdataPathU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdataPathU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t614343650, ___U24this_1)); }
	inline TestPersonalityInsightsV2_t1756789131 * get_U24this_1() const { return ___U24this_1; }
	inline TestPersonalityInsightsV2_t1756789131 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(TestPersonalityInsightsV2_t1756789131 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t614343650, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t614343650, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t614343650, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRUNTESTU3EC__ITERATOR0_T614343650_H
#ifndef U3CRUNTESTU3EC__ITERATOR0_T480647684_H
#define U3CRUNTESTU3EC__ITERATOR0_T480647684_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.UnitTests.TestRetrieveAndRank/<RunTest>c__Iterator0
struct  U3CRunTestU3Ec__Iterator0_t480647684  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.UnitTests.TestRetrieveAndRank IBM.Watson.DeveloperCloud.UnitTests.TestRetrieveAndRank/<RunTest>c__Iterator0::$this
	TestRetrieveAndRank_t2134779753 * ___U24this_0;
	// System.Object IBM.Watson.DeveloperCloud.UnitTests.TestRetrieveAndRank/<RunTest>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestRetrieveAndRank/<RunTest>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 IBM.Watson.DeveloperCloud.UnitTests.TestRetrieveAndRank/<RunTest>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t480647684, ___U24this_0)); }
	inline TestRetrieveAndRank_t2134779753 * get_U24this_0() const { return ___U24this_0; }
	inline TestRetrieveAndRank_t2134779753 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(TestRetrieveAndRank_t2134779753 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t480647684, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t480647684, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t480647684, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRUNTESTU3EC__ITERATOR0_T480647684_H
#ifndef U3CRUNTESTU3EC__ITERATOR0_T980178097_H
#define U3CRUNTESTU3EC__ITERATOR0_T980178097_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.UnitTests.TestNaturalLanguageClassifier/<RunTest>c__Iterator0
struct  U3CRunTestU3Ec__Iterator0_t980178097  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestNaturalLanguageClassifier/<RunTest>c__Iterator0::<trainingData>__1
	String_t* ___U3CtrainingDataU3E__1_0;
	// IBM.Watson.DeveloperCloud.UnitTests.TestNaturalLanguageClassifier IBM.Watson.DeveloperCloud.UnitTests.TestNaturalLanguageClassifier/<RunTest>c__Iterator0::$this
	TestNaturalLanguageClassifier_t1226850908 * ___U24this_1;
	// System.Object IBM.Watson.DeveloperCloud.UnitTests.TestNaturalLanguageClassifier/<RunTest>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestNaturalLanguageClassifier/<RunTest>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 IBM.Watson.DeveloperCloud.UnitTests.TestNaturalLanguageClassifier/<RunTest>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CtrainingDataU3E__1_0() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t980178097, ___U3CtrainingDataU3E__1_0)); }
	inline String_t* get_U3CtrainingDataU3E__1_0() const { return ___U3CtrainingDataU3E__1_0; }
	inline String_t** get_address_of_U3CtrainingDataU3E__1_0() { return &___U3CtrainingDataU3E__1_0; }
	inline void set_U3CtrainingDataU3E__1_0(String_t* value)
	{
		___U3CtrainingDataU3E__1_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtrainingDataU3E__1_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t980178097, ___U24this_1)); }
	inline TestNaturalLanguageClassifier_t1226850908 * get_U24this_1() const { return ___U24this_1; }
	inline TestNaturalLanguageClassifier_t1226850908 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(TestNaturalLanguageClassifier_t1226850908 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t980178097, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t980178097, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t980178097, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRUNTESTU3EC__ITERATOR0_T980178097_H
#ifndef U3CCHECKENVIRONMENTSTATUSU3EC__ITERATOR5_T3997571362_H
#define U3CCHECKENVIRONMENTSTATUSU3EC__ITERATOR5_T3997571362_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery/<CheckEnvironmentStatus>c__Iterator5
struct  U3CCheckEnvironmentStatusU3Ec__Iterator5_t3997571362  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery/<CheckEnvironmentStatus>c__Iterator5::$this
	TestDiscovery_t1320997924 * ___U24this_0;
	// System.Object IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery/<CheckEnvironmentStatus>c__Iterator5::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery/<CheckEnvironmentStatus>c__Iterator5::$disposing
	bool ___U24disposing_2;
	// System.Int32 IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery/<CheckEnvironmentStatus>c__Iterator5::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CCheckEnvironmentStatusU3Ec__Iterator5_t3997571362, ___U24this_0)); }
	inline TestDiscovery_t1320997924 * get_U24this_0() const { return ___U24this_0; }
	inline TestDiscovery_t1320997924 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(TestDiscovery_t1320997924 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CCheckEnvironmentStatusU3Ec__Iterator5_t3997571362, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CCheckEnvironmentStatusU3Ec__Iterator5_t3997571362, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CCheckEnvironmentStatusU3Ec__Iterator5_t3997571362, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCHECKENVIRONMENTSTATUSU3EC__ITERATOR5_T3997571362_H
#ifndef U3CDELETEENVIRONMENTU3EC__ITERATOR4_T645273570_H
#define U3CDELETEENVIRONMENTU3EC__ITERATOR4_T645273570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery/<DeleteEnvironment>c__Iterator4
struct  U3CDeleteEnvironmentU3Ec__Iterator4_t645273570  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery/<DeleteEnvironment>c__Iterator4::$this
	TestDiscovery_t1320997924 * ___U24this_0;
	// System.Object IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery/<DeleteEnvironment>c__Iterator4::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery/<DeleteEnvironment>c__Iterator4::$disposing
	bool ___U24disposing_2;
	// System.Int32 IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery/<DeleteEnvironment>c__Iterator4::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CDeleteEnvironmentU3Ec__Iterator4_t645273570, ___U24this_0)); }
	inline TestDiscovery_t1320997924 * get_U24this_0() const { return ___U24this_0; }
	inline TestDiscovery_t1320997924 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(TestDiscovery_t1320997924 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CDeleteEnvironmentU3Ec__Iterator4_t645273570, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CDeleteEnvironmentU3Ec__Iterator4_t645273570, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CDeleteEnvironmentU3Ec__Iterator4_t645273570, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDELETEENVIRONMENTU3EC__ITERATOR4_T645273570_H
#ifndef U3CDELETECONFIGURATIONU3EC__ITERATOR3_T783559642_H
#define U3CDELETECONFIGURATIONU3EC__ITERATOR3_T783559642_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery/<DeleteConfiguration>c__Iterator3
struct  U3CDeleteConfigurationU3Ec__Iterator3_t783559642  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery/<DeleteConfiguration>c__Iterator3::$this
	TestDiscovery_t1320997924 * ___U24this_0;
	// System.Object IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery/<DeleteConfiguration>c__Iterator3::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery/<DeleteConfiguration>c__Iterator3::$disposing
	bool ___U24disposing_2;
	// System.Int32 IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery/<DeleteConfiguration>c__Iterator3::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CDeleteConfigurationU3Ec__Iterator3_t783559642, ___U24this_0)); }
	inline TestDiscovery_t1320997924 * get_U24this_0() const { return ___U24this_0; }
	inline TestDiscovery_t1320997924 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(TestDiscovery_t1320997924 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CDeleteConfigurationU3Ec__Iterator3_t783559642, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CDeleteConfigurationU3Ec__Iterator3_t783559642, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CDeleteConfigurationU3Ec__Iterator3_t783559642, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDELETECONFIGURATIONU3EC__ITERATOR3_T783559642_H
#ifndef U3CRUNTESTU3EC__ITERATOR0_T2238499560_H
#define U3CRUNTESTU3EC__ITERATOR0_T2238499560_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.UnitTests.TestEventManager/<RunTest>c__Iterator0
struct  U3CRunTestU3Ec__Iterator0_t2238499560  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.UnitTests.TestEventManager IBM.Watson.DeveloperCloud.UnitTests.TestEventManager/<RunTest>c__Iterator0::$this
	TestEventManager_t1784053145 * ___U24this_0;
	// System.Object IBM.Watson.DeveloperCloud.UnitTests.TestEventManager/<RunTest>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestEventManager/<RunTest>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 IBM.Watson.DeveloperCloud.UnitTests.TestEventManager/<RunTest>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t2238499560, ___U24this_0)); }
	inline TestEventManager_t1784053145 * get_U24this_0() const { return ___U24this_0; }
	inline TestEventManager_t1784053145 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(TestEventManager_t1784053145 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t2238499560, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t2238499560, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t2238499560, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRUNTESTU3EC__ITERATOR0_T2238499560_H
#ifndef U3CTESTCOROUTINEU3EC__ITERATOR1_T1222338220_H
#define U3CTESTCOROUTINEU3EC__ITERATOR1_T1222338220_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.UnitTests.TestRunnable/<TestCoroutine>c__Iterator1
struct  U3CTestCoroutineU3Ec__Iterator1_t1222338220  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestRunnable/<TestCoroutine>c__Iterator1::a_Argument
	String_t* ___a_Argument_0;
	// IBM.Watson.DeveloperCloud.UnitTests.TestRunnable IBM.Watson.DeveloperCloud.UnitTests.TestRunnable/<TestCoroutine>c__Iterator1::$this
	TestRunnable_t2118330963 * ___U24this_1;
	// System.Object IBM.Watson.DeveloperCloud.UnitTests.TestRunnable/<TestCoroutine>c__Iterator1::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestRunnable/<TestCoroutine>c__Iterator1::$disposing
	bool ___U24disposing_3;
	// System.Int32 IBM.Watson.DeveloperCloud.UnitTests.TestRunnable/<TestCoroutine>c__Iterator1::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_a_Argument_0() { return static_cast<int32_t>(offsetof(U3CTestCoroutineU3Ec__Iterator1_t1222338220, ___a_Argument_0)); }
	inline String_t* get_a_Argument_0() const { return ___a_Argument_0; }
	inline String_t** get_address_of_a_Argument_0() { return &___a_Argument_0; }
	inline void set_a_Argument_0(String_t* value)
	{
		___a_Argument_0 = value;
		Il2CppCodeGenWriteBarrier((&___a_Argument_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CTestCoroutineU3Ec__Iterator1_t1222338220, ___U24this_1)); }
	inline TestRunnable_t2118330963 * get_U24this_1() const { return ___U24this_1; }
	inline TestRunnable_t2118330963 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(TestRunnable_t2118330963 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CTestCoroutineU3Ec__Iterator1_t1222338220, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CTestCoroutineU3Ec__Iterator1_t1222338220, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CTestCoroutineU3Ec__Iterator1_t1222338220, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTESTCOROUTINEU3EC__ITERATOR1_T1222338220_H
#ifndef U3CRUNTESTU3EC__ITERATOR0_T2987565082_H
#define U3CRUNTESTU3EC__ITERATOR0_T2987565082_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.UnitTests.TestDocumentConversion/<RunTest>c__Iterator0
struct  U3CRunTestU3Ec__Iterator0_t2987565082  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestDocumentConversion/<RunTest>c__Iterator0::<examplePath>__0
	String_t* ___U3CexamplePathU3E__0_0;
	// IBM.Watson.DeveloperCloud.UnitTests.TestDocumentConversion IBM.Watson.DeveloperCloud.UnitTests.TestDocumentConversion/<RunTest>c__Iterator0::$this
	TestDocumentConversion_t3132176613 * ___U24this_1;
	// System.Object IBM.Watson.DeveloperCloud.UnitTests.TestDocumentConversion/<RunTest>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestDocumentConversion/<RunTest>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 IBM.Watson.DeveloperCloud.UnitTests.TestDocumentConversion/<RunTest>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CexamplePathU3E__0_0() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t2987565082, ___U3CexamplePathU3E__0_0)); }
	inline String_t* get_U3CexamplePathU3E__0_0() const { return ___U3CexamplePathU3E__0_0; }
	inline String_t** get_address_of_U3CexamplePathU3E__0_0() { return &___U3CexamplePathU3E__0_0; }
	inline void set_U3CexamplePathU3E__0_0(String_t* value)
	{
		___U3CexamplePathU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CexamplePathU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t2987565082, ___U24this_1)); }
	inline TestDocumentConversion_t3132176613 * get_U24this_1() const { return ___U24this_1; }
	inline TestDocumentConversion_t3132176613 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(TestDocumentConversion_t3132176613 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t2987565082, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t2987565082, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t2987565082, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRUNTESTU3EC__ITERATOR0_T2987565082_H
#ifndef U3CRUNTESTU3EC__ITERATOR0_T161392710_H
#define U3CRUNTESTU3EC__ITERATOR0_T161392710_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.UnitTests.TestTradeoffAnalytics/<RunTest>c__Iterator0
struct  U3CRunTestU3Ec__Iterator0_t161392710  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Problem IBM.Watson.DeveloperCloud.UnitTests.TestTradeoffAnalytics/<RunTest>c__Iterator0::<problemToSolve>__0
	Problem_t2814813345 * ___U3CproblemToSolveU3E__0_0;
	// System.Collections.Generic.List`1<IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column> IBM.Watson.DeveloperCloud.UnitTests.TestTradeoffAnalytics/<RunTest>c__Iterator0::<listColumn>__0
	List_1_t1981607956 * ___U3ClistColumnU3E__0_1;
	// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column IBM.Watson.DeveloperCloud.UnitTests.TestTradeoffAnalytics/<RunTest>c__Iterator0::<columnPrice>__0
	Column_t2612486824 * ___U3CcolumnPriceU3E__0_2;
	// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column IBM.Watson.DeveloperCloud.UnitTests.TestTradeoffAnalytics/<RunTest>c__Iterator0::<columnWeight>__0
	Column_t2612486824 * ___U3CcolumnWeightU3E__0_3;
	// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Column IBM.Watson.DeveloperCloud.UnitTests.TestTradeoffAnalytics/<RunTest>c__Iterator0::<columnBrandName>__0
	Column_t2612486824 * ___U3CcolumnBrandNameU3E__0_4;
	// System.Collections.Generic.List`1<IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Option> IBM.Watson.DeveloperCloud.UnitTests.TestTradeoffAnalytics/<RunTest>c__Iterator0::<listOption>__0
	List_1_t1144248177 * ___U3ClistOptionU3E__0_5;
	// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Option IBM.Watson.DeveloperCloud.UnitTests.TestTradeoffAnalytics/<RunTest>c__Iterator0::<option1>__0
	Option_t1775127045 * ___U3Coption1U3E__0_6;
	// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Option IBM.Watson.DeveloperCloud.UnitTests.TestTradeoffAnalytics/<RunTest>c__Iterator0::<option2>__0
	Option_t1775127045 * ___U3Coption2U3E__0_7;
	// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.Option IBM.Watson.DeveloperCloud.UnitTests.TestTradeoffAnalytics/<RunTest>c__Iterator0::<option3>__0
	Option_t1775127045 * ___U3Coption3U3E__0_8;
	// IBM.Watson.DeveloperCloud.UnitTests.TestTradeoffAnalytics IBM.Watson.DeveloperCloud.UnitTests.TestTradeoffAnalytics/<RunTest>c__Iterator0::$this
	TestTradeoffAnalytics_t1911417121 * ___U24this_9;
	// System.Object IBM.Watson.DeveloperCloud.UnitTests.TestTradeoffAnalytics/<RunTest>c__Iterator0::$current
	RuntimeObject * ___U24current_10;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestTradeoffAnalytics/<RunTest>c__Iterator0::$disposing
	bool ___U24disposing_11;
	// System.Int32 IBM.Watson.DeveloperCloud.UnitTests.TestTradeoffAnalytics/<RunTest>c__Iterator0::$PC
	int32_t ___U24PC_12;

public:
	inline static int32_t get_offset_of_U3CproblemToSolveU3E__0_0() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t161392710, ___U3CproblemToSolveU3E__0_0)); }
	inline Problem_t2814813345 * get_U3CproblemToSolveU3E__0_0() const { return ___U3CproblemToSolveU3E__0_0; }
	inline Problem_t2814813345 ** get_address_of_U3CproblemToSolveU3E__0_0() { return &___U3CproblemToSolveU3E__0_0; }
	inline void set_U3CproblemToSolveU3E__0_0(Problem_t2814813345 * value)
	{
		___U3CproblemToSolveU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CproblemToSolveU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3ClistColumnU3E__0_1() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t161392710, ___U3ClistColumnU3E__0_1)); }
	inline List_1_t1981607956 * get_U3ClistColumnU3E__0_1() const { return ___U3ClistColumnU3E__0_1; }
	inline List_1_t1981607956 ** get_address_of_U3ClistColumnU3E__0_1() { return &___U3ClistColumnU3E__0_1; }
	inline void set_U3ClistColumnU3E__0_1(List_1_t1981607956 * value)
	{
		___U3ClistColumnU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClistColumnU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CcolumnPriceU3E__0_2() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t161392710, ___U3CcolumnPriceU3E__0_2)); }
	inline Column_t2612486824 * get_U3CcolumnPriceU3E__0_2() const { return ___U3CcolumnPriceU3E__0_2; }
	inline Column_t2612486824 ** get_address_of_U3CcolumnPriceU3E__0_2() { return &___U3CcolumnPriceU3E__0_2; }
	inline void set_U3CcolumnPriceU3E__0_2(Column_t2612486824 * value)
	{
		___U3CcolumnPriceU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcolumnPriceU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CcolumnWeightU3E__0_3() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t161392710, ___U3CcolumnWeightU3E__0_3)); }
	inline Column_t2612486824 * get_U3CcolumnWeightU3E__0_3() const { return ___U3CcolumnWeightU3E__0_3; }
	inline Column_t2612486824 ** get_address_of_U3CcolumnWeightU3E__0_3() { return &___U3CcolumnWeightU3E__0_3; }
	inline void set_U3CcolumnWeightU3E__0_3(Column_t2612486824 * value)
	{
		___U3CcolumnWeightU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcolumnWeightU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U3CcolumnBrandNameU3E__0_4() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t161392710, ___U3CcolumnBrandNameU3E__0_4)); }
	inline Column_t2612486824 * get_U3CcolumnBrandNameU3E__0_4() const { return ___U3CcolumnBrandNameU3E__0_4; }
	inline Column_t2612486824 ** get_address_of_U3CcolumnBrandNameU3E__0_4() { return &___U3CcolumnBrandNameU3E__0_4; }
	inline void set_U3CcolumnBrandNameU3E__0_4(Column_t2612486824 * value)
	{
		___U3CcolumnBrandNameU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcolumnBrandNameU3E__0_4), value);
	}

	inline static int32_t get_offset_of_U3ClistOptionU3E__0_5() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t161392710, ___U3ClistOptionU3E__0_5)); }
	inline List_1_t1144248177 * get_U3ClistOptionU3E__0_5() const { return ___U3ClistOptionU3E__0_5; }
	inline List_1_t1144248177 ** get_address_of_U3ClistOptionU3E__0_5() { return &___U3ClistOptionU3E__0_5; }
	inline void set_U3ClistOptionU3E__0_5(List_1_t1144248177 * value)
	{
		___U3ClistOptionU3E__0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClistOptionU3E__0_5), value);
	}

	inline static int32_t get_offset_of_U3Coption1U3E__0_6() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t161392710, ___U3Coption1U3E__0_6)); }
	inline Option_t1775127045 * get_U3Coption1U3E__0_6() const { return ___U3Coption1U3E__0_6; }
	inline Option_t1775127045 ** get_address_of_U3Coption1U3E__0_6() { return &___U3Coption1U3E__0_6; }
	inline void set_U3Coption1U3E__0_6(Option_t1775127045 * value)
	{
		___U3Coption1U3E__0_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3Coption1U3E__0_6), value);
	}

	inline static int32_t get_offset_of_U3Coption2U3E__0_7() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t161392710, ___U3Coption2U3E__0_7)); }
	inline Option_t1775127045 * get_U3Coption2U3E__0_7() const { return ___U3Coption2U3E__0_7; }
	inline Option_t1775127045 ** get_address_of_U3Coption2U3E__0_7() { return &___U3Coption2U3E__0_7; }
	inline void set_U3Coption2U3E__0_7(Option_t1775127045 * value)
	{
		___U3Coption2U3E__0_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3Coption2U3E__0_7), value);
	}

	inline static int32_t get_offset_of_U3Coption3U3E__0_8() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t161392710, ___U3Coption3U3E__0_8)); }
	inline Option_t1775127045 * get_U3Coption3U3E__0_8() const { return ___U3Coption3U3E__0_8; }
	inline Option_t1775127045 ** get_address_of_U3Coption3U3E__0_8() { return &___U3Coption3U3E__0_8; }
	inline void set_U3Coption3U3E__0_8(Option_t1775127045 * value)
	{
		___U3Coption3U3E__0_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3Coption3U3E__0_8), value);
	}

	inline static int32_t get_offset_of_U24this_9() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t161392710, ___U24this_9)); }
	inline TestTradeoffAnalytics_t1911417121 * get_U24this_9() const { return ___U24this_9; }
	inline TestTradeoffAnalytics_t1911417121 ** get_address_of_U24this_9() { return &___U24this_9; }
	inline void set_U24this_9(TestTradeoffAnalytics_t1911417121 * value)
	{
		___U24this_9 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_9), value);
	}

	inline static int32_t get_offset_of_U24current_10() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t161392710, ___U24current_10)); }
	inline RuntimeObject * get_U24current_10() const { return ___U24current_10; }
	inline RuntimeObject ** get_address_of_U24current_10() { return &___U24current_10; }
	inline void set_U24current_10(RuntimeObject * value)
	{
		___U24current_10 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_10), value);
	}

	inline static int32_t get_offset_of_U24disposing_11() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t161392710, ___U24disposing_11)); }
	inline bool get_U24disposing_11() const { return ___U24disposing_11; }
	inline bool* get_address_of_U24disposing_11() { return &___U24disposing_11; }
	inline void set_U24disposing_11(bool value)
	{
		___U24disposing_11 = value;
	}

	inline static int32_t get_offset_of_U24PC_12() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t161392710, ___U24PC_12)); }
	inline int32_t get_U24PC_12() const { return ___U24PC_12; }
	inline int32_t* get_address_of_U24PC_12() { return &___U24PC_12; }
	inline void set_U24PC_12(int32_t value)
	{
		___U24PC_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRUNTESTU3EC__ITERATOR0_T161392710_H
#ifndef U3CLOADCONFIGCRU3EC__ITERATOR0_T3553416038_H
#define U3CLOADCONFIGCRU3EC__ITERATOR0_T3553416038_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Utilities.Config/<LoadConfigCR>c__Iterator0
struct  U3CLoadConfigCRU3Ec__Iterator0_t3553416038  : public RuntimeObject
{
public:
	// UnityEngine.WWW IBM.Watson.DeveloperCloud.Utilities.Config/<LoadConfigCR>c__Iterator0::<request>__0
	WWW_t2919945039 * ___U3CrequestU3E__0_0;
	// IBM.Watson.DeveloperCloud.Utilities.Config IBM.Watson.DeveloperCloud.Utilities.Config/<LoadConfigCR>c__Iterator0::$this
	Config_t3637807320 * ___U24this_1;
	// System.Object IBM.Watson.DeveloperCloud.Utilities.Config/<LoadConfigCR>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean IBM.Watson.DeveloperCloud.Utilities.Config/<LoadConfigCR>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 IBM.Watson.DeveloperCloud.Utilities.Config/<LoadConfigCR>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CrequestU3E__0_0() { return static_cast<int32_t>(offsetof(U3CLoadConfigCRU3Ec__Iterator0_t3553416038, ___U3CrequestU3E__0_0)); }
	inline WWW_t2919945039 * get_U3CrequestU3E__0_0() const { return ___U3CrequestU3E__0_0; }
	inline WWW_t2919945039 ** get_address_of_U3CrequestU3E__0_0() { return &___U3CrequestU3E__0_0; }
	inline void set_U3CrequestU3E__0_0(WWW_t2919945039 * value)
	{
		___U3CrequestU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CLoadConfigCRU3Ec__Iterator0_t3553416038, ___U24this_1)); }
	inline Config_t3637807320 * get_U24this_1() const { return ___U24this_1; }
	inline Config_t3637807320 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(Config_t3637807320 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CLoadConfigCRU3Ec__Iterator0_t3553416038, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CLoadConfigCRU3Ec__Iterator0_t3553416038, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CLoadConfigCRU3Ec__Iterator0_t3553416038, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADCONFIGCRU3EC__ITERATOR0_T3553416038_H
#ifndef CONSTANTS_T3488194005_H
#define CONSTANTS_T3488194005_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Utilities.Constants
struct  Constants_t3488194005  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTANTS_T3488194005_H
#ifndef REQUEST_T466816980_H
#define REQUEST_T466816980_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request
struct  Request_t466816980  : public RuntimeObject
{
public:
	// System.Single IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Timeout>k__BackingField
	float ___U3CTimeoutU3Ek__BackingField_0;
	// System.Boolean IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Cancel>k__BackingField
	bool ___U3CCancelU3Ek__BackingField_1;
	// System.Boolean IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Delete>k__BackingField
	bool ___U3CDeleteU3Ek__BackingField_2;
	// System.String IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Function>k__BackingField
	String_t* ___U3CFunctionU3Ek__BackingField_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Parameters>k__BackingField
	Dictionary_2_t309261261 * ___U3CParametersU3Ek__BackingField_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Headers>k__BackingField
	Dictionary_2_t3943999495 * ___U3CHeadersU3Ek__BackingField_5;
	// System.Byte[] IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Send>k__BackingField
	ByteU5BU5D_t3397334013* ___U3CSendU3Ek__BackingField_6;
	// System.Collections.Generic.Dictionary`2<System.String,IBM.Watson.DeveloperCloud.Connection.RESTConnector/Form> IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<Forms>k__BackingField
	Dictionary_2_t2694055125 * ___U3CFormsU3Ek__BackingField_7;
	// IBM.Watson.DeveloperCloud.Connection.RESTConnector/ResponseEvent IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<OnResponse>k__BackingField
	ResponseEvent_t1616568356 * ___U3COnResponseU3Ek__BackingField_8;
	// IBM.Watson.DeveloperCloud.Connection.RESTConnector/ProgressEvent IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<OnDownloadProgress>k__BackingField
	ProgressEvent_t4185145044 * ___U3COnDownloadProgressU3Ek__BackingField_9;
	// IBM.Watson.DeveloperCloud.Connection.RESTConnector/ProgressEvent IBM.Watson.DeveloperCloud.Connection.RESTConnector/Request::<OnUploadProgress>k__BackingField
	ProgressEvent_t4185145044 * ___U3COnUploadProgressU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of_U3CTimeoutU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CTimeoutU3Ek__BackingField_0)); }
	inline float get_U3CTimeoutU3Ek__BackingField_0() const { return ___U3CTimeoutU3Ek__BackingField_0; }
	inline float* get_address_of_U3CTimeoutU3Ek__BackingField_0() { return &___U3CTimeoutU3Ek__BackingField_0; }
	inline void set_U3CTimeoutU3Ek__BackingField_0(float value)
	{
		___U3CTimeoutU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CCancelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CCancelU3Ek__BackingField_1)); }
	inline bool get_U3CCancelU3Ek__BackingField_1() const { return ___U3CCancelU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CCancelU3Ek__BackingField_1() { return &___U3CCancelU3Ek__BackingField_1; }
	inline void set_U3CCancelU3Ek__BackingField_1(bool value)
	{
		___U3CCancelU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CDeleteU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CDeleteU3Ek__BackingField_2)); }
	inline bool get_U3CDeleteU3Ek__BackingField_2() const { return ___U3CDeleteU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CDeleteU3Ek__BackingField_2() { return &___U3CDeleteU3Ek__BackingField_2; }
	inline void set_U3CDeleteU3Ek__BackingField_2(bool value)
	{
		___U3CDeleteU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CFunctionU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CFunctionU3Ek__BackingField_3)); }
	inline String_t* get_U3CFunctionU3Ek__BackingField_3() const { return ___U3CFunctionU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CFunctionU3Ek__BackingField_3() { return &___U3CFunctionU3Ek__BackingField_3; }
	inline void set_U3CFunctionU3Ek__BackingField_3(String_t* value)
	{
		___U3CFunctionU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFunctionU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CParametersU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CParametersU3Ek__BackingField_4)); }
	inline Dictionary_2_t309261261 * get_U3CParametersU3Ek__BackingField_4() const { return ___U3CParametersU3Ek__BackingField_4; }
	inline Dictionary_2_t309261261 ** get_address_of_U3CParametersU3Ek__BackingField_4() { return &___U3CParametersU3Ek__BackingField_4; }
	inline void set_U3CParametersU3Ek__BackingField_4(Dictionary_2_t309261261 * value)
	{
		___U3CParametersU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CParametersU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CHeadersU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CHeadersU3Ek__BackingField_5)); }
	inline Dictionary_2_t3943999495 * get_U3CHeadersU3Ek__BackingField_5() const { return ___U3CHeadersU3Ek__BackingField_5; }
	inline Dictionary_2_t3943999495 ** get_address_of_U3CHeadersU3Ek__BackingField_5() { return &___U3CHeadersU3Ek__BackingField_5; }
	inline void set_U3CHeadersU3Ek__BackingField_5(Dictionary_2_t3943999495 * value)
	{
		___U3CHeadersU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CHeadersU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CSendU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CSendU3Ek__BackingField_6)); }
	inline ByteU5BU5D_t3397334013* get_U3CSendU3Ek__BackingField_6() const { return ___U3CSendU3Ek__BackingField_6; }
	inline ByteU5BU5D_t3397334013** get_address_of_U3CSendU3Ek__BackingField_6() { return &___U3CSendU3Ek__BackingField_6; }
	inline void set_U3CSendU3Ek__BackingField_6(ByteU5BU5D_t3397334013* value)
	{
		___U3CSendU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSendU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CFormsU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3CFormsU3Ek__BackingField_7)); }
	inline Dictionary_2_t2694055125 * get_U3CFormsU3Ek__BackingField_7() const { return ___U3CFormsU3Ek__BackingField_7; }
	inline Dictionary_2_t2694055125 ** get_address_of_U3CFormsU3Ek__BackingField_7() { return &___U3CFormsU3Ek__BackingField_7; }
	inline void set_U3CFormsU3Ek__BackingField_7(Dictionary_2_t2694055125 * value)
	{
		___U3CFormsU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFormsU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3COnResponseU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3COnResponseU3Ek__BackingField_8)); }
	inline ResponseEvent_t1616568356 * get_U3COnResponseU3Ek__BackingField_8() const { return ___U3COnResponseU3Ek__BackingField_8; }
	inline ResponseEvent_t1616568356 ** get_address_of_U3COnResponseU3Ek__BackingField_8() { return &___U3COnResponseU3Ek__BackingField_8; }
	inline void set_U3COnResponseU3Ek__BackingField_8(ResponseEvent_t1616568356 * value)
	{
		___U3COnResponseU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3COnResponseU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3COnDownloadProgressU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3COnDownloadProgressU3Ek__BackingField_9)); }
	inline ProgressEvent_t4185145044 * get_U3COnDownloadProgressU3Ek__BackingField_9() const { return ___U3COnDownloadProgressU3Ek__BackingField_9; }
	inline ProgressEvent_t4185145044 ** get_address_of_U3COnDownloadProgressU3Ek__BackingField_9() { return &___U3COnDownloadProgressU3Ek__BackingField_9; }
	inline void set_U3COnDownloadProgressU3Ek__BackingField_9(ProgressEvent_t4185145044 * value)
	{
		___U3COnDownloadProgressU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3COnDownloadProgressU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3COnUploadProgressU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(Request_t466816980, ___U3COnUploadProgressU3Ek__BackingField_10)); }
	inline ProgressEvent_t4185145044 * get_U3COnUploadProgressU3Ek__BackingField_10() const { return ___U3COnUploadProgressU3Ek__BackingField_10; }
	inline ProgressEvent_t4185145044 ** get_address_of_U3COnUploadProgressU3Ek__BackingField_10() { return &___U3COnUploadProgressU3Ek__BackingField_10; }
	inline void set_U3COnUploadProgressU3Ek__BackingField_10(ProgressEvent_t4185145044 * value)
	{
		___U3COnUploadProgressU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3COnUploadProgressU3Ek__BackingField_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUEST_T466816980_H
#ifndef FINDCLASSIFIERREQ_T2124707147_H
#define FINDCLASSIFIERREQ_T2124707147_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/FindClassifierReq
struct  FindClassifierReq_t2124707147  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/FindClassifierReq::<Service>k__BackingField
	VisualRecognition_t3119563755 * ___U3CServiceU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/FindClassifierReq::<ClassifierName>k__BackingField
	String_t* ___U3CClassifierNameU3Ek__BackingField_1;
	// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/OnFindClassifier IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/FindClassifierReq::<Callback>k__BackingField
	OnFindClassifier_t2896727766 * ___U3CCallbackU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CServiceU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(FindClassifierReq_t2124707147, ___U3CServiceU3Ek__BackingField_0)); }
	inline VisualRecognition_t3119563755 * get_U3CServiceU3Ek__BackingField_0() const { return ___U3CServiceU3Ek__BackingField_0; }
	inline VisualRecognition_t3119563755 ** get_address_of_U3CServiceU3Ek__BackingField_0() { return &___U3CServiceU3Ek__BackingField_0; }
	inline void set_U3CServiceU3Ek__BackingField_0(VisualRecognition_t3119563755 * value)
	{
		___U3CServiceU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CServiceU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CClassifierNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(FindClassifierReq_t2124707147, ___U3CClassifierNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CClassifierNameU3Ek__BackingField_1() const { return ___U3CClassifierNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CClassifierNameU3Ek__BackingField_1() { return &___U3CClassifierNameU3Ek__BackingField_1; }
	inline void set_U3CClassifierNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CClassifierNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CClassifierNameU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(FindClassifierReq_t2124707147, ___U3CCallbackU3Ek__BackingField_2)); }
	inline OnFindClassifier_t2896727766 * get_U3CCallbackU3Ek__BackingField_2() const { return ___U3CCallbackU3Ek__BackingField_2; }
	inline OnFindClassifier_t2896727766 ** get_address_of_U3CCallbackU3Ek__BackingField_2() { return &___U3CCallbackU3Ek__BackingField_2; }
	inline void set_U3CCallbackU3Ek__BackingField_2(OnFindClassifier_t2896727766 * value)
	{
		___U3CCallbackU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FINDCLASSIFIERREQ_T2124707147_H
#ifndef CONFIG_T3637807320_H
#define CONFIG_T3637807320_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Utilities.Config
struct  Config_t3637807320  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Utilities.Config::m_ClassifierDirectory
	String_t* ___m_ClassifierDirectory_0;
	// System.Single IBM.Watson.DeveloperCloud.Utilities.Config::m_TimeOut
	float ___m_TimeOut_1;
	// System.Int32 IBM.Watson.DeveloperCloud.Utilities.Config::m_MaxRestConnections
	int32_t ___m_MaxRestConnections_2;
	// System.Collections.Generic.List`1<IBM.Watson.DeveloperCloud.Utilities.Config/CredentialInfo> IBM.Watson.DeveloperCloud.Utilities.Config::m_Credentials
	List_1_t3698242869 * ___m_Credentials_3;
	// System.Collections.Generic.List`1<IBM.Watson.DeveloperCloud.Utilities.Config/Variable> IBM.Watson.DeveloperCloud.Utilities.Config::m_Variables
	List_1_t4101323382 * ___m_Variables_4;
	// System.Boolean IBM.Watson.DeveloperCloud.Utilities.Config::<ConfigLoaded>k__BackingField
	bool ___U3CConfigLoadedU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_m_ClassifierDirectory_0() { return static_cast<int32_t>(offsetof(Config_t3637807320, ___m_ClassifierDirectory_0)); }
	inline String_t* get_m_ClassifierDirectory_0() const { return ___m_ClassifierDirectory_0; }
	inline String_t** get_address_of_m_ClassifierDirectory_0() { return &___m_ClassifierDirectory_0; }
	inline void set_m_ClassifierDirectory_0(String_t* value)
	{
		___m_ClassifierDirectory_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ClassifierDirectory_0), value);
	}

	inline static int32_t get_offset_of_m_TimeOut_1() { return static_cast<int32_t>(offsetof(Config_t3637807320, ___m_TimeOut_1)); }
	inline float get_m_TimeOut_1() const { return ___m_TimeOut_1; }
	inline float* get_address_of_m_TimeOut_1() { return &___m_TimeOut_1; }
	inline void set_m_TimeOut_1(float value)
	{
		___m_TimeOut_1 = value;
	}

	inline static int32_t get_offset_of_m_MaxRestConnections_2() { return static_cast<int32_t>(offsetof(Config_t3637807320, ___m_MaxRestConnections_2)); }
	inline int32_t get_m_MaxRestConnections_2() const { return ___m_MaxRestConnections_2; }
	inline int32_t* get_address_of_m_MaxRestConnections_2() { return &___m_MaxRestConnections_2; }
	inline void set_m_MaxRestConnections_2(int32_t value)
	{
		___m_MaxRestConnections_2 = value;
	}

	inline static int32_t get_offset_of_m_Credentials_3() { return static_cast<int32_t>(offsetof(Config_t3637807320, ___m_Credentials_3)); }
	inline List_1_t3698242869 * get_m_Credentials_3() const { return ___m_Credentials_3; }
	inline List_1_t3698242869 ** get_address_of_m_Credentials_3() { return &___m_Credentials_3; }
	inline void set_m_Credentials_3(List_1_t3698242869 * value)
	{
		___m_Credentials_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Credentials_3), value);
	}

	inline static int32_t get_offset_of_m_Variables_4() { return static_cast<int32_t>(offsetof(Config_t3637807320, ___m_Variables_4)); }
	inline List_1_t4101323382 * get_m_Variables_4() const { return ___m_Variables_4; }
	inline List_1_t4101323382 ** get_address_of_m_Variables_4() { return &___m_Variables_4; }
	inline void set_m_Variables_4(List_1_t4101323382 * value)
	{
		___m_Variables_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Variables_4), value);
	}

	inline static int32_t get_offset_of_U3CConfigLoadedU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Config_t3637807320, ___U3CConfigLoadedU3Ek__BackingField_6)); }
	inline bool get_U3CConfigLoadedU3Ek__BackingField_6() const { return ___U3CConfigLoadedU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CConfigLoadedU3Ek__BackingField_6() { return &___U3CConfigLoadedU3Ek__BackingField_6; }
	inline void set_U3CConfigLoadedU3Ek__BackingField_6(bool value)
	{
		___U3CConfigLoadedU3Ek__BackingField_6 = value;
	}
};

struct Config_t3637807320_StaticFields
{
public:
	// FullSerializer.fsSerializer IBM.Watson.DeveloperCloud.Utilities.Config::sm_Serializer
	fsSerializer_t4193731081 * ___sm_Serializer_5;

public:
	inline static int32_t get_offset_of_sm_Serializer_5() { return static_cast<int32_t>(offsetof(Config_t3637807320_StaticFields, ___sm_Serializer_5)); }
	inline fsSerializer_t4193731081 * get_sm_Serializer_5() const { return ___sm_Serializer_5; }
	inline fsSerializer_t4193731081 ** get_address_of_sm_Serializer_5() { return &___sm_Serializer_5; }
	inline void set_sm_Serializer_5(fsSerializer_t4193731081 * value)
	{
		___sm_Serializer_5 = value;
		Il2CppCodeGenWriteBarrier((&___sm_Serializer_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIG_T3637807320_H
#ifndef VARIABLE_T437234954_H
#define VARIABLE_T437234954_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Utilities.Config/Variable
struct  Variable_t437234954  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Utilities.Config/Variable::<Key>k__BackingField
	String_t* ___U3CKeyU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Utilities.Config/Variable::<Value>k__BackingField
	String_t* ___U3CValueU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CKeyU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Variable_t437234954, ___U3CKeyU3Ek__BackingField_0)); }
	inline String_t* get_U3CKeyU3Ek__BackingField_0() const { return ___U3CKeyU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CKeyU3Ek__BackingField_0() { return &___U3CKeyU3Ek__BackingField_0; }
	inline void set_U3CKeyU3Ek__BackingField_0(String_t* value)
	{
		___U3CKeyU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CKeyU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CValueU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Variable_t437234954, ___U3CValueU3Ek__BackingField_1)); }
	inline String_t* get_U3CValueU3Ek__BackingField_1() const { return ___U3CValueU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CValueU3Ek__BackingField_1() { return &___U3CValueU3Ek__BackingField_1; }
	inline void set_U3CValueU3Ek__BackingField_1(String_t* value)
	{
		___U3CValueU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CValueU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VARIABLE_T437234954_H
#ifndef PATH_T2333244422_H
#define PATH_T2333244422_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Utilities.Constants/Path
struct  Path_t2333244422  : public RuntimeObject
{
public:

public:
};

struct Path_t2333244422_StaticFields
{
public:
	// System.String IBM.Watson.DeveloperCloud.Utilities.Constants/Path::CACHE_FOLDER
	String_t* ___CACHE_FOLDER_1;
	// System.String IBM.Watson.DeveloperCloud.Utilities.Constants/Path::LOG_FOLDER
	String_t* ___LOG_FOLDER_2;

public:
	inline static int32_t get_offset_of_CACHE_FOLDER_1() { return static_cast<int32_t>(offsetof(Path_t2333244422_StaticFields, ___CACHE_FOLDER_1)); }
	inline String_t* get_CACHE_FOLDER_1() const { return ___CACHE_FOLDER_1; }
	inline String_t** get_address_of_CACHE_FOLDER_1() { return &___CACHE_FOLDER_1; }
	inline void set_CACHE_FOLDER_1(String_t* value)
	{
		___CACHE_FOLDER_1 = value;
		Il2CppCodeGenWriteBarrier((&___CACHE_FOLDER_1), value);
	}

	inline static int32_t get_offset_of_LOG_FOLDER_2() { return static_cast<int32_t>(offsetof(Path_t2333244422_StaticFields, ___LOG_FOLDER_2)); }
	inline String_t* get_LOG_FOLDER_2() const { return ___LOG_FOLDER_2; }
	inline String_t** get_address_of_LOG_FOLDER_2() { return &___LOG_FOLDER_2; }
	inline void set_LOG_FOLDER_2(String_t* value)
	{
		___LOG_FOLDER_2 = value;
		Il2CppCodeGenWriteBarrier((&___LOG_FOLDER_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATH_T2333244422_H
#ifndef RESOURCES_T2675506754_H
#define RESOURCES_T2675506754_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Utilities.Constants/Resources
struct  Resources_t2675506754  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESOURCES_T2675506754_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef STRING_T3367353832_H
#define STRING_T3367353832_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Utilities.Constants/String
struct  String_t3367353832  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T3367353832_H
#ifndef APPLICATIONDATAVALUE_T1721671971_H
#define APPLICATIONDATAVALUE_T1721671971_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.ApplicationDataValue
struct  ApplicationDataValue_t1721671971  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPLICATIONDATAVALUE_T1721671971_H
#ifndef APPLICATIONDATA_T1885408412_H
#define APPLICATIONDATA_T1885408412_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.ApplicationData
struct  ApplicationData_t1885408412  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPLICATIONDATA_T1885408412_H
#ifndef UNITTEST_T237671048_H
#define UNITTEST_T237671048_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.UnitTests.UnitTest
struct  UnitTest_t237671048  : public RuntimeObject
{
public:
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.UnitTest::<TestFailed>k__BackingField
	bool ___U3CTestFailedU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CTestFailedU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(UnitTest_t237671048, ___U3CTestFailedU3Ek__BackingField_0)); }
	inline bool get_U3CTestFailedU3Ek__BackingField_0() const { return ___U3CTestFailedU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CTestFailedU3Ek__BackingField_0() { return &___U3CTestFailedU3Ek__BackingField_0; }
	inline void set_U3CTestFailedU3Ek__BackingField_0(bool value)
	{
		___U3CTestFailedU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITTEST_T237671048_H
#ifndef U3CPROCESSASYNCEVENTSU3EC__ITERATOR0_T1795645094_H
#define U3CPROCESSASYNCEVENTSU3EC__ITERATOR0_T1795645094_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Utilities.EventManager/<ProcessAsyncEvents>c__Iterator0
struct  U3CProcessAsyncEventsU3Ec__Iterator0_t1795645094  : public RuntimeObject
{
public:
	// IBM.Watson.DeveloperCloud.Utilities.EventManager IBM.Watson.DeveloperCloud.Utilities.EventManager/<ProcessAsyncEvents>c__Iterator0::$this
	EventManager_t605335149 * ___U24this_0;
	// System.Object IBM.Watson.DeveloperCloud.Utilities.EventManager/<ProcessAsyncEvents>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean IBM.Watson.DeveloperCloud.Utilities.EventManager/<ProcessAsyncEvents>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 IBM.Watson.DeveloperCloud.Utilities.EventManager/<ProcessAsyncEvents>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CProcessAsyncEventsU3Ec__Iterator0_t1795645094, ___U24this_0)); }
	inline EventManager_t605335149 * get_U24this_0() const { return ___U24this_0; }
	inline EventManager_t605335149 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(EventManager_t605335149 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CProcessAsyncEventsU3Ec__Iterator0_t1795645094, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CProcessAsyncEventsU3Ec__Iterator0_t1795645094, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CProcessAsyncEventsU3Ec__Iterator0_t1795645094, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPROCESSASYNCEVENTSU3EC__ITERATOR0_T1795645094_H
#ifndef U3CRUNTESTU3EC__ITERATOR0_T270095488_H
#define U3CRUNTESTU3EC__ITERATOR0_T270095488_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.UnitTests.TestVisualRecognition/<RunTest>c__Iterator0
struct  U3CRunTestU3Ec__Iterator0_t270095488  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestVisualRecognition/<RunTest>c__Iterator0::<m_positiveExamplesPath>__1
	String_t* ___U3Cm_positiveExamplesPathU3E__1_0;
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestVisualRecognition/<RunTest>c__Iterator0::<m_negativeExamplesPath>__1
	String_t* ___U3Cm_negativeExamplesPathU3E__1_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> IBM.Watson.DeveloperCloud.UnitTests.TestVisualRecognition/<RunTest>c__Iterator0::<positiveExamples>__1
	Dictionary_2_t3943999495 * ___U3CpositiveExamplesU3E__1_2;
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestVisualRecognition/<RunTest>c__Iterator0::<m_positiveUpdated>__2
	String_t* ___U3Cm_positiveUpdatedU3E__2_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> IBM.Watson.DeveloperCloud.UnitTests.TestVisualRecognition/<RunTest>c__Iterator0::<positiveUpdatedExamples>__2
	Dictionary_2_t3943999495 * ___U3CpositiveUpdatedExamplesU3E__2_4;
	// System.String[] IBM.Watson.DeveloperCloud.UnitTests.TestVisualRecognition/<RunTest>c__Iterator0::<m_owners>__2
	StringU5BU5D_t1642385972* ___U3Cm_ownersU3E__2_5;
	// System.String[] IBM.Watson.DeveloperCloud.UnitTests.TestVisualRecognition/<RunTest>c__Iterator0::<m_classifierIds>__2
	StringU5BU5D_t1642385972* ___U3Cm_classifierIdsU3E__2_6;
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestVisualRecognition/<RunTest>c__Iterator0::<m_classifyImagePath>__2
	String_t* ___U3Cm_classifyImagePathU3E__2_7;
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestVisualRecognition/<RunTest>c__Iterator0::<m_detectFaceImagePath>__0
	String_t* ___U3Cm_detectFaceImagePathU3E__0_8;
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestVisualRecognition/<RunTest>c__Iterator0::<m_recognizeTextImagePath>__0
	String_t* ___U3Cm_recognizeTextImagePathU3E__0_9;
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestVisualRecognition/<RunTest>c__Iterator0::<m_collectionImagePath>__0
	String_t* ___U3Cm_collectionImagePathU3E__0_10;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> IBM.Watson.DeveloperCloud.UnitTests.TestVisualRecognition/<RunTest>c__Iterator0::<imageMetadata>__0
	Dictionary_2_t3943999495 * ___U3CimageMetadataU3E__0_11;
	// IBM.Watson.DeveloperCloud.UnitTests.TestVisualRecognition IBM.Watson.DeveloperCloud.UnitTests.TestVisualRecognition/<RunTest>c__Iterator0::$this
	TestVisualRecognition_t3349626417 * ___U24this_12;
	// System.Object IBM.Watson.DeveloperCloud.UnitTests.TestVisualRecognition/<RunTest>c__Iterator0::$current
	RuntimeObject * ___U24current_13;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestVisualRecognition/<RunTest>c__Iterator0::$disposing
	bool ___U24disposing_14;
	// System.Int32 IBM.Watson.DeveloperCloud.UnitTests.TestVisualRecognition/<RunTest>c__Iterator0::$PC
	int32_t ___U24PC_15;

public:
	inline static int32_t get_offset_of_U3Cm_positiveExamplesPathU3E__1_0() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t270095488, ___U3Cm_positiveExamplesPathU3E__1_0)); }
	inline String_t* get_U3Cm_positiveExamplesPathU3E__1_0() const { return ___U3Cm_positiveExamplesPathU3E__1_0; }
	inline String_t** get_address_of_U3Cm_positiveExamplesPathU3E__1_0() { return &___U3Cm_positiveExamplesPathU3E__1_0; }
	inline void set_U3Cm_positiveExamplesPathU3E__1_0(String_t* value)
	{
		___U3Cm_positiveExamplesPathU3E__1_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cm_positiveExamplesPathU3E__1_0), value);
	}

	inline static int32_t get_offset_of_U3Cm_negativeExamplesPathU3E__1_1() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t270095488, ___U3Cm_negativeExamplesPathU3E__1_1)); }
	inline String_t* get_U3Cm_negativeExamplesPathU3E__1_1() const { return ___U3Cm_negativeExamplesPathU3E__1_1; }
	inline String_t** get_address_of_U3Cm_negativeExamplesPathU3E__1_1() { return &___U3Cm_negativeExamplesPathU3E__1_1; }
	inline void set_U3Cm_negativeExamplesPathU3E__1_1(String_t* value)
	{
		___U3Cm_negativeExamplesPathU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cm_negativeExamplesPathU3E__1_1), value);
	}

	inline static int32_t get_offset_of_U3CpositiveExamplesU3E__1_2() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t270095488, ___U3CpositiveExamplesU3E__1_2)); }
	inline Dictionary_2_t3943999495 * get_U3CpositiveExamplesU3E__1_2() const { return ___U3CpositiveExamplesU3E__1_2; }
	inline Dictionary_2_t3943999495 ** get_address_of_U3CpositiveExamplesU3E__1_2() { return &___U3CpositiveExamplesU3E__1_2; }
	inline void set_U3CpositiveExamplesU3E__1_2(Dictionary_2_t3943999495 * value)
	{
		___U3CpositiveExamplesU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpositiveExamplesU3E__1_2), value);
	}

	inline static int32_t get_offset_of_U3Cm_positiveUpdatedU3E__2_3() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t270095488, ___U3Cm_positiveUpdatedU3E__2_3)); }
	inline String_t* get_U3Cm_positiveUpdatedU3E__2_3() const { return ___U3Cm_positiveUpdatedU3E__2_3; }
	inline String_t** get_address_of_U3Cm_positiveUpdatedU3E__2_3() { return &___U3Cm_positiveUpdatedU3E__2_3; }
	inline void set_U3Cm_positiveUpdatedU3E__2_3(String_t* value)
	{
		___U3Cm_positiveUpdatedU3E__2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cm_positiveUpdatedU3E__2_3), value);
	}

	inline static int32_t get_offset_of_U3CpositiveUpdatedExamplesU3E__2_4() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t270095488, ___U3CpositiveUpdatedExamplesU3E__2_4)); }
	inline Dictionary_2_t3943999495 * get_U3CpositiveUpdatedExamplesU3E__2_4() const { return ___U3CpositiveUpdatedExamplesU3E__2_4; }
	inline Dictionary_2_t3943999495 ** get_address_of_U3CpositiveUpdatedExamplesU3E__2_4() { return &___U3CpositiveUpdatedExamplesU3E__2_4; }
	inline void set_U3CpositiveUpdatedExamplesU3E__2_4(Dictionary_2_t3943999495 * value)
	{
		___U3CpositiveUpdatedExamplesU3E__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpositiveUpdatedExamplesU3E__2_4), value);
	}

	inline static int32_t get_offset_of_U3Cm_ownersU3E__2_5() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t270095488, ___U3Cm_ownersU3E__2_5)); }
	inline StringU5BU5D_t1642385972* get_U3Cm_ownersU3E__2_5() const { return ___U3Cm_ownersU3E__2_5; }
	inline StringU5BU5D_t1642385972** get_address_of_U3Cm_ownersU3E__2_5() { return &___U3Cm_ownersU3E__2_5; }
	inline void set_U3Cm_ownersU3E__2_5(StringU5BU5D_t1642385972* value)
	{
		___U3Cm_ownersU3E__2_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cm_ownersU3E__2_5), value);
	}

	inline static int32_t get_offset_of_U3Cm_classifierIdsU3E__2_6() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t270095488, ___U3Cm_classifierIdsU3E__2_6)); }
	inline StringU5BU5D_t1642385972* get_U3Cm_classifierIdsU3E__2_6() const { return ___U3Cm_classifierIdsU3E__2_6; }
	inline StringU5BU5D_t1642385972** get_address_of_U3Cm_classifierIdsU3E__2_6() { return &___U3Cm_classifierIdsU3E__2_6; }
	inline void set_U3Cm_classifierIdsU3E__2_6(StringU5BU5D_t1642385972* value)
	{
		___U3Cm_classifierIdsU3E__2_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cm_classifierIdsU3E__2_6), value);
	}

	inline static int32_t get_offset_of_U3Cm_classifyImagePathU3E__2_7() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t270095488, ___U3Cm_classifyImagePathU3E__2_7)); }
	inline String_t* get_U3Cm_classifyImagePathU3E__2_7() const { return ___U3Cm_classifyImagePathU3E__2_7; }
	inline String_t** get_address_of_U3Cm_classifyImagePathU3E__2_7() { return &___U3Cm_classifyImagePathU3E__2_7; }
	inline void set_U3Cm_classifyImagePathU3E__2_7(String_t* value)
	{
		___U3Cm_classifyImagePathU3E__2_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cm_classifyImagePathU3E__2_7), value);
	}

	inline static int32_t get_offset_of_U3Cm_detectFaceImagePathU3E__0_8() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t270095488, ___U3Cm_detectFaceImagePathU3E__0_8)); }
	inline String_t* get_U3Cm_detectFaceImagePathU3E__0_8() const { return ___U3Cm_detectFaceImagePathU3E__0_8; }
	inline String_t** get_address_of_U3Cm_detectFaceImagePathU3E__0_8() { return &___U3Cm_detectFaceImagePathU3E__0_8; }
	inline void set_U3Cm_detectFaceImagePathU3E__0_8(String_t* value)
	{
		___U3Cm_detectFaceImagePathU3E__0_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cm_detectFaceImagePathU3E__0_8), value);
	}

	inline static int32_t get_offset_of_U3Cm_recognizeTextImagePathU3E__0_9() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t270095488, ___U3Cm_recognizeTextImagePathU3E__0_9)); }
	inline String_t* get_U3Cm_recognizeTextImagePathU3E__0_9() const { return ___U3Cm_recognizeTextImagePathU3E__0_9; }
	inline String_t** get_address_of_U3Cm_recognizeTextImagePathU3E__0_9() { return &___U3Cm_recognizeTextImagePathU3E__0_9; }
	inline void set_U3Cm_recognizeTextImagePathU3E__0_9(String_t* value)
	{
		___U3Cm_recognizeTextImagePathU3E__0_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cm_recognizeTextImagePathU3E__0_9), value);
	}

	inline static int32_t get_offset_of_U3Cm_collectionImagePathU3E__0_10() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t270095488, ___U3Cm_collectionImagePathU3E__0_10)); }
	inline String_t* get_U3Cm_collectionImagePathU3E__0_10() const { return ___U3Cm_collectionImagePathU3E__0_10; }
	inline String_t** get_address_of_U3Cm_collectionImagePathU3E__0_10() { return &___U3Cm_collectionImagePathU3E__0_10; }
	inline void set_U3Cm_collectionImagePathU3E__0_10(String_t* value)
	{
		___U3Cm_collectionImagePathU3E__0_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cm_collectionImagePathU3E__0_10), value);
	}

	inline static int32_t get_offset_of_U3CimageMetadataU3E__0_11() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t270095488, ___U3CimageMetadataU3E__0_11)); }
	inline Dictionary_2_t3943999495 * get_U3CimageMetadataU3E__0_11() const { return ___U3CimageMetadataU3E__0_11; }
	inline Dictionary_2_t3943999495 ** get_address_of_U3CimageMetadataU3E__0_11() { return &___U3CimageMetadataU3E__0_11; }
	inline void set_U3CimageMetadataU3E__0_11(Dictionary_2_t3943999495 * value)
	{
		___U3CimageMetadataU3E__0_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CimageMetadataU3E__0_11), value);
	}

	inline static int32_t get_offset_of_U24this_12() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t270095488, ___U24this_12)); }
	inline TestVisualRecognition_t3349626417 * get_U24this_12() const { return ___U24this_12; }
	inline TestVisualRecognition_t3349626417 ** get_address_of_U24this_12() { return &___U24this_12; }
	inline void set_U24this_12(TestVisualRecognition_t3349626417 * value)
	{
		___U24this_12 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_12), value);
	}

	inline static int32_t get_offset_of_U24current_13() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t270095488, ___U24current_13)); }
	inline RuntimeObject * get_U24current_13() const { return ___U24current_13; }
	inline RuntimeObject ** get_address_of_U24current_13() { return &___U24current_13; }
	inline void set_U24current_13(RuntimeObject * value)
	{
		___U24current_13 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_13), value);
	}

	inline static int32_t get_offset_of_U24disposing_14() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t270095488, ___U24disposing_14)); }
	inline bool get_U24disposing_14() const { return ___U24disposing_14; }
	inline bool* get_address_of_U24disposing_14() { return &___U24disposing_14; }
	inline void set_U24disposing_14(bool value)
	{
		___U24disposing_14 = value;
	}

	inline static int32_t get_offset_of_U24PC_15() { return static_cast<int32_t>(offsetof(U3CRunTestU3Ec__Iterator0_t270095488, ___U24PC_15)); }
	inline int32_t get_U24PC_15() const { return ___U24PC_15; }
	inline int32_t* get_address_of_U24PC_15() { return &___U24PC_15; }
	inline void set_U24PC_15(int32_t value)
	{
		___U24PC_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRUNTESTU3EC__ITERATOR0_T270095488_H
#ifndef CREDENTIALINFO_T34154441_H
#define CREDENTIALINFO_T34154441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Utilities.Config/CredentialInfo
struct  CredentialInfo_t34154441  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Utilities.Config/CredentialInfo::m_ServiceID
	String_t* ___m_ServiceID_0;
	// System.String IBM.Watson.DeveloperCloud.Utilities.Config/CredentialInfo::m_URL
	String_t* ___m_URL_1;
	// System.String IBM.Watson.DeveloperCloud.Utilities.Config/CredentialInfo::m_User
	String_t* ___m_User_2;
	// System.String IBM.Watson.DeveloperCloud.Utilities.Config/CredentialInfo::m_Password
	String_t* ___m_Password_3;
	// System.String IBM.Watson.DeveloperCloud.Utilities.Config/CredentialInfo::m_Apikey
	String_t* ___m_Apikey_4;
	// System.String IBM.Watson.DeveloperCloud.Utilities.Config/CredentialInfo::m_Note
	String_t* ___m_Note_5;

public:
	inline static int32_t get_offset_of_m_ServiceID_0() { return static_cast<int32_t>(offsetof(CredentialInfo_t34154441, ___m_ServiceID_0)); }
	inline String_t* get_m_ServiceID_0() const { return ___m_ServiceID_0; }
	inline String_t** get_address_of_m_ServiceID_0() { return &___m_ServiceID_0; }
	inline void set_m_ServiceID_0(String_t* value)
	{
		___m_ServiceID_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ServiceID_0), value);
	}

	inline static int32_t get_offset_of_m_URL_1() { return static_cast<int32_t>(offsetof(CredentialInfo_t34154441, ___m_URL_1)); }
	inline String_t* get_m_URL_1() const { return ___m_URL_1; }
	inline String_t** get_address_of_m_URL_1() { return &___m_URL_1; }
	inline void set_m_URL_1(String_t* value)
	{
		___m_URL_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_URL_1), value);
	}

	inline static int32_t get_offset_of_m_User_2() { return static_cast<int32_t>(offsetof(CredentialInfo_t34154441, ___m_User_2)); }
	inline String_t* get_m_User_2() const { return ___m_User_2; }
	inline String_t** get_address_of_m_User_2() { return &___m_User_2; }
	inline void set_m_User_2(String_t* value)
	{
		___m_User_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_User_2), value);
	}

	inline static int32_t get_offset_of_m_Password_3() { return static_cast<int32_t>(offsetof(CredentialInfo_t34154441, ___m_Password_3)); }
	inline String_t* get_m_Password_3() const { return ___m_Password_3; }
	inline String_t** get_address_of_m_Password_3() { return &___m_Password_3; }
	inline void set_m_Password_3(String_t* value)
	{
		___m_Password_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Password_3), value);
	}

	inline static int32_t get_offset_of_m_Apikey_4() { return static_cast<int32_t>(offsetof(CredentialInfo_t34154441, ___m_Apikey_4)); }
	inline String_t* get_m_Apikey_4() const { return ___m_Apikey_4; }
	inline String_t** get_address_of_m_Apikey_4() { return &___m_Apikey_4; }
	inline void set_m_Apikey_4(String_t* value)
	{
		___m_Apikey_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Apikey_4), value);
	}

	inline static int32_t get_offset_of_m_Note_5() { return static_cast<int32_t>(offsetof(CredentialInfo_t34154441, ___m_Note_5)); }
	inline String_t* get_m_Note_5() const { return ___m_Note_5; }
	inline String_t** get_address_of_m_Note_5() { return &___m_Note_5; }
	inline void set_m_Note_5(String_t* value)
	{
		___m_Note_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Note_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREDENTIALINFO_T34154441_H
#ifndef DATACACHE_T4250340070_H
#define DATACACHE_T4250340070_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Utilities.DataCache
struct  DataCache_t4250340070  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Utilities.DataCache::m_CachePath
	String_t* ___m_CachePath_0;
	// System.Int64 IBM.Watson.DeveloperCloud.Utilities.DataCache::m_MaxCacheSize
	int64_t ___m_MaxCacheSize_1;
	// System.Double IBM.Watson.DeveloperCloud.Utilities.DataCache::m_MaxCacheAge
	double ___m_MaxCacheAge_2;
	// System.Int64 IBM.Watson.DeveloperCloud.Utilities.DataCache::m_CurrentCacheSize
	int64_t ___m_CurrentCacheSize_3;
	// System.Collections.Generic.Dictionary`2<System.String,IBM.Watson.DeveloperCloud.Utilities.DataCache/CacheItem> IBM.Watson.DeveloperCloud.Utilities.DataCache::m_Cache
	Dictionary_2_t92195697 * ___m_Cache_4;

public:
	inline static int32_t get_offset_of_m_CachePath_0() { return static_cast<int32_t>(offsetof(DataCache_t4250340070, ___m_CachePath_0)); }
	inline String_t* get_m_CachePath_0() const { return ___m_CachePath_0; }
	inline String_t** get_address_of_m_CachePath_0() { return &___m_CachePath_0; }
	inline void set_m_CachePath_0(String_t* value)
	{
		___m_CachePath_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_CachePath_0), value);
	}

	inline static int32_t get_offset_of_m_MaxCacheSize_1() { return static_cast<int32_t>(offsetof(DataCache_t4250340070, ___m_MaxCacheSize_1)); }
	inline int64_t get_m_MaxCacheSize_1() const { return ___m_MaxCacheSize_1; }
	inline int64_t* get_address_of_m_MaxCacheSize_1() { return &___m_MaxCacheSize_1; }
	inline void set_m_MaxCacheSize_1(int64_t value)
	{
		___m_MaxCacheSize_1 = value;
	}

	inline static int32_t get_offset_of_m_MaxCacheAge_2() { return static_cast<int32_t>(offsetof(DataCache_t4250340070, ___m_MaxCacheAge_2)); }
	inline double get_m_MaxCacheAge_2() const { return ___m_MaxCacheAge_2; }
	inline double* get_address_of_m_MaxCacheAge_2() { return &___m_MaxCacheAge_2; }
	inline void set_m_MaxCacheAge_2(double value)
	{
		___m_MaxCacheAge_2 = value;
	}

	inline static int32_t get_offset_of_m_CurrentCacheSize_3() { return static_cast<int32_t>(offsetof(DataCache_t4250340070, ___m_CurrentCacheSize_3)); }
	inline int64_t get_m_CurrentCacheSize_3() const { return ___m_CurrentCacheSize_3; }
	inline int64_t* get_address_of_m_CurrentCacheSize_3() { return &___m_CurrentCacheSize_3; }
	inline void set_m_CurrentCacheSize_3(int64_t value)
	{
		___m_CurrentCacheSize_3 = value;
	}

	inline static int32_t get_offset_of_m_Cache_4() { return static_cast<int32_t>(offsetof(DataCache_t4250340070, ___m_Cache_4)); }
	inline Dictionary_2_t92195697 * get_m_Cache_4() const { return ___m_Cache_4; }
	inline Dictionary_2_t92195697 ** get_address_of_m_Cache_4() { return &___m_Cache_4; }
	inline void set_m_Cache_4(Dictionary_2_t92195697 * value)
	{
		___m_Cache_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Cache_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATACACHE_T4250340070_H
#ifndef CREDENTIALS_T1494051450_H
#define CREDENTIALS_T1494051450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Utilities.Credentials
struct  Credentials_t1494051450  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Utilities.Credentials::<User>k__BackingField
	String_t* ___U3CUserU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Utilities.Credentials::<Password>k__BackingField
	String_t* ___U3CPasswordU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CUserU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Credentials_t1494051450, ___U3CUserU3Ek__BackingField_0)); }
	inline String_t* get_U3CUserU3Ek__BackingField_0() const { return ___U3CUserU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CUserU3Ek__BackingField_0() { return &___U3CUserU3Ek__BackingField_0; }
	inline void set_U3CUserU3Ek__BackingField_0(String_t* value)
	{
		___U3CUserU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUserU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CPasswordU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Credentials_t1494051450, ___U3CPasswordU3Ek__BackingField_1)); }
	inline String_t* get_U3CPasswordU3Ek__BackingField_1() const { return ___U3CPasswordU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CPasswordU3Ek__BackingField_1() { return &___U3CPasswordU3Ek__BackingField_1; }
	inline void set_U3CPasswordU3Ek__BackingField_1(String_t* value)
	{
		___U3CPasswordU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPasswordU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREDENTIALS_T1494051450_H
#ifndef EVENTMANAGER_T605335149_H
#define EVENTMANAGER_T605335149_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Utilities.EventManager
struct  EventManager_t605335149  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.Dictionary`2<System.Object,System.String>> IBM.Watson.DeveloperCloud.Utilities.EventManager::m_EventTypeName
	Dictionary_2_t3558638258 * ___m_EventTypeName_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<IBM.Watson.DeveloperCloud.Utilities.EventManager/OnReceiveEvent>> IBM.Watson.DeveloperCloud.Utilities.EventManager::m_EventMap
	Dictionary_2_t1226040769 * ___m_EventMap_1;
	// System.Collections.Generic.Queue`1<IBM.Watson.DeveloperCloud.Utilities.EventManager/AsyncEvent> IBM.Watson.DeveloperCloud.Utilities.EventManager::m_AsyncEvents
	Queue_1_t2152331082 * ___m_AsyncEvents_2;
	// System.Int32 IBM.Watson.DeveloperCloud.Utilities.EventManager::m_ProcesserCount
	int32_t ___m_ProcesserCount_3;

public:
	inline static int32_t get_offset_of_m_EventTypeName_0() { return static_cast<int32_t>(offsetof(EventManager_t605335149, ___m_EventTypeName_0)); }
	inline Dictionary_2_t3558638258 * get_m_EventTypeName_0() const { return ___m_EventTypeName_0; }
	inline Dictionary_2_t3558638258 ** get_address_of_m_EventTypeName_0() { return &___m_EventTypeName_0; }
	inline void set_m_EventTypeName_0(Dictionary_2_t3558638258 * value)
	{
		___m_EventTypeName_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventTypeName_0), value);
	}

	inline static int32_t get_offset_of_m_EventMap_1() { return static_cast<int32_t>(offsetof(EventManager_t605335149, ___m_EventMap_1)); }
	inline Dictionary_2_t1226040769 * get_m_EventMap_1() const { return ___m_EventMap_1; }
	inline Dictionary_2_t1226040769 ** get_address_of_m_EventMap_1() { return &___m_EventMap_1; }
	inline void set_m_EventMap_1(Dictionary_2_t1226040769 * value)
	{
		___m_EventMap_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventMap_1), value);
	}

	inline static int32_t get_offset_of_m_AsyncEvents_2() { return static_cast<int32_t>(offsetof(EventManager_t605335149, ___m_AsyncEvents_2)); }
	inline Queue_1_t2152331082 * get_m_AsyncEvents_2() const { return ___m_AsyncEvents_2; }
	inline Queue_1_t2152331082 ** get_address_of_m_AsyncEvents_2() { return &___m_AsyncEvents_2; }
	inline void set_m_AsyncEvents_2(Queue_1_t2152331082 * value)
	{
		___m_AsyncEvents_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_AsyncEvents_2), value);
	}

	inline static int32_t get_offset_of_m_ProcesserCount_3() { return static_cast<int32_t>(offsetof(EventManager_t605335149, ___m_ProcesserCount_3)); }
	inline int32_t get_m_ProcesserCount_3() const { return ___m_ProcesserCount_3; }
	inline int32_t* get_address_of_m_ProcesserCount_3() { return &___m_ProcesserCount_3; }
	inline void set_m_ProcesserCount_3(int32_t value)
	{
		___m_ProcesserCount_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTMANAGER_T605335149_H
#ifndef RUNUNITTEST_T690597011_H
#define RUNUNITTEST_T690597011_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RunUnitTest
struct  RunUnitTest_t690597011  : public RuntimeObject
{
public:

public:
};

struct RunUnitTest_t690597011_StaticFields
{
public:
	// IBM.Watson.DeveloperCloud.Editor.UnitTestManager/OnTestsComplete RunUnitTest::<>f__mg$cache0
	OnTestsComplete_t1412473303 * ___U3CU3Ef__mgU24cache0_0;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_0() { return static_cast<int32_t>(offsetof(RunUnitTest_t690597011_StaticFields, ___U3CU3Ef__mgU24cache0_0)); }
	inline OnTestsComplete_t1412473303 * get_U3CU3Ef__mgU24cache0_0() const { return ___U3CU3Ef__mgU24cache0_0; }
	inline OnTestsComplete_t1412473303 ** get_address_of_U3CU3Ef__mgU24cache0_0() { return &___U3CU3Ef__mgU24cache0_0; }
	inline void set_U3CU3Ef__mgU24cache0_0(OnTestsComplete_t1412473303 * value)
	{
		___U3CU3Ef__mgU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNUNITTEST_T690597011_H
#ifndef AUDIOCLIPUTIL_T2269251034_H
#define AUDIOCLIPUTIL_T2269251034_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Utilities.AudioClipUtil
struct  AudioClipUtil_t2269251034  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOCLIPUTIL_T2269251034_H
#ifndef ASYNCEVENT_T2332674247_H
#define ASYNCEVENT_T2332674247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Utilities.EventManager/AsyncEvent
struct  AsyncEvent_t2332674247  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Utilities.EventManager/AsyncEvent::m_EventName
	String_t* ___m_EventName_0;
	// System.Object[] IBM.Watson.DeveloperCloud.Utilities.EventManager/AsyncEvent::m_Args
	ObjectU5BU5D_t3614634134* ___m_Args_1;

public:
	inline static int32_t get_offset_of_m_EventName_0() { return static_cast<int32_t>(offsetof(AsyncEvent_t2332674247, ___m_EventName_0)); }
	inline String_t* get_m_EventName_0() const { return ___m_EventName_0; }
	inline String_t** get_address_of_m_EventName_0() { return &___m_EventName_0; }
	inline void set_m_EventName_0(String_t* value)
	{
		___m_EventName_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventName_0), value);
	}

	inline static int32_t get_offset_of_m_Args_1() { return static_cast<int32_t>(offsetof(AsyncEvent_t2332674247, ___m_Args_1)); }
	inline ObjectU5BU5D_t3614634134* get_m_Args_1() const { return ___m_Args_1; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_Args_1() { return &___m_Args_1; }
	inline void set_m_Args_1(ObjectU5BU5D_t3614634134* value)
	{
		___m_Args_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Args_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCEVENT_T2332674247_H
#ifndef TESTRETRIEVEANDRANK_T2134779753_H
#define TESTRETRIEVEANDRANK_T2134779753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.UnitTests.TestRetrieveAndRank
struct  TestRetrieveAndRank_t2134779753  : public UnitTest_t237671048
{
public:
	// IBM.Watson.DeveloperCloud.Services.RetrieveAndRank.v1.RetrieveAndRank IBM.Watson.DeveloperCloud.UnitTests.TestRetrieveAndRank::m_RetrieveAndRank
	RetrieveAndRank_t1381045327 * ___m_RetrieveAndRank_1;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestRetrieveAndRank::IsFullTest
	bool ___IsFullTest_2;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestRetrieveAndRank::m_GetClustersTested
	bool ___m_GetClustersTested_3;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestRetrieveAndRank::m_GetClusterTested
	bool ___m_GetClusterTested_4;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestRetrieveAndRank::m_ListClusterConfigsTested
	bool ___m_ListClusterConfigsTested_5;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestRetrieveAndRank::m_GetClusterConfigTested
	bool ___m_GetClusterConfigTested_6;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestRetrieveAndRank::m_ListCollectionRequestTested
	bool ___m_ListCollectionRequestTested_7;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestRetrieveAndRank::m_StandardSearchTested
	bool ___m_StandardSearchTested_8;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestRetrieveAndRank::m_RankedSearchTested
	bool ___m_RankedSearchTested_9;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestRetrieveAndRank::m_GetRankersTested
	bool ___m_GetRankersTested_10;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestRetrieveAndRank::m_RankTested
	bool ___m_RankTested_11;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestRetrieveAndRank::m_GetRankerInfoTested
	bool ___m_GetRankerInfoTested_12;
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestRetrieveAndRank::m_ConfigToCreateName
	String_t* ___m_ConfigToCreateName_13;
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestRetrieveAndRank::m_CollectionToCreateName
	String_t* ___m_CollectionToCreateName_14;
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestRetrieveAndRank::m_CreatedRankerID
	String_t* ___m_CreatedRankerID_15;
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestRetrieveAndRank::m_CreatedClusterID
	String_t* ___m_CreatedClusterID_16;
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestRetrieveAndRank::m_ExampleClusterID
	String_t* ___m_ExampleClusterID_17;
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestRetrieveAndRank::m_ExampleConfigName
	String_t* ___m_ExampleConfigName_18;
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestRetrieveAndRank::m_ExampleRankerID
	String_t* ___m_ExampleRankerID_19;
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestRetrieveAndRank::m_ExampleCollectionName
	String_t* ___m_ExampleCollectionName_20;
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestRetrieveAndRank::m_IntegrationTestQuery
	String_t* ___m_IntegrationTestQuery_21;
	// System.String[] IBM.Watson.DeveloperCloud.UnitTests.TestRetrieveAndRank::m_Fl
	StringU5BU5D_t1642385972* ___m_Fl_22;
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestRetrieveAndRank::m_IntegrationTestClusterConfigPath
	String_t* ___m_IntegrationTestClusterConfigPath_23;
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestRetrieveAndRank::m_IntegrationTestRankerTrainingPath
	String_t* ___m_IntegrationTestRankerTrainingPath_24;
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestRetrieveAndRank::m_IntegrationTestRankerAnswerDataPath
	String_t* ___m_IntegrationTestRankerAnswerDataPath_25;
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestRetrieveAndRank::m_IntegrationTestIndexDataPath
	String_t* ___m_IntegrationTestIndexDataPath_26;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestRetrieveAndRank::m_IsClusterReady
	bool ___m_IsClusterReady_27;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestRetrieveAndRank::m_IsRankerReady
	bool ___m_IsRankerReady_28;

public:
	inline static int32_t get_offset_of_m_RetrieveAndRank_1() { return static_cast<int32_t>(offsetof(TestRetrieveAndRank_t2134779753, ___m_RetrieveAndRank_1)); }
	inline RetrieveAndRank_t1381045327 * get_m_RetrieveAndRank_1() const { return ___m_RetrieveAndRank_1; }
	inline RetrieveAndRank_t1381045327 ** get_address_of_m_RetrieveAndRank_1() { return &___m_RetrieveAndRank_1; }
	inline void set_m_RetrieveAndRank_1(RetrieveAndRank_t1381045327 * value)
	{
		___m_RetrieveAndRank_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_RetrieveAndRank_1), value);
	}

	inline static int32_t get_offset_of_IsFullTest_2() { return static_cast<int32_t>(offsetof(TestRetrieveAndRank_t2134779753, ___IsFullTest_2)); }
	inline bool get_IsFullTest_2() const { return ___IsFullTest_2; }
	inline bool* get_address_of_IsFullTest_2() { return &___IsFullTest_2; }
	inline void set_IsFullTest_2(bool value)
	{
		___IsFullTest_2 = value;
	}

	inline static int32_t get_offset_of_m_GetClustersTested_3() { return static_cast<int32_t>(offsetof(TestRetrieveAndRank_t2134779753, ___m_GetClustersTested_3)); }
	inline bool get_m_GetClustersTested_3() const { return ___m_GetClustersTested_3; }
	inline bool* get_address_of_m_GetClustersTested_3() { return &___m_GetClustersTested_3; }
	inline void set_m_GetClustersTested_3(bool value)
	{
		___m_GetClustersTested_3 = value;
	}

	inline static int32_t get_offset_of_m_GetClusterTested_4() { return static_cast<int32_t>(offsetof(TestRetrieveAndRank_t2134779753, ___m_GetClusterTested_4)); }
	inline bool get_m_GetClusterTested_4() const { return ___m_GetClusterTested_4; }
	inline bool* get_address_of_m_GetClusterTested_4() { return &___m_GetClusterTested_4; }
	inline void set_m_GetClusterTested_4(bool value)
	{
		___m_GetClusterTested_4 = value;
	}

	inline static int32_t get_offset_of_m_ListClusterConfigsTested_5() { return static_cast<int32_t>(offsetof(TestRetrieveAndRank_t2134779753, ___m_ListClusterConfigsTested_5)); }
	inline bool get_m_ListClusterConfigsTested_5() const { return ___m_ListClusterConfigsTested_5; }
	inline bool* get_address_of_m_ListClusterConfigsTested_5() { return &___m_ListClusterConfigsTested_5; }
	inline void set_m_ListClusterConfigsTested_5(bool value)
	{
		___m_ListClusterConfigsTested_5 = value;
	}

	inline static int32_t get_offset_of_m_GetClusterConfigTested_6() { return static_cast<int32_t>(offsetof(TestRetrieveAndRank_t2134779753, ___m_GetClusterConfigTested_6)); }
	inline bool get_m_GetClusterConfigTested_6() const { return ___m_GetClusterConfigTested_6; }
	inline bool* get_address_of_m_GetClusterConfigTested_6() { return &___m_GetClusterConfigTested_6; }
	inline void set_m_GetClusterConfigTested_6(bool value)
	{
		___m_GetClusterConfigTested_6 = value;
	}

	inline static int32_t get_offset_of_m_ListCollectionRequestTested_7() { return static_cast<int32_t>(offsetof(TestRetrieveAndRank_t2134779753, ___m_ListCollectionRequestTested_7)); }
	inline bool get_m_ListCollectionRequestTested_7() const { return ___m_ListCollectionRequestTested_7; }
	inline bool* get_address_of_m_ListCollectionRequestTested_7() { return &___m_ListCollectionRequestTested_7; }
	inline void set_m_ListCollectionRequestTested_7(bool value)
	{
		___m_ListCollectionRequestTested_7 = value;
	}

	inline static int32_t get_offset_of_m_StandardSearchTested_8() { return static_cast<int32_t>(offsetof(TestRetrieveAndRank_t2134779753, ___m_StandardSearchTested_8)); }
	inline bool get_m_StandardSearchTested_8() const { return ___m_StandardSearchTested_8; }
	inline bool* get_address_of_m_StandardSearchTested_8() { return &___m_StandardSearchTested_8; }
	inline void set_m_StandardSearchTested_8(bool value)
	{
		___m_StandardSearchTested_8 = value;
	}

	inline static int32_t get_offset_of_m_RankedSearchTested_9() { return static_cast<int32_t>(offsetof(TestRetrieveAndRank_t2134779753, ___m_RankedSearchTested_9)); }
	inline bool get_m_RankedSearchTested_9() const { return ___m_RankedSearchTested_9; }
	inline bool* get_address_of_m_RankedSearchTested_9() { return &___m_RankedSearchTested_9; }
	inline void set_m_RankedSearchTested_9(bool value)
	{
		___m_RankedSearchTested_9 = value;
	}

	inline static int32_t get_offset_of_m_GetRankersTested_10() { return static_cast<int32_t>(offsetof(TestRetrieveAndRank_t2134779753, ___m_GetRankersTested_10)); }
	inline bool get_m_GetRankersTested_10() const { return ___m_GetRankersTested_10; }
	inline bool* get_address_of_m_GetRankersTested_10() { return &___m_GetRankersTested_10; }
	inline void set_m_GetRankersTested_10(bool value)
	{
		___m_GetRankersTested_10 = value;
	}

	inline static int32_t get_offset_of_m_RankTested_11() { return static_cast<int32_t>(offsetof(TestRetrieveAndRank_t2134779753, ___m_RankTested_11)); }
	inline bool get_m_RankTested_11() const { return ___m_RankTested_11; }
	inline bool* get_address_of_m_RankTested_11() { return &___m_RankTested_11; }
	inline void set_m_RankTested_11(bool value)
	{
		___m_RankTested_11 = value;
	}

	inline static int32_t get_offset_of_m_GetRankerInfoTested_12() { return static_cast<int32_t>(offsetof(TestRetrieveAndRank_t2134779753, ___m_GetRankerInfoTested_12)); }
	inline bool get_m_GetRankerInfoTested_12() const { return ___m_GetRankerInfoTested_12; }
	inline bool* get_address_of_m_GetRankerInfoTested_12() { return &___m_GetRankerInfoTested_12; }
	inline void set_m_GetRankerInfoTested_12(bool value)
	{
		___m_GetRankerInfoTested_12 = value;
	}

	inline static int32_t get_offset_of_m_ConfigToCreateName_13() { return static_cast<int32_t>(offsetof(TestRetrieveAndRank_t2134779753, ___m_ConfigToCreateName_13)); }
	inline String_t* get_m_ConfigToCreateName_13() const { return ___m_ConfigToCreateName_13; }
	inline String_t** get_address_of_m_ConfigToCreateName_13() { return &___m_ConfigToCreateName_13; }
	inline void set_m_ConfigToCreateName_13(String_t* value)
	{
		___m_ConfigToCreateName_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_ConfigToCreateName_13), value);
	}

	inline static int32_t get_offset_of_m_CollectionToCreateName_14() { return static_cast<int32_t>(offsetof(TestRetrieveAndRank_t2134779753, ___m_CollectionToCreateName_14)); }
	inline String_t* get_m_CollectionToCreateName_14() const { return ___m_CollectionToCreateName_14; }
	inline String_t** get_address_of_m_CollectionToCreateName_14() { return &___m_CollectionToCreateName_14; }
	inline void set_m_CollectionToCreateName_14(String_t* value)
	{
		___m_CollectionToCreateName_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_CollectionToCreateName_14), value);
	}

	inline static int32_t get_offset_of_m_CreatedRankerID_15() { return static_cast<int32_t>(offsetof(TestRetrieveAndRank_t2134779753, ___m_CreatedRankerID_15)); }
	inline String_t* get_m_CreatedRankerID_15() const { return ___m_CreatedRankerID_15; }
	inline String_t** get_address_of_m_CreatedRankerID_15() { return &___m_CreatedRankerID_15; }
	inline void set_m_CreatedRankerID_15(String_t* value)
	{
		___m_CreatedRankerID_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_CreatedRankerID_15), value);
	}

	inline static int32_t get_offset_of_m_CreatedClusterID_16() { return static_cast<int32_t>(offsetof(TestRetrieveAndRank_t2134779753, ___m_CreatedClusterID_16)); }
	inline String_t* get_m_CreatedClusterID_16() const { return ___m_CreatedClusterID_16; }
	inline String_t** get_address_of_m_CreatedClusterID_16() { return &___m_CreatedClusterID_16; }
	inline void set_m_CreatedClusterID_16(String_t* value)
	{
		___m_CreatedClusterID_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_CreatedClusterID_16), value);
	}

	inline static int32_t get_offset_of_m_ExampleClusterID_17() { return static_cast<int32_t>(offsetof(TestRetrieveAndRank_t2134779753, ___m_ExampleClusterID_17)); }
	inline String_t* get_m_ExampleClusterID_17() const { return ___m_ExampleClusterID_17; }
	inline String_t** get_address_of_m_ExampleClusterID_17() { return &___m_ExampleClusterID_17; }
	inline void set_m_ExampleClusterID_17(String_t* value)
	{
		___m_ExampleClusterID_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_ExampleClusterID_17), value);
	}

	inline static int32_t get_offset_of_m_ExampleConfigName_18() { return static_cast<int32_t>(offsetof(TestRetrieveAndRank_t2134779753, ___m_ExampleConfigName_18)); }
	inline String_t* get_m_ExampleConfigName_18() const { return ___m_ExampleConfigName_18; }
	inline String_t** get_address_of_m_ExampleConfigName_18() { return &___m_ExampleConfigName_18; }
	inline void set_m_ExampleConfigName_18(String_t* value)
	{
		___m_ExampleConfigName_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_ExampleConfigName_18), value);
	}

	inline static int32_t get_offset_of_m_ExampleRankerID_19() { return static_cast<int32_t>(offsetof(TestRetrieveAndRank_t2134779753, ___m_ExampleRankerID_19)); }
	inline String_t* get_m_ExampleRankerID_19() const { return ___m_ExampleRankerID_19; }
	inline String_t** get_address_of_m_ExampleRankerID_19() { return &___m_ExampleRankerID_19; }
	inline void set_m_ExampleRankerID_19(String_t* value)
	{
		___m_ExampleRankerID_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_ExampleRankerID_19), value);
	}

	inline static int32_t get_offset_of_m_ExampleCollectionName_20() { return static_cast<int32_t>(offsetof(TestRetrieveAndRank_t2134779753, ___m_ExampleCollectionName_20)); }
	inline String_t* get_m_ExampleCollectionName_20() const { return ___m_ExampleCollectionName_20; }
	inline String_t** get_address_of_m_ExampleCollectionName_20() { return &___m_ExampleCollectionName_20; }
	inline void set_m_ExampleCollectionName_20(String_t* value)
	{
		___m_ExampleCollectionName_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_ExampleCollectionName_20), value);
	}

	inline static int32_t get_offset_of_m_IntegrationTestQuery_21() { return static_cast<int32_t>(offsetof(TestRetrieveAndRank_t2134779753, ___m_IntegrationTestQuery_21)); }
	inline String_t* get_m_IntegrationTestQuery_21() const { return ___m_IntegrationTestQuery_21; }
	inline String_t** get_address_of_m_IntegrationTestQuery_21() { return &___m_IntegrationTestQuery_21; }
	inline void set_m_IntegrationTestQuery_21(String_t* value)
	{
		___m_IntegrationTestQuery_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_IntegrationTestQuery_21), value);
	}

	inline static int32_t get_offset_of_m_Fl_22() { return static_cast<int32_t>(offsetof(TestRetrieveAndRank_t2134779753, ___m_Fl_22)); }
	inline StringU5BU5D_t1642385972* get_m_Fl_22() const { return ___m_Fl_22; }
	inline StringU5BU5D_t1642385972** get_address_of_m_Fl_22() { return &___m_Fl_22; }
	inline void set_m_Fl_22(StringU5BU5D_t1642385972* value)
	{
		___m_Fl_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_Fl_22), value);
	}

	inline static int32_t get_offset_of_m_IntegrationTestClusterConfigPath_23() { return static_cast<int32_t>(offsetof(TestRetrieveAndRank_t2134779753, ___m_IntegrationTestClusterConfigPath_23)); }
	inline String_t* get_m_IntegrationTestClusterConfigPath_23() const { return ___m_IntegrationTestClusterConfigPath_23; }
	inline String_t** get_address_of_m_IntegrationTestClusterConfigPath_23() { return &___m_IntegrationTestClusterConfigPath_23; }
	inline void set_m_IntegrationTestClusterConfigPath_23(String_t* value)
	{
		___m_IntegrationTestClusterConfigPath_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_IntegrationTestClusterConfigPath_23), value);
	}

	inline static int32_t get_offset_of_m_IntegrationTestRankerTrainingPath_24() { return static_cast<int32_t>(offsetof(TestRetrieveAndRank_t2134779753, ___m_IntegrationTestRankerTrainingPath_24)); }
	inline String_t* get_m_IntegrationTestRankerTrainingPath_24() const { return ___m_IntegrationTestRankerTrainingPath_24; }
	inline String_t** get_address_of_m_IntegrationTestRankerTrainingPath_24() { return &___m_IntegrationTestRankerTrainingPath_24; }
	inline void set_m_IntegrationTestRankerTrainingPath_24(String_t* value)
	{
		___m_IntegrationTestRankerTrainingPath_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_IntegrationTestRankerTrainingPath_24), value);
	}

	inline static int32_t get_offset_of_m_IntegrationTestRankerAnswerDataPath_25() { return static_cast<int32_t>(offsetof(TestRetrieveAndRank_t2134779753, ___m_IntegrationTestRankerAnswerDataPath_25)); }
	inline String_t* get_m_IntegrationTestRankerAnswerDataPath_25() const { return ___m_IntegrationTestRankerAnswerDataPath_25; }
	inline String_t** get_address_of_m_IntegrationTestRankerAnswerDataPath_25() { return &___m_IntegrationTestRankerAnswerDataPath_25; }
	inline void set_m_IntegrationTestRankerAnswerDataPath_25(String_t* value)
	{
		___m_IntegrationTestRankerAnswerDataPath_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_IntegrationTestRankerAnswerDataPath_25), value);
	}

	inline static int32_t get_offset_of_m_IntegrationTestIndexDataPath_26() { return static_cast<int32_t>(offsetof(TestRetrieveAndRank_t2134779753, ___m_IntegrationTestIndexDataPath_26)); }
	inline String_t* get_m_IntegrationTestIndexDataPath_26() const { return ___m_IntegrationTestIndexDataPath_26; }
	inline String_t** get_address_of_m_IntegrationTestIndexDataPath_26() { return &___m_IntegrationTestIndexDataPath_26; }
	inline void set_m_IntegrationTestIndexDataPath_26(String_t* value)
	{
		___m_IntegrationTestIndexDataPath_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_IntegrationTestIndexDataPath_26), value);
	}

	inline static int32_t get_offset_of_m_IsClusterReady_27() { return static_cast<int32_t>(offsetof(TestRetrieveAndRank_t2134779753, ___m_IsClusterReady_27)); }
	inline bool get_m_IsClusterReady_27() const { return ___m_IsClusterReady_27; }
	inline bool* get_address_of_m_IsClusterReady_27() { return &___m_IsClusterReady_27; }
	inline void set_m_IsClusterReady_27(bool value)
	{
		___m_IsClusterReady_27 = value;
	}

	inline static int32_t get_offset_of_m_IsRankerReady_28() { return static_cast<int32_t>(offsetof(TestRetrieveAndRank_t2134779753, ___m_IsRankerReady_28)); }
	inline bool get_m_IsRankerReady_28() const { return ___m_IsRankerReady_28; }
	inline bool* get_address_of_m_IsRankerReady_28() { return &___m_IsRankerReady_28; }
	inline void set_m_IsRankerReady_28(bool value)
	{
		___m_IsRankerReady_28 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TESTRETRIEVEANDRANK_T2134779753_H
#ifndef TESTTRADEOFFANALYTICS_T1911417121_H
#define TESTTRADEOFFANALYTICS_T1911417121_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.UnitTests.TestTradeoffAnalytics
struct  TestTradeoffAnalytics_t1911417121  : public UnitTest_t237671048
{
public:
	// IBM.Watson.DeveloperCloud.Services.TradeoffAnalytics.v1.TradeoffAnalytics IBM.Watson.DeveloperCloud.UnitTests.TestTradeoffAnalytics::m_TradeoffAnalytics
	TradeoffAnalytics_t100002595 * ___m_TradeoffAnalytics_1;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestTradeoffAnalytics::m_GetDilemmaTested
	bool ___m_GetDilemmaTested_2;

public:
	inline static int32_t get_offset_of_m_TradeoffAnalytics_1() { return static_cast<int32_t>(offsetof(TestTradeoffAnalytics_t1911417121, ___m_TradeoffAnalytics_1)); }
	inline TradeoffAnalytics_t100002595 * get_m_TradeoffAnalytics_1() const { return ___m_TradeoffAnalytics_1; }
	inline TradeoffAnalytics_t100002595 ** get_address_of_m_TradeoffAnalytics_1() { return &___m_TradeoffAnalytics_1; }
	inline void set_m_TradeoffAnalytics_1(TradeoffAnalytics_t100002595 * value)
	{
		___m_TradeoffAnalytics_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_TradeoffAnalytics_1), value);
	}

	inline static int32_t get_offset_of_m_GetDilemmaTested_2() { return static_cast<int32_t>(offsetof(TestTradeoffAnalytics_t1911417121, ___m_GetDilemmaTested_2)); }
	inline bool get_m_GetDilemmaTested_2() const { return ___m_GetDilemmaTested_2; }
	inline bool* get_address_of_m_GetDilemmaTested_2() { return &___m_GetDilemmaTested_2; }
	inline void set_m_GetDilemmaTested_2(bool value)
	{
		___m_GetDilemmaTested_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TESTTRADEOFFANALYTICS_T1911417121_H
#ifndef TESTRUNNABLE_T2118330963_H
#define TESTRUNNABLE_T2118330963_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.UnitTests.TestRunnable
struct  TestRunnable_t2118330963  : public UnitTest_t237671048
{
public:
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestRunnable::m_CoroutineRan
	bool ___m_CoroutineRan_1;

public:
	inline static int32_t get_offset_of_m_CoroutineRan_1() { return static_cast<int32_t>(offsetof(TestRunnable_t2118330963, ___m_CoroutineRan_1)); }
	inline bool get_m_CoroutineRan_1() const { return ___m_CoroutineRan_1; }
	inline bool* get_address_of_m_CoroutineRan_1() { return &___m_CoroutineRan_1; }
	inline void set_m_CoroutineRan_1(bool value)
	{
		___m_CoroutineRan_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TESTRUNNABLE_T2118330963_H
#ifndef TESTTONEANALYZER_T1700654784_H
#define TESTTONEANALYZER_T1700654784_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.UnitTests.TestToneAnalyzer
struct  TestToneAnalyzer_t1700654784  : public UnitTest_t237671048
{
public:
	// IBM.Watson.DeveloperCloud.Services.ToneAnalyzer.v3.ToneAnalyzer IBM.Watson.DeveloperCloud.UnitTests.TestToneAnalyzer::m_ToneAnalyzer
	ToneAnalyzer_t1356110496 * ___m_ToneAnalyzer_1;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestToneAnalyzer::m_GetToneAnalyzerTested
	bool ___m_GetToneAnalyzerTested_2;
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestToneAnalyzer::m_StringToTestTone
	String_t* ___m_StringToTestTone_3;

public:
	inline static int32_t get_offset_of_m_ToneAnalyzer_1() { return static_cast<int32_t>(offsetof(TestToneAnalyzer_t1700654784, ___m_ToneAnalyzer_1)); }
	inline ToneAnalyzer_t1356110496 * get_m_ToneAnalyzer_1() const { return ___m_ToneAnalyzer_1; }
	inline ToneAnalyzer_t1356110496 ** get_address_of_m_ToneAnalyzer_1() { return &___m_ToneAnalyzer_1; }
	inline void set_m_ToneAnalyzer_1(ToneAnalyzer_t1356110496 * value)
	{
		___m_ToneAnalyzer_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_ToneAnalyzer_1), value);
	}

	inline static int32_t get_offset_of_m_GetToneAnalyzerTested_2() { return static_cast<int32_t>(offsetof(TestToneAnalyzer_t1700654784, ___m_GetToneAnalyzerTested_2)); }
	inline bool get_m_GetToneAnalyzerTested_2() const { return ___m_GetToneAnalyzerTested_2; }
	inline bool* get_address_of_m_GetToneAnalyzerTested_2() { return &___m_GetToneAnalyzerTested_2; }
	inline void set_m_GetToneAnalyzerTested_2(bool value)
	{
		___m_GetToneAnalyzerTested_2 = value;
	}

	inline static int32_t get_offset_of_m_StringToTestTone_3() { return static_cast<int32_t>(offsetof(TestToneAnalyzer_t1700654784, ___m_StringToTestTone_3)); }
	inline String_t* get_m_StringToTestTone_3() const { return ___m_StringToTestTone_3; }
	inline String_t** get_address_of_m_StringToTestTone_3() { return &___m_StringToTestTone_3; }
	inline void set_m_StringToTestTone_3(String_t* value)
	{
		___m_StringToTestTone_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_StringToTestTone_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TESTTONEANALYZER_T1700654784_H
#ifndef TESTVISUALRECOGNITION_T3349626417_H
#define TESTVISUALRECOGNITION_T3349626417_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.UnitTests.TestVisualRecognition
struct  TestVisualRecognition_t3349626417  : public UnitTest_t237671048
{
public:
	// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition IBM.Watson.DeveloperCloud.UnitTests.TestVisualRecognition::m_VisualRecognition
	VisualRecognition_t3119563755 * ___m_VisualRecognition_1;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestVisualRecognition::m_TrainClasifierTested
	bool ___m_TrainClasifierTested_2;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestVisualRecognition::m_GetClassifiersTested
	bool ___m_GetClassifiersTested_3;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestVisualRecognition::m_FindClassifierTested
	bool ___m_FindClassifierTested_4;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestVisualRecognition::m_GetClassifierTested
	bool ___m_GetClassifierTested_5;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestVisualRecognition::m_UpdateClassifierTested
	bool ___m_UpdateClassifierTested_6;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestVisualRecognition::m_ClassifyGETTested
	bool ___m_ClassifyGETTested_7;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestVisualRecognition::m_ClassifyPOSTTested
	bool ___m_ClassifyPOSTTested_8;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestVisualRecognition::m_DetectFacesGETTested
	bool ___m_DetectFacesGETTested_9;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestVisualRecognition::m_DetectFacesPOSTTested
	bool ___m_DetectFacesPOSTTested_10;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestVisualRecognition::m_RecognizeTextGETTested
	bool ___m_RecognizeTextGETTested_11;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestVisualRecognition::m_RecognizeTextPOSTTested
	bool ___m_RecognizeTextPOSTTested_12;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestVisualRecognition::m_DeleteClassifierTested
	bool ___m_DeleteClassifierTested_13;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestVisualRecognition::m_ListCollectionsTested
	bool ___m_ListCollectionsTested_14;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestVisualRecognition::m_CreateCollectionTested
	bool ___m_CreateCollectionTested_15;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestVisualRecognition::m_DeleteCollectionTested
	bool ___m_DeleteCollectionTested_16;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestVisualRecognition::m_RetrieveCollectionDetailsTested
	bool ___m_RetrieveCollectionDetailsTested_17;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestVisualRecognition::m_ListImagesTested
	bool ___m_ListImagesTested_18;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestVisualRecognition::m_AddImagesToCollectionTested
	bool ___m_AddImagesToCollectionTested_19;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestVisualRecognition::m_DeleteImageFromCollectionTested
	bool ___m_DeleteImageFromCollectionTested_20;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestVisualRecognition::m_ListImageDetailsTested
	bool ___m_ListImageDetailsTested_21;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestVisualRecognition::m_DeleteImageMetadataTested
	bool ___m_DeleteImageMetadataTested_22;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestVisualRecognition::m_ListImageMetadataTested
	bool ___m_ListImageMetadataTested_23;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestVisualRecognition::m_FindSimilarTested
	bool ___m_FindSimilarTested_24;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestVisualRecognition::m_TrainClassifier
	bool ___m_TrainClassifier_25;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestVisualRecognition::m_IsClassifierReady
	bool ___m_IsClassifierReady_26;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestVisualRecognition::m_HasUpdatedClassifier
	bool ___m_HasUpdatedClassifier_27;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestVisualRecognition::m_IsUpdatedClassifierReady
	bool ___m_IsUpdatedClassifierReady_28;
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestVisualRecognition::m_ClassifierId
	String_t* ___m_ClassifierId_29;
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestVisualRecognition::m_ClassifierName
	String_t* ___m_ClassifierName_30;
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestVisualRecognition::m_ClassName_Giraffe
	String_t* ___m_ClassName_Giraffe_31;
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestVisualRecognition::m_ClassName_Turtle
	String_t* ___m_ClassName_Turtle_32;
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestVisualRecognition::m_ImageURL
	String_t* ___m_ImageURL_33;
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestVisualRecognition::m_ImageFaceURL
	String_t* ___m_ImageFaceURL_34;
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestVisualRecognition::m_ImageTextURL
	String_t* ___m_ImageTextURL_35;
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestVisualRecognition::m_CreatedCollectionID
	String_t* ___m_CreatedCollectionID_36;
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestVisualRecognition::m_CreatedCollectionImage
	String_t* ___m_CreatedCollectionImage_37;

public:
	inline static int32_t get_offset_of_m_VisualRecognition_1() { return static_cast<int32_t>(offsetof(TestVisualRecognition_t3349626417, ___m_VisualRecognition_1)); }
	inline VisualRecognition_t3119563755 * get_m_VisualRecognition_1() const { return ___m_VisualRecognition_1; }
	inline VisualRecognition_t3119563755 ** get_address_of_m_VisualRecognition_1() { return &___m_VisualRecognition_1; }
	inline void set_m_VisualRecognition_1(VisualRecognition_t3119563755 * value)
	{
		___m_VisualRecognition_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_VisualRecognition_1), value);
	}

	inline static int32_t get_offset_of_m_TrainClasifierTested_2() { return static_cast<int32_t>(offsetof(TestVisualRecognition_t3349626417, ___m_TrainClasifierTested_2)); }
	inline bool get_m_TrainClasifierTested_2() const { return ___m_TrainClasifierTested_2; }
	inline bool* get_address_of_m_TrainClasifierTested_2() { return &___m_TrainClasifierTested_2; }
	inline void set_m_TrainClasifierTested_2(bool value)
	{
		___m_TrainClasifierTested_2 = value;
	}

	inline static int32_t get_offset_of_m_GetClassifiersTested_3() { return static_cast<int32_t>(offsetof(TestVisualRecognition_t3349626417, ___m_GetClassifiersTested_3)); }
	inline bool get_m_GetClassifiersTested_3() const { return ___m_GetClassifiersTested_3; }
	inline bool* get_address_of_m_GetClassifiersTested_3() { return &___m_GetClassifiersTested_3; }
	inline void set_m_GetClassifiersTested_3(bool value)
	{
		___m_GetClassifiersTested_3 = value;
	}

	inline static int32_t get_offset_of_m_FindClassifierTested_4() { return static_cast<int32_t>(offsetof(TestVisualRecognition_t3349626417, ___m_FindClassifierTested_4)); }
	inline bool get_m_FindClassifierTested_4() const { return ___m_FindClassifierTested_4; }
	inline bool* get_address_of_m_FindClassifierTested_4() { return &___m_FindClassifierTested_4; }
	inline void set_m_FindClassifierTested_4(bool value)
	{
		___m_FindClassifierTested_4 = value;
	}

	inline static int32_t get_offset_of_m_GetClassifierTested_5() { return static_cast<int32_t>(offsetof(TestVisualRecognition_t3349626417, ___m_GetClassifierTested_5)); }
	inline bool get_m_GetClassifierTested_5() const { return ___m_GetClassifierTested_5; }
	inline bool* get_address_of_m_GetClassifierTested_5() { return &___m_GetClassifierTested_5; }
	inline void set_m_GetClassifierTested_5(bool value)
	{
		___m_GetClassifierTested_5 = value;
	}

	inline static int32_t get_offset_of_m_UpdateClassifierTested_6() { return static_cast<int32_t>(offsetof(TestVisualRecognition_t3349626417, ___m_UpdateClassifierTested_6)); }
	inline bool get_m_UpdateClassifierTested_6() const { return ___m_UpdateClassifierTested_6; }
	inline bool* get_address_of_m_UpdateClassifierTested_6() { return &___m_UpdateClassifierTested_6; }
	inline void set_m_UpdateClassifierTested_6(bool value)
	{
		___m_UpdateClassifierTested_6 = value;
	}

	inline static int32_t get_offset_of_m_ClassifyGETTested_7() { return static_cast<int32_t>(offsetof(TestVisualRecognition_t3349626417, ___m_ClassifyGETTested_7)); }
	inline bool get_m_ClassifyGETTested_7() const { return ___m_ClassifyGETTested_7; }
	inline bool* get_address_of_m_ClassifyGETTested_7() { return &___m_ClassifyGETTested_7; }
	inline void set_m_ClassifyGETTested_7(bool value)
	{
		___m_ClassifyGETTested_7 = value;
	}

	inline static int32_t get_offset_of_m_ClassifyPOSTTested_8() { return static_cast<int32_t>(offsetof(TestVisualRecognition_t3349626417, ___m_ClassifyPOSTTested_8)); }
	inline bool get_m_ClassifyPOSTTested_8() const { return ___m_ClassifyPOSTTested_8; }
	inline bool* get_address_of_m_ClassifyPOSTTested_8() { return &___m_ClassifyPOSTTested_8; }
	inline void set_m_ClassifyPOSTTested_8(bool value)
	{
		___m_ClassifyPOSTTested_8 = value;
	}

	inline static int32_t get_offset_of_m_DetectFacesGETTested_9() { return static_cast<int32_t>(offsetof(TestVisualRecognition_t3349626417, ___m_DetectFacesGETTested_9)); }
	inline bool get_m_DetectFacesGETTested_9() const { return ___m_DetectFacesGETTested_9; }
	inline bool* get_address_of_m_DetectFacesGETTested_9() { return &___m_DetectFacesGETTested_9; }
	inline void set_m_DetectFacesGETTested_9(bool value)
	{
		___m_DetectFacesGETTested_9 = value;
	}

	inline static int32_t get_offset_of_m_DetectFacesPOSTTested_10() { return static_cast<int32_t>(offsetof(TestVisualRecognition_t3349626417, ___m_DetectFacesPOSTTested_10)); }
	inline bool get_m_DetectFacesPOSTTested_10() const { return ___m_DetectFacesPOSTTested_10; }
	inline bool* get_address_of_m_DetectFacesPOSTTested_10() { return &___m_DetectFacesPOSTTested_10; }
	inline void set_m_DetectFacesPOSTTested_10(bool value)
	{
		___m_DetectFacesPOSTTested_10 = value;
	}

	inline static int32_t get_offset_of_m_RecognizeTextGETTested_11() { return static_cast<int32_t>(offsetof(TestVisualRecognition_t3349626417, ___m_RecognizeTextGETTested_11)); }
	inline bool get_m_RecognizeTextGETTested_11() const { return ___m_RecognizeTextGETTested_11; }
	inline bool* get_address_of_m_RecognizeTextGETTested_11() { return &___m_RecognizeTextGETTested_11; }
	inline void set_m_RecognizeTextGETTested_11(bool value)
	{
		___m_RecognizeTextGETTested_11 = value;
	}

	inline static int32_t get_offset_of_m_RecognizeTextPOSTTested_12() { return static_cast<int32_t>(offsetof(TestVisualRecognition_t3349626417, ___m_RecognizeTextPOSTTested_12)); }
	inline bool get_m_RecognizeTextPOSTTested_12() const { return ___m_RecognizeTextPOSTTested_12; }
	inline bool* get_address_of_m_RecognizeTextPOSTTested_12() { return &___m_RecognizeTextPOSTTested_12; }
	inline void set_m_RecognizeTextPOSTTested_12(bool value)
	{
		___m_RecognizeTextPOSTTested_12 = value;
	}

	inline static int32_t get_offset_of_m_DeleteClassifierTested_13() { return static_cast<int32_t>(offsetof(TestVisualRecognition_t3349626417, ___m_DeleteClassifierTested_13)); }
	inline bool get_m_DeleteClassifierTested_13() const { return ___m_DeleteClassifierTested_13; }
	inline bool* get_address_of_m_DeleteClassifierTested_13() { return &___m_DeleteClassifierTested_13; }
	inline void set_m_DeleteClassifierTested_13(bool value)
	{
		___m_DeleteClassifierTested_13 = value;
	}

	inline static int32_t get_offset_of_m_ListCollectionsTested_14() { return static_cast<int32_t>(offsetof(TestVisualRecognition_t3349626417, ___m_ListCollectionsTested_14)); }
	inline bool get_m_ListCollectionsTested_14() const { return ___m_ListCollectionsTested_14; }
	inline bool* get_address_of_m_ListCollectionsTested_14() { return &___m_ListCollectionsTested_14; }
	inline void set_m_ListCollectionsTested_14(bool value)
	{
		___m_ListCollectionsTested_14 = value;
	}

	inline static int32_t get_offset_of_m_CreateCollectionTested_15() { return static_cast<int32_t>(offsetof(TestVisualRecognition_t3349626417, ___m_CreateCollectionTested_15)); }
	inline bool get_m_CreateCollectionTested_15() const { return ___m_CreateCollectionTested_15; }
	inline bool* get_address_of_m_CreateCollectionTested_15() { return &___m_CreateCollectionTested_15; }
	inline void set_m_CreateCollectionTested_15(bool value)
	{
		___m_CreateCollectionTested_15 = value;
	}

	inline static int32_t get_offset_of_m_DeleteCollectionTested_16() { return static_cast<int32_t>(offsetof(TestVisualRecognition_t3349626417, ___m_DeleteCollectionTested_16)); }
	inline bool get_m_DeleteCollectionTested_16() const { return ___m_DeleteCollectionTested_16; }
	inline bool* get_address_of_m_DeleteCollectionTested_16() { return &___m_DeleteCollectionTested_16; }
	inline void set_m_DeleteCollectionTested_16(bool value)
	{
		___m_DeleteCollectionTested_16 = value;
	}

	inline static int32_t get_offset_of_m_RetrieveCollectionDetailsTested_17() { return static_cast<int32_t>(offsetof(TestVisualRecognition_t3349626417, ___m_RetrieveCollectionDetailsTested_17)); }
	inline bool get_m_RetrieveCollectionDetailsTested_17() const { return ___m_RetrieveCollectionDetailsTested_17; }
	inline bool* get_address_of_m_RetrieveCollectionDetailsTested_17() { return &___m_RetrieveCollectionDetailsTested_17; }
	inline void set_m_RetrieveCollectionDetailsTested_17(bool value)
	{
		___m_RetrieveCollectionDetailsTested_17 = value;
	}

	inline static int32_t get_offset_of_m_ListImagesTested_18() { return static_cast<int32_t>(offsetof(TestVisualRecognition_t3349626417, ___m_ListImagesTested_18)); }
	inline bool get_m_ListImagesTested_18() const { return ___m_ListImagesTested_18; }
	inline bool* get_address_of_m_ListImagesTested_18() { return &___m_ListImagesTested_18; }
	inline void set_m_ListImagesTested_18(bool value)
	{
		___m_ListImagesTested_18 = value;
	}

	inline static int32_t get_offset_of_m_AddImagesToCollectionTested_19() { return static_cast<int32_t>(offsetof(TestVisualRecognition_t3349626417, ___m_AddImagesToCollectionTested_19)); }
	inline bool get_m_AddImagesToCollectionTested_19() const { return ___m_AddImagesToCollectionTested_19; }
	inline bool* get_address_of_m_AddImagesToCollectionTested_19() { return &___m_AddImagesToCollectionTested_19; }
	inline void set_m_AddImagesToCollectionTested_19(bool value)
	{
		___m_AddImagesToCollectionTested_19 = value;
	}

	inline static int32_t get_offset_of_m_DeleteImageFromCollectionTested_20() { return static_cast<int32_t>(offsetof(TestVisualRecognition_t3349626417, ___m_DeleteImageFromCollectionTested_20)); }
	inline bool get_m_DeleteImageFromCollectionTested_20() const { return ___m_DeleteImageFromCollectionTested_20; }
	inline bool* get_address_of_m_DeleteImageFromCollectionTested_20() { return &___m_DeleteImageFromCollectionTested_20; }
	inline void set_m_DeleteImageFromCollectionTested_20(bool value)
	{
		___m_DeleteImageFromCollectionTested_20 = value;
	}

	inline static int32_t get_offset_of_m_ListImageDetailsTested_21() { return static_cast<int32_t>(offsetof(TestVisualRecognition_t3349626417, ___m_ListImageDetailsTested_21)); }
	inline bool get_m_ListImageDetailsTested_21() const { return ___m_ListImageDetailsTested_21; }
	inline bool* get_address_of_m_ListImageDetailsTested_21() { return &___m_ListImageDetailsTested_21; }
	inline void set_m_ListImageDetailsTested_21(bool value)
	{
		___m_ListImageDetailsTested_21 = value;
	}

	inline static int32_t get_offset_of_m_DeleteImageMetadataTested_22() { return static_cast<int32_t>(offsetof(TestVisualRecognition_t3349626417, ___m_DeleteImageMetadataTested_22)); }
	inline bool get_m_DeleteImageMetadataTested_22() const { return ___m_DeleteImageMetadataTested_22; }
	inline bool* get_address_of_m_DeleteImageMetadataTested_22() { return &___m_DeleteImageMetadataTested_22; }
	inline void set_m_DeleteImageMetadataTested_22(bool value)
	{
		___m_DeleteImageMetadataTested_22 = value;
	}

	inline static int32_t get_offset_of_m_ListImageMetadataTested_23() { return static_cast<int32_t>(offsetof(TestVisualRecognition_t3349626417, ___m_ListImageMetadataTested_23)); }
	inline bool get_m_ListImageMetadataTested_23() const { return ___m_ListImageMetadataTested_23; }
	inline bool* get_address_of_m_ListImageMetadataTested_23() { return &___m_ListImageMetadataTested_23; }
	inline void set_m_ListImageMetadataTested_23(bool value)
	{
		___m_ListImageMetadataTested_23 = value;
	}

	inline static int32_t get_offset_of_m_FindSimilarTested_24() { return static_cast<int32_t>(offsetof(TestVisualRecognition_t3349626417, ___m_FindSimilarTested_24)); }
	inline bool get_m_FindSimilarTested_24() const { return ___m_FindSimilarTested_24; }
	inline bool* get_address_of_m_FindSimilarTested_24() { return &___m_FindSimilarTested_24; }
	inline void set_m_FindSimilarTested_24(bool value)
	{
		___m_FindSimilarTested_24 = value;
	}

	inline static int32_t get_offset_of_m_TrainClassifier_25() { return static_cast<int32_t>(offsetof(TestVisualRecognition_t3349626417, ___m_TrainClassifier_25)); }
	inline bool get_m_TrainClassifier_25() const { return ___m_TrainClassifier_25; }
	inline bool* get_address_of_m_TrainClassifier_25() { return &___m_TrainClassifier_25; }
	inline void set_m_TrainClassifier_25(bool value)
	{
		___m_TrainClassifier_25 = value;
	}

	inline static int32_t get_offset_of_m_IsClassifierReady_26() { return static_cast<int32_t>(offsetof(TestVisualRecognition_t3349626417, ___m_IsClassifierReady_26)); }
	inline bool get_m_IsClassifierReady_26() const { return ___m_IsClassifierReady_26; }
	inline bool* get_address_of_m_IsClassifierReady_26() { return &___m_IsClassifierReady_26; }
	inline void set_m_IsClassifierReady_26(bool value)
	{
		___m_IsClassifierReady_26 = value;
	}

	inline static int32_t get_offset_of_m_HasUpdatedClassifier_27() { return static_cast<int32_t>(offsetof(TestVisualRecognition_t3349626417, ___m_HasUpdatedClassifier_27)); }
	inline bool get_m_HasUpdatedClassifier_27() const { return ___m_HasUpdatedClassifier_27; }
	inline bool* get_address_of_m_HasUpdatedClassifier_27() { return &___m_HasUpdatedClassifier_27; }
	inline void set_m_HasUpdatedClassifier_27(bool value)
	{
		___m_HasUpdatedClassifier_27 = value;
	}

	inline static int32_t get_offset_of_m_IsUpdatedClassifierReady_28() { return static_cast<int32_t>(offsetof(TestVisualRecognition_t3349626417, ___m_IsUpdatedClassifierReady_28)); }
	inline bool get_m_IsUpdatedClassifierReady_28() const { return ___m_IsUpdatedClassifierReady_28; }
	inline bool* get_address_of_m_IsUpdatedClassifierReady_28() { return &___m_IsUpdatedClassifierReady_28; }
	inline void set_m_IsUpdatedClassifierReady_28(bool value)
	{
		___m_IsUpdatedClassifierReady_28 = value;
	}

	inline static int32_t get_offset_of_m_ClassifierId_29() { return static_cast<int32_t>(offsetof(TestVisualRecognition_t3349626417, ___m_ClassifierId_29)); }
	inline String_t* get_m_ClassifierId_29() const { return ___m_ClassifierId_29; }
	inline String_t** get_address_of_m_ClassifierId_29() { return &___m_ClassifierId_29; }
	inline void set_m_ClassifierId_29(String_t* value)
	{
		___m_ClassifierId_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_ClassifierId_29), value);
	}

	inline static int32_t get_offset_of_m_ClassifierName_30() { return static_cast<int32_t>(offsetof(TestVisualRecognition_t3349626417, ___m_ClassifierName_30)); }
	inline String_t* get_m_ClassifierName_30() const { return ___m_ClassifierName_30; }
	inline String_t** get_address_of_m_ClassifierName_30() { return &___m_ClassifierName_30; }
	inline void set_m_ClassifierName_30(String_t* value)
	{
		___m_ClassifierName_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_ClassifierName_30), value);
	}

	inline static int32_t get_offset_of_m_ClassName_Giraffe_31() { return static_cast<int32_t>(offsetof(TestVisualRecognition_t3349626417, ___m_ClassName_Giraffe_31)); }
	inline String_t* get_m_ClassName_Giraffe_31() const { return ___m_ClassName_Giraffe_31; }
	inline String_t** get_address_of_m_ClassName_Giraffe_31() { return &___m_ClassName_Giraffe_31; }
	inline void set_m_ClassName_Giraffe_31(String_t* value)
	{
		___m_ClassName_Giraffe_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_ClassName_Giraffe_31), value);
	}

	inline static int32_t get_offset_of_m_ClassName_Turtle_32() { return static_cast<int32_t>(offsetof(TestVisualRecognition_t3349626417, ___m_ClassName_Turtle_32)); }
	inline String_t* get_m_ClassName_Turtle_32() const { return ___m_ClassName_Turtle_32; }
	inline String_t** get_address_of_m_ClassName_Turtle_32() { return &___m_ClassName_Turtle_32; }
	inline void set_m_ClassName_Turtle_32(String_t* value)
	{
		___m_ClassName_Turtle_32 = value;
		Il2CppCodeGenWriteBarrier((&___m_ClassName_Turtle_32), value);
	}

	inline static int32_t get_offset_of_m_ImageURL_33() { return static_cast<int32_t>(offsetof(TestVisualRecognition_t3349626417, ___m_ImageURL_33)); }
	inline String_t* get_m_ImageURL_33() const { return ___m_ImageURL_33; }
	inline String_t** get_address_of_m_ImageURL_33() { return &___m_ImageURL_33; }
	inline void set_m_ImageURL_33(String_t* value)
	{
		___m_ImageURL_33 = value;
		Il2CppCodeGenWriteBarrier((&___m_ImageURL_33), value);
	}

	inline static int32_t get_offset_of_m_ImageFaceURL_34() { return static_cast<int32_t>(offsetof(TestVisualRecognition_t3349626417, ___m_ImageFaceURL_34)); }
	inline String_t* get_m_ImageFaceURL_34() const { return ___m_ImageFaceURL_34; }
	inline String_t** get_address_of_m_ImageFaceURL_34() { return &___m_ImageFaceURL_34; }
	inline void set_m_ImageFaceURL_34(String_t* value)
	{
		___m_ImageFaceURL_34 = value;
		Il2CppCodeGenWriteBarrier((&___m_ImageFaceURL_34), value);
	}

	inline static int32_t get_offset_of_m_ImageTextURL_35() { return static_cast<int32_t>(offsetof(TestVisualRecognition_t3349626417, ___m_ImageTextURL_35)); }
	inline String_t* get_m_ImageTextURL_35() const { return ___m_ImageTextURL_35; }
	inline String_t** get_address_of_m_ImageTextURL_35() { return &___m_ImageTextURL_35; }
	inline void set_m_ImageTextURL_35(String_t* value)
	{
		___m_ImageTextURL_35 = value;
		Il2CppCodeGenWriteBarrier((&___m_ImageTextURL_35), value);
	}

	inline static int32_t get_offset_of_m_CreatedCollectionID_36() { return static_cast<int32_t>(offsetof(TestVisualRecognition_t3349626417, ___m_CreatedCollectionID_36)); }
	inline String_t* get_m_CreatedCollectionID_36() const { return ___m_CreatedCollectionID_36; }
	inline String_t** get_address_of_m_CreatedCollectionID_36() { return &___m_CreatedCollectionID_36; }
	inline void set_m_CreatedCollectionID_36(String_t* value)
	{
		___m_CreatedCollectionID_36 = value;
		Il2CppCodeGenWriteBarrier((&___m_CreatedCollectionID_36), value);
	}

	inline static int32_t get_offset_of_m_CreatedCollectionImage_37() { return static_cast<int32_t>(offsetof(TestVisualRecognition_t3349626417, ___m_CreatedCollectionImage_37)); }
	inline String_t* get_m_CreatedCollectionImage_37() const { return ___m_CreatedCollectionImage_37; }
	inline String_t** get_address_of_m_CreatedCollectionImage_37() { return &___m_CreatedCollectionImage_37; }
	inline void set_m_CreatedCollectionImage_37(String_t* value)
	{
		___m_CreatedCollectionImage_37 = value;
		Il2CppCodeGenWriteBarrier((&___m_CreatedCollectionImage_37), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TESTVISUALRECOGNITION_T3349626417_H
#ifndef TESTDATAVALUE_T312670927_H
#define TESTDATAVALUE_T312670927_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.UnitTests.TestTradeoffAnalytics/TestDataValue
struct  TestDataValue_t312670927  : public ApplicationDataValue_t1721671971
{
public:
	// System.Double IBM.Watson.DeveloperCloud.UnitTests.TestTradeoffAnalytics/TestDataValue::<price>k__BackingField
	double ___U3CpriceU3Ek__BackingField_0;
	// System.Double IBM.Watson.DeveloperCloud.UnitTests.TestTradeoffAnalytics/TestDataValue::<weight>k__BackingField
	double ___U3CweightU3Ek__BackingField_1;
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestTradeoffAnalytics/TestDataValue::<brand>k__BackingField
	String_t* ___U3CbrandU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CpriceU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TestDataValue_t312670927, ___U3CpriceU3Ek__BackingField_0)); }
	inline double get_U3CpriceU3Ek__BackingField_0() const { return ___U3CpriceU3Ek__BackingField_0; }
	inline double* get_address_of_U3CpriceU3Ek__BackingField_0() { return &___U3CpriceU3Ek__BackingField_0; }
	inline void set_U3CpriceU3Ek__BackingField_0(double value)
	{
		___U3CpriceU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CweightU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TestDataValue_t312670927, ___U3CweightU3Ek__BackingField_1)); }
	inline double get_U3CweightU3Ek__BackingField_1() const { return ___U3CweightU3Ek__BackingField_1; }
	inline double* get_address_of_U3CweightU3Ek__BackingField_1() { return &___U3CweightU3Ek__BackingField_1; }
	inline void set_U3CweightU3Ek__BackingField_1(double value)
	{
		___U3CweightU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CbrandU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(TestDataValue_t312670927, ___U3CbrandU3Ek__BackingField_2)); }
	inline String_t* get_U3CbrandU3Ek__BackingField_2() const { return ___U3CbrandU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CbrandU3Ek__BackingField_2() { return &___U3CbrandU3Ek__BackingField_2; }
	inline void set_U3CbrandU3Ek__BackingField_2(String_t* value)
	{
		___U3CbrandU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CbrandU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TESTDATAVALUE_T312670927_H
#ifndef TESTSPEECHTOTEXT_T3601723574_H
#define TESTSPEECHTOTEXT_T3601723574_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.UnitTests.TestSpeechToText
struct  TestSpeechToText_t3601723574  : public UnitTest_t237671048
{
public:
	// IBM.Watson.DeveloperCloud.Services.SpeechToText.v1.SpeechToText IBM.Watson.DeveloperCloud.UnitTests.TestSpeechToText::m_SpeechToText
	SpeechToText_t2713896346 * ___m_SpeechToText_1;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestSpeechToText::m_GetModelsTested
	bool ___m_GetModelsTested_2;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestSpeechToText::m_GetModelTested
	bool ___m_GetModelTested_3;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestSpeechToText::m_GetCustomizationsTested
	bool ___m_GetCustomizationsTested_4;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestSpeechToText::m_CreateCustomizationTested
	bool ___m_CreateCustomizationTested_5;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestSpeechToText::m_GetCustomizationTested
	bool ___m_GetCustomizationTested_6;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestSpeechToText::m_GetCustomCorporaTested
	bool ___m_GetCustomCorporaTested_7;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestSpeechToText::m_AddCustomCorpusTested
	bool ___m_AddCustomCorpusTested_8;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestSpeechToText::m_GetCustomWordsTested
	bool ___m_GetCustomWordsTested_9;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestSpeechToText::m_AddCustomWordsUsingFileTested
	bool ___m_AddCustomWordsUsingFileTested_10;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestSpeechToText::m_AddCustomWordsUsingObjectTested
	bool ___m_AddCustomWordsUsingObjectTested_11;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestSpeechToText::m_GetCustomWordTested
	bool ___m_GetCustomWordTested_12;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestSpeechToText::m_TrainCustomizationTested
	bool ___m_TrainCustomizationTested_13;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestSpeechToText::m_DeleteCustomCorpusTested
	bool ___m_DeleteCustomCorpusTested_14;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestSpeechToText::m_DeleteCustomWordTested
	bool ___m_DeleteCustomWordTested_15;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestSpeechToText::m_ResetCustomizationTested
	bool ___m_ResetCustomizationTested_16;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestSpeechToText::m_DeleteCustomizationTested
	bool ___m_DeleteCustomizationTested_17;
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestSpeechToText::m_CreatedCustomizationID
	String_t* ___m_CreatedCustomizationID_18;
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestSpeechToText::m_CreatedCustomizationName
	String_t* ___m_CreatedCustomizationName_19;
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestSpeechToText::m_CreatedCorpusName
	String_t* ___m_CreatedCorpusName_20;
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestSpeechToText::m_CustomCorpusFilePath
	String_t* ___m_CustomCorpusFilePath_21;
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestSpeechToText::m_SpeechToTextModelEnglish
	String_t* ___m_SpeechToTextModelEnglish_22;
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestSpeechToText::m_CustomWordsFilePath
	String_t* ___m_CustomWordsFilePath_23;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestSpeechToText::m_AllowOverwrite
	bool ___m_AllowOverwrite_24;
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestSpeechToText::m_WordToGet
	String_t* ___m_WordToGet_25;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestSpeechToText::m_IsCustomizationBusy
	bool ___m_IsCustomizationBusy_26;

public:
	inline static int32_t get_offset_of_m_SpeechToText_1() { return static_cast<int32_t>(offsetof(TestSpeechToText_t3601723574, ___m_SpeechToText_1)); }
	inline SpeechToText_t2713896346 * get_m_SpeechToText_1() const { return ___m_SpeechToText_1; }
	inline SpeechToText_t2713896346 ** get_address_of_m_SpeechToText_1() { return &___m_SpeechToText_1; }
	inline void set_m_SpeechToText_1(SpeechToText_t2713896346 * value)
	{
		___m_SpeechToText_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_SpeechToText_1), value);
	}

	inline static int32_t get_offset_of_m_GetModelsTested_2() { return static_cast<int32_t>(offsetof(TestSpeechToText_t3601723574, ___m_GetModelsTested_2)); }
	inline bool get_m_GetModelsTested_2() const { return ___m_GetModelsTested_2; }
	inline bool* get_address_of_m_GetModelsTested_2() { return &___m_GetModelsTested_2; }
	inline void set_m_GetModelsTested_2(bool value)
	{
		___m_GetModelsTested_2 = value;
	}

	inline static int32_t get_offset_of_m_GetModelTested_3() { return static_cast<int32_t>(offsetof(TestSpeechToText_t3601723574, ___m_GetModelTested_3)); }
	inline bool get_m_GetModelTested_3() const { return ___m_GetModelTested_3; }
	inline bool* get_address_of_m_GetModelTested_3() { return &___m_GetModelTested_3; }
	inline void set_m_GetModelTested_3(bool value)
	{
		___m_GetModelTested_3 = value;
	}

	inline static int32_t get_offset_of_m_GetCustomizationsTested_4() { return static_cast<int32_t>(offsetof(TestSpeechToText_t3601723574, ___m_GetCustomizationsTested_4)); }
	inline bool get_m_GetCustomizationsTested_4() const { return ___m_GetCustomizationsTested_4; }
	inline bool* get_address_of_m_GetCustomizationsTested_4() { return &___m_GetCustomizationsTested_4; }
	inline void set_m_GetCustomizationsTested_4(bool value)
	{
		___m_GetCustomizationsTested_4 = value;
	}

	inline static int32_t get_offset_of_m_CreateCustomizationTested_5() { return static_cast<int32_t>(offsetof(TestSpeechToText_t3601723574, ___m_CreateCustomizationTested_5)); }
	inline bool get_m_CreateCustomizationTested_5() const { return ___m_CreateCustomizationTested_5; }
	inline bool* get_address_of_m_CreateCustomizationTested_5() { return &___m_CreateCustomizationTested_5; }
	inline void set_m_CreateCustomizationTested_5(bool value)
	{
		___m_CreateCustomizationTested_5 = value;
	}

	inline static int32_t get_offset_of_m_GetCustomizationTested_6() { return static_cast<int32_t>(offsetof(TestSpeechToText_t3601723574, ___m_GetCustomizationTested_6)); }
	inline bool get_m_GetCustomizationTested_6() const { return ___m_GetCustomizationTested_6; }
	inline bool* get_address_of_m_GetCustomizationTested_6() { return &___m_GetCustomizationTested_6; }
	inline void set_m_GetCustomizationTested_6(bool value)
	{
		___m_GetCustomizationTested_6 = value;
	}

	inline static int32_t get_offset_of_m_GetCustomCorporaTested_7() { return static_cast<int32_t>(offsetof(TestSpeechToText_t3601723574, ___m_GetCustomCorporaTested_7)); }
	inline bool get_m_GetCustomCorporaTested_7() const { return ___m_GetCustomCorporaTested_7; }
	inline bool* get_address_of_m_GetCustomCorporaTested_7() { return &___m_GetCustomCorporaTested_7; }
	inline void set_m_GetCustomCorporaTested_7(bool value)
	{
		___m_GetCustomCorporaTested_7 = value;
	}

	inline static int32_t get_offset_of_m_AddCustomCorpusTested_8() { return static_cast<int32_t>(offsetof(TestSpeechToText_t3601723574, ___m_AddCustomCorpusTested_8)); }
	inline bool get_m_AddCustomCorpusTested_8() const { return ___m_AddCustomCorpusTested_8; }
	inline bool* get_address_of_m_AddCustomCorpusTested_8() { return &___m_AddCustomCorpusTested_8; }
	inline void set_m_AddCustomCorpusTested_8(bool value)
	{
		___m_AddCustomCorpusTested_8 = value;
	}

	inline static int32_t get_offset_of_m_GetCustomWordsTested_9() { return static_cast<int32_t>(offsetof(TestSpeechToText_t3601723574, ___m_GetCustomWordsTested_9)); }
	inline bool get_m_GetCustomWordsTested_9() const { return ___m_GetCustomWordsTested_9; }
	inline bool* get_address_of_m_GetCustomWordsTested_9() { return &___m_GetCustomWordsTested_9; }
	inline void set_m_GetCustomWordsTested_9(bool value)
	{
		___m_GetCustomWordsTested_9 = value;
	}

	inline static int32_t get_offset_of_m_AddCustomWordsUsingFileTested_10() { return static_cast<int32_t>(offsetof(TestSpeechToText_t3601723574, ___m_AddCustomWordsUsingFileTested_10)); }
	inline bool get_m_AddCustomWordsUsingFileTested_10() const { return ___m_AddCustomWordsUsingFileTested_10; }
	inline bool* get_address_of_m_AddCustomWordsUsingFileTested_10() { return &___m_AddCustomWordsUsingFileTested_10; }
	inline void set_m_AddCustomWordsUsingFileTested_10(bool value)
	{
		___m_AddCustomWordsUsingFileTested_10 = value;
	}

	inline static int32_t get_offset_of_m_AddCustomWordsUsingObjectTested_11() { return static_cast<int32_t>(offsetof(TestSpeechToText_t3601723574, ___m_AddCustomWordsUsingObjectTested_11)); }
	inline bool get_m_AddCustomWordsUsingObjectTested_11() const { return ___m_AddCustomWordsUsingObjectTested_11; }
	inline bool* get_address_of_m_AddCustomWordsUsingObjectTested_11() { return &___m_AddCustomWordsUsingObjectTested_11; }
	inline void set_m_AddCustomWordsUsingObjectTested_11(bool value)
	{
		___m_AddCustomWordsUsingObjectTested_11 = value;
	}

	inline static int32_t get_offset_of_m_GetCustomWordTested_12() { return static_cast<int32_t>(offsetof(TestSpeechToText_t3601723574, ___m_GetCustomWordTested_12)); }
	inline bool get_m_GetCustomWordTested_12() const { return ___m_GetCustomWordTested_12; }
	inline bool* get_address_of_m_GetCustomWordTested_12() { return &___m_GetCustomWordTested_12; }
	inline void set_m_GetCustomWordTested_12(bool value)
	{
		___m_GetCustomWordTested_12 = value;
	}

	inline static int32_t get_offset_of_m_TrainCustomizationTested_13() { return static_cast<int32_t>(offsetof(TestSpeechToText_t3601723574, ___m_TrainCustomizationTested_13)); }
	inline bool get_m_TrainCustomizationTested_13() const { return ___m_TrainCustomizationTested_13; }
	inline bool* get_address_of_m_TrainCustomizationTested_13() { return &___m_TrainCustomizationTested_13; }
	inline void set_m_TrainCustomizationTested_13(bool value)
	{
		___m_TrainCustomizationTested_13 = value;
	}

	inline static int32_t get_offset_of_m_DeleteCustomCorpusTested_14() { return static_cast<int32_t>(offsetof(TestSpeechToText_t3601723574, ___m_DeleteCustomCorpusTested_14)); }
	inline bool get_m_DeleteCustomCorpusTested_14() const { return ___m_DeleteCustomCorpusTested_14; }
	inline bool* get_address_of_m_DeleteCustomCorpusTested_14() { return &___m_DeleteCustomCorpusTested_14; }
	inline void set_m_DeleteCustomCorpusTested_14(bool value)
	{
		___m_DeleteCustomCorpusTested_14 = value;
	}

	inline static int32_t get_offset_of_m_DeleteCustomWordTested_15() { return static_cast<int32_t>(offsetof(TestSpeechToText_t3601723574, ___m_DeleteCustomWordTested_15)); }
	inline bool get_m_DeleteCustomWordTested_15() const { return ___m_DeleteCustomWordTested_15; }
	inline bool* get_address_of_m_DeleteCustomWordTested_15() { return &___m_DeleteCustomWordTested_15; }
	inline void set_m_DeleteCustomWordTested_15(bool value)
	{
		___m_DeleteCustomWordTested_15 = value;
	}

	inline static int32_t get_offset_of_m_ResetCustomizationTested_16() { return static_cast<int32_t>(offsetof(TestSpeechToText_t3601723574, ___m_ResetCustomizationTested_16)); }
	inline bool get_m_ResetCustomizationTested_16() const { return ___m_ResetCustomizationTested_16; }
	inline bool* get_address_of_m_ResetCustomizationTested_16() { return &___m_ResetCustomizationTested_16; }
	inline void set_m_ResetCustomizationTested_16(bool value)
	{
		___m_ResetCustomizationTested_16 = value;
	}

	inline static int32_t get_offset_of_m_DeleteCustomizationTested_17() { return static_cast<int32_t>(offsetof(TestSpeechToText_t3601723574, ___m_DeleteCustomizationTested_17)); }
	inline bool get_m_DeleteCustomizationTested_17() const { return ___m_DeleteCustomizationTested_17; }
	inline bool* get_address_of_m_DeleteCustomizationTested_17() { return &___m_DeleteCustomizationTested_17; }
	inline void set_m_DeleteCustomizationTested_17(bool value)
	{
		___m_DeleteCustomizationTested_17 = value;
	}

	inline static int32_t get_offset_of_m_CreatedCustomizationID_18() { return static_cast<int32_t>(offsetof(TestSpeechToText_t3601723574, ___m_CreatedCustomizationID_18)); }
	inline String_t* get_m_CreatedCustomizationID_18() const { return ___m_CreatedCustomizationID_18; }
	inline String_t** get_address_of_m_CreatedCustomizationID_18() { return &___m_CreatedCustomizationID_18; }
	inline void set_m_CreatedCustomizationID_18(String_t* value)
	{
		___m_CreatedCustomizationID_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_CreatedCustomizationID_18), value);
	}

	inline static int32_t get_offset_of_m_CreatedCustomizationName_19() { return static_cast<int32_t>(offsetof(TestSpeechToText_t3601723574, ___m_CreatedCustomizationName_19)); }
	inline String_t* get_m_CreatedCustomizationName_19() const { return ___m_CreatedCustomizationName_19; }
	inline String_t** get_address_of_m_CreatedCustomizationName_19() { return &___m_CreatedCustomizationName_19; }
	inline void set_m_CreatedCustomizationName_19(String_t* value)
	{
		___m_CreatedCustomizationName_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_CreatedCustomizationName_19), value);
	}

	inline static int32_t get_offset_of_m_CreatedCorpusName_20() { return static_cast<int32_t>(offsetof(TestSpeechToText_t3601723574, ___m_CreatedCorpusName_20)); }
	inline String_t* get_m_CreatedCorpusName_20() const { return ___m_CreatedCorpusName_20; }
	inline String_t** get_address_of_m_CreatedCorpusName_20() { return &___m_CreatedCorpusName_20; }
	inline void set_m_CreatedCorpusName_20(String_t* value)
	{
		___m_CreatedCorpusName_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_CreatedCorpusName_20), value);
	}

	inline static int32_t get_offset_of_m_CustomCorpusFilePath_21() { return static_cast<int32_t>(offsetof(TestSpeechToText_t3601723574, ___m_CustomCorpusFilePath_21)); }
	inline String_t* get_m_CustomCorpusFilePath_21() const { return ___m_CustomCorpusFilePath_21; }
	inline String_t** get_address_of_m_CustomCorpusFilePath_21() { return &___m_CustomCorpusFilePath_21; }
	inline void set_m_CustomCorpusFilePath_21(String_t* value)
	{
		___m_CustomCorpusFilePath_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_CustomCorpusFilePath_21), value);
	}

	inline static int32_t get_offset_of_m_SpeechToTextModelEnglish_22() { return static_cast<int32_t>(offsetof(TestSpeechToText_t3601723574, ___m_SpeechToTextModelEnglish_22)); }
	inline String_t* get_m_SpeechToTextModelEnglish_22() const { return ___m_SpeechToTextModelEnglish_22; }
	inline String_t** get_address_of_m_SpeechToTextModelEnglish_22() { return &___m_SpeechToTextModelEnglish_22; }
	inline void set_m_SpeechToTextModelEnglish_22(String_t* value)
	{
		___m_SpeechToTextModelEnglish_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_SpeechToTextModelEnglish_22), value);
	}

	inline static int32_t get_offset_of_m_CustomWordsFilePath_23() { return static_cast<int32_t>(offsetof(TestSpeechToText_t3601723574, ___m_CustomWordsFilePath_23)); }
	inline String_t* get_m_CustomWordsFilePath_23() const { return ___m_CustomWordsFilePath_23; }
	inline String_t** get_address_of_m_CustomWordsFilePath_23() { return &___m_CustomWordsFilePath_23; }
	inline void set_m_CustomWordsFilePath_23(String_t* value)
	{
		___m_CustomWordsFilePath_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_CustomWordsFilePath_23), value);
	}

	inline static int32_t get_offset_of_m_AllowOverwrite_24() { return static_cast<int32_t>(offsetof(TestSpeechToText_t3601723574, ___m_AllowOverwrite_24)); }
	inline bool get_m_AllowOverwrite_24() const { return ___m_AllowOverwrite_24; }
	inline bool* get_address_of_m_AllowOverwrite_24() { return &___m_AllowOverwrite_24; }
	inline void set_m_AllowOverwrite_24(bool value)
	{
		___m_AllowOverwrite_24 = value;
	}

	inline static int32_t get_offset_of_m_WordToGet_25() { return static_cast<int32_t>(offsetof(TestSpeechToText_t3601723574, ___m_WordToGet_25)); }
	inline String_t* get_m_WordToGet_25() const { return ___m_WordToGet_25; }
	inline String_t** get_address_of_m_WordToGet_25() { return &___m_WordToGet_25; }
	inline void set_m_WordToGet_25(String_t* value)
	{
		___m_WordToGet_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_WordToGet_25), value);
	}

	inline static int32_t get_offset_of_m_IsCustomizationBusy_26() { return static_cast<int32_t>(offsetof(TestSpeechToText_t3601723574, ___m_IsCustomizationBusy_26)); }
	inline bool get_m_IsCustomizationBusy_26() const { return ___m_IsCustomizationBusy_26; }
	inline bool* get_address_of_m_IsCustomizationBusy_26() { return &___m_IsCustomizationBusy_26; }
	inline void set_m_IsCustomizationBusy_26(bool value)
	{
		___m_IsCustomizationBusy_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TESTSPEECHTOTEXT_T3601723574_H
#ifndef TESTDATA_T435165324_H
#define TESTDATA_T435165324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.UnitTests.TestTradeoffAnalytics/TestData
struct  TestData_t435165324  : public ApplicationData_t1885408412
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TESTDATA_T435165324_H
#ifndef TESTTEXTTOSPEECH_T3466033430_H
#define TESTTEXTTOSPEECH_T3466033430_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.UnitTests.TestTextToSpeech
struct  TestTextToSpeech_t3466033430  : public UnitTest_t237671048
{
public:
	// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.TextToSpeech IBM.Watson.DeveloperCloud.UnitTests.TestTextToSpeech::m_TextToSpeech
	TextToSpeech_t3349357562 * ___m_TextToSpeech_1;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestTextToSpeech::m_GetTested
	bool ___m_GetTested_2;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestTextToSpeech::m_PostTested
	bool ___m_PostTested_3;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestTextToSpeech::m_GetVoicesTested
	bool ___m_GetVoicesTested_4;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestTextToSpeech::m_GetVoiceTested
	bool ___m_GetVoiceTested_5;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestTextToSpeech::m_GetPronunciationTested
	bool ___m_GetPronunciationTested_6;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestTextToSpeech::m_GetCustomizationsTested
	bool ___m_GetCustomizationsTested_7;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestTextToSpeech::m_CreateCustomizationTested
	bool ___m_CreateCustomizationTested_8;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestTextToSpeech::m_DeleteCustomizationTested
	bool ___m_DeleteCustomizationTested_9;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestTextToSpeech::m_GetCustomizationTested
	bool ___m_GetCustomizationTested_10;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestTextToSpeech::m_UpdateCustomizationTested
	bool ___m_UpdateCustomizationTested_11;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestTextToSpeech::m_GetCustomizationWordsTested
	bool ___m_GetCustomizationWordsTested_12;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestTextToSpeech::m_AddCustomizationWordsTested
	bool ___m_AddCustomizationWordsTested_13;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestTextToSpeech::m_GetCustomizationWordTested
	bool ___m_GetCustomizationWordTested_14;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestTextToSpeech::m_DeleteCustomizationWordTested
	bool ___m_DeleteCustomizationWordTested_15;
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestTextToSpeech::m_CustomizationIDToTest
	String_t* ___m_CustomizationIDToTest_16;
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestTextToSpeech::m_CustomizationToCreateName
	String_t* ___m_CustomizationToCreateName_17;
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestTextToSpeech::m_CustomizationToCreateLanguage
	String_t* ___m_CustomizationToCreateLanguage_18;
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestTextToSpeech::m_CustomizationToCreateDescription
	String_t* ___m_CustomizationToCreateDescription_19;
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestTextToSpeech::m_UpdateWord0
	String_t* ___m_UpdateWord0_20;
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestTextToSpeech::m_UpdateWord1
	String_t* ___m_UpdateWord1_21;
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestTextToSpeech::m_UpdateTranslation0
	String_t* ___m_UpdateTranslation0_22;
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestTextToSpeech::m_UpdateTranslation1
	String_t* ___m_UpdateTranslation1_23;
	// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Word IBM.Watson.DeveloperCloud.UnitTests.TestTextToSpeech::m_UpdateWordObject0
	Word_t4274554970 * ___m_UpdateWordObject0_24;
	// IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1.Word IBM.Watson.DeveloperCloud.UnitTests.TestTextToSpeech::m_UpdateWordObject1
	Word_t4274554970 * ___m_UpdateWordObject1_25;
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestTextToSpeech::m_CustomizationIdCreated
	String_t* ___m_CustomizationIdCreated_26;

public:
	inline static int32_t get_offset_of_m_TextToSpeech_1() { return static_cast<int32_t>(offsetof(TestTextToSpeech_t3466033430, ___m_TextToSpeech_1)); }
	inline TextToSpeech_t3349357562 * get_m_TextToSpeech_1() const { return ___m_TextToSpeech_1; }
	inline TextToSpeech_t3349357562 ** get_address_of_m_TextToSpeech_1() { return &___m_TextToSpeech_1; }
	inline void set_m_TextToSpeech_1(TextToSpeech_t3349357562 * value)
	{
		___m_TextToSpeech_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextToSpeech_1), value);
	}

	inline static int32_t get_offset_of_m_GetTested_2() { return static_cast<int32_t>(offsetof(TestTextToSpeech_t3466033430, ___m_GetTested_2)); }
	inline bool get_m_GetTested_2() const { return ___m_GetTested_2; }
	inline bool* get_address_of_m_GetTested_2() { return &___m_GetTested_2; }
	inline void set_m_GetTested_2(bool value)
	{
		___m_GetTested_2 = value;
	}

	inline static int32_t get_offset_of_m_PostTested_3() { return static_cast<int32_t>(offsetof(TestTextToSpeech_t3466033430, ___m_PostTested_3)); }
	inline bool get_m_PostTested_3() const { return ___m_PostTested_3; }
	inline bool* get_address_of_m_PostTested_3() { return &___m_PostTested_3; }
	inline void set_m_PostTested_3(bool value)
	{
		___m_PostTested_3 = value;
	}

	inline static int32_t get_offset_of_m_GetVoicesTested_4() { return static_cast<int32_t>(offsetof(TestTextToSpeech_t3466033430, ___m_GetVoicesTested_4)); }
	inline bool get_m_GetVoicesTested_4() const { return ___m_GetVoicesTested_4; }
	inline bool* get_address_of_m_GetVoicesTested_4() { return &___m_GetVoicesTested_4; }
	inline void set_m_GetVoicesTested_4(bool value)
	{
		___m_GetVoicesTested_4 = value;
	}

	inline static int32_t get_offset_of_m_GetVoiceTested_5() { return static_cast<int32_t>(offsetof(TestTextToSpeech_t3466033430, ___m_GetVoiceTested_5)); }
	inline bool get_m_GetVoiceTested_5() const { return ___m_GetVoiceTested_5; }
	inline bool* get_address_of_m_GetVoiceTested_5() { return &___m_GetVoiceTested_5; }
	inline void set_m_GetVoiceTested_5(bool value)
	{
		___m_GetVoiceTested_5 = value;
	}

	inline static int32_t get_offset_of_m_GetPronunciationTested_6() { return static_cast<int32_t>(offsetof(TestTextToSpeech_t3466033430, ___m_GetPronunciationTested_6)); }
	inline bool get_m_GetPronunciationTested_6() const { return ___m_GetPronunciationTested_6; }
	inline bool* get_address_of_m_GetPronunciationTested_6() { return &___m_GetPronunciationTested_6; }
	inline void set_m_GetPronunciationTested_6(bool value)
	{
		___m_GetPronunciationTested_6 = value;
	}

	inline static int32_t get_offset_of_m_GetCustomizationsTested_7() { return static_cast<int32_t>(offsetof(TestTextToSpeech_t3466033430, ___m_GetCustomizationsTested_7)); }
	inline bool get_m_GetCustomizationsTested_7() const { return ___m_GetCustomizationsTested_7; }
	inline bool* get_address_of_m_GetCustomizationsTested_7() { return &___m_GetCustomizationsTested_7; }
	inline void set_m_GetCustomizationsTested_7(bool value)
	{
		___m_GetCustomizationsTested_7 = value;
	}

	inline static int32_t get_offset_of_m_CreateCustomizationTested_8() { return static_cast<int32_t>(offsetof(TestTextToSpeech_t3466033430, ___m_CreateCustomizationTested_8)); }
	inline bool get_m_CreateCustomizationTested_8() const { return ___m_CreateCustomizationTested_8; }
	inline bool* get_address_of_m_CreateCustomizationTested_8() { return &___m_CreateCustomizationTested_8; }
	inline void set_m_CreateCustomizationTested_8(bool value)
	{
		___m_CreateCustomizationTested_8 = value;
	}

	inline static int32_t get_offset_of_m_DeleteCustomizationTested_9() { return static_cast<int32_t>(offsetof(TestTextToSpeech_t3466033430, ___m_DeleteCustomizationTested_9)); }
	inline bool get_m_DeleteCustomizationTested_9() const { return ___m_DeleteCustomizationTested_9; }
	inline bool* get_address_of_m_DeleteCustomizationTested_9() { return &___m_DeleteCustomizationTested_9; }
	inline void set_m_DeleteCustomizationTested_9(bool value)
	{
		___m_DeleteCustomizationTested_9 = value;
	}

	inline static int32_t get_offset_of_m_GetCustomizationTested_10() { return static_cast<int32_t>(offsetof(TestTextToSpeech_t3466033430, ___m_GetCustomizationTested_10)); }
	inline bool get_m_GetCustomizationTested_10() const { return ___m_GetCustomizationTested_10; }
	inline bool* get_address_of_m_GetCustomizationTested_10() { return &___m_GetCustomizationTested_10; }
	inline void set_m_GetCustomizationTested_10(bool value)
	{
		___m_GetCustomizationTested_10 = value;
	}

	inline static int32_t get_offset_of_m_UpdateCustomizationTested_11() { return static_cast<int32_t>(offsetof(TestTextToSpeech_t3466033430, ___m_UpdateCustomizationTested_11)); }
	inline bool get_m_UpdateCustomizationTested_11() const { return ___m_UpdateCustomizationTested_11; }
	inline bool* get_address_of_m_UpdateCustomizationTested_11() { return &___m_UpdateCustomizationTested_11; }
	inline void set_m_UpdateCustomizationTested_11(bool value)
	{
		___m_UpdateCustomizationTested_11 = value;
	}

	inline static int32_t get_offset_of_m_GetCustomizationWordsTested_12() { return static_cast<int32_t>(offsetof(TestTextToSpeech_t3466033430, ___m_GetCustomizationWordsTested_12)); }
	inline bool get_m_GetCustomizationWordsTested_12() const { return ___m_GetCustomizationWordsTested_12; }
	inline bool* get_address_of_m_GetCustomizationWordsTested_12() { return &___m_GetCustomizationWordsTested_12; }
	inline void set_m_GetCustomizationWordsTested_12(bool value)
	{
		___m_GetCustomizationWordsTested_12 = value;
	}

	inline static int32_t get_offset_of_m_AddCustomizationWordsTested_13() { return static_cast<int32_t>(offsetof(TestTextToSpeech_t3466033430, ___m_AddCustomizationWordsTested_13)); }
	inline bool get_m_AddCustomizationWordsTested_13() const { return ___m_AddCustomizationWordsTested_13; }
	inline bool* get_address_of_m_AddCustomizationWordsTested_13() { return &___m_AddCustomizationWordsTested_13; }
	inline void set_m_AddCustomizationWordsTested_13(bool value)
	{
		___m_AddCustomizationWordsTested_13 = value;
	}

	inline static int32_t get_offset_of_m_GetCustomizationWordTested_14() { return static_cast<int32_t>(offsetof(TestTextToSpeech_t3466033430, ___m_GetCustomizationWordTested_14)); }
	inline bool get_m_GetCustomizationWordTested_14() const { return ___m_GetCustomizationWordTested_14; }
	inline bool* get_address_of_m_GetCustomizationWordTested_14() { return &___m_GetCustomizationWordTested_14; }
	inline void set_m_GetCustomizationWordTested_14(bool value)
	{
		___m_GetCustomizationWordTested_14 = value;
	}

	inline static int32_t get_offset_of_m_DeleteCustomizationWordTested_15() { return static_cast<int32_t>(offsetof(TestTextToSpeech_t3466033430, ___m_DeleteCustomizationWordTested_15)); }
	inline bool get_m_DeleteCustomizationWordTested_15() const { return ___m_DeleteCustomizationWordTested_15; }
	inline bool* get_address_of_m_DeleteCustomizationWordTested_15() { return &___m_DeleteCustomizationWordTested_15; }
	inline void set_m_DeleteCustomizationWordTested_15(bool value)
	{
		___m_DeleteCustomizationWordTested_15 = value;
	}

	inline static int32_t get_offset_of_m_CustomizationIDToTest_16() { return static_cast<int32_t>(offsetof(TestTextToSpeech_t3466033430, ___m_CustomizationIDToTest_16)); }
	inline String_t* get_m_CustomizationIDToTest_16() const { return ___m_CustomizationIDToTest_16; }
	inline String_t** get_address_of_m_CustomizationIDToTest_16() { return &___m_CustomizationIDToTest_16; }
	inline void set_m_CustomizationIDToTest_16(String_t* value)
	{
		___m_CustomizationIDToTest_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_CustomizationIDToTest_16), value);
	}

	inline static int32_t get_offset_of_m_CustomizationToCreateName_17() { return static_cast<int32_t>(offsetof(TestTextToSpeech_t3466033430, ___m_CustomizationToCreateName_17)); }
	inline String_t* get_m_CustomizationToCreateName_17() const { return ___m_CustomizationToCreateName_17; }
	inline String_t** get_address_of_m_CustomizationToCreateName_17() { return &___m_CustomizationToCreateName_17; }
	inline void set_m_CustomizationToCreateName_17(String_t* value)
	{
		___m_CustomizationToCreateName_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_CustomizationToCreateName_17), value);
	}

	inline static int32_t get_offset_of_m_CustomizationToCreateLanguage_18() { return static_cast<int32_t>(offsetof(TestTextToSpeech_t3466033430, ___m_CustomizationToCreateLanguage_18)); }
	inline String_t* get_m_CustomizationToCreateLanguage_18() const { return ___m_CustomizationToCreateLanguage_18; }
	inline String_t** get_address_of_m_CustomizationToCreateLanguage_18() { return &___m_CustomizationToCreateLanguage_18; }
	inline void set_m_CustomizationToCreateLanguage_18(String_t* value)
	{
		___m_CustomizationToCreateLanguage_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_CustomizationToCreateLanguage_18), value);
	}

	inline static int32_t get_offset_of_m_CustomizationToCreateDescription_19() { return static_cast<int32_t>(offsetof(TestTextToSpeech_t3466033430, ___m_CustomizationToCreateDescription_19)); }
	inline String_t* get_m_CustomizationToCreateDescription_19() const { return ___m_CustomizationToCreateDescription_19; }
	inline String_t** get_address_of_m_CustomizationToCreateDescription_19() { return &___m_CustomizationToCreateDescription_19; }
	inline void set_m_CustomizationToCreateDescription_19(String_t* value)
	{
		___m_CustomizationToCreateDescription_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_CustomizationToCreateDescription_19), value);
	}

	inline static int32_t get_offset_of_m_UpdateWord0_20() { return static_cast<int32_t>(offsetof(TestTextToSpeech_t3466033430, ___m_UpdateWord0_20)); }
	inline String_t* get_m_UpdateWord0_20() const { return ___m_UpdateWord0_20; }
	inline String_t** get_address_of_m_UpdateWord0_20() { return &___m_UpdateWord0_20; }
	inline void set_m_UpdateWord0_20(String_t* value)
	{
		___m_UpdateWord0_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_UpdateWord0_20), value);
	}

	inline static int32_t get_offset_of_m_UpdateWord1_21() { return static_cast<int32_t>(offsetof(TestTextToSpeech_t3466033430, ___m_UpdateWord1_21)); }
	inline String_t* get_m_UpdateWord1_21() const { return ___m_UpdateWord1_21; }
	inline String_t** get_address_of_m_UpdateWord1_21() { return &___m_UpdateWord1_21; }
	inline void set_m_UpdateWord1_21(String_t* value)
	{
		___m_UpdateWord1_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_UpdateWord1_21), value);
	}

	inline static int32_t get_offset_of_m_UpdateTranslation0_22() { return static_cast<int32_t>(offsetof(TestTextToSpeech_t3466033430, ___m_UpdateTranslation0_22)); }
	inline String_t* get_m_UpdateTranslation0_22() const { return ___m_UpdateTranslation0_22; }
	inline String_t** get_address_of_m_UpdateTranslation0_22() { return &___m_UpdateTranslation0_22; }
	inline void set_m_UpdateTranslation0_22(String_t* value)
	{
		___m_UpdateTranslation0_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_UpdateTranslation0_22), value);
	}

	inline static int32_t get_offset_of_m_UpdateTranslation1_23() { return static_cast<int32_t>(offsetof(TestTextToSpeech_t3466033430, ___m_UpdateTranslation1_23)); }
	inline String_t* get_m_UpdateTranslation1_23() const { return ___m_UpdateTranslation1_23; }
	inline String_t** get_address_of_m_UpdateTranslation1_23() { return &___m_UpdateTranslation1_23; }
	inline void set_m_UpdateTranslation1_23(String_t* value)
	{
		___m_UpdateTranslation1_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_UpdateTranslation1_23), value);
	}

	inline static int32_t get_offset_of_m_UpdateWordObject0_24() { return static_cast<int32_t>(offsetof(TestTextToSpeech_t3466033430, ___m_UpdateWordObject0_24)); }
	inline Word_t4274554970 * get_m_UpdateWordObject0_24() const { return ___m_UpdateWordObject0_24; }
	inline Word_t4274554970 ** get_address_of_m_UpdateWordObject0_24() { return &___m_UpdateWordObject0_24; }
	inline void set_m_UpdateWordObject0_24(Word_t4274554970 * value)
	{
		___m_UpdateWordObject0_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_UpdateWordObject0_24), value);
	}

	inline static int32_t get_offset_of_m_UpdateWordObject1_25() { return static_cast<int32_t>(offsetof(TestTextToSpeech_t3466033430, ___m_UpdateWordObject1_25)); }
	inline Word_t4274554970 * get_m_UpdateWordObject1_25() const { return ___m_UpdateWordObject1_25; }
	inline Word_t4274554970 ** get_address_of_m_UpdateWordObject1_25() { return &___m_UpdateWordObject1_25; }
	inline void set_m_UpdateWordObject1_25(Word_t4274554970 * value)
	{
		___m_UpdateWordObject1_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_UpdateWordObject1_25), value);
	}

	inline static int32_t get_offset_of_m_CustomizationIdCreated_26() { return static_cast<int32_t>(offsetof(TestTextToSpeech_t3466033430, ___m_CustomizationIdCreated_26)); }
	inline String_t* get_m_CustomizationIdCreated_26() const { return ___m_CustomizationIdCreated_26; }
	inline String_t** get_address_of_m_CustomizationIdCreated_26() { return &___m_CustomizationIdCreated_26; }
	inline void set_m_CustomizationIdCreated_26(String_t* value)
	{
		___m_CustomizationIdCreated_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_CustomizationIdCreated_26), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TESTTEXTTOSPEECH_T3466033430_H
#ifndef TRAINCLASSIFIERREQ_T531588072_H
#define TRAINCLASSIFIERREQ_T531588072_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/TrainClassifierReq
struct  TrainClassifierReq_t531588072  : public Request_t466816980
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/TrainClassifierReq::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_11;
	// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/OnTrainClassifier IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/TrainClassifierReq::<Callback>k__BackingField
	OnTrainClassifier_t294819893 * ___U3CCallbackU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(TrainClassifierReq_t531588072, ___U3CDataU3Ek__BackingField_11)); }
	inline String_t* get_U3CDataU3Ek__BackingField_11() const { return ___U3CDataU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_11() { return &___U3CDataU3Ek__BackingField_11; }
	inline void set_U3CDataU3Ek__BackingField_11(String_t* value)
	{
		___U3CDataU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(TrainClassifierReq_t531588072, ___U3CCallbackU3Ek__BackingField_12)); }
	inline OnTrainClassifier_t294819893 * get_U3CCallbackU3Ek__BackingField_12() const { return ___U3CCallbackU3Ek__BackingField_12; }
	inline OnTrainClassifier_t294819893 ** get_address_of_U3CCallbackU3Ek__BackingField_12() { return &___U3CCallbackU3Ek__BackingField_12; }
	inline void set_U3CCallbackU3Ek__BackingField_12(OnTrainClassifier_t294819893 * value)
	{
		___U3CCallbackU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRAINCLASSIFIERREQ_T531588072_H
#ifndef DELETECLASSIFIERREQ_T2958897583_H
#define DELETECLASSIFIERREQ_T2958897583_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/DeleteClassifierReq
struct  DeleteClassifierReq_t2958897583  : public Request_t466816980
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/DeleteClassifierReq::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_11;
	// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/OnDeleteClassifier IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/DeleteClassifierReq::<Callback>k__BackingField
	OnDeleteClassifier_t917631666 * ___U3CCallbackU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(DeleteClassifierReq_t2958897583, ___U3CDataU3Ek__BackingField_11)); }
	inline String_t* get_U3CDataU3Ek__BackingField_11() const { return ___U3CDataU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_11() { return &___U3CDataU3Ek__BackingField_11; }
	inline void set_U3CDataU3Ek__BackingField_11(String_t* value)
	{
		___U3CDataU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(DeleteClassifierReq_t2958897583, ___U3CCallbackU3Ek__BackingField_12)); }
	inline OnDeleteClassifier_t917631666 * get_U3CCallbackU3Ek__BackingField_12() const { return ___U3CCallbackU3Ek__BackingField_12; }
	inline OnDeleteClassifier_t917631666 ** get_address_of_U3CCallbackU3Ek__BackingField_12() { return &___U3CCallbackU3Ek__BackingField_12; }
	inline void set_U3CCallbackU3Ek__BackingField_12(OnDeleteClassifier_t917631666 * value)
	{
		___U3CCallbackU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELETECLASSIFIERREQ_T2958897583_H
#ifndef GETCLASSIFIERSREQ_T425495667_H
#define GETCLASSIFIERSREQ_T425495667_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/GetClassifiersReq
struct  GetClassifiersReq_t425495667  : public Request_t466816980
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/GetClassifiersReq::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_11;
	// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/OnGetClassifiers IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/GetClassifiersReq::<Callback>k__BackingField
	OnGetClassifiers_t226395526 * ___U3CCallbackU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetClassifiersReq_t425495667, ___U3CDataU3Ek__BackingField_11)); }
	inline String_t* get_U3CDataU3Ek__BackingField_11() const { return ___U3CDataU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_11() { return &___U3CDataU3Ek__BackingField_11; }
	inline void set_U3CDataU3Ek__BackingField_11(String_t* value)
	{
		___U3CDataU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GetClassifiersReq_t425495667, ___U3CCallbackU3Ek__BackingField_12)); }
	inline OnGetClassifiers_t226395526 * get_U3CCallbackU3Ek__BackingField_12() const { return ___U3CCallbackU3Ek__BackingField_12; }
	inline OnGetClassifiers_t226395526 ** get_address_of_U3CCallbackU3Ek__BackingField_12() { return &___U3CCallbackU3Ek__BackingField_12; }
	inline void set_U3CCallbackU3Ek__BackingField_12(OnGetClassifiers_t226395526 * value)
	{
		___U3CCallbackU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETCLASSIFIERSREQ_T425495667_H
#ifndef GETCLASSIFIERREQ_T1479437154_H
#define GETCLASSIFIERREQ_T1479437154_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/GetClassifierReq
struct  GetClassifierReq_t1479437154  : public Request_t466816980
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/GetClassifierReq::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_11;
	// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/OnGetClassifier IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/GetClassifierReq::<Callback>k__BackingField
	OnGetClassifier_t713440359 * ___U3CCallbackU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetClassifierReq_t1479437154, ___U3CDataU3Ek__BackingField_11)); }
	inline String_t* get_U3CDataU3Ek__BackingField_11() const { return ___U3CDataU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_11() { return &___U3CDataU3Ek__BackingField_11; }
	inline void set_U3CDataU3Ek__BackingField_11(String_t* value)
	{
		___U3CDataU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GetClassifierReq_t1479437154, ___U3CCallbackU3Ek__BackingField_12)); }
	inline OnGetClassifier_t713440359 * get_U3CCallbackU3Ek__BackingField_12() const { return ___U3CCallbackU3Ek__BackingField_12; }
	inline OnGetClassifier_t713440359 ** get_address_of_U3CCallbackU3Ek__BackingField_12() { return &___U3CCallbackU3Ek__BackingField_12; }
	inline void set_U3CCallbackU3Ek__BackingField_12(OnGetClassifier_t713440359 * value)
	{
		___U3CCallbackU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETCLASSIFIERREQ_T1479437154_H
#ifndef DELETECOLLECTIONREQ_T2946528802_H
#define DELETECOLLECTIONREQ_T2946528802_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/DeleteCollectionReq
struct  DeleteCollectionReq_t2946528802  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/OnDeleteCollection IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/DeleteCollectionReq::<Callback>k__BackingField
	OnDeleteCollection_t1354946561 * ___U3CCallbackU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/DeleteCollectionReq::<CollectionID>k__BackingField
	String_t* ___U3CCollectionIDU3Ek__BackingField_12;
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/DeleteCollectionReq::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(DeleteCollectionReq_t2946528802, ___U3CCallbackU3Ek__BackingField_11)); }
	inline OnDeleteCollection_t1354946561 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline OnDeleteCollection_t1354946561 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(OnDeleteCollection_t1354946561 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCollectionIDU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(DeleteCollectionReq_t2946528802, ___U3CCollectionIDU3Ek__BackingField_12)); }
	inline String_t* get_U3CCollectionIDU3Ek__BackingField_12() const { return ___U3CCollectionIDU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CCollectionIDU3Ek__BackingField_12() { return &___U3CCollectionIDU3Ek__BackingField_12; }
	inline void set_U3CCollectionIDU3Ek__BackingField_12(String_t* value)
	{
		___U3CCollectionIDU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCollectionIDU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(DeleteCollectionReq_t2946528802, ___U3CDataU3Ek__BackingField_13)); }
	inline String_t* get_U3CDataU3Ek__BackingField_13() const { return ___U3CDataU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_13() { return &___U3CDataU3Ek__BackingField_13; }
	inline void set_U3CDataU3Ek__BackingField_13(String_t* value)
	{
		___U3CDataU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELETECOLLECTIONREQ_T2946528802_H
#ifndef GETCOLLECTIONREQ_T1390443149_H
#define GETCOLLECTIONREQ_T1390443149_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/GetCollectionReq
struct  GetCollectionReq_t1390443149  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/OnGetCollection IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/GetCollectionReq::<Callback>k__BackingField
	OnGetCollection_t621465616 * ___U3CCallbackU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/GetCollectionReq::<CollectionID>k__BackingField
	String_t* ___U3CCollectionIDU3Ek__BackingField_12;
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/GetCollectionReq::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetCollectionReq_t1390443149, ___U3CCallbackU3Ek__BackingField_11)); }
	inline OnGetCollection_t621465616 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline OnGetCollection_t621465616 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(OnGetCollection_t621465616 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCollectionIDU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GetCollectionReq_t1390443149, ___U3CCollectionIDU3Ek__BackingField_12)); }
	inline String_t* get_U3CCollectionIDU3Ek__BackingField_12() const { return ___U3CCollectionIDU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CCollectionIDU3Ek__BackingField_12() { return &___U3CCollectionIDU3Ek__BackingField_12; }
	inline void set_U3CCollectionIDU3Ek__BackingField_12(String_t* value)
	{
		___U3CCollectionIDU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCollectionIDU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(GetCollectionReq_t1390443149, ___U3CDataU3Ek__BackingField_13)); }
	inline String_t* get_U3CDataU3Ek__BackingField_13() const { return ___U3CDataU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_13() { return &___U3CDataU3Ek__BackingField_13; }
	inline void set_U3CDataU3Ek__BackingField_13(String_t* value)
	{
		___U3CDataU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETCOLLECTIONREQ_T1390443149_H
#ifndef GETCOLLECTIONSREQ_T4268407020_H
#define GETCOLLECTIONSREQ_T4268407020_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/GetCollectionsReq
struct  GetCollectionsReq_t4268407020  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/OnGetCollections IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/GetCollectionsReq::<Callback>k__BackingField
	OnGetCollections_t484200113 * ___U3CCallbackU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/GetCollectionsReq::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetCollectionsReq_t4268407020, ___U3CCallbackU3Ek__BackingField_11)); }
	inline OnGetCollections_t484200113 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline OnGetCollections_t484200113 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(OnGetCollections_t484200113 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GetCollectionsReq_t4268407020, ___U3CDataU3Ek__BackingField_12)); }
	inline String_t* get_U3CDataU3Ek__BackingField_12() const { return ___U3CDataU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_12() { return &___U3CDataU3Ek__BackingField_12; }
	inline void set_U3CDataU3Ek__BackingField_12(String_t* value)
	{
		___U3CDataU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETCOLLECTIONSREQ_T4268407020_H
#ifndef CREATECOLLECTIONREQ_T2577939415_H
#define CREATECOLLECTIONREQ_T2577939415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/CreateCollectionReq
struct  CreateCollectionReq_t2577939415  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/OnCreateCollection IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/CreateCollectionReq::<Callback>k__BackingField
	OnCreateCollection_t970139976 * ___U3CCallbackU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/CreateCollectionReq::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_12;
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/CreateCollectionReq::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(CreateCollectionReq_t2577939415, ___U3CCallbackU3Ek__BackingField_11)); }
	inline OnCreateCollection_t970139976 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline OnCreateCollection_t970139976 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(OnCreateCollection_t970139976 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(CreateCollectionReq_t2577939415, ___U3CNameU3Ek__BackingField_12)); }
	inline String_t* get_U3CNameU3Ek__BackingField_12() const { return ___U3CNameU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_12() { return &___U3CNameU3Ek__BackingField_12; }
	inline void set_U3CNameU3Ek__BackingField_12(String_t* value)
	{
		___U3CNameU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(CreateCollectionReq_t2577939415, ___U3CDataU3Ek__BackingField_13)); }
	inline String_t* get_U3CDataU3Ek__BackingField_13() const { return ___U3CDataU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_13() { return &___U3CDataU3Ek__BackingField_13; }
	inline void set_U3CDataU3Ek__BackingField_13(String_t* value)
	{
		___U3CDataU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREATECOLLECTIONREQ_T2577939415_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t1328083999* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t1328083999* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t1328083999** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t1328083999* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef BOOLEAN_T3825574718_H
#define BOOLEAN_T3825574718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t3825574718 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t3825574718, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t3825574718_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t3825574718_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t3825574718_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T3825574718_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	IntPtr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline IntPtr_t get_Zero_1() const { return ___Zero_1; }
	inline IntPtr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(IntPtr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef TIMESPAN_T3430258949_H
#define TIMESPAN_T3430258949_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t3430258949 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_8;

public:
	inline static int32_t get_offset_of__ticks_8() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949, ____ticks_8)); }
	inline int64_t get__ticks_8() const { return ____ticks_8; }
	inline int64_t* get_address_of__ticks_8() { return &____ticks_8; }
	inline void set__ticks_8(int64_t value)
	{
		____ticks_8 = value;
	}
};

struct TimeSpan_t3430258949_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t3430258949  ___MaxValue_5;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t3430258949  ___MinValue_6;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t3430258949  ___Zero_7;

public:
	inline static int32_t get_offset_of_MaxValue_5() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949_StaticFields, ___MaxValue_5)); }
	inline TimeSpan_t3430258949  get_MaxValue_5() const { return ___MaxValue_5; }
	inline TimeSpan_t3430258949 * get_address_of_MaxValue_5() { return &___MaxValue_5; }
	inline void set_MaxValue_5(TimeSpan_t3430258949  value)
	{
		___MaxValue_5 = value;
	}

	inline static int32_t get_offset_of_MinValue_6() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949_StaticFields, ___MinValue_6)); }
	inline TimeSpan_t3430258949  get_MinValue_6() const { return ___MinValue_6; }
	inline TimeSpan_t3430258949 * get_address_of_MinValue_6() { return &___MinValue_6; }
	inline void set_MinValue_6(TimeSpan_t3430258949  value)
	{
		___MinValue_6 = value;
	}

	inline static int32_t get_offset_of_Zero_7() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949_StaticFields, ___Zero_7)); }
	inline TimeSpan_t3430258949  get_Zero_7() const { return ___Zero_7; }
	inline TimeSpan_t3430258949 * get_address_of_Zero_7() { return &___Zero_7; }
	inline void set_Zero_7(TimeSpan_t3430258949  value)
	{
		___Zero_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T3430258949_H
#ifndef DETECTFACESREQ_T538961576_H
#define DETECTFACESREQ_T538961576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/DetectFacesReq
struct  DetectFacesReq_t538961576  : public Request_t466816980
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/DetectFacesReq::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_11;
	// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/OnDetectFaces IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/DetectFacesReq::<Callback>k__BackingField
	OnDetectFaces_t4020255763 * ___U3CCallbackU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(DetectFacesReq_t538961576, ___U3CDataU3Ek__BackingField_11)); }
	inline String_t* get_U3CDataU3Ek__BackingField_11() const { return ___U3CDataU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_11() { return &___U3CDataU3Ek__BackingField_11; }
	inline void set_U3CDataU3Ek__BackingField_11(String_t* value)
	{
		___U3CDataU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(DetectFacesReq_t538961576, ___U3CCallbackU3Ek__BackingField_12)); }
	inline OnDetectFaces_t4020255763 * get_U3CCallbackU3Ek__BackingField_12() const { return ___U3CCallbackU3Ek__BackingField_12; }
	inline OnDetectFaces_t4020255763 ** get_address_of_U3CCallbackU3Ek__BackingField_12() { return &___U3CCallbackU3Ek__BackingField_12; }
	inline void set_U3CCallbackU3Ek__BackingField_12(OnDetectFaces_t4020255763 * value)
	{
		___U3CCallbackU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DETECTFACESREQ_T538961576_H
#ifndef RECOGNIZETEXTREQ_T3895541236_H
#define RECOGNIZETEXTREQ_T3895541236_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/RecognizeTextReq
struct  RecognizeTextReq_t3895541236  : public Request_t466816980
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/RecognizeTextReq::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_11;
	// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/OnRecognizeText IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/RecognizeTextReq::<Callback>k__BackingField
	OnRecognizeText_t793159563 * ___U3CCallbackU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(RecognizeTextReq_t3895541236, ___U3CDataU3Ek__BackingField_11)); }
	inline String_t* get_U3CDataU3Ek__BackingField_11() const { return ___U3CDataU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_11() { return &___U3CDataU3Ek__BackingField_11; }
	inline void set_U3CDataU3Ek__BackingField_11(String_t* value)
	{
		___U3CDataU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(RecognizeTextReq_t3895541236, ___U3CCallbackU3Ek__BackingField_12)); }
	inline OnRecognizeText_t793159563 * get_U3CCallbackU3Ek__BackingField_12() const { return ___U3CCallbackU3Ek__BackingField_12; }
	inline OnRecognizeText_t793159563 ** get_address_of_U3CCallbackU3Ek__BackingField_12() { return &___U3CCallbackU3Ek__BackingField_12; }
	inline void set_U3CCallbackU3Ek__BackingField_12(OnRecognizeText_t793159563 * value)
	{
		___U3CCallbackU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECOGNIZETEXTREQ_T3895541236_H
#ifndef VOID_T1841601450_H
#define VOID_T1841601450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1841601450 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1841601450_H
#ifndef CLASSIFYREQ_T1500879585_H
#define CLASSIFYREQ_T1500879585_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/ClassifyReq
struct  ClassifyReq_t1500879585  : public Request_t466816980
{
public:
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/ClassifyReq::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_11;
	// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/OnClassify IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/ClassifyReq::<Callback>k__BackingField
	OnClassify_t311067478 * ___U3CCallbackU3Ek__BackingField_12;
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/ClassifyReq::<AcceptLanguage>k__BackingField
	String_t* ___U3CAcceptLanguageU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(ClassifyReq_t1500879585, ___U3CDataU3Ek__BackingField_11)); }
	inline String_t* get_U3CDataU3Ek__BackingField_11() const { return ___U3CDataU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_11() { return &___U3CDataU3Ek__BackingField_11; }
	inline void set_U3CDataU3Ek__BackingField_11(String_t* value)
	{
		___U3CDataU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(ClassifyReq_t1500879585, ___U3CCallbackU3Ek__BackingField_12)); }
	inline OnClassify_t311067478 * get_U3CCallbackU3Ek__BackingField_12() const { return ___U3CCallbackU3Ek__BackingField_12; }
	inline OnClassify_t311067478 ** get_address_of_U3CCallbackU3Ek__BackingField_12() { return &___U3CCallbackU3Ek__BackingField_12; }
	inline void set_U3CCallbackU3Ek__BackingField_12(OnClassify_t311067478 * value)
	{
		___U3CCallbackU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CAcceptLanguageU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(ClassifyReq_t1500879585, ___U3CAcceptLanguageU3Ek__BackingField_13)); }
	inline String_t* get_U3CAcceptLanguageU3Ek__BackingField_13() const { return ___U3CAcceptLanguageU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CAcceptLanguageU3Ek__BackingField_13() { return &___U3CAcceptLanguageU3Ek__BackingField_13; }
	inline void set_U3CAcceptLanguageU3Ek__BackingField_13(String_t* value)
	{
		___U3CAcceptLanguageU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAcceptLanguageU3Ek__BackingField_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLASSIFYREQ_T1500879585_H
#ifndef GETCOLLECTIONIMAGESREQ_T1811544573_H
#define GETCOLLECTIONIMAGESREQ_T1811544573_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/GetCollectionImagesReq
struct  GetCollectionImagesReq_t1811544573  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/OnGetCollectionImages IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/GetCollectionImagesReq::<Callback>k__BackingField
	OnGetCollectionImages_t2736783742 * ___U3CCallbackU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/GetCollectionImagesReq::<CollectionID>k__BackingField
	String_t* ___U3CCollectionIDU3Ek__BackingField_12;
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/GetCollectionImagesReq::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetCollectionImagesReq_t1811544573, ___U3CCallbackU3Ek__BackingField_11)); }
	inline OnGetCollectionImages_t2736783742 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline OnGetCollectionImages_t2736783742 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(OnGetCollectionImages_t2736783742 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCollectionIDU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GetCollectionImagesReq_t1811544573, ___U3CCollectionIDU3Ek__BackingField_12)); }
	inline String_t* get_U3CCollectionIDU3Ek__BackingField_12() const { return ___U3CCollectionIDU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CCollectionIDU3Ek__BackingField_12() { return &___U3CCollectionIDU3Ek__BackingField_12; }
	inline void set_U3CCollectionIDU3Ek__BackingField_12(String_t* value)
	{
		___U3CCollectionIDU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCollectionIDU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(GetCollectionImagesReq_t1811544573, ___U3CDataU3Ek__BackingField_13)); }
	inline String_t* get_U3CDataU3Ek__BackingField_13() const { return ___U3CDataU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_13() { return &___U3CDataU3Ek__BackingField_13; }
	inline void set_U3CDataU3Ek__BackingField_13(String_t* value)
	{
		___U3CDataU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETCOLLECTIONIMAGESREQ_T1811544573_H
#ifndef TESTEVENTMANAGER_T1784053145_H
#define TESTEVENTMANAGER_T1784053145_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.UnitTests.TestEventManager
struct  TestEventManager_t1784053145  : public UnitTest_t237671048
{
public:
	// IBM.Watson.DeveloperCloud.Utilities.EventManager IBM.Watson.DeveloperCloud.UnitTests.TestEventManager::m_Manager
	EventManager_t605335149 * ___m_Manager_1;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestEventManager::m_SendTested
	bool ___m_SendTested_2;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestEventManager::m_SendAsyncTested
	bool ___m_SendAsyncTested_3;

public:
	inline static int32_t get_offset_of_m_Manager_1() { return static_cast<int32_t>(offsetof(TestEventManager_t1784053145, ___m_Manager_1)); }
	inline EventManager_t605335149 * get_m_Manager_1() const { return ___m_Manager_1; }
	inline EventManager_t605335149 ** get_address_of_m_Manager_1() { return &___m_Manager_1; }
	inline void set_m_Manager_1(EventManager_t605335149 * value)
	{
		___m_Manager_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Manager_1), value);
	}

	inline static int32_t get_offset_of_m_SendTested_2() { return static_cast<int32_t>(offsetof(TestEventManager_t1784053145, ___m_SendTested_2)); }
	inline bool get_m_SendTested_2() const { return ___m_SendTested_2; }
	inline bool* get_address_of_m_SendTested_2() { return &___m_SendTested_2; }
	inline void set_m_SendTested_2(bool value)
	{
		___m_SendTested_2 = value;
	}

	inline static int32_t get_offset_of_m_SendAsyncTested_3() { return static_cast<int32_t>(offsetof(TestEventManager_t1784053145, ___m_SendAsyncTested_3)); }
	inline bool get_m_SendAsyncTested_3() const { return ___m_SendAsyncTested_3; }
	inline bool* get_address_of_m_SendAsyncTested_3() { return &___m_SendAsyncTested_3; }
	inline void set_m_SendAsyncTested_3(bool value)
	{
		___m_SendAsyncTested_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TESTEVENTMANAGER_T1784053145_H
#ifndef TESTDOCUMENTCONVERSION_T3132176613_H
#define TESTDOCUMENTCONVERSION_T3132176613_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.UnitTests.TestDocumentConversion
struct  TestDocumentConversion_t3132176613  : public UnitTest_t237671048
{
public:
	// IBM.Watson.DeveloperCloud.Services.DocumentConversion.v1.DocumentConversion IBM.Watson.DeveloperCloud.UnitTests.TestDocumentConversion::m_DocumentConversion
	DocumentConversion_t1610681923 * ___m_DocumentConversion_1;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestDocumentConversion::m_DocumentConversionAnswerUnitsTested
	bool ___m_DocumentConversionAnswerUnitsTested_2;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestDocumentConversion::m_DocumentConversionTextTested
	bool ___m_DocumentConversionTextTested_3;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestDocumentConversion::m_DocumentConversionHTMLTested
	bool ___m_DocumentConversionHTMLTested_4;

public:
	inline static int32_t get_offset_of_m_DocumentConversion_1() { return static_cast<int32_t>(offsetof(TestDocumentConversion_t3132176613, ___m_DocumentConversion_1)); }
	inline DocumentConversion_t1610681923 * get_m_DocumentConversion_1() const { return ___m_DocumentConversion_1; }
	inline DocumentConversion_t1610681923 ** get_address_of_m_DocumentConversion_1() { return &___m_DocumentConversion_1; }
	inline void set_m_DocumentConversion_1(DocumentConversion_t1610681923 * value)
	{
		___m_DocumentConversion_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_DocumentConversion_1), value);
	}

	inline static int32_t get_offset_of_m_DocumentConversionAnswerUnitsTested_2() { return static_cast<int32_t>(offsetof(TestDocumentConversion_t3132176613, ___m_DocumentConversionAnswerUnitsTested_2)); }
	inline bool get_m_DocumentConversionAnswerUnitsTested_2() const { return ___m_DocumentConversionAnswerUnitsTested_2; }
	inline bool* get_address_of_m_DocumentConversionAnswerUnitsTested_2() { return &___m_DocumentConversionAnswerUnitsTested_2; }
	inline void set_m_DocumentConversionAnswerUnitsTested_2(bool value)
	{
		___m_DocumentConversionAnswerUnitsTested_2 = value;
	}

	inline static int32_t get_offset_of_m_DocumentConversionTextTested_3() { return static_cast<int32_t>(offsetof(TestDocumentConversion_t3132176613, ___m_DocumentConversionTextTested_3)); }
	inline bool get_m_DocumentConversionTextTested_3() const { return ___m_DocumentConversionTextTested_3; }
	inline bool* get_address_of_m_DocumentConversionTextTested_3() { return &___m_DocumentConversionTextTested_3; }
	inline void set_m_DocumentConversionTextTested_3(bool value)
	{
		___m_DocumentConversionTextTested_3 = value;
	}

	inline static int32_t get_offset_of_m_DocumentConversionHTMLTested_4() { return static_cast<int32_t>(offsetof(TestDocumentConversion_t3132176613, ___m_DocumentConversionHTMLTested_4)); }
	inline bool get_m_DocumentConversionHTMLTested_4() const { return ___m_DocumentConversionHTMLTested_4; }
	inline bool* get_address_of_m_DocumentConversionHTMLTested_4() { return &___m_DocumentConversionHTMLTested_4; }
	inline void set_m_DocumentConversionHTMLTested_4(bool value)
	{
		___m_DocumentConversionHTMLTested_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TESTDOCUMENTCONVERSION_T3132176613_H
#ifndef TESTDISCOVERY_T1320997924_H
#define TESTDISCOVERY_T1320997924_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery
struct  TestDiscovery_t1320997924  : public UnitTest_t237671048
{
public:
	// IBM.Watson.DeveloperCloud.Services.Discovery.v1.Discovery IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery::m_Discovery
	Discovery_t1478121420 * ___m_Discovery_1;
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery::m_CreatedEnvironmentName
	String_t* ___m_CreatedEnvironmentName_2;
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery::m_CreatedEnvironmentDescription
	String_t* ___m_CreatedEnvironmentDescription_3;
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery::m_CreatedEnvironmentID
	String_t* ___m_CreatedEnvironmentID_4;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery::m_IsEnvironmentActive
	bool ___m_IsEnvironmentActive_5;
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery::m_ConfigurationJsonPath
	String_t* ___m_ConfigurationJsonPath_6;
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery::m_CreatedConfigurationID
	String_t* ___m_CreatedConfigurationID_7;
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery::m_CreatedCollectionName
	String_t* ___m_CreatedCollectionName_8;
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery::m_CreatedCollectionDescription
	String_t* ___m_CreatedCollectionDescription_9;
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery::m_CreatedCollectionID
	String_t* ___m_CreatedCollectionID_10;
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery::m_FilePathToIngest
	String_t* ___m_FilePathToIngest_11;
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery::m_DocumentFilePath
	String_t* ___m_DocumentFilePath_12;
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery::m_Metadata
	String_t* ___m_Metadata_13;
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery::m_CreatedDocumentID
	String_t* ___m_CreatedDocumentID_14;
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery::m_Query
	String_t* ___m_Query_15;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery::m_GetEnvironmentsTested
	bool ___m_GetEnvironmentsTested_16;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery::m_AddEnvironmentsTested
	bool ___m_AddEnvironmentsTested_17;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery::m_GetEnvironmentTested
	bool ___m_GetEnvironmentTested_18;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery::m_GetConfigurationsTested
	bool ___m_GetConfigurationsTested_19;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery::m_AddConfigurationTested
	bool ___m_AddConfigurationTested_20;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery::m_GetConfigurationTested
	bool ___m_GetConfigurationTested_21;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery::m_PreviewConfigurationTested
	bool ___m_PreviewConfigurationTested_22;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery::m_GetCollectionsTested
	bool ___m_GetCollectionsTested_23;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery::m_AddCollectionTested
	bool ___m_AddCollectionTested_24;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery::m_GetCollectionTested
	bool ___m_GetCollectionTested_25;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery::m_GetFieldsTested
	bool ___m_GetFieldsTested_26;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery::m_AddDocumentTested
	bool ___m_AddDocumentTested_27;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery::m_GetDocumentTested
	bool ___m_GetDocumentTested_28;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery::m_UpdateDocumentTested
	bool ___m_UpdateDocumentTested_29;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery::m_QueryTested
	bool ___m_QueryTested_30;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery::m_DeleteDocumentTested
	bool ___m_DeleteDocumentTested_31;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery::m_DeleteCollectionTested
	bool ___m_DeleteCollectionTested_32;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery::m_DeleteConfigurationTested
	bool ___m_DeleteConfigurationTested_33;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestDiscovery::m_DeleteEnvironmentTested
	bool ___m_DeleteEnvironmentTested_34;

public:
	inline static int32_t get_offset_of_m_Discovery_1() { return static_cast<int32_t>(offsetof(TestDiscovery_t1320997924, ___m_Discovery_1)); }
	inline Discovery_t1478121420 * get_m_Discovery_1() const { return ___m_Discovery_1; }
	inline Discovery_t1478121420 ** get_address_of_m_Discovery_1() { return &___m_Discovery_1; }
	inline void set_m_Discovery_1(Discovery_t1478121420 * value)
	{
		___m_Discovery_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Discovery_1), value);
	}

	inline static int32_t get_offset_of_m_CreatedEnvironmentName_2() { return static_cast<int32_t>(offsetof(TestDiscovery_t1320997924, ___m_CreatedEnvironmentName_2)); }
	inline String_t* get_m_CreatedEnvironmentName_2() const { return ___m_CreatedEnvironmentName_2; }
	inline String_t** get_address_of_m_CreatedEnvironmentName_2() { return &___m_CreatedEnvironmentName_2; }
	inline void set_m_CreatedEnvironmentName_2(String_t* value)
	{
		___m_CreatedEnvironmentName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_CreatedEnvironmentName_2), value);
	}

	inline static int32_t get_offset_of_m_CreatedEnvironmentDescription_3() { return static_cast<int32_t>(offsetof(TestDiscovery_t1320997924, ___m_CreatedEnvironmentDescription_3)); }
	inline String_t* get_m_CreatedEnvironmentDescription_3() const { return ___m_CreatedEnvironmentDescription_3; }
	inline String_t** get_address_of_m_CreatedEnvironmentDescription_3() { return &___m_CreatedEnvironmentDescription_3; }
	inline void set_m_CreatedEnvironmentDescription_3(String_t* value)
	{
		___m_CreatedEnvironmentDescription_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_CreatedEnvironmentDescription_3), value);
	}

	inline static int32_t get_offset_of_m_CreatedEnvironmentID_4() { return static_cast<int32_t>(offsetof(TestDiscovery_t1320997924, ___m_CreatedEnvironmentID_4)); }
	inline String_t* get_m_CreatedEnvironmentID_4() const { return ___m_CreatedEnvironmentID_4; }
	inline String_t** get_address_of_m_CreatedEnvironmentID_4() { return &___m_CreatedEnvironmentID_4; }
	inline void set_m_CreatedEnvironmentID_4(String_t* value)
	{
		___m_CreatedEnvironmentID_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_CreatedEnvironmentID_4), value);
	}

	inline static int32_t get_offset_of_m_IsEnvironmentActive_5() { return static_cast<int32_t>(offsetof(TestDiscovery_t1320997924, ___m_IsEnvironmentActive_5)); }
	inline bool get_m_IsEnvironmentActive_5() const { return ___m_IsEnvironmentActive_5; }
	inline bool* get_address_of_m_IsEnvironmentActive_5() { return &___m_IsEnvironmentActive_5; }
	inline void set_m_IsEnvironmentActive_5(bool value)
	{
		___m_IsEnvironmentActive_5 = value;
	}

	inline static int32_t get_offset_of_m_ConfigurationJsonPath_6() { return static_cast<int32_t>(offsetof(TestDiscovery_t1320997924, ___m_ConfigurationJsonPath_6)); }
	inline String_t* get_m_ConfigurationJsonPath_6() const { return ___m_ConfigurationJsonPath_6; }
	inline String_t** get_address_of_m_ConfigurationJsonPath_6() { return &___m_ConfigurationJsonPath_6; }
	inline void set_m_ConfigurationJsonPath_6(String_t* value)
	{
		___m_ConfigurationJsonPath_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_ConfigurationJsonPath_6), value);
	}

	inline static int32_t get_offset_of_m_CreatedConfigurationID_7() { return static_cast<int32_t>(offsetof(TestDiscovery_t1320997924, ___m_CreatedConfigurationID_7)); }
	inline String_t* get_m_CreatedConfigurationID_7() const { return ___m_CreatedConfigurationID_7; }
	inline String_t** get_address_of_m_CreatedConfigurationID_7() { return &___m_CreatedConfigurationID_7; }
	inline void set_m_CreatedConfigurationID_7(String_t* value)
	{
		___m_CreatedConfigurationID_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_CreatedConfigurationID_7), value);
	}

	inline static int32_t get_offset_of_m_CreatedCollectionName_8() { return static_cast<int32_t>(offsetof(TestDiscovery_t1320997924, ___m_CreatedCollectionName_8)); }
	inline String_t* get_m_CreatedCollectionName_8() const { return ___m_CreatedCollectionName_8; }
	inline String_t** get_address_of_m_CreatedCollectionName_8() { return &___m_CreatedCollectionName_8; }
	inline void set_m_CreatedCollectionName_8(String_t* value)
	{
		___m_CreatedCollectionName_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_CreatedCollectionName_8), value);
	}

	inline static int32_t get_offset_of_m_CreatedCollectionDescription_9() { return static_cast<int32_t>(offsetof(TestDiscovery_t1320997924, ___m_CreatedCollectionDescription_9)); }
	inline String_t* get_m_CreatedCollectionDescription_9() const { return ___m_CreatedCollectionDescription_9; }
	inline String_t** get_address_of_m_CreatedCollectionDescription_9() { return &___m_CreatedCollectionDescription_9; }
	inline void set_m_CreatedCollectionDescription_9(String_t* value)
	{
		___m_CreatedCollectionDescription_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_CreatedCollectionDescription_9), value);
	}

	inline static int32_t get_offset_of_m_CreatedCollectionID_10() { return static_cast<int32_t>(offsetof(TestDiscovery_t1320997924, ___m_CreatedCollectionID_10)); }
	inline String_t* get_m_CreatedCollectionID_10() const { return ___m_CreatedCollectionID_10; }
	inline String_t** get_address_of_m_CreatedCollectionID_10() { return &___m_CreatedCollectionID_10; }
	inline void set_m_CreatedCollectionID_10(String_t* value)
	{
		___m_CreatedCollectionID_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_CreatedCollectionID_10), value);
	}

	inline static int32_t get_offset_of_m_FilePathToIngest_11() { return static_cast<int32_t>(offsetof(TestDiscovery_t1320997924, ___m_FilePathToIngest_11)); }
	inline String_t* get_m_FilePathToIngest_11() const { return ___m_FilePathToIngest_11; }
	inline String_t** get_address_of_m_FilePathToIngest_11() { return &___m_FilePathToIngest_11; }
	inline void set_m_FilePathToIngest_11(String_t* value)
	{
		___m_FilePathToIngest_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_FilePathToIngest_11), value);
	}

	inline static int32_t get_offset_of_m_DocumentFilePath_12() { return static_cast<int32_t>(offsetof(TestDiscovery_t1320997924, ___m_DocumentFilePath_12)); }
	inline String_t* get_m_DocumentFilePath_12() const { return ___m_DocumentFilePath_12; }
	inline String_t** get_address_of_m_DocumentFilePath_12() { return &___m_DocumentFilePath_12; }
	inline void set_m_DocumentFilePath_12(String_t* value)
	{
		___m_DocumentFilePath_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_DocumentFilePath_12), value);
	}

	inline static int32_t get_offset_of_m_Metadata_13() { return static_cast<int32_t>(offsetof(TestDiscovery_t1320997924, ___m_Metadata_13)); }
	inline String_t* get_m_Metadata_13() const { return ___m_Metadata_13; }
	inline String_t** get_address_of_m_Metadata_13() { return &___m_Metadata_13; }
	inline void set_m_Metadata_13(String_t* value)
	{
		___m_Metadata_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_Metadata_13), value);
	}

	inline static int32_t get_offset_of_m_CreatedDocumentID_14() { return static_cast<int32_t>(offsetof(TestDiscovery_t1320997924, ___m_CreatedDocumentID_14)); }
	inline String_t* get_m_CreatedDocumentID_14() const { return ___m_CreatedDocumentID_14; }
	inline String_t** get_address_of_m_CreatedDocumentID_14() { return &___m_CreatedDocumentID_14; }
	inline void set_m_CreatedDocumentID_14(String_t* value)
	{
		___m_CreatedDocumentID_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_CreatedDocumentID_14), value);
	}

	inline static int32_t get_offset_of_m_Query_15() { return static_cast<int32_t>(offsetof(TestDiscovery_t1320997924, ___m_Query_15)); }
	inline String_t* get_m_Query_15() const { return ___m_Query_15; }
	inline String_t** get_address_of_m_Query_15() { return &___m_Query_15; }
	inline void set_m_Query_15(String_t* value)
	{
		___m_Query_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_Query_15), value);
	}

	inline static int32_t get_offset_of_m_GetEnvironmentsTested_16() { return static_cast<int32_t>(offsetof(TestDiscovery_t1320997924, ___m_GetEnvironmentsTested_16)); }
	inline bool get_m_GetEnvironmentsTested_16() const { return ___m_GetEnvironmentsTested_16; }
	inline bool* get_address_of_m_GetEnvironmentsTested_16() { return &___m_GetEnvironmentsTested_16; }
	inline void set_m_GetEnvironmentsTested_16(bool value)
	{
		___m_GetEnvironmentsTested_16 = value;
	}

	inline static int32_t get_offset_of_m_AddEnvironmentsTested_17() { return static_cast<int32_t>(offsetof(TestDiscovery_t1320997924, ___m_AddEnvironmentsTested_17)); }
	inline bool get_m_AddEnvironmentsTested_17() const { return ___m_AddEnvironmentsTested_17; }
	inline bool* get_address_of_m_AddEnvironmentsTested_17() { return &___m_AddEnvironmentsTested_17; }
	inline void set_m_AddEnvironmentsTested_17(bool value)
	{
		___m_AddEnvironmentsTested_17 = value;
	}

	inline static int32_t get_offset_of_m_GetEnvironmentTested_18() { return static_cast<int32_t>(offsetof(TestDiscovery_t1320997924, ___m_GetEnvironmentTested_18)); }
	inline bool get_m_GetEnvironmentTested_18() const { return ___m_GetEnvironmentTested_18; }
	inline bool* get_address_of_m_GetEnvironmentTested_18() { return &___m_GetEnvironmentTested_18; }
	inline void set_m_GetEnvironmentTested_18(bool value)
	{
		___m_GetEnvironmentTested_18 = value;
	}

	inline static int32_t get_offset_of_m_GetConfigurationsTested_19() { return static_cast<int32_t>(offsetof(TestDiscovery_t1320997924, ___m_GetConfigurationsTested_19)); }
	inline bool get_m_GetConfigurationsTested_19() const { return ___m_GetConfigurationsTested_19; }
	inline bool* get_address_of_m_GetConfigurationsTested_19() { return &___m_GetConfigurationsTested_19; }
	inline void set_m_GetConfigurationsTested_19(bool value)
	{
		___m_GetConfigurationsTested_19 = value;
	}

	inline static int32_t get_offset_of_m_AddConfigurationTested_20() { return static_cast<int32_t>(offsetof(TestDiscovery_t1320997924, ___m_AddConfigurationTested_20)); }
	inline bool get_m_AddConfigurationTested_20() const { return ___m_AddConfigurationTested_20; }
	inline bool* get_address_of_m_AddConfigurationTested_20() { return &___m_AddConfigurationTested_20; }
	inline void set_m_AddConfigurationTested_20(bool value)
	{
		___m_AddConfigurationTested_20 = value;
	}

	inline static int32_t get_offset_of_m_GetConfigurationTested_21() { return static_cast<int32_t>(offsetof(TestDiscovery_t1320997924, ___m_GetConfigurationTested_21)); }
	inline bool get_m_GetConfigurationTested_21() const { return ___m_GetConfigurationTested_21; }
	inline bool* get_address_of_m_GetConfigurationTested_21() { return &___m_GetConfigurationTested_21; }
	inline void set_m_GetConfigurationTested_21(bool value)
	{
		___m_GetConfigurationTested_21 = value;
	}

	inline static int32_t get_offset_of_m_PreviewConfigurationTested_22() { return static_cast<int32_t>(offsetof(TestDiscovery_t1320997924, ___m_PreviewConfigurationTested_22)); }
	inline bool get_m_PreviewConfigurationTested_22() const { return ___m_PreviewConfigurationTested_22; }
	inline bool* get_address_of_m_PreviewConfigurationTested_22() { return &___m_PreviewConfigurationTested_22; }
	inline void set_m_PreviewConfigurationTested_22(bool value)
	{
		___m_PreviewConfigurationTested_22 = value;
	}

	inline static int32_t get_offset_of_m_GetCollectionsTested_23() { return static_cast<int32_t>(offsetof(TestDiscovery_t1320997924, ___m_GetCollectionsTested_23)); }
	inline bool get_m_GetCollectionsTested_23() const { return ___m_GetCollectionsTested_23; }
	inline bool* get_address_of_m_GetCollectionsTested_23() { return &___m_GetCollectionsTested_23; }
	inline void set_m_GetCollectionsTested_23(bool value)
	{
		___m_GetCollectionsTested_23 = value;
	}

	inline static int32_t get_offset_of_m_AddCollectionTested_24() { return static_cast<int32_t>(offsetof(TestDiscovery_t1320997924, ___m_AddCollectionTested_24)); }
	inline bool get_m_AddCollectionTested_24() const { return ___m_AddCollectionTested_24; }
	inline bool* get_address_of_m_AddCollectionTested_24() { return &___m_AddCollectionTested_24; }
	inline void set_m_AddCollectionTested_24(bool value)
	{
		___m_AddCollectionTested_24 = value;
	}

	inline static int32_t get_offset_of_m_GetCollectionTested_25() { return static_cast<int32_t>(offsetof(TestDiscovery_t1320997924, ___m_GetCollectionTested_25)); }
	inline bool get_m_GetCollectionTested_25() const { return ___m_GetCollectionTested_25; }
	inline bool* get_address_of_m_GetCollectionTested_25() { return &___m_GetCollectionTested_25; }
	inline void set_m_GetCollectionTested_25(bool value)
	{
		___m_GetCollectionTested_25 = value;
	}

	inline static int32_t get_offset_of_m_GetFieldsTested_26() { return static_cast<int32_t>(offsetof(TestDiscovery_t1320997924, ___m_GetFieldsTested_26)); }
	inline bool get_m_GetFieldsTested_26() const { return ___m_GetFieldsTested_26; }
	inline bool* get_address_of_m_GetFieldsTested_26() { return &___m_GetFieldsTested_26; }
	inline void set_m_GetFieldsTested_26(bool value)
	{
		___m_GetFieldsTested_26 = value;
	}

	inline static int32_t get_offset_of_m_AddDocumentTested_27() { return static_cast<int32_t>(offsetof(TestDiscovery_t1320997924, ___m_AddDocumentTested_27)); }
	inline bool get_m_AddDocumentTested_27() const { return ___m_AddDocumentTested_27; }
	inline bool* get_address_of_m_AddDocumentTested_27() { return &___m_AddDocumentTested_27; }
	inline void set_m_AddDocumentTested_27(bool value)
	{
		___m_AddDocumentTested_27 = value;
	}

	inline static int32_t get_offset_of_m_GetDocumentTested_28() { return static_cast<int32_t>(offsetof(TestDiscovery_t1320997924, ___m_GetDocumentTested_28)); }
	inline bool get_m_GetDocumentTested_28() const { return ___m_GetDocumentTested_28; }
	inline bool* get_address_of_m_GetDocumentTested_28() { return &___m_GetDocumentTested_28; }
	inline void set_m_GetDocumentTested_28(bool value)
	{
		___m_GetDocumentTested_28 = value;
	}

	inline static int32_t get_offset_of_m_UpdateDocumentTested_29() { return static_cast<int32_t>(offsetof(TestDiscovery_t1320997924, ___m_UpdateDocumentTested_29)); }
	inline bool get_m_UpdateDocumentTested_29() const { return ___m_UpdateDocumentTested_29; }
	inline bool* get_address_of_m_UpdateDocumentTested_29() { return &___m_UpdateDocumentTested_29; }
	inline void set_m_UpdateDocumentTested_29(bool value)
	{
		___m_UpdateDocumentTested_29 = value;
	}

	inline static int32_t get_offset_of_m_QueryTested_30() { return static_cast<int32_t>(offsetof(TestDiscovery_t1320997924, ___m_QueryTested_30)); }
	inline bool get_m_QueryTested_30() const { return ___m_QueryTested_30; }
	inline bool* get_address_of_m_QueryTested_30() { return &___m_QueryTested_30; }
	inline void set_m_QueryTested_30(bool value)
	{
		___m_QueryTested_30 = value;
	}

	inline static int32_t get_offset_of_m_DeleteDocumentTested_31() { return static_cast<int32_t>(offsetof(TestDiscovery_t1320997924, ___m_DeleteDocumentTested_31)); }
	inline bool get_m_DeleteDocumentTested_31() const { return ___m_DeleteDocumentTested_31; }
	inline bool* get_address_of_m_DeleteDocumentTested_31() { return &___m_DeleteDocumentTested_31; }
	inline void set_m_DeleteDocumentTested_31(bool value)
	{
		___m_DeleteDocumentTested_31 = value;
	}

	inline static int32_t get_offset_of_m_DeleteCollectionTested_32() { return static_cast<int32_t>(offsetof(TestDiscovery_t1320997924, ___m_DeleteCollectionTested_32)); }
	inline bool get_m_DeleteCollectionTested_32() const { return ___m_DeleteCollectionTested_32; }
	inline bool* get_address_of_m_DeleteCollectionTested_32() { return &___m_DeleteCollectionTested_32; }
	inline void set_m_DeleteCollectionTested_32(bool value)
	{
		___m_DeleteCollectionTested_32 = value;
	}

	inline static int32_t get_offset_of_m_DeleteConfigurationTested_33() { return static_cast<int32_t>(offsetof(TestDiscovery_t1320997924, ___m_DeleteConfigurationTested_33)); }
	inline bool get_m_DeleteConfigurationTested_33() const { return ___m_DeleteConfigurationTested_33; }
	inline bool* get_address_of_m_DeleteConfigurationTested_33() { return &___m_DeleteConfigurationTested_33; }
	inline void set_m_DeleteConfigurationTested_33(bool value)
	{
		___m_DeleteConfigurationTested_33 = value;
	}

	inline static int32_t get_offset_of_m_DeleteEnvironmentTested_34() { return static_cast<int32_t>(offsetof(TestDiscovery_t1320997924, ___m_DeleteEnvironmentTested_34)); }
	inline bool get_m_DeleteEnvironmentTested_34() const { return ___m_DeleteEnvironmentTested_34; }
	inline bool* get_address_of_m_DeleteEnvironmentTested_34() { return &___m_DeleteEnvironmentTested_34; }
	inline void set_m_DeleteEnvironmentTested_34(bool value)
	{
		___m_DeleteEnvironmentTested_34 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TESTDISCOVERY_T1320997924_H
#ifndef TESTLANGUAGETRANSLATION_T1772800819_H
#define TESTLANGUAGETRANSLATION_T1772800819_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.UnitTests.TestLanguageTranslation
struct  TestLanguageTranslation_t1772800819  : public UnitTest_t237671048
{
public:
	// IBM.Watson.DeveloperCloud.Services.LanguageTranslation.v1.LanguageTranslation IBM.Watson.DeveloperCloud.UnitTests.TestLanguageTranslation::m_Translate
	LanguageTranslation_t3099415401 * ___m_Translate_1;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestLanguageTranslation::m_GetModelTested
	bool ___m_GetModelTested_2;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestLanguageTranslation::m_GetModelsTested
	bool ___m_GetModelsTested_3;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestLanguageTranslation::m_GetLanguagesTested
	bool ___m_GetLanguagesTested_4;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestLanguageTranslation::m_IdentifyTested
	bool ___m_IdentifyTested_5;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestLanguageTranslation::m_TranslateTested
	bool ___m_TranslateTested_6;

public:
	inline static int32_t get_offset_of_m_Translate_1() { return static_cast<int32_t>(offsetof(TestLanguageTranslation_t1772800819, ___m_Translate_1)); }
	inline LanguageTranslation_t3099415401 * get_m_Translate_1() const { return ___m_Translate_1; }
	inline LanguageTranslation_t3099415401 ** get_address_of_m_Translate_1() { return &___m_Translate_1; }
	inline void set_m_Translate_1(LanguageTranslation_t3099415401 * value)
	{
		___m_Translate_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Translate_1), value);
	}

	inline static int32_t get_offset_of_m_GetModelTested_2() { return static_cast<int32_t>(offsetof(TestLanguageTranslation_t1772800819, ___m_GetModelTested_2)); }
	inline bool get_m_GetModelTested_2() const { return ___m_GetModelTested_2; }
	inline bool* get_address_of_m_GetModelTested_2() { return &___m_GetModelTested_2; }
	inline void set_m_GetModelTested_2(bool value)
	{
		___m_GetModelTested_2 = value;
	}

	inline static int32_t get_offset_of_m_GetModelsTested_3() { return static_cast<int32_t>(offsetof(TestLanguageTranslation_t1772800819, ___m_GetModelsTested_3)); }
	inline bool get_m_GetModelsTested_3() const { return ___m_GetModelsTested_3; }
	inline bool* get_address_of_m_GetModelsTested_3() { return &___m_GetModelsTested_3; }
	inline void set_m_GetModelsTested_3(bool value)
	{
		___m_GetModelsTested_3 = value;
	}

	inline static int32_t get_offset_of_m_GetLanguagesTested_4() { return static_cast<int32_t>(offsetof(TestLanguageTranslation_t1772800819, ___m_GetLanguagesTested_4)); }
	inline bool get_m_GetLanguagesTested_4() const { return ___m_GetLanguagesTested_4; }
	inline bool* get_address_of_m_GetLanguagesTested_4() { return &___m_GetLanguagesTested_4; }
	inline void set_m_GetLanguagesTested_4(bool value)
	{
		___m_GetLanguagesTested_4 = value;
	}

	inline static int32_t get_offset_of_m_IdentifyTested_5() { return static_cast<int32_t>(offsetof(TestLanguageTranslation_t1772800819, ___m_IdentifyTested_5)); }
	inline bool get_m_IdentifyTested_5() const { return ___m_IdentifyTested_5; }
	inline bool* get_address_of_m_IdentifyTested_5() { return &___m_IdentifyTested_5; }
	inline void set_m_IdentifyTested_5(bool value)
	{
		___m_IdentifyTested_5 = value;
	}

	inline static int32_t get_offset_of_m_TranslateTested_6() { return static_cast<int32_t>(offsetof(TestLanguageTranslation_t1772800819, ___m_TranslateTested_6)); }
	inline bool get_m_TranslateTested_6() const { return ___m_TranslateTested_6; }
	inline bool* get_address_of_m_TranslateTested_6() { return &___m_TranslateTested_6; }
	inline void set_m_TranslateTested_6(bool value)
	{
		___m_TranslateTested_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TESTLANGUAGETRANSLATION_T1772800819_H
#ifndef TESTPERSONALITYINSIGHTSV3_T1756789132_H
#define TESTPERSONALITYINSIGHTSV3_T1756789132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.UnitTests.TestPersonalityInsightsV3
struct  TestPersonalityInsightsV3_t1756789132  : public UnitTest_t237671048
{
public:
	// IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v3.PersonalityInsights IBM.Watson.DeveloperCloud.UnitTests.TestPersonalityInsightsV3::m_personalityInsights
	PersonalityInsights_t2503525937 * ___m_personalityInsights_1;
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestPersonalityInsightsV3::testString
	String_t* ___testString_2;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestPersonalityInsightsV3::m_GetProfileTextTested
	bool ___m_GetProfileTextTested_3;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestPersonalityInsightsV3::m_GetProfileJsonTested
	bool ___m_GetProfileJsonTested_4;

public:
	inline static int32_t get_offset_of_m_personalityInsights_1() { return static_cast<int32_t>(offsetof(TestPersonalityInsightsV3_t1756789132, ___m_personalityInsights_1)); }
	inline PersonalityInsights_t2503525937 * get_m_personalityInsights_1() const { return ___m_personalityInsights_1; }
	inline PersonalityInsights_t2503525937 ** get_address_of_m_personalityInsights_1() { return &___m_personalityInsights_1; }
	inline void set_m_personalityInsights_1(PersonalityInsights_t2503525937 * value)
	{
		___m_personalityInsights_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_personalityInsights_1), value);
	}

	inline static int32_t get_offset_of_testString_2() { return static_cast<int32_t>(offsetof(TestPersonalityInsightsV3_t1756789132, ___testString_2)); }
	inline String_t* get_testString_2() const { return ___testString_2; }
	inline String_t** get_address_of_testString_2() { return &___testString_2; }
	inline void set_testString_2(String_t* value)
	{
		___testString_2 = value;
		Il2CppCodeGenWriteBarrier((&___testString_2), value);
	}

	inline static int32_t get_offset_of_m_GetProfileTextTested_3() { return static_cast<int32_t>(offsetof(TestPersonalityInsightsV3_t1756789132, ___m_GetProfileTextTested_3)); }
	inline bool get_m_GetProfileTextTested_3() const { return ___m_GetProfileTextTested_3; }
	inline bool* get_address_of_m_GetProfileTextTested_3() { return &___m_GetProfileTextTested_3; }
	inline void set_m_GetProfileTextTested_3(bool value)
	{
		___m_GetProfileTextTested_3 = value;
	}

	inline static int32_t get_offset_of_m_GetProfileJsonTested_4() { return static_cast<int32_t>(offsetof(TestPersonalityInsightsV3_t1756789132, ___m_GetProfileJsonTested_4)); }
	inline bool get_m_GetProfileJsonTested_4() const { return ___m_GetProfileJsonTested_4; }
	inline bool* get_address_of_m_GetProfileJsonTested_4() { return &___m_GetProfileJsonTested_4; }
	inline void set_m_GetProfileJsonTested_4(bool value)
	{
		___m_GetProfileJsonTested_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TESTPERSONALITYINSIGHTSV3_T1756789132_H
#ifndef TESTPERSONALITYINSIGHTSV2_T1756789131_H
#define TESTPERSONALITYINSIGHTSV2_T1756789131_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.UnitTests.TestPersonalityInsightsV2
struct  TestPersonalityInsightsV2_t1756789131  : public UnitTest_t237671048
{
public:
	// IBM.Watson.DeveloperCloud.Services.PersonalityInsights.v2.PersonalityInsights IBM.Watson.DeveloperCloud.UnitTests.TestPersonalityInsightsV2::m_personalityInsights
	PersonalityInsights_t582046669 * ___m_personalityInsights_1;
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestPersonalityInsightsV2::testString
	String_t* ___testString_2;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestPersonalityInsightsV2::m_GetProfileTextTested
	bool ___m_GetProfileTextTested_3;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestPersonalityInsightsV2::m_GetProfileJsonTested
	bool ___m_GetProfileJsonTested_4;

public:
	inline static int32_t get_offset_of_m_personalityInsights_1() { return static_cast<int32_t>(offsetof(TestPersonalityInsightsV2_t1756789131, ___m_personalityInsights_1)); }
	inline PersonalityInsights_t582046669 * get_m_personalityInsights_1() const { return ___m_personalityInsights_1; }
	inline PersonalityInsights_t582046669 ** get_address_of_m_personalityInsights_1() { return &___m_personalityInsights_1; }
	inline void set_m_personalityInsights_1(PersonalityInsights_t582046669 * value)
	{
		___m_personalityInsights_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_personalityInsights_1), value);
	}

	inline static int32_t get_offset_of_testString_2() { return static_cast<int32_t>(offsetof(TestPersonalityInsightsV2_t1756789131, ___testString_2)); }
	inline String_t* get_testString_2() const { return ___testString_2; }
	inline String_t** get_address_of_testString_2() { return &___testString_2; }
	inline void set_testString_2(String_t* value)
	{
		___testString_2 = value;
		Il2CppCodeGenWriteBarrier((&___testString_2), value);
	}

	inline static int32_t get_offset_of_m_GetProfileTextTested_3() { return static_cast<int32_t>(offsetof(TestPersonalityInsightsV2_t1756789131, ___m_GetProfileTextTested_3)); }
	inline bool get_m_GetProfileTextTested_3() const { return ___m_GetProfileTextTested_3; }
	inline bool* get_address_of_m_GetProfileTextTested_3() { return &___m_GetProfileTextTested_3; }
	inline void set_m_GetProfileTextTested_3(bool value)
	{
		___m_GetProfileTextTested_3 = value;
	}

	inline static int32_t get_offset_of_m_GetProfileJsonTested_4() { return static_cast<int32_t>(offsetof(TestPersonalityInsightsV2_t1756789131, ___m_GetProfileJsonTested_4)); }
	inline bool get_m_GetProfileJsonTested_4() const { return ___m_GetProfileJsonTested_4; }
	inline bool* get_address_of_m_GetProfileJsonTested_4() { return &___m_GetProfileJsonTested_4; }
	inline void set_m_GetProfileJsonTested_4(bool value)
	{
		___m_GetProfileJsonTested_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TESTPERSONALITYINSIGHTSV2_T1756789131_H
#ifndef TESTNATURALLANGUAGECLASSIFIER_T1226850908_H
#define TESTNATURALLANGUAGECLASSIFIER_T1226850908_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.UnitTests.TestNaturalLanguageClassifier
struct  TestNaturalLanguageClassifier_t1226850908  : public UnitTest_t237671048
{
public:
	// IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1.NaturalLanguageClassifier IBM.Watson.DeveloperCloud.UnitTests.TestNaturalLanguageClassifier::m_NaturalLanguageClassifier
	NaturalLanguageClassifier_t2492034444 * ___m_NaturalLanguageClassifier_1;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestNaturalLanguageClassifier::m_FindClassifierTested
	bool ___m_FindClassifierTested_2;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestNaturalLanguageClassifier::m_TrainClasifierTested
	bool ___m_TrainClasifierTested_3;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestNaturalLanguageClassifier::m_TrainClassifier
	bool ___m_TrainClassifier_4;
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestNaturalLanguageClassifier::m_ClassifierId
	String_t* ___m_ClassifierId_5;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestNaturalLanguageClassifier::m_ClassifyTested
	bool ___m_ClassifyTested_6;

public:
	inline static int32_t get_offset_of_m_NaturalLanguageClassifier_1() { return static_cast<int32_t>(offsetof(TestNaturalLanguageClassifier_t1226850908, ___m_NaturalLanguageClassifier_1)); }
	inline NaturalLanguageClassifier_t2492034444 * get_m_NaturalLanguageClassifier_1() const { return ___m_NaturalLanguageClassifier_1; }
	inline NaturalLanguageClassifier_t2492034444 ** get_address_of_m_NaturalLanguageClassifier_1() { return &___m_NaturalLanguageClassifier_1; }
	inline void set_m_NaturalLanguageClassifier_1(NaturalLanguageClassifier_t2492034444 * value)
	{
		___m_NaturalLanguageClassifier_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_NaturalLanguageClassifier_1), value);
	}

	inline static int32_t get_offset_of_m_FindClassifierTested_2() { return static_cast<int32_t>(offsetof(TestNaturalLanguageClassifier_t1226850908, ___m_FindClassifierTested_2)); }
	inline bool get_m_FindClassifierTested_2() const { return ___m_FindClassifierTested_2; }
	inline bool* get_address_of_m_FindClassifierTested_2() { return &___m_FindClassifierTested_2; }
	inline void set_m_FindClassifierTested_2(bool value)
	{
		___m_FindClassifierTested_2 = value;
	}

	inline static int32_t get_offset_of_m_TrainClasifierTested_3() { return static_cast<int32_t>(offsetof(TestNaturalLanguageClassifier_t1226850908, ___m_TrainClasifierTested_3)); }
	inline bool get_m_TrainClasifierTested_3() const { return ___m_TrainClasifierTested_3; }
	inline bool* get_address_of_m_TrainClasifierTested_3() { return &___m_TrainClasifierTested_3; }
	inline void set_m_TrainClasifierTested_3(bool value)
	{
		___m_TrainClasifierTested_3 = value;
	}

	inline static int32_t get_offset_of_m_TrainClassifier_4() { return static_cast<int32_t>(offsetof(TestNaturalLanguageClassifier_t1226850908, ___m_TrainClassifier_4)); }
	inline bool get_m_TrainClassifier_4() const { return ___m_TrainClassifier_4; }
	inline bool* get_address_of_m_TrainClassifier_4() { return &___m_TrainClassifier_4; }
	inline void set_m_TrainClassifier_4(bool value)
	{
		___m_TrainClassifier_4 = value;
	}

	inline static int32_t get_offset_of_m_ClassifierId_5() { return static_cast<int32_t>(offsetof(TestNaturalLanguageClassifier_t1226850908, ___m_ClassifierId_5)); }
	inline String_t* get_m_ClassifierId_5() const { return ___m_ClassifierId_5; }
	inline String_t** get_address_of_m_ClassifierId_5() { return &___m_ClassifierId_5; }
	inline void set_m_ClassifierId_5(String_t* value)
	{
		___m_ClassifierId_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_ClassifierId_5), value);
	}

	inline static int32_t get_offset_of_m_ClassifyTested_6() { return static_cast<int32_t>(offsetof(TestNaturalLanguageClassifier_t1226850908, ___m_ClassifyTested_6)); }
	inline bool get_m_ClassifyTested_6() const { return ___m_ClassifyTested_6; }
	inline bool* get_address_of_m_ClassifyTested_6() { return &___m_ClassifyTested_6; }
	inline void set_m_ClassifyTested_6(bool value)
	{
		___m_ClassifyTested_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TESTNATURALLANGUAGECLASSIFIER_T1226850908_H
#ifndef TESTCONVERSATION_T1577552265_H
#define TESTCONVERSATION_T1577552265_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TestConversation
struct  TestConversation_t1577552265  : public UnitTest_t237671048
{
public:
	// IBM.Watson.DeveloperCloud.Services.Conversation.v1.Conversation TestConversation::m_Conversation
	Conversation_t105466997 * ___m_Conversation_1;
	// System.String TestConversation::m_WorkspaceID
	String_t* ___m_WorkspaceID_2;
	// System.String TestConversation::m_Input
	String_t* ___m_Input_3;
	// System.Boolean TestConversation::m_MessageInputTested
	bool ___m_MessageInputTested_4;
	// System.Boolean TestConversation::m_MessageObjectTested
	bool ___m_MessageObjectTested_5;

public:
	inline static int32_t get_offset_of_m_Conversation_1() { return static_cast<int32_t>(offsetof(TestConversation_t1577552265, ___m_Conversation_1)); }
	inline Conversation_t105466997 * get_m_Conversation_1() const { return ___m_Conversation_1; }
	inline Conversation_t105466997 ** get_address_of_m_Conversation_1() { return &___m_Conversation_1; }
	inline void set_m_Conversation_1(Conversation_t105466997 * value)
	{
		___m_Conversation_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Conversation_1), value);
	}

	inline static int32_t get_offset_of_m_WorkspaceID_2() { return static_cast<int32_t>(offsetof(TestConversation_t1577552265, ___m_WorkspaceID_2)); }
	inline String_t* get_m_WorkspaceID_2() const { return ___m_WorkspaceID_2; }
	inline String_t** get_address_of_m_WorkspaceID_2() { return &___m_WorkspaceID_2; }
	inline void set_m_WorkspaceID_2(String_t* value)
	{
		___m_WorkspaceID_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_WorkspaceID_2), value);
	}

	inline static int32_t get_offset_of_m_Input_3() { return static_cast<int32_t>(offsetof(TestConversation_t1577552265, ___m_Input_3)); }
	inline String_t* get_m_Input_3() const { return ___m_Input_3; }
	inline String_t** get_address_of_m_Input_3() { return &___m_Input_3; }
	inline void set_m_Input_3(String_t* value)
	{
		___m_Input_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Input_3), value);
	}

	inline static int32_t get_offset_of_m_MessageInputTested_4() { return static_cast<int32_t>(offsetof(TestConversation_t1577552265, ___m_MessageInputTested_4)); }
	inline bool get_m_MessageInputTested_4() const { return ___m_MessageInputTested_4; }
	inline bool* get_address_of_m_MessageInputTested_4() { return &___m_MessageInputTested_4; }
	inline void set_m_MessageInputTested_4(bool value)
	{
		___m_MessageInputTested_4 = value;
	}

	inline static int32_t get_offset_of_m_MessageObjectTested_5() { return static_cast<int32_t>(offsetof(TestConversation_t1577552265, ___m_MessageObjectTested_5)); }
	inline bool get_m_MessageObjectTested_5() const { return ___m_MessageObjectTested_5; }
	inline bool* get_address_of_m_MessageObjectTested_5() { return &___m_MessageObjectTested_5; }
	inline void set_m_MessageObjectTested_5(bool value)
	{
		___m_MessageObjectTested_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TESTCONVERSATION_T1577552265_H
#ifndef GETCOLLECTIONIMAGEREQ_T2536251924_H
#define GETCOLLECTIONIMAGEREQ_T2536251924_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/GetCollectionImageReq
struct  GetCollectionImageReq_t2536251924  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/OnGetImageDetails IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/GetCollectionImageReq::<Callback>k__BackingField
	OnGetImageDetails_t1270200205 * ___U3CCallbackU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/GetCollectionImageReq::<CollectionID>k__BackingField
	String_t* ___U3CCollectionIDU3Ek__BackingField_12;
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/GetCollectionImageReq::<ImageID>k__BackingField
	String_t* ___U3CImageIDU3Ek__BackingField_13;
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/GetCollectionImageReq::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetCollectionImageReq_t2536251924, ___U3CCallbackU3Ek__BackingField_11)); }
	inline OnGetImageDetails_t1270200205 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline OnGetImageDetails_t1270200205 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(OnGetImageDetails_t1270200205 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCollectionIDU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GetCollectionImageReq_t2536251924, ___U3CCollectionIDU3Ek__BackingField_12)); }
	inline String_t* get_U3CCollectionIDU3Ek__BackingField_12() const { return ___U3CCollectionIDU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CCollectionIDU3Ek__BackingField_12() { return &___U3CCollectionIDU3Ek__BackingField_12; }
	inline void set_U3CCollectionIDU3Ek__BackingField_12(String_t* value)
	{
		___U3CCollectionIDU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCollectionIDU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CImageIDU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(GetCollectionImageReq_t2536251924, ___U3CImageIDU3Ek__BackingField_13)); }
	inline String_t* get_U3CImageIDU3Ek__BackingField_13() const { return ___U3CImageIDU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CImageIDU3Ek__BackingField_13() { return &___U3CImageIDU3Ek__BackingField_13; }
	inline void set_U3CImageIDU3Ek__BackingField_13(String_t* value)
	{
		___U3CImageIDU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CImageIDU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(GetCollectionImageReq_t2536251924, ___U3CDataU3Ek__BackingField_14)); }
	inline String_t* get_U3CDataU3Ek__BackingField_14() const { return ___U3CDataU3Ek__BackingField_14; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_14() { return &___U3CDataU3Ek__BackingField_14; }
	inline void set_U3CDataU3Ek__BackingField_14(String_t* value)
	{
		___U3CDataU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETCOLLECTIONIMAGEREQ_T2536251924_H
#ifndef DELETECOLLECTIONIMAGEREQ_T2682337773_H
#define DELETECOLLECTIONIMAGEREQ_T2682337773_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/DeleteCollectionImageReq
struct  DeleteCollectionImageReq_t2682337773  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/OnDeleteCollectionImage IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/DeleteCollectionImageReq::<Callback>k__BackingField
	OnDeleteCollectionImage_t241020072 * ___U3CCallbackU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/DeleteCollectionImageReq::<CollectionID>k__BackingField
	String_t* ___U3CCollectionIDU3Ek__BackingField_12;
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/DeleteCollectionImageReq::<ImageID>k__BackingField
	String_t* ___U3CImageIDU3Ek__BackingField_13;
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/DeleteCollectionImageReq::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(DeleteCollectionImageReq_t2682337773, ___U3CCallbackU3Ek__BackingField_11)); }
	inline OnDeleteCollectionImage_t241020072 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline OnDeleteCollectionImage_t241020072 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(OnDeleteCollectionImage_t241020072 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCollectionIDU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(DeleteCollectionImageReq_t2682337773, ___U3CCollectionIDU3Ek__BackingField_12)); }
	inline String_t* get_U3CCollectionIDU3Ek__BackingField_12() const { return ___U3CCollectionIDU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CCollectionIDU3Ek__BackingField_12() { return &___U3CCollectionIDU3Ek__BackingField_12; }
	inline void set_U3CCollectionIDU3Ek__BackingField_12(String_t* value)
	{
		___U3CCollectionIDU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCollectionIDU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CImageIDU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(DeleteCollectionImageReq_t2682337773, ___U3CImageIDU3Ek__BackingField_13)); }
	inline String_t* get_U3CImageIDU3Ek__BackingField_13() const { return ___U3CImageIDU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CImageIDU3Ek__BackingField_13() { return &___U3CImageIDU3Ek__BackingField_13; }
	inline void set_U3CImageIDU3Ek__BackingField_13(String_t* value)
	{
		___U3CImageIDU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CImageIDU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(DeleteCollectionImageReq_t2682337773, ___U3CDataU3Ek__BackingField_14)); }
	inline String_t* get_U3CDataU3Ek__BackingField_14() const { return ___U3CDataU3Ek__BackingField_14; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_14() { return &___U3CDataU3Ek__BackingField_14; }
	inline void set_U3CDataU3Ek__BackingField_14(String_t* value)
	{
		___U3CDataU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELETECOLLECTIONIMAGEREQ_T2682337773_H
#ifndef ADDCOLLECTIONIMAGEREQ_T3653897285_H
#define ADDCOLLECTIONIMAGEREQ_T3653897285_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/AddCollectionImageReq
struct  AddCollectionImageReq_t3653897285  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/OnAddCollectionImage IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/AddCollectionImageReq::<Callback>k__BackingField
	OnAddCollectionImage_t1084877344 * ___U3CCallbackU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/AddCollectionImageReq::<CollectionID>k__BackingField
	String_t* ___U3CCollectionIDU3Ek__BackingField_12;
	// System.Byte[] IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/AddCollectionImageReq::<ImageData>k__BackingField
	ByteU5BU5D_t3397334013* ___U3CImageDataU3Ek__BackingField_13;
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/AddCollectionImageReq::<Metadata>k__BackingField
	String_t* ___U3CMetadataU3Ek__BackingField_14;
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/AddCollectionImageReq::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_15;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(AddCollectionImageReq_t3653897285, ___U3CCallbackU3Ek__BackingField_11)); }
	inline OnAddCollectionImage_t1084877344 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline OnAddCollectionImage_t1084877344 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(OnAddCollectionImage_t1084877344 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCollectionIDU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(AddCollectionImageReq_t3653897285, ___U3CCollectionIDU3Ek__BackingField_12)); }
	inline String_t* get_U3CCollectionIDU3Ek__BackingField_12() const { return ___U3CCollectionIDU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CCollectionIDU3Ek__BackingField_12() { return &___U3CCollectionIDU3Ek__BackingField_12; }
	inline void set_U3CCollectionIDU3Ek__BackingField_12(String_t* value)
	{
		___U3CCollectionIDU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCollectionIDU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CImageDataU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(AddCollectionImageReq_t3653897285, ___U3CImageDataU3Ek__BackingField_13)); }
	inline ByteU5BU5D_t3397334013* get_U3CImageDataU3Ek__BackingField_13() const { return ___U3CImageDataU3Ek__BackingField_13; }
	inline ByteU5BU5D_t3397334013** get_address_of_U3CImageDataU3Ek__BackingField_13() { return &___U3CImageDataU3Ek__BackingField_13; }
	inline void set_U3CImageDataU3Ek__BackingField_13(ByteU5BU5D_t3397334013* value)
	{
		___U3CImageDataU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CImageDataU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CMetadataU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(AddCollectionImageReq_t3653897285, ___U3CMetadataU3Ek__BackingField_14)); }
	inline String_t* get_U3CMetadataU3Ek__BackingField_14() const { return ___U3CMetadataU3Ek__BackingField_14; }
	inline String_t** get_address_of_U3CMetadataU3Ek__BackingField_14() { return &___U3CMetadataU3Ek__BackingField_14; }
	inline void set_U3CMetadataU3Ek__BackingField_14(String_t* value)
	{
		___U3CMetadataU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMetadataU3Ek__BackingField_14), value);
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(AddCollectionImageReq_t3653897285, ___U3CDataU3Ek__BackingField_15)); }
	inline String_t* get_U3CDataU3Ek__BackingField_15() const { return ___U3CDataU3Ek__BackingField_15; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_15() { return &___U3CDataU3Ek__BackingField_15; }
	inline void set_U3CDataU3Ek__BackingField_15(String_t* value)
	{
		___U3CDataU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDCOLLECTIONIMAGEREQ_T3653897285_H
#ifndef DELETECOLLECTIONIMAGEMETADATAREQ_T3072827134_H
#define DELETECOLLECTIONIMAGEMETADATAREQ_T3072827134_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/DeleteCollectionImageMetadataReq
struct  DeleteCollectionImageMetadataReq_t3072827134  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/OnDeleteImageMetadata IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/DeleteCollectionImageMetadataReq::<Callback>k__BackingField
	OnDeleteImageMetadata_t2373116469 * ___U3CCallbackU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/DeleteCollectionImageMetadataReq::<CollectionID>k__BackingField
	String_t* ___U3CCollectionIDU3Ek__BackingField_12;
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/DeleteCollectionImageMetadataReq::<ImageID>k__BackingField
	String_t* ___U3CImageIDU3Ek__BackingField_13;
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/DeleteCollectionImageMetadataReq::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(DeleteCollectionImageMetadataReq_t3072827134, ___U3CCallbackU3Ek__BackingField_11)); }
	inline OnDeleteImageMetadata_t2373116469 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline OnDeleteImageMetadata_t2373116469 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(OnDeleteImageMetadata_t2373116469 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCollectionIDU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(DeleteCollectionImageMetadataReq_t3072827134, ___U3CCollectionIDU3Ek__BackingField_12)); }
	inline String_t* get_U3CCollectionIDU3Ek__BackingField_12() const { return ___U3CCollectionIDU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CCollectionIDU3Ek__BackingField_12() { return &___U3CCollectionIDU3Ek__BackingField_12; }
	inline void set_U3CCollectionIDU3Ek__BackingField_12(String_t* value)
	{
		___U3CCollectionIDU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCollectionIDU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CImageIDU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(DeleteCollectionImageMetadataReq_t3072827134, ___U3CImageIDU3Ek__BackingField_13)); }
	inline String_t* get_U3CImageIDU3Ek__BackingField_13() const { return ___U3CImageIDU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CImageIDU3Ek__BackingField_13() { return &___U3CImageIDU3Ek__BackingField_13; }
	inline void set_U3CImageIDU3Ek__BackingField_13(String_t* value)
	{
		___U3CImageIDU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CImageIDU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(DeleteCollectionImageMetadataReq_t3072827134, ___U3CDataU3Ek__BackingField_14)); }
	inline String_t* get_U3CDataU3Ek__BackingField_14() const { return ___U3CDataU3Ek__BackingField_14; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_14() { return &___U3CDataU3Ek__BackingField_14; }
	inline void set_U3CDataU3Ek__BackingField_14(String_t* value)
	{
		___U3CDataU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELETECOLLECTIONIMAGEMETADATAREQ_T3072827134_H
#ifndef TESTALCHEMYAPI_T3509857629_H
#define TESTALCHEMYAPI_T3509857629_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.UnitTests.TestAlchemyAPI
struct  TestAlchemyAPI_t3509857629  : public UnitTest_t237671048
{
public:
	// IBM.Watson.DeveloperCloud.Services.AlchemyAPI.v1.AlchemyAPI IBM.Watson.DeveloperCloud.UnitTests.TestAlchemyAPI::m_AlchemyAPI
	AlchemyAPI_t2955839919 * ___m_AlchemyAPI_1;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestAlchemyAPI::m_GetAuthorsURLTested
	bool ___m_GetAuthorsURLTested_2;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestAlchemyAPI::m_GetAuthorsHTMLTested
	bool ___m_GetAuthorsHTMLTested_3;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestAlchemyAPI::m_GetRankedConceptsHTMLTested
	bool ___m_GetRankedConceptsHTMLTested_4;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestAlchemyAPI::m_GetRankedConceptsURLTested
	bool ___m_GetRankedConceptsURLTested_5;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestAlchemyAPI::m_GetRankedConceptsTextTested
	bool ___m_GetRankedConceptsTextTested_6;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestAlchemyAPI::m_GetDatesHTMLTested
	bool ___m_GetDatesHTMLTested_7;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestAlchemyAPI::m_GetDatesURLTested
	bool ___m_GetDatesURLTested_8;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestAlchemyAPI::m_GetDatesTextTested
	bool ___m_GetDatesTextTested_9;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestAlchemyAPI::m_GetEmotionHTMLTested
	bool ___m_GetEmotionHTMLTested_10;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestAlchemyAPI::m_GetEmotionURLTested
	bool ___m_GetEmotionURLTested_11;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestAlchemyAPI::m_GetEmotionTextTested
	bool ___m_GetEmotionTextTested_12;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestAlchemyAPI::m_GetEntityExtractionHTMLTested
	bool ___m_GetEntityExtractionHTMLTested_13;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestAlchemyAPI::m_GetEntityExtractionURLTested
	bool ___m_GetEntityExtractionURLTested_14;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestAlchemyAPI::m_GetEntityExtractionTextTested
	bool ___m_GetEntityExtractionTextTested_15;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestAlchemyAPI::m_DetectFeedsURLTested
	bool ___m_DetectFeedsURLTested_16;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestAlchemyAPI::m_DetectFeedsHTMLTested
	bool ___m_DetectFeedsHTMLTested_17;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestAlchemyAPI::m_GetKeywordExtractionHTMLTested
	bool ___m_GetKeywordExtractionHTMLTested_18;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestAlchemyAPI::m_GetKeywordExtractionURLTested
	bool ___m_GetKeywordExtractionURLTested_19;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestAlchemyAPI::m_GetKeywordExtractionTextTested
	bool ___m_GetKeywordExtractionTextTested_20;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestAlchemyAPI::m_GetLanguageHTMLTested
	bool ___m_GetLanguageHTMLTested_21;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestAlchemyAPI::m_GetLanguageURLTested
	bool ___m_GetLanguageURLTested_22;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestAlchemyAPI::m_GetLanguageTextTested
	bool ___m_GetLanguageTextTested_23;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestAlchemyAPI::m_GetMicroformatURLTested
	bool ___m_GetMicroformatURLTested_24;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestAlchemyAPI::m_GetMicroformatHTMLTested
	bool ___m_GetMicroformatHTMLTested_25;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestAlchemyAPI::m_GetPubDateURLTested
	bool ___m_GetPubDateURLTested_26;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestAlchemyAPI::m_GetPubldateHTMLTested
	bool ___m_GetPubldateHTMLTested_27;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestAlchemyAPI::m_GetRelationsHTMLTested
	bool ___m_GetRelationsHTMLTested_28;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestAlchemyAPI::m_GetRelationsURLTested
	bool ___m_GetRelationsURLTested_29;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestAlchemyAPI::m_GetRelationsTextTested
	bool ___m_GetRelationsTextTested_30;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestAlchemyAPI::m_GetTextSentimentHTMLTested
	bool ___m_GetTextSentimentHTMLTested_31;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestAlchemyAPI::m_GetTextSentimentURLTested
	bool ___m_GetTextSentimentURLTested_32;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestAlchemyAPI::m_GetTextSentimentTextTested
	bool ___m_GetTextSentimentTextTested_33;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestAlchemyAPI::m_GetTargetedSentimentHTMLTested
	bool ___m_GetTargetedSentimentHTMLTested_34;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestAlchemyAPI::m_GetTargetedSentimentURLTested
	bool ___m_GetTargetedSentimentURLTested_35;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestAlchemyAPI::m_GetTargetedSentimentTextTested
	bool ___m_GetTargetedSentimentTextTested_36;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestAlchemyAPI::m_GetRankedTaxonomyHTMLTested
	bool ___m_GetRankedTaxonomyHTMLTested_37;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestAlchemyAPI::m_GetRankedTaxonomyURLTested
	bool ___m_GetRankedTaxonomyURLTested_38;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestAlchemyAPI::m_GetRankedTaxonomyTextTested
	bool ___m_GetRankedTaxonomyTextTested_39;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestAlchemyAPI::m_GetTextHTMLTested
	bool ___m_GetTextHTMLTested_40;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestAlchemyAPI::m_GetTextURLTested
	bool ___m_GetTextURLTested_41;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestAlchemyAPI::m_GetRawTextHTMLTested
	bool ___m_GetRawTextHTMLTested_42;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestAlchemyAPI::m_GetRawTextURLTested
	bool ___m_GetRawTextURLTested_43;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestAlchemyAPI::m_GetTitleHTMLTested
	bool ___m_GetTitleHTMLTested_44;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestAlchemyAPI::m_GetTitleURLTested
	bool ___m_GetTitleURLTested_45;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestAlchemyAPI::m_GetCombinedDataHTMLTested
	bool ___m_GetCombinedDataHTMLTested_46;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestAlchemyAPI::m_GetCombinedDataURLTested
	bool ___m_GetCombinedDataURLTested_47;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestAlchemyAPI::m_GetCombinedDataTextTested
	bool ___m_GetCombinedDataTextTested_48;
	// System.Boolean IBM.Watson.DeveloperCloud.UnitTests.TestAlchemyAPI::m_GetNewsTested
	bool ___m_GetNewsTested_49;
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestAlchemyAPI::m_ExampleURL_article
	String_t* ___m_ExampleURL_article_50;
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestAlchemyAPI::m_ExampleURL_microformats
	String_t* ___m_ExampleURL_microformats_51;
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestAlchemyAPI::m_ExampleURL_feed
	String_t* ___m_ExampleURL_feed_52;
	// System.String IBM.Watson.DeveloperCloud.UnitTests.TestAlchemyAPI::m_ExampleText_article
	String_t* ___m_ExampleText_article_53;

public:
	inline static int32_t get_offset_of_m_AlchemyAPI_1() { return static_cast<int32_t>(offsetof(TestAlchemyAPI_t3509857629, ___m_AlchemyAPI_1)); }
	inline AlchemyAPI_t2955839919 * get_m_AlchemyAPI_1() const { return ___m_AlchemyAPI_1; }
	inline AlchemyAPI_t2955839919 ** get_address_of_m_AlchemyAPI_1() { return &___m_AlchemyAPI_1; }
	inline void set_m_AlchemyAPI_1(AlchemyAPI_t2955839919 * value)
	{
		___m_AlchemyAPI_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_AlchemyAPI_1), value);
	}

	inline static int32_t get_offset_of_m_GetAuthorsURLTested_2() { return static_cast<int32_t>(offsetof(TestAlchemyAPI_t3509857629, ___m_GetAuthorsURLTested_2)); }
	inline bool get_m_GetAuthorsURLTested_2() const { return ___m_GetAuthorsURLTested_2; }
	inline bool* get_address_of_m_GetAuthorsURLTested_2() { return &___m_GetAuthorsURLTested_2; }
	inline void set_m_GetAuthorsURLTested_2(bool value)
	{
		___m_GetAuthorsURLTested_2 = value;
	}

	inline static int32_t get_offset_of_m_GetAuthorsHTMLTested_3() { return static_cast<int32_t>(offsetof(TestAlchemyAPI_t3509857629, ___m_GetAuthorsHTMLTested_3)); }
	inline bool get_m_GetAuthorsHTMLTested_3() const { return ___m_GetAuthorsHTMLTested_3; }
	inline bool* get_address_of_m_GetAuthorsHTMLTested_3() { return &___m_GetAuthorsHTMLTested_3; }
	inline void set_m_GetAuthorsHTMLTested_3(bool value)
	{
		___m_GetAuthorsHTMLTested_3 = value;
	}

	inline static int32_t get_offset_of_m_GetRankedConceptsHTMLTested_4() { return static_cast<int32_t>(offsetof(TestAlchemyAPI_t3509857629, ___m_GetRankedConceptsHTMLTested_4)); }
	inline bool get_m_GetRankedConceptsHTMLTested_4() const { return ___m_GetRankedConceptsHTMLTested_4; }
	inline bool* get_address_of_m_GetRankedConceptsHTMLTested_4() { return &___m_GetRankedConceptsHTMLTested_4; }
	inline void set_m_GetRankedConceptsHTMLTested_4(bool value)
	{
		___m_GetRankedConceptsHTMLTested_4 = value;
	}

	inline static int32_t get_offset_of_m_GetRankedConceptsURLTested_5() { return static_cast<int32_t>(offsetof(TestAlchemyAPI_t3509857629, ___m_GetRankedConceptsURLTested_5)); }
	inline bool get_m_GetRankedConceptsURLTested_5() const { return ___m_GetRankedConceptsURLTested_5; }
	inline bool* get_address_of_m_GetRankedConceptsURLTested_5() { return &___m_GetRankedConceptsURLTested_5; }
	inline void set_m_GetRankedConceptsURLTested_5(bool value)
	{
		___m_GetRankedConceptsURLTested_5 = value;
	}

	inline static int32_t get_offset_of_m_GetRankedConceptsTextTested_6() { return static_cast<int32_t>(offsetof(TestAlchemyAPI_t3509857629, ___m_GetRankedConceptsTextTested_6)); }
	inline bool get_m_GetRankedConceptsTextTested_6() const { return ___m_GetRankedConceptsTextTested_6; }
	inline bool* get_address_of_m_GetRankedConceptsTextTested_6() { return &___m_GetRankedConceptsTextTested_6; }
	inline void set_m_GetRankedConceptsTextTested_6(bool value)
	{
		___m_GetRankedConceptsTextTested_6 = value;
	}

	inline static int32_t get_offset_of_m_GetDatesHTMLTested_7() { return static_cast<int32_t>(offsetof(TestAlchemyAPI_t3509857629, ___m_GetDatesHTMLTested_7)); }
	inline bool get_m_GetDatesHTMLTested_7() const { return ___m_GetDatesHTMLTested_7; }
	inline bool* get_address_of_m_GetDatesHTMLTested_7() { return &___m_GetDatesHTMLTested_7; }
	inline void set_m_GetDatesHTMLTested_7(bool value)
	{
		___m_GetDatesHTMLTested_7 = value;
	}

	inline static int32_t get_offset_of_m_GetDatesURLTested_8() { return static_cast<int32_t>(offsetof(TestAlchemyAPI_t3509857629, ___m_GetDatesURLTested_8)); }
	inline bool get_m_GetDatesURLTested_8() const { return ___m_GetDatesURLTested_8; }
	inline bool* get_address_of_m_GetDatesURLTested_8() { return &___m_GetDatesURLTested_8; }
	inline void set_m_GetDatesURLTested_8(bool value)
	{
		___m_GetDatesURLTested_8 = value;
	}

	inline static int32_t get_offset_of_m_GetDatesTextTested_9() { return static_cast<int32_t>(offsetof(TestAlchemyAPI_t3509857629, ___m_GetDatesTextTested_9)); }
	inline bool get_m_GetDatesTextTested_9() const { return ___m_GetDatesTextTested_9; }
	inline bool* get_address_of_m_GetDatesTextTested_9() { return &___m_GetDatesTextTested_9; }
	inline void set_m_GetDatesTextTested_9(bool value)
	{
		___m_GetDatesTextTested_9 = value;
	}

	inline static int32_t get_offset_of_m_GetEmotionHTMLTested_10() { return static_cast<int32_t>(offsetof(TestAlchemyAPI_t3509857629, ___m_GetEmotionHTMLTested_10)); }
	inline bool get_m_GetEmotionHTMLTested_10() const { return ___m_GetEmotionHTMLTested_10; }
	inline bool* get_address_of_m_GetEmotionHTMLTested_10() { return &___m_GetEmotionHTMLTested_10; }
	inline void set_m_GetEmotionHTMLTested_10(bool value)
	{
		___m_GetEmotionHTMLTested_10 = value;
	}

	inline static int32_t get_offset_of_m_GetEmotionURLTested_11() { return static_cast<int32_t>(offsetof(TestAlchemyAPI_t3509857629, ___m_GetEmotionURLTested_11)); }
	inline bool get_m_GetEmotionURLTested_11() const { return ___m_GetEmotionURLTested_11; }
	inline bool* get_address_of_m_GetEmotionURLTested_11() { return &___m_GetEmotionURLTested_11; }
	inline void set_m_GetEmotionURLTested_11(bool value)
	{
		___m_GetEmotionURLTested_11 = value;
	}

	inline static int32_t get_offset_of_m_GetEmotionTextTested_12() { return static_cast<int32_t>(offsetof(TestAlchemyAPI_t3509857629, ___m_GetEmotionTextTested_12)); }
	inline bool get_m_GetEmotionTextTested_12() const { return ___m_GetEmotionTextTested_12; }
	inline bool* get_address_of_m_GetEmotionTextTested_12() { return &___m_GetEmotionTextTested_12; }
	inline void set_m_GetEmotionTextTested_12(bool value)
	{
		___m_GetEmotionTextTested_12 = value;
	}

	inline static int32_t get_offset_of_m_GetEntityExtractionHTMLTested_13() { return static_cast<int32_t>(offsetof(TestAlchemyAPI_t3509857629, ___m_GetEntityExtractionHTMLTested_13)); }
	inline bool get_m_GetEntityExtractionHTMLTested_13() const { return ___m_GetEntityExtractionHTMLTested_13; }
	inline bool* get_address_of_m_GetEntityExtractionHTMLTested_13() { return &___m_GetEntityExtractionHTMLTested_13; }
	inline void set_m_GetEntityExtractionHTMLTested_13(bool value)
	{
		___m_GetEntityExtractionHTMLTested_13 = value;
	}

	inline static int32_t get_offset_of_m_GetEntityExtractionURLTested_14() { return static_cast<int32_t>(offsetof(TestAlchemyAPI_t3509857629, ___m_GetEntityExtractionURLTested_14)); }
	inline bool get_m_GetEntityExtractionURLTested_14() const { return ___m_GetEntityExtractionURLTested_14; }
	inline bool* get_address_of_m_GetEntityExtractionURLTested_14() { return &___m_GetEntityExtractionURLTested_14; }
	inline void set_m_GetEntityExtractionURLTested_14(bool value)
	{
		___m_GetEntityExtractionURLTested_14 = value;
	}

	inline static int32_t get_offset_of_m_GetEntityExtractionTextTested_15() { return static_cast<int32_t>(offsetof(TestAlchemyAPI_t3509857629, ___m_GetEntityExtractionTextTested_15)); }
	inline bool get_m_GetEntityExtractionTextTested_15() const { return ___m_GetEntityExtractionTextTested_15; }
	inline bool* get_address_of_m_GetEntityExtractionTextTested_15() { return &___m_GetEntityExtractionTextTested_15; }
	inline void set_m_GetEntityExtractionTextTested_15(bool value)
	{
		___m_GetEntityExtractionTextTested_15 = value;
	}

	inline static int32_t get_offset_of_m_DetectFeedsURLTested_16() { return static_cast<int32_t>(offsetof(TestAlchemyAPI_t3509857629, ___m_DetectFeedsURLTested_16)); }
	inline bool get_m_DetectFeedsURLTested_16() const { return ___m_DetectFeedsURLTested_16; }
	inline bool* get_address_of_m_DetectFeedsURLTested_16() { return &___m_DetectFeedsURLTested_16; }
	inline void set_m_DetectFeedsURLTested_16(bool value)
	{
		___m_DetectFeedsURLTested_16 = value;
	}

	inline static int32_t get_offset_of_m_DetectFeedsHTMLTested_17() { return static_cast<int32_t>(offsetof(TestAlchemyAPI_t3509857629, ___m_DetectFeedsHTMLTested_17)); }
	inline bool get_m_DetectFeedsHTMLTested_17() const { return ___m_DetectFeedsHTMLTested_17; }
	inline bool* get_address_of_m_DetectFeedsHTMLTested_17() { return &___m_DetectFeedsHTMLTested_17; }
	inline void set_m_DetectFeedsHTMLTested_17(bool value)
	{
		___m_DetectFeedsHTMLTested_17 = value;
	}

	inline static int32_t get_offset_of_m_GetKeywordExtractionHTMLTested_18() { return static_cast<int32_t>(offsetof(TestAlchemyAPI_t3509857629, ___m_GetKeywordExtractionHTMLTested_18)); }
	inline bool get_m_GetKeywordExtractionHTMLTested_18() const { return ___m_GetKeywordExtractionHTMLTested_18; }
	inline bool* get_address_of_m_GetKeywordExtractionHTMLTested_18() { return &___m_GetKeywordExtractionHTMLTested_18; }
	inline void set_m_GetKeywordExtractionHTMLTested_18(bool value)
	{
		___m_GetKeywordExtractionHTMLTested_18 = value;
	}

	inline static int32_t get_offset_of_m_GetKeywordExtractionURLTested_19() { return static_cast<int32_t>(offsetof(TestAlchemyAPI_t3509857629, ___m_GetKeywordExtractionURLTested_19)); }
	inline bool get_m_GetKeywordExtractionURLTested_19() const { return ___m_GetKeywordExtractionURLTested_19; }
	inline bool* get_address_of_m_GetKeywordExtractionURLTested_19() { return &___m_GetKeywordExtractionURLTested_19; }
	inline void set_m_GetKeywordExtractionURLTested_19(bool value)
	{
		___m_GetKeywordExtractionURLTested_19 = value;
	}

	inline static int32_t get_offset_of_m_GetKeywordExtractionTextTested_20() { return static_cast<int32_t>(offsetof(TestAlchemyAPI_t3509857629, ___m_GetKeywordExtractionTextTested_20)); }
	inline bool get_m_GetKeywordExtractionTextTested_20() const { return ___m_GetKeywordExtractionTextTested_20; }
	inline bool* get_address_of_m_GetKeywordExtractionTextTested_20() { return &___m_GetKeywordExtractionTextTested_20; }
	inline void set_m_GetKeywordExtractionTextTested_20(bool value)
	{
		___m_GetKeywordExtractionTextTested_20 = value;
	}

	inline static int32_t get_offset_of_m_GetLanguageHTMLTested_21() { return static_cast<int32_t>(offsetof(TestAlchemyAPI_t3509857629, ___m_GetLanguageHTMLTested_21)); }
	inline bool get_m_GetLanguageHTMLTested_21() const { return ___m_GetLanguageHTMLTested_21; }
	inline bool* get_address_of_m_GetLanguageHTMLTested_21() { return &___m_GetLanguageHTMLTested_21; }
	inline void set_m_GetLanguageHTMLTested_21(bool value)
	{
		___m_GetLanguageHTMLTested_21 = value;
	}

	inline static int32_t get_offset_of_m_GetLanguageURLTested_22() { return static_cast<int32_t>(offsetof(TestAlchemyAPI_t3509857629, ___m_GetLanguageURLTested_22)); }
	inline bool get_m_GetLanguageURLTested_22() const { return ___m_GetLanguageURLTested_22; }
	inline bool* get_address_of_m_GetLanguageURLTested_22() { return &___m_GetLanguageURLTested_22; }
	inline void set_m_GetLanguageURLTested_22(bool value)
	{
		___m_GetLanguageURLTested_22 = value;
	}

	inline static int32_t get_offset_of_m_GetLanguageTextTested_23() { return static_cast<int32_t>(offsetof(TestAlchemyAPI_t3509857629, ___m_GetLanguageTextTested_23)); }
	inline bool get_m_GetLanguageTextTested_23() const { return ___m_GetLanguageTextTested_23; }
	inline bool* get_address_of_m_GetLanguageTextTested_23() { return &___m_GetLanguageTextTested_23; }
	inline void set_m_GetLanguageTextTested_23(bool value)
	{
		___m_GetLanguageTextTested_23 = value;
	}

	inline static int32_t get_offset_of_m_GetMicroformatURLTested_24() { return static_cast<int32_t>(offsetof(TestAlchemyAPI_t3509857629, ___m_GetMicroformatURLTested_24)); }
	inline bool get_m_GetMicroformatURLTested_24() const { return ___m_GetMicroformatURLTested_24; }
	inline bool* get_address_of_m_GetMicroformatURLTested_24() { return &___m_GetMicroformatURLTested_24; }
	inline void set_m_GetMicroformatURLTested_24(bool value)
	{
		___m_GetMicroformatURLTested_24 = value;
	}

	inline static int32_t get_offset_of_m_GetMicroformatHTMLTested_25() { return static_cast<int32_t>(offsetof(TestAlchemyAPI_t3509857629, ___m_GetMicroformatHTMLTested_25)); }
	inline bool get_m_GetMicroformatHTMLTested_25() const { return ___m_GetMicroformatHTMLTested_25; }
	inline bool* get_address_of_m_GetMicroformatHTMLTested_25() { return &___m_GetMicroformatHTMLTested_25; }
	inline void set_m_GetMicroformatHTMLTested_25(bool value)
	{
		___m_GetMicroformatHTMLTested_25 = value;
	}

	inline static int32_t get_offset_of_m_GetPubDateURLTested_26() { return static_cast<int32_t>(offsetof(TestAlchemyAPI_t3509857629, ___m_GetPubDateURLTested_26)); }
	inline bool get_m_GetPubDateURLTested_26() const { return ___m_GetPubDateURLTested_26; }
	inline bool* get_address_of_m_GetPubDateURLTested_26() { return &___m_GetPubDateURLTested_26; }
	inline void set_m_GetPubDateURLTested_26(bool value)
	{
		___m_GetPubDateURLTested_26 = value;
	}

	inline static int32_t get_offset_of_m_GetPubldateHTMLTested_27() { return static_cast<int32_t>(offsetof(TestAlchemyAPI_t3509857629, ___m_GetPubldateHTMLTested_27)); }
	inline bool get_m_GetPubldateHTMLTested_27() const { return ___m_GetPubldateHTMLTested_27; }
	inline bool* get_address_of_m_GetPubldateHTMLTested_27() { return &___m_GetPubldateHTMLTested_27; }
	inline void set_m_GetPubldateHTMLTested_27(bool value)
	{
		___m_GetPubldateHTMLTested_27 = value;
	}

	inline static int32_t get_offset_of_m_GetRelationsHTMLTested_28() { return static_cast<int32_t>(offsetof(TestAlchemyAPI_t3509857629, ___m_GetRelationsHTMLTested_28)); }
	inline bool get_m_GetRelationsHTMLTested_28() const { return ___m_GetRelationsHTMLTested_28; }
	inline bool* get_address_of_m_GetRelationsHTMLTested_28() { return &___m_GetRelationsHTMLTested_28; }
	inline void set_m_GetRelationsHTMLTested_28(bool value)
	{
		___m_GetRelationsHTMLTested_28 = value;
	}

	inline static int32_t get_offset_of_m_GetRelationsURLTested_29() { return static_cast<int32_t>(offsetof(TestAlchemyAPI_t3509857629, ___m_GetRelationsURLTested_29)); }
	inline bool get_m_GetRelationsURLTested_29() const { return ___m_GetRelationsURLTested_29; }
	inline bool* get_address_of_m_GetRelationsURLTested_29() { return &___m_GetRelationsURLTested_29; }
	inline void set_m_GetRelationsURLTested_29(bool value)
	{
		___m_GetRelationsURLTested_29 = value;
	}

	inline static int32_t get_offset_of_m_GetRelationsTextTested_30() { return static_cast<int32_t>(offsetof(TestAlchemyAPI_t3509857629, ___m_GetRelationsTextTested_30)); }
	inline bool get_m_GetRelationsTextTested_30() const { return ___m_GetRelationsTextTested_30; }
	inline bool* get_address_of_m_GetRelationsTextTested_30() { return &___m_GetRelationsTextTested_30; }
	inline void set_m_GetRelationsTextTested_30(bool value)
	{
		___m_GetRelationsTextTested_30 = value;
	}

	inline static int32_t get_offset_of_m_GetTextSentimentHTMLTested_31() { return static_cast<int32_t>(offsetof(TestAlchemyAPI_t3509857629, ___m_GetTextSentimentHTMLTested_31)); }
	inline bool get_m_GetTextSentimentHTMLTested_31() const { return ___m_GetTextSentimentHTMLTested_31; }
	inline bool* get_address_of_m_GetTextSentimentHTMLTested_31() { return &___m_GetTextSentimentHTMLTested_31; }
	inline void set_m_GetTextSentimentHTMLTested_31(bool value)
	{
		___m_GetTextSentimentHTMLTested_31 = value;
	}

	inline static int32_t get_offset_of_m_GetTextSentimentURLTested_32() { return static_cast<int32_t>(offsetof(TestAlchemyAPI_t3509857629, ___m_GetTextSentimentURLTested_32)); }
	inline bool get_m_GetTextSentimentURLTested_32() const { return ___m_GetTextSentimentURLTested_32; }
	inline bool* get_address_of_m_GetTextSentimentURLTested_32() { return &___m_GetTextSentimentURLTested_32; }
	inline void set_m_GetTextSentimentURLTested_32(bool value)
	{
		___m_GetTextSentimentURLTested_32 = value;
	}

	inline static int32_t get_offset_of_m_GetTextSentimentTextTested_33() { return static_cast<int32_t>(offsetof(TestAlchemyAPI_t3509857629, ___m_GetTextSentimentTextTested_33)); }
	inline bool get_m_GetTextSentimentTextTested_33() const { return ___m_GetTextSentimentTextTested_33; }
	inline bool* get_address_of_m_GetTextSentimentTextTested_33() { return &___m_GetTextSentimentTextTested_33; }
	inline void set_m_GetTextSentimentTextTested_33(bool value)
	{
		___m_GetTextSentimentTextTested_33 = value;
	}

	inline static int32_t get_offset_of_m_GetTargetedSentimentHTMLTested_34() { return static_cast<int32_t>(offsetof(TestAlchemyAPI_t3509857629, ___m_GetTargetedSentimentHTMLTested_34)); }
	inline bool get_m_GetTargetedSentimentHTMLTested_34() const { return ___m_GetTargetedSentimentHTMLTested_34; }
	inline bool* get_address_of_m_GetTargetedSentimentHTMLTested_34() { return &___m_GetTargetedSentimentHTMLTested_34; }
	inline void set_m_GetTargetedSentimentHTMLTested_34(bool value)
	{
		___m_GetTargetedSentimentHTMLTested_34 = value;
	}

	inline static int32_t get_offset_of_m_GetTargetedSentimentURLTested_35() { return static_cast<int32_t>(offsetof(TestAlchemyAPI_t3509857629, ___m_GetTargetedSentimentURLTested_35)); }
	inline bool get_m_GetTargetedSentimentURLTested_35() const { return ___m_GetTargetedSentimentURLTested_35; }
	inline bool* get_address_of_m_GetTargetedSentimentURLTested_35() { return &___m_GetTargetedSentimentURLTested_35; }
	inline void set_m_GetTargetedSentimentURLTested_35(bool value)
	{
		___m_GetTargetedSentimentURLTested_35 = value;
	}

	inline static int32_t get_offset_of_m_GetTargetedSentimentTextTested_36() { return static_cast<int32_t>(offsetof(TestAlchemyAPI_t3509857629, ___m_GetTargetedSentimentTextTested_36)); }
	inline bool get_m_GetTargetedSentimentTextTested_36() const { return ___m_GetTargetedSentimentTextTested_36; }
	inline bool* get_address_of_m_GetTargetedSentimentTextTested_36() { return &___m_GetTargetedSentimentTextTested_36; }
	inline void set_m_GetTargetedSentimentTextTested_36(bool value)
	{
		___m_GetTargetedSentimentTextTested_36 = value;
	}

	inline static int32_t get_offset_of_m_GetRankedTaxonomyHTMLTested_37() { return static_cast<int32_t>(offsetof(TestAlchemyAPI_t3509857629, ___m_GetRankedTaxonomyHTMLTested_37)); }
	inline bool get_m_GetRankedTaxonomyHTMLTested_37() const { return ___m_GetRankedTaxonomyHTMLTested_37; }
	inline bool* get_address_of_m_GetRankedTaxonomyHTMLTested_37() { return &___m_GetRankedTaxonomyHTMLTested_37; }
	inline void set_m_GetRankedTaxonomyHTMLTested_37(bool value)
	{
		___m_GetRankedTaxonomyHTMLTested_37 = value;
	}

	inline static int32_t get_offset_of_m_GetRankedTaxonomyURLTested_38() { return static_cast<int32_t>(offsetof(TestAlchemyAPI_t3509857629, ___m_GetRankedTaxonomyURLTested_38)); }
	inline bool get_m_GetRankedTaxonomyURLTested_38() const { return ___m_GetRankedTaxonomyURLTested_38; }
	inline bool* get_address_of_m_GetRankedTaxonomyURLTested_38() { return &___m_GetRankedTaxonomyURLTested_38; }
	inline void set_m_GetRankedTaxonomyURLTested_38(bool value)
	{
		___m_GetRankedTaxonomyURLTested_38 = value;
	}

	inline static int32_t get_offset_of_m_GetRankedTaxonomyTextTested_39() { return static_cast<int32_t>(offsetof(TestAlchemyAPI_t3509857629, ___m_GetRankedTaxonomyTextTested_39)); }
	inline bool get_m_GetRankedTaxonomyTextTested_39() const { return ___m_GetRankedTaxonomyTextTested_39; }
	inline bool* get_address_of_m_GetRankedTaxonomyTextTested_39() { return &___m_GetRankedTaxonomyTextTested_39; }
	inline void set_m_GetRankedTaxonomyTextTested_39(bool value)
	{
		___m_GetRankedTaxonomyTextTested_39 = value;
	}

	inline static int32_t get_offset_of_m_GetTextHTMLTested_40() { return static_cast<int32_t>(offsetof(TestAlchemyAPI_t3509857629, ___m_GetTextHTMLTested_40)); }
	inline bool get_m_GetTextHTMLTested_40() const { return ___m_GetTextHTMLTested_40; }
	inline bool* get_address_of_m_GetTextHTMLTested_40() { return &___m_GetTextHTMLTested_40; }
	inline void set_m_GetTextHTMLTested_40(bool value)
	{
		___m_GetTextHTMLTested_40 = value;
	}

	inline static int32_t get_offset_of_m_GetTextURLTested_41() { return static_cast<int32_t>(offsetof(TestAlchemyAPI_t3509857629, ___m_GetTextURLTested_41)); }
	inline bool get_m_GetTextURLTested_41() const { return ___m_GetTextURLTested_41; }
	inline bool* get_address_of_m_GetTextURLTested_41() { return &___m_GetTextURLTested_41; }
	inline void set_m_GetTextURLTested_41(bool value)
	{
		___m_GetTextURLTested_41 = value;
	}

	inline static int32_t get_offset_of_m_GetRawTextHTMLTested_42() { return static_cast<int32_t>(offsetof(TestAlchemyAPI_t3509857629, ___m_GetRawTextHTMLTested_42)); }
	inline bool get_m_GetRawTextHTMLTested_42() const { return ___m_GetRawTextHTMLTested_42; }
	inline bool* get_address_of_m_GetRawTextHTMLTested_42() { return &___m_GetRawTextHTMLTested_42; }
	inline void set_m_GetRawTextHTMLTested_42(bool value)
	{
		___m_GetRawTextHTMLTested_42 = value;
	}

	inline static int32_t get_offset_of_m_GetRawTextURLTested_43() { return static_cast<int32_t>(offsetof(TestAlchemyAPI_t3509857629, ___m_GetRawTextURLTested_43)); }
	inline bool get_m_GetRawTextURLTested_43() const { return ___m_GetRawTextURLTested_43; }
	inline bool* get_address_of_m_GetRawTextURLTested_43() { return &___m_GetRawTextURLTested_43; }
	inline void set_m_GetRawTextURLTested_43(bool value)
	{
		___m_GetRawTextURLTested_43 = value;
	}

	inline static int32_t get_offset_of_m_GetTitleHTMLTested_44() { return static_cast<int32_t>(offsetof(TestAlchemyAPI_t3509857629, ___m_GetTitleHTMLTested_44)); }
	inline bool get_m_GetTitleHTMLTested_44() const { return ___m_GetTitleHTMLTested_44; }
	inline bool* get_address_of_m_GetTitleHTMLTested_44() { return &___m_GetTitleHTMLTested_44; }
	inline void set_m_GetTitleHTMLTested_44(bool value)
	{
		___m_GetTitleHTMLTested_44 = value;
	}

	inline static int32_t get_offset_of_m_GetTitleURLTested_45() { return static_cast<int32_t>(offsetof(TestAlchemyAPI_t3509857629, ___m_GetTitleURLTested_45)); }
	inline bool get_m_GetTitleURLTested_45() const { return ___m_GetTitleURLTested_45; }
	inline bool* get_address_of_m_GetTitleURLTested_45() { return &___m_GetTitleURLTested_45; }
	inline void set_m_GetTitleURLTested_45(bool value)
	{
		___m_GetTitleURLTested_45 = value;
	}

	inline static int32_t get_offset_of_m_GetCombinedDataHTMLTested_46() { return static_cast<int32_t>(offsetof(TestAlchemyAPI_t3509857629, ___m_GetCombinedDataHTMLTested_46)); }
	inline bool get_m_GetCombinedDataHTMLTested_46() const { return ___m_GetCombinedDataHTMLTested_46; }
	inline bool* get_address_of_m_GetCombinedDataHTMLTested_46() { return &___m_GetCombinedDataHTMLTested_46; }
	inline void set_m_GetCombinedDataHTMLTested_46(bool value)
	{
		___m_GetCombinedDataHTMLTested_46 = value;
	}

	inline static int32_t get_offset_of_m_GetCombinedDataURLTested_47() { return static_cast<int32_t>(offsetof(TestAlchemyAPI_t3509857629, ___m_GetCombinedDataURLTested_47)); }
	inline bool get_m_GetCombinedDataURLTested_47() const { return ___m_GetCombinedDataURLTested_47; }
	inline bool* get_address_of_m_GetCombinedDataURLTested_47() { return &___m_GetCombinedDataURLTested_47; }
	inline void set_m_GetCombinedDataURLTested_47(bool value)
	{
		___m_GetCombinedDataURLTested_47 = value;
	}

	inline static int32_t get_offset_of_m_GetCombinedDataTextTested_48() { return static_cast<int32_t>(offsetof(TestAlchemyAPI_t3509857629, ___m_GetCombinedDataTextTested_48)); }
	inline bool get_m_GetCombinedDataTextTested_48() const { return ___m_GetCombinedDataTextTested_48; }
	inline bool* get_address_of_m_GetCombinedDataTextTested_48() { return &___m_GetCombinedDataTextTested_48; }
	inline void set_m_GetCombinedDataTextTested_48(bool value)
	{
		___m_GetCombinedDataTextTested_48 = value;
	}

	inline static int32_t get_offset_of_m_GetNewsTested_49() { return static_cast<int32_t>(offsetof(TestAlchemyAPI_t3509857629, ___m_GetNewsTested_49)); }
	inline bool get_m_GetNewsTested_49() const { return ___m_GetNewsTested_49; }
	inline bool* get_address_of_m_GetNewsTested_49() { return &___m_GetNewsTested_49; }
	inline void set_m_GetNewsTested_49(bool value)
	{
		___m_GetNewsTested_49 = value;
	}

	inline static int32_t get_offset_of_m_ExampleURL_article_50() { return static_cast<int32_t>(offsetof(TestAlchemyAPI_t3509857629, ___m_ExampleURL_article_50)); }
	inline String_t* get_m_ExampleURL_article_50() const { return ___m_ExampleURL_article_50; }
	inline String_t** get_address_of_m_ExampleURL_article_50() { return &___m_ExampleURL_article_50; }
	inline void set_m_ExampleURL_article_50(String_t* value)
	{
		___m_ExampleURL_article_50 = value;
		Il2CppCodeGenWriteBarrier((&___m_ExampleURL_article_50), value);
	}

	inline static int32_t get_offset_of_m_ExampleURL_microformats_51() { return static_cast<int32_t>(offsetof(TestAlchemyAPI_t3509857629, ___m_ExampleURL_microformats_51)); }
	inline String_t* get_m_ExampleURL_microformats_51() const { return ___m_ExampleURL_microformats_51; }
	inline String_t** get_address_of_m_ExampleURL_microformats_51() { return &___m_ExampleURL_microformats_51; }
	inline void set_m_ExampleURL_microformats_51(String_t* value)
	{
		___m_ExampleURL_microformats_51 = value;
		Il2CppCodeGenWriteBarrier((&___m_ExampleURL_microformats_51), value);
	}

	inline static int32_t get_offset_of_m_ExampleURL_feed_52() { return static_cast<int32_t>(offsetof(TestAlchemyAPI_t3509857629, ___m_ExampleURL_feed_52)); }
	inline String_t* get_m_ExampleURL_feed_52() const { return ___m_ExampleURL_feed_52; }
	inline String_t** get_address_of_m_ExampleURL_feed_52() { return &___m_ExampleURL_feed_52; }
	inline void set_m_ExampleURL_feed_52(String_t* value)
	{
		___m_ExampleURL_feed_52 = value;
		Il2CppCodeGenWriteBarrier((&___m_ExampleURL_feed_52), value);
	}

	inline static int32_t get_offset_of_m_ExampleText_article_53() { return static_cast<int32_t>(offsetof(TestAlchemyAPI_t3509857629, ___m_ExampleText_article_53)); }
	inline String_t* get_m_ExampleText_article_53() const { return ___m_ExampleText_article_53; }
	inline String_t** get_address_of_m_ExampleText_article_53() { return &___m_ExampleText_article_53; }
	inline void set_m_ExampleText_article_53(String_t* value)
	{
		___m_ExampleText_article_53 = value;
		Il2CppCodeGenWriteBarrier((&___m_ExampleText_article_53), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TESTALCHEMYAPI_T3509857629_H
#ifndef FINDSIMILARREQ_T671452345_H
#define FINDSIMILARREQ_T671452345_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/FindSimilarReq
struct  FindSimilarReq_t671452345  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/OnFindSimilar IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/FindSimilarReq::<Callback>k__BackingField
	OnFindSimilar_t219840828 * ___U3CCallbackU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/FindSimilarReq::<CollectionID>k__BackingField
	String_t* ___U3CCollectionIDU3Ek__BackingField_12;
	// System.Byte[] IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/FindSimilarReq::<ImageData>k__BackingField
	ByteU5BU5D_t3397334013* ___U3CImageDataU3Ek__BackingField_13;
	// System.Int32 IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/FindSimilarReq::<Limit>k__BackingField
	int32_t ___U3CLimitU3Ek__BackingField_14;
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/FindSimilarReq::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_15;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(FindSimilarReq_t671452345, ___U3CCallbackU3Ek__BackingField_11)); }
	inline OnFindSimilar_t219840828 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline OnFindSimilar_t219840828 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(OnFindSimilar_t219840828 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCollectionIDU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(FindSimilarReq_t671452345, ___U3CCollectionIDU3Ek__BackingField_12)); }
	inline String_t* get_U3CCollectionIDU3Ek__BackingField_12() const { return ___U3CCollectionIDU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CCollectionIDU3Ek__BackingField_12() { return &___U3CCollectionIDU3Ek__BackingField_12; }
	inline void set_U3CCollectionIDU3Ek__BackingField_12(String_t* value)
	{
		___U3CCollectionIDU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCollectionIDU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CImageDataU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(FindSimilarReq_t671452345, ___U3CImageDataU3Ek__BackingField_13)); }
	inline ByteU5BU5D_t3397334013* get_U3CImageDataU3Ek__BackingField_13() const { return ___U3CImageDataU3Ek__BackingField_13; }
	inline ByteU5BU5D_t3397334013** get_address_of_U3CImageDataU3Ek__BackingField_13() { return &___U3CImageDataU3Ek__BackingField_13; }
	inline void set_U3CImageDataU3Ek__BackingField_13(ByteU5BU5D_t3397334013* value)
	{
		___U3CImageDataU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CImageDataU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CLimitU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(FindSimilarReq_t671452345, ___U3CLimitU3Ek__BackingField_14)); }
	inline int32_t get_U3CLimitU3Ek__BackingField_14() const { return ___U3CLimitU3Ek__BackingField_14; }
	inline int32_t* get_address_of_U3CLimitU3Ek__BackingField_14() { return &___U3CLimitU3Ek__BackingField_14; }
	inline void set_U3CLimitU3Ek__BackingField_14(int32_t value)
	{
		___U3CLimitU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(FindSimilarReq_t671452345, ___U3CDataU3Ek__BackingField_15)); }
	inline String_t* get_U3CDataU3Ek__BackingField_15() const { return ___U3CDataU3Ek__BackingField_15; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_15() { return &___U3CDataU3Ek__BackingField_15; }
	inline void set_U3CDataU3Ek__BackingField_15(String_t* value)
	{
		___U3CDataU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FINDSIMILARREQ_T671452345_H
#ifndef GETCOLLECTIONIMAGEMETADATAREQ_T1443854471_H
#define GETCOLLECTIONIMAGEMETADATAREQ_T1443854471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/GetCollectionImageMetadataReq
struct  GetCollectionImageMetadataReq_t1443854471  : public Request_t466816980
{
public:
	// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/OnGetImageMetadata IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/GetCollectionImageMetadataReq::<Callback>k__BackingField
	OnGetImageMetadata_t54004548 * ___U3CCallbackU3Ek__BackingField_11;
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/GetCollectionImageMetadataReq::<CollectionID>k__BackingField
	String_t* ___U3CCollectionIDU3Ek__BackingField_12;
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/GetCollectionImageMetadataReq::<ImageID>k__BackingField
	String_t* ___U3CImageIDU3Ek__BackingField_13;
	// System.String IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/GetCollectionImageMetadataReq::<Data>k__BackingField
	String_t* ___U3CDataU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GetCollectionImageMetadataReq_t1443854471, ___U3CCallbackU3Ek__BackingField_11)); }
	inline OnGetImageMetadata_t54004548 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline OnGetImageMetadata_t54004548 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(OnGetImageMetadata_t54004548 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCollectionIDU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GetCollectionImageMetadataReq_t1443854471, ___U3CCollectionIDU3Ek__BackingField_12)); }
	inline String_t* get_U3CCollectionIDU3Ek__BackingField_12() const { return ___U3CCollectionIDU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CCollectionIDU3Ek__BackingField_12() { return &___U3CCollectionIDU3Ek__BackingField_12; }
	inline void set_U3CCollectionIDU3Ek__BackingField_12(String_t* value)
	{
		___U3CCollectionIDU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCollectionIDU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CImageIDU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(GetCollectionImageMetadataReq_t1443854471, ___U3CImageIDU3Ek__BackingField_13)); }
	inline String_t* get_U3CImageIDU3Ek__BackingField_13() const { return ___U3CImageIDU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CImageIDU3Ek__BackingField_13() { return &___U3CImageIDU3Ek__BackingField_13; }
	inline void set_U3CImageIDU3Ek__BackingField_13(String_t* value)
	{
		___U3CImageIDU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CImageIDU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(GetCollectionImageMetadataReq_t1443854471, ___U3CDataU3Ek__BackingField_14)); }
	inline String_t* get_U3CDataU3Ek__BackingField_14() const { return ___U3CDataU3Ek__BackingField_14; }
	inline String_t** get_address_of_U3CDataU3Ek__BackingField_14() { return &___U3CDataU3Ek__BackingField_14; }
	inline void set_U3CDataU3Ek__BackingField_14(String_t* value)
	{
		___U3CDataU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETCOLLECTIONIMAGEMETADATAREQ_T1443854471_H
#ifndef DELEGATE_T3022476291_H
#define DELEGATE_T3022476291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t3022476291  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	IntPtr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	IntPtr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	IntPtr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	IntPtr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1572802995 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___invoke_impl_1)); }
	inline IntPtr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline IntPtr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(IntPtr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_3)); }
	inline IntPtr_t get_method_3() const { return ___method_3; }
	inline IntPtr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(IntPtr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___delegate_trampoline_4)); }
	inline IntPtr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline IntPtr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(IntPtr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_code_5)); }
	inline IntPtr_t get_method_code_5() const { return ___method_code_5; }
	inline IntPtr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(IntPtr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___data_8)); }
	inline DelegateData_t1572802995 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1572802995 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1572802995 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T3022476291_H
#ifndef DATETIMEKIND_T2186819611_H
#define DATETIMEKIND_T2186819611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeKind
struct  DateTimeKind_t2186819611 
{
public:
	// System.Int32 System.DateTimeKind::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeKind_t2186819611, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEKIND_T2186819611_H
#ifndef KEYMODIFIERS_T231190421_H
#define KEYMODIFIERS_T231190421_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Utilities.KeyModifiers
struct  KeyModifiers_t231190421 
{
public:
	// System.Int32 IBM.Watson.DeveloperCloud.Utilities.KeyModifiers::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(KeyModifiers_t231190421, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYMODIFIERS_T231190421_H
#ifndef OBJECT_T1021602117_H
#define OBJECT_T1021602117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1021602117  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	IntPtr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1021602117, ___m_CachedPtr_0)); }
	inline IntPtr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline IntPtr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(IntPtr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1021602117_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1021602117_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1021602117_H
#ifndef DATETIME_T693205669_H
#define DATETIME_T693205669_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t693205669 
{
public:
	// System.TimeSpan System.DateTime::ticks
	TimeSpan_t3430258949  ___ticks_10;
	// System.DateTimeKind System.DateTime::kind
	int32_t ___kind_11;

public:
	inline static int32_t get_offset_of_ticks_10() { return static_cast<int32_t>(offsetof(DateTime_t693205669, ___ticks_10)); }
	inline TimeSpan_t3430258949  get_ticks_10() const { return ___ticks_10; }
	inline TimeSpan_t3430258949 * get_address_of_ticks_10() { return &___ticks_10; }
	inline void set_ticks_10(TimeSpan_t3430258949  value)
	{
		___ticks_10 = value;
	}

	inline static int32_t get_offset_of_kind_11() { return static_cast<int32_t>(offsetof(DateTime_t693205669, ___kind_11)); }
	inline int32_t get_kind_11() const { return ___kind_11; }
	inline int32_t* get_address_of_kind_11() { return &___kind_11; }
	inline void set_kind_11(int32_t value)
	{
		___kind_11 = value;
	}
};

struct DateTime_t693205669_StaticFields
{
public:
	// System.DateTime System.DateTime::MaxValue
	DateTime_t693205669  ___MaxValue_12;
	// System.DateTime System.DateTime::MinValue
	DateTime_t693205669  ___MinValue_13;
	// System.String[] System.DateTime::ParseTimeFormats
	StringU5BU5D_t1642385972* ___ParseTimeFormats_14;
	// System.String[] System.DateTime::ParseYearDayMonthFormats
	StringU5BU5D_t1642385972* ___ParseYearDayMonthFormats_15;
	// System.String[] System.DateTime::ParseYearMonthDayFormats
	StringU5BU5D_t1642385972* ___ParseYearMonthDayFormats_16;
	// System.String[] System.DateTime::ParseDayMonthYearFormats
	StringU5BU5D_t1642385972* ___ParseDayMonthYearFormats_17;
	// System.String[] System.DateTime::ParseMonthDayYearFormats
	StringU5BU5D_t1642385972* ___ParseMonthDayYearFormats_18;
	// System.String[] System.DateTime::MonthDayShortFormats
	StringU5BU5D_t1642385972* ___MonthDayShortFormats_19;
	// System.String[] System.DateTime::DayMonthShortFormats
	StringU5BU5D_t1642385972* ___DayMonthShortFormats_20;
	// System.Int32[] System.DateTime::daysmonth
	Int32U5BU5D_t3030399641* ___daysmonth_21;
	// System.Int32[] System.DateTime::daysmonthleap
	Int32U5BU5D_t3030399641* ___daysmonthleap_22;
	// System.Object System.DateTime::to_local_time_span_object
	RuntimeObject * ___to_local_time_span_object_23;
	// System.Int64 System.DateTime::last_now
	int64_t ___last_now_24;

public:
	inline static int32_t get_offset_of_MaxValue_12() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___MaxValue_12)); }
	inline DateTime_t693205669  get_MaxValue_12() const { return ___MaxValue_12; }
	inline DateTime_t693205669 * get_address_of_MaxValue_12() { return &___MaxValue_12; }
	inline void set_MaxValue_12(DateTime_t693205669  value)
	{
		___MaxValue_12 = value;
	}

	inline static int32_t get_offset_of_MinValue_13() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___MinValue_13)); }
	inline DateTime_t693205669  get_MinValue_13() const { return ___MinValue_13; }
	inline DateTime_t693205669 * get_address_of_MinValue_13() { return &___MinValue_13; }
	inline void set_MinValue_13(DateTime_t693205669  value)
	{
		___MinValue_13 = value;
	}

	inline static int32_t get_offset_of_ParseTimeFormats_14() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseTimeFormats_14)); }
	inline StringU5BU5D_t1642385972* get_ParseTimeFormats_14() const { return ___ParseTimeFormats_14; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseTimeFormats_14() { return &___ParseTimeFormats_14; }
	inline void set_ParseTimeFormats_14(StringU5BU5D_t1642385972* value)
	{
		___ParseTimeFormats_14 = value;
		Il2CppCodeGenWriteBarrier((&___ParseTimeFormats_14), value);
	}

	inline static int32_t get_offset_of_ParseYearDayMonthFormats_15() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseYearDayMonthFormats_15)); }
	inline StringU5BU5D_t1642385972* get_ParseYearDayMonthFormats_15() const { return ___ParseYearDayMonthFormats_15; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseYearDayMonthFormats_15() { return &___ParseYearDayMonthFormats_15; }
	inline void set_ParseYearDayMonthFormats_15(StringU5BU5D_t1642385972* value)
	{
		___ParseYearDayMonthFormats_15 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearDayMonthFormats_15), value);
	}

	inline static int32_t get_offset_of_ParseYearMonthDayFormats_16() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseYearMonthDayFormats_16)); }
	inline StringU5BU5D_t1642385972* get_ParseYearMonthDayFormats_16() const { return ___ParseYearMonthDayFormats_16; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseYearMonthDayFormats_16() { return &___ParseYearMonthDayFormats_16; }
	inline void set_ParseYearMonthDayFormats_16(StringU5BU5D_t1642385972* value)
	{
		___ParseYearMonthDayFormats_16 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearMonthDayFormats_16), value);
	}

	inline static int32_t get_offset_of_ParseDayMonthYearFormats_17() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseDayMonthYearFormats_17)); }
	inline StringU5BU5D_t1642385972* get_ParseDayMonthYearFormats_17() const { return ___ParseDayMonthYearFormats_17; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseDayMonthYearFormats_17() { return &___ParseDayMonthYearFormats_17; }
	inline void set_ParseDayMonthYearFormats_17(StringU5BU5D_t1642385972* value)
	{
		___ParseDayMonthYearFormats_17 = value;
		Il2CppCodeGenWriteBarrier((&___ParseDayMonthYearFormats_17), value);
	}

	inline static int32_t get_offset_of_ParseMonthDayYearFormats_18() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseMonthDayYearFormats_18)); }
	inline StringU5BU5D_t1642385972* get_ParseMonthDayYearFormats_18() const { return ___ParseMonthDayYearFormats_18; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseMonthDayYearFormats_18() { return &___ParseMonthDayYearFormats_18; }
	inline void set_ParseMonthDayYearFormats_18(StringU5BU5D_t1642385972* value)
	{
		___ParseMonthDayYearFormats_18 = value;
		Il2CppCodeGenWriteBarrier((&___ParseMonthDayYearFormats_18), value);
	}

	inline static int32_t get_offset_of_MonthDayShortFormats_19() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___MonthDayShortFormats_19)); }
	inline StringU5BU5D_t1642385972* get_MonthDayShortFormats_19() const { return ___MonthDayShortFormats_19; }
	inline StringU5BU5D_t1642385972** get_address_of_MonthDayShortFormats_19() { return &___MonthDayShortFormats_19; }
	inline void set_MonthDayShortFormats_19(StringU5BU5D_t1642385972* value)
	{
		___MonthDayShortFormats_19 = value;
		Il2CppCodeGenWriteBarrier((&___MonthDayShortFormats_19), value);
	}

	inline static int32_t get_offset_of_DayMonthShortFormats_20() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___DayMonthShortFormats_20)); }
	inline StringU5BU5D_t1642385972* get_DayMonthShortFormats_20() const { return ___DayMonthShortFormats_20; }
	inline StringU5BU5D_t1642385972** get_address_of_DayMonthShortFormats_20() { return &___DayMonthShortFormats_20; }
	inline void set_DayMonthShortFormats_20(StringU5BU5D_t1642385972* value)
	{
		___DayMonthShortFormats_20 = value;
		Il2CppCodeGenWriteBarrier((&___DayMonthShortFormats_20), value);
	}

	inline static int32_t get_offset_of_daysmonth_21() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___daysmonth_21)); }
	inline Int32U5BU5D_t3030399641* get_daysmonth_21() const { return ___daysmonth_21; }
	inline Int32U5BU5D_t3030399641** get_address_of_daysmonth_21() { return &___daysmonth_21; }
	inline void set_daysmonth_21(Int32U5BU5D_t3030399641* value)
	{
		___daysmonth_21 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonth_21), value);
	}

	inline static int32_t get_offset_of_daysmonthleap_22() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___daysmonthleap_22)); }
	inline Int32U5BU5D_t3030399641* get_daysmonthleap_22() const { return ___daysmonthleap_22; }
	inline Int32U5BU5D_t3030399641** get_address_of_daysmonthleap_22() { return &___daysmonthleap_22; }
	inline void set_daysmonthleap_22(Int32U5BU5D_t3030399641* value)
	{
		___daysmonthleap_22 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonthleap_22), value);
	}

	inline static int32_t get_offset_of_to_local_time_span_object_23() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___to_local_time_span_object_23)); }
	inline RuntimeObject * get_to_local_time_span_object_23() const { return ___to_local_time_span_object_23; }
	inline RuntimeObject ** get_address_of_to_local_time_span_object_23() { return &___to_local_time_span_object_23; }
	inline void set_to_local_time_span_object_23(RuntimeObject * value)
	{
		___to_local_time_span_object_23 = value;
		Il2CppCodeGenWriteBarrier((&___to_local_time_span_object_23), value);
	}

	inline static int32_t get_offset_of_last_now_24() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___last_now_24)); }
	inline int64_t get_last_now_24() const { return ___last_now_24; }
	inline int64_t* get_address_of_last_now_24() { return &___last_now_24; }
	inline void set_last_now_24(int64_t value)
	{
		___last_now_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T693205669_H
#ifndef MULTICASTDELEGATE_T3201952435_H
#define MULTICASTDELEGATE_T3201952435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t3201952435  : public Delegate_t3022476291
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t3201952435 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t3201952435 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___prev_9)); }
	inline MulticastDelegate_t3201952435 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t3201952435 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t3201952435 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___kpm_next_10)); }
	inline MulticastDelegate_t3201952435 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t3201952435 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t3201952435 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T3201952435_H
#ifndef COMPONENT_T3819376471_H
#define COMPONENT_T3819376471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t3819376471  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T3819376471_H
#ifndef ONDELETECOLLECTIONIMAGE_T241020072_H
#define ONDELETECOLLECTIONIMAGE_T241020072_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/OnDeleteCollectionImage
struct  OnDeleteCollectionImage_t241020072  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDELETECOLLECTIONIMAGE_T241020072_H
#ifndef ONGETCOLLECTIONIMAGES_T2736783742_H
#define ONGETCOLLECTIONIMAGES_T2736783742_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/OnGetCollectionImages
struct  OnGetCollectionImages_t2736783742  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONGETCOLLECTIONIMAGES_T2736783742_H
#ifndef ONADDCOLLECTIONIMAGE_T1084877344_H
#define ONADDCOLLECTIONIMAGE_T1084877344_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/OnAddCollectionImage
struct  OnAddCollectionImage_t1084877344  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONADDCOLLECTIONIMAGE_T1084877344_H
#ifndef ONGETCOLLECTION_T621465616_H
#define ONGETCOLLECTION_T621465616_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/OnGetCollection
struct  OnGetCollection_t621465616  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONGETCOLLECTION_T621465616_H
#ifndef ONCREATECOLLECTION_T970139976_H
#define ONCREATECOLLECTION_T970139976_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/OnCreateCollection
struct  OnCreateCollection_t970139976  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONCREATECOLLECTION_T970139976_H
#ifndef ONGETCOLLECTIONS_T484200113_H
#define ONGETCOLLECTIONS_T484200113_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/OnGetCollections
struct  OnGetCollections_t484200113  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONGETCOLLECTIONS_T484200113_H
#ifndef ONDELETECOLLECTION_T1354946561_H
#define ONDELETECOLLECTION_T1354946561_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/OnDeleteCollection
struct  OnDeleteCollection_t1354946561  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDELETECOLLECTION_T1354946561_H
#ifndef ONRECOGNIZETEXT_T793159563_H
#define ONRECOGNIZETEXT_T793159563_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/OnRecognizeText
struct  OnRecognizeText_t793159563  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONRECOGNIZETEXT_T793159563_H
#ifndef BEHAVIOUR_T955675639_H
#define BEHAVIOUR_T955675639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t955675639  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T955675639_H
#ifndef ONGETIMAGEDETAILS_T1270200205_H
#define ONGETIMAGEDETAILS_T1270200205_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/OnGetImageDetails
struct  OnGetImageDetails_t1270200205  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONGETIMAGEDETAILS_T1270200205_H
#ifndef ONTESTSCOMPLETE_T1412473303_H
#define ONTESTSCOMPLETE_T1412473303_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Editor.UnitTestManager/OnTestsComplete
struct  OnTestsComplete_t1412473303  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONTESTSCOMPLETE_T1412473303_H
#ifndef CACHEITEM_T2472383731_H
#define CACHEITEM_T2472383731_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Utilities.DataCache/CacheItem
struct  CacheItem_t2472383731  : public RuntimeObject
{
public:
	// System.String IBM.Watson.DeveloperCloud.Utilities.DataCache/CacheItem::<Path>k__BackingField
	String_t* ___U3CPathU3Ek__BackingField_0;
	// System.String IBM.Watson.DeveloperCloud.Utilities.DataCache/CacheItem::<Id>k__BackingField
	String_t* ___U3CIdU3Ek__BackingField_1;
	// System.DateTime IBM.Watson.DeveloperCloud.Utilities.DataCache/CacheItem::<Time>k__BackingField
	DateTime_t693205669  ___U3CTimeU3Ek__BackingField_2;
	// System.Byte[] IBM.Watson.DeveloperCloud.Utilities.DataCache/CacheItem::<Data>k__BackingField
	ByteU5BU5D_t3397334013* ___U3CDataU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CPathU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CacheItem_t2472383731, ___U3CPathU3Ek__BackingField_0)); }
	inline String_t* get_U3CPathU3Ek__BackingField_0() const { return ___U3CPathU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CPathU3Ek__BackingField_0() { return &___U3CPathU3Ek__BackingField_0; }
	inline void set_U3CPathU3Ek__BackingField_0(String_t* value)
	{
		___U3CPathU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPathU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CIdU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CacheItem_t2472383731, ___U3CIdU3Ek__BackingField_1)); }
	inline String_t* get_U3CIdU3Ek__BackingField_1() const { return ___U3CIdU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CIdU3Ek__BackingField_1() { return &___U3CIdU3Ek__BackingField_1; }
	inline void set_U3CIdU3Ek__BackingField_1(String_t* value)
	{
		___U3CIdU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CIdU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CTimeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CacheItem_t2472383731, ___U3CTimeU3Ek__BackingField_2)); }
	inline DateTime_t693205669  get_U3CTimeU3Ek__BackingField_2() const { return ___U3CTimeU3Ek__BackingField_2; }
	inline DateTime_t693205669 * get_address_of_U3CTimeU3Ek__BackingField_2() { return &___U3CTimeU3Ek__BackingField_2; }
	inline void set_U3CTimeU3Ek__BackingField_2(DateTime_t693205669  value)
	{
		___U3CTimeU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(CacheItem_t2472383731, ___U3CDataU3Ek__BackingField_3)); }
	inline ByteU5BU5D_t3397334013* get_U3CDataU3Ek__BackingField_3() const { return ___U3CDataU3Ek__BackingField_3; }
	inline ByteU5BU5D_t3397334013** get_address_of_U3CDataU3Ek__BackingField_3() { return &___U3CDataU3Ek__BackingField_3; }
	inline void set_U3CDataU3Ek__BackingField_3(ByteU5BU5D_t3397334013* value)
	{
		___U3CDataU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDataU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CACHEITEM_T2472383731_H
#ifndef U3CRUNTESTSCRU3EC__ITERATOR0_T2904975748_H
#define U3CRUNTESTSCRU3EC__ITERATOR0_T2904975748_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Editor.UnitTestManager/<RunTestsCR>c__Iterator0
struct  U3CRunTestsCRU3Ec__Iterator0_t2904975748  : public RuntimeObject
{
public:
	// System.Type IBM.Watson.DeveloperCloud.Editor.UnitTestManager/<RunTestsCR>c__Iterator0::<testType>__1
	Type_t * ___U3CtestTypeU3E__1_0;
	// System.Boolean IBM.Watson.DeveloperCloud.Editor.UnitTestManager/<RunTestsCR>c__Iterator0::<bTestException>__2
	bool ___U3CbTestExceptionU3E__2_1;
	// System.DateTime IBM.Watson.DeveloperCloud.Editor.UnitTestManager/<RunTestsCR>c__Iterator0::<startTime>__2
	DateTime_t693205669  ___U3CstartTimeU3E__2_2;
	// System.Collections.IEnumerator IBM.Watson.DeveloperCloud.Editor.UnitTestManager/<RunTestsCR>c__Iterator0::<e>__3
	RuntimeObject* ___U3CeU3E__3_3;
	// IBM.Watson.DeveloperCloud.Editor.UnitTestManager IBM.Watson.DeveloperCloud.Editor.UnitTestManager/<RunTestsCR>c__Iterator0::$this
	UnitTestManager_t2486317649 * ___U24this_4;
	// System.Object IBM.Watson.DeveloperCloud.Editor.UnitTestManager/<RunTestsCR>c__Iterator0::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean IBM.Watson.DeveloperCloud.Editor.UnitTestManager/<RunTestsCR>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 IBM.Watson.DeveloperCloud.Editor.UnitTestManager/<RunTestsCR>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CtestTypeU3E__1_0() { return static_cast<int32_t>(offsetof(U3CRunTestsCRU3Ec__Iterator0_t2904975748, ___U3CtestTypeU3E__1_0)); }
	inline Type_t * get_U3CtestTypeU3E__1_0() const { return ___U3CtestTypeU3E__1_0; }
	inline Type_t ** get_address_of_U3CtestTypeU3E__1_0() { return &___U3CtestTypeU3E__1_0; }
	inline void set_U3CtestTypeU3E__1_0(Type_t * value)
	{
		___U3CtestTypeU3E__1_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtestTypeU3E__1_0), value);
	}

	inline static int32_t get_offset_of_U3CbTestExceptionU3E__2_1() { return static_cast<int32_t>(offsetof(U3CRunTestsCRU3Ec__Iterator0_t2904975748, ___U3CbTestExceptionU3E__2_1)); }
	inline bool get_U3CbTestExceptionU3E__2_1() const { return ___U3CbTestExceptionU3E__2_1; }
	inline bool* get_address_of_U3CbTestExceptionU3E__2_1() { return &___U3CbTestExceptionU3E__2_1; }
	inline void set_U3CbTestExceptionU3E__2_1(bool value)
	{
		___U3CbTestExceptionU3E__2_1 = value;
	}

	inline static int32_t get_offset_of_U3CstartTimeU3E__2_2() { return static_cast<int32_t>(offsetof(U3CRunTestsCRU3Ec__Iterator0_t2904975748, ___U3CstartTimeU3E__2_2)); }
	inline DateTime_t693205669  get_U3CstartTimeU3E__2_2() const { return ___U3CstartTimeU3E__2_2; }
	inline DateTime_t693205669 * get_address_of_U3CstartTimeU3E__2_2() { return &___U3CstartTimeU3E__2_2; }
	inline void set_U3CstartTimeU3E__2_2(DateTime_t693205669  value)
	{
		___U3CstartTimeU3E__2_2 = value;
	}

	inline static int32_t get_offset_of_U3CeU3E__3_3() { return static_cast<int32_t>(offsetof(U3CRunTestsCRU3Ec__Iterator0_t2904975748, ___U3CeU3E__3_3)); }
	inline RuntimeObject* get_U3CeU3E__3_3() const { return ___U3CeU3E__3_3; }
	inline RuntimeObject** get_address_of_U3CeU3E__3_3() { return &___U3CeU3E__3_3; }
	inline void set_U3CeU3E__3_3(RuntimeObject* value)
	{
		___U3CeU3E__3_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CeU3E__3_3), value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CRunTestsCRU3Ec__Iterator0_t2904975748, ___U24this_4)); }
	inline UnitTestManager_t2486317649 * get_U24this_4() const { return ___U24this_4; }
	inline UnitTestManager_t2486317649 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(UnitTestManager_t2486317649 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CRunTestsCRU3Ec__Iterator0_t2904975748, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CRunTestsCRU3Ec__Iterator0_t2904975748, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CRunTestsCRU3Ec__Iterator0_t2904975748, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRUNTESTSCRU3EC__ITERATOR0_T2904975748_H
#ifndef ONRECEIVEEVENT_T4237107671_H
#define ONRECEIVEEVENT_T4237107671_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Utilities.EventManager/OnReceiveEvent
struct  OnReceiveEvent_t4237107671  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONRECEIVEEVENT_T4237107671_H
#ifndef LOADFILEDELEGATE_T3728218844_H
#define LOADFILEDELEGATE_T3728218844_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/LoadFileDelegate
struct  LoadFileDelegate_t3728218844  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADFILEDELEGATE_T3728218844_H
#ifndef ONGETIMAGEMETADATA_T54004548_H
#define ONGETIMAGEMETADATA_T54004548_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/OnGetImageMetadata
struct  OnGetImageMetadata_t54004548  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONGETIMAGEMETADATA_T54004548_H
#ifndef ONDELETEIMAGEMETADATA_T2373116469_H
#define ONDELETEIMAGEMETADATA_T2373116469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/OnDeleteImageMetadata
struct  OnDeleteImageMetadata_t2373116469  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDELETEIMAGEMETADATA_T2373116469_H
#ifndef ONFINDSIMILAR_T219840828_H
#define ONFINDSIMILAR_T219840828_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Services.VisualRecognition.v3.VisualRecognition/OnFindSimilar
struct  OnFindSimilar_t219840828  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONFINDSIMILAR_T219840828_H
#ifndef MONOBEHAVIOUR_T1158329972_H
#define MONOBEHAVIOUR_T1158329972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t1158329972  : public Behaviour_t955675639
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T1158329972_H
#ifndef KEYEVENTMANAGER_T3314674286_H
#define KEYEVENTMANAGER_T3314674286_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Utilities.KeyEventManager
struct  KeyEventManager_t3314674286  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 IBM.Watson.DeveloperCloud.Utilities.KeyEventManager::MODIFIER_SHIFT_BITS
	int32_t ___MODIFIER_SHIFT_BITS_2;
	// System.Int32 IBM.Watson.DeveloperCloud.Utilities.KeyEventManager::KEYCODE_MASK
	int32_t ___KEYCODE_MASK_3;
	// System.Boolean IBM.Watson.DeveloperCloud.Utilities.KeyEventManager::m_Active
	bool ___m_Active_4;
	// System.Boolean IBM.Watson.DeveloperCloud.Utilities.KeyEventManager::m_UpdateActivate
	bool ___m_UpdateActivate_5;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.String> IBM.Watson.DeveloperCloud.Utilities.KeyEventManager::m_KeyEvents
	Dictionary_2_t1037045868 * ___m_KeyEvents_6;

public:
	inline static int32_t get_offset_of_MODIFIER_SHIFT_BITS_2() { return static_cast<int32_t>(offsetof(KeyEventManager_t3314674286, ___MODIFIER_SHIFT_BITS_2)); }
	inline int32_t get_MODIFIER_SHIFT_BITS_2() const { return ___MODIFIER_SHIFT_BITS_2; }
	inline int32_t* get_address_of_MODIFIER_SHIFT_BITS_2() { return &___MODIFIER_SHIFT_BITS_2; }
	inline void set_MODIFIER_SHIFT_BITS_2(int32_t value)
	{
		___MODIFIER_SHIFT_BITS_2 = value;
	}

	inline static int32_t get_offset_of_KEYCODE_MASK_3() { return static_cast<int32_t>(offsetof(KeyEventManager_t3314674286, ___KEYCODE_MASK_3)); }
	inline int32_t get_KEYCODE_MASK_3() const { return ___KEYCODE_MASK_3; }
	inline int32_t* get_address_of_KEYCODE_MASK_3() { return &___KEYCODE_MASK_3; }
	inline void set_KEYCODE_MASK_3(int32_t value)
	{
		___KEYCODE_MASK_3 = value;
	}

	inline static int32_t get_offset_of_m_Active_4() { return static_cast<int32_t>(offsetof(KeyEventManager_t3314674286, ___m_Active_4)); }
	inline bool get_m_Active_4() const { return ___m_Active_4; }
	inline bool* get_address_of_m_Active_4() { return &___m_Active_4; }
	inline void set_m_Active_4(bool value)
	{
		___m_Active_4 = value;
	}

	inline static int32_t get_offset_of_m_UpdateActivate_5() { return static_cast<int32_t>(offsetof(KeyEventManager_t3314674286, ___m_UpdateActivate_5)); }
	inline bool get_m_UpdateActivate_5() const { return ___m_UpdateActivate_5; }
	inline bool* get_address_of_m_UpdateActivate_5() { return &___m_UpdateActivate_5; }
	inline void set_m_UpdateActivate_5(bool value)
	{
		___m_UpdateActivate_5 = value;
	}

	inline static int32_t get_offset_of_m_KeyEvents_6() { return static_cast<int32_t>(offsetof(KeyEventManager_t3314674286, ___m_KeyEvents_6)); }
	inline Dictionary_2_t1037045868 * get_m_KeyEvents_6() const { return ___m_KeyEvents_6; }
	inline Dictionary_2_t1037045868 ** get_address_of_m_KeyEvents_6() { return &___m_KeyEvents_6; }
	inline void set_m_KeyEvents_6(Dictionary_2_t1037045868 * value)
	{
		___m_KeyEvents_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_KeyEvents_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYEVENTMANAGER_T3314674286_H
#ifndef FRAMERATECOUNTER_T1983932695_H
#define FRAMERATECOUNTER_T1983932695_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Utilities.FrameRateCounter
struct  FrameRateCounter_t1983932695  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 IBM.Watson.DeveloperCloud.Utilities.FrameRateCounter::m_FpsAccumulator
	int32_t ___m_FpsAccumulator_4;
	// System.Single IBM.Watson.DeveloperCloud.Utilities.FrameRateCounter::m_FpsNextPeriod
	float ___m_FpsNextPeriod_5;
	// System.Int32 IBM.Watson.DeveloperCloud.Utilities.FrameRateCounter::m_CurrentFps
	int32_t ___m_CurrentFps_6;
	// UnityEngine.UI.Text IBM.Watson.DeveloperCloud.Utilities.FrameRateCounter::m_Text
	Text_t356221433 * ___m_Text_7;

public:
	inline static int32_t get_offset_of_m_FpsAccumulator_4() { return static_cast<int32_t>(offsetof(FrameRateCounter_t1983932695, ___m_FpsAccumulator_4)); }
	inline int32_t get_m_FpsAccumulator_4() const { return ___m_FpsAccumulator_4; }
	inline int32_t* get_address_of_m_FpsAccumulator_4() { return &___m_FpsAccumulator_4; }
	inline void set_m_FpsAccumulator_4(int32_t value)
	{
		___m_FpsAccumulator_4 = value;
	}

	inline static int32_t get_offset_of_m_FpsNextPeriod_5() { return static_cast<int32_t>(offsetof(FrameRateCounter_t1983932695, ___m_FpsNextPeriod_5)); }
	inline float get_m_FpsNextPeriod_5() const { return ___m_FpsNextPeriod_5; }
	inline float* get_address_of_m_FpsNextPeriod_5() { return &___m_FpsNextPeriod_5; }
	inline void set_m_FpsNextPeriod_5(float value)
	{
		___m_FpsNextPeriod_5 = value;
	}

	inline static int32_t get_offset_of_m_CurrentFps_6() { return static_cast<int32_t>(offsetof(FrameRateCounter_t1983932695, ___m_CurrentFps_6)); }
	inline int32_t get_m_CurrentFps_6() const { return ___m_CurrentFps_6; }
	inline int32_t* get_address_of_m_CurrentFps_6() { return &___m_CurrentFps_6; }
	inline void set_m_CurrentFps_6(int32_t value)
	{
		___m_CurrentFps_6 = value;
	}

	inline static int32_t get_offset_of_m_Text_7() { return static_cast<int32_t>(offsetof(FrameRateCounter_t1983932695, ___m_Text_7)); }
	inline Text_t356221433 * get_m_Text_7() const { return ___m_Text_7; }
	inline Text_t356221433 ** get_address_of_m_Text_7() { return &___m_Text_7; }
	inline void set_m_Text_7(Text_t356221433 * value)
	{
		___m_Text_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Text_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRAMERATECOUNTER_T1983932695_H
#ifndef UNITTESTMANAGER_T2486317649_H
#define UNITTESTMANAGER_T2486317649_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IBM.Watson.DeveloperCloud.Editor.UnitTestManager
struct  UnitTestManager_t2486317649  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 IBM.Watson.DeveloperCloud.Editor.UnitTestManager::<TestsFailed>k__BackingField
	int32_t ___U3CTestsFailedU3Ek__BackingField_3;
	// System.Int32 IBM.Watson.DeveloperCloud.Editor.UnitTestManager::<TestsComplete>k__BackingField
	int32_t ___U3CTestsCompleteU3Ek__BackingField_4;
	// System.Boolean IBM.Watson.DeveloperCloud.Editor.UnitTestManager::<QuitOnTestsComplete>k__BackingField
	bool ___U3CQuitOnTestsCompleteU3Ek__BackingField_5;
	// IBM.Watson.DeveloperCloud.Editor.UnitTestManager/OnTestsComplete IBM.Watson.DeveloperCloud.Editor.UnitTestManager::<OnTestCompleteCallback>k__BackingField
	OnTestsComplete_t1412473303 * ___U3COnTestCompleteCallbackU3Ek__BackingField_6;
	// System.String IBM.Watson.DeveloperCloud.Editor.UnitTestManager::m_ProjectNameToTest
	String_t* ___m_ProjectNameToTest_7;
	// System.Collections.Generic.Queue`1<System.Type> IBM.Watson.DeveloperCloud.Editor.UnitTestManager::m_QueuedTests
	Queue_1_t1123460061 * ___m_QueuedTests_9;
	// System.Type[] IBM.Watson.DeveloperCloud.Editor.UnitTestManager::m_TestsAvailable
	TypeU5BU5D_t1664964607* ___m_TestsAvailable_10;
	// IBM.Watson.DeveloperCloud.UnitTests.UnitTest IBM.Watson.DeveloperCloud.Editor.UnitTestManager::m_ActiveTest
	UnitTest_t237671048 * ___m_ActiveTest_11;

public:
	inline static int32_t get_offset_of_U3CTestsFailedU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(UnitTestManager_t2486317649, ___U3CTestsFailedU3Ek__BackingField_3)); }
	inline int32_t get_U3CTestsFailedU3Ek__BackingField_3() const { return ___U3CTestsFailedU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CTestsFailedU3Ek__BackingField_3() { return &___U3CTestsFailedU3Ek__BackingField_3; }
	inline void set_U3CTestsFailedU3Ek__BackingField_3(int32_t value)
	{
		___U3CTestsFailedU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CTestsCompleteU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(UnitTestManager_t2486317649, ___U3CTestsCompleteU3Ek__BackingField_4)); }
	inline int32_t get_U3CTestsCompleteU3Ek__BackingField_4() const { return ___U3CTestsCompleteU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CTestsCompleteU3Ek__BackingField_4() { return &___U3CTestsCompleteU3Ek__BackingField_4; }
	inline void set_U3CTestsCompleteU3Ek__BackingField_4(int32_t value)
	{
		___U3CTestsCompleteU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CQuitOnTestsCompleteU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(UnitTestManager_t2486317649, ___U3CQuitOnTestsCompleteU3Ek__BackingField_5)); }
	inline bool get_U3CQuitOnTestsCompleteU3Ek__BackingField_5() const { return ___U3CQuitOnTestsCompleteU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CQuitOnTestsCompleteU3Ek__BackingField_5() { return &___U3CQuitOnTestsCompleteU3Ek__BackingField_5; }
	inline void set_U3CQuitOnTestsCompleteU3Ek__BackingField_5(bool value)
	{
		___U3CQuitOnTestsCompleteU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3COnTestCompleteCallbackU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(UnitTestManager_t2486317649, ___U3COnTestCompleteCallbackU3Ek__BackingField_6)); }
	inline OnTestsComplete_t1412473303 * get_U3COnTestCompleteCallbackU3Ek__BackingField_6() const { return ___U3COnTestCompleteCallbackU3Ek__BackingField_6; }
	inline OnTestsComplete_t1412473303 ** get_address_of_U3COnTestCompleteCallbackU3Ek__BackingField_6() { return &___U3COnTestCompleteCallbackU3Ek__BackingField_6; }
	inline void set_U3COnTestCompleteCallbackU3Ek__BackingField_6(OnTestsComplete_t1412473303 * value)
	{
		___U3COnTestCompleteCallbackU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3COnTestCompleteCallbackU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_m_ProjectNameToTest_7() { return static_cast<int32_t>(offsetof(UnitTestManager_t2486317649, ___m_ProjectNameToTest_7)); }
	inline String_t* get_m_ProjectNameToTest_7() const { return ___m_ProjectNameToTest_7; }
	inline String_t** get_address_of_m_ProjectNameToTest_7() { return &___m_ProjectNameToTest_7; }
	inline void set_m_ProjectNameToTest_7(String_t* value)
	{
		___m_ProjectNameToTest_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_ProjectNameToTest_7), value);
	}

	inline static int32_t get_offset_of_m_QueuedTests_9() { return static_cast<int32_t>(offsetof(UnitTestManager_t2486317649, ___m_QueuedTests_9)); }
	inline Queue_1_t1123460061 * get_m_QueuedTests_9() const { return ___m_QueuedTests_9; }
	inline Queue_1_t1123460061 ** get_address_of_m_QueuedTests_9() { return &___m_QueuedTests_9; }
	inline void set_m_QueuedTests_9(Queue_1_t1123460061 * value)
	{
		___m_QueuedTests_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_QueuedTests_9), value);
	}

	inline static int32_t get_offset_of_m_TestsAvailable_10() { return static_cast<int32_t>(offsetof(UnitTestManager_t2486317649, ___m_TestsAvailable_10)); }
	inline TypeU5BU5D_t1664964607* get_m_TestsAvailable_10() const { return ___m_TestsAvailable_10; }
	inline TypeU5BU5D_t1664964607** get_address_of_m_TestsAvailable_10() { return &___m_TestsAvailable_10; }
	inline void set_m_TestsAvailable_10(TypeU5BU5D_t1664964607* value)
	{
		___m_TestsAvailable_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_TestsAvailable_10), value);
	}

	inline static int32_t get_offset_of_m_ActiveTest_11() { return static_cast<int32_t>(offsetof(UnitTestManager_t2486317649, ___m_ActiveTest_11)); }
	inline UnitTest_t237671048 * get_m_ActiveTest_11() const { return ___m_ActiveTest_11; }
	inline UnitTest_t237671048 ** get_address_of_m_ActiveTest_11() { return &___m_ActiveTest_11; }
	inline void set_m_ActiveTest_11(UnitTest_t237671048 * value)
	{
		___m_ActiveTest_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_ActiveTest_11), value);
	}
};

struct UnitTestManager_t2486317649_StaticFields
{
public:
	// System.String IBM.Watson.DeveloperCloud.Editor.UnitTestManager::ProjectToTest
	String_t* ___ProjectToTest_8;

public:
	inline static int32_t get_offset_of_ProjectToTest_8() { return static_cast<int32_t>(offsetof(UnitTestManager_t2486317649_StaticFields, ___ProjectToTest_8)); }
	inline String_t* get_ProjectToTest_8() const { return ___ProjectToTest_8; }
	inline String_t** get_address_of_ProjectToTest_8() { return &___ProjectToTest_8; }
	inline void set_ProjectToTest_8(String_t* value)
	{
		___ProjectToTest_8 = value;
		Il2CppCodeGenWriteBarrier((&___ProjectToTest_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITTESTMANAGER_T2486317649_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2900 = { sizeof (OnRecognizeText_t793159563), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2901 = { sizeof (OnGetCollections_t484200113), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2902 = { sizeof (OnCreateCollection_t970139976), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2903 = { sizeof (OnDeleteCollection_t1354946561), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2904 = { sizeof (OnGetCollection_t621465616), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2905 = { sizeof (OnGetCollectionImages_t2736783742), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2906 = { sizeof (OnAddCollectionImage_t1084877344), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2907 = { sizeof (OnDeleteCollectionImage_t241020072), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2908 = { sizeof (OnGetImageDetails_t1270200205), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2909 = { sizeof (OnDeleteImageMetadata_t2373116469), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2910 = { sizeof (OnGetImageMetadata_t54004548), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2911 = { sizeof (OnFindSimilar_t219840828), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2912 = { sizeof (LoadFileDelegate_t3728218844), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2913 = { sizeof (ClassifyReq_t1500879585), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2913[3] = 
{
	ClassifyReq_t1500879585::get_offset_of_U3CDataU3Ek__BackingField_11(),
	ClassifyReq_t1500879585::get_offset_of_U3CCallbackU3Ek__BackingField_12(),
	ClassifyReq_t1500879585::get_offset_of_U3CAcceptLanguageU3Ek__BackingField_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2914 = { sizeof (DetectFacesReq_t538961576), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2914[2] = 
{
	DetectFacesReq_t538961576::get_offset_of_U3CDataU3Ek__BackingField_11(),
	DetectFacesReq_t538961576::get_offset_of_U3CCallbackU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2915 = { sizeof (RecognizeTextReq_t3895541236), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2915[2] = 
{
	RecognizeTextReq_t3895541236::get_offset_of_U3CDataU3Ek__BackingField_11(),
	RecognizeTextReq_t3895541236::get_offset_of_U3CCallbackU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2916 = { sizeof (FindClassifierReq_t2124707147), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2916[3] = 
{
	FindClassifierReq_t2124707147::get_offset_of_U3CServiceU3Ek__BackingField_0(),
	FindClassifierReq_t2124707147::get_offset_of_U3CClassifierNameU3Ek__BackingField_1(),
	FindClassifierReq_t2124707147::get_offset_of_U3CCallbackU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2917 = { sizeof (GetClassifiersReq_t425495667), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2917[2] = 
{
	GetClassifiersReq_t425495667::get_offset_of_U3CDataU3Ek__BackingField_11(),
	GetClassifiersReq_t425495667::get_offset_of_U3CCallbackU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2918 = { sizeof (GetClassifierReq_t1479437154), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2918[2] = 
{
	GetClassifierReq_t1479437154::get_offset_of_U3CDataU3Ek__BackingField_11(),
	GetClassifierReq_t1479437154::get_offset_of_U3CCallbackU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2919 = { sizeof (TrainClassifierReq_t531588072), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2919[2] = 
{
	TrainClassifierReq_t531588072::get_offset_of_U3CDataU3Ek__BackingField_11(),
	TrainClassifierReq_t531588072::get_offset_of_U3CCallbackU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2920 = { sizeof (DeleteClassifierReq_t2958897583), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2920[2] = 
{
	DeleteClassifierReq_t2958897583::get_offset_of_U3CDataU3Ek__BackingField_11(),
	DeleteClassifierReq_t2958897583::get_offset_of_U3CCallbackU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2921 = { sizeof (GetCollectionsReq_t4268407020), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2921[2] = 
{
	GetCollectionsReq_t4268407020::get_offset_of_U3CCallbackU3Ek__BackingField_11(),
	GetCollectionsReq_t4268407020::get_offset_of_U3CDataU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2922 = { sizeof (CreateCollectionReq_t2577939415), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2922[3] = 
{
	CreateCollectionReq_t2577939415::get_offset_of_U3CCallbackU3Ek__BackingField_11(),
	CreateCollectionReq_t2577939415::get_offset_of_U3CNameU3Ek__BackingField_12(),
	CreateCollectionReq_t2577939415::get_offset_of_U3CDataU3Ek__BackingField_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2923 = { sizeof (DeleteCollectionReq_t2946528802), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2923[3] = 
{
	DeleteCollectionReq_t2946528802::get_offset_of_U3CCallbackU3Ek__BackingField_11(),
	DeleteCollectionReq_t2946528802::get_offset_of_U3CCollectionIDU3Ek__BackingField_12(),
	DeleteCollectionReq_t2946528802::get_offset_of_U3CDataU3Ek__BackingField_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2924 = { sizeof (GetCollectionReq_t1390443149), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2924[3] = 
{
	GetCollectionReq_t1390443149::get_offset_of_U3CCallbackU3Ek__BackingField_11(),
	GetCollectionReq_t1390443149::get_offset_of_U3CCollectionIDU3Ek__BackingField_12(),
	GetCollectionReq_t1390443149::get_offset_of_U3CDataU3Ek__BackingField_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2925 = { sizeof (GetCollectionImagesReq_t1811544573), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2925[3] = 
{
	GetCollectionImagesReq_t1811544573::get_offset_of_U3CCallbackU3Ek__BackingField_11(),
	GetCollectionImagesReq_t1811544573::get_offset_of_U3CCollectionIDU3Ek__BackingField_12(),
	GetCollectionImagesReq_t1811544573::get_offset_of_U3CDataU3Ek__BackingField_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2926 = { sizeof (AddCollectionImageReq_t3653897285), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2926[5] = 
{
	AddCollectionImageReq_t3653897285::get_offset_of_U3CCallbackU3Ek__BackingField_11(),
	AddCollectionImageReq_t3653897285::get_offset_of_U3CCollectionIDU3Ek__BackingField_12(),
	AddCollectionImageReq_t3653897285::get_offset_of_U3CImageDataU3Ek__BackingField_13(),
	AddCollectionImageReq_t3653897285::get_offset_of_U3CMetadataU3Ek__BackingField_14(),
	AddCollectionImageReq_t3653897285::get_offset_of_U3CDataU3Ek__BackingField_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2927 = { sizeof (DeleteCollectionImageReq_t2682337773), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2927[4] = 
{
	DeleteCollectionImageReq_t2682337773::get_offset_of_U3CCallbackU3Ek__BackingField_11(),
	DeleteCollectionImageReq_t2682337773::get_offset_of_U3CCollectionIDU3Ek__BackingField_12(),
	DeleteCollectionImageReq_t2682337773::get_offset_of_U3CImageIDU3Ek__BackingField_13(),
	DeleteCollectionImageReq_t2682337773::get_offset_of_U3CDataU3Ek__BackingField_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2928 = { sizeof (GetCollectionImageReq_t2536251924), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2928[4] = 
{
	GetCollectionImageReq_t2536251924::get_offset_of_U3CCallbackU3Ek__BackingField_11(),
	GetCollectionImageReq_t2536251924::get_offset_of_U3CCollectionIDU3Ek__BackingField_12(),
	GetCollectionImageReq_t2536251924::get_offset_of_U3CImageIDU3Ek__BackingField_13(),
	GetCollectionImageReq_t2536251924::get_offset_of_U3CDataU3Ek__BackingField_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2929 = { sizeof (DeleteCollectionImageMetadataReq_t3072827134), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2929[4] = 
{
	DeleteCollectionImageMetadataReq_t3072827134::get_offset_of_U3CCallbackU3Ek__BackingField_11(),
	DeleteCollectionImageMetadataReq_t3072827134::get_offset_of_U3CCollectionIDU3Ek__BackingField_12(),
	DeleteCollectionImageMetadataReq_t3072827134::get_offset_of_U3CImageIDU3Ek__BackingField_13(),
	DeleteCollectionImageMetadataReq_t3072827134::get_offset_of_U3CDataU3Ek__BackingField_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2930 = { sizeof (GetCollectionImageMetadataReq_t1443854471), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2930[4] = 
{
	GetCollectionImageMetadataReq_t1443854471::get_offset_of_U3CCallbackU3Ek__BackingField_11(),
	GetCollectionImageMetadataReq_t1443854471::get_offset_of_U3CCollectionIDU3Ek__BackingField_12(),
	GetCollectionImageMetadataReq_t1443854471::get_offset_of_U3CImageIDU3Ek__BackingField_13(),
	GetCollectionImageMetadataReq_t1443854471::get_offset_of_U3CDataU3Ek__BackingField_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2931 = { sizeof (FindSimilarReq_t671452345), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2931[5] = 
{
	FindSimilarReq_t671452345::get_offset_of_U3CCallbackU3Ek__BackingField_11(),
	FindSimilarReq_t671452345::get_offset_of_U3CCollectionIDU3Ek__BackingField_12(),
	FindSimilarReq_t671452345::get_offset_of_U3CImageDataU3Ek__BackingField_13(),
	FindSimilarReq_t671452345::get_offset_of_U3CLimitU3Ek__BackingField_14(),
	FindSimilarReq_t671452345::get_offset_of_U3CDataU3Ek__BackingField_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2932 = { sizeof (CheckServiceStatus_t4150044828), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2932[4] = 
{
	CheckServiceStatus_t4150044828::get_offset_of_m_Service_0(),
	CheckServiceStatus_t4150044828::get_offset_of_m_Callback_1(),
	CheckServiceStatus_t4150044828::get_offset_of_m_GetClassifierCount_2(),
	CheckServiceStatus_t4150044828::get_offset_of_m_ClassifyCount_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2933 = { sizeof (TestAlchemyAPI_t3509857629), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2933[53] = 
{
	TestAlchemyAPI_t3509857629::get_offset_of_m_AlchemyAPI_1(),
	TestAlchemyAPI_t3509857629::get_offset_of_m_GetAuthorsURLTested_2(),
	TestAlchemyAPI_t3509857629::get_offset_of_m_GetAuthorsHTMLTested_3(),
	TestAlchemyAPI_t3509857629::get_offset_of_m_GetRankedConceptsHTMLTested_4(),
	TestAlchemyAPI_t3509857629::get_offset_of_m_GetRankedConceptsURLTested_5(),
	TestAlchemyAPI_t3509857629::get_offset_of_m_GetRankedConceptsTextTested_6(),
	TestAlchemyAPI_t3509857629::get_offset_of_m_GetDatesHTMLTested_7(),
	TestAlchemyAPI_t3509857629::get_offset_of_m_GetDatesURLTested_8(),
	TestAlchemyAPI_t3509857629::get_offset_of_m_GetDatesTextTested_9(),
	TestAlchemyAPI_t3509857629::get_offset_of_m_GetEmotionHTMLTested_10(),
	TestAlchemyAPI_t3509857629::get_offset_of_m_GetEmotionURLTested_11(),
	TestAlchemyAPI_t3509857629::get_offset_of_m_GetEmotionTextTested_12(),
	TestAlchemyAPI_t3509857629::get_offset_of_m_GetEntityExtractionHTMLTested_13(),
	TestAlchemyAPI_t3509857629::get_offset_of_m_GetEntityExtractionURLTested_14(),
	TestAlchemyAPI_t3509857629::get_offset_of_m_GetEntityExtractionTextTested_15(),
	TestAlchemyAPI_t3509857629::get_offset_of_m_DetectFeedsURLTested_16(),
	TestAlchemyAPI_t3509857629::get_offset_of_m_DetectFeedsHTMLTested_17(),
	TestAlchemyAPI_t3509857629::get_offset_of_m_GetKeywordExtractionHTMLTested_18(),
	TestAlchemyAPI_t3509857629::get_offset_of_m_GetKeywordExtractionURLTested_19(),
	TestAlchemyAPI_t3509857629::get_offset_of_m_GetKeywordExtractionTextTested_20(),
	TestAlchemyAPI_t3509857629::get_offset_of_m_GetLanguageHTMLTested_21(),
	TestAlchemyAPI_t3509857629::get_offset_of_m_GetLanguageURLTested_22(),
	TestAlchemyAPI_t3509857629::get_offset_of_m_GetLanguageTextTested_23(),
	TestAlchemyAPI_t3509857629::get_offset_of_m_GetMicroformatURLTested_24(),
	TestAlchemyAPI_t3509857629::get_offset_of_m_GetMicroformatHTMLTested_25(),
	TestAlchemyAPI_t3509857629::get_offset_of_m_GetPubDateURLTested_26(),
	TestAlchemyAPI_t3509857629::get_offset_of_m_GetPubldateHTMLTested_27(),
	TestAlchemyAPI_t3509857629::get_offset_of_m_GetRelationsHTMLTested_28(),
	TestAlchemyAPI_t3509857629::get_offset_of_m_GetRelationsURLTested_29(),
	TestAlchemyAPI_t3509857629::get_offset_of_m_GetRelationsTextTested_30(),
	TestAlchemyAPI_t3509857629::get_offset_of_m_GetTextSentimentHTMLTested_31(),
	TestAlchemyAPI_t3509857629::get_offset_of_m_GetTextSentimentURLTested_32(),
	TestAlchemyAPI_t3509857629::get_offset_of_m_GetTextSentimentTextTested_33(),
	TestAlchemyAPI_t3509857629::get_offset_of_m_GetTargetedSentimentHTMLTested_34(),
	TestAlchemyAPI_t3509857629::get_offset_of_m_GetTargetedSentimentURLTested_35(),
	TestAlchemyAPI_t3509857629::get_offset_of_m_GetTargetedSentimentTextTested_36(),
	TestAlchemyAPI_t3509857629::get_offset_of_m_GetRankedTaxonomyHTMLTested_37(),
	TestAlchemyAPI_t3509857629::get_offset_of_m_GetRankedTaxonomyURLTested_38(),
	TestAlchemyAPI_t3509857629::get_offset_of_m_GetRankedTaxonomyTextTested_39(),
	TestAlchemyAPI_t3509857629::get_offset_of_m_GetTextHTMLTested_40(),
	TestAlchemyAPI_t3509857629::get_offset_of_m_GetTextURLTested_41(),
	TestAlchemyAPI_t3509857629::get_offset_of_m_GetRawTextHTMLTested_42(),
	TestAlchemyAPI_t3509857629::get_offset_of_m_GetRawTextURLTested_43(),
	TestAlchemyAPI_t3509857629::get_offset_of_m_GetTitleHTMLTested_44(),
	TestAlchemyAPI_t3509857629::get_offset_of_m_GetTitleURLTested_45(),
	TestAlchemyAPI_t3509857629::get_offset_of_m_GetCombinedDataHTMLTested_46(),
	TestAlchemyAPI_t3509857629::get_offset_of_m_GetCombinedDataURLTested_47(),
	TestAlchemyAPI_t3509857629::get_offset_of_m_GetCombinedDataTextTested_48(),
	TestAlchemyAPI_t3509857629::get_offset_of_m_GetNewsTested_49(),
	TestAlchemyAPI_t3509857629::get_offset_of_m_ExampleURL_article_50(),
	TestAlchemyAPI_t3509857629::get_offset_of_m_ExampleURL_microformats_51(),
	TestAlchemyAPI_t3509857629::get_offset_of_m_ExampleURL_feed_52(),
	TestAlchemyAPI_t3509857629::get_offset_of_m_ExampleText_article_53(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2934 = { sizeof (U3CRunTestU3Ec__Iterator0_t406948982), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2934[9] = 
{
	U3CRunTestU3Ec__Iterator0_t406948982::get_offset_of_U3Cexample_article_htmlU3E__0_0(),
	U3CRunTestU3Ec__Iterator0_t406948982::get_offset_of_U3Cexample_microformats_htmlU3E__0_1(),
	U3CRunTestU3Ec__Iterator0_t406948982::get_offset_of_U3Cexample_feed_htmlU3E__0_2(),
	U3CRunTestU3Ec__Iterator0_t406948982::get_offset_of_U3CreturnFieldsU3E__0_3(),
	U3CRunTestU3Ec__Iterator0_t406948982::get_offset_of_U3CqueryFieldsU3E__0_4(),
	U3CRunTestU3Ec__Iterator0_t406948982::get_offset_of_U24this_5(),
	U3CRunTestU3Ec__Iterator0_t406948982::get_offset_of_U24current_6(),
	U3CRunTestU3Ec__Iterator0_t406948982::get_offset_of_U24disposing_7(),
	U3CRunTestU3Ec__Iterator0_t406948982::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2935 = { sizeof (TestConversation_t1577552265), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2935[5] = 
{
	TestConversation_t1577552265::get_offset_of_m_Conversation_1(),
	TestConversation_t1577552265::get_offset_of_m_WorkspaceID_2(),
	TestConversation_t1577552265::get_offset_of_m_Input_3(),
	TestConversation_t1577552265::get_offset_of_m_MessageInputTested_4(),
	TestConversation_t1577552265::get_offset_of_m_MessageObjectTested_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2936 = { sizeof (U3CRunTestU3Ec__Iterator0_t3048207354), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2936[5] = 
{
	U3CRunTestU3Ec__Iterator0_t3048207354::get_offset_of_U3CmessageRequestU3E__1_0(),
	U3CRunTestU3Ec__Iterator0_t3048207354::get_offset_of_U24this_1(),
	U3CRunTestU3Ec__Iterator0_t3048207354::get_offset_of_U24current_2(),
	U3CRunTestU3Ec__Iterator0_t3048207354::get_offset_of_U24disposing_3(),
	U3CRunTestU3Ec__Iterator0_t3048207354::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2937 = { sizeof (TestConversationExperimental_t3721563105), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2937[4] = 
{
	TestConversationExperimental_t3721563105::get_offset_of_m_Conversation_0(),
	TestConversationExperimental_t3721563105::get_offset_of_m_WorkspaceID_1(),
	TestConversationExperimental_t3721563105::get_offset_of_m_Input_2(),
	TestConversationExperimental_t3721563105::get_offset_of_m_MessageTested_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2938 = { sizeof (TestDiscovery_t1320997924), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2938[34] = 
{
	TestDiscovery_t1320997924::get_offset_of_m_Discovery_1(),
	TestDiscovery_t1320997924::get_offset_of_m_CreatedEnvironmentName_2(),
	TestDiscovery_t1320997924::get_offset_of_m_CreatedEnvironmentDescription_3(),
	TestDiscovery_t1320997924::get_offset_of_m_CreatedEnvironmentID_4(),
	TestDiscovery_t1320997924::get_offset_of_m_IsEnvironmentActive_5(),
	TestDiscovery_t1320997924::get_offset_of_m_ConfigurationJsonPath_6(),
	TestDiscovery_t1320997924::get_offset_of_m_CreatedConfigurationID_7(),
	TestDiscovery_t1320997924::get_offset_of_m_CreatedCollectionName_8(),
	TestDiscovery_t1320997924::get_offset_of_m_CreatedCollectionDescription_9(),
	TestDiscovery_t1320997924::get_offset_of_m_CreatedCollectionID_10(),
	TestDiscovery_t1320997924::get_offset_of_m_FilePathToIngest_11(),
	TestDiscovery_t1320997924::get_offset_of_m_DocumentFilePath_12(),
	TestDiscovery_t1320997924::get_offset_of_m_Metadata_13(),
	TestDiscovery_t1320997924::get_offset_of_m_CreatedDocumentID_14(),
	TestDiscovery_t1320997924::get_offset_of_m_Query_15(),
	TestDiscovery_t1320997924::get_offset_of_m_GetEnvironmentsTested_16(),
	TestDiscovery_t1320997924::get_offset_of_m_AddEnvironmentsTested_17(),
	TestDiscovery_t1320997924::get_offset_of_m_GetEnvironmentTested_18(),
	TestDiscovery_t1320997924::get_offset_of_m_GetConfigurationsTested_19(),
	TestDiscovery_t1320997924::get_offset_of_m_AddConfigurationTested_20(),
	TestDiscovery_t1320997924::get_offset_of_m_GetConfigurationTested_21(),
	TestDiscovery_t1320997924::get_offset_of_m_PreviewConfigurationTested_22(),
	TestDiscovery_t1320997924::get_offset_of_m_GetCollectionsTested_23(),
	TestDiscovery_t1320997924::get_offset_of_m_AddCollectionTested_24(),
	TestDiscovery_t1320997924::get_offset_of_m_GetCollectionTested_25(),
	TestDiscovery_t1320997924::get_offset_of_m_GetFieldsTested_26(),
	TestDiscovery_t1320997924::get_offset_of_m_AddDocumentTested_27(),
	TestDiscovery_t1320997924::get_offset_of_m_GetDocumentTested_28(),
	TestDiscovery_t1320997924::get_offset_of_m_UpdateDocumentTested_29(),
	TestDiscovery_t1320997924::get_offset_of_m_QueryTested_30(),
	TestDiscovery_t1320997924::get_offset_of_m_DeleteDocumentTested_31(),
	TestDiscovery_t1320997924::get_offset_of_m_DeleteCollectionTested_32(),
	TestDiscovery_t1320997924::get_offset_of_m_DeleteConfigurationTested_33(),
	TestDiscovery_t1320997924::get_offset_of_m_DeleteEnvironmentTested_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2939 = { sizeof (U3CRunTestU3Ec__Iterator0_t1695959971), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2939[4] = 
{
	U3CRunTestU3Ec__Iterator0_t1695959971::get_offset_of_U24this_0(),
	U3CRunTestU3Ec__Iterator0_t1695959971::get_offset_of_U24current_1(),
	U3CRunTestU3Ec__Iterator0_t1695959971::get_offset_of_U24disposing_2(),
	U3CRunTestU3Ec__Iterator0_t1695959971::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2940 = { sizeof (U3CDeleteDocumentU3Ec__Iterator1_t1606296947), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2940[4] = 
{
	U3CDeleteDocumentU3Ec__Iterator1_t1606296947::get_offset_of_U24this_0(),
	U3CDeleteDocumentU3Ec__Iterator1_t1606296947::get_offset_of_U24current_1(),
	U3CDeleteDocumentU3Ec__Iterator1_t1606296947::get_offset_of_U24disposing_2(),
	U3CDeleteDocumentU3Ec__Iterator1_t1606296947::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2941 = { sizeof (U3CDeleteCollectionU3Ec__Iterator2_t3345212421), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2941[4] = 
{
	U3CDeleteCollectionU3Ec__Iterator2_t3345212421::get_offset_of_U24this_0(),
	U3CDeleteCollectionU3Ec__Iterator2_t3345212421::get_offset_of_U24current_1(),
	U3CDeleteCollectionU3Ec__Iterator2_t3345212421::get_offset_of_U24disposing_2(),
	U3CDeleteCollectionU3Ec__Iterator2_t3345212421::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2942 = { sizeof (U3CDeleteConfigurationU3Ec__Iterator3_t783559642), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2942[4] = 
{
	U3CDeleteConfigurationU3Ec__Iterator3_t783559642::get_offset_of_U24this_0(),
	U3CDeleteConfigurationU3Ec__Iterator3_t783559642::get_offset_of_U24current_1(),
	U3CDeleteConfigurationU3Ec__Iterator3_t783559642::get_offset_of_U24disposing_2(),
	U3CDeleteConfigurationU3Ec__Iterator3_t783559642::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2943 = { sizeof (U3CDeleteEnvironmentU3Ec__Iterator4_t645273570), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2943[4] = 
{
	U3CDeleteEnvironmentU3Ec__Iterator4_t645273570::get_offset_of_U24this_0(),
	U3CDeleteEnvironmentU3Ec__Iterator4_t645273570::get_offset_of_U24current_1(),
	U3CDeleteEnvironmentU3Ec__Iterator4_t645273570::get_offset_of_U24disposing_2(),
	U3CDeleteEnvironmentU3Ec__Iterator4_t645273570::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2944 = { sizeof (U3CCheckEnvironmentStatusU3Ec__Iterator5_t3997571362), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2944[4] = 
{
	U3CCheckEnvironmentStatusU3Ec__Iterator5_t3997571362::get_offset_of_U24this_0(),
	U3CCheckEnvironmentStatusU3Ec__Iterator5_t3997571362::get_offset_of_U24current_1(),
	U3CCheckEnvironmentStatusU3Ec__Iterator5_t3997571362::get_offset_of_U24disposing_2(),
	U3CCheckEnvironmentStatusU3Ec__Iterator5_t3997571362::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2945 = { sizeof (TestDocumentConversion_t3132176613), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2945[4] = 
{
	TestDocumentConversion_t3132176613::get_offset_of_m_DocumentConversion_1(),
	TestDocumentConversion_t3132176613::get_offset_of_m_DocumentConversionAnswerUnitsTested_2(),
	TestDocumentConversion_t3132176613::get_offset_of_m_DocumentConversionTextTested_3(),
	TestDocumentConversion_t3132176613::get_offset_of_m_DocumentConversionHTMLTested_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2946 = { sizeof (U3CRunTestU3Ec__Iterator0_t2987565082), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2946[5] = 
{
	U3CRunTestU3Ec__Iterator0_t2987565082::get_offset_of_U3CexamplePathU3E__0_0(),
	U3CRunTestU3Ec__Iterator0_t2987565082::get_offset_of_U24this_1(),
	U3CRunTestU3Ec__Iterator0_t2987565082::get_offset_of_U24current_2(),
	U3CRunTestU3Ec__Iterator0_t2987565082::get_offset_of_U24disposing_3(),
	U3CRunTestU3Ec__Iterator0_t2987565082::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2947 = { sizeof (TestEventManager_t1784053145), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2947[3] = 
{
	TestEventManager_t1784053145::get_offset_of_m_Manager_1(),
	TestEventManager_t1784053145::get_offset_of_m_SendTested_2(),
	TestEventManager_t1784053145::get_offset_of_m_SendAsyncTested_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2948 = { sizeof (U3CRunTestU3Ec__Iterator0_t2238499560), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2948[4] = 
{
	U3CRunTestU3Ec__Iterator0_t2238499560::get_offset_of_U24this_0(),
	U3CRunTestU3Ec__Iterator0_t2238499560::get_offset_of_U24current_1(),
	U3CRunTestU3Ec__Iterator0_t2238499560::get_offset_of_U24disposing_2(),
	U3CRunTestU3Ec__Iterator0_t2238499560::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2949 = { sizeof (TestLanguageTranslation_t1772800819), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2949[6] = 
{
	TestLanguageTranslation_t1772800819::get_offset_of_m_Translate_1(),
	TestLanguageTranslation_t1772800819::get_offset_of_m_GetModelTested_2(),
	TestLanguageTranslation_t1772800819::get_offset_of_m_GetModelsTested_3(),
	TestLanguageTranslation_t1772800819::get_offset_of_m_GetLanguagesTested_4(),
	TestLanguageTranslation_t1772800819::get_offset_of_m_IdentifyTested_5(),
	TestLanguageTranslation_t1772800819::get_offset_of_m_TranslateTested_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2950 = { sizeof (U3CRunTestU3Ec__Iterator0_t1152436222), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2950[4] = 
{
	U3CRunTestU3Ec__Iterator0_t1152436222::get_offset_of_U24this_0(),
	U3CRunTestU3Ec__Iterator0_t1152436222::get_offset_of_U24current_1(),
	U3CRunTestU3Ec__Iterator0_t1152436222::get_offset_of_U24disposing_2(),
	U3CRunTestU3Ec__Iterator0_t1152436222::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2951 = { sizeof (TestLanguageTranslator_t3684829362), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2951[6] = 
{
	TestLanguageTranslator_t3684829362::get_offset_of_m_Translate_0(),
	TestLanguageTranslator_t3684829362::get_offset_of_m_GetModelTested_1(),
	TestLanguageTranslator_t3684829362::get_offset_of_m_GetModelsTested_2(),
	TestLanguageTranslator_t3684829362::get_offset_of_m_GetLanguagesTested_3(),
	TestLanguageTranslator_t3684829362::get_offset_of_m_IdentifyTested_4(),
	TestLanguageTranslator_t3684829362::get_offset_of_m_TranslateTested_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2952 = { sizeof (TestNaturalLanguageClassifier_t1226850908), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2952[6] = 
{
	TestNaturalLanguageClassifier_t1226850908::get_offset_of_m_NaturalLanguageClassifier_1(),
	TestNaturalLanguageClassifier_t1226850908::get_offset_of_m_FindClassifierTested_2(),
	TestNaturalLanguageClassifier_t1226850908::get_offset_of_m_TrainClasifierTested_3(),
	TestNaturalLanguageClassifier_t1226850908::get_offset_of_m_TrainClassifier_4(),
	TestNaturalLanguageClassifier_t1226850908::get_offset_of_m_ClassifierId_5(),
	TestNaturalLanguageClassifier_t1226850908::get_offset_of_m_ClassifyTested_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2953 = { sizeof (U3CRunTestU3Ec__Iterator0_t980178097), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2953[5] = 
{
	U3CRunTestU3Ec__Iterator0_t980178097::get_offset_of_U3CtrainingDataU3E__1_0(),
	U3CRunTestU3Ec__Iterator0_t980178097::get_offset_of_U24this_1(),
	U3CRunTestU3Ec__Iterator0_t980178097::get_offset_of_U24current_2(),
	U3CRunTestU3Ec__Iterator0_t980178097::get_offset_of_U24disposing_3(),
	U3CRunTestU3Ec__Iterator0_t980178097::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2954 = { sizeof (TestPersonalityInsightsV2_t1756789131), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2954[4] = 
{
	TestPersonalityInsightsV2_t1756789131::get_offset_of_m_personalityInsights_1(),
	TestPersonalityInsightsV2_t1756789131::get_offset_of_testString_2(),
	TestPersonalityInsightsV2_t1756789131::get_offset_of_m_GetProfileTextTested_3(),
	TestPersonalityInsightsV2_t1756789131::get_offset_of_m_GetProfileJsonTested_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2955 = { sizeof (U3CRunTestU3Ec__Iterator0_t614343650), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2955[5] = 
{
	U3CRunTestU3Ec__Iterator0_t614343650::get_offset_of_U3CdataPathU3E__0_0(),
	U3CRunTestU3Ec__Iterator0_t614343650::get_offset_of_U24this_1(),
	U3CRunTestU3Ec__Iterator0_t614343650::get_offset_of_U24current_2(),
	U3CRunTestU3Ec__Iterator0_t614343650::get_offset_of_U24disposing_3(),
	U3CRunTestU3Ec__Iterator0_t614343650::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2956 = { sizeof (TestPersonalityInsightsV3_t1756789132), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2956[4] = 
{
	TestPersonalityInsightsV3_t1756789132::get_offset_of_m_personalityInsights_1(),
	TestPersonalityInsightsV3_t1756789132::get_offset_of_testString_2(),
	TestPersonalityInsightsV3_t1756789132::get_offset_of_m_GetProfileTextTested_3(),
	TestPersonalityInsightsV3_t1756789132::get_offset_of_m_GetProfileJsonTested_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2957 = { sizeof (U3CRunTestU3Ec__Iterator0_t1192622081), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2957[5] = 
{
	U3CRunTestU3Ec__Iterator0_t1192622081::get_offset_of_U3CdataPathU3E__0_0(),
	U3CRunTestU3Ec__Iterator0_t1192622081::get_offset_of_U24this_1(),
	U3CRunTestU3Ec__Iterator0_t1192622081::get_offset_of_U24current_2(),
	U3CRunTestU3Ec__Iterator0_t1192622081::get_offset_of_U24disposing_3(),
	U3CRunTestU3Ec__Iterator0_t1192622081::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2958 = { sizeof (TestRetrieveAndRank_t2134779753), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2958[28] = 
{
	TestRetrieveAndRank_t2134779753::get_offset_of_m_RetrieveAndRank_1(),
	TestRetrieveAndRank_t2134779753::get_offset_of_IsFullTest_2(),
	TestRetrieveAndRank_t2134779753::get_offset_of_m_GetClustersTested_3(),
	TestRetrieveAndRank_t2134779753::get_offset_of_m_GetClusterTested_4(),
	TestRetrieveAndRank_t2134779753::get_offset_of_m_ListClusterConfigsTested_5(),
	TestRetrieveAndRank_t2134779753::get_offset_of_m_GetClusterConfigTested_6(),
	TestRetrieveAndRank_t2134779753::get_offset_of_m_ListCollectionRequestTested_7(),
	TestRetrieveAndRank_t2134779753::get_offset_of_m_StandardSearchTested_8(),
	TestRetrieveAndRank_t2134779753::get_offset_of_m_RankedSearchTested_9(),
	TestRetrieveAndRank_t2134779753::get_offset_of_m_GetRankersTested_10(),
	TestRetrieveAndRank_t2134779753::get_offset_of_m_RankTested_11(),
	TestRetrieveAndRank_t2134779753::get_offset_of_m_GetRankerInfoTested_12(),
	TestRetrieveAndRank_t2134779753::get_offset_of_m_ConfigToCreateName_13(),
	TestRetrieveAndRank_t2134779753::get_offset_of_m_CollectionToCreateName_14(),
	TestRetrieveAndRank_t2134779753::get_offset_of_m_CreatedRankerID_15(),
	TestRetrieveAndRank_t2134779753::get_offset_of_m_CreatedClusterID_16(),
	TestRetrieveAndRank_t2134779753::get_offset_of_m_ExampleClusterID_17(),
	TestRetrieveAndRank_t2134779753::get_offset_of_m_ExampleConfigName_18(),
	TestRetrieveAndRank_t2134779753::get_offset_of_m_ExampleRankerID_19(),
	TestRetrieveAndRank_t2134779753::get_offset_of_m_ExampleCollectionName_20(),
	TestRetrieveAndRank_t2134779753::get_offset_of_m_IntegrationTestQuery_21(),
	TestRetrieveAndRank_t2134779753::get_offset_of_m_Fl_22(),
	TestRetrieveAndRank_t2134779753::get_offset_of_m_IntegrationTestClusterConfigPath_23(),
	TestRetrieveAndRank_t2134779753::get_offset_of_m_IntegrationTestRankerTrainingPath_24(),
	TestRetrieveAndRank_t2134779753::get_offset_of_m_IntegrationTestRankerAnswerDataPath_25(),
	TestRetrieveAndRank_t2134779753::get_offset_of_m_IntegrationTestIndexDataPath_26(),
	TestRetrieveAndRank_t2134779753::get_offset_of_m_IsClusterReady_27(),
	TestRetrieveAndRank_t2134779753::get_offset_of_m_IsRankerReady_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2959 = { sizeof (U3CRunTestU3Ec__Iterator0_t480647684), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2959[4] = 
{
	U3CRunTestU3Ec__Iterator0_t480647684::get_offset_of_U24this_0(),
	U3CRunTestU3Ec__Iterator0_t480647684::get_offset_of_U24current_1(),
	U3CRunTestU3Ec__Iterator0_t480647684::get_offset_of_U24disposing_2(),
	U3CRunTestU3Ec__Iterator0_t480647684::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2960 = { sizeof (TestRunnable_t2118330963), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2960[1] = 
{
	TestRunnable_t2118330963::get_offset_of_m_CoroutineRan_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2961 = { sizeof (U3CRunTestU3Ec__Iterator0_t2458294274), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2961[4] = 
{
	U3CRunTestU3Ec__Iterator0_t2458294274::get_offset_of_U24this_0(),
	U3CRunTestU3Ec__Iterator0_t2458294274::get_offset_of_U24current_1(),
	U3CRunTestU3Ec__Iterator0_t2458294274::get_offset_of_U24disposing_2(),
	U3CRunTestU3Ec__Iterator0_t2458294274::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2962 = { sizeof (U3CTestCoroutineU3Ec__Iterator1_t1222338220), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2962[5] = 
{
	U3CTestCoroutineU3Ec__Iterator1_t1222338220::get_offset_of_a_Argument_0(),
	U3CTestCoroutineU3Ec__Iterator1_t1222338220::get_offset_of_U24this_1(),
	U3CTestCoroutineU3Ec__Iterator1_t1222338220::get_offset_of_U24current_2(),
	U3CTestCoroutineU3Ec__Iterator1_t1222338220::get_offset_of_U24disposing_3(),
	U3CTestCoroutineU3Ec__Iterator1_t1222338220::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2963 = { sizeof (TestSpeechToText_t3601723574), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2963[26] = 
{
	TestSpeechToText_t3601723574::get_offset_of_m_SpeechToText_1(),
	TestSpeechToText_t3601723574::get_offset_of_m_GetModelsTested_2(),
	TestSpeechToText_t3601723574::get_offset_of_m_GetModelTested_3(),
	TestSpeechToText_t3601723574::get_offset_of_m_GetCustomizationsTested_4(),
	TestSpeechToText_t3601723574::get_offset_of_m_CreateCustomizationTested_5(),
	TestSpeechToText_t3601723574::get_offset_of_m_GetCustomizationTested_6(),
	TestSpeechToText_t3601723574::get_offset_of_m_GetCustomCorporaTested_7(),
	TestSpeechToText_t3601723574::get_offset_of_m_AddCustomCorpusTested_8(),
	TestSpeechToText_t3601723574::get_offset_of_m_GetCustomWordsTested_9(),
	TestSpeechToText_t3601723574::get_offset_of_m_AddCustomWordsUsingFileTested_10(),
	TestSpeechToText_t3601723574::get_offset_of_m_AddCustomWordsUsingObjectTested_11(),
	TestSpeechToText_t3601723574::get_offset_of_m_GetCustomWordTested_12(),
	TestSpeechToText_t3601723574::get_offset_of_m_TrainCustomizationTested_13(),
	TestSpeechToText_t3601723574::get_offset_of_m_DeleteCustomCorpusTested_14(),
	TestSpeechToText_t3601723574::get_offset_of_m_DeleteCustomWordTested_15(),
	TestSpeechToText_t3601723574::get_offset_of_m_ResetCustomizationTested_16(),
	TestSpeechToText_t3601723574::get_offset_of_m_DeleteCustomizationTested_17(),
	TestSpeechToText_t3601723574::get_offset_of_m_CreatedCustomizationID_18(),
	TestSpeechToText_t3601723574::get_offset_of_m_CreatedCustomizationName_19(),
	TestSpeechToText_t3601723574::get_offset_of_m_CreatedCorpusName_20(),
	TestSpeechToText_t3601723574::get_offset_of_m_CustomCorpusFilePath_21(),
	TestSpeechToText_t3601723574::get_offset_of_m_SpeechToTextModelEnglish_22(),
	TestSpeechToText_t3601723574::get_offset_of_m_CustomWordsFilePath_23(),
	TestSpeechToText_t3601723574::get_offset_of_m_AllowOverwrite_24(),
	TestSpeechToText_t3601723574::get_offset_of_m_WordToGet_25(),
	TestSpeechToText_t3601723574::get_offset_of_m_IsCustomizationBusy_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2964 = { sizeof (U3CRunTestU3Ec__Iterator0_t1033965803), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2964[9] = 
{
	U3CRunTestU3Ec__Iterator0_t1033965803::get_offset_of_U3CwordsU3E__0_0(),
	U3CRunTestU3Ec__Iterator0_t1033965803::get_offset_of_U3Cw0U3E__0_1(),
	U3CRunTestU3Ec__Iterator0_t1033965803::get_offset_of_U3CwordListU3E__0_2(),
	U3CRunTestU3Ec__Iterator0_t1033965803::get_offset_of_U3Cw1U3E__0_3(),
	U3CRunTestU3Ec__Iterator0_t1033965803::get_offset_of_U3Cw2U3E__0_4(),
	U3CRunTestU3Ec__Iterator0_t1033965803::get_offset_of_U24this_5(),
	U3CRunTestU3Ec__Iterator0_t1033965803::get_offset_of_U24current_6(),
	U3CRunTestU3Ec__Iterator0_t1033965803::get_offset_of_U24disposing_7(),
	U3CRunTestU3Ec__Iterator0_t1033965803::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2965 = { sizeof (U3CCheckCustomizationStatusU3Ec__Iterator1_t3285635976), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2965[6] = 
{
	U3CCheckCustomizationStatusU3Ec__Iterator1_t3285635976::get_offset_of_delay_0(),
	U3CCheckCustomizationStatusU3Ec__Iterator1_t3285635976::get_offset_of_customizationID_1(),
	U3CCheckCustomizationStatusU3Ec__Iterator1_t3285635976::get_offset_of_U24this_2(),
	U3CCheckCustomizationStatusU3Ec__Iterator1_t3285635976::get_offset_of_U24current_3(),
	U3CCheckCustomizationStatusU3Ec__Iterator1_t3285635976::get_offset_of_U24disposing_4(),
	U3CCheckCustomizationStatusU3Ec__Iterator1_t3285635976::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2966 = { sizeof (TestTextToSpeech_t3466033430), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2966[26] = 
{
	TestTextToSpeech_t3466033430::get_offset_of_m_TextToSpeech_1(),
	TestTextToSpeech_t3466033430::get_offset_of_m_GetTested_2(),
	TestTextToSpeech_t3466033430::get_offset_of_m_PostTested_3(),
	TestTextToSpeech_t3466033430::get_offset_of_m_GetVoicesTested_4(),
	TestTextToSpeech_t3466033430::get_offset_of_m_GetVoiceTested_5(),
	TestTextToSpeech_t3466033430::get_offset_of_m_GetPronunciationTested_6(),
	TestTextToSpeech_t3466033430::get_offset_of_m_GetCustomizationsTested_7(),
	TestTextToSpeech_t3466033430::get_offset_of_m_CreateCustomizationTested_8(),
	TestTextToSpeech_t3466033430::get_offset_of_m_DeleteCustomizationTested_9(),
	TestTextToSpeech_t3466033430::get_offset_of_m_GetCustomizationTested_10(),
	TestTextToSpeech_t3466033430::get_offset_of_m_UpdateCustomizationTested_11(),
	TestTextToSpeech_t3466033430::get_offset_of_m_GetCustomizationWordsTested_12(),
	TestTextToSpeech_t3466033430::get_offset_of_m_AddCustomizationWordsTested_13(),
	TestTextToSpeech_t3466033430::get_offset_of_m_GetCustomizationWordTested_14(),
	TestTextToSpeech_t3466033430::get_offset_of_m_DeleteCustomizationWordTested_15(),
	TestTextToSpeech_t3466033430::get_offset_of_m_CustomizationIDToTest_16(),
	TestTextToSpeech_t3466033430::get_offset_of_m_CustomizationToCreateName_17(),
	TestTextToSpeech_t3466033430::get_offset_of_m_CustomizationToCreateLanguage_18(),
	TestTextToSpeech_t3466033430::get_offset_of_m_CustomizationToCreateDescription_19(),
	TestTextToSpeech_t3466033430::get_offset_of_m_UpdateWord0_20(),
	TestTextToSpeech_t3466033430::get_offset_of_m_UpdateWord1_21(),
	TestTextToSpeech_t3466033430::get_offset_of_m_UpdateTranslation0_22(),
	TestTextToSpeech_t3466033430::get_offset_of_m_UpdateTranslation1_23(),
	TestTextToSpeech_t3466033430::get_offset_of_m_UpdateWordObject0_24(),
	TestTextToSpeech_t3466033430::get_offset_of_m_UpdateWordObject1_25(),
	TestTextToSpeech_t3466033430::get_offset_of_m_CustomizationIdCreated_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2967 = { sizeof (U3CRunTestU3Ec__Iterator0_t2943339915), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2967[9] = 
{
	U3CRunTestU3Ec__Iterator0_t2943339915::get_offset_of_U3CtestWordU3E__0_0(),
	U3CRunTestU3Ec__Iterator0_t2943339915::get_offset_of_U3CwordsU3E__0_1(),
	U3CRunTestU3Ec__Iterator0_t2943339915::get_offset_of_U3CcustomVoiceUpdateU3E__0_2(),
	U3CRunTestU3Ec__Iterator0_t2943339915::get_offset_of_U3CwordArrayU3E__0_3(),
	U3CRunTestU3Ec__Iterator0_t2943339915::get_offset_of_U3CwordsObjectU3E__0_4(),
	U3CRunTestU3Ec__Iterator0_t2943339915::get_offset_of_U24this_5(),
	U3CRunTestU3Ec__Iterator0_t2943339915::get_offset_of_U24current_6(),
	U3CRunTestU3Ec__Iterator0_t2943339915::get_offset_of_U24disposing_7(),
	U3CRunTestU3Ec__Iterator0_t2943339915::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2968 = { sizeof (TestToneAnalyzer_t1700654784), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2968[3] = 
{
	TestToneAnalyzer_t1700654784::get_offset_of_m_ToneAnalyzer_1(),
	TestToneAnalyzer_t1700654784::get_offset_of_m_GetToneAnalyzerTested_2(),
	TestToneAnalyzer_t1700654784::get_offset_of_m_StringToTestTone_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2969 = { sizeof (U3CRunTestU3Ec__Iterator0_t464230963), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2969[4] = 
{
	U3CRunTestU3Ec__Iterator0_t464230963::get_offset_of_U24this_0(),
	U3CRunTestU3Ec__Iterator0_t464230963::get_offset_of_U24current_1(),
	U3CRunTestU3Ec__Iterator0_t464230963::get_offset_of_U24disposing_2(),
	U3CRunTestU3Ec__Iterator0_t464230963::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2970 = { sizeof (TestTradeoffAnalytics_t1911417121), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2970[2] = 
{
	TestTradeoffAnalytics_t1911417121::get_offset_of_m_TradeoffAnalytics_1(),
	TestTradeoffAnalytics_t1911417121::get_offset_of_m_GetDilemmaTested_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2971 = { sizeof (TestDataValue_t312670927), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2971[3] = 
{
	TestDataValue_t312670927::get_offset_of_U3CpriceU3Ek__BackingField_0(),
	TestDataValue_t312670927::get_offset_of_U3CweightU3Ek__BackingField_1(),
	TestDataValue_t312670927::get_offset_of_U3CbrandU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2972 = { sizeof (TestData_t435165324), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2973 = { sizeof (U3CRunTestU3Ec__Iterator0_t161392710), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2973[13] = 
{
	U3CRunTestU3Ec__Iterator0_t161392710::get_offset_of_U3CproblemToSolveU3E__0_0(),
	U3CRunTestU3Ec__Iterator0_t161392710::get_offset_of_U3ClistColumnU3E__0_1(),
	U3CRunTestU3Ec__Iterator0_t161392710::get_offset_of_U3CcolumnPriceU3E__0_2(),
	U3CRunTestU3Ec__Iterator0_t161392710::get_offset_of_U3CcolumnWeightU3E__0_3(),
	U3CRunTestU3Ec__Iterator0_t161392710::get_offset_of_U3CcolumnBrandNameU3E__0_4(),
	U3CRunTestU3Ec__Iterator0_t161392710::get_offset_of_U3ClistOptionU3E__0_5(),
	U3CRunTestU3Ec__Iterator0_t161392710::get_offset_of_U3Coption1U3E__0_6(),
	U3CRunTestU3Ec__Iterator0_t161392710::get_offset_of_U3Coption2U3E__0_7(),
	U3CRunTestU3Ec__Iterator0_t161392710::get_offset_of_U3Coption3U3E__0_8(),
	U3CRunTestU3Ec__Iterator0_t161392710::get_offset_of_U24this_9(),
	U3CRunTestU3Ec__Iterator0_t161392710::get_offset_of_U24current_10(),
	U3CRunTestU3Ec__Iterator0_t161392710::get_offset_of_U24disposing_11(),
	U3CRunTestU3Ec__Iterator0_t161392710::get_offset_of_U24PC_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2974 = { sizeof (TestVisualRecognition_t3349626417), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2974[37] = 
{
	TestVisualRecognition_t3349626417::get_offset_of_m_VisualRecognition_1(),
	TestVisualRecognition_t3349626417::get_offset_of_m_TrainClasifierTested_2(),
	TestVisualRecognition_t3349626417::get_offset_of_m_GetClassifiersTested_3(),
	TestVisualRecognition_t3349626417::get_offset_of_m_FindClassifierTested_4(),
	TestVisualRecognition_t3349626417::get_offset_of_m_GetClassifierTested_5(),
	TestVisualRecognition_t3349626417::get_offset_of_m_UpdateClassifierTested_6(),
	TestVisualRecognition_t3349626417::get_offset_of_m_ClassifyGETTested_7(),
	TestVisualRecognition_t3349626417::get_offset_of_m_ClassifyPOSTTested_8(),
	TestVisualRecognition_t3349626417::get_offset_of_m_DetectFacesGETTested_9(),
	TestVisualRecognition_t3349626417::get_offset_of_m_DetectFacesPOSTTested_10(),
	TestVisualRecognition_t3349626417::get_offset_of_m_RecognizeTextGETTested_11(),
	TestVisualRecognition_t3349626417::get_offset_of_m_RecognizeTextPOSTTested_12(),
	TestVisualRecognition_t3349626417::get_offset_of_m_DeleteClassifierTested_13(),
	TestVisualRecognition_t3349626417::get_offset_of_m_ListCollectionsTested_14(),
	TestVisualRecognition_t3349626417::get_offset_of_m_CreateCollectionTested_15(),
	TestVisualRecognition_t3349626417::get_offset_of_m_DeleteCollectionTested_16(),
	TestVisualRecognition_t3349626417::get_offset_of_m_RetrieveCollectionDetailsTested_17(),
	TestVisualRecognition_t3349626417::get_offset_of_m_ListImagesTested_18(),
	TestVisualRecognition_t3349626417::get_offset_of_m_AddImagesToCollectionTested_19(),
	TestVisualRecognition_t3349626417::get_offset_of_m_DeleteImageFromCollectionTested_20(),
	TestVisualRecognition_t3349626417::get_offset_of_m_ListImageDetailsTested_21(),
	TestVisualRecognition_t3349626417::get_offset_of_m_DeleteImageMetadataTested_22(),
	TestVisualRecognition_t3349626417::get_offset_of_m_ListImageMetadataTested_23(),
	TestVisualRecognition_t3349626417::get_offset_of_m_FindSimilarTested_24(),
	TestVisualRecognition_t3349626417::get_offset_of_m_TrainClassifier_25(),
	TestVisualRecognition_t3349626417::get_offset_of_m_IsClassifierReady_26(),
	TestVisualRecognition_t3349626417::get_offset_of_m_HasUpdatedClassifier_27(),
	TestVisualRecognition_t3349626417::get_offset_of_m_IsUpdatedClassifierReady_28(),
	TestVisualRecognition_t3349626417::get_offset_of_m_ClassifierId_29(),
	TestVisualRecognition_t3349626417::get_offset_of_m_ClassifierName_30(),
	TestVisualRecognition_t3349626417::get_offset_of_m_ClassName_Giraffe_31(),
	TestVisualRecognition_t3349626417::get_offset_of_m_ClassName_Turtle_32(),
	TestVisualRecognition_t3349626417::get_offset_of_m_ImageURL_33(),
	TestVisualRecognition_t3349626417::get_offset_of_m_ImageFaceURL_34(),
	TestVisualRecognition_t3349626417::get_offset_of_m_ImageTextURL_35(),
	TestVisualRecognition_t3349626417::get_offset_of_m_CreatedCollectionID_36(),
	TestVisualRecognition_t3349626417::get_offset_of_m_CreatedCollectionImage_37(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2975 = { sizeof (U3CRunTestU3Ec__Iterator0_t270095488), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2975[16] = 
{
	U3CRunTestU3Ec__Iterator0_t270095488::get_offset_of_U3Cm_positiveExamplesPathU3E__1_0(),
	U3CRunTestU3Ec__Iterator0_t270095488::get_offset_of_U3Cm_negativeExamplesPathU3E__1_1(),
	U3CRunTestU3Ec__Iterator0_t270095488::get_offset_of_U3CpositiveExamplesU3E__1_2(),
	U3CRunTestU3Ec__Iterator0_t270095488::get_offset_of_U3Cm_positiveUpdatedU3E__2_3(),
	U3CRunTestU3Ec__Iterator0_t270095488::get_offset_of_U3CpositiveUpdatedExamplesU3E__2_4(),
	U3CRunTestU3Ec__Iterator0_t270095488::get_offset_of_U3Cm_ownersU3E__2_5(),
	U3CRunTestU3Ec__Iterator0_t270095488::get_offset_of_U3Cm_classifierIdsU3E__2_6(),
	U3CRunTestU3Ec__Iterator0_t270095488::get_offset_of_U3Cm_classifyImagePathU3E__2_7(),
	U3CRunTestU3Ec__Iterator0_t270095488::get_offset_of_U3Cm_detectFaceImagePathU3E__0_8(),
	U3CRunTestU3Ec__Iterator0_t270095488::get_offset_of_U3Cm_recognizeTextImagePathU3E__0_9(),
	U3CRunTestU3Ec__Iterator0_t270095488::get_offset_of_U3Cm_collectionImagePathU3E__0_10(),
	U3CRunTestU3Ec__Iterator0_t270095488::get_offset_of_U3CimageMetadataU3E__0_11(),
	U3CRunTestU3Ec__Iterator0_t270095488::get_offset_of_U24this_12(),
	U3CRunTestU3Ec__Iterator0_t270095488::get_offset_of_U24current_13(),
	U3CRunTestU3Ec__Iterator0_t270095488::get_offset_of_U24disposing_14(),
	U3CRunTestU3Ec__Iterator0_t270095488::get_offset_of_U24PC_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2976 = { sizeof (UnitTest_t237671048), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2976[1] = 
{
	UnitTest_t237671048::get_offset_of_U3CTestFailedU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2977 = { sizeof (UnitTestManager_t2486317649), -1, sizeof(UnitTestManager_t2486317649_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2977[10] = 
{
	0,
	UnitTestManager_t2486317649::get_offset_of_U3CTestsFailedU3Ek__BackingField_3(),
	UnitTestManager_t2486317649::get_offset_of_U3CTestsCompleteU3Ek__BackingField_4(),
	UnitTestManager_t2486317649::get_offset_of_U3CQuitOnTestsCompleteU3Ek__BackingField_5(),
	UnitTestManager_t2486317649::get_offset_of_U3COnTestCompleteCallbackU3Ek__BackingField_6(),
	UnitTestManager_t2486317649::get_offset_of_m_ProjectNameToTest_7(),
	UnitTestManager_t2486317649_StaticFields::get_offset_of_ProjectToTest_8(),
	UnitTestManager_t2486317649::get_offset_of_m_QueuedTests_9(),
	UnitTestManager_t2486317649::get_offset_of_m_TestsAvailable_10(),
	UnitTestManager_t2486317649::get_offset_of_m_ActiveTest_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2978 = { sizeof (OnTestsComplete_t1412473303), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2979 = { sizeof (U3CRunTestsCRU3Ec__Iterator0_t2904975748), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2979[8] = 
{
	U3CRunTestsCRU3Ec__Iterator0_t2904975748::get_offset_of_U3CtestTypeU3E__1_0(),
	U3CRunTestsCRU3Ec__Iterator0_t2904975748::get_offset_of_U3CbTestExceptionU3E__2_1(),
	U3CRunTestsCRU3Ec__Iterator0_t2904975748::get_offset_of_U3CstartTimeU3E__2_2(),
	U3CRunTestsCRU3Ec__Iterator0_t2904975748::get_offset_of_U3CeU3E__3_3(),
	U3CRunTestsCRU3Ec__Iterator0_t2904975748::get_offset_of_U24this_4(),
	U3CRunTestsCRU3Ec__Iterator0_t2904975748::get_offset_of_U24current_5(),
	U3CRunTestsCRU3Ec__Iterator0_t2904975748::get_offset_of_U24disposing_6(),
	U3CRunTestsCRU3Ec__Iterator0_t2904975748::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2980 = { sizeof (RunUnitTest_t690597011), -1, sizeof(RunUnitTest_t690597011_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2980[1] = 
{
	RunUnitTest_t690597011_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2981 = { sizeof (AudioClipUtil_t2269251034), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2982 = { sizeof (Config_t3637807320), -1, sizeof(Config_t3637807320_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2982[7] = 
{
	Config_t3637807320::get_offset_of_m_ClassifierDirectory_0(),
	Config_t3637807320::get_offset_of_m_TimeOut_1(),
	Config_t3637807320::get_offset_of_m_MaxRestConnections_2(),
	Config_t3637807320::get_offset_of_m_Credentials_3(),
	Config_t3637807320::get_offset_of_m_Variables_4(),
	Config_t3637807320_StaticFields::get_offset_of_sm_Serializer_5(),
	Config_t3637807320::get_offset_of_U3CConfigLoadedU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2983 = { sizeof (Variable_t437234954), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2983[2] = 
{
	Variable_t437234954::get_offset_of_U3CKeyU3Ek__BackingField_0(),
	Variable_t437234954::get_offset_of_U3CValueU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2984 = { sizeof (CredentialInfo_t34154441), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2984[6] = 
{
	CredentialInfo_t34154441::get_offset_of_m_ServiceID_0(),
	CredentialInfo_t34154441::get_offset_of_m_URL_1(),
	CredentialInfo_t34154441::get_offset_of_m_User_2(),
	CredentialInfo_t34154441::get_offset_of_m_Password_3(),
	CredentialInfo_t34154441::get_offset_of_m_Apikey_4(),
	CredentialInfo_t34154441::get_offset_of_m_Note_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2985 = { sizeof (U3CLoadConfigCRU3Ec__Iterator0_t3553416038), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2985[5] = 
{
	U3CLoadConfigCRU3Ec__Iterator0_t3553416038::get_offset_of_U3CrequestU3E__0_0(),
	U3CLoadConfigCRU3Ec__Iterator0_t3553416038::get_offset_of_U24this_1(),
	U3CLoadConfigCRU3Ec__Iterator0_t3553416038::get_offset_of_U24current_2(),
	U3CLoadConfigCRU3Ec__Iterator0_t3553416038::get_offset_of_U24disposing_3(),
	U3CLoadConfigCRU3Ec__Iterator0_t3553416038::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2986 = { sizeof (Constants_t3488194005), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2987 = { sizeof (Path_t2333244422), -1, sizeof(Path_t2333244422_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2987[3] = 
{
	0,
	Path_t2333244422_StaticFields::get_offset_of_CACHE_FOLDER_1(),
	Path_t2333244422_StaticFields::get_offset_of_LOG_FOLDER_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2988 = { sizeof (Resources_t2675506754), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2988[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2989 = { sizeof (String_t3367353832), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2989[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2990 = { sizeof (Credentials_t1494051450), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2990[2] = 
{
	Credentials_t1494051450::get_offset_of_U3CUserU3Ek__BackingField_0(),
	Credentials_t1494051450::get_offset_of_U3CPasswordU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2991 = { sizeof (DataCache_t4250340070), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2991[5] = 
{
	DataCache_t4250340070::get_offset_of_m_CachePath_0(),
	DataCache_t4250340070::get_offset_of_m_MaxCacheSize_1(),
	DataCache_t4250340070::get_offset_of_m_MaxCacheAge_2(),
	DataCache_t4250340070::get_offset_of_m_CurrentCacheSize_3(),
	DataCache_t4250340070::get_offset_of_m_Cache_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2992 = { sizeof (CacheItem_t2472383731), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2992[4] = 
{
	CacheItem_t2472383731::get_offset_of_U3CPathU3Ek__BackingField_0(),
	CacheItem_t2472383731::get_offset_of_U3CIdU3Ek__BackingField_1(),
	CacheItem_t2472383731::get_offset_of_U3CTimeU3Ek__BackingField_2(),
	CacheItem_t2472383731::get_offset_of_U3CDataU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2993 = { sizeof (EventManager_t605335149), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2993[4] = 
{
	EventManager_t605335149::get_offset_of_m_EventTypeName_0(),
	EventManager_t605335149::get_offset_of_m_EventMap_1(),
	EventManager_t605335149::get_offset_of_m_AsyncEvents_2(),
	EventManager_t605335149::get_offset_of_m_ProcesserCount_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2994 = { sizeof (OnReceiveEvent_t4237107671), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2995 = { sizeof (AsyncEvent_t2332674247), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2995[2] = 
{
	AsyncEvent_t2332674247::get_offset_of_m_EventName_0(),
	AsyncEvent_t2332674247::get_offset_of_m_Args_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2996 = { sizeof (U3CProcessAsyncEventsU3Ec__Iterator0_t1795645094), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2996[4] = 
{
	U3CProcessAsyncEventsU3Ec__Iterator0_t1795645094::get_offset_of_U24this_0(),
	U3CProcessAsyncEventsU3Ec__Iterator0_t1795645094::get_offset_of_U24current_1(),
	U3CProcessAsyncEventsU3Ec__Iterator0_t1795645094::get_offset_of_U24disposing_2(),
	U3CProcessAsyncEventsU3Ec__Iterator0_t1795645094::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2997 = { sizeof (FrameRateCounter_t1983932695), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2997[6] = 
{
	0,
	0,
	FrameRateCounter_t1983932695::get_offset_of_m_FpsAccumulator_4(),
	FrameRateCounter_t1983932695::get_offset_of_m_FpsNextPeriod_5(),
	FrameRateCounter_t1983932695::get_offset_of_m_CurrentFps_6(),
	FrameRateCounter_t1983932695::get_offset_of_m_Text_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2998 = { sizeof (KeyModifiers_t231190421)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2998[5] = 
{
	KeyModifiers_t231190421::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2999 = { sizeof (KeyEventManager_t3314674286), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2999[5] = 
{
	KeyEventManager_t3314674286::get_offset_of_MODIFIER_SHIFT_BITS_2(),
	KeyEventManager_t3314674286::get_offset_of_KEYCODE_MASK_3(),
	KeyEventManager_t3314674286::get_offset_of_m_Active_4(),
	KeyEventManager_t3314674286::get_offset_of_m_UpdateActivate_5(),
	KeyEventManager_t3314674286::get_offset_of_m_KeyEvents_6(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
