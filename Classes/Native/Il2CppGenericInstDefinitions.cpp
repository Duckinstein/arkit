﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"




extern const Il2CppType RuntimeObject_0_0_0;
extern const Il2CppType Int32_t2071877448_0_0_0;
extern const Il2CppType Char_t3454481338_0_0_0;
extern const Il2CppType Int64_t909078037_0_0_0;
extern const Il2CppType UInt32_t2149682021_0_0_0;
extern const Il2CppType UInt64_t2909196914_0_0_0;
extern const Il2CppType Byte_t3683104436_0_0_0;
extern const Il2CppType SByte_t454417549_0_0_0;
extern const Il2CppType Int16_t4041245914_0_0_0;
extern const Il2CppType UInt16_t986882611_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType IConvertible_t908092482_0_0_0;
extern const Il2CppType IComparable_t1857082765_0_0_0;
extern const Il2CppType IEnumerable_t2911409499_0_0_0;
extern const Il2CppType ICloneable_t3853279282_0_0_0;
extern const Il2CppType IComparable_1_t3861059456_0_0_0;
extern const Il2CppType IEquatable_1_t4233202402_0_0_0;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType IReflect_t3412036974_0_0_0;
extern const Il2CppType _Type_t102776839_0_0_0;
extern const Il2CppType MemberInfo_t_0_0_0;
extern const Il2CppType ICustomAttributeProvider_t502202687_0_0_0;
extern const Il2CppType _MemberInfo_t332722161_0_0_0;
extern const Il2CppType Double_t4078015681_0_0_0;
extern const Il2CppType Single_t2076509932_0_0_0;
extern const Il2CppType Decimal_t724701077_0_0_0;
extern const Il2CppType Boolean_t3825574718_0_0_0;
extern const Il2CppType Delegate_t3022476291_0_0_0;
extern const Il2CppType ISerializable_t1245643778_0_0_0;
extern const Il2CppType ParameterInfo_t2249040075_0_0_0;
extern const Il2CppType _ParameterInfo_t470209990_0_0_0;
extern const Il2CppType ParameterModifier_t1820634920_0_0_0;
extern const Il2CppType EventInfo_t_0_0_0;
extern const Il2CppType _EventInfo_t2430923913_0_0_0;
extern const Il2CppType FieldInfo_t_0_0_0;
extern const Il2CppType _FieldInfo_t2511231167_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
extern const Il2CppType _MethodInfo_t3642518830_0_0_0;
extern const Il2CppType MethodBase_t904190842_0_0_0;
extern const Il2CppType _MethodBase_t1935530873_0_0_0;
extern const Il2CppType PropertyInfo_t_0_0_0;
extern const Il2CppType _PropertyInfo_t1567586598_0_0_0;
extern const Il2CppType ConstructorInfo_t2851816542_0_0_0;
extern const Il2CppType _ConstructorInfo_t3269099341_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType TableRange_t2011406615_0_0_0;
extern const Il2CppType TailoringInfo_t1449609243_0_0_0;
extern const Il2CppType KeyValuePair_2_t3716250094_0_0_0;
extern const Il2CppType Link_t2723257478_0_0_0;
extern const Il2CppType DictionaryEntry_t3048875398_0_0_0;
extern const Il2CppType KeyValuePair_2_t1744001932_0_0_0;
extern const Il2CppType Contraction_t1673853792_0_0_0;
extern const Il2CppType Level2Map_t3322505726_0_0_0;
extern const Il2CppType BigInteger_t925946152_0_0_0;
extern const Il2CppType KeySizes_t3144736271_0_0_0;
extern const Il2CppType KeyValuePair_2_t38854645_0_0_0;
extern const Il2CppType Slot_t2022531261_0_0_0;
extern const Il2CppType Slot_t2267560602_0_0_0;
extern const Il2CppType StackFrame_t2050294881_0_0_0;
extern const Il2CppType Calendar_t585061108_0_0_0;
extern const Il2CppType DateTime_t693205669_0_0_0;
extern const Il2CppType ModuleBuilder_t4156028127_0_0_0;
extern const Il2CppType _ModuleBuilder_t1075102050_0_0_0;
extern const Il2CppType Module_t4282841206_0_0_0;
extern const Il2CppType _Module_t2144668161_0_0_0;
extern const Il2CppType ParameterBuilder_t3344728474_0_0_0;
extern const Il2CppType _ParameterBuilder_t2251638747_0_0_0;
extern const Il2CppType TypeU5BU5D_t1664964607_0_0_0;
extern const Il2CppType RuntimeArray_0_0_0;
extern const Il2CppType ICollection_t91669223_0_0_0;
extern const Il2CppType IList_t3321498491_0_0_0;
extern const Il2CppType IList_1_t1844743827_0_0_0;
extern const Il2CppType ICollection_1_t2255878531_0_0_0;
extern const Il2CppType IEnumerable_1_t1595930271_0_0_0;
extern const Il2CppType IList_1_t3952977575_0_0_0;
extern const Il2CppType ICollection_1_t69144983_0_0_0;
extern const Il2CppType IEnumerable_1_t3704164019_0_0_0;
extern const Il2CppType IList_1_t643717440_0_0_0;
extern const Il2CppType ICollection_1_t1054852144_0_0_0;
extern const Il2CppType IEnumerable_1_t394903884_0_0_0;
extern const Il2CppType IList_1_t289070565_0_0_0;
extern const Il2CppType ICollection_1_t700205269_0_0_0;
extern const Il2CppType IEnumerable_1_t40257009_0_0_0;
extern const Il2CppType IList_1_t1043143288_0_0_0;
extern const Il2CppType ICollection_1_t1454277992_0_0_0;
extern const Il2CppType IEnumerable_1_t794329732_0_0_0;
extern const Il2CppType IList_1_t873662762_0_0_0;
extern const Il2CppType ICollection_1_t1284797466_0_0_0;
extern const Il2CppType IEnumerable_1_t624849206_0_0_0;
extern const Il2CppType IList_1_t3230389896_0_0_0;
extern const Il2CppType ICollection_1_t3641524600_0_0_0;
extern const Il2CppType IEnumerable_1_t2981576340_0_0_0;
extern const Il2CppType ILTokenInfo_t149559338_0_0_0;
extern const Il2CppType LabelData_t3712112744_0_0_0;
extern const Il2CppType LabelFixup_t4090909514_0_0_0;
extern const Il2CppType GenericTypeParameterBuilder_t1370236603_0_0_0;
extern const Il2CppType TypeBuilder_t3308873219_0_0_0;
extern const Il2CppType _TypeBuilder_t2783404358_0_0_0;
extern const Il2CppType MethodBuilder_t644187984_0_0_0;
extern const Il2CppType _MethodBuilder_t3932949077_0_0_0;
extern const Il2CppType ConstructorBuilder_t700974433_0_0_0;
extern const Il2CppType _ConstructorBuilder_t1236878896_0_0_0;
extern const Il2CppType PropertyBuilder_t3694255912_0_0_0;
extern const Il2CppType _PropertyBuilder_t3341912621_0_0_0;
extern const Il2CppType FieldBuilder_t2784804005_0_0_0;
extern const Il2CppType _FieldBuilder_t1895266044_0_0_0;
extern const Il2CppType CustomAttributeTypedArgument_t1498197914_0_0_0;
extern const Il2CppType CustomAttributeNamedArgument_t94157543_0_0_0;
extern const Il2CppType CustomAttributeData_t3093286891_0_0_0;
extern const Il2CppType ResourceInfo_t3933049236_0_0_0;
extern const Il2CppType ResourceCacheItem_t333236149_0_0_0;
extern const Il2CppType IContextProperty_t287246399_0_0_0;
extern const Il2CppType Header_t2756440555_0_0_0;
extern const Il2CppType ITrackingHandler_t2759960940_0_0_0;
extern const Il2CppType IContextAttribute_t2439121372_0_0_0;
extern const Il2CppType TimeSpan_t3430258949_0_0_0;
extern const Il2CppType TypeTag_t141209596_0_0_0;
extern const Il2CppType MonoType_t_0_0_0;
extern const Il2CppType StrongName_t2988747270_0_0_0;
extern const Il2CppType IBuiltInEvidence_t1114073477_0_0_0;
extern const Il2CppType IIdentityPermissionFactory_t2988326850_0_0_0;
extern const Il2CppType Assembly_t4268412390_0_0_0;
extern const Il2CppType _Assembly_t2937922309_0_0_0;
extern const Il2CppType DateTimeOffset_t1362988906_0_0_0;
extern const Il2CppType Guid_t_0_0_0;
extern const Il2CppType Version_t1755874712_0_0_0;
extern const Il2CppType NetworkInterface_t63927633_0_0_0;
extern const Il2CppType IPAddress_t1399971723_0_0_0;
extern const Il2CppType LinuxNetworkInterface_t3864470295_0_0_0;
extern const Il2CppType KeyValuePair_2_t3536594779_0_0_0;
extern const Il2CppType MacOsNetworkInterface_t1454185290_0_0_0;
extern const Il2CppType KeyValuePair_2_t1126309774_0_0_0;
extern const Il2CppType Win32_IP_ADAPTER_ADDRESSES_t680756680_0_0_0;
extern const Il2CppType X509ChainStatus_t4278378721_0_0_0;
extern const Il2CppType ArraySegment_1_t2594217482_0_0_0;
extern const Il2CppType KeyValuePair_2_t1174980068_0_0_0;
extern const Il2CppType KeyValuePair_2_t3497699202_0_0_0;
extern const Il2CppType X509Certificate_t283079845_0_0_0;
extern const Il2CppType IDeserializationCallback_t327125377_0_0_0;
extern const Il2CppType Capture_t4157900610_0_0_0;
extern const Il2CppType Group_t3761430853_0_0_0;
extern const Il2CppType Mark_t2724874473_0_0_0;
extern const Il2CppType UriScheme_t1876590943_0_0_0;
extern const Il2CppType BigInteger_t925946153_0_0_0;
extern const Il2CppType ByteU5BU5D_t3397334013_0_0_0;
extern const Il2CppType IList_1_t4224045037_0_0_0;
extern const Il2CppType ICollection_1_t340212445_0_0_0;
extern const Il2CppType IEnumerable_1_t3975231481_0_0_0;
extern const Il2CppType ClientCertificateType_t4001384466_0_0_0;
extern const Il2CppType Link_t865133271_0_0_0;
extern const Il2CppType OscPacketReceivedEventArgs_t3282045269_0_0_0;
extern const Il2CppType OscBundleReceivedEventArgs_t3673224441_0_0_0;
extern const Il2CppType ExceptionEventArgs_t892713536_0_0_0;
extern const Il2CppType OscMessageReceivedEventArgs_t1263041860_0_0_0;
extern const Il2CppType XPathNavigator_t3981235968_0_0_0;
extern const Il2CppType IXPathNavigable_t845515791_0_0_0;
extern const Il2CppType IXmlNamespaceResolver_t3928241465_0_0_0;
extern const Il2CppType XPathItem_t3130801258_0_0_0;
extern const Il2CppType XPathResultType_t1521569578_0_0_0;
extern const Il2CppType Entry_t2583369454_0_0_0;
extern const Il2CppType NsDecl_t3210081295_0_0_0;
extern const Il2CppType Object_t1021602117_0_0_0;
extern const Il2CppType Camera_t189460977_0_0_0;
extern const Il2CppType Behaviour_t955675639_0_0_0;
extern const Il2CppType Component_t3819376471_0_0_0;
extern const Il2CppType Display_t3666191348_0_0_0;
extern const Il2CppType GradientColorKey_t1677506814_0_0_0;
extern const Il2CppType GradientAlphaKey_t648783245_0_0_0;
extern const Il2CppType Keyframe_t1449471340_0_0_0;
extern const Il2CppType Vector3_t2243707580_0_0_0;
extern const Il2CppType Vector4_t2243707581_0_0_0;
extern const Il2CppType Vector2_t2243707579_0_0_0;
extern const Il2CppType Color32_t874517518_0_0_0;
extern const Il2CppType Playable_t1896841784_0_0_0;
extern const Il2CppType PlayableOutput_t988259697_0_0_0;
extern const Il2CppType Scene_t1684909666_0_0_0;
extern const Il2CppType LoadSceneMode_t2981886439_0_0_0;
extern const Il2CppType SpriteAtlas_t3132429450_0_0_0;
extern const Il2CppType Particle_t250075699_0_0_0;
extern const Il2CppType ContactPoint_t1376425630_0_0_0;
extern const Il2CppType RaycastHit_t87180320_0_0_0;
extern const Il2CppType Rigidbody2D_t502193897_0_0_0;
extern const Il2CppType RaycastHit2D_t4063908774_0_0_0;
extern const Il2CppType ContactPoint2D_t3659330976_0_0_0;
extern const Il2CppType AudioClipPlayable_t192218916_0_0_0;
extern const Il2CppType AudioMixerPlayable_t1831808911_0_0_0;
extern const Il2CppType WebCamDevice_t3983871389_0_0_0;
extern const Il2CppType AnimationClipPlayable_t4099382200_0_0_0;
extern const Il2CppType AnimationLayerMixerPlayable_t3057952312_0_0_0;
extern const Il2CppType AnimationMixerPlayable_t1343787797_0_0_0;
extern const Il2CppType AnimationOffsetPlayable_t1019600543_0_0_0;
extern const Il2CppType AnimatorControllerPlayable_t1744083903_0_0_0;
extern const Il2CppType AnimatorClipInfo_t3905751349_0_0_0;
extern const Il2CppType AnimatorControllerParameter_t1381019216_0_0_0;
extern const Il2CppType UIVertex_t1204258818_0_0_0;
extern const Il2CppType UICharInfo_t3056636800_0_0_0;
extern const Il2CppType UILineInfo_t3621277874_0_0_0;
extern const Il2CppType Font_t4239498691_0_0_0;
extern const Il2CppType GUILayoutOption_t4183744904_0_0_0;
extern const Il2CppType GUILayoutEntry_t3828586629_0_0_0;
extern const Il2CppType LayoutCache_t3120781045_0_0_0;
extern const Il2CppType KeyValuePair_2_t3749587448_0_0_0;
extern const Il2CppType KeyValuePair_2_t4180919198_0_0_0;
extern const Il2CppType GUIStyle_t1799908754_0_0_0;
extern const Il2CppType KeyValuePair_2_t1472033238_0_0_0;
extern const Il2CppType Exception_t1927440687_0_0_0;
extern const Il2CppType KeyValuePair_2_t1701344717_0_0_0;
extern const Il2CppType UserProfile_t3365630962_0_0_0;
extern const Il2CppType IUserProfile_t4108565527_0_0_0;
extern const Il2CppType AchievementDescription_t3110978151_0_0_0;
extern const Il2CppType IAchievementDescription_t3498529102_0_0_0;
extern const Il2CppType GcLeaderboard_t453887929_0_0_0;
extern const Il2CppType IAchievementDescriptionU5BU5D_t4083280315_0_0_0;
extern const Il2CppType IAchievementU5BU5D_t2709554645_0_0_0;
extern const Il2CppType IAchievement_t1752291260_0_0_0;
extern const Il2CppType GcAchievementData_t1754866149_0_0_0;
extern const Il2CppType Achievement_t1333316625_0_0_0;
extern const Il2CppType IScoreU5BU5D_t3237304636_0_0_0;
extern const Il2CppType IScore_t513966369_0_0_0;
extern const Il2CppType GcScoreData_t3676783238_0_0_0;
extern const Il2CppType Score_t2307748940_0_0_0;
extern const Il2CppType IUserProfileU5BU5D_t3461248430_0_0_0;
extern const Il2CppType DisallowMultipleComponent_t2656950_0_0_0;
extern const Il2CppType Attribute_t542643598_0_0_0;
extern const Il2CppType _Attribute_t1557664299_0_0_0;
extern const Il2CppType ExecuteInEditMode_t3043633143_0_0_0;
extern const Il2CppType RequireComponent_t864575032_0_0_0;
extern const Il2CppType HitInfo_t1761367055_0_0_0;
extern const Il2CppType PersistentCall_t3793436469_0_0_0;
extern const Il2CppType BaseInvokableCall_t2229564840_0_0_0;
extern const Il2CppType WorkRequest_t1154022482_0_0_0;
extern const Il2CppType PlayableBinding_t2498078091_0_0_0;
extern const Il2CppType MessageTypeSubscribers_t2291506050_0_0_0;
extern const Il2CppType MessageEventArgs_t301283622_0_0_0;
extern const Il2CppType WeakReference_t1077405567_0_0_0;
extern const Il2CppType KeyValuePair_2_t888819835_0_0_0;
extern const Il2CppType KeyValuePair_2_t3571743403_0_0_0;
extern const Il2CppType List_1_t61287617_0_0_0;
extern const Il2CppType DispatcherKey_t708950850_0_0_0;
extern const Il2CppType Dispatcher_t2240407071_0_0_0;
extern const Il2CppType KeyValuePair_2_t3846770086_0_0_0;
extern const Il2CppType HtmlNode_t2048434459_0_0_0;
extern const Il2CppType HtmlElementFlag_t260274357_0_0_0;
extern const Il2CppType KeyValuePair_2_t1904647003_0_0_0;
extern const Il2CppType KeyValuePair_2_t4227366137_0_0_0;
extern const Il2CppType KeyValuePair_2_t3108572612_0_0_0;
extern const Il2CppType HtmlAttribute_t1804523403_0_0_0;
extern const Il2CppType KeyValuePair_2_t1720558943_0_0_0;
extern const Il2CppType KeyValuePair_2_t1476647887_0_0_0;
extern const Il2CppType HtmlParseError_t1115179162_0_0_0;
extern const Il2CppType List_1_t1070465849_0_0_0;
extern const Il2CppType KeyValuePair_2_t742590333_0_0_0;
extern const Il2CppType TuioBlobEventArgs_t3562978179_0_0_0;
extern const Il2CppType TuioBlob_t2046943414_0_0_0;
extern const Il2CppType KeyValuePair_2_t3107081567_0_0_0;
extern const Il2CppType TuioCursorEventArgs_t362990012_0_0_0;
extern const Il2CppType TuioCursor_t1850351419_0_0_0;
extern const Il2CppType KeyValuePair_2_t2910489572_0_0_0;
extern const Il2CppType TuioObjectEventArgs_t1810743341_0_0_0;
extern const Il2CppType TuioObject_t1236821014_0_0_0;
extern const Il2CppType KeyValuePair_2_t2296959167_0_0_0;
extern const Il2CppType IDataProcessor_t2214055399_0_0_0;
extern const Il2CppType BaseInputModule_t1295781545_0_0_0;
extern const Il2CppType RaycastResult_t21186376_0_0_0;
extern const Il2CppType IDeselectHandler_t3182198310_0_0_0;
extern const Il2CppType IEventSystemHandler_t2741188318_0_0_0;
extern const Il2CppType List_1_t2110309450_0_0_0;
extern const Il2CppType List_1_t2058570427_0_0_0;
extern const Il2CppType List_1_t3188497603_0_0_0;
extern const Il2CppType ISelectHandler_t2812555161_0_0_0;
extern const Il2CppType BaseRaycaster_t2336171397_0_0_0;
extern const Il2CppType Entry_t3365010046_0_0_0;
extern const Il2CppType BaseEventData_t2681005625_0_0_0;
extern const Il2CppType IPointerEnterHandler_t193164956_0_0_0;
extern const Il2CppType IPointerExitHandler_t461019860_0_0_0;
extern const Il2CppType IPointerDownHandler_t3929046918_0_0_0;
extern const Il2CppType IPointerUpHandler_t1847764461_0_0_0;
extern const Il2CppType IPointerClickHandler_t96169666_0_0_0;
extern const Il2CppType IInitializePotentialDragHandler_t3350809087_0_0_0;
extern const Il2CppType IBeginDragHandler_t3135127860_0_0_0;
extern const Il2CppType IDragHandler_t2583993319_0_0_0;
extern const Il2CppType IEndDragHandler_t1349123600_0_0_0;
extern const Il2CppType IDropHandler_t2390101210_0_0_0;
extern const Il2CppType IScrollHandler_t3834677510_0_0_0;
extern const Il2CppType IUpdateSelectedHandler_t3778909353_0_0_0;
extern const Il2CppType IMoveHandler_t2611925506_0_0_0;
extern const Il2CppType ISubmitHandler_t525803901_0_0_0;
extern const Il2CppType ICancelHandler_t1980319651_0_0_0;
extern const Il2CppType Transform_t3275118058_0_0_0;
extern const Il2CppType GameObject_t1756533147_0_0_0;
extern const Il2CppType BaseInput_t621514313_0_0_0;
extern const Il2CppType UIBehaviour_t3960014691_0_0_0;
extern const Il2CppType MonoBehaviour_t1158329972_0_0_0;
extern const Il2CppType PointerEventData_t1599784723_0_0_0;
extern const Il2CppType KeyValuePair_2_t2659922876_0_0_0;
extern const Il2CppType ButtonState_t2688375492_0_0_0;
extern const Il2CppType Color_t2020392075_0_0_0;
extern const Il2CppType ICanvasElement_t986520779_0_0_0;
extern const Il2CppType ColorBlock_t2652774230_0_0_0;
extern const Il2CppType OptionData_t2420267500_0_0_0;
extern const Il2CppType DropdownItem_t4139978805_0_0_0;
extern const Il2CppType FloatTween_t2986189219_0_0_0;
extern const Il2CppType Sprite_t309593783_0_0_0;
extern const Il2CppType Canvas_t209405766_0_0_0;
extern const Il2CppType List_1_t3873494194_0_0_0;
extern const Il2CppType HashSet_1_t2984649583_0_0_0;
extern const Il2CppType Text_t356221433_0_0_0;
extern const Il2CppType KeyValuePair_2_t850112849_0_0_0;
extern const Il2CppType ColorTween_t3438117476_0_0_0;
extern const Il2CppType Graphic_t2426225576_0_0_0;
extern const Il2CppType IndexedSet_1_t286373651_0_0_0;
extern const Il2CppType KeyValuePair_2_t2391682566_0_0_0;
extern const Il2CppType KeyValuePair_2_t3010968081_0_0_0;
extern const Il2CppType KeyValuePair_2_t1912381698_0_0_0;
extern const Il2CppType Type_t3352948571_0_0_0;
extern const Il2CppType FillMethod_t1640962579_0_0_0;
extern const Il2CppType ContentType_t1028629049_0_0_0;
extern const Il2CppType LineType_t2931319356_0_0_0;
extern const Il2CppType InputType_t1274231802_0_0_0;
extern const Il2CppType TouchScreenKeyboardType_t875112366_0_0_0;
extern const Il2CppType CharacterValidation_t3437478890_0_0_0;
extern const Il2CppType Mask_t2977958238_0_0_0;
extern const Il2CppType List_1_t2347079370_0_0_0;
extern const Il2CppType RectMask2D_t1156185964_0_0_0;
extern const Il2CppType List_1_t525307096_0_0_0;
extern const Il2CppType Navigation_t1571958496_0_0_0;
extern const Il2CppType IClippable_t1941276057_0_0_0;
extern const Il2CppType Direction_t3696775921_0_0_0;
extern const Il2CppType Selectable_t1490392188_0_0_0;
extern const Il2CppType Transition_t605142169_0_0_0;
extern const Il2CppType SpriteState_t1353336012_0_0_0;
extern const Il2CppType CanvasGroup_t3296560743_0_0_0;
extern const Il2CppType Direction_t1525323322_0_0_0;
extern const Il2CppType MatEntry_t3157325053_0_0_0;
extern const Il2CppType Toggle_t3976754468_0_0_0;
extern const Il2CppType IClipper_t900477982_0_0_0;
extern const Il2CppType KeyValuePair_2_t379984643_0_0_0;
extern const Il2CppType AspectMode_t1166448724_0_0_0;
extern const Il2CppType FitMode_t4030874534_0_0_0;
extern const Il2CppType RectTransform_t3349966182_0_0_0;
extern const Il2CppType LayoutRebuilder_t2155218138_0_0_0;
extern const Il2CppType ILayoutElement_t1975293769_0_0_0;
extern const Il2CppType List_1_t1612828712_0_0_0;
extern const Il2CppType List_1_t243638650_0_0_0;
extern const Il2CppType List_1_t1612828711_0_0_0;
extern const Il2CppType List_1_t1612828713_0_0_0;
extern const Il2CppType List_1_t1440998580_0_0_0;
extern const Il2CppType List_1_t573379950_0_0_0;
extern const Il2CppType ARHitTestResult_t3275513025_0_0_0;
extern const Il2CppType ARPlaneAnchorGameObject_t2305225887_0_0_0;
extern const Il2CppType ARHitTestResultType_t3616749745_0_0_0;
extern const Il2CppType UnityARSessionRunOption_t3123075684_0_0_0;
extern const Il2CppType UnityARAlignment_t2379988631_0_0_0;
extern const Il2CppType UnityARPlaneDetection_t612575857_0_0_0;
extern const Il2CppType KeyValuePair_2_t1977350371_0_0_0;
extern const Il2CppType ParticleSystem_t3394631041_0_0_0;
extern const Il2CppType Animator_t69676727_0_0_0;
extern const Il2CppType Concept_t2322716066_0_0_0;
extern const Il2CppType Date_t2577429686_0_0_0;
extern const Il2CppType Entity_t2757698109_0_0_0;
extern const Il2CppType Feed_t2248050640_0_0_0;
extern const Il2CppType Keyword_t3384522415_0_0_0;
extern const Il2CppType Microformat_t171559559_0_0_0;
extern const Il2CppType Relation_t2336466576_0_0_0;
extern const Il2CppType TargetedSentiment_t494351509_0_0_0;
extern const Il2CppType Taxonomy_t1527601757_0_0_0;
extern const Il2CppType ImageKeyword_t1009764024_0_0_0;
extern const Il2CppType DocEmotions_t3627397418_0_0_0;
extern const Il2CppType Intent_t1324785294_0_0_0;
extern const Il2CppType MessageIntent_t315351975_0_0_0;
extern const Il2CppType Environment_t3491688169_0_0_0;
extern const Il2CppType ConfigurationRef_t2639036165_0_0_0;
extern const Il2CppType CollectionRef_t3248558799_0_0_0;
extern const Il2CppType QueryResult_t1852366331_0_0_0;
extern const Il2CppType Metadata_t1932344831_0_0_0;
extern const Il2CppType AnswerUnit_t3274674984_0_0_0;
extern const Il2CppType Content_t2184047853_0_0_0;
extern const Il2CppType Translation_t1796874115_0_0_0;
extern const Il2CppType Translation_t308652637_0_0_0;
extern const Il2CppType TraitTreeNode_t694336958_0_0_0;
extern const Il2CppType TraitTreeNode_t3511443394_0_0_0;
extern const Il2CppType BehaviorNode_t287073406_0_0_0;
extern const Il2CppType ConsumptionPreferencesCategoryNode_t38182357_0_0_0;
extern const Il2CppType ConsumptionPreferencesNode_t1038535837_0_0_0;
extern const Il2CppType SolrClusterResponse_t1008480713_0_0_0;
extern const Il2CppType RankerInfoPayload_t1776809245_0_0_0;
extern const Il2CppType RankedAnswer_t193891511_0_0_0;
extern const Il2CppType Doc_t3160830270_0_0_0;
extern const Il2CppType Model_t2340542523_0_0_0;
extern const Il2CppType SpeechRecognitionResult_t3827468010_0_0_0;
extern const Il2CppType SpeechRecognitionAlternative_t84095082_0_0_0;
extern const Il2CppType Customization_t47167237_0_0_0;
extern const Il2CppType Corpus_t3396664578_0_0_0;
extern const Il2CppType WordData_t612265514_0_0_0;
extern const Il2CppType Word_t4274554970_0_0_0;
extern const Il2CppType Voice_t3646862260_0_0_0;
extern const Il2CppType Customization_t2261013253_0_0_0;
extern const Il2CppType Column_t2612486824_0_0_0;
extern const Il2CppType Option_t1775127045_0_0_0;
extern const Il2CppType GetClassifiersPerClassifierBrief_t3946924106_0_0_0;
extern const Il2CppType ClassifyTopLevelSingle_t3733844951_0_0_0;
extern const Il2CppType ClassifyPerClassifier_t2357796318_0_0_0;
extern const Il2CppType ClassResult_t3791036973_0_0_0;
extern const Il2CppType FacesTopLevelSingle_t2140351533_0_0_0;
extern const Il2CppType OneFaceResult_t2674708326_0_0_0;
extern const Il2CppType TextRecogTopLevelSingle_t1839648508_0_0_0;
extern const Il2CppType TextRecogOneWord_t2687074657_0_0_0;
extern const Il2CppType MenuScene_t1232739575_0_0_0;
extern const Il2CppType CameraTarget_t552179396_0_0_0;
extern const Il2CppType RESTConnector_t3705102247_0_0_0;
extern const Il2CppType KeyValuePair_2_t3377226731_0_0_0;
extern const Il2CppType Request_t466816980_0_0_0;
extern const Il2CppType KeyValuePair_2_t2361573779_0_0_0;
extern const Il2CppType Form_t779275863_0_0_0;
extern const Il2CppType KeyValuePair_2_t451400347_0_0_0;
extern const Il2CppType Message_t2515021790_0_0_0;
extern const Il2CppType CloseEventArgs_t344507773_0_0_0;
extern const Il2CppType ErrorEventArgs_t502222999_0_0_0;
extern const Il2CppType MessageEventArgs_t2890051726_0_0_0;
extern const Il2CppType DebugInfo_t379980446_0_0_0;
extern const Il2CppType QualityManager_t3466824526_0_0_0;
extern const Il2CppType LogSystem_t3371271695_0_0_0;
extern const Il2CppType ILogReactor_t2178507087_0_0_0;
extern const Il2CppType Quotation_t2638841872_0_0_0;
extern const Il2CppType Docs_t1723806121_0_0_0;
extern const Il2CppType EnrichedTitle_t2959558712_0_0_0;
extern const Il2CppType EntityResponse_t2137526154_0_0_0;
extern const Il2CppType LogMessageResponse_t3273544838_0_0_0;
extern const Il2CppType Workspace_t1018343827_0_0_0;
extern const Il2CppType EntityExample_t3008277273_0_0_0;
extern const Il2CppType Intent_t3642575974_0_0_0;
extern const Il2CppType Example_t1927362902_0_0_0;
extern const Il2CppType Entity_t4255761867_0_0_0;
extern const Il2CppType DialogNode_t2015514522_0_0_0;
extern const Il2CppType Synonym_t1752818203_0_0_0;
extern const Il2CppType SynSet_t2414122624_0_0_0;
extern const Il2CppType Evidence_t859225293_0_0_0;
extern const Il2CppType Table_t205705827_0_0_0;
extern const Il2CppType Row_t3628585283_0_0_0;
extern const Il2CppType Cell_t3725717219_0_0_0;
extern const Il2CppType Word_t3826420310_0_0_0;
extern const Il2CppType Value_t2642671545_0_0_0;
extern const Il2CppType SynonymList_t1745302257_0_0_0;
extern const Il2CppType ParseTree_t2821719349_0_0_0;
extern const Il2CppType Answer_t3626344892_0_0_0;
extern const Il2CppType QuestionClass_t734245290_0_0_0;
extern const Il2CppType Response_t4265902649_0_0_0;
extern const Il2CppType Notice_t1592579614_0_0_0;
extern const Il2CppType Enrichment_t533139721_0_0_0;
extern const Il2CppType NormalizationOperation_t3873208172_0_0_0;
extern const Il2CppType FontSetting_t3267780181_0_0_0;
extern const Il2CppType WordStyle_t1557885949_0_0_0;
extern const Il2CppType Field_t3865657682_0_0_0;
extern const Il2CppType Warning_t1238678040_0_0_0;
extern const Il2CppType IWatsonService_t2784709738_0_0_0;
extern const Il2CppType Language_t834895248_0_0_0;
extern const Il2CppType TranslationModel_t4027325182_0_0_0;
extern const Il2CppType Language_t2171379860_0_0_0;
extern const Il2CppType TranslationModel_t2730767334_0_0_0;
extern const Il2CppType Classifier_t1414011573_0_0_0;
extern const Il2CppType Class_t2988469534_0_0_0;
extern const Il2CppType DataCache_t4250340070_0_0_0;
extern const Il2CppType KeyValuePair_2_t3922464554_0_0_0;
extern const Il2CppType Warning_t514099562_0_0_0;
extern const Il2CppType ContentItem_t1958644056_0_0_0;
extern const Il2CppType Warning_t31395408_0_0_0;
extern const Il2CppType CollectionsResponse_t1627544196_0_0_0;
extern const Il2CppType TimeStamp_t82276870_0_0_0;
extern const Il2CppType WordConfidence_t2757029394_0_0_0;
extern const Il2CppType KeywordResult_t2819026148_0_0_0;
extern const Il2CppType WordAlternativeResult_t3127007908_0_0_0;
extern const Il2CppType JobStatus_t2358384683_0_0_0;
extern const Il2CppType Word_t3204175642_0_0_0;
extern const Il2CppType AudioData_t3894398524_0_0_0;
extern const Il2CppType VoiceType_t981025524_0_0_0;
extern const Il2CppType KeyValuePair_2_t4282424860_0_0_0;
extern const Il2CppType KeyValuePair_2_t3622195798_0_0_0;
extern const Il2CppType AudioFormatType_t2756784245_0_0_0;
extern const Il2CppType KeyValuePair_2_t993946999_0_0_0;
extern const Il2CppType KeyValuePair_2_t333717937_0_0_0;
extern const Il2CppType SentenceTone_t3234952115_0_0_0;
extern const Il2CppType ToneCategory_t3724297374_0_0_0;
extern const Il2CppType Tone_t1138009090_0_0_0;
extern const Il2CppType Solution_t204511443_0_0_0;
extern const Il2CppType Anchor_t1470404909_0_0_0;
extern const Il2CppType Node_t3986951550_0_0_0;
extern const Il2CppType WarningInfo_t624028094_0_0_0;
extern const Il2CppType Class_t4054986160_0_0_0;
extern const Il2CppType CreateCollection_t3312638156_0_0_0;
extern const Il2CppType GetCollectionsBrief_t2166116487_0_0_0;
extern const Il2CppType CollectionImagesConfig_t1273369062_0_0_0;
extern const Il2CppType SimilarImageConfig_t3734626070_0_0_0;
extern const Il2CppType KeyValuePair_2_t3069458497_0_0_0;
extern const Il2CppType UnitTestManager_t2486317649_0_0_0;
extern const Il2CppType AudioClip_t1932558630_0_0_0;
extern const Il2CppType Config_t3637807320_0_0_0;
extern const Il2CppType CredentialInfo_t34154441_0_0_0;
extern const Il2CppType Variable_t437234954_0_0_0;
extern const Il2CppType CacheItem_t2472383731_0_0_0;
extern const Il2CppType KeyValuePair_2_t2144508215_0_0_0;
extern const Il2CppType EventManager_t605335149_0_0_0;
extern const Il2CppType Dictionary_2_t1621280361_0_0_0;
extern const Il2CppType KeyValuePair_2_t3673592879_0_0_0;
extern const Il2CppType KeyValuePair_2_t1315983480_0_0_0;
extern const Il2CppType List_1_t3606228803_0_0_0;
extern const Il2CppType OnReceiveEvent_t4237107671_0_0_0;
extern const Il2CppType KeyValuePair_2_t3278353287_0_0_0;
extern const Il2CppType AsyncEvent_t2332674247_0_0_0;
extern const Il2CppType KeyEventManager_t3314674286_0_0_0;
extern const Il2CppType KeyValuePair_2_t3089358386_0_0_0;
extern const Il2CppType Runnable_t1422227699_0_0_0;
extern const Il2CppType Routine_t1973058165_0_0_0;
extern const Il2CppType KeyValuePair_2_t3033196318_0_0_0;
extern const Il2CppType List_1_t1441188381_0_0_0;
extern const Il2CppType TouchEventData_t2072067249_0_0_0;
extern const Il2CppType KeyValuePair_2_t2501326534_0_0_0;
extern const Il2CppType EventArgs_t3289624707_0_0_0;
extern const Il2CppType Collider_t3497673348_0_0_0;
extern const Il2CppType Collider2D_t646061738_0_0_0;
extern const Il2CppType Mapping_t2602146240_0_0_0;
extern const Il2CppType Mapping_t1442940263_0_0_0;
extern const Il2CppType Mapping_t3124476604_0_0_0;
extern const Il2CppType List_1_t1398341365_0_0_0;
extern const Il2CppType KeyValuePair_2_t1070465850_0_0_0;
extern const Il2CppType ClassEventMapping_t3594451529_0_0_0;
extern const Il2CppType List_1_t3743315550_0_0_0;
extern const Il2CppType List_1_t3738683066_0_0_0;
extern const Il2CppType Speech_t1663141205_0_0_0;
extern const Il2CppType TapEventMapping_t2308161515_0_0_0;
extern const Il2CppType FullScreenDragEventMapping_t3872427665_0_0_0;
extern const Il2CppType Input_t3157785889_0_0_0;
extern const Il2CppType Output_t220081548_0_0_0;
extern const Il2CppType Widget_t3624771850_0_0_0;
extern const Il2CppType Connection_t2486334317_0_0_0;
extern const Il2CppType fsData_t2583805605_0_0_0;
extern const Il2CppType KeyValuePair_2_t2255930089_0_0_0;
extern const Il2CppType fsDataType_t1645355485_0_0_0;
extern const Il2CppType fsMetaProperty_t2249223145_0_0_0;
extern const Il2CppType AnimationCurve_t3306541151_0_0_0;
extern const Il2CppType Bounds_t3033363703_0_0_0;
extern const Il2CppType Gradient_t3600583008_0_0_0;
extern const Il2CppType LayerMask_t3188175821_0_0_0;
extern const Il2CppType Rect_t3681755626_0_0_0;
extern const Il2CppType KeyValuePair_2_t1723923352_0_0_0;
extern const Il2CppType AotCompilation_t21908242_0_0_0;
extern const Il2CppType KeyValuePair_2_t2384152414_0_0_0;
extern const Il2CppType fsBaseConverter_t1241677426_0_0_0;
extern const Il2CppType KeyValuePair_2_t936380545_0_0_0;
extern const Il2CppType List_1_t2055375476_0_0_0;
extern const Il2CppType fsObjectProcessor_t2686254344_0_0_0;
extern const Il2CppType KeyValuePair_2_t1750078595_0_0_0;
extern const Il2CppType fsConverter_t466758137_0_0_0;
extern const Il2CppType fsDirectConverter_t763460818_0_0_0;
extern const Il2CppType KeyValuePair_2_t458163937_0_0_0;
extern const Il2CppType fsVersionedType_t654750358_0_0_0;
extern const Il2CppType KeyValuePair_2_t3643943758_0_0_0;
extern const Il2CppType Link_t247561424_0_0_0;
extern const Il2CppType AttributeQuery_t604298480_0_0_0;
extern const Il2CppType KeyValuePair_2_t288805040_0_0_0;
extern const Il2CppType KeyValuePair_2_t2436966639_0_0_0;
extern const Il2CppType fsOption_1_t972008496_0_0_0;
extern const Il2CppType KeyValuePair_2_t2616381142_0_0_0;
extern const Il2CppType KeyValuePair_2_t666711615_0_0_0;
extern const Il2CppType KeyValuePair_2_t975927710_0_0_0;
extern const Il2CppType fsMetaType_t3266798926_0_0_0;
extern const Il2CppType KeyValuePair_2_t2961502045_0_0_0;
extern const Il2CppType TuioObjectMapping_t4080927128_0_0_0;
extern const Il2CppType TouchPoint_t959629083_0_0_0;
extern const Il2CppType KeyValuePair_2_t4169991845_0_0_0;
extern const Il2CppType KeyValuePair_2_t2907529438_0_0_0;
extern const Il2CppType KeyValuePair_2_t2840757246_0_0_0;
extern const Il2CppType TouchEventArgs_t1917927166_0_0_0;
extern const Il2CppType ITransformGesture_t1540584490_0_0_0;
extern const Il2CppType Gesture_t2352305985_0_0_0;
extern const Il2CppType DebuggableMonoBehaviour_t3136086048_0_0_0;
extern const Il2CppType IDebuggable_t2346276731_0_0_0;
extern const Il2CppType TouchProxyBase_t4188753234_0_0_0;
extern const Il2CppType KeyValuePair_2_t953924091_0_0_0;
extern const Il2CppType GestureManagerInstance_t505647059_0_0_0;
extern const Il2CppType IGestureManager_t4266705231_0_0_0;
extern const Il2CppType IList_1_t1500569684_0_0_0;
extern const Il2CppType List_1_t328750215_0_0_0;
extern const Il2CppType KeyValuePair_2_t2122717638_0_0_0;
extern const Il2CppType KeyValuePair_2_t2725989683_0_0_0;
extern const Il2CppType List_1_t1721427117_0_0_0;
extern const Il2CppType List_1_t2644239190_0_0_0;
extern const Il2CppType GestureStateChangeEventArgs_t3499981191_0_0_0;
extern const Il2CppType MetaGestureEventArgs_t256591615_0_0_0;
extern const Il2CppType TouchData_t3880599975_0_0_0;
extern const Il2CppType KeyValuePair_2_t645770832_0_0_0;
extern const Il2CppType Tags_t1265380163_0_0_0;
extern const Il2CppType TouchState_t2732082299_0_0_0;
extern const Il2CppType KeyValuePair_2_t3792220452_0_0_0;
extern const Il2CppType TouchLayer_t2635439978_0_0_0;
extern const Il2CppType IInputSource_t3266560338_0_0_0;
extern const Il2CppType HitTest_t768639505_0_0_0;
extern const Il2CppType KeyValuePair_2_t3132015601_0_0_0;
extern const Il2CppType TouchLayerEventArgs_t1247401065_0_0_0;
extern const Il2CppType ProjectionParams_t2712959773_0_0_0;
extern const Il2CppType KeyValuePair_2_t523301392_0_0_0;
extern const Il2CppType TouchManagerInstance_t2629118981_0_0_0;
extern const Il2CppType ITouchManager_t2552034033_0_0_0;
extern const Il2CppType KeyValuePair_2_t2019767236_0_0_0;
extern const Il2CppType TouchManager_t3980263048_0_0_0;
extern const Il2CppType StringU5BU5D_t1642385972_0_0_0;
extern const Il2CppType HttpResponse_t2820540315_0_0_0;
extern const Il2CppType HttpRequest_t1845443631_0_0_0;
extern const Il2CppType Cookie_t1826188460_0_0_0;
extern const Il2CppType LogData_t4095822710_0_0_0;
extern const Il2CppType Chunk_t2303927151_0_0_0;
extern const Il2CppType HttpListenerPrefix_t529778486_0_0_0;
extern const Il2CppType HttpListener_t4179429670_0_0_0;
extern const Il2CppType KeyValuePair_2_t2112148073_0_0_0;
extern const Il2CppType HttpConnection_t2649486862_0_0_0;
extern const Il2CppType KeyValuePair_2_t1689813529_0_0_0;
extern const Il2CppType Dictionary_2_t2945377568_0_0_0;
extern const Il2CppType EndPointListener_t3937551933_0_0_0;
extern const Il2CppType KeyValuePair_2_t702722790_0_0_0;
extern const Il2CppType KeyValuePair_2_t767379738_0_0_0;
extern const Il2CppType HttpListenerRequest_t2316381291_0_0_0;
extern const Il2CppType AuthenticationSchemes_t29593226_0_0_0;
extern const Il2CppType IIdentity_t2445095625_0_0_0;
extern const Il2CppType NetworkCredential_t3911206805_0_0_0;
extern const Il2CppType HttpListenerContext_t994708409_0_0_0;
extern const Il2CppType KeyValuePair_2_t138893069_0_0_0;
extern const Il2CppType HttpListenerAsyncResult_t3506939685_0_0_0;
extern const Il2CppType KeyValuePair_2_t803886688_0_0_0;
extern const Il2CppType KeyValuePair_2_t3126605822_0_0_0;
extern const Il2CppType HttpHeaderInfo_t2096319561_0_0_0;
extern const Il2CppType KeyValuePair_2_t1768444045_0_0_0;
extern const Il2CppType HttpRequestEventArgs_t918469868_0_0_0;
extern const Il2CppType WebSocketServiceHost_t492106494_0_0_0;
extern const Il2CppType KeyValuePair_2_t164230978_0_0_0;
extern const Il2CppType CookieCollection_t4248997468_0_0_0;
extern const Il2CppType WebSocketContext_t3488732344_0_0_0;
extern const Il2CppType CompressionMethod_t4066553457_0_0_0;
extern const Il2CppType KeyValuePair_2_t2919672843_0_0_0;
extern const Il2CppType KeyValuePair_2_t3627557561_0_0_0;
extern const Il2CppType Stream_t3255436806_0_0_0;
extern const Il2CppType KeyValuePair_2_t3485660354_0_0_0;
extern const Il2CppType Dictionary_2_t1445386684_0_0_0;
extern const Il2CppType KeyValuePair_2_t1117511168_0_0_0;
extern const Il2CppType IWebSocketSession_t1432056748_0_0_0;
extern const Il2CppType KeyValuePair_2_t1104181232_0_0_0;
extern const Il2CppType Opcode_t2313788840_0_0_0;
extern const Il2CppType WebSocketFrame_t764750278_0_0_0;
extern const Il2CppType Action_4_t3853297115_0_0_0;
extern const Il2CppType SpeechbubblePrefab_t736279730_0_0_0;
extern const Il2CppType DialogueLine_t768215248_0_0_0;
extern const Il2CppType SpeechbubbleType_t1311649088_0_0_0;
extern const Il2CppType KeyValuePair_2_t1971397920_0_0_0;
extern const Il2CppType KeyValuePair_2_t1038481772_0_0_0;
extern const Il2CppType Queue_1_t2840177624_0_0_0;
extern const Il2CppType SpeechbubbleBehaviour_t3020520789_0_0_0;
extern const Il2CppType KeyValuePair_2_t2122126249_0_0_0;
extern const Il2CppType IEnumerable_1_t4048664256_gp_0_0_0_0;
extern const Il2CppType Array_InternalArray__IEnumerable_GetEnumerator_m801276902_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m2069139338_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m1999601238_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m1999601238_gp_1_0_0_0;
extern const Il2CppType Array_Sort_m4168899348_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m1711256281_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m1711256281_gp_1_0_0_0;
extern const Il2CppType Array_Sort_m714603628_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m4057794348_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m4057794348_gp_1_0_0_0;
extern const Il2CppType Array_Sort_m2544329074_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m54102389_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m54102389_gp_1_0_0_0;
extern const Il2CppType Array_Sort_m2673640633_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m285045864_gp_0_0_0_0;
extern const Il2CppType Array_qsort_m965712368_gp_0_0_0_0;
extern const Il2CppType Array_qsort_m965712368_gp_1_0_0_0;
extern const Il2CppType Array_compare_m1681301885_gp_0_0_0_0;
extern const Il2CppType Array_qsort_m2216502740_gp_0_0_0_0;
extern const Il2CppType Array_Resize_m2875826811_gp_0_0_0_0;
extern const Il2CppType Array_TrueForAll_m1718874735_gp_0_0_0_0;
extern const Il2CppType Array_ForEach_m1854649314_gp_0_0_0_0;
extern const Il2CppType Array_ConvertAll_m4274024367_gp_0_0_0_0;
extern const Il2CppType Array_ConvertAll_m4274024367_gp_1_0_0_0;
extern const Il2CppType Array_FindLastIndex_m2314439434_gp_0_0_0_0;
extern const Il2CppType Array_FindLastIndex_m3151928313_gp_0_0_0_0;
extern const Il2CppType Array_FindLastIndex_m2323782676_gp_0_0_0_0;
extern const Il2CppType Array_FindIndex_m1557343438_gp_0_0_0_0;
extern const Il2CppType Array_FindIndex_m1631399507_gp_0_0_0_0;
extern const Il2CppType Array_FindIndex_m3415315332_gp_0_0_0_0;
extern const Il2CppType Array_BinarySearch_m2990936537_gp_0_0_0_0;
extern const Il2CppType Array_BinarySearch_m2009305197_gp_0_0_0_0;
extern const Il2CppType Array_BinarySearch_m2427841765_gp_0_0_0_0;
extern const Il2CppType Array_BinarySearch_m3348121441_gp_0_0_0_0;
extern const Il2CppType Array_IndexOf_m2010744443_gp_0_0_0_0;
extern const Il2CppType Array_IndexOf_m4126518092_gp_0_0_0_0;
extern const Il2CppType Array_IndexOf_m3127862871_gp_0_0_0_0;
extern const Il2CppType Array_LastIndexOf_m1523107937_gp_0_0_0_0;
extern const Il2CppType Array_LastIndexOf_m2200535596_gp_0_0_0_0;
extern const Il2CppType Array_LastIndexOf_m555775493_gp_0_0_0_0;
extern const Il2CppType Array_FindAll_m2331462890_gp_0_0_0_0;
extern const Il2CppType Array_Exists_m976807351_gp_0_0_0_0;
extern const Il2CppType Array_AsReadOnly_m884706992_gp_0_0_0_0;
extern const Il2CppType Array_Find_m1125132281_gp_0_0_0_0;
extern const Il2CppType Array_FindLast_m4092932075_gp_0_0_0_0;
extern const Il2CppType InternalEnumerator_1_t3582267753_gp_0_0_0_0;
extern const Il2CppType ArrayReadOnlyList_1_t3367196019_gp_0_0_0_0;
extern const Il2CppType U3CGetEnumeratorU3Ec__Iterator0_t1667190089_gp_0_0_0_0;
extern const Il2CppType IList_1_t3737699284_gp_0_0_0_0;
extern const Il2CppType ICollection_1_t1552160836_gp_0_0_0_0;
extern const Il2CppType Nullable_1_t1398937014_gp_0_0_0_0;
extern const Il2CppType Comparer_1_t1036860714_gp_0_0_0_0;
extern const Il2CppType DefaultComparer_t3074655092_gp_0_0_0_0;
extern const Il2CppType GenericComparer_1_t1787398723_gp_0_0_0_0;
extern const Il2CppType Dictionary_2_t2276497324_gp_0_0_0_0;
extern const Il2CppType Dictionary_2_t2276497324_gp_1_0_0_0;
extern const Il2CppType KeyValuePair_2_t3180694294_0_0_0;
extern const Il2CppType Dictionary_2_Do_CopyTo_m3006662979_gp_0_0_0_0;
extern const Il2CppType Dictionary_2_Do_ICollectionCopyTo_m1628523394_gp_0_0_0_0;
extern const Il2CppType ShimEnumerator_t3895203923_gp_0_0_0_0;
extern const Il2CppType ShimEnumerator_t3895203923_gp_1_0_0_0;
extern const Il2CppType Enumerator_t2089681430_gp_0_0_0_0;
extern const Il2CppType Enumerator_t2089681430_gp_1_0_0_0;
extern const Il2CppType KeyValuePair_2_t3434615342_0_0_0;
extern const Il2CppType KeyCollection_t1229212677_gp_0_0_0_0;
extern const Il2CppType KeyCollection_t1229212677_gp_1_0_0_0;
extern const Il2CppType Enumerator_t83320710_gp_0_0_0_0;
extern const Il2CppType Enumerator_t83320710_gp_1_0_0_0;
extern const Il2CppType ValueCollection_t2262344653_gp_0_0_0_0;
extern const Il2CppType ValueCollection_t2262344653_gp_1_0_0_0;
extern const Il2CppType Enumerator_t3111723616_gp_0_0_0_0;
extern const Il2CppType Enumerator_t3111723616_gp_1_0_0_0;
extern const Il2CppType EqualityComparer_1_t2066709010_gp_0_0_0_0;
extern const Il2CppType DefaultComparer_t1766400012_gp_0_0_0_0;
extern const Il2CppType GenericEqualityComparer_1_t2202941003_gp_0_0_0_0;
extern const Il2CppType KeyValuePair_2_t4174120762_0_0_0;
extern const Il2CppType IDictionary_2_t3502329323_gp_0_0_0_0;
extern const Il2CppType IDictionary_2_t3502329323_gp_1_0_0_0;
extern const Il2CppType KeyValuePair_2_t1988958766_gp_0_0_0_0;
extern const Il2CppType KeyValuePair_2_t1988958766_gp_1_0_0_0;
extern const Il2CppType List_1_t1169184319_gp_0_0_0_0;
extern const Il2CppType Enumerator_t1292967705_gp_0_0_0_0;
extern const Il2CppType Collection_1_t686054069_gp_0_0_0_0;
extern const Il2CppType ReadOnlyCollection_1_t3540981679_gp_0_0_0_0;
extern const Il2CppType MonoProperty_GetterAdapterFrame_m609993266_gp_0_0_0_0;
extern const Il2CppType MonoProperty_GetterAdapterFrame_m609993266_gp_1_0_0_0;
extern const Il2CppType MonoProperty_StaticGetterAdapterFrame_m781526467_gp_0_0_0_0;
extern const Il2CppType ArraySegment_1_t1001032761_gp_0_0_0_0;
extern const Il2CppType Queue_1_t1458930734_gp_0_0_0_0;
extern const Il2CppType Enumerator_t4000919638_gp_0_0_0_0;
extern const Il2CppType Stack_1_t4016656541_gp_0_0_0_0;
extern const Il2CppType Enumerator_t546412149_gp_0_0_0_0;
extern const Il2CppType HashSet_1_t2624254809_gp_0_0_0_0;
extern const Il2CppType Enumerator_t2109956843_gp_0_0_0_0;
extern const Il2CppType PrimeHelper_t3424417428_gp_0_0_0_0;
extern const Il2CppType Enumerable_Any_m1395534165_gp_0_0_0_0;
extern const Il2CppType Enumerable_Any_m4174937590_gp_0_0_0_0;
extern const Il2CppType Enumerable_First_m2440166523_gp_0_0_0_0;
extern const Il2CppType Enumerable_FirstOrDefault_m1564979201_gp_0_0_0_0;
extern const Il2CppType Enumerable_Select_m791198632_gp_0_0_0_0;
extern const Il2CppType Enumerable_Select_m791198632_gp_1_0_0_0;
extern const Il2CppType Enumerable_CreateSelectIterator_m3979771306_gp_0_0_0_0;
extern const Il2CppType Enumerable_CreateSelectIterator_m3979771306_gp_1_0_0_0;
extern const Il2CppType Enumerable_Skip_m3785421448_gp_0_0_0_0;
extern const Il2CppType Enumerable_CreateSkipIterator_m365619102_gp_0_0_0_0;
extern const Il2CppType Enumerable_ToArray_m747609077_gp_0_0_0_0;
extern const Il2CppType Enumerable_ToList_m2644368746_gp_0_0_0_0;
extern const Il2CppType Enumerable_Where_m1602106588_gp_0_0_0_0;
extern const Il2CppType Enumerable_CreateWhereIterator_m3985242862_gp_0_0_0_0;
extern const Il2CppType U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0;
extern const Il2CppType U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0;
extern const Il2CppType U3CCreateSkipIteratorU3Ec__Iterator16_1_t284123852_gp_0_0_0_0;
extern const Il2CppType U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0;
extern const Il2CppType Component_GetComponentInChildren_m1236184570_gp_0_0_0_0;
extern const Il2CppType Component_GetComponentsInChildren_m4167514082_gp_0_0_0_0;
extern const Il2CppType Component_GetComponentsInChildren_m4085536263_gp_0_0_0_0;
extern const Il2CppType Component_GetComponentsInChildren_m84946435_gp_0_0_0_0;
extern const Il2CppType Component_GetComponentsInChildren_m989421820_gp_0_0_0_0;
extern const Il2CppType Component_GetComponentsInParent_m893247195_gp_0_0_0_0;
extern const Il2CppType Component_GetComponentsInParent_m2646750694_gp_0_0_0_0;
extern const Il2CppType Component_GetComponentsInParent_m3278624542_gp_0_0_0_0;
extern const Il2CppType Component_GetComponents_m1252126604_gp_0_0_0_0;
extern const Il2CppType Component_GetComponents_m1618642439_gp_0_0_0_0;
extern const Il2CppType GameObject_GetComponentInChildren_m477715742_gp_0_0_0_0;
extern const Il2CppType GameObject_GetComponents_m3045349666_gp_0_0_0_0;
extern const Il2CppType GameObject_GetComponentsInChildren_m1567454139_gp_0_0_0_0;
extern const Il2CppType GameObject_GetComponentsInChildren_m1151940851_gp_0_0_0_0;
extern const Il2CppType GameObject_GetComponentsInParent_m4028060632_gp_0_0_0_0;
extern const Il2CppType Mesh_GetAllocArrayFromChannel_m4154891606_gp_0_0_0_0;
extern const Il2CppType Mesh_SafeLength_m1789465785_gp_0_0_0_0;
extern const Il2CppType Mesh_SetListForChannel_m2929091838_gp_0_0_0_0;
extern const Il2CppType Mesh_SetListForChannel_m3597787760_gp_0_0_0_0;
extern const Il2CppType Mesh_SetUvsImpl_m2321318716_gp_0_0_0_0;
extern const Il2CppType Object_Instantiate_m2766660278_gp_0_0_0_0;
extern const Il2CppType Object_FindObjectsOfType_m488103269_gp_0_0_0_0;
extern const Il2CppType Playable_IsPlayableOfType_m140883719_gp_0_0_0_0;
extern const Il2CppType PlayableOutput_IsPlayableOutputOfType_m2834483815_gp_0_0_0_0;
extern const Il2CppType InvokableCall_1_t476640868_gp_0_0_0_0;
extern const Il2CppType UnityAction_1_t2490859068_0_0_0;
extern const Il2CppType InvokableCall_2_t2042724809_gp_0_0_0_0;
extern const Il2CppType InvokableCall_2_t2042724809_gp_1_0_0_0;
extern const Il2CppType UnityAction_2_t601835599_0_0_0;
extern const Il2CppType InvokableCall_3_t3608808750_gp_0_0_0_0;
extern const Il2CppType InvokableCall_3_t3608808750_gp_1_0_0_0;
extern const Il2CppType InvokableCall_3_t3608808750_gp_2_0_0_0;
extern const Il2CppType UnityAction_3_t155920421_0_0_0;
extern const Il2CppType InvokableCall_4_t879925395_gp_0_0_0_0;
extern const Il2CppType InvokableCall_4_t879925395_gp_1_0_0_0;
extern const Il2CppType InvokableCall_4_t879925395_gp_2_0_0_0;
extern const Il2CppType InvokableCall_4_t879925395_gp_3_0_0_0;
extern const Il2CppType CachedInvokableCall_1_t224769006_gp_0_0_0_0;
extern const Il2CppType UnityEvent_1_t4075366602_gp_0_0_0_0;
extern const Il2CppType UnityEvent_2_t4075366599_gp_0_0_0_0;
extern const Il2CppType UnityEvent_2_t4075366599_gp_1_0_0_0;
extern const Il2CppType UnityEvent_3_t4075366600_gp_0_0_0_0;
extern const Il2CppType UnityEvent_3_t4075366600_gp_1_0_0_0;
extern const Il2CppType UnityEvent_3_t4075366600_gp_2_0_0_0;
extern const Il2CppType UnityEvent_4_t4075366597_gp_0_0_0_0;
extern const Il2CppType UnityEvent_4_t4075366597_gp_1_0_0_0;
extern const Il2CppType UnityEvent_4_t4075366597_gp_2_0_0_0;
extern const Il2CppType UnityEvent_4_t4075366597_gp_3_0_0_0;
extern const Il2CppType List_1_t2244783541_gp_0_0_0_0;
extern const Il2CppType U3CGetEnumeratorU3Ec__Iterator6_t1336800315_gp_0_0_0_0;
extern const Il2CppType List_1_t264254312_0_0_0;
extern const Il2CppType Utilities_GetDictionaryValueOrNull_m3096514106_gp_0_0_0_0;
extern const Il2CppType Utilities_GetDictionaryValueOrNull_m3096514106_gp_1_0_0_0;
extern const Il2CppType ExecuteEvents_Execute_m1744114673_gp_0_0_0_0;
extern const Il2CppType ExecuteEvents_ExecuteHierarchy_m3091757213_gp_0_0_0_0;
extern const Il2CppType ExecuteEvents_GetEventList_m2263439269_gp_0_0_0_0;
extern const Il2CppType ExecuteEvents_CanHandleEvent_m3190794379_gp_0_0_0_0;
extern const Il2CppType ExecuteEvents_GetEventHandler_m3792320302_gp_0_0_0_0;
extern const Il2CppType TweenRunner_1_t2584777480_gp_0_0_0_0;
extern const Il2CppType Dropdown_GetOrAddComponent_m41832880_gp_0_0_0_0;
extern const Il2CppType SetPropertyUtility_SetStruct_m1212286169_gp_0_0_0_0;
extern const Il2CppType IndexedSet_1_t573160278_gp_0_0_0_0;
extern const Il2CppType ListPool_1_t1984115411_gp_0_0_0_0;
extern const Il2CppType List_1_t2000868992_0_0_0;
extern const Il2CppType ObjectPool_1_t4265859154_gp_0_0_0_0;
extern const Il2CppType Singleton_1_t2363452800_gp_0_0_0_0;
extern const Il2CppType Utility_FindObjects_m2142378685_gp_0_0_0_0;
extern const Il2CppType Utility_GetFromCache_m3302286621_gp_0_0_0_0;
extern const Il2CppType Utility_DeserializeResponse_m1146239540_gp_0_0_0_0;
extern const Il2CppType Utility_DeserializeResponse_m549829201_gp_0_0_0_0;
extern const Il2CppType Widget_GetMembersByType_m59718008_gp_0_0_0_0;
extern const Il2CppType fsEnumConverter_ArrayContains_m409985959_gp_0_0_0_0;
extern const Il2CppType fsDirectConverter_1_t3257037661_gp_0_0_0_0;
extern const Il2CppType fsOption_1_t4013878301_gp_0_0_0_0;
extern const Il2CppType fsOption_Just_m130823817_gp_0_0_0_0;
extern const Il2CppType fsPortableReflection_GetAttribute_m2688546569_gp_0_0_0_0;
extern const Il2CppType EventHandlerExtensions_InvokeHandleExceptions_m2334443745_gp_0_0_0_0;
extern const Il2CppType ObjectPool_1_t3254757658_gp_0_0_0_0;
extern const Il2CppType TimedSequence_1_t2332636227_gp_0_0_0_0;
extern const Il2CppType Ext_Contains_m2286382361_gp_0_0_0_0;
extern const Il2CppType Ext_ToList_m3286762625_gp_0_0_0_0;
extern const Il2CppType Ext_Emit_m2311426299_gp_0_0_0_0;
extern const Il2CppType Ext_ToString_m3280318684_gp_0_0_0_0;
extern const Il2CppType HttpBase_Read_m191930573_gp_0_0_0_0;
extern const Il2CppType HttpServer_AddWebSocketService_m682758425_gp_0_0_0_0;
extern const Il2CppType HttpServer_AddWebSocketService_m2770388401_gp_0_0_0_0;
extern const Il2CppType HttpServer_U3CAddWebSocketService_1U3Em__0_m662434750_gp_0_0_0_0;
extern const Il2CppType WebSocketServer_AddWebSocketService_m2616226100_gp_0_0_0_0;
extern const Il2CppType WebSocketServer_AddWebSocketService_m1120545350_gp_0_0_0_0;
extern const Il2CppType WebSocketServer_U3CAddWebSocketService_1U3Em__0_m3359671309_gp_0_0_0_0;
extern const Il2CppType WebSocketServiceHost_1_t1698079305_gp_0_0_0_0;
extern const Il2CppType WebSocketServiceManager_Add_m3346916827_gp_0_0_0_0;
extern const Il2CppType OscTimeTag_t625345318_0_0_0;
extern const Il2CppType AnimationPlayableOutput_t260357453_0_0_0;
extern const Il2CppType DefaultExecutionOrder_t2717914595_0_0_0;
extern const Il2CppType AudioPlayableOutput_t3110750149_0_0_0;
extern const Il2CppType PlayerConnection_t3517219175_0_0_0;
extern const Il2CppType ScriptPlayableOutput_t1666207618_0_0_0;
extern const Il2CppType GUILayer_t3254902478_0_0_0;
extern const Il2CppType EventSystem_t3466835263_0_0_0;
extern const Il2CppType AxisEventData_t1524870173_0_0_0;
extern const Il2CppType SpriteRenderer_t1209076198_0_0_0;
extern const Il2CppType Image_t2042527209_0_0_0;
extern const Il2CppType Button_t2872111280_0_0_0;
extern const Il2CppType RawImage_t2749640213_0_0_0;
extern const Il2CppType Slider_t297367283_0_0_0;
extern const Il2CppType Scrollbar_t3248359358_0_0_0;
extern const Il2CppType InputField_t1631627530_0_0_0;
extern const Il2CppType ScrollRect_t1199013257_0_0_0;
extern const Il2CppType Dropdown_t1985816271_0_0_0;
extern const Il2CppType GraphicRaycaster_t410733016_0_0_0;
extern const Il2CppType CanvasRenderer_t261436805_0_0_0;
extern const Il2CppType Corner_t1077473318_0_0_0;
extern const Il2CppType Axis_t1431825778_0_0_0;
extern const Il2CppType Constraint_t3558160636_0_0_0;
extern const Il2CppType SubmitEvent_t907918422_0_0_0;
extern const Il2CppType OnChangeEvent_t2863344003_0_0_0;
extern const Il2CppType OnValidateInput_t1946318473_0_0_0;
extern const Il2CppType LayoutElement_t2808691390_0_0_0;
extern const Il2CppType RectOffset_t3387826427_0_0_0;
extern const Il2CppType TextAnchor_t112990806_0_0_0;
extern const Il2CppType AnimationTriggers_t3244928895_0_0_0;
extern const Il2CppType UnityARVideo_t2351297253_0_0_0;
extern const Il2CppType DontDestroyOnLoad_t3235789354_0_0_0;
extern const Il2CppType MeshFilter_t3026937449_0_0_0;
extern const Il2CppType MeshRenderer_t1268241104_0_0_0;
extern const Il2CppType AudioSource_t1135106623_0_0_0;
extern const Il2CppType Dictionary_2_t203617571_0_0_0;
extern const Il2CppType List_1_t1952926737_0_0_0;
extern const Il2CppType fsObjectAttribute_t2382840370_0_0_0;
extern const Il2CppType fsForwardAttribute_t3349051188_0_0_0;
extern const Il2CppType KeyframeU5BU5D_t449065829_0_0_0;
extern const Il2CppType WrapMode_t255797857_0_0_0;
extern const Il2CppType GradientAlphaKeyU5BU5D_t3410745760_0_0_0;
extern const Il2CppType GradientColorKeyU5BU5D_t3863936331_0_0_0;
extern const Il2CppType FlagsAttribute_t859561169_0_0_0;
extern const Il2CppType fsPropertyAttribute_t4237399860_0_0_0;
extern const Il2CppType WatsonCamera_t824577261_0_0_0;
extern const Il2CppType MicrophoneWidget_t2442369682_0_0_0;
extern const Il2CppType Problem_t2814813345_0_0_0;
extern const Il2CppType IFF_FORM_CHUNK_t624775701_0_0_0;
extern const Il2CppType IFF_CHUNK_t4256546638_0_0_0;
extern const Il2CppType WAV_PCM_t2882996802_0_0_0;
extern const Il2CppType BoxSlider_t1871650694_0_0_0;
extern const Il2CppType GenericDisplayDevice_t579608737_0_0_0;
extern const Il2CppType CameraLayer_t464507322_0_0_0;
extern const Il2CppType StandardInput_t4102879489_0_0_0;
extern const Il2CppType Light_t494725636_0_0_0;
extern const Il2CppType Rigidbody_t4233889191_0_0_0;




static const RuntimeType* GenInst_RuntimeObject_0_0_0_Types[] = { (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0 = { 1, GenInst_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0 = { 1, GenInst_Int32_t2071877448_0_0_0_Types };
static const RuntimeType* GenInst_Char_t3454481338_0_0_0_Types[] = { (&Char_t3454481338_0_0_0) };
extern const Il2CppGenericInst GenInst_Char_t3454481338_0_0_0 = { 1, GenInst_Char_t3454481338_0_0_0_Types };
static const RuntimeType* GenInst_Int64_t909078037_0_0_0_Types[] = { (&Int64_t909078037_0_0_0) };
extern const Il2CppGenericInst GenInst_Int64_t909078037_0_0_0 = { 1, GenInst_Int64_t909078037_0_0_0_Types };
static const RuntimeType* GenInst_UInt32_t2149682021_0_0_0_Types[] = { (&UInt32_t2149682021_0_0_0) };
extern const Il2CppGenericInst GenInst_UInt32_t2149682021_0_0_0 = { 1, GenInst_UInt32_t2149682021_0_0_0_Types };
static const RuntimeType* GenInst_UInt64_t2909196914_0_0_0_Types[] = { (&UInt64_t2909196914_0_0_0) };
extern const Il2CppGenericInst GenInst_UInt64_t2909196914_0_0_0 = { 1, GenInst_UInt64_t2909196914_0_0_0_Types };
static const RuntimeType* GenInst_Byte_t3683104436_0_0_0_Types[] = { (&Byte_t3683104436_0_0_0) };
extern const Il2CppGenericInst GenInst_Byte_t3683104436_0_0_0 = { 1, GenInst_Byte_t3683104436_0_0_0_Types };
static const RuntimeType* GenInst_SByte_t454417549_0_0_0_Types[] = { (&SByte_t454417549_0_0_0) };
extern const Il2CppGenericInst GenInst_SByte_t454417549_0_0_0 = { 1, GenInst_SByte_t454417549_0_0_0_Types };
static const RuntimeType* GenInst_Int16_t4041245914_0_0_0_Types[] = { (&Int16_t4041245914_0_0_0) };
extern const Il2CppGenericInst GenInst_Int16_t4041245914_0_0_0 = { 1, GenInst_Int16_t4041245914_0_0_0_Types };
static const RuntimeType* GenInst_UInt16_t986882611_0_0_0_Types[] = { (&UInt16_t986882611_0_0_0) };
extern const Il2CppGenericInst GenInst_UInt16_t986882611_0_0_0 = { 1, GenInst_UInt16_t986882611_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Types[] = { (&String_t_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0 = { 1, GenInst_String_t_0_0_0_Types };
static const RuntimeType* GenInst_IConvertible_t908092482_0_0_0_Types[] = { (&IConvertible_t908092482_0_0_0) };
extern const Il2CppGenericInst GenInst_IConvertible_t908092482_0_0_0 = { 1, GenInst_IConvertible_t908092482_0_0_0_Types };
static const RuntimeType* GenInst_IComparable_t1857082765_0_0_0_Types[] = { (&IComparable_t1857082765_0_0_0) };
extern const Il2CppGenericInst GenInst_IComparable_t1857082765_0_0_0 = { 1, GenInst_IComparable_t1857082765_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_t2911409499_0_0_0_Types[] = { (&IEnumerable_t2911409499_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_t2911409499_0_0_0 = { 1, GenInst_IEnumerable_t2911409499_0_0_0_Types };
static const RuntimeType* GenInst_ICloneable_t3853279282_0_0_0_Types[] = { (&ICloneable_t3853279282_0_0_0) };
extern const Il2CppGenericInst GenInst_ICloneable_t3853279282_0_0_0 = { 1, GenInst_ICloneable_t3853279282_0_0_0_Types };
static const RuntimeType* GenInst_IComparable_1_t3861059456_0_0_0_Types[] = { (&IComparable_1_t3861059456_0_0_0) };
extern const Il2CppGenericInst GenInst_IComparable_1_t3861059456_0_0_0 = { 1, GenInst_IComparable_1_t3861059456_0_0_0_Types };
static const RuntimeType* GenInst_IEquatable_1_t4233202402_0_0_0_Types[] = { (&IEquatable_1_t4233202402_0_0_0) };
extern const Il2CppGenericInst GenInst_IEquatable_1_t4233202402_0_0_0 = { 1, GenInst_IEquatable_1_t4233202402_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_Types[] = { (&Type_t_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0 = { 1, GenInst_Type_t_0_0_0_Types };
static const RuntimeType* GenInst_IReflect_t3412036974_0_0_0_Types[] = { (&IReflect_t3412036974_0_0_0) };
extern const Il2CppGenericInst GenInst_IReflect_t3412036974_0_0_0 = { 1, GenInst_IReflect_t3412036974_0_0_0_Types };
static const RuntimeType* GenInst__Type_t102776839_0_0_0_Types[] = { (&_Type_t102776839_0_0_0) };
extern const Il2CppGenericInst GenInst__Type_t102776839_0_0_0 = { 1, GenInst__Type_t102776839_0_0_0_Types };
static const RuntimeType* GenInst_MemberInfo_t_0_0_0_Types[] = { (&MemberInfo_t_0_0_0) };
extern const Il2CppGenericInst GenInst_MemberInfo_t_0_0_0 = { 1, GenInst_MemberInfo_t_0_0_0_Types };
static const RuntimeType* GenInst_ICustomAttributeProvider_t502202687_0_0_0_Types[] = { (&ICustomAttributeProvider_t502202687_0_0_0) };
extern const Il2CppGenericInst GenInst_ICustomAttributeProvider_t502202687_0_0_0 = { 1, GenInst_ICustomAttributeProvider_t502202687_0_0_0_Types };
static const RuntimeType* GenInst__MemberInfo_t332722161_0_0_0_Types[] = { (&_MemberInfo_t332722161_0_0_0) };
extern const Il2CppGenericInst GenInst__MemberInfo_t332722161_0_0_0 = { 1, GenInst__MemberInfo_t332722161_0_0_0_Types };
static const RuntimeType* GenInst_Double_t4078015681_0_0_0_Types[] = { (&Double_t4078015681_0_0_0) };
extern const Il2CppGenericInst GenInst_Double_t4078015681_0_0_0 = { 1, GenInst_Double_t4078015681_0_0_0_Types };
static const RuntimeType* GenInst_Single_t2076509932_0_0_0_Types[] = { (&Single_t2076509932_0_0_0) };
extern const Il2CppGenericInst GenInst_Single_t2076509932_0_0_0 = { 1, GenInst_Single_t2076509932_0_0_0_Types };
static const RuntimeType* GenInst_Decimal_t724701077_0_0_0_Types[] = { (&Decimal_t724701077_0_0_0) };
extern const Il2CppGenericInst GenInst_Decimal_t724701077_0_0_0 = { 1, GenInst_Decimal_t724701077_0_0_0_Types };
static const RuntimeType* GenInst_Boolean_t3825574718_0_0_0_Types[] = { (&Boolean_t3825574718_0_0_0) };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0 = { 1, GenInst_Boolean_t3825574718_0_0_0_Types };
static const RuntimeType* GenInst_Delegate_t3022476291_0_0_0_Types[] = { (&Delegate_t3022476291_0_0_0) };
extern const Il2CppGenericInst GenInst_Delegate_t3022476291_0_0_0 = { 1, GenInst_Delegate_t3022476291_0_0_0_Types };
static const RuntimeType* GenInst_ISerializable_t1245643778_0_0_0_Types[] = { (&ISerializable_t1245643778_0_0_0) };
extern const Il2CppGenericInst GenInst_ISerializable_t1245643778_0_0_0 = { 1, GenInst_ISerializable_t1245643778_0_0_0_Types };
static const RuntimeType* GenInst_ParameterInfo_t2249040075_0_0_0_Types[] = { (&ParameterInfo_t2249040075_0_0_0) };
extern const Il2CppGenericInst GenInst_ParameterInfo_t2249040075_0_0_0 = { 1, GenInst_ParameterInfo_t2249040075_0_0_0_Types };
static const RuntimeType* GenInst__ParameterInfo_t470209990_0_0_0_Types[] = { (&_ParameterInfo_t470209990_0_0_0) };
extern const Il2CppGenericInst GenInst__ParameterInfo_t470209990_0_0_0 = { 1, GenInst__ParameterInfo_t470209990_0_0_0_Types };
static const RuntimeType* GenInst_ParameterModifier_t1820634920_0_0_0_Types[] = { (&ParameterModifier_t1820634920_0_0_0) };
extern const Il2CppGenericInst GenInst_ParameterModifier_t1820634920_0_0_0 = { 1, GenInst_ParameterModifier_t1820634920_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_EventInfo_t_0_0_0_Types[] = { (&EventInfo_t_0_0_0) };
extern const Il2CppGenericInst GenInst_EventInfo_t_0_0_0 = { 1, GenInst_EventInfo_t_0_0_0_Types };
static const RuntimeType* GenInst__EventInfo_t2430923913_0_0_0_Types[] = { (&_EventInfo_t2430923913_0_0_0) };
extern const Il2CppGenericInst GenInst__EventInfo_t2430923913_0_0_0 = { 1, GenInst__EventInfo_t2430923913_0_0_0_Types };
static const RuntimeType* GenInst_FieldInfo_t_0_0_0_Types[] = { (&FieldInfo_t_0_0_0) };
extern const Il2CppGenericInst GenInst_FieldInfo_t_0_0_0 = { 1, GenInst_FieldInfo_t_0_0_0_Types };
static const RuntimeType* GenInst__FieldInfo_t2511231167_0_0_0_Types[] = { (&_FieldInfo_t2511231167_0_0_0) };
extern const Il2CppGenericInst GenInst__FieldInfo_t2511231167_0_0_0 = { 1, GenInst__FieldInfo_t2511231167_0_0_0_Types };
static const RuntimeType* GenInst_MethodInfo_t_0_0_0_Types[] = { (&MethodInfo_t_0_0_0) };
extern const Il2CppGenericInst GenInst_MethodInfo_t_0_0_0 = { 1, GenInst_MethodInfo_t_0_0_0_Types };
static const RuntimeType* GenInst__MethodInfo_t3642518830_0_0_0_Types[] = { (&_MethodInfo_t3642518830_0_0_0) };
extern const Il2CppGenericInst GenInst__MethodInfo_t3642518830_0_0_0 = { 1, GenInst__MethodInfo_t3642518830_0_0_0_Types };
static const RuntimeType* GenInst_MethodBase_t904190842_0_0_0_Types[] = { (&MethodBase_t904190842_0_0_0) };
extern const Il2CppGenericInst GenInst_MethodBase_t904190842_0_0_0 = { 1, GenInst_MethodBase_t904190842_0_0_0_Types };
static const RuntimeType* GenInst__MethodBase_t1935530873_0_0_0_Types[] = { (&_MethodBase_t1935530873_0_0_0) };
extern const Il2CppGenericInst GenInst__MethodBase_t1935530873_0_0_0 = { 1, GenInst__MethodBase_t1935530873_0_0_0_Types };
static const RuntimeType* GenInst_PropertyInfo_t_0_0_0_Types[] = { (&PropertyInfo_t_0_0_0) };
extern const Il2CppGenericInst GenInst_PropertyInfo_t_0_0_0 = { 1, GenInst_PropertyInfo_t_0_0_0_Types };
static const RuntimeType* GenInst__PropertyInfo_t1567586598_0_0_0_Types[] = { (&_PropertyInfo_t1567586598_0_0_0) };
extern const Il2CppGenericInst GenInst__PropertyInfo_t1567586598_0_0_0 = { 1, GenInst__PropertyInfo_t1567586598_0_0_0_Types };
static const RuntimeType* GenInst_ConstructorInfo_t2851816542_0_0_0_Types[] = { (&ConstructorInfo_t2851816542_0_0_0) };
extern const Il2CppGenericInst GenInst_ConstructorInfo_t2851816542_0_0_0 = { 1, GenInst_ConstructorInfo_t2851816542_0_0_0_Types };
static const RuntimeType* GenInst__ConstructorInfo_t3269099341_0_0_0_Types[] = { (&_ConstructorInfo_t3269099341_0_0_0) };
extern const Il2CppGenericInst GenInst__ConstructorInfo_t3269099341_0_0_0 = { 1, GenInst__ConstructorInfo_t3269099341_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_Types[] = { (&IntPtr_t_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0 = { 1, GenInst_IntPtr_t_0_0_0_Types };
static const RuntimeType* GenInst_TableRange_t2011406615_0_0_0_Types[] = { (&TableRange_t2011406615_0_0_0) };
extern const Il2CppGenericInst GenInst_TableRange_t2011406615_0_0_0 = { 1, GenInst_TableRange_t2011406615_0_0_0_Types };
static const RuntimeType* GenInst_TailoringInfo_t1449609243_0_0_0_Types[] = { (&TailoringInfo_t1449609243_0_0_0) };
extern const Il2CppGenericInst GenInst_TailoringInfo_t1449609243_0_0_0 = { 1, GenInst_TailoringInfo_t1449609243_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_Types[] = { (&String_t_0_0_0), (&Int32_t2071877448_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Int32_t2071877448_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Int32_t2071877448_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_RuntimeObject_0_0_0_Int32_t2071877448_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3716250094_0_0_0_Types[] = { (&KeyValuePair_2_t3716250094_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3716250094_0_0_0 = { 1, GenInst_KeyValuePair_2_t3716250094_0_0_0_Types };
static const RuntimeType* GenInst_Link_t2723257478_0_0_0_Types[] = { (&Link_t2723257478_0_0_0) };
extern const Il2CppGenericInst GenInst_Link_t2723257478_0_0_0 = { 1, GenInst_Link_t2723257478_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Int32_t2071877448_0_0_0_RuntimeObject_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Int32_t2071877448_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Int32_t2071877448_0_0_0_RuntimeObject_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_Int32_t2071877448_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Int32_t2071877448_0_0_0), (&Int32_t2071877448_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Int32_t2071877448_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t3048875398_0_0_0 = { 1, GenInst_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3716250094_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Int32_t2071877448_0_0_0), (&KeyValuePair_2_t3716250094_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3716250094_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3716250094_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&String_t_0_0_0), (&Int32_t2071877448_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t1744001932_0_0_0_Types[] = { (&String_t_0_0_0), (&Int32_t2071877448_0_0_0), (&KeyValuePair_2_t1744001932_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t1744001932_0_0_0 = { 3, GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t1744001932_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1744001932_0_0_0_Types[] = { (&KeyValuePair_2_t1744001932_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1744001932_0_0_0 = { 1, GenInst_KeyValuePair_2_t1744001932_0_0_0_Types };
static const RuntimeType* GenInst_Contraction_t1673853792_0_0_0_Types[] = { (&Contraction_t1673853792_0_0_0) };
extern const Il2CppGenericInst GenInst_Contraction_t1673853792_0_0_0 = { 1, GenInst_Contraction_t1673853792_0_0_0_Types };
static const RuntimeType* GenInst_Level2Map_t3322505726_0_0_0_Types[] = { (&Level2Map_t3322505726_0_0_0) };
extern const Il2CppGenericInst GenInst_Level2Map_t3322505726_0_0_0 = { 1, GenInst_Level2Map_t3322505726_0_0_0_Types };
static const RuntimeType* GenInst_BigInteger_t925946152_0_0_0_Types[] = { (&BigInteger_t925946152_0_0_0) };
extern const Il2CppGenericInst GenInst_BigInteger_t925946152_0_0_0 = { 1, GenInst_BigInteger_t925946152_0_0_0_Types };
static const RuntimeType* GenInst_KeySizes_t3144736271_0_0_0_Types[] = { (&KeySizes_t3144736271_0_0_0) };
extern const Il2CppGenericInst GenInst_KeySizes_t3144736271_0_0_0 = { 1, GenInst_KeySizes_t3144736271_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t38854645_0_0_0_Types[] = { (&KeyValuePair_2_t38854645_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t38854645_0_0_0 = { 1, GenInst_KeyValuePair_2_t38854645_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t38854645_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0), (&KeyValuePair_2_t38854645_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t38854645_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t38854645_0_0_0_Types };
static const RuntimeType* GenInst_Slot_t2022531261_0_0_0_Types[] = { (&Slot_t2022531261_0_0_0) };
extern const Il2CppGenericInst GenInst_Slot_t2022531261_0_0_0 = { 1, GenInst_Slot_t2022531261_0_0_0_Types };
static const RuntimeType* GenInst_Slot_t2267560602_0_0_0_Types[] = { (&Slot_t2267560602_0_0_0) };
extern const Il2CppGenericInst GenInst_Slot_t2267560602_0_0_0 = { 1, GenInst_Slot_t2267560602_0_0_0_Types };
static const RuntimeType* GenInst_StackFrame_t2050294881_0_0_0_Types[] = { (&StackFrame_t2050294881_0_0_0) };
extern const Il2CppGenericInst GenInst_StackFrame_t2050294881_0_0_0 = { 1, GenInst_StackFrame_t2050294881_0_0_0_Types };
static const RuntimeType* GenInst_Calendar_t585061108_0_0_0_Types[] = { (&Calendar_t585061108_0_0_0) };
extern const Il2CppGenericInst GenInst_Calendar_t585061108_0_0_0 = { 1, GenInst_Calendar_t585061108_0_0_0_Types };
static const RuntimeType* GenInst_DateTime_t693205669_0_0_0_Types[] = { (&DateTime_t693205669_0_0_0) };
extern const Il2CppGenericInst GenInst_DateTime_t693205669_0_0_0 = { 1, GenInst_DateTime_t693205669_0_0_0_Types };
static const RuntimeType* GenInst_ModuleBuilder_t4156028127_0_0_0_Types[] = { (&ModuleBuilder_t4156028127_0_0_0) };
extern const Il2CppGenericInst GenInst_ModuleBuilder_t4156028127_0_0_0 = { 1, GenInst_ModuleBuilder_t4156028127_0_0_0_Types };
static const RuntimeType* GenInst__ModuleBuilder_t1075102050_0_0_0_Types[] = { (&_ModuleBuilder_t1075102050_0_0_0) };
extern const Il2CppGenericInst GenInst__ModuleBuilder_t1075102050_0_0_0 = { 1, GenInst__ModuleBuilder_t1075102050_0_0_0_Types };
static const RuntimeType* GenInst_Module_t4282841206_0_0_0_Types[] = { (&Module_t4282841206_0_0_0) };
extern const Il2CppGenericInst GenInst_Module_t4282841206_0_0_0 = { 1, GenInst_Module_t4282841206_0_0_0_Types };
static const RuntimeType* GenInst__Module_t2144668161_0_0_0_Types[] = { (&_Module_t2144668161_0_0_0) };
extern const Il2CppGenericInst GenInst__Module_t2144668161_0_0_0 = { 1, GenInst__Module_t2144668161_0_0_0_Types };
static const RuntimeType* GenInst_ParameterBuilder_t3344728474_0_0_0_Types[] = { (&ParameterBuilder_t3344728474_0_0_0) };
extern const Il2CppGenericInst GenInst_ParameterBuilder_t3344728474_0_0_0 = { 1, GenInst_ParameterBuilder_t3344728474_0_0_0_Types };
static const RuntimeType* GenInst__ParameterBuilder_t2251638747_0_0_0_Types[] = { (&_ParameterBuilder_t2251638747_0_0_0) };
extern const Il2CppGenericInst GenInst__ParameterBuilder_t2251638747_0_0_0 = { 1, GenInst__ParameterBuilder_t2251638747_0_0_0_Types };
static const RuntimeType* GenInst_TypeU5BU5D_t1664964607_0_0_0_Types[] = { (&TypeU5BU5D_t1664964607_0_0_0) };
extern const Il2CppGenericInst GenInst_TypeU5BU5D_t1664964607_0_0_0 = { 1, GenInst_TypeU5BU5D_t1664964607_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeArray_0_0_0_Types[] = { (&RuntimeArray_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeArray_0_0_0 = { 1, GenInst_RuntimeArray_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_t91669223_0_0_0_Types[] = { (&ICollection_t91669223_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_t91669223_0_0_0 = { 1, GenInst_ICollection_t91669223_0_0_0_Types };
static const RuntimeType* GenInst_IList_t3321498491_0_0_0_Types[] = { (&IList_t3321498491_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_t3321498491_0_0_0 = { 1, GenInst_IList_t3321498491_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t1844743827_0_0_0_Types[] = { (&IList_1_t1844743827_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t1844743827_0_0_0 = { 1, GenInst_IList_1_t1844743827_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t2255878531_0_0_0_Types[] = { (&ICollection_1_t2255878531_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t2255878531_0_0_0 = { 1, GenInst_ICollection_1_t2255878531_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t1595930271_0_0_0_Types[] = { (&IEnumerable_1_t1595930271_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t1595930271_0_0_0 = { 1, GenInst_IEnumerable_1_t1595930271_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t3952977575_0_0_0_Types[] = { (&IList_1_t3952977575_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t3952977575_0_0_0 = { 1, GenInst_IList_1_t3952977575_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t69144983_0_0_0_Types[] = { (&ICollection_1_t69144983_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t69144983_0_0_0 = { 1, GenInst_ICollection_1_t69144983_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t3704164019_0_0_0_Types[] = { (&IEnumerable_1_t3704164019_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t3704164019_0_0_0 = { 1, GenInst_IEnumerable_1_t3704164019_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t643717440_0_0_0_Types[] = { (&IList_1_t643717440_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t643717440_0_0_0 = { 1, GenInst_IList_1_t643717440_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t1054852144_0_0_0_Types[] = { (&ICollection_1_t1054852144_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t1054852144_0_0_0 = { 1, GenInst_ICollection_1_t1054852144_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t394903884_0_0_0_Types[] = { (&IEnumerable_1_t394903884_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t394903884_0_0_0 = { 1, GenInst_IEnumerable_1_t394903884_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t289070565_0_0_0_Types[] = { (&IList_1_t289070565_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t289070565_0_0_0 = { 1, GenInst_IList_1_t289070565_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t700205269_0_0_0_Types[] = { (&ICollection_1_t700205269_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t700205269_0_0_0 = { 1, GenInst_ICollection_1_t700205269_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t40257009_0_0_0_Types[] = { (&IEnumerable_1_t40257009_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t40257009_0_0_0 = { 1, GenInst_IEnumerable_1_t40257009_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t1043143288_0_0_0_Types[] = { (&IList_1_t1043143288_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t1043143288_0_0_0 = { 1, GenInst_IList_1_t1043143288_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t1454277992_0_0_0_Types[] = { (&ICollection_1_t1454277992_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t1454277992_0_0_0 = { 1, GenInst_ICollection_1_t1454277992_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t794329732_0_0_0_Types[] = { (&IEnumerable_1_t794329732_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t794329732_0_0_0 = { 1, GenInst_IEnumerable_1_t794329732_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t873662762_0_0_0_Types[] = { (&IList_1_t873662762_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t873662762_0_0_0 = { 1, GenInst_IList_1_t873662762_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t1284797466_0_0_0_Types[] = { (&ICollection_1_t1284797466_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t1284797466_0_0_0 = { 1, GenInst_ICollection_1_t1284797466_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t624849206_0_0_0_Types[] = { (&IEnumerable_1_t624849206_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t624849206_0_0_0 = { 1, GenInst_IEnumerable_1_t624849206_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t3230389896_0_0_0_Types[] = { (&IList_1_t3230389896_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t3230389896_0_0_0 = { 1, GenInst_IList_1_t3230389896_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t3641524600_0_0_0_Types[] = { (&ICollection_1_t3641524600_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t3641524600_0_0_0 = { 1, GenInst_ICollection_1_t3641524600_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t2981576340_0_0_0_Types[] = { (&IEnumerable_1_t2981576340_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t2981576340_0_0_0 = { 1, GenInst_IEnumerable_1_t2981576340_0_0_0_Types };
static const RuntimeType* GenInst_ILTokenInfo_t149559338_0_0_0_Types[] = { (&ILTokenInfo_t149559338_0_0_0) };
extern const Il2CppGenericInst GenInst_ILTokenInfo_t149559338_0_0_0 = { 1, GenInst_ILTokenInfo_t149559338_0_0_0_Types };
static const RuntimeType* GenInst_LabelData_t3712112744_0_0_0_Types[] = { (&LabelData_t3712112744_0_0_0) };
extern const Il2CppGenericInst GenInst_LabelData_t3712112744_0_0_0 = { 1, GenInst_LabelData_t3712112744_0_0_0_Types };
static const RuntimeType* GenInst_LabelFixup_t4090909514_0_0_0_Types[] = { (&LabelFixup_t4090909514_0_0_0) };
extern const Il2CppGenericInst GenInst_LabelFixup_t4090909514_0_0_0 = { 1, GenInst_LabelFixup_t4090909514_0_0_0_Types };
static const RuntimeType* GenInst_GenericTypeParameterBuilder_t1370236603_0_0_0_Types[] = { (&GenericTypeParameterBuilder_t1370236603_0_0_0) };
extern const Il2CppGenericInst GenInst_GenericTypeParameterBuilder_t1370236603_0_0_0 = { 1, GenInst_GenericTypeParameterBuilder_t1370236603_0_0_0_Types };
static const RuntimeType* GenInst_TypeBuilder_t3308873219_0_0_0_Types[] = { (&TypeBuilder_t3308873219_0_0_0) };
extern const Il2CppGenericInst GenInst_TypeBuilder_t3308873219_0_0_0 = { 1, GenInst_TypeBuilder_t3308873219_0_0_0_Types };
static const RuntimeType* GenInst__TypeBuilder_t2783404358_0_0_0_Types[] = { (&_TypeBuilder_t2783404358_0_0_0) };
extern const Il2CppGenericInst GenInst__TypeBuilder_t2783404358_0_0_0 = { 1, GenInst__TypeBuilder_t2783404358_0_0_0_Types };
static const RuntimeType* GenInst_MethodBuilder_t644187984_0_0_0_Types[] = { (&MethodBuilder_t644187984_0_0_0) };
extern const Il2CppGenericInst GenInst_MethodBuilder_t644187984_0_0_0 = { 1, GenInst_MethodBuilder_t644187984_0_0_0_Types };
static const RuntimeType* GenInst__MethodBuilder_t3932949077_0_0_0_Types[] = { (&_MethodBuilder_t3932949077_0_0_0) };
extern const Il2CppGenericInst GenInst__MethodBuilder_t3932949077_0_0_0 = { 1, GenInst__MethodBuilder_t3932949077_0_0_0_Types };
static const RuntimeType* GenInst_ConstructorBuilder_t700974433_0_0_0_Types[] = { (&ConstructorBuilder_t700974433_0_0_0) };
extern const Il2CppGenericInst GenInst_ConstructorBuilder_t700974433_0_0_0 = { 1, GenInst_ConstructorBuilder_t700974433_0_0_0_Types };
static const RuntimeType* GenInst__ConstructorBuilder_t1236878896_0_0_0_Types[] = { (&_ConstructorBuilder_t1236878896_0_0_0) };
extern const Il2CppGenericInst GenInst__ConstructorBuilder_t1236878896_0_0_0 = { 1, GenInst__ConstructorBuilder_t1236878896_0_0_0_Types };
static const RuntimeType* GenInst_PropertyBuilder_t3694255912_0_0_0_Types[] = { (&PropertyBuilder_t3694255912_0_0_0) };
extern const Il2CppGenericInst GenInst_PropertyBuilder_t3694255912_0_0_0 = { 1, GenInst_PropertyBuilder_t3694255912_0_0_0_Types };
static const RuntimeType* GenInst__PropertyBuilder_t3341912621_0_0_0_Types[] = { (&_PropertyBuilder_t3341912621_0_0_0) };
extern const Il2CppGenericInst GenInst__PropertyBuilder_t3341912621_0_0_0 = { 1, GenInst__PropertyBuilder_t3341912621_0_0_0_Types };
static const RuntimeType* GenInst_FieldBuilder_t2784804005_0_0_0_Types[] = { (&FieldBuilder_t2784804005_0_0_0) };
extern const Il2CppGenericInst GenInst_FieldBuilder_t2784804005_0_0_0 = { 1, GenInst_FieldBuilder_t2784804005_0_0_0_Types };
static const RuntimeType* GenInst__FieldBuilder_t1895266044_0_0_0_Types[] = { (&_FieldBuilder_t1895266044_0_0_0) };
extern const Il2CppGenericInst GenInst__FieldBuilder_t1895266044_0_0_0 = { 1, GenInst__FieldBuilder_t1895266044_0_0_0_Types };
static const RuntimeType* GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0_Types[] = { (&CustomAttributeTypedArgument_t1498197914_0_0_0) };
extern const Il2CppGenericInst GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0 = { 1, GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0_Types };
static const RuntimeType* GenInst_CustomAttributeNamedArgument_t94157543_0_0_0_Types[] = { (&CustomAttributeNamedArgument_t94157543_0_0_0) };
extern const Il2CppGenericInst GenInst_CustomAttributeNamedArgument_t94157543_0_0_0 = { 1, GenInst_CustomAttributeNamedArgument_t94157543_0_0_0_Types };
static const RuntimeType* GenInst_CustomAttributeData_t3093286891_0_0_0_Types[] = { (&CustomAttributeData_t3093286891_0_0_0) };
extern const Il2CppGenericInst GenInst_CustomAttributeData_t3093286891_0_0_0 = { 1, GenInst_CustomAttributeData_t3093286891_0_0_0_Types };
static const RuntimeType* GenInst_ResourceInfo_t3933049236_0_0_0_Types[] = { (&ResourceInfo_t3933049236_0_0_0) };
extern const Il2CppGenericInst GenInst_ResourceInfo_t3933049236_0_0_0 = { 1, GenInst_ResourceInfo_t3933049236_0_0_0_Types };
static const RuntimeType* GenInst_ResourceCacheItem_t333236149_0_0_0_Types[] = { (&ResourceCacheItem_t333236149_0_0_0) };
extern const Il2CppGenericInst GenInst_ResourceCacheItem_t333236149_0_0_0 = { 1, GenInst_ResourceCacheItem_t333236149_0_0_0_Types };
static const RuntimeType* GenInst_IContextProperty_t287246399_0_0_0_Types[] = { (&IContextProperty_t287246399_0_0_0) };
extern const Il2CppGenericInst GenInst_IContextProperty_t287246399_0_0_0 = { 1, GenInst_IContextProperty_t287246399_0_0_0_Types };
static const RuntimeType* GenInst_Header_t2756440555_0_0_0_Types[] = { (&Header_t2756440555_0_0_0) };
extern const Il2CppGenericInst GenInst_Header_t2756440555_0_0_0 = { 1, GenInst_Header_t2756440555_0_0_0_Types };
static const RuntimeType* GenInst_ITrackingHandler_t2759960940_0_0_0_Types[] = { (&ITrackingHandler_t2759960940_0_0_0) };
extern const Il2CppGenericInst GenInst_ITrackingHandler_t2759960940_0_0_0 = { 1, GenInst_ITrackingHandler_t2759960940_0_0_0_Types };
static const RuntimeType* GenInst_IContextAttribute_t2439121372_0_0_0_Types[] = { (&IContextAttribute_t2439121372_0_0_0) };
extern const Il2CppGenericInst GenInst_IContextAttribute_t2439121372_0_0_0 = { 1, GenInst_IContextAttribute_t2439121372_0_0_0_Types };
static const RuntimeType* GenInst_TimeSpan_t3430258949_0_0_0_Types[] = { (&TimeSpan_t3430258949_0_0_0) };
extern const Il2CppGenericInst GenInst_TimeSpan_t3430258949_0_0_0 = { 1, GenInst_TimeSpan_t3430258949_0_0_0_Types };
static const RuntimeType* GenInst_TypeTag_t141209596_0_0_0_Types[] = { (&TypeTag_t141209596_0_0_0) };
extern const Il2CppGenericInst GenInst_TypeTag_t141209596_0_0_0 = { 1, GenInst_TypeTag_t141209596_0_0_0_Types };
static const RuntimeType* GenInst_MonoType_t_0_0_0_Types[] = { (&MonoType_t_0_0_0) };
extern const Il2CppGenericInst GenInst_MonoType_t_0_0_0 = { 1, GenInst_MonoType_t_0_0_0_Types };
static const RuntimeType* GenInst_StrongName_t2988747270_0_0_0_Types[] = { (&StrongName_t2988747270_0_0_0) };
extern const Il2CppGenericInst GenInst_StrongName_t2988747270_0_0_0 = { 1, GenInst_StrongName_t2988747270_0_0_0_Types };
static const RuntimeType* GenInst_IBuiltInEvidence_t1114073477_0_0_0_Types[] = { (&IBuiltInEvidence_t1114073477_0_0_0) };
extern const Il2CppGenericInst GenInst_IBuiltInEvidence_t1114073477_0_0_0 = { 1, GenInst_IBuiltInEvidence_t1114073477_0_0_0_Types };
static const RuntimeType* GenInst_IIdentityPermissionFactory_t2988326850_0_0_0_Types[] = { (&IIdentityPermissionFactory_t2988326850_0_0_0) };
extern const Il2CppGenericInst GenInst_IIdentityPermissionFactory_t2988326850_0_0_0 = { 1, GenInst_IIdentityPermissionFactory_t2988326850_0_0_0_Types };
static const RuntimeType* GenInst_Assembly_t4268412390_0_0_0_Types[] = { (&Assembly_t4268412390_0_0_0) };
extern const Il2CppGenericInst GenInst_Assembly_t4268412390_0_0_0 = { 1, GenInst_Assembly_t4268412390_0_0_0_Types };
static const RuntimeType* GenInst__Assembly_t2937922309_0_0_0_Types[] = { (&_Assembly_t2937922309_0_0_0) };
extern const Il2CppGenericInst GenInst__Assembly_t2937922309_0_0_0 = { 1, GenInst__Assembly_t2937922309_0_0_0_Types };
static const RuntimeType* GenInst_DateTimeOffset_t1362988906_0_0_0_Types[] = { (&DateTimeOffset_t1362988906_0_0_0) };
extern const Il2CppGenericInst GenInst_DateTimeOffset_t1362988906_0_0_0 = { 1, GenInst_DateTimeOffset_t1362988906_0_0_0_Types };
static const RuntimeType* GenInst_Guid_t_0_0_0_Types[] = { (&Guid_t_0_0_0) };
extern const Il2CppGenericInst GenInst_Guid_t_0_0_0 = { 1, GenInst_Guid_t_0_0_0_Types };
static const RuntimeType* GenInst_Version_t1755874712_0_0_0_Types[] = { (&Version_t1755874712_0_0_0) };
extern const Il2CppGenericInst GenInst_Version_t1755874712_0_0_0 = { 1, GenInst_Version_t1755874712_0_0_0_Types };
static const RuntimeType* GenInst_NetworkInterface_t63927633_0_0_0_Types[] = { (&NetworkInterface_t63927633_0_0_0) };
extern const Il2CppGenericInst GenInst_NetworkInterface_t63927633_0_0_0 = { 1, GenInst_NetworkInterface_t63927633_0_0_0_Types };
static const RuntimeType* GenInst_IPAddress_t1399971723_0_0_0_Types[] = { (&IPAddress_t1399971723_0_0_0) };
extern const Il2CppGenericInst GenInst_IPAddress_t1399971723_0_0_0 = { 1, GenInst_IPAddress_t1399971723_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_LinuxNetworkInterface_t3864470295_0_0_0_Types[] = { (&String_t_0_0_0), (&LinuxNetworkInterface_t3864470295_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_LinuxNetworkInterface_t3864470295_0_0_0 = { 2, GenInst_String_t_0_0_0_LinuxNetworkInterface_t3864470295_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_LinuxNetworkInterface_t3864470295_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&String_t_0_0_0), (&LinuxNetworkInterface_t3864470295_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_LinuxNetworkInterface_t3864470295_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_LinuxNetworkInterface_t3864470295_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_LinuxNetworkInterface_t3864470295_0_0_0_KeyValuePair_2_t3536594779_0_0_0_Types[] = { (&String_t_0_0_0), (&LinuxNetworkInterface_t3864470295_0_0_0), (&KeyValuePair_2_t3536594779_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_LinuxNetworkInterface_t3864470295_0_0_0_KeyValuePair_2_t3536594779_0_0_0 = { 3, GenInst_String_t_0_0_0_LinuxNetworkInterface_t3864470295_0_0_0_KeyValuePair_2_t3536594779_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3536594779_0_0_0_Types[] = { (&KeyValuePair_2_t3536594779_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3536594779_0_0_0 = { 1, GenInst_KeyValuePair_2_t3536594779_0_0_0_Types };
static const RuntimeType* GenInst_LinuxNetworkInterface_t3864470295_0_0_0_Types[] = { (&LinuxNetworkInterface_t3864470295_0_0_0) };
extern const Il2CppGenericInst GenInst_LinuxNetworkInterface_t3864470295_0_0_0 = { 1, GenInst_LinuxNetworkInterface_t3864470295_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_LinuxNetworkInterface_t3864470295_0_0_0_LinuxNetworkInterface_t3864470295_0_0_0_Types[] = { (&String_t_0_0_0), (&LinuxNetworkInterface_t3864470295_0_0_0), (&LinuxNetworkInterface_t3864470295_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_LinuxNetworkInterface_t3864470295_0_0_0_LinuxNetworkInterface_t3864470295_0_0_0 = { 3, GenInst_String_t_0_0_0_LinuxNetworkInterface_t3864470295_0_0_0_LinuxNetworkInterface_t3864470295_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_MacOsNetworkInterface_t1454185290_0_0_0_Types[] = { (&String_t_0_0_0), (&MacOsNetworkInterface_t1454185290_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_MacOsNetworkInterface_t1454185290_0_0_0 = { 2, GenInst_String_t_0_0_0_MacOsNetworkInterface_t1454185290_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_MacOsNetworkInterface_t1454185290_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&String_t_0_0_0), (&MacOsNetworkInterface_t1454185290_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_MacOsNetworkInterface_t1454185290_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_MacOsNetworkInterface_t1454185290_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_MacOsNetworkInterface_t1454185290_0_0_0_KeyValuePair_2_t1126309774_0_0_0_Types[] = { (&String_t_0_0_0), (&MacOsNetworkInterface_t1454185290_0_0_0), (&KeyValuePair_2_t1126309774_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_MacOsNetworkInterface_t1454185290_0_0_0_KeyValuePair_2_t1126309774_0_0_0 = { 3, GenInst_String_t_0_0_0_MacOsNetworkInterface_t1454185290_0_0_0_KeyValuePair_2_t1126309774_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1126309774_0_0_0_Types[] = { (&KeyValuePair_2_t1126309774_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1126309774_0_0_0 = { 1, GenInst_KeyValuePair_2_t1126309774_0_0_0_Types };
static const RuntimeType* GenInst_MacOsNetworkInterface_t1454185290_0_0_0_Types[] = { (&MacOsNetworkInterface_t1454185290_0_0_0) };
extern const Il2CppGenericInst GenInst_MacOsNetworkInterface_t1454185290_0_0_0 = { 1, GenInst_MacOsNetworkInterface_t1454185290_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_MacOsNetworkInterface_t1454185290_0_0_0_MacOsNetworkInterface_t1454185290_0_0_0_Types[] = { (&String_t_0_0_0), (&MacOsNetworkInterface_t1454185290_0_0_0), (&MacOsNetworkInterface_t1454185290_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_MacOsNetworkInterface_t1454185290_0_0_0_MacOsNetworkInterface_t1454185290_0_0_0 = { 3, GenInst_String_t_0_0_0_MacOsNetworkInterface_t1454185290_0_0_0_MacOsNetworkInterface_t1454185290_0_0_0_Types };
static const RuntimeType* GenInst_Win32_IP_ADAPTER_ADDRESSES_t680756680_0_0_0_Types[] = { (&Win32_IP_ADAPTER_ADDRESSES_t680756680_0_0_0) };
extern const Il2CppGenericInst GenInst_Win32_IP_ADAPTER_ADDRESSES_t680756680_0_0_0 = { 1, GenInst_Win32_IP_ADAPTER_ADDRESSES_t680756680_0_0_0_Types };
static const RuntimeType* GenInst_X509ChainStatus_t4278378721_0_0_0_Types[] = { (&X509ChainStatus_t4278378721_0_0_0) };
extern const Il2CppGenericInst GenInst_X509ChainStatus_t4278378721_0_0_0 = { 1, GenInst_X509ChainStatus_t4278378721_0_0_0_Types };
static const RuntimeType* GenInst_ArraySegment_1_t2594217482_0_0_0_Types[] = { (&ArraySegment_1_t2594217482_0_0_0) };
extern const Il2CppGenericInst GenInst_ArraySegment_1_t2594217482_0_0_0 = { 1, GenInst_ArraySegment_1_t2594217482_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { (&String_t_0_0_0), (&Boolean_t3825574718_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Boolean_t3825574718_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_RuntimeObject_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1174980068_0_0_0_Types[] = { (&KeyValuePair_2_t1174980068_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1174980068_0_0_0 = { 1, GenInst_KeyValuePair_2_t1174980068_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Boolean_t3825574718_0_0_0_RuntimeObject_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Boolean_t3825574718_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Boolean_t3825574718_0_0_0_RuntimeObject_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_Boolean_t3825574718_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Boolean_t3825574718_0_0_0), (&Boolean_t3825574718_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Boolean_t3825574718_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Boolean_t3825574718_0_0_0_KeyValuePair_2_t1174980068_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Boolean_t3825574718_0_0_0), (&KeyValuePair_2_t1174980068_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Boolean_t3825574718_0_0_0_KeyValuePair_2_t1174980068_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_Boolean_t3825574718_0_0_0_KeyValuePair_2_t1174980068_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&String_t_0_0_0), (&Boolean_t3825574718_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_KeyValuePair_2_t3497699202_0_0_0_Types[] = { (&String_t_0_0_0), (&Boolean_t3825574718_0_0_0), (&KeyValuePair_2_t3497699202_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_KeyValuePair_2_t3497699202_0_0_0 = { 3, GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_KeyValuePair_2_t3497699202_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3497699202_0_0_0_Types[] = { (&KeyValuePair_2_t3497699202_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3497699202_0_0_0 = { 1, GenInst_KeyValuePair_2_t3497699202_0_0_0_Types };
static const RuntimeType* GenInst_X509Certificate_t283079845_0_0_0_Types[] = { (&X509Certificate_t283079845_0_0_0) };
extern const Il2CppGenericInst GenInst_X509Certificate_t283079845_0_0_0 = { 1, GenInst_X509Certificate_t283079845_0_0_0_Types };
static const RuntimeType* GenInst_IDeserializationCallback_t327125377_0_0_0_Types[] = { (&IDeserializationCallback_t327125377_0_0_0) };
extern const Il2CppGenericInst GenInst_IDeserializationCallback_t327125377_0_0_0 = { 1, GenInst_IDeserializationCallback_t327125377_0_0_0_Types };
static const RuntimeType* GenInst_Capture_t4157900610_0_0_0_Types[] = { (&Capture_t4157900610_0_0_0) };
extern const Il2CppGenericInst GenInst_Capture_t4157900610_0_0_0 = { 1, GenInst_Capture_t4157900610_0_0_0_Types };
static const RuntimeType* GenInst_Group_t3761430853_0_0_0_Types[] = { (&Group_t3761430853_0_0_0) };
extern const Il2CppGenericInst GenInst_Group_t3761430853_0_0_0 = { 1, GenInst_Group_t3761430853_0_0_0_Types };
static const RuntimeType* GenInst_Mark_t2724874473_0_0_0_Types[] = { (&Mark_t2724874473_0_0_0) };
extern const Il2CppGenericInst GenInst_Mark_t2724874473_0_0_0 = { 1, GenInst_Mark_t2724874473_0_0_0_Types };
static const RuntimeType* GenInst_UriScheme_t1876590943_0_0_0_Types[] = { (&UriScheme_t1876590943_0_0_0) };
extern const Il2CppGenericInst GenInst_UriScheme_t1876590943_0_0_0 = { 1, GenInst_UriScheme_t1876590943_0_0_0_Types };
static const RuntimeType* GenInst_BigInteger_t925946153_0_0_0_Types[] = { (&BigInteger_t925946153_0_0_0) };
extern const Il2CppGenericInst GenInst_BigInteger_t925946153_0_0_0 = { 1, GenInst_BigInteger_t925946153_0_0_0_Types };
static const RuntimeType* GenInst_ByteU5BU5D_t3397334013_0_0_0_Types[] = { (&ByteU5BU5D_t3397334013_0_0_0) };
extern const Il2CppGenericInst GenInst_ByteU5BU5D_t3397334013_0_0_0 = { 1, GenInst_ByteU5BU5D_t3397334013_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t4224045037_0_0_0_Types[] = { (&IList_1_t4224045037_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t4224045037_0_0_0 = { 1, GenInst_IList_1_t4224045037_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t340212445_0_0_0_Types[] = { (&ICollection_1_t340212445_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t340212445_0_0_0 = { 1, GenInst_ICollection_1_t340212445_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t3975231481_0_0_0_Types[] = { (&IEnumerable_1_t3975231481_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t3975231481_0_0_0 = { 1, GenInst_IEnumerable_1_t3975231481_0_0_0_Types };
static const RuntimeType* GenInst_ClientCertificateType_t4001384466_0_0_0_Types[] = { (&ClientCertificateType_t4001384466_0_0_0) };
extern const Il2CppGenericInst GenInst_ClientCertificateType_t4001384466_0_0_0 = { 1, GenInst_ClientCertificateType_t4001384466_0_0_0_Types };
static const RuntimeType* GenInst_Link_t865133271_0_0_0_Types[] = { (&Link_t865133271_0_0_0) };
extern const Il2CppGenericInst GenInst_Link_t865133271_0_0_0 = { 1, GenInst_Link_t865133271_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0 = { 4, GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_OscPacketReceivedEventArgs_t3282045269_0_0_0_Types[] = { (&OscPacketReceivedEventArgs_t3282045269_0_0_0) };
extern const Il2CppGenericInst GenInst_OscPacketReceivedEventArgs_t3282045269_0_0_0 = { 1, GenInst_OscPacketReceivedEventArgs_t3282045269_0_0_0_Types };
static const RuntimeType* GenInst_OscBundleReceivedEventArgs_t3673224441_0_0_0_Types[] = { (&OscBundleReceivedEventArgs_t3673224441_0_0_0) };
extern const Il2CppGenericInst GenInst_OscBundleReceivedEventArgs_t3673224441_0_0_0 = { 1, GenInst_OscBundleReceivedEventArgs_t3673224441_0_0_0_Types };
static const RuntimeType* GenInst_ExceptionEventArgs_t892713536_0_0_0_Types[] = { (&ExceptionEventArgs_t892713536_0_0_0) };
extern const Il2CppGenericInst GenInst_ExceptionEventArgs_t892713536_0_0_0 = { 1, GenInst_ExceptionEventArgs_t892713536_0_0_0_Types };
static const RuntimeType* GenInst_OscMessageReceivedEventArgs_t1263041860_0_0_0_Types[] = { (&OscMessageReceivedEventArgs_t1263041860_0_0_0) };
extern const Il2CppGenericInst GenInst_OscMessageReceivedEventArgs_t1263041860_0_0_0 = { 1, GenInst_OscMessageReceivedEventArgs_t1263041860_0_0_0_Types };
static const RuntimeType* GenInst_XPathNavigator_t3981235968_0_0_0_Types[] = { (&XPathNavigator_t3981235968_0_0_0) };
extern const Il2CppGenericInst GenInst_XPathNavigator_t3981235968_0_0_0 = { 1, GenInst_XPathNavigator_t3981235968_0_0_0_Types };
static const RuntimeType* GenInst_IXPathNavigable_t845515791_0_0_0_Types[] = { (&IXPathNavigable_t845515791_0_0_0) };
extern const Il2CppGenericInst GenInst_IXPathNavigable_t845515791_0_0_0 = { 1, GenInst_IXPathNavigable_t845515791_0_0_0_Types };
static const RuntimeType* GenInst_IXmlNamespaceResolver_t3928241465_0_0_0_Types[] = { (&IXmlNamespaceResolver_t3928241465_0_0_0) };
extern const Il2CppGenericInst GenInst_IXmlNamespaceResolver_t3928241465_0_0_0 = { 1, GenInst_IXmlNamespaceResolver_t3928241465_0_0_0_Types };
static const RuntimeType* GenInst_XPathItem_t3130801258_0_0_0_Types[] = { (&XPathItem_t3130801258_0_0_0) };
extern const Il2CppGenericInst GenInst_XPathItem_t3130801258_0_0_0 = { 1, GenInst_XPathItem_t3130801258_0_0_0_Types };
static const RuntimeType* GenInst_XPathResultType_t1521569578_0_0_0_Types[] = { (&XPathResultType_t1521569578_0_0_0) };
extern const Il2CppGenericInst GenInst_XPathResultType_t1521569578_0_0_0 = { 1, GenInst_XPathResultType_t1521569578_0_0_0_Types };
static const RuntimeType* GenInst_Entry_t2583369454_0_0_0_Types[] = { (&Entry_t2583369454_0_0_0) };
extern const Il2CppGenericInst GenInst_Entry_t2583369454_0_0_0 = { 1, GenInst_Entry_t2583369454_0_0_0_Types };
static const RuntimeType* GenInst_NsDecl_t3210081295_0_0_0_Types[] = { (&NsDecl_t3210081295_0_0_0) };
extern const Il2CppGenericInst GenInst_NsDecl_t3210081295_0_0_0 = { 1, GenInst_NsDecl_t3210081295_0_0_0_Types };
static const RuntimeType* GenInst_Object_t1021602117_0_0_0_Types[] = { (&Object_t1021602117_0_0_0) };
extern const Il2CppGenericInst GenInst_Object_t1021602117_0_0_0 = { 1, GenInst_Object_t1021602117_0_0_0_Types };
static const RuntimeType* GenInst_Camera_t189460977_0_0_0_Types[] = { (&Camera_t189460977_0_0_0) };
extern const Il2CppGenericInst GenInst_Camera_t189460977_0_0_0 = { 1, GenInst_Camera_t189460977_0_0_0_Types };
static const RuntimeType* GenInst_Behaviour_t955675639_0_0_0_Types[] = { (&Behaviour_t955675639_0_0_0) };
extern const Il2CppGenericInst GenInst_Behaviour_t955675639_0_0_0 = { 1, GenInst_Behaviour_t955675639_0_0_0_Types };
static const RuntimeType* GenInst_Component_t3819376471_0_0_0_Types[] = { (&Component_t3819376471_0_0_0) };
extern const Il2CppGenericInst GenInst_Component_t3819376471_0_0_0 = { 1, GenInst_Component_t3819376471_0_0_0_Types };
static const RuntimeType* GenInst_Display_t3666191348_0_0_0_Types[] = { (&Display_t3666191348_0_0_0) };
extern const Il2CppGenericInst GenInst_Display_t3666191348_0_0_0 = { 1, GenInst_Display_t3666191348_0_0_0_Types };
static const RuntimeType* GenInst_GradientColorKey_t1677506814_0_0_0_Types[] = { (&GradientColorKey_t1677506814_0_0_0) };
extern const Il2CppGenericInst GenInst_GradientColorKey_t1677506814_0_0_0 = { 1, GenInst_GradientColorKey_t1677506814_0_0_0_Types };
static const RuntimeType* GenInst_GradientAlphaKey_t648783245_0_0_0_Types[] = { (&GradientAlphaKey_t648783245_0_0_0) };
extern const Il2CppGenericInst GenInst_GradientAlphaKey_t648783245_0_0_0 = { 1, GenInst_GradientAlphaKey_t648783245_0_0_0_Types };
static const RuntimeType* GenInst_Keyframe_t1449471340_0_0_0_Types[] = { (&Keyframe_t1449471340_0_0_0) };
extern const Il2CppGenericInst GenInst_Keyframe_t1449471340_0_0_0 = { 1, GenInst_Keyframe_t1449471340_0_0_0_Types };
static const RuntimeType* GenInst_Vector3_t2243707580_0_0_0_Types[] = { (&Vector3_t2243707580_0_0_0) };
extern const Il2CppGenericInst GenInst_Vector3_t2243707580_0_0_0 = { 1, GenInst_Vector3_t2243707580_0_0_0_Types };
static const RuntimeType* GenInst_Vector4_t2243707581_0_0_0_Types[] = { (&Vector4_t2243707581_0_0_0) };
extern const Il2CppGenericInst GenInst_Vector4_t2243707581_0_0_0 = { 1, GenInst_Vector4_t2243707581_0_0_0_Types };
static const RuntimeType* GenInst_Vector2_t2243707579_0_0_0_Types[] = { (&Vector2_t2243707579_0_0_0) };
extern const Il2CppGenericInst GenInst_Vector2_t2243707579_0_0_0 = { 1, GenInst_Vector2_t2243707579_0_0_0_Types };
static const RuntimeType* GenInst_Color32_t874517518_0_0_0_Types[] = { (&Color32_t874517518_0_0_0) };
extern const Il2CppGenericInst GenInst_Color32_t874517518_0_0_0 = { 1, GenInst_Color32_t874517518_0_0_0_Types };
static const RuntimeType* GenInst_Playable_t1896841784_0_0_0_Types[] = { (&Playable_t1896841784_0_0_0) };
extern const Il2CppGenericInst GenInst_Playable_t1896841784_0_0_0 = { 1, GenInst_Playable_t1896841784_0_0_0_Types };
static const RuntimeType* GenInst_PlayableOutput_t988259697_0_0_0_Types[] = { (&PlayableOutput_t988259697_0_0_0) };
extern const Il2CppGenericInst GenInst_PlayableOutput_t988259697_0_0_0 = { 1, GenInst_PlayableOutput_t988259697_0_0_0_Types };
static const RuntimeType* GenInst_Scene_t1684909666_0_0_0_LoadSceneMode_t2981886439_0_0_0_Types[] = { (&Scene_t1684909666_0_0_0), (&LoadSceneMode_t2981886439_0_0_0) };
extern const Il2CppGenericInst GenInst_Scene_t1684909666_0_0_0_LoadSceneMode_t2981886439_0_0_0 = { 2, GenInst_Scene_t1684909666_0_0_0_LoadSceneMode_t2981886439_0_0_0_Types };
static const RuntimeType* GenInst_Scene_t1684909666_0_0_0_Types[] = { (&Scene_t1684909666_0_0_0) };
extern const Il2CppGenericInst GenInst_Scene_t1684909666_0_0_0 = { 1, GenInst_Scene_t1684909666_0_0_0_Types };
static const RuntimeType* GenInst_Scene_t1684909666_0_0_0_Scene_t1684909666_0_0_0_Types[] = { (&Scene_t1684909666_0_0_0), (&Scene_t1684909666_0_0_0) };
extern const Il2CppGenericInst GenInst_Scene_t1684909666_0_0_0_Scene_t1684909666_0_0_0 = { 2, GenInst_Scene_t1684909666_0_0_0_Scene_t1684909666_0_0_0_Types };
static const RuntimeType* GenInst_SpriteAtlas_t3132429450_0_0_0_Types[] = { (&SpriteAtlas_t3132429450_0_0_0) };
extern const Il2CppGenericInst GenInst_SpriteAtlas_t3132429450_0_0_0 = { 1, GenInst_SpriteAtlas_t3132429450_0_0_0_Types };
static const RuntimeType* GenInst_Particle_t250075699_0_0_0_Types[] = { (&Particle_t250075699_0_0_0) };
extern const Il2CppGenericInst GenInst_Particle_t250075699_0_0_0 = { 1, GenInst_Particle_t250075699_0_0_0_Types };
static const RuntimeType* GenInst_ContactPoint_t1376425630_0_0_0_Types[] = { (&ContactPoint_t1376425630_0_0_0) };
extern const Il2CppGenericInst GenInst_ContactPoint_t1376425630_0_0_0 = { 1, GenInst_ContactPoint_t1376425630_0_0_0_Types };
static const RuntimeType* GenInst_RaycastHit_t87180320_0_0_0_Types[] = { (&RaycastHit_t87180320_0_0_0) };
extern const Il2CppGenericInst GenInst_RaycastHit_t87180320_0_0_0 = { 1, GenInst_RaycastHit_t87180320_0_0_0_Types };
static const RuntimeType* GenInst_Rigidbody2D_t502193897_0_0_0_Types[] = { (&Rigidbody2D_t502193897_0_0_0) };
extern const Il2CppGenericInst GenInst_Rigidbody2D_t502193897_0_0_0 = { 1, GenInst_Rigidbody2D_t502193897_0_0_0_Types };
static const RuntimeType* GenInst_RaycastHit2D_t4063908774_0_0_0_Types[] = { (&RaycastHit2D_t4063908774_0_0_0) };
extern const Il2CppGenericInst GenInst_RaycastHit2D_t4063908774_0_0_0 = { 1, GenInst_RaycastHit2D_t4063908774_0_0_0_Types };
static const RuntimeType* GenInst_ContactPoint2D_t3659330976_0_0_0_Types[] = { (&ContactPoint2D_t3659330976_0_0_0) };
extern const Il2CppGenericInst GenInst_ContactPoint2D_t3659330976_0_0_0 = { 1, GenInst_ContactPoint2D_t3659330976_0_0_0_Types };
static const RuntimeType* GenInst_AudioClipPlayable_t192218916_0_0_0_Types[] = { (&AudioClipPlayable_t192218916_0_0_0) };
extern const Il2CppGenericInst GenInst_AudioClipPlayable_t192218916_0_0_0 = { 1, GenInst_AudioClipPlayable_t192218916_0_0_0_Types };
static const RuntimeType* GenInst_AudioMixerPlayable_t1831808911_0_0_0_Types[] = { (&AudioMixerPlayable_t1831808911_0_0_0) };
extern const Il2CppGenericInst GenInst_AudioMixerPlayable_t1831808911_0_0_0 = { 1, GenInst_AudioMixerPlayable_t1831808911_0_0_0_Types };
static const RuntimeType* GenInst_WebCamDevice_t3983871389_0_0_0_Types[] = { (&WebCamDevice_t3983871389_0_0_0) };
extern const Il2CppGenericInst GenInst_WebCamDevice_t3983871389_0_0_0 = { 1, GenInst_WebCamDevice_t3983871389_0_0_0_Types };
static const RuntimeType* GenInst_AnimationClipPlayable_t4099382200_0_0_0_Types[] = { (&AnimationClipPlayable_t4099382200_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimationClipPlayable_t4099382200_0_0_0 = { 1, GenInst_AnimationClipPlayable_t4099382200_0_0_0_Types };
static const RuntimeType* GenInst_AnimationLayerMixerPlayable_t3057952312_0_0_0_Types[] = { (&AnimationLayerMixerPlayable_t3057952312_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimationLayerMixerPlayable_t3057952312_0_0_0 = { 1, GenInst_AnimationLayerMixerPlayable_t3057952312_0_0_0_Types };
static const RuntimeType* GenInst_AnimationMixerPlayable_t1343787797_0_0_0_Types[] = { (&AnimationMixerPlayable_t1343787797_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimationMixerPlayable_t1343787797_0_0_0 = { 1, GenInst_AnimationMixerPlayable_t1343787797_0_0_0_Types };
static const RuntimeType* GenInst_AnimationOffsetPlayable_t1019600543_0_0_0_Types[] = { (&AnimationOffsetPlayable_t1019600543_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimationOffsetPlayable_t1019600543_0_0_0 = { 1, GenInst_AnimationOffsetPlayable_t1019600543_0_0_0_Types };
static const RuntimeType* GenInst_AnimatorControllerPlayable_t1744083903_0_0_0_Types[] = { (&AnimatorControllerPlayable_t1744083903_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimatorControllerPlayable_t1744083903_0_0_0 = { 1, GenInst_AnimatorControllerPlayable_t1744083903_0_0_0_Types };
static const RuntimeType* GenInst_AnimatorClipInfo_t3905751349_0_0_0_Types[] = { (&AnimatorClipInfo_t3905751349_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimatorClipInfo_t3905751349_0_0_0 = { 1, GenInst_AnimatorClipInfo_t3905751349_0_0_0_Types };
static const RuntimeType* GenInst_AnimatorControllerParameter_t1381019216_0_0_0_Types[] = { (&AnimatorControllerParameter_t1381019216_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimatorControllerParameter_t1381019216_0_0_0 = { 1, GenInst_AnimatorControllerParameter_t1381019216_0_0_0_Types };
static const RuntimeType* GenInst_UIVertex_t1204258818_0_0_0_Types[] = { (&UIVertex_t1204258818_0_0_0) };
extern const Il2CppGenericInst GenInst_UIVertex_t1204258818_0_0_0 = { 1, GenInst_UIVertex_t1204258818_0_0_0_Types };
static const RuntimeType* GenInst_UICharInfo_t3056636800_0_0_0_Types[] = { (&UICharInfo_t3056636800_0_0_0) };
extern const Il2CppGenericInst GenInst_UICharInfo_t3056636800_0_0_0 = { 1, GenInst_UICharInfo_t3056636800_0_0_0_Types };
static const RuntimeType* GenInst_UILineInfo_t3621277874_0_0_0_Types[] = { (&UILineInfo_t3621277874_0_0_0) };
extern const Il2CppGenericInst GenInst_UILineInfo_t3621277874_0_0_0 = { 1, GenInst_UILineInfo_t3621277874_0_0_0_Types };
static const RuntimeType* GenInst_Font_t4239498691_0_0_0_Types[] = { (&Font_t4239498691_0_0_0) };
extern const Il2CppGenericInst GenInst_Font_t4239498691_0_0_0 = { 1, GenInst_Font_t4239498691_0_0_0_Types };
static const RuntimeType* GenInst_GUILayoutOption_t4183744904_0_0_0_Types[] = { (&GUILayoutOption_t4183744904_0_0_0) };
extern const Il2CppGenericInst GenInst_GUILayoutOption_t4183744904_0_0_0 = { 1, GenInst_GUILayoutOption_t4183744904_0_0_0_Types };
static const RuntimeType* GenInst_GUILayoutEntry_t3828586629_0_0_0_Types[] = { (&GUILayoutEntry_t3828586629_0_0_0) };
extern const Il2CppGenericInst GenInst_GUILayoutEntry_t3828586629_0_0_0 = { 1, GenInst_GUILayoutEntry_t3828586629_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&LayoutCache_t3120781045_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_RuntimeObject_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3749587448_0_0_0_Types[] = { (&KeyValuePair_2_t3749587448_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3749587448_0_0_0 = { 1, GenInst_KeyValuePair_2_t3749587448_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_RuntimeObject_0_0_0_Int32_t2071877448_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&RuntimeObject_0_0_0), (&Int32_t2071877448_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_RuntimeObject_0_0_0_Int32_t2071877448_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_RuntimeObject_0_0_0_Int32_t2071877448_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&RuntimeObject_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t3749587448_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&RuntimeObject_0_0_0), (&KeyValuePair_2_t3749587448_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t3749587448_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t3749587448_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&LayoutCache_t3120781045_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_KeyValuePair_2_t4180919198_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&LayoutCache_t3120781045_0_0_0), (&KeyValuePair_2_t4180919198_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_KeyValuePair_2_t4180919198_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_KeyValuePair_2_t4180919198_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t4180919198_0_0_0_Types[] = { (&KeyValuePair_2_t4180919198_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4180919198_0_0_0 = { 1, GenInst_KeyValuePair_2_t4180919198_0_0_0_Types };
static const RuntimeType* GenInst_GUIStyle_t1799908754_0_0_0_Types[] = { (&GUIStyle_t1799908754_0_0_0) };
extern const Il2CppGenericInst GenInst_GUIStyle_t1799908754_0_0_0 = { 1, GenInst_GUIStyle_t1799908754_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_Types[] = { (&String_t_0_0_0), (&GUIStyle_t1799908754_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0 = { 2, GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&String_t_0_0_0), (&GUIStyle_t1799908754_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_KeyValuePair_2_t1472033238_0_0_0_Types[] = { (&String_t_0_0_0), (&GUIStyle_t1799908754_0_0_0), (&KeyValuePair_2_t1472033238_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_KeyValuePair_2_t1472033238_0_0_0 = { 3, GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_KeyValuePair_2_t1472033238_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1472033238_0_0_0_Types[] = { (&KeyValuePair_2_t1472033238_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1472033238_0_0_0 = { 1, GenInst_KeyValuePair_2_t1472033238_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_GUIStyle_t1799908754_0_0_0_Types[] = { (&String_t_0_0_0), (&GUIStyle_t1799908754_0_0_0), (&GUIStyle_t1799908754_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_GUIStyle_t1799908754_0_0_0 = { 3, GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_GUIStyle_t1799908754_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_IntPtr_t_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&IntPtr_t_0_0_0), (&Boolean_t3825574718_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_IntPtr_t_0_0_0_Boolean_t3825574718_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_IntPtr_t_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const RuntimeType* GenInst_Exception_t1927440687_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { (&Exception_t1927440687_0_0_0), (&Boolean_t3825574718_0_0_0) };
extern const Il2CppGenericInst GenInst_Exception_t1927440687_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Exception_t1927440687_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_String_t_0_0_0_Types[] = { (&String_t_0_0_0), (&String_t_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0 = { 2, GenInst_String_t_0_0_0_String_t_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&String_t_0_0_0), (&String_t_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_String_t_0_0_0_KeyValuePair_2_t1701344717_0_0_0_Types[] = { (&String_t_0_0_0), (&String_t_0_0_0), (&KeyValuePair_2_t1701344717_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0_KeyValuePair_2_t1701344717_0_0_0 = { 3, GenInst_String_t_0_0_0_String_t_0_0_0_KeyValuePair_2_t1701344717_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1701344717_0_0_0_Types[] = { (&KeyValuePair_2_t1701344717_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1701344717_0_0_0 = { 1, GenInst_KeyValuePair_2_t1701344717_0_0_0_Types };
static const RuntimeType* GenInst_UserProfile_t3365630962_0_0_0_Types[] = { (&UserProfile_t3365630962_0_0_0) };
extern const Il2CppGenericInst GenInst_UserProfile_t3365630962_0_0_0 = { 1, GenInst_UserProfile_t3365630962_0_0_0_Types };
static const RuntimeType* GenInst_IUserProfile_t4108565527_0_0_0_Types[] = { (&IUserProfile_t4108565527_0_0_0) };
extern const Il2CppGenericInst GenInst_IUserProfile_t4108565527_0_0_0 = { 1, GenInst_IUserProfile_t4108565527_0_0_0_Types };
static const RuntimeType* GenInst_Boolean_t3825574718_0_0_0_String_t_0_0_0_Types[] = { (&Boolean_t3825574718_0_0_0), (&String_t_0_0_0) };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0_String_t_0_0_0 = { 2, GenInst_Boolean_t3825574718_0_0_0_String_t_0_0_0_Types };
static const RuntimeType* GenInst_Boolean_t3825574718_0_0_0_RuntimeObject_0_0_0_Types[] = { (&Boolean_t3825574718_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_Boolean_t3825574718_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_AchievementDescription_t3110978151_0_0_0_Types[] = { (&AchievementDescription_t3110978151_0_0_0) };
extern const Il2CppGenericInst GenInst_AchievementDescription_t3110978151_0_0_0 = { 1, GenInst_AchievementDescription_t3110978151_0_0_0_Types };
static const RuntimeType* GenInst_IAchievementDescription_t3498529102_0_0_0_Types[] = { (&IAchievementDescription_t3498529102_0_0_0) };
extern const Il2CppGenericInst GenInst_IAchievementDescription_t3498529102_0_0_0 = { 1, GenInst_IAchievementDescription_t3498529102_0_0_0_Types };
static const RuntimeType* GenInst_GcLeaderboard_t453887929_0_0_0_Types[] = { (&GcLeaderboard_t453887929_0_0_0) };
extern const Il2CppGenericInst GenInst_GcLeaderboard_t453887929_0_0_0 = { 1, GenInst_GcLeaderboard_t453887929_0_0_0_Types };
static const RuntimeType* GenInst_IAchievementDescriptionU5BU5D_t4083280315_0_0_0_Types[] = { (&IAchievementDescriptionU5BU5D_t4083280315_0_0_0) };
extern const Il2CppGenericInst GenInst_IAchievementDescriptionU5BU5D_t4083280315_0_0_0 = { 1, GenInst_IAchievementDescriptionU5BU5D_t4083280315_0_0_0_Types };
static const RuntimeType* GenInst_IAchievementU5BU5D_t2709554645_0_0_0_Types[] = { (&IAchievementU5BU5D_t2709554645_0_0_0) };
extern const Il2CppGenericInst GenInst_IAchievementU5BU5D_t2709554645_0_0_0 = { 1, GenInst_IAchievementU5BU5D_t2709554645_0_0_0_Types };
static const RuntimeType* GenInst_IAchievement_t1752291260_0_0_0_Types[] = { (&IAchievement_t1752291260_0_0_0) };
extern const Il2CppGenericInst GenInst_IAchievement_t1752291260_0_0_0 = { 1, GenInst_IAchievement_t1752291260_0_0_0_Types };
static const RuntimeType* GenInst_GcAchievementData_t1754866149_0_0_0_Types[] = { (&GcAchievementData_t1754866149_0_0_0) };
extern const Il2CppGenericInst GenInst_GcAchievementData_t1754866149_0_0_0 = { 1, GenInst_GcAchievementData_t1754866149_0_0_0_Types };
static const RuntimeType* GenInst_Achievement_t1333316625_0_0_0_Types[] = { (&Achievement_t1333316625_0_0_0) };
extern const Il2CppGenericInst GenInst_Achievement_t1333316625_0_0_0 = { 1, GenInst_Achievement_t1333316625_0_0_0_Types };
static const RuntimeType* GenInst_IScoreU5BU5D_t3237304636_0_0_0_Types[] = { (&IScoreU5BU5D_t3237304636_0_0_0) };
extern const Il2CppGenericInst GenInst_IScoreU5BU5D_t3237304636_0_0_0 = { 1, GenInst_IScoreU5BU5D_t3237304636_0_0_0_Types };
static const RuntimeType* GenInst_IScore_t513966369_0_0_0_Types[] = { (&IScore_t513966369_0_0_0) };
extern const Il2CppGenericInst GenInst_IScore_t513966369_0_0_0 = { 1, GenInst_IScore_t513966369_0_0_0_Types };
static const RuntimeType* GenInst_GcScoreData_t3676783238_0_0_0_Types[] = { (&GcScoreData_t3676783238_0_0_0) };
extern const Il2CppGenericInst GenInst_GcScoreData_t3676783238_0_0_0 = { 1, GenInst_GcScoreData_t3676783238_0_0_0_Types };
static const RuntimeType* GenInst_Score_t2307748940_0_0_0_Types[] = { (&Score_t2307748940_0_0_0) };
extern const Il2CppGenericInst GenInst_Score_t2307748940_0_0_0 = { 1, GenInst_Score_t2307748940_0_0_0_Types };
static const RuntimeType* GenInst_IUserProfileU5BU5D_t3461248430_0_0_0_Types[] = { (&IUserProfileU5BU5D_t3461248430_0_0_0) };
extern const Il2CppGenericInst GenInst_IUserProfileU5BU5D_t3461248430_0_0_0 = { 1, GenInst_IUserProfileU5BU5D_t3461248430_0_0_0_Types };
static const RuntimeType* GenInst_DisallowMultipleComponent_t2656950_0_0_0_Types[] = { (&DisallowMultipleComponent_t2656950_0_0_0) };
extern const Il2CppGenericInst GenInst_DisallowMultipleComponent_t2656950_0_0_0 = { 1, GenInst_DisallowMultipleComponent_t2656950_0_0_0_Types };
static const RuntimeType* GenInst_Attribute_t542643598_0_0_0_Types[] = { (&Attribute_t542643598_0_0_0) };
extern const Il2CppGenericInst GenInst_Attribute_t542643598_0_0_0 = { 1, GenInst_Attribute_t542643598_0_0_0_Types };
static const RuntimeType* GenInst__Attribute_t1557664299_0_0_0_Types[] = { (&_Attribute_t1557664299_0_0_0) };
extern const Il2CppGenericInst GenInst__Attribute_t1557664299_0_0_0 = { 1, GenInst__Attribute_t1557664299_0_0_0_Types };
static const RuntimeType* GenInst_ExecuteInEditMode_t3043633143_0_0_0_Types[] = { (&ExecuteInEditMode_t3043633143_0_0_0) };
extern const Il2CppGenericInst GenInst_ExecuteInEditMode_t3043633143_0_0_0 = { 1, GenInst_ExecuteInEditMode_t3043633143_0_0_0_Types };
static const RuntimeType* GenInst_RequireComponent_t864575032_0_0_0_Types[] = { (&RequireComponent_t864575032_0_0_0) };
extern const Il2CppGenericInst GenInst_RequireComponent_t864575032_0_0_0 = { 1, GenInst_RequireComponent_t864575032_0_0_0_Types };
static const RuntimeType* GenInst_HitInfo_t1761367055_0_0_0_Types[] = { (&HitInfo_t1761367055_0_0_0) };
extern const Il2CppGenericInst GenInst_HitInfo_t1761367055_0_0_0 = { 1, GenInst_HitInfo_t1761367055_0_0_0_Types };
static const RuntimeType* GenInst_PersistentCall_t3793436469_0_0_0_Types[] = { (&PersistentCall_t3793436469_0_0_0) };
extern const Il2CppGenericInst GenInst_PersistentCall_t3793436469_0_0_0 = { 1, GenInst_PersistentCall_t3793436469_0_0_0_Types };
static const RuntimeType* GenInst_BaseInvokableCall_t2229564840_0_0_0_Types[] = { (&BaseInvokableCall_t2229564840_0_0_0) };
extern const Il2CppGenericInst GenInst_BaseInvokableCall_t2229564840_0_0_0 = { 1, GenInst_BaseInvokableCall_t2229564840_0_0_0_Types };
static const RuntimeType* GenInst_WorkRequest_t1154022482_0_0_0_Types[] = { (&WorkRequest_t1154022482_0_0_0) };
extern const Il2CppGenericInst GenInst_WorkRequest_t1154022482_0_0_0 = { 1, GenInst_WorkRequest_t1154022482_0_0_0_Types };
static const RuntimeType* GenInst_PlayableBinding_t2498078091_0_0_0_Types[] = { (&PlayableBinding_t2498078091_0_0_0) };
extern const Il2CppGenericInst GenInst_PlayableBinding_t2498078091_0_0_0 = { 1, GenInst_PlayableBinding_t2498078091_0_0_0_Types };
static const RuntimeType* GenInst_MessageTypeSubscribers_t2291506050_0_0_0_Types[] = { (&MessageTypeSubscribers_t2291506050_0_0_0) };
extern const Il2CppGenericInst GenInst_MessageTypeSubscribers_t2291506050_0_0_0 = { 1, GenInst_MessageTypeSubscribers_t2291506050_0_0_0_Types };
static const RuntimeType* GenInst_MessageTypeSubscribers_t2291506050_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { (&MessageTypeSubscribers_t2291506050_0_0_0), (&Boolean_t3825574718_0_0_0) };
extern const Il2CppGenericInst GenInst_MessageTypeSubscribers_t2291506050_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_MessageTypeSubscribers_t2291506050_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const RuntimeType* GenInst_MessageEventArgs_t301283622_0_0_0_Types[] = { (&MessageEventArgs_t301283622_0_0_0) };
extern const Il2CppGenericInst GenInst_MessageEventArgs_t301283622_0_0_0 = { 1, GenInst_MessageEventArgs_t301283622_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_WeakReference_t1077405567_0_0_0_Types[] = { (&IntPtr_t_0_0_0), (&WeakReference_t1077405567_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_WeakReference_t1077405567_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_WeakReference_t1077405567_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_Types[] = { (&IntPtr_t_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t888819835_0_0_0_Types[] = { (&KeyValuePair_2_t888819835_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t888819835_0_0_0 = { 1, GenInst_KeyValuePair_2_t888819835_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_IntPtr_t_0_0_0_Types[] = { (&IntPtr_t_0_0_0), (&RuntimeObject_0_0_0), (&IntPtr_t_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_IntPtr_t_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_IntPtr_t_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types[] = { (&IntPtr_t_0_0_0), (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&IntPtr_t_0_0_0), (&RuntimeObject_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t888819835_0_0_0_Types[] = { (&IntPtr_t_0_0_0), (&RuntimeObject_0_0_0), (&KeyValuePair_2_t888819835_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t888819835_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t888819835_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_WeakReference_t1077405567_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&IntPtr_t_0_0_0), (&WeakReference_t1077405567_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_WeakReference_t1077405567_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_WeakReference_t1077405567_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_WeakReference_t1077405567_0_0_0_KeyValuePair_2_t3571743403_0_0_0_Types[] = { (&IntPtr_t_0_0_0), (&WeakReference_t1077405567_0_0_0), (&KeyValuePair_2_t3571743403_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_WeakReference_t1077405567_0_0_0_KeyValuePair_2_t3571743403_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_WeakReference_t1077405567_0_0_0_KeyValuePair_2_t3571743403_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3571743403_0_0_0_Types[] = { (&KeyValuePair_2_t3571743403_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3571743403_0_0_0 = { 1, GenInst_KeyValuePair_2_t3571743403_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t61287617_0_0_0_Types[] = { (&List_1_t61287617_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t61287617_0_0_0 = { 1, GenInst_List_1_t61287617_0_0_0_Types };
static const RuntimeType* GenInst_DispatcherKey_t708950850_0_0_0_Dispatcher_t2240407071_0_0_0_Types[] = { (&DispatcherKey_t708950850_0_0_0), (&Dispatcher_t2240407071_0_0_0) };
extern const Il2CppGenericInst GenInst_DispatcherKey_t708950850_0_0_0_Dispatcher_t2240407071_0_0_0 = { 2, GenInst_DispatcherKey_t708950850_0_0_0_Dispatcher_t2240407071_0_0_0_Types };
static const RuntimeType* GenInst_DispatcherKey_t708950850_0_0_0_Types[] = { (&DispatcherKey_t708950850_0_0_0) };
extern const Il2CppGenericInst GenInst_DispatcherKey_t708950850_0_0_0 = { 1, GenInst_DispatcherKey_t708950850_0_0_0_Types };
static const RuntimeType* GenInst_DispatcherKey_t708950850_0_0_0_Dispatcher_t2240407071_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&DispatcherKey_t708950850_0_0_0), (&Dispatcher_t2240407071_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_DispatcherKey_t708950850_0_0_0_Dispatcher_t2240407071_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_DispatcherKey_t708950850_0_0_0_Dispatcher_t2240407071_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_DispatcherKey_t708950850_0_0_0_Dispatcher_t2240407071_0_0_0_KeyValuePair_2_t3846770086_0_0_0_Types[] = { (&DispatcherKey_t708950850_0_0_0), (&Dispatcher_t2240407071_0_0_0), (&KeyValuePair_2_t3846770086_0_0_0) };
extern const Il2CppGenericInst GenInst_DispatcherKey_t708950850_0_0_0_Dispatcher_t2240407071_0_0_0_KeyValuePair_2_t3846770086_0_0_0 = { 3, GenInst_DispatcherKey_t708950850_0_0_0_Dispatcher_t2240407071_0_0_0_KeyValuePair_2_t3846770086_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3846770086_0_0_0_Types[] = { (&KeyValuePair_2_t3846770086_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3846770086_0_0_0 = { 1, GenInst_KeyValuePair_2_t3846770086_0_0_0_Types };
static const RuntimeType* GenInst_HtmlNode_t2048434459_0_0_0_Types[] = { (&HtmlNode_t2048434459_0_0_0) };
extern const Il2CppGenericInst GenInst_HtmlNode_t2048434459_0_0_0 = { 1, GenInst_HtmlNode_t2048434459_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_HtmlElementFlag_t260274357_0_0_0_Types[] = { (&String_t_0_0_0), (&HtmlElementFlag_t260274357_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_HtmlElementFlag_t260274357_0_0_0 = { 2, GenInst_String_t_0_0_0_HtmlElementFlag_t260274357_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_HtmlElementFlag_t260274357_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&HtmlElementFlag_t260274357_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_HtmlElementFlag_t260274357_0_0_0 = { 2, GenInst_RuntimeObject_0_0_0_HtmlElementFlag_t260274357_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1904647003_0_0_0_Types[] = { (&KeyValuePair_2_t1904647003_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1904647003_0_0_0 = { 1, GenInst_KeyValuePair_2_t1904647003_0_0_0_Types };
static const RuntimeType* GenInst_HtmlElementFlag_t260274357_0_0_0_Types[] = { (&HtmlElementFlag_t260274357_0_0_0) };
extern const Il2CppGenericInst GenInst_HtmlElementFlag_t260274357_0_0_0 = { 1, GenInst_HtmlElementFlag_t260274357_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_HtmlElementFlag_t260274357_0_0_0_RuntimeObject_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&HtmlElementFlag_t260274357_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_HtmlElementFlag_t260274357_0_0_0_RuntimeObject_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_HtmlElementFlag_t260274357_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_HtmlElementFlag_t260274357_0_0_0_HtmlElementFlag_t260274357_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&HtmlElementFlag_t260274357_0_0_0), (&HtmlElementFlag_t260274357_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_HtmlElementFlag_t260274357_0_0_0_HtmlElementFlag_t260274357_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_HtmlElementFlag_t260274357_0_0_0_HtmlElementFlag_t260274357_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_HtmlElementFlag_t260274357_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&HtmlElementFlag_t260274357_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_HtmlElementFlag_t260274357_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_HtmlElementFlag_t260274357_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_HtmlElementFlag_t260274357_0_0_0_KeyValuePair_2_t1904647003_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&HtmlElementFlag_t260274357_0_0_0), (&KeyValuePair_2_t1904647003_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_HtmlElementFlag_t260274357_0_0_0_KeyValuePair_2_t1904647003_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_HtmlElementFlag_t260274357_0_0_0_KeyValuePair_2_t1904647003_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_HtmlElementFlag_t260274357_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&String_t_0_0_0), (&HtmlElementFlag_t260274357_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_HtmlElementFlag_t260274357_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_HtmlElementFlag_t260274357_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_HtmlElementFlag_t260274357_0_0_0_KeyValuePair_2_t4227366137_0_0_0_Types[] = { (&String_t_0_0_0), (&HtmlElementFlag_t260274357_0_0_0), (&KeyValuePair_2_t4227366137_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_HtmlElementFlag_t260274357_0_0_0_KeyValuePair_2_t4227366137_0_0_0 = { 3, GenInst_String_t_0_0_0_HtmlElementFlag_t260274357_0_0_0_KeyValuePair_2_t4227366137_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t4227366137_0_0_0_Types[] = { (&KeyValuePair_2_t4227366137_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4227366137_0_0_0 = { 1, GenInst_KeyValuePair_2_t4227366137_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_HtmlNode_t2048434459_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&HtmlNode_t2048434459_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_HtmlNode_t2048434459_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_HtmlNode_t2048434459_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_HtmlNode_t2048434459_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&HtmlNode_t2048434459_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_HtmlNode_t2048434459_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_HtmlNode_t2048434459_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_HtmlNode_t2048434459_0_0_0_KeyValuePair_2_t3108572612_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&HtmlNode_t2048434459_0_0_0), (&KeyValuePair_2_t3108572612_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_HtmlNode_t2048434459_0_0_0_KeyValuePair_2_t3108572612_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_HtmlNode_t2048434459_0_0_0_KeyValuePair_2_t3108572612_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3108572612_0_0_0_Types[] = { (&KeyValuePair_2_t3108572612_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3108572612_0_0_0 = { 1, GenInst_KeyValuePair_2_t3108572612_0_0_0_Types };
static const RuntimeType* GenInst_HtmlAttribute_t1804523403_0_0_0_Types[] = { (&HtmlAttribute_t1804523403_0_0_0) };
extern const Il2CppGenericInst GenInst_HtmlAttribute_t1804523403_0_0_0 = { 1, GenInst_HtmlAttribute_t1804523403_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_HtmlNode_t2048434459_0_0_0_Types[] = { (&String_t_0_0_0), (&HtmlNode_t2048434459_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_HtmlNode_t2048434459_0_0_0 = { 2, GenInst_String_t_0_0_0_HtmlNode_t2048434459_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_HtmlNode_t2048434459_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&String_t_0_0_0), (&HtmlNode_t2048434459_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_HtmlNode_t2048434459_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_HtmlNode_t2048434459_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_HtmlNode_t2048434459_0_0_0_KeyValuePair_2_t1720558943_0_0_0_Types[] = { (&String_t_0_0_0), (&HtmlNode_t2048434459_0_0_0), (&KeyValuePair_2_t1720558943_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_HtmlNode_t2048434459_0_0_0_KeyValuePair_2_t1720558943_0_0_0 = { 3, GenInst_String_t_0_0_0_HtmlNode_t2048434459_0_0_0_KeyValuePair_2_t1720558943_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1720558943_0_0_0_Types[] = { (&KeyValuePair_2_t1720558943_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1720558943_0_0_0 = { 1, GenInst_KeyValuePair_2_t1720558943_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_HtmlAttribute_t1804523403_0_0_0_Types[] = { (&String_t_0_0_0), (&HtmlAttribute_t1804523403_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_HtmlAttribute_t1804523403_0_0_0 = { 2, GenInst_String_t_0_0_0_HtmlAttribute_t1804523403_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_HtmlAttribute_t1804523403_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&String_t_0_0_0), (&HtmlAttribute_t1804523403_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_HtmlAttribute_t1804523403_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_HtmlAttribute_t1804523403_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_HtmlAttribute_t1804523403_0_0_0_KeyValuePair_2_t1476647887_0_0_0_Types[] = { (&String_t_0_0_0), (&HtmlAttribute_t1804523403_0_0_0), (&KeyValuePair_2_t1476647887_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_HtmlAttribute_t1804523403_0_0_0_KeyValuePair_2_t1476647887_0_0_0 = { 3, GenInst_String_t_0_0_0_HtmlAttribute_t1804523403_0_0_0_KeyValuePair_2_t1476647887_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1476647887_0_0_0_Types[] = { (&KeyValuePair_2_t1476647887_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1476647887_0_0_0 = { 1, GenInst_KeyValuePair_2_t1476647887_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_HtmlAttribute_t1804523403_0_0_0_HtmlAttribute_t1804523403_0_0_0_Types[] = { (&String_t_0_0_0), (&HtmlAttribute_t1804523403_0_0_0), (&HtmlAttribute_t1804523403_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_HtmlAttribute_t1804523403_0_0_0_HtmlAttribute_t1804523403_0_0_0 = { 3, GenInst_String_t_0_0_0_HtmlAttribute_t1804523403_0_0_0_HtmlAttribute_t1804523403_0_0_0_Types };
static const RuntimeType* GenInst_HtmlParseError_t1115179162_0_0_0_Types[] = { (&HtmlParseError_t1115179162_0_0_0) };
extern const Il2CppGenericInst GenInst_HtmlParseError_t1115179162_0_0_0 = { 1, GenInst_HtmlParseError_t1115179162_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_HtmlNode_t2048434459_0_0_0_HtmlNode_t2048434459_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&HtmlNode_t2048434459_0_0_0), (&HtmlNode_t2048434459_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_HtmlNode_t2048434459_0_0_0_HtmlNode_t2048434459_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_HtmlNode_t2048434459_0_0_0_HtmlNode_t2048434459_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_List_1_t1070465849_0_0_0_Types[] = { (&String_t_0_0_0), (&List_1_t1070465849_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t1070465849_0_0_0 = { 2, GenInst_String_t_0_0_0_List_1_t1070465849_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_List_1_t1070465849_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&String_t_0_0_0), (&List_1_t1070465849_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t1070465849_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_List_1_t1070465849_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_List_1_t1070465849_0_0_0_KeyValuePair_2_t742590333_0_0_0_Types[] = { (&String_t_0_0_0), (&List_1_t1070465849_0_0_0), (&KeyValuePair_2_t742590333_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t1070465849_0_0_0_KeyValuePair_2_t742590333_0_0_0 = { 3, GenInst_String_t_0_0_0_List_1_t1070465849_0_0_0_KeyValuePair_2_t742590333_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t742590333_0_0_0_Types[] = { (&KeyValuePair_2_t742590333_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t742590333_0_0_0 = { 1, GenInst_KeyValuePair_2_t742590333_0_0_0_Types };
static const RuntimeType* GenInst_TuioBlobEventArgs_t3562978179_0_0_0_Types[] = { (&TuioBlobEventArgs_t3562978179_0_0_0) };
extern const Il2CppGenericInst GenInst_TuioBlobEventArgs_t3562978179_0_0_0 = { 1, GenInst_TuioBlobEventArgs_t3562978179_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_TuioBlob_t2046943414_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&TuioBlob_t2046943414_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_TuioBlob_t2046943414_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_TuioBlob_t2046943414_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_TuioBlob_t2046943414_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&TuioBlob_t2046943414_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_TuioBlob_t2046943414_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_TuioBlob_t2046943414_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_TuioBlob_t2046943414_0_0_0_KeyValuePair_2_t3107081567_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&TuioBlob_t2046943414_0_0_0), (&KeyValuePair_2_t3107081567_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_TuioBlob_t2046943414_0_0_0_KeyValuePair_2_t3107081567_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_TuioBlob_t2046943414_0_0_0_KeyValuePair_2_t3107081567_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3107081567_0_0_0_Types[] = { (&KeyValuePair_2_t3107081567_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3107081567_0_0_0 = { 1, GenInst_KeyValuePair_2_t3107081567_0_0_0_Types };
static const RuntimeType* GenInst_TuioBlob_t2046943414_0_0_0_Types[] = { (&TuioBlob_t2046943414_0_0_0) };
extern const Il2CppGenericInst GenInst_TuioBlob_t2046943414_0_0_0 = { 1, GenInst_TuioBlob_t2046943414_0_0_0_Types };
static const RuntimeType* GenInst_TuioCursorEventArgs_t362990012_0_0_0_Types[] = { (&TuioCursorEventArgs_t362990012_0_0_0) };
extern const Il2CppGenericInst GenInst_TuioCursorEventArgs_t362990012_0_0_0 = { 1, GenInst_TuioCursorEventArgs_t362990012_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_TuioCursor_t1850351419_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&TuioCursor_t1850351419_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_TuioCursor_t1850351419_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_TuioCursor_t1850351419_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_TuioCursor_t1850351419_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&TuioCursor_t1850351419_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_TuioCursor_t1850351419_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_TuioCursor_t1850351419_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_TuioCursor_t1850351419_0_0_0_KeyValuePair_2_t2910489572_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&TuioCursor_t1850351419_0_0_0), (&KeyValuePair_2_t2910489572_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_TuioCursor_t1850351419_0_0_0_KeyValuePair_2_t2910489572_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_TuioCursor_t1850351419_0_0_0_KeyValuePair_2_t2910489572_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2910489572_0_0_0_Types[] = { (&KeyValuePair_2_t2910489572_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2910489572_0_0_0 = { 1, GenInst_KeyValuePair_2_t2910489572_0_0_0_Types };
static const RuntimeType* GenInst_TuioCursor_t1850351419_0_0_0_Types[] = { (&TuioCursor_t1850351419_0_0_0) };
extern const Il2CppGenericInst GenInst_TuioCursor_t1850351419_0_0_0 = { 1, GenInst_TuioCursor_t1850351419_0_0_0_Types };
static const RuntimeType* GenInst_TuioObjectEventArgs_t1810743341_0_0_0_Types[] = { (&TuioObjectEventArgs_t1810743341_0_0_0) };
extern const Il2CppGenericInst GenInst_TuioObjectEventArgs_t1810743341_0_0_0 = { 1, GenInst_TuioObjectEventArgs_t1810743341_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_TuioObject_t1236821014_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&TuioObject_t1236821014_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_TuioObject_t1236821014_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_TuioObject_t1236821014_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_TuioObject_t1236821014_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&TuioObject_t1236821014_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_TuioObject_t1236821014_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_TuioObject_t1236821014_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_TuioObject_t1236821014_0_0_0_KeyValuePair_2_t2296959167_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&TuioObject_t1236821014_0_0_0), (&KeyValuePair_2_t2296959167_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_TuioObject_t1236821014_0_0_0_KeyValuePair_2_t2296959167_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_TuioObject_t1236821014_0_0_0_KeyValuePair_2_t2296959167_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2296959167_0_0_0_Types[] = { (&KeyValuePair_2_t2296959167_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2296959167_0_0_0 = { 1, GenInst_KeyValuePair_2_t2296959167_0_0_0_Types };
static const RuntimeType* GenInst_TuioObject_t1236821014_0_0_0_Types[] = { (&TuioObject_t1236821014_0_0_0) };
extern const Il2CppGenericInst GenInst_TuioObject_t1236821014_0_0_0 = { 1, GenInst_TuioObject_t1236821014_0_0_0_Types };
static const RuntimeType* GenInst_IDataProcessor_t2214055399_0_0_0_Types[] = { (&IDataProcessor_t2214055399_0_0_0) };
extern const Il2CppGenericInst GenInst_IDataProcessor_t2214055399_0_0_0 = { 1, GenInst_IDataProcessor_t2214055399_0_0_0_Types };
static const RuntimeType* GenInst_BaseInputModule_t1295781545_0_0_0_Types[] = { (&BaseInputModule_t1295781545_0_0_0) };
extern const Il2CppGenericInst GenInst_BaseInputModule_t1295781545_0_0_0 = { 1, GenInst_BaseInputModule_t1295781545_0_0_0_Types };
static const RuntimeType* GenInst_RaycastResult_t21186376_0_0_0_Types[] = { (&RaycastResult_t21186376_0_0_0) };
extern const Il2CppGenericInst GenInst_RaycastResult_t21186376_0_0_0 = { 1, GenInst_RaycastResult_t21186376_0_0_0_Types };
static const RuntimeType* GenInst_IDeselectHandler_t3182198310_0_0_0_Types[] = { (&IDeselectHandler_t3182198310_0_0_0) };
extern const Il2CppGenericInst GenInst_IDeselectHandler_t3182198310_0_0_0 = { 1, GenInst_IDeselectHandler_t3182198310_0_0_0_Types };
static const RuntimeType* GenInst_IEventSystemHandler_t2741188318_0_0_0_Types[] = { (&IEventSystemHandler_t2741188318_0_0_0) };
extern const Il2CppGenericInst GenInst_IEventSystemHandler_t2741188318_0_0_0 = { 1, GenInst_IEventSystemHandler_t2741188318_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t2110309450_0_0_0_Types[] = { (&List_1_t2110309450_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t2110309450_0_0_0 = { 1, GenInst_List_1_t2110309450_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t2058570427_0_0_0_Types[] = { (&List_1_t2058570427_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t2058570427_0_0_0 = { 1, GenInst_List_1_t2058570427_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t3188497603_0_0_0_Types[] = { (&List_1_t3188497603_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t3188497603_0_0_0 = { 1, GenInst_List_1_t3188497603_0_0_0_Types };
static const RuntimeType* GenInst_ISelectHandler_t2812555161_0_0_0_Types[] = { (&ISelectHandler_t2812555161_0_0_0) };
extern const Il2CppGenericInst GenInst_ISelectHandler_t2812555161_0_0_0 = { 1, GenInst_ISelectHandler_t2812555161_0_0_0_Types };
static const RuntimeType* GenInst_BaseRaycaster_t2336171397_0_0_0_Types[] = { (&BaseRaycaster_t2336171397_0_0_0) };
extern const Il2CppGenericInst GenInst_BaseRaycaster_t2336171397_0_0_0 = { 1, GenInst_BaseRaycaster_t2336171397_0_0_0_Types };
static const RuntimeType* GenInst_Entry_t3365010046_0_0_0_Types[] = { (&Entry_t3365010046_0_0_0) };
extern const Il2CppGenericInst GenInst_Entry_t3365010046_0_0_0 = { 1, GenInst_Entry_t3365010046_0_0_0_Types };
static const RuntimeType* GenInst_BaseEventData_t2681005625_0_0_0_Types[] = { (&BaseEventData_t2681005625_0_0_0) };
extern const Il2CppGenericInst GenInst_BaseEventData_t2681005625_0_0_0 = { 1, GenInst_BaseEventData_t2681005625_0_0_0_Types };
static const RuntimeType* GenInst_IPointerEnterHandler_t193164956_0_0_0_Types[] = { (&IPointerEnterHandler_t193164956_0_0_0) };
extern const Il2CppGenericInst GenInst_IPointerEnterHandler_t193164956_0_0_0 = { 1, GenInst_IPointerEnterHandler_t193164956_0_0_0_Types };
static const RuntimeType* GenInst_IPointerExitHandler_t461019860_0_0_0_Types[] = { (&IPointerExitHandler_t461019860_0_0_0) };
extern const Il2CppGenericInst GenInst_IPointerExitHandler_t461019860_0_0_0 = { 1, GenInst_IPointerExitHandler_t461019860_0_0_0_Types };
static const RuntimeType* GenInst_IPointerDownHandler_t3929046918_0_0_0_Types[] = { (&IPointerDownHandler_t3929046918_0_0_0) };
extern const Il2CppGenericInst GenInst_IPointerDownHandler_t3929046918_0_0_0 = { 1, GenInst_IPointerDownHandler_t3929046918_0_0_0_Types };
static const RuntimeType* GenInst_IPointerUpHandler_t1847764461_0_0_0_Types[] = { (&IPointerUpHandler_t1847764461_0_0_0) };
extern const Il2CppGenericInst GenInst_IPointerUpHandler_t1847764461_0_0_0 = { 1, GenInst_IPointerUpHandler_t1847764461_0_0_0_Types };
static const RuntimeType* GenInst_IPointerClickHandler_t96169666_0_0_0_Types[] = { (&IPointerClickHandler_t96169666_0_0_0) };
extern const Il2CppGenericInst GenInst_IPointerClickHandler_t96169666_0_0_0 = { 1, GenInst_IPointerClickHandler_t96169666_0_0_0_Types };
static const RuntimeType* GenInst_IInitializePotentialDragHandler_t3350809087_0_0_0_Types[] = { (&IInitializePotentialDragHandler_t3350809087_0_0_0) };
extern const Il2CppGenericInst GenInst_IInitializePotentialDragHandler_t3350809087_0_0_0 = { 1, GenInst_IInitializePotentialDragHandler_t3350809087_0_0_0_Types };
static const RuntimeType* GenInst_IBeginDragHandler_t3135127860_0_0_0_Types[] = { (&IBeginDragHandler_t3135127860_0_0_0) };
extern const Il2CppGenericInst GenInst_IBeginDragHandler_t3135127860_0_0_0 = { 1, GenInst_IBeginDragHandler_t3135127860_0_0_0_Types };
static const RuntimeType* GenInst_IDragHandler_t2583993319_0_0_0_Types[] = { (&IDragHandler_t2583993319_0_0_0) };
extern const Il2CppGenericInst GenInst_IDragHandler_t2583993319_0_0_0 = { 1, GenInst_IDragHandler_t2583993319_0_0_0_Types };
static const RuntimeType* GenInst_IEndDragHandler_t1349123600_0_0_0_Types[] = { (&IEndDragHandler_t1349123600_0_0_0) };
extern const Il2CppGenericInst GenInst_IEndDragHandler_t1349123600_0_0_0 = { 1, GenInst_IEndDragHandler_t1349123600_0_0_0_Types };
static const RuntimeType* GenInst_IDropHandler_t2390101210_0_0_0_Types[] = { (&IDropHandler_t2390101210_0_0_0) };
extern const Il2CppGenericInst GenInst_IDropHandler_t2390101210_0_0_0 = { 1, GenInst_IDropHandler_t2390101210_0_0_0_Types };
static const RuntimeType* GenInst_IScrollHandler_t3834677510_0_0_0_Types[] = { (&IScrollHandler_t3834677510_0_0_0) };
extern const Il2CppGenericInst GenInst_IScrollHandler_t3834677510_0_0_0 = { 1, GenInst_IScrollHandler_t3834677510_0_0_0_Types };
static const RuntimeType* GenInst_IUpdateSelectedHandler_t3778909353_0_0_0_Types[] = { (&IUpdateSelectedHandler_t3778909353_0_0_0) };
extern const Il2CppGenericInst GenInst_IUpdateSelectedHandler_t3778909353_0_0_0 = { 1, GenInst_IUpdateSelectedHandler_t3778909353_0_0_0_Types };
static const RuntimeType* GenInst_IMoveHandler_t2611925506_0_0_0_Types[] = { (&IMoveHandler_t2611925506_0_0_0) };
extern const Il2CppGenericInst GenInst_IMoveHandler_t2611925506_0_0_0 = { 1, GenInst_IMoveHandler_t2611925506_0_0_0_Types };
static const RuntimeType* GenInst_ISubmitHandler_t525803901_0_0_0_Types[] = { (&ISubmitHandler_t525803901_0_0_0) };
extern const Il2CppGenericInst GenInst_ISubmitHandler_t525803901_0_0_0 = { 1, GenInst_ISubmitHandler_t525803901_0_0_0_Types };
static const RuntimeType* GenInst_ICancelHandler_t1980319651_0_0_0_Types[] = { (&ICancelHandler_t1980319651_0_0_0) };
extern const Il2CppGenericInst GenInst_ICancelHandler_t1980319651_0_0_0 = { 1, GenInst_ICancelHandler_t1980319651_0_0_0_Types };
static const RuntimeType* GenInst_Transform_t3275118058_0_0_0_Types[] = { (&Transform_t3275118058_0_0_0) };
extern const Il2CppGenericInst GenInst_Transform_t3275118058_0_0_0 = { 1, GenInst_Transform_t3275118058_0_0_0_Types };
static const RuntimeType* GenInst_GameObject_t1756533147_0_0_0_Types[] = { (&GameObject_t1756533147_0_0_0) };
extern const Il2CppGenericInst GenInst_GameObject_t1756533147_0_0_0 = { 1, GenInst_GameObject_t1756533147_0_0_0_Types };
static const RuntimeType* GenInst_BaseInput_t621514313_0_0_0_Types[] = { (&BaseInput_t621514313_0_0_0) };
extern const Il2CppGenericInst GenInst_BaseInput_t621514313_0_0_0 = { 1, GenInst_BaseInput_t621514313_0_0_0_Types };
static const RuntimeType* GenInst_UIBehaviour_t3960014691_0_0_0_Types[] = { (&UIBehaviour_t3960014691_0_0_0) };
extern const Il2CppGenericInst GenInst_UIBehaviour_t3960014691_0_0_0 = { 1, GenInst_UIBehaviour_t3960014691_0_0_0_Types };
static const RuntimeType* GenInst_MonoBehaviour_t1158329972_0_0_0_Types[] = { (&MonoBehaviour_t1158329972_0_0_0) };
extern const Il2CppGenericInst GenInst_MonoBehaviour_t1158329972_0_0_0 = { 1, GenInst_MonoBehaviour_t1158329972_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&PointerEventData_t1599784723_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&PointerEventData_t1599784723_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_KeyValuePair_2_t2659922876_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&PointerEventData_t1599784723_0_0_0), (&KeyValuePair_2_t2659922876_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_KeyValuePair_2_t2659922876_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_KeyValuePair_2_t2659922876_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2659922876_0_0_0_Types[] = { (&KeyValuePair_2_t2659922876_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2659922876_0_0_0 = { 1, GenInst_KeyValuePair_2_t2659922876_0_0_0_Types };
static const RuntimeType* GenInst_PointerEventData_t1599784723_0_0_0_Types[] = { (&PointerEventData_t1599784723_0_0_0) };
extern const Il2CppGenericInst GenInst_PointerEventData_t1599784723_0_0_0 = { 1, GenInst_PointerEventData_t1599784723_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_PointerEventData_t1599784723_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&PointerEventData_t1599784723_0_0_0), (&PointerEventData_t1599784723_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_PointerEventData_t1599784723_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_PointerEventData_t1599784723_0_0_0_Types };
static const RuntimeType* GenInst_ButtonState_t2688375492_0_0_0_Types[] = { (&ButtonState_t2688375492_0_0_0) };
extern const Il2CppGenericInst GenInst_ButtonState_t2688375492_0_0_0 = { 1, GenInst_ButtonState_t2688375492_0_0_0_Types };
static const RuntimeType* GenInst_Color_t2020392075_0_0_0_Types[] = { (&Color_t2020392075_0_0_0) };
extern const Il2CppGenericInst GenInst_Color_t2020392075_0_0_0 = { 1, GenInst_Color_t2020392075_0_0_0_Types };
static const RuntimeType* GenInst_ICanvasElement_t986520779_0_0_0_Types[] = { (&ICanvasElement_t986520779_0_0_0) };
extern const Il2CppGenericInst GenInst_ICanvasElement_t986520779_0_0_0 = { 1, GenInst_ICanvasElement_t986520779_0_0_0_Types };
static const RuntimeType* GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_Types[] = { (&ICanvasElement_t986520779_0_0_0), (&Int32_t2071877448_0_0_0) };
extern const Il2CppGenericInst GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_Types };
static const RuntimeType* GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&ICanvasElement_t986520779_0_0_0), (&Int32_t2071877448_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_ColorBlock_t2652774230_0_0_0_Types[] = { (&ColorBlock_t2652774230_0_0_0) };
extern const Il2CppGenericInst GenInst_ColorBlock_t2652774230_0_0_0 = { 1, GenInst_ColorBlock_t2652774230_0_0_0_Types };
static const RuntimeType* GenInst_OptionData_t2420267500_0_0_0_Types[] = { (&OptionData_t2420267500_0_0_0) };
extern const Il2CppGenericInst GenInst_OptionData_t2420267500_0_0_0 = { 1, GenInst_OptionData_t2420267500_0_0_0_Types };
static const RuntimeType* GenInst_DropdownItem_t4139978805_0_0_0_Types[] = { (&DropdownItem_t4139978805_0_0_0) };
extern const Il2CppGenericInst GenInst_DropdownItem_t4139978805_0_0_0 = { 1, GenInst_DropdownItem_t4139978805_0_0_0_Types };
static const RuntimeType* GenInst_FloatTween_t2986189219_0_0_0_Types[] = { (&FloatTween_t2986189219_0_0_0) };
extern const Il2CppGenericInst GenInst_FloatTween_t2986189219_0_0_0 = { 1, GenInst_FloatTween_t2986189219_0_0_0_Types };
static const RuntimeType* GenInst_Sprite_t309593783_0_0_0_Types[] = { (&Sprite_t309593783_0_0_0) };
extern const Il2CppGenericInst GenInst_Sprite_t309593783_0_0_0 = { 1, GenInst_Sprite_t309593783_0_0_0_Types };
static const RuntimeType* GenInst_Canvas_t209405766_0_0_0_Types[] = { (&Canvas_t209405766_0_0_0) };
extern const Il2CppGenericInst GenInst_Canvas_t209405766_0_0_0 = { 1, GenInst_Canvas_t209405766_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t3873494194_0_0_0_Types[] = { (&List_1_t3873494194_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t3873494194_0_0_0 = { 1, GenInst_List_1_t3873494194_0_0_0_Types };
static const RuntimeType* GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0_Types[] = { (&Font_t4239498691_0_0_0), (&HashSet_1_t2984649583_0_0_0) };
extern const Il2CppGenericInst GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0 = { 2, GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0_Types };
static const RuntimeType* GenInst_Text_t356221433_0_0_0_Types[] = { (&Text_t356221433_0_0_0) };
extern const Il2CppGenericInst GenInst_Text_t356221433_0_0_0 = { 1, GenInst_Text_t356221433_0_0_0_Types };
static const RuntimeType* GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Font_t4239498691_0_0_0), (&HashSet_1_t2984649583_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0_KeyValuePair_2_t850112849_0_0_0_Types[] = { (&Font_t4239498691_0_0_0), (&HashSet_1_t2984649583_0_0_0), (&KeyValuePair_2_t850112849_0_0_0) };
extern const Il2CppGenericInst GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0_KeyValuePair_2_t850112849_0_0_0 = { 3, GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0_KeyValuePair_2_t850112849_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t850112849_0_0_0_Types[] = { (&KeyValuePair_2_t850112849_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t850112849_0_0_0 = { 1, GenInst_KeyValuePair_2_t850112849_0_0_0_Types };
static const RuntimeType* GenInst_ColorTween_t3438117476_0_0_0_Types[] = { (&ColorTween_t3438117476_0_0_0) };
extern const Il2CppGenericInst GenInst_ColorTween_t3438117476_0_0_0 = { 1, GenInst_ColorTween_t3438117476_0_0_0_Types };
static const RuntimeType* GenInst_Graphic_t2426225576_0_0_0_Types[] = { (&Graphic_t2426225576_0_0_0) };
extern const Il2CppGenericInst GenInst_Graphic_t2426225576_0_0_0 = { 1, GenInst_Graphic_t2426225576_0_0_0_Types };
static const RuntimeType* GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_Types[] = { (&Canvas_t209405766_0_0_0), (&IndexedSet_1_t286373651_0_0_0) };
extern const Il2CppGenericInst GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0 = { 2, GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_Types };
static const RuntimeType* GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_Types[] = { (&Graphic_t2426225576_0_0_0), (&Int32_t2071877448_0_0_0) };
extern const Il2CppGenericInst GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_Types };
static const RuntimeType* GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Graphic_t2426225576_0_0_0), (&Int32_t2071877448_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Canvas_t209405766_0_0_0), (&IndexedSet_1_t286373651_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_KeyValuePair_2_t2391682566_0_0_0_Types[] = { (&Canvas_t209405766_0_0_0), (&IndexedSet_1_t286373651_0_0_0), (&KeyValuePair_2_t2391682566_0_0_0) };
extern const Il2CppGenericInst GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_KeyValuePair_2_t2391682566_0_0_0 = { 3, GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_KeyValuePair_2_t2391682566_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2391682566_0_0_0_Types[] = { (&KeyValuePair_2_t2391682566_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2391682566_0_0_0 = { 1, GenInst_KeyValuePair_2_t2391682566_0_0_0_Types };
static const RuntimeType* GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3010968081_0_0_0_Types[] = { (&Graphic_t2426225576_0_0_0), (&Int32_t2071877448_0_0_0), (&KeyValuePair_2_t3010968081_0_0_0) };
extern const Il2CppGenericInst GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3010968081_0_0_0 = { 3, GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3010968081_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3010968081_0_0_0_Types[] = { (&KeyValuePair_2_t3010968081_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3010968081_0_0_0 = { 1, GenInst_KeyValuePair_2_t3010968081_0_0_0_Types };
static const RuntimeType* GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t1912381698_0_0_0_Types[] = { (&ICanvasElement_t986520779_0_0_0), (&Int32_t2071877448_0_0_0), (&KeyValuePair_2_t1912381698_0_0_0) };
extern const Il2CppGenericInst GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t1912381698_0_0_0 = { 3, GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t1912381698_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1912381698_0_0_0_Types[] = { (&KeyValuePair_2_t1912381698_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1912381698_0_0_0 = { 1, GenInst_KeyValuePair_2_t1912381698_0_0_0_Types };
static const RuntimeType* GenInst_Type_t3352948571_0_0_0_Types[] = { (&Type_t3352948571_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t3352948571_0_0_0 = { 1, GenInst_Type_t3352948571_0_0_0_Types };
static const RuntimeType* GenInst_FillMethod_t1640962579_0_0_0_Types[] = { (&FillMethod_t1640962579_0_0_0) };
extern const Il2CppGenericInst GenInst_FillMethod_t1640962579_0_0_0 = { 1, GenInst_FillMethod_t1640962579_0_0_0_Types };
static const RuntimeType* GenInst_ContentType_t1028629049_0_0_0_Types[] = { (&ContentType_t1028629049_0_0_0) };
extern const Il2CppGenericInst GenInst_ContentType_t1028629049_0_0_0 = { 1, GenInst_ContentType_t1028629049_0_0_0_Types };
static const RuntimeType* GenInst_LineType_t2931319356_0_0_0_Types[] = { (&LineType_t2931319356_0_0_0) };
extern const Il2CppGenericInst GenInst_LineType_t2931319356_0_0_0 = { 1, GenInst_LineType_t2931319356_0_0_0_Types };
static const RuntimeType* GenInst_InputType_t1274231802_0_0_0_Types[] = { (&InputType_t1274231802_0_0_0) };
extern const Il2CppGenericInst GenInst_InputType_t1274231802_0_0_0 = { 1, GenInst_InputType_t1274231802_0_0_0_Types };
static const RuntimeType* GenInst_TouchScreenKeyboardType_t875112366_0_0_0_Types[] = { (&TouchScreenKeyboardType_t875112366_0_0_0) };
extern const Il2CppGenericInst GenInst_TouchScreenKeyboardType_t875112366_0_0_0 = { 1, GenInst_TouchScreenKeyboardType_t875112366_0_0_0_Types };
static const RuntimeType* GenInst_CharacterValidation_t3437478890_0_0_0_Types[] = { (&CharacterValidation_t3437478890_0_0_0) };
extern const Il2CppGenericInst GenInst_CharacterValidation_t3437478890_0_0_0 = { 1, GenInst_CharacterValidation_t3437478890_0_0_0_Types };
static const RuntimeType* GenInst_Mask_t2977958238_0_0_0_Types[] = { (&Mask_t2977958238_0_0_0) };
extern const Il2CppGenericInst GenInst_Mask_t2977958238_0_0_0 = { 1, GenInst_Mask_t2977958238_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t2347079370_0_0_0_Types[] = { (&List_1_t2347079370_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t2347079370_0_0_0 = { 1, GenInst_List_1_t2347079370_0_0_0_Types };
static const RuntimeType* GenInst_RectMask2D_t1156185964_0_0_0_Types[] = { (&RectMask2D_t1156185964_0_0_0) };
extern const Il2CppGenericInst GenInst_RectMask2D_t1156185964_0_0_0 = { 1, GenInst_RectMask2D_t1156185964_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t525307096_0_0_0_Types[] = { (&List_1_t525307096_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t525307096_0_0_0 = { 1, GenInst_List_1_t525307096_0_0_0_Types };
static const RuntimeType* GenInst_Navigation_t1571958496_0_0_0_Types[] = { (&Navigation_t1571958496_0_0_0) };
extern const Il2CppGenericInst GenInst_Navigation_t1571958496_0_0_0 = { 1, GenInst_Navigation_t1571958496_0_0_0_Types };
static const RuntimeType* GenInst_IClippable_t1941276057_0_0_0_Types[] = { (&IClippable_t1941276057_0_0_0) };
extern const Il2CppGenericInst GenInst_IClippable_t1941276057_0_0_0 = { 1, GenInst_IClippable_t1941276057_0_0_0_Types };
static const RuntimeType* GenInst_Direction_t3696775921_0_0_0_Types[] = { (&Direction_t3696775921_0_0_0) };
extern const Il2CppGenericInst GenInst_Direction_t3696775921_0_0_0 = { 1, GenInst_Direction_t3696775921_0_0_0_Types };
static const RuntimeType* GenInst_Selectable_t1490392188_0_0_0_Types[] = { (&Selectable_t1490392188_0_0_0) };
extern const Il2CppGenericInst GenInst_Selectable_t1490392188_0_0_0 = { 1, GenInst_Selectable_t1490392188_0_0_0_Types };
static const RuntimeType* GenInst_Transition_t605142169_0_0_0_Types[] = { (&Transition_t605142169_0_0_0) };
extern const Il2CppGenericInst GenInst_Transition_t605142169_0_0_0 = { 1, GenInst_Transition_t605142169_0_0_0_Types };
static const RuntimeType* GenInst_SpriteState_t1353336012_0_0_0_Types[] = { (&SpriteState_t1353336012_0_0_0) };
extern const Il2CppGenericInst GenInst_SpriteState_t1353336012_0_0_0 = { 1, GenInst_SpriteState_t1353336012_0_0_0_Types };
static const RuntimeType* GenInst_CanvasGroup_t3296560743_0_0_0_Types[] = { (&CanvasGroup_t3296560743_0_0_0) };
extern const Il2CppGenericInst GenInst_CanvasGroup_t3296560743_0_0_0 = { 1, GenInst_CanvasGroup_t3296560743_0_0_0_Types };
static const RuntimeType* GenInst_Direction_t1525323322_0_0_0_Types[] = { (&Direction_t1525323322_0_0_0) };
extern const Il2CppGenericInst GenInst_Direction_t1525323322_0_0_0 = { 1, GenInst_Direction_t1525323322_0_0_0_Types };
static const RuntimeType* GenInst_MatEntry_t3157325053_0_0_0_Types[] = { (&MatEntry_t3157325053_0_0_0) };
extern const Il2CppGenericInst GenInst_MatEntry_t3157325053_0_0_0 = { 1, GenInst_MatEntry_t3157325053_0_0_0_Types };
static const RuntimeType* GenInst_Toggle_t3976754468_0_0_0_Types[] = { (&Toggle_t3976754468_0_0_0) };
extern const Il2CppGenericInst GenInst_Toggle_t3976754468_0_0_0 = { 1, GenInst_Toggle_t3976754468_0_0_0_Types };
static const RuntimeType* GenInst_Toggle_t3976754468_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { (&Toggle_t3976754468_0_0_0), (&Boolean_t3825574718_0_0_0) };
extern const Il2CppGenericInst GenInst_Toggle_t3976754468_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Toggle_t3976754468_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const RuntimeType* GenInst_IClipper_t900477982_0_0_0_Types[] = { (&IClipper_t900477982_0_0_0) };
extern const Il2CppGenericInst GenInst_IClipper_t900477982_0_0_0 = { 1, GenInst_IClipper_t900477982_0_0_0_Types };
static const RuntimeType* GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_Types[] = { (&IClipper_t900477982_0_0_0), (&Int32_t2071877448_0_0_0) };
extern const Il2CppGenericInst GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_Types };
static const RuntimeType* GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&IClipper_t900477982_0_0_0), (&Int32_t2071877448_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t379984643_0_0_0_Types[] = { (&IClipper_t900477982_0_0_0), (&Int32_t2071877448_0_0_0), (&KeyValuePair_2_t379984643_0_0_0) };
extern const Il2CppGenericInst GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t379984643_0_0_0 = { 3, GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t379984643_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t379984643_0_0_0_Types[] = { (&KeyValuePair_2_t379984643_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t379984643_0_0_0 = { 1, GenInst_KeyValuePair_2_t379984643_0_0_0_Types };
static const RuntimeType* GenInst_AspectMode_t1166448724_0_0_0_Types[] = { (&AspectMode_t1166448724_0_0_0) };
extern const Il2CppGenericInst GenInst_AspectMode_t1166448724_0_0_0 = { 1, GenInst_AspectMode_t1166448724_0_0_0_Types };
static const RuntimeType* GenInst_FitMode_t4030874534_0_0_0_Types[] = { (&FitMode_t4030874534_0_0_0) };
extern const Il2CppGenericInst GenInst_FitMode_t4030874534_0_0_0 = { 1, GenInst_FitMode_t4030874534_0_0_0_Types };
static const RuntimeType* GenInst_RectTransform_t3349966182_0_0_0_Types[] = { (&RectTransform_t3349966182_0_0_0) };
extern const Il2CppGenericInst GenInst_RectTransform_t3349966182_0_0_0 = { 1, GenInst_RectTransform_t3349966182_0_0_0_Types };
static const RuntimeType* GenInst_LayoutRebuilder_t2155218138_0_0_0_Types[] = { (&LayoutRebuilder_t2155218138_0_0_0) };
extern const Il2CppGenericInst GenInst_LayoutRebuilder_t2155218138_0_0_0 = { 1, GenInst_LayoutRebuilder_t2155218138_0_0_0_Types };
static const RuntimeType* GenInst_ILayoutElement_t1975293769_0_0_0_Single_t2076509932_0_0_0_Types[] = { (&ILayoutElement_t1975293769_0_0_0), (&Single_t2076509932_0_0_0) };
extern const Il2CppGenericInst GenInst_ILayoutElement_t1975293769_0_0_0_Single_t2076509932_0_0_0 = { 2, GenInst_ILayoutElement_t1975293769_0_0_0_Single_t2076509932_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Single_t2076509932_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Single_t2076509932_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Single_t2076509932_0_0_0 = { 2, GenInst_RuntimeObject_0_0_0_Single_t2076509932_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t1612828712_0_0_0_Types[] = { (&List_1_t1612828712_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t1612828712_0_0_0 = { 1, GenInst_List_1_t1612828712_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t243638650_0_0_0_Types[] = { (&List_1_t243638650_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t243638650_0_0_0 = { 1, GenInst_List_1_t243638650_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t1612828711_0_0_0_Types[] = { (&List_1_t1612828711_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t1612828711_0_0_0 = { 1, GenInst_List_1_t1612828711_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t1612828713_0_0_0_Types[] = { (&List_1_t1612828713_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t1612828713_0_0_0 = { 1, GenInst_List_1_t1612828713_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t1440998580_0_0_0_Types[] = { (&List_1_t1440998580_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t1440998580_0_0_0 = { 1, GenInst_List_1_t1440998580_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t573379950_0_0_0_Types[] = { (&List_1_t573379950_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t573379950_0_0_0 = { 1, GenInst_List_1_t573379950_0_0_0_Types };
static const RuntimeType* GenInst_ARHitTestResult_t3275513025_0_0_0_Types[] = { (&ARHitTestResult_t3275513025_0_0_0) };
extern const Il2CppGenericInst GenInst_ARHitTestResult_t3275513025_0_0_0 = { 1, GenInst_ARHitTestResult_t3275513025_0_0_0_Types };
static const RuntimeType* GenInst_ARPlaneAnchorGameObject_t2305225887_0_0_0_Types[] = { (&ARPlaneAnchorGameObject_t2305225887_0_0_0) };
extern const Il2CppGenericInst GenInst_ARPlaneAnchorGameObject_t2305225887_0_0_0 = { 1, GenInst_ARPlaneAnchorGameObject_t2305225887_0_0_0_Types };
static const RuntimeType* GenInst_ARHitTestResultType_t3616749745_0_0_0_Types[] = { (&ARHitTestResultType_t3616749745_0_0_0) };
extern const Il2CppGenericInst GenInst_ARHitTestResultType_t3616749745_0_0_0 = { 1, GenInst_ARHitTestResultType_t3616749745_0_0_0_Types };
static const RuntimeType* GenInst_UnityARSessionRunOption_t3123075684_0_0_0_Types[] = { (&UnityARSessionRunOption_t3123075684_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityARSessionRunOption_t3123075684_0_0_0 = { 1, GenInst_UnityARSessionRunOption_t3123075684_0_0_0_Types };
static const RuntimeType* GenInst_UnityARAlignment_t2379988631_0_0_0_Types[] = { (&UnityARAlignment_t2379988631_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityARAlignment_t2379988631_0_0_0 = { 1, GenInst_UnityARAlignment_t2379988631_0_0_0_Types };
static const RuntimeType* GenInst_UnityARPlaneDetection_t612575857_0_0_0_Types[] = { (&UnityARPlaneDetection_t612575857_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityARPlaneDetection_t612575857_0_0_0 = { 1, GenInst_UnityARPlaneDetection_t612575857_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_ARPlaneAnchorGameObject_t2305225887_0_0_0_Types[] = { (&String_t_0_0_0), (&ARPlaneAnchorGameObject_t2305225887_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ARPlaneAnchorGameObject_t2305225887_0_0_0 = { 2, GenInst_String_t_0_0_0_ARPlaneAnchorGameObject_t2305225887_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_ARPlaneAnchorGameObject_t2305225887_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&String_t_0_0_0), (&ARPlaneAnchorGameObject_t2305225887_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ARPlaneAnchorGameObject_t2305225887_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_ARPlaneAnchorGameObject_t2305225887_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_ARPlaneAnchorGameObject_t2305225887_0_0_0_KeyValuePair_2_t1977350371_0_0_0_Types[] = { (&String_t_0_0_0), (&ARPlaneAnchorGameObject_t2305225887_0_0_0), (&KeyValuePair_2_t1977350371_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ARPlaneAnchorGameObject_t2305225887_0_0_0_KeyValuePair_2_t1977350371_0_0_0 = { 3, GenInst_String_t_0_0_0_ARPlaneAnchorGameObject_t2305225887_0_0_0_KeyValuePair_2_t1977350371_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1977350371_0_0_0_Types[] = { (&KeyValuePair_2_t1977350371_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1977350371_0_0_0 = { 1, GenInst_KeyValuePair_2_t1977350371_0_0_0_Types };
static const RuntimeType* GenInst_ParticleSystem_t3394631041_0_0_0_Types[] = { (&ParticleSystem_t3394631041_0_0_0) };
extern const Il2CppGenericInst GenInst_ParticleSystem_t3394631041_0_0_0 = { 1, GenInst_ParticleSystem_t3394631041_0_0_0_Types };
static const RuntimeType* GenInst_Single_t2076509932_0_0_0_Single_t2076509932_0_0_0_Single_t2076509932_0_0_0_Types[] = { (&Single_t2076509932_0_0_0), (&Single_t2076509932_0_0_0), (&Single_t2076509932_0_0_0) };
extern const Il2CppGenericInst GenInst_Single_t2076509932_0_0_0_Single_t2076509932_0_0_0_Single_t2076509932_0_0_0 = { 3, GenInst_Single_t2076509932_0_0_0_Single_t2076509932_0_0_0_Single_t2076509932_0_0_0_Types };
static const RuntimeType* GenInst_Single_t2076509932_0_0_0_Single_t2076509932_0_0_0_Types[] = { (&Single_t2076509932_0_0_0), (&Single_t2076509932_0_0_0) };
extern const Il2CppGenericInst GenInst_Single_t2076509932_0_0_0_Single_t2076509932_0_0_0 = { 2, GenInst_Single_t2076509932_0_0_0_Single_t2076509932_0_0_0_Types };
static const RuntimeType* GenInst_Animator_t69676727_0_0_0_Types[] = { (&Animator_t69676727_0_0_0) };
extern const Il2CppGenericInst GenInst_Animator_t69676727_0_0_0 = { 1, GenInst_Animator_t69676727_0_0_0_Types };
static const RuntimeType* GenInst_Concept_t2322716066_0_0_0_Types[] = { (&Concept_t2322716066_0_0_0) };
extern const Il2CppGenericInst GenInst_Concept_t2322716066_0_0_0 = { 1, GenInst_Concept_t2322716066_0_0_0_Types };
static const RuntimeType* GenInst_Date_t2577429686_0_0_0_Types[] = { (&Date_t2577429686_0_0_0) };
extern const Il2CppGenericInst GenInst_Date_t2577429686_0_0_0 = { 1, GenInst_Date_t2577429686_0_0_0_Types };
static const RuntimeType* GenInst_Entity_t2757698109_0_0_0_Types[] = { (&Entity_t2757698109_0_0_0) };
extern const Il2CppGenericInst GenInst_Entity_t2757698109_0_0_0 = { 1, GenInst_Entity_t2757698109_0_0_0_Types };
static const RuntimeType* GenInst_Feed_t2248050640_0_0_0_Types[] = { (&Feed_t2248050640_0_0_0) };
extern const Il2CppGenericInst GenInst_Feed_t2248050640_0_0_0 = { 1, GenInst_Feed_t2248050640_0_0_0_Types };
static const RuntimeType* GenInst_Keyword_t3384522415_0_0_0_Types[] = { (&Keyword_t3384522415_0_0_0) };
extern const Il2CppGenericInst GenInst_Keyword_t3384522415_0_0_0 = { 1, GenInst_Keyword_t3384522415_0_0_0_Types };
static const RuntimeType* GenInst_Microformat_t171559559_0_0_0_Types[] = { (&Microformat_t171559559_0_0_0) };
extern const Il2CppGenericInst GenInst_Microformat_t171559559_0_0_0 = { 1, GenInst_Microformat_t171559559_0_0_0_Types };
static const RuntimeType* GenInst_Relation_t2336466576_0_0_0_Types[] = { (&Relation_t2336466576_0_0_0) };
extern const Il2CppGenericInst GenInst_Relation_t2336466576_0_0_0 = { 1, GenInst_Relation_t2336466576_0_0_0_Types };
static const RuntimeType* GenInst_TargetedSentiment_t494351509_0_0_0_Types[] = { (&TargetedSentiment_t494351509_0_0_0) };
extern const Il2CppGenericInst GenInst_TargetedSentiment_t494351509_0_0_0 = { 1, GenInst_TargetedSentiment_t494351509_0_0_0_Types };
static const RuntimeType* GenInst_Taxonomy_t1527601757_0_0_0_Types[] = { (&Taxonomy_t1527601757_0_0_0) };
extern const Il2CppGenericInst GenInst_Taxonomy_t1527601757_0_0_0 = { 1, GenInst_Taxonomy_t1527601757_0_0_0_Types };
static const RuntimeType* GenInst_ImageKeyword_t1009764024_0_0_0_Types[] = { (&ImageKeyword_t1009764024_0_0_0) };
extern const Il2CppGenericInst GenInst_ImageKeyword_t1009764024_0_0_0 = { 1, GenInst_ImageKeyword_t1009764024_0_0_0_Types };
static const RuntimeType* GenInst_DocEmotions_t3627397418_0_0_0_Types[] = { (&DocEmotions_t3627397418_0_0_0) };
extern const Il2CppGenericInst GenInst_DocEmotions_t3627397418_0_0_0 = { 1, GenInst_DocEmotions_t3627397418_0_0_0_Types };
static const RuntimeType* GenInst_Intent_t1324785294_0_0_0_Types[] = { (&Intent_t1324785294_0_0_0) };
extern const Il2CppGenericInst GenInst_Intent_t1324785294_0_0_0 = { 1, GenInst_Intent_t1324785294_0_0_0_Types };
static const RuntimeType* GenInst_MessageIntent_t315351975_0_0_0_Types[] = { (&MessageIntent_t315351975_0_0_0) };
extern const Il2CppGenericInst GenInst_MessageIntent_t315351975_0_0_0 = { 1, GenInst_MessageIntent_t315351975_0_0_0_Types };
static const RuntimeType* GenInst_Environment_t3491688169_0_0_0_Types[] = { (&Environment_t3491688169_0_0_0) };
extern const Il2CppGenericInst GenInst_Environment_t3491688169_0_0_0 = { 1, GenInst_Environment_t3491688169_0_0_0_Types };
static const RuntimeType* GenInst_ConfigurationRef_t2639036165_0_0_0_Types[] = { (&ConfigurationRef_t2639036165_0_0_0) };
extern const Il2CppGenericInst GenInst_ConfigurationRef_t2639036165_0_0_0 = { 1, GenInst_ConfigurationRef_t2639036165_0_0_0_Types };
static const RuntimeType* GenInst_CollectionRef_t3248558799_0_0_0_Types[] = { (&CollectionRef_t3248558799_0_0_0) };
extern const Il2CppGenericInst GenInst_CollectionRef_t3248558799_0_0_0 = { 1, GenInst_CollectionRef_t3248558799_0_0_0_Types };
static const RuntimeType* GenInst_QueryResult_t1852366331_0_0_0_Types[] = { (&QueryResult_t1852366331_0_0_0) };
extern const Il2CppGenericInst GenInst_QueryResult_t1852366331_0_0_0 = { 1, GenInst_QueryResult_t1852366331_0_0_0_Types };
static const RuntimeType* GenInst_Metadata_t1932344831_0_0_0_Types[] = { (&Metadata_t1932344831_0_0_0) };
extern const Il2CppGenericInst GenInst_Metadata_t1932344831_0_0_0 = { 1, GenInst_Metadata_t1932344831_0_0_0_Types };
static const RuntimeType* GenInst_AnswerUnit_t3274674984_0_0_0_Types[] = { (&AnswerUnit_t3274674984_0_0_0) };
extern const Il2CppGenericInst GenInst_AnswerUnit_t3274674984_0_0_0 = { 1, GenInst_AnswerUnit_t3274674984_0_0_0_Types };
static const RuntimeType* GenInst_Content_t2184047853_0_0_0_Types[] = { (&Content_t2184047853_0_0_0) };
extern const Il2CppGenericInst GenInst_Content_t2184047853_0_0_0 = { 1, GenInst_Content_t2184047853_0_0_0_Types };
static const RuntimeType* GenInst_Translation_t1796874115_0_0_0_Types[] = { (&Translation_t1796874115_0_0_0) };
extern const Il2CppGenericInst GenInst_Translation_t1796874115_0_0_0 = { 1, GenInst_Translation_t1796874115_0_0_0_Types };
static const RuntimeType* GenInst_Translation_t308652637_0_0_0_Types[] = { (&Translation_t308652637_0_0_0) };
extern const Il2CppGenericInst GenInst_Translation_t308652637_0_0_0 = { 1, GenInst_Translation_t308652637_0_0_0_Types };
static const RuntimeType* GenInst_TraitTreeNode_t694336958_0_0_0_Types[] = { (&TraitTreeNode_t694336958_0_0_0) };
extern const Il2CppGenericInst GenInst_TraitTreeNode_t694336958_0_0_0 = { 1, GenInst_TraitTreeNode_t694336958_0_0_0_Types };
static const RuntimeType* GenInst_TraitTreeNode_t3511443394_0_0_0_Types[] = { (&TraitTreeNode_t3511443394_0_0_0) };
extern const Il2CppGenericInst GenInst_TraitTreeNode_t3511443394_0_0_0 = { 1, GenInst_TraitTreeNode_t3511443394_0_0_0_Types };
static const RuntimeType* GenInst_BehaviorNode_t287073406_0_0_0_Types[] = { (&BehaviorNode_t287073406_0_0_0) };
extern const Il2CppGenericInst GenInst_BehaviorNode_t287073406_0_0_0 = { 1, GenInst_BehaviorNode_t287073406_0_0_0_Types };
static const RuntimeType* GenInst_ConsumptionPreferencesCategoryNode_t38182357_0_0_0_Types[] = { (&ConsumptionPreferencesCategoryNode_t38182357_0_0_0) };
extern const Il2CppGenericInst GenInst_ConsumptionPreferencesCategoryNode_t38182357_0_0_0 = { 1, GenInst_ConsumptionPreferencesCategoryNode_t38182357_0_0_0_Types };
static const RuntimeType* GenInst_ConsumptionPreferencesNode_t1038535837_0_0_0_Types[] = { (&ConsumptionPreferencesNode_t1038535837_0_0_0) };
extern const Il2CppGenericInst GenInst_ConsumptionPreferencesNode_t1038535837_0_0_0 = { 1, GenInst_ConsumptionPreferencesNode_t1038535837_0_0_0_Types };
static const RuntimeType* GenInst_SolrClusterResponse_t1008480713_0_0_0_Types[] = { (&SolrClusterResponse_t1008480713_0_0_0) };
extern const Il2CppGenericInst GenInst_SolrClusterResponse_t1008480713_0_0_0 = { 1, GenInst_SolrClusterResponse_t1008480713_0_0_0_Types };
static const RuntimeType* GenInst_RankerInfoPayload_t1776809245_0_0_0_Types[] = { (&RankerInfoPayload_t1776809245_0_0_0) };
extern const Il2CppGenericInst GenInst_RankerInfoPayload_t1776809245_0_0_0 = { 1, GenInst_RankerInfoPayload_t1776809245_0_0_0_Types };
static const RuntimeType* GenInst_RankedAnswer_t193891511_0_0_0_Types[] = { (&RankedAnswer_t193891511_0_0_0) };
extern const Il2CppGenericInst GenInst_RankedAnswer_t193891511_0_0_0 = { 1, GenInst_RankedAnswer_t193891511_0_0_0_Types };
static const RuntimeType* GenInst_Doc_t3160830270_0_0_0_Types[] = { (&Doc_t3160830270_0_0_0) };
extern const Il2CppGenericInst GenInst_Doc_t3160830270_0_0_0 = { 1, GenInst_Doc_t3160830270_0_0_0_Types };
static const RuntimeType* GenInst_Model_t2340542523_0_0_0_Types[] = { (&Model_t2340542523_0_0_0) };
extern const Il2CppGenericInst GenInst_Model_t2340542523_0_0_0 = { 1, GenInst_Model_t2340542523_0_0_0_Types };
static const RuntimeType* GenInst_SpeechRecognitionResult_t3827468010_0_0_0_Types[] = { (&SpeechRecognitionResult_t3827468010_0_0_0) };
extern const Il2CppGenericInst GenInst_SpeechRecognitionResult_t3827468010_0_0_0 = { 1, GenInst_SpeechRecognitionResult_t3827468010_0_0_0_Types };
static const RuntimeType* GenInst_SpeechRecognitionAlternative_t84095082_0_0_0_Types[] = { (&SpeechRecognitionAlternative_t84095082_0_0_0) };
extern const Il2CppGenericInst GenInst_SpeechRecognitionAlternative_t84095082_0_0_0 = { 1, GenInst_SpeechRecognitionAlternative_t84095082_0_0_0_Types };
static const RuntimeType* GenInst_Customization_t47167237_0_0_0_Types[] = { (&Customization_t47167237_0_0_0) };
extern const Il2CppGenericInst GenInst_Customization_t47167237_0_0_0 = { 1, GenInst_Customization_t47167237_0_0_0_Types };
static const RuntimeType* GenInst_Corpus_t3396664578_0_0_0_Types[] = { (&Corpus_t3396664578_0_0_0) };
extern const Il2CppGenericInst GenInst_Corpus_t3396664578_0_0_0 = { 1, GenInst_Corpus_t3396664578_0_0_0_Types };
static const RuntimeType* GenInst_WordData_t612265514_0_0_0_Types[] = { (&WordData_t612265514_0_0_0) };
extern const Il2CppGenericInst GenInst_WordData_t612265514_0_0_0 = { 1, GenInst_WordData_t612265514_0_0_0_Types };
static const RuntimeType* GenInst_Word_t4274554970_0_0_0_Types[] = { (&Word_t4274554970_0_0_0) };
extern const Il2CppGenericInst GenInst_Word_t4274554970_0_0_0 = { 1, GenInst_Word_t4274554970_0_0_0_Types };
static const RuntimeType* GenInst_Voice_t3646862260_0_0_0_Types[] = { (&Voice_t3646862260_0_0_0) };
extern const Il2CppGenericInst GenInst_Voice_t3646862260_0_0_0 = { 1, GenInst_Voice_t3646862260_0_0_0_Types };
static const RuntimeType* GenInst_Customization_t2261013253_0_0_0_Types[] = { (&Customization_t2261013253_0_0_0) };
extern const Il2CppGenericInst GenInst_Customization_t2261013253_0_0_0 = { 1, GenInst_Customization_t2261013253_0_0_0_Types };
static const RuntimeType* GenInst_Column_t2612486824_0_0_0_Types[] = { (&Column_t2612486824_0_0_0) };
extern const Il2CppGenericInst GenInst_Column_t2612486824_0_0_0 = { 1, GenInst_Column_t2612486824_0_0_0_Types };
static const RuntimeType* GenInst_Option_t1775127045_0_0_0_Types[] = { (&Option_t1775127045_0_0_0) };
extern const Il2CppGenericInst GenInst_Option_t1775127045_0_0_0 = { 1, GenInst_Option_t1775127045_0_0_0_Types };
static const RuntimeType* GenInst_GetClassifiersPerClassifierBrief_t3946924106_0_0_0_Types[] = { (&GetClassifiersPerClassifierBrief_t3946924106_0_0_0) };
extern const Il2CppGenericInst GenInst_GetClassifiersPerClassifierBrief_t3946924106_0_0_0 = { 1, GenInst_GetClassifiersPerClassifierBrief_t3946924106_0_0_0_Types };
static const RuntimeType* GenInst_ClassifyTopLevelSingle_t3733844951_0_0_0_Types[] = { (&ClassifyTopLevelSingle_t3733844951_0_0_0) };
extern const Il2CppGenericInst GenInst_ClassifyTopLevelSingle_t3733844951_0_0_0 = { 1, GenInst_ClassifyTopLevelSingle_t3733844951_0_0_0_Types };
static const RuntimeType* GenInst_ClassifyPerClassifier_t2357796318_0_0_0_Types[] = { (&ClassifyPerClassifier_t2357796318_0_0_0) };
extern const Il2CppGenericInst GenInst_ClassifyPerClassifier_t2357796318_0_0_0 = { 1, GenInst_ClassifyPerClassifier_t2357796318_0_0_0_Types };
static const RuntimeType* GenInst_ClassResult_t3791036973_0_0_0_Types[] = { (&ClassResult_t3791036973_0_0_0) };
extern const Il2CppGenericInst GenInst_ClassResult_t3791036973_0_0_0 = { 1, GenInst_ClassResult_t3791036973_0_0_0_Types };
static const RuntimeType* GenInst_FacesTopLevelSingle_t2140351533_0_0_0_Types[] = { (&FacesTopLevelSingle_t2140351533_0_0_0) };
extern const Il2CppGenericInst GenInst_FacesTopLevelSingle_t2140351533_0_0_0 = { 1, GenInst_FacesTopLevelSingle_t2140351533_0_0_0_Types };
static const RuntimeType* GenInst_OneFaceResult_t2674708326_0_0_0_Types[] = { (&OneFaceResult_t2674708326_0_0_0) };
extern const Il2CppGenericInst GenInst_OneFaceResult_t2674708326_0_0_0 = { 1, GenInst_OneFaceResult_t2674708326_0_0_0_Types };
static const RuntimeType* GenInst_TextRecogTopLevelSingle_t1839648508_0_0_0_Types[] = { (&TextRecogTopLevelSingle_t1839648508_0_0_0) };
extern const Il2CppGenericInst GenInst_TextRecogTopLevelSingle_t1839648508_0_0_0 = { 1, GenInst_TextRecogTopLevelSingle_t1839648508_0_0_0_Types };
static const RuntimeType* GenInst_TextRecogOneWord_t2687074657_0_0_0_Types[] = { (&TextRecogOneWord_t2687074657_0_0_0) };
extern const Il2CppGenericInst GenInst_TextRecogOneWord_t2687074657_0_0_0 = { 1, GenInst_TextRecogOneWord_t2687074657_0_0_0_Types };
static const RuntimeType* GenInst_MenuScene_t1232739575_0_0_0_Types[] = { (&MenuScene_t1232739575_0_0_0) };
extern const Il2CppGenericInst GenInst_MenuScene_t1232739575_0_0_0 = { 1, GenInst_MenuScene_t1232739575_0_0_0_Types };
static const RuntimeType* GenInst_CameraTarget_t552179396_0_0_0_Types[] = { (&CameraTarget_t552179396_0_0_0) };
extern const Il2CppGenericInst GenInst_CameraTarget_t552179396_0_0_0 = { 1, GenInst_CameraTarget_t552179396_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_RESTConnector_t3705102247_0_0_0_Types[] = { (&String_t_0_0_0), (&RESTConnector_t3705102247_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_RESTConnector_t3705102247_0_0_0 = { 2, GenInst_String_t_0_0_0_RESTConnector_t3705102247_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_RESTConnector_t3705102247_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&String_t_0_0_0), (&RESTConnector_t3705102247_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_RESTConnector_t3705102247_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_RESTConnector_t3705102247_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_RESTConnector_t3705102247_0_0_0_KeyValuePair_2_t3377226731_0_0_0_Types[] = { (&String_t_0_0_0), (&RESTConnector_t3705102247_0_0_0), (&KeyValuePair_2_t3377226731_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_RESTConnector_t3705102247_0_0_0_KeyValuePair_2_t3377226731_0_0_0 = { 3, GenInst_String_t_0_0_0_RESTConnector_t3705102247_0_0_0_KeyValuePair_2_t3377226731_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3377226731_0_0_0_Types[] = { (&KeyValuePair_2_t3377226731_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3377226731_0_0_0 = { 1, GenInst_KeyValuePair_2_t3377226731_0_0_0_Types };
static const RuntimeType* GenInst_Request_t466816980_0_0_0_Types[] = { (&Request_t466816980_0_0_0) };
extern const Il2CppGenericInst GenInst_Request_t466816980_0_0_0 = { 1, GenInst_Request_t466816980_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_RuntimeObject_0_0_0_Types[] = { (&String_t_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_String_t_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&String_t_0_0_0), (&RuntimeObject_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t2361573779_0_0_0_Types[] = { (&String_t_0_0_0), (&RuntimeObject_0_0_0), (&KeyValuePair_2_t2361573779_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t2361573779_0_0_0 = { 3, GenInst_String_t_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t2361573779_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2361573779_0_0_0_Types[] = { (&KeyValuePair_2_t2361573779_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2361573779_0_0_0 = { 1, GenInst_KeyValuePair_2_t2361573779_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Form_t779275863_0_0_0_Types[] = { (&String_t_0_0_0), (&Form_t779275863_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Form_t779275863_0_0_0 = { 2, GenInst_String_t_0_0_0_Form_t779275863_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Form_t779275863_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&String_t_0_0_0), (&Form_t779275863_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Form_t779275863_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_Form_t779275863_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Form_t779275863_0_0_0_KeyValuePair_2_t451400347_0_0_0_Types[] = { (&String_t_0_0_0), (&Form_t779275863_0_0_0), (&KeyValuePair_2_t451400347_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Form_t779275863_0_0_0_KeyValuePair_2_t451400347_0_0_0 = { 3, GenInst_String_t_0_0_0_Form_t779275863_0_0_0_KeyValuePair_2_t451400347_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t451400347_0_0_0_Types[] = { (&KeyValuePair_2_t451400347_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t451400347_0_0_0 = { 1, GenInst_KeyValuePair_2_t451400347_0_0_0_Types };
static const RuntimeType* GenInst_Message_t2515021790_0_0_0_Types[] = { (&Message_t2515021790_0_0_0) };
extern const Il2CppGenericInst GenInst_Message_t2515021790_0_0_0 = { 1, GenInst_Message_t2515021790_0_0_0_Types };
static const RuntimeType* GenInst_CloseEventArgs_t344507773_0_0_0_Types[] = { (&CloseEventArgs_t344507773_0_0_0) };
extern const Il2CppGenericInst GenInst_CloseEventArgs_t344507773_0_0_0 = { 1, GenInst_CloseEventArgs_t344507773_0_0_0_Types };
static const RuntimeType* GenInst_ErrorEventArgs_t502222999_0_0_0_Types[] = { (&ErrorEventArgs_t502222999_0_0_0) };
extern const Il2CppGenericInst GenInst_ErrorEventArgs_t502222999_0_0_0 = { 1, GenInst_ErrorEventArgs_t502222999_0_0_0_Types };
static const RuntimeType* GenInst_MessageEventArgs_t2890051726_0_0_0_Types[] = { (&MessageEventArgs_t2890051726_0_0_0) };
extern const Il2CppGenericInst GenInst_MessageEventArgs_t2890051726_0_0_0 = { 1, GenInst_MessageEventArgs_t2890051726_0_0_0_Types };
static const RuntimeType* GenInst_DebugInfo_t379980446_0_0_0_Types[] = { (&DebugInfo_t379980446_0_0_0) };
extern const Il2CppGenericInst GenInst_DebugInfo_t379980446_0_0_0 = { 1, GenInst_DebugInfo_t379980446_0_0_0_Types };
static const RuntimeType* GenInst_QualityManager_t3466824526_0_0_0_Types[] = { (&QualityManager_t3466824526_0_0_0) };
extern const Il2CppGenericInst GenInst_QualityManager_t3466824526_0_0_0 = { 1, GenInst_QualityManager_t3466824526_0_0_0_Types };
static const RuntimeType* GenInst_LogSystem_t3371271695_0_0_0_Types[] = { (&LogSystem_t3371271695_0_0_0) };
extern const Il2CppGenericInst GenInst_LogSystem_t3371271695_0_0_0 = { 1, GenInst_LogSystem_t3371271695_0_0_0_Types };
static const RuntimeType* GenInst_ILogReactor_t2178507087_0_0_0_Types[] = { (&ILogReactor_t2178507087_0_0_0) };
extern const Il2CppGenericInst GenInst_ILogReactor_t2178507087_0_0_0 = { 1, GenInst_ILogReactor_t2178507087_0_0_0_Types };
static const RuntimeType* GenInst_Quotation_t2638841872_0_0_0_Types[] = { (&Quotation_t2638841872_0_0_0) };
extern const Il2CppGenericInst GenInst_Quotation_t2638841872_0_0_0 = { 1, GenInst_Quotation_t2638841872_0_0_0_Types };
static const RuntimeType* GenInst_Docs_t1723806121_0_0_0_Types[] = { (&Docs_t1723806121_0_0_0) };
extern const Il2CppGenericInst GenInst_Docs_t1723806121_0_0_0 = { 1, GenInst_Docs_t1723806121_0_0_0_Types };
static const RuntimeType* GenInst_EnrichedTitle_t2959558712_0_0_0_Types[] = { (&EnrichedTitle_t2959558712_0_0_0) };
extern const Il2CppGenericInst GenInst_EnrichedTitle_t2959558712_0_0_0 = { 1, GenInst_EnrichedTitle_t2959558712_0_0_0_Types };
static const RuntimeType* GenInst_EntityResponse_t2137526154_0_0_0_Types[] = { (&EntityResponse_t2137526154_0_0_0) };
extern const Il2CppGenericInst GenInst_EntityResponse_t2137526154_0_0_0 = { 1, GenInst_EntityResponse_t2137526154_0_0_0_Types };
static const RuntimeType* GenInst_LogMessageResponse_t3273544838_0_0_0_Types[] = { (&LogMessageResponse_t3273544838_0_0_0) };
extern const Il2CppGenericInst GenInst_LogMessageResponse_t3273544838_0_0_0 = { 1, GenInst_LogMessageResponse_t3273544838_0_0_0_Types };
static const RuntimeType* GenInst_Workspace_t1018343827_0_0_0_Types[] = { (&Workspace_t1018343827_0_0_0) };
extern const Il2CppGenericInst GenInst_Workspace_t1018343827_0_0_0 = { 1, GenInst_Workspace_t1018343827_0_0_0_Types };
static const RuntimeType* GenInst_EntityExample_t3008277273_0_0_0_Types[] = { (&EntityExample_t3008277273_0_0_0) };
extern const Il2CppGenericInst GenInst_EntityExample_t3008277273_0_0_0 = { 1, GenInst_EntityExample_t3008277273_0_0_0_Types };
static const RuntimeType* GenInst_Intent_t3642575974_0_0_0_Types[] = { (&Intent_t3642575974_0_0_0) };
extern const Il2CppGenericInst GenInst_Intent_t3642575974_0_0_0 = { 1, GenInst_Intent_t3642575974_0_0_0_Types };
static const RuntimeType* GenInst_Example_t1927362902_0_0_0_Types[] = { (&Example_t1927362902_0_0_0) };
extern const Il2CppGenericInst GenInst_Example_t1927362902_0_0_0 = { 1, GenInst_Example_t1927362902_0_0_0_Types };
static const RuntimeType* GenInst_Entity_t4255761867_0_0_0_Types[] = { (&Entity_t4255761867_0_0_0) };
extern const Il2CppGenericInst GenInst_Entity_t4255761867_0_0_0 = { 1, GenInst_Entity_t4255761867_0_0_0_Types };
static const RuntimeType* GenInst_DialogNode_t2015514522_0_0_0_Types[] = { (&DialogNode_t2015514522_0_0_0) };
extern const Il2CppGenericInst GenInst_DialogNode_t2015514522_0_0_0 = { 1, GenInst_DialogNode_t2015514522_0_0_0_Types };
static const RuntimeType* GenInst_Synonym_t1752818203_0_0_0_Types[] = { (&Synonym_t1752818203_0_0_0) };
extern const Il2CppGenericInst GenInst_Synonym_t1752818203_0_0_0 = { 1, GenInst_Synonym_t1752818203_0_0_0_Types };
static const RuntimeType* GenInst_SynSet_t2414122624_0_0_0_Types[] = { (&SynSet_t2414122624_0_0_0) };
extern const Il2CppGenericInst GenInst_SynSet_t2414122624_0_0_0 = { 1, GenInst_SynSet_t2414122624_0_0_0_Types };
static const RuntimeType* GenInst_Evidence_t859225293_0_0_0_Types[] = { (&Evidence_t859225293_0_0_0) };
extern const Il2CppGenericInst GenInst_Evidence_t859225293_0_0_0 = { 1, GenInst_Evidence_t859225293_0_0_0_Types };
static const RuntimeType* GenInst_Table_t205705827_0_0_0_Types[] = { (&Table_t205705827_0_0_0) };
extern const Il2CppGenericInst GenInst_Table_t205705827_0_0_0 = { 1, GenInst_Table_t205705827_0_0_0_Types };
static const RuntimeType* GenInst_Row_t3628585283_0_0_0_Types[] = { (&Row_t3628585283_0_0_0) };
extern const Il2CppGenericInst GenInst_Row_t3628585283_0_0_0 = { 1, GenInst_Row_t3628585283_0_0_0_Types };
static const RuntimeType* GenInst_Cell_t3725717219_0_0_0_Types[] = { (&Cell_t3725717219_0_0_0) };
extern const Il2CppGenericInst GenInst_Cell_t3725717219_0_0_0 = { 1, GenInst_Cell_t3725717219_0_0_0_Types };
static const RuntimeType* GenInst_Word_t3826420310_0_0_0_Types[] = { (&Word_t3826420310_0_0_0) };
extern const Il2CppGenericInst GenInst_Word_t3826420310_0_0_0 = { 1, GenInst_Word_t3826420310_0_0_0_Types };
static const RuntimeType* GenInst_Value_t2642671545_0_0_0_Types[] = { (&Value_t2642671545_0_0_0) };
extern const Il2CppGenericInst GenInst_Value_t2642671545_0_0_0 = { 1, GenInst_Value_t2642671545_0_0_0_Types };
static const RuntimeType* GenInst_SynonymList_t1745302257_0_0_0_Types[] = { (&SynonymList_t1745302257_0_0_0) };
extern const Il2CppGenericInst GenInst_SynonymList_t1745302257_0_0_0 = { 1, GenInst_SynonymList_t1745302257_0_0_0_Types };
static const RuntimeType* GenInst_ParseTree_t2821719349_0_0_0_Types[] = { (&ParseTree_t2821719349_0_0_0) };
extern const Il2CppGenericInst GenInst_ParseTree_t2821719349_0_0_0 = { 1, GenInst_ParseTree_t2821719349_0_0_0_Types };
static const RuntimeType* GenInst_Answer_t3626344892_0_0_0_Types[] = { (&Answer_t3626344892_0_0_0) };
extern const Il2CppGenericInst GenInst_Answer_t3626344892_0_0_0 = { 1, GenInst_Answer_t3626344892_0_0_0_Types };
static const RuntimeType* GenInst_QuestionClass_t734245290_0_0_0_Types[] = { (&QuestionClass_t734245290_0_0_0) };
extern const Il2CppGenericInst GenInst_QuestionClass_t734245290_0_0_0 = { 1, GenInst_QuestionClass_t734245290_0_0_0_Types };
static const RuntimeType* GenInst_Response_t4265902649_0_0_0_Types[] = { (&Response_t4265902649_0_0_0) };
extern const Il2CppGenericInst GenInst_Response_t4265902649_0_0_0 = { 1, GenInst_Response_t4265902649_0_0_0_Types };
static const RuntimeType* GenInst_Notice_t1592579614_0_0_0_Types[] = { (&Notice_t1592579614_0_0_0) };
extern const Il2CppGenericInst GenInst_Notice_t1592579614_0_0_0 = { 1, GenInst_Notice_t1592579614_0_0_0_Types };
static const RuntimeType* GenInst_Enrichment_t533139721_0_0_0_Types[] = { (&Enrichment_t533139721_0_0_0) };
extern const Il2CppGenericInst GenInst_Enrichment_t533139721_0_0_0 = { 1, GenInst_Enrichment_t533139721_0_0_0_Types };
static const RuntimeType* GenInst_NormalizationOperation_t3873208172_0_0_0_Types[] = { (&NormalizationOperation_t3873208172_0_0_0) };
extern const Il2CppGenericInst GenInst_NormalizationOperation_t3873208172_0_0_0 = { 1, GenInst_NormalizationOperation_t3873208172_0_0_0_Types };
static const RuntimeType* GenInst_FontSetting_t3267780181_0_0_0_Types[] = { (&FontSetting_t3267780181_0_0_0) };
extern const Il2CppGenericInst GenInst_FontSetting_t3267780181_0_0_0 = { 1, GenInst_FontSetting_t3267780181_0_0_0_Types };
static const RuntimeType* GenInst_WordStyle_t1557885949_0_0_0_Types[] = { (&WordStyle_t1557885949_0_0_0) };
extern const Il2CppGenericInst GenInst_WordStyle_t1557885949_0_0_0 = { 1, GenInst_WordStyle_t1557885949_0_0_0_Types };
static const RuntimeType* GenInst_Field_t3865657682_0_0_0_Types[] = { (&Field_t3865657682_0_0_0) };
extern const Il2CppGenericInst GenInst_Field_t3865657682_0_0_0 = { 1, GenInst_Field_t3865657682_0_0_0_Types };
static const RuntimeType* GenInst_Warning_t1238678040_0_0_0_Types[] = { (&Warning_t1238678040_0_0_0) };
extern const Il2CppGenericInst GenInst_Warning_t1238678040_0_0_0 = { 1, GenInst_Warning_t1238678040_0_0_0_Types };
static const RuntimeType* GenInst_IWatsonService_t2784709738_0_0_0_Types[] = { (&IWatsonService_t2784709738_0_0_0) };
extern const Il2CppGenericInst GenInst_IWatsonService_t2784709738_0_0_0 = { 1, GenInst_IWatsonService_t2784709738_0_0_0_Types };
static const RuntimeType* GenInst_Language_t834895248_0_0_0_Types[] = { (&Language_t834895248_0_0_0) };
extern const Il2CppGenericInst GenInst_Language_t834895248_0_0_0 = { 1, GenInst_Language_t834895248_0_0_0_Types };
static const RuntimeType* GenInst_TranslationModel_t4027325182_0_0_0_Types[] = { (&TranslationModel_t4027325182_0_0_0) };
extern const Il2CppGenericInst GenInst_TranslationModel_t4027325182_0_0_0 = { 1, GenInst_TranslationModel_t4027325182_0_0_0_Types };
static const RuntimeType* GenInst_Language_t2171379860_0_0_0_Types[] = { (&Language_t2171379860_0_0_0) };
extern const Il2CppGenericInst GenInst_Language_t2171379860_0_0_0 = { 1, GenInst_Language_t2171379860_0_0_0_Types };
static const RuntimeType* GenInst_TranslationModel_t2730767334_0_0_0_Types[] = { (&TranslationModel_t2730767334_0_0_0) };
extern const Il2CppGenericInst GenInst_TranslationModel_t2730767334_0_0_0 = { 1, GenInst_TranslationModel_t2730767334_0_0_0_Types };
static const RuntimeType* GenInst_Classifier_t1414011573_0_0_0_Types[] = { (&Classifier_t1414011573_0_0_0) };
extern const Il2CppGenericInst GenInst_Classifier_t1414011573_0_0_0 = { 1, GenInst_Classifier_t1414011573_0_0_0_Types };
static const RuntimeType* GenInst_Class_t2988469534_0_0_0_Types[] = { (&Class_t2988469534_0_0_0) };
extern const Il2CppGenericInst GenInst_Class_t2988469534_0_0_0 = { 1, GenInst_Class_t2988469534_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_DataCache_t4250340070_0_0_0_Types[] = { (&String_t_0_0_0), (&DataCache_t4250340070_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_DataCache_t4250340070_0_0_0 = { 2, GenInst_String_t_0_0_0_DataCache_t4250340070_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_DataCache_t4250340070_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&String_t_0_0_0), (&DataCache_t4250340070_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_DataCache_t4250340070_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_DataCache_t4250340070_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_DataCache_t4250340070_0_0_0_KeyValuePair_2_t3922464554_0_0_0_Types[] = { (&String_t_0_0_0), (&DataCache_t4250340070_0_0_0), (&KeyValuePair_2_t3922464554_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_DataCache_t4250340070_0_0_0_KeyValuePair_2_t3922464554_0_0_0 = { 3, GenInst_String_t_0_0_0_DataCache_t4250340070_0_0_0_KeyValuePair_2_t3922464554_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3922464554_0_0_0_Types[] = { (&KeyValuePair_2_t3922464554_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3922464554_0_0_0 = { 1, GenInst_KeyValuePair_2_t3922464554_0_0_0_Types };
static const RuntimeType* GenInst_Warning_t514099562_0_0_0_Types[] = { (&Warning_t514099562_0_0_0) };
extern const Il2CppGenericInst GenInst_Warning_t514099562_0_0_0 = { 1, GenInst_Warning_t514099562_0_0_0_Types };
static const RuntimeType* GenInst_ContentItem_t1958644056_0_0_0_Types[] = { (&ContentItem_t1958644056_0_0_0) };
extern const Il2CppGenericInst GenInst_ContentItem_t1958644056_0_0_0 = { 1, GenInst_ContentItem_t1958644056_0_0_0_Types };
static const RuntimeType* GenInst_Warning_t31395408_0_0_0_Types[] = { (&Warning_t31395408_0_0_0) };
extern const Il2CppGenericInst GenInst_Warning_t31395408_0_0_0 = { 1, GenInst_Warning_t31395408_0_0_0_Types };
static const RuntimeType* GenInst_CollectionsResponse_t1627544196_0_0_0_Types[] = { (&CollectionsResponse_t1627544196_0_0_0) };
extern const Il2CppGenericInst GenInst_CollectionsResponse_t1627544196_0_0_0 = { 1, GenInst_CollectionsResponse_t1627544196_0_0_0_Types };
static const RuntimeType* GenInst_TimeStamp_t82276870_0_0_0_Types[] = { (&TimeStamp_t82276870_0_0_0) };
extern const Il2CppGenericInst GenInst_TimeStamp_t82276870_0_0_0 = { 1, GenInst_TimeStamp_t82276870_0_0_0_Types };
static const RuntimeType* GenInst_WordConfidence_t2757029394_0_0_0_Types[] = { (&WordConfidence_t2757029394_0_0_0) };
extern const Il2CppGenericInst GenInst_WordConfidence_t2757029394_0_0_0 = { 1, GenInst_WordConfidence_t2757029394_0_0_0_Types };
static const RuntimeType* GenInst_KeywordResult_t2819026148_0_0_0_Types[] = { (&KeywordResult_t2819026148_0_0_0) };
extern const Il2CppGenericInst GenInst_KeywordResult_t2819026148_0_0_0 = { 1, GenInst_KeywordResult_t2819026148_0_0_0_Types };
static const RuntimeType* GenInst_WordAlternativeResult_t3127007908_0_0_0_Types[] = { (&WordAlternativeResult_t3127007908_0_0_0) };
extern const Il2CppGenericInst GenInst_WordAlternativeResult_t3127007908_0_0_0 = { 1, GenInst_WordAlternativeResult_t3127007908_0_0_0_Types };
static const RuntimeType* GenInst_JobStatus_t2358384683_0_0_0_Types[] = { (&JobStatus_t2358384683_0_0_0) };
extern const Il2CppGenericInst GenInst_JobStatus_t2358384683_0_0_0 = { 1, GenInst_JobStatus_t2358384683_0_0_0_Types };
static const RuntimeType* GenInst_Word_t3204175642_0_0_0_Types[] = { (&Word_t3204175642_0_0_0) };
extern const Il2CppGenericInst GenInst_Word_t3204175642_0_0_0 = { 1, GenInst_Word_t3204175642_0_0_0_Types };
static const RuntimeType* GenInst_AudioData_t3894398524_0_0_0_Types[] = { (&AudioData_t3894398524_0_0_0) };
extern const Il2CppGenericInst GenInst_AudioData_t3894398524_0_0_0 = { 1, GenInst_AudioData_t3894398524_0_0_0_Types };
static const RuntimeType* GenInst_VoiceType_t981025524_0_0_0_String_t_0_0_0_Types[] = { (&VoiceType_t981025524_0_0_0), (&String_t_0_0_0) };
extern const Il2CppGenericInst GenInst_VoiceType_t981025524_0_0_0_String_t_0_0_0 = { 2, GenInst_VoiceType_t981025524_0_0_0_String_t_0_0_0_Types };
static const RuntimeType* GenInst_VoiceType_t981025524_0_0_0_RuntimeObject_0_0_0_Types[] = { (&VoiceType_t981025524_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_VoiceType_t981025524_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_VoiceType_t981025524_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t4282424860_0_0_0_Types[] = { (&KeyValuePair_2_t4282424860_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4282424860_0_0_0 = { 1, GenInst_KeyValuePair_2_t4282424860_0_0_0_Types };
static const RuntimeType* GenInst_VoiceType_t981025524_0_0_0_Types[] = { (&VoiceType_t981025524_0_0_0) };
extern const Il2CppGenericInst GenInst_VoiceType_t981025524_0_0_0 = { 1, GenInst_VoiceType_t981025524_0_0_0_Types };
static const RuntimeType* GenInst_VoiceType_t981025524_0_0_0_RuntimeObject_0_0_0_VoiceType_t981025524_0_0_0_Types[] = { (&VoiceType_t981025524_0_0_0), (&RuntimeObject_0_0_0), (&VoiceType_t981025524_0_0_0) };
extern const Il2CppGenericInst GenInst_VoiceType_t981025524_0_0_0_RuntimeObject_0_0_0_VoiceType_t981025524_0_0_0 = { 3, GenInst_VoiceType_t981025524_0_0_0_RuntimeObject_0_0_0_VoiceType_t981025524_0_0_0_Types };
static const RuntimeType* GenInst_VoiceType_t981025524_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types[] = { (&VoiceType_t981025524_0_0_0), (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_VoiceType_t981025524_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0 = { 3, GenInst_VoiceType_t981025524_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_VoiceType_t981025524_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&VoiceType_t981025524_0_0_0), (&RuntimeObject_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_VoiceType_t981025524_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_VoiceType_t981025524_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_VoiceType_t981025524_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t4282424860_0_0_0_Types[] = { (&VoiceType_t981025524_0_0_0), (&RuntimeObject_0_0_0), (&KeyValuePair_2_t4282424860_0_0_0) };
extern const Il2CppGenericInst GenInst_VoiceType_t981025524_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t4282424860_0_0_0 = { 3, GenInst_VoiceType_t981025524_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t4282424860_0_0_0_Types };
static const RuntimeType* GenInst_VoiceType_t981025524_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&VoiceType_t981025524_0_0_0), (&String_t_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_VoiceType_t981025524_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_VoiceType_t981025524_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_VoiceType_t981025524_0_0_0_String_t_0_0_0_KeyValuePair_2_t3622195798_0_0_0_Types[] = { (&VoiceType_t981025524_0_0_0), (&String_t_0_0_0), (&KeyValuePair_2_t3622195798_0_0_0) };
extern const Il2CppGenericInst GenInst_VoiceType_t981025524_0_0_0_String_t_0_0_0_KeyValuePair_2_t3622195798_0_0_0 = { 3, GenInst_VoiceType_t981025524_0_0_0_String_t_0_0_0_KeyValuePair_2_t3622195798_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3622195798_0_0_0_Types[] = { (&KeyValuePair_2_t3622195798_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3622195798_0_0_0 = { 1, GenInst_KeyValuePair_2_t3622195798_0_0_0_Types };
static const RuntimeType* GenInst_AudioFormatType_t2756784245_0_0_0_String_t_0_0_0_Types[] = { (&AudioFormatType_t2756784245_0_0_0), (&String_t_0_0_0) };
extern const Il2CppGenericInst GenInst_AudioFormatType_t2756784245_0_0_0_String_t_0_0_0 = { 2, GenInst_AudioFormatType_t2756784245_0_0_0_String_t_0_0_0_Types };
static const RuntimeType* GenInst_AudioFormatType_t2756784245_0_0_0_RuntimeObject_0_0_0_Types[] = { (&AudioFormatType_t2756784245_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_AudioFormatType_t2756784245_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_AudioFormatType_t2756784245_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t993946999_0_0_0_Types[] = { (&KeyValuePair_2_t993946999_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t993946999_0_0_0 = { 1, GenInst_KeyValuePair_2_t993946999_0_0_0_Types };
static const RuntimeType* GenInst_AudioFormatType_t2756784245_0_0_0_Types[] = { (&AudioFormatType_t2756784245_0_0_0) };
extern const Il2CppGenericInst GenInst_AudioFormatType_t2756784245_0_0_0 = { 1, GenInst_AudioFormatType_t2756784245_0_0_0_Types };
static const RuntimeType* GenInst_AudioFormatType_t2756784245_0_0_0_RuntimeObject_0_0_0_AudioFormatType_t2756784245_0_0_0_Types[] = { (&AudioFormatType_t2756784245_0_0_0), (&RuntimeObject_0_0_0), (&AudioFormatType_t2756784245_0_0_0) };
extern const Il2CppGenericInst GenInst_AudioFormatType_t2756784245_0_0_0_RuntimeObject_0_0_0_AudioFormatType_t2756784245_0_0_0 = { 3, GenInst_AudioFormatType_t2756784245_0_0_0_RuntimeObject_0_0_0_AudioFormatType_t2756784245_0_0_0_Types };
static const RuntimeType* GenInst_AudioFormatType_t2756784245_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types[] = { (&AudioFormatType_t2756784245_0_0_0), (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_AudioFormatType_t2756784245_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0 = { 3, GenInst_AudioFormatType_t2756784245_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_AudioFormatType_t2756784245_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&AudioFormatType_t2756784245_0_0_0), (&RuntimeObject_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_AudioFormatType_t2756784245_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_AudioFormatType_t2756784245_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_AudioFormatType_t2756784245_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t993946999_0_0_0_Types[] = { (&AudioFormatType_t2756784245_0_0_0), (&RuntimeObject_0_0_0), (&KeyValuePair_2_t993946999_0_0_0) };
extern const Il2CppGenericInst GenInst_AudioFormatType_t2756784245_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t993946999_0_0_0 = { 3, GenInst_AudioFormatType_t2756784245_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t993946999_0_0_0_Types };
static const RuntimeType* GenInst_AudioFormatType_t2756784245_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&AudioFormatType_t2756784245_0_0_0), (&String_t_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_AudioFormatType_t2756784245_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_AudioFormatType_t2756784245_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_AudioFormatType_t2756784245_0_0_0_String_t_0_0_0_KeyValuePair_2_t333717937_0_0_0_Types[] = { (&AudioFormatType_t2756784245_0_0_0), (&String_t_0_0_0), (&KeyValuePair_2_t333717937_0_0_0) };
extern const Il2CppGenericInst GenInst_AudioFormatType_t2756784245_0_0_0_String_t_0_0_0_KeyValuePair_2_t333717937_0_0_0 = { 3, GenInst_AudioFormatType_t2756784245_0_0_0_String_t_0_0_0_KeyValuePair_2_t333717937_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t333717937_0_0_0_Types[] = { (&KeyValuePair_2_t333717937_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t333717937_0_0_0 = { 1, GenInst_KeyValuePair_2_t333717937_0_0_0_Types };
static const RuntimeType* GenInst_SentenceTone_t3234952115_0_0_0_Types[] = { (&SentenceTone_t3234952115_0_0_0) };
extern const Il2CppGenericInst GenInst_SentenceTone_t3234952115_0_0_0 = { 1, GenInst_SentenceTone_t3234952115_0_0_0_Types };
static const RuntimeType* GenInst_ToneCategory_t3724297374_0_0_0_Types[] = { (&ToneCategory_t3724297374_0_0_0) };
extern const Il2CppGenericInst GenInst_ToneCategory_t3724297374_0_0_0 = { 1, GenInst_ToneCategory_t3724297374_0_0_0_Types };
static const RuntimeType* GenInst_Tone_t1138009090_0_0_0_Types[] = { (&Tone_t1138009090_0_0_0) };
extern const Il2CppGenericInst GenInst_Tone_t1138009090_0_0_0 = { 1, GenInst_Tone_t1138009090_0_0_0_Types };
static const RuntimeType* GenInst_Solution_t204511443_0_0_0_Types[] = { (&Solution_t204511443_0_0_0) };
extern const Il2CppGenericInst GenInst_Solution_t204511443_0_0_0 = { 1, GenInst_Solution_t204511443_0_0_0_Types };
static const RuntimeType* GenInst_Anchor_t1470404909_0_0_0_Types[] = { (&Anchor_t1470404909_0_0_0) };
extern const Il2CppGenericInst GenInst_Anchor_t1470404909_0_0_0 = { 1, GenInst_Anchor_t1470404909_0_0_0_Types };
static const RuntimeType* GenInst_Node_t3986951550_0_0_0_Types[] = { (&Node_t3986951550_0_0_0) };
extern const Il2CppGenericInst GenInst_Node_t3986951550_0_0_0 = { 1, GenInst_Node_t3986951550_0_0_0_Types };
static const RuntimeType* GenInst_WarningInfo_t624028094_0_0_0_Types[] = { (&WarningInfo_t624028094_0_0_0) };
extern const Il2CppGenericInst GenInst_WarningInfo_t624028094_0_0_0 = { 1, GenInst_WarningInfo_t624028094_0_0_0_Types };
static const RuntimeType* GenInst_Class_t4054986160_0_0_0_Types[] = { (&Class_t4054986160_0_0_0) };
extern const Il2CppGenericInst GenInst_Class_t4054986160_0_0_0 = { 1, GenInst_Class_t4054986160_0_0_0_Types };
static const RuntimeType* GenInst_CreateCollection_t3312638156_0_0_0_Types[] = { (&CreateCollection_t3312638156_0_0_0) };
extern const Il2CppGenericInst GenInst_CreateCollection_t3312638156_0_0_0 = { 1, GenInst_CreateCollection_t3312638156_0_0_0_Types };
static const RuntimeType* GenInst_GetCollectionsBrief_t2166116487_0_0_0_Types[] = { (&GetCollectionsBrief_t2166116487_0_0_0) };
extern const Il2CppGenericInst GenInst_GetCollectionsBrief_t2166116487_0_0_0 = { 1, GenInst_GetCollectionsBrief_t2166116487_0_0_0_Types };
static const RuntimeType* GenInst_CollectionImagesConfig_t1273369062_0_0_0_Types[] = { (&CollectionImagesConfig_t1273369062_0_0_0) };
extern const Il2CppGenericInst GenInst_CollectionImagesConfig_t1273369062_0_0_0 = { 1, GenInst_CollectionImagesConfig_t1273369062_0_0_0_Types };
static const RuntimeType* GenInst_SimilarImageConfig_t3734626070_0_0_0_Types[] = { (&SimilarImageConfig_t3734626070_0_0_0) };
extern const Il2CppGenericInst GenInst_SimilarImageConfig_t3734626070_0_0_0 = { 1, GenInst_SimilarImageConfig_t3734626070_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_ByteU5BU5D_t3397334013_0_0_0_Types[] = { (&String_t_0_0_0), (&ByteU5BU5D_t3397334013_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ByteU5BU5D_t3397334013_0_0_0 = { 2, GenInst_String_t_0_0_0_ByteU5BU5D_t3397334013_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_ByteU5BU5D_t3397334013_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&String_t_0_0_0), (&ByteU5BU5D_t3397334013_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ByteU5BU5D_t3397334013_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_ByteU5BU5D_t3397334013_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_ByteU5BU5D_t3397334013_0_0_0_KeyValuePair_2_t3069458497_0_0_0_Types[] = { (&String_t_0_0_0), (&ByteU5BU5D_t3397334013_0_0_0), (&KeyValuePair_2_t3069458497_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ByteU5BU5D_t3397334013_0_0_0_KeyValuePair_2_t3069458497_0_0_0 = { 3, GenInst_String_t_0_0_0_ByteU5BU5D_t3397334013_0_0_0_KeyValuePair_2_t3069458497_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3069458497_0_0_0_Types[] = { (&KeyValuePair_2_t3069458497_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3069458497_0_0_0 = { 1, GenInst_KeyValuePair_2_t3069458497_0_0_0_Types };
static const RuntimeType* GenInst_UnitTestManager_t2486317649_0_0_0_Types[] = { (&UnitTestManager_t2486317649_0_0_0) };
extern const Il2CppGenericInst GenInst_UnitTestManager_t2486317649_0_0_0 = { 1, GenInst_UnitTestManager_t2486317649_0_0_0_Types };
static const RuntimeType* GenInst_AudioClip_t1932558630_0_0_0_Types[] = { (&AudioClip_t1932558630_0_0_0) };
extern const Il2CppGenericInst GenInst_AudioClip_t1932558630_0_0_0 = { 1, GenInst_AudioClip_t1932558630_0_0_0_Types };
static const RuntimeType* GenInst_Config_t3637807320_0_0_0_Types[] = { (&Config_t3637807320_0_0_0) };
extern const Il2CppGenericInst GenInst_Config_t3637807320_0_0_0 = { 1, GenInst_Config_t3637807320_0_0_0_Types };
static const RuntimeType* GenInst_CredentialInfo_t34154441_0_0_0_Types[] = { (&CredentialInfo_t34154441_0_0_0) };
extern const Il2CppGenericInst GenInst_CredentialInfo_t34154441_0_0_0 = { 1, GenInst_CredentialInfo_t34154441_0_0_0_Types };
static const RuntimeType* GenInst_Variable_t437234954_0_0_0_Types[] = { (&Variable_t437234954_0_0_0) };
extern const Il2CppGenericInst GenInst_Variable_t437234954_0_0_0 = { 1, GenInst_Variable_t437234954_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_CacheItem_t2472383731_0_0_0_Types[] = { (&String_t_0_0_0), (&CacheItem_t2472383731_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_CacheItem_t2472383731_0_0_0 = { 2, GenInst_String_t_0_0_0_CacheItem_t2472383731_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_CacheItem_t2472383731_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&String_t_0_0_0), (&CacheItem_t2472383731_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_CacheItem_t2472383731_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_CacheItem_t2472383731_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_CacheItem_t2472383731_0_0_0_KeyValuePair_2_t2144508215_0_0_0_Types[] = { (&String_t_0_0_0), (&CacheItem_t2472383731_0_0_0), (&KeyValuePair_2_t2144508215_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_CacheItem_t2472383731_0_0_0_KeyValuePair_2_t2144508215_0_0_0 = { 3, GenInst_String_t_0_0_0_CacheItem_t2472383731_0_0_0_KeyValuePair_2_t2144508215_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2144508215_0_0_0_Types[] = { (&KeyValuePair_2_t2144508215_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2144508215_0_0_0 = { 1, GenInst_KeyValuePair_2_t2144508215_0_0_0_Types };
static const RuntimeType* GenInst_CacheItem_t2472383731_0_0_0_Types[] = { (&CacheItem_t2472383731_0_0_0) };
extern const Il2CppGenericInst GenInst_CacheItem_t2472383731_0_0_0 = { 1, GenInst_CacheItem_t2472383731_0_0_0_Types };
static const RuntimeType* GenInst_EventManager_t605335149_0_0_0_Types[] = { (&EventManager_t605335149_0_0_0) };
extern const Il2CppGenericInst GenInst_EventManager_t605335149_0_0_0 = { 1, GenInst_EventManager_t605335149_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_Dictionary_2_t1621280361_0_0_0_Types[] = { (&Type_t_0_0_0), (&Dictionary_2_t1621280361_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_Dictionary_2_t1621280361_0_0_0 = { 2, GenInst_Type_t_0_0_0_Dictionary_2_t1621280361_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_String_t_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&String_t_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_String_t_0_0_0 = { 2, GenInst_RuntimeObject_0_0_0_String_t_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&String_t_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_String_t_0_0_0_KeyValuePair_2_t3673592879_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&String_t_0_0_0), (&KeyValuePair_2_t3673592879_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_String_t_0_0_0_KeyValuePair_2_t3673592879_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_String_t_0_0_0_KeyValuePair_2_t3673592879_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3673592879_0_0_0_Types[] = { (&KeyValuePair_2_t3673592879_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3673592879_0_0_0 = { 1, GenInst_KeyValuePair_2_t3673592879_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_Dictionary_2_t1621280361_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Type_t_0_0_0), (&Dictionary_2_t1621280361_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_Dictionary_2_t1621280361_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_Dictionary_2_t1621280361_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_Dictionary_2_t1621280361_0_0_0_KeyValuePair_2_t1315983480_0_0_0_Types[] = { (&Type_t_0_0_0), (&Dictionary_2_t1621280361_0_0_0), (&KeyValuePair_2_t1315983480_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_Dictionary_2_t1621280361_0_0_0_KeyValuePair_2_t1315983480_0_0_0 = { 3, GenInst_Type_t_0_0_0_Dictionary_2_t1621280361_0_0_0_KeyValuePair_2_t1315983480_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1315983480_0_0_0_Types[] = { (&KeyValuePair_2_t1315983480_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1315983480_0_0_0 = { 1, GenInst_KeyValuePair_2_t1315983480_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_List_1_t3606228803_0_0_0_Types[] = { (&String_t_0_0_0), (&List_1_t3606228803_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t3606228803_0_0_0 = { 2, GenInst_String_t_0_0_0_List_1_t3606228803_0_0_0_Types };
static const RuntimeType* GenInst_OnReceiveEvent_t4237107671_0_0_0_Types[] = { (&OnReceiveEvent_t4237107671_0_0_0) };
extern const Il2CppGenericInst GenInst_OnReceiveEvent_t4237107671_0_0_0 = { 1, GenInst_OnReceiveEvent_t4237107671_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_List_1_t3606228803_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&String_t_0_0_0), (&List_1_t3606228803_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t3606228803_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_List_1_t3606228803_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_List_1_t3606228803_0_0_0_KeyValuePair_2_t3278353287_0_0_0_Types[] = { (&String_t_0_0_0), (&List_1_t3606228803_0_0_0), (&KeyValuePair_2_t3278353287_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t3606228803_0_0_0_KeyValuePair_2_t3278353287_0_0_0 = { 3, GenInst_String_t_0_0_0_List_1_t3606228803_0_0_0_KeyValuePair_2_t3278353287_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3278353287_0_0_0_Types[] = { (&KeyValuePair_2_t3278353287_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3278353287_0_0_0 = { 1, GenInst_KeyValuePair_2_t3278353287_0_0_0_Types };
static const RuntimeType* GenInst_AsyncEvent_t2332674247_0_0_0_Types[] = { (&AsyncEvent_t2332674247_0_0_0) };
extern const Il2CppGenericInst GenInst_AsyncEvent_t2332674247_0_0_0 = { 1, GenInst_AsyncEvent_t2332674247_0_0_0_Types };
static const RuntimeType* GenInst_KeyEventManager_t3314674286_0_0_0_Types[] = { (&KeyEventManager_t3314674286_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyEventManager_t3314674286_0_0_0 = { 1, GenInst_KeyEventManager_t3314674286_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_String_t_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&String_t_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_String_t_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_String_t_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&String_t_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_String_t_0_0_0_KeyValuePair_2_t3089358386_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&String_t_0_0_0), (&KeyValuePair_2_t3089358386_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_String_t_0_0_0_KeyValuePair_2_t3089358386_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_String_t_0_0_0_KeyValuePair_2_t3089358386_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3089358386_0_0_0_Types[] = { (&KeyValuePair_2_t3089358386_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3089358386_0_0_0 = { 1, GenInst_KeyValuePair_2_t3089358386_0_0_0_Types };
static const RuntimeType* GenInst_Runnable_t1422227699_0_0_0_Types[] = { (&Runnable_t1422227699_0_0_0) };
extern const Il2CppGenericInst GenInst_Runnable_t1422227699_0_0_0 = { 1, GenInst_Runnable_t1422227699_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_Routine_t1973058165_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&Routine_t1973058165_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Routine_t1973058165_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_Routine_t1973058165_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_Routine_t1973058165_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&Routine_t1973058165_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Routine_t1973058165_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Routine_t1973058165_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_Routine_t1973058165_0_0_0_KeyValuePair_2_t3033196318_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&Routine_t1973058165_0_0_0), (&KeyValuePair_2_t3033196318_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Routine_t1973058165_0_0_0_KeyValuePair_2_t3033196318_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Routine_t1973058165_0_0_0_KeyValuePair_2_t3033196318_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3033196318_0_0_0_Types[] = { (&KeyValuePair_2_t3033196318_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3033196318_0_0_0 = { 1, GenInst_KeyValuePair_2_t3033196318_0_0_0_Types };
static const RuntimeType* GenInst_Routine_t1973058165_0_0_0_Types[] = { (&Routine_t1973058165_0_0_0) };
extern const Il2CppGenericInst GenInst_Routine_t1973058165_0_0_0 = { 1, GenInst_Routine_t1973058165_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_List_1_t1441188381_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&List_1_t1441188381_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_List_1_t1441188381_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_List_1_t1441188381_0_0_0_Types };
static const RuntimeType* GenInst_TouchEventData_t2072067249_0_0_0_Types[] = { (&TouchEventData_t2072067249_0_0_0) };
extern const Il2CppGenericInst GenInst_TouchEventData_t2072067249_0_0_0 = { 1, GenInst_TouchEventData_t2072067249_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_List_1_t1441188381_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&List_1_t1441188381_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_List_1_t1441188381_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_List_1_t1441188381_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_List_1_t1441188381_0_0_0_KeyValuePair_2_t2501326534_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&List_1_t1441188381_0_0_0), (&KeyValuePair_2_t2501326534_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_List_1_t1441188381_0_0_0_KeyValuePair_2_t2501326534_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_List_1_t1441188381_0_0_0_KeyValuePair_2_t2501326534_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2501326534_0_0_0_Types[] = { (&KeyValuePair_2_t2501326534_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2501326534_0_0_0 = { 1, GenInst_KeyValuePair_2_t2501326534_0_0_0_Types };
static const RuntimeType* GenInst_EventArgs_t3289624707_0_0_0_Types[] = { (&EventArgs_t3289624707_0_0_0) };
extern const Il2CppGenericInst GenInst_EventArgs_t3289624707_0_0_0 = { 1, GenInst_EventArgs_t3289624707_0_0_0_Types };
static const RuntimeType* GenInst_Collider_t3497673348_0_0_0_Types[] = { (&Collider_t3497673348_0_0_0) };
extern const Il2CppGenericInst GenInst_Collider_t3497673348_0_0_0 = { 1, GenInst_Collider_t3497673348_0_0_0_Types };
static const RuntimeType* GenInst_Collider2D_t646061738_0_0_0_Types[] = { (&Collider2D_t646061738_0_0_0) };
extern const Il2CppGenericInst GenInst_Collider2D_t646061738_0_0_0 = { 1, GenInst_Collider2D_t646061738_0_0_0_Types };
static const RuntimeType* GenInst_Mapping_t2602146240_0_0_0_Types[] = { (&Mapping_t2602146240_0_0_0) };
extern const Il2CppGenericInst GenInst_Mapping_t2602146240_0_0_0 = { 1, GenInst_Mapping_t2602146240_0_0_0_Types };
static const RuntimeType* GenInst_Mapping_t1442940263_0_0_0_Types[] = { (&Mapping_t1442940263_0_0_0) };
extern const Il2CppGenericInst GenInst_Mapping_t1442940263_0_0_0 = { 1, GenInst_Mapping_t1442940263_0_0_0_Types };
static const RuntimeType* GenInst_Mapping_t3124476604_0_0_0_Types[] = { (&Mapping_t3124476604_0_0_0) };
extern const Il2CppGenericInst GenInst_Mapping_t3124476604_0_0_0 = { 1, GenInst_Mapping_t3124476604_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_List_1_t1398341365_0_0_0_Types[] = { (&String_t_0_0_0), (&List_1_t1398341365_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t1398341365_0_0_0 = { 2, GenInst_String_t_0_0_0_List_1_t1398341365_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_List_1_t1398341365_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&String_t_0_0_0), (&List_1_t1398341365_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t1398341365_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_List_1_t1398341365_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_List_1_t1398341365_0_0_0_KeyValuePair_2_t1070465850_0_0_0_Types[] = { (&String_t_0_0_0), (&List_1_t1398341365_0_0_0), (&KeyValuePair_2_t1070465850_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t1398341365_0_0_0_KeyValuePair_2_t1070465850_0_0_0 = { 3, GenInst_String_t_0_0_0_List_1_t1398341365_0_0_0_KeyValuePair_2_t1070465850_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1070465850_0_0_0_Types[] = { (&KeyValuePair_2_t1070465850_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1070465850_0_0_0 = { 1, GenInst_KeyValuePair_2_t1070465850_0_0_0_Types };
static const RuntimeType* GenInst_ClassEventMapping_t3594451529_0_0_0_Types[] = { (&ClassEventMapping_t3594451529_0_0_0) };
extern const Il2CppGenericInst GenInst_ClassEventMapping_t3594451529_0_0_0 = { 1, GenInst_ClassEventMapping_t3594451529_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t3743315550_0_0_0_Types[] = { (&List_1_t3743315550_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t3743315550_0_0_0 = { 1, GenInst_List_1_t3743315550_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t3738683066_0_0_0_Types[] = { (&List_1_t3738683066_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t3738683066_0_0_0 = { 1, GenInst_List_1_t3738683066_0_0_0_Types };
static const RuntimeType* GenInst_Speech_t1663141205_0_0_0_Types[] = { (&Speech_t1663141205_0_0_0) };
extern const Il2CppGenericInst GenInst_Speech_t1663141205_0_0_0 = { 1, GenInst_Speech_t1663141205_0_0_0_Types };
static const RuntimeType* GenInst_TapEventMapping_t2308161515_0_0_0_Types[] = { (&TapEventMapping_t2308161515_0_0_0) };
extern const Il2CppGenericInst GenInst_TapEventMapping_t2308161515_0_0_0 = { 1, GenInst_TapEventMapping_t2308161515_0_0_0_Types };
static const RuntimeType* GenInst_FullScreenDragEventMapping_t3872427665_0_0_0_Types[] = { (&FullScreenDragEventMapping_t3872427665_0_0_0) };
extern const Il2CppGenericInst GenInst_FullScreenDragEventMapping_t3872427665_0_0_0 = { 1, GenInst_FullScreenDragEventMapping_t3872427665_0_0_0_Types };
static const RuntimeType* GenInst_Input_t3157785889_0_0_0_Types[] = { (&Input_t3157785889_0_0_0) };
extern const Il2CppGenericInst GenInst_Input_t3157785889_0_0_0 = { 1, GenInst_Input_t3157785889_0_0_0_Types };
static const RuntimeType* GenInst_Output_t220081548_0_0_0_Types[] = { (&Output_t220081548_0_0_0) };
extern const Il2CppGenericInst GenInst_Output_t220081548_0_0_0 = { 1, GenInst_Output_t220081548_0_0_0_Types };
static const RuntimeType* GenInst_Widget_t3624771850_0_0_0_Types[] = { (&Widget_t3624771850_0_0_0) };
extern const Il2CppGenericInst GenInst_Widget_t3624771850_0_0_0 = { 1, GenInst_Widget_t3624771850_0_0_0_Types };
static const RuntimeType* GenInst_Connection_t2486334317_0_0_0_Types[] = { (&Connection_t2486334317_0_0_0) };
extern const Il2CppGenericInst GenInst_Connection_t2486334317_0_0_0 = { 1, GenInst_Connection_t2486334317_0_0_0_Types };
static const RuntimeType* GenInst_fsData_t2583805605_0_0_0_Types[] = { (&fsData_t2583805605_0_0_0) };
extern const Il2CppGenericInst GenInst_fsData_t2583805605_0_0_0 = { 1, GenInst_fsData_t2583805605_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_fsData_t2583805605_0_0_0_Types[] = { (&String_t_0_0_0), (&fsData_t2583805605_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_fsData_t2583805605_0_0_0 = { 2, GenInst_String_t_0_0_0_fsData_t2583805605_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_fsData_t2583805605_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&String_t_0_0_0), (&fsData_t2583805605_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_fsData_t2583805605_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_fsData_t2583805605_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_fsData_t2583805605_0_0_0_KeyValuePair_2_t2255930089_0_0_0_Types[] = { (&String_t_0_0_0), (&fsData_t2583805605_0_0_0), (&KeyValuePair_2_t2255930089_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_fsData_t2583805605_0_0_0_KeyValuePair_2_t2255930089_0_0_0 = { 3, GenInst_String_t_0_0_0_fsData_t2583805605_0_0_0_KeyValuePair_2_t2255930089_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2255930089_0_0_0_Types[] = { (&KeyValuePair_2_t2255930089_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2255930089_0_0_0 = { 1, GenInst_KeyValuePair_2_t2255930089_0_0_0_Types };
static const RuntimeType* GenInst_fsDataType_t1645355485_0_0_0_Types[] = { (&fsDataType_t1645355485_0_0_0) };
extern const Il2CppGenericInst GenInst_fsDataType_t1645355485_0_0_0 = { 1, GenInst_fsDataType_t1645355485_0_0_0_Types };
static const RuntimeType* GenInst_fsMetaProperty_t2249223145_0_0_0_Types[] = { (&fsMetaProperty_t2249223145_0_0_0) };
extern const Il2CppGenericInst GenInst_fsMetaProperty_t2249223145_0_0_0 = { 1, GenInst_fsMetaProperty_t2249223145_0_0_0_Types };
static const RuntimeType* GenInst_AnimationCurve_t3306541151_0_0_0_Types[] = { (&AnimationCurve_t3306541151_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimationCurve_t3306541151_0_0_0 = { 1, GenInst_AnimationCurve_t3306541151_0_0_0_Types };
static const RuntimeType* GenInst_Bounds_t3033363703_0_0_0_Types[] = { (&Bounds_t3033363703_0_0_0) };
extern const Il2CppGenericInst GenInst_Bounds_t3033363703_0_0_0 = { 1, GenInst_Bounds_t3033363703_0_0_0_Types };
static const RuntimeType* GenInst_Gradient_t3600583008_0_0_0_Types[] = { (&Gradient_t3600583008_0_0_0) };
extern const Il2CppGenericInst GenInst_Gradient_t3600583008_0_0_0 = { 1, GenInst_Gradient_t3600583008_0_0_0_Types };
static const RuntimeType* GenInst_LayerMask_t3188175821_0_0_0_Types[] = { (&LayerMask_t3188175821_0_0_0) };
extern const Il2CppGenericInst GenInst_LayerMask_t3188175821_0_0_0 = { 1, GenInst_LayerMask_t3188175821_0_0_0_Types };
static const RuntimeType* GenInst_Rect_t3681755626_0_0_0_Types[] = { (&Rect_t3681755626_0_0_0) };
extern const Il2CppGenericInst GenInst_Rect_t3681755626_0_0_0 = { 1, GenInst_Rect_t3681755626_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_String_t_0_0_0_Types[] = { (&Type_t_0_0_0), (&String_t_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_String_t_0_0_0 = { 2, GenInst_Type_t_0_0_0_String_t_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Type_t_0_0_0), (&String_t_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_String_t_0_0_0_KeyValuePair_2_t1723923352_0_0_0_Types[] = { (&Type_t_0_0_0), (&String_t_0_0_0), (&KeyValuePair_2_t1723923352_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_String_t_0_0_0_KeyValuePair_2_t1723923352_0_0_0 = { 3, GenInst_Type_t_0_0_0_String_t_0_0_0_KeyValuePair_2_t1723923352_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1723923352_0_0_0_Types[] = { (&KeyValuePair_2_t1723923352_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1723923352_0_0_0 = { 1, GenInst_KeyValuePair_2_t1723923352_0_0_0_Types };
static const RuntimeType* GenInst_AotCompilation_t21908242_0_0_0_Types[] = { (&AotCompilation_t21908242_0_0_0) };
extern const Il2CppGenericInst GenInst_AotCompilation_t21908242_0_0_0 = { 1, GenInst_AotCompilation_t21908242_0_0_0_Types };
static const RuntimeType* GenInst_fsDataType_t1645355485_0_0_0_String_t_0_0_0_Types[] = { (&fsDataType_t1645355485_0_0_0), (&String_t_0_0_0) };
extern const Il2CppGenericInst GenInst_fsDataType_t1645355485_0_0_0_String_t_0_0_0 = { 2, GenInst_fsDataType_t1645355485_0_0_0_String_t_0_0_0_Types };
static const RuntimeType* GenInst_fsDataType_t1645355485_0_0_0_RuntimeObject_0_0_0_Types[] = { (&fsDataType_t1645355485_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_fsDataType_t1645355485_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_fsDataType_t1645355485_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_RuntimeObject_0_0_0_Types[] = { (&Type_t_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_Type_t_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Type_t_0_0_0), (&RuntimeObject_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t2384152414_0_0_0_Types[] = { (&Type_t_0_0_0), (&RuntimeObject_0_0_0), (&KeyValuePair_2_t2384152414_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t2384152414_0_0_0 = { 3, GenInst_Type_t_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t2384152414_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2384152414_0_0_0_Types[] = { (&KeyValuePair_2_t2384152414_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2384152414_0_0_0 = { 1, GenInst_KeyValuePair_2_t2384152414_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_fsData_t2583805605_0_0_0_String_t_0_0_0_Types[] = { (&String_t_0_0_0), (&fsData_t2583805605_0_0_0), (&String_t_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_fsData_t2583805605_0_0_0_String_t_0_0_0 = { 3, GenInst_String_t_0_0_0_fsData_t2583805605_0_0_0_String_t_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_fsBaseConverter_t1241677426_0_0_0_Types[] = { (&Type_t_0_0_0), (&fsBaseConverter_t1241677426_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_fsBaseConverter_t1241677426_0_0_0 = { 2, GenInst_Type_t_0_0_0_fsBaseConverter_t1241677426_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_fsBaseConverter_t1241677426_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Type_t_0_0_0), (&fsBaseConverter_t1241677426_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_fsBaseConverter_t1241677426_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_fsBaseConverter_t1241677426_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_fsBaseConverter_t1241677426_0_0_0_KeyValuePair_2_t936380545_0_0_0_Types[] = { (&Type_t_0_0_0), (&fsBaseConverter_t1241677426_0_0_0), (&KeyValuePair_2_t936380545_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_fsBaseConverter_t1241677426_0_0_0_KeyValuePair_2_t936380545_0_0_0 = { 3, GenInst_Type_t_0_0_0_fsBaseConverter_t1241677426_0_0_0_KeyValuePair_2_t936380545_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t936380545_0_0_0_Types[] = { (&KeyValuePair_2_t936380545_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t936380545_0_0_0 = { 1, GenInst_KeyValuePair_2_t936380545_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_List_1_t2055375476_0_0_0_Types[] = { (&Type_t_0_0_0), (&List_1_t2055375476_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_List_1_t2055375476_0_0_0 = { 2, GenInst_Type_t_0_0_0_List_1_t2055375476_0_0_0_Types };
static const RuntimeType* GenInst_fsObjectProcessor_t2686254344_0_0_0_Types[] = { (&fsObjectProcessor_t2686254344_0_0_0) };
extern const Il2CppGenericInst GenInst_fsObjectProcessor_t2686254344_0_0_0 = { 1, GenInst_fsObjectProcessor_t2686254344_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_List_1_t2055375476_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Type_t_0_0_0), (&List_1_t2055375476_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_List_1_t2055375476_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_List_1_t2055375476_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_List_1_t2055375476_0_0_0_KeyValuePair_2_t1750078595_0_0_0_Types[] = { (&Type_t_0_0_0), (&List_1_t2055375476_0_0_0), (&KeyValuePair_2_t1750078595_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_List_1_t2055375476_0_0_0_KeyValuePair_2_t1750078595_0_0_0 = { 3, GenInst_Type_t_0_0_0_List_1_t2055375476_0_0_0_KeyValuePair_2_t1750078595_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1750078595_0_0_0_Types[] = { (&KeyValuePair_2_t1750078595_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1750078595_0_0_0 = { 1, GenInst_KeyValuePair_2_t1750078595_0_0_0_Types };
static const RuntimeType* GenInst_fsConverter_t466758137_0_0_0_Types[] = { (&fsConverter_t466758137_0_0_0) };
extern const Il2CppGenericInst GenInst_fsConverter_t466758137_0_0_0 = { 1, GenInst_fsConverter_t466758137_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_fsDirectConverter_t763460818_0_0_0_Types[] = { (&Type_t_0_0_0), (&fsDirectConverter_t763460818_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_fsDirectConverter_t763460818_0_0_0 = { 2, GenInst_Type_t_0_0_0_fsDirectConverter_t763460818_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_fsDirectConverter_t763460818_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Type_t_0_0_0), (&fsDirectConverter_t763460818_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_fsDirectConverter_t763460818_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_fsDirectConverter_t763460818_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_fsDirectConverter_t763460818_0_0_0_KeyValuePair_2_t458163937_0_0_0_Types[] = { (&Type_t_0_0_0), (&fsDirectConverter_t763460818_0_0_0), (&KeyValuePair_2_t458163937_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_fsDirectConverter_t763460818_0_0_0_KeyValuePair_2_t458163937_0_0_0 = { 3, GenInst_Type_t_0_0_0_fsDirectConverter_t763460818_0_0_0_KeyValuePair_2_t458163937_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t458163937_0_0_0_Types[] = { (&KeyValuePair_2_t458163937_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t458163937_0_0_0 = { 1, GenInst_KeyValuePair_2_t458163937_0_0_0_Types };
static const RuntimeType* GenInst_fsVersionedType_t654750358_0_0_0_Types[] = { (&fsVersionedType_t654750358_0_0_0) };
extern const Il2CppGenericInst GenInst_fsVersionedType_t654750358_0_0_0 = { 1, GenInst_fsVersionedType_t654750358_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_fsData_t2583805605_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&fsData_t2583805605_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_fsData_t2583805605_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_fsData_t2583805605_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_fsData_t2583805605_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&fsData_t2583805605_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_fsData_t2583805605_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_fsData_t2583805605_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_fsData_t2583805605_0_0_0_KeyValuePair_2_t3643943758_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&fsData_t2583805605_0_0_0), (&KeyValuePair_2_t3643943758_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_fsData_t2583805605_0_0_0_KeyValuePair_2_t3643943758_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_fsData_t2583805605_0_0_0_KeyValuePair_2_t3643943758_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3643943758_0_0_0_Types[] = { (&KeyValuePair_2_t3643943758_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3643943758_0_0_0 = { 1, GenInst_KeyValuePair_2_t3643943758_0_0_0_Types };
static const RuntimeType* GenInst_Link_t247561424_0_0_0_Types[] = { (&Link_t247561424_0_0_0) };
extern const Il2CppGenericInst GenInst_Link_t247561424_0_0_0 = { 1, GenInst_Link_t247561424_0_0_0_Types };
static const RuntimeType* GenInst_AttributeQuery_t604298480_0_0_0_Attribute_t542643598_0_0_0_Types[] = { (&AttributeQuery_t604298480_0_0_0), (&Attribute_t542643598_0_0_0) };
extern const Il2CppGenericInst GenInst_AttributeQuery_t604298480_0_0_0_Attribute_t542643598_0_0_0 = { 2, GenInst_AttributeQuery_t604298480_0_0_0_Attribute_t542643598_0_0_0_Types };
static const RuntimeType* GenInst_AttributeQuery_t604298480_0_0_0_RuntimeObject_0_0_0_Types[] = { (&AttributeQuery_t604298480_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_AttributeQuery_t604298480_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_AttributeQuery_t604298480_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t288805040_0_0_0_Types[] = { (&KeyValuePair_2_t288805040_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t288805040_0_0_0 = { 1, GenInst_KeyValuePair_2_t288805040_0_0_0_Types };
static const RuntimeType* GenInst_AttributeQuery_t604298480_0_0_0_Types[] = { (&AttributeQuery_t604298480_0_0_0) };
extern const Il2CppGenericInst GenInst_AttributeQuery_t604298480_0_0_0 = { 1, GenInst_AttributeQuery_t604298480_0_0_0_Types };
static const RuntimeType* GenInst_AttributeQuery_t604298480_0_0_0_RuntimeObject_0_0_0_AttributeQuery_t604298480_0_0_0_Types[] = { (&AttributeQuery_t604298480_0_0_0), (&RuntimeObject_0_0_0), (&AttributeQuery_t604298480_0_0_0) };
extern const Il2CppGenericInst GenInst_AttributeQuery_t604298480_0_0_0_RuntimeObject_0_0_0_AttributeQuery_t604298480_0_0_0 = { 3, GenInst_AttributeQuery_t604298480_0_0_0_RuntimeObject_0_0_0_AttributeQuery_t604298480_0_0_0_Types };
static const RuntimeType* GenInst_AttributeQuery_t604298480_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types[] = { (&AttributeQuery_t604298480_0_0_0), (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_AttributeQuery_t604298480_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0 = { 3, GenInst_AttributeQuery_t604298480_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_AttributeQuery_t604298480_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&AttributeQuery_t604298480_0_0_0), (&RuntimeObject_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_AttributeQuery_t604298480_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_AttributeQuery_t604298480_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_AttributeQuery_t604298480_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t288805040_0_0_0_Types[] = { (&AttributeQuery_t604298480_0_0_0), (&RuntimeObject_0_0_0), (&KeyValuePair_2_t288805040_0_0_0) };
extern const Il2CppGenericInst GenInst_AttributeQuery_t604298480_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t288805040_0_0_0 = { 3, GenInst_AttributeQuery_t604298480_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t288805040_0_0_0_Types };
static const RuntimeType* GenInst_AttributeQuery_t604298480_0_0_0_Attribute_t542643598_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&AttributeQuery_t604298480_0_0_0), (&Attribute_t542643598_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_AttributeQuery_t604298480_0_0_0_Attribute_t542643598_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_AttributeQuery_t604298480_0_0_0_Attribute_t542643598_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_AttributeQuery_t604298480_0_0_0_Attribute_t542643598_0_0_0_KeyValuePair_2_t2436966639_0_0_0_Types[] = { (&AttributeQuery_t604298480_0_0_0), (&Attribute_t542643598_0_0_0), (&KeyValuePair_2_t2436966639_0_0_0) };
extern const Il2CppGenericInst GenInst_AttributeQuery_t604298480_0_0_0_Attribute_t542643598_0_0_0_KeyValuePair_2_t2436966639_0_0_0 = { 3, GenInst_AttributeQuery_t604298480_0_0_0_Attribute_t542643598_0_0_0_KeyValuePair_2_t2436966639_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2436966639_0_0_0_Types[] = { (&KeyValuePair_2_t2436966639_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2436966639_0_0_0 = { 1, GenInst_KeyValuePair_2_t2436966639_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_fsOption_1_t972008496_0_0_0_Types[] = { (&Type_t_0_0_0), (&fsOption_1_t972008496_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_fsOption_1_t972008496_0_0_0 = { 2, GenInst_Type_t_0_0_0_fsOption_1_t972008496_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_fsOption_1_t972008496_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&fsOption_1_t972008496_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_fsOption_1_t972008496_0_0_0 = { 2, GenInst_RuntimeObject_0_0_0_fsOption_1_t972008496_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2616381142_0_0_0_Types[] = { (&KeyValuePair_2_t2616381142_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2616381142_0_0_0 = { 1, GenInst_KeyValuePair_2_t2616381142_0_0_0_Types };
static const RuntimeType* GenInst_fsOption_1_t972008496_0_0_0_Types[] = { (&fsOption_1_t972008496_0_0_0) };
extern const Il2CppGenericInst GenInst_fsOption_1_t972008496_0_0_0 = { 1, GenInst_fsOption_1_t972008496_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_fsOption_1_t972008496_0_0_0_RuntimeObject_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&fsOption_1_t972008496_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_fsOption_1_t972008496_0_0_0_RuntimeObject_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_fsOption_1_t972008496_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_fsOption_1_t972008496_0_0_0_fsOption_1_t972008496_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&fsOption_1_t972008496_0_0_0), (&fsOption_1_t972008496_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_fsOption_1_t972008496_0_0_0_fsOption_1_t972008496_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_fsOption_1_t972008496_0_0_0_fsOption_1_t972008496_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_fsOption_1_t972008496_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&fsOption_1_t972008496_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_fsOption_1_t972008496_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_fsOption_1_t972008496_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_fsOption_1_t972008496_0_0_0_KeyValuePair_2_t2616381142_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&fsOption_1_t972008496_0_0_0), (&KeyValuePair_2_t2616381142_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_fsOption_1_t972008496_0_0_0_KeyValuePair_2_t2616381142_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_fsOption_1_t972008496_0_0_0_KeyValuePair_2_t2616381142_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_fsOption_1_t972008496_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Type_t_0_0_0), (&fsOption_1_t972008496_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_fsOption_1_t972008496_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_fsOption_1_t972008496_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_fsOption_1_t972008496_0_0_0_KeyValuePair_2_t666711615_0_0_0_Types[] = { (&Type_t_0_0_0), (&fsOption_1_t972008496_0_0_0), (&KeyValuePair_2_t666711615_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_fsOption_1_t972008496_0_0_0_KeyValuePair_2_t666711615_0_0_0 = { 3, GenInst_Type_t_0_0_0_fsOption_1_t972008496_0_0_0_KeyValuePair_2_t666711615_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t666711615_0_0_0_Types[] = { (&KeyValuePair_2_t666711615_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t666711615_0_0_0 = { 1, GenInst_KeyValuePair_2_t666711615_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Type_t_0_0_0_Types[] = { (&String_t_0_0_0), (&Type_t_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Type_t_0_0_0 = { 2, GenInst_String_t_0_0_0_Type_t_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Type_t_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&String_t_0_0_0), (&Type_t_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Type_t_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_Type_t_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Type_t_0_0_0_KeyValuePair_2_t975927710_0_0_0_Types[] = { (&String_t_0_0_0), (&Type_t_0_0_0), (&KeyValuePair_2_t975927710_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Type_t_0_0_0_KeyValuePair_2_t975927710_0_0_0 = { 3, GenInst_String_t_0_0_0_Type_t_0_0_0_KeyValuePair_2_t975927710_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t975927710_0_0_0_Types[] = { (&KeyValuePair_2_t975927710_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t975927710_0_0_0 = { 1, GenInst_KeyValuePair_2_t975927710_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_fsMetaType_t3266798926_0_0_0_Types[] = { (&Type_t_0_0_0), (&fsMetaType_t3266798926_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_fsMetaType_t3266798926_0_0_0 = { 2, GenInst_Type_t_0_0_0_fsMetaType_t3266798926_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_fsMetaType_t3266798926_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Type_t_0_0_0), (&fsMetaType_t3266798926_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_fsMetaType_t3266798926_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_fsMetaType_t3266798926_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_fsMetaType_t3266798926_0_0_0_KeyValuePair_2_t2961502045_0_0_0_Types[] = { (&Type_t_0_0_0), (&fsMetaType_t3266798926_0_0_0), (&KeyValuePair_2_t2961502045_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_fsMetaType_t3266798926_0_0_0_KeyValuePair_2_t2961502045_0_0_0 = { 3, GenInst_Type_t_0_0_0_fsMetaType_t3266798926_0_0_0_KeyValuePair_2_t2961502045_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2961502045_0_0_0_Types[] = { (&KeyValuePair_2_t2961502045_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2961502045_0_0_0 = { 1, GenInst_KeyValuePair_2_t2961502045_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { (&Type_t_0_0_0), (&Boolean_t3825574718_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Type_t_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const RuntimeType* GenInst_TuioObjectMapping_t4080927128_0_0_0_Types[] = { (&TuioObjectMapping_t4080927128_0_0_0) };
extern const Il2CppGenericInst GenInst_TuioObjectMapping_t4080927128_0_0_0 = { 1, GenInst_TuioObjectMapping_t4080927128_0_0_0_Types };
static const RuntimeType* GenInst_TuioCursor_t1850351419_0_0_0_TouchPoint_t959629083_0_0_0_Types[] = { (&TuioCursor_t1850351419_0_0_0), (&TouchPoint_t959629083_0_0_0) };
extern const Il2CppGenericInst GenInst_TuioCursor_t1850351419_0_0_0_TouchPoint_t959629083_0_0_0 = { 2, GenInst_TuioCursor_t1850351419_0_0_0_TouchPoint_t959629083_0_0_0_Types };
static const RuntimeType* GenInst_TuioCursor_t1850351419_0_0_0_TouchPoint_t959629083_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&TuioCursor_t1850351419_0_0_0), (&TouchPoint_t959629083_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_TuioCursor_t1850351419_0_0_0_TouchPoint_t959629083_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_TuioCursor_t1850351419_0_0_0_TouchPoint_t959629083_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_TuioCursor_t1850351419_0_0_0_TouchPoint_t959629083_0_0_0_KeyValuePair_2_t4169991845_0_0_0_Types[] = { (&TuioCursor_t1850351419_0_0_0), (&TouchPoint_t959629083_0_0_0), (&KeyValuePair_2_t4169991845_0_0_0) };
extern const Il2CppGenericInst GenInst_TuioCursor_t1850351419_0_0_0_TouchPoint_t959629083_0_0_0_KeyValuePair_2_t4169991845_0_0_0 = { 3, GenInst_TuioCursor_t1850351419_0_0_0_TouchPoint_t959629083_0_0_0_KeyValuePair_2_t4169991845_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t4169991845_0_0_0_Types[] = { (&KeyValuePair_2_t4169991845_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4169991845_0_0_0 = { 1, GenInst_KeyValuePair_2_t4169991845_0_0_0_Types };
static const RuntimeType* GenInst_TuioBlob_t2046943414_0_0_0_TouchPoint_t959629083_0_0_0_Types[] = { (&TuioBlob_t2046943414_0_0_0), (&TouchPoint_t959629083_0_0_0) };
extern const Il2CppGenericInst GenInst_TuioBlob_t2046943414_0_0_0_TouchPoint_t959629083_0_0_0 = { 2, GenInst_TuioBlob_t2046943414_0_0_0_TouchPoint_t959629083_0_0_0_Types };
static const RuntimeType* GenInst_TuioBlob_t2046943414_0_0_0_TouchPoint_t959629083_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&TuioBlob_t2046943414_0_0_0), (&TouchPoint_t959629083_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_TuioBlob_t2046943414_0_0_0_TouchPoint_t959629083_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_TuioBlob_t2046943414_0_0_0_TouchPoint_t959629083_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_TuioBlob_t2046943414_0_0_0_TouchPoint_t959629083_0_0_0_KeyValuePair_2_t2907529438_0_0_0_Types[] = { (&TuioBlob_t2046943414_0_0_0), (&TouchPoint_t959629083_0_0_0), (&KeyValuePair_2_t2907529438_0_0_0) };
extern const Il2CppGenericInst GenInst_TuioBlob_t2046943414_0_0_0_TouchPoint_t959629083_0_0_0_KeyValuePair_2_t2907529438_0_0_0 = { 3, GenInst_TuioBlob_t2046943414_0_0_0_TouchPoint_t959629083_0_0_0_KeyValuePair_2_t2907529438_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2907529438_0_0_0_Types[] = { (&KeyValuePair_2_t2907529438_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2907529438_0_0_0 = { 1, GenInst_KeyValuePair_2_t2907529438_0_0_0_Types };
static const RuntimeType* GenInst_TuioObject_t1236821014_0_0_0_TouchPoint_t959629083_0_0_0_Types[] = { (&TuioObject_t1236821014_0_0_0), (&TouchPoint_t959629083_0_0_0) };
extern const Il2CppGenericInst GenInst_TuioObject_t1236821014_0_0_0_TouchPoint_t959629083_0_0_0 = { 2, GenInst_TuioObject_t1236821014_0_0_0_TouchPoint_t959629083_0_0_0_Types };
static const RuntimeType* GenInst_TuioObject_t1236821014_0_0_0_TouchPoint_t959629083_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&TuioObject_t1236821014_0_0_0), (&TouchPoint_t959629083_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_TuioObject_t1236821014_0_0_0_TouchPoint_t959629083_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_TuioObject_t1236821014_0_0_0_TouchPoint_t959629083_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_TuioObject_t1236821014_0_0_0_TouchPoint_t959629083_0_0_0_KeyValuePair_2_t2840757246_0_0_0_Types[] = { (&TuioObject_t1236821014_0_0_0), (&TouchPoint_t959629083_0_0_0), (&KeyValuePair_2_t2840757246_0_0_0) };
extern const Il2CppGenericInst GenInst_TuioObject_t1236821014_0_0_0_TouchPoint_t959629083_0_0_0_KeyValuePair_2_t2840757246_0_0_0 = { 3, GenInst_TuioObject_t1236821014_0_0_0_TouchPoint_t959629083_0_0_0_KeyValuePair_2_t2840757246_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2840757246_0_0_0_Types[] = { (&KeyValuePair_2_t2840757246_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2840757246_0_0_0 = { 1, GenInst_KeyValuePair_2_t2840757246_0_0_0_Types };
static const RuntimeType* GenInst_TouchEventArgs_t1917927166_0_0_0_Types[] = { (&TouchEventArgs_t1917927166_0_0_0) };
extern const Il2CppGenericInst GenInst_TouchEventArgs_t1917927166_0_0_0 = { 1, GenInst_TouchEventArgs_t1917927166_0_0_0_Types };
static const RuntimeType* GenInst_TouchPoint_t959629083_0_0_0_Types[] = { (&TouchPoint_t959629083_0_0_0) };
extern const Il2CppGenericInst GenInst_TouchPoint_t959629083_0_0_0 = { 1, GenInst_TouchPoint_t959629083_0_0_0_Types };
static const RuntimeType* GenInst_ITransformGesture_t1540584490_0_0_0_Types[] = { (&ITransformGesture_t1540584490_0_0_0) };
extern const Il2CppGenericInst GenInst_ITransformGesture_t1540584490_0_0_0 = { 1, GenInst_ITransformGesture_t1540584490_0_0_0_Types };
static const RuntimeType* GenInst_Gesture_t2352305985_0_0_0_Types[] = { (&Gesture_t2352305985_0_0_0) };
extern const Il2CppGenericInst GenInst_Gesture_t2352305985_0_0_0 = { 1, GenInst_Gesture_t2352305985_0_0_0_Types };
static const RuntimeType* GenInst_DebuggableMonoBehaviour_t3136086048_0_0_0_Types[] = { (&DebuggableMonoBehaviour_t3136086048_0_0_0) };
extern const Il2CppGenericInst GenInst_DebuggableMonoBehaviour_t3136086048_0_0_0 = { 1, GenInst_DebuggableMonoBehaviour_t3136086048_0_0_0_Types };
static const RuntimeType* GenInst_IDebuggable_t2346276731_0_0_0_Types[] = { (&IDebuggable_t2346276731_0_0_0) };
extern const Il2CppGenericInst GenInst_IDebuggable_t2346276731_0_0_0 = { 1, GenInst_IDebuggable_t2346276731_0_0_0_Types };
static const RuntimeType* GenInst_TouchProxyBase_t4188753234_0_0_0_Types[] = { (&TouchProxyBase_t4188753234_0_0_0) };
extern const Il2CppGenericInst GenInst_TouchProxyBase_t4188753234_0_0_0 = { 1, GenInst_TouchProxyBase_t4188753234_0_0_0_Types };
static const RuntimeType* GenInst_TouchProxyBase_t4188753234_0_0_0_TouchProxyBase_t4188753234_0_0_0_Types[] = { (&TouchProxyBase_t4188753234_0_0_0), (&TouchProxyBase_t4188753234_0_0_0) };
extern const Il2CppGenericInst GenInst_TouchProxyBase_t4188753234_0_0_0_TouchProxyBase_t4188753234_0_0_0 = { 2, GenInst_TouchProxyBase_t4188753234_0_0_0_TouchProxyBase_t4188753234_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_TouchProxyBase_t4188753234_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&TouchProxyBase_t4188753234_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_TouchProxyBase_t4188753234_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_TouchProxyBase_t4188753234_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_TouchProxyBase_t4188753234_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&TouchProxyBase_t4188753234_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_TouchProxyBase_t4188753234_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_TouchProxyBase_t4188753234_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_TouchProxyBase_t4188753234_0_0_0_KeyValuePair_2_t953924091_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&TouchProxyBase_t4188753234_0_0_0), (&KeyValuePair_2_t953924091_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_TouchProxyBase_t4188753234_0_0_0_KeyValuePair_2_t953924091_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_TouchProxyBase_t4188753234_0_0_0_KeyValuePair_2_t953924091_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t953924091_0_0_0_Types[] = { (&KeyValuePair_2_t953924091_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t953924091_0_0_0 = { 1, GenInst_KeyValuePair_2_t953924091_0_0_0_Types };
static const RuntimeType* GenInst_GestureManagerInstance_t505647059_0_0_0_Types[] = { (&GestureManagerInstance_t505647059_0_0_0) };
extern const Il2CppGenericInst GenInst_GestureManagerInstance_t505647059_0_0_0 = { 1, GenInst_GestureManagerInstance_t505647059_0_0_0_Types };
static const RuntimeType* GenInst_IGestureManager_t4266705231_0_0_0_Types[] = { (&IGestureManager_t4266705231_0_0_0) };
extern const Il2CppGenericInst GenInst_IGestureManager_t4266705231_0_0_0 = { 1, GenInst_IGestureManager_t4266705231_0_0_0_Types };
static const RuntimeType* GenInst_Gesture_t2352305985_0_0_0_IList_1_t1500569684_0_0_0_Types[] = { (&Gesture_t2352305985_0_0_0), (&IList_1_t1500569684_0_0_0) };
extern const Il2CppGenericInst GenInst_Gesture_t2352305985_0_0_0_IList_1_t1500569684_0_0_0 = { 2, GenInst_Gesture_t2352305985_0_0_0_IList_1_t1500569684_0_0_0_Types };
static const RuntimeType* GenInst_Transform_t3275118058_0_0_0_List_1_t328750215_0_0_0_Types[] = { (&Transform_t3275118058_0_0_0), (&List_1_t328750215_0_0_0) };
extern const Il2CppGenericInst GenInst_Transform_t3275118058_0_0_0_List_1_t328750215_0_0_0 = { 2, GenInst_Transform_t3275118058_0_0_0_List_1_t328750215_0_0_0_Types };
static const RuntimeType* GenInst_Transform_t3275118058_0_0_0_List_1_t328750215_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Transform_t3275118058_0_0_0), (&List_1_t328750215_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Transform_t3275118058_0_0_0_List_1_t328750215_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Transform_t3275118058_0_0_0_List_1_t328750215_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_Transform_t3275118058_0_0_0_List_1_t328750215_0_0_0_KeyValuePair_2_t2122717638_0_0_0_Types[] = { (&Transform_t3275118058_0_0_0), (&List_1_t328750215_0_0_0), (&KeyValuePair_2_t2122717638_0_0_0) };
extern const Il2CppGenericInst GenInst_Transform_t3275118058_0_0_0_List_1_t328750215_0_0_0_KeyValuePair_2_t2122717638_0_0_0 = { 3, GenInst_Transform_t3275118058_0_0_0_List_1_t328750215_0_0_0_KeyValuePair_2_t2122717638_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2122717638_0_0_0_Types[] = { (&KeyValuePair_2_t2122717638_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2122717638_0_0_0 = { 1, GenInst_KeyValuePair_2_t2122717638_0_0_0_Types };
static const RuntimeType* GenInst_Gesture_t2352305985_0_0_0_List_1_t328750215_0_0_0_Types[] = { (&Gesture_t2352305985_0_0_0), (&List_1_t328750215_0_0_0) };
extern const Il2CppGenericInst GenInst_Gesture_t2352305985_0_0_0_List_1_t328750215_0_0_0 = { 2, GenInst_Gesture_t2352305985_0_0_0_List_1_t328750215_0_0_0_Types };
static const RuntimeType* GenInst_Gesture_t2352305985_0_0_0_List_1_t328750215_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Gesture_t2352305985_0_0_0), (&List_1_t328750215_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Gesture_t2352305985_0_0_0_List_1_t328750215_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Gesture_t2352305985_0_0_0_List_1_t328750215_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_Gesture_t2352305985_0_0_0_List_1_t328750215_0_0_0_KeyValuePair_2_t2725989683_0_0_0_Types[] = { (&Gesture_t2352305985_0_0_0), (&List_1_t328750215_0_0_0), (&KeyValuePair_2_t2725989683_0_0_0) };
extern const Il2CppGenericInst GenInst_Gesture_t2352305985_0_0_0_List_1_t328750215_0_0_0_KeyValuePair_2_t2725989683_0_0_0 = { 3, GenInst_Gesture_t2352305985_0_0_0_List_1_t328750215_0_0_0_KeyValuePair_2_t2725989683_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2725989683_0_0_0_Types[] = { (&KeyValuePair_2_t2725989683_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2725989683_0_0_0 = { 1, GenInst_KeyValuePair_2_t2725989683_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t1721427117_0_0_0_Types[] = { (&List_1_t1721427117_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t1721427117_0_0_0 = { 1, GenInst_List_1_t1721427117_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t1721427117_0_0_0_List_1_t1721427117_0_0_0_Types[] = { (&List_1_t1721427117_0_0_0), (&List_1_t1721427117_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t1721427117_0_0_0_List_1_t1721427117_0_0_0 = { 2, GenInst_List_1_t1721427117_0_0_0_List_1_t1721427117_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t328750215_0_0_0_Types[] = { (&List_1_t328750215_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t328750215_0_0_0 = { 1, GenInst_List_1_t328750215_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t328750215_0_0_0_List_1_t328750215_0_0_0_Types[] = { (&List_1_t328750215_0_0_0), (&List_1_t328750215_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t328750215_0_0_0_List_1_t328750215_0_0_0 = { 2, GenInst_List_1_t328750215_0_0_0_List_1_t328750215_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t2644239190_0_0_0_Types[] = { (&List_1_t2644239190_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t2644239190_0_0_0 = { 1, GenInst_List_1_t2644239190_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t2644239190_0_0_0_List_1_t2644239190_0_0_0_Types[] = { (&List_1_t2644239190_0_0_0), (&List_1_t2644239190_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t2644239190_0_0_0_List_1_t2644239190_0_0_0 = { 2, GenInst_List_1_t2644239190_0_0_0_List_1_t2644239190_0_0_0_Types };
static const RuntimeType* GenInst_GestureStateChangeEventArgs_t3499981191_0_0_0_Types[] = { (&GestureStateChangeEventArgs_t3499981191_0_0_0) };
extern const Il2CppGenericInst GenInst_GestureStateChangeEventArgs_t3499981191_0_0_0 = { 1, GenInst_GestureStateChangeEventArgs_t3499981191_0_0_0_Types };
static const RuntimeType* GenInst_MetaGestureEventArgs_t256591615_0_0_0_Types[] = { (&MetaGestureEventArgs_t256591615_0_0_0) };
extern const Il2CppGenericInst GenInst_MetaGestureEventArgs_t256591615_0_0_0 = { 1, GenInst_MetaGestureEventArgs_t256591615_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_TouchData_t3880599975_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&TouchData_t3880599975_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_TouchData_t3880599975_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_TouchData_t3880599975_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t645770832_0_0_0_Types[] = { (&KeyValuePair_2_t645770832_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t645770832_0_0_0 = { 1, GenInst_KeyValuePair_2_t645770832_0_0_0_Types };
static const RuntimeType* GenInst_TouchData_t3880599975_0_0_0_Types[] = { (&TouchData_t3880599975_0_0_0) };
extern const Il2CppGenericInst GenInst_TouchData_t3880599975_0_0_0 = { 1, GenInst_TouchData_t3880599975_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_TouchData_t3880599975_0_0_0_Int32_t2071877448_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&TouchData_t3880599975_0_0_0), (&Int32_t2071877448_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_TouchData_t3880599975_0_0_0_Int32_t2071877448_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_TouchData_t3880599975_0_0_0_Int32_t2071877448_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_TouchData_t3880599975_0_0_0_TouchData_t3880599975_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&TouchData_t3880599975_0_0_0), (&TouchData_t3880599975_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_TouchData_t3880599975_0_0_0_TouchData_t3880599975_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_TouchData_t3880599975_0_0_0_TouchData_t3880599975_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_TouchData_t3880599975_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&TouchData_t3880599975_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_TouchData_t3880599975_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_TouchData_t3880599975_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_TouchData_t3880599975_0_0_0_KeyValuePair_2_t645770832_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&TouchData_t3880599975_0_0_0), (&KeyValuePair_2_t645770832_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_TouchData_t3880599975_0_0_0_KeyValuePair_2_t645770832_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_TouchData_t3880599975_0_0_0_KeyValuePair_2_t645770832_0_0_0_Types };
static const RuntimeType* GenInst_Vector2_t2243707579_0_0_0_Tags_t1265380163_0_0_0_Boolean_t3825574718_0_0_0_TouchPoint_t959629083_0_0_0_Types[] = { (&Vector2_t2243707579_0_0_0), (&Tags_t1265380163_0_0_0), (&Boolean_t3825574718_0_0_0), (&TouchPoint_t959629083_0_0_0) };
extern const Il2CppGenericInst GenInst_Vector2_t2243707579_0_0_0_Tags_t1265380163_0_0_0_Boolean_t3825574718_0_0_0_TouchPoint_t959629083_0_0_0 = { 4, GenInst_Vector2_t2243707579_0_0_0_Tags_t1265380163_0_0_0_Boolean_t3825574718_0_0_0_TouchPoint_t959629083_0_0_0_Types };
static const RuntimeType* GenInst_Vector2_t2243707579_0_0_0_RuntimeObject_0_0_0_Boolean_t3825574718_0_0_0_RuntimeObject_0_0_0_Types[] = { (&Vector2_t2243707579_0_0_0), (&RuntimeObject_0_0_0), (&Boolean_t3825574718_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_Vector2_t2243707579_0_0_0_RuntimeObject_0_0_0_Boolean_t3825574718_0_0_0_RuntimeObject_0_0_0 = { 4, GenInst_Vector2_t2243707579_0_0_0_RuntimeObject_0_0_0_Boolean_t3825574718_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_Vector2_t2243707579_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&Vector2_t2243707579_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Vector2_t2243707579_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_Vector2_t2243707579_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_TouchState_t2732082299_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&TouchState_t2732082299_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_TouchState_t2732082299_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_TouchState_t2732082299_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3792220452_0_0_0_Types[] = { (&KeyValuePair_2_t3792220452_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3792220452_0_0_0 = { 1, GenInst_KeyValuePair_2_t3792220452_0_0_0_Types };
static const RuntimeType* GenInst_TouchState_t2732082299_0_0_0_Types[] = { (&TouchState_t2732082299_0_0_0) };
extern const Il2CppGenericInst GenInst_TouchState_t2732082299_0_0_0 = { 1, GenInst_TouchState_t2732082299_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_TouchState_t2732082299_0_0_0_Int32_t2071877448_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&TouchState_t2732082299_0_0_0), (&Int32_t2071877448_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_TouchState_t2732082299_0_0_0_Int32_t2071877448_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_TouchState_t2732082299_0_0_0_Int32_t2071877448_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_TouchState_t2732082299_0_0_0_TouchState_t2732082299_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&TouchState_t2732082299_0_0_0), (&TouchState_t2732082299_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_TouchState_t2732082299_0_0_0_TouchState_t2732082299_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_TouchState_t2732082299_0_0_0_TouchState_t2732082299_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_TouchState_t2732082299_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&TouchState_t2732082299_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_TouchState_t2732082299_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_TouchState_t2732082299_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_TouchState_t2732082299_0_0_0_KeyValuePair_2_t3792220452_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&TouchState_t2732082299_0_0_0), (&KeyValuePair_2_t3792220452_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_TouchState_t2732082299_0_0_0_KeyValuePair_2_t3792220452_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_TouchState_t2732082299_0_0_0_KeyValuePair_2_t3792220452_0_0_0_Types };
static const RuntimeType* GenInst_TouchLayer_t2635439978_0_0_0_Types[] = { (&TouchLayer_t2635439978_0_0_0) };
extern const Il2CppGenericInst GenInst_TouchLayer_t2635439978_0_0_0 = { 1, GenInst_TouchLayer_t2635439978_0_0_0_Types };
static const RuntimeType* GenInst_IInputSource_t3266560338_0_0_0_Types[] = { (&IInputSource_t3266560338_0_0_0) };
extern const Il2CppGenericInst GenInst_IInputSource_t3266560338_0_0_0 = { 1, GenInst_IInputSource_t3266560338_0_0_0_Types };
static const RuntimeType* GenInst_HitTest_t768639505_0_0_0_Types[] = { (&HitTest_t768639505_0_0_0) };
extern const Il2CppGenericInst GenInst_HitTest_t768639505_0_0_0 = { 1, GenInst_HitTest_t768639505_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&Int32_t2071877448_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3132015601_0_0_0_Types[] = { (&KeyValuePair_2_t3132015601_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3132015601_0_0_0 = { 1, GenInst_KeyValuePair_2_t3132015601_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&Int32_t2071877448_0_0_0), (&Int32_t2071877448_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&Int32_t2071877448_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3132015601_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&Int32_t2071877448_0_0_0), (&KeyValuePair_2_t3132015601_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3132015601_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3132015601_0_0_0_Types };
static const RuntimeType* GenInst_TouchLayerEventArgs_t1247401065_0_0_0_Types[] = { (&TouchLayerEventArgs_t1247401065_0_0_0) };
extern const Il2CppGenericInst GenInst_TouchLayerEventArgs_t1247401065_0_0_0 = { 1, GenInst_TouchLayerEventArgs_t1247401065_0_0_0_Types };
static const RuntimeType* GenInst_Canvas_t209405766_0_0_0_ProjectionParams_t2712959773_0_0_0_Types[] = { (&Canvas_t209405766_0_0_0), (&ProjectionParams_t2712959773_0_0_0) };
extern const Il2CppGenericInst GenInst_Canvas_t209405766_0_0_0_ProjectionParams_t2712959773_0_0_0 = { 2, GenInst_Canvas_t209405766_0_0_0_ProjectionParams_t2712959773_0_0_0_Types };
static const RuntimeType* GenInst_Canvas_t209405766_0_0_0_ProjectionParams_t2712959773_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Canvas_t209405766_0_0_0), (&ProjectionParams_t2712959773_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Canvas_t209405766_0_0_0_ProjectionParams_t2712959773_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Canvas_t209405766_0_0_0_ProjectionParams_t2712959773_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_Canvas_t209405766_0_0_0_ProjectionParams_t2712959773_0_0_0_KeyValuePair_2_t523301392_0_0_0_Types[] = { (&Canvas_t209405766_0_0_0), (&ProjectionParams_t2712959773_0_0_0), (&KeyValuePair_2_t523301392_0_0_0) };
extern const Il2CppGenericInst GenInst_Canvas_t209405766_0_0_0_ProjectionParams_t2712959773_0_0_0_KeyValuePair_2_t523301392_0_0_0 = { 3, GenInst_Canvas_t209405766_0_0_0_ProjectionParams_t2712959773_0_0_0_KeyValuePair_2_t523301392_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t523301392_0_0_0_Types[] = { (&KeyValuePair_2_t523301392_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t523301392_0_0_0 = { 1, GenInst_KeyValuePair_2_t523301392_0_0_0_Types };
static const RuntimeType* GenInst_TouchManagerInstance_t2629118981_0_0_0_Types[] = { (&TouchManagerInstance_t2629118981_0_0_0) };
extern const Il2CppGenericInst GenInst_TouchManagerInstance_t2629118981_0_0_0 = { 1, GenInst_TouchManagerInstance_t2629118981_0_0_0_Types };
static const RuntimeType* GenInst_ITouchManager_t2552034033_0_0_0_Types[] = { (&ITouchManager_t2552034033_0_0_0) };
extern const Il2CppGenericInst GenInst_ITouchManager_t2552034033_0_0_0 = { 1, GenInst_ITouchManager_t2552034033_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_TouchPoint_t959629083_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&TouchPoint_t959629083_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_TouchPoint_t959629083_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_TouchPoint_t959629083_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_TouchPoint_t959629083_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&TouchPoint_t959629083_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_TouchPoint_t959629083_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_TouchPoint_t959629083_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_TouchPoint_t959629083_0_0_0_KeyValuePair_2_t2019767236_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&TouchPoint_t959629083_0_0_0), (&KeyValuePair_2_t2019767236_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_TouchPoint_t959629083_0_0_0_KeyValuePair_2_t2019767236_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_TouchPoint_t959629083_0_0_0_KeyValuePair_2_t2019767236_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2019767236_0_0_0_Types[] = { (&KeyValuePair_2_t2019767236_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2019767236_0_0_0 = { 1, GenInst_KeyValuePair_2_t2019767236_0_0_0_Types };
static const RuntimeType* GenInst_TouchPoint_t959629083_0_0_0_TouchPoint_t959629083_0_0_0_Types[] = { (&TouchPoint_t959629083_0_0_0), (&TouchPoint_t959629083_0_0_0) };
extern const Il2CppGenericInst GenInst_TouchPoint_t959629083_0_0_0_TouchPoint_t959629083_0_0_0 = { 2, GenInst_TouchPoint_t959629083_0_0_0_TouchPoint_t959629083_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t1440998580_0_0_0_List_1_t1440998580_0_0_0_Types[] = { (&List_1_t1440998580_0_0_0), (&List_1_t1440998580_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t1440998580_0_0_0_List_1_t1440998580_0_0_0 = { 2, GenInst_List_1_t1440998580_0_0_0_List_1_t1440998580_0_0_0_Types };
static const RuntimeType* GenInst_TouchManager_t3980263048_0_0_0_Types[] = { (&TouchManager_t3980263048_0_0_0) };
extern const Il2CppGenericInst GenInst_TouchManager_t3980263048_0_0_0 = { 1, GenInst_TouchManager_t3980263048_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&Boolean_t3825574718_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const RuntimeType* GenInst_Exception_t1927440687_0_0_0_Types[] = { (&Exception_t1927440687_0_0_0) };
extern const Il2CppGenericInst GenInst_Exception_t1927440687_0_0_0 = { 1, GenInst_Exception_t1927440687_0_0_0_Types };
static const RuntimeType* GenInst_StringU5BU5D_t1642385972_0_0_0_RuntimeObject_0_0_0_Types[] = { (&StringU5BU5D_t1642385972_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_StringU5BU5D_t1642385972_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_StringU5BU5D_t1642385972_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_StringU5BU5D_t1642385972_0_0_0_HttpResponse_t2820540315_0_0_0_Types[] = { (&StringU5BU5D_t1642385972_0_0_0), (&HttpResponse_t2820540315_0_0_0) };
extern const Il2CppGenericInst GenInst_StringU5BU5D_t1642385972_0_0_0_HttpResponse_t2820540315_0_0_0 = { 2, GenInst_StringU5BU5D_t1642385972_0_0_0_HttpResponse_t2820540315_0_0_0_Types };
static const RuntimeType* GenInst_StringU5BU5D_t1642385972_0_0_0_HttpRequest_t1845443631_0_0_0_Types[] = { (&StringU5BU5D_t1642385972_0_0_0), (&HttpRequest_t1845443631_0_0_0) };
extern const Il2CppGenericInst GenInst_StringU5BU5D_t1642385972_0_0_0_HttpRequest_t1845443631_0_0_0 = { 2, GenInst_StringU5BU5D_t1642385972_0_0_0_HttpRequest_t1845443631_0_0_0_Types };
static const RuntimeType* GenInst_HttpResponse_t2820540315_0_0_0_Types[] = { (&HttpResponse_t2820540315_0_0_0) };
extern const Il2CppGenericInst GenInst_HttpResponse_t2820540315_0_0_0 = { 1, GenInst_HttpResponse_t2820540315_0_0_0_Types };
static const RuntimeType* GenInst_HttpRequest_t1845443631_0_0_0_Types[] = { (&HttpRequest_t1845443631_0_0_0) };
extern const Il2CppGenericInst GenInst_HttpRequest_t1845443631_0_0_0 = { 1, GenInst_HttpRequest_t1845443631_0_0_0_Types };
static const RuntimeType* GenInst_Cookie_t1826188460_0_0_0_Types[] = { (&Cookie_t1826188460_0_0_0) };
extern const Il2CppGenericInst GenInst_Cookie_t1826188460_0_0_0 = { 1, GenInst_Cookie_t1826188460_0_0_0_Types };
static const RuntimeType* GenInst_LogData_t4095822710_0_0_0_String_t_0_0_0_Types[] = { (&LogData_t4095822710_0_0_0), (&String_t_0_0_0) };
extern const Il2CppGenericInst GenInst_LogData_t4095822710_0_0_0_String_t_0_0_0 = { 2, GenInst_LogData_t4095822710_0_0_0_String_t_0_0_0_Types };
static const RuntimeType* GenInst_Chunk_t2303927151_0_0_0_Types[] = { (&Chunk_t2303927151_0_0_0) };
extern const Il2CppGenericInst GenInst_Chunk_t2303927151_0_0_0 = { 1, GenInst_Chunk_t2303927151_0_0_0_Types };
static const RuntimeType* GenInst_HttpListenerPrefix_t529778486_0_0_0_Types[] = { (&HttpListenerPrefix_t529778486_0_0_0) };
extern const Il2CppGenericInst GenInst_HttpListenerPrefix_t529778486_0_0_0 = { 1, GenInst_HttpListenerPrefix_t529778486_0_0_0_Types };
static const RuntimeType* GenInst_HttpListenerPrefix_t529778486_0_0_0_HttpListener_t4179429670_0_0_0_Types[] = { (&HttpListenerPrefix_t529778486_0_0_0), (&HttpListener_t4179429670_0_0_0) };
extern const Il2CppGenericInst GenInst_HttpListenerPrefix_t529778486_0_0_0_HttpListener_t4179429670_0_0_0 = { 2, GenInst_HttpListenerPrefix_t529778486_0_0_0_HttpListener_t4179429670_0_0_0_Types };
static const RuntimeType* GenInst_HttpListenerPrefix_t529778486_0_0_0_HttpListener_t4179429670_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&HttpListenerPrefix_t529778486_0_0_0), (&HttpListener_t4179429670_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_HttpListenerPrefix_t529778486_0_0_0_HttpListener_t4179429670_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_HttpListenerPrefix_t529778486_0_0_0_HttpListener_t4179429670_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_HttpListenerPrefix_t529778486_0_0_0_HttpListener_t4179429670_0_0_0_KeyValuePair_2_t2112148073_0_0_0_Types[] = { (&HttpListenerPrefix_t529778486_0_0_0), (&HttpListener_t4179429670_0_0_0), (&KeyValuePair_2_t2112148073_0_0_0) };
extern const Il2CppGenericInst GenInst_HttpListenerPrefix_t529778486_0_0_0_HttpListener_t4179429670_0_0_0_KeyValuePair_2_t2112148073_0_0_0 = { 3, GenInst_HttpListenerPrefix_t529778486_0_0_0_HttpListener_t4179429670_0_0_0_KeyValuePair_2_t2112148073_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2112148073_0_0_0_Types[] = { (&KeyValuePair_2_t2112148073_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2112148073_0_0_0 = { 1, GenInst_KeyValuePair_2_t2112148073_0_0_0_Types };
static const RuntimeType* GenInst_HttpConnection_t2649486862_0_0_0_HttpConnection_t2649486862_0_0_0_Types[] = { (&HttpConnection_t2649486862_0_0_0), (&HttpConnection_t2649486862_0_0_0) };
extern const Il2CppGenericInst GenInst_HttpConnection_t2649486862_0_0_0_HttpConnection_t2649486862_0_0_0 = { 2, GenInst_HttpConnection_t2649486862_0_0_0_HttpConnection_t2649486862_0_0_0_Types };
static const RuntimeType* GenInst_HttpConnection_t2649486862_0_0_0_Types[] = { (&HttpConnection_t2649486862_0_0_0) };
extern const Il2CppGenericInst GenInst_HttpConnection_t2649486862_0_0_0 = { 1, GenInst_HttpConnection_t2649486862_0_0_0_Types };
static const RuntimeType* GenInst_HttpConnection_t2649486862_0_0_0_HttpConnection_t2649486862_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&HttpConnection_t2649486862_0_0_0), (&HttpConnection_t2649486862_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_HttpConnection_t2649486862_0_0_0_HttpConnection_t2649486862_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_HttpConnection_t2649486862_0_0_0_HttpConnection_t2649486862_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_HttpConnection_t2649486862_0_0_0_HttpConnection_t2649486862_0_0_0_KeyValuePair_2_t1689813529_0_0_0_Types[] = { (&HttpConnection_t2649486862_0_0_0), (&HttpConnection_t2649486862_0_0_0), (&KeyValuePair_2_t1689813529_0_0_0) };
extern const Il2CppGenericInst GenInst_HttpConnection_t2649486862_0_0_0_HttpConnection_t2649486862_0_0_0_KeyValuePair_2_t1689813529_0_0_0 = { 3, GenInst_HttpConnection_t2649486862_0_0_0_HttpConnection_t2649486862_0_0_0_KeyValuePair_2_t1689813529_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1689813529_0_0_0_Types[] = { (&KeyValuePair_2_t1689813529_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1689813529_0_0_0 = { 1, GenInst_KeyValuePair_2_t1689813529_0_0_0_Types };
static const RuntimeType* GenInst_HttpListenerPrefix_t529778486_0_0_0_HttpListener_t4179429670_0_0_0_HttpListenerPrefix_t529778486_0_0_0_Types[] = { (&HttpListenerPrefix_t529778486_0_0_0), (&HttpListener_t4179429670_0_0_0), (&HttpListenerPrefix_t529778486_0_0_0) };
extern const Il2CppGenericInst GenInst_HttpListenerPrefix_t529778486_0_0_0_HttpListener_t4179429670_0_0_0_HttpListenerPrefix_t529778486_0_0_0 = { 3, GenInst_HttpListenerPrefix_t529778486_0_0_0_HttpListener_t4179429670_0_0_0_HttpListenerPrefix_t529778486_0_0_0_Types };
static const RuntimeType* GenInst_HttpConnection_t2649486862_0_0_0_HttpConnection_t2649486862_0_0_0_HttpConnection_t2649486862_0_0_0_Types[] = { (&HttpConnection_t2649486862_0_0_0), (&HttpConnection_t2649486862_0_0_0), (&HttpConnection_t2649486862_0_0_0) };
extern const Il2CppGenericInst GenInst_HttpConnection_t2649486862_0_0_0_HttpConnection_t2649486862_0_0_0_HttpConnection_t2649486862_0_0_0 = { 3, GenInst_HttpConnection_t2649486862_0_0_0_HttpConnection_t2649486862_0_0_0_HttpConnection_t2649486862_0_0_0_Types };
static const RuntimeType* GenInst_IPAddress_t1399971723_0_0_0_Dictionary_2_t2945377568_0_0_0_Types[] = { (&IPAddress_t1399971723_0_0_0), (&Dictionary_2_t2945377568_0_0_0) };
extern const Il2CppGenericInst GenInst_IPAddress_t1399971723_0_0_0_Dictionary_2_t2945377568_0_0_0 = { 2, GenInst_IPAddress_t1399971723_0_0_0_Dictionary_2_t2945377568_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_EndPointListener_t3937551933_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&EndPointListener_t3937551933_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_EndPointListener_t3937551933_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_EndPointListener_t3937551933_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_EndPointListener_t3937551933_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&EndPointListener_t3937551933_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_EndPointListener_t3937551933_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_EndPointListener_t3937551933_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_EndPointListener_t3937551933_0_0_0_KeyValuePair_2_t702722790_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&EndPointListener_t3937551933_0_0_0), (&KeyValuePair_2_t702722790_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_EndPointListener_t3937551933_0_0_0_KeyValuePair_2_t702722790_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_EndPointListener_t3937551933_0_0_0_KeyValuePair_2_t702722790_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t702722790_0_0_0_Types[] = { (&KeyValuePair_2_t702722790_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t702722790_0_0_0 = { 1, GenInst_KeyValuePair_2_t702722790_0_0_0_Types };
static const RuntimeType* GenInst_IPAddress_t1399971723_0_0_0_Dictionary_2_t2945377568_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&IPAddress_t1399971723_0_0_0), (&Dictionary_2_t2945377568_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_IPAddress_t1399971723_0_0_0_Dictionary_2_t2945377568_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_IPAddress_t1399971723_0_0_0_Dictionary_2_t2945377568_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_IPAddress_t1399971723_0_0_0_Dictionary_2_t2945377568_0_0_0_KeyValuePair_2_t767379738_0_0_0_Types[] = { (&IPAddress_t1399971723_0_0_0), (&Dictionary_2_t2945377568_0_0_0), (&KeyValuePair_2_t767379738_0_0_0) };
extern const Il2CppGenericInst GenInst_IPAddress_t1399971723_0_0_0_Dictionary_2_t2945377568_0_0_0_KeyValuePair_2_t767379738_0_0_0 = { 3, GenInst_IPAddress_t1399971723_0_0_0_Dictionary_2_t2945377568_0_0_0_KeyValuePair_2_t767379738_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t767379738_0_0_0_Types[] = { (&KeyValuePair_2_t767379738_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t767379738_0_0_0 = { 1, GenInst_KeyValuePair_2_t767379738_0_0_0_Types };
static const RuntimeType* GenInst_HttpListenerRequest_t2316381291_0_0_0_AuthenticationSchemes_t29593226_0_0_0_Types[] = { (&HttpListenerRequest_t2316381291_0_0_0), (&AuthenticationSchemes_t29593226_0_0_0) };
extern const Il2CppGenericInst GenInst_HttpListenerRequest_t2316381291_0_0_0_AuthenticationSchemes_t29593226_0_0_0 = { 2, GenInst_HttpListenerRequest_t2316381291_0_0_0_AuthenticationSchemes_t29593226_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_AuthenticationSchemes_t29593226_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&AuthenticationSchemes_t29593226_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_AuthenticationSchemes_t29593226_0_0_0 = { 2, GenInst_RuntimeObject_0_0_0_AuthenticationSchemes_t29593226_0_0_0_Types };
static const RuntimeType* GenInst_IIdentity_t2445095625_0_0_0_NetworkCredential_t3911206805_0_0_0_Types[] = { (&IIdentity_t2445095625_0_0_0), (&NetworkCredential_t3911206805_0_0_0) };
extern const Il2CppGenericInst GenInst_IIdentity_t2445095625_0_0_0_NetworkCredential_t3911206805_0_0_0 = { 2, GenInst_IIdentity_t2445095625_0_0_0_NetworkCredential_t3911206805_0_0_0_Types };
static const RuntimeType* GenInst_HttpListenerContext_t994708409_0_0_0_Types[] = { (&HttpListenerContext_t994708409_0_0_0) };
extern const Il2CppGenericInst GenInst_HttpListenerContext_t994708409_0_0_0 = { 1, GenInst_HttpListenerContext_t994708409_0_0_0_Types };
static const RuntimeType* GenInst_HttpListenerContext_t994708409_0_0_0_HttpListenerContext_t994708409_0_0_0_Types[] = { (&HttpListenerContext_t994708409_0_0_0), (&HttpListenerContext_t994708409_0_0_0) };
extern const Il2CppGenericInst GenInst_HttpListenerContext_t994708409_0_0_0_HttpListenerContext_t994708409_0_0_0 = { 2, GenInst_HttpListenerContext_t994708409_0_0_0_HttpListenerContext_t994708409_0_0_0_Types };
static const RuntimeType* GenInst_HttpListenerContext_t994708409_0_0_0_HttpListenerContext_t994708409_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&HttpListenerContext_t994708409_0_0_0), (&HttpListenerContext_t994708409_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_HttpListenerContext_t994708409_0_0_0_HttpListenerContext_t994708409_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_HttpListenerContext_t994708409_0_0_0_HttpListenerContext_t994708409_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_HttpListenerContext_t994708409_0_0_0_HttpListenerContext_t994708409_0_0_0_KeyValuePair_2_t138893069_0_0_0_Types[] = { (&HttpListenerContext_t994708409_0_0_0), (&HttpListenerContext_t994708409_0_0_0), (&KeyValuePair_2_t138893069_0_0_0) };
extern const Il2CppGenericInst GenInst_HttpListenerContext_t994708409_0_0_0_HttpListenerContext_t994708409_0_0_0_KeyValuePair_2_t138893069_0_0_0 = { 3, GenInst_HttpListenerContext_t994708409_0_0_0_HttpListenerContext_t994708409_0_0_0_KeyValuePair_2_t138893069_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t138893069_0_0_0_Types[] = { (&KeyValuePair_2_t138893069_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t138893069_0_0_0 = { 1, GenInst_KeyValuePair_2_t138893069_0_0_0_Types };
static const RuntimeType* GenInst_HttpListenerAsyncResult_t3506939685_0_0_0_Types[] = { (&HttpListenerAsyncResult_t3506939685_0_0_0) };
extern const Il2CppGenericInst GenInst_HttpListenerAsyncResult_t3506939685_0_0_0 = { 1, GenInst_HttpListenerAsyncResult_t3506939685_0_0_0_Types };
static const RuntimeType* GenInst_HttpListenerContext_t994708409_0_0_0_HttpListenerContext_t994708409_0_0_0_HttpListenerContext_t994708409_0_0_0_Types[] = { (&HttpListenerContext_t994708409_0_0_0), (&HttpListenerContext_t994708409_0_0_0), (&HttpListenerContext_t994708409_0_0_0) };
extern const Il2CppGenericInst GenInst_HttpListenerContext_t994708409_0_0_0_HttpListenerContext_t994708409_0_0_0_HttpListenerContext_t994708409_0_0_0 = { 3, GenInst_HttpListenerContext_t994708409_0_0_0_HttpListenerContext_t994708409_0_0_0_HttpListenerContext_t994708409_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Char_t3454481338_0_0_0_Types[] = { (&String_t_0_0_0), (&Char_t3454481338_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Char_t3454481338_0_0_0 = { 2, GenInst_String_t_0_0_0_Char_t3454481338_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Char_t3454481338_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Char_t3454481338_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Char_t3454481338_0_0_0 = { 2, GenInst_RuntimeObject_0_0_0_Char_t3454481338_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t803886688_0_0_0_Types[] = { (&KeyValuePair_2_t803886688_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t803886688_0_0_0 = { 1, GenInst_KeyValuePair_2_t803886688_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Char_t3454481338_0_0_0_RuntimeObject_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Char_t3454481338_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Char_t3454481338_0_0_0_RuntimeObject_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_Char_t3454481338_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Char_t3454481338_0_0_0_Char_t3454481338_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Char_t3454481338_0_0_0), (&Char_t3454481338_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Char_t3454481338_0_0_0_Char_t3454481338_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_Char_t3454481338_0_0_0_Char_t3454481338_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Char_t3454481338_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Char_t3454481338_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Char_t3454481338_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_Char_t3454481338_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Char_t3454481338_0_0_0_KeyValuePair_2_t803886688_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Char_t3454481338_0_0_0), (&KeyValuePair_2_t803886688_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Char_t3454481338_0_0_0_KeyValuePair_2_t803886688_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_Char_t3454481338_0_0_0_KeyValuePair_2_t803886688_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Char_t3454481338_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&String_t_0_0_0), (&Char_t3454481338_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Char_t3454481338_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_Char_t3454481338_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Char_t3454481338_0_0_0_KeyValuePair_2_t3126605822_0_0_0_Types[] = { (&String_t_0_0_0), (&Char_t3454481338_0_0_0), (&KeyValuePair_2_t3126605822_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Char_t3454481338_0_0_0_KeyValuePair_2_t3126605822_0_0_0 = { 3, GenInst_String_t_0_0_0_Char_t3454481338_0_0_0_KeyValuePair_2_t3126605822_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3126605822_0_0_0_Types[] = { (&KeyValuePair_2_t3126605822_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3126605822_0_0_0 = { 1, GenInst_KeyValuePair_2_t3126605822_0_0_0_Types };
static const RuntimeType* GenInst_ByteU5BU5D_t3397334013_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types[] = { (&ByteU5BU5D_t3397334013_0_0_0), (&Int32_t2071877448_0_0_0), (&Int32_t2071877448_0_0_0) };
extern const Il2CppGenericInst GenInst_ByteU5BU5D_t3397334013_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0 = { 3, GenInst_ByteU5BU5D_t3397334013_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_HttpHeaderInfo_t2096319561_0_0_0_Types[] = { (&String_t_0_0_0), (&HttpHeaderInfo_t2096319561_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_HttpHeaderInfo_t2096319561_0_0_0 = { 2, GenInst_String_t_0_0_0_HttpHeaderInfo_t2096319561_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_HttpHeaderInfo_t2096319561_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&String_t_0_0_0), (&HttpHeaderInfo_t2096319561_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_HttpHeaderInfo_t2096319561_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_HttpHeaderInfo_t2096319561_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_HttpHeaderInfo_t2096319561_0_0_0_KeyValuePair_2_t1768444045_0_0_0_Types[] = { (&String_t_0_0_0), (&HttpHeaderInfo_t2096319561_0_0_0), (&KeyValuePair_2_t1768444045_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_HttpHeaderInfo_t2096319561_0_0_0_KeyValuePair_2_t1768444045_0_0_0 = { 3, GenInst_String_t_0_0_0_HttpHeaderInfo_t2096319561_0_0_0_KeyValuePair_2_t1768444045_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1768444045_0_0_0_Types[] = { (&KeyValuePair_2_t1768444045_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1768444045_0_0_0 = { 1, GenInst_KeyValuePair_2_t1768444045_0_0_0_Types };
static const RuntimeType* GenInst_HttpHeaderInfo_t2096319561_0_0_0_Types[] = { (&HttpHeaderInfo_t2096319561_0_0_0) };
extern const Il2CppGenericInst GenInst_HttpHeaderInfo_t2096319561_0_0_0 = { 1, GenInst_HttpHeaderInfo_t2096319561_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_HttpHeaderInfo_t2096319561_0_0_0_HttpHeaderInfo_t2096319561_0_0_0_Types[] = { (&String_t_0_0_0), (&HttpHeaderInfo_t2096319561_0_0_0), (&HttpHeaderInfo_t2096319561_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_HttpHeaderInfo_t2096319561_0_0_0_HttpHeaderInfo_t2096319561_0_0_0 = { 3, GenInst_String_t_0_0_0_HttpHeaderInfo_t2096319561_0_0_0_HttpHeaderInfo_t2096319561_0_0_0_Types };
static const RuntimeType* GenInst_HttpRequestEventArgs_t918469868_0_0_0_Types[] = { (&HttpRequestEventArgs_t918469868_0_0_0) };
extern const Il2CppGenericInst GenInst_HttpRequestEventArgs_t918469868_0_0_0 = { 1, GenInst_HttpRequestEventArgs_t918469868_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_WebSocketServiceHost_t492106494_0_0_0_Types[] = { (&String_t_0_0_0), (&WebSocketServiceHost_t492106494_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_WebSocketServiceHost_t492106494_0_0_0 = { 2, GenInst_String_t_0_0_0_WebSocketServiceHost_t492106494_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_WebSocketServiceHost_t492106494_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&String_t_0_0_0), (&WebSocketServiceHost_t492106494_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_WebSocketServiceHost_t492106494_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_WebSocketServiceHost_t492106494_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_WebSocketServiceHost_t492106494_0_0_0_KeyValuePair_2_t164230978_0_0_0_Types[] = { (&String_t_0_0_0), (&WebSocketServiceHost_t492106494_0_0_0), (&KeyValuePair_2_t164230978_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_WebSocketServiceHost_t492106494_0_0_0_KeyValuePair_2_t164230978_0_0_0 = { 3, GenInst_String_t_0_0_0_WebSocketServiceHost_t492106494_0_0_0_KeyValuePair_2_t164230978_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t164230978_0_0_0_Types[] = { (&KeyValuePair_2_t164230978_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t164230978_0_0_0 = { 1, GenInst_KeyValuePair_2_t164230978_0_0_0_Types };
static const RuntimeType* GenInst_CookieCollection_t4248997468_0_0_0_CookieCollection_t4248997468_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { (&CookieCollection_t4248997468_0_0_0), (&CookieCollection_t4248997468_0_0_0), (&Boolean_t3825574718_0_0_0) };
extern const Il2CppGenericInst GenInst_CookieCollection_t4248997468_0_0_0_CookieCollection_t4248997468_0_0_0_Boolean_t3825574718_0_0_0 = { 3, GenInst_CookieCollection_t4248997468_0_0_0_CookieCollection_t4248997468_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0), (&Boolean_t3825574718_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Boolean_t3825574718_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const RuntimeType* GenInst_WebSocketContext_t3488732344_0_0_0_String_t_0_0_0_Types[] = { (&WebSocketContext_t3488732344_0_0_0), (&String_t_0_0_0) };
extern const Il2CppGenericInst GenInst_WebSocketContext_t3488732344_0_0_0_String_t_0_0_0 = { 2, GenInst_WebSocketContext_t3488732344_0_0_0_String_t_0_0_0_Types };
static const RuntimeType* GenInst_WebSocketServiceHost_t492106494_0_0_0_Types[] = { (&WebSocketServiceHost_t492106494_0_0_0) };
extern const Il2CppGenericInst GenInst_WebSocketServiceHost_t492106494_0_0_0 = { 1, GenInst_WebSocketServiceHost_t492106494_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_WebSocketServiceHost_t492106494_0_0_0_WebSocketServiceHost_t492106494_0_0_0_Types[] = { (&String_t_0_0_0), (&WebSocketServiceHost_t492106494_0_0_0), (&WebSocketServiceHost_t492106494_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_WebSocketServiceHost_t492106494_0_0_0_WebSocketServiceHost_t492106494_0_0_0 = { 3, GenInst_String_t_0_0_0_WebSocketServiceHost_t492106494_0_0_0_WebSocketServiceHost_t492106494_0_0_0_Types };
static const RuntimeType* GenInst_CompressionMethod_t4066553457_0_0_0_ByteU5BU5D_t3397334013_0_0_0_Types[] = { (&CompressionMethod_t4066553457_0_0_0), (&ByteU5BU5D_t3397334013_0_0_0) };
extern const Il2CppGenericInst GenInst_CompressionMethod_t4066553457_0_0_0_ByteU5BU5D_t3397334013_0_0_0 = { 2, GenInst_CompressionMethod_t4066553457_0_0_0_ByteU5BU5D_t3397334013_0_0_0_Types };
static const RuntimeType* GenInst_CompressionMethod_t4066553457_0_0_0_RuntimeObject_0_0_0_Types[] = { (&CompressionMethod_t4066553457_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_CompressionMethod_t4066553457_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_CompressionMethod_t4066553457_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2919672843_0_0_0_Types[] = { (&KeyValuePair_2_t2919672843_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2919672843_0_0_0 = { 1, GenInst_KeyValuePair_2_t2919672843_0_0_0_Types };
static const RuntimeType* GenInst_CompressionMethod_t4066553457_0_0_0_Types[] = { (&CompressionMethod_t4066553457_0_0_0) };
extern const Il2CppGenericInst GenInst_CompressionMethod_t4066553457_0_0_0 = { 1, GenInst_CompressionMethod_t4066553457_0_0_0_Types };
static const RuntimeType* GenInst_CompressionMethod_t4066553457_0_0_0_RuntimeObject_0_0_0_CompressionMethod_t4066553457_0_0_0_Types[] = { (&CompressionMethod_t4066553457_0_0_0), (&RuntimeObject_0_0_0), (&CompressionMethod_t4066553457_0_0_0) };
extern const Il2CppGenericInst GenInst_CompressionMethod_t4066553457_0_0_0_RuntimeObject_0_0_0_CompressionMethod_t4066553457_0_0_0 = { 3, GenInst_CompressionMethod_t4066553457_0_0_0_RuntimeObject_0_0_0_CompressionMethod_t4066553457_0_0_0_Types };
static const RuntimeType* GenInst_CompressionMethod_t4066553457_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types[] = { (&CompressionMethod_t4066553457_0_0_0), (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_CompressionMethod_t4066553457_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0 = { 3, GenInst_CompressionMethod_t4066553457_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_CompressionMethod_t4066553457_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&CompressionMethod_t4066553457_0_0_0), (&RuntimeObject_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_CompressionMethod_t4066553457_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_CompressionMethod_t4066553457_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_CompressionMethod_t4066553457_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t2919672843_0_0_0_Types[] = { (&CompressionMethod_t4066553457_0_0_0), (&RuntimeObject_0_0_0), (&KeyValuePair_2_t2919672843_0_0_0) };
extern const Il2CppGenericInst GenInst_CompressionMethod_t4066553457_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t2919672843_0_0_0 = { 3, GenInst_CompressionMethod_t4066553457_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t2919672843_0_0_0_Types };
static const RuntimeType* GenInst_CompressionMethod_t4066553457_0_0_0_ByteU5BU5D_t3397334013_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&CompressionMethod_t4066553457_0_0_0), (&ByteU5BU5D_t3397334013_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_CompressionMethod_t4066553457_0_0_0_ByteU5BU5D_t3397334013_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_CompressionMethod_t4066553457_0_0_0_ByteU5BU5D_t3397334013_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_CompressionMethod_t4066553457_0_0_0_ByteU5BU5D_t3397334013_0_0_0_KeyValuePair_2_t3627557561_0_0_0_Types[] = { (&CompressionMethod_t4066553457_0_0_0), (&ByteU5BU5D_t3397334013_0_0_0), (&KeyValuePair_2_t3627557561_0_0_0) };
extern const Il2CppGenericInst GenInst_CompressionMethod_t4066553457_0_0_0_ByteU5BU5D_t3397334013_0_0_0_KeyValuePair_2_t3627557561_0_0_0 = { 3, GenInst_CompressionMethod_t4066553457_0_0_0_ByteU5BU5D_t3397334013_0_0_0_KeyValuePair_2_t3627557561_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3627557561_0_0_0_Types[] = { (&KeyValuePair_2_t3627557561_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3627557561_0_0_0 = { 1, GenInst_KeyValuePair_2_t3627557561_0_0_0_Types };
static const RuntimeType* GenInst_CompressionMethod_t4066553457_0_0_0_Stream_t3255436806_0_0_0_Types[] = { (&CompressionMethod_t4066553457_0_0_0), (&Stream_t3255436806_0_0_0) };
extern const Il2CppGenericInst GenInst_CompressionMethod_t4066553457_0_0_0_Stream_t3255436806_0_0_0 = { 2, GenInst_CompressionMethod_t4066553457_0_0_0_Stream_t3255436806_0_0_0_Types };
static const RuntimeType* GenInst_CompressionMethod_t4066553457_0_0_0_Stream_t3255436806_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&CompressionMethod_t4066553457_0_0_0), (&Stream_t3255436806_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_CompressionMethod_t4066553457_0_0_0_Stream_t3255436806_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_CompressionMethod_t4066553457_0_0_0_Stream_t3255436806_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_CompressionMethod_t4066553457_0_0_0_Stream_t3255436806_0_0_0_KeyValuePair_2_t3485660354_0_0_0_Types[] = { (&CompressionMethod_t4066553457_0_0_0), (&Stream_t3255436806_0_0_0), (&KeyValuePair_2_t3485660354_0_0_0) };
extern const Il2CppGenericInst GenInst_CompressionMethod_t4066553457_0_0_0_Stream_t3255436806_0_0_0_KeyValuePair_2_t3485660354_0_0_0 = { 3, GenInst_CompressionMethod_t4066553457_0_0_0_Stream_t3255436806_0_0_0_KeyValuePair_2_t3485660354_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3485660354_0_0_0_Types[] = { (&KeyValuePair_2_t3485660354_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3485660354_0_0_0 = { 1, GenInst_KeyValuePair_2_t3485660354_0_0_0_Types };
static const RuntimeType* GenInst_Stream_t3255436806_0_0_0_Types[] = { (&Stream_t3255436806_0_0_0) };
extern const Il2CppGenericInst GenInst_Stream_t3255436806_0_0_0 = { 1, GenInst_Stream_t3255436806_0_0_0_Types };
static const RuntimeType* GenInst_CompressionMethod_t4066553457_0_0_0_Stream_t3255436806_0_0_0_Stream_t3255436806_0_0_0_Types[] = { (&CompressionMethod_t4066553457_0_0_0), (&Stream_t3255436806_0_0_0), (&Stream_t3255436806_0_0_0) };
extern const Il2CppGenericInst GenInst_CompressionMethod_t4066553457_0_0_0_Stream_t3255436806_0_0_0_Stream_t3255436806_0_0_0 = { 3, GenInst_CompressionMethod_t4066553457_0_0_0_Stream_t3255436806_0_0_0_Stream_t3255436806_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Dictionary_2_t1445386684_0_0_0_Types[] = { (&String_t_0_0_0), (&Dictionary_2_t1445386684_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Dictionary_2_t1445386684_0_0_0 = { 2, GenInst_String_t_0_0_0_Dictionary_2_t1445386684_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Dictionary_2_t1445386684_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&String_t_0_0_0), (&Dictionary_2_t1445386684_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Dictionary_2_t1445386684_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_Dictionary_2_t1445386684_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Dictionary_2_t1445386684_0_0_0_KeyValuePair_2_t1117511168_0_0_0_Types[] = { (&String_t_0_0_0), (&Dictionary_2_t1445386684_0_0_0), (&KeyValuePair_2_t1117511168_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Dictionary_2_t1445386684_0_0_0_KeyValuePair_2_t1117511168_0_0_0 = { 3, GenInst_String_t_0_0_0_Dictionary_2_t1445386684_0_0_0_KeyValuePair_2_t1117511168_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1117511168_0_0_0_Types[] = { (&KeyValuePair_2_t1117511168_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1117511168_0_0_0 = { 1, GenInst_KeyValuePair_2_t1117511168_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_IWebSocketSession_t1432056748_0_0_0_Types[] = { (&String_t_0_0_0), (&IWebSocketSession_t1432056748_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_IWebSocketSession_t1432056748_0_0_0 = { 2, GenInst_String_t_0_0_0_IWebSocketSession_t1432056748_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_IWebSocketSession_t1432056748_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&String_t_0_0_0), (&IWebSocketSession_t1432056748_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_IWebSocketSession_t1432056748_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_IWebSocketSession_t1432056748_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_IWebSocketSession_t1432056748_0_0_0_KeyValuePair_2_t1104181232_0_0_0_Types[] = { (&String_t_0_0_0), (&IWebSocketSession_t1432056748_0_0_0), (&KeyValuePair_2_t1104181232_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_IWebSocketSession_t1432056748_0_0_0_KeyValuePair_2_t1104181232_0_0_0 = { 3, GenInst_String_t_0_0_0_IWebSocketSession_t1432056748_0_0_0_KeyValuePair_2_t1104181232_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1104181232_0_0_0_Types[] = { (&KeyValuePair_2_t1104181232_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1104181232_0_0_0 = { 1, GenInst_KeyValuePair_2_t1104181232_0_0_0_Types };
static const RuntimeType* GenInst_IWebSocketSession_t1432056748_0_0_0_Types[] = { (&IWebSocketSession_t1432056748_0_0_0) };
extern const Il2CppGenericInst GenInst_IWebSocketSession_t1432056748_0_0_0 = { 1, GenInst_IWebSocketSession_t1432056748_0_0_0_Types };
static const RuntimeType* GenInst_CloseEventArgs_t344507773_0_0_0_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { (&CloseEventArgs_t344507773_0_0_0), (&Boolean_t3825574718_0_0_0), (&Boolean_t3825574718_0_0_0), (&Boolean_t3825574718_0_0_0) };
extern const Il2CppGenericInst GenInst_CloseEventArgs_t344507773_0_0_0_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0 = { 4, GenInst_CloseEventArgs_t344507773_0_0_0_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Boolean_t3825574718_0_0_0), (&Boolean_t3825574718_0_0_0), (&Boolean_t3825574718_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0 = { 4, GenInst_RuntimeObject_0_0_0_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const RuntimeType* GenInst_Opcode_t2313788840_0_0_0_Stream_t3255436806_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { (&Opcode_t2313788840_0_0_0), (&Stream_t3255436806_0_0_0), (&Boolean_t3825574718_0_0_0) };
extern const Il2CppGenericInst GenInst_Opcode_t2313788840_0_0_0_Stream_t3255436806_0_0_0_Boolean_t3825574718_0_0_0 = { 3, GenInst_Opcode_t2313788840_0_0_0_Stream_t3255436806_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const RuntimeType* GenInst_Opcode_t2313788840_0_0_0_RuntimeObject_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { (&Opcode_t2313788840_0_0_0), (&RuntimeObject_0_0_0), (&Boolean_t3825574718_0_0_0) };
extern const Il2CppGenericInst GenInst_Opcode_t2313788840_0_0_0_RuntimeObject_0_0_0_Boolean_t3825574718_0_0_0 = { 3, GenInst_Opcode_t2313788840_0_0_0_RuntimeObject_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const RuntimeType* GenInst_WebSocketFrame_t764750278_0_0_0_Types[] = { (&WebSocketFrame_t764750278_0_0_0) };
extern const Il2CppGenericInst GenInst_WebSocketFrame_t764750278_0_0_0 = { 1, GenInst_WebSocketFrame_t764750278_0_0_0_Types };
static const RuntimeType* GenInst_Action_4_t3853297115_0_0_0_Types[] = { (&Action_4_t3853297115_0_0_0) };
extern const Il2CppGenericInst GenInst_Action_4_t3853297115_0_0_0 = { 1, GenInst_Action_4_t3853297115_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_String_t_0_0_0_String_t_0_0_0_String_t_0_0_0_Types[] = { (&String_t_0_0_0), (&String_t_0_0_0), (&String_t_0_0_0), (&String_t_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0_String_t_0_0_0_String_t_0_0_0 = { 4, GenInst_String_t_0_0_0_String_t_0_0_0_String_t_0_0_0_String_t_0_0_0_Types };
static const RuntimeType* GenInst_SpeechbubblePrefab_t736279730_0_0_0_Types[] = { (&SpeechbubblePrefab_t736279730_0_0_0) };
extern const Il2CppGenericInst GenInst_SpeechbubblePrefab_t736279730_0_0_0 = { 1, GenInst_SpeechbubblePrefab_t736279730_0_0_0_Types };
static const RuntimeType* GenInst_DialogueLine_t768215248_0_0_0_Types[] = { (&DialogueLine_t768215248_0_0_0) };
extern const Il2CppGenericInst GenInst_DialogueLine_t768215248_0_0_0 = { 1, GenInst_DialogueLine_t768215248_0_0_0_Types };
static const RuntimeType* GenInst_SpeechbubbleType_t1311649088_0_0_0_GameObject_t1756533147_0_0_0_Types[] = { (&SpeechbubbleType_t1311649088_0_0_0), (&GameObject_t1756533147_0_0_0) };
extern const Il2CppGenericInst GenInst_SpeechbubbleType_t1311649088_0_0_0_GameObject_t1756533147_0_0_0 = { 2, GenInst_SpeechbubbleType_t1311649088_0_0_0_GameObject_t1756533147_0_0_0_Types };
static const RuntimeType* GenInst_SpeechbubbleType_t1311649088_0_0_0_RuntimeObject_0_0_0_Types[] = { (&SpeechbubbleType_t1311649088_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_SpeechbubbleType_t1311649088_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_SpeechbubbleType_t1311649088_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1971397920_0_0_0_Types[] = { (&KeyValuePair_2_t1971397920_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1971397920_0_0_0 = { 1, GenInst_KeyValuePair_2_t1971397920_0_0_0_Types };
static const RuntimeType* GenInst_SpeechbubbleType_t1311649088_0_0_0_Types[] = { (&SpeechbubbleType_t1311649088_0_0_0) };
extern const Il2CppGenericInst GenInst_SpeechbubbleType_t1311649088_0_0_0 = { 1, GenInst_SpeechbubbleType_t1311649088_0_0_0_Types };
static const RuntimeType* GenInst_SpeechbubbleType_t1311649088_0_0_0_RuntimeObject_0_0_0_SpeechbubbleType_t1311649088_0_0_0_Types[] = { (&SpeechbubbleType_t1311649088_0_0_0), (&RuntimeObject_0_0_0), (&SpeechbubbleType_t1311649088_0_0_0) };
extern const Il2CppGenericInst GenInst_SpeechbubbleType_t1311649088_0_0_0_RuntimeObject_0_0_0_SpeechbubbleType_t1311649088_0_0_0 = { 3, GenInst_SpeechbubbleType_t1311649088_0_0_0_RuntimeObject_0_0_0_SpeechbubbleType_t1311649088_0_0_0_Types };
static const RuntimeType* GenInst_SpeechbubbleType_t1311649088_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types[] = { (&SpeechbubbleType_t1311649088_0_0_0), (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_SpeechbubbleType_t1311649088_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0 = { 3, GenInst_SpeechbubbleType_t1311649088_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_SpeechbubbleType_t1311649088_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&SpeechbubbleType_t1311649088_0_0_0), (&RuntimeObject_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_SpeechbubbleType_t1311649088_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_SpeechbubbleType_t1311649088_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_SpeechbubbleType_t1311649088_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t1971397920_0_0_0_Types[] = { (&SpeechbubbleType_t1311649088_0_0_0), (&RuntimeObject_0_0_0), (&KeyValuePair_2_t1971397920_0_0_0) };
extern const Il2CppGenericInst GenInst_SpeechbubbleType_t1311649088_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t1971397920_0_0_0 = { 3, GenInst_SpeechbubbleType_t1311649088_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t1971397920_0_0_0_Types };
static const RuntimeType* GenInst_SpeechbubbleType_t1311649088_0_0_0_GameObject_t1756533147_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&SpeechbubbleType_t1311649088_0_0_0), (&GameObject_t1756533147_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_SpeechbubbleType_t1311649088_0_0_0_GameObject_t1756533147_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_SpeechbubbleType_t1311649088_0_0_0_GameObject_t1756533147_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_SpeechbubbleType_t1311649088_0_0_0_GameObject_t1756533147_0_0_0_KeyValuePair_2_t1038481772_0_0_0_Types[] = { (&SpeechbubbleType_t1311649088_0_0_0), (&GameObject_t1756533147_0_0_0), (&KeyValuePair_2_t1038481772_0_0_0) };
extern const Il2CppGenericInst GenInst_SpeechbubbleType_t1311649088_0_0_0_GameObject_t1756533147_0_0_0_KeyValuePair_2_t1038481772_0_0_0 = { 3, GenInst_SpeechbubbleType_t1311649088_0_0_0_GameObject_t1756533147_0_0_0_KeyValuePair_2_t1038481772_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1038481772_0_0_0_Types[] = { (&KeyValuePair_2_t1038481772_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1038481772_0_0_0 = { 1, GenInst_KeyValuePair_2_t1038481772_0_0_0_Types };
static const RuntimeType* GenInst_SpeechbubbleType_t1311649088_0_0_0_Queue_1_t2840177624_0_0_0_Types[] = { (&SpeechbubbleType_t1311649088_0_0_0), (&Queue_1_t2840177624_0_0_0) };
extern const Il2CppGenericInst GenInst_SpeechbubbleType_t1311649088_0_0_0_Queue_1_t2840177624_0_0_0 = { 2, GenInst_SpeechbubbleType_t1311649088_0_0_0_Queue_1_t2840177624_0_0_0_Types };
static const RuntimeType* GenInst_SpeechbubbleBehaviour_t3020520789_0_0_0_Types[] = { (&SpeechbubbleBehaviour_t3020520789_0_0_0) };
extern const Il2CppGenericInst GenInst_SpeechbubbleBehaviour_t3020520789_0_0_0 = { 1, GenInst_SpeechbubbleBehaviour_t3020520789_0_0_0_Types };
static const RuntimeType* GenInst_SpeechbubbleType_t1311649088_0_0_0_Queue_1_t2840177624_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&SpeechbubbleType_t1311649088_0_0_0), (&Queue_1_t2840177624_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_SpeechbubbleType_t1311649088_0_0_0_Queue_1_t2840177624_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_SpeechbubbleType_t1311649088_0_0_0_Queue_1_t2840177624_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_SpeechbubbleType_t1311649088_0_0_0_Queue_1_t2840177624_0_0_0_KeyValuePair_2_t2122126249_0_0_0_Types[] = { (&SpeechbubbleType_t1311649088_0_0_0), (&Queue_1_t2840177624_0_0_0), (&KeyValuePair_2_t2122126249_0_0_0) };
extern const Il2CppGenericInst GenInst_SpeechbubbleType_t1311649088_0_0_0_Queue_1_t2840177624_0_0_0_KeyValuePair_2_t2122126249_0_0_0 = { 3, GenInst_SpeechbubbleType_t1311649088_0_0_0_Queue_1_t2840177624_0_0_0_KeyValuePair_2_t2122126249_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2122126249_0_0_0_Types[] = { (&KeyValuePair_2_t2122126249_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2122126249_0_0_0 = { 1, GenInst_KeyValuePair_2_t2122126249_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t4048664256_gp_0_0_0_0_Types[] = { (&IEnumerable_1_t4048664256_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t4048664256_gp_0_0_0_0 = { 1, GenInst_IEnumerable_1_t4048664256_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m801276902_gp_0_0_0_0_Types[] = { (&Array_InternalArray__IEnumerable_GetEnumerator_m801276902_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m801276902_gp_0_0_0_0 = { 1, GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m801276902_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m2069139338_gp_0_0_0_0_Array_Sort_m2069139338_gp_0_0_0_0_Types[] = { (&Array_Sort_m2069139338_gp_0_0_0_0), (&Array_Sort_m2069139338_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m2069139338_gp_0_0_0_0_Array_Sort_m2069139338_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m2069139338_gp_0_0_0_0_Array_Sort_m2069139338_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m1999601238_gp_0_0_0_0_Array_Sort_m1999601238_gp_1_0_0_0_Types[] = { (&Array_Sort_m1999601238_gp_0_0_0_0), (&Array_Sort_m1999601238_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m1999601238_gp_0_0_0_0_Array_Sort_m1999601238_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m1999601238_gp_0_0_0_0_Array_Sort_m1999601238_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m4168899348_gp_0_0_0_0_Types[] = { (&Array_Sort_m4168899348_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m4168899348_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m4168899348_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m4168899348_gp_0_0_0_0_Array_Sort_m4168899348_gp_0_0_0_0_Types[] = { (&Array_Sort_m4168899348_gp_0_0_0_0), (&Array_Sort_m4168899348_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m4168899348_gp_0_0_0_0_Array_Sort_m4168899348_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m4168899348_gp_0_0_0_0_Array_Sort_m4168899348_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m1711256281_gp_0_0_0_0_Types[] = { (&Array_Sort_m1711256281_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m1711256281_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m1711256281_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m1711256281_gp_0_0_0_0_Array_Sort_m1711256281_gp_1_0_0_0_Types[] = { (&Array_Sort_m1711256281_gp_0_0_0_0), (&Array_Sort_m1711256281_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m1711256281_gp_0_0_0_0_Array_Sort_m1711256281_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m1711256281_gp_0_0_0_0_Array_Sort_m1711256281_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m714603628_gp_0_0_0_0_Array_Sort_m714603628_gp_0_0_0_0_Types[] = { (&Array_Sort_m714603628_gp_0_0_0_0), (&Array_Sort_m714603628_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m714603628_gp_0_0_0_0_Array_Sort_m714603628_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m714603628_gp_0_0_0_0_Array_Sort_m714603628_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m4057794348_gp_0_0_0_0_Array_Sort_m4057794348_gp_1_0_0_0_Types[] = { (&Array_Sort_m4057794348_gp_0_0_0_0), (&Array_Sort_m4057794348_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m4057794348_gp_0_0_0_0_Array_Sort_m4057794348_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m4057794348_gp_0_0_0_0_Array_Sort_m4057794348_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m2544329074_gp_0_0_0_0_Types[] = { (&Array_Sort_m2544329074_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m2544329074_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m2544329074_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m2544329074_gp_0_0_0_0_Array_Sort_m2544329074_gp_0_0_0_0_Types[] = { (&Array_Sort_m2544329074_gp_0_0_0_0), (&Array_Sort_m2544329074_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m2544329074_gp_0_0_0_0_Array_Sort_m2544329074_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m2544329074_gp_0_0_0_0_Array_Sort_m2544329074_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m54102389_gp_0_0_0_0_Types[] = { (&Array_Sort_m54102389_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m54102389_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m54102389_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m54102389_gp_1_0_0_0_Types[] = { (&Array_Sort_m54102389_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m54102389_gp_1_0_0_0 = { 1, GenInst_Array_Sort_m54102389_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m54102389_gp_0_0_0_0_Array_Sort_m54102389_gp_1_0_0_0_Types[] = { (&Array_Sort_m54102389_gp_0_0_0_0), (&Array_Sort_m54102389_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m54102389_gp_0_0_0_0_Array_Sort_m54102389_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m54102389_gp_0_0_0_0_Array_Sort_m54102389_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m2673640633_gp_0_0_0_0_Types[] = { (&Array_Sort_m2673640633_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m2673640633_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m2673640633_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m285045864_gp_0_0_0_0_Types[] = { (&Array_Sort_m285045864_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m285045864_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m285045864_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_qsort_m965712368_gp_0_0_0_0_Types[] = { (&Array_qsort_m965712368_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_qsort_m965712368_gp_0_0_0_0 = { 1, GenInst_Array_qsort_m965712368_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_qsort_m965712368_gp_0_0_0_0_Array_qsort_m965712368_gp_1_0_0_0_Types[] = { (&Array_qsort_m965712368_gp_0_0_0_0), (&Array_qsort_m965712368_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_qsort_m965712368_gp_0_0_0_0_Array_qsort_m965712368_gp_1_0_0_0 = { 2, GenInst_Array_qsort_m965712368_gp_0_0_0_0_Array_qsort_m965712368_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Array_compare_m1681301885_gp_0_0_0_0_Types[] = { (&Array_compare_m1681301885_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_compare_m1681301885_gp_0_0_0_0 = { 1, GenInst_Array_compare_m1681301885_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_qsort_m2216502740_gp_0_0_0_0_Types[] = { (&Array_qsort_m2216502740_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_qsort_m2216502740_gp_0_0_0_0 = { 1, GenInst_Array_qsort_m2216502740_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Resize_m2875826811_gp_0_0_0_0_Types[] = { (&Array_Resize_m2875826811_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Resize_m2875826811_gp_0_0_0_0 = { 1, GenInst_Array_Resize_m2875826811_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_TrueForAll_m1718874735_gp_0_0_0_0_Types[] = { (&Array_TrueForAll_m1718874735_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_TrueForAll_m1718874735_gp_0_0_0_0 = { 1, GenInst_Array_TrueForAll_m1718874735_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_ForEach_m1854649314_gp_0_0_0_0_Types[] = { (&Array_ForEach_m1854649314_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_ForEach_m1854649314_gp_0_0_0_0 = { 1, GenInst_Array_ForEach_m1854649314_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_ConvertAll_m4274024367_gp_0_0_0_0_Array_ConvertAll_m4274024367_gp_1_0_0_0_Types[] = { (&Array_ConvertAll_m4274024367_gp_0_0_0_0), (&Array_ConvertAll_m4274024367_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_ConvertAll_m4274024367_gp_0_0_0_0_Array_ConvertAll_m4274024367_gp_1_0_0_0 = { 2, GenInst_Array_ConvertAll_m4274024367_gp_0_0_0_0_Array_ConvertAll_m4274024367_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Array_FindLastIndex_m2314439434_gp_0_0_0_0_Types[] = { (&Array_FindLastIndex_m2314439434_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m2314439434_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m2314439434_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_FindLastIndex_m3151928313_gp_0_0_0_0_Types[] = { (&Array_FindLastIndex_m3151928313_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m3151928313_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m3151928313_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_FindLastIndex_m2323782676_gp_0_0_0_0_Types[] = { (&Array_FindLastIndex_m2323782676_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m2323782676_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m2323782676_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_FindIndex_m1557343438_gp_0_0_0_0_Types[] = { (&Array_FindIndex_m1557343438_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m1557343438_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m1557343438_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_FindIndex_m1631399507_gp_0_0_0_0_Types[] = { (&Array_FindIndex_m1631399507_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m1631399507_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m1631399507_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_FindIndex_m3415315332_gp_0_0_0_0_Types[] = { (&Array_FindIndex_m3415315332_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m3415315332_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m3415315332_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_BinarySearch_m2990936537_gp_0_0_0_0_Types[] = { (&Array_BinarySearch_m2990936537_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m2990936537_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m2990936537_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_BinarySearch_m2009305197_gp_0_0_0_0_Types[] = { (&Array_BinarySearch_m2009305197_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m2009305197_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m2009305197_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_BinarySearch_m2427841765_gp_0_0_0_0_Types[] = { (&Array_BinarySearch_m2427841765_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m2427841765_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m2427841765_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_BinarySearch_m3348121441_gp_0_0_0_0_Types[] = { (&Array_BinarySearch_m3348121441_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m3348121441_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m3348121441_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_IndexOf_m2010744443_gp_0_0_0_0_Types[] = { (&Array_IndexOf_m2010744443_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m2010744443_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m2010744443_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_IndexOf_m4126518092_gp_0_0_0_0_Types[] = { (&Array_IndexOf_m4126518092_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m4126518092_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m4126518092_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_IndexOf_m3127862871_gp_0_0_0_0_Types[] = { (&Array_IndexOf_m3127862871_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m3127862871_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m3127862871_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_LastIndexOf_m1523107937_gp_0_0_0_0_Types[] = { (&Array_LastIndexOf_m1523107937_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m1523107937_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m1523107937_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_LastIndexOf_m2200535596_gp_0_0_0_0_Types[] = { (&Array_LastIndexOf_m2200535596_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m2200535596_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m2200535596_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_LastIndexOf_m555775493_gp_0_0_0_0_Types[] = { (&Array_LastIndexOf_m555775493_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m555775493_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m555775493_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_FindAll_m2331462890_gp_0_0_0_0_Types[] = { (&Array_FindAll_m2331462890_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_FindAll_m2331462890_gp_0_0_0_0 = { 1, GenInst_Array_FindAll_m2331462890_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Exists_m976807351_gp_0_0_0_0_Types[] = { (&Array_Exists_m976807351_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Exists_m976807351_gp_0_0_0_0 = { 1, GenInst_Array_Exists_m976807351_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_AsReadOnly_m884706992_gp_0_0_0_0_Types[] = { (&Array_AsReadOnly_m884706992_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_AsReadOnly_m884706992_gp_0_0_0_0 = { 1, GenInst_Array_AsReadOnly_m884706992_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Find_m1125132281_gp_0_0_0_0_Types[] = { (&Array_Find_m1125132281_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Find_m1125132281_gp_0_0_0_0 = { 1, GenInst_Array_Find_m1125132281_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_FindLast_m4092932075_gp_0_0_0_0_Types[] = { (&Array_FindLast_m4092932075_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_FindLast_m4092932075_gp_0_0_0_0 = { 1, GenInst_Array_FindLast_m4092932075_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_InternalEnumerator_1_t3582267753_gp_0_0_0_0_Types[] = { (&InternalEnumerator_1_t3582267753_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_InternalEnumerator_1_t3582267753_gp_0_0_0_0 = { 1, GenInst_InternalEnumerator_1_t3582267753_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_ArrayReadOnlyList_1_t3367196019_gp_0_0_0_0_Types[] = { (&ArrayReadOnlyList_1_t3367196019_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ArrayReadOnlyList_1_t3367196019_gp_0_0_0_0 = { 1, GenInst_ArrayReadOnlyList_1_t3367196019_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_U3CGetEnumeratorU3Ec__Iterator0_t1667190089_gp_0_0_0_0_Types[] = { (&U3CGetEnumeratorU3Ec__Iterator0_t1667190089_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_U3CGetEnumeratorU3Ec__Iterator0_t1667190089_gp_0_0_0_0 = { 1, GenInst_U3CGetEnumeratorU3Ec__Iterator0_t1667190089_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t3737699284_gp_0_0_0_0_Types[] = { (&IList_1_t3737699284_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t3737699284_gp_0_0_0_0 = { 1, GenInst_IList_1_t3737699284_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t1552160836_gp_0_0_0_0_Types[] = { (&ICollection_1_t1552160836_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t1552160836_gp_0_0_0_0 = { 1, GenInst_ICollection_1_t1552160836_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Nullable_1_t1398937014_gp_0_0_0_0_Types[] = { (&Nullable_1_t1398937014_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Nullable_1_t1398937014_gp_0_0_0_0 = { 1, GenInst_Nullable_1_t1398937014_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Comparer_1_t1036860714_gp_0_0_0_0_Types[] = { (&Comparer_1_t1036860714_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Comparer_1_t1036860714_gp_0_0_0_0 = { 1, GenInst_Comparer_1_t1036860714_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_DefaultComparer_t3074655092_gp_0_0_0_0_Types[] = { (&DefaultComparer_t3074655092_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_DefaultComparer_t3074655092_gp_0_0_0_0 = { 1, GenInst_DefaultComparer_t3074655092_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_GenericComparer_1_t1787398723_gp_0_0_0_0_Types[] = { (&GenericComparer_1_t1787398723_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_GenericComparer_1_t1787398723_gp_0_0_0_0 = { 1, GenInst_GenericComparer_1_t1787398723_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Types[] = { (&Dictionary_2_t2276497324_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2276497324_gp_0_0_0_0 = { 1, GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Types[] = { (&Dictionary_2_t2276497324_gp_0_0_0_0), (&Dictionary_2_t2276497324_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0 = { 2, GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3180694294_0_0_0_Types[] = { (&KeyValuePair_2_t3180694294_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3180694294_0_0_0 = { 1, GenInst_KeyValuePair_2_t3180694294_0_0_0_Types };
static const RuntimeType* GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m3006662979_gp_0_0_0_0_Types[] = { (&Dictionary_2_t2276497324_gp_0_0_0_0), (&Dictionary_2_t2276497324_gp_1_0_0_0), (&Dictionary_2_Do_CopyTo_m3006662979_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m3006662979_gp_0_0_0_0 = { 3, GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m3006662979_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m1628523394_gp_0_0_0_0_Types[] = { (&Dictionary_2_t2276497324_gp_0_0_0_0), (&Dictionary_2_t2276497324_gp_1_0_0_0), (&Dictionary_2_Do_ICollectionCopyTo_m1628523394_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m1628523394_gp_0_0_0_0 = { 3, GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m1628523394_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Dictionary_2_Do_ICollectionCopyTo_m1628523394_gp_0_0_0_0_RuntimeObject_0_0_0_Types[] = { (&Dictionary_2_Do_ICollectionCopyTo_m1628523394_gp_0_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_Dictionary_2_Do_ICollectionCopyTo_m1628523394_gp_0_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_Dictionary_2_Do_ICollectionCopyTo_m1628523394_gp_0_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Dictionary_2_t2276497324_gp_0_0_0_0), (&Dictionary_2_t2276497324_gp_1_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_ShimEnumerator_t3895203923_gp_0_0_0_0_ShimEnumerator_t3895203923_gp_1_0_0_0_Types[] = { (&ShimEnumerator_t3895203923_gp_0_0_0_0), (&ShimEnumerator_t3895203923_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_ShimEnumerator_t3895203923_gp_0_0_0_0_ShimEnumerator_t3895203923_gp_1_0_0_0 = { 2, GenInst_ShimEnumerator_t3895203923_gp_0_0_0_0_ShimEnumerator_t3895203923_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Enumerator_t2089681430_gp_0_0_0_0_Enumerator_t2089681430_gp_1_0_0_0_Types[] = { (&Enumerator_t2089681430_gp_0_0_0_0), (&Enumerator_t2089681430_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerator_t2089681430_gp_0_0_0_0_Enumerator_t2089681430_gp_1_0_0_0 = { 2, GenInst_Enumerator_t2089681430_gp_0_0_0_0_Enumerator_t2089681430_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3434615342_0_0_0_Types[] = { (&KeyValuePair_2_t3434615342_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3434615342_0_0_0 = { 1, GenInst_KeyValuePair_2_t3434615342_0_0_0_Types };
static const RuntimeType* GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0_Types[] = { (&KeyCollection_t1229212677_gp_0_0_0_0), (&KeyCollection_t1229212677_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0 = { 2, GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_KeyCollection_t1229212677_gp_0_0_0_0_Types[] = { (&KeyCollection_t1229212677_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyCollection_t1229212677_gp_0_0_0_0 = { 1, GenInst_KeyCollection_t1229212677_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerator_t83320710_gp_0_0_0_0_Enumerator_t83320710_gp_1_0_0_0_Types[] = { (&Enumerator_t83320710_gp_0_0_0_0), (&Enumerator_t83320710_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerator_t83320710_gp_0_0_0_0_Enumerator_t83320710_gp_1_0_0_0 = { 2, GenInst_Enumerator_t83320710_gp_0_0_0_0_Enumerator_t83320710_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Enumerator_t83320710_gp_0_0_0_0_Types[] = { (&Enumerator_t83320710_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerator_t83320710_gp_0_0_0_0 = { 1, GenInst_Enumerator_t83320710_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0_Types[] = { (&KeyCollection_t1229212677_gp_0_0_0_0), (&KeyCollection_t1229212677_gp_1_0_0_0), (&KeyCollection_t1229212677_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0 = { 3, GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0_Types[] = { (&KeyCollection_t1229212677_gp_0_0_0_0), (&KeyCollection_t1229212677_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0 = { 2, GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_Types[] = { (&ValueCollection_t2262344653_gp_0_0_0_0), (&ValueCollection_t2262344653_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0 = { 2, GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_ValueCollection_t2262344653_gp_1_0_0_0_Types[] = { (&ValueCollection_t2262344653_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_ValueCollection_t2262344653_gp_1_0_0_0 = { 1, GenInst_ValueCollection_t2262344653_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Enumerator_t3111723616_gp_0_0_0_0_Enumerator_t3111723616_gp_1_0_0_0_Types[] = { (&Enumerator_t3111723616_gp_0_0_0_0), (&Enumerator_t3111723616_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerator_t3111723616_gp_0_0_0_0_Enumerator_t3111723616_gp_1_0_0_0 = { 2, GenInst_Enumerator_t3111723616_gp_0_0_0_0_Enumerator_t3111723616_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Enumerator_t3111723616_gp_1_0_0_0_Types[] = { (&Enumerator_t3111723616_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerator_t3111723616_gp_1_0_0_0 = { 1, GenInst_Enumerator_t3111723616_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_Types[] = { (&ValueCollection_t2262344653_gp_0_0_0_0), (&ValueCollection_t2262344653_gp_1_0_0_0), (&ValueCollection_t2262344653_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0 = { 3, GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_Types[] = { (&ValueCollection_t2262344653_gp_1_0_0_0), (&ValueCollection_t2262344653_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0 = { 2, GenInst_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_DictionaryEntry_t3048875398_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&DictionaryEntry_t3048875398_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t3048875398_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 2, GenInst_DictionaryEntry_t3048875398_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_KeyValuePair_2_t3180694294_0_0_0_Types[] = { (&Dictionary_2_t2276497324_gp_0_0_0_0), (&Dictionary_2_t2276497324_gp_1_0_0_0), (&KeyValuePair_2_t3180694294_0_0_0) };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_KeyValuePair_2_t3180694294_0_0_0 = { 3, GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_KeyValuePair_2_t3180694294_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3180694294_0_0_0_KeyValuePair_2_t3180694294_0_0_0_Types[] = { (&KeyValuePair_2_t3180694294_0_0_0), (&KeyValuePair_2_t3180694294_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3180694294_0_0_0_KeyValuePair_2_t3180694294_0_0_0 = { 2, GenInst_KeyValuePair_2_t3180694294_0_0_0_KeyValuePair_2_t3180694294_0_0_0_Types };
static const RuntimeType* GenInst_Dictionary_2_t2276497324_gp_1_0_0_0_Types[] = { (&Dictionary_2_t2276497324_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2276497324_gp_1_0_0_0 = { 1, GenInst_Dictionary_2_t2276497324_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_EqualityComparer_1_t2066709010_gp_0_0_0_0_Types[] = { (&EqualityComparer_1_t2066709010_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_EqualityComparer_1_t2066709010_gp_0_0_0_0 = { 1, GenInst_EqualityComparer_1_t2066709010_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_DefaultComparer_t1766400012_gp_0_0_0_0_Types[] = { (&DefaultComparer_t1766400012_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_DefaultComparer_t1766400012_gp_0_0_0_0 = { 1, GenInst_DefaultComparer_t1766400012_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_GenericEqualityComparer_1_t2202941003_gp_0_0_0_0_Types[] = { (&GenericEqualityComparer_1_t2202941003_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_GenericEqualityComparer_1_t2202941003_gp_0_0_0_0 = { 1, GenInst_GenericEqualityComparer_1_t2202941003_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t4174120762_0_0_0_Types[] = { (&KeyValuePair_2_t4174120762_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4174120762_0_0_0 = { 1, GenInst_KeyValuePair_2_t4174120762_0_0_0_Types };
static const RuntimeType* GenInst_IDictionary_2_t3502329323_gp_0_0_0_0_IDictionary_2_t3502329323_gp_1_0_0_0_Types[] = { (&IDictionary_2_t3502329323_gp_0_0_0_0), (&IDictionary_2_t3502329323_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_IDictionary_2_t3502329323_gp_0_0_0_0_IDictionary_2_t3502329323_gp_1_0_0_0 = { 2, GenInst_IDictionary_2_t3502329323_gp_0_0_0_0_IDictionary_2_t3502329323_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1988958766_gp_0_0_0_0_KeyValuePair_2_t1988958766_gp_1_0_0_0_Types[] = { (&KeyValuePair_2_t1988958766_gp_0_0_0_0), (&KeyValuePair_2_t1988958766_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1988958766_gp_0_0_0_0_KeyValuePair_2_t1988958766_gp_1_0_0_0 = { 2, GenInst_KeyValuePair_2_t1988958766_gp_0_0_0_0_KeyValuePair_2_t1988958766_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t1169184319_gp_0_0_0_0_Types[] = { (&List_1_t1169184319_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t1169184319_gp_0_0_0_0 = { 1, GenInst_List_1_t1169184319_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerator_t1292967705_gp_0_0_0_0_Types[] = { (&Enumerator_t1292967705_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerator_t1292967705_gp_0_0_0_0 = { 1, GenInst_Enumerator_t1292967705_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Collection_1_t686054069_gp_0_0_0_0_Types[] = { (&Collection_1_t686054069_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Collection_1_t686054069_gp_0_0_0_0 = { 1, GenInst_Collection_1_t686054069_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_ReadOnlyCollection_1_t3540981679_gp_0_0_0_0_Types[] = { (&ReadOnlyCollection_1_t3540981679_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ReadOnlyCollection_1_t3540981679_gp_0_0_0_0 = { 1, GenInst_ReadOnlyCollection_1_t3540981679_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_MonoProperty_GetterAdapterFrame_m609993266_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m609993266_gp_1_0_0_0_Types[] = { (&MonoProperty_GetterAdapterFrame_m609993266_gp_0_0_0_0), (&MonoProperty_GetterAdapterFrame_m609993266_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_MonoProperty_GetterAdapterFrame_m609993266_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m609993266_gp_1_0_0_0 = { 2, GenInst_MonoProperty_GetterAdapterFrame_m609993266_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m609993266_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_MonoProperty_StaticGetterAdapterFrame_m781526467_gp_0_0_0_0_Types[] = { (&MonoProperty_StaticGetterAdapterFrame_m781526467_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_MonoProperty_StaticGetterAdapterFrame_m781526467_gp_0_0_0_0 = { 1, GenInst_MonoProperty_StaticGetterAdapterFrame_m781526467_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_ArraySegment_1_t1001032761_gp_0_0_0_0_Types[] = { (&ArraySegment_1_t1001032761_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ArraySegment_1_t1001032761_gp_0_0_0_0 = { 1, GenInst_ArraySegment_1_t1001032761_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Queue_1_t1458930734_gp_0_0_0_0_Types[] = { (&Queue_1_t1458930734_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Queue_1_t1458930734_gp_0_0_0_0 = { 1, GenInst_Queue_1_t1458930734_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerator_t4000919638_gp_0_0_0_0_Types[] = { (&Enumerator_t4000919638_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerator_t4000919638_gp_0_0_0_0 = { 1, GenInst_Enumerator_t4000919638_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Stack_1_t4016656541_gp_0_0_0_0_Types[] = { (&Stack_1_t4016656541_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Stack_1_t4016656541_gp_0_0_0_0 = { 1, GenInst_Stack_1_t4016656541_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerator_t546412149_gp_0_0_0_0_Types[] = { (&Enumerator_t546412149_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerator_t546412149_gp_0_0_0_0 = { 1, GenInst_Enumerator_t546412149_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_HashSet_1_t2624254809_gp_0_0_0_0_Types[] = { (&HashSet_1_t2624254809_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_HashSet_1_t2624254809_gp_0_0_0_0 = { 1, GenInst_HashSet_1_t2624254809_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerator_t2109956843_gp_0_0_0_0_Types[] = { (&Enumerator_t2109956843_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerator_t2109956843_gp_0_0_0_0 = { 1, GenInst_Enumerator_t2109956843_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_PrimeHelper_t3424417428_gp_0_0_0_0_Types[] = { (&PrimeHelper_t3424417428_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_PrimeHelper_t3424417428_gp_0_0_0_0 = { 1, GenInst_PrimeHelper_t3424417428_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_Any_m1395534165_gp_0_0_0_0_Types[] = { (&Enumerable_Any_m1395534165_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_Any_m1395534165_gp_0_0_0_0 = { 1, GenInst_Enumerable_Any_m1395534165_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_Any_m4174937590_gp_0_0_0_0_Types[] = { (&Enumerable_Any_m4174937590_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_Any_m4174937590_gp_0_0_0_0 = { 1, GenInst_Enumerable_Any_m4174937590_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_Any_m4174937590_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { (&Enumerable_Any_m4174937590_gp_0_0_0_0), (&Boolean_t3825574718_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_Any_m4174937590_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Enumerable_Any_m4174937590_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_First_m2440166523_gp_0_0_0_0_Types[] = { (&Enumerable_First_m2440166523_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_First_m2440166523_gp_0_0_0_0 = { 1, GenInst_Enumerable_First_m2440166523_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_FirstOrDefault_m1564979201_gp_0_0_0_0_Types[] = { (&Enumerable_FirstOrDefault_m1564979201_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_FirstOrDefault_m1564979201_gp_0_0_0_0 = { 1, GenInst_Enumerable_FirstOrDefault_m1564979201_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_Select_m791198632_gp_0_0_0_0_Types[] = { (&Enumerable_Select_m791198632_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_Select_m791198632_gp_0_0_0_0 = { 1, GenInst_Enumerable_Select_m791198632_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_Select_m791198632_gp_0_0_0_0_Enumerable_Select_m791198632_gp_1_0_0_0_Types[] = { (&Enumerable_Select_m791198632_gp_0_0_0_0), (&Enumerable_Select_m791198632_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_Select_m791198632_gp_0_0_0_0_Enumerable_Select_m791198632_gp_1_0_0_0 = { 2, GenInst_Enumerable_Select_m791198632_gp_0_0_0_0_Enumerable_Select_m791198632_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_Select_m791198632_gp_1_0_0_0_Types[] = { (&Enumerable_Select_m791198632_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_Select_m791198632_gp_1_0_0_0 = { 1, GenInst_Enumerable_Select_m791198632_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_CreateSelectIterator_m3979771306_gp_0_0_0_0_Types[] = { (&Enumerable_CreateSelectIterator_m3979771306_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSelectIterator_m3979771306_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateSelectIterator_m3979771306_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_CreateSelectIterator_m3979771306_gp_0_0_0_0_Enumerable_CreateSelectIterator_m3979771306_gp_1_0_0_0_Types[] = { (&Enumerable_CreateSelectIterator_m3979771306_gp_0_0_0_0), (&Enumerable_CreateSelectIterator_m3979771306_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSelectIterator_m3979771306_gp_0_0_0_0_Enumerable_CreateSelectIterator_m3979771306_gp_1_0_0_0 = { 2, GenInst_Enumerable_CreateSelectIterator_m3979771306_gp_0_0_0_0_Enumerable_CreateSelectIterator_m3979771306_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_CreateSelectIterator_m3979771306_gp_1_0_0_0_Types[] = { (&Enumerable_CreateSelectIterator_m3979771306_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSelectIterator_m3979771306_gp_1_0_0_0 = { 1, GenInst_Enumerable_CreateSelectIterator_m3979771306_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_Skip_m3785421448_gp_0_0_0_0_Types[] = { (&Enumerable_Skip_m3785421448_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_Skip_m3785421448_gp_0_0_0_0 = { 1, GenInst_Enumerable_Skip_m3785421448_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_CreateSkipIterator_m365619102_gp_0_0_0_0_Types[] = { (&Enumerable_CreateSkipIterator_m365619102_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSkipIterator_m365619102_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateSkipIterator_m365619102_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_ToArray_m747609077_gp_0_0_0_0_Types[] = { (&Enumerable_ToArray_m747609077_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_ToArray_m747609077_gp_0_0_0_0 = { 1, GenInst_Enumerable_ToArray_m747609077_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_ToList_m2644368746_gp_0_0_0_0_Types[] = { (&Enumerable_ToList_m2644368746_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_ToList_m2644368746_gp_0_0_0_0 = { 1, GenInst_Enumerable_ToList_m2644368746_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_Where_m1602106588_gp_0_0_0_0_Types[] = { (&Enumerable_Where_m1602106588_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_Where_m1602106588_gp_0_0_0_0 = { 1, GenInst_Enumerable_Where_m1602106588_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_Where_m1602106588_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { (&Enumerable_Where_m1602106588_gp_0_0_0_0), (&Boolean_t3825574718_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_Where_m1602106588_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Enumerable_Where_m1602106588_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_CreateWhereIterator_m3985242862_gp_0_0_0_0_Types[] = { (&Enumerable_CreateWhereIterator_m3985242862_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_CreateWhereIterator_m3985242862_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateWhereIterator_m3985242862_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_CreateWhereIterator_m3985242862_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { (&Enumerable_CreateWhereIterator_m3985242862_gp_0_0_0_0), (&Boolean_t3825574718_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_CreateWhereIterator_m3985242862_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Enumerable_CreateWhereIterator_m3985242862_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const RuntimeType* GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0_Types[] = { (&U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0 = { 1, GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0_Types[] = { (&U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0 = { 1, GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0_Types[] = { (&U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0), (&U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0 = { 2, GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_U3CCreateSkipIteratorU3Ec__Iterator16_1_t284123852_gp_0_0_0_0_Types[] = { (&U3CCreateSkipIteratorU3Ec__Iterator16_1_t284123852_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_U3CCreateSkipIteratorU3Ec__Iterator16_1_t284123852_gp_0_0_0_0 = { 1, GenInst_U3CCreateSkipIteratorU3Ec__Iterator16_1_t284123852_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0_Types[] = { (&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0 = { 1, GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { (&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0), (&Boolean_t3825574718_0_0_0) };
extern const Il2CppGenericInst GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const RuntimeType* GenInst_Component_GetComponentInChildren_m1236184570_gp_0_0_0_0_Types[] = { (&Component_GetComponentInChildren_m1236184570_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Component_GetComponentInChildren_m1236184570_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentInChildren_m1236184570_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Component_GetComponentsInChildren_m4167514082_gp_0_0_0_0_Types[] = { (&Component_GetComponentsInChildren_m4167514082_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m4167514082_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m4167514082_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Component_GetComponentsInChildren_m4085536263_gp_0_0_0_0_Types[] = { (&Component_GetComponentsInChildren_m4085536263_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m4085536263_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m4085536263_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Component_GetComponentsInChildren_m84946435_gp_0_0_0_0_Types[] = { (&Component_GetComponentsInChildren_m84946435_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m84946435_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m84946435_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Component_GetComponentsInChildren_m989421820_gp_0_0_0_0_Types[] = { (&Component_GetComponentsInChildren_m989421820_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m989421820_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m989421820_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Component_GetComponentsInParent_m893247195_gp_0_0_0_0_Types[] = { (&Component_GetComponentsInParent_m893247195_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInParent_m893247195_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInParent_m893247195_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Component_GetComponentsInParent_m2646750694_gp_0_0_0_0_Types[] = { (&Component_GetComponentsInParent_m2646750694_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInParent_m2646750694_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInParent_m2646750694_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Component_GetComponentsInParent_m3278624542_gp_0_0_0_0_Types[] = { (&Component_GetComponentsInParent_m3278624542_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInParent_m3278624542_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInParent_m3278624542_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Component_GetComponents_m1252126604_gp_0_0_0_0_Types[] = { (&Component_GetComponents_m1252126604_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Component_GetComponents_m1252126604_gp_0_0_0_0 = { 1, GenInst_Component_GetComponents_m1252126604_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Component_GetComponents_m1618642439_gp_0_0_0_0_Types[] = { (&Component_GetComponents_m1618642439_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Component_GetComponents_m1618642439_gp_0_0_0_0 = { 1, GenInst_Component_GetComponents_m1618642439_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_GameObject_GetComponentInChildren_m477715742_gp_0_0_0_0_Types[] = { (&GameObject_GetComponentInChildren_m477715742_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentInChildren_m477715742_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentInChildren_m477715742_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_GameObject_GetComponents_m3045349666_gp_0_0_0_0_Types[] = { (&GameObject_GetComponents_m3045349666_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_GameObject_GetComponents_m3045349666_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponents_m3045349666_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_GameObject_GetComponentsInChildren_m1567454139_gp_0_0_0_0_Types[] = { (&GameObject_GetComponentsInChildren_m1567454139_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInChildren_m1567454139_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInChildren_m1567454139_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_GameObject_GetComponentsInChildren_m1151940851_gp_0_0_0_0_Types[] = { (&GameObject_GetComponentsInChildren_m1151940851_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInChildren_m1151940851_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInChildren_m1151940851_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_GameObject_GetComponentsInParent_m4028060632_gp_0_0_0_0_Types[] = { (&GameObject_GetComponentsInParent_m4028060632_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInParent_m4028060632_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInParent_m4028060632_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Mesh_GetAllocArrayFromChannel_m4154891606_gp_0_0_0_0_Types[] = { (&Mesh_GetAllocArrayFromChannel_m4154891606_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Mesh_GetAllocArrayFromChannel_m4154891606_gp_0_0_0_0 = { 1, GenInst_Mesh_GetAllocArrayFromChannel_m4154891606_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Mesh_SafeLength_m1789465785_gp_0_0_0_0_Types[] = { (&Mesh_SafeLength_m1789465785_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Mesh_SafeLength_m1789465785_gp_0_0_0_0 = { 1, GenInst_Mesh_SafeLength_m1789465785_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Mesh_SetListForChannel_m2929091838_gp_0_0_0_0_Types[] = { (&Mesh_SetListForChannel_m2929091838_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Mesh_SetListForChannel_m2929091838_gp_0_0_0_0 = { 1, GenInst_Mesh_SetListForChannel_m2929091838_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Mesh_SetListForChannel_m3597787760_gp_0_0_0_0_Types[] = { (&Mesh_SetListForChannel_m3597787760_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Mesh_SetListForChannel_m3597787760_gp_0_0_0_0 = { 1, GenInst_Mesh_SetListForChannel_m3597787760_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Mesh_SetUvsImpl_m2321318716_gp_0_0_0_0_Types[] = { (&Mesh_SetUvsImpl_m2321318716_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Mesh_SetUvsImpl_m2321318716_gp_0_0_0_0 = { 1, GenInst_Mesh_SetUvsImpl_m2321318716_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Object_Instantiate_m2766660278_gp_0_0_0_0_Types[] = { (&Object_Instantiate_m2766660278_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Object_Instantiate_m2766660278_gp_0_0_0_0 = { 1, GenInst_Object_Instantiate_m2766660278_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Object_FindObjectsOfType_m488103269_gp_0_0_0_0_Types[] = { (&Object_FindObjectsOfType_m488103269_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Object_FindObjectsOfType_m488103269_gp_0_0_0_0 = { 1, GenInst_Object_FindObjectsOfType_m488103269_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Playable_IsPlayableOfType_m140883719_gp_0_0_0_0_Types[] = { (&Playable_IsPlayableOfType_m140883719_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Playable_IsPlayableOfType_m140883719_gp_0_0_0_0 = { 1, GenInst_Playable_IsPlayableOfType_m140883719_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_PlayableOutput_IsPlayableOutputOfType_m2834483815_gp_0_0_0_0_Types[] = { (&PlayableOutput_IsPlayableOutputOfType_m2834483815_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_PlayableOutput_IsPlayableOutputOfType_m2834483815_gp_0_0_0_0 = { 1, GenInst_PlayableOutput_IsPlayableOutputOfType_m2834483815_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_1_t476640868_gp_0_0_0_0_Types[] = { (&InvokableCall_1_t476640868_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_1_t476640868_gp_0_0_0_0 = { 1, GenInst_InvokableCall_1_t476640868_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_UnityAction_1_t2490859068_0_0_0_Types[] = { (&UnityAction_1_t2490859068_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityAction_1_t2490859068_0_0_0 = { 1, GenInst_UnityAction_1_t2490859068_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0_InvokableCall_2_t2042724809_gp_1_0_0_0_Types[] = { (&InvokableCall_2_t2042724809_gp_0_0_0_0), (&InvokableCall_2_t2042724809_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0_InvokableCall_2_t2042724809_gp_1_0_0_0 = { 2, GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0_InvokableCall_2_t2042724809_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_UnityAction_2_t601835599_0_0_0_Types[] = { (&UnityAction_2_t601835599_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityAction_2_t601835599_0_0_0 = { 1, GenInst_UnityAction_2_t601835599_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0_Types[] = { (&InvokableCall_2_t2042724809_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0 = { 1, GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_2_t2042724809_gp_1_0_0_0_Types[] = { (&InvokableCall_2_t2042724809_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t2042724809_gp_1_0_0_0 = { 1, GenInst_InvokableCall_2_t2042724809_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0_InvokableCall_3_t3608808750_gp_1_0_0_0_InvokableCall_3_t3608808750_gp_2_0_0_0_Types[] = { (&InvokableCall_3_t3608808750_gp_0_0_0_0), (&InvokableCall_3_t3608808750_gp_1_0_0_0), (&InvokableCall_3_t3608808750_gp_2_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0_InvokableCall_3_t3608808750_gp_1_0_0_0_InvokableCall_3_t3608808750_gp_2_0_0_0 = { 3, GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0_InvokableCall_3_t3608808750_gp_1_0_0_0_InvokableCall_3_t3608808750_gp_2_0_0_0_Types };
static const RuntimeType* GenInst_UnityAction_3_t155920421_0_0_0_Types[] = { (&UnityAction_3_t155920421_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityAction_3_t155920421_0_0_0 = { 1, GenInst_UnityAction_3_t155920421_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0_Types[] = { (&InvokableCall_3_t3608808750_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0 = { 1, GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_3_t3608808750_gp_1_0_0_0_Types[] = { (&InvokableCall_3_t3608808750_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3608808750_gp_1_0_0_0 = { 1, GenInst_InvokableCall_3_t3608808750_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_3_t3608808750_gp_2_0_0_0_Types[] = { (&InvokableCall_3_t3608808750_gp_2_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3608808750_gp_2_0_0_0 = { 1, GenInst_InvokableCall_3_t3608808750_gp_2_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_4_t879925395_gp_0_0_0_0_InvokableCall_4_t879925395_gp_1_0_0_0_InvokableCall_4_t879925395_gp_2_0_0_0_InvokableCall_4_t879925395_gp_3_0_0_0_Types[] = { (&InvokableCall_4_t879925395_gp_0_0_0_0), (&InvokableCall_4_t879925395_gp_1_0_0_0), (&InvokableCall_4_t879925395_gp_2_0_0_0), (&InvokableCall_4_t879925395_gp_3_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t879925395_gp_0_0_0_0_InvokableCall_4_t879925395_gp_1_0_0_0_InvokableCall_4_t879925395_gp_2_0_0_0_InvokableCall_4_t879925395_gp_3_0_0_0 = { 4, GenInst_InvokableCall_4_t879925395_gp_0_0_0_0_InvokableCall_4_t879925395_gp_1_0_0_0_InvokableCall_4_t879925395_gp_2_0_0_0_InvokableCall_4_t879925395_gp_3_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_4_t879925395_gp_0_0_0_0_Types[] = { (&InvokableCall_4_t879925395_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t879925395_gp_0_0_0_0 = { 1, GenInst_InvokableCall_4_t879925395_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_4_t879925395_gp_1_0_0_0_Types[] = { (&InvokableCall_4_t879925395_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t879925395_gp_1_0_0_0 = { 1, GenInst_InvokableCall_4_t879925395_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_4_t879925395_gp_2_0_0_0_Types[] = { (&InvokableCall_4_t879925395_gp_2_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t879925395_gp_2_0_0_0 = { 1, GenInst_InvokableCall_4_t879925395_gp_2_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_4_t879925395_gp_3_0_0_0_Types[] = { (&InvokableCall_4_t879925395_gp_3_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t879925395_gp_3_0_0_0 = { 1, GenInst_InvokableCall_4_t879925395_gp_3_0_0_0_Types };
static const RuntimeType* GenInst_CachedInvokableCall_1_t224769006_gp_0_0_0_0_Types[] = { (&CachedInvokableCall_1_t224769006_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_CachedInvokableCall_1_t224769006_gp_0_0_0_0 = { 1, GenInst_CachedInvokableCall_1_t224769006_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_UnityEvent_1_t4075366602_gp_0_0_0_0_Types[] = { (&UnityEvent_1_t4075366602_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityEvent_1_t4075366602_gp_0_0_0_0 = { 1, GenInst_UnityEvent_1_t4075366602_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_UnityEvent_2_t4075366599_gp_0_0_0_0_UnityEvent_2_t4075366599_gp_1_0_0_0_Types[] = { (&UnityEvent_2_t4075366599_gp_0_0_0_0), (&UnityEvent_2_t4075366599_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityEvent_2_t4075366599_gp_0_0_0_0_UnityEvent_2_t4075366599_gp_1_0_0_0 = { 2, GenInst_UnityEvent_2_t4075366599_gp_0_0_0_0_UnityEvent_2_t4075366599_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_UnityEvent_3_t4075366600_gp_0_0_0_0_UnityEvent_3_t4075366600_gp_1_0_0_0_UnityEvent_3_t4075366600_gp_2_0_0_0_Types[] = { (&UnityEvent_3_t4075366600_gp_0_0_0_0), (&UnityEvent_3_t4075366600_gp_1_0_0_0), (&UnityEvent_3_t4075366600_gp_2_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityEvent_3_t4075366600_gp_0_0_0_0_UnityEvent_3_t4075366600_gp_1_0_0_0_UnityEvent_3_t4075366600_gp_2_0_0_0 = { 3, GenInst_UnityEvent_3_t4075366600_gp_0_0_0_0_UnityEvent_3_t4075366600_gp_1_0_0_0_UnityEvent_3_t4075366600_gp_2_0_0_0_Types };
static const RuntimeType* GenInst_UnityEvent_4_t4075366597_gp_0_0_0_0_UnityEvent_4_t4075366597_gp_1_0_0_0_UnityEvent_4_t4075366597_gp_2_0_0_0_UnityEvent_4_t4075366597_gp_3_0_0_0_Types[] = { (&UnityEvent_4_t4075366597_gp_0_0_0_0), (&UnityEvent_4_t4075366597_gp_1_0_0_0), (&UnityEvent_4_t4075366597_gp_2_0_0_0), (&UnityEvent_4_t4075366597_gp_3_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityEvent_4_t4075366597_gp_0_0_0_0_UnityEvent_4_t4075366597_gp_1_0_0_0_UnityEvent_4_t4075366597_gp_2_0_0_0_UnityEvent_4_t4075366597_gp_3_0_0_0 = { 4, GenInst_UnityEvent_4_t4075366597_gp_0_0_0_0_UnityEvent_4_t4075366597_gp_1_0_0_0_UnityEvent_4_t4075366597_gp_2_0_0_0_UnityEvent_4_t4075366597_gp_3_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t2244783541_gp_0_0_0_0_Types[] = { (&List_1_t2244783541_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t2244783541_gp_0_0_0_0 = { 1, GenInst_List_1_t2244783541_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_U3CGetEnumeratorU3Ec__Iterator6_t1336800315_gp_0_0_0_0_Types[] = { (&U3CGetEnumeratorU3Ec__Iterator6_t1336800315_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_U3CGetEnumeratorU3Ec__Iterator6_t1336800315_gp_0_0_0_0 = { 1, GenInst_U3CGetEnumeratorU3Ec__Iterator6_t1336800315_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t264254312_0_0_0_Types[] = { (&List_1_t264254312_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t264254312_0_0_0 = { 1, GenInst_List_1_t264254312_0_0_0_Types };
static const RuntimeType* GenInst_Utilities_GetDictionaryValueOrNull_m3096514106_gp_0_0_0_0_Utilities_GetDictionaryValueOrNull_m3096514106_gp_1_0_0_0_Types[] = { (&Utilities_GetDictionaryValueOrNull_m3096514106_gp_0_0_0_0), (&Utilities_GetDictionaryValueOrNull_m3096514106_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Utilities_GetDictionaryValueOrNull_m3096514106_gp_0_0_0_0_Utilities_GetDictionaryValueOrNull_m3096514106_gp_1_0_0_0 = { 2, GenInst_Utilities_GetDictionaryValueOrNull_m3096514106_gp_0_0_0_0_Utilities_GetDictionaryValueOrNull_m3096514106_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_ExecuteEvents_Execute_m1744114673_gp_0_0_0_0_Types[] = { (&ExecuteEvents_Execute_m1744114673_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ExecuteEvents_Execute_m1744114673_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_Execute_m1744114673_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_ExecuteEvents_ExecuteHierarchy_m3091757213_gp_0_0_0_0_Types[] = { (&ExecuteEvents_ExecuteHierarchy_m3091757213_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ExecuteEvents_ExecuteHierarchy_m3091757213_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_ExecuteHierarchy_m3091757213_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_ExecuteEvents_GetEventList_m2263439269_gp_0_0_0_0_Types[] = { (&ExecuteEvents_GetEventList_m2263439269_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ExecuteEvents_GetEventList_m2263439269_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_GetEventList_m2263439269_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_ExecuteEvents_CanHandleEvent_m3190794379_gp_0_0_0_0_Types[] = { (&ExecuteEvents_CanHandleEvent_m3190794379_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ExecuteEvents_CanHandleEvent_m3190794379_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_CanHandleEvent_m3190794379_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_ExecuteEvents_GetEventHandler_m3792320302_gp_0_0_0_0_Types[] = { (&ExecuteEvents_GetEventHandler_m3792320302_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ExecuteEvents_GetEventHandler_m3792320302_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_GetEventHandler_m3792320302_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_TweenRunner_1_t2584777480_gp_0_0_0_0_Types[] = { (&TweenRunner_1_t2584777480_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_TweenRunner_1_t2584777480_gp_0_0_0_0 = { 1, GenInst_TweenRunner_1_t2584777480_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Dropdown_GetOrAddComponent_m41832880_gp_0_0_0_0_Types[] = { (&Dropdown_GetOrAddComponent_m41832880_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Dropdown_GetOrAddComponent_m41832880_gp_0_0_0_0 = { 1, GenInst_Dropdown_GetOrAddComponent_m41832880_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_SetPropertyUtility_SetStruct_m1212286169_gp_0_0_0_0_Types[] = { (&SetPropertyUtility_SetStruct_m1212286169_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_SetPropertyUtility_SetStruct_m1212286169_gp_0_0_0_0 = { 1, GenInst_SetPropertyUtility_SetStruct_m1212286169_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_IndexedSet_1_t573160278_gp_0_0_0_0_Types[] = { (&IndexedSet_1_t573160278_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_IndexedSet_1_t573160278_gp_0_0_0_0 = { 1, GenInst_IndexedSet_1_t573160278_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_IndexedSet_1_t573160278_gp_0_0_0_0_Int32_t2071877448_0_0_0_Types[] = { (&IndexedSet_1_t573160278_gp_0_0_0_0), (&Int32_t2071877448_0_0_0) };
extern const Il2CppGenericInst GenInst_IndexedSet_1_t573160278_gp_0_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_IndexedSet_1_t573160278_gp_0_0_0_0_Int32_t2071877448_0_0_0_Types };
static const RuntimeType* GenInst_ListPool_1_t1984115411_gp_0_0_0_0_Types[] = { (&ListPool_1_t1984115411_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ListPool_1_t1984115411_gp_0_0_0_0 = { 1, GenInst_ListPool_1_t1984115411_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t2000868992_0_0_0_Types[] = { (&List_1_t2000868992_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t2000868992_0_0_0 = { 1, GenInst_List_1_t2000868992_0_0_0_Types };
static const RuntimeType* GenInst_ObjectPool_1_t4265859154_gp_0_0_0_0_Types[] = { (&ObjectPool_1_t4265859154_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ObjectPool_1_t4265859154_gp_0_0_0_0 = { 1, GenInst_ObjectPool_1_t4265859154_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Singleton_1_t2363452800_gp_0_0_0_0_Types[] = { (&Singleton_1_t2363452800_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Singleton_1_t2363452800_gp_0_0_0_0 = { 1, GenInst_Singleton_1_t2363452800_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Utility_FindObjects_m2142378685_gp_0_0_0_0_Types[] = { (&Utility_FindObjects_m2142378685_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Utility_FindObjects_m2142378685_gp_0_0_0_0 = { 1, GenInst_Utility_FindObjects_m2142378685_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Utility_GetFromCache_m3302286621_gp_0_0_0_0_Types[] = { (&Utility_GetFromCache_m3302286621_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Utility_GetFromCache_m3302286621_gp_0_0_0_0 = { 1, GenInst_Utility_GetFromCache_m3302286621_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Utility_DeserializeResponse_m1146239540_gp_0_0_0_0_Types[] = { (&Utility_DeserializeResponse_m1146239540_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Utility_DeserializeResponse_m1146239540_gp_0_0_0_0 = { 1, GenInst_Utility_DeserializeResponse_m1146239540_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Utility_DeserializeResponse_m549829201_gp_0_0_0_0_Types[] = { (&Utility_DeserializeResponse_m549829201_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Utility_DeserializeResponse_m549829201_gp_0_0_0_0 = { 1, GenInst_Utility_DeserializeResponse_m549829201_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Widget_GetMembersByType_m59718008_gp_0_0_0_0_Types[] = { (&Widget_GetMembersByType_m59718008_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Widget_GetMembersByType_m59718008_gp_0_0_0_0 = { 1, GenInst_Widget_GetMembersByType_m59718008_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_fsEnumConverter_ArrayContains_m409985959_gp_0_0_0_0_Types[] = { (&fsEnumConverter_ArrayContains_m409985959_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_fsEnumConverter_ArrayContains_m409985959_gp_0_0_0_0 = { 1, GenInst_fsEnumConverter_ArrayContains_m409985959_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_fsDirectConverter_1_t3257037661_gp_0_0_0_0_Types[] = { (&fsDirectConverter_1_t3257037661_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_fsDirectConverter_1_t3257037661_gp_0_0_0_0 = { 1, GenInst_fsDirectConverter_1_t3257037661_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_fsOption_1_t4013878301_gp_0_0_0_0_Types[] = { (&fsOption_1_t4013878301_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_fsOption_1_t4013878301_gp_0_0_0_0 = { 1, GenInst_fsOption_1_t4013878301_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_fsOption_Just_m130823817_gp_0_0_0_0_Types[] = { (&fsOption_Just_m130823817_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_fsOption_Just_m130823817_gp_0_0_0_0 = { 1, GenInst_fsOption_Just_m130823817_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_fsPortableReflection_GetAttribute_m2688546569_gp_0_0_0_0_Types[] = { (&fsPortableReflection_GetAttribute_m2688546569_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_fsPortableReflection_GetAttribute_m2688546569_gp_0_0_0_0 = { 1, GenInst_fsPortableReflection_GetAttribute_m2688546569_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_EventHandlerExtensions_InvokeHandleExceptions_m2334443745_gp_0_0_0_0_Types[] = { (&EventHandlerExtensions_InvokeHandleExceptions_m2334443745_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_EventHandlerExtensions_InvokeHandleExceptions_m2334443745_gp_0_0_0_0 = { 1, GenInst_EventHandlerExtensions_InvokeHandleExceptions_m2334443745_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_ObjectPool_1_t3254757658_gp_0_0_0_0_ObjectPool_1_t3254757658_gp_0_0_0_0_Types[] = { (&ObjectPool_1_t3254757658_gp_0_0_0_0), (&ObjectPool_1_t3254757658_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ObjectPool_1_t3254757658_gp_0_0_0_0_ObjectPool_1_t3254757658_gp_0_0_0_0 = { 2, GenInst_ObjectPool_1_t3254757658_gp_0_0_0_0_ObjectPool_1_t3254757658_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_ObjectPool_1_t3254757658_gp_0_0_0_0_Types[] = { (&ObjectPool_1_t3254757658_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ObjectPool_1_t3254757658_gp_0_0_0_0 = { 1, GenInst_ObjectPool_1_t3254757658_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_TimedSequence_1_t2332636227_gp_0_0_0_0_Types[] = { (&TimedSequence_1_t2332636227_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_TimedSequence_1_t2332636227_gp_0_0_0_0 = { 1, GenInst_TimedSequence_1_t2332636227_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Ext_Contains_m2286382361_gp_0_0_0_0_Types[] = { (&Ext_Contains_m2286382361_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Ext_Contains_m2286382361_gp_0_0_0_0 = { 1, GenInst_Ext_Contains_m2286382361_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Ext_Contains_m2286382361_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { (&Ext_Contains_m2286382361_gp_0_0_0_0), (&Boolean_t3825574718_0_0_0) };
extern const Il2CppGenericInst GenInst_Ext_Contains_m2286382361_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Ext_Contains_m2286382361_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const RuntimeType* GenInst_Ext_ToList_m3286762625_gp_0_0_0_0_Types[] = { (&Ext_ToList_m3286762625_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Ext_ToList_m3286762625_gp_0_0_0_0 = { 1, GenInst_Ext_ToList_m3286762625_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Ext_Emit_m2311426299_gp_0_0_0_0_Types[] = { (&Ext_Emit_m2311426299_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Ext_Emit_m2311426299_gp_0_0_0_0 = { 1, GenInst_Ext_Emit_m2311426299_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Ext_ToString_m3280318684_gp_0_0_0_0_Types[] = { (&Ext_ToString_m3280318684_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Ext_ToString_m3280318684_gp_0_0_0_0 = { 1, GenInst_Ext_ToString_m3280318684_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_StringU5BU5D_t1642385972_0_0_0_HttpBase_Read_m191930573_gp_0_0_0_0_Types[] = { (&StringU5BU5D_t1642385972_0_0_0), (&HttpBase_Read_m191930573_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_StringU5BU5D_t1642385972_0_0_0_HttpBase_Read_m191930573_gp_0_0_0_0 = { 2, GenInst_StringU5BU5D_t1642385972_0_0_0_HttpBase_Read_m191930573_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_HttpBase_Read_m191930573_gp_0_0_0_0_Types[] = { (&HttpBase_Read_m191930573_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_HttpBase_Read_m191930573_gp_0_0_0_0 = { 1, GenInst_HttpBase_Read_m191930573_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_HttpServer_AddWebSocketService_m682758425_gp_0_0_0_0_Types[] = { (&HttpServer_AddWebSocketService_m682758425_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_HttpServer_AddWebSocketService_m682758425_gp_0_0_0_0 = { 1, GenInst_HttpServer_AddWebSocketService_m682758425_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_HttpServer_AddWebSocketService_m2770388401_gp_0_0_0_0_Types[] = { (&HttpServer_AddWebSocketService_m2770388401_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_HttpServer_AddWebSocketService_m2770388401_gp_0_0_0_0 = { 1, GenInst_HttpServer_AddWebSocketService_m2770388401_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_HttpServer_U3CAddWebSocketService_1U3Em__0_m662434750_gp_0_0_0_0_Types[] = { (&HttpServer_U3CAddWebSocketService_1U3Em__0_m662434750_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_HttpServer_U3CAddWebSocketService_1U3Em__0_m662434750_gp_0_0_0_0 = { 1, GenInst_HttpServer_U3CAddWebSocketService_1U3Em__0_m662434750_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_WebSocketServer_AddWebSocketService_m2616226100_gp_0_0_0_0_Types[] = { (&WebSocketServer_AddWebSocketService_m2616226100_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_WebSocketServer_AddWebSocketService_m2616226100_gp_0_0_0_0 = { 1, GenInst_WebSocketServer_AddWebSocketService_m2616226100_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_WebSocketServer_AddWebSocketService_m1120545350_gp_0_0_0_0_Types[] = { (&WebSocketServer_AddWebSocketService_m1120545350_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_WebSocketServer_AddWebSocketService_m1120545350_gp_0_0_0_0 = { 1, GenInst_WebSocketServer_AddWebSocketService_m1120545350_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_WebSocketServer_U3CAddWebSocketService_1U3Em__0_m3359671309_gp_0_0_0_0_Types[] = { (&WebSocketServer_U3CAddWebSocketService_1U3Em__0_m3359671309_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_WebSocketServer_U3CAddWebSocketService_1U3Em__0_m3359671309_gp_0_0_0_0 = { 1, GenInst_WebSocketServer_U3CAddWebSocketService_1U3Em__0_m3359671309_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_WebSocketServiceHost_1_t1698079305_gp_0_0_0_0_Types[] = { (&WebSocketServiceHost_1_t1698079305_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_WebSocketServiceHost_1_t1698079305_gp_0_0_0_0 = { 1, GenInst_WebSocketServiceHost_1_t1698079305_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_WebSocketServiceManager_Add_m3346916827_gp_0_0_0_0_Types[] = { (&WebSocketServiceManager_Add_m3346916827_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_WebSocketServiceManager_Add_m3346916827_gp_0_0_0_0 = { 1, GenInst_WebSocketServiceManager_Add_m3346916827_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_OscTimeTag_t625345318_0_0_0_Types[] = { (&OscTimeTag_t625345318_0_0_0) };
extern const Il2CppGenericInst GenInst_OscTimeTag_t625345318_0_0_0 = { 1, GenInst_OscTimeTag_t625345318_0_0_0_Types };
static const RuntimeType* GenInst_AnimationPlayableOutput_t260357453_0_0_0_Types[] = { (&AnimationPlayableOutput_t260357453_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimationPlayableOutput_t260357453_0_0_0 = { 1, GenInst_AnimationPlayableOutput_t260357453_0_0_0_Types };
static const RuntimeType* GenInst_DefaultExecutionOrder_t2717914595_0_0_0_Types[] = { (&DefaultExecutionOrder_t2717914595_0_0_0) };
extern const Il2CppGenericInst GenInst_DefaultExecutionOrder_t2717914595_0_0_0 = { 1, GenInst_DefaultExecutionOrder_t2717914595_0_0_0_Types };
static const RuntimeType* GenInst_AudioPlayableOutput_t3110750149_0_0_0_Types[] = { (&AudioPlayableOutput_t3110750149_0_0_0) };
extern const Il2CppGenericInst GenInst_AudioPlayableOutput_t3110750149_0_0_0 = { 1, GenInst_AudioPlayableOutput_t3110750149_0_0_0_Types };
static const RuntimeType* GenInst_PlayerConnection_t3517219175_0_0_0_Types[] = { (&PlayerConnection_t3517219175_0_0_0) };
extern const Il2CppGenericInst GenInst_PlayerConnection_t3517219175_0_0_0 = { 1, GenInst_PlayerConnection_t3517219175_0_0_0_Types };
static const RuntimeType* GenInst_ScriptPlayableOutput_t1666207618_0_0_0_Types[] = { (&ScriptPlayableOutput_t1666207618_0_0_0) };
extern const Il2CppGenericInst GenInst_ScriptPlayableOutput_t1666207618_0_0_0 = { 1, GenInst_ScriptPlayableOutput_t1666207618_0_0_0_Types };
static const RuntimeType* GenInst_GUILayer_t3254902478_0_0_0_Types[] = { (&GUILayer_t3254902478_0_0_0) };
extern const Il2CppGenericInst GenInst_GUILayer_t3254902478_0_0_0 = { 1, GenInst_GUILayer_t3254902478_0_0_0_Types };
static const RuntimeType* GenInst_EventSystem_t3466835263_0_0_0_Types[] = { (&EventSystem_t3466835263_0_0_0) };
extern const Il2CppGenericInst GenInst_EventSystem_t3466835263_0_0_0 = { 1, GenInst_EventSystem_t3466835263_0_0_0_Types };
static const RuntimeType* GenInst_AxisEventData_t1524870173_0_0_0_Types[] = { (&AxisEventData_t1524870173_0_0_0) };
extern const Il2CppGenericInst GenInst_AxisEventData_t1524870173_0_0_0 = { 1, GenInst_AxisEventData_t1524870173_0_0_0_Types };
static const RuntimeType* GenInst_SpriteRenderer_t1209076198_0_0_0_Types[] = { (&SpriteRenderer_t1209076198_0_0_0) };
extern const Il2CppGenericInst GenInst_SpriteRenderer_t1209076198_0_0_0 = { 1, GenInst_SpriteRenderer_t1209076198_0_0_0_Types };
static const RuntimeType* GenInst_Image_t2042527209_0_0_0_Types[] = { (&Image_t2042527209_0_0_0) };
extern const Il2CppGenericInst GenInst_Image_t2042527209_0_0_0 = { 1, GenInst_Image_t2042527209_0_0_0_Types };
static const RuntimeType* GenInst_Button_t2872111280_0_0_0_Types[] = { (&Button_t2872111280_0_0_0) };
extern const Il2CppGenericInst GenInst_Button_t2872111280_0_0_0 = { 1, GenInst_Button_t2872111280_0_0_0_Types };
static const RuntimeType* GenInst_RawImage_t2749640213_0_0_0_Types[] = { (&RawImage_t2749640213_0_0_0) };
extern const Il2CppGenericInst GenInst_RawImage_t2749640213_0_0_0 = { 1, GenInst_RawImage_t2749640213_0_0_0_Types };
static const RuntimeType* GenInst_Slider_t297367283_0_0_0_Types[] = { (&Slider_t297367283_0_0_0) };
extern const Il2CppGenericInst GenInst_Slider_t297367283_0_0_0 = { 1, GenInst_Slider_t297367283_0_0_0_Types };
static const RuntimeType* GenInst_Scrollbar_t3248359358_0_0_0_Types[] = { (&Scrollbar_t3248359358_0_0_0) };
extern const Il2CppGenericInst GenInst_Scrollbar_t3248359358_0_0_0 = { 1, GenInst_Scrollbar_t3248359358_0_0_0_Types };
static const RuntimeType* GenInst_InputField_t1631627530_0_0_0_Types[] = { (&InputField_t1631627530_0_0_0) };
extern const Il2CppGenericInst GenInst_InputField_t1631627530_0_0_0 = { 1, GenInst_InputField_t1631627530_0_0_0_Types };
static const RuntimeType* GenInst_ScrollRect_t1199013257_0_0_0_Types[] = { (&ScrollRect_t1199013257_0_0_0) };
extern const Il2CppGenericInst GenInst_ScrollRect_t1199013257_0_0_0 = { 1, GenInst_ScrollRect_t1199013257_0_0_0_Types };
static const RuntimeType* GenInst_Dropdown_t1985816271_0_0_0_Types[] = { (&Dropdown_t1985816271_0_0_0) };
extern const Il2CppGenericInst GenInst_Dropdown_t1985816271_0_0_0 = { 1, GenInst_Dropdown_t1985816271_0_0_0_Types };
static const RuntimeType* GenInst_GraphicRaycaster_t410733016_0_0_0_Types[] = { (&GraphicRaycaster_t410733016_0_0_0) };
extern const Il2CppGenericInst GenInst_GraphicRaycaster_t410733016_0_0_0 = { 1, GenInst_GraphicRaycaster_t410733016_0_0_0_Types };
static const RuntimeType* GenInst_CanvasRenderer_t261436805_0_0_0_Types[] = { (&CanvasRenderer_t261436805_0_0_0) };
extern const Il2CppGenericInst GenInst_CanvasRenderer_t261436805_0_0_0 = { 1, GenInst_CanvasRenderer_t261436805_0_0_0_Types };
static const RuntimeType* GenInst_Corner_t1077473318_0_0_0_Types[] = { (&Corner_t1077473318_0_0_0) };
extern const Il2CppGenericInst GenInst_Corner_t1077473318_0_0_0 = { 1, GenInst_Corner_t1077473318_0_0_0_Types };
static const RuntimeType* GenInst_Axis_t1431825778_0_0_0_Types[] = { (&Axis_t1431825778_0_0_0) };
extern const Il2CppGenericInst GenInst_Axis_t1431825778_0_0_0 = { 1, GenInst_Axis_t1431825778_0_0_0_Types };
static const RuntimeType* GenInst_Constraint_t3558160636_0_0_0_Types[] = { (&Constraint_t3558160636_0_0_0) };
extern const Il2CppGenericInst GenInst_Constraint_t3558160636_0_0_0 = { 1, GenInst_Constraint_t3558160636_0_0_0_Types };
static const RuntimeType* GenInst_SubmitEvent_t907918422_0_0_0_Types[] = { (&SubmitEvent_t907918422_0_0_0) };
extern const Il2CppGenericInst GenInst_SubmitEvent_t907918422_0_0_0 = { 1, GenInst_SubmitEvent_t907918422_0_0_0_Types };
static const RuntimeType* GenInst_OnChangeEvent_t2863344003_0_0_0_Types[] = { (&OnChangeEvent_t2863344003_0_0_0) };
extern const Il2CppGenericInst GenInst_OnChangeEvent_t2863344003_0_0_0 = { 1, GenInst_OnChangeEvent_t2863344003_0_0_0_Types };
static const RuntimeType* GenInst_OnValidateInput_t1946318473_0_0_0_Types[] = { (&OnValidateInput_t1946318473_0_0_0) };
extern const Il2CppGenericInst GenInst_OnValidateInput_t1946318473_0_0_0 = { 1, GenInst_OnValidateInput_t1946318473_0_0_0_Types };
static const RuntimeType* GenInst_LayoutElement_t2808691390_0_0_0_Types[] = { (&LayoutElement_t2808691390_0_0_0) };
extern const Il2CppGenericInst GenInst_LayoutElement_t2808691390_0_0_0 = { 1, GenInst_LayoutElement_t2808691390_0_0_0_Types };
static const RuntimeType* GenInst_RectOffset_t3387826427_0_0_0_Types[] = { (&RectOffset_t3387826427_0_0_0) };
extern const Il2CppGenericInst GenInst_RectOffset_t3387826427_0_0_0 = { 1, GenInst_RectOffset_t3387826427_0_0_0_Types };
static const RuntimeType* GenInst_TextAnchor_t112990806_0_0_0_Types[] = { (&TextAnchor_t112990806_0_0_0) };
extern const Il2CppGenericInst GenInst_TextAnchor_t112990806_0_0_0 = { 1, GenInst_TextAnchor_t112990806_0_0_0_Types };
static const RuntimeType* GenInst_AnimationTriggers_t3244928895_0_0_0_Types[] = { (&AnimationTriggers_t3244928895_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimationTriggers_t3244928895_0_0_0 = { 1, GenInst_AnimationTriggers_t3244928895_0_0_0_Types };
static const RuntimeType* GenInst_UnityARVideo_t2351297253_0_0_0_Types[] = { (&UnityARVideo_t2351297253_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityARVideo_t2351297253_0_0_0 = { 1, GenInst_UnityARVideo_t2351297253_0_0_0_Types };
static const RuntimeType* GenInst_DontDestroyOnLoad_t3235789354_0_0_0_Types[] = { (&DontDestroyOnLoad_t3235789354_0_0_0) };
extern const Il2CppGenericInst GenInst_DontDestroyOnLoad_t3235789354_0_0_0 = { 1, GenInst_DontDestroyOnLoad_t3235789354_0_0_0_Types };
static const RuntimeType* GenInst_MeshFilter_t3026937449_0_0_0_Types[] = { (&MeshFilter_t3026937449_0_0_0) };
extern const Il2CppGenericInst GenInst_MeshFilter_t3026937449_0_0_0 = { 1, GenInst_MeshFilter_t3026937449_0_0_0_Types };
static const RuntimeType* GenInst_MeshRenderer_t1268241104_0_0_0_Types[] = { (&MeshRenderer_t1268241104_0_0_0) };
extern const Il2CppGenericInst GenInst_MeshRenderer_t1268241104_0_0_0 = { 1, GenInst_MeshRenderer_t1268241104_0_0_0_Types };
static const RuntimeType* GenInst_AudioSource_t1135106623_0_0_0_Types[] = { (&AudioSource_t1135106623_0_0_0) };
extern const Il2CppGenericInst GenInst_AudioSource_t1135106623_0_0_0 = { 1, GenInst_AudioSource_t1135106623_0_0_0_Types };
static const RuntimeType* GenInst_Dictionary_2_t203617571_0_0_0_Types[] = { (&Dictionary_2_t203617571_0_0_0) };
extern const Il2CppGenericInst GenInst_Dictionary_2_t203617571_0_0_0 = { 1, GenInst_Dictionary_2_t203617571_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t1952926737_0_0_0_Types[] = { (&List_1_t1952926737_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t1952926737_0_0_0 = { 1, GenInst_List_1_t1952926737_0_0_0_Types };
static const RuntimeType* GenInst_fsObjectAttribute_t2382840370_0_0_0_Types[] = { (&fsObjectAttribute_t2382840370_0_0_0) };
extern const Il2CppGenericInst GenInst_fsObjectAttribute_t2382840370_0_0_0 = { 1, GenInst_fsObjectAttribute_t2382840370_0_0_0_Types };
static const RuntimeType* GenInst_fsForwardAttribute_t3349051188_0_0_0_Types[] = { (&fsForwardAttribute_t3349051188_0_0_0) };
extern const Il2CppGenericInst GenInst_fsForwardAttribute_t3349051188_0_0_0 = { 1, GenInst_fsForwardAttribute_t3349051188_0_0_0_Types };
static const RuntimeType* GenInst_KeyframeU5BU5D_t449065829_0_0_0_Types[] = { (&KeyframeU5BU5D_t449065829_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyframeU5BU5D_t449065829_0_0_0 = { 1, GenInst_KeyframeU5BU5D_t449065829_0_0_0_Types };
static const RuntimeType* GenInst_WrapMode_t255797857_0_0_0_Types[] = { (&WrapMode_t255797857_0_0_0) };
extern const Il2CppGenericInst GenInst_WrapMode_t255797857_0_0_0 = { 1, GenInst_WrapMode_t255797857_0_0_0_Types };
static const RuntimeType* GenInst_GradientAlphaKeyU5BU5D_t3410745760_0_0_0_Types[] = { (&GradientAlphaKeyU5BU5D_t3410745760_0_0_0) };
extern const Il2CppGenericInst GenInst_GradientAlphaKeyU5BU5D_t3410745760_0_0_0 = { 1, GenInst_GradientAlphaKeyU5BU5D_t3410745760_0_0_0_Types };
static const RuntimeType* GenInst_GradientColorKeyU5BU5D_t3863936331_0_0_0_Types[] = { (&GradientColorKeyU5BU5D_t3863936331_0_0_0) };
extern const Il2CppGenericInst GenInst_GradientColorKeyU5BU5D_t3863936331_0_0_0 = { 1, GenInst_GradientColorKeyU5BU5D_t3863936331_0_0_0_Types };
static const RuntimeType* GenInst_FlagsAttribute_t859561169_0_0_0_Types[] = { (&FlagsAttribute_t859561169_0_0_0) };
extern const Il2CppGenericInst GenInst_FlagsAttribute_t859561169_0_0_0 = { 1, GenInst_FlagsAttribute_t859561169_0_0_0_Types };
static const RuntimeType* GenInst_fsPropertyAttribute_t4237399860_0_0_0_Types[] = { (&fsPropertyAttribute_t4237399860_0_0_0) };
extern const Il2CppGenericInst GenInst_fsPropertyAttribute_t4237399860_0_0_0 = { 1, GenInst_fsPropertyAttribute_t4237399860_0_0_0_Types };
static const RuntimeType* GenInst_WatsonCamera_t824577261_0_0_0_Types[] = { (&WatsonCamera_t824577261_0_0_0) };
extern const Il2CppGenericInst GenInst_WatsonCamera_t824577261_0_0_0 = { 1, GenInst_WatsonCamera_t824577261_0_0_0_Types };
static const RuntimeType* GenInst_MicrophoneWidget_t2442369682_0_0_0_Types[] = { (&MicrophoneWidget_t2442369682_0_0_0) };
extern const Il2CppGenericInst GenInst_MicrophoneWidget_t2442369682_0_0_0 = { 1, GenInst_MicrophoneWidget_t2442369682_0_0_0_Types };
static const RuntimeType* GenInst_Problem_t2814813345_0_0_0_Types[] = { (&Problem_t2814813345_0_0_0) };
extern const Il2CppGenericInst GenInst_Problem_t2814813345_0_0_0 = { 1, GenInst_Problem_t2814813345_0_0_0_Types };
static const RuntimeType* GenInst_IFF_FORM_CHUNK_t624775701_0_0_0_Types[] = { (&IFF_FORM_CHUNK_t624775701_0_0_0) };
extern const Il2CppGenericInst GenInst_IFF_FORM_CHUNK_t624775701_0_0_0 = { 1, GenInst_IFF_FORM_CHUNK_t624775701_0_0_0_Types };
static const RuntimeType* GenInst_IFF_CHUNK_t4256546638_0_0_0_Types[] = { (&IFF_CHUNK_t4256546638_0_0_0) };
extern const Il2CppGenericInst GenInst_IFF_CHUNK_t4256546638_0_0_0 = { 1, GenInst_IFF_CHUNK_t4256546638_0_0_0_Types };
static const RuntimeType* GenInst_WAV_PCM_t2882996802_0_0_0_Types[] = { (&WAV_PCM_t2882996802_0_0_0) };
extern const Il2CppGenericInst GenInst_WAV_PCM_t2882996802_0_0_0 = { 1, GenInst_WAV_PCM_t2882996802_0_0_0_Types };
static const RuntimeType* GenInst_BoxSlider_t1871650694_0_0_0_Types[] = { (&BoxSlider_t1871650694_0_0_0) };
extern const Il2CppGenericInst GenInst_BoxSlider_t1871650694_0_0_0 = { 1, GenInst_BoxSlider_t1871650694_0_0_0_Types };
static const RuntimeType* GenInst_GenericDisplayDevice_t579608737_0_0_0_Types[] = { (&GenericDisplayDevice_t579608737_0_0_0) };
extern const Il2CppGenericInst GenInst_GenericDisplayDevice_t579608737_0_0_0 = { 1, GenInst_GenericDisplayDevice_t579608737_0_0_0_Types };
static const RuntimeType* GenInst_CameraLayer_t464507322_0_0_0_Types[] = { (&CameraLayer_t464507322_0_0_0) };
extern const Il2CppGenericInst GenInst_CameraLayer_t464507322_0_0_0 = { 1, GenInst_CameraLayer_t464507322_0_0_0_Types };
static const RuntimeType* GenInst_StandardInput_t4102879489_0_0_0_Types[] = { (&StandardInput_t4102879489_0_0_0) };
extern const Il2CppGenericInst GenInst_StandardInput_t4102879489_0_0_0 = { 1, GenInst_StandardInput_t4102879489_0_0_0_Types };
static const RuntimeType* GenInst_Light_t494725636_0_0_0_Types[] = { (&Light_t494725636_0_0_0) };
extern const Il2CppGenericInst GenInst_Light_t494725636_0_0_0 = { 1, GenInst_Light_t494725636_0_0_0_Types };
static const RuntimeType* GenInst_Rigidbody_t4233889191_0_0_0_Types[] = { (&Rigidbody_t4233889191_0_0_0) };
extern const Il2CppGenericInst GenInst_Rigidbody_t4233889191_0_0_0 = { 1, GenInst_Rigidbody_t4233889191_0_0_0_Types };
static const RuntimeType* GenInst_AotCompilation_t21908242_0_0_0_AotCompilation_t21908242_0_0_0_Types[] = { (&AotCompilation_t21908242_0_0_0), (&AotCompilation_t21908242_0_0_0) };
extern const Il2CppGenericInst GenInst_AotCompilation_t21908242_0_0_0_AotCompilation_t21908242_0_0_0 = { 2, GenInst_AotCompilation_t21908242_0_0_0_AotCompilation_t21908242_0_0_0_Types };
static const RuntimeType* GenInst_fsVersionedType_t654750358_0_0_0_fsVersionedType_t654750358_0_0_0_Types[] = { (&fsVersionedType_t654750358_0_0_0), (&fsVersionedType_t654750358_0_0_0) };
extern const Il2CppGenericInst GenInst_fsVersionedType_t654750358_0_0_0_fsVersionedType_t654750358_0_0_0 = { 2, GenInst_fsVersionedType_t654750358_0_0_0_fsVersionedType_t654750358_0_0_0_Types };
static const RuntimeType* GenInst_Byte_t3683104436_0_0_0_Byte_t3683104436_0_0_0_Types[] = { (&Byte_t3683104436_0_0_0), (&Byte_t3683104436_0_0_0) };
extern const Il2CppGenericInst GenInst_Byte_t3683104436_0_0_0_Byte_t3683104436_0_0_0 = { 2, GenInst_Byte_t3683104436_0_0_0_Byte_t3683104436_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t38854645_0_0_0_KeyValuePair_2_t38854645_0_0_0_Types[] = { (&KeyValuePair_2_t38854645_0_0_0), (&KeyValuePair_2_t38854645_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t38854645_0_0_0_KeyValuePair_2_t38854645_0_0_0 = { 2, GenInst_KeyValuePair_2_t38854645_0_0_0_KeyValuePair_2_t38854645_0_0_0_Types };
static const RuntimeType* GenInst_CustomAttributeNamedArgument_t94157543_0_0_0_CustomAttributeNamedArgument_t94157543_0_0_0_Types[] = { (&CustomAttributeNamedArgument_t94157543_0_0_0), (&CustomAttributeNamedArgument_t94157543_0_0_0) };
extern const Il2CppGenericInst GenInst_CustomAttributeNamedArgument_t94157543_0_0_0_CustomAttributeNamedArgument_t94157543_0_0_0 = { 2, GenInst_CustomAttributeNamedArgument_t94157543_0_0_0_CustomAttributeNamedArgument_t94157543_0_0_0_Types };
static const RuntimeType* GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0_CustomAttributeTypedArgument_t1498197914_0_0_0_Types[] = { (&CustomAttributeTypedArgument_t1498197914_0_0_0), (&CustomAttributeTypedArgument_t1498197914_0_0_0) };
extern const Il2CppGenericInst GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0_CustomAttributeTypedArgument_t1498197914_0_0_0 = { 2, GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0_CustomAttributeTypedArgument_t1498197914_0_0_0_Types };
static const RuntimeType* GenInst_AnimatorClipInfo_t3905751349_0_0_0_AnimatorClipInfo_t3905751349_0_0_0_Types[] = { (&AnimatorClipInfo_t3905751349_0_0_0), (&AnimatorClipInfo_t3905751349_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimatorClipInfo_t3905751349_0_0_0_AnimatorClipInfo_t3905751349_0_0_0 = { 2, GenInst_AnimatorClipInfo_t3905751349_0_0_0_AnimatorClipInfo_t3905751349_0_0_0_Types };
static const RuntimeType* GenInst_Color32_t874517518_0_0_0_Color32_t874517518_0_0_0_Types[] = { (&Color32_t874517518_0_0_0), (&Color32_t874517518_0_0_0) };
extern const Il2CppGenericInst GenInst_Color32_t874517518_0_0_0_Color32_t874517518_0_0_0 = { 2, GenInst_Color32_t874517518_0_0_0_Color32_t874517518_0_0_0_Types };
static const RuntimeType* GenInst_RaycastResult_t21186376_0_0_0_RaycastResult_t21186376_0_0_0_Types[] = { (&RaycastResult_t21186376_0_0_0), (&RaycastResult_t21186376_0_0_0) };
extern const Il2CppGenericInst GenInst_RaycastResult_t21186376_0_0_0_RaycastResult_t21186376_0_0_0 = { 2, GenInst_RaycastResult_t21186376_0_0_0_RaycastResult_t21186376_0_0_0_Types };
static const RuntimeType* GenInst_RaycastHit_t87180320_0_0_0_RaycastHit_t87180320_0_0_0_Types[] = { (&RaycastHit_t87180320_0_0_0), (&RaycastHit_t87180320_0_0_0) };
extern const Il2CppGenericInst GenInst_RaycastHit_t87180320_0_0_0_RaycastHit_t87180320_0_0_0 = { 2, GenInst_RaycastHit_t87180320_0_0_0_RaycastHit_t87180320_0_0_0_Types };
static const RuntimeType* GenInst_RaycastHit2D_t4063908774_0_0_0_RaycastHit2D_t4063908774_0_0_0_Types[] = { (&RaycastHit2D_t4063908774_0_0_0), (&RaycastHit2D_t4063908774_0_0_0) };
extern const Il2CppGenericInst GenInst_RaycastHit2D_t4063908774_0_0_0_RaycastHit2D_t4063908774_0_0_0 = { 2, GenInst_RaycastHit2D_t4063908774_0_0_0_RaycastHit2D_t4063908774_0_0_0_Types };
static const RuntimeType* GenInst_UICharInfo_t3056636800_0_0_0_UICharInfo_t3056636800_0_0_0_Types[] = { (&UICharInfo_t3056636800_0_0_0), (&UICharInfo_t3056636800_0_0_0) };
extern const Il2CppGenericInst GenInst_UICharInfo_t3056636800_0_0_0_UICharInfo_t3056636800_0_0_0 = { 2, GenInst_UICharInfo_t3056636800_0_0_0_UICharInfo_t3056636800_0_0_0_Types };
static const RuntimeType* GenInst_UILineInfo_t3621277874_0_0_0_UILineInfo_t3621277874_0_0_0_Types[] = { (&UILineInfo_t3621277874_0_0_0), (&UILineInfo_t3621277874_0_0_0) };
extern const Il2CppGenericInst GenInst_UILineInfo_t3621277874_0_0_0_UILineInfo_t3621277874_0_0_0 = { 2, GenInst_UILineInfo_t3621277874_0_0_0_UILineInfo_t3621277874_0_0_0_Types };
static const RuntimeType* GenInst_UIVertex_t1204258818_0_0_0_UIVertex_t1204258818_0_0_0_Types[] = { (&UIVertex_t1204258818_0_0_0), (&UIVertex_t1204258818_0_0_0) };
extern const Il2CppGenericInst GenInst_UIVertex_t1204258818_0_0_0_UIVertex_t1204258818_0_0_0 = { 2, GenInst_UIVertex_t1204258818_0_0_0_UIVertex_t1204258818_0_0_0_Types };
static const RuntimeType* GenInst_Vector2_t2243707579_0_0_0_Vector2_t2243707579_0_0_0_Types[] = { (&Vector2_t2243707579_0_0_0), (&Vector2_t2243707579_0_0_0) };
extern const Il2CppGenericInst GenInst_Vector2_t2243707579_0_0_0_Vector2_t2243707579_0_0_0 = { 2, GenInst_Vector2_t2243707579_0_0_0_Vector2_t2243707579_0_0_0_Types };
static const RuntimeType* GenInst_Vector3_t2243707580_0_0_0_Vector3_t2243707580_0_0_0_Types[] = { (&Vector3_t2243707580_0_0_0), (&Vector3_t2243707580_0_0_0) };
extern const Il2CppGenericInst GenInst_Vector3_t2243707580_0_0_0_Vector3_t2243707580_0_0_0 = { 2, GenInst_Vector3_t2243707580_0_0_0_Vector3_t2243707580_0_0_0_Types };
static const RuntimeType* GenInst_Vector4_t2243707581_0_0_0_Vector4_t2243707581_0_0_0_Types[] = { (&Vector4_t2243707581_0_0_0), (&Vector4_t2243707581_0_0_0) };
extern const Il2CppGenericInst GenInst_Vector4_t2243707581_0_0_0_Vector4_t2243707581_0_0_0 = { 2, GenInst_Vector4_t2243707581_0_0_0_Vector4_t2243707581_0_0_0_Types };
static const RuntimeType* GenInst_ARHitTestResult_t3275513025_0_0_0_ARHitTestResult_t3275513025_0_0_0_Types[] = { (&ARHitTestResult_t3275513025_0_0_0), (&ARHitTestResult_t3275513025_0_0_0) };
extern const Il2CppGenericInst GenInst_ARHitTestResult_t3275513025_0_0_0_ARHitTestResult_t3275513025_0_0_0 = { 2, GenInst_ARHitTestResult_t3275513025_0_0_0_ARHitTestResult_t3275513025_0_0_0_Types };
static const RuntimeType* GenInst_AttributeQuery_t604298480_0_0_0_AttributeQuery_t604298480_0_0_0_Types[] = { (&AttributeQuery_t604298480_0_0_0), (&AttributeQuery_t604298480_0_0_0) };
extern const Il2CppGenericInst GenInst_AttributeQuery_t604298480_0_0_0_AttributeQuery_t604298480_0_0_0 = { 2, GenInst_AttributeQuery_t604298480_0_0_0_AttributeQuery_t604298480_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t288805040_0_0_0_KeyValuePair_2_t288805040_0_0_0_Types[] = { (&KeyValuePair_2_t288805040_0_0_0), (&KeyValuePair_2_t288805040_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t288805040_0_0_0_KeyValuePair_2_t288805040_0_0_0 = { 2, GenInst_KeyValuePair_2_t288805040_0_0_0_KeyValuePair_2_t288805040_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t288805040_0_0_0_RuntimeObject_0_0_0_Types[] = { (&KeyValuePair_2_t288805040_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t288805040_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t288805040_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_AudioFormatType_t2756784245_0_0_0_AudioFormatType_t2756784245_0_0_0_Types[] = { (&AudioFormatType_t2756784245_0_0_0), (&AudioFormatType_t2756784245_0_0_0) };
extern const Il2CppGenericInst GenInst_AudioFormatType_t2756784245_0_0_0_AudioFormatType_t2756784245_0_0_0 = { 2, GenInst_AudioFormatType_t2756784245_0_0_0_AudioFormatType_t2756784245_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t993946999_0_0_0_KeyValuePair_2_t993946999_0_0_0_Types[] = { (&KeyValuePair_2_t993946999_0_0_0), (&KeyValuePair_2_t993946999_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t993946999_0_0_0_KeyValuePair_2_t993946999_0_0_0 = { 2, GenInst_KeyValuePair_2_t993946999_0_0_0_KeyValuePair_2_t993946999_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t993946999_0_0_0_RuntimeObject_0_0_0_Types[] = { (&KeyValuePair_2_t993946999_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t993946999_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t993946999_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_VoiceType_t981025524_0_0_0_VoiceType_t981025524_0_0_0_Types[] = { (&VoiceType_t981025524_0_0_0), (&VoiceType_t981025524_0_0_0) };
extern const Il2CppGenericInst GenInst_VoiceType_t981025524_0_0_0_VoiceType_t981025524_0_0_0 = { 2, GenInst_VoiceType_t981025524_0_0_0_VoiceType_t981025524_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t4282424860_0_0_0_KeyValuePair_2_t4282424860_0_0_0_Types[] = { (&KeyValuePair_2_t4282424860_0_0_0), (&KeyValuePair_2_t4282424860_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4282424860_0_0_0_KeyValuePair_2_t4282424860_0_0_0 = { 2, GenInst_KeyValuePair_2_t4282424860_0_0_0_KeyValuePair_2_t4282424860_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t4282424860_0_0_0_RuntimeObject_0_0_0_Types[] = { (&KeyValuePair_2_t4282424860_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4282424860_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t4282424860_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3132015601_0_0_0_KeyValuePair_2_t3132015601_0_0_0_Types[] = { (&KeyValuePair_2_t3132015601_0_0_0), (&KeyValuePair_2_t3132015601_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3132015601_0_0_0_KeyValuePair_2_t3132015601_0_0_0 = { 2, GenInst_KeyValuePair_2_t3132015601_0_0_0_KeyValuePair_2_t3132015601_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3132015601_0_0_0_RuntimeObject_0_0_0_Types[] = { (&KeyValuePair_2_t3132015601_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3132015601_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t3132015601_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3749587448_0_0_0_KeyValuePair_2_t3749587448_0_0_0_Types[] = { (&KeyValuePair_2_t3749587448_0_0_0), (&KeyValuePair_2_t3749587448_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3749587448_0_0_0_KeyValuePair_2_t3749587448_0_0_0 = { 2, GenInst_KeyValuePair_2_t3749587448_0_0_0_KeyValuePair_2_t3749587448_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3749587448_0_0_0_RuntimeObject_0_0_0_Types[] = { (&KeyValuePair_2_t3749587448_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3749587448_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t3749587448_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t645770832_0_0_0_KeyValuePair_2_t645770832_0_0_0_Types[] = { (&KeyValuePair_2_t645770832_0_0_0), (&KeyValuePair_2_t645770832_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t645770832_0_0_0_KeyValuePair_2_t645770832_0_0_0 = { 2, GenInst_KeyValuePair_2_t645770832_0_0_0_KeyValuePair_2_t645770832_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t645770832_0_0_0_RuntimeObject_0_0_0_Types[] = { (&KeyValuePair_2_t645770832_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t645770832_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t645770832_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_TouchData_t3880599975_0_0_0_RuntimeObject_0_0_0_Types[] = { (&TouchData_t3880599975_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_TouchData_t3880599975_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_TouchData_t3880599975_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_TouchData_t3880599975_0_0_0_TouchData_t3880599975_0_0_0_Types[] = { (&TouchData_t3880599975_0_0_0), (&TouchData_t3880599975_0_0_0) };
extern const Il2CppGenericInst GenInst_TouchData_t3880599975_0_0_0_TouchData_t3880599975_0_0_0 = { 2, GenInst_TouchData_t3880599975_0_0_0_TouchData_t3880599975_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3792220452_0_0_0_KeyValuePair_2_t3792220452_0_0_0_Types[] = { (&KeyValuePair_2_t3792220452_0_0_0), (&KeyValuePair_2_t3792220452_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3792220452_0_0_0_KeyValuePair_2_t3792220452_0_0_0 = { 2, GenInst_KeyValuePair_2_t3792220452_0_0_0_KeyValuePair_2_t3792220452_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3792220452_0_0_0_RuntimeObject_0_0_0_Types[] = { (&KeyValuePair_2_t3792220452_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3792220452_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t3792220452_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_TouchState_t2732082299_0_0_0_RuntimeObject_0_0_0_Types[] = { (&TouchState_t2732082299_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_TouchState_t2732082299_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_TouchState_t2732082299_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_TouchState_t2732082299_0_0_0_TouchState_t2732082299_0_0_0_Types[] = { (&TouchState_t2732082299_0_0_0), (&TouchState_t2732082299_0_0_0) };
extern const Il2CppGenericInst GenInst_TouchState_t2732082299_0_0_0_TouchState_t2732082299_0_0_0 = { 2, GenInst_TouchState_t2732082299_0_0_0_TouchState_t2732082299_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t888819835_0_0_0_KeyValuePair_2_t888819835_0_0_0_Types[] = { (&KeyValuePair_2_t888819835_0_0_0), (&KeyValuePair_2_t888819835_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t888819835_0_0_0_KeyValuePair_2_t888819835_0_0_0 = { 2, GenInst_KeyValuePair_2_t888819835_0_0_0_KeyValuePair_2_t888819835_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t888819835_0_0_0_RuntimeObject_0_0_0_Types[] = { (&KeyValuePair_2_t888819835_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t888819835_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t888819835_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_IntPtr_t_0_0_0_Types[] = { (&IntPtr_t_0_0_0), (&IntPtr_t_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_IntPtr_t_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_IntPtr_t_0_0_0_Types };
static const RuntimeType* GenInst_fsOption_1_t972008496_0_0_0_fsOption_1_t972008496_0_0_0_Types[] = { (&fsOption_1_t972008496_0_0_0), (&fsOption_1_t972008496_0_0_0) };
extern const Il2CppGenericInst GenInst_fsOption_1_t972008496_0_0_0_fsOption_1_t972008496_0_0_0 = { 2, GenInst_fsOption_1_t972008496_0_0_0_fsOption_1_t972008496_0_0_0_Types };
static const RuntimeType* GenInst_fsOption_1_t972008496_0_0_0_RuntimeObject_0_0_0_Types[] = { (&fsOption_1_t972008496_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_fsOption_1_t972008496_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_fsOption_1_t972008496_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2616381142_0_0_0_KeyValuePair_2_t2616381142_0_0_0_Types[] = { (&KeyValuePair_2_t2616381142_0_0_0), (&KeyValuePair_2_t2616381142_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2616381142_0_0_0_KeyValuePair_2_t2616381142_0_0_0 = { 2, GenInst_KeyValuePair_2_t2616381142_0_0_0_KeyValuePair_2_t2616381142_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2616381142_0_0_0_RuntimeObject_0_0_0_Types[] = { (&KeyValuePair_2_t2616381142_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2616381142_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t2616381142_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_HtmlElementFlag_t260274357_0_0_0_HtmlElementFlag_t260274357_0_0_0_Types[] = { (&HtmlElementFlag_t260274357_0_0_0), (&HtmlElementFlag_t260274357_0_0_0) };
extern const Il2CppGenericInst GenInst_HtmlElementFlag_t260274357_0_0_0_HtmlElementFlag_t260274357_0_0_0 = { 2, GenInst_HtmlElementFlag_t260274357_0_0_0_HtmlElementFlag_t260274357_0_0_0_Types };
static const RuntimeType* GenInst_HtmlElementFlag_t260274357_0_0_0_RuntimeObject_0_0_0_Types[] = { (&HtmlElementFlag_t260274357_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_HtmlElementFlag_t260274357_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_HtmlElementFlag_t260274357_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1904647003_0_0_0_KeyValuePair_2_t1904647003_0_0_0_Types[] = { (&KeyValuePair_2_t1904647003_0_0_0), (&KeyValuePair_2_t1904647003_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1904647003_0_0_0_KeyValuePair_2_t1904647003_0_0_0 = { 2, GenInst_KeyValuePair_2_t1904647003_0_0_0_KeyValuePair_2_t1904647003_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1904647003_0_0_0_RuntimeObject_0_0_0_Types[] = { (&KeyValuePair_2_t1904647003_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1904647003_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t1904647003_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { (&Boolean_t3825574718_0_0_0), (&Boolean_t3825574718_0_0_0) };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1174980068_0_0_0_KeyValuePair_2_t1174980068_0_0_0_Types[] = { (&KeyValuePair_2_t1174980068_0_0_0), (&KeyValuePair_2_t1174980068_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1174980068_0_0_0_KeyValuePair_2_t1174980068_0_0_0 = { 2, GenInst_KeyValuePair_2_t1174980068_0_0_0_KeyValuePair_2_t1174980068_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1174980068_0_0_0_RuntimeObject_0_0_0_Types[] = { (&KeyValuePair_2_t1174980068_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1174980068_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t1174980068_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_Char_t3454481338_0_0_0_Char_t3454481338_0_0_0_Types[] = { (&Char_t3454481338_0_0_0), (&Char_t3454481338_0_0_0) };
extern const Il2CppGenericInst GenInst_Char_t3454481338_0_0_0_Char_t3454481338_0_0_0 = { 2, GenInst_Char_t3454481338_0_0_0_Char_t3454481338_0_0_0_Types };
static const RuntimeType* GenInst_Char_t3454481338_0_0_0_RuntimeObject_0_0_0_Types[] = { (&Char_t3454481338_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_Char_t3454481338_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_Char_t3454481338_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t803886688_0_0_0_KeyValuePair_2_t803886688_0_0_0_Types[] = { (&KeyValuePair_2_t803886688_0_0_0), (&KeyValuePair_2_t803886688_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t803886688_0_0_0_KeyValuePair_2_t803886688_0_0_0 = { 2, GenInst_KeyValuePair_2_t803886688_0_0_0_KeyValuePair_2_t803886688_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t803886688_0_0_0_RuntimeObject_0_0_0_Types[] = { (&KeyValuePair_2_t803886688_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t803886688_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t803886688_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3716250094_0_0_0_KeyValuePair_2_t3716250094_0_0_0_Types[] = { (&KeyValuePair_2_t3716250094_0_0_0), (&KeyValuePair_2_t3716250094_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3716250094_0_0_0_KeyValuePair_2_t3716250094_0_0_0 = { 2, GenInst_KeyValuePair_2_t3716250094_0_0_0_KeyValuePair_2_t3716250094_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3716250094_0_0_0_RuntimeObject_0_0_0_Types[] = { (&KeyValuePair_2_t3716250094_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3716250094_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t3716250094_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t38854645_0_0_0_RuntimeObject_0_0_0_Types[] = { (&KeyValuePair_2_t38854645_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t38854645_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t38854645_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1971397920_0_0_0_KeyValuePair_2_t1971397920_0_0_0_Types[] = { (&KeyValuePair_2_t1971397920_0_0_0), (&KeyValuePair_2_t1971397920_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1971397920_0_0_0_KeyValuePair_2_t1971397920_0_0_0 = { 2, GenInst_KeyValuePair_2_t1971397920_0_0_0_KeyValuePair_2_t1971397920_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1971397920_0_0_0_RuntimeObject_0_0_0_Types[] = { (&KeyValuePair_2_t1971397920_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1971397920_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t1971397920_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_SpeechbubbleType_t1311649088_0_0_0_SpeechbubbleType_t1311649088_0_0_0_Types[] = { (&SpeechbubbleType_t1311649088_0_0_0), (&SpeechbubbleType_t1311649088_0_0_0) };
extern const Il2CppGenericInst GenInst_SpeechbubbleType_t1311649088_0_0_0_SpeechbubbleType_t1311649088_0_0_0 = { 2, GenInst_SpeechbubbleType_t1311649088_0_0_0_SpeechbubbleType_t1311649088_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2919672843_0_0_0_KeyValuePair_2_t2919672843_0_0_0_Types[] = { (&KeyValuePair_2_t2919672843_0_0_0), (&KeyValuePair_2_t2919672843_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2919672843_0_0_0_KeyValuePair_2_t2919672843_0_0_0 = { 2, GenInst_KeyValuePair_2_t2919672843_0_0_0_KeyValuePair_2_t2919672843_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2919672843_0_0_0_RuntimeObject_0_0_0_Types[] = { (&KeyValuePair_2_t2919672843_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2919672843_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t2919672843_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_CompressionMethod_t4066553457_0_0_0_CompressionMethod_t4066553457_0_0_0_Types[] = { (&CompressionMethod_t4066553457_0_0_0), (&CompressionMethod_t4066553457_0_0_0) };
extern const Il2CppGenericInst GenInst_CompressionMethod_t4066553457_0_0_0_CompressionMethod_t4066553457_0_0_0 = { 2, GenInst_CompressionMethod_t4066553457_0_0_0_CompressionMethod_t4066553457_0_0_0_Types };
extern const Il2CppGenericInst* const g_Il2CppGenericInstTable[1310] = 
{
	&GenInst_RuntimeObject_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0,
	&GenInst_Char_t3454481338_0_0_0,
	&GenInst_Int64_t909078037_0_0_0,
	&GenInst_UInt32_t2149682021_0_0_0,
	&GenInst_UInt64_t2909196914_0_0_0,
	&GenInst_Byte_t3683104436_0_0_0,
	&GenInst_SByte_t454417549_0_0_0,
	&GenInst_Int16_t4041245914_0_0_0,
	&GenInst_UInt16_t986882611_0_0_0,
	&GenInst_String_t_0_0_0,
	&GenInst_IConvertible_t908092482_0_0_0,
	&GenInst_IComparable_t1857082765_0_0_0,
	&GenInst_IEnumerable_t2911409499_0_0_0,
	&GenInst_ICloneable_t3853279282_0_0_0,
	&GenInst_IComparable_1_t3861059456_0_0_0,
	&GenInst_IEquatable_1_t4233202402_0_0_0,
	&GenInst_Type_t_0_0_0,
	&GenInst_IReflect_t3412036974_0_0_0,
	&GenInst__Type_t102776839_0_0_0,
	&GenInst_MemberInfo_t_0_0_0,
	&GenInst_ICustomAttributeProvider_t502202687_0_0_0,
	&GenInst__MemberInfo_t332722161_0_0_0,
	&GenInst_Double_t4078015681_0_0_0,
	&GenInst_Single_t2076509932_0_0_0,
	&GenInst_Decimal_t724701077_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0,
	&GenInst_Delegate_t3022476291_0_0_0,
	&GenInst_ISerializable_t1245643778_0_0_0,
	&GenInst_ParameterInfo_t2249040075_0_0_0,
	&GenInst__ParameterInfo_t470209990_0_0_0,
	&GenInst_ParameterModifier_t1820634920_0_0_0,
	&GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0,
	&GenInst_EventInfo_t_0_0_0,
	&GenInst__EventInfo_t2430923913_0_0_0,
	&GenInst_FieldInfo_t_0_0_0,
	&GenInst__FieldInfo_t2511231167_0_0_0,
	&GenInst_MethodInfo_t_0_0_0,
	&GenInst__MethodInfo_t3642518830_0_0_0,
	&GenInst_MethodBase_t904190842_0_0_0,
	&GenInst__MethodBase_t1935530873_0_0_0,
	&GenInst_PropertyInfo_t_0_0_0,
	&GenInst__PropertyInfo_t1567586598_0_0_0,
	&GenInst_ConstructorInfo_t2851816542_0_0_0,
	&GenInst__ConstructorInfo_t3269099341_0_0_0,
	&GenInst_IntPtr_t_0_0_0,
	&GenInst_TableRange_t2011406615_0_0_0,
	&GenInst_TailoringInfo_t1449609243_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_KeyValuePair_2_t3716250094_0_0_0,
	&GenInst_Link_t2723257478_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Int32_t2071877448_0_0_0_RuntimeObject_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3716250094_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t1744001932_0_0_0,
	&GenInst_KeyValuePair_2_t1744001932_0_0_0,
	&GenInst_Contraction_t1673853792_0_0_0,
	&GenInst_Level2Map_t3322505726_0_0_0,
	&GenInst_BigInteger_t925946152_0_0_0,
	&GenInst_KeySizes_t3144736271_0_0_0,
	&GenInst_KeyValuePair_2_t38854645_0_0_0,
	&GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0,
	&GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t38854645_0_0_0,
	&GenInst_Slot_t2022531261_0_0_0,
	&GenInst_Slot_t2267560602_0_0_0,
	&GenInst_StackFrame_t2050294881_0_0_0,
	&GenInst_Calendar_t585061108_0_0_0,
	&GenInst_DateTime_t693205669_0_0_0,
	&GenInst_ModuleBuilder_t4156028127_0_0_0,
	&GenInst__ModuleBuilder_t1075102050_0_0_0,
	&GenInst_Module_t4282841206_0_0_0,
	&GenInst__Module_t2144668161_0_0_0,
	&GenInst_ParameterBuilder_t3344728474_0_0_0,
	&GenInst__ParameterBuilder_t2251638747_0_0_0,
	&GenInst_TypeU5BU5D_t1664964607_0_0_0,
	&GenInst_RuntimeArray_0_0_0,
	&GenInst_ICollection_t91669223_0_0_0,
	&GenInst_IList_t3321498491_0_0_0,
	&GenInst_IList_1_t1844743827_0_0_0,
	&GenInst_ICollection_1_t2255878531_0_0_0,
	&GenInst_IEnumerable_1_t1595930271_0_0_0,
	&GenInst_IList_1_t3952977575_0_0_0,
	&GenInst_ICollection_1_t69144983_0_0_0,
	&GenInst_IEnumerable_1_t3704164019_0_0_0,
	&GenInst_IList_1_t643717440_0_0_0,
	&GenInst_ICollection_1_t1054852144_0_0_0,
	&GenInst_IEnumerable_1_t394903884_0_0_0,
	&GenInst_IList_1_t289070565_0_0_0,
	&GenInst_ICollection_1_t700205269_0_0_0,
	&GenInst_IEnumerable_1_t40257009_0_0_0,
	&GenInst_IList_1_t1043143288_0_0_0,
	&GenInst_ICollection_1_t1454277992_0_0_0,
	&GenInst_IEnumerable_1_t794329732_0_0_0,
	&GenInst_IList_1_t873662762_0_0_0,
	&GenInst_ICollection_1_t1284797466_0_0_0,
	&GenInst_IEnumerable_1_t624849206_0_0_0,
	&GenInst_IList_1_t3230389896_0_0_0,
	&GenInst_ICollection_1_t3641524600_0_0_0,
	&GenInst_IEnumerable_1_t2981576340_0_0_0,
	&GenInst_ILTokenInfo_t149559338_0_0_0,
	&GenInst_LabelData_t3712112744_0_0_0,
	&GenInst_LabelFixup_t4090909514_0_0_0,
	&GenInst_GenericTypeParameterBuilder_t1370236603_0_0_0,
	&GenInst_TypeBuilder_t3308873219_0_0_0,
	&GenInst__TypeBuilder_t2783404358_0_0_0,
	&GenInst_MethodBuilder_t644187984_0_0_0,
	&GenInst__MethodBuilder_t3932949077_0_0_0,
	&GenInst_ConstructorBuilder_t700974433_0_0_0,
	&GenInst__ConstructorBuilder_t1236878896_0_0_0,
	&GenInst_PropertyBuilder_t3694255912_0_0_0,
	&GenInst__PropertyBuilder_t3341912621_0_0_0,
	&GenInst_FieldBuilder_t2784804005_0_0_0,
	&GenInst__FieldBuilder_t1895266044_0_0_0,
	&GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0,
	&GenInst_CustomAttributeNamedArgument_t94157543_0_0_0,
	&GenInst_CustomAttributeData_t3093286891_0_0_0,
	&GenInst_ResourceInfo_t3933049236_0_0_0,
	&GenInst_ResourceCacheItem_t333236149_0_0_0,
	&GenInst_IContextProperty_t287246399_0_0_0,
	&GenInst_Header_t2756440555_0_0_0,
	&GenInst_ITrackingHandler_t2759960940_0_0_0,
	&GenInst_IContextAttribute_t2439121372_0_0_0,
	&GenInst_TimeSpan_t3430258949_0_0_0,
	&GenInst_TypeTag_t141209596_0_0_0,
	&GenInst_MonoType_t_0_0_0,
	&GenInst_StrongName_t2988747270_0_0_0,
	&GenInst_IBuiltInEvidence_t1114073477_0_0_0,
	&GenInst_IIdentityPermissionFactory_t2988326850_0_0_0,
	&GenInst_Assembly_t4268412390_0_0_0,
	&GenInst__Assembly_t2937922309_0_0_0,
	&GenInst_DateTimeOffset_t1362988906_0_0_0,
	&GenInst_Guid_t_0_0_0,
	&GenInst_Version_t1755874712_0_0_0,
	&GenInst_NetworkInterface_t63927633_0_0_0,
	&GenInst_IPAddress_t1399971723_0_0_0,
	&GenInst_String_t_0_0_0_LinuxNetworkInterface_t3864470295_0_0_0,
	&GenInst_String_t_0_0_0_LinuxNetworkInterface_t3864470295_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_String_t_0_0_0_LinuxNetworkInterface_t3864470295_0_0_0_KeyValuePair_2_t3536594779_0_0_0,
	&GenInst_KeyValuePair_2_t3536594779_0_0_0,
	&GenInst_LinuxNetworkInterface_t3864470295_0_0_0,
	&GenInst_String_t_0_0_0_LinuxNetworkInterface_t3864470295_0_0_0_LinuxNetworkInterface_t3864470295_0_0_0,
	&GenInst_String_t_0_0_0_MacOsNetworkInterface_t1454185290_0_0_0,
	&GenInst_String_t_0_0_0_MacOsNetworkInterface_t1454185290_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_String_t_0_0_0_MacOsNetworkInterface_t1454185290_0_0_0_KeyValuePair_2_t1126309774_0_0_0,
	&GenInst_KeyValuePair_2_t1126309774_0_0_0,
	&GenInst_MacOsNetworkInterface_t1454185290_0_0_0,
	&GenInst_String_t_0_0_0_MacOsNetworkInterface_t1454185290_0_0_0_MacOsNetworkInterface_t1454185290_0_0_0,
	&GenInst_Win32_IP_ADAPTER_ADDRESSES_t680756680_0_0_0,
	&GenInst_X509ChainStatus_t4278378721_0_0_0,
	&GenInst_ArraySegment_1_t2594217482_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_KeyValuePair_2_t1174980068_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Boolean_t3825574718_0_0_0_RuntimeObject_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Boolean_t3825574718_0_0_0_KeyValuePair_2_t1174980068_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_KeyValuePair_2_t3497699202_0_0_0,
	&GenInst_KeyValuePair_2_t3497699202_0_0_0,
	&GenInst_X509Certificate_t283079845_0_0_0,
	&GenInst_IDeserializationCallback_t327125377_0_0_0,
	&GenInst_Capture_t4157900610_0_0_0,
	&GenInst_Group_t3761430853_0_0_0,
	&GenInst_Mark_t2724874473_0_0_0,
	&GenInst_UriScheme_t1876590943_0_0_0,
	&GenInst_BigInteger_t925946153_0_0_0,
	&GenInst_ByteU5BU5D_t3397334013_0_0_0,
	&GenInst_IList_1_t4224045037_0_0_0,
	&GenInst_ICollection_1_t340212445_0_0_0,
	&GenInst_IEnumerable_1_t3975231481_0_0_0,
	&GenInst_ClientCertificateType_t4001384466_0_0_0,
	&GenInst_Link_t865133271_0_0_0,
	&GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0,
	&GenInst_OscPacketReceivedEventArgs_t3282045269_0_0_0,
	&GenInst_OscBundleReceivedEventArgs_t3673224441_0_0_0,
	&GenInst_ExceptionEventArgs_t892713536_0_0_0,
	&GenInst_OscMessageReceivedEventArgs_t1263041860_0_0_0,
	&GenInst_XPathNavigator_t3981235968_0_0_0,
	&GenInst_IXPathNavigable_t845515791_0_0_0,
	&GenInst_IXmlNamespaceResolver_t3928241465_0_0_0,
	&GenInst_XPathItem_t3130801258_0_0_0,
	&GenInst_XPathResultType_t1521569578_0_0_0,
	&GenInst_Entry_t2583369454_0_0_0,
	&GenInst_NsDecl_t3210081295_0_0_0,
	&GenInst_Object_t1021602117_0_0_0,
	&GenInst_Camera_t189460977_0_0_0,
	&GenInst_Behaviour_t955675639_0_0_0,
	&GenInst_Component_t3819376471_0_0_0,
	&GenInst_Display_t3666191348_0_0_0,
	&GenInst_GradientColorKey_t1677506814_0_0_0,
	&GenInst_GradientAlphaKey_t648783245_0_0_0,
	&GenInst_Keyframe_t1449471340_0_0_0,
	&GenInst_Vector3_t2243707580_0_0_0,
	&GenInst_Vector4_t2243707581_0_0_0,
	&GenInst_Vector2_t2243707579_0_0_0,
	&GenInst_Color32_t874517518_0_0_0,
	&GenInst_Playable_t1896841784_0_0_0,
	&GenInst_PlayableOutput_t988259697_0_0_0,
	&GenInst_Scene_t1684909666_0_0_0_LoadSceneMode_t2981886439_0_0_0,
	&GenInst_Scene_t1684909666_0_0_0,
	&GenInst_Scene_t1684909666_0_0_0_Scene_t1684909666_0_0_0,
	&GenInst_SpriteAtlas_t3132429450_0_0_0,
	&GenInst_Particle_t250075699_0_0_0,
	&GenInst_ContactPoint_t1376425630_0_0_0,
	&GenInst_RaycastHit_t87180320_0_0_0,
	&GenInst_Rigidbody2D_t502193897_0_0_0,
	&GenInst_RaycastHit2D_t4063908774_0_0_0,
	&GenInst_ContactPoint2D_t3659330976_0_0_0,
	&GenInst_AudioClipPlayable_t192218916_0_0_0,
	&GenInst_AudioMixerPlayable_t1831808911_0_0_0,
	&GenInst_WebCamDevice_t3983871389_0_0_0,
	&GenInst_AnimationClipPlayable_t4099382200_0_0_0,
	&GenInst_AnimationLayerMixerPlayable_t3057952312_0_0_0,
	&GenInst_AnimationMixerPlayable_t1343787797_0_0_0,
	&GenInst_AnimationOffsetPlayable_t1019600543_0_0_0,
	&GenInst_AnimatorControllerPlayable_t1744083903_0_0_0,
	&GenInst_AnimatorClipInfo_t3905751349_0_0_0,
	&GenInst_AnimatorControllerParameter_t1381019216_0_0_0,
	&GenInst_UIVertex_t1204258818_0_0_0,
	&GenInst_UICharInfo_t3056636800_0_0_0,
	&GenInst_UILineInfo_t3621277874_0_0_0,
	&GenInst_Font_t4239498691_0_0_0,
	&GenInst_GUILayoutOption_t4183744904_0_0_0,
	&GenInst_GUILayoutEntry_t3828586629_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_RuntimeObject_0_0_0,
	&GenInst_KeyValuePair_2_t3749587448_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_RuntimeObject_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t3749587448_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_KeyValuePair_2_t4180919198_0_0_0,
	&GenInst_KeyValuePair_2_t4180919198_0_0_0,
	&GenInst_GUIStyle_t1799908754_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_KeyValuePair_2_t1472033238_0_0_0,
	&GenInst_KeyValuePair_2_t1472033238_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_GUIStyle_t1799908754_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_IntPtr_t_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Exception_t1927440687_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_String_t_0_0_0_String_t_0_0_0,
	&GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_String_t_0_0_0_String_t_0_0_0_KeyValuePair_2_t1701344717_0_0_0,
	&GenInst_KeyValuePair_2_t1701344717_0_0_0,
	&GenInst_UserProfile_t3365630962_0_0_0,
	&GenInst_IUserProfile_t4108565527_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0_String_t_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0_RuntimeObject_0_0_0,
	&GenInst_AchievementDescription_t3110978151_0_0_0,
	&GenInst_IAchievementDescription_t3498529102_0_0_0,
	&GenInst_GcLeaderboard_t453887929_0_0_0,
	&GenInst_IAchievementDescriptionU5BU5D_t4083280315_0_0_0,
	&GenInst_IAchievementU5BU5D_t2709554645_0_0_0,
	&GenInst_IAchievement_t1752291260_0_0_0,
	&GenInst_GcAchievementData_t1754866149_0_0_0,
	&GenInst_Achievement_t1333316625_0_0_0,
	&GenInst_IScoreU5BU5D_t3237304636_0_0_0,
	&GenInst_IScore_t513966369_0_0_0,
	&GenInst_GcScoreData_t3676783238_0_0_0,
	&GenInst_Score_t2307748940_0_0_0,
	&GenInst_IUserProfileU5BU5D_t3461248430_0_0_0,
	&GenInst_DisallowMultipleComponent_t2656950_0_0_0,
	&GenInst_Attribute_t542643598_0_0_0,
	&GenInst__Attribute_t1557664299_0_0_0,
	&GenInst_ExecuteInEditMode_t3043633143_0_0_0,
	&GenInst_RequireComponent_t864575032_0_0_0,
	&GenInst_HitInfo_t1761367055_0_0_0,
	&GenInst_PersistentCall_t3793436469_0_0_0,
	&GenInst_BaseInvokableCall_t2229564840_0_0_0,
	&GenInst_WorkRequest_t1154022482_0_0_0,
	&GenInst_PlayableBinding_t2498078091_0_0_0,
	&GenInst_MessageTypeSubscribers_t2291506050_0_0_0,
	&GenInst_MessageTypeSubscribers_t2291506050_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_MessageEventArgs_t301283622_0_0_0,
	&GenInst_IntPtr_t_0_0_0_WeakReference_t1077405567_0_0_0,
	&GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0,
	&GenInst_KeyValuePair_2_t888819835_0_0_0,
	&GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_IntPtr_t_0_0_0,
	&GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0,
	&GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t888819835_0_0_0,
	&GenInst_IntPtr_t_0_0_0_WeakReference_t1077405567_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_IntPtr_t_0_0_0_WeakReference_t1077405567_0_0_0_KeyValuePair_2_t3571743403_0_0_0,
	&GenInst_KeyValuePair_2_t3571743403_0_0_0,
	&GenInst_List_1_t61287617_0_0_0,
	&GenInst_DispatcherKey_t708950850_0_0_0_Dispatcher_t2240407071_0_0_0,
	&GenInst_DispatcherKey_t708950850_0_0_0,
	&GenInst_DispatcherKey_t708950850_0_0_0_Dispatcher_t2240407071_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_DispatcherKey_t708950850_0_0_0_Dispatcher_t2240407071_0_0_0_KeyValuePair_2_t3846770086_0_0_0,
	&GenInst_KeyValuePair_2_t3846770086_0_0_0,
	&GenInst_HtmlNode_t2048434459_0_0_0,
	&GenInst_String_t_0_0_0_HtmlElementFlag_t260274357_0_0_0,
	&GenInst_RuntimeObject_0_0_0_HtmlElementFlag_t260274357_0_0_0,
	&GenInst_KeyValuePair_2_t1904647003_0_0_0,
	&GenInst_HtmlElementFlag_t260274357_0_0_0,
	&GenInst_RuntimeObject_0_0_0_HtmlElementFlag_t260274357_0_0_0_RuntimeObject_0_0_0,
	&GenInst_RuntimeObject_0_0_0_HtmlElementFlag_t260274357_0_0_0_HtmlElementFlag_t260274357_0_0_0,
	&GenInst_RuntimeObject_0_0_0_HtmlElementFlag_t260274357_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_RuntimeObject_0_0_0_HtmlElementFlag_t260274357_0_0_0_KeyValuePair_2_t1904647003_0_0_0,
	&GenInst_String_t_0_0_0_HtmlElementFlag_t260274357_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_String_t_0_0_0_HtmlElementFlag_t260274357_0_0_0_KeyValuePair_2_t4227366137_0_0_0,
	&GenInst_KeyValuePair_2_t4227366137_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_HtmlNode_t2048434459_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_HtmlNode_t2048434459_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_HtmlNode_t2048434459_0_0_0_KeyValuePair_2_t3108572612_0_0_0,
	&GenInst_KeyValuePair_2_t3108572612_0_0_0,
	&GenInst_HtmlAttribute_t1804523403_0_0_0,
	&GenInst_String_t_0_0_0_HtmlNode_t2048434459_0_0_0,
	&GenInst_String_t_0_0_0_HtmlNode_t2048434459_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_String_t_0_0_0_HtmlNode_t2048434459_0_0_0_KeyValuePair_2_t1720558943_0_0_0,
	&GenInst_KeyValuePair_2_t1720558943_0_0_0,
	&GenInst_String_t_0_0_0_HtmlAttribute_t1804523403_0_0_0,
	&GenInst_String_t_0_0_0_HtmlAttribute_t1804523403_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_String_t_0_0_0_HtmlAttribute_t1804523403_0_0_0_KeyValuePair_2_t1476647887_0_0_0,
	&GenInst_KeyValuePair_2_t1476647887_0_0_0,
	&GenInst_String_t_0_0_0_HtmlAttribute_t1804523403_0_0_0_HtmlAttribute_t1804523403_0_0_0,
	&GenInst_HtmlParseError_t1115179162_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_HtmlNode_t2048434459_0_0_0_HtmlNode_t2048434459_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t1070465849_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t1070465849_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t1070465849_0_0_0_KeyValuePair_2_t742590333_0_0_0,
	&GenInst_KeyValuePair_2_t742590333_0_0_0,
	&GenInst_TuioBlobEventArgs_t3562978179_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_TuioBlob_t2046943414_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_TuioBlob_t2046943414_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_TuioBlob_t2046943414_0_0_0_KeyValuePair_2_t3107081567_0_0_0,
	&GenInst_KeyValuePair_2_t3107081567_0_0_0,
	&GenInst_TuioBlob_t2046943414_0_0_0,
	&GenInst_TuioCursorEventArgs_t362990012_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_TuioCursor_t1850351419_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_TuioCursor_t1850351419_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_TuioCursor_t1850351419_0_0_0_KeyValuePair_2_t2910489572_0_0_0,
	&GenInst_KeyValuePair_2_t2910489572_0_0_0,
	&GenInst_TuioCursor_t1850351419_0_0_0,
	&GenInst_TuioObjectEventArgs_t1810743341_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_TuioObject_t1236821014_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_TuioObject_t1236821014_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_TuioObject_t1236821014_0_0_0_KeyValuePair_2_t2296959167_0_0_0,
	&GenInst_KeyValuePair_2_t2296959167_0_0_0,
	&GenInst_TuioObject_t1236821014_0_0_0,
	&GenInst_IDataProcessor_t2214055399_0_0_0,
	&GenInst_BaseInputModule_t1295781545_0_0_0,
	&GenInst_RaycastResult_t21186376_0_0_0,
	&GenInst_IDeselectHandler_t3182198310_0_0_0,
	&GenInst_IEventSystemHandler_t2741188318_0_0_0,
	&GenInst_List_1_t2110309450_0_0_0,
	&GenInst_List_1_t2058570427_0_0_0,
	&GenInst_List_1_t3188497603_0_0_0,
	&GenInst_ISelectHandler_t2812555161_0_0_0,
	&GenInst_BaseRaycaster_t2336171397_0_0_0,
	&GenInst_Entry_t3365010046_0_0_0,
	&GenInst_BaseEventData_t2681005625_0_0_0,
	&GenInst_IPointerEnterHandler_t193164956_0_0_0,
	&GenInst_IPointerExitHandler_t461019860_0_0_0,
	&GenInst_IPointerDownHandler_t3929046918_0_0_0,
	&GenInst_IPointerUpHandler_t1847764461_0_0_0,
	&GenInst_IPointerClickHandler_t96169666_0_0_0,
	&GenInst_IInitializePotentialDragHandler_t3350809087_0_0_0,
	&GenInst_IBeginDragHandler_t3135127860_0_0_0,
	&GenInst_IDragHandler_t2583993319_0_0_0,
	&GenInst_IEndDragHandler_t1349123600_0_0_0,
	&GenInst_IDropHandler_t2390101210_0_0_0,
	&GenInst_IScrollHandler_t3834677510_0_0_0,
	&GenInst_IUpdateSelectedHandler_t3778909353_0_0_0,
	&GenInst_IMoveHandler_t2611925506_0_0_0,
	&GenInst_ISubmitHandler_t525803901_0_0_0,
	&GenInst_ICancelHandler_t1980319651_0_0_0,
	&GenInst_Transform_t3275118058_0_0_0,
	&GenInst_GameObject_t1756533147_0_0_0,
	&GenInst_BaseInput_t621514313_0_0_0,
	&GenInst_UIBehaviour_t3960014691_0_0_0,
	&GenInst_MonoBehaviour_t1158329972_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_KeyValuePair_2_t2659922876_0_0_0,
	&GenInst_KeyValuePair_2_t2659922876_0_0_0,
	&GenInst_PointerEventData_t1599784723_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_PointerEventData_t1599784723_0_0_0,
	&GenInst_ButtonState_t2688375492_0_0_0,
	&GenInst_Color_t2020392075_0_0_0,
	&GenInst_ICanvasElement_t986520779_0_0_0,
	&GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_ColorBlock_t2652774230_0_0_0,
	&GenInst_OptionData_t2420267500_0_0_0,
	&GenInst_DropdownItem_t4139978805_0_0_0,
	&GenInst_FloatTween_t2986189219_0_0_0,
	&GenInst_Sprite_t309593783_0_0_0,
	&GenInst_Canvas_t209405766_0_0_0,
	&GenInst_List_1_t3873494194_0_0_0,
	&GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0,
	&GenInst_Text_t356221433_0_0_0,
	&GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0_KeyValuePair_2_t850112849_0_0_0,
	&GenInst_KeyValuePair_2_t850112849_0_0_0,
	&GenInst_ColorTween_t3438117476_0_0_0,
	&GenInst_Graphic_t2426225576_0_0_0,
	&GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0,
	&GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_KeyValuePair_2_t2391682566_0_0_0,
	&GenInst_KeyValuePair_2_t2391682566_0_0_0,
	&GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3010968081_0_0_0,
	&GenInst_KeyValuePair_2_t3010968081_0_0_0,
	&GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t1912381698_0_0_0,
	&GenInst_KeyValuePair_2_t1912381698_0_0_0,
	&GenInst_Type_t3352948571_0_0_0,
	&GenInst_FillMethod_t1640962579_0_0_0,
	&GenInst_ContentType_t1028629049_0_0_0,
	&GenInst_LineType_t2931319356_0_0_0,
	&GenInst_InputType_t1274231802_0_0_0,
	&GenInst_TouchScreenKeyboardType_t875112366_0_0_0,
	&GenInst_CharacterValidation_t3437478890_0_0_0,
	&GenInst_Mask_t2977958238_0_0_0,
	&GenInst_List_1_t2347079370_0_0_0,
	&GenInst_RectMask2D_t1156185964_0_0_0,
	&GenInst_List_1_t525307096_0_0_0,
	&GenInst_Navigation_t1571958496_0_0_0,
	&GenInst_IClippable_t1941276057_0_0_0,
	&GenInst_Direction_t3696775921_0_0_0,
	&GenInst_Selectable_t1490392188_0_0_0,
	&GenInst_Transition_t605142169_0_0_0,
	&GenInst_SpriteState_t1353336012_0_0_0,
	&GenInst_CanvasGroup_t3296560743_0_0_0,
	&GenInst_Direction_t1525323322_0_0_0,
	&GenInst_MatEntry_t3157325053_0_0_0,
	&GenInst_Toggle_t3976754468_0_0_0,
	&GenInst_Toggle_t3976754468_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_IClipper_t900477982_0_0_0,
	&GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t379984643_0_0_0,
	&GenInst_KeyValuePair_2_t379984643_0_0_0,
	&GenInst_AspectMode_t1166448724_0_0_0,
	&GenInst_FitMode_t4030874534_0_0_0,
	&GenInst_RectTransform_t3349966182_0_0_0,
	&GenInst_LayoutRebuilder_t2155218138_0_0_0,
	&GenInst_ILayoutElement_t1975293769_0_0_0_Single_t2076509932_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Single_t2076509932_0_0_0,
	&GenInst_List_1_t1612828712_0_0_0,
	&GenInst_List_1_t243638650_0_0_0,
	&GenInst_List_1_t1612828711_0_0_0,
	&GenInst_List_1_t1612828713_0_0_0,
	&GenInst_List_1_t1440998580_0_0_0,
	&GenInst_List_1_t573379950_0_0_0,
	&GenInst_ARHitTestResult_t3275513025_0_0_0,
	&GenInst_ARPlaneAnchorGameObject_t2305225887_0_0_0,
	&GenInst_ARHitTestResultType_t3616749745_0_0_0,
	&GenInst_UnityARSessionRunOption_t3123075684_0_0_0,
	&GenInst_UnityARAlignment_t2379988631_0_0_0,
	&GenInst_UnityARPlaneDetection_t612575857_0_0_0,
	&GenInst_String_t_0_0_0_ARPlaneAnchorGameObject_t2305225887_0_0_0,
	&GenInst_String_t_0_0_0_ARPlaneAnchorGameObject_t2305225887_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_String_t_0_0_0_ARPlaneAnchorGameObject_t2305225887_0_0_0_KeyValuePair_2_t1977350371_0_0_0,
	&GenInst_KeyValuePair_2_t1977350371_0_0_0,
	&GenInst_ParticleSystem_t3394631041_0_0_0,
	&GenInst_Single_t2076509932_0_0_0_Single_t2076509932_0_0_0_Single_t2076509932_0_0_0,
	&GenInst_Single_t2076509932_0_0_0_Single_t2076509932_0_0_0,
	&GenInst_Animator_t69676727_0_0_0,
	&GenInst_Concept_t2322716066_0_0_0,
	&GenInst_Date_t2577429686_0_0_0,
	&GenInst_Entity_t2757698109_0_0_0,
	&GenInst_Feed_t2248050640_0_0_0,
	&GenInst_Keyword_t3384522415_0_0_0,
	&GenInst_Microformat_t171559559_0_0_0,
	&GenInst_Relation_t2336466576_0_0_0,
	&GenInst_TargetedSentiment_t494351509_0_0_0,
	&GenInst_Taxonomy_t1527601757_0_0_0,
	&GenInst_ImageKeyword_t1009764024_0_0_0,
	&GenInst_DocEmotions_t3627397418_0_0_0,
	&GenInst_Intent_t1324785294_0_0_0,
	&GenInst_MessageIntent_t315351975_0_0_0,
	&GenInst_Environment_t3491688169_0_0_0,
	&GenInst_ConfigurationRef_t2639036165_0_0_0,
	&GenInst_CollectionRef_t3248558799_0_0_0,
	&GenInst_QueryResult_t1852366331_0_0_0,
	&GenInst_Metadata_t1932344831_0_0_0,
	&GenInst_AnswerUnit_t3274674984_0_0_0,
	&GenInst_Content_t2184047853_0_0_0,
	&GenInst_Translation_t1796874115_0_0_0,
	&GenInst_Translation_t308652637_0_0_0,
	&GenInst_TraitTreeNode_t694336958_0_0_0,
	&GenInst_TraitTreeNode_t3511443394_0_0_0,
	&GenInst_BehaviorNode_t287073406_0_0_0,
	&GenInst_ConsumptionPreferencesCategoryNode_t38182357_0_0_0,
	&GenInst_ConsumptionPreferencesNode_t1038535837_0_0_0,
	&GenInst_SolrClusterResponse_t1008480713_0_0_0,
	&GenInst_RankerInfoPayload_t1776809245_0_0_0,
	&GenInst_RankedAnswer_t193891511_0_0_0,
	&GenInst_Doc_t3160830270_0_0_0,
	&GenInst_Model_t2340542523_0_0_0,
	&GenInst_SpeechRecognitionResult_t3827468010_0_0_0,
	&GenInst_SpeechRecognitionAlternative_t84095082_0_0_0,
	&GenInst_Customization_t47167237_0_0_0,
	&GenInst_Corpus_t3396664578_0_0_0,
	&GenInst_WordData_t612265514_0_0_0,
	&GenInst_Word_t4274554970_0_0_0,
	&GenInst_Voice_t3646862260_0_0_0,
	&GenInst_Customization_t2261013253_0_0_0,
	&GenInst_Column_t2612486824_0_0_0,
	&GenInst_Option_t1775127045_0_0_0,
	&GenInst_GetClassifiersPerClassifierBrief_t3946924106_0_0_0,
	&GenInst_ClassifyTopLevelSingle_t3733844951_0_0_0,
	&GenInst_ClassifyPerClassifier_t2357796318_0_0_0,
	&GenInst_ClassResult_t3791036973_0_0_0,
	&GenInst_FacesTopLevelSingle_t2140351533_0_0_0,
	&GenInst_OneFaceResult_t2674708326_0_0_0,
	&GenInst_TextRecogTopLevelSingle_t1839648508_0_0_0,
	&GenInst_TextRecogOneWord_t2687074657_0_0_0,
	&GenInst_MenuScene_t1232739575_0_0_0,
	&GenInst_CameraTarget_t552179396_0_0_0,
	&GenInst_String_t_0_0_0_RESTConnector_t3705102247_0_0_0,
	&GenInst_String_t_0_0_0_RESTConnector_t3705102247_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_String_t_0_0_0_RESTConnector_t3705102247_0_0_0_KeyValuePair_2_t3377226731_0_0_0,
	&GenInst_KeyValuePair_2_t3377226731_0_0_0,
	&GenInst_Request_t466816980_0_0_0,
	&GenInst_String_t_0_0_0_RuntimeObject_0_0_0,
	&GenInst_String_t_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_String_t_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t2361573779_0_0_0,
	&GenInst_KeyValuePair_2_t2361573779_0_0_0,
	&GenInst_String_t_0_0_0_Form_t779275863_0_0_0,
	&GenInst_String_t_0_0_0_Form_t779275863_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_String_t_0_0_0_Form_t779275863_0_0_0_KeyValuePair_2_t451400347_0_0_0,
	&GenInst_KeyValuePair_2_t451400347_0_0_0,
	&GenInst_Message_t2515021790_0_0_0,
	&GenInst_CloseEventArgs_t344507773_0_0_0,
	&GenInst_ErrorEventArgs_t502222999_0_0_0,
	&GenInst_MessageEventArgs_t2890051726_0_0_0,
	&GenInst_DebugInfo_t379980446_0_0_0,
	&GenInst_QualityManager_t3466824526_0_0_0,
	&GenInst_LogSystem_t3371271695_0_0_0,
	&GenInst_ILogReactor_t2178507087_0_0_0,
	&GenInst_Quotation_t2638841872_0_0_0,
	&GenInst_Docs_t1723806121_0_0_0,
	&GenInst_EnrichedTitle_t2959558712_0_0_0,
	&GenInst_EntityResponse_t2137526154_0_0_0,
	&GenInst_LogMessageResponse_t3273544838_0_0_0,
	&GenInst_Workspace_t1018343827_0_0_0,
	&GenInst_EntityExample_t3008277273_0_0_0,
	&GenInst_Intent_t3642575974_0_0_0,
	&GenInst_Example_t1927362902_0_0_0,
	&GenInst_Entity_t4255761867_0_0_0,
	&GenInst_DialogNode_t2015514522_0_0_0,
	&GenInst_Synonym_t1752818203_0_0_0,
	&GenInst_SynSet_t2414122624_0_0_0,
	&GenInst_Evidence_t859225293_0_0_0,
	&GenInst_Table_t205705827_0_0_0,
	&GenInst_Row_t3628585283_0_0_0,
	&GenInst_Cell_t3725717219_0_0_0,
	&GenInst_Word_t3826420310_0_0_0,
	&GenInst_Value_t2642671545_0_0_0,
	&GenInst_SynonymList_t1745302257_0_0_0,
	&GenInst_ParseTree_t2821719349_0_0_0,
	&GenInst_Answer_t3626344892_0_0_0,
	&GenInst_QuestionClass_t734245290_0_0_0,
	&GenInst_Response_t4265902649_0_0_0,
	&GenInst_Notice_t1592579614_0_0_0,
	&GenInst_Enrichment_t533139721_0_0_0,
	&GenInst_NormalizationOperation_t3873208172_0_0_0,
	&GenInst_FontSetting_t3267780181_0_0_0,
	&GenInst_WordStyle_t1557885949_0_0_0,
	&GenInst_Field_t3865657682_0_0_0,
	&GenInst_Warning_t1238678040_0_0_0,
	&GenInst_IWatsonService_t2784709738_0_0_0,
	&GenInst_Language_t834895248_0_0_0,
	&GenInst_TranslationModel_t4027325182_0_0_0,
	&GenInst_Language_t2171379860_0_0_0,
	&GenInst_TranslationModel_t2730767334_0_0_0,
	&GenInst_Classifier_t1414011573_0_0_0,
	&GenInst_Class_t2988469534_0_0_0,
	&GenInst_String_t_0_0_0_DataCache_t4250340070_0_0_0,
	&GenInst_String_t_0_0_0_DataCache_t4250340070_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_String_t_0_0_0_DataCache_t4250340070_0_0_0_KeyValuePair_2_t3922464554_0_0_0,
	&GenInst_KeyValuePair_2_t3922464554_0_0_0,
	&GenInst_Warning_t514099562_0_0_0,
	&GenInst_ContentItem_t1958644056_0_0_0,
	&GenInst_Warning_t31395408_0_0_0,
	&GenInst_CollectionsResponse_t1627544196_0_0_0,
	&GenInst_TimeStamp_t82276870_0_0_0,
	&GenInst_WordConfidence_t2757029394_0_0_0,
	&GenInst_KeywordResult_t2819026148_0_0_0,
	&GenInst_WordAlternativeResult_t3127007908_0_0_0,
	&GenInst_JobStatus_t2358384683_0_0_0,
	&GenInst_Word_t3204175642_0_0_0,
	&GenInst_AudioData_t3894398524_0_0_0,
	&GenInst_VoiceType_t981025524_0_0_0_String_t_0_0_0,
	&GenInst_VoiceType_t981025524_0_0_0_RuntimeObject_0_0_0,
	&GenInst_KeyValuePair_2_t4282424860_0_0_0,
	&GenInst_VoiceType_t981025524_0_0_0,
	&GenInst_VoiceType_t981025524_0_0_0_RuntimeObject_0_0_0_VoiceType_t981025524_0_0_0,
	&GenInst_VoiceType_t981025524_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0,
	&GenInst_VoiceType_t981025524_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_VoiceType_t981025524_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t4282424860_0_0_0,
	&GenInst_VoiceType_t981025524_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_VoiceType_t981025524_0_0_0_String_t_0_0_0_KeyValuePair_2_t3622195798_0_0_0,
	&GenInst_KeyValuePair_2_t3622195798_0_0_0,
	&GenInst_AudioFormatType_t2756784245_0_0_0_String_t_0_0_0,
	&GenInst_AudioFormatType_t2756784245_0_0_0_RuntimeObject_0_0_0,
	&GenInst_KeyValuePair_2_t993946999_0_0_0,
	&GenInst_AudioFormatType_t2756784245_0_0_0,
	&GenInst_AudioFormatType_t2756784245_0_0_0_RuntimeObject_0_0_0_AudioFormatType_t2756784245_0_0_0,
	&GenInst_AudioFormatType_t2756784245_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0,
	&GenInst_AudioFormatType_t2756784245_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_AudioFormatType_t2756784245_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t993946999_0_0_0,
	&GenInst_AudioFormatType_t2756784245_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_AudioFormatType_t2756784245_0_0_0_String_t_0_0_0_KeyValuePair_2_t333717937_0_0_0,
	&GenInst_KeyValuePair_2_t333717937_0_0_0,
	&GenInst_SentenceTone_t3234952115_0_0_0,
	&GenInst_ToneCategory_t3724297374_0_0_0,
	&GenInst_Tone_t1138009090_0_0_0,
	&GenInst_Solution_t204511443_0_0_0,
	&GenInst_Anchor_t1470404909_0_0_0,
	&GenInst_Node_t3986951550_0_0_0,
	&GenInst_WarningInfo_t624028094_0_0_0,
	&GenInst_Class_t4054986160_0_0_0,
	&GenInst_CreateCollection_t3312638156_0_0_0,
	&GenInst_GetCollectionsBrief_t2166116487_0_0_0,
	&GenInst_CollectionImagesConfig_t1273369062_0_0_0,
	&GenInst_SimilarImageConfig_t3734626070_0_0_0,
	&GenInst_String_t_0_0_0_ByteU5BU5D_t3397334013_0_0_0,
	&GenInst_String_t_0_0_0_ByteU5BU5D_t3397334013_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_String_t_0_0_0_ByteU5BU5D_t3397334013_0_0_0_KeyValuePair_2_t3069458497_0_0_0,
	&GenInst_KeyValuePair_2_t3069458497_0_0_0,
	&GenInst_UnitTestManager_t2486317649_0_0_0,
	&GenInst_AudioClip_t1932558630_0_0_0,
	&GenInst_Config_t3637807320_0_0_0,
	&GenInst_CredentialInfo_t34154441_0_0_0,
	&GenInst_Variable_t437234954_0_0_0,
	&GenInst_String_t_0_0_0_CacheItem_t2472383731_0_0_0,
	&GenInst_String_t_0_0_0_CacheItem_t2472383731_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_String_t_0_0_0_CacheItem_t2472383731_0_0_0_KeyValuePair_2_t2144508215_0_0_0,
	&GenInst_KeyValuePair_2_t2144508215_0_0_0,
	&GenInst_CacheItem_t2472383731_0_0_0,
	&GenInst_EventManager_t605335149_0_0_0,
	&GenInst_Type_t_0_0_0_Dictionary_2_t1621280361_0_0_0,
	&GenInst_RuntimeObject_0_0_0_String_t_0_0_0,
	&GenInst_RuntimeObject_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_RuntimeObject_0_0_0_String_t_0_0_0_KeyValuePair_2_t3673592879_0_0_0,
	&GenInst_KeyValuePair_2_t3673592879_0_0_0,
	&GenInst_Type_t_0_0_0_Dictionary_2_t1621280361_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Type_t_0_0_0_Dictionary_2_t1621280361_0_0_0_KeyValuePair_2_t1315983480_0_0_0,
	&GenInst_KeyValuePair_2_t1315983480_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t3606228803_0_0_0,
	&GenInst_OnReceiveEvent_t4237107671_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t3606228803_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t3606228803_0_0_0_KeyValuePair_2_t3278353287_0_0_0,
	&GenInst_KeyValuePair_2_t3278353287_0_0_0,
	&GenInst_AsyncEvent_t2332674247_0_0_0,
	&GenInst_KeyEventManager_t3314674286_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_String_t_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_String_t_0_0_0_KeyValuePair_2_t3089358386_0_0_0,
	&GenInst_KeyValuePair_2_t3089358386_0_0_0,
	&GenInst_Runnable_t1422227699_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Routine_t1973058165_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Routine_t1973058165_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Routine_t1973058165_0_0_0_KeyValuePair_2_t3033196318_0_0_0,
	&GenInst_KeyValuePair_2_t3033196318_0_0_0,
	&GenInst_Routine_t1973058165_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_List_1_t1441188381_0_0_0,
	&GenInst_TouchEventData_t2072067249_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_List_1_t1441188381_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_List_1_t1441188381_0_0_0_KeyValuePair_2_t2501326534_0_0_0,
	&GenInst_KeyValuePair_2_t2501326534_0_0_0,
	&GenInst_EventArgs_t3289624707_0_0_0,
	&GenInst_Collider_t3497673348_0_0_0,
	&GenInst_Collider2D_t646061738_0_0_0,
	&GenInst_Mapping_t2602146240_0_0_0,
	&GenInst_Mapping_t1442940263_0_0_0,
	&GenInst_Mapping_t3124476604_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t1398341365_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t1398341365_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t1398341365_0_0_0_KeyValuePair_2_t1070465850_0_0_0,
	&GenInst_KeyValuePair_2_t1070465850_0_0_0,
	&GenInst_ClassEventMapping_t3594451529_0_0_0,
	&GenInst_List_1_t3743315550_0_0_0,
	&GenInst_List_1_t3738683066_0_0_0,
	&GenInst_Speech_t1663141205_0_0_0,
	&GenInst_TapEventMapping_t2308161515_0_0_0,
	&GenInst_FullScreenDragEventMapping_t3872427665_0_0_0,
	&GenInst_Input_t3157785889_0_0_0,
	&GenInst_Output_t220081548_0_0_0,
	&GenInst_Widget_t3624771850_0_0_0,
	&GenInst_Connection_t2486334317_0_0_0,
	&GenInst_fsData_t2583805605_0_0_0,
	&GenInst_String_t_0_0_0_fsData_t2583805605_0_0_0,
	&GenInst_String_t_0_0_0_fsData_t2583805605_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_String_t_0_0_0_fsData_t2583805605_0_0_0_KeyValuePair_2_t2255930089_0_0_0,
	&GenInst_KeyValuePair_2_t2255930089_0_0_0,
	&GenInst_fsDataType_t1645355485_0_0_0,
	&GenInst_fsMetaProperty_t2249223145_0_0_0,
	&GenInst_AnimationCurve_t3306541151_0_0_0,
	&GenInst_Bounds_t3033363703_0_0_0,
	&GenInst_Gradient_t3600583008_0_0_0,
	&GenInst_LayerMask_t3188175821_0_0_0,
	&GenInst_Rect_t3681755626_0_0_0,
	&GenInst_Type_t_0_0_0_String_t_0_0_0,
	&GenInst_Type_t_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Type_t_0_0_0_String_t_0_0_0_KeyValuePair_2_t1723923352_0_0_0,
	&GenInst_KeyValuePair_2_t1723923352_0_0_0,
	&GenInst_AotCompilation_t21908242_0_0_0,
	&GenInst_fsDataType_t1645355485_0_0_0_String_t_0_0_0,
	&GenInst_fsDataType_t1645355485_0_0_0_RuntimeObject_0_0_0,
	&GenInst_Type_t_0_0_0_RuntimeObject_0_0_0,
	&GenInst_Type_t_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Type_t_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t2384152414_0_0_0,
	&GenInst_KeyValuePair_2_t2384152414_0_0_0,
	&GenInst_String_t_0_0_0_fsData_t2583805605_0_0_0_String_t_0_0_0,
	&GenInst_Type_t_0_0_0_fsBaseConverter_t1241677426_0_0_0,
	&GenInst_Type_t_0_0_0_fsBaseConverter_t1241677426_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Type_t_0_0_0_fsBaseConverter_t1241677426_0_0_0_KeyValuePair_2_t936380545_0_0_0,
	&GenInst_KeyValuePair_2_t936380545_0_0_0,
	&GenInst_Type_t_0_0_0_List_1_t2055375476_0_0_0,
	&GenInst_fsObjectProcessor_t2686254344_0_0_0,
	&GenInst_Type_t_0_0_0_List_1_t2055375476_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Type_t_0_0_0_List_1_t2055375476_0_0_0_KeyValuePair_2_t1750078595_0_0_0,
	&GenInst_KeyValuePair_2_t1750078595_0_0_0,
	&GenInst_fsConverter_t466758137_0_0_0,
	&GenInst_Type_t_0_0_0_fsDirectConverter_t763460818_0_0_0,
	&GenInst_Type_t_0_0_0_fsDirectConverter_t763460818_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Type_t_0_0_0_fsDirectConverter_t763460818_0_0_0_KeyValuePair_2_t458163937_0_0_0,
	&GenInst_KeyValuePair_2_t458163937_0_0_0,
	&GenInst_fsVersionedType_t654750358_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_fsData_t2583805605_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_fsData_t2583805605_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_fsData_t2583805605_0_0_0_KeyValuePair_2_t3643943758_0_0_0,
	&GenInst_KeyValuePair_2_t3643943758_0_0_0,
	&GenInst_Link_t247561424_0_0_0,
	&GenInst_AttributeQuery_t604298480_0_0_0_Attribute_t542643598_0_0_0,
	&GenInst_AttributeQuery_t604298480_0_0_0_RuntimeObject_0_0_0,
	&GenInst_KeyValuePair_2_t288805040_0_0_0,
	&GenInst_AttributeQuery_t604298480_0_0_0,
	&GenInst_AttributeQuery_t604298480_0_0_0_RuntimeObject_0_0_0_AttributeQuery_t604298480_0_0_0,
	&GenInst_AttributeQuery_t604298480_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0,
	&GenInst_AttributeQuery_t604298480_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_AttributeQuery_t604298480_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t288805040_0_0_0,
	&GenInst_AttributeQuery_t604298480_0_0_0_Attribute_t542643598_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_AttributeQuery_t604298480_0_0_0_Attribute_t542643598_0_0_0_KeyValuePair_2_t2436966639_0_0_0,
	&GenInst_KeyValuePair_2_t2436966639_0_0_0,
	&GenInst_Type_t_0_0_0_fsOption_1_t972008496_0_0_0,
	&GenInst_RuntimeObject_0_0_0_fsOption_1_t972008496_0_0_0,
	&GenInst_KeyValuePair_2_t2616381142_0_0_0,
	&GenInst_fsOption_1_t972008496_0_0_0,
	&GenInst_RuntimeObject_0_0_0_fsOption_1_t972008496_0_0_0_RuntimeObject_0_0_0,
	&GenInst_RuntimeObject_0_0_0_fsOption_1_t972008496_0_0_0_fsOption_1_t972008496_0_0_0,
	&GenInst_RuntimeObject_0_0_0_fsOption_1_t972008496_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_RuntimeObject_0_0_0_fsOption_1_t972008496_0_0_0_KeyValuePair_2_t2616381142_0_0_0,
	&GenInst_Type_t_0_0_0_fsOption_1_t972008496_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Type_t_0_0_0_fsOption_1_t972008496_0_0_0_KeyValuePair_2_t666711615_0_0_0,
	&GenInst_KeyValuePair_2_t666711615_0_0_0,
	&GenInst_String_t_0_0_0_Type_t_0_0_0,
	&GenInst_String_t_0_0_0_Type_t_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_String_t_0_0_0_Type_t_0_0_0_KeyValuePair_2_t975927710_0_0_0,
	&GenInst_KeyValuePair_2_t975927710_0_0_0,
	&GenInst_Type_t_0_0_0_fsMetaType_t3266798926_0_0_0,
	&GenInst_Type_t_0_0_0_fsMetaType_t3266798926_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Type_t_0_0_0_fsMetaType_t3266798926_0_0_0_KeyValuePair_2_t2961502045_0_0_0,
	&GenInst_KeyValuePair_2_t2961502045_0_0_0,
	&GenInst_Type_t_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_TuioObjectMapping_t4080927128_0_0_0,
	&GenInst_TuioCursor_t1850351419_0_0_0_TouchPoint_t959629083_0_0_0,
	&GenInst_TuioCursor_t1850351419_0_0_0_TouchPoint_t959629083_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_TuioCursor_t1850351419_0_0_0_TouchPoint_t959629083_0_0_0_KeyValuePair_2_t4169991845_0_0_0,
	&GenInst_KeyValuePair_2_t4169991845_0_0_0,
	&GenInst_TuioBlob_t2046943414_0_0_0_TouchPoint_t959629083_0_0_0,
	&GenInst_TuioBlob_t2046943414_0_0_0_TouchPoint_t959629083_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_TuioBlob_t2046943414_0_0_0_TouchPoint_t959629083_0_0_0_KeyValuePair_2_t2907529438_0_0_0,
	&GenInst_KeyValuePair_2_t2907529438_0_0_0,
	&GenInst_TuioObject_t1236821014_0_0_0_TouchPoint_t959629083_0_0_0,
	&GenInst_TuioObject_t1236821014_0_0_0_TouchPoint_t959629083_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_TuioObject_t1236821014_0_0_0_TouchPoint_t959629083_0_0_0_KeyValuePair_2_t2840757246_0_0_0,
	&GenInst_KeyValuePair_2_t2840757246_0_0_0,
	&GenInst_TouchEventArgs_t1917927166_0_0_0,
	&GenInst_TouchPoint_t959629083_0_0_0,
	&GenInst_ITransformGesture_t1540584490_0_0_0,
	&GenInst_Gesture_t2352305985_0_0_0,
	&GenInst_DebuggableMonoBehaviour_t3136086048_0_0_0,
	&GenInst_IDebuggable_t2346276731_0_0_0,
	&GenInst_TouchProxyBase_t4188753234_0_0_0,
	&GenInst_TouchProxyBase_t4188753234_0_0_0_TouchProxyBase_t4188753234_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_TouchProxyBase_t4188753234_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_TouchProxyBase_t4188753234_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_TouchProxyBase_t4188753234_0_0_0_KeyValuePair_2_t953924091_0_0_0,
	&GenInst_KeyValuePair_2_t953924091_0_0_0,
	&GenInst_GestureManagerInstance_t505647059_0_0_0,
	&GenInst_IGestureManager_t4266705231_0_0_0,
	&GenInst_Gesture_t2352305985_0_0_0_IList_1_t1500569684_0_0_0,
	&GenInst_Transform_t3275118058_0_0_0_List_1_t328750215_0_0_0,
	&GenInst_Transform_t3275118058_0_0_0_List_1_t328750215_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Transform_t3275118058_0_0_0_List_1_t328750215_0_0_0_KeyValuePair_2_t2122717638_0_0_0,
	&GenInst_KeyValuePair_2_t2122717638_0_0_0,
	&GenInst_Gesture_t2352305985_0_0_0_List_1_t328750215_0_0_0,
	&GenInst_Gesture_t2352305985_0_0_0_List_1_t328750215_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Gesture_t2352305985_0_0_0_List_1_t328750215_0_0_0_KeyValuePair_2_t2725989683_0_0_0,
	&GenInst_KeyValuePair_2_t2725989683_0_0_0,
	&GenInst_List_1_t1721427117_0_0_0,
	&GenInst_List_1_t1721427117_0_0_0_List_1_t1721427117_0_0_0,
	&GenInst_List_1_t328750215_0_0_0,
	&GenInst_List_1_t328750215_0_0_0_List_1_t328750215_0_0_0,
	&GenInst_List_1_t2644239190_0_0_0,
	&GenInst_List_1_t2644239190_0_0_0_List_1_t2644239190_0_0_0,
	&GenInst_GestureStateChangeEventArgs_t3499981191_0_0_0,
	&GenInst_MetaGestureEventArgs_t256591615_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_TouchData_t3880599975_0_0_0,
	&GenInst_KeyValuePair_2_t645770832_0_0_0,
	&GenInst_TouchData_t3880599975_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_TouchData_t3880599975_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_TouchData_t3880599975_0_0_0_TouchData_t3880599975_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_TouchData_t3880599975_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_TouchData_t3880599975_0_0_0_KeyValuePair_2_t645770832_0_0_0,
	&GenInst_Vector2_t2243707579_0_0_0_Tags_t1265380163_0_0_0_Boolean_t3825574718_0_0_0_TouchPoint_t959629083_0_0_0,
	&GenInst_Vector2_t2243707579_0_0_0_RuntimeObject_0_0_0_Boolean_t3825574718_0_0_0_RuntimeObject_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Vector2_t2243707579_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_TouchState_t2732082299_0_0_0,
	&GenInst_KeyValuePair_2_t3792220452_0_0_0,
	&GenInst_TouchState_t2732082299_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_TouchState_t2732082299_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_TouchState_t2732082299_0_0_0_TouchState_t2732082299_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_TouchState_t2732082299_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_TouchState_t2732082299_0_0_0_KeyValuePair_2_t3792220452_0_0_0,
	&GenInst_TouchLayer_t2635439978_0_0_0,
	&GenInst_IInputSource_t3266560338_0_0_0,
	&GenInst_HitTest_t768639505_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_KeyValuePair_2_t3132015601_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3132015601_0_0_0,
	&GenInst_TouchLayerEventArgs_t1247401065_0_0_0,
	&GenInst_Canvas_t209405766_0_0_0_ProjectionParams_t2712959773_0_0_0,
	&GenInst_Canvas_t209405766_0_0_0_ProjectionParams_t2712959773_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Canvas_t209405766_0_0_0_ProjectionParams_t2712959773_0_0_0_KeyValuePair_2_t523301392_0_0_0,
	&GenInst_KeyValuePair_2_t523301392_0_0_0,
	&GenInst_TouchManagerInstance_t2629118981_0_0_0,
	&GenInst_ITouchManager_t2552034033_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_TouchPoint_t959629083_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_TouchPoint_t959629083_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_TouchPoint_t959629083_0_0_0_KeyValuePair_2_t2019767236_0_0_0,
	&GenInst_KeyValuePair_2_t2019767236_0_0_0,
	&GenInst_TouchPoint_t959629083_0_0_0_TouchPoint_t959629083_0_0_0,
	&GenInst_List_1_t1440998580_0_0_0_List_1_t1440998580_0_0_0,
	&GenInst_TouchManager_t3980263048_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Exception_t1927440687_0_0_0,
	&GenInst_StringU5BU5D_t1642385972_0_0_0_RuntimeObject_0_0_0,
	&GenInst_StringU5BU5D_t1642385972_0_0_0_HttpResponse_t2820540315_0_0_0,
	&GenInst_StringU5BU5D_t1642385972_0_0_0_HttpRequest_t1845443631_0_0_0,
	&GenInst_HttpResponse_t2820540315_0_0_0,
	&GenInst_HttpRequest_t1845443631_0_0_0,
	&GenInst_Cookie_t1826188460_0_0_0,
	&GenInst_LogData_t4095822710_0_0_0_String_t_0_0_0,
	&GenInst_Chunk_t2303927151_0_0_0,
	&GenInst_HttpListenerPrefix_t529778486_0_0_0,
	&GenInst_HttpListenerPrefix_t529778486_0_0_0_HttpListener_t4179429670_0_0_0,
	&GenInst_HttpListenerPrefix_t529778486_0_0_0_HttpListener_t4179429670_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_HttpListenerPrefix_t529778486_0_0_0_HttpListener_t4179429670_0_0_0_KeyValuePair_2_t2112148073_0_0_0,
	&GenInst_KeyValuePair_2_t2112148073_0_0_0,
	&GenInst_HttpConnection_t2649486862_0_0_0_HttpConnection_t2649486862_0_0_0,
	&GenInst_HttpConnection_t2649486862_0_0_0,
	&GenInst_HttpConnection_t2649486862_0_0_0_HttpConnection_t2649486862_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_HttpConnection_t2649486862_0_0_0_HttpConnection_t2649486862_0_0_0_KeyValuePair_2_t1689813529_0_0_0,
	&GenInst_KeyValuePair_2_t1689813529_0_0_0,
	&GenInst_HttpListenerPrefix_t529778486_0_0_0_HttpListener_t4179429670_0_0_0_HttpListenerPrefix_t529778486_0_0_0,
	&GenInst_HttpConnection_t2649486862_0_0_0_HttpConnection_t2649486862_0_0_0_HttpConnection_t2649486862_0_0_0,
	&GenInst_IPAddress_t1399971723_0_0_0_Dictionary_2_t2945377568_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_EndPointListener_t3937551933_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_EndPointListener_t3937551933_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_EndPointListener_t3937551933_0_0_0_KeyValuePair_2_t702722790_0_0_0,
	&GenInst_KeyValuePair_2_t702722790_0_0_0,
	&GenInst_IPAddress_t1399971723_0_0_0_Dictionary_2_t2945377568_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_IPAddress_t1399971723_0_0_0_Dictionary_2_t2945377568_0_0_0_KeyValuePair_2_t767379738_0_0_0,
	&GenInst_KeyValuePair_2_t767379738_0_0_0,
	&GenInst_HttpListenerRequest_t2316381291_0_0_0_AuthenticationSchemes_t29593226_0_0_0,
	&GenInst_RuntimeObject_0_0_0_AuthenticationSchemes_t29593226_0_0_0,
	&GenInst_IIdentity_t2445095625_0_0_0_NetworkCredential_t3911206805_0_0_0,
	&GenInst_HttpListenerContext_t994708409_0_0_0,
	&GenInst_HttpListenerContext_t994708409_0_0_0_HttpListenerContext_t994708409_0_0_0,
	&GenInst_HttpListenerContext_t994708409_0_0_0_HttpListenerContext_t994708409_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_HttpListenerContext_t994708409_0_0_0_HttpListenerContext_t994708409_0_0_0_KeyValuePair_2_t138893069_0_0_0,
	&GenInst_KeyValuePair_2_t138893069_0_0_0,
	&GenInst_HttpListenerAsyncResult_t3506939685_0_0_0,
	&GenInst_HttpListenerContext_t994708409_0_0_0_HttpListenerContext_t994708409_0_0_0_HttpListenerContext_t994708409_0_0_0,
	&GenInst_String_t_0_0_0_Char_t3454481338_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Char_t3454481338_0_0_0,
	&GenInst_KeyValuePair_2_t803886688_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Char_t3454481338_0_0_0_RuntimeObject_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Char_t3454481338_0_0_0_Char_t3454481338_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Char_t3454481338_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Char_t3454481338_0_0_0_KeyValuePair_2_t803886688_0_0_0,
	&GenInst_String_t_0_0_0_Char_t3454481338_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_String_t_0_0_0_Char_t3454481338_0_0_0_KeyValuePair_2_t3126605822_0_0_0,
	&GenInst_KeyValuePair_2_t3126605822_0_0_0,
	&GenInst_ByteU5BU5D_t3397334013_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_String_t_0_0_0_HttpHeaderInfo_t2096319561_0_0_0,
	&GenInst_String_t_0_0_0_HttpHeaderInfo_t2096319561_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_String_t_0_0_0_HttpHeaderInfo_t2096319561_0_0_0_KeyValuePair_2_t1768444045_0_0_0,
	&GenInst_KeyValuePair_2_t1768444045_0_0_0,
	&GenInst_HttpHeaderInfo_t2096319561_0_0_0,
	&GenInst_String_t_0_0_0_HttpHeaderInfo_t2096319561_0_0_0_HttpHeaderInfo_t2096319561_0_0_0,
	&GenInst_HttpRequestEventArgs_t918469868_0_0_0,
	&GenInst_String_t_0_0_0_WebSocketServiceHost_t492106494_0_0_0,
	&GenInst_String_t_0_0_0_WebSocketServiceHost_t492106494_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_String_t_0_0_0_WebSocketServiceHost_t492106494_0_0_0_KeyValuePair_2_t164230978_0_0_0,
	&GenInst_KeyValuePair_2_t164230978_0_0_0,
	&GenInst_CookieCollection_t4248997468_0_0_0_CookieCollection_t4248997468_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_WebSocketContext_t3488732344_0_0_0_String_t_0_0_0,
	&GenInst_WebSocketServiceHost_t492106494_0_0_0,
	&GenInst_String_t_0_0_0_WebSocketServiceHost_t492106494_0_0_0_WebSocketServiceHost_t492106494_0_0_0,
	&GenInst_CompressionMethod_t4066553457_0_0_0_ByteU5BU5D_t3397334013_0_0_0,
	&GenInst_CompressionMethod_t4066553457_0_0_0_RuntimeObject_0_0_0,
	&GenInst_KeyValuePair_2_t2919672843_0_0_0,
	&GenInst_CompressionMethod_t4066553457_0_0_0,
	&GenInst_CompressionMethod_t4066553457_0_0_0_RuntimeObject_0_0_0_CompressionMethod_t4066553457_0_0_0,
	&GenInst_CompressionMethod_t4066553457_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0,
	&GenInst_CompressionMethod_t4066553457_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_CompressionMethod_t4066553457_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t2919672843_0_0_0,
	&GenInst_CompressionMethod_t4066553457_0_0_0_ByteU5BU5D_t3397334013_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_CompressionMethod_t4066553457_0_0_0_ByteU5BU5D_t3397334013_0_0_0_KeyValuePair_2_t3627557561_0_0_0,
	&GenInst_KeyValuePair_2_t3627557561_0_0_0,
	&GenInst_CompressionMethod_t4066553457_0_0_0_Stream_t3255436806_0_0_0,
	&GenInst_CompressionMethod_t4066553457_0_0_0_Stream_t3255436806_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_CompressionMethod_t4066553457_0_0_0_Stream_t3255436806_0_0_0_KeyValuePair_2_t3485660354_0_0_0,
	&GenInst_KeyValuePair_2_t3485660354_0_0_0,
	&GenInst_Stream_t3255436806_0_0_0,
	&GenInst_CompressionMethod_t4066553457_0_0_0_Stream_t3255436806_0_0_0_Stream_t3255436806_0_0_0,
	&GenInst_String_t_0_0_0_Dictionary_2_t1445386684_0_0_0,
	&GenInst_String_t_0_0_0_Dictionary_2_t1445386684_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_String_t_0_0_0_Dictionary_2_t1445386684_0_0_0_KeyValuePair_2_t1117511168_0_0_0,
	&GenInst_KeyValuePair_2_t1117511168_0_0_0,
	&GenInst_String_t_0_0_0_IWebSocketSession_t1432056748_0_0_0,
	&GenInst_String_t_0_0_0_IWebSocketSession_t1432056748_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_String_t_0_0_0_IWebSocketSession_t1432056748_0_0_0_KeyValuePair_2_t1104181232_0_0_0,
	&GenInst_KeyValuePair_2_t1104181232_0_0_0,
	&GenInst_IWebSocketSession_t1432056748_0_0_0,
	&GenInst_CloseEventArgs_t344507773_0_0_0_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Opcode_t2313788840_0_0_0_Stream_t3255436806_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Opcode_t2313788840_0_0_0_RuntimeObject_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_WebSocketFrame_t764750278_0_0_0,
	&GenInst_Action_4_t3853297115_0_0_0,
	&GenInst_String_t_0_0_0_String_t_0_0_0_String_t_0_0_0_String_t_0_0_0,
	&GenInst_SpeechbubblePrefab_t736279730_0_0_0,
	&GenInst_DialogueLine_t768215248_0_0_0,
	&GenInst_SpeechbubbleType_t1311649088_0_0_0_GameObject_t1756533147_0_0_0,
	&GenInst_SpeechbubbleType_t1311649088_0_0_0_RuntimeObject_0_0_0,
	&GenInst_KeyValuePair_2_t1971397920_0_0_0,
	&GenInst_SpeechbubbleType_t1311649088_0_0_0,
	&GenInst_SpeechbubbleType_t1311649088_0_0_0_RuntimeObject_0_0_0_SpeechbubbleType_t1311649088_0_0_0,
	&GenInst_SpeechbubbleType_t1311649088_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0,
	&GenInst_SpeechbubbleType_t1311649088_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_SpeechbubbleType_t1311649088_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t1971397920_0_0_0,
	&GenInst_SpeechbubbleType_t1311649088_0_0_0_GameObject_t1756533147_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_SpeechbubbleType_t1311649088_0_0_0_GameObject_t1756533147_0_0_0_KeyValuePair_2_t1038481772_0_0_0,
	&GenInst_KeyValuePair_2_t1038481772_0_0_0,
	&GenInst_SpeechbubbleType_t1311649088_0_0_0_Queue_1_t2840177624_0_0_0,
	&GenInst_SpeechbubbleBehaviour_t3020520789_0_0_0,
	&GenInst_SpeechbubbleType_t1311649088_0_0_0_Queue_1_t2840177624_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_SpeechbubbleType_t1311649088_0_0_0_Queue_1_t2840177624_0_0_0_KeyValuePair_2_t2122126249_0_0_0,
	&GenInst_KeyValuePair_2_t2122126249_0_0_0,
	&GenInst_IEnumerable_1_t4048664256_gp_0_0_0_0,
	&GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m801276902_gp_0_0_0_0,
	&GenInst_Array_Sort_m2069139338_gp_0_0_0_0_Array_Sort_m2069139338_gp_0_0_0_0,
	&GenInst_Array_Sort_m1999601238_gp_0_0_0_0_Array_Sort_m1999601238_gp_1_0_0_0,
	&GenInst_Array_Sort_m4168899348_gp_0_0_0_0,
	&GenInst_Array_Sort_m4168899348_gp_0_0_0_0_Array_Sort_m4168899348_gp_0_0_0_0,
	&GenInst_Array_Sort_m1711256281_gp_0_0_0_0,
	&GenInst_Array_Sort_m1711256281_gp_0_0_0_0_Array_Sort_m1711256281_gp_1_0_0_0,
	&GenInst_Array_Sort_m714603628_gp_0_0_0_0_Array_Sort_m714603628_gp_0_0_0_0,
	&GenInst_Array_Sort_m4057794348_gp_0_0_0_0_Array_Sort_m4057794348_gp_1_0_0_0,
	&GenInst_Array_Sort_m2544329074_gp_0_0_0_0,
	&GenInst_Array_Sort_m2544329074_gp_0_0_0_0_Array_Sort_m2544329074_gp_0_0_0_0,
	&GenInst_Array_Sort_m54102389_gp_0_0_0_0,
	&GenInst_Array_Sort_m54102389_gp_1_0_0_0,
	&GenInst_Array_Sort_m54102389_gp_0_0_0_0_Array_Sort_m54102389_gp_1_0_0_0,
	&GenInst_Array_Sort_m2673640633_gp_0_0_0_0,
	&GenInst_Array_Sort_m285045864_gp_0_0_0_0,
	&GenInst_Array_qsort_m965712368_gp_0_0_0_0,
	&GenInst_Array_qsort_m965712368_gp_0_0_0_0_Array_qsort_m965712368_gp_1_0_0_0,
	&GenInst_Array_compare_m1681301885_gp_0_0_0_0,
	&GenInst_Array_qsort_m2216502740_gp_0_0_0_0,
	&GenInst_Array_Resize_m2875826811_gp_0_0_0_0,
	&GenInst_Array_TrueForAll_m1718874735_gp_0_0_0_0,
	&GenInst_Array_ForEach_m1854649314_gp_0_0_0_0,
	&GenInst_Array_ConvertAll_m4274024367_gp_0_0_0_0_Array_ConvertAll_m4274024367_gp_1_0_0_0,
	&GenInst_Array_FindLastIndex_m2314439434_gp_0_0_0_0,
	&GenInst_Array_FindLastIndex_m3151928313_gp_0_0_0_0,
	&GenInst_Array_FindLastIndex_m2323782676_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m1557343438_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m1631399507_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m3415315332_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m2990936537_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m2009305197_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m2427841765_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m3348121441_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m2010744443_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m4126518092_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m3127862871_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m1523107937_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m2200535596_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m555775493_gp_0_0_0_0,
	&GenInst_Array_FindAll_m2331462890_gp_0_0_0_0,
	&GenInst_Array_Exists_m976807351_gp_0_0_0_0,
	&GenInst_Array_AsReadOnly_m884706992_gp_0_0_0_0,
	&GenInst_Array_Find_m1125132281_gp_0_0_0_0,
	&GenInst_Array_FindLast_m4092932075_gp_0_0_0_0,
	&GenInst_InternalEnumerator_1_t3582267753_gp_0_0_0_0,
	&GenInst_ArrayReadOnlyList_1_t3367196019_gp_0_0_0_0,
	&GenInst_U3CGetEnumeratorU3Ec__Iterator0_t1667190089_gp_0_0_0_0,
	&GenInst_IList_1_t3737699284_gp_0_0_0_0,
	&GenInst_ICollection_1_t1552160836_gp_0_0_0_0,
	&GenInst_Nullable_1_t1398937014_gp_0_0_0_0,
	&GenInst_Comparer_1_t1036860714_gp_0_0_0_0,
	&GenInst_DefaultComparer_t3074655092_gp_0_0_0_0,
	&GenInst_GenericComparer_1_t1787398723_gp_0_0_0_0,
	&GenInst_Dictionary_2_t2276497324_gp_0_0_0_0,
	&GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t3180694294_0_0_0,
	&GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m3006662979_gp_0_0_0_0,
	&GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m1628523394_gp_0_0_0_0,
	&GenInst_Dictionary_2_Do_ICollectionCopyTo_m1628523394_gp_0_0_0_0_RuntimeObject_0_0_0,
	&GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_ShimEnumerator_t3895203923_gp_0_0_0_0_ShimEnumerator_t3895203923_gp_1_0_0_0,
	&GenInst_Enumerator_t2089681430_gp_0_0_0_0_Enumerator_t2089681430_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t3434615342_0_0_0,
	&GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0,
	&GenInst_KeyCollection_t1229212677_gp_0_0_0_0,
	&GenInst_Enumerator_t83320710_gp_0_0_0_0_Enumerator_t83320710_gp_1_0_0_0,
	&GenInst_Enumerator_t83320710_gp_0_0_0_0,
	&GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0,
	&GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0,
	&GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0,
	&GenInst_ValueCollection_t2262344653_gp_1_0_0_0,
	&GenInst_Enumerator_t3111723616_gp_0_0_0_0_Enumerator_t3111723616_gp_1_0_0_0,
	&GenInst_Enumerator_t3111723616_gp_1_0_0_0,
	&GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0,
	&GenInst_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0,
	&GenInst_DictionaryEntry_t3048875398_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_KeyValuePair_2_t3180694294_0_0_0,
	&GenInst_KeyValuePair_2_t3180694294_0_0_0_KeyValuePair_2_t3180694294_0_0_0,
	&GenInst_Dictionary_2_t2276497324_gp_1_0_0_0,
	&GenInst_EqualityComparer_1_t2066709010_gp_0_0_0_0,
	&GenInst_DefaultComparer_t1766400012_gp_0_0_0_0,
	&GenInst_GenericEqualityComparer_1_t2202941003_gp_0_0_0_0,
	&GenInst_KeyValuePair_2_t4174120762_0_0_0,
	&GenInst_IDictionary_2_t3502329323_gp_0_0_0_0_IDictionary_2_t3502329323_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t1988958766_gp_0_0_0_0_KeyValuePair_2_t1988958766_gp_1_0_0_0,
	&GenInst_List_1_t1169184319_gp_0_0_0_0,
	&GenInst_Enumerator_t1292967705_gp_0_0_0_0,
	&GenInst_Collection_1_t686054069_gp_0_0_0_0,
	&GenInst_ReadOnlyCollection_1_t3540981679_gp_0_0_0_0,
	&GenInst_MonoProperty_GetterAdapterFrame_m609993266_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m609993266_gp_1_0_0_0,
	&GenInst_MonoProperty_StaticGetterAdapterFrame_m781526467_gp_0_0_0_0,
	&GenInst_ArraySegment_1_t1001032761_gp_0_0_0_0,
	&GenInst_Queue_1_t1458930734_gp_0_0_0_0,
	&GenInst_Enumerator_t4000919638_gp_0_0_0_0,
	&GenInst_Stack_1_t4016656541_gp_0_0_0_0,
	&GenInst_Enumerator_t546412149_gp_0_0_0_0,
	&GenInst_HashSet_1_t2624254809_gp_0_0_0_0,
	&GenInst_Enumerator_t2109956843_gp_0_0_0_0,
	&GenInst_PrimeHelper_t3424417428_gp_0_0_0_0,
	&GenInst_Enumerable_Any_m1395534165_gp_0_0_0_0,
	&GenInst_Enumerable_Any_m4174937590_gp_0_0_0_0,
	&GenInst_Enumerable_Any_m4174937590_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Enumerable_First_m2440166523_gp_0_0_0_0,
	&GenInst_Enumerable_FirstOrDefault_m1564979201_gp_0_0_0_0,
	&GenInst_Enumerable_Select_m791198632_gp_0_0_0_0,
	&GenInst_Enumerable_Select_m791198632_gp_0_0_0_0_Enumerable_Select_m791198632_gp_1_0_0_0,
	&GenInst_Enumerable_Select_m791198632_gp_1_0_0_0,
	&GenInst_Enumerable_CreateSelectIterator_m3979771306_gp_0_0_0_0,
	&GenInst_Enumerable_CreateSelectIterator_m3979771306_gp_0_0_0_0_Enumerable_CreateSelectIterator_m3979771306_gp_1_0_0_0,
	&GenInst_Enumerable_CreateSelectIterator_m3979771306_gp_1_0_0_0,
	&GenInst_Enumerable_Skip_m3785421448_gp_0_0_0_0,
	&GenInst_Enumerable_CreateSkipIterator_m365619102_gp_0_0_0_0,
	&GenInst_Enumerable_ToArray_m747609077_gp_0_0_0_0,
	&GenInst_Enumerable_ToList_m2644368746_gp_0_0_0_0,
	&GenInst_Enumerable_Where_m1602106588_gp_0_0_0_0,
	&GenInst_Enumerable_Where_m1602106588_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Enumerable_CreateWhereIterator_m3985242862_gp_0_0_0_0,
	&GenInst_Enumerable_CreateWhereIterator_m3985242862_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0,
	&GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0,
	&GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0,
	&GenInst_U3CCreateSkipIteratorU3Ec__Iterator16_1_t284123852_gp_0_0_0_0,
	&GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0,
	&GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Component_GetComponentInChildren_m1236184570_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m4167514082_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m4085536263_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m84946435_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m989421820_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInParent_m893247195_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInParent_m2646750694_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInParent_m3278624542_gp_0_0_0_0,
	&GenInst_Component_GetComponents_m1252126604_gp_0_0_0_0,
	&GenInst_Component_GetComponents_m1618642439_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentInChildren_m477715742_gp_0_0_0_0,
	&GenInst_GameObject_GetComponents_m3045349666_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInChildren_m1567454139_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInChildren_m1151940851_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInParent_m4028060632_gp_0_0_0_0,
	&GenInst_Mesh_GetAllocArrayFromChannel_m4154891606_gp_0_0_0_0,
	&GenInst_Mesh_SafeLength_m1789465785_gp_0_0_0_0,
	&GenInst_Mesh_SetListForChannel_m2929091838_gp_0_0_0_0,
	&GenInst_Mesh_SetListForChannel_m3597787760_gp_0_0_0_0,
	&GenInst_Mesh_SetUvsImpl_m2321318716_gp_0_0_0_0,
	&GenInst_Object_Instantiate_m2766660278_gp_0_0_0_0,
	&GenInst_Object_FindObjectsOfType_m488103269_gp_0_0_0_0,
	&GenInst_Playable_IsPlayableOfType_m140883719_gp_0_0_0_0,
	&GenInst_PlayableOutput_IsPlayableOutputOfType_m2834483815_gp_0_0_0_0,
	&GenInst_InvokableCall_1_t476640868_gp_0_0_0_0,
	&GenInst_UnityAction_1_t2490859068_0_0_0,
	&GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0_InvokableCall_2_t2042724809_gp_1_0_0_0,
	&GenInst_UnityAction_2_t601835599_0_0_0,
	&GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0,
	&GenInst_InvokableCall_2_t2042724809_gp_1_0_0_0,
	&GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0_InvokableCall_3_t3608808750_gp_1_0_0_0_InvokableCall_3_t3608808750_gp_2_0_0_0,
	&GenInst_UnityAction_3_t155920421_0_0_0,
	&GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0,
	&GenInst_InvokableCall_3_t3608808750_gp_1_0_0_0,
	&GenInst_InvokableCall_3_t3608808750_gp_2_0_0_0,
	&GenInst_InvokableCall_4_t879925395_gp_0_0_0_0_InvokableCall_4_t879925395_gp_1_0_0_0_InvokableCall_4_t879925395_gp_2_0_0_0_InvokableCall_4_t879925395_gp_3_0_0_0,
	&GenInst_InvokableCall_4_t879925395_gp_0_0_0_0,
	&GenInst_InvokableCall_4_t879925395_gp_1_0_0_0,
	&GenInst_InvokableCall_4_t879925395_gp_2_0_0_0,
	&GenInst_InvokableCall_4_t879925395_gp_3_0_0_0,
	&GenInst_CachedInvokableCall_1_t224769006_gp_0_0_0_0,
	&GenInst_UnityEvent_1_t4075366602_gp_0_0_0_0,
	&GenInst_UnityEvent_2_t4075366599_gp_0_0_0_0_UnityEvent_2_t4075366599_gp_1_0_0_0,
	&GenInst_UnityEvent_3_t4075366600_gp_0_0_0_0_UnityEvent_3_t4075366600_gp_1_0_0_0_UnityEvent_3_t4075366600_gp_2_0_0_0,
	&GenInst_UnityEvent_4_t4075366597_gp_0_0_0_0_UnityEvent_4_t4075366597_gp_1_0_0_0_UnityEvent_4_t4075366597_gp_2_0_0_0_UnityEvent_4_t4075366597_gp_3_0_0_0,
	&GenInst_List_1_t2244783541_gp_0_0_0_0,
	&GenInst_U3CGetEnumeratorU3Ec__Iterator6_t1336800315_gp_0_0_0_0,
	&GenInst_List_1_t264254312_0_0_0,
	&GenInst_Utilities_GetDictionaryValueOrNull_m3096514106_gp_0_0_0_0_Utilities_GetDictionaryValueOrNull_m3096514106_gp_1_0_0_0,
	&GenInst_ExecuteEvents_Execute_m1744114673_gp_0_0_0_0,
	&GenInst_ExecuteEvents_ExecuteHierarchy_m3091757213_gp_0_0_0_0,
	&GenInst_ExecuteEvents_GetEventList_m2263439269_gp_0_0_0_0,
	&GenInst_ExecuteEvents_CanHandleEvent_m3190794379_gp_0_0_0_0,
	&GenInst_ExecuteEvents_GetEventHandler_m3792320302_gp_0_0_0_0,
	&GenInst_TweenRunner_1_t2584777480_gp_0_0_0_0,
	&GenInst_Dropdown_GetOrAddComponent_m41832880_gp_0_0_0_0,
	&GenInst_SetPropertyUtility_SetStruct_m1212286169_gp_0_0_0_0,
	&GenInst_IndexedSet_1_t573160278_gp_0_0_0_0,
	&GenInst_IndexedSet_1_t573160278_gp_0_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_ListPool_1_t1984115411_gp_0_0_0_0,
	&GenInst_List_1_t2000868992_0_0_0,
	&GenInst_ObjectPool_1_t4265859154_gp_0_0_0_0,
	&GenInst_Singleton_1_t2363452800_gp_0_0_0_0,
	&GenInst_Utility_FindObjects_m2142378685_gp_0_0_0_0,
	&GenInst_Utility_GetFromCache_m3302286621_gp_0_0_0_0,
	&GenInst_Utility_DeserializeResponse_m1146239540_gp_0_0_0_0,
	&GenInst_Utility_DeserializeResponse_m549829201_gp_0_0_0_0,
	&GenInst_Widget_GetMembersByType_m59718008_gp_0_0_0_0,
	&GenInst_fsEnumConverter_ArrayContains_m409985959_gp_0_0_0_0,
	&GenInst_fsDirectConverter_1_t3257037661_gp_0_0_0_0,
	&GenInst_fsOption_1_t4013878301_gp_0_0_0_0,
	&GenInst_fsOption_Just_m130823817_gp_0_0_0_0,
	&GenInst_fsPortableReflection_GetAttribute_m2688546569_gp_0_0_0_0,
	&GenInst_EventHandlerExtensions_InvokeHandleExceptions_m2334443745_gp_0_0_0_0,
	&GenInst_ObjectPool_1_t3254757658_gp_0_0_0_0_ObjectPool_1_t3254757658_gp_0_0_0_0,
	&GenInst_ObjectPool_1_t3254757658_gp_0_0_0_0,
	&GenInst_TimedSequence_1_t2332636227_gp_0_0_0_0,
	&GenInst_Ext_Contains_m2286382361_gp_0_0_0_0,
	&GenInst_Ext_Contains_m2286382361_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Ext_ToList_m3286762625_gp_0_0_0_0,
	&GenInst_Ext_Emit_m2311426299_gp_0_0_0_0,
	&GenInst_Ext_ToString_m3280318684_gp_0_0_0_0,
	&GenInst_StringU5BU5D_t1642385972_0_0_0_HttpBase_Read_m191930573_gp_0_0_0_0,
	&GenInst_HttpBase_Read_m191930573_gp_0_0_0_0,
	&GenInst_HttpServer_AddWebSocketService_m682758425_gp_0_0_0_0,
	&GenInst_HttpServer_AddWebSocketService_m2770388401_gp_0_0_0_0,
	&GenInst_HttpServer_U3CAddWebSocketService_1U3Em__0_m662434750_gp_0_0_0_0,
	&GenInst_WebSocketServer_AddWebSocketService_m2616226100_gp_0_0_0_0,
	&GenInst_WebSocketServer_AddWebSocketService_m1120545350_gp_0_0_0_0,
	&GenInst_WebSocketServer_U3CAddWebSocketService_1U3Em__0_m3359671309_gp_0_0_0_0,
	&GenInst_WebSocketServiceHost_1_t1698079305_gp_0_0_0_0,
	&GenInst_WebSocketServiceManager_Add_m3346916827_gp_0_0_0_0,
	&GenInst_OscTimeTag_t625345318_0_0_0,
	&GenInst_AnimationPlayableOutput_t260357453_0_0_0,
	&GenInst_DefaultExecutionOrder_t2717914595_0_0_0,
	&GenInst_AudioPlayableOutput_t3110750149_0_0_0,
	&GenInst_PlayerConnection_t3517219175_0_0_0,
	&GenInst_ScriptPlayableOutput_t1666207618_0_0_0,
	&GenInst_GUILayer_t3254902478_0_0_0,
	&GenInst_EventSystem_t3466835263_0_0_0,
	&GenInst_AxisEventData_t1524870173_0_0_0,
	&GenInst_SpriteRenderer_t1209076198_0_0_0,
	&GenInst_Image_t2042527209_0_0_0,
	&GenInst_Button_t2872111280_0_0_0,
	&GenInst_RawImage_t2749640213_0_0_0,
	&GenInst_Slider_t297367283_0_0_0,
	&GenInst_Scrollbar_t3248359358_0_0_0,
	&GenInst_InputField_t1631627530_0_0_0,
	&GenInst_ScrollRect_t1199013257_0_0_0,
	&GenInst_Dropdown_t1985816271_0_0_0,
	&GenInst_GraphicRaycaster_t410733016_0_0_0,
	&GenInst_CanvasRenderer_t261436805_0_0_0,
	&GenInst_Corner_t1077473318_0_0_0,
	&GenInst_Axis_t1431825778_0_0_0,
	&GenInst_Constraint_t3558160636_0_0_0,
	&GenInst_SubmitEvent_t907918422_0_0_0,
	&GenInst_OnChangeEvent_t2863344003_0_0_0,
	&GenInst_OnValidateInput_t1946318473_0_0_0,
	&GenInst_LayoutElement_t2808691390_0_0_0,
	&GenInst_RectOffset_t3387826427_0_0_0,
	&GenInst_TextAnchor_t112990806_0_0_0,
	&GenInst_AnimationTriggers_t3244928895_0_0_0,
	&GenInst_UnityARVideo_t2351297253_0_0_0,
	&GenInst_DontDestroyOnLoad_t3235789354_0_0_0,
	&GenInst_MeshFilter_t3026937449_0_0_0,
	&GenInst_MeshRenderer_t1268241104_0_0_0,
	&GenInst_AudioSource_t1135106623_0_0_0,
	&GenInst_Dictionary_2_t203617571_0_0_0,
	&GenInst_List_1_t1952926737_0_0_0,
	&GenInst_fsObjectAttribute_t2382840370_0_0_0,
	&GenInst_fsForwardAttribute_t3349051188_0_0_0,
	&GenInst_KeyframeU5BU5D_t449065829_0_0_0,
	&GenInst_WrapMode_t255797857_0_0_0,
	&GenInst_GradientAlphaKeyU5BU5D_t3410745760_0_0_0,
	&GenInst_GradientColorKeyU5BU5D_t3863936331_0_0_0,
	&GenInst_FlagsAttribute_t859561169_0_0_0,
	&GenInst_fsPropertyAttribute_t4237399860_0_0_0,
	&GenInst_WatsonCamera_t824577261_0_0_0,
	&GenInst_MicrophoneWidget_t2442369682_0_0_0,
	&GenInst_Problem_t2814813345_0_0_0,
	&GenInst_IFF_FORM_CHUNK_t624775701_0_0_0,
	&GenInst_IFF_CHUNK_t4256546638_0_0_0,
	&GenInst_WAV_PCM_t2882996802_0_0_0,
	&GenInst_BoxSlider_t1871650694_0_0_0,
	&GenInst_GenericDisplayDevice_t579608737_0_0_0,
	&GenInst_CameraLayer_t464507322_0_0_0,
	&GenInst_StandardInput_t4102879489_0_0_0,
	&GenInst_Light_t494725636_0_0_0,
	&GenInst_Rigidbody_t4233889191_0_0_0,
	&GenInst_AotCompilation_t21908242_0_0_0_AotCompilation_t21908242_0_0_0,
	&GenInst_fsVersionedType_t654750358_0_0_0_fsVersionedType_t654750358_0_0_0,
	&GenInst_Byte_t3683104436_0_0_0_Byte_t3683104436_0_0_0,
	&GenInst_KeyValuePair_2_t38854645_0_0_0_KeyValuePair_2_t38854645_0_0_0,
	&GenInst_CustomAttributeNamedArgument_t94157543_0_0_0_CustomAttributeNamedArgument_t94157543_0_0_0,
	&GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0_CustomAttributeTypedArgument_t1498197914_0_0_0,
	&GenInst_AnimatorClipInfo_t3905751349_0_0_0_AnimatorClipInfo_t3905751349_0_0_0,
	&GenInst_Color32_t874517518_0_0_0_Color32_t874517518_0_0_0,
	&GenInst_RaycastResult_t21186376_0_0_0_RaycastResult_t21186376_0_0_0,
	&GenInst_RaycastHit_t87180320_0_0_0_RaycastHit_t87180320_0_0_0,
	&GenInst_RaycastHit2D_t4063908774_0_0_0_RaycastHit2D_t4063908774_0_0_0,
	&GenInst_UICharInfo_t3056636800_0_0_0_UICharInfo_t3056636800_0_0_0,
	&GenInst_UILineInfo_t3621277874_0_0_0_UILineInfo_t3621277874_0_0_0,
	&GenInst_UIVertex_t1204258818_0_0_0_UIVertex_t1204258818_0_0_0,
	&GenInst_Vector2_t2243707579_0_0_0_Vector2_t2243707579_0_0_0,
	&GenInst_Vector3_t2243707580_0_0_0_Vector3_t2243707580_0_0_0,
	&GenInst_Vector4_t2243707581_0_0_0_Vector4_t2243707581_0_0_0,
	&GenInst_ARHitTestResult_t3275513025_0_0_0_ARHitTestResult_t3275513025_0_0_0,
	&GenInst_AttributeQuery_t604298480_0_0_0_AttributeQuery_t604298480_0_0_0,
	&GenInst_KeyValuePair_2_t288805040_0_0_0_KeyValuePair_2_t288805040_0_0_0,
	&GenInst_KeyValuePair_2_t288805040_0_0_0_RuntimeObject_0_0_0,
	&GenInst_AudioFormatType_t2756784245_0_0_0_AudioFormatType_t2756784245_0_0_0,
	&GenInst_KeyValuePair_2_t993946999_0_0_0_KeyValuePair_2_t993946999_0_0_0,
	&GenInst_KeyValuePair_2_t993946999_0_0_0_RuntimeObject_0_0_0,
	&GenInst_VoiceType_t981025524_0_0_0_VoiceType_t981025524_0_0_0,
	&GenInst_KeyValuePair_2_t4282424860_0_0_0_KeyValuePair_2_t4282424860_0_0_0,
	&GenInst_KeyValuePair_2_t4282424860_0_0_0_RuntimeObject_0_0_0,
	&GenInst_KeyValuePair_2_t3132015601_0_0_0_KeyValuePair_2_t3132015601_0_0_0,
	&GenInst_KeyValuePair_2_t3132015601_0_0_0_RuntimeObject_0_0_0,
	&GenInst_KeyValuePair_2_t3749587448_0_0_0_KeyValuePair_2_t3749587448_0_0_0,
	&GenInst_KeyValuePair_2_t3749587448_0_0_0_RuntimeObject_0_0_0,
	&GenInst_KeyValuePair_2_t645770832_0_0_0_KeyValuePair_2_t645770832_0_0_0,
	&GenInst_KeyValuePair_2_t645770832_0_0_0_RuntimeObject_0_0_0,
	&GenInst_TouchData_t3880599975_0_0_0_RuntimeObject_0_0_0,
	&GenInst_TouchData_t3880599975_0_0_0_TouchData_t3880599975_0_0_0,
	&GenInst_KeyValuePair_2_t3792220452_0_0_0_KeyValuePair_2_t3792220452_0_0_0,
	&GenInst_KeyValuePair_2_t3792220452_0_0_0_RuntimeObject_0_0_0,
	&GenInst_TouchState_t2732082299_0_0_0_RuntimeObject_0_0_0,
	&GenInst_TouchState_t2732082299_0_0_0_TouchState_t2732082299_0_0_0,
	&GenInst_KeyValuePair_2_t888819835_0_0_0_KeyValuePair_2_t888819835_0_0_0,
	&GenInst_KeyValuePair_2_t888819835_0_0_0_RuntimeObject_0_0_0,
	&GenInst_IntPtr_t_0_0_0_IntPtr_t_0_0_0,
	&GenInst_fsOption_1_t972008496_0_0_0_fsOption_1_t972008496_0_0_0,
	&GenInst_fsOption_1_t972008496_0_0_0_RuntimeObject_0_0_0,
	&GenInst_KeyValuePair_2_t2616381142_0_0_0_KeyValuePair_2_t2616381142_0_0_0,
	&GenInst_KeyValuePair_2_t2616381142_0_0_0_RuntimeObject_0_0_0,
	&GenInst_HtmlElementFlag_t260274357_0_0_0_HtmlElementFlag_t260274357_0_0_0,
	&GenInst_HtmlElementFlag_t260274357_0_0_0_RuntimeObject_0_0_0,
	&GenInst_KeyValuePair_2_t1904647003_0_0_0_KeyValuePair_2_t1904647003_0_0_0,
	&GenInst_KeyValuePair_2_t1904647003_0_0_0_RuntimeObject_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_KeyValuePair_2_t1174980068_0_0_0_KeyValuePair_2_t1174980068_0_0_0,
	&GenInst_KeyValuePair_2_t1174980068_0_0_0_RuntimeObject_0_0_0,
	&GenInst_Char_t3454481338_0_0_0_Char_t3454481338_0_0_0,
	&GenInst_Char_t3454481338_0_0_0_RuntimeObject_0_0_0,
	&GenInst_KeyValuePair_2_t803886688_0_0_0_KeyValuePair_2_t803886688_0_0_0,
	&GenInst_KeyValuePair_2_t803886688_0_0_0_RuntimeObject_0_0_0,
	&GenInst_KeyValuePair_2_t3716250094_0_0_0_KeyValuePair_2_t3716250094_0_0_0,
	&GenInst_KeyValuePair_2_t3716250094_0_0_0_RuntimeObject_0_0_0,
	&GenInst_KeyValuePair_2_t38854645_0_0_0_RuntimeObject_0_0_0,
	&GenInst_KeyValuePair_2_t1971397920_0_0_0_KeyValuePair_2_t1971397920_0_0_0,
	&GenInst_KeyValuePair_2_t1971397920_0_0_0_RuntimeObject_0_0_0,
	&GenInst_SpeechbubbleType_t1311649088_0_0_0_SpeechbubbleType_t1311649088_0_0_0,
	&GenInst_KeyValuePair_2_t2919672843_0_0_0_KeyValuePair_2_t2919672843_0_0_0,
	&GenInst_KeyValuePair_2_t2919672843_0_0_0_RuntimeObject_0_0_0,
	&GenInst_CompressionMethod_t4066553457_0_0_0_CompressionMethod_t4066553457_0_0_0,
};
